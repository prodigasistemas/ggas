def LABEL_ID = "${env.JOB_NAME}-${UUID.randomUUID().toString()}"
def ENVIRONMENT = "staging"

properties([
    parameters([
        gitParameter(branch: '',
                     branchFilter: 'origin/(.*)',
                     defaultValue: 'master',
                     description: '',
                     name: 'BRANCH',
                     quickFilterEnabled: false,
                     selectedValue: 'NONE',
                     sortMode: 'NONE',
                     tagFilter: '*',
                     type: 'PT_BRANCH')
    ])
])

podTemplate(
        name: "${env.JOB_NAME}",
        namespace: "${ENVIRONMENT}", 
        label: LABEL_ID,
) 
{    
    node(LABEL_ID) {
        
        stage('SCM') {    
            echo 'Iniciando Clone do Repositório.'
            checkout scm
        }

        stage('Compile') {
            sh './gradlew compileJava'
        }
        
        stage('SonarQube Analysis') {
            def ggasProperties = readProperties  file: 'src/main/resources/constantes.properties'
            def scannerHome = tool name: 'sonarqube-scanner'

            withSonarQubeEnv('sonarqube') {
                sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectVersion=${ggasProperties.VERSAO_BUILD} -Dsonar.projectKey=${JOB_NAME} -Dsonar.sources=src/main/java -Dsonar.java.binaries=build/classes/main -Dsonar.scm.disabled=true"
            }
        }
    }
        
    stage("Quality Gate"){
        timeout(time: 5, unit: 'MINUTES') {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
                error "Pipeline abortado devido a uma falha no quality gate: ${qg.status}"
            }
        }
    }
}
