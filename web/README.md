Para visualizar o conteúdo deste arquivo, utilize um visualizador de markdown ou cole o conteúdo deste arquivo em https://dillinger.io/

# Sobre o diretório /web

O diretório _/web_ contém o _source_ da nova infraestrtura Front End do GGAS. 
Para utilizá-lo e modificar o código gerado por essa infraestrutura, é necessário instalar o [NodeJS](https://nodejs.org/en/download/) (Versão >= 8.11.4). 
É utilizado o [npm](https://www.npmjs.com/) para gerenciar as dependências de bibliotecas JavaScript, todas as dependências instaladas, versões e configurações estão detalhadas no package.json.


### Como alterar e contribuir com o código 

  - Instale o NodeJS (v >= 8.11.4) na sua máquina de desenvolvimento: https://nodejs.org/en/download/
  - Após a instalação, abra um terminal do Linux/MacOS ou um prompt de comando do Windows e execute os seguintes passos: 
    - Navegue até a pasta `/web` localizada dentro odo diretório raiz do projeto
    - Execute `npm install` -> Este comando irá baixar as dependências listadas no package.json na máquina de desenvolvimento. As dependências baixadas são todas armazenadas na pasta node_modules. Essa pasta foi adicionada ao .gitignore e não deve ser versionada. O npm install só é necessário executar na primeira vez que o ambiente de desenvolvimento é configurado.
    - `npm start` -> Este comando irá iniciar o servidor de desenvolvimento do webpack e servirá os arquivos compilados no endereço http://localhost:9000 . Toda vez que um arquivo .js, .ts, .tsx, .css ou .scss dentro da pasta `/web` for modificado e salvo, o webpack recompilará o código automaticamente enquanto esse serviço estiver rodando. 
    - Será necessário também ativar a flag de desenvolvimento, para isso altere a propriedade `IS_AMBIENTE_PRODUCAO` para false no arquivo `src/main/resources/constantes.properties`.

### Como referenciar os javascripts compilados pelo webpack nas páginas `.jsp`

O webpack cria um endpoint para todo index.ts, index.js, index.css, index.scss ou index.tsx que estiver dentro da pasta web/src e o serve no mesmo caminho relativo. 
Por exemplo, suponha que haja um arquivo localizado em: `web/src/modulos/cadastro/pessoa/index.js`, assim, o javascript deverá ser referenciado na `jsp` da seguinte maneira:
`<script src="${ctxWebpack}/dist/modulos/cadastro/pessoa/index.js"></script>`
Perceba que o caminho na jsp é o mesmo a partir de `web/src/`. A pasta `dist` e a variável `${ctxWebpack}` será explicada melhor no próximo tópico.


### Como são gerados os códigos JavaScript para o GGAS em produção

Enquanto em desenvolvimento, os arquivos .js dentro da pasta /web/ são servidos pelo webpack no localhost:9000, 
no modo de produção os javascripts serão servidos a partir da pasta `src/main/webapp/dist`. 
Assim, para chavear entre os modos de `PRODUÇÃO` e `DESENVOLVIMENTO` do GGAS, altere a propriedade  `IS_AMBIENTE_PRODUCAO` no arquivo `src/main/resources/constantes.properties` (`true` para production e `false` para development). 
Recomenda-se fortemente que esta propriedade esteja sempre ativada como `true` nos repositórios `master` das fornecedores e mantenedor. 
Deve ser utilizada como false somente nos ambientes de desenvolvimento.

Os javascripts minificados são gerados pelo webpack na pasta `/web/dist` usando a task `npmBuild` do gradle e então os arquivos gerados são copiados para 
a pasta `src/main/webapp/dist` ao executar a task `copyJavascriptDist`. Estas tasks não precisam ser executadas manualmente, elas são executadas automaticamente ao disparar 
as  tasks de `build` ou `war`. 

No tópico anterior foi visto que na JSP existe uma variável chamada `${ctxWebpack}`, esta variável possui valor igual a "http://localhost:9000" quando a aplicação está executando em modo de desenvolvimento e "/" quando está executando em modo de produção.

### Quais são as tecnologias e libs utilizadas
  - Webpack 4 (Bundle de arquivos - https://webpack.js.org/)
  - ES6 (Habilita as funções e sintaxe mais recentes do javascript - https://www.w3schools.com/js/js_es6.asp)
  - Bootstrap v4 (Framework JS/CSS - https://getbootstrap.com/)
  - TypeScript (Habilita tipagem ao JavaScript, seu uso é opcional, porém recomendado - https://www.typescriptlang.org/)
  - InversifyJS (Container de inversão de controle - IoC - para JavaScript, seu uso é opcional, porém recomendado - http://inversify.io/)
  - SASS (Pré-compilador de arquivos SCSS para CSS, seu uso é opcional, porém recomendado - https://sass-lang.com/)

### Como migrar uma interface antiga do GGAS para utilizar o bootstrap

Para migrar uma interface antiga para o bootstrap é necessário ativar a opção `responsivo` no xml de definição do tiles da página. 
Veja um exemplo de como foi feito na tela de pesquisa de tipo de serviço:
**tiles-mvc-servicotipo.xml**:
```
<definition name="exibirPesquisaServicoTipo" extends="tilesTemplate">
     	<put-attribute name="corpo" value="/jsp/atendimento/servicotipo/exibirPesquisaServicoTipo.jsp"  />
        <put-attribute name="responsivo" value="true"  />
     	<put-list-attribute name="breadcrumb">
            <add-attribute value="Atendimento" />
            <add-attribute value="Autorização de Seviço" />
            <add-attribute value="Incluir Tipo de Serviço" />
        </put-list-attribute>
    </definition>
```
Quando se ativa a flag  `responsivo` as classes do bootstrap ficam disponíveis para serem usadas no corpo da página da jsp. 
Obviamente, assim que ativá-la em uma página antiga, os estilos da página irão quebrar. Assim, é necessário alterar a estrutura do HTML 
e adicionar as classes do bootstrap nos elementos da página. Você pode usar o padrão adotado na tela de pesquisa 
de tipo de serviço (`exibirPesquisaServicoTipo.jsp`) como base para remodelar as outras telas do sistema.

Na página de pesquisa de tipo de serviço também foi realizada uma refatoração do código JavaScript. A ideia foi remover ao máximo o código JavaScript que estava misturado com o código HTML da JSP. O código refatorado pode ser encontrado em: `web/src/modulos/atendimentoAoPublico/autorizacaoServico/tipoServico/pesquisaTipoServico/index.ts`. A inclusão deste arquivo na página é realizada no final da página `exibirPesquisaServicoTipo.jsp`:
```
<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/autorizacaoServico/tipoServico/pesquisaTipoServico/index.js"></script>
```
  
@author <jose.victor@logiquesistemas.com.br>, <italo.alan@logiquesistemas.com.br>
