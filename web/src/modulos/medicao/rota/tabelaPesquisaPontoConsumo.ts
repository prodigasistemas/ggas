import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {PropriedadesSistemaService} from "../../../servicos/propriedades.sistema.service";
import {PontoConsumo} from "../../../dominio/PontoConsumo";
import {Datatable, DataTableLanguageConfigs} from "../../../negocio/datatables";
import {PesquisaPontoConsumoRota} from "./modalPesquisarPontoConsumo";


/**
 *
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provide(TabelaPesquisaPontoConsumoController)
export class TabelaPesquisaPontoConsumoController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    private callbackPre;
    private callbackComplete;
    private tabela;

    @postConstruct()
    public onInit() {}

    public configurarCallbacks(callbackPre, callbackComplete) {
        this.callbackPre = callbackPre;
        this.callbackComplete = callbackComplete;
    }

    public recarregarTabela(dadosBusca : PesquisaPontoConsumoRota) {
        this.tabela = new Datatable($("#tablePesquisaPontoConsumo"));
        this.tabela.inicializar({
            ordering: false,
            searching: false,
            responsive: true,
            destroy: true,
            processing: false,
            serverSide: true,
            language: $.extend(DataTableLanguageConfigs, {
                emptyTable: "Sua pesquisa não retornou nenhum resultado, tente modificar os filtros atuais"
            }),
            lengthMenu: [10, 25, 50, 100, 200, 500, 1000, 2000, 2500],
            ajax: {
                url: `${this.props.contextoAplicacao()}/rest/rota/buscarPontoConsumoPaginado`,
                type: 'POST',
                contentType: "application/json",
                data: (d) => {
                    let pesquisa = dadosBusca;
                    return JSON.stringify({
                        qtdRegistros : d.length,
                        offset: d.start,
                        ...pesquisa,
                    });
                },
                dataFilter: (resposta) => {
                    let json = $.parseJSON(resposta);
                    console.log(json);
                    return JSON.stringify({
                        recordsTotal: json.quantidadeTotal,
                        recordsFiltered: json.quantidadeTotal,
                        data : json.dados
                    });
                },
                error: function () {
                    alert("Ocorreu um erro inesperado ao buscar os pontos de consumo!");
                },
                beforeSend : this.beforeSend.bind(this),
                complete : this.complete.bind(this)
            },
            columns: [
                {data : null, className : 'align-middle text-center details-control', render: this.renderColunaExpandir.bind(this)},
                {data : "descricao", className: 'align-middle text-center', render : (data) => "<span title='"+ data +"'>"+ data + "</span>"},
                {data : null, className: 'align-middle text-center', "defaultContent": '' },
                {data : null, className: 'align-middle text-center', "defaultContent": '' },
                {data : null, className: 'align-middle text-center', "defaultContent": '' },
                {data : null, className: 'align-middle text-center', "defaultContent": '' },
            ],
            columnDefs: [{className: "align-middle text-center", targets : "_all"}]
        }, false);
    }

    private renderColunaExpandir() {
        return `<a href="#"><i title="Exibir pontos de consumo" class="fa fa-plus text-primary"></i></a>`;
    }

    format (data) {
        let dados = ``;
        let pontosConsumo = data.pontosConsumo;
        pontosConsumo.forEach( (value) => {
            dados += `<tr>
                         <td class="align-middle text-center"><i class="fas fa-level-up-alt fa-rotate-90 fa-lg text-primary"></i></td>                         
                         <td class="align-middle text-center">${value.imovel}</td>
                         <td class="align-middle text-center">${value.descricao}</td>
                         <td class="align-middle text-center">${value.segmento}</td>
                         <td class="align-middle text-center">${value.ramoAtividade}</td>
                         <td class="align-middle text-center">${value.situacaoConsumo}</td>
                      </tr>`;
        });
        return $(dados);
    }

    private configurarAcaoExpadir() {
        $('#tablePesquisaPontoConsumo .details-control').click('click', (event) => {
            var element = event.currentTarget;
            var tr = $(element).closest('tr');
            var row = this.tabela.tabela.row(tr);
            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
                $(tr).find("i").removeClass("fa-minus").addClass("fa-plus");
            } else {
                $(tr).find("i").removeClass("fa-plus").addClass("fa-minus");
                if ( this.tabela.tabela.row( '.shown' ).length ) {
                    $('.details-control', this.tabela.tabela.row( '.shown' ).node()).click();
                }
                row.child( this.format(row.data())).show();
                tr.addClass('shown');
            }
        });
    }

    private beforeSend() {
        this.callbackPre ? this.callbackPre() : null;
        this.ocultarTabela(true);
    }

    private complete() {
        this.ocultarTabela(false);
        $(".containerTabelaPontoConsumo").css("height", "auto");
        this.callbackComplete ? this.callbackComplete() : null;
        $('.dataTables_empty').prop('colspan', $('#tablePesquisaPontoConsumo').find('th').length);
        this.configurarAcaoExpadir();
    }

    public ocultarTabela(ocultar) {
        if (ocultar) {
            $(".containerTabelaPontoConsumo").hide();
        } else {
            $(".containerTabelaPontoConsumo").show();
        }
    }
}
