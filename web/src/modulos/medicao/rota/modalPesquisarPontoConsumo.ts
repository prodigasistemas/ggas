import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {LoadingService} from "../../../servicos/loading.service";
import {TabelaPesquisaPontoConsumoController} from "./tabelaPesquisaPontoConsumo";
import {NotificadorService} from "../../../servicos/notificador.service";

@provide(ModalPesquisarPontoConsumo)
export class ModalPesquisarPontoConsumo {
    private FORMATO_DATA_HORA = "d/m/Y H:i:s";
    private $modal;
    private $botaoBuscar;

    @inject(NotificadorService)
    private notificador : NotificadorService;

    @inject(TabelaPesquisaPontoConsumoController)
    private tabela : TabelaPesquisaPontoConsumoController;

    @inject(LoadingService)
    private loading : LoadingService;

    @postConstruct()
    public onInit() {
        this.$modal = $("#modalPontoConsumo");
        this.$botaoBuscar = $('#botaoBuscarPontoConsumo');
        this.$botaoBuscar.click(() => {
            const dados = this.extrairPesquisaModal();
            if (this.validarDados()) {
                this.tabela.recarregarTabela(dados);
            }
        });
        this.configurarTabela();
        setTimeout(function() { $('#descricaoPontoConsumo_modal').focus(); }, 500);
    }

    private configurarTabela() {
        let preLoading = () => {
            this.loading.exibir(true);
        };

        let posLoading = () => {
            this.loading.exibir(false);
        };

        this.tabela.configurarCallbacks(preLoading, posLoading);
    }

    public exibir() {
        this.$modal.modal();
    }

    public extrairPesquisaModal() : PesquisaPontoConsumoRota {
        let situacaoContrato = $("#situacaoContrato").val();
        let situacaoMedidor = $("#situacaoMedidor").val();
        let dataInicioAssinaturaContrato, dataFimAssinaturaContrato, dataInicioAtivacaoMedidor, dataFimAtivacaoMedidor;
        if ($('#dataInicioAssinaturaContrato').val()) {
            dataInicioAssinaturaContrato = $('#dataInicioAssinaturaContrato').datepicker('getDate').format(this.FORMATO_DATA_HORA);
        }
        if ($('#dataFimAssinaturaContrato').val()) {
            dataFimAssinaturaContrato = $('#dataFimAssinaturaContrato').datepicker('getDate').format(this.FORMATO_DATA_HORA);
        }
        if ($('#dataInicioAtivacaoMedidor').val()) {
            dataInicioAtivacaoMedidor = $('#dataInicioAtivacaoMedidor').datepicker('getDate').format(this.FORMATO_DATA_HORA);
        }
        if ($('#dataFimAtivacaoMedidor').val()) {
            dataFimAtivacaoMedidor = $('#dataFimAtivacaoMedidor').datepicker('getDate').format(this.FORMATO_DATA_HORA);
        }

        return {
            situacaoMedidor : situacaoMedidor,
            situacaoContrato : situacaoContrato,
            dataInicioAssinaturaContrato : dataInicioAssinaturaContrato,
            dataFimAssinaturaContrato : dataFimAssinaturaContrato,
            dataInicioAtivacaoMedidor : dataInicioAtivacaoMedidor,
            dataFimAtivacaoMedidor : dataFimAtivacaoMedidor
        }
    }

    private validarDados() {
        return this.validarFormatoDasDatas() && this.validarIntevaloEntreDatas();
    }

    private validarFormatoDasDatas() {
        let valido = true;
        let self = this;
        $(".campoData").each(function(){
            let val = $(this).val();
            if (val !== '' ){
                let parts = val.split("/");
                let ano = Number(parts[2]);
                let mes = Number(parts[1]) - 1;
                let dia = Number(parts[0]);
                let d = new Date(ano, mes, dia);
                if (d.getFullYear() !== ano || d.getMonth() !== mes || d.getDate() !== dia) {
                    self.notificador.exibirMensagemAtencao(null, "Informe uma data válida.");
                    $(this).focus();
                    valido = false;
                    return;
                }
            }
        });
        return valido;
    }

    private validarIntevaloEntreDatas() {
        let valido = true;
        let dataInicioAssinaturaContrato = $('#dataInicioAssinaturaContrato').val();
        let dataFimAssinaturaContrato = $('#dataFimAssinaturaContrato').val();
        let dataInicioAtivacaoMedidor = $('#dataInicioAtivacaoMedidor').val();
        let dataFimAtivacaoMedidor = $('#dataFimAtivacaoMedidor').val();
        if (dataInicioAssinaturaContrato != '' && dataFimAssinaturaContrato != '') {
            if (this.dataStringTodataObj(dataInicioAssinaturaContrato) > this.dataStringTodataObj(dataFimAssinaturaContrato)) {
                this.notificador.exibirMensagemAtencao(null, "A data inicial da assinatura do contrato deve ser menor ou igual a data final.");
                valido = false;
            }
        }
        if (dataInicioAtivacaoMedidor != '' && dataFimAtivacaoMedidor != '') {
            if (this.dataStringTodataObj(dataInicioAtivacaoMedidor) > this.dataStringTodataObj(dataFimAtivacaoMedidor)) {
                this.notificador.exibirMensagemAtencao(null, "A data inicial da ativação do medidor deve ser menor ou igual a data final.");
                valido = false;
            }
        }
        return valido;
    }

    private dataStringTodataObj(dataString) {
        let parts = dataString.split("/");
        let ano = Number(parts[2]);
        let mes = Number(parts[1]) - 1;
        let dia = Number(parts[0]);
        return new Date(ano, mes, dia);
    }
}

export interface PesquisaPontoConsumoRota {
    situacaoMedidor : number,
    situacaoContrato : number,
    dataInicioAssinaturaContrato : string,
    dataFimAssinaturaContrato : string,
    dataInicioAtivacaoMedidor : string,
    dataFimAtivacaoMedidor : string
}
