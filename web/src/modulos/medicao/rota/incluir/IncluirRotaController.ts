import {inject, postConstruct} from "inversify";
import {provide} from "inversify-binding-decorators";
import {GenericController} from "../../../genericController";
import {ModalPesquisarPontoConsumo} from "../modalPesquisarPontoConsumo";

declare let $: any;

/**
 * Controlador padrão da tela de inclusão de rota
 *
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provide(IncluirRotaController)
export class IncluirRotaController extends GenericController {

    @inject(ModalPesquisarPontoConsumo)
    private modalPesquisarPontoConsumo : ModalPesquisarPontoConsumo;

    @postConstruct()
    public onInit() {
        $("#pesquisarPontoConsumo").click(() => {
            this.modalPesquisarPontoConsumo.exibir();
        });
    }
}
