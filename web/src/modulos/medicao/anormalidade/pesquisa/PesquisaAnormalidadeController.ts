/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
import {postConstruct} from "inversify";
import {Datatable} from "../../../../negocio/datatables";
import {provide} from "inversify-binding-decorators";
import {GenericController} from "../../../genericController";


declare let $: any;

/**
 * Controlador padrão da tela de listagem de leitura movimento
 *
 * @author italo.alan@logiquesistemas.com.br
 */
@provide(PesquisaAnormalidadeController)
export class PesquisaAnormalidadeController extends GenericController {

    private $tabelaLeitura;
    private $tabelaConsumo;
    private $tipoAnormalidade;

    @postConstruct()
    public onInit() {
        this.inicializarTabelas();
        this.configurarSelectAnormalidade();
        this.configurarAcoes();
        this.atualizarFormulario(this.$tipoAnormalidade.val() === "true");
    }

    private configurarAcoes() {
        $(".linkGoDetalhamentoLeitura").click((e) => {
            let chavePrimaria = $(e.currentTarget).data("chaveprimaria");
            $("#anormalidadeForm").append($("<input>", {type: "hidden", name: "chavePrimaria"}).val(chavePrimaria));
            $("#anormalidadeForm")
                .attr("action", "exibirDetalhamentoAnormalidadeLeitura")
                .submit();
        });

        $(".linkGoDetalhamentoConsumo").click(e => {
            let chavePrimaria = $(e.currentTarget).data("chaveprimaria");
            $("#anormalidadeForm").append($("<input>", {type: "hidden", name: "chavePrimaria"}).val(chavePrimaria));
            $("#anormalidadeForm")
                .attr("action", "exibirDetalhamentoAnormalidadeConsumo")
                .submit();
        })

        $("#btnRemover").click(async () => {
            let chavesAnormalidadesLeitura = this.obterListaAnormalidadesLeituraSelecionadas();

            if (chavesAnormalidadesLeitura.length === 0) {
                this.notificador.exibirMensagemAtencao("", "Selecione uma ou mais anormalidades de leitura para proceder");
            } else {
                let result = await this.notificador.exibirPergunta("", `Deseja realmente excluir os ${chavesAnormalidadesLeitura.length} registros selecionados?`);
                if (result.value) {
                    $("#anormalidadeForm").append($("<input>", {name: "chavesPrimarias", type: "hidden"}).val(chavesAnormalidadesLeitura.join(",")));
                    $("#anormalidadeForm")
                        .attr("action", "excluirAnormalidadeLeitura")
                        .submit();
                }
            }
        })

        $("#botaoLimpar").click(() => {
            $("#descricao").val("");
            $("#anormalidadeForm select").val("-1");
            this.$tipoAnormalidade.val("false").change();
            $("#indicadorUso").val("true");
        })

        $("#buttonIncluir").click(() => {
            $("#anormalidadeForm")
                .attr("action", "incluirAnormalidade")
                .submit();
        })

        $("#btnAlterar").click(() => {
            let redirecionamento = null;
            let chaveAnormalidadesLeitura = [];
            if (this.$tipoAnormalidade.val() === "false") {
                chaveAnormalidadesLeitura = this.obterListaAnormalidadesLeituraSelecionadas();
                redirecionamento = 'exibirAlteracaoAnormalidadeLeitura';
            } else {
                chaveAnormalidadesLeitura = this.obterListaAnormalidadesConsumoSelecionadas();
                redirecionamento = 'exibirAlteracaoAnormalidadeConsumo';
            }

            if (chaveAnormalidadesLeitura.length === 0) {
                this.notificador.exibirMensagemAtencao("", "Selecione uma anormalidade para ser alterada");
            } else if (chaveAnormalidadesLeitura.length > 1) {
                this.notificador.exibirMensagemAtencao("", "Selecione apenas uma anormalidade para ser alterada");
            } else {

                $("#anormalidadeForm")
                    .append($("<input>", {type: "hidden", name: "chavePrimaria"}).val(chaveAnormalidadesLeitura[0]))
                    .attr("action", redirecionamento)
                    .submit();
            }
        })
    }

    private obterListaAnormalidadesLeituraSelecionadas() {
        return this.obterListaAnormalidadesSelecionadas(this.$tabelaLeitura, "[name='chavesAnormalidadeLeitura']:checked");
    }

    private obterListaAnormalidadesConsumoSelecionadas() {
        return this.obterListaAnormalidadesSelecionadas(this.$tabelaConsumo, "[name='chavesPrimariasConsumo']:checked");
    }

    private obterListaAnormalidadesSelecionadas($tabela, filterCheckSelecionado) {
        let chavesAnormalidadesLeitura = [];
        $tabela.DataTable().rows().eq(0).each(indice => {
            let linha = $tabela.DataTable().row(indice);
            let $input = $(linha.node()).find(filterCheckSelecionado);

            if ($input[0]) {
                chavesAnormalidadesLeitura.push($input.data("chaveprimaria"));
            }
        });
        return chavesAnormalidadesLeitura;
    }

    private atualizarFormulario(isAnormalidadeConsumo : boolean) {
        this.toggleExibicaoTabelas(isAnormalidadeConsumo);
        this.toggleContainerLeitura(isAnormalidadeConsumo);
        this.toggleBtnIncluirAnormalidadeLeitura(isAnormalidadeConsumo);
    }

    private configurarSelectAnormalidade() {
        this.$tipoAnormalidade = $("#tipoAnormalidade");
        this.$tipoAnormalidade.change((e) => {
            let isAnormalidadeConsumo = e.currentTarget.value === "true";
            this.toggleExibicaoTabelas(isAnormalidadeConsumo);
            this.toggleContainerLeitura(isAnormalidadeConsumo);
            this.toggleBtnIncluirAnormalidadeLeitura(isAnormalidadeConsumo);
        });
    }

    private inicializarTabelas() {
        const tableLeitura = new Datatable('#table-anormalidades-leitura');
        tableLeitura.columnDefs = [
            {"targets": [2], "type": 'text-locale'},
            {"targets" : [3, 4], width: 100}
        ];
        tableLeitura.inicializar();

        const tableConsumo = new Datatable('#table-anormalidades-consumo');
        tableConsumo.columnDefs = [
            {"targets": [2], "type": 'text-locale'}
        ];
        tableConsumo.inicializar();

        this.$tabelaLeitura = $("#table-anormalidades-leitura");
        this.$tabelaConsumo = $("#table-anormalidades-consumo");
    }

    private toggleExibicaoTabelas(isAnormalidadeConsumo : boolean) {
        $("#containerTabelaAnormalidadeLeitura").toggleClass("hide", isAnormalidadeConsumo);
        $("#containerTabelaAnormalidadeConsumo").toggleClass("hide", !isAnormalidadeConsumo);
    }

    private toggleContainerLeitura(isAnormalidadeConsumo : boolean) {
        $("#containerLeitura").toggleClass("hide", isAnormalidadeConsumo);
    }


    /**
     * Apresenta ou esconde em tela o botão de anormalidade de leitura.
     * Apenas anormalidades de leitura podem ser incluídas no sistema
     *
     * @param {boolean} isAnormalidadeConsumo se a anormalidade selecionada é de leitura ou consumo
     */
    private toggleBtnIncluirAnormalidadeLeitura(isAnormalidadeConsumo : boolean) {
        $("#buttonIncluir, #btnRemover").toggleClass("hide", isAnormalidadeConsumo);
    }
}
