import {Datatable} from "../../../../../negocio/datatables";
declare const $: any;
$(document).ready(function () {
    const d = new Datatable('#table-clientes');
    d.columnDefs = [
        {"targets": [0], "searchable": false, "orderable": false, "visible": true},
        {"targets": [1], "searchable": false, "orderable": false, "visible": true},
        {"targets": [3], "type": 'date-time' },
        {"targets": [4], "type": 'date-time' }
    ];
    d.inicializar();
});
