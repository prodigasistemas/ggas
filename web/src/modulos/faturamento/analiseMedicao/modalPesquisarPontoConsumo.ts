import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {TabelaPesquisaPontoConsumoController} from "./tabela/tabelaPesquisaPontoConsumo";
import {LoadingService} from "../../../servicos/loading.service";
import {PontoConsumo} from "../../../dominio/PontoConsumo";

@provide(ModalPesquisarPontoConsumo)
export class ModalPesquisarPontoConsumo {

    private $modal;
    private $botaoBuscar;

    @inject(TabelaPesquisaPontoConsumoController)
    private tabela : TabelaPesquisaPontoConsumoController;

    @inject(LoadingService)
    private loading : LoadingService;

    @postConstruct()
    public onInit() {
        this.$modal = $("#modalPontoConsumo");
        this.$botaoBuscar = $('#botaoBuscarPontoConsumo');
        this.$botaoBuscar.click(() => {
            const valor = $('#descricaoPontoConsumo_modal').val();
            if (valor.length >= 3 ) {
                const dados = this.extrairPesquisaModal();
                this.tabela.recarregarTabela(dados);
            }
        });
        this.configurarInputBusca();
        this.configurarTabela();
        this.configurarBotaoAdicionar();
        setTimeout(function() { $('#descricaoPontoConsumo_modal').focus(); }, 500);
    }

    private configurarInputBusca() {
        $("#descricaoPontoConsumo_modal").keyup(function(event) {
            if (event.keyCode === 13) {
                $("#botaoBuscarPontoConsumo").click();
            }
        });
    }

    private configurarTabela() {
        let preLoading = () => {
            this.loading.exibir(true);
        };

        let posLoading = () => {
            this.loading.exibir(false);
        };

        this.tabela.configurarCallbacks(preLoading, posLoading);
    }

    public exibir() {
        this.$modal.modal();
        const pontos : PontoConsumo[] = $("#selectPontoConsumo").select2("data").map(v => v.data);
        this.tabela.carregarPontosSelecionados(pontos);
    }

    private configurarBotaoAdicionar() {
        $('#btnEscolherPontosConsumo').click('click', () => {
            this.preencherSelectPontos(this.tabela.pontosMarcados);
            this.$modal.modal('toggle');
        });
    }

    private preencherSelectPontos(pontos) {
        $("#selectPontoConsumo").empty().trigger('change');
        $('#selectPontoConsumo').select2({
            theme: "bootstrap",
            containerCssClass: ':all:',
            language: 'pt-BR',
            allowClear: true,
            width: null,
            data : pontos.map((d: PontoConsumo) => { return {
                        id: d.chavePrimaria,
                        text: d.descricao,
                        selected : true,
                        data: d
                    }
                }),
            placeholder: 'Busque um ponto de consumo pela sua descrição',
            minimumInputLength: 3,
            cache: true
        });
    }

    public extrairPesquisaModal() {
        const valor = $('#descricaoPontoConsumo_modal').val();
        const rotas = $("#rota").select2("data").map(v => Number(v.id));
        return {
            descricao: valor,
            rotas: rotas
        }
    }

}
