/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {inject, postConstruct} from "inversify";
import {provideSingleton} from "../../../singleton";
import {DateRangePickerLocale} from "../../../negocio/util/DateRangePickerLocale";
import {PropriedadesSistemaService} from "../../../servicos/propriedades.sistema.service";
import {PontoConsumo} from "../../../dominio/PontoConsumo";
import {UrlUtil} from "../../../negocio/util/UrlUtil";
import {RotaService} from "../../../servicos/http/rota.service";
import {NotificadorService} from "../../../servicos/notificador.service";
import {LoadingService} from "../../../servicos/loading.service";
import {Rota} from "../../../dominio/Rota";
import {LeituraMovimentoService} from "../../../servicos/http/leituraMovimentoService";
import {ModalPesquisarPontoConsumo} from "./modalPesquisarPontoConsumo";

declare let cpfCnpj : any;

/**
 * Formulário de pesquisa de leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@provideSingleton(FormPesquisaLeituraMovimento)
export class FormPesquisaLeituraMovimento {

    private FORMATO_DATA_HORA = "DD/MM/YYYY HH:mm:ss";
    private FORMATO_DATA = "DD/MM/YYYY";
    private $selectPontoConsumo;
    private $anormalidade;
    private $anormalidadeConsumo;
    private $periodoLeitura;
    private $rota;
    private $grupoFaturamento;
    private $ciclo;
    private $segmento;
    private $limparData;
    private $referencia;

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(RotaService)
    private rotaService : RotaService;

    @inject(NotificadorService)
    private notificador : NotificadorService;

    @inject(LoadingService)
    private loading : LoadingService;

    @inject(LeituraMovimentoService)
    private leituraMovimentoService : LeituraMovimentoService;

    @inject(ModalPesquisarPontoConsumo)
    private modalPesquisarPontoConsumo : ModalPesquisarPontoConsumo;

    private carregamentoInicial = true;

    @postConstruct()
    public async onInit() {
        this.$rota = $("#rota");
        this.$ciclo = $("#ciclo");
        this.$grupoFaturamento = $("#grupoFaturamento");
        this.$selectPontoConsumo = $("#selectPontoConsumo");
        this.$anormalidade = $("#anormalidade");
        this.$anormalidadeConsumo = $("#anormalidadeConsumo");
        this.$periodoLeitura = $("#periodoLeitura");
        this.$segmento = $("#segmento");
        this.$limparData = $("#limparData");
        this.configurarSelectAnormalidade();
        this.configurarSelectAnormalidadeConsumo();
        this.configurarSelectPontoConsumo();
        this.configurarPeriodoLeituraPicker();
        this.configurarGrupoFaturamento();
        this.configurarSelectRotas();
        this.configurarRotas();
        this.configurarBotaoPesquisar();
        let grupoFaturamento = UrlUtil.getParametro(window.location.search, "grupoFaturamento");
        if (grupoFaturamento) {
            this.$grupoFaturamento.val(grupoFaturamento);
            this.$grupoFaturamento.change();
        }
        
        this.$referencia = $("#referencia");

    }

    public onSubmit(callback) {
        $("#formPesquisarLeituras").unbind();
        $("#formPesquisarLeituras").submit((e) => {
            e.preventDefault();
            if (this.validar()) {
                callback(e);
            }
        });
    }

	public extrairPesquisaFormulario() : PesquisaAprovacaoLeitura {
        let chavesAnormalidades = this.$anormalidade.select2("data").map(a => Number(a.id));
        let chavesAnormalidadesConsumo = this.$anormalidadeConsumo.select2("data").map(a => Number(a.id));
        let pontosConsumo : PontoConsumo[] = this.$selectPontoConsumo.select2("data").map(a => a.data);
        let dataInicial, dataFinal;
        if (this.$periodoLeitura.val()) {
            dataInicial = this.$periodoLeitura.data("daterangepicker").startDate.format(this.FORMATO_DATA_HORA);
            dataFinal = this.$periodoLeitura.data("daterangepicker").endDate.format(this.FORMATO_DATA_HORA);
        }

        let situacaoLeituras = $("[name='situacaoLeitura']:checked").toArray().map(a => Number(a.value));
        let rotas = this.$rota.select2("data").map(v => Number(v.id));
        let grupoFaturamento = Number(this.$grupoFaturamento.val());
        let ciclo = Number(this.$ciclo.val());
        let segmento = this.$segmento.val();
        let anormalidadeLeituraBloqueiaFaturamento = $("[name='anormLeituraBloqueia']:checked").val();
        let anormalidadeConsumoBloqueiaFaturamento = $("[name='anormConsumoBloqueia']:checked").val();
        let possuiAnormalidadeConsumo = $("[name='possuiAnormalidadeConsumo']:checked").val();
        let possuiAnormalidadeLeitura = $("[name='possuiAnormalidadeLeitura']:checked").val();
        let referencia = this.$referencia.val();
        
        return { chavesAnormalidades, chavesAnormalidadesConsumo, pontosConsumo, dataInicial, dataFinal,
            situacaoLeituras, rotas, grupoFaturamento, ciclo, segmento, anormalidadeLeituraBloqueiaFaturamento, anormalidadeConsumoBloqueiaFaturamento, referencia, possuiAnormalidadeLeitura, possuiAnormalidadeConsumo};
    }

    private configurarSelectAnormalidade() {
        this.$anormalidade.select2({
            theme: "bootstrap",
            placeholder: "Selecione uma ou mais opções",
            containerCssClass: ':all:',
            language: 'pt-BR',
            width: null
        });
    }

    private configurarSelectAnormalidadeConsumo() {
        this.$anormalidadeConsumo.select2({
            theme: "bootstrap",
            placeholder: "Selecione uma ou mais opções",
            containerCssClass: ':all:',
            language: 'pt-BR',
            width: null
        });
    }

    private configurarSelectRotas() {
        this.$rota.select2({
            theme: "bootstrap",
            placeholder: "Selecione uma ou mais opções de rota",
            containerCssClass: ':all:',
            language: 'pt-BR',
            width: null
        });
    }

    private configurarPeriodoLeituraPicker() {
        this.$periodoLeitura.daterangepicker({
            locale: DateRangePickerLocale,
            autoApply: true,
            linkedCalendars: false,
            autoUpdateInput: false
        }).on("show.daterangepicker", () => {
            $(".daterangepicker").toggleClass("bootstrap", true);
        }).on("apply.daterangepicker", (e, picker) => {
            let inicio = picker.startDate.format(this.FORMATO_DATA);
            let fim = picker.endDate.format(this.FORMATO_DATA);
            this.$periodoLeitura.val(`${inicio} - ${fim}`);
            $("#formPesquisarLeituras").submit();
        });

        this.$limparData.click(() => {
            this.$periodoLeitura.val("");
        })
    }

    private configurarSelectPontoConsumo() {
        this.$selectPontoConsumo.select2({
            theme: "bootstrap",
            placeholder: "Selecione uma ou mais opções de ponto de consumo",
            containerCssClass: ':all:',
            allowClear: true,
            language: 'pt-BR',
            width: null
        });
        this.$selectPontoConsumo.on('select2:unselect', () => {
            $(this).data('state', 'unselected');
        });
        this.$selectPontoConsumo.on('select2:opening', (e) => {
            if ($(this).data('state') === 'unselected') {
                $(this).removeData('state');
                e.preventDefault();
            } else {
                let rotas = this.$rota.select2("data").map(a => Number(a.id));
                if (rotas.length == 0) {
                    this.notificador.exibirMensagemAtencao(null, "O campo Rota é obrigatório para realizar a busca de pontos de consumo");
                } else {
                    e.preventDefault();
                    this.modalPesquisarPontoConsumo.exibir();
                    setTimeout(function() { $('#descricaoPontoConsumo_modal').focus(); }, 500);
                }
            }
        });
    }

    private configurarGrupoFaturamento() {
        this.$grupoFaturamento.change(async (e) => {
            let grupoFaturamento = e.target.value;
            if (grupoFaturamento) {
                this.loading.exibir(true);
                let resposta = await this.rotaService.listaPorGrupoFaturamento(grupoFaturamento);
                this.loading.exibir(false);
                this.notificador.tratarRespostaHttp(resposta, null, (resposta) => {
                    this.popularRotas(resposta.conteudo);
                    this.atualizarUrl();
                    this.$rota.change();
                });
            } else {
                this.popularRotas([]);
                this.atualizarUrl();
            }

        })
    }

    private configurarRotas() {
        this.$rota.change(async () => {
            this.atualizarUrl();
            let rotas = this.$rota.select2("data").map(v => Number(v));
            let grupoFaturamento = Number(this.$grupoFaturamento.val());

            await this.recarregarPeriodoBusca(rotas[0], grupoFaturamento);

            if (this.carregamentoInicial) {
                this.carregamentoInicial = false;
                if (this.validar()) {
                    $("#formPesquisarLeituras").submit();
                }
            }
        })
    }

    private popularRotas(rotas : Rota[]) {
        this.$rota.empty();

        let rotasPram = UrlUtil.getParametro(window.location.search, "rotas");
        let rotaSelecionada = rotasPram ? rotasPram.split(",") : [];
        rotas.forEach(rota => {
            this.$rota.append(new Option(rota.numeroRota, String(rota.chavePrimaria), false, rotaSelecionada.includes(String(rota.chavePrimaria))));
        });
        this.$rota.trigger("change");
    }

    private async recarregarPeriodoBusca(rota : number, grupoFaturamento : number) {
        if (rota && grupoFaturamento) {
            this.loading.exibir(true);
            let resposta = await this.leituraMovimentoService.obterPeriodoBusca(rota, grupoFaturamento);
            this.loading.exibir(false);
            this.notificador.tratarRespostaHttp(resposta, null, () => {
                this.$periodoLeitura.data("daterangepicker").setStartDate(resposta.conteudo.split(" - ")[0]);
                this.$periodoLeitura.data("daterangepicker").setEndDate(resposta.conteudo.split(" - ")[1]);
            })
        }
    }

    private atualizarUrl() {
        let rotas = this.$rota.select2("data").map(a => Number(a.id)).join(",");
        rotas = rotas ? rotas : [];
        let idCronograma = UrlUtil.getParametro(window.location.search, "idCronograma");
        let grupoFaturamento = this.$grupoFaturamento.val();
        grupoFaturamento = grupoFaturamento ? grupoFaturamento : "";

        UrlUtil.atualizarUrl({grupoFaturamento, rotas, idCronograma});
    }

    private validar() : boolean {
        let pesquisa = this.extrairPesquisaFormulario();
        let valido = true;
        if (!pesquisa.grupoFaturamento || !pesquisa.rotas || !pesquisa.rotas.length || !pesquisa.ciclo || !pesquisa.referencia) {
            valido = false;
        }

        return valido;

    }

    private configurarBotaoPesquisar() {
        $("#botaoPesquisar").click(e => {
            e.preventDefault();
            if (this.validar()) {
                $("#formPesquisarLeituras").submit();
            } else {
                this.notificador.exibirMensagemAtencao(null, "Os campos Grupo de Faturamento, " +
                    "Rota, Referência e Ciclo são obrigatórios para realizar a busca");
            }
        });
    }
}

export interface PesquisaAprovacaoLeitura {
    chavesAnormalidades : number[],
    chavesAnormalidadesConsumo : number[],
    pontosConsumo : PontoConsumo[],
    dataInicial : string,
    dataFinal : string,
    situacaoLeituras : number[],
    rotas : number[],
    grupoFaturamento : number,
    ciclo : number,
    segmento : string,
    anormalidadeLeituraBloqueiaFaturamento : boolean,
    anormalidadeConsumoBloqueiaFaturamento : boolean,
    referencia: string,
    possuiAnormalidadeLeitura : boolean,
    possuiAnormalidadeConsumo : boolean
}
