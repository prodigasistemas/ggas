import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {TabelaHistoricoLeituraController} from "./tabela/tabelaHistoricoLeitura";
import {LoadingService} from "../../../servicos/loading.service";

@provide(ModalHistoricoLeitura)
export class ModalHistoricoLeitura {

    private $modal;

    @inject(TabelaHistoricoLeituraController)
    private tabela : TabelaHistoricoLeituraController;

    @inject(LoadingService)
    private loading : LoadingService;

    @postConstruct()
    public onInit() {
        this.$modal = $("#modalHistoricoLeitura");
            // Define a largura mínima diretamente em pixels
        this.configurarTabela();
    }

    private configurarTabela() {
        let preLoading = () => {
            this.loading.exibir(true);
        };

        let posLoading = () => {
            this.loading.exibir(false);
            this.ajustarTamanhoModal();
        };

        this.tabela.configurarCallbacks(preLoading, posLoading);
    }

    public exibir(chavePontoConsumo : number) {
        this.$modal.modal();
        this.tabela.recarregarTabela(chavePontoConsumo);
    }
    
    private ajustarTamanhoModal() {
    // Aguarda um breve momento para garantir que a tabela foi totalmente renderizada
    setTimeout(() => {
        const tabelaWidth = this.$modal.find("#tabelaHistoricoLeitura").width() + 30; // Obtém a largura da tabela
        this.$modal.find('.modal-content').css('width', tabelaWidth + 'px');      // Define a largura do modal

        // Opcional: Centralizar o modal na tela
        //const windowWidth = $(window).width();
        //const modalLeft = (windowWidth - tabelaWidth) / 2;
        //this.$modal.find('.modal-content').css('margin-left', modalLeft + 'px');
    }, 100); // Tempo de espera para garantir que a tabela esteja carregada
}

}
