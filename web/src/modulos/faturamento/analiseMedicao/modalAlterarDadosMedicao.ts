/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {postConstruct} from "inversify";
import {LeituraMovimentoDto} from "../../../dominio/dto/LeituraMovimentoDto";

/**
 *
 * @author jose.victor@logiquesistemas.com.br
 */
export type CallbackDadosMedicao = (dados : LeituraMovimentoDto) => any;

@provide(ModalAlterarDadosMedicao)
export class ModalAlterarDadosMedicao {

    private $modal;
    private $inputChaveLeitura;

    private $divAlterarLeitura;
    private $divSelectAnormalidade;
    private $divAlterarObservacao;
    private $divAlterarTemperatura;
    private $divAlterarPressao;
    private $divAlterarDataLeitura;

    private $selectAlterarAnormalidade;
    private $inputAlterarObservacao;
    private $inputAlterarTemperatura;
    private $inputAlterarPressao;
    private $inputAlterarLeitura;
    private $inputAlterarDataLeitura;
    private $spanDescricaoPontoConsumo;

    private $btnConfirmarDadosMedicao;
    private $limparAlterarDataLeitura;
    private _callback : CallbackDadosMedicao;

    @postConstruct()
    public onInit() {
        this.$modal = $("#modalAlterarDadosMedicao");
        this.$inputChaveLeitura = $("#chaveAlterarLeitura");

        this.$inputAlterarLeitura = $("#inputAlterarLeitura");
        this.$spanDescricaoPontoConsumo= $("#descricaoPontoConsumo");
        this.$inputAlterarPressao = $("#inputAlterarPressao");
        this.$inputAlterarObservacao = $("#inputAlterarObservacao");
        this.$inputAlterarTemperatura = $("#inputAlterarTemperatura");
        this.$inputAlterarDataLeitura = $("#inputAlterarDataLeitura");
        this.$selectAlterarAnormalidade = $("#selectAlterarAnormalidade");

        this.$divAlterarLeitura = $(".divAlterarLeitura");
        this.$divSelectAnormalidade = $(".divSelectAnormalidade");
        this.$divAlterarObservacao = $(".divAlterarObservacao");
        this.$divAlterarTemperatura = $(".divAlterarTemperatura");
        this.$divAlterarPressao = $(".divAlterarPressao");
        this.$divAlterarDataLeitura = $(".divAlterarDataLeitura");

        this.$btnConfirmarDadosMedicao = $("#confirmarDadosMedicao");
        this.$limparAlterarDataLeitura = $("#limparAlterarDataLeitura");

        this.$btnConfirmarDadosMedicao.click(() => {
            let chaveLeituraMovimento = Number(this.$inputChaveLeitura.val());
            let leituraAtual = this.$inputAlterarLeitura.val() ? Number(this.$inputAlterarLeitura.val()) : undefined;
            let pressao = this.$inputAlterarPressao.val() ? Number(this.$inputAlterarPressao.val()) : undefined;
            let temperatura = this.$inputAlterarTemperatura.val() ? Number(this.$inputAlterarTemperatura.val()) : undefined;
            let observacaoLeitura = this.$inputAlterarObservacao.val() ? this.$inputAlterarObservacao.val() : undefined;
            let chaveAnormalidade = this.$selectAlterarAnormalidade.val() ? Number(this.$selectAlterarAnormalidade.val()) : undefined;
            let dataLeitura = this.$inputAlterarDataLeitura.val() ? this.$inputAlterarDataLeitura.val() : undefined;
            let anormalidadeLeitura;
            if (chaveAnormalidade) {
                anormalidadeLeitura = {chavePrimaria: chaveAnormalidade, descricao: this.$selectAlterarAnormalidade.find("option:selected").text() }
            }

            this._callback ? this._callback({chaveLeituraMovimento, leituraAtual, anormalidadeLeitura,
                pressao, temperatura, observacaoLeitura, dataLeitura}) : null;
        });

        this.$inputAlterarLeitura.keypress(e => e.which == 13 ? this.$btnConfirmarDadosMedicao.click() : null);
        this.$inputAlterarPressao.keypress(e => e.which == 13 ? this.$btnConfirmarDadosMedicao.click() : null);
        this.$inputAlterarTemperatura.keypress(e => e.which == 13 ? this.$btnConfirmarDadosMedicao.click() : null);
        this.$inputAlterarDataLeitura.datetimepicker({
            locale: "pt-BR",
            format: 'DD/MM/YYYY'
        });
        this.$limparAlterarDataLeitura.click(() => this.$inputAlterarDataLeitura.val(""));
    }


    public exibirAlterarAnormalidade(leituraMovimento : LeituraMovimentoDto, _callback : CallbackDadosMedicao) {
        this.popularCampos(leituraMovimento);
        this.$divAlterarLeitura.hide();
        this.$divAlterarPressao.hide();
        this.$divAlterarTemperatura.hide();
        this.$divAlterarObservacao.hide();
        this.$divSelectAnormalidade.show();
        this.$divAlterarDataLeitura.hide();
        this._callback = _callback;
        setTimeout(() => this.$selectAlterarAnormalidade.focus(), 350);
    }

    public exibirAlterarLeitura(leituraMovimento : LeituraMovimentoDto, _callback: CallbackDadosMedicao) {
        this.popularCampos(leituraMovimento);
        this.$divSelectAnormalidade.hide();
        this.$divAlterarPressao.hide();
        this.$divAlterarTemperatura.hide();
        this.$divAlterarObservacao.hide();
        this.$divAlterarLeitura.show();
        this.$divAlterarDataLeitura.hide();
        this._callback = _callback;
        setTimeout(() => this.$inputAlterarLeitura.focus(), 350);
    }

    public exibirAlterarPressao(leituraMovimento : LeituraMovimentoDto, _callback: CallbackDadosMedicao) {
        this.popularCampos(leituraMovimento);
        this.$divSelectAnormalidade.hide();
        this.$divAlterarPressao.show();
        this.$divAlterarTemperatura.hide();
        this.$divAlterarObservacao.hide();
        this.$divAlterarLeitura.hide();
        this.$divAlterarDataLeitura.hide();
        this._callback = _callback;
        setTimeout(() => this.$inputAlterarPressao.focus(), 350);
    }

    public exibirAlterarTemperatura(leituraMovimento : LeituraMovimentoDto, _callback: CallbackDadosMedicao) {
        this.popularCampos(leituraMovimento);
        this.$divSelectAnormalidade.hide();
        this.$divAlterarPressao.hide();
        this.$divAlterarTemperatura.show();
        this.$divAlterarObservacao.hide();
        this.$divAlterarLeitura.hide();
        this.$divAlterarDataLeitura.hide();
        this._callback = _callback;
        setTimeout(() => this.$inputAlterarTemperatura.focus(), 350);
    }

    public exibirAlterarObservacao(leituraMovimento : LeituraMovimentoDto, _callback: CallbackDadosMedicao) {
        this.popularCampos(leituraMovimento);
        this.$divSelectAnormalidade.hide();
        this.$divAlterarPressao.hide();
        this.$divAlterarTemperatura.hide();
        this.$divAlterarObservacao.show();
        this.$divAlterarLeitura.hide();
        this.$divAlterarDataLeitura.hide();
        this._callback = _callback;
        setTimeout(() => this.$inputAlterarObservacao.focus(), 350);
    }

    public exibirAlterarDataLeitura(leituraMovimento : LeituraMovimentoDto, _callback: CallbackDadosMedicao) {
        this.popularCampos(leituraMovimento);
        this.$divSelectAnormalidade.hide();
        this.$divAlterarPressao.hide();
        this.$divAlterarTemperatura.hide();
        this.$divAlterarObservacao.hide();
        this.$divAlterarLeitura.hide();
        this.$divAlterarDataLeitura.show();
        this._callback = _callback;
        setTimeout(() => {
            this.$divAlterarDataLeitura.focus();
            this.$inputAlterarDataLeitura.data("DateTimePicker").show();
        }, 350);
    }

    private popularCampos(leituraMovimento : LeituraMovimentoDto) {
        this.$modal.modal();
        this.$inputChaveLeitura.val(leituraMovimento.chaveLeituraMovimento);

        if (leituraMovimento.leituraAtual) {
            this.$inputAlterarLeitura.val(leituraMovimento.leituraAtual);
        } else {
            this.$inputAlterarLeitura.val("");
        }

        if (leituraMovimento.anormalidadeLeitura) {
            this.$selectAlterarAnormalidade.val(leituraMovimento.anormalidadeLeitura.chavePrimaria);
        } else {
            this.$selectAlterarAnormalidade.val("");
        }

        this.$spanDescricaoPontoConsumo.text(leituraMovimento.pontoConsumo);
        this.$inputAlterarObservacao.val(leituraMovimento.observacaoLeitura || "");
        this.$inputAlterarTemperatura.val(leituraMovimento.temperatura || "");
        this.$inputAlterarPressao.val(leituraMovimento.pressao || "");
        this.$inputAlterarDataLeitura.val(leituraMovimento.dataLeitura || "");
    }

}

export interface DadosMedicao {
    chaveLeitura : number,
    valorLeitura : number,
    pressao : number,
    temperatura : number,
    observacaoLeitura : string,
    dataLeitura : string,
    anormalidade : {chavePrimaria  : number, descricao : string}
}
