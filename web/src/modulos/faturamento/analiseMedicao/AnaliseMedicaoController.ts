/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import * as Ladda from "ladda";
import "ladda/dist/ladda-themeless.min.css"
import {PropriedadesSistemaService} from "../../../servicos/propriedades.sistema.service";
import {LoadingService} from "../../../servicos/loading.service";
import {NotificadorService} from "../../../servicos/notificador.service";
import {LeituraMovimentoService} from "../../../servicos/http/leituraMovimentoService";
import {FormPesquisaLeituraMovimento} from "./FormPesquisaLeituraMovimento";
import {TabelaListagemLeituraController} from "./tabela/tabelaLeituraMovimento";
import {ControladorAcoesTabelaLeitura} from "./tabela/controladorAcoesTabelaLeitura";
import {DadosMedicao} from "./modalAlterarDadosMedicao";
import {ProcessoService} from "../../../servicos/http/processoService";
import {LeituraMovimentoDto} from "../../../dominio/dto/LeituraMovimentoDto";
import {ModalDetalhamentoLeitura, AnaliseLeitura} from "./modalDetalhamentoLeitura";

declare let $ : any, cpfCnpj : any;

/**
 * Controlador padrão da tela de listagem de leitura movimento
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(AnaliseMedicaoController)
export class AnaliseMedicaoController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(LoadingService)
    private loading : LoadingService;

    @inject(NotificadorService)
    private notificador : NotificadorService;

    @inject(LeituraMovimentoService)
    private leituraMovimentoService : LeituraMovimentoService;

    @inject(FormPesquisaLeituraMovimento)
    private form : FormPesquisaLeituraMovimento;

    @inject(TabelaListagemLeituraController)
    private tabela : TabelaListagemLeituraController;

    @inject(ControladorAcoesTabelaLeitura)
    private controladorAcoes : ControladorAcoesTabelaLeitura;

    @inject(ProcessoService)
    private processoService : ProcessoService;
    
    @inject(ModalDetalhamentoLeitura)
    private modalDetalhamento : ModalDetalhamentoLeitura;

    private btnPesquisar;
    private $btnConfirmarDadosMedicao;
    private $btnSalvarLeituras;
    private $btnRegistrarLeituras;
    private $btnSolicitarReleitura;
    private $btnSalvarConsitir;

    @postConstruct()
    public onInit() {
        this.$btnConfirmarDadosMedicao = $("#confirmarDadosMedicao");
        this.btnPesquisar = Ladda.create($("#botaoPesquisar")[0]);
        this.$btnSalvarLeituras = $("#salvarLeituras");
        this.$btnRegistrarLeituras = $("#registrarLeituras");
        this.$btnSolicitarReleitura = $("#solicitarReleitura");
        this.$btnSalvarConsitir = $("#salvarConsitir");
        this.configurarTabela();

        this.form.onSubmit((e) => {
            e.preventDefault();
            this.tabela.recarregarTabela();
        });

        this.$btnSolicitarReleitura.click(async () => {
            if (this.controladorAcoes.apenasLeiturasInformadasEstaoSelecionadas()) {
                let leiturasSelecionadas = this.controladorAcoes.obterLeiturasInformadas();
                if (leiturasSelecionadas.length > 0) {
                    //let resultado = await this.notificador.exibirPergunta("", `Deseja realmente solicitar a releitura desses ${leiturasSelecionadas.length} registros?`);
                    let mensagemRegistroLeituras  = this.controladorAcoes.obterLeiturasModificadas().length ?
                        "Existem leituras que foram modificadas e suas alterações não foram salvas. As modificações serão descartadas." : "";
                    let resultado = await this.notificador.exibirPergunta("", "Deseja executar o processo de solicitar releituras? " + mensagemRegistroLeituras);
                    if (resultado.value) {
                        this.solicitarReleitura(leiturasSelecionadas);
                    }
                } else {
                    this.notificador.exibirMensagemAtencao("", "Selecione um registro com situação 'Leitura Informada' para releitura");
                }
            } else {
                this.notificador.exibirMensagemAtencao("", "Apenas registros com situação 'Leitura Informada' podem ter a releitura solicitada");
            }
        });

        this.$btnSalvarLeituras.click(async () => {
            if (this.controladorAcoes.possuiLeiturasProcessadasMarcadas()) {
                this.notificador.exibirMensagemAtencao("", "Registros com situação 'Processado' não podem ser salvos");
            } else {
                if ($('#tabelaLeituras input[name=chavesPrimarias]:checked').length > 0) {
                    let dadosMedicoes = this.controladorAcoes.obterLeiturasModificadas();

                    for(var i=0;i<dadosMedicoes.length;i++){
                        if(!dadosMedicoes[i].dataLeitura || typeof dadosMedicoes[i].valorLeitura === "undefined"){
                            this.notificador.exibirMensagemAtencao('','Os campos Leitura Atual e Data de Leitura dos registros alterados são obrigatórios para salvar as leituras.')
                            return;
                        }
                    }

                    let resultado = await this.notificador.exibirPergunta("", `Deseja realmente alterar os dados de medições de ${dadosMedicoes.length} leituras?`);
                    if (resultado.value) {
                        let pesquisa = this.form.extrairPesquisaFormulario();
                        this.alterarDadosMedicao(pesquisa.grupoFaturamento, dadosMedicoes);
                    }
                } else {
                    this.notificador.exibirMensagemAtencao("", "Selecione ao menos uma leitura para salvar");
                }
            }
        });

        this.$btnRegistrarLeituras.click(async (e) => {
            e.preventDefault();
            if (this.controladorAcoes.apenasLeiturasInformadasEstaoSelecionadas()) {
                let mensagemRegistroLeituras  = this.controladorAcoes.obterLeiturasModificadas().length ?
                    "Existem leituras que foram modificadas e suas alterações não foram salvas. As modificações serão descartadas." : "";
                let resposta = await this.notificador.exibirPergunta("", "Deseja executar o processo de registrar leituras? " + mensagemRegistroLeituras);
                if (resposta.value) {
                    if ($('#tabelaLeituras input[name=chavesPrimarias]:checked').length > 0) {
                        await this.executarRegistrarLeitura();
                    } else {
                        this.notificador.exibirMensagemAtencao("", "Selecione ao menos uma leitura para registrar");
                    }
                }
            } else {
                this.notificador.exibirMensagemAtencao("", "Apenas registros com situação 'Leitura Informada' podem ter a releitura solicitada");
            }
        });
        
        this.$btnSalvarConsitir.click(async (e) => {
			e.preventDefault();
			let dadosAnaliseLeitura = this.controladorAcoes.obterDadosAnaliseLeitura();
			
			if(this.validarDadosAnaliseLeitura(dadosAnaliseLeitura)){
				this.notificador.exibirMensagemAtencao('','Os campos Leitura Atual, Leitura Anterior, Data Leitura Atual e Data Leitura Anterior são obrigatórios para realizar a operação.')
				return
			}
			
			let resposta = await this.notificador.exibirPergunta("", "Deseja executar o processo de Consistir Leitura? ");
			
			dadosAnaliseLeitura.isConsistir = resposta ? "true" : "false";
			
			await this.executarSalvarConsistirLeitura(dadosAnaliseLeitura);
			
		});
    }


    /**
     * Configura os callbacks de tabela
     */
    private configurarTabela() {
        let preLoading = () => {
            this.btnPesquisar.start();
            this.loading.exibir(true);
        };

        let posLoading = () => {
            this.btnPesquisar.stop();
            this.loading.exibir(false);
            this.$btnSalvarLeituras.toggleClass("btn-secondary", true);
            this.$btnSalvarLeituras.toggleClass("btn-success", false);
            this.$btnSalvarLeituras.attr("disabled", true);
        };

        this.tabela.configurarCallbacks(preLoading, posLoading);
    }

    /**
     * altera os status das leituras para solicitar a releitura
     * @param leituras as leituras para alteração de status
     */
    private async solicitarReleitura(leituras : number[]) {
        this.loading.exibir(true);
        let resposta = await this.leituraMovimentoService.solicitarReleitura(leituras);
        this.loading.exibir(false);
        await this.notificador.tratarRespostaHttp(resposta, `${leituras.length} registro(s) de leitura alterado(s) com sucesso!`);
        this.tabela.recarregarTabela();
    }

    /**
     * altera os dados de medição
     * @param {number} grupoFaturamento
     * @param {DadosMedicao[]} dadosMedicoes
     * @returns {Promise<void>}
     */
    private async alterarDadosMedicao(grupoFaturamento : number, dadosMedicoes : DadosMedicao[]) {
        this.loading.exibir(true);

        let resposta = await this.leituraMovimentoService.alterarDadosMedicao(grupoFaturamento, dadosMedicoes);

        this.loading.exibir(false);

        await this.notificador.tratarRespostaHttp(resposta, `Dados de Medição de ${dadosMedicoes.length} leituras alteradas com sucesso!`);
        this.tabela.recarregarTabela();
    }

    private async executarRegistrarLeitura() {
        let idOperacao = $("#idOperacao").val();
        let idGrupoFaturamento = $("#idGrupoFaturamento").val();
        let idRota = $("#idRota").val();
        let idModulo = $("#idModulo").val();
        let leiturasSelecionadas = $('#tabelaLeituras input[name=chavesPrimarias]:checked').map(function(){
            return $(this).val();
        }).get();
        this.loading.exibir(true);
        let resposta = await this.processoService.executarProcesso(idOperacao, idGrupoFaturamento, idRota, leiturasSelecionadas);
        this.loading.exibir(false);
        this.notificador.tratarRespostaHttp(resposta, null, () => {
            window.location.href = `${this.props.contextoAplicacao()}/pesquisarProcesso?&idModulo=${idModulo}&idOperacao=${idOperacao}`;
        })
    }
    
    private async executarSalvarConsistirLeitura(dadosAnaliseLeitura: AnaliseLeitura) {
        this.loading.exibir(true);
        let resposta = await this.leituraMovimentoService.salvarExecutarConsistir(dadosAnaliseLeitura)
        this.loading.exibir(false);
        await this.notificador.tratarRespostaHttp(resposta, `Operacao Realizada com sucesso!`);
        this.tabela.recarregarTabela();
        this.modalDetalhamento.esconder();
    }
    
    private validarDadosAnaliseLeitura(dadosAnaliseLeitura : AnaliseLeitura) : boolean {
		if(!dadosAnaliseLeitura.dataLeitura || !dadosAnaliseLeitura.dataLeituraAnterior || !dadosAnaliseLeitura.leituraAnterior || !dadosAnaliseLeitura.leituraAtual) {
			return true
		}
		
		return false;
	}

}
