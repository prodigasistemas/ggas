import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {Datatable, DataTableLanguageConfigs} from "../../../../negocio/datatables";
import {PontoConsumo} from "../../../../dominio/PontoConsumo";
import {LeituraMovimentoDto} from "../../../../dominio/dto/LeituraMovimentoDto";
import {HttpService} from "../../../../servicos/http/http.service";


@provide(TabelaHistoricoLeituraController)
export class TabelaHistoricoLeituraController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;
    
    @inject(HttpService)
    private httpService : HttpService;

    private callbackPre;
    private callbackComplete;
    private tabela;
	private alturaTabela;

    @postConstruct()
    public onInit() {
		$("#tabelaHistoricoLeitura").on('dt.draw tabela.atualizacaoConteudo', () => {
        $("#tabelaHistoricoLeitura").colResizable({disable: true});
        $("#tabelaHistoricoLeitura").colResizable({resizeMode:'overflow', liveDrag:true});
        });
		
	}

    public configurarCallbacks(callbackPre, callbackComplete) {
        this.callbackPre = callbackPre;
        this.callbackComplete = callbackComplete;
    }

    public async recarregarTabela(chavePontoConsumo : number) {
		
		let teste =  JSON.stringify({ chavePontoConsumo: chavePontoConsumo });
		console.log(teste);
		
		
		this.alturaTabela = $(".table-responsive").height();
		new Datatable($("#tabelaHistoricoLeitura")).inicializar({
			ordering: false,
			searching: false,
			responsive: false,
			destroy: true,
			processing: false,
			serverSide: true,
			language: $.extend(DataTableLanguageConfigs, {
				emptyTable: "Sua pesquisa não retornou nenhum resultado, tente modificar os filtros atuais"
			}),
			pageLength: 2500,
			lengthMenu: [10, 25, 50, 100, 200, 500, 1000, 2000, 2500],
			ajax: {
				url: `${this.props.contextoAplicacao()}/analiseMedicao/buscarHistoricoLeitura`,
				type: 'POST',
				contentType: "application/json; charset=utf-8",
				data: (d) => {
					    // Certifique-se de que chavePontoConsumo está sendo passado corretamente.
					    d.chavePontoConsumo = chavePontoConsumo;
					    console.log('Data enviado:', JSON.stringify(d));  // Adicione essa linha para depurar
					    return JSON.stringify(d);
					},
			    xhrFields: {
			        withCredentials: true  // This replaces 'credentials: "include"'
			    },
				dataFilter: (resposta) => {
					let json = $.parseJSON(resposta);
					return JSON.stringify({
						recordsTotal: json.quantidadeTotal,
						recordsFiltered: json.quantidadeTotal,
						data: json.dados
					});
				},
				beforeSend: this.beforeSend.bind(this),
				complete: this.complete.bind(this)
			},
			columns: [
				{
					data: "mesAnoCiclo", className: 'align-middle text-center',
					render: (data) => this.formatarMesAno(data)
				},			
				{ data: null, render: (data: LeituraMovimentoDto) => data.anormalidadeLeitura ? data.anormalidadeLeitura.descricao : "-" },
				{
					data: null, render: (data: LeituraMovimentoDto) => data.dataLeituraAnterior ? data.dataLeituraAnterior : "-", className: 'align-middle text-center dataLeitura', 
				},
				{
					data: null, render: (data: LeituraMovimentoDto) => data.leituraAnterior ? data.leituraAnterior : "-" , className: 'align-middle text-center leituraAtual'
				},
				{
					data: null, render: (data: LeituraMovimentoDto) => data.dataLeitura ? data.dataLeitura : "-", className: 'align-middle text-center dataLeitura', 
				},
				{
					data: null, render: (data: LeituraMovimentoDto) => data.leituraAtual ? data.leituraAtual : "-" , className: 'align-middle text-center leituraAtual'
				},
				{
					data: null, render: (data: LeituraMovimentoDto) => data.consumo ? data.consumo : "-" , className: 'align-middle text-center leituraAtual'
				},
				{
					data: null, render: (data: LeituraMovimentoDto) => data.consumoApurado ? data.consumoApurado : "-" , className: 'align-middle text-center leituraAtual'
				},
				{ data: null, render: (data: LeituraMovimentoDto) => data.anormalidadeConsumo ? data.anormalidadeConsumo.descricao : "-" },
				{ data: "fcPtz", render: (data) => this.renderNumero(data, 4) },
				{ data: "fcPcs", render: (data) => this.renderNumero(data, 4) },
				{ data: "quantidadeDias", defaultContent: "-" },

			],
			columnDefs: [{ className: "align-middle text-center", targets: "_all" }]
		}, false); 
    }

    private beforeSend() {
        this.callbackPre ? this.callbackPre() : null;
        this.alturaTabela = this.alturaTabela ? this.alturaTabela : $(".containerTabelaHistorico").height();
        this.ocultarTabela(true);
        $(".containerTabelaHistorico").css("height", this.alturaTabela);
    }

    private complete() {
        this.ocultarTabela(false);
        $(".containerTabelaHistorico").css("height", "auto");
        this.alturaTabela = null;
        this.callbackComplete ? this.callbackComplete() : null;
        $("#tabelaHistoricoLeitura").trigger('tabela.atualizacaoConteudo');

    }

    public ocultarTabela(ocultar) {
		 $(".containerTabelaHistorico").toggleClass("ocultarTabela", ocultar);

        if (ocultar) {
            $(".containerTabelaHistorico").hide();
        } else {
            $(".containerTabelaHistorico").show();
        }
    }
    
    private formatarMesAno(data) {
        let ano = data.substring(0,4);
        let mes = data.substring(4,6);
        return mes + "/" + ano;
    }
    
   	private renderNumero(data, numCasas = 2) {
        return (data !== null && data !== undefined) ? String(Number(data).toFixed(numCasas)).replace(".", ",") : "-";
    }
}
