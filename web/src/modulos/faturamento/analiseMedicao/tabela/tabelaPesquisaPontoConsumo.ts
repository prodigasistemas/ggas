import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {Datatable, DataTableLanguageConfigs} from "../../../../negocio/datatables";
import {PontoConsumo} from "../../../../dominio/PontoConsumo";

/**
 *
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provide(TabelaPesquisaPontoConsumoController)
export class TabelaPesquisaPontoConsumoController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    private callbackPre;
    private callbackComplete;
    private tabela;
    public pontosMarcados : PontoConsumo[] = [];

    @postConstruct()
    public onInit() {}

    public configurarCallbacks(callbackPre, callbackComplete) {
        this.callbackPre = callbackPre;
        this.callbackComplete = callbackComplete;
    }

    public carregarPontosSelecionados(pontos) {
        this.desmarcarTodos();
        this.pontosMarcados = pontos;
        let self = this;
        $('.checkPontoConsumo').each(function() {
            let idPC = parseInt($(this).val());
            let marcado =  self.pontosMarcados.some((element) => element.chavePrimaria == idPC) ? true : false;
            $(this).prop('checked', marcado);
        });
    }

    public recarregarTabela(dadosBusca) {
        this.tabela = new Datatable($("#tablePesquisaPontoConsumo"));
        this.tabela.inicializar({
            ordering: false,
            searching: false,
            responsive: true,
            destroy: true,
            processing: false,
            serverSide: true,
            language: $.extend(DataTableLanguageConfigs, {
                emptyTable: "Sua pesquisa não retornou nenhum resultado, tente modificar os filtros atuais"
            }),
            lengthMenu: [10, 25, 50, 100, 200, 500, 1000, 2000, 2500],
            ajax: {
                url: `${this.props.contextoAplicacao()}/rest/imovel/buscarPaginado`,
                type: 'POST',
                contentType: "application/json",
                data: (d) => {
                    let pesquisa = dadosBusca;
                    return JSON.stringify({
                        qtdRegistros : d.length,
                        offset: d.start,
                        ...pesquisa,
                    });
                },
                dataFilter: (resposta) => {
                    let json = $.parseJSON(resposta);
                    return JSON.stringify({
                        recordsTotal: json.quantidadeTotal,
                        recordsFiltered: json.quantidadeTotal,
                        data : json.dados
                    });
                },
                error: function () {
                    alert("Ocorreu um erro inesperado ao buscar os pontos de consumo!");
                },
                beforeSend : this.beforeSend.bind(this),
                complete : this.complete.bind(this)
            },
            columns: [
                {data : null, render: this.renderColunaCheck.bind(this)},
                {data : null, className : 'align-middle text-center details-control', render: this.renderColunaExpandir.bind(this)},
                {data : "descricao", className: 'align-middle text-center', render : (data) => "<span title='"+ data +"'>"+ data + "</span>"},
                {data : null, className: 'align-middle text-center', "defaultContent": '' }
            ],
            columnDefs: [{className: "align-middle text-center", targets : "_all"}]
        }, false);
    }

    private renderColunaCheck(data) {
        return `
        <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
            <input id="chk${data.chavePrimaria}" type="checkbox" name="imoveis" class="custom-control-input checkImovel" value="${data.chavePrimaria}">
            <label class="custom-control-label p-0" for="chk${data.chavePrimaria}"></label>
        </div>`;
    }

    private renderColunaExpandir() {
        return `<a href="#"><i title="Exibir pontos de consumo" class="fa fa-plus text-primary"></i></a>`;
    }

    format (data) {
        let dados = ``;
        let pontosConsumo = data.pontosConsumo;
        pontosConsumo.forEach( (value) => {
            let marcado =  this.pontosMarcados.some((element) => element.chavePrimaria == value.chavePrimaria) ? 'checked' : '';
            dados += `<tr>
                         <td class="align-middle text-center"><i class="fas fa-level-up-alt fa-rotate-90 fa-lg text-primary"></i></td>
                         <td class="align-middle text-center">
                            <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
                                <input id="pontoConsumo${value.chavePrimaria}" type="checkbox" name="pontosConsumo" class="custom-control-input checkPontoConsumo" data-imovel="${data.chavePrimaria}" 
                                    ${marcado} value="${value.chavePrimaria}">
                                <label class="custom-control-label p-0" for="pontoConsumo${value.chavePrimaria}"></label>
                            </div>
                         </td>
                         
                         <td class="align-middle text-center">${value.imovel}</td>
                         <td class="align-middle text-center descricaoPonto">${value.descricao}</td>
                      </tr>`;
        });
        return $(dados);
    }

    private marcarTodos() {
        $('.checkImovel').each(function(){
            if (!$(this).is(":checked")) {
                $(this).trigger('click');
            }
        });
    }

    private desmarcarTodos() {
        $('.checkImovel').each(function(){
            if ($(this).is(":checked")) {
                $(this).trigger('click');
            }
        });
    }

    private configurarAcaoCheckBoxTodos() {
        $('#checkAllPontoConsumo').click(()=>{
            if ($('#checkAllPontoConsumo').is(":checked")) {
                this.marcarTodos();
            } else {
                this.desmarcarTodos();
            }
        });
    }

    private acaoClickCheckImovel(elemento) {
        let tr = $(elemento).closest('tr');
        let row = this.tabela.tabela.row(tr);
        let marcado = false;
        if ($(elemento).is(":checked")) {
            marcado = true;
        }
        if ( this.tabela.tabela.row( '.shown' ).length ) {
            $('.details-control', this.tabela.tabela.row( '.shown' ).node()).click();
        }
        if (!row.child.isShown()) {
            row.child( this.format(row.data())).show();
            tr.addClass('shown');
            $(tr).find("i").removeClass("fa-plus").addClass("fa-minus");
        }
        let idImovel = $(elemento).val();
        let self = this;
        $('.checkPontoConsumo[data-imovel='+idImovel+']').each(function(){
            if (marcado) {
                if (!$(this).is(":checked")) {
                    $(this).prop('checked', marcado);
                    self.armazenarPontoClicado($(this));
                }
            } else {
                if ($(this).is(":checked")) {
                    $(this).prop('checked', marcado);
                    self.armazenarPontoClicado($(this));
                }
            }
        });
        this.configurarClickPontoConsumo();
    }

    private configurarClickImovel() {
        $('.checkImovel').click('click', (event) => {
            let element = event.currentTarget;
            this.acaoClickCheckImovel(element);
        });
    }

    private configurarClickPontoConsumo() {
        $(".checkPontoConsumo").click((event) => {
            let element = event.currentTarget;
            this.armazenarPontoClicado($(element));
        });
    }

    private armazenarPontoClicado(elemento) {
        let idPonto = parseInt(elemento.val());
        let linha = elemento.closest('tr');
        let infoPontoConsumo = linha.children('.descricaoPonto').first();
        let descricao = infoPontoConsumo.html();
        if (elemento.is(":checked")) {
            this.pontosMarcados.push({chavePrimaria : idPonto, descricao : descricao});
        } else {
            this.pontosMarcados = this.pontosMarcados.filter((ele) => {
                return ele.chavePrimaria != idPonto;
            });
            let imovel = elemento.data('imovel');
            $('#chk' + imovel).prop('checked', false);
            $('#checkAllPontoConsumo').prop('checked', false);
        }
    }

    private configurarAcaoExpadir() {
        $('#tablePesquisaPontoConsumo .details-control').click('click', (event) => {
            var element = event.currentTarget;
            var tr = $(element).closest('tr');
            var row = this.tabela.tabela.row(tr);
            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
                $(tr).find("i").removeClass("fa-minus").addClass("fa-plus");
            } else {
                $(tr).find("i").removeClass("fa-plus").addClass("fa-minus");
                if ( this.tabela.tabela.row( '.shown' ).length ) {
                    $('.details-control', this.tabela.tabela.row( '.shown' ).node()).click();
                }
                row.child( this.format(row.data())).show();
                tr.addClass('shown');
                this.configurarClickPontoConsumo();
            }
        });
    }

    private beforeSend() {
        this.callbackPre ? this.callbackPre() : null;
        this.ocultarTabela(true);
    }

    private complete() {
        this.ocultarTabela(false);
        $(".containerTabelaPontoConsumo").css("height", "auto");

        $("#chkTodos").prop("checked", false);
        this.callbackComplete ? this.callbackComplete() : null;
        $('.dataTables_empty').prop('colspan', $('#tablePesquisaPontoConsumo').find('th').length);
        this.configurarAcaoExpadir();
        this.configurarClickImovel();
        this.configurarClickPontoConsumo();
        this.configurarAcaoCheckBoxTodos();
    }

    public ocultarTabela(ocultar) {
        if (ocultar) {
            $(".containerTabelaPontoConsumo").hide();
        } else {
            $(".containerTabelaPontoConsumo").show();
        }
    }
}
