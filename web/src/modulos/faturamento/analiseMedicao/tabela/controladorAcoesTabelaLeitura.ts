/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {CallbackDadosMedicao, DadosMedicao, ModalAlterarDadosMedicao} from "../modalAlterarDadosMedicao";
import {LeituraMovimentoDto} from "../../../../dominio/dto/LeituraMovimentoDto";
import {ModalDetalhamentoLeitura, AnaliseLeitura} from "../modalDetalhamentoLeitura";
import {LeituraMovimentoService} from "../../../../servicos/http/leituraMovimentoService";
import {ModalHistoricoLeitura} from "../modalHistoricoLeitura";


/**
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(ControladorAcoesTabelaLeitura)
export class ControladorAcoesTabelaLeitura {

    @inject(ModalAlterarDadosMedicao)
    private modalAlterarDadosMedicao : ModalAlterarDadosMedicao;

    @inject(ModalDetalhamentoLeitura)
    private modalDetalhamento : ModalDetalhamentoLeitura;
    
    @inject(LeituraMovimentoService)
    private leituraMovimentoService : LeituraMovimentoService;
    
   	@inject(ModalHistoricoLeitura)
    private modalHistoricoLeitura : ModalHistoricoLeitura;

    private $tabela;
    private btnSalvarLeituras;

    @postConstruct()
    public onInit() {
        this.$tabela = $("#tabelaLeituras");
        this.$tabela.on( 'draw.dt', this.registrarClicksAcoes.bind(this));
        this.btnSalvarLeituras = $("#salvarLeituras");
    }

    public obterLeiturasInformadas() {
        let leiturasInformadas : number[] = this.$tabela.DataTable().data().toArray()
            .filter(function(data) {
                return $('#chk' + data.chaveLeituraMovimento).is(':checked') && data.situacaoLeitura.chavePrimaria == 3;
            }).map((data : LeituraMovimentoDto) => (
                data.chaveLeituraMovimento
            ));
        return leiturasInformadas;
    }

    public apenasLeiturasInformadasEstaoSelecionadas() {
        let leiturasInformadas : number[] = this.$tabela.DataTable().data().toArray()
            .filter(function(data) {
                return $('#chk' + data.chaveLeituraMovimento).is(':checked') && data.situacaoLeitura.chavePrimaria != 3;
            }).map((data : LeituraMovimentoDto) => (
                data.chaveLeituraMovimento
            ));
        return leiturasInformadas.length == 0;
    }

    public possuiLeiturasProcessadasMarcadas() {
        let leiturasInformadas : number[] = this.$tabela.DataTable().data().toArray()
            .filter(function(data) {
                return $('#chk' + data.chaveLeituraMovimento).is(':checked') && data.situacaoLeitura.chavePrimaria == 4;
            }).map((data : LeituraMovimentoDto) => (
                data.chaveLeituraMovimento
            ));
        return leiturasInformadas.length > 0;
    }

    public obterLeiturasModificadas() : DadosMedicao[] {
        let medicoesAlteradas : DadosMedicao[] = this.$tabela.DataTable().data().toArray()
            .filter(data => $('#chk' + data.chaveLeituraMovimento).is(':checked') && data.medicaoAlterada)
            .map((data : LeituraMovimentoDto) => ({
                chaveLeitura : data.chaveLeituraMovimento,
                valorLeitura : data.leituraAtual,
                anormalidade: data.anormalidadeLeitura,
                dataLeitura: data.dataLeitura,
                pressao: data.pressao,
                temperatura: data.temperatura,
                observacaoLeitura: data.observacaoLeitura
            }));
        return medicoesAlteradas;
    }

    private registrarClicksAcoes() {

        let gerarFnClickColuna = (fnExibicaoModal : FuncExbicaoModal, fnCallbackAlterarMedicao : FuncAlterarMedicao) => {
            return (e) => {
                let $span = $(e.currentTarget);
                let leituraMovimento = this.extrairLeituraMovimento($span);
                fnExibicaoModal(leituraMovimento, (dados) => {
                    let $linha = $span.parents("tr");
                    fnCallbackAlterarMedicao(leituraMovimento, dados);

                    (leituraMovimento as any).medicaoAlterada = true;
                    this.$tabela.DataTable().row($linha).data(leituraMovimento);
                    this.btnSalvarLeituras.toggleClass("btn-secondary", false);
                    this.btnSalvarLeituras.toggleClass("btn-success", true);
                    this.btnSalvarLeituras.removeAttr("disabled");
                    this.registrarClicksAcoes();
                    this.$tabela.trigger("tabela.atualizacaoConteudo");
                    $('#chk' + leituraMovimento.chaveLeituraMovimento).prop('checked', true);
                });
            }
        };

        this.$tabela.find("span.alterarAnormalidade").unbind("click");
        this.$tabela.find("span.alterarAnormalidade").click(gerarFnClickColuna(
            this.modalAlterarDadosMedicao.exibirAlterarAnormalidade.bind(this.modalAlterarDadosMedicao),
            (leitura, novosDados) => leitura.anormalidadeLeitura = novosDados.anormalidadeLeitura
        ));

        this.$tabela.find("span.alterarLeitura").unbind("click");
        this.$tabela.find("span.alterarLeitura").click(gerarFnClickColuna(
            this.modalAlterarDadosMedicao.exibirAlterarLeitura.bind(this.modalAlterarDadosMedicao),
            (leitura, novosDados) => leitura.leituraAtual = novosDados.leituraAtual
        ));

        this.$tabela.find("span.alterarObservacao").unbind("click");
        this.$tabela.find("span.alterarObservacao").click(gerarFnClickColuna(
            this.modalAlterarDadosMedicao.exibirAlterarObservacao.bind(this.modalAlterarDadosMedicao),
            (leitura, novosDados) => leitura.observacaoLeitura = novosDados.observacaoLeitura
        ));

        this.$tabela.find("span.alterarPressao").unbind("click");
        this.$tabela.find("span.alterarPressao").click(gerarFnClickColuna(
            this.modalAlterarDadosMedicao.exibirAlterarPressao.bind(this.modalAlterarDadosMedicao),
            (leitura, novosDados) => leitura.pressao = novosDados.pressao
        ));

        this.$tabela.find("span.alterarTemperatura").unbind("click");
        this.$tabela.find("span.alterarTemperatura").click(gerarFnClickColuna(
            this.modalAlterarDadosMedicao.exibirAlterarTemperatura.bind(this.modalAlterarDadosMedicao),
            (leitura, novosDados) => leitura.temperatura = novosDados.temperatura
        ));

        this.$tabela.find("span.alterarDataLeitura").unbind("click");
        this.$tabela.find("span.alterarDataLeitura").click(gerarFnClickColuna(
            this.modalAlterarDadosMedicao.exibirAlterarDataLeitura.bind(this.modalAlterarDadosMedicao),
            (leitura, novosDados) => leitura.dataLeitura = novosDados.dataLeitura
        ));

        this.$tabela.find("a.verDetalhes").unbind("click");
        this.$tabela.find("a.verDetalhes").click((e) => {
            let $linha = $(e.currentTarget).parents("tr");
            let leitura : LeituraMovimentoDto = this.$tabela.DataTable().row($linha).data();
            this.modalDetalhamento.exibir(leitura);

        });

        this.$tabela.find(".anormalidade i, .leituraAtual i, .observacaoLeitura i, .temperatura i, .pressao i, .dataLeitura i").unbind("click");
        this.$tabela.find(".anormalidade i, .leituraAtual i, .observacaoLeitura i, .temperatura i, .pressao i, .dataLeitura i").click((e) => {
            $(e.currentTarget).siblings("span").click();
        })
        
        this.$tabela.find("a.verHistorico").unbind("click");
        this.$tabela.find("a.verHistorico").click(async (e) => {
            let $linha = $(e.currentTarget).parents("tr");
            let leitura : LeituraMovimentoDto = this.$tabela.DataTable().row($linha).data();
            
            this.modalHistoricoLeitura.exibir(leitura.chavePontoConsumo);

        });
    }

    private extrairLeituraMovimento($elem) : LeituraMovimentoDto {
        let $linha = $elem.parents("tr");
        return this.$tabela.DataTable().row($linha).data();
    }
    
    
    public obterDadosAnaliseLeitura() : AnaliseLeitura {
		let analiseLeitura: AnaliseLeitura = {
			chavePontoConsumo: 0,
			chaveHistoricoMedicao: 0,
			dataLeitura: "",
			leituraAnterior: "",
			leituraAtual: "",
			dataLeituraAnterior: "",
			chaveAnormalidadeLeitura: 0,
			isConsistir: "false"
		};
        
        //let $inputAnormalidade = $("#detalhamentoAnormalidade").val();
        let leituraAnterior = $("#leituraAnterior").val();
        let leituraAtual = $("#leituraAtual").val();
        let dataLeituraAnterior = $("#dataLeituraAnterior").val();
        let dataLeituraAtual = $("#dataLeituraAtual").val();
        let chaveHistoricoMedicao = $("#chaveHistoricoMedicao").val();
    	let chavePontoConsumo = $("#chavePontoConsumo").val();    
        
        analiseLeitura.leituraAnterior = leituraAnterior;
        analiseLeitura.leituraAtual = leituraAtual;
        analiseLeitura.dataLeitura = dataLeituraAtual;
        analiseLeitura.dataLeituraAnterior = dataLeituraAnterior;
        analiseLeitura.chaveHistoricoMedicao = chaveHistoricoMedicao;
        analiseLeitura.chavePontoConsumo = chavePontoConsumo;
        
        return analiseLeitura;
    }

}

type FuncExbicaoModal = (leituraMovimento : LeituraMovimentoDto, callbackDadosMedicao : CallbackDadosMedicao) => any;
type FuncAlterarMedicao = (leitura : LeituraMovimentoDto, novaLeituraMovimento : LeituraMovimentoDto) => any;
