/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {Datatable, DataTableLanguageConfigs} from "../../../../negocio/datatables";
import {FormPesquisaLeituraMovimento, PesquisaAprovacaoLeitura} from "../FormPesquisaLeituraMovimento";
import {LeituraMovimentoDto} from "../../../../dominio/dto/LeituraMovimentoDto";

/**
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(TabelaListagemLeituraController)
export class TabelaListagemLeituraController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(FormPesquisaLeituraMovimento)
    private form : FormPesquisaLeituraMovimento;

    private callbackPre;
    private callbackComplete;
    private alturaTabela;

    @postConstruct()
    public onInit() {
        $("#chkTodos").click(() => {
            if ($("#chkTodos").prop("checked")) {
                $("[name='checkRegistro']:not([disabled])").prop("checked", true);
            } else {
                $("[name='checkRegistro']:not([disabled])").prop("checked", false);
            }
        });
        $("#tabelaLeituras").on('dt.draw tabela.atualizacaoConteudo', () => {
            $("#tabelaLeituras").colResizable({disable: true});
            $("#tabelaLeituras").colResizable({resizeMode:'overflow', liveDrag:true});
        });
    }

    public configurarCallbacks(callbackPre, callbackComplete) {
        this.callbackPre = callbackPre;
        this.callbackComplete = callbackComplete;
    }


	public recarregarTabela() {
		this.alturaTabela = $(".table-responsive").height();
		new Datatable($("#tabelaLeituras")).inicializar({
			ordering: false,
			searching: false,
			responsive: false,
			destroy: true,
			processing: false,
			serverSide: true,
			language: $.extend(DataTableLanguageConfigs, {
				emptyTable: "Sua pesquisa não retornou nenhum resultado, tente modificar os filtros atuais"
			}),
			pageLength: 2500,
			lengthMenu: [10, 25, 50, 100, 200, 500, 1000, 2000, 2500],
			ajax: {
				url: `${this.props.contextoAplicacao()}/analiseMedicao/buscarLeituras`,
				type: 'POST',
				contentType: "application/json",

				data: (d) => {
					let pesquisa: PesquisaAprovacaoLeitura = this.form.extrairPesquisaFormulario();
					return JSON.stringify({
						qtdRegistros: d.length,
						offset: d.start,
						...pesquisa,
					});
				},
				dataFilter: (resposta) => {
					let json = $.parseJSON(resposta);
					return JSON.stringify({
						recordsTotal: json.quantidadeTotal,
						recordsFiltered: json.quantidadeTotal,
						data: json.dados
					});
				},
				beforeSend: this.beforeSend.bind(this),
				complete: this.complete.bind(this)
			},
			createdRow: function(row, data, dataIndex) {
				if ("anormalidadeLeitura" in data) {
					if (data.anormalidadeLeitura.bloqueiaFaturamento) {
						if (!data.consumoApurado) {
							//$(row).addClass("possuiAnormalidadeGrave");
						}
					} else {
						//$(row).addClass("possuiAnormalidade");
					}
				}
				if ("anormalidadeConsumo" in data) {
					if (data.anormalidadeConsumo.bloqueiaFaturamento) {
						if (!data.consumoApurado) {
							//$(row).addClass("possuiAnormalidadeGrave");
						}
					} else {
						//$(row).addClass("possuiAnormalidade");
					}

				}
			},
			columns: [
				{ data: null, render: (data) => this.renderColunaCheckBox(data) },
				{ data:null, render: this.renderAnormalidade.bind(this)},
				{ data: null, className: "align-middle text-center anormalidade", render: this.renderColunaAnormalidade.bind(this) },
				{ data: null, render: this.renderColunaSituacao.bind(this) },
				{
					data: "pontoConsumo", className: 'align-middle text-center textPontoConsumo',
					render: (data) => "<span title='" + data + "'>" + data + "</span>"
				},
				{
					data: "mesAnoCiclo", className: 'align-middle text-center',
					render: (data) => this.formatarMesAno(data)
				},
				{
					data: "situacaoPontoConsumo", className: 'align-middle text-center textPontoConsumo',
					render: (data) => "<span title='" + data + "'>" + data + "</span>"
				},
				{ data: "leituraAnterior", defaultContent: "-" },
				{
					data: null, className: "align-middle text-center leituraAtual",
					render: this.renderColunaLeituraAtual.bind(this)
				},
				{
					data: null, className: 'align-middle text-center dataLeitura',
					render: (data) => this.renderColunaEditavel(data.dataLeitura, data.medicaoAlterada,
						data.situacaoLeitura.chavePrimaria !== 4, "alterarDataLeitura", 'Alterar Data de Leitura')
				},
				{
					data: null, className: 'align-middle text-center pressao',
					render: (data) => this.renderColunaEditavel(this.renderNumero(data.pressao), data.medicaoAlterada,
						data.situacaoLeitura.chavePrimaria !== 4 && (data.corrigePT == undefined || data.corrigePT == true)
						, "alterarPressao", 'Alterar Pressão')
				},
				{
					data: null, className: 'align-middle text-center temperatura',
					render: (data) => this.renderColunaEditavel(this.renderNumero(data.temperatura, 1), data.medicaoAlterada,
						data.situacaoLeitura.chavePrimaria !== 4 && (data.corrigePT == undefined || data.corrigePT == true)
						, "alterarTemperatura", 'Alterar Temperatura')
				},
				{ data: null, className: "align-middle text-center", render: this.renderFotoColetor.bind(this) },
				{
					data: null, defaultContent: "-", className: 'align-middle text-center observacaoLeitura',
					render: data => this.renderColunaEditavel(data.observacaoLeitura, data.medicaoAlterada,
						data.situacaoLeitura.chavePrimaria !== 4, "alterarObservacao", 'Alterar Observação de Leitura')
				},
				{ data: "volume", render: (data) => this.renderNumero(data) },
				{ data: "fcPtz", render: (data) => this.renderNumero(data, 4) },
				{ data: "volumePtz", render: (data) => this.renderNumero(data, 4) },
				{ data: null, render: (data: LeituraMovimentoDto) => data.anormalidadeConsumo ? data.anormalidadeConsumo.descricao : "-" },
				{ data: "tipoConsumo", defaultContent: "-" },
				{ data: "consumoApurado", render: data => this.renderNumero(data, 3) },
				{ data: null, className: "align-middle text-center acoes", render: this.renderColunaAcoes.bind(this) },
				{ data: null, className: "align-middle text-center acoes", render: this.renderColunaHistorico.bind(this) },

			],
			columnDefs: [{ className: "align-middle text-center", targets: "_all" }]
		}, false);

	}

    private renderColunaCheckBox(data) {
        return `
        <div class="custom-control custom-checkbox" style="display: inline-grid !important;">
            <input id="chk${data.chaveLeituraMovimento}" type="checkbox" name="chavesPrimarias" class="custom-control-input" value="${data.chaveLeituraMovimento}">
            <label class="custom-control-label" for="chk${data.chaveLeituraMovimento}"></label>
        </div>`;
    }

    private renderColunaSituacao(data) {
        if (data.situacaoLeitura.chavePrimaria != '3') {
            return data.situacaoLeitura.descricao
        } else if (data.situacaoLeitura.chavePrimaria == '3') {
            return "Leitura Informada";
        }
    }

    private renderColunaLeituraAtual(data) {
        let leitura = typeof data.leituraAtual === "undefined" ? "Sem Leitura" : data.leituraAtual;
        if (data.situacaoLeitura.chavePrimaria === 4) {
            return leitura;
        } else {
            return `<i title="Alterar Leitura" class='fa fa-sm fa-edit text-secondary fa-fw float-right'></i>`
                + `<span class='alterarLeitura ${data.medicaoAlterada ? 'text-danger' : ''}' title="${leitura}" >${leitura}</span>`;
        }
    }

    private renderColunaAnormalidade(data) {
        let render = "-";
        if (data.anormalidadeLeitura) {
            render = data.anormalidadeLeitura.descricao;
        }
        if (data.situacaoLeitura.chavePrimaria === 4) {
            return render;
        } else {
            return `<i class='fa text-secondary fa-sm fa-edit float-right fa-fw' title="Alterar Anormalidade de Leitura"></i> `
                + `<span title='${render}' class='alterarAnormalidade ${data.medicaoAlterada ? 'text-danger' : ''}'>${render}</span>`;
        }
    }

    private formatarMesAno(data) {
        let ano = data.substring(0,4);
        let mes = data.substring(4,6);
        return mes + "/" + ano;
    }

    private renderColunaEditavel(data, medicaoAlterada, permitirEdicao, classeCss = '', customEditTile = 'Clique para alterar este valor') {
        let render = data ? data : "-";
        if (permitirEdicao) {
            return `<span title='${render}' class='${classeCss} ${medicaoAlterada ? 'text-danger' : ''}'>${render}</span>`
                + `<i class='fa text-secondary fa-sm fa-edit float-right fa-fw' title="${customEditTile}"></i>`;
        } else {
            return `<span title='${render}' class="truncaTexto">${render}</span>`;
        }
    }

    private renderColunaAcoes(data) {
        return $("<div>")
            .append(
                $("<a>", {'data-idLeitura': data.chaveLeituraMovimento, 'data-leitura': data.leituraAtual || "",
                    'data-anormalidade' : data.anormalidadeLeitura && data.anormalidadeLeitura.chavePrimaria || '',
                    title: 'Ver detalhes', href: "javascript:void(0)", class: 'verDetalhes text-secondary'})
                    .append($("<i>", {class: "fa fa-search fa-sm fa-fw"}))

            ).html();
    }
    
    private renderColunaHistorico(data) {
		        return $("<div>")
            .append(
                $("<a>", {'data-idPontoConsumo': data.chavePontoConsumo,
                    title: 'Ver detalhes', href: "javascript:void(0)", class: 'verHistorico text-secondary'})
                    .append($("<i>", {class: "fa fa-search fa-sm fa-fw"}))

            ).html();
	}

    private renderNumero(data, numCasas = 2) {
        return (data !== null && data !== undefined) ? String(Number(data).toFixed(numCasas)).replace(".", ",") : "-";
    }

    private beforeSend() {
        this.callbackPre ? this.callbackPre() : null;
        this.alturaTabela = this.alturaTabela ? this.alturaTabela : $(".containerTabela").height();
        this.ocultarTabela(true);
        $(".containerTabela").css("height", this.alturaTabela);
    }

    private complete() {
        this.ocultarTabela(false);
        $(".containerTabela").css("height", "auto");
        this.alturaTabela = null;
        $("#chkTodos").prop("checked", false);
        this.callbackComplete ? this.callbackComplete() : null;
        $("#tabelaLeituras").trigger('tabela.atualizacaoConteudo');
        if ($('#tabelaLeituras input[name=chavesPrimarias]').length > 0) {
            $('#solicitarReleitura').prop('disabled', false);
        } else {
            $('#solicitarReleitura').prop('disabled', true);
        }
        
        var opt = {
			autoOpen: false,
			width: 800,
			modal: true,
			minHeight: 120,
			resizable: false
		};

		$("a[rel=modal]").click(function(ev) {
			$("#fotoColetorPopup").dialog(opt).dialog("open");
		});
    }

    public ocultarTabela(ocultar) {
        $(".containerTabela").toggleClass("ocultarTabela", ocultar);
    }
    
	private renderFotoColetor(data) {
		if (data.fotoColetor != null && data.fotoColetor != "") {
			return "<a id='fotoColetor' rel='modal' onclick='selecionarFoto("+data.chaveRota+", "+data.mesAnoCiclo+", "+data.chaveLeituraMovimento+")'>"
						+ " <img id='fotoColetor' src='imagens/images.png' border='0' style='width:40px; height:40px;'> "
					+ " </a> "
					+ " <div class='window' id='fotoColetorPopup'  title='Foto Coletor' style='display :none;'> "
					+ " </div> "
		} else {
			return "";
		}
	}
	
	private renderAnormalidade(data){
		
		var retorno = "<img id='imagem' src='imagens/circle_green.png' border='0'>";
		
		if ("anormalidadeLeitura" in data) {
			if (data.anormalidadeLeitura.bloqueiaFaturamento) {
				if (!data.consumoApurado) {
					retorno = "<img id='imagem' src='imagens/circle_red.png' border='0'>";
				}
			} else {
				retorno = "<img id='imagem' src='imagens/circle_yellow.png' border='0'>";
			}
		}
		if ("anormalidadeConsumo" in data) {
			if (data.anormalidadeConsumo.bloqueiaFaturamento) {
				retorno = "<img id='imagem' src='imagens/circle_red.png' border='0'>";
			} else {
				retorno = "<img id='imagem' src='imagens/circle_yellow.png' border='0'>";
			}

		}
		return retorno;
	}

}
