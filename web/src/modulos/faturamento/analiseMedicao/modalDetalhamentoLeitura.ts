/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {postConstruct} from "inversify";
import {LeituraMovimentoDto} from "../../../dominio/dto/LeituraMovimentoDto";

/**
 * Controlador do Modal de detalhamento da análise de medição de leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(ModalDetalhamentoLeitura)
export class ModalDetalhamentoLeitura {

    private $modal;
    private $inputAnormalidade;
    private $inputOrigemMedicao;
    private $detalhamentoLeitura;
    private $inputAnormalidadeConsumo;
    private $inputLeituraAnterior;
    private $inputLeituraAtual;
    private $inputDataLeituraAnterior;
    private $inputDataLeituraAtual;
    private $inputChaveHistoricoMedicao;
    private $inputChavePontoConsumo;
    private $inputBotaoSalvarConsistir;


    @postConstruct()
    public onInit() {
        this.$modal = $("#modalDetalhamentoLeitura");
        this.$inputAnormalidade = $("#detalhamentoAnormalidade");
        this.$detalhamentoLeitura = $("#detalhamentoLeitura")
        this.$inputOrigemMedicao = $("#detalhamentoOrigem")
        this.$inputAnormalidadeConsumo = $("#detalhamentoAnormalidadeConsumo")
        this.$inputLeituraAnterior = $("#leituraAnterior")
        this.$inputLeituraAtual = $("#leituraAtual")
        this.$inputDataLeituraAnterior = $("#dataLeituraAnterior")
        this.$inputDataLeituraAtual = $("#dataLeituraAtual")
        this.$inputChaveHistoricoMedicao = $("#chaveHistoricoMedicao")
    	this.$inputChavePontoConsumo = $("#chavePontoConsumo")
    	this.$inputBotaoSalvarConsistir = $("#salvarConsitir");
    	
    	this.$inputDataLeituraAtual.datetimepicker({
            locale: "pt-BR",
            format: 'DD/MM/YYYY'
        });
        
    	this.$inputDataLeituraAnterior.datetimepicker({
            locale: "pt-BR",
            format: 'DD/MM/YYYY'
        });
                                                    
    }

    public exibir(leitura : LeituraMovimentoDto) {
        this.$modal.modal();
        this.$detalhamentoLeitura.text(leitura.pontoConsumo + (leitura.dataLeitura ? (" - " + leitura.dataLeitura) : ""));
        this.$inputOrigemMedicao.val(leitura.origem ? leitura.origem : "");
       	this.$inputAnormalidadeConsumo.val(leitura.anormalidadeConsumo ? leitura.anormalidadeConsumo.descricao : "");
    	this.$inputLeituraAnterior.val(leitura.leituraAnterior);
    	this.$inputLeituraAtual.val(leitura.leituraAtual);
    	this.$inputDataLeituraAnterior.val(leitura.dataLeituraAnterior);
    	this.$inputDataLeituraAtual.val(leitura.dataLeitura);
    	this.$inputChavePontoConsumo.val(leitura.chavePontoConsumo);
    	this.$inputChaveHistoricoMedicao.val(leitura.chaveHistoricoMedicao);
    	
    	 if (leitura.anormalidadeLeitura) {
            this.$inputAnormalidade.val(leitura.anormalidadeLeitura.chavePrimaria);
        } else {
            this.$inputAnormalidade.val("");
        }
        
        if(leitura.chaveHistoricoMedicao == null) {
			this.desabilitarCamposModal();
		} else {
			this.habilitarCamposModal();
		}

    }
    
    public esconder(){
		this.$modal.modal('hide');
	}
	
	private desabilitarCamposModal(): void {
    this.$modal.find('input, select, textarea').each((index, element) => {
        $(element).prop('disabled', true);
    });
    
    this.$inputBotaoSalvarConsistir.prop('disabled', true);
}
	private habilitarCamposModal(): void {
		this.$modal.find('input, select, textarea').each((index, element) => {
			$(element).prop('disabled', false);
		});

		this.$inputBotaoSalvarConsistir.prop('disabled', false);
		this.$inputAnormalidadeConsumo.prop('disabled', true);
		this.$inputOrigemMedicao.prop('disabled', true);


	}
    
}

export interface AnaliseLeitura {
	chavePontoConsumo: number,
	chaveHistoricoMedicao: number,
	dataLeitura: string,
	leituraAnterior: string,
	leituraAtual: string,
	dataLeituraAnterior: string,
	chaveAnormalidadeLeitura: number,
	isConsistir: string
		
}