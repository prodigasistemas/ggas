/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {inject, postConstruct} from "inversify";
import {provideSingleton} from "../../../../singleton";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {LoadingService} from "../../../../servicos/loading.service";
import {NotificadorService} from "../../../../servicos/notificador.service";
import {PesquisaAnormalidadeController} from "../../../medicao/anormalidade/pesquisa/PesquisaAnormalidadeController";


/**
 * Formulário de pesquisa de leitura movimento
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provideSingleton(FormAnaliseAnormalidade)
export class FormAnaliseAnormalidade {

    private FORMATO_DATA_HORA = "d/m/Y";
    private $rota;

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(NotificadorService)
    private notificador : NotificadorService;

    @inject(LoadingService)
    private loading : LoadingService;

    @postConstruct()
    public async onInit() {
        this.$rota = $("#idsRota");
        this.configurarSelectRotas();
        this.configurarBotaoPesquisar();
    }


    public onSubmit(callback) {
        $("#excecaoLeituraFaturaForm").unbind();
        $("#excecaoLeituraFaturaForm").submit((e) => {
            e.preventDefault();
            if (this.validar()) {
                callback(e);
            }
        });
    }

    public extrairPesquisaFormulario() : PesquisaAnaliseAnormalidade {
        let dataGeracaoInicial, dataGeracaoFinal, dataMedicaoInicial, dataMedicaoFinal;
        if ($('#dataGeracaoInicial').val()) {
            dataGeracaoInicial = $('#dataGeracaoInicial').datepicker('getDate');
            dataGeracaoInicial = dataGeracaoInicial.format(this.FORMATO_DATA_HORA);
        }
        if ($('#dataGeracaoFinal').val()) {
            dataGeracaoFinal = $('#dataGeracaoFinal').datepicker('getDate');
            dataGeracaoFinal.setDate(dataGeracaoFinal.getDate() + 1);
            dataGeracaoFinal = dataGeracaoFinal.format(this.FORMATO_DATA_HORA);
        }
        if ($('#dataMedicaoInicial').val()) {
            dataMedicaoInicial = $('#dataMedicaoInicial').datepicker('getDate');
            dataMedicaoInicial = dataMedicaoInicial.format(this.FORMATO_DATA_HORA);
        }
        if ($('#dataMedicaoFinal').val()) {
            dataMedicaoFinal = $('#dataMedicaoFinal').datepicker('getDate');
            dataMedicaoFinal.setDate(dataMedicaoFinal.getDate() + 1);
            dataMedicaoFinal = dataMedicaoFinal.format(this.FORMATO_DATA_HORA);
        }

        let percentualVariacaoValorFatura = $('#percentualVariacaoValorFatura').val();
        let referencia = $('#referencia').val();
        let ciclo = $('#ciclo').val();


        let idPontoConsumo = $('#idPontoConsumo').val();
        let cepImovel = $('#cepImovel').val();
        let numeroImovel = $('#numeroImovel').val();
        let complementoImovel = $('#complementoImovel').val();
        let nomeImovel = $('#nomeImovel').val();
        let matriculaImovel = $('#matriculaImovel').val();
        let indicadorCondominio = $("[name='indicadorCondominio']:checked").val();

        let cpfCliente = $('#cpfCliente').val();
        let rgCliente = $('#rgCliente').val();
        let nomeCliente = $('#nomeCliente').val();
        let numeroPassaporte = $('#numeroPassaporte').val();
        let cnpjCliente = $('#cnpjCliente').val();
        let nomeFantasiaCliente = $('#nomeFantasiaCliente').val();

        let chavePrimaria = $('#chavePrimaria').val();
        let idLocalidade = $('#idLocalidade').val();
        let idSetorComercial = $('#idSetorComercial').val();
        let idGrupoFaturamento = $('#idGrupoFaturamento').val();
        let idSegmento = $('#idSegmento').val();
        let idSituacaoPontoConsumo = $('#idSituacaoPontoConsumo').val();
        let idTipoConsumo = $('#idTipoConsumo').val();
        let idAnormalidadeFaturamento = $('#idAnormalidadeFaturamento').val();
        let analisada = $("[name='analisada']:checked").val();
        let indicadorImpedeFaturamento = $("[name='indicadorImpedeFaturamento']:checked").val();
        let chavesPrimarias = [];//$('#chavesPrimarias').val(); tem q transformar em array
        let idsRota = this.$rota.select2("data").map(v => Number(v.id));

        return {dataGeracaoInicial: dataGeracaoInicial, dataGeracaoFinal: dataGeracaoFinal, dataMedicaoInicial: dataMedicaoInicial,
            dataMedicaoFinal: dataMedicaoFinal, percentualVariacaoValorFatura: percentualVariacaoValorFatura, referencia: referencia,
            ciclo: ciclo, idPontoConsumo: idPontoConsumo, cepImovel: cepImovel, numeroImovel: numeroImovel, complementoImovel: complementoImovel, nomeImovel: nomeImovel,
            matriculaImovel: matriculaImovel, indicadorCondominio: indicadorCondominio, cpfCliente: cpfCliente, rgCliente: rgCliente,
            nomeCliente: nomeCliente, numeroPassaporte: numeroPassaporte, cnpjCliente: cnpjCliente, nomeFantasiaCliente: nomeFantasiaCliente,
            chavePrimaria: chavePrimaria, idLocalidade: idLocalidade, idSetorComercial: idSetorComercial, idGrupoFaturamento: idGrupoFaturamento,
            idSegmento: idSegmento, idSituacaoPontoConsumo: idSituacaoPontoConsumo, idTipoConsumo: idTipoConsumo, idAnormalidadeFaturamento: idAnormalidadeFaturamento,
            analisada: analisada, indicadorImpedeFaturamento: indicadorImpedeFaturamento, chavesPrimarias: chavesPrimarias, idsRota: idsRota};
    }

    private configurarSelectRotas() {
        this.$rota.select2({
            theme: "bootstrap",
            placeholder: "Selecione uma ou várias rotas",
            containerCssClass: ':all:',
            language: 'pt-BR',
            width: null
        });
    }

    private validar() : boolean {
        return this.validarFormatoDasDatas() && this.validarIntevaloEntreDatas();
    }

    private validarFormatoDasDatas() {
        let valido = true;
        let self = this;
        $(".campoData").each(function(){
            let val = $(this).val();
            if (val !== '' ){
                let parts = val.split("/");
                let ano = Number(parts[2]);
                let mes = Number(parts[1]) - 1;
                let dia = Number(parts[0]);
                let d = new Date(ano, mes, dia);
                if (d.getFullYear() !== ano || d.getMonth() !== mes || d.getDate() !== dia) {
                    self.notificador.exibirMensagemAtencao(null, "Informe uma data válida.");
                    $(this).focus();
                    valido = false;
                    return;
                }
            }
        });
        return valido;
    }

    private validarIntevaloEntreDatas() {
        let valido = true;
        let dataGeracaoInicial = $('#dataGeracaoInicial').val();
        let dataGeracaoFinal = $('#dataGeracaoFinal').val();
        let dataMedicaoInicial = $('#dataMedicaoInicial').val();
        let dataMedicaoFinal = $('#dataMedicaoFinal').val();
        if (dataGeracaoInicial != '' && dataGeracaoFinal != '') {
            if (this.dataStringTodataObj(dataGeracaoInicial) > this.dataStringTodataObj(dataGeracaoFinal)) {
                this.notificador.exibirMensagemAtencao(null, "A data inicial da emissão deve ser menor ou igual a data final.");
                valido = false;
            }
        }
        if (dataMedicaoInicial != '' && dataMedicaoFinal != '') {
            if (this.dataStringTodataObj(dataMedicaoInicial) > this.dataStringTodataObj(dataMedicaoFinal)) {
                this.notificador.exibirMensagemAtencao(null, "A data inicial da medição deve ser menor ou igual a data final.");
                valido = false;
            }
        }
        return valido;
    }

    private dataStringTodataObj(dataString) {
        let parts = dataString.split("/");
        let ano = Number(parts[2]);
        let mes = Number(parts[1]) - 1;
        let dia = Number(parts[0]);
        return new Date(ano, mes, dia);
    }

    private configurarBotaoPesquisar() {
        $("#botaoPesquisar").click(e => {
            e.preventDefault();
            if (this.validar()) {
                $("#excecaoLeituraFaturaForm").submit();
            }
        });
    }
}

export interface PesquisaAnaliseAnormalidade {
    dataGeracaoInicial :string,
    dataGeracaoFinal :string,
    dataMedicaoInicial :string,
    dataMedicaoFinal :string,
    percentualVariacaoValorFatura :string,
    referencia :string,
    ciclo :string,

    idPontoConsumo : number,
    cepImovel :string,
    numeroImovel :string,
    complementoImovel :string,
    nomeImovel :string,
    matriculaImovel :string,
    indicadorCondominio :boolean,

    cpfCliente :string,
    rgCliente :string,
    nomeCliente :string,
    numeroPassaporte :string,
    cnpjCliente :string,
    nomeFantasiaCliente :string,

    chavePrimaria :number,
    idLocalidade :number,
    idSetorComercial :number,
    idGrupoFaturamento :number,
    idSegmento :number,
    idSituacaoPontoConsumo :number,
    idTipoConsumo :number,
    idAnormalidadeFaturamento :number,

    analisada :boolean,
    indicadorImpedeFaturamento :boolean,

    chavesPrimarias : number[],
    idsRota : number[]
}
