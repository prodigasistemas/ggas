/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {LoadingService} from "../../../../servicos/loading.service";
import {NotificadorService} from "../../../../servicos/notificador.service";
import {FormAnaliseAnormalidade} from "./FormAnaliseAnormalidade";
import {TabelaAnaliseAnormalidade} from "./tabelaAnaliseAnormalidade";

declare let $ : any, cpfCnpj : any;

declare function marcarAnalisada(): void;
declare function incluirFaturaAnomalia(): void;
declare function gerarRelatorio(): void;

/**
 * Controlador padrão da tela de pesquisa da análise de anormalidade de faturamento
 *
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provide(AnaliseAnormalidadeController)
export class AnaliseAnormalidadeController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(LoadingService)
    private loading : LoadingService;

    @inject(NotificadorService)
    private notificador : NotificadorService;


    @inject(FormAnaliseAnormalidade)
    private form : FormAnaliseAnormalidade;

    @inject(TabelaAnaliseAnormalidade)
    private tabela : TabelaAnaliseAnormalidade;



    @postConstruct()
    public onInit() {
        this.configurarTabela();

        this.form.onSubmit((e) => {
            e.preventDefault();
            this.tabela.recarregarTabela();
        });
        $('#resolverAnormalidade').click(() => this.resolverAnormalidade());
        $('#naoFaturar').click(() => this.naoFaturar());
        $('#gerarRelatorio').click(() => this.relatorioAnaliseFatura());
        let pesquisaAutomatica = $('#pesquisaAutomatica').val();
        if (pesquisaAutomatica) {
            $("#botaoPesquisar").submit();
        }
    }

    private verificarCheckBoxSelecionado() {
        let qtdSelecionado = $('#tableAnaliseAnormalidade input[name=chavesPrimarias]:checked').length;
        if (qtdSelecionado > 0) {
            return true;
        } else {
            this.notificador.exibirMensagemAtencao(null, "Selecione um ou mais registros para realizar a operação!");
            return false
        }
    }

    private async resolverAnormalidade() {
        if (this.verificarCheckBoxSelecionado()) {
            let resposta = await this.notificador.exibirPergunta("", "Deseja incluir fatura para os itens selecionados?");
            if (resposta.value) {
                incluirFaturaAnomalia();
            }
        }
    }

    private async naoFaturar() {
        if (this.verificarCheckBoxSelecionado()) {
            let resposta = await this.notificador.exibirPergunta("", "Deseja marcar os itens selecionados como analisados?");
            if (resposta.value) {
                marcarAnalisada();
            }
        }
    }

    private async relatorioAnaliseFatura() {
        if (this.verificarCheckBoxSelecionado()) {
            let resposta = await this.notificador.exibirPergunta("", "Deseja gerar o relatório dos itens selecionados?");
            if (resposta.value) {
                gerarRelatorio();
            }
        }
    }

    /**
     * Configura os callbacks de tabela
     */
    private configurarTabela() {
        let preLoading = () => {
            this.loading.exibir(true);
        };

        let posLoading = () => {
            this.loading.exibir(false);
        };

        this.tabela.configurarCallbacks(preLoading, posLoading);
    }

}
