import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {Datatable, DataTableLanguageConfigs} from "../../../../negocio/datatables";
import {FormAnaliseAnormalidade} from "./FormAnaliseAnormalidade";


/**
 *
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provide(TabelaAnaliseAnormalidade)
export class TabelaAnaliseAnormalidade {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(FormAnaliseAnormalidade)
    private form : FormAnaliseAnormalidade;

    private callbackPre;
    private callbackComplete;
    private tabela;

    @postConstruct()
    public onInit() {
        $("#tableAnaliseAnormalidade").on('dt.draw tabela.atualizacaoConteudo', () => {
            $("#tableAnaliseAnormalidade").colResizable({disable: true});
            $("#tableAnaliseAnormalidade").colResizable({resizeMode:'overflow', liveDrag:true});
        });
    }

    public configurarCallbacks(callbackPre, callbackComplete) {
        this.callbackPre = callbackPre;
        this.callbackComplete = callbackComplete;
    }

    public recarregarTabela() {
        new Datatable($("#tableAnaliseAnormalidade")).inicializar({
            ordering: false,
            searching: false,
            responsive: false,
            destroy: true,
            processing: false,
            serverSide: true,
            language: $.extend(DataTableLanguageConfigs, {
                emptyTable: "Sua pesquisa não retornou nenhum resultado, tente modificar os filtros atuais"
            }),
            lengthMenu: [10, 25, 50, 100, 200, 500, 1000, 2000, 2500],
            ajax: {
                url: `${this.props.contextoAplicacao()}/pesquisarExcecaoLeituraFaturaDataTable`,
                type: 'POST',
                contentType: "application/json",
                data: (d) => {
                    let pesquisa = this.form.extrairPesquisaFormulario();
                    return JSON.stringify({
                        qtdRegistros : d.length,
                        offset: d.start,
                        ...pesquisa,
                    });
                },
                dataFilter: (resposta) => {
                    let json = $.parseJSON(resposta);
                    return JSON.stringify({
                        recordsTotal: json.quantidadeTotal,
                        recordsFiltered: json.quantidadeTotal,
                        data : json.dados
                    });
                },
                error: function () {
                    alert("Ocorreu um erro inesperado ao realizar a pesquisa!");
                },
                beforeSend : this.beforeSend.bind(this),
                complete : this.complete.bind(this)
            },
            columns: [
                {data : null, render: this.renderColunaCheck.bind(this)},
                {data : "imovel", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "pontoConsumo", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "segmento", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "anormalidade", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : null, className: 'align-middle text-center', render: this.renderColunaTarifa.bind(this)},
                {data : "faturaId", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "anoMes", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "ciclo", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "dataEmissao", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "dataResolucao", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "analisada", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : "usuario", defaultContent: '', className: 'align-middle text-center', render : (data) => data},
                {data : null, defaultContent: '', className: 'align-middle text-center', render: this.renderColunaDetalhes.bind(this)},
            ],
            columnDefs: [{className: "align-middle text-center", targets : "_all"}]
        }, false);
    }

    private renderColunaDetalhes(data) {
        return `<a href="exibirDetalhamentoAnormalidadeFaturamento?historico=${data.historicoId}">
                    <i class="fa fa-search"></i>
                </a>`;
    }

    private renderColunaCheck(data) {
        return `
        <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1" style="display: inline-grid !important;">
            <input id="chk${data.historicoId}" type="checkbox" name="chavesPrimarias" class="custom-control-input" value="${data.historicoId}">
            <label class="custom-control-label p-0" for="chk${data.historicoId}"></label>
        </div>`;
    }

    private renderColunaTarifa(data) {
        if (data.tarifas)
            return data.tarifas.join(", ");
        return ``;
    }

    private beforeSend() {
        this.callbackPre ? this.callbackPre() : null;
        this.ocultarTabela(true);
    }

    private complete() {
        this.ocultarTabela(false);
        this.callbackComplete ? this.callbackComplete() : null;
        $("#chkTodos").prop("checked", false);
        $("#tableAnaliseAnormalidade").trigger('tabela.atualizacaoConteudo');
        $('.dataTables_empty').prop('colspan', $('#tableAnaliseAnormalidade').find('th').length);
    }

    public ocultarTabela(ocultar) {
        $(".containerTabela").toggleClass("ocultarTabela", ocultar);
    }
}
