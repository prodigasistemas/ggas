/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {FaturamentoAnormalidadeDto} from "../../../dominio/dto/FaturamentoAnormalidade";
import {NotificadorService} from "../../../servicos/notificador.service";
import {AnormalidadeService} from "../../../servicos/http/anormalidadeService";
import {LoadingService} from "../../../servicos/loading.service";
import {PropriedadesSistemaService} from "../../../servicos/propriedades.sistema.service";

/**
 * Controlador do Modal de detalhamento da análise de medição de leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(ModalAlterarAnormalidade)
export class ModalAlterarAnormalidade {

    private $modal;

    private $inputChavePrimariaAnormalidadeForm;
    private $inputDescricao;
    private $inputImpedirFaturamentoTrue;
    private $inputImpedirFaturamentoFalse;
    private $inputIndicadorUsoForm;
    private $inputIndicadorUsoFalse;
    private $btnSalvar;

    @inject(NotificadorService)
    private notificador: NotificadorService;

    @inject(AnormalidadeService)
    private anormalidadeService: AnormalidadeService;

    @inject(LoadingService)
    private loading: LoadingService;

    @inject(PropriedadesSistemaService)
    private props: PropriedadesSistemaService;

    @postConstruct()
    public onInit() {
        this.$modal = $("#modalAlterarAnormalidade");
        this.$inputChavePrimariaAnormalidadeForm = $("#chavePrimariaAnormalidadeForm");
        this.$inputDescricao = $("#descricaoForm");

        this.$inputImpedirFaturamentoTrue = $("#impedirFaturamento");
        this.$inputImpedirFaturamentoFalse = $("#impedirFaturamentoFalse");

        this.$inputIndicadorUsoForm = $("#indicadorUsoForm");
        this.$inputIndicadorUsoFalse = $("#indicadorUsoFalse");

        this.$btnSalvar = $("#salvarAnormalidade");

        this.$btnSalvar.click(async (e) => {
            e.preventDefault();
            let resposta = await this.notificador.exibirPergunta("", "Deseja realmente alterar a anormalidade? ");
            if (resposta.value) {
                await this.salvarAnormalidade();
            }
        });
    }

    public exibir(anormalidade: FaturamentoAnormalidadeDto) {
        this.$modal.modal();

        this.$inputChavePrimariaAnormalidadeForm.val(anormalidade.chaveprimaria);
        this.$inputDescricao.val(anormalidade.descricao);
        this.$inputImpedirFaturamentoTrue.attr('checked', anormalidade.indicadorimpedefaturamento);
        this.$inputImpedirFaturamentoFalse.attr('checked', !anormalidade.indicadorimpedefaturamento);

        this.$inputIndicadorUsoForm.attr('checked', anormalidade.habilitado);
        this.$inputIndicadorUsoFalse.attr('checked', !anormalidade.habilitado);
    }

    private async salvarAnormalidade() {
        const descricao = $('#descricao').val();
        const indicadorUso = $('#indicadorUso').val();
        const indicadorImpedeFaturamento = $('#indicadorImpedeFaturamento').val();

        this.loading.exibir(true);
        let resposta = await this.anormalidadeService.salvarAnormalidade(
            this.$inputChavePrimariaAnormalidadeForm.val(),
            this.$inputDescricao.val(),
            $("input[name='impedirFaturamentoForm']:checked").val() === 'true',
            $("input[name='indicadorUsoForm']:checked").val() === 'true');
        this.loading.exibir(false);
        console.log('ok');
        this.notificador.tratarRespostaHttp(resposta, 'Anormalidade salva com sucesso', () => {
            window.location.href = `${this.props.contextoAplicacao()}/pesquisarFaturamentoAnormalidade?&descricao=${descricao}&indicadorUso=${indicadorUso}&indicadorImpedeFaturamento=${indicadorImpedeFaturamento}`;
        });
    }

}
