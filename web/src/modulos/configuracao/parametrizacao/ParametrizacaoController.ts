/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
import { inject, postConstruct } from "inversify";
import { provide } from "inversify-binding-decorators";
import { NotificadorService } from "../../../servicos/notificador.service";
import {PropriedadesSistemaService} from "../../../servicos/propriedades.sistema.service";


declare let $: any;


@provide(ParametrizacaoController)
export class ParametrizacaoController {
	
	@inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;
    
	@inject(NotificadorService)
	private notificador: NotificadorService;

	private $buttonValidarCertificado;

	@postConstruct()
	public onInit() {
		this.configurarElementosPagina();
	}


private configurarElementosPagina() {
    this.$buttonValidarCertificado = $("#botaoValidarPopup");

    this.$buttonValidarCertificado.click(async () => {
        try {
            const arquivoInput = document.getElementById("arquivoCertificado") as HTMLInputElement;
            const senhaInput = document.querySelector("input[name='senhaCertificado']") as HTMLInputElement;

            if (!arquivoInput || !senhaInput) {
                this.notificador.exibirMensagemErro("Erro!", "Certifique-se de que o arquivo e a senha foram preenchidos.");
                return;
            }

			const arquivo = arquivoInput.files && arquivoInput.files[0] ? arquivoInput.files[0] : null;

            const senha = senhaInput.value;

            if (!arquivo || !senha) {
                this.notificador.exibirMensagemErro("Erro!", "Certifique-se de que o arquivo e a senha foram preenchidos.");
            	return;
            }

            const tipoArquivo = arquivo.type;
            const extensaoPermitida = ["application/x-pkcs12", "application/x-pkcs7-certificates"]; // Tipo MIME para .pfx e .p12

            if (!extensaoPermitida.includes(tipoArquivo)) {
				this.notificador.exibirMensagemErro("Formato Invalido!", "Por favor, selecione um arquivo do tipo .pfx ou .p12." );
                return;
            }

            const formData = new FormData();
            formData.append("arquivo", arquivo);
            formData.append("senhaCertificado", senha);

            const response = await fetch(`${this.props.contextoAplicacao()}/validarCertificado`, {
                method: "POST",
                body: formData,
            });

            if (!response.ok) {
                throw new Error(`Erro na resposta do servidor: ${response.statusText}`);
            }

            const data = await response.json();

            if (data.status === "success") {
				this.notificador.exibirMensagemSucesso("Sucesso!", data.message);
                $("#nomeCertificadoDigital").val(data.nomeArquivo);
                $("#importarCertificadoPopup").dialog("close");
            } else {
                this.notificador.exibirMensagemErro("Erro!", data.message);
            }

        } catch (error) {
            this.notificador.exibirMensagemErro("Erro!","Ocorreu um erro ao validar o certificado. Tente novamente.");
        }
    });
}

}
