import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import "ladda/dist/ladda-themeless.min.css"
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {LoadingService} from "../../../../servicos/loading.service";
import {NotificadorService} from "../../../../servicos/notificador.service";
import {TabelaListagemProcessoController} from "./tabelaProcessos";
import {ProcessoService} from "../../../../servicos/http/processoService";
import {FormPesquisaProcesso} from "./FormPesquisaProcesso";

/**
 * Controlador padrão da tela de listagem de leitura movimento
 *
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provide(ControleProcessoController)
export class ControleProcessoController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(LoadingService)
    private loading : LoadingService;

    @inject(NotificadorService)
    private notificador : NotificadorService;

    @inject(TabelaListagemProcessoController)
    private tabela : TabelaListagemProcessoController;

    @inject(FormPesquisaProcesso)
    private form : FormPesquisaProcesso;

    @inject(ProcessoService)
    private processoService : ProcessoService;

    private scrollPosition;

    @postConstruct()
    public onInit() {
        this.configurarTabela();
        this.form.onSubmit((e) => {
            e.preventDefault();
            if (this.formValido()) {
                this.tabela.recarregarTabela();
            }
        });
        let pesquisaAutomatica = $('#pesquisaAutomatica').val();
        if (pesquisaAutomatica) {
            $("#formPesquisaProcesso").submit();
        }
    }

    private formValido() {
        let valido = true;
        $(".campoData").each(function(){
            let val = $(this).val();
            if (val !== '' ){
                let parts = val.split("/");
                let ano = Number(parts[2]);
                let mes = Number(parts[1]) - 1;
                let dia = Number(parts[0]);
                let d = new Date(ano, mes, dia);
                if (d.getFullYear() !== ano || d.getMonth() !== mes || d.getDate() !== dia) {
                    alert("Informe uma data válida.");
                    $(this).focus();
                    valido = false;
                    return;
                }
            }
        });
        return valido;
    }

    /**
     * Configura os callbacks de tabela
     */
    private configurarTabela() {
        let preLoading = () => {
            this.scrollPosition = document.scrollingElement.scrollTop;
            this.loading.exibir(true);
        };

        let posLoading = () => {
            this.loading.exibir(false);
            document.scrollingElement.scrollTop = this.scrollPosition;
        };

        this.tabela.configurarCallbacks(preLoading, posLoading);
    }

}
