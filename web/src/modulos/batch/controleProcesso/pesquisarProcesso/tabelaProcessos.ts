import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {Datatable, DataTableLanguageConfigs} from "../../../../negocio/datatables";
import {FormPesquisaProcesso, PesquisaProcesso} from "./FormPesquisaProcesso";


/**
 *
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provide(TabelaListagemProcessoController)
export class TabelaListagemProcessoController {

    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(FormPesquisaProcesso)
    private form : FormPesquisaProcesso;

    private callbackPre;
    private callbackComplete;

    @postConstruct()
    public onInit() {
        $("#chkTodos").click(() => {
            if ($("#chkTodos").prop("checked")) {
                $("[name='checkRegistro']:not([disabled])").prop("checked", true);
            } else {
                $("[name='checkRegistro']:not([disabled])").prop("checked", false);
            }
        });
    }

    public configurarCallbacks(callbackPre, callbackComplete) {
        this.callbackPre = callbackPre;
        this.callbackComplete = callbackComplete;
    }

    public recarregarTabela() {

        new Datatable($("#tableProcessos")).inicializar({
            ordering: true,
            searching: false,
            responsive: true,
            destroy: true,
            processing: false,
            serverSide: true,
            language: $.extend(DataTableLanguageConfigs, {
                emptyTable: "Sua pesquisa não retornou resultado, tente modificar os filtros atuais"
            }),
            lengthMenu: [10, 25, 50, 100, 200, 500, 1000, 2000, 2500],
            ajax: {
                url: `${this.props.contextoAplicacao()}/pesquisarProcessoDatatable`,
                type: 'POST',
                contentType: "application/json",

                data: (d) => {
                    let orderColumn = '';
                    let orderDir = '';
                    if (d.order.length > 0) {
                        orderColumn = d.columns[d.order[0].column].name;
                        orderDir = d.order[0].dir;
                    }
                    let processo : PesquisaProcesso = this.form.extrairPesquisaFormulario();
                    return JSON.stringify({
                        colunaOrdenacao : orderColumn,
                        direcaoOrdenacao : orderDir,
                        qtdRegistros : d.length,
                        offset: d.start,
                        ...processo,
                    });
                },
                dataFilter: (resposta) => {
                    let json = $.parseJSON(resposta);
                    return JSON.stringify({
                        recordsTotal: json.quantidadeTotal,
                        recordsFiltered: json.quantidadeTotal,
                        data : json.dados
                    });
                },
                beforeSend : this.beforeSend.bind(this),
                complete : this.complete.bind(this)
            },
            columns: [
                {data : null, render : (data) => this.renderColunaCheckBox(data)},
                {data : "chavePrimaria", name : "chavePrimaria", render : data => data},
                {data : "descricao", name : "descricao", render : data => data},
                {data : "periodicidadeExtenso", render : data => data},
                {data : null, name : "dataInicioAgendamento", render : (data) => data.inicioAgendamento ? new Date(data.inicioAgendamento).toLocaleString() : "" },
                {data : null, name : "dataFinalAgendamento", render : (data) => data.fimAgendamento ? new Date(data.fimAgendamento).toLocaleString() : "" },
                {data : null, render : (data) => this.renderDataExecucao(data) },
                {data : null, name : "situacao", render : (data) => this.renderColunaSituacao(data) },
                {data : null, render : (data) => this.renderColunaLog(data) },
                {data : null, render : (data) => this.renderColunaRelatorio(data)},
                {data : null, render : (data) => this.renderColunaUtil(data)}
            ],
            columnDefs: [
                {className: "align-middle text-center", targets : "_all"},
                {"targets": [0], "searchable": false, "orderable": false, "visible": true},
                {"targets": [3], "searchable": false, "orderable": false, "visible": true},
                {"targets": [6], "searchable": false, "orderable": false, "visible": true},
                {"targets": [8], "searchable": false, "orderable": false, "visible": true},
                {"targets": [9], "searchable": false, "orderable": false, "visible": true},
                {"targets": [10], "searchable": false, "orderable": false, "visible": true}
            ]
        }, false);
    }

    private renderColunaCheckBox(data) {
        return `
        <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
            <input id="chk${data.chavePrimaria}" type="checkbox" name="chavesPrimarias" class="custom-control-input" value="${data.chavePrimaria}">
            <label class="custom-control-label p-0" for="chk${data.chavePrimaria}"></label>
        </div>`;
    }

    private renderColunaLog(data) {
        if (data.situacao == 3) {
            if (data.logErro && data.logErro != '') {
                return `        
                    <a href="javascript:void(0)" onclick="exibirLog(${data.chavePrimaria}, true)">
                        <span class="fa-stack fa-sm" style="font-size: 10px;">                                   
                            <i class="fas fa-file text-light fa-stack-2x" style="-webkit-text-fill-color: white; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: black;"></i>      
                            <i class="fas fa-exclamation fa-stack-1x" title="Abrir log de erro (nova aba)" style='color: #ff8000'></i>
                        </span>           
                    </a>`;
            }
        } else {
            if (data.logExecucao && data.logExecucao != '') {
                return `
                    <a href="javascript:void(0)" onclick="exibirLog(${data.chavePrimaria}, false)">
                        <span class="fa-stack fa-sm" style="font-size: 10px;">                                 
                            <i class="fas fa-file text-light fa-stack-2x" style="-webkit-text-fill-color: white; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: black;"></i>
                            <i class="fas fa-check text-success fa-stack-1x"  title="Abrir log do processamento (nova aba)"></i>
                        </span>        
                    </a>`;
            }
        }
        return '';
    }

    private renderColunaRelatorio(data) {
        if (data.quantidadeDocumento == 1) {
            if (data.indicadorDocumentoZip) {
                return `<a href="imprimirRelatorio?chavePrimaria=${data.chavePrimaria}&chavePrimariaDocumento=${data.chaveDocumento}">
                    <i title="Download do zip gerado" class="fas fa-file-archive" style="font-size: 19px; color: #e65c00"></i>
                </a>`;
            } else {
                return `<a href="imprimirRelatorio?chavePrimaria=${data.chavePrimaria}&chavePrimariaDocumento=${data.chaveDocumento}">
                    <i title="Download do relatório gerado" class="fas fa-file-pdf" style="font-size: 19px; color: #e60000"></i>
                </a>`;
            }
        } else if (data.quantidadeDocumento > 1) {
            if (data.indicadorDocumentoZip) {
                return `<a href="javascript:exibirRelatorioPopup(${data.chavePrimaria});">
                    <i title="Download dos zips gerados" class="fas fa-file-archive" style="font-size: 19px; color: #e65c00"></i>
                </a>`;
            } else {
                return `<a href="javascript:exibirRelatorioPopup(${data.chavePrimaria});">
                    <i title="Download dos relatórios gerados" class="fas fa-file-pdf" style="font-size: 19px; color: #e60000"></i>
                </a>`;
            }
        }
        return '';
    }

    private renderColunaUtil(data) {
        if (data.situacao == 3 || data.situacao == 4) {
            return `<a href="javascript:reiniciarProcesso(${data.chavePrimaria}, ${data.versao});">
                        <i title="Reiniciar" class="fas fa-sync-alt text-secondary fa-lg"></i>
                    </a>`;
        } else if(data.situacao == 2) {
            return `<a href="javascript:excluirProcesso(${data.chavePrimaria});">
                        <i title="Apagar" class="fas fa-trash text-secondary fa-lg">
                    </a>`;
        }
        return '';
    }

    private renderDataExecucao(data) {
        return `<a href="javascript:exibirDialogInfoExecucaoProcesso(${data.chavePrimaria});">
                    <i class="fas fa-info-circle fa-lg"></i>
                </a>`;
    }


    private renderColunaSituacao(data) {
        switch (data.situacao) {
            case 0:
                return "<i title=\"Em espera\" class='fas fa-clock text-primary fa-lg'></i>";
            case 1:
                return "<i title=\"Processando...\" class='fas fa-spinner fa-spin fa-lg'></i>";
            case 2:
                return "<i title=\"Concluído\" class='fa fa-check-circle text-success fa-lg'></i>";
            case 3:
                return "<i title=\"Erro\" style='color: #ff8000' class='fas fa-exclamation-circle fa-lg'></i>";
            case 4:
                return "<i title=\"Abortado\" class='fa fa-times-circle text-danger fa-lg'></i>";
            case 5:
                return "<i title=\"Cancelado\" class='fa fa-times-circle text-danger fa-lg'></i>";
        }
        return '';
    }

    private beforeSend() {
        this.callbackPre ? this.callbackPre() : null;
        this.ocultarTabela(true);

    }

    private complete() {
        this.ocultarTabela(false);
        $(".containerTabela").css("height", "auto");

        $("#chkTodos").prop("checked", false);
        this.callbackComplete ? this.callbackComplete() : null;
        $('.dataTables_empty').prop('colspan', $('#tableProcessos').find('th').length);
        if ($('#tableProcessos .dataTables_empty').length == 0) {
            $('#btnCancelar').show();
        } else {
            $('#btnCancelar').hide();
        }
    }

    public ocultarTabela(ocultar) {
        if (ocultar) {
            $(".containerTabela").hide();
        } else {
            $(".containerTabela").show();
        }
    }
}
