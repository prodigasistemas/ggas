import {inject, postConstruct} from "inversify";
import {provideSingleton} from "../../../../singleton";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {NotificadorService} from "../../../../servicos/notificador.service";
import {LoadingService} from "../../../../servicos/loading.service";

/**
 * Formulário de pesquisa de processo
 * @author gilberto.silva@logiquesistemas.com.br
 */
@provideSingleton(FormPesquisaProcesso)
export class FormPesquisaProcesso {
    private $modulo;
    private $operacao;
    private $periodicidade;
    private $situacao;
    private $dataInicioAgendamento;
    private $dataFimAgendamento;
    private $dataInicioExecucao;
    private $dataFimExecucao;


    @inject(PropriedadesSistemaService)
    private props : PropriedadesSistemaService;

    @inject(NotificadorService)
    private notificador : NotificadorService;

    @inject(LoadingService)
    private loading : LoadingService;

    @postConstruct()
    public async onInit() {
        this.$modulo = $("#idModulo");
        this.$operacao = $("#idOperacao");
        this.$periodicidade = $("#idPeriodicidade");
        this.$situacao = $("#idSituacao");

        this.$dataInicioAgendamento = $("#dataAgendada");
        this.$dataFimAgendamento = $("#dataAgendadaFinal");
        this.$dataInicioExecucao = $("#dataInicio");
        this.$dataFimExecucao = $("#dataFim");

        this.configurarBotaoPesquisar();

    }

    public onSubmit(callback) {
        $("#formPesquisaProcesso").unbind();
        $("#formPesquisaProcesso").submit((e) => {
            e.preventDefault();
            callback(e);
        });
    }

    public extrairPesquisaFormulario() : PesquisaProcesso {

        let modulo = this.$modulo.val();
        let operacao = this.$operacao.val();
        let periodicidade = this.$periodicidade.val();
        let situacao = this.$situacao.val();

        let dataInicioAgendamento = this.$dataInicioAgendamento.val();
        let dataFinalAgendamento = this.$dataFimAgendamento.val();
        let dataInicio = this.$dataInicioExecucao.val();
        let dataFim = this.$dataFimExecucao.val();

        let indicadorAgendamento = $("[name='indicadorAgendamento']:checked").val();
        let idGrupoFaturamento = $('#idGrupoFaturamento').val();
        let rotasSelecionadas = $('#rotasSelecionadas').val();


        return { modulo, operacao, periodicidade, situacao, dataInicioAgendamento, dataFinalAgendamento, dataInicio, dataFim,
            indicadorAgendamento, idGrupoFaturamento, rotasSelecionadas};
    }


    private configurarBotaoPesquisar() {
        $("#botaoPesquisar").click(e => {
            e.preventDefault();
            $("#formPesquisaProcesso").submit();
        });
    }
}

export interface PesquisaProcesso {
    modulo : number,
    operacao : number,
    periodicidade : number,
    situacao : number,
    dataInicioAgendamento : string,
    dataFinalAgendamento : string,
    dataInicio : string,
    dataFim : string,
    indicadorAgendamento : boolean,
    idGrupoFaturamento : number,
    rotasSelecionadas : number[]
}
