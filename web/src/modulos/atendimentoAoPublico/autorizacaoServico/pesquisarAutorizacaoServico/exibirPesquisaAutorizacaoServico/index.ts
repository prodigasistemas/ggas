import {Datatable} from "../../../../../negocio/datatables";
import {Utilidades} from "../../../../../negocio/util/utilidades";

declare const $: any, formatarCampoInteiro;
$(document).ready(function () {

    try {
        const d = new Datatable('#table-servicos');
        d.columnDefs = [
            {"targets": [0], "searchable": false, "orderable": false, "visible": true},
            {"targets": [2], "type": 'date-time' },
            {"targets": [3], "type": 'date' },
        ];
        d.inicializar();
    } catch (error) {
        Utilidades.ocultarLoading();
    }

    $("#chavePrimaria, #numeroProtocoloChamado, #numeroContrato").on("paste", (e) => {
        return formatarCampoInteiro(e);
    })

});
