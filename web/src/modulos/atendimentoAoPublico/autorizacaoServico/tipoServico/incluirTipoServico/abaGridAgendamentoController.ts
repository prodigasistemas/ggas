import {provide} from "inversify-binding-decorators";
import {postConstruct} from "inversify";
import {Datatable} from "../../../../../negocio/datatables";

declare const carregarFragmento: Function; //Função antiga, global, definida no importsHeader.jsp
declare let AjaxService : any;

@provide(AbaGridAgendamentoController)
export class AbaGridAgendamentoController {

    @postConstruct()
    public onInit() {
        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["gridAgendamentos"].includes(alvo)) {
                this.configurarElementosPagina();
            }
        })
    }

    private configurarElementosPagina() {
        $("#botaoIncluirTurno").click(() => {
            this.incluirTurno();
        });

        $("#botaoAlterarTurno").click(() => {
            this.alterarTurno();
        });

        $("#botaoLimparTurno").click(() => {
            this.limparTurno();
        });

        $('.acaoAlterarTurnoAgendamento').click((event) => {

            const $elemento = $(event.currentTarget);

            const indice = $elemento.data('indice');
            const idTurno = $elemento.data('idturno');
            const quantidade = $elemento.data('quantidade');
            const equipe = $elemento.data('idequipe');
            this.exibirAlteracaoTurno(indice, equipe, idTurno, quantidade);
            
        });

        $('.acaoRemoverTurnoAgendamento').click((event) => {
            const $elemento = $(event.currentTarget);

            const idTurno = $elemento.data('idturno');
            const idServicoAgendamentoTurno = $elemento.data('idservicoagendamento');
            this.removerTurno(idTurno, idServicoAgendamentoTurno);
        });

        this.carregarDataTable();
    }
    private carregarDataTable() {
        new Datatable('#servicoTipoAgendamentoTurno').inicializar({destroy: true});
    }

    private incluirTurno() {
        var chavePrimaria = document.forms[0].chavePrimaria.value;
        var chaveTurno = document.forms[0].turno.value;
        var quantidadeAgendamentosTurno = document.forms[0].quantidadeAgendamentosTurno.value;
        var chaveEquipe = document.forms[0].equipe.value;
        var indexList = -1;
        var noCache = "noCache=" + new Date().getTime();

		var url = "incluirTurno?equipe="+chaveEquipe+"&turno="+chaveTurno+"&quantidadeAgendamentosTurno="+quantidadeAgendamentosTurno+"&chavePrimaria="+chavePrimaria+"&indexList="+indexList+"&"+noCache;

        if (chaveTurno != "-1" && quantidadeAgendamentosTurno > 0 && chaveEquipe != "-1") {
            carregarFragmento("gridAgendamentos", url, null);
        } else {
            this.exibirCamposComErros(chaveTurno, chaveEquipe, quantidadeAgendamentosTurno);
        }
    }

    private alterarTurno() {
        var chavePrimaria = document.forms[0].chavePrimaria.value;
        var chaveTurno = document.forms[0].turno.value;
        var quantidadeAgendamentosTurno = document.forms[0].quantidadeAgendamentosTurno.value;
        var indexList = document.forms[0].indexList.value;
        var chaveEquipe = document.forms[0].equipe.value;
        var noCache = "noCache=" + new Date().getTime();

    	var url="incluirTurno?equipe="+chaveEquipe+"&turno="+chaveTurno+"&quantidadeAgendamentosTurno="+quantidadeAgendamentosTurno+"&chavePrimaria="+chavePrimaria+"&indexList="+indexList+"&"+noCache;


        if (chaveTurno != "-1" && quantidadeAgendamentosTurno > 0 && chaveEquipe != "-1") {
            $('#turnoFeedback').hide();
            $('#quantidadeFeedback').hide();
            $('#equipeFeedback').hide();
            carregarFragmento("gridAgendamentos", url);
        } else {
            this.exibirCamposComErros(chaveTurno, chaveEquipe, quantidadeAgendamentosTurno);
        }
    }

    private exibirCamposComErros(chaveTurno, chaveEquipe, quantidadeAgendamentosTurno) {
        if (chaveTurno != '-1') {
            $('#turnoFeedback').hide();
        } else {
            $('#turnoFeedback').show();
        }
        if (quantidadeAgendamentosTurno <= 0 || quantidadeAgendamentosTurno == '') {
            $('#quantidadeFeedback').show();
        } else {
            $('#quantidadeFeedback').hide();
        }
        
        if (chaveEquipe != '-1') {
            $('#equipeFeedback').hide();
        } else {
            $('#equipeFeedback').show();
        }
    }

    private limparTurno() {
        document.forms[0].turno.value = -1;
        document.forms[0].quantidadeAgendamentosTurno.value = "";
    }

    private exibirAlteracaoTurno(indice, chaveEquipe, idTurno, quantidade) {

        document.forms[0].indexList.value = indice;
        document.forms[0].turno.value = idTurno;
        document.forms[0].equipe.value = chaveEquipe;
        document.forms[0].quantidadeAgendamentosTurno.value = quantidade;

        $("#botaoAlterarTurno").attr('disabled', false);
        $("#botaoLimparTurno").attr('disabled', false);
        $("#botaoIncluirTurno").attr('disabled', true);
    }

    private removerTurno(chavePrimariaTurno, idServicoAgendamentoTurno) {
    	
        AjaxService.consultarServicoAgendamentoItemLote(idServicoAgendamentoTurno , {
            callback: (retorno: boolean) => {
            	console.log(retorno);
				if(retorno) {
					alert("Não é Possivel Remover o item pois ele está sendo utilizado em outro lugar");
				} else {
			        var noCache = "noCache=" + new Date().getTime();
			        $("#gridAgendamentos").load("removerTurno?chavePrimariaTurno=" + chavePrimariaTurno + "&" + noCache, () => {
			            $("body").trigger("carregarFragmentoFinished", ["gridAgendamentos"]);
			        });
				}
            }
        });

    }


}
