import {inject, postConstruct} from "inversify";

import {provide} from "inversify-binding-decorators";
import {AbaGridAgendamentoController} from "./abaGridAgendamentoController";
import {AbaMateriaisController} from "./abaMateriaisController";
import {GridEquipamentoEspecialController} from "./gridEquipamentoEspecialController";


/**
 * Controlador padrão da lógica da tela de incluir tipo de serviço
 *
 * @author jose.victor@logiquesistemas.com.br, italo.alan@logiquesistemas.com.br
 */

@provide(IncluirTipoServicoController)
export class IncluirTipoServicoController {

    @inject(AbaMateriaisController)
    private gridMaterial: AbaMateriaisController;

    @inject(AbaGridAgendamentoController)
    private gridAgendamento: AbaGridAgendamentoController;

    @inject(GridEquipamentoEspecialController)
    private gridEquipamentoEspecial: GridEquipamentoEspecialController;

    @postConstruct()
    public onInit() {
        console.debug('IncluirTipoServicoController onInit');
    }
}
