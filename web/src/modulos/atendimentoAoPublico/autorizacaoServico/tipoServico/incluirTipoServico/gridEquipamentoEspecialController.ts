import {provide} from "inversify-binding-decorators";
import {postConstruct} from "inversify";
import {Datatable} from "../../../../../negocio/datatables";

declare const carregarFragmento: Function; //Função antiga, global, definida no importsHeader.jsp

@provide(GridEquipamentoEspecialController)
export class GridEquipamentoEspecialController {

    idDatatableEquipamento = '#datatable-equipamento-especial';
    displayLength = 10;

    @postConstruct()
    public onInit() {

        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["gridEquipamentos"].includes(alvo)) {
                this.configurarElementosPagina();
            }
        });

        $("input[name='indicadorEquipamento").change((event) => {
            if (event.target.value === 'true') {
                this.configurarElementosPagina();
            }
        });
    }

    private configurarElementosPagina() {
        $("#botaoIncluirEquipamento").unbind("click");
        $("#botaoIncluirEquipamento").click(() => {
            this.incluirEquipamento();
        });

        $("#botaoLimparEquipamento").click(() => {
            this.limparEquipamento();
        });

        $('.acaoRemoverEquipamento').click((event) => {
            const $elemento = $(event.currentTarget);

            const idEquipamento = $elemento.data('id');

            $('#id-equipamento-especial-excluir').val(idEquipamento);

            $('#modal-confirmar-exclusao-equipamento-especial').modal('show');
        });

        $('#confirmar-exclusao-material-necessario').click((e) => {
            this.removerEquipamento($('#id-equipamento-especial-excluir').val());
        });

        this.carregarDataTable();
    }

    private carregarDataTable() {
        new Datatable(this.idDatatableEquipamento).inicializar({destroy: true, displayLength : this.displayLength});
        $(this.idDatatableEquipamento).on("length.dt", (e, set, len) => this.displayLength = len);
    }

    private incluirEquipamento() {
        var chavePrimaria = document.forms[0].chavePrimaria.value;
        var chaveEquipamento = document.forms[0].equipamento.value;
        var noCache = "noCache=" + new Date().getTime();

        var url = "incluirEquipamento?equipamento=" + chaveEquipamento + "&chavePrimaria=" + chavePrimaria + "&" + noCache;

        if (chaveEquipamento != "-1") {
            //$('.notification.failure.hideit').hide();
            carregarFragmento("gridEquipamentos", url);
        } else {
            this.exibirCamposComErros(chaveEquipamento);
        }
    }

    private exibirCamposComErros(chaveEquipamento) {
        if (chaveEquipamento != '-1') {
            $('#equipamentoFeedback').hide();
        } else {
            $('#equipamentoFeedback').show();
        }
    }

    private limparEquipamento() {
        document.forms[0].indexList.value = '';
        document.forms[0].turno.value = -1;
        document.forms[0].quantidadeAgendamentosTurno.value = "";
    }

    private removerEquipamento(chaveEquipamento) {

        $('#modal-confirmar-exclusao-equipamento-especial').modal('hide');

        /**
         * Necessário ocultar o modal antes de realizar a requisição, se não o modal fica "congelado" na tela
         */
        setTimeout(() => {
            var noCache = "noCache=" + new Date().getTime();
            $("#gridEquipamentos").load("removerEquipamento?chavePrimariaEquipamento=" + chaveEquipamento + "&" + noCache, () => {
                $("body").trigger("carregarFragmentoFinished", ["gridEquipamentos"]);
            });
        }, 500);
    }


}
