/*
 *
 *  Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 *  Este programa é um software livre; você pode redistribuí-lo e/ou
 *  modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 *  publicada pela Free Software Foundation; versão 2 da Licença.
 *
 *  O GGAS é distribuído na expectativa de ser útil,
 *  mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 *  COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 *  Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 *  Você deve ter recebido uma cópia da Licença Pública Geral GNU
 *  junto com este programa; se não, escreva para Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 *  GGAS is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  GGAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 *  
 */

import {provide} from "inversify-binding-decorators";
import {postConstruct} from "inversify";
import {Datatable} from "../../../../../negocio/datatables";

declare const submeter, obterValorUnicoCheckboxSelecionado, verificarSelecao, getCheckedValue, formatarCampoInteiro, unescape;

/**
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(PesquisarTipoServicoController)
export class PesquisarTipoServicoController {

    @postConstruct()
    public init() {

        new Datatable("#table-servicos").inicializar();
        const indicadorAtualizacaoCadastral = $("[name='servicoTipo.indicadorAtualizacaoCadastral']").val();
        const descricaoTelaAtualizacao = $("[name='servicoTipo.telaAtualizacao.descricao']").val();
        const descricaoTelaAtualizacaoPontoConsumo = $("[name='descricaoTelaAtualizacaoPontoConsumo']").val();

        if (!indicadorAtualizacaoCadastral || indicadorAtualizacaoCadastral === "N") {
            document.forms[0].telaAtualizacao.value = -1;
            $("#telaAtualizacao").attr("disabled", true);
            $("#indicadorTipoAssociacaoPontoConsumo").attr("disabled", true);
            $("#indicadorTipoAssociacaoPontoConsumo").removeAttr("style");
            $("#indicadorOperacaoMedidor").attr("disabled", true);
            $("#indicadorOperacaoMedidor").removeAttr("style");
        } else {
            $("#telaAtualizacao").removeAttr("disabled");
        }

        if (descricaoTelaAtualizacao != descricaoTelaAtualizacaoPontoConsumo) {
            this.esconderDadosOperacaoMedidor();
        } else {
            this.exibirDadosOperacaoMedidor();
            this.obterOperacoes();
        }

        this.inicializarCampos();

    }

    private inicializarCampos() {
        $("#telaAtualizacao").change(() => this.manipularExibicaoDivOperacaoMedidor());
        $("#indicadorAtualizacaoCadastral").change((e) => {
            const valor = $(e.currentTarget).val();
            if (!valor || valor === "N") {
                this.esconderDadosTelaAtualizacao();
            } else {
                this.exibirDadosTelaAtualizacao();
            }
        });
        $("#botaoPesquisar").click(() => this.pesquisar());
        $("[name='linkTabelaServicoTipo']").click((e) => {
            const chavePrimaria = $(e.currentTarget).data("chaveprimaria");
            this.detalharServicoTipo(chavePrimaria);
        })

        $("#buttonAlterar").click(() => this.alterarServicoTipo());
        $("#buttonIncluir").click(() => this.incluir());
        $("#buttonRemover").click(() => this.removerServicoTipo());
        $("#limparFormulario").click(() => this.limparFormulario());
        $("#indicadorTipoAssociacaoPontoConsumo").change(() => this.obterOperacoes());
        $("#quantidadeTempoMedio, #quantidadePrazoExecucao").on("paste", (e) => {
            return formatarCampoInteiro(e);
        })
    }

    private pesquisar() {
        if ($("#servicoTipoForm")[0].checkValidity()) {
            console.log('submetendo');
            submeter('servicoTipoForm', 'pesquisarServicoTipo');
        }
    }

    private detalharServicoTipo(chave){
        document.forms["servicoTipoForm"].chavePrimaria.value = chave;
        submeter('servicoTipoForm','exibirDetalhamentoServicoTipo');
    }

    private alterarServicoTipo() {
        let selecao = this.verificarSelecaoApenasUm();
        if (selecao == true) {
            document.forms["servicoTipoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
            submeter('servicoTipoForm','exibirAlteracaoServicoTipo');
        }

    }

    private removerServicoTipo(){
        let selecao = verificarSelecao();

        if (selecao == true) {
            let retorno = confirm("Deseja remover os itens selecionados?\nEsta Operação não poderá ser desfeita.");
            if(retorno == true) {
                submeter('servicoTipoForm', 'removerServicoTipo');
            }
        }
    }

    private incluir() {
        submeter('servicoTipoForm','exibirInclusaoServicoTipo');
    }

    private verificarSelecaoApenasUm() {
        let flag = 0;
        let form = document.forms[0];

        if (form != undefined && form.chavesPrimarias != undefined) {
            let total = form.chavesPrimarias.length;
            if (total != undefined) {
                for (let i = 0; i< total; i++) {
                    if(form.chavesPrimarias[i].checked == true){
                        flag++;
                    }
                }
            } else {
                if(form.chavesPrimarias.checked == true){
                    flag++;
                }
            }

            if (flag != 1) {
                alert (unescape("Selecione apenas um registro para realizar a operação!"));
                return false;
            }

        } else {
            return false;
        }

        return true;
    }
    private limparFormulario() {
        document.forms[0].documentoImpressaoLayout.value = -1;
        document.forms[0].servicoTipoPrioridade.value = -1;
        document.forms[0].descricao.value = "";
        document.forms[0].quantidadeTempoMedio.value = "";
        document.forms[0].quantidadePrazoExecucao.value = "";

        $("#indicadorServicoRegulamento").val("");
        $("#indicadorAgendamento").val("");
        $("#indicadorVeiculo").val("");
        $("#indicadorPesquisaSatisfacao").val("");
        $("#indicadorGeraLote").val("");
        $("#indicadorEncerramentoAuto").val("");
        $("#indicadorEquipamento").val("");
        $("#indicadorAtualizacaoCadastral").val("");
        $("#indicadorUso").val("true");
        $("#indicadorClienteObrigatorio").val("");
        $("#indicadorImovelObrigatorio").val("");
        $("#indicadorContratoObrigatorio").val("");
        $("#indicadorPontoConsumoObrigatorio").val("");

        $("#numeroMaximoExecucacao").val("");
        $("#prazoGarantia").val("");
        this.esconderDadosTelaAtualizacao();
    }

    private exibirDadosTelaAtualizacao(){
        $("#telaAtualizacao").removeAttr("disabled");
    }

    private esconderDadosTelaAtualizacao(){
        document.forms[0].telaAtualizacao.value = -1;
        $('#telaAtualizacao').attr("disabled", true);
        $('#telaAtualizacao').removeAttr("style");
        this.esconderDadosOperacaoMedidor();
    }

    private exibirDadosOperacaoMedidor(){
        $("#indicadorTipoAssociacaoPontoConsumo").removeAttr("disabled");
    }

    private esconderDadosOperacaoMedidor(){
        $("#indicadorTipoAssociacaoPontoConsumo").val("");
        $("#indicadorOperacaoMedidor").val("");
        $("#indicadorTipoAssociacaoPontoConsumo").attr("disabled", true);
        $("#indicadorTipoAssociacaoPontoConsumo").removeAttr("style");
        $("#indicadorOperacaoMedidor").removeAttr("style");
    }

    private manipularExibicaoDivOperacaoMedidor(){
        let descricaoTelaAtualizacao = $( "#telaAtualizacao option:selected" ).text();
        let descricao = "Tela Associação Medidor Ponto de Consumo";
        if(descricaoTelaAtualizacao.toLowerCase().trim() === descricao.toLowerCase().trim()){
            this.exibirDadosOperacaoMedidor();
        }else{
            this.esconderDadosOperacaoMedidor();
        }
    }

    private obterOperacoes(){
        let noCache = "noCache=" + new Date().getTime();
        let codigoAssociacao = getCheckedValue((document as any).servicoTipoForm.indicadorTipoAssociacaoPontoConsumo);
        let chaveOperacao = $("[name='servicoTipo.indicadorOperacaoMedidor']").val();
        $("#divOperacaoMedidor").load("obterOperacoes?tipoAssociacao="+codigoAssociacao+"&chaveOperacao="+chaveOperacao+"&"+noCache, () => {
            if ($("#indicadorOperacaoMedidor option").length > 1) {
                $("#indicadorOperacaoMedidor").removeAttr("disabled");
            }
        });
    }

}


