import {inject, postConstruct} from "inversify";
import {Datatable} from "../../../../negocio/datatables";
import {provide} from "inversify-binding-decorators";
import {AbaMateriaisController} from "./abaMateriaisController";
import {GridEquipamentoEspecialController} from "./gridEquipamentoEspecialController";
import {AbaImovelAutorizacaoServicoController} from "./abaImovelController";
import {ModalResponderFormulario} from "../../../../componentes/responderFormularioAS/modal";
import {GridServicoAutorizacaoHistorico} from "./gridServicoAutorizacaoHistorico";
import {AbaMedidorAutorizacaoServicoController} from "./abaMedidorController";
import {AbaMobileAutorizacaoServicoController} from "./abaMobileController";

/**
 * Controlador padrão da lógica da tela de incluir chamado
 *
 * @author italo.alan@logiquesistemas.com.br
 */

@provide(IncluirServicoAutorizacaoController)
export class IncluirServicoAutorizacaoController {

    @inject(AbaMateriaisController)
    private gridMaterial: AbaMateriaisController;

    @inject(GridEquipamentoEspecialController)
    private gridEquipamentoEspecial: GridEquipamentoEspecialController;

    @inject(AbaImovelAutorizacaoServicoController)
    private gridImovelAutorizacaoServicoController;

    @inject(ModalResponderFormulario)
    private modalResponderFormulario : ModalResponderFormulario;

    @inject(GridServicoAutorizacaoHistorico)
    private gridServicoAutorizacaoHistorico : GridServicoAutorizacaoHistorico;
    
    @inject(AbaMedidorAutorizacaoServicoController)
    private gridMedidorAutorizacaoServicoController;
    
    @inject(AbaMobileAutorizacaoServicoController)
    private gridMobileAutorizacaoServicoController;

    @postConstruct()
    public onInit() {
        this.configurarDatatable();
        this.configurarListenCarregarFragmento();
        this.modalResponderFormulario.setCallback(() => {
            $("#responderFormulario").removeClass("btn-primary").addClass("btn-success");
            $("#responderFormulario i").removeClass("fa-file-signature").addClass("fa-check");
            $("#responderFormulario span").text("Formulário Respondido!");
            $("#responderFormulario").prop("disabled", true);
        })
        $("#responderFormulario").click((e) => {
            e.preventDefault();
            this.modalResponderFormulario.exibir();
        })
    }

    private configurarDatatable() {
        new Datatable('#table-grid-anexo').inicializar({destroy: true});
        new Datatable('#table-grid-pontos-consumo').inicializar({destroy: true});
        new Datatable('#table-servico-autorizacao-historico').inicializar({destroy: true});
        new Datatable('#table-servico-autorizacao-historico-anexos').inicializar({destroy: true});
        new Datatable('#table-grid-chamado-historico').inicializar({destroy : true});
        new Datatable('#table-grid-chamado-historico-anexo').inicializar({destroy : true});
        new Datatable('#table-grid-medidor').inicializar({destroy : true});
        new Datatable('#table-grid-imoveis').inicializar({destroy: true});
        new Datatable('#table-grid-medidor-mobile').inicializar({destroy : true});
    }

    private configurarListenCarregarFragmento() {
        const ids = ["gridChamadosCliente", "gridChamadoPontosConsumo", "gridImoveis"];

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (ids.includes(alvo)) {
                this.configurarDatatable();
            }
        })
    }

}
