import {provide} from "inversify-binding-decorators";
import {postConstruct} from "inversify";
import {Datatable} from "../../../../negocio/datatables";

declare const carregarFragmento: Function; //Função antiga, global, definida no importsHeader.jsp

@provide(AbaMateriaisController)
export class AbaMateriaisController {

    @postConstruct()
    public onInit() {
        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["gridMateriaisServicoAutorizacao"].includes(alvo)) {
                this.configurarElementosPagina();
            }
        })
    }

    private configurarElementosPagina() {
        $("#botaoIncluirMaterial").click(() => {
            this.incluirMaterialServicoAutorizacao();
        });

        $("#botaoAlterarMaterial").click(() => {
            this.alterarMaterialServicoAutorizacao();
        });

        $("#botaoLimparMaterial").click(() => {
            this.limparMaterialServicoAutorizacao();
        });

        $('.acaoAlterarMaterial').click((event) => {

            const $elemento = $(event.currentTarget);

            const indice = $elemento.data('indice');
            const idMaterial = $elemento.data('idmaterial');
            const quantidade = $elemento.data('quantidade');
            this.exibirAlteracaoMaterialServicoAutorizacao(indice, idMaterial, quantidade);
        });

        $('.acaoRemoverMaterial').click((event) => {
            const $elemento = $(event.currentTarget);

            const idMaterial = $elemento.data('id');

            $('#id-material-servico-excluir').val(idMaterial);

            $('#modal-confirmar-exclusao-material-servico-tipo').modal('show');
        });

        $('#confirmar-exclusao-material-servico-tipo').click((e) => {
            this.removerMaterialServicoAutorizacao($('#id-material-servico-excluir').val());
        });

        this.carregarDataTable();
    }

    private carregarDataTable() {
        new Datatable('#servicoAutorizacaoMaterial').inicializar({destroy: true});
    }

    private incluirMaterialServicoAutorizacao() {
        var chavePrimaria = document.forms[0].chavePrimaria.value;
        var chaveMaterial = document.forms[0].material.value;
        var quantidadeMaterial = document.forms[0].quantidadeMaterial.value;
        var indexList = -1;
        var noCache = "noCache=" + new Date().getTime();


        var url = "incluirMaterialServicoAutorizacao?material=" + chaveMaterial + "&quantidadeMaterial=" + quantidadeMaterial + "&chavePrimaria=" + chavePrimaria + "&indexList=" + indexList + "&" + noCache;


        if (chaveMaterial != "-1" && quantidadeMaterial > 0) {
            carregarFragmento('gridMateriaisServicoAutorizacao', url);
        } else {
            this.exibirCamposComErros(chaveMaterial, quantidadeMaterial);
        }
    }

    private alterarMaterialServicoAutorizacao() {
        var chavePrimaria = document.forms[0].chavePrimaria.value;
        var chaveMaterial = document.forms[0].material.value;
        var quantidadeMaterial = document.forms[0].quantidadeMaterial.value;
        var indexList = document.forms[0].indexList.value;
        var noCache = "noCache=" + new Date().getTime();

        var url = "incluirMaterialServicoAutorizacao?material=" + chaveMaterial + "&quantidadeMaterial=" + quantidadeMaterial + "&chavePrimaria=" + chavePrimaria + "&indexList=" + indexList + "&" + noCache;

        if (chaveMaterial != "-1" && chaveMaterial != "" && quantidadeMaterial > 0) {
            carregarFragmento('gridMateriaisServicoAutorizacao', url);
            $("#botaoAlterarMaterial").attr('disabled', true);
        } else {
            this.exibirCamposComErros(chaveMaterial, quantidadeMaterial);
        }
    }

    private exibirCamposComErros(chaveMaterial, quantidadeMaterial) {
        if (chaveMaterial == '-1') {
            $('#descricaoMaterialFeedback').show();
        } else {
            $('#descricaoMaterialFeedback').hide();
        }
        if (quantidadeMaterial <= 0 || quantidadeMaterial == '') {
            $('#quantidadeMaterialFeedback').show();
        } else {
            $('#quantidadeMaterialFeedback').hide();
        }
    }

    private limparMaterialServicoAutorizacao() {
        document.forms[0].material.value = -1;
        document.forms[0].quantidadeMaterial.value = "";
    }

    private exibirAlteracaoMaterialServicoAutorizacao(indice, idMaterial, quantidade) {

        document.forms[0].indexList.value = indice;
        document.forms[0].material.value = idMaterial;
        document.forms[0].quantidadeMaterial.value = quantidade;

        $("#botaoAlterarMaterial").attr('disabled', false);
        $("#botaoLimparMaterial").attr('disabled', false);
        $("#botaoIncluirMaterial").attr('disabled', true);
    }

    private removerMaterialServicoAutorizacao(chavePrimariaMaterial) {

        $('#modal-confirmar-exclusao-material-servico-tipo').modal('hide');
        /**
         * Necessário ocultar o modal antes de realizar a requisição, se não o modal fica "congelado" na tela
         */
        setTimeout(() => {
            var noCache = "noCache=" + new Date().getTime();
            $("#gridMateriaisServicoAutorizacao").load("removerMaterialServicoAutorizacao?chavePrimariaMaterial=" + chavePrimariaMaterial + "&" + noCache, () => {
                $("body").trigger("carregarFragmentoFinished", ["gridMateriaisServicoAutorizacao"]);
            });
        }, 500);
    }


}
