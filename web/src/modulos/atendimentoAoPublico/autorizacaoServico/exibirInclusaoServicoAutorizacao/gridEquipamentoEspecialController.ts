import {provide} from "inversify-binding-decorators";
import {postConstruct} from "inversify";
import {Datatable} from "../../../../negocio/datatables";

declare const carregarFragmento: Function; //Função antiga, global, definida no importsHeader.jsp

@provide(GridEquipamentoEspecialController)
export class GridEquipamentoEspecialController {

    idDatatableEquipamento = '#datatable-equipamento-especial';

    @postConstruct()
    public onInit() {

        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["gridEquipamentosServicoAutorizacao"].includes(alvo)) {
                this.configurarElementosPagina();
            }
        });
    }

    private configurarElementosPagina() {
        $("#botaoIncluirEquipamento").unbind("click");
        $("#botaoIncluirEquipamento").click(() => {
            this.incluirEquipamentoAS();
        });

        $('.acaoRemoverEquipamento').click((event) => {
            const $elemento = $(event.currentTarget);

            const idEquipamento = $elemento.data('id');

            $('#id-equipamento-excluir').val(idEquipamento);

            $('#modal-confirmar-exclusao-equipamento-tipo-servico').modal('show');
        });

        $('#confirmar-exclusao-equipamento-servico-tipo').click((e) => {
            console.log('ok');
            this.removerEquipamentoAS($('#id-equipamento-excluir').val());
        });

        this.carregarDataTable();
    }

    private carregarDataTable() {
        new Datatable(this.idDatatableEquipamento).inicializar({destroy: true});
    }

    private incluirEquipamentoAS() {
        var chaveEquipamento = document.forms[0].equipamento.value;
        var noCache = "noCache=" + new Date().getTime();

        var url = "adicionarEquipamento?equipamento=" + chaveEquipamento +"&" + noCache;

        if (chaveEquipamento != "-1") {
            carregarFragmento("gridEquipamentosServicoAutorizacao", url);
        } else {
            this.exibirCamposComErros(chaveEquipamento);
        }
    }

    private exibirCamposComErros(chaveEquipamento) {
        if (chaveEquipamento != '-1') {
            $('#equipamentoFeedback').hide();
        } else {
            $('#equipamentoFeedback').show();
        }
    }

    private removerEquipamentoAS(chaveEquipamento) {

        $('#modal-confirmar-exclusao-equipamento-tipo-servico').modal('hide');

        /**
         * Necessário ocultar o modal antes de realizar a requisição, se não o modal fica "congelado" na tela
         */
        setTimeout(() => {
            var noCache = "noCache=" + new Date().getTime();
            $("#gridEquipamentosServicoAutorizacao").load("removerEquipamentoServicoAutorizacao?chavePrimariaEquipamento=" + chaveEquipamento + "&" + noCache, () => {
                $("body").trigger("carregarFragmentoFinished", ["gridEquipamentosServicoAutorizacao"]);
            });
        }, 500);
    }


}
