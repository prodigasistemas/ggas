/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {LoadingService} from "../../../../servicos/loading.service";
import {Datatable} from "../../../../negocio/datatables";


@provide(AbaMedidorAutorizacaoServicoController)
export class AbaMedidorAutorizacaoServicoController {

    @inject(LoadingService)
    private loading;

    @postConstruct()
    public onInit() {
        $("#buttonPesquisarMedidor").click(() => {
            this.consultarMedidorDisponivel();
        });
    }
    
    
	private consultarMedidorDisponivel() {
		let numeroSerie = encodeURI(document.forms[0].numeroSerie.value);
		let marcaMedidor = document.forms[0].marcaMedidor.value;
		let tipoMedidor = document.forms[0].tipoMedidor.value;
		let modelo = document.forms[0].modelo.value;
		let modoUso = document.forms[0].modoUso.value;

		let url = "consultarMedidorDisponivel?numeroSerie=" + numeroSerie + "&marcaMedidor=" + marcaMedidor + "&tipoMedidor=" + tipoMedidor + "&modelo=" + modelo + "&modoUso=" + modoUso;

		this.loading.exibir(true);
		$("#gridMedidores").load(url, null, () => {
			new Datatable('#table-grid-medidor').inicializar({
				destroy: true,
				columnDefs: [
					{ orderable: false, targets: 0 }
				]
			});

			if ($("input[name='chaveMedidor']").size() == 1) {
				$("input[name='chaveMedidor']").trigger("click");
			} else {
				$("input[name='chaveMedidor']").each(function() {
					$(this).trigger("click");
					return false;
				});
			}

			this.loading.exibir(false);
		});
	}
}
