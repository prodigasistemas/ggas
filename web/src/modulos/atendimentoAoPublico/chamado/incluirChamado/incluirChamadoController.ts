import {inject, postConstruct} from "inversify";
import {Datatable} from "../../../../negocio/datatables";
import {AbaClienteController} from "./abaClienteController";
import {provide} from "inversify-binding-decorators";
import {ChamadoContatoController} from "./chamadoContatoController";
import {AbaImoveisController} from "./abaImovelController";
import {AbaEnderecoController} from "./abaEnderecoController";
import {AbaTitulosController} from "./abaTituloController";

/**
 * Controlador padrão da lógica da tela de incluir chamado
 *
 * @author jose.victor@logiquesistemas.com.br, italo.alan@logiquesistemas.com.br
 */

@provide(IncluirChamadoController)
export class IncluirChamadoController {

    @inject(AbaClienteController)
    private abaCliente : AbaClienteController;

    @inject(AbaImoveisController)
    private abaImoveis : AbaImoveisController;

    @inject(ChamadoContatoController)
    private chamadoContato : ChamadoContatoController;
    
    @inject(AbaTitulosController)
    private abaTitulosController : AbaTitulosController;
    

	@inject(AbaEnderecoController)
	private abaEndereco : AbaEnderecoController;

    @postConstruct()
    public onInit() {
        this.configurarDatatable();
        this.configurarListenCarregarFragmento();
    }

    private configurarDatatable() {

        new Datatable('#table-grid-chamado-historico').inicializar({destroy : true});
        new Datatable('#table-grid-chamado-historico-anexo').inicializar({destroy : true});
        new Datatable('#table-grid-autorizacao-servico').inicializar({destroy : true});
        new Datatable('#table-grid-email').inicializar({destroy : true});
        new Datatable('#table-grid-anexo').inicializar({destroy : true});
        new Datatable('#table-grid-chamado-cliente').inicializar({destroy : true});
        new Datatable('#table-grid-tabela-ponto-consumo').inicializar({destroy : true});
        new Datatable('#table-grid-enderecos').inicializar({destroy : true});

    }

    private configurarListenCarregarFragmento() {
        const ids = ["gridChamadosCliente", "gridChamadoPontosConsumo", "gridImoveisBootstrap","gridEnderecoBootstrap",
					 "gridChamadoEndereco"];

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (ids.includes(alvo)) {
                this.configurarDatatable();
            }
        })
    }


}
