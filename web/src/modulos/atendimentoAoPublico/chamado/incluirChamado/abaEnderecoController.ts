import {provide} from "inversify-binding-decorators";
import {postConstruct, inject} from "inversify";
import {Datatable} from "../../../../negocio/datatables";
import {LoadingService} from "../../../../servicos/loading.service";

@provide(AbaEnderecoController)
export class AbaEnderecoController {

    @inject(LoadingService)
    private loading;

    @postConstruct()
    public onInit() {
        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["divEndereco", "gridEndereco"].includes(alvo)) {
                this.configurarElementosPagina();
            }
            this.inicializarTabelaEnderecos();
			this.inicializarTabelaChamadoEndereco();
        })
    }

    private configurarElementosPagina() {
        $("#botaoPesquisarEndereco").unbind('click');
        $("#botaoPesquisarEndereco").click((e) => {
            this.pesquisarEndereco();
			this.limparEndereco();
			this.limparChamadoEndereco();
			this.bindCarregarEndereco();
        });
		this.bindCarregarEndereco();
		
		this.inicializarTabelaEnderecos();
		this.inicializarTabelaChamadoEndereco();
    }

	private bindCarregarEndereco(){
		$("input[name='checkEndereco']").click((e)=>{
			this.carregarEndereco();
			this.limparEndereco();
			this.limparChamadoEndereco();
		})
	}

	private inicializarTabelaEnderecos() {
        new Datatable('#table-grid-enderecos').inicializar({
            destroy: true,
            columnDefs: [
                {"targets": [0], "searchable": false, "orderable": false, "visible": true}
            ]
        });		
	}
	private inicializarTabelaChamadoEndereco() {
        new Datatable('#table-grid-chamado-endereco').inicializar({
            destroy: true,
            columnDefs: [
                {"targets": [0], "searchable": false, "orderable": false, "visible": true}
            ]
        });		
	}

    private pesquisarEndereco() {
        var noCache = "noCache=" + new Date().getTime();
		var nomeLogradouro = encodeURI(document.forms[0].nomeLogradouro.value);
		var tipoLogradouro = encodeURI(document.forms[0].tipoLogradouro.value);
        this.loading.exibir(true);
        $("#gridEndereco").load(`consultarCepLogradouro?logradouro=${nomeLogradouro}&tipoLogradouro=${tipoLogradouro}`, () => {
            this.inicializarTabelaEnderecos();
            this.loading.exibir(false);
			this.bindCarregarEndereco();
        });

    }

    private carregarEndereco() {
        var noCache = "noCache=" + new Date().getTime();
		var chaveEndereco = $("input[name='checkEndereco']:checked").val();
        this.loading.exibir(true);
        $("#gridChamadoEndereco").load(`carregarEndereco?chavePrimaria=${chaveEndereco}`, () => {
            this.inicializarTabelaChamadoEndereco();
            this.loading.exibir(false);
        });

    	document.forms[0].enderecoChamado.value = chaveEndereco;
    }


    private limparEndereco() {
        document.forms[0].logradouro.value = "";
		document.forms[0].tipoLogradouro.value="";
    }
    private limparChamadoEndereco() {
        var fluxoExecucao = "fluxoInclusao";
        $("#gridChamadoEndereco").load("limparEnderecos?&fluxoExecucao=" + fluxoExecucao, () => {
            this.inicializarTabelaChamadoEndereco();
        });
    }


}
