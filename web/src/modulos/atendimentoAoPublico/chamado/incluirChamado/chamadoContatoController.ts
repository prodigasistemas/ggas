import {postConstruct} from "inversify";
import {provide} from "inversify-binding-decorators";
import {Datatable} from "../../../../negocio/datatables";

declare const carregarFragmento : Function; //Função antiga, global, definida no importsHeader.jsp

/**
 * Controlador padrão da aba de cliente da tela de incluir chamados
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(ChamadoContatoController)
export class ChamadoContatoController {

    @postConstruct()
    public init() {
        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["gridAbaChamadoContato", "divChamadoContatoCliente"].includes(alvo)) {
                this.configurarComponentes();
            }
        });

        this.configurarComponentes();
    }

    private configurarComponentes() {
        new Datatable('#table-grid-chamado-contato').inicializar({
            destroy : true,
            columnDefs: [
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: -1 }
            ],
            initComplete : () => {

                $("#removerContato").click((e) => {
                    if (confirm('Deseja excluir o contato?')) {
                        this.removerContato($(e.currentTarget).data("indexlistachamadocontato"));
                    }
                });

                $("#editarContato").click((e) => {
                    const $input = $(e.currentTarget);
                    let indice = $input.data("indexlistachamadocontato");
                    let idTipoContato = $input.data("idtipocontato");
                    let nomeContato = $input.data("nome");
                    let dddContato = $input.data("dddcontato");
                    let telefoneContato = $input.data("telefonecontato");
                    let ramalContato = $input.data("ramalcontato");
                    let cargoContato = $input.data("cargocontato");
                    let areaContato = $input.data("areacontato");
                    let emailContato = $input.data("emailcontato");

                    this.exibirAlteracaoDoContato(indice, idTipoContato, nomeContato, dddContato, telefoneContato,
                        ramalContato, cargoContato, areaContato, emailContato)
                });

                $("#setContatoPrincipal").click(e => {
                    const $input = $(e.currentTarget);
                    this.atualizarContatoPrincipal($input.data("index"));
                })
            }
        });
        $("#botaoAlterarContato").unbind("click").click(() => {
            if ($("#indexListaContatos").val()) {
                this.alterarContato();
            }
        });

        $("#botaoIncluirContato").unbind("click").click(() => {
            this.incluirContato();
        });

        $("#botaoLimparContato").unbind("click").click(() => {
            this.limparCamposContato();
        });
    }

    private exibirAlteracaoDoContato(indice, idTipoContato, nomeContato, dddContato, telefoneContato, ramalContato,
                                     cargoContato, areaContato, emailContato) {
        $("#indexListaContatos").val(indice);
        $("#tipoContato").val(idTipoContato);
        $("#nomeContato").val(nomeContato);
        $("#codigoDDD").val(dddContato);
        $("#foneContato").val(telefoneContato);
        $("#ramalContatoBootstrap").val(ramalContato);
        $("#profissaoContato").val(cargoContato);
        $("#descricaoArea").val(areaContato);
        $("#emailContato").val(emailContato);
        $("#botaoIncluirContato").attr("disabled", "disabled");
        $("#botaoAlterarContato").removeAttr("disabled");

    }

    private alterarContato() {
        if (this.validarCampos()) {
            let nomeContato = $("#nomeContato").val();
            let codigoDDD = $("#codigoDDD").val();
            let foneContato = $("#foneContato").val();
            let ramalContato = $("#ramalContatoBootstrap").val();
            let descricaoArea = $("#descricaoArea").val();
            let emailContato = $("#emailContato").val();
            let idTipoContato = $("#tipoContato").val();
            let idProfissaoContato = $("#profissaoContato").val();
            let indexListaContatos = $("#indexListaContatos").val();

            let url = `alterarChamadoContato?nomeContato=${nomeContato}`
                + "&codigoDDD=" + (codigoDDD ? codigoDDD : "")
                + "&foneContato=" + (foneContato ? foneContato: "")
                + "&ramalContato=" + (ramalContato ? ramalContato: "")
                + "&descricaoArea=" + (descricaoArea ? descricaoArea: "")
                + "&emailContato=" + (emailContato ? emailContato: "")
                + "&idTipoContato=" + (idTipoContato ? idTipoContato: "")
                + "&idProfissaoContato=" + (idProfissaoContato ? idProfissaoContato : "")
                + `&indexListaContatos=${indexListaContatos}`;

            carregarFragmento('gridAbaChamadoContato', url);

            $("#indexListaContatos").val(null);
            $("#botaoAlterarContato").attr("disabled", "disabled");
            $("#botaoIncluirContato").removeAttr("disabled");
            this.limparCamposContato();
        } else {
            alert("Preencha todos os campos obrigatórios e, caso informe um email, digite um email válido!");
        }
    }

    private limparCamposContato() {
        $("#tipoContato").val('-1');
        $("#profissaoContato").val('-1');
        $("#foneContato").val(null);
        $("#nomeContato").val(null);
        $("#codigoDDD").val(null);
        $("#ramalContatoBootstrap").val(null);
        $("#descricaoArea").val(null);
        $("#emailContato").val(null);

    }

    private incluirContato() {

        if (this.validarCampos()) {
            let nomeContato = $("#nomeContato").val();
            let codigoDDD = $("#codigoDDD").val();
            let foneContato = $("#foneContato").val();
            let ramalContato = $("#ramalContatoBootstrap").val();
            let descricaoArea = $("#descricaoArea").val();
            let emailContato = $("#emailContato").val();
            let idTipoContato = $("#tipoContato").val();
            let idProfissaoContato = $("#profissaoContato").val();

            let url = `adicionarChamadoContato?nomeContato=${nomeContato}`
                + "&codigoDDD=" + (codigoDDD ? codigoDDD : "")
                + "&foneContato=" + (foneContato ? foneContato: "")
                + "&ramalContato=" + (ramalContato ? ramalContato: "")
                + "&descricaoArea=" + (descricaoArea ? descricaoArea: "")
                + "&emailContato=" + (emailContato ? emailContato: "")
                + "&idTipoContato=" + (idTipoContato ? idTipoContato: "")
                + "&idProfissaoContato=" + (idProfissaoContato ? idProfissaoContato : "");
            carregarFragmento('gridAbaChamadoContato', url);
            this.limparCamposContato();
        } else {
            alert("Preencha todos os campos obrigatórios e, caso informe um email, digite um email válido!");
        }
    }

    private validarCampos() : boolean {
        let camposValidos = $("#tipoContato").val() > 0 && $("#nomeContato") != null && $("#nomeContato").val() != "";
        let campoEmailValido = $("#emailContato")[0].checkValidity();
        return camposValidos && campoEmailValido;
    }

    private removerContato(indice) {

        var noCache = "noCache=" + new Date().getTime();
        $("#gridAbaChamadoContato").load("removerChamadoContato?indexListaContatos=" + indice + "&" + noCache, () => {
            this.configurarComponentes();
        });
        $("#indexListaContatos").val(null);
        $("#botaoAlterarContato").attr("disabled", "disabled");
        $("#botaoIncluirContato").removeAttr("disabled");
    }

    private atualizarContatoPrincipal(indice) {

        var noCache = "noCache=" + new Date().getTime();

        $("#gridAbaChamadoContato").load("atualizarChamadoContatoPrincipal?indexListaContatos=" + indice + "&" + noCache);
    }

}
