import {Cliente} from "../../../../dominio/Cliente";
import {ModalPesquisarCliente} from "../../../../componentes/pesquisarCliente/modal";
import {inject, postConstruct} from "inversify";
import {provide} from "inversify-binding-decorators";
import {ClienteService} from "../../../../servicos/http/cliente.service";
import {ContratoService} from "../../../../servicos/http/contrato.service";
import {LoadingService} from "../../../../servicos/loading.service";
import { AbaTitulosController } from "./abaTituloController";

declare const carregarFragmento, selecionarPontoConsumo, verificarGarantia : Function; //Função antiga, global, definida no importsHeader.jsp

/**
 * Controlador padrão da aba de cliente da tela de incluir chamados
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(AbaClienteController)
export class AbaClienteController {

    @inject(ModalPesquisarCliente)
    private modalPesquisarCliente : ModalPesquisarCliente;

    @inject(ClienteService)
    private clienteService : ClienteService;

    @inject(ContratoService)
    private contratoService : ContratoService;

    @inject(LoadingService)
    private loading : LoadingService;
    
    abaTitulosController : AbaTitulosController;

    @postConstruct()
    public init() {
		
		this.abaTitulosController = new AbaTitulosController();
		
        this.modalPesquisarCliente.setCallback(this.selecionarCliente.bind(this));
        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["divCliente"].includes(alvo)) {
                this.configurarElementosPagina();
            }
        })
    }

    private configurarElementosPagina() {
        $("#abaCliente_pesquisarCliente").click(() => {
            this.modalPesquisarCliente.exibir();
        });

        this.configurarAutoComplete();
    }

    private async selecionarCliente(cliente : Cliente) {
        this.loading.exibir(true);
        let resultado = await this.contratoService.obterContratoPontoConsumoPorCliente(cliente.chavePrimaria);
        this.loading.exibir(false);

        if (resultado.ok) {
            let contratos = resultado.conteudo;
            let idContrato = contratos && contratos.length ? contratos[0].contrato.chavePrimaria : null;
            this.carregarCliente(cliente.chavePrimaria, idContrato);
        }
    }

    private async configurarAutoComplete() {
        const cfgAutoComplete = (requisicao : Function) => {
            return {
                source: async (request, response) => {

                    let resultado = await requisicao(request.term);
                    if (resultado.ok) {
                        response(resultado.conteudo.map((item, index) => {
                            return {
                                label: item.nome + '<br>' +
                                this.getValorNullable(item.cpfCnpj) +
                                this.getValorNullable(item.endereco) +
                                this.getValorNullable(item.numeroContrato) +
                                this.getValorNullable(item.inicioVigenciaContrato) +
                                this.getValorNullable(item.fimVigenciaContrato, false),
                                value: item.chavePrimaria,
                                idContrato: item.idContrato,
                                index
                            }
                        }));
                    }
                },
                create: function () {
                    $(this).data('ui-autocomplete')._renderItem = (ul, item) => {
                        let li = '<li>';
                        if(item.index % 2 == 0) {
                            li = '<li style="background-color: #E3F3FF">'
                        }
                        return $(li)
                            .append('<a>' + item.label + '</a>')
                            .appendTo(ul);
                    };
                },
                minLength: 3,
                select: async (event, ui) => {
                    this.carregarCliente(ui.item.value, ui.item.idContrato);
                },
                open: function () {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            };

        };

        $("#nome").autocomplete(cfgAutoComplete((nome) => {
            return this.clienteService.pesquisarClienteContratoPorNome(nome);
        })).focus(function () {
            $(this).data("uiAutocomplete").search($(this).val());
        });

        $("#nomeFantasia").autocomplete(cfgAutoComplete((nomeFantasia) => {
            return this.clienteService.pesquisarClienteContratoPorNome(null, nomeFantasia)
        }));

    }


    private getValorNullable(item, quebra = true) {
        let valor = '';
        if (item) {
            valor = item;
            if (quebra) {
                valor += '<br>';
            }
        }

        return valor;
    }

    /**
     * Carrega as informações do cliente  selecionado na tela imóvel, chamados e pontos de consumo
     * a partir da sua chave primária ou identificador do contrato
     *
     * Função antiga, copiada do JSP refatorado. O código foi mantido o mesmo por conter muita lógica.
     */
    private carregarCliente(chavePrimaria, idContrato) {

        document.forms[0].contrato.value = "";
        let url;
        if (idContrato != null) {
            url = "carregarClientePorIdContrato?chavePrimariaContrato=" + idContrato;
        } else if (chavePrimaria != null) {
            url = "carregarCliente?chavePrimaria=" + chavePrimaria;
        }

        if (url != null) {
			carregarFragmento('divCliente', url, null);
            let chavePrimariaCliente = document.forms[0].chaveCliente.value;

            if ($('#exibirPassaporteCliente').val() == "2") {
                $("#divPassaporte").hide();
            }
            $("#gridChamadosCliente").load("carregarChamadosClienteImovel?chavePrimariaCliente=" + chavePrimariaCliente, () => {
                $("body").trigger("carregarFragmentoFinished", ["gridChamadosCliente"]);
            });
            document.forms[0].cliente.value = chavePrimariaCliente;
            document.forms[0].chaveContrato.value = idContrato;

            if (idContrato != null) {
                $("#gridImoveis").load("carregarImovelPorContrato?chavePrimariaContrato=" + idContrato + "&numeroContrato=", () => {
                    if($("input[name='chaveImovel']").size() == 1) {
                        $("input[name='chaveImovel']").trigger( "click" );
                        verificarGarantia(chavePrimariaCliente);
                    } else if ($("input[name='chaveImovel']").size() == 0) {
                        selecionarPontoConsumo("-1")
                    }
                    $("body").trigger("carregarFragmentoFinished", ["gridImoveis"]);
                });
            } else {
                $("#gridImoveis").load("carregarImovelPorContrato?chavePrimariaContrato=-1&numeroContrato=", () => {
                    $("body").trigger("carregarFragmentoFinished", ["gridImoveis"]);
                });
                $("#gridChamadoPontosConsumo").load("carregarPontosConsumo?chavePrimariaImovel=-1&fluxoExecucao=fluxoInclusao&chavePrimariaContrato=-1", () => {
                    $("body").trigger("carregarFragmentoFinished", ["gridChamadoPontosConsumo"]);
                });
                selecionarPontoConsumo("-1");
            }


            this.exibirAlertaClienteDiferenteSolicitante();


            (document.getElementById('bottonChamadoContato') as any).disabled = false;
            url = encodeURI("exibirContatoCliente?chavePrimariaCliente=" + chavePrimariaCliente);
            carregarFragmento('divChamadoContatoCliente', url);
            
            this.abaTitulosController.pesquisarTitulo(document.forms[0].cpfCnpj.value);
            
        }
    }

    /**
     * Exibe um alerta sobre replicar informações do cliente para o solicitante
     *
     * Função antiga, copiada do JSP refatorado.
     */
    private exibirAlertaClienteDiferenteSolicitante() {
        let isClienteSelecionadoDiferenteSolicitante = $("#nome").val() != $("#nomeSolicitante").val() || $("#cpfCnpj").val() != $("#cpfCnpjSolicitante").val() ||
            $("#email").val() != $("#emailSolicitante").val() || $("#telefone").val() != $("#telefoneSolicitante").val()
            || $("#rg").val() != $("#rgSolicitante").val();

        if (isClienteSelecionadoDiferenteSolicitante
            && confirm("Deseja replicar os dados do cliente para o solicitante?")) {

            $("#nomeSolicitante").val($("#nome").val());
            $("#cpfCnpjSolicitante").val($("#cpfCnpj").val());
            $("#emailSolicitante").val($("#email").val());
            $("#telefoneSolicitante").val($("#telefone").val());
            $("#rgSolicitante").val($("#rg").val());

        }

    }

}
