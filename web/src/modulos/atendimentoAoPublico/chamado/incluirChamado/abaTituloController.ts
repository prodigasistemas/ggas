import { provide } from "inversify-binding-decorators";
import { postConstruct, inject } from "inversify";
import { Datatable } from "../../../../negocio/datatables";

@provide(AbaTitulosController)
export class AbaTitulosController {

	private $buttonAdicionarAnalise;

	private $inputRemoveAnalise;

	@postConstruct()
	public onInit() {
		this.configurarElementosPagina();

	}

	private configurarElementosPagina() {
		this.inicializarTabelaTituloAberto();
		this.inicializarTabelaTituloAnalise();

	}

	public pesquisarTitulo(cnpfCpf: string) {
	    var isTabelaTitulosPesquisada = $("#isTabelaTitulosPesquisada").val();
	    var isInclusaoComErro = $("#isInclusaoComErro").val();
	   	
	   	if(isTabelaTitulosPesquisada != cnpfCpf && isInclusaoComErro != "true") {
			$("#gridTituloAberto").load(`consultarTitulosAbertos?cnpjCpf=${cnpfCpf}`, () => {
				this.inicializarTabelaTituloAberto();
				
				const tableElement = $('#table-grid-titulos-analise');

				if ($.fn.DataTable.isDataTable(tableElement)) {
				    tableElement.DataTable().clear().destroy();
				}
				
				new Datatable('#table-grid-titulos-analise').inicializar({
					destroy: true
				});				
				
			});
			
			$("#isTabelaTitulosPesquisada").val(cnpfCpf);
		}

	}

	private inicializarTabelaTituloAberto() {
		new Datatable('#table-grid-titulos').inicializar({
			destroy: true
		});

		this.$buttonAdicionarAnalise = $("#buttonAdicionarAnalise");

		this.$buttonAdicionarAnalise.click(async () => {

			const titulosMarcados = document.querySelectorAll<HTMLInputElement>('input[name="numeroTitulo"]:checked');

			const titulosAnalise = Array.from(titulosMarcados).map(checkbox => checkbox.value);

			if (titulosAnalise.length > 0) {
				this.adicionarTituloAnalise(titulosAnalise);
			} else {
				alert("Selecione ao menos um título aberto.");
			}

		});
	}

	private inicializarTabelaTituloAnalise() {
		new Datatable('#table-grid-titulos-analise').inicializar({
			destroy: true
		});

		this.$inputRemoveAnalise = document.querySelectorAll('.botaoRemoverAnalise');
		this.$inputRemoveAnalise.forEach(input => {
			input.addEventListener('click', (event) => {
				const target = event.target as HTMLInputElement;
				const numeroTitulo = target.id;

				this.removerTituloAnalise(numeroTitulo);
			});
		});
	}

	private adicionarTituloAnalise(numerosTitulos: String[]) {
		$("#gridTituloAnalise").load(`adicionarTituloAnalise?titulosAbertos=${numerosTitulos}`, () => {
			this.inicializarTabelaTituloAnalise();
			this.atualizarTitulosAberto();

			this.$inputRemoveAnalise = document.querySelectorAll('.botaoRemoverAnalise');
			this.$inputRemoveAnalise.forEach(input => {
				input.addEventListener('click', (event) => {
					const target = event.target as HTMLInputElement;
					const numeroTitulo = target.id;

					this.removerTituloAnalise(numeroTitulo);
				});
			});


		});
	}

	private removerTituloAnalise(numeroTitulo: String) {

		$("#gridTituloAnalise").load(`removerTituloAnalise?tituloRemover=${numeroTitulo}`, () => {
			this.inicializarTabelaTituloAnalise();
			this.atualizarTitulosAberto();
		});
	}

	private atualizarTitulosAberto() {
		$("#gridTituloAberto").load(`atualizarTitulosAbertos`, () => {
			this.inicializarTabelaTituloAberto();

		});
	}


}

(window as any).AbaTitulosController = AbaTitulosController;

