import {provide} from "inversify-binding-decorators";
import {postConstruct, inject} from "inversify";
import {Datatable} from "../../../../negocio/datatables";
import {LoadingService} from "../../../../servicos/loading.service";

@provide(AbaImoveisController)
export class AbaImoveisController {

    @inject(LoadingService)
    private loading;

    @postConstruct()
    public onInit() {
        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["divCliente", "gridImoveis"].includes(alvo)) {
                this.configurarElementosPagina();
            }
            this.inicializarTabelaPontosConumo();
        })
    }

    private configurarElementosPagina() {
        $("#botaoPesquisar").unbind('click');
        $("#botaoPesquisar").click((e) => {
            this.pesquisarImovel();
            this.limparContratoECliente();
            this.limparPontoConsumo();
        });
        this.inicializarTabelaImoveis();
        this.inicializarTabelaPontosConumo();
        this.inicializarTabelaTituloAberto();
    }

    private inicializarTabelaImoveis() {
        new Datatable('#table-grid-imoveis').inicializar({
            destroy: true,
            columnDefs: [
                {"targets": [0], "searchable": false, "orderable": false, "visible": true}
            ]
        });
    }

    private inicializarTabelaPontosConumo() {
        new Datatable('#table-grid-pontos-consumo').inicializar({
            destroy: true,
            columnDefs: [
                {"targets": [0], "searchable": false, "orderable": false, "visible": true}
            ]
        });
    }

    private bloquearBotaoPesquisar(bloquear) {
        if (bloquear) {
            $("#botaoPesquisar").attr('disabled', true);
            $("#table-grid-imoveis_wrapper").hide();
        } else {
            $("#botaoPesquisar").attr('disabled', false);
            $("#table-grid-imoveis_wrapper").show();
        }
    }

    private pesquisarImovel() {
        var noCache = "noCache=" + new Date().getTime();
        var numeroImovel = document.forms[0].numeroImovel.value;
        var complemento = encodeURI(document.forms[0].complemento.value);
        var nome = encodeURI(document.forms[0].nomeImovel.value);
        var matricula = document.forms[0].matricula.value;
        var numeroCep = (document.getElementById('cepImovel') as any).value;


        this.loading.exibir(true);
        $("#gridImoveis").load(`consultarImovel?numeroImove=${numeroImovel}&complemento=${complemento}` +
            `&nome=${nome}&matricula=${matricula}&numeroCep=${numeroCep}`, () => {
            this.inicializarTabelaImoveis();
            this.loading.exibir(false);
        });

    }


    private limparContratoECliente() {

        document.forms[0].chaveCliente.value = -1;
        document.forms[0].chaveContrato.value = -1;
        document.forms[0].contrato.value = "";
        document.forms[0].nome.value = "";
        document.forms[0].nomeFantasia.value = "";
        document.forms[0].cpfCnpj.value = "";
        document.forms[0].numeroPassaporte.value = "";
        document.forms[0].email.value = "";
        document.forms[0].telefone.value = "";

    }

    private limparPontoConsumo() {

        $('#chavePontoConsumo').val('');
        var fluxoExecucao = "fluxoInclusao";
        $("#gridChamadoPontosConsumo").load("limparPontosConsumo?&fluxoExecucao=" + fluxoExecucao, () => {
            this.inicializarTabelaPontosConumo();
        });
    }
    
    
        private inicializarTabelaTituloAberto() {
        new Datatable('#table-grid-titulos').inicializar({
            destroy: true,
            columnDefs: [
                {"targets": [0], "searchable": false, "orderable": false, "visible": true}
            ]
        });
    }


}
