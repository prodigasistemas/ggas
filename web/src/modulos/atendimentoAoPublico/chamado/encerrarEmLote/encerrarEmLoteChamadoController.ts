import {inject, postConstruct} from "inversify";
import {provide} from "inversify-binding-decorators";
import {LoadingService} from "../../../../servicos/loading.service";
import {HttpService} from "../../../../servicos/http/http.service";
import {PropriedadesSistemaService} from "../../../../servicos/propriedades.sistema.service";
import {NotificadorService} from "../../../../servicos/notificador.service";
import {MensagemService} from "../../../../servicos/mensagem.service";

declare const submeter: Function; //Função antiga, global

/**
 * Controlador padrão da lógica da tela de encerrar chamado em lote
 *
 * @author italo.alan@logiquesistemas.com.br
 */

@provide(EncerrarEmLoteChamadoController)
export class EncerrarEmLoteChamadoController {

    @inject(LoadingService)
    private loading: LoadingService;

    @inject(HttpService)
    private httpService: HttpService;

    @inject(PropriedadesSistemaService)
    private propriedades: PropriedadesSistemaService;

    @inject(NotificadorService)
    private notificador: NotificadorService;

    @inject(MensagemService)
    private messageService: MensagemService;

    @postConstruct()
    public onInit() {
        console.log('EncerrarEmLoteChamadoController iniciado');
        this.configurarAcoesEEventos();
    }

    private configurarAcoesEEventos() {
        this.carregarMascaraDataResolucao();

        $('#cancelar').on('click', () => {
            submeter('chamadoForm', 'exibirPesquisaChamado');
        });

        $('#voltar').on('click', () => {
            document.forms['chamadoForm'].descricao.value = '';
            document.forms['chamadoForm'].idMotivo.value = '-1';
            document.forms['chamadoForm'].dataResolucao.value = '';
        });

        $('#botaoEncerrar').on('click', async () => {
            console.log('clicado para encerrar');


            let resultado = await this.notificador.exibirPergunta("Encerrar chamados em lote", `Deseja realmente encerrar todos os chamados?`);
            if (resultado.value) {
                this.encerrarEmLote();
            }
        })
    }

    private carregarMascaraDataResolucao() {
        $("#dataResolucao").inputmask("99/99/9999 99:99", {placeholder: "_", greedy: false});
        $("#dataResolucao").datetimepicker({
            maxDate: '+0d',
            showOn: 'button',
            buttonImage: $('#imagemCalendario').val(),
            buttonImageOnly: true,
            buttonText: 'Exibir Calendário',
            dateFormat: 'dd/mm/yy',
            timeFormat: 'HH:mm'
        });
        $('#dataResolucao').removeAttr('disabled');
    }

    async encerrarEmLote() {
        this.loading.exibir(true);
        const descricao = $('#descricao').val();
        const dataResolucao = $('#dataResolucao').val();
        const idMotivo = $('#idMotivo').val()
        let url = `${this.propriedades.contextoAplicacao()}/encerramentoEmLoteChamado/encerrar`;
        const resposta = await this.httpService.post<any>(url, {descricao, dataResolucao, idMotivo});
        console.log('resposta', resposta);

        this.loading.exibir(false);
        if (resposta.codigo === 400) {
            const resultado = JSON.parse(resposta.erro);
            const erros = ['Corrija os erros abaixo antes de prosseguir'];
            this.messageService.exibirMensagemErro(erros.concat(resultado.erros));
        } else if (resposta.codigo === 200) {
            this.marcarTodasAsLinhasComErro();
            this.marcarAsLinhasComSucesso(resposta);

            if (resposta.conteudo.erros.length === 0) {
                this.tratarRespostaTodosChamadosEncerradosComSucesso();
            } else {
                this.exibirMensagemChamadosComFalhaNoEncerramento(resposta);
            }
        } else {
            this.messageService.exibirMensagemErro('Ocorreu um erro ao encerrar os chamados em lote');
        }
    }

    private exibirMensagemChamadosComFalhaNoEncerramento(resposta) {
        const erros = [];
        erros.push('Não foi possível encerrar todos os chamados do lote, verifique abaixo os chamados que não foram encerrados.');
        this.messageService.exibirMensagemErro(erros.concat(resposta.conteudo.erros));
    }

    private tratarRespostaTodosChamadosEncerradosComSucesso() {
        this.messageService.exibirMensagemSucesso('Todos os chamados foram encerrados com sucesso, redirecionando para tela de pesquisa...');

        setTimeout(() => {
            window.location.href = `${this.propriedades.contextoAplicacao()}/exibirPesquisaChamado`;
        }, 3000);
    }

    private marcarAsLinhasComSucesso(resposta) {
        resposta.conteudo.chamadosSucesso.forEach(chamadoSucesso => {
            $(`#status${chamadoSucesso.chavePrimaria}`).html(chamadoSucesso.statusDescricao);
            $(`#chamado${chamadoSucesso.chavePrimaria}`).css({'color': '#21b133'});
        });
    }

    private marcarTodasAsLinhasComErro() {
        $('#table-chamados > tbody  > tr').each(function () {
            $(this).css({'color': '#ff2c17'});
        });
    }
}
