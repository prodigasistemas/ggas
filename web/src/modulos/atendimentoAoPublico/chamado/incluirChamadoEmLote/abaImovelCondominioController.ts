/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {postConstruct, inject} from "inversify";
import {Datatable} from "../../../../negocio/datatables";
import {LoadingService} from "../../../../servicos/loading.service";
import {ImovelService} from "../../../../servicos/http/imovel.service";
import {NotificadorService} from "../../../../servicos/notificador.service";
import {Imovel} from "../../../../dominio/Imovel";
import {PontoConsumoService} from "../../../../servicos/http/pontoConsumo.service";
import {PontoConsumo} from "../../../../dominio/PontoConsumo";

@provide(AbaImovelCondominioController)
export class AbaImovelCondominioController {

    @inject(LoadingService)
    private loading;

    @inject(ImovelService)
    private imovelService : ImovelService;

    @inject(PontoConsumoService)
    private pontoConsumoService : PontoConsumoService;

    @inject(NotificadorService)
    private notificador : NotificadorService;

    private $tabela;
    private $tabelaPontoConsumo;

    @postConstruct()
    public onInit() {
        this.$tabela = $("#table-grid-imoveis");
        this.$tabelaPontoConsumo = $("#table-grid-pontos-consumo");
        this.configurarElementosPagina();

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["divCliente", "gridImoveis"].includes(alvo)) {
                this.configurarElementosPagina();
            }
            this.inicializarTabelaPontosConsumo([]);
        })
    }

    public obterPontosConsumoSelecionados() : PontoConsumo[] {
        let data = this.$tabelaPontoConsumo.DataTable().data();
        return data.toArray().filter(c => c.marcado);
    }

    private configurarElementosPagina() {
        $("#botaoPesquisar").unbind('click');
        $("#botaoPesquisar").click((e) => {
            this.pesquisarImovel();
            this.inicializarTabelaPontosConsumo([]);
        });
        this.inicializarTabelaImoveis([]);
        this.inicializarTabelaPontosConsumo([]);
        $("#selecionarTodosPontosConsumo").click((e) => {
            let marcarTodos = $(e.currentTarget).prop("checked");
            let data = this.$tabelaPontoConsumo.DataTable().data();
            data.toArray().forEach(c => c.marcado = marcarTodos);
            this.$tabelaPontoConsumo.DataTable().rows().every( function () {
                this.invalidate();
            });
            this.$tabelaPontoConsumo.DataTable().draw();

        })
    }

    private inicializarTabelaImoveis(imovel : Imovel[]) {
        new Datatable('#table-grid-imoveis').inicializar({
            destroy: true,
            columnDefs: [
                {"targets": [0], "searchable": false, "orderable": false, "visible": true}
            ],
            data : imovel,
            columns: [
                {data : null, render : (data : Imovel) => {
                    return `<input type='radio' name='chaveImovel' value='${data.chavePrimaria}' />`;
                }, className: "align-middle text-center" },
                {data : "chavePrimaria", className: "align-middle text-center"},
                {data : "nome", className: "align-middle text-center"},
                {data : "situacaoImovel.descricao", className: "align-middle text-center"},
                {data : "endereco", className: "align-middle text-center"},
                {data : "tipoMedicao", defaultContent: "", className: "align-middle text-center"}
            ],
            drawCallback: () => {
                $("[name='chaveImovel']").unbind("click");
                $("[name='chaveImovel']").click(async (e) => {
                    let $linha = $(e.currentTarget).parents("tr");
                    let imovel : Imovel = this.$tabela.DataTable().row($linha).data();
                    this.loading.exibir(true);
                    let resposta = await this.pontoConsumoService.buscarPontosConsumoPorImovel(imovel.chavePrimaria);
                    this.loading.exibir(false);
                    this.notificador.tratarRespostaHttp(resposta, null, () => {
                        this.inicializarTabelaPontosConsumo(resposta.conteudo);
                    })
                })
            }
        });
    }

    private inicializarTabelaPontosConsumo(pontosConsumo : PontoConsumo[]) {
        new Datatable('#table-grid-pontos-consumo').inicializar({
            destroy: true,
            columnDefs: [
                {"targets": [0], "searchable": false, "orderable": false, "visible": true}
            ],
            data : pontosConsumo,
            columns: [
                {data : null, render : (data : PontoConsumo) => {
                    return $("<div>").append(

                        $("<div>", {class : "custom-control custom-checkbox custom-control-inline mr-0"}).append(
                            $("<input>", { class : "custom-control-input", type : "checkbox", checked: (data as any).marcado,
                                id : "chkPonto" + data.chavePrimaria, name: "chavePontoConsumoAux[]", value : data.chavePrimaria}),
                            $("<label>", {class : "custom-control-label labelPontoConsumo", for: "chkPonto" + data.chavePrimaria})

                        )).html();
                }, className: "align-middle text-center"},
                {data : "descricao", defaultContent : "", className: "align-middle text-center"}
            ],
            drawCallback: () => {
                $("[name='chavePontoConsumoAux[]']").unbind("click");
                $("[name='chavePontoConsumoAux[]']").click(async (e) => {
                    let $linha = $(e.currentTarget).parents("tr");
                    let imovel : Imovel = this.$tabelaPontoConsumo.DataTable().row($linha).data();
                    (imovel as any).marcado = $(e.currentTarget).prop("checked");
                    this.$tabelaPontoConsumo.DataTable().row($linha).invalidate();
                    this.$tabelaPontoConsumo.DataTable().draw();
                    this.atualizarSelecionarTodos();

                })
            }
        });
    }

    private async pesquisarImovel() {
        let numeroImovel = document.forms[0].numeroImovel.value;
        let complemento = encodeURI(document.forms[0].complemento.value);
        let nome = encodeURI(document.forms[0].nomeImovel.value);
        let matricula = document.forms[0].matricula.value;
        let numeroCep = (document.getElementById('cepImovel') as any).value;


        this.loading.exibir(true);
        let resposta = await this.imovelService.consultarImovel(nome, complemento, matricula, numeroImovel, numeroCep, true);
        this.loading.exibir(false);
        this.notificador.tratarRespostaHttp(resposta, null, () => {
            this.inicializarTabelaImoveis(resposta.conteudo);
        })
    }

    private atualizarSelecionarTodos() {
        let selecionarTodosMarcado = this.$tabelaPontoConsumo.DataTable().data().toArray().every(a => a.marcado);
        $("#selecionarTodosPontosConsumo").prop("checked", selecionarTodosMarcado);
    }


}
