/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {GenericController} from "../../../genericController";
import {PontoConsumo} from "../../../../dominio/PontoConsumo";
import {Datatable} from "../../../../negocio/datatables";

declare let AjaxService : any;

/**
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(VerificacaoGarantia)
export class VerificacaoGarantia extends GenericController{

    private $dialogGarantiaServico = $("#dialogGarantiaServico");

    public exibirVerificacaoGarantia(chaveAssunto, pontosConsumo : PontoConsumo[]) : Promise<boolean> {
        return new Promise<boolean>((resolve) => {

            this.loading.exibir(true);

            AjaxService.servicosChamadoComPrazosEmLote(chaveAssunto, pontosConsumo.map(p => p.chavePrimaria), {
                callback: (garantias: GarantiaServico[]) => {

                    AjaxService.isGarantiaVigentePontoConsumo(chaveAssunto, pontosConsumo.map(p => p.chavePrimaria), {
                        callback: (garantiaVigente: boolean) => {
                            this.loading.exibir(false);
                            this.exibirModal(garantias, garantiaVigente);

                            $("#button-salvar-servico-disponivel").click(() => {
                                resolve(true);
                            });
                            this.$dialogGarantiaServico.on('hidden.bs.modal', () => {
                                resolve(false);
                            });
                        }
                    });
                }
            });
        });
    }

    private exibirModal(garantias: GarantiaServico[], garantiaVigente: boolean) {
        this.$dialogGarantiaServico.modal();
        let urlImagem = $('#url_imagens').val();
        new Datatable("#tabelaGarantia").inicializar({
            destroy: true,
            data : garantias,
            columns: [
                {data : null, render : (data : GarantiaServico) => {
                    if (data.naGarantia) {
                        return '<img style="margin-bottom: 2px;" src=\"' + urlImagem + 'success_icon.png\" />' + ' Disponível';
                    } else if (data.naGarantia === undefined) {
                        return '<img style="margin-bottom: 2px;" src=\"' + urlImagem + 'success_icon.png\" />' + ' Ilimitado';
                    } else {
                        return '<img style="margin-bottom: 2px;" src=\"' + urlImagem + 'cancel16.png" />' + ' <span style="color:' +
                        ' red">Indisponível</span>';
                    }
                }, className: "align-middle text-center" },
                {data : "descricao", defaultContent: "-"},
                {data : "pontoConsumo", defaultContent: "-"},
                {data : "cliente", defaultContent: "-"},
                {data : "numeroMaximoExecucoes", defaultContent: "-"},
                {data : "numeroExecucoesDisponiveis", defaultContent: "-"},
                {data : "dataExecucao", defaultContent: "Não executado"},
                {data : "prazoGarantia", defaultContent: "-"},
                {data : "dataLimiteGarantia", defaultContent: "Não iniciado"},
            ],

        });

        if (garantiaVigente) {
            $("#notificacao-servico-disponivel").show();
            $("#notificacao-servico-indisponivel").hide();
            $("#titulo-servicos-garantia-disponivel").show();
            $("#titulo-servicos-garantia-indisponivel").hide();
            $("#footer-disponivel").show();
            $("#footer-indisponivel").hide();
        } else {
            $("#notificacao-servico-disponivel").hide();
            $("#notificacao-servico-indisponivel").show();
            $("#titulo-servicos-garantia-disponivel").hide();
            $("#titulo-servicos-garantia-indisponivel").show();
            $("#footer-disponivel").hide();
            $("#footer-indisponivel").show();
        }
    }

}


interface GarantiaServico {
    naGarantia : boolean,
    cliente : String,
    pontoConsumo : String,
    descricao : String,
    numeroMaximoExecucoes : number,
    prazoGarantia : number,
    dataExecucao : String,
    dataLimiteGarantia : String,
    numeroExecucoesDisponiveis : number,
}
