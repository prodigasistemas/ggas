import {inject, postConstruct} from "inversify";
import {Datatable} from "../../../../negocio/datatables";
import {provide} from "inversify-binding-decorators";
import {AbaImovelCondominioController} from "./abaImovelCondominioController";
import {ChamadoContatoController} from "../incluirChamado/chamadoContatoController";
import {ChamadoAssuntoService} from "../../../../servicos/http/chamadoAssunto.service";
import {GenericController} from "../../../genericController";
import {HttpService} from "../../../../servicos/http/http.service";
import {UrlUtil} from "../../../../negocio/util/UrlUtil";
import {PontoConsumo} from "../../../../dominio/PontoConsumo";
import {VerificacaoGarantia} from "./verificacaoGarantia";

/**
 * Controlador padrão da lógica da tela de incluir chamado
 *
 * @author jose.victor@logiquesistemas.com.br, italo.alan@logiquesistemas.com.br
 */

@provide(IncluirChamadoEmLoteController)
export class IncluirChamadoEmLoteController extends GenericController{

    @inject(AbaImovelCondominioController)
    private abaImoveisCondominio : AbaImovelCondominioController;

    @inject(ChamadoContatoController)
    private chamadoContato : ChamadoContatoController;

    @inject(ChamadoAssuntoService)
    private chamadoAssuntoService : ChamadoAssuntoService;

    @inject(VerificacaoGarantia)
    private verificacaoGarantia : VerificacaoGarantia;

    @inject(HttpService)
    protected http : HttpService;

    private $chamadoAssunto;
    private $limparEmLote;


    @postConstruct()
    public onInit() {
        this.$limparEmLote = $("#limparFormulario");
        this.$chamadoAssunto = $("#chamadoAssunto");
        this.configurarDatatable();
        this.configurarListenCarregarFragmento();
        $("#chamadoTipo").change(e => {
            $(e.currentTarget).val();

        });
        this.configurarListenTipoChamado();
        this.configurarValidacaoForm();
        this.configurarLimpar();
    }

    private configurarDatatable() {

        new Datatable('#table-grid-chamado-historico').inicializar({destroy : true});
        new Datatable('#table-grid-chamado-historico-anexo').inicializar({destroy : true});
        new Datatable('#table-grid-anexo').inicializar({destroy : true});
        new Datatable('#table-grid-chamado-cliente').inicializar({destroy : true});
        new Datatable('#table-grid-tabela-ponto-consumo').inicializar({destroy : true});
    }

    private configurarListenCarregarFragmento() {
        const ids = ["gridChamadosCliente", "gridChamadoPontosConsumo", "gridImoveisBootstrap"];

        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (ids.includes(alvo)) {
                this.configurarDatatable();
            }
        })
    }

    private configurarListenTipoChamado() {
        $("#chamadoTipo").change(async e => {
            let tipoChamado = $(e.currentTarget).val();
            this.loading.exibir(true);
            let resposta = await this.chamadoAssuntoService.obterAssuntosChamadosEmLote(tipoChamado);
            this.loading.exibir(false);
            this.$chamadoAssunto.empty();
            this.notificador.tratarRespostaHttp(resposta, null, () => {
                this.$chamadoAssunto.append(new Option("", ""));
                resposta.conteudo.forEach(
                    assunto => this.$chamadoAssunto.append(new Option(assunto.descricao, String(assunto.chavePrimaria)))
                );
            });

        })
    }


    private configurarValidacaoForm() {
        $("#chamadoForm").unbind("submit");
        $("#chamadoForm").submit(async e => {
            e.preventDefault();

            if (this.validarCamposObrigatorios()) {
                let pontoConsumos = this.abaImoveisCondominio.obterPontosConsumoSelecionados();
                await this.exibirVerificacaoGarantia(pontoConsumos);
            }

        });

    }

    private validarCamposObrigatorios() {
        let mensagens = [];

        if (!$("#descricao").val()) {
            mensagens.push("O campo descrição deve ser preenchido");
        }

        if (!$("#chamadoTipo").val()) {
            mensagens.push("Selecione um tipo de chamado");
        }

        if (!$("#chamadoAssunto").val()) {
            mensagens.push("Selecione um assunto de chamado");
        }

        if (!$("#unidadeOrganizacional").val()) {
            mensagens.push("Selecione uma unidade organizacional");
        }

        if (!$("#canalAtendimento").val()) {
            mensagens.push("Selecione um canal de atendimento");
        }

        if (mensagens.length) {
            this.mensagem.exibirMensagemErro(mensagens);
        }

        return mensagens.length === 0;
    }

    private async exibirVerificacaoGarantia(pontoConsumos : PontoConsumo[]) {
        let deveSalvar = await this.verificacaoGarantia.exibirVerificacaoGarantia($("#chamadoAssunto").val(), pontoConsumos);

        if (deveSalvar) {

            $("#chamadoForm [name='chavePontoConsumo[]']").remove();
            pontoConsumos.forEach(p => {
                $("#chamadoForm").append($("<input>", {type: "hidden", name: 'chavePontoConsumo[]', value: p.chavePrimaria}));
            });

            $($("[name='imovel']").val($("[name='chaveImovel']:checked").val()));

            const formData = new FormData($("#chamadoForm")[0]);
            this.loading.exibir(true);
            let resultado = await this.http.postFormData<any>(this.propriedades.contextoAplicacao()
                + "/incluirChamadoEmLote", formData);
            this.loading.exibir(false);

            if (resultado.ok) {
                window.location.href = this.propriedades.contextoAplicacao()
                    + "/exibirPesquisaChamadoSucesso"
                    + UrlUtil.gerarQueryString({protocolos : resultado.conteudo.protocolos});
            } else {

                let mensagens;
                try {
                    let {erros} = JSON.parse(resultado.erro);
                    mensagens = erros;
                } catch(e) {
                    mensagens = resultado.erro;
                }

                this.mensagem.exibirMensagemErro(mensagens);
            }

        }

    }

    private configurarLimpar() {
        this.$limparEmLote.click(e => {
            e.preventDefault();
            window.location.href = `${this.propriedades.contextoAplicacao()}/exibirInclusaoChamadoEmLote`;
        });

        $("#btnVoltar").click(e => {
            e.preventDefault();
            window.location.href = `${this.propriedades.contextoAplicacao()}/exibirPesquisaChamado`;
        });
    }
}
