import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {Datatable} from "../../../../../negocio/datatables";
import {LoadingService} from "../../../../../servicos/loading.service";

declare const obterListaServicos: Function;

@provide(IncluirTipoChamadoAssuntoController)
export class IncluirTipoChamadoAssuntoController {

    @inject(LoadingService)
    private loading : LoadingService;

    @postConstruct()
    public init() {
        this.carregarTabelas();
        $("body").on("carregarFragmentoFinished", (e, alvo) => {
            if (["gridComponentesServicoTipo"].includes(alvo)) {
                this.carregarTabelas();
                obterListaServicos();
            }
            if ($('#tableServico tbody tr').length > 0) {
                $("input[name=indicadorClienteObrigatorio]").prop("disabled", true);
                $("input[name=indicadorImovelObrigatorio]").prop("disabled", true);
            } else {
                $("input[name=indicadorClienteObrigatorio]").prop("disabled", false);
                $("input[name=indicadorImovelObrigatorio]").prop("disabled", false);
            }
        });
        $('#chamadoAssuntoForm').on("submit", function () {
            this.carregarTabelas();
        });
    }

    private carregarTabelas() {
        new Datatable("#tableAssunto").inicializar({destroy : true});
        new Datatable("#tableServico").inicializar({destroy : true});
    }
}


