import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import {Datatable} from "../../../../../negocio/datatables";
import {LoadingService} from "../../../../../servicos/loading.service";

@provide(PesquisarTipoChamadoAssuntoController)
export class PesquisarTipoChamadoAssuntoController {

    @inject(LoadingService)
    private loading : LoadingService;

    @postConstruct()
    public init() {

        $("#chamadoAssuntoForm").submit((e) => {
            $(".ui-widget-overlay2").hide();
            $("#indicadorProcessamento").hide();
            this.loading.exibir(true);
        });

        const d = new Datatable('#tableChamadoAssunto');
        d.columnDefs = [
            {"targets": [0], "searchable": false, "orderable": false, "visible": true},
            {"targets": [1], "searchable": false, "orderable": false, "visible": true}
        ];
        d.inicializar();
    }
}


