/*
 * Copyright© set/2017 - Logique Sistemas®.
 * All rights reserved
 */
import {Container} from "inversify";
import "reflect-metadata";
import {buildProviderModule} from "inversify-binding-decorators";

/**
 * Define o container de injeção de dependências do InvesifyJS e provê mecanismos genéricos para injeção
 * automática de dependências
 *
 * @author jose.victor@logiquesistemas.com.br
 */

const container = new Container();
container.load(buildProviderModule());

export { container};

