import {provideSingleton} from "../singleton";

declare let $ : any;

/**
 * Contém os métodos comuns para obtenção das propriedades de configuração do sistema
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provideSingleton(PropriedadesSistemaService)
export class PropriedadesSistemaService {

    /**
     * Obtém o contexto da aplicação, por exemplo: http://localhost:8080/ggas
     */
    public contextoAplicacao() : string {
        return $("#ctx").val();
    }

}
