/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";

/**
 * Service utilitário para exibição de mensagens de erro / sucesso,
 * do padrão antigo do GGAS.
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(MensagemService)
export class MensagemService {

    private $mensagemSpring = $('.mensagensSpring');

    /**
     * Exibe uma mensagem de erro em tela
     * @param {string | string[]} mensagem uma ou mais mensagens de erro a serem exibidas
     * @param {boolean} scrollToMsg se deve subir a tela até as mensagens ou não
     */
    public exibirMensagemErro(mensagem: string | string[], scrollToMsg = true) {

        this.$mensagemSpring.html(
            $("<div>", {class : 'failure notification hideit'}).append(
                $("<p>").append(
                    $("<strong>").html("Erro: "), (mensagem as string[]).join ?
                        (mensagem as string[]).join("<br/>") : mensagem
                )
            )
        ).show().click(() => this.$mensagemSpring.hide());

        scrollToMsg ? $('html, body').animate({
            scrollTop: this.$mensagemSpring.offset().top - 80
        }, 300) : null;
    }

    /**
     * Exibe uma mensagem de sucesso em tela
     * @param mensagem mensagem de sucesso
     * @param {boolean} scrollToMsg se deve subir a tela até as mensagens ou não
     */
    public exibirMensagemSucesso(mensagem : string, scrollToMsg = true) {

        this.$mensagemSpring.html(
            $("<div>", {class : 'mensagemSucesso notification hideit'}).append(
                $("<p>").append(
                    $("<strong>").html("Sucesso: "), mensagem
                )
            )
        ).show().click(() => this.$mensagemSpring.hide());

        scrollToMsg ? $('html, body').animate({
            scrollTop: this.$mensagemSpring.offset().top - 80
        }, 300) : null;
    }

}
