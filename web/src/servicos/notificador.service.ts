/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import swal from "sweetalert2";
import {RespostaHttp} from "../dominio/dto/RespostaHttp";
import {AnaliseMedicaoController} from "../modulos/faturamento/analiseMedicao/AnaliseMedicaoController";
declare let $ : any;

/**
 * Service de notificações do GGAS
 * Injete este service na sua classe utilizando a anotação @inject
 *
 * Exemplos de Uso: {@link AnaliseMedicaoController}
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(NotificadorService)
export class NotificadorService {

    /**
     * Exibe uma mensagem de sucesso na tela
     * @param {string} titulo da mensagem
     * @param {string} mensagem detalhamento da mensagem
     * @returns {Promise<any>} promise
     */
    exibirMensagemSucesso(titulo : string, mensagem : string) : Promise<any> {
        return swal({
            title: titulo,
            html: mensagem,
            type: "success"
        });
    }

    /**
     * Exibe uma mensagem de erro em tela
     * @param {string} titulo titulo da mensagem de erro
     * @param {string} mensagem detalhamento da mensagem de erro
     * @returns {Promise<any>}
     */
    exibirMensagemErro(titulo : string, mensagem : string) : Promise<any> {
        return swal({
            title: titulo,
            html: mensagem,
            type: "error"
        });
    }

    /**
     * Exibe uma mensagem de atenção em tela
     * @param {string} titulo título da mensagem de atenção
     * @param {string} mensagem detalhamento da mensagem
     * @returns {Promise<any>}
     */
    exibirMensagemAtencao(titulo : string, mensagem : string) : Promise<any> {
        return swal({
            title: titulo,
            html: mensagem,
            type: "warning"
        });
    }

    /**
     * Exibe uma mensagem informativa na tela
     * @param {string} titulo titulo da mensagem
     * @param {string} mensagem detalhamento da mensagem
     * @returns {Promise<any>}
     */
    exibirMensagemInfo(titulo : string, mensagem : string) : Promise<any> {
        return swal({
            title: titulo,
            html: mensagem,
            type: "info"
        });
    }

    /**
     * Exibe um alerta de pergunta se o usuário deve realizar um procedimento
     * @param {string} titulo titulo da mensagem
     * @param {string} mensagem detalhamento da mensagem de texto
     * @param {{confirmButtonText: string; cancelButtonText: string}} opcoes opcoes dos botões do modal. Deixe undefined para default
     * @returns {Promise<any>}
     */
    exibirPergunta(titulo : string, mensagem : string,
                   opcoes = { confirmButtonText : "Sim", cancelButtonText : "Cancelar"}) : Promise<any> {

        return swal({
            title: titulo,
            html: mensagem,
            type: 'question',
            showCancelButton: true,
            ...opcoes
        })
    }

    /**
     * Trata a {@link RespostaHttp} retornado por services da aplicação
     * Exibe uma mensagem de sucesso caso tenha executado o procedimento OK, somente se o
     * atributo mensagemSucesso for definido
     *
     * @param {RespostaHttp<any>} resposta resposta retornada pelo http
     * @param {string} mensagemSucesso mensagem de sucesso caso necessária. (Se não definida, nenhuma mensagem é exibida)
     * @param {(r) => any} casoSucessoFaca callback que será executado se a resposta for sucesso (código 200)
     * @param {(r) => any} casoErroFaca callback que será invocado caso a resposta seja erro (código diferente de 200)
     * @returns {Promise<void>}
     */
    async tratarRespostaHttp(resposta : RespostaHttp<any>, mensagemSucesso : string = null,
                         casoSucessoFaca = (r : RespostaHttp<any>) => {}, casoErroFaca = (r : RespostaHttp<any>) => {}) {
        if (resposta.ok) {
            mensagemSucesso ? await this.exibirMensagemSucesso("Sucesso!", mensagemSucesso) : null;
            casoSucessoFaca(resposta);
        } else {
            console.error("Falha ao realizar operação", resposta, resposta.erro);
            await this.exibirMensagemErro("Falha!", "Houve alguma falha ao realizar a operação, tente novamente ou se o problema persistir contate o nosso suporte");
            casoErroFaca(resposta);
        }
    }

}
