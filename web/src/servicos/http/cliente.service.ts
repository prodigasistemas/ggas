/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {HttpService} from "./http.service";
import {RespostaHttp} from "../../dominio/dto/RespostaHttp";
import {Cliente} from "../../dominio/Cliente";
import {PesquisaCliente} from "../../dominio/dto/PesquisaCliente";
import {inject} from "inversify";
import {PropriedadesSistemaService} from "../propriedades.sistema.service";
import {provide} from "inversify-binding-decorators";
import {ClienteVOAutoComplete} from "../../dominio/dto/ClienteVOAutoComplete";
import {UrlUtil} from "../../negocio/util/UrlUtil";

/**
 * Encapsula lógica de requisições REST ao BackEnd relacionadas a entidade Cliente
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(ClienteService)
export class ClienteService {

    protected url;

    constructor(@inject(HttpService) private httpService : HttpService,
                @inject(PropriedadesSistemaService) private propriedades : PropriedadesSistemaService) {
        this.url = `${this.propriedades.contextoAplicacao()}/rest/cliente`
    }

    /**
     * Pesquisa uma lista de clientes a partir de informações de pesquisa
     * @param {PesquisaCliente} pesquisa
     * @returns {Promise<RespostaHttp<Cliente[]>>}
     */
    async pesquisarCliente(pesquisa : PesquisaCliente) : Promise<RespostaHttp<Cliente[]>> {
        let urlBuscar = `${this.url}/buscar`;
        console.debug("Requisitando", urlBuscar, "com parâmetros:", pesquisa);
        return await this.httpService.post<Cliente[]>(urlBuscar, pesquisa);

    }

    /**
     * Busca o cliente e suas informações de contrato pelo nome (url : /carregaNomesClientes)
     */
    async pesquisarClienteContratoPorNome(nome ?: string, nomeFantasia ?: string) : Promise<RespostaHttp<ClienteVOAutoComplete[]>> {
        let url = `carregaNomesClientes${UrlUtil.gerarQueryString({nome, nomeFantasia})}`;
        console.debug("Requisitando", url, "com nome:", nome);
        return await this.httpService.get<ClienteVOAutoComplete[]>(url);
    }

}
