import {RespostaHttp} from "../../dominio/dto/RespostaHttp";
import {provide} from "inversify-binding-decorators";

/**
 * Classe responsável por encapsular a lógica de requisições e tratamento de erros via AJAX de forma genérica
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(HttpService)
export class HttpService {

    private headers : Headers = new Headers({
        'Content-Type': 'application/json',
    });

    /**
     * Realiza uma requisição post para a url informada passando o parâmetro no corpo da requisição.
     * Exemplo de chamada:
     *
     * let resposta = await service.post("/rest/pesquisarCliente", { nome : "João" });
     *
     *
     * @param {string} url
     * @param parametros objeto javascript que será enviado
     * @returns {Promise<RespostaHttp<T>>}
     */
    async post<T>(url: string, parametros ?: any) : Promise<RespostaHttp<T>> {
        let resposta = await fetch(url, {
            method: "POST",
            headers: this.headers,
            credentials: "include",
            body: JSON.stringify(parametros)
        });

        return this.tratarResposta<T>(resposta);
    }

    /**
     * Realiza uma requisição post para a url informada passando os parâmetros com form data no corpo da requisição.
     * Pode ser utilizado para enviar arquivos.
     *
     * Exemplo de chamada:
     *
     * let resposta = await service.postFormData("/rest/pesquisarCliente", { nome : "João" });
     *
     *
     * @param {string} url
     * @param data informação que será enviada
     * @returns {Promise<RespostaHttp<T>>}
     */
    async postFormData<T>(url: string, data : object | FormData = {}) : Promise<RespostaHttp<T>> {

        let formData;
        if (data instanceof FormData) {
            formData = data;
        } else {
            formData = new FormData();
            Object.entries(data).forEach(entry => {
                formData.append(entry[0], entry[1]);
            });
        }

        let resposta = await fetch(url, {
            method: "POST",
            credentials: "include",
            body: formData
        });

        return this.tratarResposta<T>(resposta);
    }

    /**
     * Realiza uma requisição get para a url informada. Considera-se que os parâmetros de requisição já
     * estão informados na url
     * @param {string} url
     * @returns {Promise<RespostaHttp<T>>}
     */
    async get<T>(url: string) : Promise<RespostaHttp<T>> {
        let response = await fetch(url, {
            method: "GET",
            headers: this.headers,
            credentials: "include"
        });
        return this.tratarResposta<T>(response);
    }

    /**
     * Realiza uma requisição do tipo DELETE para a utl informada.
     * @param {string} url
     * @returns {Promise<RespostaHttp<T>>}
     */
    async delete<T>(url : string) : Promise<RespostaHttp<T>> {
        let response = await fetch(url, {
            method: "DELETE",
            headers: this.headers,
            credentials: "include"
        });
        return this.tratarResposta<T>(response);
    }

    /**
     * Trata internamente a resposta da requisição construindo uma RespostaHttp ao final do processo
     * @param {Response} response resposta do Fetch
     * @returns {Promise<RespostaHttp<T>>} retorna uma promise de uma resposta http
     */
    private async tratarResposta<T>(response : Response) : Promise<RespostaHttp<T>> {
        let obj : Promise<any>;

        if (response.ok) {
            obj = response.text().then((text) => {
                let mensagem = new RespostaHttp();
                try {
                    mensagem.conteudo = JSON.parse(text);
                } catch(error) {
                    mensagem.conteudo = text;
                    mensagem.codigo = response.status;
                }

                return mensagem;
            });

        } else {
            obj = this.tratarErro(response);
        }

        return obj;
    }

    /**
     * Método invocado para tratamento de erros da requisição
     * @param {Response} respostaErro
     * @returns {Promise<RespostaHttp<any>>}
     */
    private async tratarErro(respostaErro: Response) {
        let error = new RespostaHttp();
        error.ok = false;

        if (respostaErro == null) {
            console.error('A conexão com o servidor foi perdida');
            error.codigo = 0;
            error.erro = "A conexão com o servidor foi perdida";

        } else {
            error.codigo = respostaErro.status;
            error.erro = await respostaErro.text().then(t => {
                console.log(t);
                return t ? t.replace(new RegExp("\\\\u0000", 'g'), '') : undefined;
            });

            if (respostaErro.status == 400) {
                console.error("Bad Request", respostaErro, error);
            } else if (respostaErro.status == 401) {
                console.error('Sem Autenticação ', respostaErro, error);
            } else if (respostaErro.status == 403) {
                console.error('Acesso Negado', respostaErro, error);
            } else if (respostaErro.status == 404) {
                console.error('Recurso Não encontrado: ', respostaErro, error);
            } else if (respostaErro.status == 500) {
                console.error('Erro interno no servidor', respostaErro, error);
            } else if (respostaErro.status == 0) {
                console.error("A conexão com o servidor foi perdida", respostaErro, error);
            } else {
                console.error("Erro desconhecido: ", respostaErro, error);
            }

        }
        return Promise.resolve(error);
    }

}
