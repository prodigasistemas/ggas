/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject} from "inversify";
import {HttpService} from "./http.service";
import {PropriedadesSistemaService} from "../propriedades.sistema.service";
import {RespostaHttp} from "../../dominio/dto/RespostaHttp";
import {DadosMedicao} from "../../modulos/faturamento/analiseMedicao/modalAlterarDadosMedicao";
import {UrlUtil} from "../../negocio/util/UrlUtil";
import { AnaliseLeitura } from "../../modulos/faturamento/analiseMedicao/modalDetalhamentoLeitura";

/**
 * Service da leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@provide(LeituraMovimentoService)
export class LeituraMovimentoService {

    @inject(HttpService)
    private httpService : HttpService;

    @inject(PropriedadesSistemaService)
    private propriedades : PropriedadesSistemaService;

    /**
     * Altera os dados de medição de leituras movimento
     * @param grupoFaturamento chave do grupo de faturamento
     * @param {DadosMedicao[]} novosDadosMedicao
     * @returns {Promise<RespostaHttp<string>>}
     */
    async alterarDadosMedicao(grupoFaturamento : number, novosDadosMedicao : DadosMedicao[]) : Promise<RespostaHttp<string>> {
        console.log("Alterando valor das leituras", novosDadosMedicao);
        let dadosEnviados = novosDadosMedicao.map(m => ({
            chaveLeitura : m.chaveLeitura,
            novaLeitura: m.valorLeitura,
            pressao: m.pressao,
            temperatura: m.temperatura,
            observacaoLeitura: m.observacaoLeitura,
            dataLeitura: m.dataLeitura,
            chaveAnormalidade : m.anormalidade ? m.anormalidade.chavePrimaria : undefined
        }));
        let url = `${this.propriedades.contextoAplicacao()}/analiseMedicao/alterarDadosMedicao/${grupoFaturamento}`;
        return await this.httpService.post<string>(url, dadosEnviados);
    }

    async solicitarReleitura(leituras : number[]) {
        let url = `${this.propriedades.contextoAplicacao()}/analiseMedicao/solicitarReleitura`;
        return await this.httpService.post<string>(url, leituras);
    }

    /**
     * Obtém o período de busca de leituras movimentos
     * @param {number} rota chave da rota
     * @param {number} grupoFaturamento chave do grupo de faturamento
     * @returns {Promise<RespostaHttp<string>>} retorna o range no formato 'inicio - fim'
     */
    async obterPeriodoBusca(rota : number, grupoFaturamento : number) : Promise<RespostaHttp<string>> {
        let queryString = UrlUtil.gerarQueryString({rota, grupoFaturamento});
        let url = `${this.propriedades.contextoAplicacao()}/analiseMedicao/obterPeriodoBusca${queryString}`;
        return await this.httpService.get<string>(url);
    }
    
   async salvarExecutarConsistir(analiseMedicao : AnaliseLeitura) : Promise<RespostaHttp<string>> {
        let url = `${this.propriedades.contextoAplicacao()}/analiseMedicao/consistirLeitura`;
        
        return await this.httpService.post<string>(url, analiseMedicao);
    }
    
}
