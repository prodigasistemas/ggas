import "bootstrap";
import "./index.scss";
import "./sidebar";
import "./topbar";

$(() => {
    $('.nav-tabs .nav-item').click(() => {
        /**
         * Quando os datatable não estão visíveis na tela, a responsividade não funciona porque o datatable não sabe
         * a resolução correta em que ele está inserido, então quando ele realmente ficar visível, é necessário que o
         * datatables seja redesenhado para ajustar as colunas (ocultar ou não), isso foi encontrado principalmente
         * nos componentes em abas (nav-tabs), então é necessário registrar no evento de clique o redesenho.
         */
        setTimeout(() => {
            $(".dataTable").DataTable().columns.adjust().draw();
        }, 1000);
    });
});