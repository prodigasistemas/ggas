
declare let $ : any;


$(() => {

    const duration = 200, smallDeviceWidth = 768;
    let $toggleMenuSmallDevices = $("a.left-sidebar-toggle")
    let $leftSpacer = $(".left-sidebar-spacer");

    $toggleMenuSmallDevices.click((e) => {


        if ($(e.currentTarget).is(".open")) {
            $leftSpacer.slideUp({
                duration,
                complete : () => $leftSpacer.removeAttr("style")
            });

        } else {
            $leftSpacer.slideDown({duration});
        }
        $(e.currentTarget).toggleClass("open");
    });

    window.onresize = (e : any) => {
        if (e.target.outerWidth > smallDeviceWidth && $toggleMenuSmallDevices.is(".open")) {
            $leftSpacer.slideUp({
                duration : 0, complete : () => $leftSpacer.removeAttr("style")
            });
            $toggleMenuSmallDevices.toggleClass("open");
        }
    }

    $(".sidebar-elements li.parent").click(e => {
        let $submenu = $(e.currentTarget).find("> ul.sub-menu");
        if ($(e.currentTarget).is(".open")) {
            $submenu.slideUp({
                duration,
                complete : () => $submenu.removeAttr("style")
            });
        } else {
            $submenu.slideDown({
                duration
            });
        }

        $(e.currentTarget).toggleClass("open");
        e.stopPropagation();
    });

    const mapeamentoIconesMenuLateral = {
        "Arrecadação" : "fa-piggy-bank",
        "Atendimento ao Público" : "fa-headset",
        "Cadastro" : "fa-pencil-alt",
        "Cobrança" : "fa-money-bill",
        "Configuração" : "fa-cogs",
        "Contabilidade" : "fa-dollar-sign",
        "Contrato" : "fa-file-signature",
        "Faturamento" : "fa-hand-holding-usd",
        "Integração" : "fa-plug",
        "Medição" : "fa-tachometer-alt",
        "Relatórios" : "fa-file-invoice",
        "Segurança" : "fa-lock",
    }

    $("ul.sidebar-elements > li > a").each((index, el) => {
        let span = $(el).find("> span")[0];
        if (span) {
            let icone = mapeamentoIconesMenuLateral[$(span).text().trim()];
            if (icone) {
                $(el).find("i").addClass(icone);
            }
        }
    });

    $("#inputPesquisar").on("input", (e) => {
        let texto = e.target.value;
        let pesquisa = [];
        if (isFiltroComAspas(texto)) {
            pesquisa = [replaceAspas(texto.toLowerCase())];
        } else {
            pesquisa = texto.toLowerCase().split(" ").filter(s => s);
        }

        if (pesquisa.length == 0) {
            $(".sidebar-elements li").toArray().forEach(elemento => $(elemento).removeClass("open destaque"));
            $(".sidebar-elements > li").toArray().forEach(elemento => $(elemento).show());
        } else {
            $(".sidebar-elements > li").toArray().forEach((elemento) => {
                let devePermanecer = filtrarItem(pesquisa, elemento);
                if (devePermanecer) {
                    $(elemento).show();
                } else {
                    $(elemento).hide();
                }
            });
        }
    });

    let isFiltroComAspas = (texto) => {
        let filtradoComAspasDuplas = texto.indexOf('"') >= 0 && texto.lastIndexOf('"') > 0;
        let filtradoComAspasSimples = texto.indexOf("'") >= 0 && texto.lastIndexOf("'") > 0;
        return filtradoComAspasDuplas || filtradoComAspasSimples;
    };

    let replaceAspas = (texto) => {
        return texto.replace(new RegExp('"', 'g'), '').replace(new RegExp("'", 'g'), '');
    };

    let filtrarItem = (pesquisa : string[], elemento : HTMLElement) : boolean => {

        let menu = $(elemento).find("> a > span").text().toLowerCase();
        let devePermanecer = pesquisa.length == 0 || pesquisa.some(p => menu.indexOf(p) > -1);
        $(elemento).removeClass("open destaque");

        if (devePermanecer) {
            $(elemento).addClass("destaque");
        }

        if ($(elemento).is(".parent")) {
            let algumFilhoAtendeCondicao = $(elemento).find("> ul.sub-menu > li")
                .toArray()
                .map(el => filtrarItem(pesquisa, el))
                .reduce((o1, o2) => o1 || o2);

            if (algumFilhoAtendeCondicao) {
                $(elemento).addClass("open");
            }

            devePermanecer = devePermanecer || algumFilhoAtendeCondicao;
        }

        return devePermanecer;
    }

    //Remove itens do menu que estão vazios pelo fato do usuário não possuir permissão
    $(".sidebar-elements li.parent").toArray().reverse().forEach(value => {

        if ($(value).find("> ul > li").length == 0){
            $(value).remove();
        }
    });
    $(".sidebar-elements").toggleClass("hide", false);

});
