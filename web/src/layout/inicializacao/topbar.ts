declare let  exibirJDialog, AjaxService : any;
import PerfectScrollbar from "perfect-scrollbar";

$(() => {

    $(".info_sistema").click(() => {
        exibirJDialog("#infoSistemaPopup");
    })

    $(".alterar_senha").click(() => {
        exibirJDialog("#alterarSenhaPopup");
    })

    $(".sair_sistema").click(() => {
        exibirJDialog("#confirmacaoLogout");
    })

    //Inicializa o scrollbar do menu lateral esquerdo
    let scrollBarPrincipal = new PerfectScrollbar(".left-sidebar-scroll");

    if ($(".favoritos .be-scroller").length) {
        new PerfectScrollbar(".favoritos .be-scroller", {
            wheelPropagation: false
        });
    }

    $(".addFavorito").click((e) => {
        e.stopPropagation();
        let chavePrimariaMenu = Number($(e.currentTarget).attr("data-chavePrimaria"));
        AjaxService.adicionarFavorito(chavePrimariaMenu, {async: false});
        location.reload();

    });

    $(".removerFavorito").click((e) => {
        e.stopPropagation();
        let chavePrimaria = Number($(e.currentTarget).attr("data-chavePrimaria"));
        AjaxService.removerFavorito(chavePrimaria, {async: false});
        $(e.currentTarget).parent().remove();
    })

});