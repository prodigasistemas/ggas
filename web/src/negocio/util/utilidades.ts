declare const $: any;

export class Utilidades {
    public static ocultarLoading(elemento_para_exibir = null) {
        $('.loading').hide();

        if (elemento_para_exibir) {
            $(elemento_para_exibir).css({'opacity': 1, 'display': 'table'});

            setTimeout(() => {
                $(elemento_para_exibir).DataTable().columns.adjust().draw();
            }, 1000);

        }
    }
}