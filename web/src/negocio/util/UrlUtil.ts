/**
 * Mantém métodos utilitários de acesso e manipulação de URL
 *
 * @author jose.victor@logiquesistemas.com.br
 */
export class UrlUtil {

    /**
     * Extrai uma parâmetro de uma dada url, por exemplo:
     * url : 'localhost/pesquisar?param1=olamundo'
     * nomeParametro: 'param1'
     * retorno : 'olamundo'
     *
     * @param search url com querty string a ser investigada ex.: '?oi=olamundo'
     * @param nomeParametro nome do parâmetro cujo valor será extraído
     */
    public static getParametro(search : string, nomeParametro : string) : string {
        return new URLSearchParams(search).get(nomeParametro);
    }

    /**
     * Gera a query string a partir de um objeto javascript. Exemplo:
     * data : {param1 : 'olá mundo', param2: 'teste'}
     * retorno : ?param1=olá%20mundo&param2=teste
     *
     * @param data objeto cujas chaves serão mapeadas
     */
    public static gerarQueryString(data : object, url = "") {
        let queryString = url;

        Object.keys(data).filter(chave => data[chave]).forEach(chave => {
            queryString = this.atualizarQueryString(queryString, chave, data[chave]);
        });

        return queryString;
    }

    /**
     * Atualiza a URL do browser com query strings baseando-se nos dados do objeto enviado.
     * A atualização da URL não causará recarregamento da página.
     *
     * Exemplo de uso:
     *      url atual: http://localhost/ggas
     *      parametro data : {param1 : 'olá mundo', param2: 'teste'}
     *
     *      Retorno : http://localhost/ggas?param1=olá%20mundo&param2=teste     *
     *
     * @param {object} data data com os parâmetros da query string
     */
    public static atualizarUrl(data : object) {
        let novaUrl = this.gerarQueryString(data, window.location.href.split("?")[0]);
        window.history.replaceState(null, null, novaUrl);
    }

    /**
     * Atualiza um parâmetro em uma query string. Exemplo:
     *
     * queryString: '?param1=olamundo&param2=teste'
     * chave: 'param1'
     * valor: 'valormodificado'
     * resultado: '?param1=valormodificado&param2=teste'
     *
     * @param queryString
     * @param chave
     * @param valor
     */
    private static atualizarQueryString(queryString, chave, valor) : string {
        let url = "";
        let re = new RegExp(`([?&])${chave}=.*?(&|$)`, "i");
        let separator = queryString.indexOf('?') !== -1 ? "&" : "?";
        if (queryString.match(re)) {
            url =  queryString.replace(re, `$1${chave}=${valor}$2`);
        }
        else {
            url = queryString + separator + chave + "=" + valor;
        }
        return url;
    }
}
