/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {Utilidades} from "./util/utilidades";
import * as moment from 'moment-mini-ts';

/**
 * Created by Italo on 10/07/2017.
 */
declare const $: any;

export const DataTableLanguageConfigs = {
    "lengthMenu": "_MENU_ registros por página",
    "zeroRecords": "Nenhum registro foi encontrado",
    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
    "infoEmpty": "0 registros",
    "infoFiltered": "(Filtrado de _MAX_ registros totais)",
    "search": "Procurar:",
    "processing": "Processando...",
    "paginate": {
        "previous": "Anterior",
        "next": "Próximo"
    }
}

export class Datatable {

    tabela;
    paginado = true;
    filtro = false;
    datatablePronto = false;
    indiceOrderBy = 0;
    orderBy = "desc";
    searching = true;
    legendaHTML = '';
    columnDefs = [];

    constructor(private id_datatable: string,
                private legendas?: any[],
                private lengthMenu: any = [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]]) {
        this.registrarTypeOrdenadorDateTime();
    }

    inicializar(cfgs = {}, ocultarLoading = true, callback = null) {
        setTimeout(() => {
            $(() => {

                const table = $(this.id_datatable).DataTable($.extend({
                    "responsive": true,
                    "paginate": this.paginado,
                    "order": [],
                    "lengthMenu": this.lengthMenu,
                    "searching": this.searching,
                    "processing": true,
                    "serverSide": false,
                    "deferRender": false,
                    "columnDefs": this.columnDefs,
                    "language": DataTableLanguageConfigs,
                    'dom': '"<\'row\'<\'col-sm-12 col-md-4\'l><\'col-sm-12 col-md-8\'f>>" +\n' +
                        '"<\'row\'<\'col-sm-12\'tr>>" +\n' +
                        '"<\'row\'<\'col-sm-12 col-md-4\'i><\'col-sm-12 col-md-8\'p>>",',
                    initComplete: () => {
                        this.legendaHTML = '';
                        if (this.legendas) {
                            this.legendaHTML += '<div class="row">';
                            this.legendaHTML += '<div class="col-md-2" style="margin-right: 18px;"><b>&nbsp;Legenda: &nbsp;</b></div>';

                            this.legendas.forEach((item) => {
                                if (item.cor) {
                                    this.legendaHTML += ('<div class="item" style="color: ' + item.cor + '"><i class="fa ' + item.icone + '"></i><br />' + item.nome + '</div>');
                                } else {
                                    this.legendaHTML += ('<div class="item"><i class="fa ' + item.icone + '"></i><br />' + item.nome + '</div>');
                                }

                            });

                            this.legendaHTML += '</div>';
                        }
                        $('.legenda').html(this.legendaHTML);
                    },

                }, cfgs));

                if (this.filtro) {
                    $(this.id_datatable + ' tfoot th').each(function () {
                        const title = $(this).text();
                        if (title !== 'Detalhes') {
                            $(this).html('<input type="text" class="form-control" style="width: 89%" placeholder="Pesquisar por ' + title + '" />');
                        }
                    });
                    $(this.id_datatable).DataTable().columns().every(function () {
                        const that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                }

                this.datatablePronto = true;

                if (ocultarLoading) {
                    Utilidades.ocultarLoading(this.id_datatable);
                }

                if (callback !== null) {
                    callback();
                }

                this.tabela = table;
            });
        }, 0);
    }

    registrarTypeOrdenadorDateTime() {
        $.extend($.fn.dataTableExt.oSort, {
            "date-time-pre": function (datetime) {
                let data = $(datetime).text().trim();
                if (data.indexOf(':') === 10) {
                    data = data.replace(':', '');
                }
                return moment(data, 'DD/MM/YYYY HH:mm').format('x');
            },

            "date-time-asc": function (a, b) {
                return a - b;
            },

            "date-time-desc": function (a, b) {
                return b - a;
            },
        });

        $.extend($.fn.dataTableExt.oSort, {
            "date-pre": function (datetime) {
                let data = $(datetime).text().trim();
                if (data.indexOf(':') === 10) {
                    data = data.replace(':', '');
                }
                return moment(data, 'DD/MM/YYYY').format('x');
            },

            "date-asc": function (a, b) {
                return a - b;
            },

            "date-desc": function (a, b) {
                return b - a;
            }
        });

        $.extend($.fn.dataTableExt.oSort, {
            "text-locale-pre": function (texto) {
                return texto.toLocaleLowerCase();
            },

            "text-locale-asc": function (a, b) {
                return a.localeCompare(b, 'br', {ignorePunctuation: true, sensitivity: 'base'});
            },

            "text-locale-desc": function (a, b) {
                return b.localeCompare(a, 'br', {ignorePunctuation: true, sensitivity: 'base'});
            },
        });
    }

    filter(column, value) {
        $(this.id_datatable).DataTable().column(column).data().search(value).draw();
    }


    reload() {
        $(this.id_datatable).DataTable().clear().draw();
    }

    destroy() {
        this.datatablePronto = false;
        $(() => {
            $(this.id_datatable).dataTable().fnDestroy();
        });
    }

    destroyOnly() {
        this.datatablePronto = true;
        $(() => {
            $(this.id_datatable).dataTable().fnDestroy();
        });
    }

    isInicializado() {
        return $.fn.dataTable.isDataTable(this.id_datatable);
    }

    reiniciar() {
        $(this.id_datatable).dataTable().fnDestroy();
        this.inicializar();
    }
}
