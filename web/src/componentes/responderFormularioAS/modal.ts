/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

import {provide} from "inversify-binding-decorators";
import {inject, postConstruct} from "inversify";
import * as Ladda from "ladda";
import {LoadingService} from "../../servicos/loading.service";

import "ladda/dist/ladda-themeless.min.css"
import {AutorizacaoServicoService} from "../../servicos/http/autorizacaoServico.service";

/**
 * Classe responsável por gerenciar o controle do modal do formulário de resposta de autorização de serviço
 *
 * Para utilizar esta classe, injete na sua classe e invoque o método setCallback para configurar o modal para
 * informar quando o clente for selecionado
 *
 *
 * @author José Victor - jose.victor@logiquesistemas.com.br
 */
@provide(ModalResponderFormulario)
export class ModalResponderFormulario {

    private $modal;
    private callBackOnResponder;
    private btnPesquisarLadda;

    @inject(LoadingService)
    private loadingService : LoadingService;

    @inject(AutorizacaoServicoService)
    private autorizacaoServicoSvc : AutorizacaoServicoService;

    @postConstruct()
    public onInit() {
        this.$modal = $("#modalResponderFormulario");
        this.btnPesquisarLadda = Ladda.create(this.$modal.find("[name='modalResponderFormulario_pesquisar']")[0]);
        $("#modalResponderFormulario_form").unbind();
        $("#modalResponderFormulario_form").submit(async (e) => {
            e.preventDefault();
            await this.submeterFormulario();
        })
    }

    public exibir() : void {
        this.$modal.find(".alert-danger").toggleClass("hide", true);
        this.$modal.modal();
    }

    public setCallback(callback : Function) {
        this.callBackOnResponder = callback;
    }

    private async submeterFormulario() {
        this.$modal.find(".alert-danger").toggleClass("hide", true);
        let data = {};
        $("#modalResponderFormulario_form").serializeArray().forEach((obj : {name : string, value : string}) => {
            data[obj.name] = obj.value;
        });

        this.btnPesquisarLadda.start();
        this.loadingService.exibir(true);
        let retorno = await this.autorizacaoServicoSvc.responderFormulario(data);
        this.btnPesquisarLadda.stop();
        this.loadingService.exibir(false);

        if (retorno.ok) {
            this.$modal.modal("hide");
            this.callBackOnResponder ? this.callBackOnResponder() : null;
        } else {
            this.$modal.find(".alert-danger").toggleClass("hide", false);
            this.$modal.find(".alert-danger span").text(retorno.erro);
            this.$modal.animate({scrollTop: 0}, "slow");
        }

    }

}
