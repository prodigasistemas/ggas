import {ClienteService} from "../../servicos/http/cliente.service";
import {PesquisaCliente} from "../../dominio/dto/PesquisaCliente";
import "ladda/dist/ladda-themeless.min.css"
import * as Ladda from 'ladda';
import {Datatable} from "../../negocio/datatables";
import {Cliente} from "../../dominio/Cliente";
import {inject, postConstruct} from "inversify";
import {provide} from "inversify-binding-decorators";

declare let $ : any;

/**
 * Classe responsável por gerenciar o controle do modal modalPesquisarCliente.jsp
 *
 * Para utilizar esta classe, injete na sua classe e invoque o método setCallback para configurar o modal para
 * informar quando o clente for selecionado
 *
 *
 * @author José Victor - jose.victor@logiquesistemas.com.br
 */
@provide(ModalPesquisarCliente)
export class ModalPesquisarCliente {

    @inject(ClienteService)
    private service : ClienteService;

    private $componente;
    private $nome;
    private $nomeFantasia;
    private $cpf;
    private $cnpj;
    private $numeroPassaporte;
    private $inscricaoEstadual;
    private $inscricaoMunicipal;
    private $inscricaoRural;
    private $cep;
    private datatable : Datatable;
    private $tabela;
    private $form;
    private callBackOnSelecao : Function;


    @postConstruct()
    public onInit() {
        this.$componente = $("#modalPesquisarCliente");
        this.$nome = this.$componente.find('#modalPesquisarCliente_nome');
        this.$nomeFantasia = this.$componente.find('#modalPesquisarCliente_nomeFantasia');
        this.$cnpj = this.$componente.find('#modalPesquisarCliente_cnpj');
        this.$cpf = this.$componente.find('#modalPesquisarCliente_cpf');
        this.$numeroPassaporte = this.$componente.find('#modalPesquisarCliente_passaporte');
        this.$inscricaoEstadual = this.$componente.find('#modalPesquisarCliente_inscricaoEstadual');
        this.$inscricaoMunicipal = this.$componente.find('#modalPesquisarCliente_inscricaoMunicipal');
        this.$inscricaoRural = this.$componente.find('#modalPesquisarCliente_inscricaoRural');
        this.$cep = this.$componente.find('#cepPessoa');
        this.$tabela = $("#modalPesquisarCliente_tabelaPessoa");
        this.datatable = new Datatable("#modalPesquisarCliente_tabelaPessoa");
        this.$form = $("#modalPesquisarCliente_form");

        this.configurarSubmissao();

        this.$cpf.inputmask("999.999.999-99",{placeholder:"_"});
        this.$cnpj.inputmask("99.999.999/9999-99",{placeholder:"_"});
    }

    /**
     * Exibe o modal do pesquisar cliente
     */
    public exibir() : void {
        $(this.$componente).modal();

        $("#modalPesquisarCliente_containerTabela").addClass("hide");
        this.$nome.val("");
        this.$nomeFantasia.val("");
        this.$cpf.val("");
        this.$cnpj.val("");
        this.$numeroPassaporte.val("");
        this.$inscricaoEstadual.val("");
        this.$inscricaoMunicipal.val("");
        this.$inscricaoRural.val("");
        this.$cep.val("");
    }

    public setCallback(callback : Function) {
        this.callBackOnSelecao = callback;
    }

    private configurarSubmissao() {
        let $btnPesquisar = this.$componente.find("[name='modalPesquisarCliente_pesquisar']");
        let ladda = Ladda.create($btnPesquisar[0]);
        this.$form.unbind();
        this.$form.submit(async (e) => {
            e.preventDefault();

            $("#modalPesquisarCliente_containerTabela").addClass("hide");
            ladda.start();
            await this.pesquisarCliente();
            ladda.stop();
            $("#modalPesquisarCliente_containerTabela").removeClass("hide");

            return false;
        });
    }



    private async pesquisarCliente() {
        let pesquisa = new PesquisaCliente(this.$nome.val(), this.$nomeFantasia.val(), this.$cpf.val(), this.$cnpj.val(),
            this.$numeroPassaporte.val(), this.$inscricaoEstadual.val(), this.$inscricaoMunicipal.val(),
            this.$inscricaoRural.val(), this.$cep.val());

        let resposta = await this.service.pesquisarCliente(pesquisa);

        this.recarregarDatatable(resposta.conteudo);
    }

    private recarregarDatatable(clientes : Cliente[]) {
        if (this.datatable.isInicializado()) {
            this.datatable.destroy();
        }

        const render = (data) => $("<div>").append(
            $("<a>").attr("title", "Clique para selecionar").attr("href", "javascript:void(0)").html(data)
        ).html();

        this.datatable.inicializar({
            data: clientes,
            columns: [
                { data: null, render : (data : Cliente) => render(data.nome) },
                { data: null, render : (data : Cliente) => render(data.nomeFantasia) },
            ],
            searching: false,
            paginate: true,
            initComplete: () => {
                this.$tabela.find("a").click((e) => {
                    let table = this.$tabela.DataTable();
                    this.selecionarCliente(table.row($(e.currentTarget).parents("tr")).data());
                });
            }
        });
    }

    private selecionarCliente(cliente : Cliente) {
        $(this.$componente).modal("hide");
        if (this.callBackOnSelecao) {
            this.callBackOnSelecao(cliente);
        }
    }

}
