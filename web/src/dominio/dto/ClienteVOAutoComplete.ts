/**
 * DTO Réplica da classe de mesmo nome no Java
 *
 * @author jose.victor@logiquesistemas.com.br
 */
export class ClienteVOAutoComplete {

    constructor(
        public chavePrimaria : number,
        public idContrato : number,
        public nome : string,
        public cpfCnpj : string,
        public endereco : string,
        public nomeImovel : string,
        public numeroContrato : string,
        public inicioVigenciaContrato : string,
        public fimVigenciaContrato : string,
        public nomeFantasia : string) {}

}