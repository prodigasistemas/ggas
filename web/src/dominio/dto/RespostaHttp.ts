/**
 * Classe responsável por encapsular a resposta de uma requisição HTTP do backend
 * @author jose.victor@logiquesistemas.com.br
 */
export class RespostaHttp<T> {
    ok : boolean = true; // Informa se a requisição foi realizada com sucesso ou não (qualquer código diferente de 200, retornará false)
    codigo : number = 200; // Código HTTP de retorno da requisição
    conteudo : T; // Conteúdo da resposta http
    erro : string = ""; // Caso ocorra algum erro durante a requisição este campo poderá estar preenchido informando o motivo do erro
}
