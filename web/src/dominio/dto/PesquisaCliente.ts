/**
 * Classe que contém as informações de pesquisa do clientes
 */

export class PesquisaCliente {

    constructor(
    public nome ?: string,
    public nomeFantasia ?: string,
    public cpf ?: string,
    public cnpj ?: string,
    public numeroPassaporte ?: string,
    public inscricaoEstadual ?: string,
    public inscricaoMunicipal ?: string,
    public inscricaoRural ?: string,
    public cep ?: string) {

    }
}