/**
 * Entidade de cliente
 * @author jose.victor@logiquesistemas.com.br
 */
import {ClienteFone} from "./ClienteFone";

export class Cliente {

    constructor(
        public chavePrimaria : number = null,
        public nome : string = "",
        public cnpjFormatado : string = "",
        public enderecoFormatado : string = "",
        public emailPrincipal : string = "",
        public nomeFantasia : string = "",
        public numeroPassaporte : string = "",
        public cnpj : string = "",
        public cpf : string = "",
        public inscricaoEstadual : string = "",
        public inscricaoMunicipal : string = "",
        public inscricaoRural : string = "",
        public fones : ClienteFone[] = [],
        public rg : string = "") {
    }
}