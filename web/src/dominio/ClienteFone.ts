/**
 * Entidade de clienteFone
 * @author jose.victor@logiquesistemas.com.br
 */
export class ClienteFone {

    constructor(
        public chavePrimaria : number = null,
        public codigoDDD : number,
        public numero : number = null,
        public ramal : number = null,
        public indicadorPrincipal : boolean = false,
        ) {
    }

    static formatarTelefone(fone : ClienteFone) {
        return fone ? `(${fone.codigoDDD}) ${fone.numero}` : "";
    }
}