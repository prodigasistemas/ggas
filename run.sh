#!/bin/bash

sudo docker kill ggas | true && sudo docker rm ggas

rm -Rf ./build/libs/* && rm -Rf /opt/webapps/*

./gradlew war && mv ./build/libs/*.war /opt/webapps/ggas.war

sudo docker run --name ggas -p 9999:8080 -d --restart always -v /opt/ggas/:/opt/ggas/ -v /opt/webapps/:/usr/local/tomcat/webapps/ tomcat:8-jdk8-openjdk
