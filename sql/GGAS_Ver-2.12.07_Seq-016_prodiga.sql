INSERT INTO FATURAMENTO_ANORMALIDADE
VALUES(47, 'VALOR TOTAL MUITO ELEVADO.', 1, 1, 1, 1, CURRENT_TIMESTAMP, 1);

INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'P_PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO',
    'Indica a porcentagem que será considerada para indicar que o faturar possui um valor elevado no faturar grupo',
    '40',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    2
);

INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'P_QUANTIDADE_REFERENCIAS_CALCULO_MEDIA',
    'Indica a quantidade de refêrencias utilizadas para cálculo da média no faturar grupo para indicar faturamento elevado',
    '3',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    2
);

COMMIT;