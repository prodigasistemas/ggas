UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoRelacionamento' WHERE MENU_DS_URL LIKE '%tipoRelacionamentoCliente%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoRelacionamento' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%tela=tipoRelacionamentoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirIncluirTipoRelacionamento' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%tela=tipoRelacionamentoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlterarTipoRelacionamento' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%tela=tipoRelacionamentoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalharTipoRelacionamento' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%tela=tipoRelacionamentoCliente';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirTipoRelacionamento' WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%tela=tipoRelacionamentoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTipoRelacionamento' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%tela=tipoRelacionamentoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTipoRelacionamento' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%tela=tipoRelacionamentoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoRelacionamento' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%tela=tipoRelacionamentoCliente';
