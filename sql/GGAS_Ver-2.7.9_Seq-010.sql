alter table acao_comando add (ACCO_IN_TIPO NUMBER(1,0));
update acao_comando set  ACCO_IN_TIPO=1;

COMMENT ON COLUMN ACAO_COMANDO."ACCO_IN_TIPO" IS 'Indica o tipo do comando de ação 0=normal, 1=estendido';

commit;