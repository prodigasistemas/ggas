insert into constante_sistema (
	cost_cd,
	tabe_cd,
	mosi_cd,
	cost_nm,
	cost_ds,
	cost_vl,
	cost_cd_classe,
	cost_nr_versao,
	cost_in_uso,
	cost_tm_ultima_alteracao)
values (
	sq_cost_cd.nextval,
	(select tabe_cd from TABELA where upper(TABE_NM) LIKE 'SERVICO_AUTORIZACAO_MOTIVO_ENC'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),
	'C_SERV_AUT_MOTIVO_ENCERRAMENTO',
	'ENCERRAMENTO',
	(select SEME_CD from servico_autorizacao_motivo_enc where upper(SEME_DS) like 'ENCERRAMENTO'),
	null,
	1,1,current_timestamp);

insert into constante_sistema (
	cost_cd,
	tabe_cd,
	mosi_cd,
	cost_nm,
	cost_ds,
	cost_vl,
	cost_cd_classe,
	cost_nr_versao,
	cost_in_uso,
	cost_tm_ultima_alteracao)
values (
	sq_cost_cd.nextval,
	(select tabe_cd from TABELA where upper(TABE_NM) LIKE 'SERVICO_AUTORIZACAO_MOTIVO_ENC'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),
	'C_SERV_AUT_MOTIVO_CANCELAMENTO',
	'CANCELAMENTO',
	(select SEME_CD from servico_autorizacao_motivo_enc where upper(SEME_DS) like 'CANCELAMENTO'),
	null,
	1,1,current_timestamp);
commit;