--OC1682981
delete from parametro_sistema where pmsi_cd_parametro like 'EMAIL_RELATORIO_REGISTRO_LEITURA_ANORMALIDADE';
--OC1684349
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_INFORMADO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_CONSUMO_INFORMADO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_MINIMO_FIXADO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_NAO_MEDIDO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_REAL');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_ESTIMADO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_CONSUMO_FORA_FAIXA');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_MEDIDOR_QUEBRADO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_VIRADA_MEDIDOR');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_MEDIDOR_SUBSTITUIDO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_REAL_ATUAL');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_REAL_COMPOSTO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_SUBSTITUICAO_MEDIDOR_NAO_INFORMADO');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_CONSUMO_LEITURA_ATUAL_MENOR_QUE_ANTERIOR');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_TIPO_CONSUMO_MEDIA');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_LEITURA_MENOR_QUE_PROJETADA');
delete from parametro_sistema where pmsi_cd_parametro in ('CODIGO_ANORMALIDADE_CONSUMO_LEITURA_NAO_INFORMADA');
--OC1684683
update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm = 'ENTIDADE_CONTEUDO') where cost_nm in ('C_MOTIVO_COMPLEMENTO_ICMS','C_MOTIVO_COMPLEMENTO_ICMS_ST');
--OC1685502
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo' where tabe_nm = 'SERVICO_TIPO';
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto' where tabe_nm = 'CHAMADO_ASSUNTO';
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),'C_ENT_CLASSE_EQUIPAMENTO','Indica o código da entidade classe Equipamento.',(select encl_cd from entidade_classe where upper(encl_ds) like 'EQUIPAMENTO'),null,'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ENT_CLASSE_EQUIPAMENTO');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),'C_ENT_CLASSE_TURNO','Indica o código da entidade classe Turno.',(select encl_cd from entidade_classe where upper(encl_ds) like 'TURNO'),null,'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ENT_CLASSE_TURNO');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),'C_ATUALIZACAO_TELA_IMOVEL','Indica o código da entidade classe Tela Imóvel.',(select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA IM%VEL'),(select encl_cd from entidade_classe where upper(encl_ds) like 'TELA ATUALIZA%'),'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ATUALIZACAO_TELA_IMOVEL');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),'C_ATUALIZACAO_TELA_CLIENTE','Indica o código da entidade classe Tela Cliente.',(select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA CLIENTE'),(select encl_cd from entidade_classe where upper(encl_ds) like 'TELA ATUALIZA%'),'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ATUALIZACAO_TELA_CLIENTE');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),'C_ATUALIZACAO_TELA_CONTRATO','Indica o código da entidade classe Tela Contrato.',(select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA CONTRATO'),(select encl_cd from entidade_classe where upper(encl_ds) like 'TELA ATUALIZA%'),'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ATUALIZACAO_TELA_CONTRATO');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),'C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO','Indica o código da entidade classe Tela Associação Medidor Ponto de Consumo.',(select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA ASSOCIA%O MEDIDOR PONTO DE CONSUMO'),(select encl_cd from entidade_classe where upper(encl_ds) like 'TELA ATUALIZA%'),'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ATENDIMENTO'),'C_ENT_CLASSE_TELA_ATUALIZACAO','Indica o código da entidade classe Tela Atualização.',(select encl_cd from entidade_classe where upper(encl_ds) like 'TELA ATUALIZA%'),null,'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ENT_CLASSE_TELA_ATUALIZACAO');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'SERVICO_TIPO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'COBRAN%'),'C_SERVICO_TIPO_CORTE','Indica se o tipo de serviço realizado é CORTE',null,null,'1','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_SERVICO_TIPO_CORTE');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'SERVICO_TIPO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'COBRAN%'),'C_SERVICO_TIPO_RELIGACAO','Indica se o tipo de servico realizado e RELIGACAO',(select srti_cd from servico_tipo where srti_ds like 'RELIGACAO NORMAL'),null,'0','1',current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_SERVICO_TIPO_RELIGACAO');
--OC1702636
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo' where tabe_nm in ('CHAMADO_ASSUNTO_SERVICO_TIPO');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo' where tabe_nm in('CHAMADO_TIPO');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.documentoimpressaolayout.dominio.DocumentoImpressaoLayout' where tabe_nm in ('DOCUMENTO_IMPRESSAO_LAYOUT');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.equipe.dominio.Equipe' where tabe_nm in ('EQUIPE');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente' where tabe_nm in ('EQUIPE_COMPONENTE');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.material.dominio.Material' where tabe_nm in ('MATERIAL');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno' where tabe_nm in ('SERVICO_TIPO_AGENDAMENTO_TURNO');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento' where tabe_nm in ('SERVICO_TIPO_EQUIPAMENTO');
update tabela set tabe_nm_classe = 'br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial' where tabe_nm in ('SERVICO_TIPO_MATERIAL');
--OC1687403
delete from parametro_sistema where pmsi_cd_parametro like 'CONFIRMA_QDS_AUTOMATICAMENTE';
--OC1688367
delete from parametro_sistema where pmsi_cd_parametro like 'PERIODICIDADE_ENVIO_EMAIL_ORDEM_SERVICO%';
--OC1688977
delete from parametro_sistema where pmsi_cd_parametro = 'CODIGOS_INDICES_FINANCEIROS_PERCENTUAL';
delete from parametro_sistema where pmsi_cd_parametro = 'CODIGOS_INDICES_FINANCEIROS_VALOR';
--OC1689204
delete from parametro_sistema where pmsi_cd_parametro like 'CONTROLE_NUMERACAO_NOTA_FISCAL_EXTERNO%';
--OC1685656
delete from parametro_sistema where pmsi_cd_parametro like 'QUANTIDADE_DIAS_REDUCAO%';



commit;