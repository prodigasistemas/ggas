INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'MENSAGEM_NOTIFICACAO_CORTE',
    'Mensagem que aparecera na nota da fatura e boleto quando a fatura foi notificada para ser cortada',
    'Mensagem Padrao Notificacao Corte',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    3
);

--------------------------------------- ALTER TABLE - FATURA ---------------------------------------
ALTER TABLE FATURA ADD FATU_IN_NOTIFICACAO NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN FATURA."FATU_IN_NOTIFICACAO" IS 'Indica que a nota fatura/boleto receberá uma mensagem de notificacao de corte';

--------------------------------------- ALTER TABLE - AVISO_CORTE ---------------------------------------
ALTER TABLE AVISO_CORTE ADD AVCO_IN_FATURA_NOTIFICADA NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN AVISO_CORTE."AVCO_IN_FATURA_NOTIFICADA" IS 'Indica que uma mensagem de notificacao aparecerá na proxima nota de fatura/boleto';

COMMIT;