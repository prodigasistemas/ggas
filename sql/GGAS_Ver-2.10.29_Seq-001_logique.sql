insert into contrato_aba_atributo (
  COAA_CD,
  COAB_CD,
  COAA_DS,
  COAA_NM_COLUNA,
  COAA_NR_VERSAO,
  COAA_IN_USO,
  COAA_TM_ULTIMA_ALTERACAO,
  COAA_CD_DEPENDENCIA,
  COAA_IN_DEPEND_OBRIGATORIA
) values (
  sq_coaa_cd.nextval, --COAA_CD
  1, --COAB_CD
  'Banco', --COAA_DS
  'banco', --COAA_NM_COLUNA
  1, --COAA_NR_VERSAO
  1, --COAA_IN_USO
  current_timestamp, --COAA_TM_ULTIMA_ALTERACAO
  null, --COAA_CD_DEPENDENCIA
  0 --COAA_IN_DEPEND_OBRIGATORIA
);
update contrato_aba_atributo
  set coaa_in_depend_obrigatoria = 1, coaa_cd_dependencia = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'debitoAutomatico')
  where COAA_CD = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'banco');
--update contrato_aba_atributo
--  set coaa_in_depend_obrigatoria = 0, coaa_cd_dependencia = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'debitoAutomatico')
--  where COAA_CD = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'arrecadadorConvenioDebitoAutomatico');

insert into contrato_aba_atributo (
  COAA_CD,
  COAB_CD,
  COAA_DS,
  COAA_NM_COLUNA,
  COAA_NR_VERSAO,
  COAA_IN_USO,
  COAA_TM_ULTIMA_ALTERACAO,
  COAA_CD_DEPENDENCIA,
  COAA_IN_DEPEND_OBRIGATORIA
) values (
  sq_coaa_cd.nextval, --COAA_CD
  1, --COAB_CD
  'Agência', --COAA_DS
  'agencia', --COAA_NM_COLUNA
  1, --COAA_NR_VERSAO
  1, --COAA_IN_USO
  current_timestamp, --COAA_TM_ULTIMA_ALTERACAO
  null, --COAA_CD_DEPENDENCIA
  0 --COAA_IN_DEPEND_OBRIGATORIA
);

update contrato_aba_atributo
  set coaa_in_depend_obrigatoria = 1,coaa_cd_dependencia = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'debitoAutomatico')
  where COAA_CD = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'agencia');

insert into contrato_aba_atributo (
  COAA_CD,
  COAB_CD,
  COAA_DS,
  COAA_NM_COLUNA,
  COAA_NR_VERSAO,
  COAA_IN_USO,
  COAA_TM_ULTIMA_ALTERACAO,
  COAA_CD_DEPENDENCIA,
  COAA_IN_DEPEND_OBRIGATORIA
) values (
  sq_coaa_cd.nextval, --COAA_CD
  1, --COAB_CD
  'Conta corrente', --COAA_DS
  'contaCorrente', --COAA_NM_COLUNA
  1, --COAA_NR_VERSAO
  1, --COAA_IN_USO
  current_timestamp, --COAA_TM_ULTIMA_ALTERACAO
  null, --COAA_CD_DEPENDENCIA
  0 --COAA_IN_DEPEND_OBRIGATORIA
);
update contrato_aba_atributo
  set coaa_in_depend_obrigatoria = 1,coaa_cd_dependencia = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'debitoAutomatico')
  where COAA_CD = (SELECT coaa_cd FROM contrato_aba_atributo where coaa_nm_coluna = 'contaCorrente');


ALTER TABLE CONTRATO ADD BANC_CD NUMBER(9,0);
ALTER TABLE CONTRATO ADD CONT_NR_AGENCIA VARCHAR2(35);
ALTER TABLE CONTRATO ADD CONT_NR_CONTA_CORRENTE VARCHAR2(35);

COMMENT ON COLUMN "CONTRATO"."BANC_CD" IS 'Chave primária da tabela BANCO';
COMMENT ON COLUMN "CONTRATO"."CONT_NR_AGENCIA" IS 'Número da agência com dígito verificador';
COMMENT ON COLUMN "CONTRATO"."CONT_NR_CONTA_CORRENTE" IS 'Número da conta corrente';

commit;
