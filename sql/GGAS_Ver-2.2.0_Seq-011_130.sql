delete from tabela_coluna where tabe_cd = (select tabe_cd from tabela where tabe_nm = 'TARIFA_TRIBUTO');
delete from tabela where tabe_nm = 'TARIFA_TRIBUTO';  

CREATE TABLE TARIFA_VIGENCIA_TRIBUTO(
                        TAVT_CD NUMBER(9,0),
                        TAVI_CD NUMBER(9,0),
                        TRIB_CD NUMBER(9,0),
                        TAVT_NR_VERSAO NUMBER(5,0),
                        TAVT_IN_USO NUMBER(1,0),
                        TAVT_TM_ULTIMA_ALTERACAO TIMESTAMP (6),
                        TAVT_DT_FIM_VIGENCIA DATE,
                        TAVT_DT_INICIO_VIGENCIA DATE
                        );
						
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TAVT_CD IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_TAVT_CD';  
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TAVI_CD IS 'Chave primária da tabela TARIFA_VIGENCIA';
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TRIB_CD IS 'Chave primária da tabela TRIBUTO';
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TAVT_NR_VERSAO IS 'Número da versão do registro, utilizado para controle de concorrência';
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TAVT_IN_USO IS 'Indicador de que o registro está ativo para o sistema (0-Nao, 1-Sim)';
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TAVT_TM_ULTIMA_ALTERACAO IS 'Data e hora da realização da última alteração';
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TAVT_DT_FIM_VIGENCIA IS 'Data que termina o período de vigência do tributo na tarifa';
COMMENT ON COLUMN TARIFA_VIGENCIA_TRIBUTO.TAVT_DT_INICIO_VIGENCIA IS 'Data que inicia o período de vigência do tributo na tarifa';
COMMENT ON TABLE TARIFA_VIGENCIA_TRIBUTO  IS 'Tributo da Tarifa Vigência';
    
Create or Replace Public Synonym TARIFA_VIGENCIA_TRIBUTO for TARIFA_VIGENCIA_TRIBUTO;
GRANT DELETE,INSERT,SELECT,UPDATE ON TARIFA_VIGENCIA_TRIBUTO TO GGAS_OPER;
    
CREATE SEQUENCE SQ_TAVT_CD MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;
CREATE OR REPLACE PUBLIC SYNONYM SQ_TAVT_CD FOR SQ_TAVT_CD;
GRANT SELECT ON SQ_TAVT_CD TO GGAS_OPER;
  
CREATE INDEX FK_TAVI_TAVT ON TARIFA_VIGENCIA_TRIBUTO (TAVI_CD);
CREATE UNIQUE INDEX PK_TAVT ON TARIFA_VIGENCIA_TRIBUTO (TAVT_CD);
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO ADD CONSTRAINT PK_TAVT PRIMARY KEY (TAVT_CD) ENABLE;
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO MODIFY (TAVT_DT_INICIO_VIGENCIA CONSTRAINT NN01_TAVT_DT_INICIO_VIGENCIA NOT NULL ENABLE);
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO MODIFY (TAVT_TM_ULTIMA_ALTERACAO CONSTRAINT NN01_TAVT_TM_ULTIMA_ALTERACAO NOT NULL ENABLE);
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO MODIFY (TAVT_IN_USO CONSTRAINT NN01_TAVT_IN_USO NOT NULL ENABLE);
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO MODIFY (TAVT_NR_VERSAO CONSTRAINT NN01_TAVT_NR_VERSAO NOT NULL ENABLE);
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO MODIFY (TRIB_CD CONSTRAINT NN07_TRIB_CD NOT NULL ENABLE);
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO MODIFY (TAVI_CD CONSTRAINT NN08_TAVI_CD NOT NULL ENABLE);
  
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO ADD CONSTRAINT FK_TAVI_TAVT FOREIGN KEY (TAVI_CD) REFERENCES TARIFA_VIGENCIA (TAVI_CD) ENABLE;
ALTER TABLE TARIFA_VIGENCIA_TRIBUTO ADD CONSTRAINT FK_TRIB_TAVT FOREIGN KEY (TRIB_CD) REFERENCES TRIBUTO (TRIB_CD) ENABLE;
  
 INSERT
                          INTO TABELA
                            (
                              TABE_CD,
                              TABE_NM,
                              TABE_DS,
                              TABE_NM_MNEMONICO,
                              TABE_IN_ATRIBUTO_DINAMICO,
                              TABE_NR_VERSAO,
                              TABE_IN_USO,
                              TABE_TM_ULTIMA_ALTERACAO,
                              TABE_IN_CONSULTA_DINAMICA,
                              TABE_NM_CLASSE,
                              TABE_IN_AUDITAVEL,
                              TABE_IN_ALCADA,
                              MENU_CD,
                              TABE_NR_MESES_DESCARTE,
                              TABE_IN_INTEGRACAO,
                              TABE_IN_MAPEAMENTO,
                              TABE_IN_CONSTANTE
                            )
                            SELECT SQ_TABE_CD.nextval,
                            'TARIFA_VIGENCIA_TRIBUTO',
                            'Tarifa Vigência Tributo',
                            'TAVT',
                            0,0,1,
                            CURRENT_TIMESTAMP,
                            1,
                            'br.com.ggas.faturamento.tarifa.impl.TarifaVigenciaTributoImpl',
                            1,
                            0,
                            NULL,
                            NULL,
                            1,0,0 FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA WHERE TABE_NM='TARIFA_VIGENCIA_TRIBUTO');
                          
 INSERT
                          INTO tabela_coluna
                            (
                              TACO_CD,
                              TABE_CD,
                              TACO_NM,
                              TACO_DS,
                              TACO_NM_PROPRIEDADE,
                              TACO_IN_CONSULTA_DINAMICA,
                              TACO_NR_VERSAO,
                              TACO_IN_USO,
                              TACO_TM_ULTIMA_ALTERACAO,
                              TACO_IN_AUDITAVEL,
                              TACO_IN_ALCADA
                            )
                            SELECT SQ_TACO_CD.nextval,
                            (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                            'TAVT_CD',
                            'Chave Primária',
                            'chavePrimaria',
                            0,0,1,
                            CURRENT_TIMESTAMP,
                            1,0
                          FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TAVT_CD');
    
 INSERT
                          INTO tabela_coluna
                            (
                              TACO_CD,
                              TABE_CD,
                              TACO_NM,
                              TACO_DS,
                              TACO_NM_PROPRIEDADE,
                              TACO_IN_CONSULTA_DINAMICA,
                              TACO_NR_VERSAO,
                              TACO_IN_USO,
                              TACO_TM_ULTIMA_ALTERACAO,
                              TACO_IN_AUDITAVEL,
                              TACO_IN_ALCADA
                            )
                            SELECT SQ_TACO_CD.nextval,
                            (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                            'TAVI_CD',
                            'Chave Primária da Tabela TARIFA_VIGENCIA',
                            'tarifaVigencia',
                            0,0,1,
                            CURRENT_TIMESTAMP,
                            1,0
                          FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TAVI_CD');
                          
 INSERT
                          INTO tabela_coluna
                            (
                              TACO_CD,
                              TABE_CD,
                              TACO_NM,
                              TACO_DS,
                              TACO_NM_PROPRIEDADE,
                              TACO_IN_CONSULTA_DINAMICA,
                              TACO_NR_VERSAO,
                              TACO_IN_USO,
                              TACO_TM_ULTIMA_ALTERACAO,
                              TACO_IN_AUDITAVEL,
                              TACO_IN_ALCADA
                            )
                            SELECT SQ_TACO_CD.nextval,
                            (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                            'TRIB_CD',
                            'Chave Primária da Tabela TRIBUTO',
                            'tributo',
                            0,0,1,
                            CURRENT_TIMESTAMP,
                            1,0
                          FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TRIB_CD');  
    
 INSERT
                          INTO tabela_coluna
                            (
                              TACO_CD,
                              TABE_CD,
                              TACO_NM,
                              TACO_DS,
                              TACO_NM_PROPRIEDADE,
                              TACO_IN_CONSULTA_DINAMICA,
                              TACO_NR_VERSAO,
                              TACO_IN_USO,
                              TACO_TM_ULTIMA_ALTERACAO,
                              TACO_IN_AUDITAVEL,
                              TACO_IN_ALCADA
                            )
                            SELECT SQ_TACO_CD.nextval,
                            (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                            'TAVT_NR_VERSAO',
                            'Número da versão do registro, utilizado para controle de concorrência',
                            'versao',
                            0,0,1,
                            CURRENT_TIMESTAMP,
                            1,0
                          FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TAVT_NR_VERSAO');
    
 INSERT
                          INTO tabela_coluna
                            (
                              TACO_CD,
                              TABE_CD,
                              TACO_NM,
                              TACO_DS,
                              TACO_NM_PROPRIEDADE,
                              TACO_IN_CONSULTA_DINAMICA,
                              TACO_NR_VERSAO,
                              TACO_IN_USO,
                              TACO_TM_ULTIMA_ALTERACAO,
                              TACO_IN_AUDITAVEL,
                              TACO_IN_ALCADA
                            )
                            SELECT SQ_TACO_CD.nextval,
                            (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                            'TAVT_TM_ULTIMA_ALTERACAO',
                            'Indica a data e hora da realização da última alteração',
                            'ultimaAlteracao',
                            0,0,1,
                            CURRENT_TIMESTAMP,
                            1,0
                          FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TAVT_TM_ULTIMA_ALTERACAO');
    
 INSERT
                          INTO tabela_coluna
                            (
                              TACO_CD,
                              TABE_CD,
                              TACO_NM,
                              TACO_DS,
                              TACO_NM_PROPRIEDADE,
                              TACO_IN_CONSULTA_DINAMICA,
                              TACO_NR_VERSAO,
                              TACO_IN_USO,
                              TACO_TM_ULTIMA_ALTERACAO,
                              TACO_IN_AUDITAVEL,
                              TACO_IN_ALCADA
                            )
                            SELECT SQ_TACO_CD.nextval,
                            (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                            'TAVT_IN_USO',
                            'Habilitado',
                            'habilitado',
                            0,0,1,
                            CURRENT_TIMESTAMP,
                            1,0
                          FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TAVT_IN_USO');
                          
   INSERT
                            INTO tabela_coluna
                              (
                                TACO_CD,
                                TABE_CD,
                                TACO_NM,
                                TACO_DS,
                                TACO_NM_PROPRIEDADE,
                                TACO_IN_CONSULTA_DINAMICA,
                                TACO_NR_VERSAO,
                                TACO_IN_USO,
                                TACO_TM_ULTIMA_ALTERACAO,
                                TACO_IN_AUDITAVEL,
                                TACO_IN_ALCADA
                              )
                              SELECT SQ_TACO_CD.nextval,
                              (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                              'TAVT_DT_INICIO_VIGENCIA',
                              'Data Vigência Inicial',
                              'dataVigenciaInicial',
                              0,0,1,
                              CURRENT_TIMESTAMP,
                              1,0
                            FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TAVT_DT_INICIO_VIGENCIA');                    
    
    
   INSERT
                            INTO tabela_coluna
                              (
                                TACO_CD,
                                TABE_CD,
                                TACO_NM,
                                TACO_DS,
                                TACO_NM_PROPRIEDADE,
                                TACO_IN_CONSULTA_DINAMICA,
                                TACO_NR_VERSAO,
                                TACO_IN_USO,
                                TACO_TM_ULTIMA_ALTERACAO,
                                TACO_IN_AUDITAVEL,
                                TACO_IN_ALCADA
                              )
                              SELECT SQ_TACO_CD.nextval,
                              (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO'),
                              'TAVT_DT_FIM_VIGENCIA',
                              'Data Vigência Final',
                              'dataVigenciaFinal',
                              0,0,1,
                              CURRENT_TIMESTAMP,
                              1,0
                            FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TARIFA_VIGENCIA_TRIBUTO') AND UPPER(TACO_NM)='TAVT_DT_FIM_VIGENCIA');
							
							
							
INSERT INTO TARIFA_VIGENCIA_TRIBUTO (select SQ_TAVI_CD.NEXTVAL as sequencia, tavi.tavi_cd as vigencia, tatr.trib_cd as tributo, 0 as versao, 1 as habilitado, current_timestamp as ultimaAlteracao, null as dataFimVigencia, tatr.tatr_dt_inicio_vigencia as dataVigencia from tarifa tari
	  inner join TARIFA_TRIBUTO tatr on tatr.tari_cd = tari.tari_cd
	  inner join tarifa_vigencia tavi on tavi.tari_cd = tari.tari_cd );								
    
 drop table tarifa_tributo cascade constraints;
 drop sequence sq_tatr_cd;

 commit;
