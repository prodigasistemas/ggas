insert into documento_impressao_layout 
  select SQ_DOIL_CD.nextval,
  'RELATORIO NOTA FISCAL FATURA',
  'relatorio de nota fiscal fatura',
  'relatorioFaturaNotaFiscalComLayoutBAHIA.jasper',
  (select ENCO_CD from ENTIDADE_CONTEUDO where enco_ds like '%RELATORIO NOTA FISCAL FATURA%'),1,0,current_timestamp
  FROM DUAL WHERE NOT EXISTS
  (select * from documento_impressao_layout  where documento_impressao_layout.DOIL_NM_ARQUIVO  =  'relatorioFaturaNotaFiscalComLayoutBAHIA.jasper');
  
commit;