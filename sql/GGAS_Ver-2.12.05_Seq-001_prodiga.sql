UPDATE CONSTANTE_SISTEMA SET COST_VL = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like '%Débito Automático%') WHERE COST_NM = 'C_TIPO_CONVENIO_DEBITO_AUTOMATICO';

update DOCUMENTO_LAYOUT_CAMPO set enco_cd_tipo_campo = (select enco_cd from entidade_conteudo where enco_ds like 'NUMÉRICO' AND encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Tipo atributo de remessa'))
WHERE DOLR_CD = (select dolr_cd from documento_layout_registro where 
dola_cd = (select dola_cd from documento_layout where enco_cd_tipo_convenio = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like '%Débito Automático%') and DOLA_DS like '%DACNAB150Caixa')
and DOLR_DS like 'HEADER - DEBITO AUTOMATICO') AND DOLC_DS like 'No Sequencial do Registro';

UPDATE DOCUMENTO_LAYOUT_CAMPO SET dolc_nr_posicao_inicial = 136, dolc_nr_posicao_final = 143
WHERE DOLR_CD = (select dolr_cd from documento_layout_registro where 
dola_cd = (select dola_cd from documento_layout where enco_cd_tipo_convenio = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like '%Débito Automático%') and DOLA_DS like '%DACNAB150Caixa')
and DOLR_DS like 'DEBITO EM CONTA CORRENTE') AND DOLC_DS like 'Reservado para o Futuro';

UPDATE DOCUMENTO_LAYOUT_CAMPO SET dolc_nr_posicao_inicial = 130, dolc_nr_posicao_final = 135 
WHERE DOLR_CD = (select dolr_cd from documento_layout_registro where 
dola_cd = (select dola_cd from documento_layout where enco_cd_tipo_convenio = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like '%Débito Automático%') and DOLA_DS like '%DACNAB150Caixa')
and DOLR_DS like 'DEBITO EM CONTA CORRENTE') AND DOLC_DS like 'Identificação';

UPDATE DOCUMENTO_LAYOUT_CAMPO SET dolc_nr_posicao_inicial = 144, dolc_nr_posicao_final = 149, dolc_ds = 'Número Sequencial do Arquvo', ENCO_CD_DOLC_DS_CAMPO = (select enco_cd from entidade_conteudo where enco_ds like 'numeroSequencialRegistro') 
WHERE DOLR_CD = (select dolr_cd from documento_layout_registro where 
dola_cd = (select dola_cd from documento_layout where enco_cd_tipo_convenio = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like '%Débito Automático%') and DOLA_DS like '%DACNAB150Caixa')
and DOLR_DS like 'DEBITO EM CONTA CORRENTE') AND DOLC_DS like 'Tipo da Identificação';

UPDATE DOCUMENTO_LAYOUT_CAMPO set enco_cd_tipo_campo = (select enco_cd from entidade_conteudo where enco_ds like 'NUMÉRICO' AND encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Tipo atributo de remessa')), 
dolc_nr_posicao_final = 42
WHERE DOLR_CD = (select dolr_cd from documento_layout_registro where 
dola_cd = (select dola_cd from documento_layout where enco_cd_tipo_convenio = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like '%Débito Automático%') and DOLA_DS like '%DACNAB150Caixa')
and DOLR_DS like 'DEBITO EM CONTA CORRENTE') AND DOLC_DS like 'ID do Cliente no Banco';

UPDATE DOCUMENTO_LAYOUT_CAMPO set enco_cd_tipo_campo = (select enco_cd from entidade_conteudo where enco_ds like 'NUMÉRICO' AND encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Tipo atributo de remessa'))
WHERE DOLR_CD = (select dolr_cd from documento_layout_registro where 
dola_cd = (select dola_cd from documento_layout where enco_cd_tipo_convenio = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like '%Débito Automático%') and DOLA_DS like '%DACNAB150Caixa')
and DOLR_DS like 'TRAILLER') AND DOLC_DS like 'No Sequencial do Registro';

COMMIT;


