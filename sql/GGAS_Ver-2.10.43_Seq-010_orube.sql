-- Módulo Relatório --> ( Sub-Menu ) Cobrança

	-- Relatório de Inadimplentes

INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'GERAR',(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Relatório de Inadimplentes%'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Relatórios%'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Relatório de Inadimplentes%')) ;
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) values (SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Relatório de Inadimplentes%'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Relatório de Inadimplentes%')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP) ;
UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRelatorioDeInadimplentes' WHERE MENU_DS_URL LIKE 'exibirPesquisaRelatorioDeInadimplentes.do?acao=exibirPesquisaRelatorioDeInadimplentes%tela=relatorioInadimplentes';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaRelatorioDeInadimplentes' WHERE resi_ds LIKE 'exibirPesquisaRelatorioDeInadimplentes.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirRelatorioDeInadimplentes', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Relatório de Inadimplentes%')) WHERE resi_ds LIKE 'exibirRelatorioDeInadimplentes.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioDeInadimplentes', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Relatório de Inadimplentes%')) WHERE resi_ds LIKE 'gerarRelatorioDeInadimplentes.do';

	-- Prazo Mínimo de Antecedência para Envio de Notificação de Corte

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) SELECT SQ_RESI_CD.nextval, 'exibirRelatorioPrazoMinimoNotificacaoCorte', 0, ((SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND mosi_cd = (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Relatórios%') AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Prazo Mínimo de Antecedência para Envio de Notificação de Corte%'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (SELECT * FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'exibirRelatorioPrazoMinimoNotificacaoCorte') ;
UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte' WHERE MENU_DS_URL LIKE 'exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte.do?acao=exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte' WHERE RESI_DS LIKE 'exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioPrazoMinimoNotificacaoCorte' WHERE RESI_DS LIKE 'gerarRelatorioPrazoMinimoNotificacaoCorte.do';

	-- Prazo Mínimo de Antecedência para Envio do Aviso de Corte

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) SELECT SQ_RESI_CD.nextval, 'exibirRelatorioPrazoMinimoAvisoCorte', 0, ((SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND mosi_cd = (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Relatórios%') AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Prazo Mínimo de Antecedência para Envio do Aviso de Corte%'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (SELECT * FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'exibirRelatorioPrazoMinimoAvisoCorte') ;
UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRelatorioPrazoMinimoAvisoCorte' WHERE MENU_DS_URL LIKE 'exibirPesquisaRelatorioPrazoMinimoAvisoCorte.do?acao=exibirPesquisaRelatorioPrazoMinimoAvisoCorte';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaRelatorioPrazoMinimoAvisoCorte' WHERE RESI_DS LIKE 'exibirPesquisaRelatorioPrazoMinimoAvisoCorte.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioPrazoMinimoAvisoCorte' WHERE RESI_DS LIKE 'gerarRelatorioPrazoMinimoAvisoCorte.do';


COMMIT;
