UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaProposta' WHERE MENU_DS_URL LIKE '%exibirPesquisaProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaProposta' WHERE RESI_DS LIKE '%exibirPesquisaProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoProposta' WHERE RESI_DS LIKE '%exibirDetalhamentoProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoProposta' WHERE RESI_DS LIKE '%exibirAlteracaoProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoProposta' WHERE RESI_DS LIKE '%exibirInclusaoProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarProposta' WHERE RESI_DS LIKE '%alterarProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerProposta' WHERE RESI_DS LIKE '%removerProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirProposta' WHERE RESI_DS LIKE '%incluirProposta%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarProposta' WHERE RESI_DS LIKE '%pesquisarProposta%';

COMMIT;