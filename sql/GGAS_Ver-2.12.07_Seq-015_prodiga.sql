insert into recurso_sistema values(SQ_RESI_CD.nextval, 'imprimirSegundaViaNotaChamado', 0, (select opsi_cd from operacao_sistema where opsi_ds like 'CONSULTAR' and menu_cd in
(select menu_cd from menu where menu_ds like 'Chamado' and menu_cd_pai in (select menu_cd from menu where menu_ds = 'Chamado'))), 1, 1, CURRENT_TIMESTAMP);

insert into recurso_sistema values(SQ_RESI_CD.nextval, 'enviarEmailFaturaChamado', 0, (select opsi_cd from operacao_sistema where opsi_ds like 'CONSULTAR' and menu_cd in
(select menu_cd from menu where menu_ds like 'Chamado' and menu_cd_pai in (select menu_cd from menu where menu_ds = 'Chamado'))), 1, 1, CURRENT_TIMESTAMP);

insert into recurso_sistema values(SQ_RESI_CD.nextval, 'gerarExtratoQuitacaoAnualChamado', 0, (select opsi_cd from operacao_sistema where opsi_ds like 'CONSULTAR' and menu_cd in
(select menu_cd from menu where menu_ds like 'Chamado' and menu_cd_pai in (select menu_cd from menu where menu_ds = 'Chamado'))), 1, 1, CURRENT_TIMESTAMP);

insert into recurso_sistema values(SQ_RESI_CD.nextval, 'salvarDebitoAutomaticoContrato', 0, (select opsi_cd from operacao_sistema where opsi_ds like 'CONSULTAR' and menu_cd in
(select menu_cd from menu where menu_ds like 'Chamado' and menu_cd_pai in (select menu_cd from menu where menu_ds = 'Chamado'))), 1, 1, CURRENT_TIMESTAMP);

Insert into MENU (MENU_CD,MENU_CD_PAI,MENU_DS,MENU_NR_VERSAO,MENU_IN_USO,MENU_TM_ULTIMA_ALTERACAO,MENU_IN_ALCADA,MENU_NR_ORDEM,RESI_CD,MENU_DS_URL)
values (SQ_MENU_CD.nextval,(select menu_cd from menu where menu_ds like 'Chamado' and menu_cd_pai = (select menu_cd from menu where menu_cd_pai is null and menu_ds like 'Atendimento ao Público')),
'Visualizar Informações Ponto de Consumo','1','1',CURRENT_TIMESTAMP,'0',
190,null,'exibirPesquisaVisualizarPontoConsumo');

Insert into RECURSO_SISTEMA (RESI_CD,RESI_DS,RESI_NR_VERSAO,OPSI_CD,RESI_IN_WEB,RESI_IN_USO,RESI_TM_ULTIMA_ALTERACAO) values 
(SQ_RESI_CD.nextval,'exibirPesquisaVisualizarPontoConsumo','0',(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS like 'CONSULTAR'
AND MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS like 'Chamado')),
'1','1',CURRENT_TIMESTAMP);

UPDATE MENU SET RESI_CD = (select resi_cd from recurso_sistema where resi_ds like 'exibirPesquisaVisualizarPontoConsumo') WHERE MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS like 'Visualizar Informações Ponto de Consumo');

INSERT INTO PERMISSAO_SISTEMA (PESI_CD, MENU_CD, OPSI_CD, PESI_NM_TIPO, PESI_NR_VERSAO, PASI_CD, PESI_IN_USO, PESI_TM_ULTIMA_ALTERACAO)
values (SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL like 'exibirPesquisaVisualizarPontoConsumo'), (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS like 'CONSULTAR' AND MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS like 'Chamado')),
0,0, (SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO PERMISSAO_SISTEMA (PESI_CD, MENU_CD, OPSI_CD, PESI_NM_TIPO, PESI_NR_VERSAO, PASI_CD, PESI_IN_USO, PESI_TM_ULTIMA_ALTERACAO)
values (SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL like 'exibirPesquisaVisualizarPontoConsumo'), (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS like 'CONSULTAR' AND MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS like 'Chamado')),
0,0, (SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ATENDIMENTO AO PUBLICO'), 1, CURRENT_TIMESTAMP);

INSERT INTO PERMISSAO_SISTEMA (PESI_CD, MENU_CD, OPSI_CD, PESI_NM_TIPO, PESI_NR_VERSAO, PASI_CD, PESI_IN_USO, PESI_TM_ULTIMA_ALTERACAO)
values (SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL like 'exibirPesquisaVisualizarPontoConsumo'), (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS like 'CONSULTAR' AND MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS like 'Chamado')),
0,0, (SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'SUPERVISORA ATENDIMENTO'), 1, CURRENT_TIMESTAMP);

Insert into RECURSO_SISTEMA (RESI_CD,RESI_DS,RESI_NR_VERSAO,OPSI_CD,RESI_IN_WEB,RESI_IN_USO,RESI_TM_ULTIMA_ALTERACAO) values 
(SQ_RESI_CD.nextval,'pesquisaPontoConsumoChamado','0',(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS like 'CONSULTAR'
AND MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS like 'Chamado')),
'1','1',CURRENT_TIMESTAMP);


COMMIT;

