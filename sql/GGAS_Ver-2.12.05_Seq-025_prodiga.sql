UPDATE CLIENTE_NEGATIVACAO SET CLNE_DT_INCLUSAO_NEGATIVACAO = null, 
CLNE_DT_EXCLUSAO_NEGATIVACAO = null, CLNE_DT_CONFIRMACAO_NEG = null, CLNE_DT_CANCELAMENTO_NEG = null;

ALTER TABLE CLIENTE_NEGATIVACAO DROP COLUMN CLNE_DT_INCLUSAO_NEGATIVACAO;
ALTER TABLE CLIENTE_NEGATIVACAO DROP COLUMN CLNE_DT_EXCLUSAO_NEGATIVACAO;
ALTER TABLE CLIENTE_NEGATIVACAO DROP COLUMN CLNE_DT_CONFIRMACAO_NEG;
ALTER TABLE CLIENTE_NEGATIVACAO DROP COLUMN CLNE_DT_CANCELAMENTO_NEG;


ALTER TABLE FATURA_CLIENTE_NEGATIVACAO ADD FCNE_DT_INCLUSAO_NEGATIVACAO timestamp(7);
ALTER TABLE FATURA_CLIENTE_NEGATIVACAO ADD FCNE_DT_EXCLUSAO_NEGATIVACAO timestamp(7);
ALTER TABLE FATURA_CLIENTE_NEGATIVACAO ADD FCNE_DT_CONFIRMACAO_NEG timestamp(7);
ALTER TABLE FATURA_CLIENTE_NEGATIVACAO ADD FCNE_DT_CANCELAMENTO_NEG timestamp(7);

COMMIT;



