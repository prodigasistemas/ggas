UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaUnidadeNegocio' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=unidadeNegocio';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaUnidadeNegocio' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=unidadeNegocio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirUnidadeNegocio' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=unidadeNegocio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoUnidadeNegocio' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=unidadeNegocio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarUnidadeNegocio' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=unidadeNegocio';


UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarUnidadeNegocio' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=unidadeNegocio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirUnidadeNegocio' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=unidadeNegocio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarUnidadeNegocio' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=unidadeNegocio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerUnidadeNegocio' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=unidadeNegocio';

COMMIT;