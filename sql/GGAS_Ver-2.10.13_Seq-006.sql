UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRamal' WHERE MENU_DS_URL LIKE '%exibirPesquisaRamal%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaRamal' WHERE RESI_DS LIKE '%exibirPesquisaRamal.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirRamal' WHERE RESI_DS LIKE '%exibirInserirRamal.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizacaoRamal' WHERE RESI_DS LIKE '%exibirAtualizacaoRamal.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarRamal' WHERE RESI_DS LIKE '%pesquisarRamal.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarRamal' WHERE RESI_DS LIKE '%atualizarRamal.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirRamais' WHERE RESI_DS LIKE '%excluirRamais.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirRamal' WHERE RESI_DS LIKE '%inserirRamal.do%';

commit;