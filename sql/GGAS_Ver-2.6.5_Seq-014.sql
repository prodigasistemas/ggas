alter table imovel modify IMOV_NR_SEQUENCIA_LEITURA	NUMBER(6,0);
alter table imovel modify IMOV_QN_PONTO_CONSUMO	NUMBER(6,0);
alter table rota modify ROTA_NR_MAX_PONTOS_CONSUMO NUMBER(6,0);
commit;
