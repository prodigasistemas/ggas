insert into PARAMETRO_SISTEMA 
(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,
PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select sq_pmsi_cd.nextval,
	'PARAMETRO_UNIDADE_PRESSAO_MEDICAO',
	'Unidade pressão da medição',
	(select unid_cd from unidade where upper(unid_ds_abreviado) = 'ATM'),
	null,
	'1',
	'1',
	'1',
	current_timestamp,
	(select tabe_cd from tabela where upper(TABE_NM)='UNIDADE'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDIÇÃO')
from dual where not exists
(select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO like 'PARAMETRO_UNIDADE_PRESSAO_MEDICAO');

insert into PARAMETRO_SISTEMA 
(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,
PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select sq_pmsi_cd.nextval,
	'PARAMETRO_UNIDADE_TEMPERATURA_MEDICAO',
	'Unidade temperatura da medição',
	(select unid_cd from unidade where upper(unid_ds) = 'CELSIUS'),
	null,
	'1',
	'1',
	'1',
	current_timestamp,
	(select tabe_cd from tabela where upper(TABE_NM)='UNIDADE'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDIÇÃO')
from dual where not exists
(select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO like 'PARAMETRO_UNIDADE_TEMPERATURA_MEDICAO');


commit;