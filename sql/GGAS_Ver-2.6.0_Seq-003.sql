-- atualiza��o dos valores das constantes referente a rubrica.
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_MARGEM_AJUSTE_PCS') where cost_nm='C_RUBRICA_CREDITO_MARGEM_AJUSTE_PCS';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS') where cost_nm='C_RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_GAS_AJUSTE_PCS') where cost_nm='C_RUBRICA_CREDITO_GAS_AJUSTE_PCS';

update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_DEBITO_GAS_AJUSTE_PCS') where cost_nm='C_RUBRICA_DEBITO_GAS_AJUSTE_PCS';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_DEBITO_MARGEM_AJUSTE_PCS') where cost_nm='C_RUBRICA_DEBITO_MARGEM_AJUSTE_PCS';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS') where cost_nm='C_RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS';

update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_ICMS_SUBSTITUTO') where cost_nm='C_RUBRICA_ICMS_SUBSTITUTO';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_MULTA_ATRASO') where cost_nm='C_RUBRICA_MULTA_ATRASO';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_JUROS_MORA') where cost_nm='C_RUBRICA_JUROS_MORA';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='CODIGO_RUBRICA_PARCELAMENTO_DEBITO') where cost_nm='C_RUBRICA_PARCELAMENTO_DEBITO';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_VOLUME') where cost_nm='C_RUBRICA_CREDITO_VOLUME';

update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='CODIGO_RUBRICA_MULTA_RECISORIA') where cost_nm='C_RUBRICA_MULTA_RECISORIA';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='PARAMETRO_RUBRICA_DEV_PAGTO_MAIOR') where cost_nm='C_RUBRICA_CREDITO_PAGTO_MAIOR';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='PARAMETRO_RUBRICA_DEB_PAGTO_MENOR') where cost_nm='C_RUBRICA_DEBITO_PAGTO_MENOR';
update constante_sistema set cost_vl=(select pmsi_vl_parametro from parametro_sistema where pmsi_cd_parametro='RUBRICA_DESCONTO_ICMS_PRODESIN') where cost_nm='C_RUBRICA_DESCONTO_ICMS_PRODESIN';

commit;

-- exclus�o dos parametros n�o utilizados de rubrica
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_MARGEM_AJUSTE_PCS';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_DEBITO_GAS_AJUSTE_PCS';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_DEBITO_MARGEM_AJUSTE_PCS';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_ICMS_SUBSTITUTO';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_MULTA_ATRASO';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_JUROS_MORA';
delete from parametro_sistema where pmsi_cd_parametro='CODIGO_RUBRICA_PARCELAMENTO_DEBITO';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_VOLUME';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_CREDITO_GAS_AJUSTE_PCS';
delete from parametro_sistema where pmsi_cd_parametro='CODIGO_RUBRICA_MULTA_RECISORIA';
delete from parametro_sistema where pmsi_cd_parametro='PARAMETRO_RUBRICA_DEV_PAGTO_MAIOR';
delete from parametro_sistema where pmsi_cd_parametro='PARAMETRO_RUBRICA_DEB_PAGTO_MENOR';
delete from parametro_sistema where pmsi_cd_parametro='RUBRICA_DESCONTO_ICMS_PRODESIN';

commit;