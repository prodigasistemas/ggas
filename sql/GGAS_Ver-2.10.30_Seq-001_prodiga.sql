SELECT * FROM PARAMETRO_SISTEMA;

INSERT INTO PARAMETRO_SISTEMA (PMSI_CD,
                              PMSI_CD_PARAMETRO,
                              PMSI_DS_PARAMETRO,
                              PMSI_VL_PARAMETRO,
                              PMSI_DS_COMPLEMENTO,
                              PMSI_CD_TIPO_PARAMETRO,
                              PMSI_NR_VERSAO,
                              PMSI_IN_USO,
                              PMSI_TM_ULTIMA_ALTERACAO,
                              TABE_CD,
                              MOSI_CD) 
SELECT SQ_PMSI_CD.NEXTVAL,
      'UTILIZAR_CICLO_CINCO',
      'INDICADOR SE O SISTEMA DEVE CALCULAR OS CICLOS COM BASE NAS DADAS AO INVÉS DAS INFORMAÇÕES DA PERIODICIDADE',
      '0',
       NULL,
       3,
       1,
       1,
       CURRENT_TIMESTAMP,
       NULL,
       (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) LIKE UPPER('FATURAMENTO'))
FROM DUAL WHERE NOT EXISTS (SELECT * FROM PARAMETRO_SISTEMA WHERE UPPER(PMSI_CD_PARAMETRO) LIKE 'UTILIZAR_CICLO_CINCO');
COMMIT;
