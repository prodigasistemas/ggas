ALTER TABLE COMANDO_NEGATIVACAO RENAME COLUMN CMNE_DIAS_ATRASO TO CMNE_DIAS_ATRASO_INICIAL;
ALTER TABLE COMANDO_NEGATIVACAO ADD CMNE_DIAS_ATRASO_FIM NUMBER(6,0);