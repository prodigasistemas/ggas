update tabela_coluna set taco_in_uso = 0 where taco_nm_propriedade='consideraParadaNaoProgramada' and tabe_cd = (select tabe_cd from tabela where tabe_nm = 'CONTRATO_PONTO_CONS_PENALIDADE');
update contrato_aba_atributo set coaa_in_uso = 0 where coaa_nm_coluna = 'consideraParadaNaoProgramada';
commit;

update tabela_coluna set taco_in_uso = 0 where taco_nm_propriedade='apuracaoParadaNaoProgramada' and tabe_cd = (select tabe_cd from tabela where tabe_nm = 'CONTRATO_PONTO_CONS_PENALIDADE');
update contrato_aba_atributo set coaa_in_uso = 0 where coaa_nm_coluna = 'apuracaoParadaNaoProgramada';
commit;