Insert into RECURSO_SISTEMA (RESI_CD,RESI_DS,RESI_NR_VERSAO,OPSI_CD,RESI_IN_WEB,RESI_IN_USO,RESI_TM_ULTIMA_ALTERACAO) values 
(SQ_RESI_CD.nextval,'exibirRoteirizarFaturas','0', (SELECT OPSI_CD FROM RECURSO_SISTEMA WHERE RESI_DS = 'roteirizarCorte'),'1','1',CURRENT_TIMESTAMP);

Insert into MENU (MENU_CD,MENU_CD_PAI,MENU_DS,MENU_NR_VERSAO,MENU_IN_USO,MENU_TM_ULTIMA_ALTERACAO,MENU_IN_ALCADA,MENU_NR_ORDEM,RESI_CD,MENU_DS_URL)
values (SQ_MENU_CD.nextval,(select menu_cd from menu where menu_ds like 'Cobrança' and menu_cd_pai is null),
'Roteirização Faturas','1','1',CURRENT_TIMESTAMP,'0',
190,(select resi_cd from recurso_sistema where resi_ds like 'exibirRoteirizarFaturas'),'exibirRoteirizarFaturas');

INSERT INTO PERMISSAO_SISTEMA (PESI_CD, MENU_CD, OPSI_CD, PESI_NM_TIPO, PESI_NR_VERSAO, PASI_CD, PESI_IN_USO, PESI_TM_ULTIMA_ALTERACAO)
values (SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL like 'exibirRoteirizarFaturas'), (SELECT OPSI_CD FROM RECURSO_SISTEMA WHERE RESI_DS = 'priorizarLiberarFaturas'),
0,0, (SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES (SQ_RESI_CD.nextval,'concluirPriorizacaoCorte','0', (SELECT OPSI_CD FROM RECURSO_SISTEMA WHERE RESI_DS = 'priorizarLiberarFaturas'),'1','1',CURRENT_TIMESTAMP);


--------------------------------------- ALTER TABLE - AVISO_CORTE ---------------------------------------

ALTER TABLE AVISO_CORTE ADD AVCO_IN_PRIORIZADO NUMBER(1,0) DEFAULT 0;

--------------------------------------- RELATORIO RESUMO AUTORIZACAO SERVICO -------------------------------
INSERT INTO ENTIDADE_CONTEUDO VALUES (SQ_ENCO_CD.NEXTVAL, (SELECT ENCL_CD FROM ENTIDADE_CLASSE where encl_ds like 'Tipo do documento impressao'), 'Autorização de Serviço', 'AS', 1, 1, 1, CURRENT_TIMESTAMP, 1);

INSERT INTO CONSTANTE_SISTEMA
VALUES(SQ_COST_CD.NEXTVAL, (select TABE_CD from tabela where tabe_nm like 'SERVICO_AUTORIZACAO'), (select mosi_cd from modulo_sistema where mosi_ds like 'Atendimento Ao Público'), 'SERVICO_AUTORIZACAO', 
'Informa o relatório de serviço de autorização', (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like  'Autorização de Serviço'), null, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES (SQ_DOIL_CD.nextval, 'Autorização de Serviço', 'Relatório de Autorização de Serviço', 'relatorioListagemAutorizacaoServico.jasper',  (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS like  'Autorização de Serviço'),
1, 1, CURRENT_TIMESTAMP)

COMMIT;
