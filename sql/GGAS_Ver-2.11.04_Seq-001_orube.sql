UPDATE MENU
SET MENU_CD_PAI = 6, MENU_DS_URL = 'exibirAcaoCobranca', MENU_NR_ORDEM = 198
WHERE MENU_CD_PAI IN (SELECT MENU_CD FROM MENU WHERE MENU_CD_PAI = 6 AND MENU_DS LIKE 'Ação de Cobrança')
AND MENU_DS LIKE 'Ação de Cobrança';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoAcaoCobranca' WHERE resi_ds LIKE 'exibirInclusaoAcaoCobranca.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoAcaoCobranca' WHERE resi_ds LIKE 'exibirAlteracaoAcaoCobranca.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAcaoCobranca' WHERE resi_ds LIKE 'exibirPesquisaAcaoCobranca.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarAcaoCobranca' WHERE resi_ds LIKE 'pesquisarAcaoCobranca.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoAcaoCobranca' WHERE resi_ds LIKE 'exibirDetalhamentoAcaoCobranca.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirAcaoCobranca' WHERE resi_ds LIKE 'incluirAcaoCobranca.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarAcaoCobranca' WHERE resi_ds LIKE 'alterarAcaoCobranca.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirAcaoCobranca' WHERE resi_ds LIKE 'excluirAcaoCobranca.do';

COMMIT;