UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaArrecadadorConvenio' WHERE MENU_DS_URL LIKE '%exibirPesquisaArrecadadorConvenio.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaArrecadadorConvenio' WHERE RESI_DS LIKE '%exibirPesquisaArrecadadorConvenio.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoArrecadadorConvenio' WHERE RESI_DS LIKE '%exibirInclusaoArrecadadorConvenio.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoArrecadadorConvenio' WHERE RESI_DS LIKE '%exibirDetalhamentoArrecadadorConvenio.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoArrecadadorConvenio' WHERE RESI_DS LIKE '%exibirAlteracaoArrecadadorConvenio.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerArrecadadorConvenio' WHERE RESI_DS LIKE '%removerArrecadadorConvenio.do%';

update ARRECADADOR_CONTRATO_CONVENIO set ARCC_IN_PADRAO = 0 where ARCC_CD_CONVENIO = 31;

COMMIT;