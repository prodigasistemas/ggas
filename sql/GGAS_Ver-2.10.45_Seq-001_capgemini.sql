--------------------------------------------------------------------------------
--  INSERCAO DE PARAMETRO PARA DIA DO VENCIMENTO
--------------------------------------------------------------------------------
--INSERT INTO ENTIDADE_CLASSE VALUES (SQ_ENCL_CD.NEXTVAL, 'Dias Possiveis Vencimento', null, 1, 1, CURRENT_TIMESTAMP);
--------------------------------------------------------------------------------
--  INSERCAO DE DATAS FIXAS PARA COMBO DIA DO VENCIMENTO - FATURAMENTO
--------------------------------------------------------------------------------
INSERT INTO ENTIDADE_CONTEUDO VALUES (SQ_ENCO_CD.NEXTVAL, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS LIKE 'Dias Possiveis Vencimento'), 05, null, 1, 1, 1, CURRENT_TIMESTAMP, null);
INSERT INTO ENTIDADE_CONTEUDO VALUES (SQ_ENCO_CD.NEXTVAL, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS LIKE 'Dias Possiveis Vencimento'), 10, null, 1, 1, 1, CURRENT_TIMESTAMP, null);
INSERT INTO ENTIDADE_CONTEUDO VALUES (SQ_ENCO_CD.NEXTVAL, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS LIKE 'Dias Possiveis Vencimento'), 15, null, 1, 1, 1, CURRENT_TIMESTAMP, null);
INSERT INTO ENTIDADE_CONTEUDO VALUES (SQ_ENCO_CD.NEXTVAL, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS LIKE 'Dias Possiveis Vencimento'), 20, null, 1, 1, 1, CURRENT_TIMESTAMP, null);
INSERT INTO ENTIDADE_CONTEUDO VALUES (SQ_ENCO_CD.NEXTVAL, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS LIKE 'Dias Possiveis Vencimento'), 25, null, 1, 1, 1, CURRENT_TIMESTAMP, null);
INSERT INTO ENTIDADE_CONTEUDO VALUES (SQ_ENCO_CD.NEXTVAL, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS LIKE 'Dias Possiveis Vencimento'), 30, null, 1, 1, 1, CURRENT_TIMESTAMP, null);
COMMIT;
--------------------------------------------------------------------------------
--  ALTERACAO DO TAMANHO DAO CAMPO PCS DA CROMATOGRAFIA
--------------------------------------------------------------------------------
ALTER TABLE CROMATOGRAFIA MODIFY (CROM_PCS NUMBER(9,4));

--------------------------------------------------------------------------------
--  ALTERACAO DO TAMANHO DAO CAMPO TARI_DS DA TARIFA
--------------------------------------------------------------------------------
ALTER TABLE TARIFA MODIFY (TARI_DS varchar (30));

--------------------------------------------------------------------------------
--  ALTERACAO DO MENU Quadra
--------------------------------------------------------------------------------
UPDATE MENU
SET MENU_DS_URL = 'pesquisarQuadra?acao=exibirPesquisaQuadra'
WHERE MENU_DS LIKE 'Quadra';

--------------------------------------------------------------------------------
--  ALTERACAO DO TAMANHO DE ACORDO COM APLICACAO
--------------------------------------------------------------------------------
ALTER TABLE CLIENTE_ENDERECO MODIFY CLEN_DS_COMPLEMENTO VARCHAR(255);
