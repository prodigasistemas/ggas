update "UNIDADE_CONVERSAO" set "UNID_DS_FORMULA"='return parametro - 273.15'
where UNID_CD_DE = (select UNID_CD from UNIDADE where UPPER(UNID_DS) like 'KELVIN')
and UNID_CD_PARA = (select UNID_CD from UNIDADE where UPPER(UNID_DS) like 'CELSIUS');
commit;