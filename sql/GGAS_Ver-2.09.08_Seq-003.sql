COMMENT ON COLUMN CHAMADO."CHAM_IN_UNID_VISUALIZADORAS" IS 'Indica se o chamado possui unidades supervisoras.';

INSERT INTO TABELA_COLUNA
  ( TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA, TACO_IN_VARIAVEL, TACO_CD_VARIAVEL )
SELECT
  SQ_TACO_CD.nextval,
   (SELECT TABE_CD FROM TABELA WHERE TABE_NM LIKE 'CHAMADO'),
  'CHAM_IN_UNID_VISUALIZADORAS',
  'Indica se o chamado possui unidades supervisoras.',
  'indicadorUnidadesOrganizacionalVisualizadora',
  '0',
  '0',
  '1',
  CURRENT_TIMESTAMP,
  '0',
  '0',
  '0',
  NULL
										FROM DUAL WHERE NOT EXISTS
  	(SELECT *
    FROM TABELA_COLUNA
    WHERE TACO_NM LIKE 'CHAM_IN_UNID_VISUALIZADORAS'
    AND TABE_CD =
  	(SELECT TABE_CD FROM TABELA WHERE TABE_NM LIKE 'CHAMADO')
  );
  
COMMENT ON COLUMN CHAMADO_ASSUNTO."CHAS_DS_ORIENTACAO" IS 'Observação ou Orientação para quem vai abrir o chamado';

COMMENT ON COLUMN SERVICO_TIPO."SRTI_IN_EMAIL_CADASTRAR" IS 'Indica se serviço do tipo cadastrado envia email ao ser criado.';

COMMENT ON COLUMN SERVICO_TIPO."SRTI_IN_EMAIL_EM_EXECUCAO" IS 'Indica se serviço do tipo cadastrado envia email ao ser colocado para execução.';

COMMENT ON COLUMN SERVICO_TIPO."SRTI_IN_EMAIL_EXECUCAO" IS 'Indica se serviço do tipo cadastrado envia email ao ser executado.';

COMMENT ON COLUMN SERVICO_TIPO."SRTI_IN_EMAIL_ENCERRAR" IS 'Indica se serviço do tipo cadastrado envia email ao ser encerrado.';

COMMENT ON COLUMN SERVICO_TIPO."SRTI_IN_EMAIL_ALTERAR" IS 'Indica se serviço do tipo cadastrado envia email ao ser altearado.';

COMMENT ON COLUMN CHAMADO_ASSUNTO."CHAS_IN_EMAIL_CADASTRAR" IS 'Indicador para saber se envia email após o cadastro de chamado.';

COMMENT ON COLUMN CHAMADO_ASSUNTO."CHAS_IN_EMAIL_TRAMITAR" IS 'Indicador para saber se envia email após o tramite de chamado.';

COMMENT ON COLUMN CHAMADO_ASSUNTO."CHAS_IN_EMAIL_REITERAR" IS 'Indicador para saber se envia email após a reiteração de chamado.';

COMMENT ON COLUMN CHAMADO_ASSUNTO."CHAS_IN_EMAIL_ENCERRAR" IS 'Indicador para saber se envia email após o encerramento de chamado.';

COMMENT ON COLUMN CHAMADO_ASSUNTO."CHAS_IN_UNID_VISUALIZADORAS" IS 'Indicador para saber se outra Unidade Organizacional pode visualizar o chamado com esse assunto.';

COMMENT ON COLUMN EQUIPAMENTO."EQPA_MD_POTENCIA_MEDIA" IS 'Valor fixo médio de potência nominal do equipamento, usado se SEGM_IN_POTENCIA_EQPA_FIXA = 1.';

COMMENT ON COLUMN EQUIPAMENTO."EQPA_MD_POTENCIA_ALTA" IS 'Valor fixo alto de potência nominal do equipamento, usado se SEGM_IN_POTENCIA_EQPA_FIXA = 1.';

COMMIT;