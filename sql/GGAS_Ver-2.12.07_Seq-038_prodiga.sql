INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Carta Manunteção Preventiva Sem Paralização Algas', 'Relatorio Carta Manunteção Preventiva Sem Paralização', 'relatorioCartaAlgasPreventivaSemParalizacao.jasper',
(SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_CD = (SELECT COST_VL FROM CONSTANTE_SISTEMA WHERE COST_NM  = 'RELATORIO_PROTOCOLO')),
1, 1, CURRENT_TIMESTAMP);

INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Carta Substituição Calibração Algas', 'Relatorio Carta Substituição Calibração', 'relatorioCartaAlgasSubsCalib.jasper',
(SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_CD = (SELECT COST_VL FROM CONSTANTE_SISTEMA WHERE COST_NM  = 'RELATORIO_PROTOCOLO')),
1, 1, CURRENT_TIMESTAMP);

COMMIT;