UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaNotaDebitoCredito' WHERE MENU_DS_URL LIKE '%exibirPesquisaNotaDebitoCredito.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirNotaDebitoCredito' WHERE RESI_DS LIKE 'incluirNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaNotaDebitoCredito' WHERE RESI_DS LIKE 'exibirPesquisaNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarNotaDebitoCredito' WHERE RESI_DS LIKE 'pesquisarNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarPontoConsumoNotaDebitoCredito' WHERE RESI_DS LIKE 'pesquisarPontoConsumoNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoNotaDebitoCredito' WHERE RESI_DS LIKE 'exibirDetalhamentoNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoNotaDebitoCredito' WHERE RESI_DS LIKE 'exibirInclusaoNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirConciliarNotaDebitoCredito' WHERE RESI_DS LIKE 'exibirConciliarNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'conciliarNotaDebitoCredito' WHERE RESI_DS LIKE 'conciliarNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirNotasDebitoCredito' WHERE RESI_DS LIKE 'exibirNotasDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirCancelamentoNotaDebitoCredito' WHERE RESI_DS LIKE 'exibirCancelamentoNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'cancelarNotaDebitoCredito' WHERE RESI_DS LIKE 'cancelarNotaDebitoCredito.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'imprimirSegundaVia' WHERE RESI_DS LIKE 'imprimirSegundaVia.do';


COMMIT;