insert into PARAMETRO_SISTEMA 
(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,
PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select sq_pmsi_cd.nextval,
'PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR',
'Indica qual a��o ser� executada para um recebimento a menor.',
'0',
null,
'1',
'1',
'1',
current_timestamp,
null,
(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'ARRECADA��O')
from dual where not exists
(select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO like 'PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR');
commit;