UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoDocumento' WHERE MENU_DS_URL LIKE '%exibirPesquisaTipoDocumento.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoDocumento' WHERE RESI_DS LIKE 'exibirPesquisaTipoDocumento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirIncluirTipoDocumento' WHERE RESI_DS LIKE 'exibirIncluirTipoDocumento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlterarTipoDocumento' WHERE RESI_DS LIKE 'exibirAlterarTipoDocumento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoDocumento' WHERE RESI_DS LIKE 'removerTipoDocumento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirModeloTipoDocumento' WHERE RESI_DS LIKE 'exibirModeloTipoDocumento.do';