INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA',
    'Quantidade minima de registros da medicao horaria para calculo da medicao diaria',
    '12',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    4
);

INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA',
    'Quantidade de medicoes diarias para o calculo da media diaria quando nao houver a minima horaria',
    '90',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    4
);
COMMIT;