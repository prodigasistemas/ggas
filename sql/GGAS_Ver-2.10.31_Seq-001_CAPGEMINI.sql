ALTER TABLE SEGMENTO 
ADD (SEGM_TIPO_CONSUMO_FATURAMENTO NUMBER(9,2));
COMMENT ON COLUMN "SEGMENTO"."SEGM_TIPO_CONSUMO_FATURAMENTO" IS 'Tipo de visualização da fatura';

INSERT INTO ENTIDADE_CLASSE (ENCL_CD, ENCL_DS, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_IN_USO, ENCL_TM_ULTIMA_ALTERACAO) 
VALUES (SQ_ENCL_CD.nextval, 'Exibição da Cascata Tarifária e Tarifa Média', 'CASMED',  '1', '1', TO_TIMESTAMP('2018-11-22 15:35:08.280126000', 'YYYY-MM-DD HH24:MI:SS.FF'));

INSERT INTO ENTIDADE_CONTEUDO (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
VALUES (SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS_ABREVIADO LIKE 'CASMED'), 'Cascata Tarifária', 'CASCTA', '1', '0', '1', TO_TIMESTAMP('2018-11-22 16:22:05.461000000', 'YYYY-MM-DD HH24:MI:SS.FF'), null);

INSERT INTO ENTIDADE_CONTEUDO (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
VALUES (SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS_ABREVIADO LIKE 'CASMED'), 'Tarifa Média', 'TMEDIA', '1', '0', '1', TO_TIMESTAMP('2018-11-22 16:22:05.461000000', 'YYYY-MM-DD HH24:MI:SS.FF'), null);

INSERT INTO CONSTANTE_SISTEMA (COST_CD, TABE_CD, MOSI_CD, COST_NM, COST_DS, COST_VL, COST_CD_CLASSE, COST_NR_VERSAO, COST_IN_USO, COST_TM_ULTIMA_ALTERACAO)
VALUES (SQ_COST_CD.nextval, 
        (SELECT TABE_CD FROM TABELA WHERE TABE_NM_MNEMONICO = 'ENCO'),
        (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Faturamento'),
        'C_TIPO_CONSUMO_FATURAMENTO',
        'Indica o tipo de consumo faturamento',
        (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS_ABREVIADO = 'CASMED'),
        NULL,
        '1',
        '1',
        TO_TIMESTAMP('2018-11-22 16:22:05.461000000', 'YYYY-MM-DD HH24:MI:SS.FF'));

update SEGMENTO set SEGM_TIPO_CONSUMO_FATURAMENTO= (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS_ABREVIADO ='TMEDIA');

commit;