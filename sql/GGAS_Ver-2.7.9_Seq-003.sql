insert into PARAMETRO_SISTEMA 
(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,
PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select sq_pmsi_cd.nextval,
	'PARAMETRO_PDD_VALOR_LIMITE_ANUAL',
	'Valor limite para caracterizar PDD no ano (R$)',
	'100000',
	null,
	'1',
	'1',
	'1',
	current_timestamp,
	null,
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'CONT%BIL%')
from dual where not exists
(select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO like 'PARAMETRO_PDD_VALOR_LIMITE_ANUAL');

insert into PARAMETRO_SISTEMA 
(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,
PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select sq_pmsi_cd.nextval,
	'PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL',
	'Valor limite para caracterizar PDD no semestre (R$)',
	'15000',
	null,
	'1',
	'1',
	'1',
	current_timestamp,
	null,
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'CONT%BIL%')
from dual where not exists
(select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO like 'PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL');

update evento_comercial set evco_ds = 'PERDA CRÉDITO SEMESTRAL' where evco_cd = 118;
update evento_comercial set evco_ds = 'PERDA CRÉDITO ANUAL' where evco_cd = 119;

commit;

