--####################################### CRIAÇÃO MENU LEVANTAMENTO DE MERCADO E AGENTE ####################################################
UPDATE menu SET menu_cd_pai = (SELECT menu_cd FROM menu WHERE menu_ds = 'Contrato' AND menu_cd_pai  IS NULL) WHERE menu_ds = 'Levantamento de Mercado' AND MENU_DS_URL is null;
                
INSERT INTO menu (menu_cd, menu_cd_pai, menu_ds, menu_nr_versao, menu_in_uso, menu_tm_ultima_alteracao, menu_in_alcada, menu_nr_ordem,
resi_cd, menu_ds_url) SELECT sq_menu_cd.NEXTVAL, ((SELECT menu_cd FROM menu WHERE menu_ds = 'Levantamento de Mercado'  and menu_ds_url is null)),
'Agente',  0, 1, CURRENT_TIMESTAMP, 0, 10, NULL, 
'exibirPesquisaAgente' FROM dual WHERE NOT EXISTS (SELECT * 
FROM menu WHERE menu_ds = 'Agente');

INSERT INTO menu (menu_cd, menu_cd_pai, menu_ds, menu_nr_versao, menu_in_uso, menu_tm_ultima_alteracao, menu_in_alcada, menu_nr_ordem,
resi_cd, menu_ds_url) SELECT sq_menu_cd.NEXTVAL, ((SELECT menu_cd FROM menu WHERE menu_ds = 'Levantamento de Mercado' AND menu_ds_url IS NULL)),
'Levantamento de Mercado',  0, 1, CURRENT_TIMESTAMP, 0, 20, NULL, 
'exibirPesquisaLevantamentoMercado' FROM dual WHERE NOT EXISTS (SELECT * 
FROM menu WHERE menu_ds = 'Levantamento de Mercado' and menu_cd_pai = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' AND menu_ds_url IS NULL));

				
--Mudança de Menu pai do Levantamento de Mercado
UPDATE menu SET menu_nr_ordem = '25' WHERE menu_ds = 'Levantamento de Mercado' AND menu_cd_pai = (SELECT menu_cd FROM menu WHERE menu_ds = 'Contrato' AND menu_cd_pai  IS NULL);
UPDATE menu SET menu_nr_ordem = '15' WHERE menu_ds = 'Contrato Adequação Parceria' AND menu_cd_pai = (SELECT menu_cd FROM menu WHERE menu_ds = 'Contrato' AND menu_cd_pai  IS NULL);


-- ################################### OPERAÇÃO SISTEMA PARA AGENTE ####################################
 insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'PESQUISAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%PESQUISAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'INCLUIR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%INCLUIR%' ); 


    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'DETALHAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%DETALHAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'REMOVER', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%REMOVER%' ); 
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'ALTERAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%ALTERAR%' ); 
  
  -- ################################### RECURSO SISTEMA PARA AGENTE ####################################
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirPesquisaAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%PESQUISAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirPesquisaAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirInclusaoAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%INCLUIR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirInclusaoAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirAlteracaoAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%ALTERAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirAlteracaoAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirDetalhamentoAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%DETALHAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirDetalhamentoAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'removerAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%REMOVER%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'removerAgente') ;
  
  -- ################################### PERMISSÃO SISTEMA PARA AGENTE ####################################
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'PESQUISAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'PESQUISAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );

  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'INCLUIR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'INCLUIR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'ALTERAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'ALTERAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'DETALHAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'DETALHAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'REMOVER' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'REMOVER' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );
  
  
   -- ################################### OPERAÇÃO SISTEMA LEVANTAMENTO DE MERCADO####################################
 insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'PESQUISAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%PESQUISAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'INCLUIR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%INCLUIR%' ); 


    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'DETALHAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%DETALHAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'REMOVER', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%REMOVER%' ); 
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'ALTERAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%ALTERAR%' ); 

   -- ################################### RECURSO SISTEMA LEVANTAMENTO DE MERCADO ####################################
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirPesquisaLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%PESQUISAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirPesquisaLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirInclusaoLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%INCLUIR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirInclusaoLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirAlteracaoLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%ALTERAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirAlteracaoLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirDetalhamentoLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%DETALHAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirDetalhamentoLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'removerLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%REMOVER%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'removerLevantamentoMercado') ;
  

    -- ################################### PERMISSÃO SISTEMA LEVANTAMENTO DE MERCADO ####################################
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'PESQUISAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'PESQUISAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );

  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'INCLUIR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'INCLUIR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'ALTERAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'ALTERAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'DETALHAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'DETALHAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'REMOVER' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'REMOVER' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  
  --################################# Entidades Classes Serão Utilizadas no Levantamento de Mercado##################################### --
  
  INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Fornecedores de Outros Combustíveis' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Fornecedores de Outros Combustíveis');
      
      INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Aparelhos Maior Índice de Consumo' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Aparelhos Maior Índice de Consumo');
      
         INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Tipos de Fontes Energéticas' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Tipos de Fontes Energéticas');
	  
	  INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Estado da Rede' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Estado da Rede');
  
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_FORNECEDORES_OUTROS_COMBUSTIVEIS',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Fornecedores de Outros Combustíveis') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_FORNECEDORES_OUTROS_COMBUSTIVEIS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_APARELHOS_MAIOR_INDICE_CONSUMO',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Aparelhos Maior Índice de Consumo') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_APARELHOS_MAIOR_INDICE_CONSUMO');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_TIPOS_FONTES_ENERGETICAS',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Tipos de Fontes Energéticas') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_TIPOS_FONTES_ENERGETICAS');
 
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_ESTADO_REDE_GLP',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Estado da Rede') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_ESTADO_REDE_GLP');


--############################################ Parâmetro Sistema - Serão Utilizadas no Manter Proposta#########################################
Insert into PARAMETRO_SISTEMA (PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select SQ_PMSI_CD.nextval,'ENVIAR_EMAIL_AO_SALVAR_PROPOSTA',
'Indica se Deve Ser Enviado Um Email Para o Contato do Imóvel Ao Salvar a Proposta.','0',null,'1','1','1',
current_timestamp,null,'4' from dual where not exists (select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO = 'ENVIAR_EMAIL_AO_SALVAR_PROPOSTA');

Insert into PARAMETRO_SISTEMA (PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select SQ_PMSI_CD.nextval,'OBRIGATORIO_CONTATO_DO_IMOVEL_PARA_INCLUSAO_PROPOSTA',
'Indica se è Obrigatório o Imóvel Possuir Contato Para Incluir Proposta.','0',null,'1','1','1',
current_timestamp,null,'4' from dual where not exists (select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO = 'OBRIGATORIO_CONTATO_DO_IMOVEL_PARA_INCLUSAO_PROPOSTA');

--############################################ Adiciona Na Tabela IMOVEL, Relacionamento com TABELA AGENTE########################################3
ALTER TABLE IMOVEL DROP COLUMN AGEN_CD;
--ALTER TABLE IMOVEL DROP CONSTRAINT FK_AGENTE_IMOVEL;

ALTER TABLE IMOVEL add AGET_CD number(9, 0);
ALTER TABLE IMOVEL ADD CONSTRAINT FK_AGENTE_IMOVEL FOREIGN KEY ( AGET_CD ) REFERENCES AGENTE ( AGET_CD );


--###################################3########Altera Nome Menu Tipo botijão ######################################################################
UPDATE MENU SET MENU_DS  = 'Tipo de Cilindro' WHERE MENU_DS LIKE '%Tipo de Botijão%';


--############################################ Novas Situações do Imóvel - SERÀ USADA NO LEVANTAMENTO DE MERCADO ##################################
INSERT into IMOVEL_SITUACAO 
select SQ_IMSI_CD.NEXTVAL, 
'EM PROSPECÇÃO', 
0, 
1, 
current_timestamp, 
2 from dual where not exists (select * from IMOVEL_SITUACAO where IMSI_DS = 'EM PROSPECÇÃO');

INSERT into IMOVEL_SITUACAO 
select SQ_IMSI_CD.NEXTVAL, 
'PROSPECTADO', 
0, 
1, 
current_timestamp, 
2 from dual where not exists (select * from IMOVEL_SITUACAO where IMSI_DS = 'PROSPECTADO');

insert into constante_sistema select sq_cost_cd.nextval, (select TABE_CD from tabela where tabe_nm in ('ENTIDADE_CONTEUDO')), 
(SELECT MOSI_cD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) = UPPER('CADASTRO')), 'C_IMOVEL_EM_PROSPECCAO', 
'Indica Status Em Prospecção do Imóvel', (select IMSI_CD from IMOVEL_SITUACAO where UPPER(IMSI_DS) = UPPER('EM PROSPECÇÃO')), NULL, 0, 1,
CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM = 'C_IMOVEL_EM_PROSPECCAO');

insert into constante_sistema select sq_cost_cd.nextval, (select TABE_CD from tabela where tabe_nm in ('ENTIDADE_CONTEUDO')), 
(SELECT MOSI_cD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) = UPPER('CADASTRO')), 'C_IMOVEL_STATUS_PROSPECTADO', 
'Indica Status Prospectado do Imóvel', (select IMSI_CD from IMOVEL_SITUACAO where UPPER(IMSI_DS) = UPPER('PROSPECTADO')), NULL, 0, 1,
CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM = 'C_IMOVEL_STATUS_PROSPECTADO');


COMMIT;
 