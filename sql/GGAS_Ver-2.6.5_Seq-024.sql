update contrato_aba_atributo 
  set coaa_cd_dependencia = (
    SELECT coaa_cd FROM contrato_aba_atributo 
      where coaa_nm_coluna like 'indicadorProgramacaoConsumo')
  where coaa_nm_coluna like 'variacaoSuperiorQDC';
  
update contrato_aba_atributo 
  set coaa_in_depend_obrigatoria = 0
  where coaa_cd_dependencia = (SELECT coaa_cd FROM contrato_aba_atributo 
      where coaa_nm_coluna like 'indicadorProgramacaoConsumo');

commit;