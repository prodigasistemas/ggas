UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaPerfilParcelamento' WHERE MENU_DS_URL LIKE '%exibirPesquisaPerfilParcelamento%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirPerfilParcelamento' WHERE RESI_DS LIKE '%excluirPerfilParcelamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirPerfilParcelamento' WHERE RESI_DS LIKE '%incluirPerfilParcelamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoPerfilParcelamento' WHERE RESI_DS LIKE '%exibirInclusaoPerfilParcelamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarPerfilParcelamento' WHERE RESI_DS LIKE '%alterarPerfilParcelamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoPerfilParcelamento' WHERE RESI_DS LIKE '%exibirAlteracaoPerfilParcelamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoPerfilParcelamento' WHERE RESI_DS LIKE '%exibirDetalhamentoPerfilParcelamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarPerfilParcelamento' WHERE RESI_DS LIKE '%pesquisarPerfilParcelamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaPerfilParcelamento' WHERE RESI_DS LIKE '%exibirPesquisaPerfilParcelamento.do%';

COMMIT;