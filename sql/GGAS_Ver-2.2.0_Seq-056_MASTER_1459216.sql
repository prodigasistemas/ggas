Alter Table Medidor Add (Medi_Cd_Supervisorio Varchar2 (50)); 
COMMENT ON COLUMN MEDIDOR.MEDI_CD_SUPERVISORIO Is 'Indica o código do Medidor no Sistema supervisório ';

Alter Table Super_Medicao_Diaria Add(SUMD_IN_MEDIDOR NUMBER (1));
COMMENT ON COLUMN Super_Medicao_Diaria.SUMD_IN_MEDIDOR Is 'Indica se o código do supervisório é referente à medidor. 1 = sim, 0 ou null = não';

ALTER TABLE corretor_vazao drop (cova_in_virtual, cova_composicao_virtual);


alter table medidor drop column medi_in_virtual;
alter table medidor add MEDI_IN_MODO_USO number(1);
update medidor set MEDI_IN_MODO_USO = 0 where MEDI_IN_MODO_USO is null;
COMMENT ON COLUMN medidor.MEDI_IN_MODO_USO Is 'Indica se o medidor é normal = 0, virtual = 1 ou independente=2.';
alter table MEDIDOR_INSTALACAO modify (POCN_CD NUMERIC(9,0) null);
alter table CORRETOR_VAZAO_OPE_HIST modify (POCN_CD NUMERIC(9,0) null);




alter table medidor add ENCO_CD_SITUACAO_MEDI_IND NUMERIC(9,0) null;
COMMENT ON COLUMN medidor.ENCO_CD_SITUACAO_MEDI_IND Is 'Chave primaria da tabela ENTIDADE_CONTEUDO que indica a situação da associação do medidor independente.';

UPDATE medidor
SET ENCO_CD_SITUACAO_MEDI_IND =
  (SELECT ENCO_CD
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Aguardando Ativação'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Situação Associação Medidor Independente'
    )
  )
WHERE ENCO_CD_SITUACAO_MEDI_IND IS NULL
AND MEDI_IN_MODO_USO= 2;





ALTER TABLE entidade_classe MODIFY ENCL_DS VARCHAR2(60);
INSERT
INTO entidade_classe
  (
    ENCL_CD,
    ENCL_DS,
    ENCL_DS_ABREVIADO,
    ENCL_NR_VERSAO,
    ENCL_IN_USO,
    ENCL_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_ENCL_CD.nextval,
  'Situação Associação Medidor Independente',
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT * FROM entidade_classe WHERE ENCL_DS = 'Situação Associação Medidor Independente'
  );



INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM entidade_classe
  WHERE ENCL_DS = 'Situação Associação Medidor Independente'
  ),
  'Aguardando Ativação',
  NULL,
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Aguardando Ativação'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Situação Associação Medidor Independente'
    )
  );
  

INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM entidade_classe
  WHERE ENCL_DS = 'Situação Associação Medidor Independente'
  ),
  'Ativo',
  NULL,
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Ativo'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Situação Associação Medidor Independente'
    )
  );
  

INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM entidade_classe
  WHERE ENCL_DS = 'Situação Associação Medidor Independente'
  ),
  'Bloqueado',
  NULL,
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Bloqueado'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Situação Associação Medidor Independente'
    )
  );

INSERT INTO constante_sistema
SELECT sq_cost_cd.nextval,
  (SELECT TABE_CD FROM tabela WHERE tabe_nm IN ('ENTIDADE_CONTEUDO')
  ),
  (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) like 'MEDI%O'
  ),
  'C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO',
  'Indica a situação aguardando ativação para associação medidor independente',
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE enco_ds ='Aguardando Ativação'
  AND encl_cd   =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE ENCL_DS = 'Situação Associação Medidor Independente'
    )
  ),
  NULL,
  0,
  1,
  CURRENT_TIMESTAMP
FROM DUAL
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM = 'C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO'
  );


INSERT INTO constante_sistema
SELECT sq_cost_cd.nextval,
  (SELECT TABE_CD FROM tabela WHERE tabe_nm IN ('ENTIDADE_CONTEUDO')
  ),
  (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) like 'MEDI%O'
  ),
  'C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO',
  'Indica a situação Ativo para associação medidor independente',
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE enco_ds ='Ativo'
  AND encl_cd   =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE ENCL_DS = 'Situação Associação Medidor Independente'
    )
  ),
  NULL,
  0,
  1,
  CURRENT_TIMESTAMP
FROM DUAL
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM = 'C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO'
  );


INSERT
INTO constante_sistema
SELECT sq_cost_cd.nextval,
  (SELECT TABE_CD FROM tabela WHERE tabe_nm IN ('ENTIDADE_CONTEUDO')
  ),
  (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) like 'MEDI%O'
  ),
  'C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_BLOQUEADO',
  'Indica a situação bloqueado para associação medidor independente',
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE enco_ds ='Bloqueado'
  AND encl_cd   =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE ENCL_DS = 'Situação Associação Medidor Independente'
    )
  ),
  NULL,
  0,
  1,
  CURRENT_TIMESTAMP
FROM DUAL
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM = 'C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_BLOQUEADO'
  );



commit;