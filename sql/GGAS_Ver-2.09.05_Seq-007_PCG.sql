
UPDATE parametro_sistema
SET pmsi_vl_parametro =
  (SELECT cost_vl
  FROM constante_sistema
  WHERE cost_nm = 'C_NAO_BAIXA_RECEBIMENTO_MENOR'
  )
WHERE pmsi_cd_parametro='PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR'
AND (pmsi_vl_parametro = '0'
OR pmsi_vl_parametro  IS NULL);
commit;