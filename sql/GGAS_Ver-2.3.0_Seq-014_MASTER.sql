Insert into CONSTANTE_SISTEMA(
        COST_CD,
        TABE_CD,
        MOSI_CD,
        COST_NM,
        COST_DS,
        COST_VL,
        COST_CD_CLASSE,
        COST_NR_VERSAO,
        COST_IN_USO,
        COST_TM_ULTIMA_ALTERACAO
)
select
        SQ_COST_CD.nextval,
        '90',
        '1',
        'C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO',
        'Indica que o sistema deverá usar um valor percentual no cálculo da substituição tributária',
        (select ENCO_CD from ENTIDADE_CONTEUDO where enco_ds like 'MARGEM DE VALOR AGREGADO'),
        '71',
        '0',
        '1',
        current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where cost_nm like 'C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO');


Insert into CONSTANTE_SISTEMA(
        COST_CD,
        TABE_CD,
        MOSI_CD,
        COST_NM,
        COST_DS,
        COST_VL,
        COST_CD_CLASSE,
        COST_NR_VERSAO,
        COST_IN_USO,
        COST_TM_ULTIMA_ALTERACAO
)
select
        SQ_COST_CD.nextval,
        '90',
        '1',
        'C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL',
        'Indica que o sistema deverá usar um valor em Real no cálculo da substituição tributária',
        (select ENCO_CD from ENTIDADE_CONTEUDO where enco_ds like 'PREÇO MÉDIO POND. A CONS FINAL'),
        '71',
        '0',
        '1',
        current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where cost_nm like 'C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL');

COMMIT;