UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaAreaConstruida' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=areaConstruida';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaAreaConstruida' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=areaConstruida';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarAreaConstruida' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=areaConstruida';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoAreaConstruida' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=areaConstruida';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirAreaConstruida' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=areaConstruida';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirFaixaAreaConstruida' WHERE RESI_DS LIKE 'incluirFaixaAreaConstruida.do?acao=inserirFaixaAreaConstruida%tela=areaConstruida';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerFaixaAreaConstruida' WHERE RESI_DS LIKE 'removerFaixaAreaConstruida.do?acao=removerFaixaAreaConstruida%tela=areaConstruida';

DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=areaConstruida';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=areaConstruida';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=areaConstruida';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=areaConstruida';

commit;