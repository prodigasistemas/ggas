
UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaAnormalidade' WHERE MENU_DS_URL LIKE '%Anormalidade%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarAnormalidadeConsumo' WHERE RESI_DS LIKE '%alterarAnormalidadeConsumo.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaAnormalidade' WHERE RESI_DS LIKE '%exibirPesquisaAnormalidade.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarAnormalidade' WHERE RESI_DS LIKE '%pesquisarAnormalidade.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoAnormalidadeLeitura' WHERE RESI_DS LIKE '%exibirDetalhamentoAnormalidadeLeitura.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoAnormalidade' WHERE RESI_DS LIKE '%exibirAlteracaoAnormalidade.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoAnormalidade' WHERE RESI_DS LIKE '%exibirInclusaoAnormalidade.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarAnormalidadeConsumo' WHERE RESI_DS LIKE '%alterarAnormalidadeConsumo.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarAnormalidadeLeitura' WHERE RESI_DS LIKE '%alterarAnormalidadeLeitura.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirAnormalidade' WHERE RESI_DS LIKE '%excluirAnormalidade.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirAnormalidade' WHERE RESI_DS LIKE '%incluirAnormalidade.do';

COMMIT;
