INSERT INTO entidade_classe cl VALUES (sq_encl_cd.nextval, 'Tipos de Combustíveis', NULL, 0, 1, SYSDATE);
commit;

INSERT INTO entidade_conteudo eco VALUES (sq_enco_cd.nextval,(SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Tipos de Combustíveis'), 'ELETRICIDADE',null,0,0,1,sysdate,null);
INSERT INTO entidade_conteudo eco VALUES (sq_enco_cd.nextval,(SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Tipos de Combustíveis'), 'GÁS NATURAL',null,0,0,1,sysdate,null);
INSERT INTO entidade_conteudo eco VALUES (sq_enco_cd.nextval,(SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Tipos de Combustíveis'), 'GLP',null,0,0,1,sysdate,null);
INSERT INTO entidade_conteudo eco VALUES (sq_enco_cd.nextval,(SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Tipos de Combustíveis'), 'LENHA',null,0,0,1,sysdate,null);
INSERT INTO entidade_conteudo eco VALUES (sq_enco_cd.nextval,(SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Tipos de Combustíveis'), 'ÓLEO COMBUSTIVEL',null,0,0,1,sysdate,null);
INSERT INTO entidade_conteudo eco VALUES (sq_enco_cd.nextval,(SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Tipos de Combustíveis'), 'ÓLEO DIESEL',null,0,0,1,sysdate,null);
INSERT INTO entidade_conteudo eco VALUES (sq_enco_cd.nextval,(SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Tipos de Combustíveis'), 'OUTRO',null,0,0,1,sysdate,null);
commit;

-- Add/modify columns 
alter table IMOVEL add enco_cd_tipo_combustivel NUMBER(9);
-- Add comments to the columns 
comment on column IMOVEL.enco_cd_tipo_combustivel
  is 'Chave para a tabela ENTIDADE_CONTEUDO com as informações dos tipos de combustíveis';

-- Create/Recreate primary, unique and foreign key constraints 
alter table IMOVEL
  add constraint FK_TIPO_COMBUSTIVEL foreign key (ENCO_CD_TIPO_COMBUSTIVEL)
  references entidade_conteudo (ENCO_CD);
  