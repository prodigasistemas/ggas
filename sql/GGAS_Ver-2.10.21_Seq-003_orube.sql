UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoTelefone' WHERE MENU_DS_URL LIKE '%tipoTelefone%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoTipoTelefone' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%tela=tipoTelefone';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoTelefone' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%tela=tipoTelefone';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTipoTelefone' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%tela=tipoTelefone';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoTipoTelefone' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%tela=tipoTelefone';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirTipoTelefone' WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%tela=tipoTelefone';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTipoTelefone' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%tela=tipoTelefone';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTipoTelefone' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%tela=tipoTelefone';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoTelefone' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%tela=tipoTelefone';
