
/* ************************* AJUSTE DE MEDIDOR *************************** */
/* Modifica��o do comportamento do medidor (independente, normal ou virtual) */
alter table medidor drop column MEDI_IN_MODO_USO;
alter table medidor add ENCO_CD_MODO_USO NUMERIC(9,0) null;
COMMENT ON COLUMN medidor.ENCO_CD_MODO_USO Is 'Chave primaria para entidade conteúdo que indica se o medidor é normal, virtual ou independente.';


/* *************************************************************************************/



/* ************************ Entidade Classe ************************** */
INSERT
INTO entidade_classe
  (
    ENCL_CD,
    ENCL_DS,
    ENCL_DS_ABREVIADO,
    ENCL_NR_VERSAO,
    ENCL_IN_USO,
    ENCL_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_ENCL_CD.nextval,
  'Modo de Uso Medidor',
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT * FROM entidade_classe WHERE ENCL_DS = 'Modo de Uso Medidor'
  );
/* ******************************************************************* */



/* *********************** Entidade Conteudo ************************* */
/* ************** Normal **************/
INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM entidade_classe
  WHERE ENCL_DS = 'Modo de Uso Medidor'
  ),
  'Normal',
  NULL,
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Normal'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Modo de Uso Medidor'
    )
  );
  
/* ****************** Virtual ******************/
INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM entidade_classe
  WHERE ENCL_DS = 'Modo de Uso Medidor'
  ),
  'Virtual',
  NULL,
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Virtual'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Modo de Uso Medidor'
    )
  );
  
/* ****************** Independente ******************/
INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM entidade_classe
  WHERE ENCL_DS = 'Modo de Uso Medidor'
  ),
  'Independente',
  NULL,
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Independente'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Modo de Uso Medidor'
    )
  );
/* ******************************************************************* */



/* ************************** Constantes ***************************** */
/* ************** Virtual **************/
INSERT INTO constante_sistema
SELECT sq_cost_cd.nextval,
  (SELECT TABE_CD FROM tabela WHERE tabe_nm IN ('ENTIDADE_CONTEUDO')
  ),
  (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) like 'MEDI%O'
  ),
  'C_MODO_USO_MEDIDOR_VIRTUAL',
  'Indica o modo de uso medidor virtual',
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE enco_ds ='Virtual'
  AND encl_cd   =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE ENCL_DS = 'Modo de Uso Medidor'
    )
  ),
  NULL,
  0,
  1,
  CURRENT_TIMESTAMP
FROM DUAL
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM = 'C_MODO_USO_MEDIDOR_VIRTUAL'
  );
/* ***************** Normal *****************/
INSERT INTO constante_sistema
SELECT sq_cost_cd.nextval,
  (SELECT TABE_CD FROM tabela WHERE tabe_nm IN ('ENTIDADE_CONTEUDO')
  ),
  (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) like 'MEDI%O'
  ),
  'C_MODO_USO_MEDIDOR_NORMAL',
  'Indica o modo de uso medidor normal',
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE enco_ds ='Normal'
  AND encl_cd   =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE ENCL_DS = 'Modo de Uso Medidor'
    )
  ),
  NULL,
  0,
  1,
  CURRENT_TIMESTAMP
FROM DUAL
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM = 'C_MODO_USO_MEDIDOR_NORMAL'
  );
/* ***************** Independente *****************/
INSERT
INTO constante_sistema
SELECT sq_cost_cd.nextval,
  (SELECT TABE_CD FROM tabela WHERE tabe_nm IN ('ENTIDADE_CONTEUDO')
  ),
  (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) like 'MEDI%O'
  ),
  'C_MODO_USO_MEDIDOR_INDEPENDENTE',
  'Indica o modo de uso medidor independente',
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE enco_ds ='Independente'
  AND encl_cd   =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE ENCL_DS = 'Modo de Uso Medidor'
    )
  ),
  NULL,
  0,
  1,
  CURRENT_TIMESTAMP
FROM DUAL
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM = 'C_MODO_USO_MEDIDOR_INDEPENDENTE'
  );
/* ******************************************************************* */


/* ************************* ANORMALIDADES *************************** */

Insert into SUPER_MEDICAO_ANORMALIDADE values (21,'ENDERECO REMOTO NAO LOCALIZADO NO GGAS.','1','1','0',current_timestamp,'1');
Insert into SUPER_MEDICAO_ANORMALIDADE values (22,'NAO EXISTE MEDIDOR VIRTUAL PARA MEDIDOR INDEPENDENTE.','1','1','0',current_timestamp,'1');
Insert into SUPER_MEDICAO_ANORMALIDADE values (23,'NAO EXISTE PONTO CONSUMO PARA MEDIDOR VIRTUAL.','1','1','0',current_timestamp,'1');
Insert into SUPER_MEDICAO_ANORMALIDADE values (24,'MEDIDOR SEM CORRETOR VAZAO INSTALADO.','1','1','0',current_timestamp,'1');
Insert into SUPER_MEDICAO_ANORMALIDADE values (25,'PONTOS DE CONSUMO NAO POSSUEM MESMO GRUPO FATURAMENTO.','1','1','0',current_timestamp,'1');
Insert into SUPER_MEDICAO_ANORMALIDADE values (26,'PONTOS DE CONSUMO NAO POSSUEM MESMA HORA INICIAL LEITURA.','1','1','0',current_timestamp,'1');
Insert into SUPER_MEDICAO_ANORMALIDADE values (27,'NAO EXISTE MEDIDOR INSTALADO.','1','1','0',current_timestamp,'1');

/* ******************************************************************* */



/* ************************ CONSUMO HISTORICO ************************ */
Alter Table consumo_historico Add (COHI_COMPOSICAO_VIRTUAL VARCHAR(500));
alter table consumo_historico add MEDI_CD  NUMBER(9, 0);
ALTER TABLE consumo_historico ADD constraint FK_COHI_MEDI FOREIGN KEY 
(MEDI_CD) references MEDIDOR(MEDI_CD);
/* ******************************************************************* */

/* ************************ ANORMALIDADE CONSUMO ************************ */

INSERT
INTO CONSUMO_ANORMALIDADE
  (
    COAN_CD,
    COAN_DS,
    COAN_DS_ABREVIADO,
    COAN_IN_BLOQUEIA_FATURAMENTO,
    COAN_NR_VERSAO,
    COAN_IN_USO,
    COAN_NR_VEZES_BLOQUEIO,
    COAN_TM_ULTIMA_ALTERACAO,
    COAN_NR_OCOR_ANORMALIDADE
  )
  VALUES
  (
    '22',
    'MEDIDOR COMPOSICAO S/ MEDICAO',
    'MESME',
    '1',
    '0',
    '1',
    NULL,
    current_timestamp,
    null
  ); 
INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD
  FROM tabela
  WHERE upper(tabe_nm)=upper('CONSUMO_ANORMALIDADE')
  ),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS) like upper('Medi%o')
  ),
  'C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO',
  'Indica a anormalidade medidor da composicao sem medicao',
  22,
  NULL,
  1,1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM='C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO'
  );
/* ******************************************************************* */

/* ********************** Consumo Historico ************************** */

Alter Table Consumo_historico Add (cohi_in_medicao_ind_atual Number (1));
COMMENT ON COLUMN Consumo_historico.cohi_in_medicao_ind_atual IS 'Indica se é medição atual para medidor independente. nao = 0 ou nulo e sim = 1';

/* ******************************************************************* */

ALTER TABLE MEDIDOR_INSTALACAO MODIFY MELI_CD null;

UPDATE medidor
SET ENCO_CD_MODO_USO =
  (SELECT ENCO_CD
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Normal'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Modo de Uso Medidor'
    )
  )
WHERE ENCO_CD_MODO_USO IS NULL;


commit;