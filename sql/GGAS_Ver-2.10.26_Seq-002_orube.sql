UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaPapel' WHERE MENU_DS_URL LIKE 'exibirPesquisaPapel.do?acao=exibirPesquisaPapel';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaPapel' WHERE RESI_DS LIKE 'exibirPesquisaPapel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoPapel' WHERE RESI_DS LIKE 'exibirInclusaoPapel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoPapel' WHERE RESI_DS LIKE 'exibirDetalhamentoPapel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoPapel' WHERE RESI_DS LIKE 'exibirAlteracaoPapel.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarPapel' WHERE RESI_DS LIKE 'pesquisarPapeis.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarPapel' WHERE RESI_DS LIKE 'atualizarPapel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirPapel' WHERE RESI_DS LIKE 'incluirPapel.do';

COMMIT;