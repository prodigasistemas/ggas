UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRecebimento' WHERE MENU_DS_URL LIKE 'exibirPesquisaRecebimento.do?acao=exibirPesquisaRecebimento%tela=recebimento';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoRecebimento' WHERE RESI_DS LIKE 'exibirDetalhamentoRecebimento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoRecebimento' WHERE RESI_DS LIKE 'exibirInclusaoRecebimento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoRecebimento' WHERE RESI_DS LIKE 'exibirAlteracaoRecebimento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarRecebimento' WHERE RESI_DS LIKE 'pesquisarRecebimento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirRecebimento' WHERE RESI_DS LIKE 'incluirRecebimento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarRecebimento' WHERE RESI_DS LIKE 'alterarRecebimento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'estornarRecebimento' WHERE RESI_DS LIKE 'estornarRecebimento.do';

COMMIT;