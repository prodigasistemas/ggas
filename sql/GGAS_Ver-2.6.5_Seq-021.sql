insert into PARAMETRO_SISTEMA 
(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,
PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select sq_pmsi_cd.nextval,
	'CONTABILIZAR_DESCONTO_TARIFA',
	'Contabilizar desconto da tarifa 1-Sim; 0-Nao',
	'1',
	null,
	'1',
	'1',
	'1',
	current_timestamp,
	null,
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'CONT%BIL%')
from dual where not exists
(select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO like 'CONTABILIZAR_DESCONTO_TARIFA');

commit;

