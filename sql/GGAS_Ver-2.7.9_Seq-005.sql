insert into CONSTANTE_SISTEMA
(COST_CD,TABE_CD,MOSI_CD,COST_NM,
COST_DS,COST_VL,COST_CD_CLASSE,
COST_NR_VERSAO,COST_IN_USO,
COST_TM_ULTIMA_ALTERACAO)
select sq_cost_cd.nextval,
	(select tabe_cd from TABELA where TABE_NM LIKE 'CONSTANTE_SISTEMA'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'RELAT%'),
	'RELATORIO_BOLETO',
	'Nome do arquivo de relátorio para boletos',
	'relatorioBoletoBancarioPadrao.jasper',
	null,
	1,1,current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where COST_NM like 'RELATORIO_BOLETO');
commit;