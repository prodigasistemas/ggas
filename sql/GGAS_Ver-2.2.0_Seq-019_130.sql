
alter table contrato_adequacao_parceria add INFI_CD  NUMBER(9, 0);

alter table contrato_adequacao_parceria add COAP_DT_CADASTRO TIMESTAMP;

alter table contrato_adequacao_parceria add COAP_DT_INICIO_VIGENCIA TIMESTAMP;

ALTER TABLE contrato_adequacao_parceria ADD CONSTRAINT FK_INFI_COAP FOREIGN KEY ( INFI_CD ) REFERENCES indice_financeiro ( INFI_CD );

commit;