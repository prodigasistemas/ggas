Insert into LEITURA_ANORMALIDADE (LEAN_CD,LEAN_DS,LEAN_DS_ABREVIADO,LEAC_CD_SEM_LEITURA,LEAC_CD_COM_LEITURA,LEAL_CD_SEM_LEITURA,LEAL_CD_COM_LEITURA,DOTI_CD,
LEAN_DS_MENSAGEM_LEITURA,LEAN_DS_MENSAGEM_MANUTENCAO,LEAN_DS_MENSAGEM_PREVENCAO,LEAN_NR_VEZES_CONSECUTIVAS,LEAN_IN_ACEITA_LEITURA,LEAN_IN_LISTAGEM,LEAN_IN_RETENCAO_CONTA,
LEAN_IN_ISENCAO,LEAN_IN_CREDITO_CONSUMO,LEAN_IN_FOTO,LEAN_IN_RELATIVO_MEDIDOR,LEAN_IN_IMOVEL_SEM_MEDIDOR,LEAN_IN_BLOQUEIA_FATURAMENTO,LEAN_IN_USO_SISTEMA,LEAN_NR_VEZES_BLOQUEIO,
LEAN_NR_VERSAO,LEAN_IN_USO,LEAN_TM_ULTIMA_ALTERACAO,SRTI_CD) 
select SQ_LEAN_CD.nextval,'Consumo depois Leit. Bloq.',null,'6','3','5','1',null,null,null,null,null,'1',null,null,null,null,null,'1',null,'0','1',null,'4','1',
current_timestamp,null from dual where not exists (select * from leitura_anormalidade where upper(LEAN_DS) like 'CONSUMO DEPOIS LEIT. BLOQ.%' );

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'LEITURA_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDI%O'),
'C_ANORMALIDADE_LEITURA_CONSUMO_DEPOIS_LEITURA_BLOQUEIO','Indica a anormalidade de leitura consumo depois da leitura de bloqueio',
(select lean_cd from leitura_anormalidade where upper(lean_ds) like 'CONSUMO DEPOIS LEIT. BLOQ.'),null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm = 'C_ANORMALIDADE_LEITURA_CONSUMO_DEPOIS_LEITURA_BLOQUEIO');

Insert into LEITURA_ANORMALIDADE (LEAN_CD,LEAN_DS,LEAN_DS_ABREVIADO,LEAC_CD_SEM_LEITURA,LEAC_CD_COM_LEITURA,LEAL_CD_SEM_LEITURA,LEAL_CD_COM_LEITURA,DOTI_CD,
LEAN_DS_MENSAGEM_LEITURA,LEAN_DS_MENSAGEM_MANUTENCAO,LEAN_DS_MENSAGEM_PREVENCAO,LEAN_NR_VEZES_CONSECUTIVAS,LEAN_IN_ACEITA_LEITURA,LEAN_IN_LISTAGEM,LEAN_IN_RETENCAO_CONTA,
LEAN_IN_ISENCAO,LEAN_IN_CREDITO_CONSUMO,LEAN_IN_FOTO,LEAN_IN_RELATIVO_MEDIDOR,LEAN_IN_IMOVEL_SEM_MEDIDOR,LEAN_IN_BLOQUEIA_FATURAMENTO,LEAN_IN_USO_SISTEMA,LEAN_NR_VEZES_BLOQUEIO,
LEAN_NR_VERSAO,LEAN_IN_USO,LEAN_TM_ULTIMA_ALTERACAO,SRTI_CD) 
select SQ_LEAN_CD.nextval,'Utiliz. da Leitura de Bloqueio',null,'6','3','5','1',null,null,null,null,null,'1',null,null,null,null,null,'1',null,'0','1',null,'4','1',
current_timestamp,null from dual where not exists (select * from leitura_anormalidade where upper(LEAN_DS) like 'UTILIZ. DA LEITURA DE BLOQUEIO%' );

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'LEITURA_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDI%O'),
'C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA','Indica a anormalidade de leitura de bloqueio utilizada',
(select lean_cd from leitura_anormalidade where upper(lean_ds) like 'UTILIZ. DA LEITURA DE BLOQUEIO'),null,'1','1',current_timestamp
from dual where not exists (select * from constante_sistema where cost_nm = 'C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA');

update ponto_consumo_situacao set pocs_in_leitura = 0, pocs_in_faturavel = 1 where pocs_ds like 'BLOQUEADO';

commit;
