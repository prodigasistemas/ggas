-- OC 1518815
	
INSERT
INTO ENTIDADE_CONTEUDO
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM ENTIDADE_CLASSE
  WHERE UPPER(ENCL_DS)='TIPO DE DOCUMENTO'
  ),
  'NOTA DE CRÉDITO',
  NULL,
  '1',
  '1',
  '1',
  CURRENT_TIMESTAMP,
  NULL
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE upper(enco_ds) = 'NOTA DE CRÉDITO'
  AND ENCL_CD          =
    (SELECT ENCL_CD
    FROM ENTIDADE_CLASSE
    WHERE UPPER(ENCL_DS)='TIPO DE DOCUMENTO'
    )
  );

  insert into documento_impressao_layout 
  select SQ_DOIL_CD.nextval,
  'Nota de Crédito',
  'Relatório Nota Crédito Bahia',
  'relatorioNotaCreditoBahia.jasper',
  (SELECT enco_cd FROM entidade_conteudo WHERE UPPER(enco_ds) like '%NOTA DE CR%DITO%' AND encl_cd = (SELECT encl_cd FROM entidade_classe WHERE UPPER(encl_ds)='TIPO DE DOCUMENTO')),1,0,current_timestamp
  FROM DUAL WHERE NOT EXISTS
  (select * from documento_impressao_layout  where doil_nm_arquivo  = 'relatorioNotaCreditoBahia.jasper' );
 
 insert into documento_impressao_layout 
  select SQ_DOIL_CD.nextval,
  'Nota de Crédito',
  'Relatório Nota Crédito',
  'relatorioNotaCredito.jasper',
  (SELECT enco_cd FROM entidade_conteudo WHERE UPPER(enco_ds) like '%NOTA DE CR%DITO%' AND encl_cd = (SELECT encl_cd FROM entidade_classe WHERE UPPER(encl_ds)='TIPO DE DOCUMENTO')),1,0,current_timestamp
  FROM DUAL WHERE NOT EXISTS
  (select * from documento_impressao_layout  where doil_nm_arquivo  = 'relatorioNotaCredito.jasper' );
  
 
  INSERT INTO constante_sistema SELECT sq_cost_cd.nextval, (SELECT TABE_CD FROM tabela WHERE tabe_nm in ('FATURA')), 
  (SELECT mosi_cd FROM modulo_sistema WHERE UPPER(MOSI_DS) = 'CADASTRO'), 'C_RELATORIO_NOTA_CREDITO', 
  'Chave Primaria da Tabela Entidade Conteudo Modelo Layout Impressoão Nota Crédito', (SELECT enco_cd FROM entidade_conteudo WHERE UPPER(enco_ds) like '%NOTA DE CRÉDITO%' AND encl_cd = (SELECT encl_cd FROM entidade_classe WHERE UPPER(encl_ds) like UPPER('%Tipo de Documento%'))), NULL, 0, 1,
  CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM constante_sistema WHERE cost_nm = 'C_RELATORIO_NOTA_CREDITO');
  
  
 
  COMMIT;
  
  
 