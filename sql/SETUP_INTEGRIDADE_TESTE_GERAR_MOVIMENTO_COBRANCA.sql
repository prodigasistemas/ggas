DELETE FROM COBRANCA_BANCARIA_MOVIMENTO WHERE DOCO_CD IN (SELECT DOCO_CD FROM DOCUMENTO_COBRANCA WHERE CLIE_CD IN (SELECT CLIE_CD FROM CLIENTE WHERE CLIE_NM = 'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL'));

DELETE FROM DOCUMENTO_COBRANCA_ITEM WHERE DOCO_CD IN (SELECT DOCO_CD FROM DOCUMENTO_COBRANCA WHERE CLIE_CD IN (SELECT CLIE_CD FROM CLIENTE WHERE CLIE_NM = 'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL'));

DELETE FROM FATURA_ITEM WHERE FATU_CD = 9999999;

DELETE FROM FATURA_GERAL WHERE FATU_CD_ATUAL IN (SELECT FATU_CD FROM FATURA WHERE CLIE_CD IN (SELECT CLIE_CD FROM CLIENTE WHERE CLIE_NM = 'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL'));

DELETE FROM FATURA WHERE CLIE_CD IN (SELECT CLIE_CD FROM CLIENTE WHERE CLIE_NM = 'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL');

DELETE FROM CONTRATO WHERE CONT_NR = '12345';

DELETE FROM DOCUMENTO_COBRANCA WHERE CLIE_CD IN (SELECT CLIE_CD FROM CLIENTE WHERE CLIE_NM = 'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL');

DELETE FROM ARRECADADOR_CONTRATO_CONVENIO WHERE ARCO_CD IN (SELECT ARCO_CD FROM ARRECADADOR_CONTRATO WHERE ARCO_NR_CONTRATO = 'SANTANDER');

DELETE FROM CONTA_BANCARIA WHERE COBA_DS = 'CONTA SANTANDER';

DELETE FROM AGENCIA WHERE AGEN_NM = 'AGENCIA SANTANDER';

DELETE FROM ARRECADADOR_CARTEIRA_COBRANCA WHERE ARCA_DS = 'CARTEIRA SANTANDER' AND ARCA_NR = 33;

DELETE FROM ARRECADADOR_CONTRATO WHERE ARCO_NR_CONTRATO = 'SANTANDER';

DELETE FROM ARRECADADOR WHERE BANC_CD IN (SELECT BANC_CD FROM BANCO WHERE BANC_NR = '033');

DELETE FROM BANCO WHERE BANC_NR = '033';

DELETE FROM CLIENTE_ENDERECO WHERE CLIE_CD IN (SELECT CLIE_CD FROM CLIENTE WHERE CLIE_NM = 'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL');

DELETE FROM CEP WHERE CEP_SG_UF = 'AL' AND CEP_NR = '57035-220';

DELETE FROM CLIENTE WHERE CLIE_NM = 'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL';

COMMIT;