INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_STATUS_AS',  'Status Autorizacao de Servico', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status Autorizacao de Servico'), NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_OCORRENCIA_AS', 'Ocorrência Autorização de Serviço', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Ocorrência Autorização de Serviço'), NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_LOCAL_AS', 'Local Autorização de Serviço', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Local Autorização de Serviço'), NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_CAUSA_AS', 'Causa Autorização de Serviço', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Causa Autorização de Serviço'), NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_SOLUCAO_AS', 'Solução Autorização de Serviço', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Solução Autorização de Serviço'), NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_CILINDRO_AS', 'CODIGO DE CILINDRO', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'CODIGO DE CILINDRO'), NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_TIPO_INST_AS', 'TIPO INSTALACAO', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'TIPO INSTALACAO'), NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO CONSTANTE_SISTEMA VALUES(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CLASSE'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'), 'C_ENT_CLASSE_ANORM_AS', 'Anormalidade de Leitura Autorização de Serviço', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Anormalidade de Leitura Autorizacão de Serviço'), NULL, 1, 1, CURRENT_TIMESTAMP);


INSERT INTO RECURSO_SISTEMA VALUES (SQ_RESI_CD.nextval, 'consultarMedidorMobile', 0,
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'INCLUIR' AND MENU_CD = 
(SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaServicoAutorizacao')), 1, 1, CURRENT_TIMESTAMP );

COMMIT;