insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'FINANCIAMENTO_TIPO'), (select mosi_cd from modulo_sistema where upper(mosi_ds) like 'COBRAN%A'), 'C_TIPO_RUBRICA_SERVICO', 'Indica se o tipo de rubrica de serviço.', (select fiti_cd from financiamento_tipo where upper(fiti_ds) like '%SERVI%O%'), null,'1','1',current_timestamp
from dual where not exists (select * from constante_sistema where cost_nm = 'C_TIPO_RUBRICA_SERVICO');

alter table rubrica add (RUBR_IN_ITEM_FATURAMENTO number(1) default (0) not null);
comment on column rubrica.RUBR_IN_ITEM_FATURAMENTO is 'Indica que a rubrica é utilizada para faturamento (0-Não, 1-Sim).';

update rubrica set RUBR_IN_ITEM_FATURAMENTO = CASE WHEN (ENCO_CD_ITEM_FATURA <> 0) THEN 1  ELSE 0 END; 

insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'RUBRICA'),'RUBR_IN_ITEM_FATURAMENTO' , 'Indica que a rubrica é utilizada para faturamento','indicadorItemFaturamento',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'RUBRICA') and TACO_NM = 'RUBR_IN_ITEM_FATURAMENTO');

commit;
