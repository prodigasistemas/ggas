UPDATE MENU SET MENU_DS_URL = 'exibirPesquisarRedeDiametro' WHERE MENU_DS_URL LIKE '%redeDiametro%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisarRedeDiametro' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=redeDiametro';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoRedeDiametro' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=redeDiametro';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirRedeDiametro' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=redeDiametro';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarRedeDiametro' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=redeDiametro';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarRedeDiametro' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=redeDiametro';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirRedeDiametro' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=redeDiametro';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarRedeDiametro' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=redeDiametro';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerRedeDiametro' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=redeDiametro';

commit;