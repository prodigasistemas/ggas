UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTronco' WHERE MENU_DS_URL LIKE '%tronco%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoTronco' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%' and RESI_DS like '%tela=tronco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTronco' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%'  and RESI_DS like '%tela=tronco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTronco' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%' and RESI_DS like '%tela=tronco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoTronco' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%' and RESI_DS like '%tela=tronco';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirTronco' WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%' and RESI_DS like '%tela=tronco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTronco' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%' and RESI_DS like '%tela=tronco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTronco' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%' and RESI_DS like '%tela=tronco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTronco' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%' and RESI_DS like '%tela=tronco';