CREATE SEQUENCE  "SQ_EQTU_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;


CREATE TABLE EQUIPE_TURNO
(
	EQTU_CD NUMBER(10) PRIMARY KEY,
	EQUI_CD NUMBER(10) NOT NULL,
	EQTU_DT_ANOMESREFERENCIA VARCHAR(10),
	EQTU_IN_USO NUMBER(1) NOT NULL,
	EQTU_TM_ULTIMA_ALTERACAO TIMESTAMP NOT NULL,
	EQTU_NR_VERSAO NUMBER(5) NOT NULL
);


ALTER TABLE EQUIPE_TURNO
ADD CONSTRAINT EQTU_EQUI_FK FOREIGN KEY (EQUI_CD) REFERENCES EQUIPE(EQUI_CD) ENABLE;




CREATE SEQUENCE  "SQ_EQTI_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;


CREATE TABLE EQUIPE_TURNO_ITEM
(
	EQTI_CD NUMBER(10) PRIMARY KEY,
	EQTU_CD NUMBER(10) NOT NULL,
	EQUI_CD NUMBER(10) NOT NULL,
	FUNC_CD NUMBER(10) NOT NULL,
	VEIC_CD NUMBER(10),
	ENCO_CD_TURNO NUMBER(10) NOT NULL,
	EQTI_DT_INICIO TIMESTAMP(6),
	EQTI_DT_FIM TIMESTAMP(6),
	EQTI_IN_USO NUMBER(1) NOT NULL,
	EQTI_TM_ULTIMA_ALTERACAO TIMESTAMP NOT NULL,
	EQTI_NR_VERSAO NUMBER(5) NOT NULL
);


ALTER TABLE EQUIPE_TURNO_ITEM
ADD CONSTRAINT EQTI_EQTU_FK FOREIGN KEY (EQTU_CD) REFERENCES EQUIPE_TURNO(EQTU_CD) ENABLE;

ALTER TABLE EQUIPE_TURNO_ITEM
ADD CONSTRAINT EQTI_ENCO_FK FOREIGN KEY (ENCO_CD_TURNO) REFERENCES ENTIDADE_CONTEUDO(ENCO_CD) ENABLE;

ALTER TABLE EQUIPE_TURNO_ITEM
ADD CONSTRAINT EQTI_FUNC_FK FOREIGN KEY (FUNC_CD) REFERENCES FUNCIONARIO(FUNC_CD) ENABLE;

ALTER TABLE EQUIPE_TURNO_ITEM
ADD CONSTRAINT EQTI_EQUI_FK FOREIGN KEY (EQUI_CD) REFERENCES EQUIPE(EQUI_CD) ENABLE;

ALTER TABLE EQUIPE_TURNO_ITEM
ADD CONSTRAINT EQTI_VEIC_FK FOREIGN KEY (VEIC_CD) REFERENCES VEICULO(VEIC_CD) ENABLE;


INSERT INTO MENU VALUES(SQ_MENU_CD.nextval, 
(SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Autorização de Serviço' AND MENU_CD_PAI IN (SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Atendimento ao Público' AND MENU_CD_PAI IS NULL)),
'Escala', 1, 1, CURRENT_TIMESTAMP, 0, NULL, NULL, 'exibirPesquisaEquipeTurno', 0);

INSERT INTO OPERACAO_SISTEMA VALUES(SQ_OPSI_CD.nextval, (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS like '%Atendimento Ao Público%'), 'CONSULTAR', 0,0,0,1,
CURRENT_TIMESTAMP, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 1);

INSERT INTO OPERACAO_SISTEMA VALUES(SQ_OPSI_CD.nextval, (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS like '%Atendimento Ao Público%'), 'SALVAR', 0,0,0,1,
CURRENT_TIMESTAMP, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 1);

INSERT INTO OPERACAO_SISTEMA VALUES(SQ_OPSI_CD.nextval, (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS like '%Atendimento Ao Público%'), 'INCLUIR', 0,0,0,1,
CURRENT_TIMESTAMP, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 1);

INSERT INTO PERMISSAO_SISTEMA VALUES(SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
0,0,(SELECT PASI_CD FROM PAPEL_SISTEMA WHERE UPPER(PASI_DS) = 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'exibirPesquisaEquipeTurno', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO OPERACAO_SISTEMA VALUES(SQ_OPSI_CD.nextval, (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS like '%Atendimento Ao Público%'), 'GERAR', 0,0,0,1,
CURRENT_TIMESTAMP, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 1);

INSERT INTO PERMISSAO_SISTEMA VALUES(SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'GERAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
0,0,(SELECT PASI_CD FROM PAPEL_SISTEMA WHERE UPPER(PASI_DS) = 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO PERMISSAO_SISTEMA VALUES(SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'SALVAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
0,0,(SELECT PASI_CD FROM PAPEL_SISTEMA WHERE UPPER(PASI_DS) = 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO PERMISSAO_SISTEMA VALUES(SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno'), 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'INCLUIR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
0,0,(SELECT PASI_CD FROM PAPEL_SISTEMA WHERE UPPER(PASI_DS) = 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'gerarEquipeTurno', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'GERAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'alterarEquipeTurnoAJAX', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'GERAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'salvarEquipeTurno', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'SALVAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'exibirIncluirEquipeTurno', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'SALVAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'pesquisarEquipeTurno', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'exibirDetalhamentoEquipeTurno', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'atualizarEquipeTurnoAJAX', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaEquipeTurno')),
1,1,CURRENT_TIMESTAMP);

COMMIT;
