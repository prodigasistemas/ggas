ALTER TABLE CHAMADO_ASSUNTO ADD "CHAS_IN_EMAIL_CADASTRAR" NUMBER(1,0) DEFAULT 0;

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO'),'CHAS_IN_EMAIL_CADASTRAR' , 'Indicador para saber se envia email apos o cadastro de chamado','indicadorEmailCadastrar',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO') and TACO_NM = 'CHAS_IN_EMAIL_CADASTRAR');

ALTER TABLE CHAMADO_ASSUNTO ADD "CHAS_IN_EMAIL_TRAMITAR" NUMBER(1,0) DEFAULT 0;

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO'),'CHAS_IN_EMAIL_TRAMITAR' , 'Indicador para saber se envia email apos o tramite de chamado','indicadorEmailTramitar',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO') and TACO_NM = 'CHAS_IN_EMAIL_TRAMITAR');

ALTER TABLE CHAMADO_ASSUNTO ADD "CHAS_IN_EMAIL_REITERAR" NUMBER(1,0) DEFAULT 0;

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO'),'CHAS_IN_EMAIL_REITERAR' , 'Indicador para saber se envia email apos a reiteração de chamado','indicadorEmailReiterar',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO') and TACO_NM = 'CHAS_IN_EMAIL_REITERAR');

ALTER TABLE CHAMADO_ASSUNTO ADD "CHAS_IN_EMAIL_ENCERRAR" NUMBER(1,0) DEFAULT 0;

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO'),'CHAS_IN_EMAIL_ENCERRAR' , 'Indicador para saber se envia email apos a reiteração de chamado','indicadorEmailEncerrar',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO') and TACO_NM = 'CHAS_IN_EMAIL_ENCERRAR');

COMMIT;