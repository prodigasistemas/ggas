--alter table proposta drop column PROP_MOTIVO_CANCELAMENTO;
alter table proposta drop column PROP_MD_CONSUMO_EQUIV_GN;
alter table proposta add ENCO_CD_MODALIDADE_USO NUMBER(9, 0) NULL;
ALTER TABLE proposta ADD CONSTRAINT FK_PROPOSTA_MODALIDADE_USO FOREIGN KEY (ENCO_CD_MODALIDADE_USO) REFERENCES entidade_conteudo( ENCO_CD );
alter table proposta drop column PROP_VL_TARIFA;
ALTER TABLE proposta ADD TARI_CD NUMBER(9, 0)  NULL;
ALTER TABLE proposta ADD CONSTRAINT FK_PROPOSTA_TARIFA FOREIGN KEY (TARI_CD) REFERENCES tarifa( TARI_CD );
ALTER TABLE PROPOSTA MODIFY prst_cd NULL;

INSERT INTO constante_sistema SELECT sq_cost_cd.nextval, (SELECT TABE_CD FROM tabela WHERE tabe_nm in ('TARIFA')), 
  (SELECT mosi_cd FROM modulo_sistema WHERE UPPER(MOSI_DS) = UPPER('CADASTRO')), 'C_TARIFA_BASE_APURACAO_DIARIA', 
  'Chave Primaria da Entidade Conteudo Base Apuração Diária', (SELECT enco_cd FROM entidade_conteudo enco 
  WHERE UPPER(enco_ds) like UPPER('Diária') AND enco.ENCL_CD = (SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Base de Apuração')), NULL, 0, 1,
  CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM constante_sistema WHERE cost_nm = 'C_TARIFA_BASE_APURACAO_DIARIA');
  
  INSERT INTO constante_sistema SELECT sq_cost_cd.nextval, (SELECT TABE_CD FROM tabela WHERE tabe_nm in ('TARIFA')), 
  (SELECT mosi_cd FROM modulo_sistema WHERE UPPER(MOSI_DS) = UPPER('CADASTRO')), 'C_TARIFA_BASE_APURACAO_PERIODICA', 
  'Chave Primaria da Entidade Conteudo Base Apuração Periódica', (SELECT enco_cd FROM entidade_conteudo enco 
  WHERE UPPER(enco_ds) like UPPER('periódico') AND enco.ENCL_CD = (SELECT encl_cd FROM entidade_classe WHERE encl_ds = 'Base de Apuração')), NULL, 0, 1,
  CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM constante_sistema WHERE cost_nm = 'C_TARIFA_BASE_APURACAO_PERIODICA');
  
  INSERT INTO proposta_situacao 
  SELECT sq_prst_cd.nextval, 
  'REJEITADA', 
  '0', 
  '1', 
  current_timestamp 
  FROM DUAL WHERE NOT EXISTS (SELECT * FROM proposta_situacao where prst_ds  = 'REJEITADA');
  
  COMMIT;