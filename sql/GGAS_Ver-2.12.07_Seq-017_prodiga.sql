INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Autorização de Serviço Algas', 'Relatorio Autorização de Serviço Algas', 'relatorioAutorizacaoServicoAlgas.jasper',
(select enco_cd from entidade_conteudo where enco_ds like 'Autorização de Serviço'), 1, 1, CURRENT_TIMESTAMP);

INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Autorização de Serviço Inicio Fornecimento Algas', 'Relatorio Autorização de Serviço Inicio Fornecimento Algas', 'relatorioASInicioFornecimentoAlgas.jasper',
(select enco_cd from entidade_conteudo where enco_ds like 'Autorização de Serviço'), 1, 1, CURRENT_TIMESTAMP);

INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Autorização de Serviço Adequação Algas', 'Relatorio Autorização de Serviço Adequação Fornecimento Algas', 'relatorioAutorizacaoServicoAlgasAdequacao.jasper',
(select enco_cd from entidade_conteudo where enco_ds like 'Autorização de Serviço'), 1, 1, CURRENT_TIMESTAMP);

INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Autorização de Serviço Atendimento Algas', 'Relatorio Autorização de Serviço Atendimento Algas', 'relatorioAutorizacaoServicoAlgasAtendimento.jasper',
(select enco_cd from entidade_conteudo where enco_ds like 'Autorização de Serviço'), 1, 1, CURRENT_TIMESTAMP);

COMMIT;