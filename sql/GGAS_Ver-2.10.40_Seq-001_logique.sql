INSERT INTO parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO', 'Indica o tempo de atualização da tela de controle de processo (em segundos)', 30, 1 , 0, 1, localtimestamp , 4);

commit;