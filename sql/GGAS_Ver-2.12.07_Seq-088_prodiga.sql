ALTER TABLE LOTE_COMUNICACAO MODIFY SRTI_CD NULL;
ALTER TABLE ITEM_LOTE_COMUNICACAO ADD ILCM_DT_EXECUCAO_ANTERIOR timestamp(6);
ALTER TABLE ITEM_LOTE_COMUNICACAO ADD ILCM_NR_SEQUENCIAL_ANTERIOR VARCHAR2(50);

COMMIT;