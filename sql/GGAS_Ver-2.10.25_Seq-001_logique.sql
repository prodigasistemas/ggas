ALTER TABLE SERVICO_TIPO ADD "SRTI_NR_MAXIMO_EXECUCAO" NUMBER(9, 0) DEFAULT 2;
COMMENT ON COLUMN SERVICO_TIPO."SRTI_NR_MAXIMO_EXECUCAO" IS 'Número máximo de vezes que aquele serviço poderá ser executado em um ponto de consumo (por ponto de consumo).';

ALTER TABLE SERVICO_TIPO ADD "SRTI_NR_PRAZO_GARANTIA" NUMBER(9, 0) DEFAULT 4;
COMMENT ON COLUMN SERVICO_TIPO."SRTI_NR_PRAZO_GARANTIA" IS 'Tempo de garantia daquele serviço (em dias).';

commit;
