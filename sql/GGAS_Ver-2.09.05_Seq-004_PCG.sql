update rubrica set rubr_ds_impressao = rubr_ds where rubr_ds like 'Debito Pagto a Menor';
commit;

alter table contrato_aba_atributo modify COAA_NM_COLUNA VARCHAR2(50);
commit;

insert into contrato_aba_atributo (COAA_CD, COAB_CD, COAA_DS, COAA_NM_COLUNA, COAA_NR_VERSAO, COAA_IN_USO, COAA_TM_ULTIMA_ALTERACAO, COAA_CD_DEPENDENCIA, COAA_IN_DEPEND_OBRIGATORIA) SELECT SQ_COAA_CD.NEXTVAL, 1, 'Indicador de Débito Automático', 'debitoAutomatico', 0, 1, CURRENT_TIMESTAMP, null, 0 FROM DUAL WHERE  NOT EXISTS (SELECT * FROM contrato_aba_atributo WHERE COAB_CD=1 and COAA_NM_COLUNA = 'debitoAutomatico'); 

insert into tabela_coluna (TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO, TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA, TACO_IN_VARIAVEL, TACO_CD_VARIAVEL) SELECT SQ_TACO_CD.NEXTVAL, 45, 'ARCC_CD_IND_DEBITO_AUTOMATICO', 'Indicador de Débito Automático', 'debitoAutomatico', 0, 0, 1, CURRENT_TIMESTAMP, 0, 0, 0, null FROM DUAL WHERE  NOT EXISTS (SELECT * FROM tabela_coluna WHERE TABE_CD=45 and TACO_NM = 'ARCC_CD_IND_DEBITO_AUTOMATICO'); 

insert into contrato_aba_atributo (COAA_CD, COAB_CD, COAA_DS, COAA_NM_COLUNA, COAA_NR_VERSAO, COAA_IN_USO, COAA_TM_ULTIMA_ALTERACAO, COAA_CD_DEPENDENCIA, COAA_IN_DEPEND_OBRIGATORIA) SELECT SQ_COAA_CD.NEXTVAL, 1, 'Arrecadador Contrato Convenio de Débito Automático', 'arrecadadorConvenioDebitoAutomatico', 0, 1, CURRENT_TIMESTAMP, (SELECT COAA_CD FROM contrato_aba_atributo WHERE COAB_CD=1 and COAA_NM_COLUNA = 'debitoAutomatico'), 0 FROM DUAL WHERE  NOT EXISTS (SELECT * FROM contrato_aba_atributo WHERE COAB_CD=1 and COAA_NM_COLUNA = 'arrecadadorConvenioDebitoAutomatico'); 

insert into tabela_coluna (TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO, TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA, TACO_IN_VARIAVEL, TACO_CD_VARIAVEL) SELECT SQ_TACO_CD.NEXTVAL, 45, 'ARCC_CD_DEBITO_AUTOMATICO', 'Arrecadador Contrato Convenio de Débito Automático', 'arrecadadorConvenioDebitoAutomatico', 0, 0, 1, CURRENT_TIMESTAMP, 0, 0, 0, null FROM DUAL WHERE  NOT EXISTS (SELECT * FROM tabela_coluna WHERE TABE_CD=45 and TACO_NM = 'ARCC_CD_DEBITO_AUTOMATICO'); 

commit;