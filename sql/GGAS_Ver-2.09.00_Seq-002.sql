create table CONTRATO_LAYOUT_RELATORIO
(
  COLR_CD                     NUMBER not null,
  COLR_DS_RELATORIO           CLOB   not null,
  COLR_IN_USO                 NUMBER default 1,
  COLR_TM_ULTIMA_ALTERACAO    DATE   DEFAULT CURRENT_TIMESTAMP
);

comment on table CONTRATO_LAYOUT_RELATORIO
  is 'Tabela com as informações dos layouts para impressão de contratos';
comment on column CONTRATO_LAYOUT_RELATORIO.COLR_CD
  is 'Chave primária gerada automaticamente e controlada pela sequence SQ_COLR_CD';
comment on column CONTRATO_LAYOUT_RELATORIO.COLR_DS_RELATORIO
  is 'Texto com as definições do layout';
comment on column CONTRATO_LAYOUT_RELATORIO.COLR_IN_USO
  is 'Indicador de em uso do registro';
comment on column CONTRATO_LAYOUT_RELATORIO.COLR_TM_ULTIMA_ALTERACAO
  is 'Data da ultima alteracao do registro';
alter table CONTRATO_LAYOUT_RELATORIO
  add constraint PK_COLR_CD primary key (COLR_CD);
  
CREATE SEQUENCE  "SQ_COLR_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOORDER  NOCYCLE ;


ALTER TABLE CONTRATO_MODELO ADD COLR_CD_LAYOUT_RELATORIO NUMBER;
COMMENT ON COLUMN CONTRATO_MODELO.COLR_CD_LAYOUT_RELATORIO
  IS 'Chave primaria da tabela CONTRATO_LAYOUT_RELATORIO para refereciar um layout de impressão para os contratos';

ALTER TABLE CONTRATO_MODELO 
  ADD CONSTRAINT FK_COME_COLR_LAYOUT_RELATORIO 
  FOREIGN KEY ( COLR_CD_LAYOUT_RELATORIO ) 
  REFERENCES CONTRATO_LAYOUT_RELATORIO ( COLR_CD );


create table CONTRATO_RELATORIO_TOKENS
(
  CORT_CD                     NUMBER not null,
  CORT_DS_DESCRICAO           VARCHAR2(100 BYTE)   not null,
  CORT_DS_VALOR               VARCHAR2(100 BYTE)   not null,
  CORT_DS_ALIAS               VARCHAR2(100 BYTE)   not null,
  CORT_TM_ULTIMA_ALTERACAO    DATE   DEFAULT CURRENT_TIMESTAMP
);

comment on table CONTRATO_RELATORIO_TOKENS
  is 'Tabela com as informações dos tokens para criação dos layouts de contratos';
comment on column CONTRATO_RELATORIO_TOKENS.CORT_CD
  is 'Chave primária gerada automaticamente e controlada pela sequence SQ_CORT_CD';
comment on column CONTRATO_RELATORIO_TOKENS.CORT_DS_DESCRICAO
  is 'Texto com a descrição do token';
comment on column CONTRATO_RELATORIO_TOKENS.CORT_DS_VALOR
  is 'Texto com o valor do token';
comment on column CONTRATO_RELATORIO_TOKENS.CORT_DS_ALIAS
  is 'Referencia ao campo na base de dados';
comment on column CONTRATO_RELATORIO_TOKENS.CORT_TM_ULTIMA_ALTERACAO
  is 'Data da ultima alteracao do registro';
alter table CONTRATO_RELATORIO_TOKENS
  add constraint PK_CORT_CD primary key (CORT_CD);
  
CREATE SEQUENCE  "SQ_CORT_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOORDER  NOCYCLE ;

COMMIT;