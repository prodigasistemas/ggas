INSERT INTO CONSUMO_ANORMALIDADE(
  COAN_CD,
  COAN_DS,
  COAN_DS_ABREVIADO,
  COAN_IN_BLOQUEIA_FATURAMENTO,
  COAN_NR_VERSAO,
  COAN_IN_USO)
VALUES
 (SQ_COAN_CD.NEXTVAL, 
  'PCS INEXISTENTE', 
  'P.I.', 
   1,
   1, 
   1);
 
COMMIT;

INSERT INTO CONSTANTE_SISTEMA (COST_CD,
	TABE_CD,
	MOSI_CD,
	COST_NM,
	COST_DS,
	COST_VL,
	COST_CD_CLASSE,
	COST_NR_VERSAO,
	COST_IN_USO)
values 
	(SQ_COST_CD.NEXTVAL,
	'37',
	'2',
	'C_ANORMALIDADE_PCS_INEXISTENTE',
	'Indica a anormalidade de ausência de PCS',
	(SELECT COAN_CD FROM CONSUMO_ANORMALIDADE WHERE COAN_DS LIKE 'PCS INEXISTENTE'),
	null,
	'1',
	'1');
	             
COMMIT;