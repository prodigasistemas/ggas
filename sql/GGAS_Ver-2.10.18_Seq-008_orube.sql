
UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaImovel' WHERE MENU_DS_URL LIKE 'exibirPesquisaImovel.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoImovel' WHERE RESI_DS LIKE 'exibirInclusaoImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoImovel' WHERE RESI_DS LIKE 'exibirAlteracaoImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaImovel' WHERE RESI_DS LIKE 'exibirPesquisaImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoImovel' WHERE RESI_DS LIKE 'exibirDetalhamentoImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarImovel' WHERE RESI_DS LIKE 'alterarImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarImovel' WHERE RESI_DS LIKE 'pesquisarImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirImovel' WHERE RESI_DS LIKE 'incluirImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerImovel' WHERE RESI_DS LIKE 'removerImovel.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirImoveisEmLote' WHERE RESI_DS LIKE 'incluirImoveisEmLote.do';

DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'adicionarContatoDoImovel.do';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'removerClienteImovel.do';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'removerContatoDoImovel.do';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'atualizarContatoPrincipalDoImovel.do';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'adicionarClienteImovel.do';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'adicionarPontoConsumo.do';
DELETE FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'adicionarUnidadeConsumidora.do';

commit;