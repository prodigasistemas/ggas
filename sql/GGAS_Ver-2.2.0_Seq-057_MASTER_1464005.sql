INSERT INTO ENTIDADE_CONTEUDO 
  select SQ_ENCO_CD.nextval, 
  (select ENCL_CD from ENTIDADE_CLASSE where UPPER(encl_ds) like UPPER('Tipo Conv%nio')), 
  'ARRECADAÇÃO', 
  null, 
  0,
  0,
  1,
  current_timestamp,null from dual where not exists (select * from ENTIDADE_CONTEUDO where encl_cd = (select ENCL_CD from ENTIDADE_CLASSE where UPPER(encl_ds) like UPPER('Tipo Conv%nio') and UPPER(enco_ds) like UPPER('ARRECADA%O')));

INSERT INTO CONSTANTE_SISTEMA
    (COST_CD, TABE_CD, COST_CD_CLASSE, COST_NM, COST_DS, COST_VL, COST_TM_ULTIMA_ALTERACAO, COST_NR_VERSAO, COST_IN_USO, MOSI_CD)
    
      SELECT SQ_COST_CD.NEXTVAL ,
      (SELECT TABE_CD FROM tabela WHERE UPPER(tabe_nm)=UPPER('RECEBIMENTO_SITUACAO')) ,
      NULL ,
      'C_TIPO_CONVENIO_ARRECADACAO' , 
      'Indica o convênio arrecadação' ,
      (select enco_cd from ENTIDADE_CONTEUDO where encl_cd = (select ENCL_CD from ENTIDADE_CLASSE where UPPER(encl_ds) like UPPER('Tipo Conv%nio') and UPPER(enco_ds) like UPPER('ARRECADA%O'))),
      CURRENT_TIMESTAMP ,
      1 ,
      1 ,
      (select MOSI_CD FROM modulo_sistema WHERE UPPER(MOSI_DS) like UPPER('Arrecada%o'))
      FROM dual WHERE NOT EXISTS ( SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM = 'C_TIPO_CONVENIO_ARRECADACAO'); 

	  
commit;