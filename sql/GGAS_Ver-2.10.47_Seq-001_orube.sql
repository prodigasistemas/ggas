-- Módulo Integração

	-- Acompanhar Integração
update menu set menu_ds_url = 'exibirPesquisaAcompanharIntegracao' where menu_ds_url like '%exibirPesquisaAcompanharIntegracao.do%';
update recurso_sistema set resi_ds = 'exibirPesquisaAcompanharIntegracao' where resi_ds like 'exibirPesquisaAcompanharIntegracao.do%';
update recurso_sistema set resi_ds = 'pesquisaAcompanharIntegracao' where resi_ds like '%pesquisaAcompanharIntegracao.do%';

INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'DETALHAR',(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE 'Integração'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'DETALHAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao')) ;

INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'ALTERAR',(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE 'Integração'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao')) ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
SELECT SQ_RESI_CD.nextval, 'exibirDetalhamentoAcompanharIntegracao', 0, ((SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'DETALHAR' 
AND mosi_cd = (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE 'Integração') 
AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'))), 1, 1,CURRENT_TIMESTAMP from dual 
where not exists (SELECT RESI_CD FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'exibirDetalhamentoAcompanharIntegracao') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
SELECT SQ_RESI_CD.nextval, 'exportarTabela', 0, ((SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' 
AND mosi_cd = (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE 'Integração') 
AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'))), 1, 1,CURRENT_TIMESTAMP from dual 
where not exists (SELECT RESI_CD FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'exportarTabela') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
SELECT SQ_RESI_CD.nextval, 'salvarAlteracao', 0, ((SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' 
AND mosi_cd = (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE 'Integração') 
AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'))), 1, 1,CURRENT_TIMESTAMP from dual 
where not exists (SELECT RESI_CD FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'salvarAlteracao') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
SELECT SQ_RESI_CD.nextval, 'filtrarTabela', 0, ((SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' 
AND mosi_cd = (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE 'Integração') 
AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'))), 1, 1,CURRENT_TIMESTAMP from dual 
where not exists (SELECT RESI_CD FROM RECURSO_SISTEMA WHERE RESI_DS LIKE 'filtrarTabela') ;

INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'DETALHAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'DETALHAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao')));

INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaAcompanharIntegracao')));

COMMIT;
