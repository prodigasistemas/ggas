ALTER TABLE CHAMADO ADD "CHAM_IN_PENDENCIA" NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN CHAMADO."CHAM_IN_PENDENCIA" IS 'Indicador de pendencia no chamado';

INSERT INTO ENTIDADE_CONTEUDO VALUES
(SQ_ENCO_CD.nextval, 
(SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status do Chamado'), 'PENDENTE', NULL, 0, 0, 1, CURRENT_TIMESTAMP, 1);

INSERT INTO CONSTANTE_SISTEMA VALUES
(SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_DS = 'Chamado'), (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento'),
'C_STATUS_CHAMADO_PENDENTE', 'Indica o Status Pendente para um Chamado', 
(SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS = 'PENDENTE' AND ENCL_CD = (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status do Chamado')),
NULL, 1, 1, CURRENT_TIMESTAMP);

COMMIT;