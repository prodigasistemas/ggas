ALTER TABLE FATURA ADD (FATU_CD_PRINCIPAL NUMBER(9, 0));
ALTER TABLE FATURA ADD (FATU_DS_COMPLEMENTO VARCHAR2(4000 BYTE));
COMMENT ON COLUMN FATURA.FATU_CD_PRINCIPAL IS 'Chave primária FATURA principal.';
COMMENT ON COLUMN FATURA.FATU_DS_COMPLEMENTO IS 'Observações da fatura complementar.';

INSERT INTO tabela_coluna (TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO, TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA) 
SELECT SQ_TACO_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'FATURA'), 'FATU_CD_PRINCIPAL', 'Chave primária FATURA principal', 'faturaPrincipal', 0,0,1, CURRENT_TIMESTAMP, 0,0 FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'FATURA') AND UPPER(TACO_NM)='FATU_CD_PRINCIPAL');

INSERT INTO tabela_coluna (TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO, TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA) 
SELECT SQ_TACO_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'FATURA'), 'FATU_DS_COMPLEMENTO', 'Observações da fatura complementar', 'observacaoComplementar', 0,0,1, CURRENT_TIMESTAMP, 0,0 FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'FATURA') AND UPPER(TACO_NM)='FATU_DS_COMPLEMENTO');

INSERT INTO ENTIDADE_CLASSE (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
SELECT SQ_ENCL_CD.NEXTVAL, 'Motivo de Complemento de Fatura', 1, NULL, 1, CURRENT_TIMESTAMP FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivo de Complemento de Fatura');

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivo de Complemento de Fatura'), 'Valor', 'VR', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Valor' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Motivo de Complemento de Fatura'));

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivo de Complemento de Fatura'), 'Volume', 'VL', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Volume' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Motivo de Complemento de Fatura'));

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivo de Complemento de Fatura'), 'Tributo', 'VL', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Tributo' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Motivo de Complemento de Fatura'));

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('FATURA')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Faturamento')),'C_TIPOS_MOTIVO_COMPLEMENTO_FATURA',
'Indica a entidade classe com os tipos de complemento de fatura', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivo de Complemento de Fatura') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_TIPOS_MOTIVO_COMPLEMENTO_FATURA');

commit;