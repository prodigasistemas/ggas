UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaArrecadador' WHERE MENU_DS LIKE 'Arrecadador';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaArrecadador' WHERE RESI_DS LIKE 'exibirPesquisaArrecadador.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoArrecadador' WHERE RESI_DS LIKE 'exibirInclusaoArrecadador.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoArrecadador' WHERE RESI_DS LIKE 'exibirDetalhamentoArrecadador.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoArrecadador' WHERE RESI_DS LIKE 'exibirAlteracaoArrecadador.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerArrecadador' WHERE RESI_DS LIKE 'removerArrecadador.do';

COMMIT;