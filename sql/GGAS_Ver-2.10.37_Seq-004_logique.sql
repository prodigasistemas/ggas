alter table leitura_movimento add lemo_md_latitude NUMBER(14,12);
alter table leitura_movimento add lemo_md_longitude NUMBER(15,12);

COMMENT ON COLUMN leitura_movimento.lemo_md_latitude IS 'Indica a latitude grau da coordenada geográfica do Ponto de Consumo.' ;
COMMENT ON COLUMN leitura_movimento.lemo_md_longitude IS 'Indica a longitude grau da coordenada geográfica do Ponto de Consumo.' ;
commit;
