update tabela set tabe_in_constante=1 where tabe_nm='RUBRICA';
	
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_CREDITO_GAS_AJUSTE_PCS','Indica a Rubrica Devolução Gás - Ajuste PCS',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_CREDITO_GAS_AJUSTE_PCS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_CREDITO_MARGEM_AJUSTE_PCS','Indica a Rubrica Devolução Margem de Distribuição - Ajuste PCS',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_CREDITO_MARGEM_AJUSTE_PCS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS','Indica a Rubrica Devolução Transporte - Ajuste PCS',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_DEBITO_GAS_AJUSTE_PCS','Indica a Rubrica Complemento Gás - Ajuste PCS',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_DEBITO_GAS_AJUSTE_PCS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_DEBITO_MARGEM_AJUSTE_PCS','Indica a Rubrica Complemento Margem de Distribuição - Ajuste PCS',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_DEBITO_MARGEM_AJUSTE_PCS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS','Indica a Rubrica Complemento Transporte - Ajuste PCS',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_MULTA_ATRASO','Indica a Rubrica Multa por atraso',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_MULTA_ATRASO');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_CREDITO_PAGTO_MAIOR','Indica a Rubrica Devolução Pagamento a Maior',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_CREDITO_PAGTO_MAIOR');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_DEBITO_PAGTO_MENOR','Indica a Rubrica Debito Pagamento a Menor',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_DEBITO_PAGTO_MENOR');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_JUROS_MORA','Indica a Rubrica Juros de mora',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_JUROS_MORA');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_CREDITO_VOLUME','Indica a Rubrica Credito de volume',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_CREDITO_VOLUME');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_DESCONTO_ICMS_PRODESIN','Indica a Rubrica Desconto ICMS PRODESIN',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_DESCONTO_ICMS_PRODESIN');
	
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_PARCELAMENTO_DEBITO','Indica a Rubrica Parcelamento de débitos',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_PARCELAMENTO_DEBITO');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_MULTA_RECISORIA','Indica a Rubrica Multa Recisoria',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_MULTA_RECISORIA');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'RUBRICA'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'FATURAMENTO'),
'C_RUBRICA_ICMS_SUBSTITUTO','Indica a Rubrica ICMS Substituto',null,null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_RUBRICA_ICMS_SUBSTITUTO');	

commit;
