UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaProfissao' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=profissao';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaProfissao' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=profissao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirProfissao' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=profissao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoProfissao' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=profissao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarProfissao' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=profissao';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarProfissao' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=profissao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirProfissao' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=profissao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarProfissao' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=profissao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerProfissao' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=profissao';

COMMIT;