update OPERACAO_SISTEMA SET MENU_CD = 219 WHERE MENU_CD = 132;

update PERMISSAO_SISTEMA SET MENU_CD = 219 WHERE MENU_CD = 132;

delete MENU WHERE MENU_CD = 132;

UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaMicrorregiao' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar%tela=microrregiao';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaMicrorregiao' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar%tela=microrregiao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoMicrorregiao' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar%tela=microrregiao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhaMicrorregiao' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar%tela=microrregiao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoMicrorregiao' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar%tela=microrregiao';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarMicrorregiao' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar%tela=microrregiao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirMicrorregiao' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar%tela=microrregiao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarMicrorregiao' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar%tela=microrregiao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerMicrorregiao' WHERE RESI_DS LIKE 'removerTabelaAuxiliar%tela=microrregiao';

COMMIT;