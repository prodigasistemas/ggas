UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaFaturaControleImpressao' WHERE MENU_DS_URL LIKE '%exibirPesquisaFaturaControleImpressao%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaFaturaControleImpressao' WHERE RESI_DS LIKE '%exibirPesquisaFaturaControleImpressao%';

----------------------------------------------- MENU ----------------------------------------------------------
INSERT INTO MENU (MENU_CD,MENU_CD_PAI,MENU_DS,MENU_NR_VERSAO,MENU_IN_USO,MENU_IN_ALCADA,MENU_NR_ORDEM,RESI_CD,MENU_DS_URL,MENU_TM_ULTIMA_ALTERACAO) 
SELECT SQ_MENU_CD.nextval,(SELECT MENU_CD FROM menu WHERE menu_ds LIKE '%Faturamento%' and MENU_CD_PAI IS NULL),'Controle de Impressão','0','1','0', SQ_MENU_CD.nextval, null,
'exibirPesquisaFaturaControleImpressao', CURRENT_TIMESTAMP 
from dual where not exists (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao' AND menu_cd_pai = (SELECT MENU_CD FROM menu WHERE menu_ds LIKE '%Faturamento%' and MENU_CD_PAI IS NULL));

----------------------------------------------- INICIO PESQUISA ------------------------------------------------
INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'PESQUISAR',
(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Faturamento%'),0,1,1,0,0,CURRENT_TIMESTAMP 
from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'PESQUISAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')) ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
select SQ_RESI_CD.nextval, 'exibirPesquisaFaturaControleImpressao', 0, (
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'PESQUISAR' 
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE '%exibirPesquisaFaturaControleImpressao%') AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Faturamento%')  
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaFaturaControleImpressao'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirPesquisaFaturaControleImpressao');


INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'CONSULTAR',(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'), 
(SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Faturamento%'),0,1,1,0,0,CURRENT_TIMESTAMP 
from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')) ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
select SQ_RESI_CD.nextval, 'pesquisarControleFatura', 0, (
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' 
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE '%exibirPesquisaFaturaControleImpressao%') AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Faturamento%')  
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaFaturaControleImpressao'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'pesquisarControleFatura');

----------------------------------------------- FIM PESQUISA ------------------------------------------------
----------------------------------------------- INCIO DETALHAMENTO ------------------------------------------------

INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) 
SELECT SQ_OPSI_CD.nextval,'DETALHAR',(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'),
(SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Fatura%'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'DETALHAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')) ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
select SQ_RESI_CD.nextval, 'exibirDetalharFaturaControleImpressao', 0, (
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'DETALHAR' 
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE '%exibirPesquisaFaturaControleImpressao%') AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Faturamento%')  
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaFaturaControleImpressao'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirDetalharFaturaControleImpressao');


INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'DETALHAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%DETALHAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%'))) ;
----------------------------------------------- FIM DETALHAMENTO ------------------------------------------------
----------------------------------------------- INCIO PROCESSO ------------------------------------------------
INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'EXECUTAR',(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Faturamento%'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'EXECUTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')) ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
select SQ_RESI_CD.nextval, 'gerarRelatorioControleImpressao', 0, (
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'EXECUTAR' 
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE '%exibirPesquisaFaturaControleImpressao%') AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Faturamento%')  
AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaFaturaControleImpressao'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'gerarRelatorioControleImpressao');


INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'EXECUTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%EXECUTAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%')) AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%')) ;
----------------------------------------------- FIM PROCESSO ------------------------------------------------

INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'PESQUISAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%PESQUISAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%')) AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%'));
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%CONSULTAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%')) AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%'));
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'DETALHAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%DETALHAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%')) AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%'));
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'EXECUTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaFaturaControleImpressao')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%EXECUTAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%')) AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE '%exibirPesquisaFaturaControleImpressao%'));