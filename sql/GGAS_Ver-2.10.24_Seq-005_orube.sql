UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaUnidadeFederacao' WHERE MENU_DS_URL LIKE '%unidadeFederacao%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirIncluirUnidadeFederacao' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%tela=unidadeFederacao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaUnidadeFederacao' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%tela=unidadeFederacao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalharUnidadeFederacao' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%tela=unidadeFederacao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlterarUnidadeFederacao' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%tela=unidadeFederacao';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirUnidadeFederacao' WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%tela=unidadeFederacao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarUnidadeFederacao' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%tela=unidadeFederacao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarUnidadeFederacao' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%tela=unidadeFederacao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerUnidadeFederacao' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%tela=unidadeFederacao';