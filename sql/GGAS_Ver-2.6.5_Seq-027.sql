update contrato_aba_atributo 
  set coaa_in_depend_obrigatoria = 1,
    coaa_cd_dependencia = (
      SELECT coaa_cd FROM contrato_aba_atributo 
        where coaa_nm_coluna = 'periodicidadeRetMaiorMenor')
  where coaa_nm_coluna = 'consumoReferenciaSobDem';

commit;