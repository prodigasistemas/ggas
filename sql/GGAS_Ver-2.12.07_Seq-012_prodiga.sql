INSERT INTO RECURSO_SISTEMA VALUES
(SQ_RESI_CD.nextval, 'imprimirSegundaViaCreditoDebitoRealizar', 0, 
(select opsi_cd from operacao_sistema where menu_cd  in (select menu_cd from menu where menu_ds like 'Crédito / Débito a Realizar') and opsi_ds like 'INCLUIR'), 1,1, CURRENT_TIMESTAMP);


INSERT INTO ENTIDADE_CONTEUDO VALUES
(SQ_ENCO_CD.NEXTVAL, (select encl_cd from entidade_classe where encl_ds like 'Tipo de Documento'), 'CREDITO A REALIZAR', null, 1, 1, 1, CURRENT_TIMESTAMP, null);

INSERT INTO CONSTANTE_SISTEMA VALUES
(SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),
(select mosi_cd from modulo_sistema where mosi_ds like 'Cadastro'), 'C_RELATORIO_NOTA_CREDITO_REALIZAR',
'Chave Primaria da Tabela Entidade Conteudo Modelo Layout Impressoão Credito a Realizar',
(select enco_cd from entidade_conteudo where enco_ds like 'CREDITO A REALIZAR'), 32, 0, 1, CURRENT_TIMESTAMP);

INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Crédito a Realizar', 'Relatório de crédito a realizar', 'relatorioNotaCreditoRealizar.jasper', (select cost_vl from constante_sistema where cost_nm like 'C_RELATORIO_NOTA_CREDITO_REALIZAR'),
1, 1, CURRENT_TIMESTAMP);

COMMIT;