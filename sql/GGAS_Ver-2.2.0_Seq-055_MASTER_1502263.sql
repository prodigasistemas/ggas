--1502263

CREATE TABLE ROTA_HISTORICO 
    ( 
		 ROHI_CD          			NUMBER  (9, 0)  NOT NULL , 
		 LEIT_CD					NUMBER  (9, 0)  NOT NULL ,
		 LEIT_CD_SUPLENTE			NUMBER  (9, 0),
		 ROTA_CD			        NUMBER  (9, 0)  NOT NULL ,
		 ROCR_CD					NUMBER  (9, 0),     
		 ROHI_NR_VERSAO 			NUMBER 	(5)  	NOT NULL , 
		 ROHI_IN_USO 				NUMBER 	(1)  	NOT NULL , 
		 ROHI_TM_ULTIMA_ALTERACAO 	TIMESTAMP  		NOT NULL 	
	) 
;

ALTER TABLE ROTA_HISTORICO ADD CONSTRAINT PK_ROHI_CD PRIMARY KEY ( ROHI_CD );
ALTER TABLE ROTA_HISTORICO ADD CONSTRAINT FK_LEIT_ROHI FOREIGN KEY ( LEIT_CD ) REFERENCES LEITURISTA (LEIT_CD);
ALTER TABLE ROTA_HISTORICO ADD CONSTRAINT FK_LEIT_ROHI_SUPLENTE FOREIGN KEY ( LEIT_CD_SUPLENTE ) REFERENCES LEITURISTA (LEIT_CD);
ALTER TABLE ROTA_HISTORICO ADD CONSTRAINT FK_ROCR_ROHI FOREIGN KEY ( ROCR_CD ) REFERENCES ROTA_CRONOGRAMA (ROCR_CD);

CREATE OR REPLACE PUBLIC SYNONYM ROTA_HISTORICO  FOR ROTA_HISTORICO ;
GRANT INSERT,SELECT, DELETE, UPDATE ON ROTA_HISTORICO  TO GGAS_OPER;
CREATE SEQUENCE "GGAS_ADMIN"."SQ_ROHI_CD" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;
CREATE OR REPLACE PUBLIC SYNONYM SQ_ROHI_CD FOR SQ_ROHI_CD;
GRANT SELECT ON SQ_ROHI_CD TO GGAS_ADMIN;

COMMENT ON COLUMN ROTA_HISTORICO.ROHI_CD IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_ROHI_CD ';
COMMENT ON COLUMN ROTA_HISTORICO.LEIT_CD IS 'Chave Estrangeira da tabela Leiturista';
COMMENT ON COLUMN ROTA_HISTORICO.LEIT_CD_SUPLENTE IS 'Chave Estrangeira da tabela Leiturista';
COMMENT ON COLUMN ROTA_HISTORICO.ROTA_CD IS 'Chave Estrangeira da tabela Rota';
COMMENT ON COLUMN ROTA_HISTORICO.ROCR_CD IS 'Chave Estrangeira da tabela rota_cronograma';
COMMENT ON COLUMN ROTA_HISTORICO.ROHI_NR_VERSAO IS 'Numero da versao do registro, utilizado para controle de concorrencia ';
COMMENT ON COLUMN ROTA_HISTORICO.ROHI_IN_USO IS 'Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)';
COMMENT ON COLUMN ROTA_HISTORICO.ROHI_TM_ULTIMA_ALTERACAO IS 'Data e hora da realizacao da ultima alteracao ';

Insert into TABELA (TABE_CD,TABE_NM,TABE_DS,TABE_NM_MNEMONICO,TABE_IN_ATRIBUTO_DINAMICO,TABE_NR_VERSAO,TABE_IN_USO,TABE_TM_ULTIMA_ALTERACAO,TABE_IN_CONSULTA_DINAMICA,TABE_NM_CLASSE,TABE_IN_AUDITAVEL,TABE_IN_ALCADA,MENU_CD,TABE_NR_MESES_DESCARTE,TABE_IN_INTEGRACAO,TABE_IN_MAPEAMENTO,TABE_IN_CONSTANTE) select SQ_TABE_CD.nextval,'ROTA_HISTORICO','Rota Historico','ROHI',0,0,1,CURRENT_timestamp,1,'br.com.ggas.medicao.rota.impl.RotaHistorico',1,0,null,null,1,0,0 from dual where not exists (select * from TABELA where TABE_NM='ROTA_HISTORICO');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'ROHI_CD' , 'Chave primária gerada automaticamente e controlada pela sequence SQ_ROHI_CD ','chavePrimaria',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'ROHI_CD');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'LEIT_CD' , 'Chave Estrangeira da tabela Leiturista ','leiturista',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'LEIT_CD');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'LEIT_CD_SUPLENTE' , 'Chave Estrangeira da tabela Leiturista ','leituristaSuplente',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'LEIT_CD_SUPLENTE');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'ROTA_CD' , 'Chave Estrangeira da tabela rota ','rota',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'ROTA_CD');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'ROCR_CD' , 'Chave Estrangeira da tabela rota_cronograma ','cronogramaRota',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'ROCR_CD');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'ROHI_NR_VERSAO','Numero da versao do registro, utilizado para controle de concorrencia','versao',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'ROHI_NR_VERSAO');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'ROHI_IN_USO','Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)','habilitado',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'ROHI_IN_USO');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA_HISTORICO'),'ROHI_TM_ULTIMA_ALTERACAO','Data e hora da realizacao da última alteracao','ultimaAlteracao',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA_HISTORICO') and TACO_NM = 'ROHI_TM_ULTIMA_ALTERACAO');


ALTER TABLE ROTA ADD LEIT_CD_SUPLENTE NUMBER(9, 0);
ALTER TABLE ROTA ADD CONSTRAINT FK_LEIT_ROTA_SUPLENTE FOREIGN KEY ( LEIT_CD_SUPLENTE ) REFERENCES LEITURISTA (LEIT_CD);
COMMENT ON COLUMN ROTA.LEIT_CD_SUPLENTE IS 'Chave Estrangeira da tabela Leiturista';
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'ROTA'),'LEIT_CD_SUPLENTE' , 'Chave Estrangeira da tabela Leiturista ','leituristaSuplente',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'ROTA') and TACO_NM = 'LEIT_CD_SUPLENTE');

insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Faturamento%' ),
  'ESCALONAR LEITURISTA', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like '%Faturamento%' and menu_cd_pai is null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Faturamento%' and menu_cd_pai is null)
  and opsi_DS LIKE '%ESCALONAR LEITURISTA%' ); 

INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirEscalonamentoLeiturista.do', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Faturamento%' and menu_cd_pai is null) 
  and opsi_DS LIKE '%ESCALONAR LEITURISTA%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirEscalonamentoLeiturista.do') ;

select * from operacao_sistema where opsi_cd = 1775;

Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds like '%Faturamento%' and menu_cd_pai is null),(select opsi_cd from OPERACAO_SISTEMA
where MOSI_CD = ( select mosi_cd from modulo_sistema where mosi_ds like '%Faturamento%' ) and OPSI_DS = 'ESCALONAR LEITURISTA' 
and menu_cd = (select menu_cd from menu where menu_ds like '%Faturamento%' and menu_cd_pai is null)),0,0,1,0
,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
and menu_cd = (select menu_cd from menu where menu_ds like '%Faturamento%' and menu_cd_pai is null) and opsi_cd =(select opsi_cd from OPERACAO_SISTEMA
where MOSI_CD = ( select mosi_cd from modulo_sistema where mosi_ds like '%Faturamento%' ) and OPSI_DS = 'ESCALONAR LEITURISTA' 
and menu_cd = (select menu_cd from menu where menu_ds like '%Faturamento%' and menu_cd_pai is null) ) ); 

COMMIT;
