UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaMedicaoSupervisorio' WHERE MENU_DS_URL LIKE '%exibirPesquisaMedicaoSupervisorio.do?acao=exibirPesquisaMedicaoSupervisorio%tela=medicaoSupervisorio%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarMedicaoSupervisorio' WHERE RESI_DS LIKE 'pesquisarMedicaoSupervisorio.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'desfazerConsolidarSupervisorio' WHERE RESI_DS LIKE 'desfazerConsolidarSupervisorio.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'desfazerIntegracaoSupervisorio' WHERE RESI_DS LIKE 'desfazerIntegracaoSupervisorio.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'transferirMedicaoSupervisorio' WHERE RESI_DS LIKE 'transferirMedicaoSupervisorio.do';

COMMIT;