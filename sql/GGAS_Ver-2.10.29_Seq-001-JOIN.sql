ALTER TABLE MEDIDOR_OPERACAO MODIFY ( MEOP_DS VARCHAR2(30 BYTE));

commit;

SELECT SQ_MEOP_CD.NEXTVAL from DUAL;
SELECT SQ_MEOP_CD.NEXTVAL from DUAL;

INSERT INTO medidor_operacao (
    meop_cd,
    meop_ds,
    meop_nr_versao,
    meop_in_uso,
    mesi_cd
) VALUES (
    SQ_MEOP_CD.NEXTVAL,
    'Instalação+Ativação',
    1,
    1,
    (
        SELECT
            mesi_cd
        FROM
            medidor_situacao
        WHERE
            mesi_ds = 'Instalado'
            AND ROWNUM = 1
    )
);

INSERT INTO constante_sistema (
    cost_cd,
    tabe_cd,
    mosi_cd,
    cost_nm,
    cost_ds,
    cost_vl,
    cost_nr_versao,
    cost_in_uso
) VALUES (
    sq_cost_cd.NEXTVAL,
    (
        SELECT
            tabe_cd
        FROM
            tabela
        WHERE
            tabe_nm = 'MEDIDOR_OPERACAO'
            AND ROWNUM = 1
    ),
    (
        SELECT
            mosi_cd
        FROM
            modulo_sistema
        WHERE
            mosi_ds = 'Medição'
            AND ROWNUM = 1
    ),
    'C_MEDIDOR_OPERACAO_INSTALACAO_ATIVACAO',
    'Indica a operação instalação do medidor com ativação do medidor',
    (
        SELECT
            meop_cd
        FROM
            medidor_operacao
        WHERE
            meop_ds = 'Instalação+Ativação'
            AND ROWNUM = 1
    ),
    1,
    1
);

commit;





