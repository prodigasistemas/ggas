UPDATE MENU SET MENU_DS_URL = 'exibirPesquisarMunicipio' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=municipio';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisarMunicipio' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=municipio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirMunicipio' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=municipio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoMunicipio' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=municipio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarMunicipio' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=municipio';


UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarMunicipio' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=municipio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirMunicipio' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=municipio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarMunicipio' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=municipio';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerMunicipio' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=municipio';

COMMIT;