DROP SEQUENCE SQ_PESI_CD;

CREATE SEQUENCE  SQ_PESI_CD MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1971 NOCACHE  NOORDER  NOCYCLE;

-- ################################### SERÁ USADA NO CADASTRO DO IMÓVEL ####################################

alter table imovel add (ENCO_CD_STATUS NUMBER(9, 0));
alter table imovel add constraint FK_ENCO_STATUS_IMOVEL FOREIGN KEY ( ENCO_CD_STATUS) REFERENCES ENTIDADE_CONTEUDO ( ENCO_CD );


INSERT INTO ENTIDADE_CLASSE
(ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
SELECT SQ_ENCL_CD.NEXTVAL,
'Situação do Imóvel' ,
1 ,
NULL ,
1 ,
CURRENT_TIMESTAMP
FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Situação do Imóvel');

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Situação do Imóvel'), 'Em Prospecção', 'EP', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Em Prospecção' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Situação do Imóvel'));

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Situação do Imóvel'), 'Prospectado', 'EP', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Prospectado' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Situação do Imóvel'));

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('IMOVEL_SITUACAO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_IMOVEL_STATUS',
'Indica o Status Que o Imóvel se encontra, Prospectado Ou Em Prospecção', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Situação do Imóvel') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_IMOVEL_STATUS');


Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('IMOVEL_SITUACAO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_IMOVEL_STATUS_EM_PROSPECCAO',
'Indica o Status que o Imóvel se Encontra', (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS = 'Em Prospecção') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_IMOVEL_STATUS_EM_PROSPECCAO');


CREATE SEQUENCE SQ_AGET_CD MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;
CREATE OR REPLACE PUBLIC SYNONYM SQ_AGET_CD FOR SQ_AGET_CD;
GRANT SELECT ON SQ_AGET_CD TO GGAS_ADMIN;

CREATE TABLE AGENTE 
    ( 
     AGEN_CD          NUMBER  (9)      NOT NULL , 
     AGENTE_NM        VARCHAR (100)    NOT NULL ,
     AGEN_NR_VERSAO NUMBER (5)  NOT NULL , 
     AGEN_IN_USO NUMBER (1)  NOT NULL , 
     AGEN_TM_ULTIMAALTERACAO TIMESTAMP  NOT NULL ,
    
	CONSTRAINT PK_AGEN_CD PRIMARY KEY ( AGEN_CD )
	) 
;

-- ################################### MENU ####################################
INSERT INTO menu (menu_cd, menu_cd_pai, menu_ds, menu_nr_versao, menu_in_uso, menu_tm_ultima_alteracao, menu_in_alcada, menu_nr_ordem,
                resi_cd, menu_ds_url) SELECT sq_menu_cd.NEXTVAL, (SELECT menu_cd FROM menu WHERE menu_ds = 'Cadastro' AND menu_cd_pai  IS NULL ) ,
  'Levantamento de Mercado',  0, 1, CURRENT_TIMESTAMP, 0, 25, NULL, 
                null FROM dual WHERE NOT EXISTS (SELECT * 
                FROM menu WHERE menu_ds = 'Levantamento de Mercado');
                
INSERT INTO menu (menu_cd, menu_cd_pai, menu_ds, menu_nr_versao, menu_in_uso, menu_tm_ultima_alteracao, menu_in_alcada, menu_nr_ordem,
                resi_cd, menu_ds_url) SELECT sq_menu_cd.NEXTVAL, ((SELECT menu_cd FROM menu WHERE menu_ds = 'Levantamento de Mercado' )),
  'Agente',  0, 1, CURRENT_TIMESTAMP, 0, 10, NULL, 
                'exibirPesquisaAgente' FROM dual WHERE NOT EXISTS (SELECT * 
                FROM menu WHERE menu_ds = 'Agente');

 
-- ################################### OPERAÇÃO SISTEMA ####################################
 insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'PESQUISAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%PESQUISAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'INCLUIR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%INCLUIR%' ); 


    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'DETALHAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%DETALHAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'REMOVER', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%REMOVER%' ); 
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'ALTERAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds like 'Agente') 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Agente')  and opsi_DS LIKE '%ALTERAR%' ); 
  
  -- ################################### RECURSO SISTEMA ####################################
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirPesquisaAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%PESQUISAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirPesquisaAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirInclusaoAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%INCLUIR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirInclusaoAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirAlteracaoAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%ALTERAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirAlteracaoAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirDetalhamentoAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%DETALHAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirDetalhamentoAgente') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'removerAgente', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds like '%Agente%') 
  and opsi_DS LIKE '%REMOVER%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'removerAgente') ;
  
  -- ################################### PERMISSÃO SISTEMA ####################################
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'PESQUISAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'PESQUISAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'INCLUIR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'INCLUIR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'ALTERAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'ALTERAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'DETALHAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'DETALHAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );

  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Agente'),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'REMOVER' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente')),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Agente') and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'REMOVER' and menu_cd =  (select menu_cd from menu where menu_ds = 'Agente') ) );

  
 -- ################################### LEVANTAMENTO DE MERCADO #################################### 

 
CREATE SEQUENCE SQ_LEME_CD MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;
CREATE OR REPLACE PUBLIC SYNONYM SQ_LEME_CD FOR SQ_LEME_CD;
GRANT SELECT ON SQ_LEME_CD TO GGAS_ADMIN;

CREATE TABLE LEVANTAMENTO_MERCADO 
    ( 
     LEME_CD                  NUMBER  (9, 0)      NOT NULL , 
     IMOV_CD                  NUMBER  (9, 0)      NOT NULL ,
     ENCO_CD_TIPO_COMBUSTIVEL NUMBER  (9, 0)      NOT NULL ,
     ENCO_CD_APARELHO		  NUMBER  (9, 0)      NOT NULL ,
     ENCO_CD_FORNECEDOR       NUMBER  (9, 0)      NOT NULL ,
	 ENCO_CD_REDE_ESTADO      NUMBER  (9, 0)      NOT NULL ,
     SEGM_CD                  NUMBER  (9, 0)      NOT NULL ,
     AGEN_CD			      NUMBER  (9, 0)      NOT NULL ,
     REMA_CD                  NUMBER  (9, 0)      NOT NULL ,
     LEME_MODALI_USO          VARCHAR (200)           NULL ,
     LEME_CONSUMO             NUMBER  (9, 2)      NOT NULL ,
     LEME_PRECO               NUMBER  (9, 2)      NOT NULL ,
     LEME_PERIODICIDADE       VARCHAR (50)        NOT NULL ,
	 LEME_CILINDRO			  VARCHAR (100)		  	  NULL ,
     LEME_DT_VIGENCIA_INICIO  TIMESTAMP           NOT NULL ,
	 LEME_DT_VIGENCIA_FIM	  TIMESTAMP           NOT NULL ,
     LEME_CENTRAL             NUMBER  (1, 0)          NULL ,
     LEME_LOCAL_CENTRAL       VARCHAR (50)            NULL ,
     LEME_REDE_TEMPO          VARCHAR (10)            NULL ,   
     LEME_LOCAL_ABRIGO        VARCHAR (200)           NULL ,
     LEME_VENTILACACAO        NUMBER  (1, 0)      NOT NULL ,
     LEME_OBS                 VARCHAR (200)           NULL ,
     LEME_POSSUI_GERADOR      NUMBER  (1, 0)      NOT NULL ,
     LEME_UN_CONSUMIDORA      NUMBER  (1, 0)      NOT NULL ,
     LEME_QT_UN_CONSUMIDORA   VARCHAR (5)             NULL ,
     LEME_NR_VERSAO           NUMBER (5)          NOT NULL , 
     LEME_IN_USO              NUMBER (1)          NOT NULL , 
     LEME_TM_ULTIMAALTERACAO  TIMESTAMP           NOT NULL , 
    
	CONSTRAINT PK_LEME PRIMARY KEY ( LEME_CD),
	CONSTRAINT FK_ENCO_LEME FOREIGN KEY ( ENCO_CD_FORNECEDOR ) REFERENCES ENTIDADE_CONTEUDO ( ENCO_CD ),
	CONSTRAINT FK_ENCO_TIPO_COMBUS_LM FOREIGN KEY ( ENCO_CD_TIPO_COMBUSTIVEL ) REFERENCES ENTIDADE_CONTEUDO ( ENCO_CD ),
	CONSTRAINT FK_ENCO_APARELHO_LM FOREIGN KEY ( ENCO_CD_APARELHO ) REFERENCES ENTIDADE_CONTEUDO ( ENCO_CD ),
	CONSTRAINT FK_ENCO_REDE_LM FOREIGN KEY ( ENCO_CD_REDE_ESTADO ) REFERENCES ENTIDADE_CONTEUDO ( ENCO_CD ),
	CONSTRAINT FK_LM_SEGMENTO FOREIGN KEY ( SEGM_CD ) REFERENCES SEGMENTO ( SEGM_CD ),
	CONSTRAINT FK_LM_IMOVEL FOREIGN KEY ( IMOV_CD ) REFERENCES IMOVEL ( IMOV_CD ),
	CONSTRAINT FK_LM_AGENTE FOREIGN KEY ( AGEN_CD ) REFERENCES AGENTE ( AGEN_CD ),
	CONSTRAINT FK_LM_REMA FOREIGN KEY ( REMA_CD ) REFERENCES REDE_MATERIAL ( REMA_CD )
	) 
;



-- ################################### MENU ####################################
INSERT INTO menu (menu_cd, menu_cd_pai, menu_ds, menu_nr_versao, menu_in_uso, menu_tm_ultima_alteracao, menu_in_alcada, menu_nr_ordem,
                resi_cd, menu_ds_url) SELECT sq_menu_cd.NEXTVAL, ((SELECT menu_cd FROM menu WHERE menu_ds = 'Levantamento de Mercado' )),
  'Levantamento de Mercado',  0, 1, CURRENT_TIMESTAMP, 0, 20, NULL, 
                'exibirPesquisaLevantamentoMercado' FROM dual WHERE NOT EXISTS (SELECT * 
FROM menu WHERE menu_ds = 'Levantamento de Mercado' and menu_cd_pai = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado'));

 -- ################################### OPERAÇÃO SISTEMA ####################################
 insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'PESQUISAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%PESQUISAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'INCLUIR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%INCLUIR%' ); 


    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'DETALHAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%DETALHAR%' );
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'REMOVER', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%REMOVER%' ); 
  
    insert into operacao_sistema select sq_opsi_cd.nextval,( select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%'),
  'ALTERAR', 0, 0, 0, 1, CURRENT_TIMESTAMP, (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  from dual where not exists (select * from operacao_sistema where menu_cd = (select menu_cd from menu where
  menu_ds like 'Levantamento de Mercado' and menu_ds_url is not null)  and opsi_DS LIKE '%ALTERAR%' ); 

   -- ################################### RECURSO SISTEMA ####################################
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirPesquisaLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%PESQUISAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirPesquisaLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirInclusaoLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%INCLUIR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirInclusaoLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirAlteracaoLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%ALTERAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirAlteracaoLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'exibirDetalhamentoLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%DETALHAR%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'exibirDetalhamentoLevantamentoMercado') ;
  
  INSERT INTO recurso_sistema select sq_resi_cd.nextval, 'removerLevantamentoMercado', 0, 
  (select OPSI_CD from operacao_sistema where menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) 
  and opsi_DS LIKE '%REMOVER%'), 1, 1, CURRENT_TIMESTAMP from dual where not exists (select * 
  from recurso_sistema where resi_ds = 'removerLevantamentoMercado') ;
  

    -- ################################### PERMISSÃO SISTEMA ####################################
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'PESQUISAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'PESQUISAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );

  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'INCLUIR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'INCLUIR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'ALTERAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'ALTERAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'DETALHAR' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'DETALHAR' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
  select sq_PESI_CD.nextval,(select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null),(select opsi_cd from OPERACAO_SISTEMA
  where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%') and OPSI_DS = 'REMOVER' 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null)),0,0,1,0
  ,current_timestamp from dual where not exists (select * from PERMISSAO_SISTEMA where pasi_cd = 1 
  and menu_cd = (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) and opsi_cd =
  (select opsi_cd from OPERACAO_SISTEMA where MOSI_CD = (select mosi_cd from modulo_sistema where mosi_ds like '%Cadastro%')
  and OPSI_DS = 'REMOVER' and menu_cd =  (select menu_cd from menu where menu_ds = 'Levantamento de Mercado' and menu_ds_url is not null) ) );
  
  --################################# Entidades Classes ##################################### --
  
  INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Fornecedores de Outros Combustíveis' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Fornecedores de Outros Combustíveis');
      
      INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Aparelhos Maior Índice de Consumo' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Aparelhos Maior Índice de Consumo');
      
         INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Tipos de Fontes Energéticas' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Tipos de Fontes Energéticas');
	  
	  INSERT INTO ENTIDADE_CLASSE
    (ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
    SELECT SQ_ENCL_CD.NEXTVAL,
    'Estado da Rede' ,
      1 ,
      NULL ,
      1 ,
      CURRENT_TIMESTAMP
      FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Estado da Rede');
  
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_FORNECEDORES_OUTROS_COMBUSTIVEIS',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Fornecedores de Outros Combustíveis') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_FORNECEDORES_OUTROS_COMBUSTIVEIS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_APARELHOS_MAIOR_INDICE_CONSUMO',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Aparelhos Maior Índice de Consumo') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_APARELHOS_MAIOR_INDICE_CONSUMO');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_TIPOS_FONTES_ENERGETICAS',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Tipos de Fontes Energéticas') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_TIPOS_FONTES_ENERGETICAS');
 
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_ESTADO_REDE_GLP',
'Indica os Fornecedores de Combustíveis', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Estado da Rede') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_ESTADO_REDE_GLP');
  
commit;