Insert into entidade_conteudo (ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (select encl_cd from entidade_classe where upper(encl_ds) like 'TIPO DO DOCUMENTO IMPRESSAO'),
'RELATORIO NOTA DE DEBITO', 'NODE','1','1','1',current_timestamp,'1'
from dual where not exists (select * from entidade_conteudo where encl_cd = (select encl_cd from entidade_classe where upper(encl_ds) like 'TIPO DO DOCUMENTO IMPRESSAO')
and enco_ds like 'RELATORIO NOTA DE DEBITO' );

Insert into documento_impressao_layout (DOIL_CD,DOIL_NM,DOIL_DS,DOIL_NM_ARQUIVO,ENCO_CD_DOCUMENTO_IMPRES_TIPO,DOIL_NR_VERSAO,DOIL_IN_USO,
DOIL_TM_ULTIMA_ALTERACAO) 
select SQ_DOIL_CD.nextval, 'RELATORIO NOTA DE DEBITO', ' relatorio nota de debito','relatorioNotaDebitoBoleto.jasper',
(select enco_cd from entidade_conteudo where encl_cd = (select encl_cd from entidade_classe where upper(encl_ds) like 'TIPO DO DOCUMENTO IMPRESSAO')
and enco_ds like 'RELATORIO NOTA DE DEBITO' ),
'1','1',current_timestamp from dual where not exists (select * from documento_impressao_layout where doil_nm like 'RELATORIO NOTA DE DEBITO');

Insert into constante_sistema (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where tabe_nm_mnemonico = 'ENCO'),
(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'CADASTRO'),
'C_RELATORIO_NOTA_DEBITO_BOLETO_ATIVO','Chave Primaria da Tabela Entidade Conteudo Modelo Layout Impressoão Nota Debito',
(select enco_cd from entidade_conteudo where encl_cd = (select encl_cd from entidade_classe where upper(encl_ds) like 'TIPO DO DOCUMENTO IMPRESSAO')
and enco_ds like 'RELATORIO NOTA DE DEBITO' ),
null,'0','1', current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_RELATORIO_NOTA_DEBITO_BOLETO_ATIVO');



update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'), cost_vl = (select encl_cd from entidade_classe where upper(encl_ds) like '%EQUIPAMENTO%') where cost_nm like 'C_ENT_CLASSE_EQUIPAMENTO';
update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'), cost_vl = (select encl_cd from entidade_classe where upper(encl_ds) like '%TURNO%') where cost_nm like 'C_ENT_CLASSE_TURNO';
update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'), cost_vl = (select encl_cd from entidade_classe where upper(encl_ds) like '%TELA ATUALIZA%') where cost_nm like 'C_ENT_CLASSE_TELA_ATUALIZACAO';
update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'), cost_vl = (select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA IM%VEL'), COST_CD_CLASSE = (select encl_cd from entidade_classe where upper(encl_ds) like '%TELA ATUALIZA%') where cost_nm like 'C_ATUALIZACAO_TELA_IMOVEL';
update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'), cost_vl = (select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA CLIENTE'), COST_CD_CLASSE = (select encl_cd from entidade_classe where upper(encl_ds) like '%TELA ATUALIZA%') where cost_nm like 'C_ATUALIZACAO_TELA_CLIENTE';
update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'), cost_vl = (select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA CONTRATO'), COST_CD_CLASSE = (select encl_cd from entidade_classe where upper(encl_ds) like '%TELA ATUALIZA%') where cost_nm like 'C_ATUALIZACAO_TELA_CONTRATO';
update constante_sistema set tabe_cd = (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'), cost_vl = (select enco_cd from entidade_conteudo where upper(enco_ds) like 'TELA ASSOCIA%O MEDIDOR PONTO DE CONSUMO'), COST_CD_CLASSE = (select encl_cd from entidade_classe where upper(encl_ds) like '%TELA ATUALIZA%') where cost_nm like 'C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO';
commit;