CREATE SEQUENCE  "SQ_ISTR_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;
 
CREATE TABLE IMOVEL_SERVICO_TIPO_RESTRICAO
(
  ISTR_CD number(10) PRIMARY KEY,
  IMOV_CD number(10) not null,
  SRTI_CD number(10) not null,
  ISTR_IN_USO number(1) NOT NULL,
  ISTR_TM_ULTIMA_ALTERACAO timestamp NOT NULL,
  ISTR_NR_VERSAO number(5,0) NOT NULL,
  CONSTRAINT ISTR_IMOV_FK FOREIGN KEY (IMOV_CD) REFERENCES IMOVEL(IMOV_CD),
  CONSTRAINT ISTR_SRTI_FK FOREIGN KEY (SRTI_CD) REFERENCES SERVICO_TIPO(SRTI_CD)
);

INSERT INTO TABELA 
	(	TABE_CD,									TABE_NM,
		TABE_DS,									TABE_NM_MNEMONICO,			
		TABE_IN_ATRIBUTO_DINAMICO,					TABE_NR_VERSAO,								
		TABE_IN_USO,								TABE_TM_ULTIMA_ALTERACAO,
		TABE_IN_CONSULTA_DINAMICA,					TABE_NM_CLASSE,		
		TABE_IN_AUDITAVEL,							TABE_IN_ALCADA,	
		MENU_CD,									TABE_NR_MESES_DESCARTE,	
		TABE_IN_INTEGRACAO,							TABE_IN_MAPEAMENTO,				
		TABE_IN_CONSTANTE) 
SELECT
		SQ_TABE_CD.nextval,							'IMOVEL_SERVICO_TIPO_RESTRICAO',
		'Restrição de tipos de serviço do imóvel',	'ISTR',
		0,											0,
		1,											current_timestamp,
		1,											'br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao',
		1,											0,						
		null,										null,
		1,											0,								
		0 
FROM dual where not exists (select * from TABELA where TABE_NM='IMOVEL_SERVICO_TIPO_RESTRICAO');

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO'),
		'ISTR_CD',									'Chave primária gerada automaticamente e controlada pela sequence SQ_ISTR_CD',
		'chavePrimaria',							0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO') and TACO_NM = 'ISTR_CD');

COMMENT ON COLUMN IMOVEL_SERVICO_TIPO_RESTRICAO.ISTR_CD IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_ISTR_CD ';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO'),
		'IMOV_CD',									'Chave Estrangeira da tabela IMOVEL',
		'imovel',									0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO') and TACO_NM = 'IMOV_CD');

COMMENT ON COLUMN IMOVEL_SERVICO_TIPO_RESTRICAO.IMOV_CD IS 'Chave Estrangeira da tabela IMOVEL';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO'),
		'SRTI_CD',									'Chave Estrangeira da tabela SERVICO_TIPO',
		'servicoTipo',								0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO') and TACO_NM = 'SRTI_CD');

COMMENT ON COLUMN IMOVEL_SERVICO_TIPO_RESTRICAO.SRTI_CD IS 'Chave Estrangeira da tabela SERVICO_TIPO';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO'),
		'ISTR_NR_VERSAO',							'Numero da versao do registro, utilizado para controle de concorrencia.',
		'versao',									0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO') and TACO_NM = 'SRTI_CD');

COMMENT ON COLUMN IMOVEL_SERVICO_TIPO_RESTRICAO.ISTR_NR_VERSAO IS 'Numero da versão do registro, utilizado para controle de concorrencia.';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO'),
		'ISTR_IN_USO',								'Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)',
		'habilitado',								0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO') and TACO_NM = 'SRTI_CD');

COMMENT ON COLUMN IMOVEL_SERVICO_TIPO_RESTRICAO.ISTR_IN_USO IS 'Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO'),
		'ISTR_TM_ULTIMA_ALTERACAO',					'Data e hora da realizacao da ultima alteracao.',
		'ultimaAlteracao',							0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'IMOVEL_SERVICO_TIPO_RESTRICAO') and TACO_NM = 'SRTI_CD');

COMMENT ON COLUMN IMOVEL_SERVICO_TIPO_RESTRICAO.ISTR_TM_ULTIMA_ALTERACAO IS 'Data e hora da realizacao da ultima alteracao.';


COMMIT;