UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTributo' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tributo';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTributo' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tributo';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirTributo' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=tributo';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTributo' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=tributo';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarTributo' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=tributo';


UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTributo' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=tributo';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirTributo' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=tributo';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarTributo' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=tributo';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTributo' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=tributo';

COMMIT;