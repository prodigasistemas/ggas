ALTER TABLE CHAMADO_ASSUNTO ADD "QUES_CD" NUMBER(9,0) DEFAULT NULL;
COMMENT ON COLUMN CHAMADO_ASSUNTO."QUES_CD" IS 'Chave primaria do questionario de satisfação';

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO'),'QUES_CD' , 'Chave primaria do questionario de satisfação','questionario',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO') and TACO_NM = 'QUES_CD');

COMMIT;