insert into ENTIDADE_CLASSE
(ENCL_CD,ENCL_DS,ENCL_DS_ABREVIADO,
ENCL_NR_VERSAO,ENCL_IN_USO,ENCL_TM_ULTIMA_ALTERACAO)
select SQ_ENCL_CD.NEXTVAL,
	'Tipo de vigência da Substituição Tributária',
	null,1,1,current_timestamp
from dual where not exists
(select * from ENTIDADE_CLASSE where ENCL_DS LIKE 'Tipo de vigência da Substituição Tributária');

insert into CONSTANTE_SISTEMA
(COST_CD,TABE_CD,MOSI_CD,COST_NM,
COST_DS,COST_VL,COST_CD_CLASSE,
COST_NR_VERSAO,COST_IN_USO,
COST_TM_ULTIMA_ALTERACAO)
select sq_cost_cd.nextval,
	(select tabe_cd from TABELA where TABE_NM LIKE 'ENTIDADE_CLASSE'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'FATURAMENTO'),
	'TIPO_VIGENCIA_SUBSTITUICAO_TRIBUTARIA',
	'Tipo de vigência da Substituição Tributária',
	(select ENCL_CD 
		from ENTIDADE_CLASSE 
		where ENCL_DS 
		like 'Tipo de vigência da Substituição Tributária'),
	null,
	1,1,current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where COST_NM like 'TIPO_VIGENCIA_SUBSTITUICAO_TRIBUTARIA');

insert into ENTIDADE_CONTEUDO
(ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,
ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,
ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO)
select SQ_ENCO_CD.NEXTVAL,
	(select ENCL_CD from ENTIDADE_CLASSE where ENCL_DS like 'Tipo de vigência da Substituição Tributária'),
	'Data atual',
	null,
	1,1,1,
	current_timestamp,null
from dual where not exists
(select * from ENTIDADE_CONTEUDO where ENCO_DS like 'Data atual');

insert into ENTIDADE_CONTEUDO
(ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,
ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,
ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO)
select SQ_ENCO_CD.NEXTVAL,
	(select ENCL_CD from ENTIDADE_CLASSE where ENCL_DS like 'Tipo de vigência da Substituição Tributária'),
	'Data do último dia do consumo',
	null,
	0,1,1,
	current_timestamp,null
from dual where not exists
(select * from ENTIDADE_CONTEUDO where ENCO_DS like 'Data do último dia do consumo');

insert into CONSTANTE_SISTEMA
(COST_CD,TABE_CD,MOSI_CD,COST_NM,
COST_DS,COST_VL,COST_CD_CLASSE,
COST_NR_VERSAO,COST_IN_USO,
COST_TM_ULTIMA_ALTERACAO)
select sq_cost_cd.nextval,
	(select tabe_cd from TABELA where TABE_NM LIKE 'ENTIDADE_CONTEUDO'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'FATURAMENTO'),
	'VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ATUAL',
	'Tributo vigente na data do faturamento',
	(select ENCO_CD 
	from ENTIDADE_CONTEUDO 
	where ENCO_DS 
	like 'Data atual' and ENCL_CD = 
		(select ENCL_CD 
		from ENTIDADE_CLASSE 
		where ENCL_DS 
		like 'Tipo de vigência da Substituição Tributária')),
	null,
	1,1,current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where COST_NM like 'VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ATUAL');

insert into CONSTANTE_SISTEMA
(COST_CD,TABE_CD,MOSI_CD,COST_NM,
COST_DS,COST_VL,COST_CD_CLASSE,
COST_NR_VERSAO,COST_IN_USO,
COST_TM_ULTIMA_ALTERACAO)
select sq_cost_cd.nextval,
	(select tabe_cd from TABELA where TABE_NM LIKE 'ENTIDADE_CONTEUDO'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'FATURAMENTO'),
	'VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ULTIMO_DIA_CONSUMO',
	'Tributo vigente no período de consumo',
	(select ENCO_CD 
	from ENTIDADE_CONTEUDO 
	where ENCO_DS 
	like 'Data do último dia do consumo' and ENCL_CD = 
		(select ENCL_CD 
		from ENTIDADE_CLASSE 
		where ENCL_DS 
		like 'Tipo de vigência da Substituição Tributária')),
	null,
	1,1,current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where COST_NM like 'VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ULTIMO_DIA_CONSUMO');

insert into PARAMETRO_SISTEMA 
(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,
PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select sq_pmsi_cd.nextval,
	'P_VIGENCIA_SUBSTITUICAO_TRIBUTARIA',
	'Indicador de vigência do tributo 1-Tributo vigente na data do faturamento; 0-Tributo vigente no período de consumo',
	(select ENCO_CD 
	from ENTIDADE_CONTEUDO 
	where ENCO_DS 
	like 'Data atual' and ENCL_CD = 
		(select ENCL_CD 
		from ENTIDADE_CLASSE 
		where ENCL_DS 
		like 'Tipo de vigência da Substituição Tributária')),
	null,
	'1',
	'1',
	'1',
	current_timestamp,
	null,
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'FATURAMENTO')
from dual where not exists
(select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO like 'P_VIGENCIA_SUBSTITUICAO_TRIBUTARIA');

commit;