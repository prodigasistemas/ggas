ALTER TABLE FUNCIONARIO ADD FUNC_MM_FOTO BLOB;

INSERT INTO RECURSO_SISTEMA 
VALUES (SQ_RESI_CD.nextval, 'exibirFotoFuncionario', 0,
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaFuncionario'))
, 1, 1, CURRENT_TIMESTAMP);


INSERT INTO ENTIDADE_CLASSE
VALUES (SQ_ENCL_CD.nextval, 'Motivos Afastamento', NULL, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO ENTIDADE_CONTEUDO
VALUES (SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivos Afastamento'),
'Férias', NULL, 0, 1, 1, CURRENT_TIMESTAMP, NULL);


CREATE SEQUENCE  "SQ_FUAF_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

CREATE TABLE FUNCIONARIO_AFASTAMENTO
(
	FUAF_CD NUMBER(10) PRIMARY KEY,
	FUNC_CD NUMBER(10) NOT NULL,
	FUAF_DT_INICIO TIMESTAMP(6) NOT NULL,
	FUAF_DT_FIM TIMESTAMP(6),
	FUAF_DS_OBSERVACAO CLOB,
	ENCO_CD_MOTIVO_AFASTAMENTO NUMBER(10) NOT NULL,
	FUAF_IN_USO NUMBER(1) NOT NULL,
	FUAF_TM_ULTIMA_ALTERACAO TIMESTAMP NOT NULL,
	FUAF_NR_VERSAO NUMBER(5) NOT NULL
);


ALTER TABLE FUNCIONARIO_AFASTAMENTO
ADD CONSTRAINT FUAF_ENCO_FK FOREIGN KEY (ENCO_CD_MOTIVO_AFASTAMENTO) REFERENCES ENTIDADE_CONTEUDO(ENCO_CD) ENABLE;

ALTER TABLE FUNCIONARIO_AFASTAMENTO
ADD CONSTRAINT FUAF_FUNC_FK FOREIGN KEY (FUNC_CD) REFERENCES FUNCIONARIO(FUNC_CD) ENABLE;

INSERT INTO RECURSO_SISTEMA 
VALUES (SQ_RESI_CD.nextval, 'carregarAdicionarAfastamentoFuncionario', 0,
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'INCLUIR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaFuncionario'))
, 1, 1, CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA 
VALUES (SQ_RESI_CD.nextval, 'removerAfastamento', 0,
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'INCLUIR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaFuncionario'))
, 1, 1, CURRENT_TIMESTAMP);


COMMIT;