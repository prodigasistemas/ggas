UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaContrato' WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato.do?acao=exibirPesquisaContrato%tela=contrato';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirPesquisaContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'pesquisarContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'excluirContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirDetalhamentoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoContratoPontoConsumo', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirDetalhamentoContratoPontoConsumo.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'popularCamposContratoPontoConsumoDetalhamento', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'popularCamposContratoPontoConsumoDetalhamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'popularCamposContratoPontoConsumoInclusao', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'popularCamposContratoPontoConsumoInclusao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'aditarContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ADITAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'aditarContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAditamentoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ADITAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirAditamentoContrato.do' AND opsi_cd != (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato'));

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAditamentoContratoPontoConsumo', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ADITAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirAditamentoContratoPontoConsumo.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoContratoPontoConsumo', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirInclusaoContratoPontoConsumo.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'reexibirAditamentoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'reexibirAditamentoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'aplicarDadosContratoPontoConsumoInclusao', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'aplicarDadosContratoPontoConsumoInclusao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'aplicarDadosContratoPontoConsumoAditamento', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'aplicarDadosContratoPontoConsumoAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirItemFaturamento', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'excluirItemFaturamentoAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'adicionarItemFaturamento', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'adicionarItemFaturamentoAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'retirarImovelInclusaoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'retirarImovelInclusaoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'retirarImovelAditamentoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'retirarImovelAditamentoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirModalidadeContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'excluirModalidadeContratoAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'adicionarModalidadeContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'adicionarModalidadeContratoAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirSalvarAditamentoPontoConsumo', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ADITAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirSalvarAditamentoPontoConsumo.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerContratoCliente', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'removerContratoClienteFluxoInclusao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'adicionarCompromissoTOPAbaModalidade', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'adicionarCompromissoTOPAbaModalidadeAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirCompromissoTOPAbaModalidade', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'excluirCompromissoTOPAbaModalidadeAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'adicionarCompromissoTOPAbaGeral', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'adicionarCompromissoTOPAbaGeralAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirCompromissoTOPAbaGeral', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'excluirCompromissoTOPAbaGeralAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirEditarCompromissoTOP', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirEditarCompromissoTOP.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirPenalidadeRetiradaMaiorMenorGeralAditamento', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'excluirPenalidadeRetiradaMaiorMenorGeralAditamento.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirPenalidadeRetiradaMaiorMenorGeral', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'EXCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'excluirPenalidadeRetiradaMaiorMenorGeral.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirEditarPenalidadeRetiradaMaiorMenorGeral', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirEditarPenalidadeRetiradaMaiorMenorGeral.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPontoConsumoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ENCERRAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirPontoConsumoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirSalvarAlteracaoPontoConsumo', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirSalvarAlteracaoPontoConsumo.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ALTERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'alterarContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirInclusaoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'reexibirInclusaoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'reexibirInclusaoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'salvarContratoParcialPasso1', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'SALVAR PARCIAL' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'salvarContratoParcialPasso1.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'salvarContratoParcialPasso2', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'SALVAR PARCIAL' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'salvarContratoParcialPasso2.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'selecionarImovelContratoFluxoIncluir', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'selecionarImovelInclusaoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'incluirContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirEncerrarRescindirContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'ENCERRAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirEncerrarRescindirContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirMigracaoModeloContratoFluxoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'MIGRAR_MODELO_CONRTATO' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirMigracaoModeloContratoFluxoContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirMigracaoModeloContratoCamposAlterados', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirMigracaoModeloContratoCamposAlterados.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'salvarMigracaoModeloContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'INCLUIR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'salvarMigracaoModeloContrato.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirLogErroMigracaoContrato', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirLogErroMigracao.do';

-------------------------------

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirAlteracaoModalidadeCadastrada', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirAlteracaoModalidadeCadastrada') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirAlteracaoItemFaturamentoCadastrado', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirAlteracaoItemFaturamentoCadastrado') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'adicionarContratoCliente', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'INCLUIR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'adicionarContratoCliente') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'popularCamposContratoPontoConsumoAditamento', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'ADITAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'popularCamposContratoPontoConsumoAditamento') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'adicionarCompromissoSOPAbaModalidade', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'INCLUIR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'adicionarCompromissoSOPAbaModalidade') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'excluirCompromissoSOPAbaModalidade', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'EXCLUIR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'excluirCompromissoSOPAbaModalidade') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'selecionarImovelContratoFluxoAditarAlterar', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'selecionarImovelContratoFluxoAditarAlterar') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'adicionarPenalidadeRetiradaMaiorMenorGeral', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'adicionarPenalidadeRetiradaMaiorMenorGeral') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'selecionarImovelContratoFluxoIncluirAbaProposta', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'selecionarImovelContratoFluxoIncluirAbaProposta') ;

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirPopupMudancaTitularidade', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirPopupMudancaTitularidade');

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'incluirMudancaTitularidade', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'INCLUIR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'incluirMudancaTitularidade');

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirEncerrarContratoChamado', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'ENCERRAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirEncerrarContratoChamado');

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirImprimirContrato', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirImprimirContrato');

------------------------------------------

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaPropostaPopup' WHERE resi_ds LIKE 'exibirPesquisaPropostaPopup.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarPropostaPopup' WHERE resi_ds LIKE 'pesquisarPropostaPopup.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'salvarOrdemFaturamentoContrato' WHERE RESI_DS LIKE '%salvarOrdemFaturamentoContrato.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarImovelContratoMigrar' WHERE resi_ds LIKE 'pesquisarImovelContratoMigrar.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'transferirSaldo' WHERE resi_ds LIKE 'transferirSaldo.do';
--------------------------------------------
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirMigrarSaldo', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'exibirMigrarSaldo.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarContratoMigrar', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds_url LIKE 'exibirPesquisaContrato')) WHERE resi_ds LIKE 'pesquisarContratoMigrar.do';
INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirOrdemFaturamentoContrato', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirOrdemFaturamentoContrato');
INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirDadosFaturamentoContratoPopup', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'CONSULTAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirDadosFaturamentoContratoPopup');
INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'salvarOrdemFaturamentoContrato', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'ALTERAR' AND MOSI_CD = (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%')  AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'salvarOrdemFaturamentoContrato');

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'exibirCopiaDadosPontoConsumoPopup', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'INCLUIR' AND MOSI_CD IN (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%') AND MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE '%exibirPesquisaContrato%'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like '%exibirCopiaDadosPontoConsumoPopup%') ;
INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'copiarDadosPontoConsumo', 0, ((SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS LIKE 'INCLUIR' AND MOSI_CD IN (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS LIKE '%Contrato%') AND MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE '%exibirPesquisaContrato%'))), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like '%copiarDadosPontoConsumo%') ;

UPDATE RECURSO_SISTEMA SET RESI_DS = 'imprimirFatura' WHERE RESI_DS LIKE '%imprimirFatura.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirNotaMultaRecisoria' WHERE RESI_DS LIKE '%incluirNotaMultaRecisoria.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'finalizarContrato' WHERE RESI_DS LIKE '%finalizarContrato.do%';

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) select SQ_RESI_CD.nextval, 'aprovarContrato', 0, (select OPSI_CD from OPERACAO_SISTEMA WHERE MENU_CD IN (SELECT MENU_CD FROM MENU WHERE MENU_DS LIKE '%Contrato%')AND OPSI_DS LIKE 'APROVAR'), 1, 1,CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'aprovarContrato');
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) 
SELECT SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato'), (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'APROVAR' AND menu_cd = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato')),'0','0', (SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'APROVAR' AND menu_cd = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL LIKE 'exibirPesquisaContrato') AND PASI_CD = (SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR')));

COMMIT;