
ALTER TABLE SERVICO_AUTORIZACAO_MOTIVO_ENC ADD "SEME_IN_FLUXO_ENCERRAMENTO" NUMBER(1,0) DEFAULT 1;
COMMENT ON COLUMN SERVICO_AUTORIZACAO_MOTIVO_ENC."SEME_IN_FLUXO_ENCERRAMENTO" IS 'Define fluxo de encerramento, 1 para encerramento e 0 para cancelamento.';
      
INSERT INTO TABELA_COLUNA
  ( TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA, TACO_IN_VARIAVEL, TACO_CD_VARIAVEL )
SELECT
  SQ_TACO_CD.nextval,
   (SELECT TABE_CD FROM TABELA WHERE TABE_NM LIKE 'SERVICO_AUTORIZACAO_MOTIVO_ENC'),
  'SEME_IN_FLUXO_ENCERRAMENTO',
  'Define fluxo de encerramento, 1 para encerramento e 0 para cancelamento.',
  'fluxoEncerramento',
  '0',
  '0',
  '1',
  CURRENT_TIMESTAMP,
  '0',
  '0',
  '0',
  NULL
  FROM DUAL WHERE NOT EXISTS
    (SELECT *
    FROM TABELA_COLUNA
    WHERE TACO_NM LIKE 'SEME_IN_FLUXO_ENCERRAMENTO'
    AND TABE_CD =
    (SELECT TABE_CD FROM TABELA WHERE TABE_NM LIKE 'SERVICO_AUTORIZACAO_MOTIVO_ENC')
  );
  
  UPDATE SERVICO_AUTORIZACAO_MOTIVO_ENC set SEME_IN_FLUXO_ENCERRAMENTO = 0 WHERE SEME_DS like 'CANCELAMENTO';

  commit;
  