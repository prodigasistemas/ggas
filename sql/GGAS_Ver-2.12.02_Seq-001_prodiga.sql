Insert into LEITURA_ANORMALIDADE (LEAN_CD,LEAN_DS,LEAN_DS_ABREVIADO,LEAC_CD_SEM_LEITURA,LEAC_CD_COM_LEITURA,LEAL_CD_SEM_LEITURA,LEAL_CD_COM_LEITURA,DOTI_CD,
LEAN_DS_MENSAGEM_LEITURA,LEAN_DS_MENSAGEM_MANUTENCAO,LEAN_DS_MENSAGEM_PREVENCAO,LEAN_NR_VEZES_CONSECUTIVAS,LEAN_IN_ACEITA_LEITURA,LEAN_IN_LISTAGEM,LEAN_IN_RETENCAO_CONTA,
LEAN_IN_ISENCAO,LEAN_IN_CREDITO_CONSUMO,LEAN_IN_FOTO,LEAN_IN_RELATIVO_MEDIDOR,LEAN_IN_IMOVEL_SEM_MEDIDOR,LEAN_IN_BLOQUEIA_FATURAMENTO,LEAN_IN_USO_SISTEMA,LEAN_NR_VEZES_BLOQUEIO,
LEAN_NR_VERSAO,LEAN_IN_USO,LEAN_TM_ULTIMA_ALTERACAO,SRTI_CD) 
select SQ_LEAN_CD.nextval,'Ponto não deve ser faturado',null,'6','6','1','1',null,null,null,null,null,'1',null,null,null,null,null,'0',null,'0','1',null,'1','1',
current_timestamp,null from dual where not exists (select * from leitura_anormalidade where upper(LEAN_DS) like 'PONTO NÃO DEVE SER FATURADO' );

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'LEITURA_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDIÇÃO'),
'C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO','Indica que nao deve faturar esse ponto',
(select lean_cd from leitura_anormalidade where upper(lean_ds) like 'PONTO NÃO DEVE SER FATURADO'),null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm = 'C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO');


COMMIT;