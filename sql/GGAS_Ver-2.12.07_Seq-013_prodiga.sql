ALTER TABLE CONSUMO_HISTORICO MODIFY COHI_MD_CONSUMO NUMBER(20,8);
ALTER TABLE CONSUMO_HISTORICO MODIFY COHI_MD_CONSUMO_MEDIDO NUMBER(20,8);
ALTER TABLE CONSUMO_HISTORICO MODIFY COHI_MD_CONSUMO_APURADO NUMBER(20,8);

COMMIT;