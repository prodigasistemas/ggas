UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaConsultaExportacao' WHERE MENU_DS_URL LIKE 'exibirPesquisaConsultaExportacao.do?acao=exibirPesquisaConsultaExportacao%tela=consultaExportacao';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaConsultaExportacao' WHERE RESI_DS LIKE 'exibirPesquisaConsultaExportacao.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoConsultaExportacao' WHERE RESI_DS LIKE 'exibirAlteracaoConsultaExportacao.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoConsultaExportacao' WHERE RESI_DS LIKE 'exibirInclusaoConsultaExportacao.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirExportacaoDados' WHERE RESI_DS LIKE 'exibirExportacaoDados.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarConsultaExportacao' WHERE RESI_DS LIKE 'pesquisarConsultaExportacao.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirConsultaExportacao' WHERE RESI_DS LIKE 'incluirConsultaExportacao.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exportarDados' WHERE RESI_DS LIKE 'exportarDados.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarConsultaExportacao' WHERE RESI_DS LIKE 'alterarConsultaExportacao.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirConsultaExportacao' WHERE RESI_DS LIKE 'excluirConsultaExportacao.do';

COMMIT;