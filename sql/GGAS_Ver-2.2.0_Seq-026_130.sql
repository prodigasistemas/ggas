alter table FATURAMENTO_GRUPO add FAGR_IN_DT_VENC_ULT_DIA_CICLO NUMBER(1) DEFAULT 0;
COMMENT ON COLUMN FATURAMENTO_GRUPO.FAGR_IN_DT_VENC_ULT_DIA_CICLO IS 'Indicador para calculo da data de vencimento baseado no ultimo dia do ciclo 1- SIM, 0 - NAO ';
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'FATURAMENTO_GRUPO'),'FAGR_IN_DT_VENC_ULT_DIA_CICLO' , 'Indicador data de vencimento ultimo dia do ciclo','indicadorDtVencimentoUltimoDiaCiclo',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'FATURAMENTO_GRUPO') and TACO_NM = 'FAGR_IN_DT_VENC_ULT_DIA_CICLO');

commit;