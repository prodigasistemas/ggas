INSERT INTO PARAMETRO_SISTEMA (
  PMSI_CD,
  PMSI_CD_PARAMETRO,
  PMSI_DS_PARAMETRO,
  PMSI_VL_PARAMETRO,
  PMSI_DS_COMPLEMENTO,
  PMSI_CD_TIPO_PARAMETRO,
  PMSI_NR_VERSAO,
  PMSI_IN_USO,
  PMSI_TM_ULTIMA_ALTERACAO,
  TABE_CD,
  MOSI_CD
)
VALUES (
  sq_pmsi_cd.NEXTVAL,
  'P_FATURA_CONSUMO_ZERO',
  'Indica se o faturar grupo irá faturar consumo 0',
  1,
  null,
  1,
  0,
  1,
  current_timestamp,
  null,
  3
);


COMMIT;