UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaPavimentoCalcada' WHERE MENU_DS_URL LIKE '%pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalharPavimentoCalcada' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%tela=pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaPavimentoCalcada' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%tela=pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirIncluirPavimentoCalcada' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%tela=pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlterarPavimentoCalcada' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%tela=pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarPavimentoCalcada' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%tela=pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarPavimentoCalcada' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%tela=pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerPavimentoCalcada' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%tela=pavimentoCalcada%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirPavimentoCalcada'  WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%tela=pavimentoCalcada%';

COMMIT;