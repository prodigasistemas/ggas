create table TI_NFE
(
  TI_NFE_CD_NF                   NUMBER not null,
  TI_EMPR_CD_EMPRESA             NUMBER not null,
  TI_NFE_IN_FORMA_EMISSAO        VARCHAR2(1),
  TI_NFE_IN_DANFE_IMPRESSA       NUMBER default 0 not null,
  TI_NFE_NR_DIGITO_VERIFICADOR   NUMBER,
  TI_NFE_DS_ARQUIVO_XML          BLOB,
  TI_NFE_NR_IBGE_UF              VARCHAR2(2) not null,
  TI_NFE_NR_CHAVE_ACESSO         VARCHAR2(44),
  TI_NFE_DS_NATUREZA_OPERACAO    VARCHAR2(60) not null,
  TI_NFE_IN_FORMA_PAGAMENTO      NUMBER not null,
  TI_NFE_NR_MOD_DOCUMENTO_FISCAL NUMBER not null,
  TI_NFE_NR_SERIE                NUMBER not null,
  TI_NFE_NR_DOCUMENTO_FISCAL     NUMBER not null,
  TI_NFE_DT_EMISSAO              DATE not null,
  TI_NFE_DT_ENTRADA_SAIDA        DATE,
  TI_NFE_IN_ENTRADA_SAIDA        NUMBER not null,
  TI_NFE_NR_IBGE_MUN_FG          VARCHAR2(7) not null,
  TI_NFE_NR_CNPJ                 VARCHAR2(14) not null,
  TI_NFE_NM_RAZAO_SOCIAL         VARCHAR2(60) not null,
  TI_NFE_NM_FANTASIA             VARCHAR2(60),
  TI_NFE_NM_LOGRADOURO           VARCHAR2(60) not null,
  TI_NFE_NR_LOGRADOURO           VARCHAR2(60) not null,
  TI_NFE_DS_CPL_LOGRADOURO       VARCHAR2(60),
  TI_NFE_DS_BAIRRO               VARCHAR2(60) not null,
  TI_NFE_NR_IBGE_MUN             VARCHAR2(7) not null,
  TI_NFE_DS_MUN                  VARCHAR2(60) not null,
  TI_NFE_DS_UF                   VARCHAR2(2) not null,
  TI_NFE_NR_CEP                  VARCHAR2(8),
  TI_NFE_NR_PAIS                 VARCHAR2(4),
  TI_NFE_NR_TELEFONE             VARCHAR2(10),
  TI_NFE_NR_IE                   VARCHAR2(14) not null,
  TI_NFE_NR_IE_ST                VARCHAR2(14),
  TI_NFE_NR_IM                   VARCHAR2(15),
  TI_NFE_VL_BC_ICMS              NUMBER not null,
  TI_NFE_VL_ICMS                 NUMBER not null,
  TI_NFE_VL_BC_ICMS_ST           NUMBER not null,
  TI_NFE_VL_ICMS_ST              NUMBER not null,
  TI_NFE_VL_PROD                 NUMBER not null,
  TI_NFE_VL_FRETE                NUMBER not null,
  TI_NFE_VL_SEGURO               NUMBER not null,
  TI_NFE_VL_DESCONTO             NUMBER not null,
  TI_NFE_VL_IPI                  NUMBER not null,
  TI_NFE_VL_PIS                  NUMBER not null,
  TI_NFE_VL_COFINS               NUMBER not null,
  TI_NFE_VL_OUTROS               NUMBER not null,
  TI_NFE_VL_NF                   NUMBER not null,
  TI_NFE_VL_SERV                 NUMBER,
  TI_NFE_VL_BC_ISS               NUMBER,
  TI_NFE_VL_ISS                  NUMBER,
  TI_NFE_VL_PIS_ISS              NUMBER,
  TI_NFE_VL_COFINS_ISS           NUMBER,
  TI_NFE_VL_RET_PIS              NUMBER,
  TI_NFE_VL_RET_COFINS           NUMBER,
  TI_NFE_VL_RET_CSLL             NUMBER,
  TI_NFE_VL_BC_IRRF              NUMBER,
  TI_NFE_VL_IRRF                 NUMBER,
  TI_NFE_VL_BC_RET_PREV          NUMBER,
  TI_NFE_VL_RET_PREV             NUMBER,
  TI_NFE_DS_INFO_FISCO           VARCHAR2(1000),
  TI_NFE_DS_INFO_CONTRIB         VARCHAR2(4000),
  TI_NFE_NR_CNPJ_CPF_DESTINO     VARCHAR2(14),
  TI_NFE_NM_DESTINO              VARCHAR2(60) not null,
  TI_NFE_NM_LOGRADOURO_DESTINO   VARCHAR2(60) not null,
  TI_NFE_NR_LOGRADOURO_DESTINO   VARCHAR2(60) not null,
  TI_NFE_DS_CPL_DESTINO          VARCHAR2(60),
  TI_NFE_DS_BAIRRO_DESTINO       VARCHAR2(60) not null,
  TI_NFE_NR_IBGE_MUN_DESTINO     VARCHAR2(7) not null,
  TI_NFE_DS_MUNICIPIO_DESTINO    VARCHAR2(60) not null,
  TI_NFE_DS_UF_DESTINO           VARCHAR2(2) not null,
  TI_NFE_NR_CEP_DESTINO          VARCHAR2(8),
  TI_NFE_NR_PAIS_DESTINO         VARCHAR2(4),
  TI_NFE_NR_TELEFONE_DESTINO     VARCHAR2(10),
  TI_NFE_NR_IE_DESTINO           VARCHAR2(14),
  TI_NFE_NR_SUFRAMA_DESTINO      VARCHAR2(9),
  TI_NFE_DS_EMAIL                VARCHAR2(241),
  TI_NFE_CD_CNAE                 VARCHAR2(7),
  TI_NFE_DS_UF_EMBARQUE          VARCHAR2(2),
  TI_NFE_DS_LOCAL_EMBARQUE       VARCHAR2(60),
  TI_NFE_DS_NF_EMPENHO           VARCHAR2(17),
  TI_NFE_DS_PEDIDO               VARCHAR2(60),
  TI_NFE_DS_CONTRATO             VARCHAR2(60),
  TI_NFLO_CD_LOTE                NUMBER,
  TI_NFE_VL_TOTAL_II             NUMBER,
  TI_NFE_DT_CANCELAMENTO         DATE,
  TI_NFE_IN_NF_CANC              NUMBER default 0 not null,
  TI_NFE_IN_FIN_EM               NUMBER not null,
  TI_NFE_IN_PROC_NF              NUMBER default 0 not null,
  TI_USUA_CD_USUARIO             NUMBER not null,
  TI_NFE_DS_JUSTIF_CANC          VARCHAR2(255),
  TI_NFE_DT_EMIT_CONT            DATE,
  TI_USUA_CD_USU_CONT            NUMBER,
  TI_NFE_DS_XML_CANCELAMENTO     BLOB,
  TI_NFE_IN_NF_AUT               NUMBER default 0 not null,
  TI_NFE_DT_NF_AUT               DATE,
  TI_USUA_CD_USU_CANCELAMENTO    NUMBER,
  TI_NFE_DS_XML_PROT_ENV         BLOB,
  TI_NFE_DS_NR_ALEATORIO         VARCHAR2(9),
  TI_NFE_DS_MOTIVO_CONT          VARCHAR2(256),
  TI_NFE_NR_ORDEM_EMBARQUE       VARCHAR2(50),
  TI_NFE_NR_PROT_CANC            VARCHAR2(15),
  TI_NFE_SQ_ORDEM_EMBARQUE       NUMBER,
  TI_NFE_SIS_OPERACAO            VARCHAR2(1) default 'N' not null,
  TI_NFE_SIS_STATUS              VARCHAR2(3) default 'NP' not null,
  TI_NFE_NR_PROT_ENV             VARCHAR2(15),
  TI_NFE_NR_EMIT_CRT             NUMBER not null,
  TI_NFE_DANFE_PDF               BLOB,
  TI_NFE_IN_IMPRESSA_CONT        NUMBER default 0 not null,
  TI_NFE_NR_CHAVE_DADOS_NFE      VARCHAR2(36),
  TI_NFE_CD_NFE                  NUMBER,
  TI_NFE_DT_ENTRADA_SISTEMA      DATE default sysdate,
  TI_NFE_IN_EMIT_CONT            NUMBER default 0,
  TI_NFE_DS_PAIS_DESTINO         VARCHAR2(60),
  TI_NFE_DS_PAIS                 VARCHAR2(60),
  TI_NFE_NR_VERSAO               NUMBER default 1,
  TI_NFE_IN_USO                  NUMBER default 1,
  TI_NFE_TM_ULTIMA_ALTERACAO     TIMESTAMP(6) default sysdate,
  TINF_VAL_TOTAL_IMP             NUMBER
);

comment on table TI_NFE
  is 'Contém os dados da nota (cabeçalho)';
comment on column TI_NFE.TI_NFE_CD_NF
  is 'Sequencial da nota (não fiscal)';
comment on column TI_NFE.TI_EMPR_CD_EMPRESA
  is 'Codigo da empresa';
comment on column TI_NFE.TI_NFE_IN_FORMA_EMISSAO
  is 'Formade emissao da nota (NAO INFORMAR) (tpEmis)';
comment on column TI_NFE.TI_NFE_IN_DANFE_IMPRESSA
  is 'Indica se o DANFE foi impresso (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_NR_DIGITO_VERIFICADOR
  is 'Numero do digito verificador (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_DS_ARQUIVO_XML
  is 'Arquivo XML da nota (quando nao enviada em lote)';
comment on column TI_NFE.TI_NFE_NR_IBGE_UF
  is 'Numero (na tabela do IBGE) da UF';
comment on column TI_NFE.TI_NFE_NR_CHAVE_ACESSO
  is 'Chave de acesso da nota';
comment on column TI_NFE.TI_NFE_DS_NATUREZA_OPERACAO
  is 'Natureza da operacao (natOp)';
comment on column TI_NFE.TI_NFE_IN_FORMA_PAGAMENTO
  is 'Forma de pagamento (indPag)';
comment on column TI_NFE.TI_NFE_NR_MOD_DOCUMENTO_FISCAL
  is 'Numero do modelo do documento fiscal (mod)';
comment on column TI_NFE.TI_NFE_NR_SERIE
  is 'Numero da serie (serie)';
comment on column TI_NFE.TI_NFE_NR_DOCUMENTO_FISCAL
  is 'Numero do documento fiscal (nNF)';
comment on column TI_NFE.TI_NFE_DT_EMISSAO
  is 'Data de emissao (dEmi)';
comment on column TI_NFE.TI_NFE_DT_ENTRADA_SAIDA
  is 'Data da entrada/saida da mercadoria (dSaiEnt)';
comment on column TI_NFE.TI_NFE_IN_ENTRADA_SAIDA
  is 'Indicador de entrada/saida (tpNF)';
comment on column TI_NFE.TI_NFE_NR_IBGE_MUN_FG
  is 'Numero (na tabela do IBGE) do municipio do fato gerador da NF (cMunFG)';
comment on column TI_NFE.TI_NFE_NR_CNPJ
  is 'CNPJ/CPF do emitente (CNPJ/CPF)';
comment on column TI_NFE.TI_NFE_NM_RAZAO_SOCIAL
  is 'Nome ou razao social do emitente (xNome)';
comment on column TI_NFE.TI_NFE_NM_FANTASIA
  is 'Nome Fantasia do emitente (xFant)';
comment on column TI_NFE.TI_NFE_NM_LOGRADOURO
  is 'Logradouro do endereco do emitente (xLgr)';
comment on column TI_NFE.TI_NFE_NR_LOGRADOURO
  is 'Numero do endereco do emitente no logradouro (nro)';
comment on column TI_NFE.TI_NFE_DS_CPL_LOGRADOURO
  is 'Complemento do endereco do emitente (xCpl)';
comment on column TI_NFE.TI_NFE_DS_BAIRRO
  is 'Bairro do endereco do emitente (xBairro)';
comment on column TI_NFE.TI_NFE_NR_IBGE_MUN
  is 'Codigo do municipio do emitente (na tabela do BGE) (cMun)';
comment on column TI_NFE.TI_NFE_DS_MUN
  is 'Descricao do municipio do emitente (xMun)';
comment on column TI_NFE.TI_NFE_DS_UF
  is 'Sigla da UF do emitente (UF)';
comment on column TI_NFE.TI_NFE_NR_CEP
  is 'Codigo do cep do emitente (CEP)';
comment on column TI_NFE.TI_NFE_NR_PAIS
  is 'Codigo do pais do emitente (cPais)';
comment on column TI_NFE.TI_NFE_NR_TELEFONE
  is 'Fone do emitente (fone)';
comment on column TI_NFE.TI_NFE_NR_IE
  is 'Inscricao estadual do emitente (IE)';
comment on column TI_NFE.TI_NFE_NR_IE_ST
  is 'Inscricao estadual do substituto tributario (IEST)';
comment on column TI_NFE.TI_NFE_NR_IM
  is 'Inscricao municipal (IM)';
comment on column TI_NFE.TI_NFE_VL_BC_ICMS
  is 'Valor total da base de calculo do ICMS (vBC)';
comment on column TI_NFE.TI_NFE_VL_ICMS
  is 'Valor total do ICMS (vICMS)';
comment on column TI_NFE.TI_NFE_VL_BC_ICMS_ST
  is 'Valor total da base de calculo do ICMS substituto (vBCST)';
comment on column TI_NFE.TI_NFE_VL_ICMS_ST
  is 'Valor total do ICMS substituto (vST)';
comment on column TI_NFE.TI_NFE_VL_PROD
  is 'Valor total dos produtos e servicos (vProd)';
comment on column TI_NFE.TI_NFE_VL_FRETE
  is 'Valor total do frete (vFrete)';
comment on column TI_NFE.TI_NFE_VL_SEGURO
  is 'Valor total do seguro (vSeg)';
comment on column TI_NFE.TI_NFE_VL_DESCONTO
  is 'Valor total do desconto (vDesc)';
comment on column TI_NFE.TI_NFE_VL_IPI
  is 'Valor total do IPI (vIPI)';
comment on column TI_NFE.TI_NFE_VL_PIS
  is 'Valor total do PIS (vPIS)';
comment on column TI_NFE.TI_NFE_VL_COFINS
  is 'Valor total do COFINS (vCOFINS)';
comment on column TI_NFE.TI_NFE_VL_OUTROS
  is 'Valor total de outros (vOutro)';
comment on column TI_NFE.TI_NFE_VL_NF
  is 'Valor total da nota (vNF)';
comment on column TI_NFE.TI_NFE_VL_SERV
  is 'Valor total de servicos sob nao-incidencia ou nao tributados pelo ICMS (vServ)';
comment on column TI_NFE.TI_NFE_VL_BC_ISS
  is 'Valor total da base de calculo do ISS (vBC)';
comment on column TI_NFE.TI_NFE_VL_ISS
  is 'Valor total do ISS (vISS)';
comment on column TI_NFE.TI_NFE_VL_PIS_ISS
  is 'Valor do PIS sobre servicos (vPIS)';
comment on column TI_NFE.TI_NFE_VL_COFINS_ISS
  is 'Valor do COFINS sobre servicos (vCOFINS)';
comment on column TI_NFE.TI_NFE_VL_RET_PIS
  is 'Valor retido do PIS (vRetPIS)';
comment on column TI_NFE.TI_NFE_VL_RET_COFINS
  is 'Valor retido do COFINS (vRetCOFINS)';
comment on column TI_NFE.TI_NFE_VL_RET_CSLL
  is 'Valor retido do CSLL (vRetCSLL)';
comment on column TI_NFE.TI_NFE_VL_BC_IRRF
  is 'Valor da base de calculo do IRRF (vBCIRRF)';
comment on column TI_NFE.TI_NFE_VL_IRRF
  is 'Valor do IRRF (vIRRF)';
comment on column TI_NFE.TI_NFE_VL_BC_RET_PREV
  is 'Valor da base de calculo da retencao da Previdencia Social (vBCRetPrev)';
comment on column TI_NFE.TI_NFE_VL_RET_PREV
  is 'Valor da retencao da Previdencia Social (vRetPrev)';
comment on column TI_NFE.TI_NFE_DS_INFO_FISCO
  is 'Informacoes adicionais de interesse do fisco (infAdFisco)';
comment on column TI_NFE.TI_NFE_DS_INFO_CONTRIB
  is 'Informacoes adicionais de interesse do contribuinte (infCpl)';
comment on column TI_NFE.TI_NFE_NR_CNPJ_CPF_DESTINO
  is 'CNPJ/CPF do destinatario (CNPJ/CPF)';
comment on column TI_NFE.TI_NFE_NM_DESTINO
  is 'Nome do destinatario (xNome)';
comment on column TI_NFE.TI_NFE_NM_LOGRADOURO_DESTINO
  is 'Logradouro do destinatario (xLgr)';
comment on column TI_NFE.TI_NFE_NR_LOGRADOURO_DESTINO
  is 'Numero do endereco no logradouro do destinatario (nro)';
comment on column TI_NFE.TI_NFE_DS_CPL_DESTINO
  is 'Complemento do endereco do destnatario (xCpl)';
comment on column TI_NFE.TI_NFE_DS_BAIRRO_DESTINO
  is 'Bairro do destinatario (xBairro)';
comment on column TI_NFE.TI_NFE_NR_IBGE_MUN_DESTINO
  is 'Numero (na tabela do IBGE) do municipio do destinatario (cMun)';
comment on column TI_NFE.TI_NFE_DS_MUNICIPIO_DESTINO
  is 'Descricao do municipio do destinatario (xMun)';
comment on column TI_NFE.TI_NFE_DS_UF_DESTINO
  is 'Sigla da UF do destinatario (UF)';
comment on column TI_NFE.TI_NFE_NR_CEP_DESTINO
  is 'Numero do CEP do destinatario (CEP)';
comment on column TI_NFE.TI_NFE_NR_PAIS_DESTINO
  is 'Codigo do pais do destinatario (cPais)';
comment on column TI_NFE.TI_NFE_NR_TELEFONE_DESTINO
  is 'numero do telefone do destinatario (fone)';
comment on column TI_NFE.TI_NFE_NR_IE_DESTINO
  is 'Numero da inscricao estadual do destinatario (IE)';
comment on column TI_NFE.TI_NFE_NR_SUFRAMA_DESTINO
  is 'Inscricao na SUFRAMA do destinatario (ISUF)';
comment on column TI_NFE.TI_NFE_DS_EMAIL
  is 'Email do destinatario (nao vai para o XML)';
comment on column TI_NFE.TI_NFE_CD_CNAE
  is 'Codigo do CNAE fiscal do emitente (informar quando for informada a IM)';
comment on column TI_NFE.TI_NFE_DS_UF_EMBARQUE
  is 'Sigla da UF de embarque (apenas para exportacao) (UFEmbarq) ';
comment on column TI_NFE.TI_NFE_DS_LOCAL_EMBARQUE
  is 'Local de embarque (apenas para exportacao) (xLocEmbarq)';
comment on column TI_NFE.TI_NFE_DS_NF_EMPENHO
  is 'Nota de empenho (apenas para compras publicas) (xNEmp)';
comment on column TI_NFE.TI_NFE_DS_PEDIDO
  is 'Pedido de compra (xPed)';
comment on column TI_NFE.TI_NFE_DS_CONTRATO
  is 'Contrato de compra (xCont)';
comment on column TI_NFE.TI_NFLO_CD_LOTE
  is 'Codigo do lote da nota quando associada a um (NAO INFOMRAR)';
comment on column TI_NFE.TI_NFE_VL_TOTAL_II
  is 'Valor total do II (vII)';
comment on column TI_NFE.TI_NFE_DT_CANCELAMENTO
  is 'Data de canelamento (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_IN_NF_CANC
  is 'Indicador de cancelamento (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_IN_FIN_EM
  is 'Finalidade de emissao da NF-e (finNFe)';
comment on column TI_NFE.TI_NFE_IN_PROC_NF
  is 'Processo de emissao da NF-e (informar 0) ';
comment on column TI_NFE.TI_USUA_CD_USUARIO
  is 'Usuario (Informar o ID do usuario)';
comment on column TI_NFE.TI_NFE_DS_JUSTIF_CANC
  is 'Justificativa para o cancelamento (apenas quando for cancelar) (xJust)';
comment on column TI_NFE.TI_NFE_DT_EMIT_CONT
  is 'Data da emissao em contingencia (NAO INFOMRAR)';
comment on column TI_NFE.TI_USUA_CD_USU_CONT
  is 'Usuario da contingencia (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_DS_XML_CANCELAMENTO
  is 'XML de cancelamento (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_IN_NF_AUT
  is 'Indicador de nota autorizada (NAO INFOMRAR)';
comment on column TI_NFE.TI_NFE_DT_NF_AUT
  is 'Data de autorizacao da NF (NAO INFORMAR)';
comment on column TI_NFE.TI_USUA_CD_USU_CANCELAMENTO
  is 'ID do usuario que soliciotu o cancelamento';
comment on column TI_NFE.TI_NFE_DS_XML_PROT_ENV
  is 'XML do protocolo de envio (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_DS_NR_ALEATORIO
  is 'Número aleatório gerado (NãO INFORMAR)';
comment on column TI_NFE.TI_NFE_DS_MOTIVO_CONT
  is 'Motivo da contingencia (quando voluntaria)';
comment on column TI_NFE.TI_NFE_NR_ORDEM_EMBARQUE
  is 'Número da ordem de embarque';
comment on column TI_NFE.TI_NFE_NR_PROT_CANC
  is 'Numero do protocolo de cancelamento (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_SQ_ORDEM_EMBARQUE
  is 'Sequencial da ordem de embarque.';
comment on column TI_NFE.TI_NFE_SIS_OPERACAO
  is 'Operacao a ser realizada (''E''nvio - para envio com montagem automatica de lote; ''C''ancelamento ; ''N''enhuma - para montagem de lote manual)';
comment on column TI_NFE.TI_NFE_SIS_STATUS
  is 'Status da nota (NAO INFORMAR)';
comment on column TI_NFE.TI_NFE_NR_PROT_ENV
  is 'Numero do protocolo de envio';
comment on column TI_NFE.TI_NFE_NR_EMIT_CRT
  is 'Código do Regime Tributário';
comment on column TI_NFE.TI_NFE_DANFE_PDF
  is 'Pdf do DANFE';
comment on column TI_NFE.TI_NFE_IN_IMPRESSA_CONT
  is 'Indica se a nota foi impressa enquanto estava em contingência (papel moeda)';
comment on column TI_NFE.TI_NFE_NR_VERSAO
  is 'Versao do registro';
comment on column TI_NFE.TI_NFE_IN_USO
  is 'Indicador de em uso do registro';
comment on column TI_NFE.TI_NFE_TM_ULTIMA_ALTERACAO
  is 'Dara da ultima alteracao do registro';
comment on column TI_NFE.TINF_VAL_TOTAL_IMP
  is 'Valor total dos impostos da NF';
alter table TI_NFE
  add constraint PK_TI_NFE primary key (TI_NFE_CD_NF);
 
create index IX_TI_NFE on TI_NFE (TI_EMPR_CD_EMPRESA);
 
create index IX_TI_NFE_02 on TI_NFE (TI_USUA_CD_USU_CANCELAMENTO);
  
create index IX_TI_NFE_03 on TI_NFE (TI_USUA_CD_USUARIO);
 
create index IX_TI_NFE_04 on TI_NFE (TI_USUA_CD_USU_CONT);
  
create index IX_TI_NFE_06 on TI_NFE (TI_NFE_NR_CHAVE_ACESSO);
 
create index IX_TI_NFE_07 on TI_NFE (TI_EMPR_CD_EMPRESA,TI_NFE_DT_EMISSAO,TI_NFE_NR_DOCUMENTO_FISCAL,TI_NFE_NR_SERIE);
  
create index IX_TI_NFE_09 on TI_NFE (TI_NFE_IN_FORMA_EMISSAO);
  
create index IX_TI_NFE_10 on TI_NFE (TI_EMPR_CD_EMPRESA,TI_NFLO_CD_LOTE,TI_NFE_IN_FORMA_EMISSAO);
  
create index IX_TI_NFE_11 on TI_NFE (TI_NFE_SIS_OPERACAO);
  
create table TI_NFE_CANA
(
  TI_NFCA_CD                   NUMBER not null,
  TI_NFE_CD                    NUMBER not null,
  TI_NFCA_NR_SAFRA             VARCHAR2(9) not null,
  TI_NFCA_DT_SAFRA_REF         DATE,
  TI_NFCA_VL_QTDE_TOT_MES      NUMBER,
  TI_NFCA_VL_QTDE_TOT_ANTERIOR NUMBER,
  TI_NFCA_VL_QTDE_TOT_GERARADO NUMBER,
  TI_NFCA_VL_FORNECIMENTO      NUMBER,
  TI_NFCA_VL_TOT_DEDUCAO       NUMBER,
  TI_NFCA_VL_LIQ_FORNECIMENTO  NUMBER,
  TI_NFCA_NR_VERSAO            NUMBER default 1,
  TI_NFCA_IN_USO               NUMBER default 1,
  TI_NFCA_TM_ULTIMA_ALTERACAO  DATE default sysdate
);

comment on table TI_NFE_CANA
  is 'Tabela que armazena as Informações do Registro de Aquisição de Cana';
comment on column TI_NFE_CANA.TI_NFCA_CD
  is 'Chave primária gerada automaticamente e controlada pela sequence SQ_NFCA_CD';
comment on column TI_NFE_CANA.TI_NFE_CD
  is 'Código da NFE. FK tabela NFE';
comment on column TI_NFE_CANA.TI_NFCA_NR_SAFRA
  is 'Número da safra';
comment on column TI_NFE_CANA.TI_NFCA_DT_SAFRA_REF
  is 'Data referencia safra';
comment on column TI_NFE_CANA.TI_NFCA_VL_QTDE_TOT_MES
  is 'Quantidade total mes';
comment on column TI_NFE_CANA.TI_NFCA_VL_QTDE_TOT_ANTERIOR
  is 'Quantidade total anterior';
comment on column TI_NFE_CANA.TI_NFCA_VL_QTDE_TOT_GERARADO
  is 'Valor quantidade total gerado.';
comment on column TI_NFE_CANA.TI_NFCA_VL_FORNECIMENTO
  is 'Valor fornecimento';
comment on column TI_NFE_CANA.TI_NFCA_VL_TOT_DEDUCAO
  is 'Valor total de redução';
comment on column TI_NFE_CANA.TI_NFCA_VL_LIQ_FORNECIMENTO
  is 'Valor liquido fornecimento';
comment on column TI_NFE_CANA.TI_NFCA_NR_VERSAO
  is 'Versao do registro';
comment on column TI_NFE_CANA.TI_NFCA_IN_USO
  is 'Indicador de em uso do registro';
comment on column TI_NFE_CANA.TI_NFCA_TM_ULTIMA_ALTERACAO
  is 'Data da ultima alteracao do registro';
alter table TI_NFE_CANA
  add constraint PK_TI_NFCA_NFE primary key (TI_NFCA_CD);
  
alter table TI_NFE_CANA
  add constraint FK_TI_NFCA_NFE foreign key (TI_NFE_CD)
  references TI_NFE (TI_NFE_CD_NF);
create unique index IX_TI_NFE_CANA on TI_NFE_CANA (TI_NFE_CD);
  
create table TI_NFE_CANA_DEDUCAO
(
  TI_NFCD_CD                  NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFCD_DS_DEDUCAO          VARCHAR2(60),
  TI_NFCD_VL_DEDUCAO          NUMBER,
  TI_NFCD_NR_VERSAO           NUMBER default 1,
  TI_NFCD_IN_USO              NUMBER default 1,
  TI_NFCD_TM_ULTIMA_ALTERACAO DATE default sysdate
);

comment on table TI_NFE_CANA_DEDUCAO
  is 'Tabela com as informações do Grupo de Deduções Taxas e Contribuições';
comment on column TI_NFE_CANA_DEDUCAO.TI_NFCD_CD
  is 'Chave primária gerada automaticamente e controlada pela sequence SQ_NFCD_CD';
comment on column TI_NFE_CANA_DEDUCAO.TI_NFE_CD_NF
  is 'Código da NFE. FK tabela NFE';
comment on column TI_NFE_CANA_DEDUCAO.TI_NFCD_DS_DEDUCAO
  is 'Descrição dedução';
comment on column TI_NFE_CANA_DEDUCAO.TI_NFCD_VL_DEDUCAO
  is 'Valor dedução';
comment on column TI_NFE_CANA_DEDUCAO.TI_NFCD_NR_VERSAO
  is 'Versao do registro';
comment on column TI_NFE_CANA_DEDUCAO.TI_NFCD_IN_USO
  is 'Indicador de em uso do registro';
comment on column TI_NFE_CANA_DEDUCAO.TI_NFCD_TM_ULTIMA_ALTERACAO
  is 'Data da ultima alteracao do registro';
alter table TI_NFE_CANA_DEDUCAO
  add constraint PK_TI_NFCD_NFE primary key (TI_NFCD_CD);
  
alter table TI_NFE_CANA_DEDUCAO
  add constraint FK_TI_NFCD_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create unique index IX_TI_NFE_CANA_DEDUCAO on TI_NFE_CANA_DEDUCAO (TI_NFE_CD_NF);
  
create table TI_NFE_CANA_FORNEC_DIARIO
(
  TI_NCFD_CD                  NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NCFD_NR_DIA              NUMBER not null,
  TI_NCFD_VL_QTDE_DIA         NUMBER,
  TI_NCFD_NR_VERSAO           NUMBER default 1,
  TI_NCFD_IN_USO              NUMBER default 1,
  TI_NCFD_TM_ULTIMA_ALTERACAO DATE default sysdate
);

comment on table TI_NFE_CANA_FORNEC_DIARIO
  is 'Tabela com as informações do Grupo de Fornecimento diário de cana';
comment on column TI_NFE_CANA_FORNEC_DIARIO.TI_NCFD_CD
  is 'Chave primária gerada automaticamente e controlada pela sequence SQ_NCFD_CD';
comment on column TI_NFE_CANA_FORNEC_DIARIO.TI_NFE_CD_NF
  is 'Código da NFE. FK tabela NFE';
comment on column TI_NFE_CANA_FORNEC_DIARIO.TI_NCFD_NR_DIA
  is 'Número do dia';
comment on column TI_NFE_CANA_FORNEC_DIARIO.TI_NCFD_VL_QTDE_DIA
  is 'Valor quantidade dia';
comment on column TI_NFE_CANA_FORNEC_DIARIO.TI_NCFD_NR_VERSAO
  is 'Versao do registro';
comment on column TI_NFE_CANA_FORNEC_DIARIO.TI_NCFD_IN_USO
  is 'Indicador de em uso do registro';
comment on column TI_NFE_CANA_FORNEC_DIARIO.TI_NCFD_TM_ULTIMA_ALTERACAO
  is 'Data da ultima alteracao do registro';
alter table TI_NFE_CANA_FORNEC_DIARIO
  add constraint PK_TI_NCFD_NFE primary key (TI_NCFD_CD);
  
alter table TI_NFE_CANA_FORNEC_DIARIO
  add constraint FK_TI_NCFD_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create unique index IX_TI_NFE_CANA_FORNEC_DIARIO on TI_NFE_CANA_FORNEC_DIARIO (TI_NFE_CD_NF);
 
create table TI_NFE_CHAVE_REFERENCIADA
(
  TI_NFCH_CD                  NUMBER not null,
  TI_NFCH_CD_CHAVE_ACESSO_REF VARCHAR2(44),
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFCH_NR_IBGE_UF          VARCHAR2(2),
  TI_NFCH_DT_EMISSAO          DATE,
  TI_NFCH_NR_CNPJ             VARCHAR2(14),
  TI_NFCH_NR_MODELO           NUMBER,
  TI_NFCH_NR_SERIE            NUMBER,
  TI_NFCH_NR_DOCUMENTO        NUMBER,
  TI_NFCH_NR_VERSAO           NUMBER default 1,
  TI_NFCH_IN_USO              NUMBER default 1,
  TI_NFCH_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_CHAVE_REFERENCIADA
  is 'Contém os dados de notas referenciadas pela nota em questão (complemento, devolução, etc)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_CD
  is 'Sequencial';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_CD_CHAVE_ACESSO_REF
  is 'Chave de acesso da nota referenciada (quando eletrÃ´nica) (refNFe)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFE_CD_NF
  is 'Sequenial da nota a que pertence a referencia (nota que referencia as outras)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_NR_IBGE_UF
  is 'Numero (na tabela do IBGE) da UF (apenas para notas referenciadas não eletrÃ´nicas) (cUF)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_DT_EMISSAO
  is 'Data de emissão da nota referenciada (apenas para notas referenciadas não eletrÃ´nicas) (AAMM)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_NR_CNPJ
  is 'CNPJ do emitente (apenas para notas referenciadas não eletrÃ´nicas) (CNPJ) ';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_NR_MODELO
  is 'Modelo do documento fiscal da nota referenciada (apenas para notas referenciadas não eletrÃ´nicas) (informar 01) (mod)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_NR_SERIE
  is 'Série da nota referenciada (apenas para notas referenciadas não eletrÃ´nicas) (serie)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_NR_DOCUMENTO
  is 'Numero do documento fiscal da nota referenciada (apenas para notas referenciadas não eletrÃ´nicas) (nNF)';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_IN_USO
  is 'Indica se está habilitado';
comment on column TI_NFE_CHAVE_REFERENCIADA.TI_NFCH_TM_ULTIMA_ALTERACAO
  is 'Data da última alteração';
alter table TI_NFE_CHAVE_REFERENCIADA
  add constraint PK_TI_NFCH_NFE primary key (TI_NFCH_CD);
  
alter table TI_NFE_CHAVE_REFERENCIADA
  add constraint FK_TI_NFCH_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create index IX_TI_NFE_CHAVE_REFERENCIADA on TI_NFE_CHAVE_REFERENCIADA (TI_NFE_CD_NF);
  
create table TI_NFE_CUPOM_FISCAL_REF
(
  TI_NFCF_CD                  NUMBER not null,
  TI_NFE_CD                   NUMBER not null,
  TI_NFCF_NR_MODELO           VARCHAR2(2),
  TI_NFCF_NR_ECF              NUMBER,
  TI_NFCF_NR_CONTAD           NUMBER,
  TI_NFCF_NR_VERSAO           NUMBER default 1,
  TI_NFCF_IN_USO              NUMBER default 1,
  TI_NFCF_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_CUPOM_FISCAL_REF
  is 'Tabela com as informações do cupom fiscal referenciado';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFCF_CD
  is 'Id da tabela de cupom fiscal referenciado';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFE_CD
  is 'Código NFE.';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFCF_NR_MODELO
  is 'Código do modelo fiscal';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFCF_NR_ECF
  is 'Número de ordem sequencial do ECF';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFCF_NR_CONTAD
  is 'Número do Contador de Ordem de Operação - COO';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFCF_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFCF_IN_USO
  is 'Indica se a empresa está ativa';
comment on column TI_NFE_CUPOM_FISCAL_REF.TI_NFCF_TM_ULTIMA_ALTERACAO
  is 'Data da última alteração';
alter table TI_NFE_CUPOM_FISCAL_REF
  add constraint PK_TI_NFCF_NFE primary key (TI_NFCF_CD);
  
alter table TI_NFE_CUPOM_FISCAL_REF
  add constraint FK_TI_NFCF_NFE foreign key (TI_NFE_CD)
  references TI_NFE (TI_NFE_CD_NF);
create index IX_TI_NFE_CUPFI_REF on TI_NFE_CUPOM_FISCAL_REF (TI_NFE_CD);
  
create table TI_NFE_ITEM
(
  TI_NFE_CD_NF                  NUMBER not null,
  TI_NFIT_CD_ITEM               NUMBER not null,
  TI_NFIT_NR_ITEM               NUMBER not null,
  TI_NFIT_CD_PRODUTO            VARCHAR2(60) not null,
  TI_NFIT_CD_EAN                VARCHAR2(14),
  TI_NFIT_DS_PRODUTO            VARCHAR2(120) not null,
  TI_NFIT_CD_NCM                VARCHAR2(8),
  TI_NFIT_CD_EX_TIPI            VARCHAR2(3),
  TI_NFIT_CD_GENERO             NUMBER,
  TI_NFIT_CD_CFOP               NUMBER not null,
  TI_NFIT_DS_UNID_TRIB          VARCHAR2(6) not null,
  TI_NFIT_DS_UNID_COM           VARCHAR2(6) not null,
  TI_NFIT_VL_QTDE_TRIB          NUMBER not null,
  TI_NFIT_VL_QTDE_COM           NUMBER not null,
  TI_NFIT_VL_TOTAL_BRUTO        NUMBER not null,
  TI_NFIT_VL_TOTAL_FRETE        NUMBER,
  TI_NFIT_VL_TOTAL_SEGURO       NUMBER,
  TI_NFIT_VL_DESCONTO           NUMBER,
  TI_NFIT_IN_TRIB_ICMS          VARCHAR2(2) not null,
  TI_NFIT_IN_ORIG_ICMS          NUMBER not null,
  TI_NFIT_IN_MOD_BC_ICMS        NUMBER,
  TI_NFIT_VL_BC_ICMS            NUMBER,
  TI_NFIT_VL_ALIQ_ICMS          NUMBER,
  TI_NFIT_VL_ICMS               NUMBER,
  TI_NFIT_IN_MOD_BC_ST_ICMS     NUMBER,
  TI_NFIT_VL_PERC_MARG_ICMS     NUMBER,
  TI_NFIT_VL_PERC_REDUC_ICMS    NUMBER,
  TI_NFIT_VL_BC_ST_ICMS         NUMBER,
  TI_NFIT_VL_ALIQ_ST_ICMS       NUMBER,
  TI_NFIT_VL_ICMS_ST            NUMBER,
  TI_NFIT_VL_PERC_REDUC_ICMS_ST NUMBER,
  TI_NFIT_CD_CLASSE_IPI         VARCHAR2(5),
  TI_NFIT_NR_CNPJ_IPI           VARCHAR2(14),
  TI_NFIT_CD_SELO_IPI           VARCHAR2(10),
  TI_NFIT_NR_SELO_IPI           NUMBER,
  TI_NFIT_CD_ENQ_IPI            VARCHAR2(3),
  TI_NFIT_IN_ST_TRIB_IPI        VARCHAR2(2),
  TI_NFIT_VL_BC_IPI             NUMBER,
  TI_NFIT_NR_UND_PRD_IPI        NUMBER,
  TI_NFIT_VL_UNID_TRIB_IPI      NUMBER,
  TI_NFIT_VL_ALIQ_IPI           NUMBER,
  TI_NFIT_VL_IPI                NUMBER,
  TI_NFIT_IN_ST_TRIB_PIS        VARCHAR2(2) not null,
  TI_NFIT_VL_BC_PIS             NUMBER,
  TI_NFIT_VL_PERC_ALIQ_PIS      NUMBER,
  TI_NFIT_VL_PIS                NUMBER,
  TI_NFIT_VL_QTDE_VEND_PIS      NUMBER,
  TI_NFIT_VL_ALIQ_PIS           NUMBER,
  TI_NFIT_IN_ST_TRIB_CF         VARCHAR2(2) not null,
  TI_NFIT_VL_BC_CF              NUMBER,
  TI_NFIT_PR_ALIQ_CF            NUMBER,
  TI_NFIT_VL_CF                 NUMBER,
  TI_NFIT_VL_ALIQ_CF            NUMBER,
  TI_NFIT_VL_QTDE_VEND_CF       NUMBER,
  TI_NFIT_VL_BC_ISSQN           NUMBER,
  TI_NFIT_VL_ALIQ_ISSQN         NUMBER,
  TI_NFIT_VL_ISSQN              NUMBER,
  TI_NFIT_NR_IBGE_MUN_ISSQN     NUMBER,
  TI_NFIT_DS_INFO               VARCHAR2(500),
  TI_NFIT_VL_UNIT               NUMBER,
  TI_NFIT_VL_UNIT_TRIBUTAVEL    NUMBER,
  TI_NFIT_CD_LISTA_SERVICOS     VARCHAR2(4),
  TI_NFIT_VL_BC_IMPOSTO_IMPORT  NUMBER,
  TI_NFIT_VL_DESP_ADUANEIRAS    NUMBER,
  TI_NFIT_VL_IMPOSTO_IMPORT     NUMBER,
  TI_NFIT_VL_IOF                NUMBER,
  TI_NFIT_CD_EAN_TRIB           VARCHAR2(14),
  TI_NFIT_VL_BC_PIS_ST          NUMBER,
  TI_NFIT_VL_PALIQ_PIS_ST       NUMBER,
  TI_NFIT_VL_QTD_PIS_ST         NUMBER,
  TI_NFIT_VL_PIS_ST             NUMBER,
  TI_NFIT_VL_BC_CF_ST           NUMBER,
  TI_NFIT_VL_PALIQ_CF_ST        NUMBER,
  TI_NFIT_VL_QTD_CF_ST          NUMBER,
  TI_NFIT_VL_ALIQ_CF_ST         NUMBER,
  TI_NFIT_VL_CF_ST              NUMBER,
  TI_NFIT_VL_RALIQ_PIS_ST       NUMBER,
  TI_NFIT_VL_OUTROS             NUMBER default 0 not null,
  TI_NFIT_NR_INDICADOR_TOTAL    NUMBER default 0 not null,
  TI_NFIT_NR_PED                VARCHAR2(60),
  TI_NFIT_NR_ITEM_PED           NUMBER,
  TI_NFIT_NR_VERSAO             NUMBER default 1,
  TI_NFIT_IN_USO                NUMBER default 1,
  TI_NFIT_TM_ULTIMA_ALT         TIMESTAMP(6) default sysdate,
  TI_NFIT_VAL_TOTAL_IMP         NUMBER
);

comment on table TI_NFE_ITEM
  is 'Contém os dados dos itens da nota';
comment on column TI_NFE_ITEM.TI_NFE_CD_NF
  is 'Id da nota ';
comment on column TI_NFE_ITEM.TI_NFIT_CD_ITEM
  is 'Id item da nota';
comment on column TI_NFE_ITEM.TI_NFIT_NR_ITEM
  is 'Numero do item na nota fiscal (nItem)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_PRODUTO
  is 'Código do produto ou serviço(cProd)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_EAN
  is 'GTIN (Global Trade Item Number) do produto, antigo código EAN ou código de barras)(cEAN)';
comment on column TI_NFE_ITEM.TI_NFIT_DS_PRODUTO
  is 'Descrição do produto ou serviço(xProd)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_NCM
  is 'Código NCM(NCM)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_EX_TIPI
  is 'EX_TIPI(EXTIPI)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_GENERO
  is 'Gênero do Produto ou Serviço(genero)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_CFOP
  is 'CFOP(CFOP)';
comment on column TI_NFE_ITEM.TI_NFIT_DS_UNID_TRIB
  is 'Unidade Tributável(unTrib)';
comment on column TI_NFE_ITEM.TI_NFIT_DS_UNID_COM
  is 'Unidade Comercial(unCom)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_QTDE_TRIB
  is 'Quantidade Tributável(qTrib)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_QTDE_COM
  is 'Quantidade Comercial(qCom)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_TOTAL_BRUTO
  is 'Valor Total Bruto';
comment on column TI_NFE_ITEM.TI_NFIT_VL_TOTAL_FRETE
  is 'Valor Total do Frete(vFrete)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_TOTAL_SEGURO
  is 'Valor Total do Seguro(vSeg)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_DESCONTO
  is 'Valor do Desconto(vDesc)';
comment on column TI_NFE_ITEM.TI_NFIT_IN_TRIB_ICMS
  is 'Tributação do ICMS(CST)';
comment on column TI_NFE_ITEM.TI_NFIT_IN_ORIG_ICMS
  is 'Origem da mercadoria(orig)';
comment on column TI_NFE_ITEM.TI_NFIT_IN_MOD_BC_ICMS
  is 'Modalidade de determinação da BC do ICMS(modBC)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_ICMS
  is 'Valor da BC do ICMS(vBC)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ALIQ_ICMS
  is 'Alíquota do imposto(pICMS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ICMS
  is 'Valor do ICMS(vICMS)';
comment on column TI_NFE_ITEM.TI_NFIT_IN_MOD_BC_ST_ICMS
  is 'Modalidade de determinação da BC do ICMS ST(modBCST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PERC_MARG_ICMS
  is 'Percentual da margem de valor do ICMS ST(pMVAST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PERC_REDUC_ICMS
  is 'Percentual da Redução de BC doICMS(pRedICMS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_ST_ICMS
  is 'Valor da BC do ICMS ST(vBCST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ALIQ_ST_ICMS
  is 'Alíquota do imposto do ICMS ST(pICMSST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ICMS_ST
  is 'Valor do ICMS ST(vICMSST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PERC_REDUC_ICMS_ST
  is 'Percentual da Redução de BC doICMS ST(pRedICMSST)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_CLASSE_IPI
  is 'Classe de enquadramento do IPIpara Cigarros e Bebidas(clEnq)';
comment on column TI_NFE_ITEM.TI_NFIT_NR_CNPJ_IPI
  is 'CNPJ(CNPJ)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_SELO_IPI
  is 'Código do selo de controle IPI(cSelo)';
comment on column TI_NFE_ITEM.TI_NFIT_NR_SELO_IPI
  is 'Quantidade de selo de controle(qSelo)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_ENQ_IPI
  is 'Código de Enquadramento Legal do IPI(cEnq)';
comment on column TI_NFE_ITEM.TI_NFIT_IN_ST_TRIB_IPI
  is 'Código da situação tributária doIPI(CST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_IPI
  is 'Valor da BC do IPI(vBC)';
comment on column TI_NFE_ITEM.TI_NFIT_NR_UND_PRD_IPI
  is 'Quantidade total na unidade padrão para tributação (somente para os produtos tributados por unidade)(qUnid)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_UNID_TRIB_IPI
  is 'Valor por Unidade Tributável(vUnid)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ALIQ_IPI
  is 'Alíquota do IPI(pIPI)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_IPI
  is 'Valor do IPI(vIPI)';
comment on column TI_NFE_ITEM.TI_NFIT_IN_ST_TRIB_PIS
  is 'Código de Situação Tributária doPIS(CST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_PIS
  is 'Valor BC PIS(vBC)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PERC_ALIQ_PIS
  is 'Alíquota do PIS (em percentual)(pPIS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PIS
  is 'Valor PIS(vPIS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_QTDE_VEND_PIS
  is 'Quantidade Vendida(qBCProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ALIQ_PIS
  is 'Alíquota do PIS (em reais)(vAliqProd)';
comment on column TI_NFE_ITEM.TI_NFIT_IN_ST_TRIB_CF
  is 'Código de Situação Tributária doCOFINS(CST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_CF
  is 'Valor da Base de Cálculo da COFINS(vBC)';
comment on column TI_NFE_ITEM.TI_NFIT_PR_ALIQ_CF
  is 'Alíquota da COFINS (em percentual)(pCOFINS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_CF
  is 'Valor do COFINS(vCOFINS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ALIQ_CF
  is 'Alíquota do COFINS (em reais)(vAliqProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_QTDE_VEND_CF
  is 'Quantidade Vendida(qBCProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_ISSQN
  is 'Valor da Base de Cálculo do ISSQN(CST)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ALIQ_ISSQN
  is 'Alíquota do ISSQN(vAliq)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ISSQN
  is 'Valor do ISSQN(vISSQN)';
comment on column TI_NFE_ITEM.TI_NFIT_NR_IBGE_MUN_ISSQN
  is 'Código do município de ocorrência do fato gerador do ISSQN(cMunFG)';
comment on column TI_NFE_ITEM.TI_NFIT_DS_INFO
  is 'Informações Adicionais do produto(infAdProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_UNIT
  is 'Valor Unitário de comercialização(vUnCom)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_UNIT_TRIBUTAVEL
  is 'Valor Unitário de tributação(vUnTrib)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_LISTA_SERVICOS
  is 'Item da lista de serviços (cListServ)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_IMPOSTO_IMPORT
  is 'Valor da base de cálculo do II (vBC)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_DESP_ADUANEIRAS
  is 'Valor das despesas aduaneitas (vDespAdu)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_IMPOSTO_IMPORT
  is 'Valor do imposto de importação (vII)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_IOF
  is 'Valor do IOF (vIOF)';
comment on column TI_NFE_ITEM.TI_NFIT_CD_EAN_TRIB
  is 'GTIN (Global Trade Item Number) da unidade tributável, antigo código EAN ou código de barras (cEANTrib)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_PIS_ST
  is 'Valor da base de cálculo do pis de Substituição tributária (vBC)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PALIQ_PIS_ST
  is 'Alíquota do PIS de substituição tributária (pPIS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_QTD_PIS_ST
  is 'Quantidade vendida do PIS substituto (qBCProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PIS_ST
  is 'Valor do PIS substituto (vPIS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_BC_CF_ST
  is 'Valor da base de cálculo do COFINS substituto (vBC)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_PALIQ_CF_ST
  is 'Percentual de alíquota do COFINS substituto (pCOFINS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_QTD_CF_ST
  is 'Quantidde vendida do COFINS substituto (qBCProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_ALIQ_CF_ST
  is 'Valor da alíquota (em reais) do COFINS substituto (vAliqProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_CF_ST
  is 'Valor do COFINS Substitiuto (vCOFINS)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_RALIQ_PIS_ST
  is 'Valor (em Reais) da aliquota do PIS de substituição tributária (vAliqProd)';
comment on column TI_NFE_ITEM.TI_NFIT_VL_OUTROS
  is 'Valor outras dispesas (ICMSTot.vNF)';
comment on column TI_NFE_ITEM.TI_NFIT_NR_INDICADOR_TOTAL
  is 'Indica se o item compÃµe o valor total da nota';
comment on column TI_NFE_ITEM.TI_NFIT_NR_PED
  is 'Pedido de compra (xPed)';
comment on column TI_NFE_ITEM.TI_NFIT_NR_ITEM_PED
  is 'Numero do item do Pedido de compra (nItemPed)';
comment on column TI_NFE_ITEM.TI_NFIT_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_ITEM.TI_NFIT_IN_USO
  is 'Indica habilitado ';
comment on column TI_NFE_ITEM.TI_NFIT_TM_ULTIMA_ALT
  is 'Data última alteração';
comment on column TI_NFE_ITEM.TI_NFIT_VAL_TOTAL_IMP
  is 'Valor total dos impostos do item';
alter table TI_NFE_ITEM
  add constraint PK_TI_NFIT_NFE primary key (TI_NFIT_CD_ITEM);
 
alter table TI_NFE_ITEM
  add constraint FK_TI_NFIT_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create index FK_TI_NFIT_NFE on TI_NFE_ITEM (TI_NFE_CD_NF);
  
create index IX_TI_NFE_ITEM_01 on TI_NFE_ITEM (TI_NFIT_NR_ITEM);
  
create table TI_NFE_ITEM_DI
(
  TI_NIDI_CD                  NUMBER not null,
  TI_NFIT_CD_ITEM             NUMBER not null,
  TI_NIDI_NR_DOCUMENTO        VARCHAR2(10) not null,
  TI_NIDI_DT_REGISTRO         DATE not null,
  TI_NIDI_DS_LOCAL            VARCHAR2(60) not null,
  TI_NIDI_DS_UF               VARCHAR2(2) not null,
  TI_NIDI_DT_SAIDA            DATE not null,
  TI_NIDI_CD_EXPORTADOR       VARCHAR2(60) not null,
  TI_NIDI_NR_VERSAO           NUMBER default 1,
  TI_NIDI_IN_USO              NUMBER default 1,
  TI_NIDI_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_ITEM_DI
  is 'Tabela com as informações da declaração de importação';
comment on column TI_NFE_ITEM_DI.TI_NIDI_CD
  is 'Id da tabela de declaração de importação';
comment on column TI_NFE_ITEM_DI.TI_NFIT_CD_ITEM
  is 'Id da tabela de item de nota fiscal eletronica';
comment on column TI_NFE_ITEM_DI.TI_NIDI_NR_DOCUMENTO
  is 'Número do documento de importação DI/DSI/DA';
comment on column TI_NFE_ITEM_DI.TI_NIDI_DT_REGISTRO
  is 'Data de Registro da DI/DSI/DA';
comment on column TI_NFE_ITEM_DI.TI_NIDI_DS_LOCAL
  is 'Descrição local';
comment on column TI_NFE_ITEM_DI.TI_NIDI_DS_UF
  is 'CNPJ do emitente';
comment on column TI_NFE_ITEM_DI.TI_NIDI_DT_SAIDA
  is 'CPF do emitente';
comment on column TI_NFE_ITEM_DI.TI_NIDI_CD_EXPORTADOR
  is 'IE do emitente';
comment on column TI_NFE_ITEM_DI.TI_NIDI_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_ITEM_DI.TI_NIDI_IN_USO
  is 'Indica se a empresa está ativa';
comment on column TI_NFE_ITEM_DI.TI_NIDI_TM_ULTIMA_ALTERACAO
  is 'Data da última alteração';
alter table TI_NFE_ITEM_DI
  add constraint PK_TI_NIDI_NFE primary key (TI_NIDI_CD);
  
alter table TI_NFE_ITEM_DI
  add constraint FK_TI_NIDI_NFIT_NFE foreign key (TI_NFIT_CD_ITEM)
  references TI_NFE_ITEM (TI_NFIT_CD_ITEM);
create index IX_TI_NFE_ITEM_DI on TI_NFE_ITEM_DI (TI_NFIT_CD_ITEM);
  
create table TI_NFE_DI_ADICAO
(
  TI_NDIA_CD                  NUMBER not null,
  TI_NIDI_CD                  NUMBER not null,
  TI_NDIA_NR_DOCUMENTO        VARCHAR2(10) not null,
  TI_NDIA_NR_SEQ              NUMBER not null,
  TI_NDIA_CD_FABRICANT        VARCHAR2(60) not null,
  TI_NDIA_VL_DESCONTO         NUMBER,
  TI_NDIA_NR_VERSAO           NUMBER default 1,
  TI_NDIA_IN_USO              NUMBER default 1,
  TI_NDIA_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_DI_ADICAO
  is 'Tabela com as informações de adição de declaração de importação';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_CD
  is 'Id da tabela de adição de declaração de importação';
comment on column TI_NFE_DI_ADICAO.TI_NIDI_CD
  is 'Id da tabela de declaração de importação';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_NR_DOCUMENTO
  is 'Numero do document';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_NR_SEQ
  is 'Numero';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_CD_FABRICANT
  is 'Codigo do fabricante';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_VL_DESCONTO
  is 'Valor Desconto';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_IN_USO
  is 'Indica se a empresa está ativa';
comment on column TI_NFE_DI_ADICAO.TI_NDIA_TM_ULTIMA_ALTERACAO
  is 'Data da última alteração';
alter table TI_NFE_DI_ADICAO
  add constraint PK_TI_NDIA_NFE primary key (TI_NDIA_CD);
  
alter table TI_NFE_DI_ADICAO
  add constraint FK_TI_NDIA_NIDI_NFE foreign key (TI_NIDI_CD)
  references TI_NFE_ITEM_DI (TI_NIDI_CD);
create index IX_TI_NFE_DINFE_DIADI on TI_NFE_DI_ADICAO (TI_NIDI_CD);
  
create table TI_NFE_DUPLICATA
(
  TI_NFDU_CD                  NUMBER not null,
  TI_NFDU_NR_DUPLICATA        VARCHAR2(60) not null,
  TI_NFDU_DT_VENCIMENTO       DATE,
  TI_NFDU_VL_DUPLICATA        NUMBER,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFDU_NR_VERSAO           NUMBER default 1,
  TI_NFDU_IN_USO              NUMBER default 1,
  TI_NFDU_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_DUPLICATA
  is 'Contém dados das duplicatas da nota';
comment on column TI_NFE_DUPLICATA.TI_NFDU_CD
  is 'Sequencial';
comment on column TI_NFE_DUPLICATA.TI_NFDU_NR_DUPLICATA
  is 'Numero da duplicata (nDup)';
comment on column TI_NFE_DUPLICATA.TI_NFDU_DT_VENCIMENTO
  is 'Data do vencimento (dVenc)';
comment on column TI_NFE_DUPLICATA.TI_NFDU_VL_DUPLICATA
  is 'Valor da duplicata (vDup)';
comment on column TI_NFE_DUPLICATA.TI_NFE_CD_NF
  is 'Nota a que a duplicata pertence';
comment on column TI_NFE_DUPLICATA.TI_NFDU_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_DUPLICATA.TI_NFDU_IN_USO
  is 'Indica habilitado';
comment on column TI_NFE_DUPLICATA.TI_NFDU_TM_ULTIMA_ALTERACAO
  is 'Data última alteração';
alter table TI_NFE_DUPLICATA
  add constraint PK_TI_NFDU_NFE primary key (TI_NFDU_CD);
  
alter table TI_NFE_DUPLICATA
  add constraint FK_TI_NFDU_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create index IX_TI_NFE_DUPLICATA on TI_NFE_DUPLICATA (TI_NFE_CD_NF);
  
create table TI_NFE_EMPRESA
(
  TI_EMPR_CD_EMPRESA          NUMBER not null,
  TI_EMPR_CD_EMPRESA_SI       VARCHAR2(8) not null,
  TI_EMPR_NR_VERSAO           NUMBER default 1 not null,
  TI_EMPR_IN_USO              NUMBER default 1 not null,
  TI_EMPR_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default SYSDATE not null
);

comment on table TI_NFE_EMPRESA
  is 'Tabela de mapeamento das empresas';
comment on column TI_NFE_EMPRESA.TI_EMPR_CD_EMPRESA
  is 'Chave da empresa Portal';
comment on column TI_NFE_EMPRESA.TI_EMPR_CD_EMPRESA_SI
  is 'Chave de empresa SI';
comment on column TI_NFE_EMPRESA.TI_EMPR_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_EMPRESA.TI_EMPR_IN_USO
  is 'Indica se a empresa está ativa';
comment on column TI_NFE_EMPRESA.TI_EMPR_TM_ULTIMA_ALTERACAO
  is 'Data da última alterao';
alter table TI_NFE_EMPRESA
  add constraint PK_TI_EMPR_NFE primary key (TI_EMPR_CD_EMPRESA);
  
create table TI_NFE_EVENTO_LOTE
(
  TI_NELT_CD_LOTE             NUMBER not null,
  TI_EMPR_CD_EMPRESA          NUMBER not null,
  TI_NELT_DT_EMISSAO          DATE default SYSDATE not null,
  TI_NELT_DS_XML_ENVIO        BLOB,
  TI_NELT_DS_XML_RETORNO      BLOB,
  TI_NELT_DT_ENVIO            DATE,
  TI_NELT_DT_RETORNO          DATE,
  TI_NELT_CD_RETORNO          NUMBER,
  TI_NELT_DS_RETORNO          VARCHAR2(2000),
  TI_NELT_SIS_OPERACAO        VARCHAR2(2) default 'CC' not null,
  TI_NELT_SIS_STATUS          VARCHAR2(3) default 'NP' not null,
  TI_NELT_SIS_DT_STATUS       DATE default SYSDATE not null,
  TI_NELT_NR_VERSAO           NUMBER default 1,
  TI_NELT_IN_USO              NUMBER default 1,
  TI_NELT_TM_ULTIMA_ALTERACAO DATE default SYSDATE not null
);

comment on table TI_NFE_EVENTO_LOTE
  is 'Tabela para armazenar as informações de Lote dos Eventos.';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_CD_LOTE
  is 'Sequencial da tabela. Sequence: SQ_NELT_CD_LOTE';
comment on column TI_NFE_EVENTO_LOTE.TI_EMPR_CD_EMPRESA
  is 'Codigo da empresa.';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_DT_EMISSAO
  is 'Data de emissão';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_DS_XML_ENVIO
  is 'Xml de envio';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_DS_XML_RETORNO
  is 'Xml de retorno.';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_DT_ENVIO
  is 'Data de envio';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_DT_RETORNO
  is 'Data de retorno';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_CD_RETORNO
  is 'Código do retorno';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_DS_RETORNO
  is 'Descrição do retorno';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_SIS_OPERACAO
  is 'Operação do sistema';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_SIS_STATUS
  is 'Status do sistema';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_SIS_DT_STATUS
  is 'Data do status do sistema.';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_IN_USO
  is 'Indica hablitado';
comment on column TI_NFE_EVENTO_LOTE.TI_NELT_TM_ULTIMA_ALTERACAO
  is 'Data última alteração';
alter table TI_NFE_EVENTO_LOTE
  add constraint PK_TI_NFE_EVENTO_LOTE primary key (TI_NELT_CD_LOTE);
  
create table TI_NFE_EVENTO_CARTA_CORRECAO
(
  TI_NECC_CD                  NUMBER not null,
  TI_EMPR_CD_EMPRESA          NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NECC_DS_CHAVE_ACESSO     VARCHAR2(44) not null,
  TI_NECC_NR_SEQ_EVENTO       NUMBER default 1 not null,
  TI_NECC_DS_STATUS_EVENTO    VARCHAR2(3) default 'NP' not null,
  TI_NECC_NR_VERSAO_EVENTO    NUMBER default 1 not null,
  TI_NECC_DS_ID_EVENTO        VARCHAR2(54) not null,
  TI_NECC_DS_DESC_EVENTO      VARCHAR2(60) not null,
  TI_NECC_DS_DESC_CORRECAO    VARCHAR2(1000) not null,
  TI_NECC_DS_COND_USO         VARCHAR2(2000) not null,
  TI_NECC_DS_TIPO_EVENTO      VARCHAR2(6) not null,
  TI_NECC_DS_ORGAO            VARCHAR2(2) not null,
  TI_NECC_TM_DAT_EVENTO       DATE default SYSDATE not null,
  TI_NELT_CD_LOTE             NUMBER,
  TI_NECC_NR_VERSAO           NUMBER default 1,
  TI_NECC_IN_USO              NUMBER default 1,
  TI_NECC_TM_ULTIMA_ALTERACAO DATE default SYSDATE
);

comment on table TI_NFE_EVENTO_CARTA_CORRECAO
  is 'Tabela para armazenar o Grupo de informações do registro do Evento.';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_CD
  is 'Sequencial da tabela. Sequence: SQ_NECC_CD';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_EMPR_CD_EMPRESA
  is 'Codigo da empresa';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NFE_CD_NF
  is 'Nota a que pertence';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_CHAVE_ACESSO
  is 'Chave de acesso';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_NR_SEQ_EVENTO
  is 'SeqÃ¼encial do evento para o mesmo tipo de evento';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_STATUS_EVENTO
  is 'Status do evento';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_NR_VERSAO_EVENTO
  is 'Versão do Evento';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_ID_EVENTO
  is 'Identificador da TAG a ser assinada';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_DESC_EVENTO
  is 'Descrição do Evento';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_DESC_CORRECAO
  is 'Descrição da Correção';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_COND_USO
  is 'Descrição da Condição de Uso';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_TIPO_EVENTO
  is 'Tipo do Evento';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_DS_ORGAO
  is 'Código do órgão de recepção do Evento';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_TM_DAT_EVENTO
  is 'Data do Evento';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NELT_CD_LOTE
  is 'Codigo do lote';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_IN_USO
  is 'Indica hablitado';
comment on column TI_NFE_EVENTO_CARTA_CORRECAO.TI_NECC_TM_ULTIMA_ALTERACAO
  is 'Data última alteração';
alter table TI_NFE_EVENTO_CARTA_CORRECAO
  add constraint PK_TI_NFE_EVE_CARTA_CORRECAO primary key (TI_NECC_CD);
  
alter table TI_NFE_EVENTO_CARTA_CORRECAO
  add constraint FK_TI_NELT_NFEEVENTO foreign key (TI_NELT_CD_LOTE)
  references TI_NFE_EVENTO_LOTE (TI_NELT_CD_LOTE);
alter table TI_NFE_EVENTO_CARTA_CORRECAO
  add constraint FK_TI_NFE_NFEEVENTO foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create index FK_TI_EMPR_NFEEVENTO on TI_NFE_EVENTO_CARTA_CORRECAO (TI_EMPR_CD_EMPRESA);
  
create index FK_TI_NELT_NFEEVENTO on TI_NFE_EVENTO_CARTA_CORRECAO (TI_NELT_CD_LOTE);
  
create index FK_TI_NFE_NFEEVENTO on TI_NFE_EVENTO_CARTA_CORRECAO (TI_NFE_CD_NF);
  
create table TI_NFE_FATURA
(
  TI_NFFA_CD                  NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFFA_NR_FATURA           VARCHAR2(60) not null,
  TI_NFFA_VL_ORIGINAL         NUMBER,
  TI_NFFA_VL_DESCONTO         NUMBER,
  TI_NFFA_VL_LIQUIDO          NUMBER,
  TI_NFFA_NR_VERSAO           NUMBER default 1,
  TI_NFFA_IN_USO              NUMBER default 1,
  TI_NFFA_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_FATURA
  is 'Contém os dados das faturas da nota';
comment on column TI_NFE_FATURA.TI_NFFA_CD
  is 'Sequencial';
comment on column TI_NFE_FATURA.TI_NFE_CD_NF
  is 'Nota a que a fatura pertence';
comment on column TI_NFE_FATURA.TI_NFFA_NR_FATURA
  is 'Numero da fatura (nFat)';
comment on column TI_NFE_FATURA.TI_NFFA_VL_ORIGINAL
  is 'Valor original (vOrig)';
comment on column TI_NFE_FATURA.TI_NFFA_VL_DESCONTO
  is 'Valor do desconto (vDesc)';
comment on column TI_NFE_FATURA.TI_NFFA_VL_LIQUIDO
  is 'Valor liquido (vLiq)';
comment on column TI_NFE_FATURA.TI_NFFA_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_FATURA.TI_NFFA_IN_USO
  is 'Indica habiltiado';
comment on column TI_NFE_FATURA.TI_NFFA_TM_ULTIMA_ALTERACAO
  is 'Data última alteração';
alter table TI_NFE_FATURA
  add constraint PK_TI_NFE_FATURA primary key (TI_NFFA_CD);
  

create table TI_NFE_IMP_XML
(
  TI_NIXM_CD                  NUMBER not null,
  TI_EMPR_CD_EMPRESA          NUMBER not null,
  TI_NIXM_DS_CHAVE_ACESSO     VARCHAR2(44),
  TI_NIXM_DS_NOME_ARQUIVO     VARCHAR2(255),
  TI_NIXM_NR_TAMANHO_ARQUIVO  NUMBER,
  TI_NIXM_DS_ARQUIVO_XML      BLOB,
  TI_NIXM_NR_VERSAO           NUMBER default 1,
  TI_NIXM_IN_USO              NUMBER default 1,
  TI_NIXM_TM_ULTIMA_ALTERACAO DATE default SYSDATE,
  TI_NIXM_SIS_STATUS          VARCHAR2(3) default 'IMP' not null
);

comment on table TI_NFE_IMP_XML
  is 'Tabela de transição, piramide/portal, para envio do xml';
comment on column TI_NFE_IMP_XML.TI_EMPR_CD_EMPRESA
  is 'Codigo da empresa';
comment on column TI_NFE_IMP_XML.TI_NIXM_DS_CHAVE_ACESSO
  is 'Chave de acesso';
comment on column TI_NFE_IMP_XML.TI_NIXM_DS_NOME_ARQUIVO
  is 'Nome do arquivo';
comment on column TI_NFE_IMP_XML.TI_NIXM_NR_TAMANHO_ARQUIVO
  is 'Tamanho do arquivo';
comment on column TI_NFE_IMP_XML.TI_NIXM_DS_ARQUIVO_XML
  is 'Arquivo binário';
comment on column TI_NFE_IMP_XML.TI_NIXM_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_IMP_XML.TI_NIXM_IN_USO
  is 'Indica se a empresa está ativa';
comment on column TI_NFE_IMP_XML.TI_NIXM_TM_ULTIMA_ALTERACAO
  is 'Data da última alteração';
alter table TI_NFE_IMP_XML
  add constraint PK_TI_NFE_IMP_XML primary key (TI_NIXM_CD);
  
create index FK_TI_EMPR_NFEIMPXML on TI_NFE_IMP_XML (TI_EMPR_CD_EMPRESA);
  
create index IX_TI_NFE_IMP_XML on TI_NFE_IMP_XML (TI_NIXM_DS_CHAVE_ACESSO);
  
create table TI_NFE_INUTILIZACAO
(
  TI_EMPR_CD_EMPRESA          NUMBER not null,
  TI_NFIN_CD_INUT             NUMBER not null,
  TI_USUA_CD_USUARIO          NUMBER not null,
  TI_NFIN_DT_INUT             DATE,
  TI_NFIN_NR_PROT             VARCHAR2(15),
  TI_NFIN_NR_SERIE            NUMBER not null,
  TI_NFIN_NR_INI              NUMBER not null,
  TI_NFIN_NR_FIM              NUMBER not null,
  TI_NFIN_DS_JUSTIF           VARCHAR2(255) not null,
  TI_NFIN_NR_MSG              NUMBER,
  TI_NFIN_MM_DS_XML_PROT      BLOB,
  TI_NFIN_NR_VERSAO           NUMBER default 1,
  TI_NFIN_IN_USO              NUMBER default 1,
  TI_NFIN_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate,
  TI_NFIN_CD_NF_PIR           NUMBER,
  TI_NFIN_SIS_STATUS          VARCHAR2(2),
  TI_NFIN_DS_ERRO             VARCHAR2(300)
);

comment on table TI_NFE_INUTILIZACAO
  is 'Contém os dados referentes a inutilização de numeração (mantida pelo sistema)';
comment on column TI_NFE_INUTILIZACAO.TI_EMPR_CD_EMPRESA
  is 'Empresa a que pertence a inutilização';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_CD_INUT
  is 'Sequencial';
comment on column TI_NFE_INUTILIZACAO.TI_USUA_CD_USUARIO
  is 'Usuário que realizou a inutilização';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_DT_INUT
  is 'Data da inutilização';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_NR_PROT
  is 'Protocolo da inutilização';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_NR_SERIE
  is 'Série';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_NR_INI
  is 'Numero de documento fiscal inicial';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_NR_FIM
  is 'Numero de documento fiscal final';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_DS_JUSTIF
  is 'Justificativa da inutilização';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_NR_MSG
  is 'Numero da mensagem retornada';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_MM_DS_XML_PROT
  is 'XML do protocolo';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_IN_USO
  is 'Indica hablitado';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_TM_ULTIMA_ALTERACAO
  is 'Data ãÂºltima alteração';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_CD_NF_PIR
  is 'Código Sequencial.';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_SIS_STATUS
  is 'Status da inutilização.';
comment on column TI_NFE_INUTILIZACAO.TI_NFIN_DS_ERRO
  is 'Descrição do erro';
alter table TI_NFE_INUTILIZACAO
  add constraint PK_TI_NFIN_NFE primary key (TI_NFIN_CD_INUT);
  
create index IX_TI_NFE_INUTILIZACAO on TI_NFE_INUTILIZACAO (TI_EMPR_CD_EMPRESA);
  
create index IX_TI_NFE_INUTILIZACAO_01 on TI_NFE_INUTILIZACAO (TI_USUA_CD_USUARIO);
  
create table TI_NFE_ITEM_COMB
(
  TI_NFIC_CD                  NUMBER not null,
  TI_NFIT_CD_ITEM             NUMBER not null,
  TI_NFIC_CD_PROD_ANP         VARCHAR2(9),
  TI_NFIC_CD_CODIF            VARCHAR2(21),
  TI_NFIC_QT_COMB_TEMP        NUMBER,
  TI_NFIC_VL_BC_CIDE          NUMBER,
  TI_NFIC_VL_ALIQ_CIDE        NUMBER,
  TI_NFIC_VL_CIDE             NUMBER,
  TI_NFIC_VL_BC_ICMS          NUMBER not null,
  TI_NFIC_VL_ICMS             NUMBER not null,
  TI_NFIC_VL_BC_ICMS_ST       NUMBER not null,
  TI_NFIC_VL_ICMS_ST          NUMBER not null,
  TI_NFIC_VL_BC_ICMS_INT      NUMBER,
  TI_NFIC_VL_ICMS_INT         NUMBER,
  TI_NFIC_VL_BC_ICMS_CON      NUMBER,
  TI_NFIC_VL_ICMS_CON         NUMBER,
  TI_NFIC_DS_UF_CON           VARCHAR2(2),
  TI_NFIC_NR_VERSAO           NUMBER default 1,
  TI_NFIC_IN_USO              NUMBER default 1,
  TI_NFIC_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_ITEM_COMB
  is 'Contém os dados específicos de combustíveis do item da nota';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_CD
  is 'Sequencial';
comment on column TI_NFE_ITEM_COMB.TI_NFIT_CD_ITEM
  is 'Sequencial do item a que pertence';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_CD_PROD_ANP
  is 'Código do produto na ANP (cProdANP)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_CD_CODIF
  is 'Código da autorização/registro do CODIF (CODIF)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_QT_COMB_TEMP
  is 'Quantidade do combustível Ã temperatura ambeinte (qTemp)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_BC_CIDE
  is 'Quantidade da base de cálculo do CIDE (qBCProd)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_ALIQ_CIDE
  is 'Valor da alíquota do CIDE (vAliqProd)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_CIDE
  is 'Valor do CIDE (vCIDE)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_BC_ICMS
  is 'Valor da base de calculo do ICMS da operação própria (vBCICMS)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_ICMS
  is 'Valor do ICMS da operação própria (vICMS)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_BC_ICMS_ST
  is 'Valor da base de calculo do ICMS substituto da operação própria (vBCICMSST)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_ICMS_ST
  is 'Valor do ICMS substituto da operação própria (vICMSST)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_BC_ICMS_INT
  is 'Valor da base de cálculo ICMS ST da UF de destino(vBCICMSSTDest)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_ICMS_INT
  is 'Valor do ICMS ST da UF de destino (vICMSSTDest)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_BC_ICMS_CON
  is 'Valor da base de cálculo ICMS ST da UF de consumo (vBCICMSSTCons)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_VL_ICMS_CON
  is 'Valor do ICMS ST da UF de consumo (vICMSSTCons)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_DS_UF_CON
  is 'Sigla da UF de consumo (UFCons)';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_IN_USO
  is 'Indica hablitado';
comment on column TI_NFE_ITEM_COMB.TI_NFIC_TM_ULTIMA_ALTERACAO
  is 'Data última alteração';
alter table TI_NFE_ITEM_COMB
  add constraint PK_TI_NFIC_NFE primary key (TI_NFIC_CD);
  
alter table TI_NFE_ITEM_COMB
  add constraint FK_TI_NFIC_NFIT_NFE foreign key (TI_NFIT_CD_ITEM)
  references TI_NFE_ITEM (TI_NFIT_CD_ITEM);
create index IX_TI_NFE_ITEM_COMB on TI_NFE_ITEM_COMB (TI_NFIT_CD_ITEM);
  
create table TI_NFE_ITEM_MEDICAMENTO
(
  TI_NFIM_CD                  NUMBER not null,
  TI_NFIT_CD_ITEM             NUMBER not null,
  TI_NFIM_NR_LOTE             VARCHAR2(20) not null,
  TI_NFIM_DT_VALIDADE         DATE not null,
  TI_NFIM_VL_PRECO_MAX        NUMBER not null,
  TI_NFIM_DT_FABR             DATE not null,
  TI_NFIM_VL_QTD_LOTE         NUMBER not null,
  TI_NFIM_NR_VERSAO           NUMBER default 1,
  TI_NFIM_IN_USO              NUMBER default 1,
  TI_NFIM_TM_ULTIMA_ALTERACAO DATE default sysdate
);

comment on table TI_NFE_ITEM_MEDICAMENTO
  is 'Tabela com as informações de Medicamento e de matérias-primas farmacêuticas';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_CD
  is 'Chave primária gerada automaticamente e controlada pela sequence SQ_NFIM_CD';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIT_CD_ITEM
  is 'Código do Item NFE. FK tabela NFE_ITEM';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_NR_LOTE
  is 'Numero lote';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_DT_VALIDADE
  is 'Data de validade';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_VL_PRECO_MAX
  is 'Valor preço maximo';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_DT_FABR
  is 'Data fabricação';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_VL_QTD_LOTE
  is 'Quantidade lote';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_NR_VERSAO
  is 'Versao do registro';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_IN_USO
  is 'Indicador de em uso do registro';
comment on column TI_NFE_ITEM_MEDICAMENTO.TI_NFIM_TM_ULTIMA_ALTERACAO
  is 'Data da ultima alteracao do registro';
alter table TI_NFE_ITEM_MEDICAMENTO
  add constraint PK_TI_NFIM_NFE primary key (TI_NFIM_CD);
  
alter table TI_NFE_ITEM_MEDICAMENTO
  add constraint FK_TI_NFIM_NFIT_NFE foreign key (TI_NFIT_CD_ITEM)
  references TI_NFE_ITEM (TI_NFIT_CD_ITEM);
create index IX_TI_NFE_ITEM_MEDICAMENTO on TI_NFE_ITEM_MEDICAMENTO (TI_NFIT_CD_ITEM);
  
create table TI_NFE_TRANSPORTE
(
  TI_NFTR_CD                  NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFTR_IN_FRETE            NUMBER not null,
  TI_NFTR_NR_CNPJ_CPF         VARCHAR2(14),
  TI_NFTR_NM_TRANSP           VARCHAR2(60),
  TI_NFTR_NR_IE               VARCHAR2(14),
  TI_NFTR_NM_LOGR             VARCHAR2(60),
  TI_NFTR_DS_MUN              VARCHAR2(60),
  TI_NFTR_DS_UF               VARCHAR2(2),
  TI_NFTR_VL_SERV             NUMBER,
  TI_NFTR_VL_BC_RET_ICMS      NUMBER,
  TI_NFTR_VL_ALIQ_RET         NUMBER,
  TI_NFTR_VL_RET_ICMS         NUMBER,
  TI_NFTR_CD_CFOP             NUMBER,
  TI_NFTR_NR_IBGE_MUN_FG      VARCHAR2(7),
  TI_NFTR_DS_PLACA            VARCHAR2(8),
  TI_NFTR_DS_UF_PLACA         VARCHAR2(2),
  TI_NFTR_NR_RNTC             VARCHAR2(20),
  TI_NFTR_NR_VERSAO           NUMBER default 1,
  TI_NFTR_IN_USO              NUMBER default 1,
  TI_NFTR_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_TRANSPORTE
  is 'Armazena os dados referentes a transporte';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_CD
  is 'Sequencial';
comment on column TI_NFE_TRANSPORTE.TI_NFE_CD_NF
  is 'Nota a que pertence';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_IN_FRETE
  is 'Modalidade do frete(modFrete)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_NR_CNPJ_CPF
  is 'CNPJ ou CPF( CNPJ ou CPF)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_NM_TRANSP
  is 'Razão Social ou nome(xNome)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_NR_IE
  is 'Inscrição Estadual(IE)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_NM_LOGR
  is 'Endereço Completo(xEnder)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_DS_MUN
  is 'Nome do município(xMun)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_DS_UF
  is 'Sigla da UF(UF)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_VL_SERV
  is 'Valor do Serviço(vServ)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_VL_BC_RET_ICMS
  is 'BC da Retenção do ICMS(vBCRet)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_VL_ALIQ_RET
  is 'Alíquota da Retenção(pICMSRet)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_VL_RET_ICMS
  is 'Valor do ICMS Retido(vICMSRet)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_CD_CFOP
  is 'CFOP(CFOP)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_NR_IBGE_MUN_FG
  is 'Código do município de ocorrência do fato gerador do ICMS do transporte(cMunFG)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_DS_PLACA
  is 'Placa do Veículo(placa)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_DS_UF_PLACA
  is 'Sigla da UF(UF)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_NR_RNTC
  is 'Registro Nacional de Transportador de Carga (ANTT)(RNTC)';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_IN_USO
  is 'Indica hablitado';
comment on column TI_NFE_TRANSPORTE.TI_NFTR_TM_ULTIMA_ALTERACAO
  is 'Data última alteração';
alter table TI_NFE_TRANSPORTE
  add constraint PK_TI_NFTR_NFE primary key (TI_NFTR_CD);
  
alter table TI_NFE_TRANSPORTE
  add constraint FK_TI_NFTR_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create index IX_TI_NFE_TRANSPORTE on TI_NFE_TRANSPORTE (TI_NFE_CD_NF);
  
create table TI_NFE_VOLUME
(
  TI_NFVO_CD                  NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFTR_CD                  NUMBER not null,
  TI_NFVO_VL_QTDE             NUMBER,
  TI_NFVO_DS_ESPECIE          VARCHAR2(60),
  TI_NFVO_DS_MARCA            VARCHAR2(60),
  TI_NFVO_NR_IDENT            VARCHAR2(60),
  TI_NFVO_VL_PESO_LIQ         NUMBER,
  TI_NFVO_VL_PESO_BRUTO       NUMBER,
  TI_NFVO_NR_VERSAO           NUMBER default 1,
  TI_NFVO_IN_USO              NUMBER default 1,
  TI_NFVO_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_VOLUME
  is 'Contém os dados referentes aos volumes das notas';
comment on column TI_NFE_VOLUME.TI_NFVO_CD
  is 'Código do volume.';
comment on column TI_NFE_VOLUME.TI_NFE_CD_NF
  is 'Código da NFe.';
comment on column TI_NFE_VOLUME.TI_NFTR_CD
  is 'Código do tranporte da nfe.';
comment on column TI_NFE_VOLUME.TI_NFVO_VL_QTDE
  is 'Quantidade de volumes transportados(qVol)';
comment on column TI_NFE_VOLUME.TI_NFVO_DS_ESPECIE
  is 'Espécie dos volumes transportados(esp)';
comment on column TI_NFE_VOLUME.TI_NFVO_DS_MARCA
  is 'Marca dos volumestransportados(marca)';
comment on column TI_NFE_VOLUME.TI_NFVO_NR_IDENT
  is 'Numeração dos volumes transportados(nvol)';
comment on column TI_NFE_VOLUME.TI_NFVO_VL_PESO_LIQ
  is 'Peso liquido total(pesoL)';
comment on column TI_NFE_VOLUME.TI_NFVO_VL_PESO_BRUTO
  is 'Peso bruto total(pesoB)';
comment on column TI_NFE_VOLUME.TI_NFVO_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_VOLUME.TI_NFVO_IN_USO
  is 'Indica hablitado';
comment on column TI_NFE_VOLUME.TI_NFVO_TM_ULTIMA_ALTERACAO
  is 'Data da última alteração';
alter table TI_NFE_VOLUME
  add constraint PK_TI_NFVO_NFE primary key (TI_NFVO_CD);
  
alter table TI_NFE_VOLUME
  add constraint FK_TI_NFVO_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
alter table TI_NFE_VOLUME
  add constraint FK_TI_NFVO_NFTR_NFE foreign key (TI_NFTR_CD)
  references TI_NFE_TRANSPORTE (TI_NFTR_CD);
create index IX_TI_NFE_VOLUME on TI_NFE_VOLUME (TI_NFE_CD_NF);
  
create index IX_TI_NFE_VOLUME_01 on TI_NFE_VOLUME (TI_NFTR_CD);
  
create table TI_NFE_LACRE
(
  TI_NFLC_CD                  NUMBER not null,
  TI_NFVO_CD                  NUMBER not null,
  TI_NFLC_NM_LACRE            VARCHAR2(60) not null,
  TI_NFLC_NR_VERSAO           NUMBER default 1,
  TI_NFLC_IN_USO              NUMBER default 1,
  TI_NFLC_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_LACRE
  is 'Armazena o histórico dos lacres para o volume';
comment on column TI_NFE_LACRE.TI_NFLC_CD
  is 'Chave primária';
comment on column TI_NFE_LACRE.TI_NFVO_CD
  is 'Código primário da tabela NFE_VOLUME';
comment on column TI_NFE_LACRE.TI_NFLC_NM_LACRE
  is 'Numero do lacre';
alter table TI_NFE_LACRE
  add constraint PK_TI_NFE_LACRE primary key (TI_NFLC_CD);
  
alter table TI_NFE_LACRE
  add constraint FK_TI_NFLC_NFVO foreign key (TI_NFVO_CD)
  references TI_NFE_VOLUME (TI_NFVO_CD);
create index IX_TI_NFE_LACRE on TI_NFE_LACRE (TI_NFVO_CD);
  
create table TI_NFE_LOCAL
(
  TI_NFLC_CD                  NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFLC_IN_RETIRADA_ENTREGA NUMBER not null,
  TI_NFLC_NR_CNPJ             VARCHAR2(14) not null,
  TI_NFLC_NM_LOGRADOURO       VARCHAR2(60) not null,
  TI_NFLC_NR_LOGRADOURO       VARCHAR2(60) not null,
  TI_NFLC_DS_COMPL            VARCHAR2(60),
  TI_NFLC_DS_BAIRRO           VARCHAR2(60) not null,
  TI_NFLC_NR_IBGE_MUN         VARCHAR2(7) not null,
  TI_NFLC_DS_MUN              VARCHAR2(60) not null,
  TI_NFLC_DS_UF               VARCHAR2(2) not null,
  TI_NFLC_NR_VERSAO           NUMBER default 1,
  TI_NFLC_IN_USO              NUMBER default 1,
  TI_NFLC_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_LOCAL
  is 'Contém dados de endereço (retirada/entrega) da nota';
comment on column TI_NFE_LOCAL.TI_NFLC_CD
  is 'Sequencial';
comment on column TI_NFE_LOCAL.TI_NFE_CD_NF
  is 'Nota a que pertence';
comment on column TI_NFE_LOCAL.TI_NFLC_IN_RETIRADA_ENTREGA
  is 'Indicador de retirada ou entrega';
comment on column TI_NFE_LOCAL.TI_NFLC_NR_CNPJ
  is 'CNPJ (CNPJ)';
comment on column TI_NFE_LOCAL.TI_NFLC_NM_LOGRADOURO
  is 'Nome do logradouro (xLgr)';
comment on column TI_NFE_LOCAL.TI_NFLC_NR_LOGRADOURO
  is 'Numero do endereço no logradouro (nro)';
comment on column TI_NFE_LOCAL.TI_NFLC_DS_COMPL
  is 'Complemento do endereço (xCpl)';
comment on column TI_NFE_LOCAL.TI_NFLC_DS_BAIRRO
  is 'Bairro (xBairro)';
comment on column TI_NFE_LOCAL.TI_NFLC_NR_IBGE_MUN
  is 'Numeor do município na tabela do IBGE (cMun)';
comment on column TI_NFE_LOCAL.TI_NFLC_DS_MUN
  is 'Descrição do município (xMun)';
comment on column TI_NFE_LOCAL.TI_NFLC_DS_UF
  is 'Sigla da UF (UF)';
alter table TI_NFE_LOCAL
  add constraint PK_TI_NFLC_NFE primary key (TI_NFLC_CD);
  
alter table TI_NFE_LOCAL
  add constraint FK_TI_NFLC_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create index IX_TI_NFE_LOCAL on TI_NFE_LOCAL (TI_NFE_CD_NF);
  
create table TI_NFE_LOG
(
  TI_NFLG_CD_LOG              NUMBER not null,
  TI_NFE_CD_NF                NUMBER not null,
  TI_NFLG_DT_LOG              DATE not null,
  TI_NFLG_DS_LOG              VARCHAR2(4000) not null,
  TI_NFLG_NR_VERSAO           NUMBER default 1,
  TI_NFLG_IN_USO              NUMBER default 1,
  TI_NFLG_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_LOG
  is 'Armazena o histórico das notas (mantida pelo sistema)';
comment on column TI_NFE_LOG.TI_NFLG_CD_LOG
  is 'Data de última alteração';
comment on column TI_NFE_LOG.TI_NFE_CD_NF
  is 'Data de última alteração';
comment on column TI_NFE_LOG.TI_NFLG_DT_LOG
  is 'Data do log';
comment on column TI_NFE_LOG.TI_NFLG_DS_LOG
  is 'Descrição do log';
comment on column TI_NFE_LOG.TI_NFLG_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_LOG.TI_NFLG_IN_USO
  is 'Indica se está ativa';
comment on column TI_NFE_LOG.TI_NFLG_TM_ULTIMA_ALTERACAO
  is 'Data de última alteração';
alter table TI_NFE_LOG
  add constraint PK_TI_NFLG_NFE primary key (TI_NFLG_CD_LOG);
  
alter table TI_NFE_LOG
  add constraint FK_TI_NFLG_NFE foreign key (TI_NFE_CD_NF)
  references TI_NFE (TI_NFE_CD_NF);
create index IX_TI_NFE_LOG on TI_NFE_LOG (TI_NFE_CD_NF);
 
  
create table TI_NFE_LOG_CARTA_CORRECAO
(
  TI_NLCC_CD_LOG              NUMBER not null,
  TI_NECC_CD                  NUMBER not null,
  TI_NECC_DT_LOG              DATE not null,
  TI_NECC_DS_LOG              VARCHAR2(4000) not null,
  TI_NECC_NR_VERSAO           NUMBER default 1,
  TI_NECC_IN_USO              NUMBER default 1,
  TI_NECC_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_LOG_CARTA_CORRECAO
  is 'Armazena o histórico das cartas de correção (mantida pelo sistema)';
comment on column TI_NFE_LOG_CARTA_CORRECAO.TI_NLCC_CD_LOG
  is 'Sequencial da tabela. Sequence: SQ_NLCC_CD_LOG';
comment on column TI_NFE_LOG_CARTA_CORRECAO.TI_NECC_CD
  is 'Carta de correção referente';
comment on column TI_NFE_LOG_CARTA_CORRECAO.TI_NECC_DT_LOG
  is 'Data do log';
comment on column TI_NFE_LOG_CARTA_CORRECAO.TI_NECC_DS_LOG
  is 'Descrição do log';
comment on column TI_NFE_LOG_CARTA_CORRECAO.TI_NECC_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_LOG_CARTA_CORRECAO.TI_NECC_IN_USO
  is 'Indica se está ativa';
comment on column TI_NFE_LOG_CARTA_CORRECAO.TI_NECC_TM_ULTIMA_ALTERACAO
  is 'Data de última alteração';
alter table TI_NFE_LOG_CARTA_CORRECAO
  add constraint PK_TI_NFE_LOG_CARTA_CORRECAO primary key (TI_NLCC_CD_LOG);
  
alter table TI_NFE_LOG_CARTA_CORRECAO
  add constraint FK_TI_NFEEVENTO_NFLOGCC foreign key (TI_NECC_CD)
  references TI_NFE_EVENTO_CARTA_CORRECAO (TI_NECC_CD);
create index FK_TI_NFEEVENTO_NFLOGCC on TI_NFE_LOG_CARTA_CORRECAO (TI_NECC_CD);
  
create table TI_NFE_LOTE
(
  TI_NFLO_CD_LOTE             NUMBER not null,
  TI_NFLO_DT_CRIACAO          DATE not null,
  TI_NFLO_IN_STATUS           VARCHAR2(2),
  TI_NFLO_DT_RETORNO          DATE,
  TI_NFLO_MM_XML_ENVIO        BLOB,
  TI_NFLO_MM_XML_RETORNO      BLOB,
  TI_NFLO_CD_RETORNO          NUMBER,
  TI_NFLO_DS_RETORNO          VARCHAR2(2000),
  TI_NFLO_NR_RECIBO           VARCHAR2(15),
  TI_NFLO_DT_TEMPO_MEDIO      DATE,
  TI_NFLO_DT_ENVIO            DATE,
  TI_EMPR_CD_EMPRESA          NUMBER,
  TI_NFLO_IN_AMB_SIS          NUMBER,
  TI_NFLO_NR_TENTATIVAS       NUMBER default 0 not null,
  TI_NFLO_NR_TAM              NUMBER,
  TI_NFLO_IN_CONT             NUMBER default 0 not null,
  TI_NFLO_NR_VERSAO           NUMBER default 1,
  TI_NFLO_IN_USO              NUMBER default 1,
  TI_NFLO_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate,
  TI_NFLO_IN_TIPO_ENVIO       VARCHAR2(2) default 'N' not null,
  TI_NFLO_MM_XML_REC_DPEC     BLOB,
  TI_NFLO_MM_XML_RET_DPEC     BLOB,
  TI_NFLO_NR_REGISTRO_DPEC    NUMBER,
  TI_NFLO_NR_STATUS_DPEC      NUMBER,
  TI_NFLO_DS_MOTIVO_DPEC      VARCHAR2(256),
  TI_NFLO_NR_CHAVE_ERRO_DPEC  VARCHAR2(44),
  TI_NFLO_DS_DG_VAL           VARCHAR2(100),
  TI_NFLO_DT_AUT_DPEC         DATE
);

comment on table TI_NFE_LOTE
  is 'Armazena os lotes de notas (mantida pelo sistema)';
comment on column TI_NFE_LOTE.TI_NFLO_CD_LOTE
  is 'Id do lote';
comment on column TI_NFE_LOTE.TI_NFLO_DT_CRIACAO
  is 'Data de criação';
comment on column TI_NFE_LOTE.TI_NFLO_IN_STATUS
  is 'Status do lote';
comment on column TI_NFE_LOTE.TI_NFLO_DT_RETORNO
  is 'Data do retorno';
comment on column TI_NFE_LOTE.TI_NFLO_MM_XML_ENVIO
  is 'XML do envio';
comment on column TI_NFE_LOTE.TI_NFLO_MM_XML_RETORNO
  is 'XML do retorno';
comment on column TI_NFE_LOTE.TI_NFLO_CD_RETORNO
  is 'Código do retorno';
comment on column TI_NFE_LOTE.TI_NFLO_DS_RETORNO
  is 'Descrição do retorno';
comment on column TI_NFE_LOTE.TI_NFLO_NR_RECIBO
  is 'Número do recibo';
comment on column TI_NFE_LOTE.TI_NFLO_DT_TEMPO_MEDIO
  is 'Tempo médio';
comment on column TI_NFE_LOTE.TI_NFLO_DT_ENVIO
  is 'Data de envio';
comment on column TI_NFE_LOTE.TI_EMPR_CD_EMPRESA
  is 'Id empresa';
comment on column TI_NFE_LOTE.TI_NFLO_IN_AMB_SIS
  is 'Ambiente do sistema';
comment on column TI_NFE_LOTE.TI_NFLO_NR_TENTATIVAS
  is 'Número de tentaivas de envio';
comment on column TI_NFE_LOTE.TI_NFLO_NR_TAM
  is 'Tamanho';
comment on column TI_NFE_LOTE.TI_NFLO_IN_CONT
  is 'Indica se o lote esta em contigencia';
comment on column TI_NFE_LOTE.TI_NFLO_IN_TIPO_ENVIO
  is 'Este campo informa se o lote vai ser enviado, Normal, SCAN ou DPEC';
comment on column TI_NFE_LOTE.TI_NFLO_MM_XML_REC_DPEC
  is 'XMl de recebimento DPEC';
comment on column TI_NFE_LOTE.TI_NFLO_MM_XML_RET_DPEC
  is 'Xml de retorno DEPEC';
comment on column TI_NFE_LOTE.TI_NFLO_NR_REGISTRO_DPEC
  is 'Numero de registro DPEC';
comment on column TI_NFE_LOTE.TI_NFLO_NR_STATUS_DPEC
  is 'Numero do status DPEC';
comment on column TI_NFE_LOTE.TI_NFLO_DS_MOTIVO_DPEC
  is 'Descrição do motivo DPEC';
comment on column TI_NFE_LOTE.TI_NFLO_NR_CHAVE_ERRO_DPEC
  is 'Número da chave erro DPEC';
comment on column TI_NFE_LOTE.TI_NFLO_DS_DG_VAL
  is 'Guarda a informarção do digVal do XML de retorno';
alter table TI_NFE_LOTE
  add constraint PK_TI_NFLO_NFE primary key (TI_NFLO_CD_LOTE);
  
create index IX_TI_NFE_LOTE on TI_NFE_LOTE (TI_EMPR_CD_EMPRESA);
  
create index IX_TI_NFE_LOTE_01 on TI_NFE_LOTE (TI_NFLO_CD_LOTE,TI_NFLO_DT_TEMPO_MEDIO);
  
create index IX_TI_NFLO_02 on TI_NFE_LOTE (TI_NFLO_IN_STATUS);
  
create table TI_NFE_MENSAGEM
(
  TI_NFME_CD_MSG              NUMBER not null,
  TI_NFME_DS_MSG              VARCHAR2(100),
  TI_NFME_IN_TIPO             NUMBER not null,
  TI_NFME_IN_ACAO             NUMBER,
  TI_NFME_NR_VERSAO           NUMBER default 1,
  TI_NFME_IN_USO              NUMBER default 1,
  TI_NFME_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_MENSAGEM
  is 'Armazena as mensagens da SEFAZ (mantida por script)';
comment on column TI_NFE_MENSAGEM.TI_NFME_CD_MSG
  is 'Código da mensagem';
comment on column TI_NFE_MENSAGEM.TI_NFME_DS_MSG
  is 'Descrição da mensagem';
comment on column TI_NFE_MENSAGEM.TI_NFME_IN_TIPO
  is 'Tipo da mensagem';
comment on column TI_NFE_MENSAGEM.TI_NFME_IN_ACAO
  is 'Ação';
comment on column TI_NFE_MENSAGEM.TI_NFME_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_MENSAGEM.TI_NFME_IN_USO
  is 'Indica se está ativa';
comment on column TI_NFE_MENSAGEM.TI_NFME_TM_ULTIMA_ALTERACAO
  is 'Data de última alteração';
alter table TI_NFE_MENSAGEM
  add constraint PK_TI_NFME_NFE primary key (TI_NFME_CD_MSG);
  
create table TI_NFE_PRODUTOR_RURAL_REFER
(
  TI_NFPR_CD                  NUMBER not null,
  TI_NFE_CD                   NUMBER not null,
  TI_NFPR_NR_IBGE_UF          NUMBER,
  TI_NFPR_DT_EMISSAO          DATE,
  TI_NFPR_NR_CNPJ             VARCHAR2(14),
  TI_NFPR_NR_CPF              VARCHAR2(11),
  TI_NFPR_NR_IE               VARCHAR2(14),
  TI_NFPR_NR_MODELO           VARCHAR2(2),
  TI_NFPR_NR_SERIE            NUMBER(3),
  TI_NFPR_NR_DOCUMENTO        NUMBER,
  TI_NFPR_NR_CHAVE_ACESSO_CTE NUMBER,
  TI_NFPR_NR_VERSAO           NUMBER default 1,
  TI_NFPR_IN_USO              NUMBER default 1,
  TI_NFPR_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate
);

comment on table TI_NFE_PRODUTOR_RURAL_REFER
  is 'Tabela com as informações do produtor rural referenciado';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_CD
  is 'Id da tabela de produtor rural referenciado';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFE_CD
  is 'Número da NFE';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_IBGE_UF
  is 'Código da UF do emitente do Documento Fiscal';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_DT_EMISSAO
  is 'Data de emissão da NFe';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_CNPJ
  is 'CNPJ do emitente';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_CPF
  is 'CPF do emitente';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_IE
  is 'IE do emitente';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_MODELO
  is 'Código do modelo do documento fiscal';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_SERIE
  is 'Código da Série do Documento Fiscal';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_DOCUMENTO
  is 'Número do Documento Fiscal';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_CHAVE_ACESSO_CTE
  is 'Chave de acesso do CT-e referenciada';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_NR_VERSAO
  is 'Versão';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_IN_USO
  is 'Indica se a empresa está ativa';
comment on column TI_NFE_PRODUTOR_RURAL_REFER.TI_NFPR_TM_ULTIMA_ALTERACAO
  is 'Data da última alteração';
alter table TI_NFE_PRODUTOR_RURAL_REFER
  add constraint PK_TI_NFPR_NFE primary key (TI_NFPR_CD);
  
alter table TI_NFE_PRODUTOR_RURAL_REFER
  add constraint FK_TI_NFPR_NFE foreign key (TI_NFE_CD)
  references TI_NFE (TI_NFE_CD_NF);
create unique index IX_TI_NFE_PRODUTOR_RURAL_REFER on TI_NFE_PRODUTOR_RURAL_REFER (TI_NFE_CD);
  
create table TI_NFE_UNIDADE_FEDERATIVA
(
  TI_UNFE_CD       NUMBER not null,
  TI_UNFE_SIGLA    VARCHAR2(2) not null,
  TI_UNFE_NOME     VARCHAR2(30) not null,
  TI_UNFE_NUM_IBGE NUMBER not null
);

comment on table TI_NFE_UNIDADE_FEDERATIVA
  is 'Tabela que armazena as unidades federativas';
comment on column TI_NFE_UNIDADE_FEDERATIVA.TI_UNFE_CD
  is 'Código da UF.';
comment on column TI_NFE_UNIDADE_FEDERATIVA.TI_UNFE_SIGLA
  is 'Sigla da UF.';
comment on column TI_NFE_UNIDADE_FEDERATIVA.TI_UNFE_NOME
  is 'Nome da UF';
comment on column TI_NFE_UNIDADE_FEDERATIVA.TI_UNFE_NUM_IBGE
  is 'Código da unidade no IBGE';
alter table TI_NFE_UNIDADE_FEDERATIVA
  add constraint PK_TI_UNFE_NFE primary key (TI_UNFE_CD);
  
create index IX_TI_NFE_UNIDADE_FEDERATIVA on TI_NFE_UNIDADE_FEDERATIVA (TI_UNFE_SIGLA);
  
create table TI_NFE_SERVICOS_URL
(
  TI_NFSE_CD                  NUMBER not null,
  TI_NFSE_DS_URL              VARCHAR2(500) not null,
  TI_NFSE_CD_UF               NUMBER,
  TI_NFSE_DS_TIPO             VARCHAR2(1) not null,
  TI_NFSE_DS_OPER_SERV        VARCHAR2(10) not null,
  TI_NFSE_IN_USO              NUMBER default 1,
  TI_NFSE_NR_VERSAO           NUMBER default 1,
  TI_NFSE_TM_ULTIMA_ALTERACAO TIMESTAMP(6) default sysdate,
  TI_NFSE_DS_VERSAO           VARCHAR2(10) default '2.00' not null
);

comment on table TI_NFE_SERVICOS_URL
  is 'Informações das URLs dos web services.';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_CD
  is 'Identificador da tabela';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_DS_URL
  is 'Endereço da URL';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_CD_UF
  is 'Sigla da UF';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_DS_TIPO
  is 'Indica se a URL é de Homologação (H) ou Produção (P).';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_DS_OPER_SERV
  is 'Indica se a URL é de Envio (EN), Cancelamento (CA)...';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_IN_USO
  is 'Indica se a URL está habilitada (1) ou desabilitada (0).';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_NR_VERSAO
  is 'Indica a versão da URL.';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_TM_ULTIMA_ALTERACAO
  is 'Indica a data da última alteração.';
comment on column TI_NFE_SERVICOS_URL.TI_NFSE_DS_VERSAO
  is 'Descrição da versão';
alter table TI_NFE_SERVICOS_URL
  add constraint PK_TI_NFSU_NFE primary key (TI_NFSE_CD);
  
alter table TI_NFE_SERVICOS_URL
  add constraint FK_TI_NFSU_UNFE_NFE foreign key (TI_NFSE_CD_UF)
  references TI_NFE_UNIDADE_FEDERATIVA (TI_UNFE_CD);
create index IX_TI_NFE_SERVICOS_URL on TI_NFE_SERVICOS_URL (TI_NFSE_CD_UF);
  

commit;