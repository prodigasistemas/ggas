UPDATE MENU SET MENU_DS_URL = 'exibirExportacaoDispositivosMoveis' WHERE MENU_DS_URL LIKE 'exibirExportacaoDispositivosMoveis.do?acao=exibirExportacaoDispositivosMoveis%tela=exportacaoDispositivosMoveis';
UPDATE MENU SET MENU_DS_URL = 'exibirImportarLeituraDispositivosMoveis' WHERE MENU_DS_URL LIKE 'exibirImportarLeituraDispositivosMoveis.do?acao=exibirImportarLeituraDispositivosMoveis%tela=importarLeituraDispositivosMoveis';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'importarLeituraDispositivosMoveis' WHERE RESI_DS LIKE 'importarLeituraDispositivosMoveis.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarProcesso' WHERE RESI_DS LIKE 'pesquisarProcesso.do';

COMMIT;