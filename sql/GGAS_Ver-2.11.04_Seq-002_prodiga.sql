INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'PARAMETRO_VALIDA_CONTRATO_CONSISTIR',
    'Indica se no processo consistir será validado o Contrato',
    '1',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    2
);
COMMIT;