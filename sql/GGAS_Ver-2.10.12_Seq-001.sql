UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaSegmento' WHERE MENU_DS_URL LIKE '%pesquisarSegmentos.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarSegmentos' WHERE RESI_DS LIKE 'pesquisarSegmentos.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirSegmento' WHERE RESI_DS LIKE 'inserirSegmento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarSegmento' WHERE RESI_DS LIKE 'atualizarSegmento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerSegmento' WHERE RESI_DS LIKE 'removerSegmento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoSegmento' WHERE RESI_DS LIKE '%exibirDetalhamentoSegmento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInsercaoSegmento' WHERE RESI_DS LIKE 'exibirInsercaoSegmento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizacaoSegmento' WHERE RESI_DS LIKE 'exibirAtualizacaoSegmento.do';
commit;