-----------------------------------------------------------------------------
--  Script para inserção do status Rescindido para Contrato
-----------------------------------------------------------------------------
INSERT INTO CONSTANTE_SISTEMA (COST_CD, TABE_CD, MOSI_CD, COST_NM, COST_DS, COST_VL, COST_CD_CLASSE, COST_NR_VERSAO, COST_IN_USO) 
SELECT SQ_COST_CD.nextval,
(SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'CONTRATO_SITUACAO'),
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS='Contrato'),
'C_CONTRATO_RESCINDIDO',
'Indica o código da entidade classe Status Rescindido.',
(SELECT COSI_CD FROM CONTRATO_SITUACAO WHERE COSI_DS LIKE 'Rescindido'),
null,
2,-- versao
1 -- Indicador de que o registro está ativo para o sistema (0-Nao, 1-Sim)
FROM dual WHERE NOT EXISTS(SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM='C_CONTRATO_RESCINDIDO');


COMMIT;