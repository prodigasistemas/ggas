UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaExcecaoLeituraFaturamento' WHERE RESI_DS = 'exibirPesquisaExcecaoLeituraFaturamento.do';

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO)
SELECT SQ_RESI_CD.nextval, 'exibirDetalhamentoAnormalidadeFaturamento', 0, (SELECT OPSI_CD FROM RECURSO_SISTEMA WHERE RESI_DS LIKE '%exibirPesquisaExcecaoLeituraFaturamento%'), 1, 1,CURRENT_TIMESTAMP 
FROM DUAL;

commit;