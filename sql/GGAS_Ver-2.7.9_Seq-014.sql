Insert into consumo_anormalidade (COAN_CD,COAN_DS,COAN_DS_ABREVIADO,COAN_IN_BLOQUEIA_FATURAMENTO,COAN_NR_VERSAO,COAN_IN_USO,
COAN_NR_VEZES_BLOQUEIO,COAN_TM_ULTIMA_ALTERACAO,COAN_NR_OCOR_ANORMALIDADE) 
select SQ_COAN_CD.nextval,'LEITURA MAIOR QUE QNT DIG. MED','LMLV','0','0','1',null,current_timestamp,null from dual
where not exists (select * from consumo_anormalidade where COAN_DS like 'LEITURA MAIOR QUE QNT DIG. MED');

Insert into constante_sistema (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'CONSUMO_ANORMALIDADE'),
(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDI%O'),'C_ANORMALIDADE_CONSUMO_LEITURA_MAIOR_QNT_DIGITOS_MEDIDOR',
'Indica a anormalidade leitura atual maior que quantidade de digitos do medidor',(select coan_cd from consumo_anormalidade where COAN_DS = 'LEITURA MAIOR QUE QNT DIG. MED'),
null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where COST_NM like 'C_ANORMALIDADE_CONSUMO_LEITURA_MAIOR_QNT_DIGITOS_MEDIDOR');

COMMIT;