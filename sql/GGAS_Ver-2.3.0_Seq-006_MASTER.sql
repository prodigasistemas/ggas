INSERT
INTO CONSUMO_ANORMALIDADE
  (
    COAN_CD,
    COAN_DS,
    COAN_DS_ABREVIADO,
    COAN_IN_BLOQUEIA_FATURAMENTO,
    COAN_NR_VERSAO,
    COAN_IN_USO,
    COAN_NR_VEZES_BLOQUEIO,
    COAN_TM_ULTIMA_ALTERACAO,
    COAN_NR_OCOR_ANORMALIDADE
  )
   select SQ_COAN_CD.nextval,
    'MEDIDOR COMPOSICAO S/ MEDICAO',
    'MESME',
    '1',
    '0',
    '1',
    NULL,
    current_timestamp,
    null
   from dual
    where not exists (select * from CONSUMO_ANORMALIDADE where COAN_DS like 'MEDIDOR COMPOSICAO S/ MEDICAO');
INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD
  FROM tabela
  WHERE upper(tabe_nm)=upper('CONSUMO_ANORMALIDADE')
  ),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS) like upper('Medi%o')
  ),
  'C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO',
  'Indica a anormalidade medidor da composicao sem medicao',
  (SELECT coan_cd FROM consumo_anormalidade WHERE COAN_DS= 'MEDIDOR COMPOSICAO S/ MEDICAO'),
  NULL,
  1,1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM='C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO'
  );
  
  commit;