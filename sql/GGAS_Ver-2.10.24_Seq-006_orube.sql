UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoUnidade' WHERE MENU_DS_URL LIKE '%exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=unidadeTipo%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoUnidade' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=unidadeTipo';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTipoUnidade' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=unidadeTipo%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirTipoUnidade' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=unidadeTipo%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlterarTipoUnidade' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=unidadeTipo%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTipoUnidade' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=unidadeTipo%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTipoUnidade' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=unidadeTipo%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoUnidade' WHERE RESI_DS LIKE '%removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=unidadeTipo%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirTipoUnidade'  WHERE RESI_DS LIKE '%incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=unidadeTipo%';

commit;