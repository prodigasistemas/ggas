-- Módulo Relatório --> ( Sub-Menu ) Contrato

	-- Quantidades Contratadas

INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'GERAR',(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Quantidades Contratadas%'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Relatórios%'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Quantidades Contratadas%')) ;
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Quantidades Contratadas%'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Quantidades Contratadas%')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Quantidades Contratadas%'))) ;
UPDATE OPERACAO_SISTEMA SET opsi_ds = 'EXIBIR' WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'CONSULTAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Quantidades Contratadas%'));
UPDATE RECURSO_SISTEMA SET resi_ds = 'exibirPesquisaRelatorioQuantidadesContratadas' WHERE resi_ds LIKE 'exibirPesquisaRelatorioQuantidadesContratadas.do';
UPDATE RECURSO_SISTEMA SET resi_ds = 'gerarRelatorioQuantidadesContratadas', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Quantidades Contratadas%')) WHERE resi_ds LIKE 'gerarRelatorioQuantidadesContratadas.do';
UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRelatorioQuantidadesContratadas' WHERE MENU_DS_URL LIKE 'exibirPesquisaRelatorioQuantidadesContratadas.do?acao=exibirPesquisaRelatorioQuantidadesContratadas%tela=relatorioQuantidadesContratadas';

	-- Cobrança e Recuperação de Penalidades

INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) SELECT SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Cobrança e Recuperação de penalidades%'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Cobrança e Recuperação de penalidades%')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP from dual where not exists (SELECT pesi_cd from permissao_sistema WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Cobrança e Recuperação de penalidades%'))) ;
UPDATE OPERACAO_SISTEMA SET opsi_ds = 'EXIBIR' WHERE opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE 'PESQUISAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Cobrança e Recuperação de penalidades%'));
UPDATE MENU SET MENU_DS_URL = 'exibirRelatorioCobrancaRecuperacaoPenalidades' WHERE MENU_DS_URL LIKE 'exibirRelatorioCobrancaRecuperacaoPenalidades.do%acao=exibirRelatorioCobrancaRecuperacaoPenalidades';
UPDATE recurso_sistema SET resi_ds = 'exibirRelatorioCobrancaRecuperacaoPenalidades' WHERE resi_ds LIKE 'exibirRelatorioCobrancaRecuperacaoPenalidades.do';
UPDATE recurso_sistema SET resi_ds = 'gerarRelatorioCobrancaRecuperacaoPenalidades' WHERE resi_ds LIKE 'gerarRelatorioCobrancaRecuperacaoPenalidades.do';

COMMIT;
