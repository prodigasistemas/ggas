UPDATE tabela
SET tabe_in_mapeamento = 1,
  tabe_in_constante    =1
WHERE tabe_nm_mnemonico='IMSI';

UPDATE constante_sistema
SET tabe_cd=(SELECT tabe_cd FROM tabela WHERE tabe_nm_mnemonico='IMSI')
WHERE cost_nm IN ('C_IMOVEL_EM_PROSPECCAO', 'C_IMOVEL_STATUS_PROSPECTADO');

commit