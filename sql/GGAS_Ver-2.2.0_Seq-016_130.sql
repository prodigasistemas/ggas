Alter Table NFE Add(
  NFE_IN_PRESENCA number,
  NFE_IN_CONSUMIDOR_FINAL number,
  NFE_IN_TIPO_LOCAL_OPER number,
  NFE_VL_ICMS_DESON number,
  NFE_IN_IE_DEST number		
  --
  
);  

Comment on column NFE.NFE_IN_PRESENCA is 'Indicador de presença do comprador no estabelecimento comercial no momento da operação (indPres)';
Comment on column NFE.NFE_IN_CONSUMIDOR_FINAL is 'Indica operação com Consumidor final (indFinal)';
Comment on column NFE.NFE_IN_TIPO_LOCAL_OPER is 'Identificador de local de destino da operação (idDest)';
Comment on column NFE.NFE_VL_ICMS_DESON is 'Valor total do ICMS desonerado (vICMSDeson).';
Comment on column NFE.NFE_IN_IE_DEST is 'Indicador da IE do Destinatário (indIEDest).';
--


Alter Table NFE_ITEM Add(
  NFIT_VL_ICMS_DESON number,
  NFIT_VL_ICMS_OPER number,
  NFIT_VL_PERC_DIFER_ICMS number,
  NFIT_VL_ICMS_DIFER number,
  NFIT_NR_RECOPI varchar2(20),
  --
  NFIT_CD_MOT_DESON Number,
  NFIT_VL_DED_ISS number,
  NFIT_VL_OUTR_ISS number,
  NFIT_VL_DESC_INC_ISS number,
  NFIT_VL_DESC_COND_ISS number,
  NFIT_VL_ISS_RET number,
  NFIT_IND_ISS number,
  NFIT_CD_SERV varchar2(20),
  NFIT_CD_MUN_SERV varchar2(7),
  NFIT_CD_PAIS_SERV varchar2(4),
  NFIT_NR_PROC_JUD_SERV varchar2(30),
  NFIT_IN_INCENT_SERV number
);

Comment on column NFE_ITEM.NFIT_VL_ICMS_DESON is 'Valor de ICMS desonerado (vICMSDeson)';
Comment on column NFE_ITEM.NFIT_VL_ICMS_OPER is 'Valor do ICMS da Operação. Valor como se não tivesse o diferimento (vICMSOp)';
Comment on column NFE_ITEM.NFIT_VL_PERC_DIFER_ICMS is 'Percentual do diferimento do ICMS (pDif)';
Comment on column NFE_ITEM.NFIT_VL_ICMS_DIFER is 'Valor do ICMS diferido (vICMSDif)';
Comment on column NFE_ITEM.NFIT_NR_RECOPI is 'Número do RECOPI (nRECOPI)';
Comment On Column NFE_ITEM.NFIT_CD_MOT_DESON Is 'Motivo da desoneração do ICMS (motDesICMS)';
Comment On Column NFE_ITEM.NFIT_VL_DED_ISS Is 'Valor dedução para redução da Base de Cálculo (vDeducao)';
Comment On Column NFE_ITEM.NFIT_VL_OUTR_ISS Is 'Valor outras retenções (vOutro)';
Comment On Column NFE_ITEM.NFIT_VL_DESC_INC_ISS Is 'Valor desconto incondicionado (vDescIncond)';
Comment On Column NFE_ITEM.NFIT_VL_DESC_COND_ISS Is 'Valor desconto condicionado (vDescCond)';
Comment On Column NFE_ITEM.NFIT_VL_ISS_RET Is 'Valor retenção ISS (vISSRet)';
Comment On Column NFE_ITEM.NFIT_IND_ISS Is 'Indicador da exigibilidade do ISS (indISS). 1=Exigível, 2=Não incidência; 3=Isenção; 4=Exportação; 5=Imunidade; 6=Exigibilidade Suspensa por Decisão Judicial; 7=Exigibilidade Suspensa por Processo Administrativo;';
Comment On Column NFE_ITEM.NFIT_CD_SERV Is 'Código do serviço prestado dentro do município (cServico)';
Comment On Column NFE_ITEM.NFIT_CD_MUN_SERV Is 'Código do Município de incidência do imposto (cMun). Informar 9999999 para serviço fora do País.';
Comment On Column NFE_ITEM.NFIT_CD_PAIS_SERV Is 'Código do País onde o serviço foi prestado (cPais). Infomar somente se o município da prestação do serviço for 9999999.';
Comment On Column NFE_ITEM.NFIT_NR_PROC_JUD_SERV Is 'Motivo da desoneração do ICMS (nProcesso). Informar somente quando declarada a suspensão da exigibilidade do ISSQN.';
Comment On Column NFE_ITEM.NFIT_IN_INCENT_SERV Is 'Motivo da desoneração do ICMS (indIncentivo). 1=Sim; 2=Não;';
  

Alter Table NFE_ITEM Modify(
  NFIT_CD_LISTA_SERVICOS VARCHAR2(5)
);


Alter Table NFE_ITEM_COMB Add(
  NFIC_PERC_MIX_GN Number
);
Comment On Column NFE_ITEM_COMB.NFIC_PERC_MIX_GN Is 'Percentual de Gás Natural para o produto GLP (cProdANP=210203001) (pMixGN)';

Alter Table NFE_LOTE_NFSE Modify LNFS_DS_MSG_ERRO varchar2(4000);

Alter Table TI_NFE Add(
  TI_NFE_IN_PRESENCA number,
  TI_NFE_IN_CONSUMIDOR_FINAL number,
  TI_NFE_VL_ICMS_DESON number,
  TI_NFE_IN_IE_DEST number	
  
); 


Comment on column TI_NFE.TI_NFE_IN_PRESENCA is 'Indicador de presença do comprador no estabelecimento comercial no momento da operação (indPres)';
Comment on column TI_NFE.TI_NFE_IN_CONSUMIDOR_FINAL is 'Indica operação com Consumidor final (indFinal)';
Comment on column TI_NFE.TI_NFE_VL_ICMS_DESON is 'Valor total do ICMS desonerado (vICMSDeson).';
Comment on column TI_NFE.TI_NFE_IN_IE_DEST is 'indica o valor da IE do destinatário (indIEDest).';
--


  

Alter Table TI_NFE_ITEM Add(
  TI_NFIT_VL_ICMS_DESON number,
  TI_NFIT_VL_ICMS_OPER number,
  TI_NFIT_VL_PERC_DIFER_ICMS number,
  TI_NFIT_VL_ICMS_DIFER number,
  TI_NFIT_NR_RECOPI varchar2(20),
  TI_NFIT_CD_MOT_DESON Number,
  TI_NFIT_VL_DED_ISS number,
  TI_NFIT_VL_OUTR_ISS number,
  TI_NFIT_VL_DESC_INC_ISS number,
  TI_NFIT_VL_DESC_COND_ISS number,
  TI_NFIT_VL_ISS_RET number,
  TI_NFIT_IND_ISS number,
  TI_NFIT_CD_SERV varchar2(20),
  TI_NFIT_CD_MUN_SERV varchar2(7),
  TI_NFIT_CD_PAIS_SERV varchar2(4),
  TI_NFIT_NR_PROC_JUD_SERV varchar2(30),
  TI_NFIT_IN_INCENT_SERV number
);

Comment on column TI_NFE_ITEM.TI_NFIT_VL_ICMS_DESON is 'Valor de ICMS desonerado (vICMSDeson)';
Comment on column TI_NFE_ITEM.TI_NFIT_VL_ICMS_OPER is 'Valor do ICMS da Operação. Valor como se não tivesse o diferimento (vICMSOp)';
Comment on column TI_NFE_ITEM.TI_NFIT_VL_PERC_DIFER_ICMS is 'Percentual do diferimento do ICMS (pDif)';
Comment on column TI_NFE_ITEM.TI_NFIT_VL_ICMS_DIFER is 'Valor do ICMS diferido (vICMSDif)';
Comment on column TI_NFE_ITEM.TI_NFIT_NR_RECOPI is 'Número do RECOPI (nRECOPI)';
Comment On Column TI_NFE_ITEM.TI_NFIT_CD_MOT_DESON Is 'Motivo da desoneração do ICMS (motDesICMS)';
Comment On Column TI_NFE_ITEM.TI_NFIT_VL_DED_ISS Is 'Valor dedução para redução da Base de Cálculo (vDeducao)';
Comment On Column TI_NFE_ITEM.TI_NFIT_VL_OUTR_ISS Is 'Valor outras retenções (vOutro)';
Comment On Column TI_NFE_ITEM.TI_NFIT_VL_DESC_INC_ISS Is 'Valor desconto incondicionado (vDescIncond)';
Comment On Column TI_NFE_ITEM.TI_NFIT_VL_DESC_COND_ISS Is 'Valor desconto condicionado (vDescCond)';
Comment On Column TI_NFE_ITEM.TI_NFIT_VL_ISS_RET Is 'Valor retenção ISS (vISSRet)';
Comment On Column TI_NFE_ITEM.TI_NFIT_IND_ISS Is 'Indicador da exigibilidade do ISS (indISS). 1=Exigível, 2=Não incidência; 3=Isenção; 4=Exportação; 5=Imunidade; 6=Exigibilidade Suspensa por Decisão Judicial; 7=Exigibilidade Suspensa por Processo Administrativo;';
Comment On Column TI_NFE_ITEM.TI_NFIT_CD_SERV Is 'Código do serviço prestado dentro do município (cServico)';
Comment On Column TI_NFE_ITEM.TI_NFIT_CD_MUN_SERV Is 'Código do Município de incidência do imposto (cMun). Informar 9999999 para serviço fora do País.';
Comment On Column TI_NFE_ITEM.TI_NFIT_CD_PAIS_SERV Is 'Código do País onde o serviço foi prestado (cPais). Infomar somente se o município da prestação do serviço for 9999999.';
Comment On Column TI_NFE_ITEM.TI_NFIT_NR_PROC_JUD_SERV Is 'Motivo da desoneração do ICMS (nProcesso). Informar somente quando declarada a suspensão da exigibilidade do ISSQN.';
Comment On Column TI_NFE_ITEM.TI_NFIT_IN_INCENT_SERV Is 'Motivo da desoneração do ICMS (indIncentivo). 1=Sim; 2=Não;';



Alter Table TI_NFE_ITEM Modify(
  TI_NFIT_CD_LISTA_SERVICOS VARCHAR2(5)
);


Alter Table TI_NFE_ITEM_COMB Add(
  TI_NFIC_PERC_MIX_GN Number
);
  
Comment On Column TI_NFE_ITEM_COMB.TI_NFIC_PERC_MIX_GN Is 'Percentual de Gás Natural para o produto GLP (cProdANP=210203001) (pMixGN)';

commit;