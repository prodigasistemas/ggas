UPDATE MENU SET MENU_DS_URL = 'exibirAssocTempTarifaPontoConsumo' where menu_DS_URL LIKE '%exibirAssocTempTarifaPontoConsumo.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirIncluisaoAssocTemporariaTarifaPontoConsumoPasso1' WHERE RESI_DS LIKE '%exibirIncluisaoAssocTemporariaTarifaPontoConsumoPasso1.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirAssocTemporariaTarifaPontoConsumoItem' WHERE RESI_DS LIKE '%excluirAssocTemporariaTarifaPontoConsumoItem.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'adicionarAssocTempTarifaPontoConsumo' WHERE RESI_DS LIKE '%adicionarAssocTempTarifaPontoConsumo.do%';

commit;

UPDATE MENU SET MENU_DS_URL = 'exibirManutencaoIndiceFinanceiro' WHERE MENU_DS_URL LIKE '%exibirManutencaoIndiceFinanceiro.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirManutencaoIndiceFinanceiro' WHERE RESI_DS LIKE '%exibirManutencaoIndiceFinanceiro.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirIndiceFinanceiro' WHERE RESI_DS LIKE '%exibirIndiceFinanceiro.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarIndiceFinanceiro' WHERE RESI_DS LIKE '%atualizarIndiceFinanceiro.do%';

commit;

UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTarifa' WHERE MENU_DS_URL LIKE '%exibirPesquisaTarifa.do?acao=exibirPesquisaTarifa&tela=tarifa%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInsercaoTarifa' WHERE RESI_DS LIKE '%exibirInsercaoTarifa.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirTarifa' WHERE RESI_DS LIKE '%inserirTarifa.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'cadastrarDescontos' WHERE RESI_DS LIKE '%cadastrarDescontos.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirDescontoCadastrado' WHERE RESI_DS LIKE '%excluirDescontoCadastrado.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirVigenciaCadastrada' WHERE RESI_DS LIKE '%excluirVigenciaCadastrada.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTarifa' WHERE RESI_DS LIKE '%exibirPesquisaTarifa.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirTarifa' WHERE RESI_DS LIKE '%excluirTarifa.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTarifa' WHERE RESI_DS LIKE '%exibirDetalhamentoTarifa.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'autorizarTarifaVigencia' WHERE RESI_DS LIKE '%autorizarTarifaVigencia.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoTarifa' WHERE RESI_DS LIKE '%exibirAlteracaoTarifa.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTarifa' WHERE RESI_DS LIKE '%alterarTarifa.do%';

COMMIT;