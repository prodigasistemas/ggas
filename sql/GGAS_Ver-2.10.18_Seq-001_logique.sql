Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL', 'Indicador de exibição do menu da agência virtual para consulta de nota fiscal','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO', 'Indicador de exibição do menu da agência virtual para consulta de consumo por período','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL', 'Indicador de exibição do menu da agência virtual para consulta de declaração de quitação anual de débito','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL', 'Indicador de exibição do menu da agência virtual para autenticação de declaração de quitação anual de débito','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO', 'Indicador de exibição do menu da agência virtual para consulta de contrato de adesão','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL', 'Indicador de exibição do menu da agência virtual para solicitação de alteração cadastral','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO', 'Indicador de exibição do menu da agência virtual para alteração de data de vencimento','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO', 'Indicador de exibição do menu da agência virtual para solicitação de interrupção de fornecimento','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA', 'Indicador de exibição do menu da agência virtual para solicitação de religação de unidade consumidora','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS', 'Indicador de exibição do menu da agência virtual para solicitação de ligação de gás','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO', 'Indicador de exibição do menu da agência virtual para programação de consumo','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA', 'Indicador de exibição do menu da agência virtual para cromatografia','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

Insert into PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_DS_COMPLEMENTO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, TABE_CD, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO', 'Indicador de exibição do menu da agência virtual para fale conosco','1','Se desabilitado o menu da agência virtual será oculto','1','1','1',to_timestamp('13/03/18 11:32:12,993000000','DD/MM/RR HH24:MI:SS,FF'),null,'17');

commit;
