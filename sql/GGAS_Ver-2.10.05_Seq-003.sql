ALTER TABLE TI_NOTA_FISCAL_ITEM ADD "TINI_CD_RAMO_ATIVIDADE" NUMBER(3,0);

COMMENT ON COLUMN TI_NOTA_FISCAL_ITEM."TINI_CD_RAMO_ATIVIDADE" IS 'Chave primaria da tabela RAMO_ATIVIDADE';
      
INSERT INTO TABELA_COLUNA
  ( TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA, TACO_IN_VARIAVEL, TACO_CD_VARIAVEL )
SELECT
  SQ_TACO_CD.nextval,
   (SELECT TABE_CD FROM TABELA WHERE TABE_NM LIKE 'TI_NOTA_FISCAL_ITEM'),
  'TINI_CD_RAMO_ATIVIDADE',
  'Chave primaria da tabela RAMO_ATIVIDADE',
  'codigoRamoAtividade',
  '0',
  '0',
  '1',
  CURRENT_TIMESTAMP,
  '0',
  '0',
  '0',
  NULL
  FROM DUAL WHERE NOT EXISTS
    (SELECT *
    FROM TABELA_COLUNA
    WHERE TACO_NM LIKE 'TINI_CD_RAMO_ATIVIDADE'
    AND TABE_CD =
    (SELECT TABE_CD FROM TABELA WHERE TABE_NM LIKE 'TI_NOTA_FISCAL_ITEM')
  );


alter table leitura_movimento modify LEMO_MD_MIN_VAR_CONSUMO NUMBER(17,8) ;
alter table leitura_movimento modify LEMO_MD_MAX_VAR_CONSUMO NUMBER(17,8) ;
    
Insert into integracao_funcao_tabela (INFT_CD,INFU_CD,TABE_CD,INFT_NR_VERSAO,INFT_IN_USO,INFT_TM_ULTIMA_ALTERACAO) 
select SQ_INFT_CD.nextval,
(select infu_cd from integracao_funcao where upper(infu_nm) like 'NOTA FISCAL'),
(select tabe_cd from tabela where upper(tabe_nm) like 'RAMO_ATIVIDADE'),
'1','1',current_timestamp from dual
where not exists
(select * from integracao_funcao_tabela where 
INFU_CD in (select infu_cd from integracao_funcao where upper(infu_nm) like 'NOTA FISCAL')
and TABE_CD in (select tabe_cd from tabela where upper(tabe_nm) like 'RAMO_ATIVIDADE'));
    
update tabela set tabe_in_mapeamento = 1 where upper(tabe_nm) like 'RAMO_ATIVIDADE';

commit;    