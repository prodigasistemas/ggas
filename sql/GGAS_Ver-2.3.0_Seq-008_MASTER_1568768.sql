insert into contrato_aba_atributo 

  select SQ_COAA_CD.nextval,
  1,
  'Pressão Coletada',
  'pressaoColetada',
  1,
  1,
  current_timestamp,
  null,
  null 
  from dual where not exists (select * from contrato_aba_atributo where coaa_nm_coluna like '%pressaoColetada');

insert into contrato_aba_atributo 

  select SQ_COAA_CD.nextval,
  3,
  'Unidade Pressão Coletada',
  'unidadePressaoColetada',
  1,
  1,
  current_timestamp,
  (select coaa_cd from contrato_aba_atributo where coaa_nm_coluna like '%pressaoColetada%'),
  1 
  from dual where not exists (select * from contrato_aba_atributo where coaa_nm_coluna like '%unidadePressaoColetada');
  
Insert into CONSTANTE_SISTEMA 

	select SQ_COST_CD.nextval,
	(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),
	(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
	'C_ENTIDADE_CONTEUDO_CORRIGE_PT_SIM_CONTRATO_PRESSAO_COLETADA',
	'Indica que corrige PT com dados do contrato', 
	(select enco_cd from entidade_conteudo where enco_ds = 'Sim (Contrato/Pressão Coletada)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like '%Corrig% PT')),
	(select encl_cd from entidade_classe where encl_ds like '%Corrig% PT'),
	'1',
	'1',
	current_timestamp from dual where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CONTEUDO_CORRIGE_PT_SIM_CONTRATO_PRESSAO_COLETADA');


---------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
INSERT INTO entidade_conteudo 
	select SQ_ENCO_CD.nextval,
	(SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS like '%Corrig% PT'),
	'Sim (Contrato/Pressão Coletada)',
	null,
	0,
	1,
	1,
	current_timestamp,
	0 from dual where not exists (select * from entidade_conteudo where ENCO_DS = 'Sim (Contrato/Pressão Coletada)' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS like '%Corrig% PT'));

	
ALTER TABLE CONTRATO_PONTO_CONSUMO ADD COPC_MD_PRESSAO_COLETADA NUMBER(13,8); 
ALTER TABLE CONTRATO_PONTO_CONSUMO ADD UNID_CD_PRESSAO_COLETADA NUMBER(13,8); 

COMMENT ON COLUMN CONTRATO_PONTO_CONSUMO.COPC_MD_PRESSAO_COLETADA IS 'Pressão Coletada';
COMMENT ON COLUMN CONTRATO_PONTO_CONSUMO.UNID_CD_PRESSAO_COLETADA IS 'Chave primária da tabela UNIDADE que indica a unidade da pressão de fornecimento';

ALTER TABLE CONTRATO_PONTO_CONSUMO ADD CONSTRAINT FK_UNID_COPC_PRESSAO_COLETADA FOREIGN KEY ( UNID_CD_PRESSAO_COLETADA ) REFERENCES UNIDADE ( UNID_CD );

Insert into TABELA_COLUNA
  
  select SQ_TACO_CD.nextval,
  (select tabe_cd from tabela where TABE_NM = 'CONTRATO_PONTO_CONSUMO'),
  'COPC_MD_PRESSAO_COLETADA',
  'Pressão Coletada',
  'pressaoColetada',
  0,
  0,
  1,
  current_timestamp,
  0,
  0,
  0,
  null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_PONTO_CONSUMO') and TACO_NM = 'COPC_MD_PRESSAO_COLETADA');

Insert into TABELA_COLUNA
  
  select SQ_TACO_CD.nextval,
  (select tabe_cd from tabela where TABE_NM = 'CONTRATO_PONTO_CONSUMO'),
  'UNID_CD_PRESSAO_COLETADA',
  'Unidade Pressão Coletada',
  'unidadePressaoColetada',
  0,
  0,
  1,
  current_timestamp,
  0,
  0,
  0,
  null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_PONTO_CONSUMO') and TACO_NM = 'UNID_CD_PRESSAO_COLETADA');

  commit;