ALTER TABLE SERVICO_TIPO ADD "SRTI_DS_ORIENTACAO" VARCHAR2(800 CHAR) DEFAULT '';

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'SERVICO_TIPO'),'SRTI_DS_ORIENTACAO' , 'Texto de orientação ou instrução de trabalho que será mostrado no momento de abertura da AS.','orientacao',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'SERVICO_TIPO') and TACO_NM = 'SRTI_DS_ORIENTACAO');

COMMENT ON COLUMN SERVICO_TIPO."SRTI_DS_ORIENTACAO" IS 'Texto de orientação ou instrução de trabalho que será mostrado no momento de abertura da AS.';

COMMIT;