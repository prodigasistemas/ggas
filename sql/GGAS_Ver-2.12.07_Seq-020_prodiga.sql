ALTER TABLE SERVICO_TIPO ADD SRTI_IN_PROTOCOLO NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_IN_PROTOCOLO" IS 'Indica se serviço do tipo cadastrado gera protocolo.'; 

ALTER TABLE SERVICO_TIPO ADD SRTI_IN_PROTOCOLO_OBRIGATORIO NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_IN_PROTOCOLO_OBRIGATORIO" IS 'Indica se serviço do tipo cadastrado é obrigatório o retorno do protocolo.'; 

ALTER TABLE SERVICO_TIPO ADD SRTI_IN_PROTOCOLO_TIPO NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_IN_PROTOCOLO_TIPO" IS 'Indica o tipo de retorno do protocolo do serviço tipo cadastrado onde 0 - Coletivo 1 - Individual'; 

ALTER TABLE SERVICO_TIPO ADD SRTI_IN_NOTIFICACAO NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_IN_NOTIFICACAO" IS 'Indica se serviço do tipo cadastrado gera notificação.'; 

ALTER TABLE SERVICO_TIPO ADD SRTI_IN_INTEGRACAO NUMBER(1,0) DEFAULT 0;
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_IN_INTEGRACAO" IS 'Indica se serviço do tipo cadastrado faz integração com sistema terceirizado.'; 

ALTER TABLE SERVICO_TIPO ADD SRTI_QN_PRAZO_PROTOCOLO NUMBER(9,0) DEFAULT 0;
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_QN_PRAZO_PROTOCOLO" IS 'Indicar o prazo máximo em dias para aguardar o retorno.'; 


ALTER TABLE SERVICO_TIPO ADD SRTI_QN_PRAZO_AS NUMBER(9,0) DEFAULT 0;
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_QN_PRAZO_AS" IS 'Indica o prazo para emissão da Autorização de Serviço, após a conclusão da comunicação.'; 


INSERT INTO ENTIDADE_CONTEUDO VALUES
(SQ_ENCO_CD.NEXTVAL, (select encl_cd from entidade_classe where encl_ds like 'Tipo do documento impressao'), 'Protocolo', null, 1, 1, 1, CURRENT_TIMESTAMP, null);

INSERT INTO CONSTANTE_SISTEMA VALUES
(SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),
(select mosi_cd from modulo_sistema where mosi_ds like 'Atendimento Ao Público'), 'RELATORIO_PROTOCOLO',
'Chave Primaria da Tabela Entidade Conteudo Modelo Layout Impressão Protocolo',
(select enco_cd from entidade_conteudo where enco_ds like 'Protocolo'), null, 0, 1, CURRENT_TIMESTAMP);

ALTER TABLE SERVICO_TIPO ADD DOIL_CD_PROTOCOLO NUMBER(3,0) DEFAULT NULL;
COMMENT ON COLUMN SERVICO_TIPO."DOIL_CD_PROTOCOLO" IS 'Chave primária da tabela DOCUMENTO_IMPRESSAO_LAYOUT para Protocolos';

COMMIT;
