ALTER TABLE FATURA ADD (ENCO_CD_MOTIVO_COMPLEMENTO NUMBER(9, 0));
COMMENT ON COLUMN FATURA.ENCO_CD_MOTIVO_COMPLEMENTO IS 'Chave primária da tabela ENTIDADE_CONTEUDO que indica o motivo do complemento.';

INSERT INTO tabela_coluna (TACO_CD, TABE_CD, TACO_NM, TACO_DS, TACO_NM_PROPRIEDADE, TACO_IN_CONSULTA_DINAMICA, TACO_NR_VERSAO, TACO_IN_USO, TACO_TM_ULTIMA_ALTERACAO, TACO_IN_AUDITAVEL, TACO_IN_ALCADA) 
SELECT SQ_TACO_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'FATURA'), 'ENCO_CD_MOTIVO_COMPLEMENTO', 'Motivo do complemento', 'motivoComplemento', 0,0,1, CURRENT_TIMESTAMP, 0,0 FROM dual WHERE NOT EXISTS (SELECT * FROM tabela_coluna WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'FATURA') AND UPPER(TACO_NM)='ENCO_CD_MOTIVO_COMPLEMENTO');

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivo de Complemento de Fatura'), 'Complemento de ICMS ST', 'ICMSST', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Complemento de ICMS ST' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Motivo de Complemento de Fatura'));

update entidade_conteudo set ENCO_DS = 'Complemento de Preço', ENCO_DS_ABREVIADO = 'PREÇO' where ENCO_DS = 'Valor' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Motivo de Complemento de Fatura');

update entidade_conteudo set ENCO_DS = 'Complemento de Volume', ENCO_DS_ABREVIADO = 'VOLUME' where ENCO_DS = 'Volume' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Motivo de Complemento de Fatura');

update entidade_conteudo set ENCO_DS = 'Complemento de ICMS', ENCO_DS_ABREVIADO = 'ICMS' where ENCO_DS = 'Tributo' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Motivo de Complemento de Fatura');


Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('FATURA')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Faturamento')),'C_TIPOS_MOTIVO_COMPLEMENTO_FATURA',
'Indica a entidade classe com os tipos de complemento de fatura', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Motivo de Complemento de Fatura') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_TIPOS_MOTIVO_COMPLEMENTO_FATURA');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('C_MOTIVO_COMPLEMENTO_ICMS')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Faturamento')),'C_MOTIVO_COMPLEMENTO_ICMS',
'Indica chave primária da entidade_conteudo', (SELECT enco_cd FROM ENTIDADE_CONTEUDO WHERE upper(enco_ds) = upper('Complemento de ICMS') AND ENCL_CD = (select encl_cd from entidade_classe where upper(entidade_classe.ENCL_DS) = upper('Motivo de Complemento de Fatura'))) , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_MOTIVO_COMPLEMENTO_ICMS');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('C_MOTIVO_COMPLEMENTO_ICMS_ST')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Faturamento')),'C_MOTIVO_COMPLEMENTO_ICMS_ST',
'Indica chave primária da entidade_conteudo', (SELECT enco_cd FROM ENTIDADE_CONTEUDO WHERE upper(enco_ds) = upper('Complemento de ICMS ST') AND ENCL_CD = (select encl_cd from entidade_classe where upper(entidade_classe.ENCL_DS) = upper('Motivo de Complemento de Fatura'))) , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_MOTIVO_COMPLEMENTO_ICMS_ST');

commit;