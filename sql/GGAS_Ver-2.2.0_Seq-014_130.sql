Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '1', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '1' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '2', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '2' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '3', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '3' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hnfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx', '4', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hnfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '4' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao', '5', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao' and nfse_cd_uf = '5' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '6', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '6' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '7', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '7' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao?wsdl', '8', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao?wsdl' and nfse_cd_uf = '8' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '9', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '9' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao', '10', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao' and nfse_cd_uf = '10' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeAutorizacao', '11', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeAutorizacao' and nfse_cd_uf = '11' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao?wsdl', '12', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao?wsdl' and nfse_cd_uf = '12' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '13', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '13' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '14', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '14' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao?wsdl', '15', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao?wsdl' and nfse_cd_uf = '15' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '16', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '16' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3?wsdl', '17', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3?wsdl' and nfse_cd_uf = '17' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '19', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '19' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '20', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '20' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '21', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '21' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '22', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '22' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '23', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '23' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '24', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '24' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '25', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '25' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx', '26', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx' and nfse_cd_uf = '26' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '27', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '27' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homnfe.sefaz.am.gov.br/services2/services/NfeAutorizacao', '28', 'H', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homnfe.sefaz.am.gov.br/services2/services/NfeAutorizacao' and nfse_cd_uf = '28' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '1', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '1' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '2', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '2' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '3', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '3' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx', '4', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '4' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao', '5', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao' and nfse_cd_uf = '5' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '6', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '6' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao?wsdl', '8', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao?wsdl' and nfse_cd_uf = '8' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao', '10', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao' and nfse_cd_uf = '10' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeAutorizacao', '11', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeAutorizacao' and nfse_cd_uf = '11' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao?wsdl', '12', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao?wsdl' and nfse_cd_uf = '12' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '14', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '14' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao?wsdl', '15', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao?wsdl' and nfse_cd_uf = '15' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3?wsdl', '17', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3?wsdl' and nfse_cd_uf = '17' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '19', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '19' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '21', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '21' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '22', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '22' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '23', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '23' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '24', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '24' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '25', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '25' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx', '26', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx' and nfse_cd_uf = '26' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx', '27', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' and nfse_cd_uf = '27' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.am.gov.br/services2/services/NfeAutorizacao', '28', 'P', 'RECNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.am.gov.br/services2/services/NfeAutorizacao' and nfse_cd_uf = '28' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RECNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '1', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '1' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '2', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '2' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '3', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '3' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hnfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx', '4', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hnfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx' and nfse_cd_uf = '4' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao', '5', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao' and nfse_cd_uf = '5' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '6', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '6' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx', '7', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' and nfse_cd_uf = '7' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao?wsdl', '8', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao?wsdl' and nfse_cd_uf = '8' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx', '9', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' and nfse_cd_uf = '9' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao', '10', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao' and nfse_cd_uf = '10' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeRetAutorizacao', '11', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeRetAutorizacao' and nfse_cd_uf = '11' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao?wsdl', '12', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao?wsdl' and nfse_cd_uf = '12' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx', '13', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' and nfse_cd_uf = '13' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '14', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '14' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao?wsdl', '15', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao?wsdl' and nfse_cd_uf = '15' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx', '16', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' and nfse_cd_uf = '16' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3?wsdl', '17', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3?wsdl' and nfse_cd_uf = '17' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '19', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '19' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx', '20', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' and nfse_cd_uf = '20' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '21', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '21' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '22', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '22' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '23', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '23' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '24', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '24' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '25', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '25' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx', '26', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx' and nfse_cd_uf = '26' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '27', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '27' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://homnfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao', '28', 'H', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://homnfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao' and nfse_cd_uf = '28' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '1', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '1' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '2', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '2' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '3', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '3' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx', '4', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx' and nfse_cd_uf = '4' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao', '5', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao' and nfse_cd_uf = '5' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '6', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '6' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '7', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '7' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao?wsdl', '8', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao?wsdl' and nfse_cd_uf = '8' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '9', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '9' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao', '10', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao' and nfse_cd_uf = '10' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRetAutorizacao', '11', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRetAutorizacao' and nfse_cd_uf = '11' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao?wsdl', '12', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao?wsdl' and nfse_cd_uf = '12' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '13', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '13' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '14', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '14' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao?wsdl', '15', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao?wsdl' and nfse_cd_uf = '15' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '16', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '16' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3?wsdl', '17', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3?wsdl' and nfse_cd_uf = '17' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '19', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '19' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx', '20', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' and nfse_cd_uf = '20' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '21', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '21' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '22', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '22' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '23', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '23' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '24', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '24' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '25', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '25' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx', '26', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx' and nfse_cd_uf = '26' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx', '27', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' and nfse_cd_uf = '27' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao', '28', 'P', 'RETNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao' and nfse_cd_uf = '28' and nfse_ds_tipo = 'P' and nfse_ds_oper_serv = 'RETNFE' and nfse_ds_versao = '3.10');

-- ################ 
Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2', '15', 'H', 'STNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2' and nfse_cd_uf = '15' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'STNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe-homologacao.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx', '2', 'H', 'STNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe-homologacao.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx' and nfse_cd_uf = '2' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'STNFE' and nfse_ds_versao = '3.10');

Insert Into NFE_SERVICOS_URL 
select SQ_NFSE_CD.NextVal, 'https://nfe-homologacao.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx', '2', 'H', 'CONNFE', '1', '1', Sysdate, '3.10'
FROM dual WHERE NOT EXISTS (select * from NFE_SERVICOS_URL where nfse_ds_url = 'https://nfe-homologacao.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx' and nfse_cd_uf = '2' and nfse_ds_tipo = 'H' and nfse_ds_oper_serv = 'CONNFE' and nfse_ds_versao = '3.10');

commit;