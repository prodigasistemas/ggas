UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoCilindro' WHERE MENU_DS_URL LIKE '%tipoBotijao%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoTipoCilindro' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%tela=tipoBotijao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoCilindro' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%tela=tipoBotijao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTipoCilindro' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%tela=tipoBotijao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoTipoCilindro' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%tela=tipoBotijao';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirTipoCilindro' WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%tela=tipoBotijao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTipoCilindro' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%tela=tipoBotijao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTipoCilindro' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%tela=tipoBotijao';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoCilindro' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%tela=tipoBotijao';

COMMIT;