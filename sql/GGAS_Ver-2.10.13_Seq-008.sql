UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaLocalidade' WHERE MENU_DS_URL LIKE '%pesquisarLocalidade.do?acao=exibirPesquisaLocalidade%';

UPDATE RECURSO_SISTEMA SET RESI_DS ='inserirLocalidade' WHERE RESI_DS LIKE '%inserirLocalidade.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS ='removerLocalidade' WHERE RESI_DS LIKE '%removerLocalidade.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS ='exibirDetalhamentoLocalidade' WHERE RESI_DS LIKE '%exibirDetalhamentoLocalidade.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS ='exibirInsercaoLocalidade' WHERE RESI_DS LIKE '%exibirInsercaoLocalidade.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS ='pesquisarLocalidade' WHERE RESI_DS LIKE '%pesquisarLocalidade.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS ='exibirAtualizacaoLocalidade' WHERE RESI_DS LIKE '%exibirAtualizacaoLocalidade.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS ='atualizarLocalidade' WHERE RESI_DS LIKE '%atualizarLocalidade.do%';

commit;