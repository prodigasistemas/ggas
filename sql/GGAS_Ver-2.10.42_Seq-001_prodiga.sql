----------------------------- RECURSO SISTEMA ----------------------------

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
select SQ_RESI_CD.nextval, 'alterarParametros', 0, 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS='ALTERAR' 
AND MENU_CD=(SELECT MENU_CD FROM MENU WHERE MENU_DS='Controle de parâmetros')), 1, 1,
CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'alterarParametros');

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
select SQ_RESI_CD.nextval, 'exibirProrrogacaoNotaCreditoDebito', 0, 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS='CONSULTAR' 
AND MENU_CD=(SELECT MENU_CD FROM MENU WHERE MENU_DS='Notas de Crédito / Débito')), 1, 1,
CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'exibirProrrogacaoNotaCreditoDebito');

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO) 
select SQ_RESI_CD.nextval, 'prorrogarNotaCreditoDebito', 0, 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS='CONSULTAR' 
AND MENU_CD=(SELECT MENU_CD FROM MENU WHERE MENU_DS='Notas de Crédito / Débito')), 1, 1,
CURRENT_TIMESTAMP from dual where not exists (select * from RECURSO_SISTEMA where RESI_DS like 'prorrogarNotaCreditoDebito');

commit;
