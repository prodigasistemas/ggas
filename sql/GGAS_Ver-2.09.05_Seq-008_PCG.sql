
INSERT INTO CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO)  
select SQ_COST_CD.nextval,
(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),
(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDI%O'), 
'C_TIPO_INTEGRACAO_SUPERVISORIO_DIARIA', 
'Indica o tipo de integração com o supervisório diária.', 
(select enco_cd from entidade_conteudo where upper(enco_ds) like 'DADOS DI%RIOS' and encl_cd=(select encl_cd from entidade_classe WHERE encl_ds like 'Tipo integração Supervis%rios')),
(SELECT encl_cd FROM entidade_classe WHERE encl_ds like 'Tipo integração Supervis%rios'),'1','1',current_timestamp
from dual where not exists (select * from constante_sistema where cost_nm = 'C_TIPO_INTEGRACAO_SUPERVISORIO_DIARIA');
  
INSERT INTO CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO)  
select SQ_COST_CD.nextval,
(select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),
(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDI%O'), 
'C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA', 
'Indica o tipo de integração com o supervisório horária.', 
(select enco_cd from entidade_conteudo where upper(enco_ds) like 'DADOS HOR%RIOS' and encl_cd=(select encl_cd from entidade_classe WHERE encl_ds like 'Tipo integração Supervis%rios')),
(SELECT encl_cd FROM entidade_classe WHERE encl_ds like 'Tipo integração Supervis%rios'),'1','1',current_timestamp
from dual where not exists (select * from constante_sistema where cost_nm = 'C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA');

commit;