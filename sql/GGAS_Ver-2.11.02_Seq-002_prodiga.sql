INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'PRAZO_MINIMO_CRITICIDADE',
    'Indica a quantidade de dias para a menor criticidade da priorizacao do corte',
    '7',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    14
);

INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'PRAZO_MAXIMO_CRITICIDADE',
    'Indica a quantidade de dias para a maior criticidade da priorizacao do corte',
    '20',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    14
);

INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'VALOR_MINIMO_CRITICIDADE',
    'Indica o valor da menor criticidade para priorizacao de corte',
    '1',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    14
);

INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'VALOR_MEDIO_CRITICIDADE',
    'Indica o valor do meio da criticidade para priorizacao de corte',
    '3',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    14
);


INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'VALOR_MAXIMO_CRITICIDADE',
    'Indica o valor da maior criticidade para priorizacao de corte',
    '5',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    14
);

commit;
