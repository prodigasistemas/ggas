UPDATE MENU SET MENU_DS_URL = 'exibirPesquisarEntregaDocumento' WHERE MENU_DS_URL LIKE '%exibirPesquisarEntregaDocumento%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisarEntregaDocumento' WHERE RESI_DS LIKE 'exibirPesquisarEntregaDocumento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirIncluirEntregaDocumento' WHERE RESI_DS LIKE 'exibirIncluirEntregaDocumento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlterarEntregaDocumento' WHERE RESI_DS LIKE 'exibirAlterarEntregaDocumento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerEntregaDocumento' WHERE RESI_DS LIKE 'removerEntregaDocumento.do';

COMMIT;
