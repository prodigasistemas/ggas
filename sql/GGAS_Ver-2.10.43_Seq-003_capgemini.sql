-----------------------------------------------------------------------------
--  Adicionando a coluna de equipe na tabela SERVICO_TIPO_AGENDAMENTO_TURNO
-----------------------------------------------------------------------------
ALTER TABLE SERVICO_TIPO_AGENDAMENTO_TURNO ADD EQUI_CD NUMBER(9,0);
COMMENT ON COLUMN "SERVICO_TIPO_AGENDAMENTO_TURNO"."EQUI_CD" IS 'Chave primaria da tabela EQUIPE';
ALTER TABLE "SERVICO_TIPO_AGENDAMENTO_TURNO" ADD CONSTRAINT FK_EQUI_SEAT FOREIGN KEY("EQUI_CD") REFERENCES "EQUIPE"("EQUI_CD");

-----------------------------------------------------------------------------
--  Alteracao da tabela PONTO CONSUMO - alteracao scala de casas decimais  --
-----------------------------------------------------------------------------
ALTER TABLE PONTO_CONSUMO MODIFY POCN_NR_LATITUDE_GRAU NUMBER(16,14); 

-----------------------------------------------------------------------------
--  Alteracao da tabela CLIENTE_IMOVEL_FIM_RELC_MOTV - aumento tamanho da coluna DS de 10 para 50 caracteres.--
-----------------------------------------------------------------------------
ALTER TABLE CLIENTE_IMOVEL_FIM_RELC_MOTV MODIFY CLIF_DS VARCHAR(50); 
-----------------------------------------------------------------------------
--  Alteracao da tabela cliente_imovel_relc_tipo - aumento tamanho da coluna DS de 10 para 50 caracteres.--
-----------------------------------------------------------------------------
ALTER TABLE CLIENTE_IMOVEL_RELC_TIPO MODIFY CLIR_DS VARCHAR(50);

-----------------------------------------------------------------------------
--  Alteracao da tabela FATURAMENTO_GRUPO --
-----------------------------------------------------------------------------
ALTER TABLE FATURAMENTO_GRUPO ADD FAGR_IN_CONTI_CASC_TARIFARIA NUMBER(1) DEFAULT 0;
ALTER TABLE FATURAMENTO_GRUPO ADD FAGR_IN_VENCIMENT_IGUAL_FATURA NUMBER(1) DEFAULT 0;



