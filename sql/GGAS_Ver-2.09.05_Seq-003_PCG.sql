
ALTER TABLE FINANCIAMENTO_TIPO ADD FITI_ESPECIE_TITULO VARCHAR2(32);
COMMENT ON COLUMN FINANCIAMENTO_TIPO.FITI_ESPECIE_TITULO IS 'Campo espécie título/documento presente nos boletos bancários';
INSERT INTO TABELA_COLUNA 
SELECT SQ_TACO_CD.NEXTVAL, 
       (SELECT TABE_CD 
        FROM   TABELA 
        WHERE  Upper(TABE_NM) LIKE 'FINANCIAMENTO_TIPO'), 
       'FITI_ESPECIE_TITULO', 
       'Campo espécie título/documento presente nos boletos bancários', 
       'especieTitulo', 
       0, 
       0, 
       1, 
       CURRENT_TIMESTAMP, 
       0, 
       0, 
       0, 
       NULL 
FROM   DUAL 
WHERE  NOT EXISTS (SELECT * 
                   FROM   TABELA_COLUNA 
                   WHERE  TABE_CD = (SELECT TABE_CD 
                                     FROM   TABELA 
                                     WHERE  TABE_NM = 'FINANCIAMENTO_TIPO') 
                          AND TACO_NM = 'FITI_ESPECIE_TITULO');

ALTER TABLE CONTRATO
ADD ARCC_CD_DEBITO_AUTOMATICO NUMBER(9, 0)
CONSTRAINT FK_ARCC_CONT_DEBITO_AUTOMATICO
REFERENCES ARRECADADOR_CONTRATO_CONVENIO (ARCC_CD);

ALTER TABLE CONTRATO
ADD ARCC_CD_IND_DEBITO_AUTOMATICO NUMBER(1, 0)
DEFAULT 0 NOT NULL;

COMMIT;


UPDATE constante_sistema
SET cost_vl =
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE upper(enco_ds) = 'SEM REGISTRO'
  AND encl_cd =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE encl_ds LIKE 'Tipo de Carteira de Cobran%a'
    )),
    cost_ds = 'Indica o tipo da carteira de cobrança não registrada.',
    cost_cd_classe =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE encl_ds LIKE 'Tipo de Carteira de Cobran%a'
    )
  WHERE cost_nm = 'C_TIPO_CONVENIO_NAO_COBRANCA_REGISTRADA';
  
UPDATE constante_sistema
SET cost_vl =
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE upper(enco_ds) = 'DIRETA'
  AND encl_cd =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE encl_ds LIKE 'Tipo de Carteira de Cobran%a'
    )),
    cost_ds = 'Indica o tipo da carteira de cobrança registrada direta.',
    cost_cd_classe =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE encl_ds LIKE 'Tipo de Carteira de Cobran%a'
    )
  WHERE cost_nm = 'C_TIPO_CONVENIO_COBRANCA_REGISTRADA';
  
INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT tabe_cd FROM tabela WHERE tabe_nm = 'ENTIDADE_CONTEUDO'
  ),
  (SELECT mosi_cd FROM modulo_sistema WHERE upper(mosi_DS) LIKE 'ARRECADA%%O'
  ),
  'C_TIPO_CONVENIO_COBRANCA_REGISTRADA_ESCRITURAL',
  'Indica o tipo da carteira de cobrança registrada escritural.',
  (SELECT enco_cd
  FROM entidade_conteudo
  WHERE upper(enco_ds) = 'ESCRITURAL'
  AND encl_cd          =
    (SELECT encl_cd
    FROM entidade_classe
    WHERE encl_ds LIKE 'Tipo de Carteira de Cobran%a'
    )),
    (SELECT encl_cd
    FROM entidade_classe
    WHERE encl_ds LIKE 'Tipo de Carteira de Cobran%a'
    ),
    '1',
    '1',
    CURRENT_TIMESTAMP
  FROM dual
  WHERE NOT EXISTS
    (SELECT *
    FROM constante_sistema
    WHERE cost_nm LIKE 'C_TIPO_CONVENIO_COBRANCA_REGISTRADA_ESCRITURAL'
    );

COMMIT;