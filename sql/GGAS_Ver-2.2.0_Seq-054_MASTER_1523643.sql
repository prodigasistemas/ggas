--1523643
INSERT INTO ENTIDADE_CLASSE
(ENCL_CD, ENCL_DS, ENCL_IN_USO, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_TM_ULTIMA_ALTERACAO)
SELECT SQ_ENCL_CD.NEXTVAL,
'Status Levantamento de Mercado' ,
1 ,
NULL ,
1 ,
CURRENT_TIMESTAMP
FROM dual WHERE NOT EXISTS ( SELECT * FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status Levantamento de Mercado');

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status Levantamento de Mercado'), 'Em Análise', 'EA', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Em Análise' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado'));

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status Levantamento de Mercado'), 'Proposta Enviada', 'PE', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Proposta Enviada' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado'));

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status Levantamento de Mercado'), 'EVTE Em Elaboração', 'EEE', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'EVTE Em Elaboração' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado'));

INSERT INTO entidade_conteudo (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval, (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status Levantamento de Mercado'), 'Encerrado', 'E', 0, 1, 1, current_timestamp, 0 from dual where not exists 
(select * from entidade_conteudo where ENCO_DS = 'Encerrado' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado'));



Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CLASSE')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_STATUS_LEVANTAMENTO_MERCADO',
'Está relacionada a todos os status do levantamento de mercado.', (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE ENCL_DS = 'Status Levantamento de Mercado') , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_STATUS_LEVANTAMENTO_MERCADO');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_LEVANTAMENTO_MERCADO_EM_ANALISE',
'Ao ser agendada uma visita técnica para o levantamento de mercado este status é acionado', (SELECT ENTIDADE_CONTEUDO.ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS = 'Em Análise' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado')) , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_LEVANTAMENTO_MERCADO_EM_ANALISE');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_LEVANTAMENTO_MERCADO_PROPOSTA_ENVIADA',
'Ao ser gerada uma proposta para o levantamento de mercado, este status é acionado', (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS = 'Proposta Enviada' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado')) , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_LEVANTAMENTO_MERCADO_PROPOSTA_ENVIADA');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_LEVANTAMENTO_MERCADO_EVTE_EM_ELABORACAO',
'Se a proposta for aprovada para o levantamento de mercado, este status é acionado', (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS = 'EVTE Em Elaboração' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado')) , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_LEVANTAMENTO_MERCADO_EVTE_EM_ELABORACAO');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select TABE_CD from tabela where upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
(select MOSI_CD from modulo_sistema where upper(MOSI_DS)=upper('Cadastro')),'C_LEVANTAMENTO_MERCADO_ENCERRADO',
'Se a proposta for Rejeitada para o levantamento de mercado, este status é acionado', (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE ENCO_DS = 'Encerrado' and ENCL_CD = (select ENCL_CD from entidade_classe where ENCL_DS = 'Status Levantamento de Mercado')) , null, 1, 1,current_timestamp from dual 
where not exists (select * from CONSTANTE_SISTEMA where COST_NM='C_LEVANTAMENTO_MERCADO_ENCERRADO');

ALTER TABLE LEVANTAMENTO_MERCADO ADD ENCO_CD_STATUS NUMBER(9, 0);
ALTER TABLE LEVANTAMENTO_MERCADO ADD CONSTRAINT FK_LEME_ENCO_STATUS FOREIGN KEY ( ENCO_CD_STATUS ) REFERENCES ENTIDADE_CONTEUDO(ENCO_CD);
COMMENT ON COLUMN LEVANTAMENTO_MERCADO.ENCO_CD_STATUS IS 'Chave Primaria da tabela ENTIDADE_CONTEUDO.';
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'LEVANTAMENTO_MERCADO'),'ENCO_CD_STATUS' , 'Chave Primaria da tabela ENTIDADE_CONTEUDO ','Status Levantamento Mercado',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'LEVANTAMENTO_MERCADO') and TACO_NM = 'ENCO_CD_STATUS');



commit;