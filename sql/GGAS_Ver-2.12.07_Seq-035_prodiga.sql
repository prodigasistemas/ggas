INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Autorização de Serviço Desligamento mais RIF Algas', 'Relatorio Autorização de Serviço Desligamento mais RIF Algas', 'relatorioAutorizacaoDesligamentoRIFAlgas.jasper',
(select enco_cd from entidade_conteudo where enco_ds like 'Autorização de Serviço'), 1, 1, CURRENT_TIMESTAMP);

INSERT INTO DOCUMENTO_IMPRESSAO_LAYOUT VALUES
(SQ_DOIL_CD.nextval, 'Relatorio Autorização de Serviço Inspeção Local Algas', 'Relatorio Autorização de Serviço Inspeção Local Algas', 'relatorioAutorizacaoInspecaoLocal.jasper',
(select enco_cd from entidade_conteudo where enco_ds like 'Autorização de Serviço'), 1, 1, CURRENT_TIMESTAMP);

COMMIT;