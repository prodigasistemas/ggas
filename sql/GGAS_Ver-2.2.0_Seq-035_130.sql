UPDATE MENU SET MENU_DS  = 'Tipo de Cilindro' WHERE MENU_DS LIKE '%Tipo de Botijão%';

ALTER TABLE LEVANTAMENTO_MERCADO DROP COLUMN LEME_CILINDRO;

alter table levantamento_mercado drop column agen_cd;

INSERT into IMOVEL_SITUACAO 
  select SQ_IMSI_CD.NEXTVAL, 
  'EM PROSPECÇÃO', 
  0, 
  1, 
  current_timestamp, 
  2 from dual where not exists (select * from IMOVEL_SITUACAO where IMSI_DS = 'EM PROSPECÇÃO');
  
  INSERT into IMOVEL_SITUACAO 
  select SQ_IMSI_CD.NEXTVAL, 
  'PROSPECTADO', 
  0, 
  1, 
  current_timestamp, 
  2 from dual where not exists (select * from IMOVEL_SITUACAO where IMSI_DS = 'PROSPECTADO');

insert into constante_sistema select sq_cost_cd.nextval, (select TABE_CD from tabela where tabe_nm in ('ENTIDADE_CONTEUDO')), 
  (SELECT MOSI_cD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) = UPPER('CADASTRO')), 'C_IMOVEL_EM_PROSPECCAO', 
  'Indica Status Em Prospecção do Imóvel', (select IMSI_CD from IMOVEL_SITUACAO where UPPER(IMSI_DS) = UPPER('EM PROSPECÇÃO')), NULL, 0, 1,
  CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM = 'C_IMOVEL_EM_PROSPECCAO');
  
   insert into constante_sistema select sq_cost_cd.nextval, (select TABE_CD from tabela where tabe_nm in ('ENTIDADE_CONTEUDO')), 
  (SELECT MOSI_cD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) = UPPER('CADASTRO')), 'C_IMOVEL_STATUS_PROSPECTADO', 
  'Indica Status Prospectado do Imóvel', (select IMSI_CD from IMOVEL_SITUACAO where UPPER(IMSI_DS) = UPPER('PROSPECTADO')), NULL, 0, 1,
  CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM = 'C_IMOVEL_STATUS_PROSPECTADO');

COMMIT;