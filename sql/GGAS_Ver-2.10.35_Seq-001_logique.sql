-- Criarção do campo em contrato para informar se participa do e-cartas
ALTER TABLE CONTRATO ADD CONT_IND_PARTICIPA_ECARTAS NUMBER(1) DEFAULT 0;
COMMENT ON COLUMN "CONTRATO"."CONT_IND_PARTICIPA_ECARTAS" IS 'Indica se o contrato participa do convênio e-cartas';

-- Criando nova opção no modelo de contrato
INSERT INTO CONTRATO_ABA_ATRIBUTO (COAA_CD, COAB_CD, COAA_DS, COAA_NM_COLUNA, COAA_NR_VERSAO, COAA_IN_USO)
VALUES (SQ_COAA_CD.NEXTVAL, 1, 'Indicador de participação no E-Cartas', 'participaECartas', 1, 1);

-- Criando operação e atividade de sistema
INSERT INTO OPERACAO_SISTEMA (OPSI_CD, MOSI_CD, OPSI_DS, OPSI_NR_TIPO, OPSI_NR_VERSAO, OPSI_IN_AUDITAVEL, OPSI_IN_USO, OPSI_TM_ULTIMA_ALTERACAO, MENU_CD, OPSI_IN_EXIBIR)
VALUES (2000, 3, 'Emitir faturas no padrão e-cartas', 1, 0, 0, 1, sysdate, null, 1);

INSERT INTO ATIVIDADE_SISTEMA (ATSI_CD, OPSI_CD, ATSI_DS, ATSI_NR_SEQUENCIA, ATSI_CD_PRECEDENTE, ATSI_NR_DIAS_INTERVALO, ATSI_NR_DIAS_DURACAO, ATSI_IN_OBRIGATORIEDADE, ATSI_IN_AGENDAMENTO, ATSI_IN_EXTERNA, ATSI_IN_DETALHA_ROTA, ATSI_NR_HORA_INICIAL_PROCES, ATSI_NR_HORA_FINAL_PROCES, ATSI_DS_EMAIL, ATSI_IN_USO, ATSI_NR_VERSAO, ATSI_TM_ULTIMA_ALTERACAO, ATSI_IN_CRONOGRAMA, ATSI_IN_ENVIA_EMAIL, ATSI_DS_EMAIL_REMETENTE, MOSI_CD)
VALUES (SQ_ATSI_CD.NEXTVAL, 2000, 'EMITIR FATURA NO PADRÃO E-CARTAS', 14, 9, NULL, 2, 1, 1, 0, 1, 24, 4, NULL, 1, 1, sysdate , 1, 1, 'ggas@copergas.com.br', 3);

-- Criando entrada nas permissões
insert into MENU (MENU_CD,MENU_CD_PAI,MENU_DS,MENU_NR_VERSAO,MENU_IN_USO,MENU_TM_ULTIMA_ALTERACAO,MENU_IN_ALCADA,MENU_NR_ORDEM,RESI_CD,MENU_DS_URL)
values (SQ_MENU_CD.nextval,85,'Faturamento - Emitir faturas no padrão e-cartas','0','1', sysdate,'0',null,null,null);

insert into OPERACAO_SISTEMA (OPSI_CD,MOSI_CD,OPSI_DS,OPSI_NR_TIPO, OPSI_IN_EXIBIR, OPSI_NR_VERSAO,OPSI_IN_AUDITAVEL,OPSI_IN_USO,OPSI_TM_ULTIMA_ALTERACAO,MENU_CD)
values (SQ_OPSI_CD.nextval, '3','CONSULTAR','1','0', '0','0','1',sysdate,
(select MENU_CD from menu where menu_ds = 'Faturamento - Emitir faturas no padrão e-cartas' and menu_cd_pai = 85));

Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO)
VALUES( SQ_PESI_CD.nextval, (select MENU_CD from menu where menu_ds = 'Faturamento - Emitir faturas no padrão e-cartas' and menu_cd_pai = 85),SQ_OPSI_CD.currval,0,'0',1,1,sysdate);

insert into OPERACAO_SISTEMA (OPSI_CD,MOSI_CD,OPSI_DS,OPSI_NR_TIPO, OPSI_IN_EXIBIR, OPSI_NR_VERSAO,OPSI_IN_AUDITAVEL,OPSI_IN_USO,OPSI_TM_ULTIMA_ALTERACAO,MENU_CD)
values (SQ_OPSI_CD.nextval, '3','EXECUTAR','1','0', '0','0','1',sysdate,
(select MENU_CD from menu where menu_ds = 'Faturamento - Emitir faturas no padrão e-cartas' and menu_cd_pai = 85));

Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO)
VALUES( SQ_PESI_CD.nextval, (select MENU_CD from menu where menu_ds = 'Faturamento - Emitir faturas no padrão e-cartas' and menu_cd_pai = 85),SQ_OPSI_CD.currval,0,'0',1,1,sysdate);

insert into OPERACAO_SISTEMA (OPSI_CD,MOSI_CD,OPSI_DS,OPSI_NR_TIPO, OPSI_IN_EXIBIR, OPSI_NR_VERSAO,OPSI_IN_AUDITAVEL,OPSI_IN_USO,OPSI_TM_ULTIMA_ALTERACAO,MENU_CD)
values (SQ_OPSI_CD.nextval, '3','EXCLUIR','1','0', '0','0','1',sysdate,
(select MENU_CD from menu where menu_ds = 'Faturamento - Emitir faturas no padrão e-cartas' and menu_cd_pai = 85));

Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO)
VALUES( SQ_PESI_CD.nextval, (select MENU_CD from menu where menu_ds = 'Faturamento - Emitir faturas no padrão e-cartas' and menu_cd_pai = 85),SQ_OPSI_CD.currval,0,'0',1,1,sysdate);

-- Criando Recurso Sistema para fazer associação entre operação e o arquivo Batch

INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO)
VALUES (SQ_RESI_CD.NEXTVAL, 'br.com.ggas.faturamento.ecarta.batch.EmitirECartasBatch', 0, 2000, 0, 1, sysdate);

-- Criando parâmetros das informações vindas dos correios

insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD) 
values (SQ_PMSI_CD.nextval, 'PARTICIPA_CONVENIO_ECARTAS', 'Indica se participa do convênio e-cartas', 0, 1 , 0, 1, localtimestamp , 6);

insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD) 
values (SQ_PMSI_CD.nextval, 'NUMERO_CONTRATO_ECARTAS', 'Número do contrato e-cartas', 'Contrato', 1 , 0, 1, localtimestamp , 6);

insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD) 
values (SQ_PMSI_CD.nextval, 'NUMERO_CARTAO_POSTAL_ECARTAS', 'Número do cartão postal e-cartas', 'Cartão postal', 1 , 0, 1, localtimestamp , 6);

insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD)
values (SQ_PMSI_CD.nextval, 'QUANTIDADE_FATURA_POR_LOTE_ECARTAS', 'Quantidade de faturas por lote do e-cartas', 20, 1 , 0, 1, localtimestamp , 6);

-- Criando tabelas de controle de lotes e arquivos

CREATE TABLE LOTE_ECARTA
(
   LOEC_CD NUMBER(9,0) PRIMARY KEY,
   LOEC_DT_EMISSAO TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   LOEC_NM_DOCUMENTO VARCHAR2(250 BYTE)
);

CREATE SEQUENCE  "SQ_LOEC_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

COMMENT ON COLUMN "LOTE_ECARTA"."LOEC_CD" IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_LOEC_CD';
COMMENT ON COLUMN "LOTE_ECARTA"."LOEC_DT_EMISSAO" IS 'Data de emissão do lote';
COMMENT ON COLUMN "LOTE_ECARTA"."LOEC_NM_DOCUMENTO" IS 'Nome do arquivo gerado';

CREATE TABLE LOTE_ECARTA_FATURA_IMPRESSAO
(
   LEFI_CD NUMBER(9,0) PRIMARY KEY,
   LOEC_CD NUMBER(9,0) NOT NULL,
   FAIM_CD NUMBER(9,0) NOT NULL,
   CONSTRAINT LOEC_FK FOREIGN KEY (LOEC_CD) REFERENCES LOTE_ECARTA(LOEC_CD),
   CONSTRAINT FAIM_FK FOREIGN KEY (FAIM_CD) REFERENCES FATURA_IMPRESSAO(FAIM_CD)
);

CREATE SEQUENCE  "SQ_LEFI_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

COMMENT ON COLUMN "LOTE_ECARTA_FATURA_IMPRESSAO"."LEFI_CD" IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_LEFI_CD';
COMMENT ON COLUMN "LOTE_ECARTA_FATURA_IMPRESSAO"."LOEC_CD" IS 'Chave primária da tabela LOTE_ECARTA';
COMMENT ON COLUMN "LOTE_ECARTA_FATURA_IMPRESSAO"."FAIM_CD" IS 'Chave primária da tabela FATURA_IMPRESSAO';

commit;
