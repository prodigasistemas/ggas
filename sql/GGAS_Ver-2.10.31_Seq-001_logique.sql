ALTER TABLE CONSUMO_ANORMALIDADE ADD "COAN_DS_REGRA" VARCHAR2(256 BYTE) DEFAULT NULL;
COMMENT ON COLUMN CONSUMO_ANORMALIDADE."COAN_DS_REGRA" IS 'Descrição do regra aplicada no GGAS para disparar a anormalidade';

ALTER TABLE CONSUMO_ANORMALIDADE ADD "COAN_IN_IGNORAR_ACAO_AUTO" NUMBER(1, 0) DEFAULT 0;
COMMENT ON COLUMN CONSUMO_ANORMALIDADE."COAN_IN_IGNORAR_ACAO_AUTO" IS 'Indicador a ação de consumo está ativa para a anormalidade (0-Nao, 1-Sim)';

ALTER TABLE LEITURA_ANORMALIDADE ADD "LEAN_DS_REGRA" VARCHAR2(256 BYTE) DEFAULT NULL;
COMMENT ON COLUMN LEITURA_ANORMALIDADE."LEAN_DS_REGRA" IS 'Descrição do regra aplicada no GGAS para disparar a ação da anormalidade de leitura';

commit;
