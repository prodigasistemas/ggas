

UPDATE contrato_aba_atributo
SET coaa_cd_dependencia =
  (SELECT coaa_cd
  FROM CONTRATO_ABA_ATRIBUTO
  WHERE COAA_NM_COLUNA='indicadorProgramacaoConsumo'
  )
WHERE COAA_NM_COLUNA='variacaoSuperiorQDC';
commit;