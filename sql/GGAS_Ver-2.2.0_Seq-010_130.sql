update constante_sistema set cost_vl = (select enco_cd_documento_impres_tipo from documento_impressao_layout where doil_nm like 'RELATORIO VERSO NOTA FISCAL FATURA' and doil_in_uso = 1) where cost_nm like 'RELATORIO_VERSO_NOTA_FISCAL_FATURA';

alter table ponto_consumo modify POCN_DS_COMPLEMENTO VARCHAR2(255 CHAR);
update ponto_consumo set pocn_ds_complemento = pocn_ds where pocn_ds_complemento is null and upper(pocn_ds) like '%APT%';

commit;