UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaCronograma' WHERE MENU_DS_URL LIKE '%exibirPesquisaCronograma.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaCronograma' WHERE RESI_DS LIKE '%exibirPesquisaCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerCronograma' WHERE RESI_DS LIKE '%removerCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarCronograma' WHERE RESI_DS LIKE '%pesquisarCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoCronograma' WHERE RESI_DS LIKE '%exibirInclusaoCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirCronograma' WHERE RESI_DS LIKE '%incluirCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoCronograma' WHERE RESI_DS LIKE '%exibirAlterarCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarCronograma' WHERE RESI_DS LIKE '%alterarCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoCronograma' WHERE RESI_DS LIKE '%exibirDetalhamentoCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarCronograma' WHERE RESI_DS LIKE '%gerarCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'obterDadosMesAnoCiclo' WHERE RESI_DS LIKE '%obterDadosMesAnoCiclo.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'desfazerCronograma' WHERE RESI_DS LIKE '%desfazerCronograma.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirEscalonamentoLeiturista' WHERE RESI_DS LIKE '%exibirEscalonamentoLeiturista.do%';

commit;