UPDATE modulo_sistema
SET mosi_ds = 'Contabilidade'
WHERE mosi_ds LIKE 'Cont%bil/Financeiro';

UPDATE operacao_sistema
SET mosi_cd = (select mosi_cd from modulo_sistema where mosi_ds = 'Contabilidade')
WHERE opsi_ds LIKE '%Encerrar Refer%ncia Cont%bil%';

UPDATE menu
SET menu_ds = 'Contabilidade'
WHERE menu_ds like 'Cont%bil/Financeiro';


commit;