Insert into CONSUMO_ANORMALIDADE(COAN_CD,COAN_DS,COAN_DS_ABREVIADO,COAN_IN_BLOQUEIA_FATURAMENTO,COAN_NR_VERSAO,COAN_IN_USO,COAN_NR_VEZES_BLOQUEIO,COAN_TM_ULTIMA_ALTERACAO,COAN_NR_OCOR_ANORMALIDADE) 
select SQ_COAN_CD.nextval,'PERIODO LEITURA MAIOR PERIODIC','DIFOR','1','0','1',null,current_timestamp,null from dual where not exists (select * from CONSUMO_ANORMALIDADE where COAN_DS like 'PERIODO LEITURA MAIOR PERIODIC');
Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,'37','2','C_ANORMALIDADE_PERIODO_LEITURA_MAIOR','Indica a anormalidade de consumo periodo do leitura maior que o informado na periodicidade',(select COAN_CD from CONSUMO_ANORMALIDADE where coan_ds like 'PERIODO LEITURA MAIOR PERIODIC'),null,'1','1',current_timestamp 
from dual where not exists (select * from CONSTANTE_SISTEMA where cost_nm like 'C_ANORMALIDADE_PERIODO_LEITURA_MAIOR');

commit;