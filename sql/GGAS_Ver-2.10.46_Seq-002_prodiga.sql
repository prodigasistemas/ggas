INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'COBRANCA_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO',
    'Indica se deve exibir mensagem na conta e, ou, emitir o arquivo PDF. 1- Apenas na conta; 2- Apenas notificação PDF; 3- Conta e notificação PDF',
    '1',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    3
);

COMMIT;