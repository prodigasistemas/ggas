
insert into contrato_aba_atributo (
  COAA_CD,
  COAB_CD,
  COAA_DS,
  COAA_NM_COLUNA,
  COAA_NR_VERSAO,
  COAA_IN_USO,
  COAA_TM_ULTIMA_ALTERACAO,
  COAA_CD_DEPENDENCIA,
  COAA_IN_DEPEND_OBRIGATORIA
) values (
  sq_coaa_cd.nextval, --COAA_CD
  1, --COAB_CD
  'Documentos Anexos', --COAA_DS
  'documentosAnexos', --COAA_NM_COLUNA
  1, --COAA_NR_VERSAO
  1, --COAA_IN_USO
  current_timestamp, --COAA_TM_ULTIMA_ALTERACAO
  null,
  null
);


CREATE TABLE CONTRATO_ANEXO
(
  COAX_CD number(10) PRIMARY KEY,
  COAX_IN_USO number(1) NOT NULL,
  COAX_TM_ULTIMA_ALTERACAO timestamp NOT NULL,
  COAX_NR_VERSAO number(5,0) NOT NULL,
  COAX_ANEXO BLOB NOT NULL,
  COAX_DS VARCHAR2(50 BYTE) NOT NULL,
  COAX_NM VARCHAR2(50 BYTE) NOT NULL,
  CONT_CD NUMBER(10)
);

ALTER TABLE "CONTRATO_ANEXO" ADD CONSTRAINT "CONTRATO_ANEXO_FK" FOREIGN KEY ("CONT_CD") REFERENCES "CONTRATO" ("CONT_CD") ENABLE;

CREATE SEQUENCE  "SQ_COAX_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

INSERT INTO TABELA 
	(	TABE_CD,									TABE_NM,
		TABE_DS,									TABE_NM_MNEMONICO,			
		TABE_IN_ATRIBUTO_DINAMICO,					TABE_NR_VERSAO,								
		TABE_IN_USO,								TABE_TM_ULTIMA_ALTERACAO,
		TABE_IN_CONSULTA_DINAMICA,					TABE_NM_CLASSE,		
		TABE_IN_AUDITAVEL,							TABE_IN_ALCADA,	
		MENU_CD,									TABE_NR_MESES_DESCARTE,	
		TABE_IN_INTEGRACAO,							TABE_IN_MAPEAMENTO,				
		TABE_IN_CONSTANTE) 
SELECT
		SQ_TABE_CD.nextval,							'CONTRATO_ANEXO',
		'Anexos do contrato',						'COAX',
		0,											0,
		1,											current_timestamp,
		1,											'br.com.ggas.contrato.contrato.impl.ContratoAnexo',
		1,											0,						
		null,										null,
		1,											0,								
		0 
FROM dual where not exists (select * from TABELA where TABE_NM='CONTRATO_ANEXO');

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'COAX_CD',									'Chave primária gerada automaticamente e controlada pela sequence SQ_COAX_CD',
		'chavePrimaria',							0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'COAX_CD');

COMMENT ON COLUMN CONTRATO_ANEXO.COAX_CD IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_COAX_CD ';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'CONT_CD',									'Chave Estrangeira da tabela CONTRATO',
		'contrato',									0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'CONT_CD');

COMMENT ON COLUMN CONTRATO_ANEXO.CONT_CD IS 'Chave Estrangeira da tabela CONTRATO';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'COAX_NR_VERSAO',							'Numero da versao do registro, utilizado para controle de concorrencia.',
		'versao',									0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'COAX_NR_VERSAO');

COMMENT ON COLUMN CONTRATO_ANEXO.COAX_NR_VERSAO IS 'Numero da versão do registro, utilizado para controle de concorrencia.';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'COAX_IN_USO',								'Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)',
		'habilitado',								0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'COAX_IN_USO');

COMMENT ON COLUMN CONTRATO_ANEXO.COAX_IN_USO IS 'Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'COAX_TM_ULTIMA_ALTERACAO',					'Data e hora da realizacao da ultima alteracao.',
		'ultimaAlteracao',							0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'COAX_TM_ULTIMA_ALTERACAO');

COMMENT ON COLUMN CONTRATO_ANEXO.COAX_TM_ULTIMA_ALTERACAO IS 'Data e hora da realizacao da ultima alteracao.';
  
INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'COAX_DS',									'Descrição do arquivo anexo.',
		'descricaoAnexo',							0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'COAX_DS');

COMMENT ON COLUMN CONTRATO_ANEXO.COAX_DS IS 'Descrição do arquivo anexo.';
  
 INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'COAX_ANEXO',								'Bytes do arquivo anexo.',
		'documentoAnexo',							0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'COAX_ANEXO');

COMMENT ON COLUMN CONTRATO_ANEXO.COAX_ANEXO IS 'Bytes do arquivo anexo.';

INSERT INTO TABELA_COLUNA 
	(	TACO_CD,									TABE_CD,
		TACO_NM,									TACO_DS,
		TACO_NM_PROPRIEDADE,						TACO_IN_CONSULTA_DINAMICA,
		TACO_NR_VERSAO,								TACO_IN_USO,
		TACO_TM_ULTIMA_ALTERACAO,					TACO_IN_AUDITAVEL,
		TACO_IN_ALCADA,								TACO_IN_VARIAVEL,
		TACO_CD_VARIAVEL)
SELECT
		SQ_TACO_CD.nextval,							(select tabe_cd from tabela where TABE_NM = 'CONTRATO_ANEXO'),
		'COAX_NM',									'Nome do arquivo anexo.',
		'nomeArquivo',								0,
		0,											1,
		current_timestamp,							0,
		0,											0,
		NULL
FROM dual WHERE NOT EXISTS (SELECT * FROM TABELA_COLUNA WHERE TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONTRATO_ANEXO') and TACO_NM = 'COAX_NM');

COMMENT ON COLUMN CONTRATO_ANEXO.COAX_DS IS 'Nome do arquivo anexo.';

COMMIT;