-- Criando operação e atividade de sistema
INSERT INTO OPERACAO_SISTEMA (OPSI_CD, MOSI_CD, OPSI_DS, OPSI_NR_TIPO, OPSI_NR_VERSAO, OPSI_IN_AUDITAVEL, OPSI_IN_USO, OPSI_TM_ULTIMA_ALTERACAO, MENU_CD, OPSI_IN_EXIBIR)
VALUES (SQ_OPSI_CD.NEXTVAL, 15, 'Processar Baixa Título Retorno', 1, 0, 0, 1, sysdate, null, 1);

INSERT INTO ATIVIDADE_SISTEMA (ATSI_CD, OPSI_CD, ATSI_DS, ATSI_NR_SEQUENCIA, ATSI_CD_PRECEDENTE, ATSI_NR_DIAS_INTERVALO, ATSI_NR_DIAS_DURACAO, ATSI_IN_OBRIGATORIEDADE, ATSI_IN_AGENDAMENTO, ATSI_IN_EXTERNA, ATSI_IN_DETALHA_ROTA, ATSI_NR_HORA_INICIAL_PROCES, ATSI_NR_HORA_FINAL_PROCES, ATSI_DS_EMAIL, ATSI_IN_USO, ATSI_NR_VERSAO, ATSI_TM_ULTIMA_ALTERACAO, ATSI_IN_CRONOGRAMA, ATSI_IN_ENVIA_EMAIL, ATSI_DS_EMAIL_REMETENTE, MOSI_CD)
VALUES (SQ_ATSI_CD.NEXTVAL, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'Processar Baixa Título Retorno'), 'Processar Baixa Título Retorno', 0, NULL, NULL, 2, 1, 1, 0, 0, 24, 4, NULL, 1, 1, sysdate, 0, 0, NULL, 15);

-- Criando Recurso Sistema para fazer associação entre operação e o arquivo Batch
INSERT INTO RECURSO_SISTEMA (RESI_CD, RESI_DS, RESI_NR_VERSAO, OPSI_CD, RESI_IN_WEB, RESI_IN_USO, RESI_TM_ULTIMA_ALTERACAO)
VALUES (SQ_RESI_CD.NEXTVAL, 'br.com.ggas.integracao.titulos.batch.ProcessarIntegracaoBaixaTituloRetornoBatch', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'Processar Baixa Título Retorno'), 0, 1, sysdate);

-- Criando entrada nas permissões
insert into MENU (MENU_CD,MENU_CD_PAI,MENU_DS,MENU_NR_VERSAO,MENU_IN_USO,MENU_TM_ULTIMA_ALTERACAO,MENU_IN_ALCADA,MENU_NR_ORDEM,RESI_CD,MENU_DS_URL)
values (SQ_MENU_CD.nextval,85,'Integração - Processar integração baixa título retorno','0','1', sysdate,'0',null,null,null);

insert into OPERACAO_SISTEMA (OPSI_CD,MOSI_CD,OPSI_DS,OPSI_NR_TIPO, OPSI_IN_EXIBIR, OPSI_NR_VERSAO,OPSI_IN_AUDITAVEL,OPSI_IN_USO,OPSI_TM_ULTIMA_ALTERACAO,MENU_CD)
values (SQ_OPSI_CD.nextval, '15','CONSULTAR','1','0', '0','0','1',sysdate,
(select MENU_CD from menu where menu_ds = 'Integração - Processar integração baixa título retorno' and menu_cd_pai = 85));

Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO)
VALUES( SQ_PESI_CD.nextval, (select MENU_CD from menu where menu_ds = 'Integração - Processar integração baixa título retorno' and menu_cd_pai = 85),SQ_OPSI_CD.currval,0,'0',1,1,sysdate);

insert into OPERACAO_SISTEMA (OPSI_CD,MOSI_CD,OPSI_DS,OPSI_NR_TIPO, OPSI_IN_EXIBIR, OPSI_NR_VERSAO,OPSI_IN_AUDITAVEL,OPSI_IN_USO,OPSI_TM_ULTIMA_ALTERACAO,MENU_CD)
values (SQ_OPSI_CD.nextval, '15','EXECUTAR','1','0', '0','0','1',sysdate,
(select MENU_CD from menu where menu_ds = 'Integração - Processar integração baixa título retorno' and menu_cd_pai = 85));

Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO)
VALUES( SQ_PESI_CD.nextval, (select MENU_CD from menu where menu_ds = 'Integração - Processar integração baixa título retorno' and menu_cd_pai = 85),SQ_OPSI_CD.currval,0,'0',1,1,sysdate);

insert into OPERACAO_SISTEMA (OPSI_CD,MOSI_CD,OPSI_DS,OPSI_NR_TIPO, OPSI_IN_EXIBIR, OPSI_NR_VERSAO,OPSI_IN_AUDITAVEL,OPSI_IN_USO,OPSI_TM_ULTIMA_ALTERACAO,MENU_CD)
values (SQ_OPSI_CD.nextval, '15','EXCLUIR','1','0', '0','0','1',sysdate,
(select MENU_CD from menu where menu_ds = 'Integração - Processar integração baixa título retorno' and menu_cd_pai = 85));

Insert into PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO)
VALUES( SQ_PESI_CD.nextval, (select MENU_CD from menu where menu_ds = 'Integração - Processar integração baixa título retorno' and menu_cd_pai = 85),SQ_OPSI_CD.currval,0,'0',1,1,sysdate);

-- Inserindo nova constante SITUACAO_PAGAMENTO_FATURA_PARCIALMENTE_QUITADO
INSERT INTO CONSTANTE_SISTEMA(
        COST_CD,
        TABE_CD,
        MOSI_CD,
        COST_NM,
        COST_DS,
        COST_VL,
        COST_CD_CLASSE,
        COST_NR_VERSAO,
        COST_IN_USO,
        COST_TM_ULTIMA_ALTERACAO
)
SELECT
        SQ_COST_CD.nextval,
        '90',
        '3',
        'SITUACAO_PAGAMENTO_FATURA_PARCIALMENTE_QUITADO',
        'Informa a situação do pagamento fatura paga parcialmente devido a um baixa parcial de algum título',
        '92',
        '25',
        '1',
        '1',
        current_timestamp
FROM dual WHERE not exists
(SELECT * FROM CONSTANTE_SISTEMA WHERE cost_nm LIKE 'SITUACAO_PAGAMENTO_FATURA_PARCIALMENTE_QUITADO');

-- Atualizando constante SITUACAO_PAGAMENTO_FATURA_QUITADO
UPDATE CONSTANTE_SISTEMA SET COST_VL = '111' WHERE COST_NM = 'SITUACAO_PAGAMENTO_FATURA_QUITADO' AND COST_CD_CLASSE = 25;

COMMIT;
