-----------------------------------------------------------------------------
--  Tabela para definir a prioridade no envio dos e-mails dos protocolos.
-----------------------------------------------------------------------------
CREATE TABLE CHAS_OPER_EMAIL_PRIORI_PROTOCO
(
  CHAS_CD number(10),
  OPEE_DS_OPERACAO VARCHAR2(100) not null,
  CONSTRAINT FK_CHAS_OPER_PROTOCOLO FOREIGN KEY (CHAS_CD) REFERENCES CHAMADO_ASSUNTO(CHAS_CD),
  CONSTRAINT PK_CHAS_OPER_PROTOCOLO PRIMARY KEY (CHAS_CD, OPEE_DS_OPERACAO)
);

-----------------------------------------------------------------------------
--  ALTERACAO NA TABELA TIPO DE SERVICO
-----------------------------------------------------------------------------
ALTER TABLE SERVICO_TIPO ADD SRTI_IN_EMAIL_AGENDAR NUMBER(1,0);
ALTER TABLE SERVICO_TIPO MODIFY (SRTI_IN_EMAIL_AGENDAR DEFAULT 0);
COMMENT ON COLUMN "SERVICO_TIPO"."SRTI_IN_EMAIL_AGENDAR" IS 'Indica se serviço do tipo cadastrado envia email ao ser agendado.'; 