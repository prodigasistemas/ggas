UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRota' WHERE MENU_DS_URL LIKE '%exibirPesquisaRota.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaRota' WHERE RESI_DS LIKE '%exibirPesquisaRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarRota' WHERE RESI_DS LIKE '%pesquisarRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoRota' WHERE RESI_DS LIKE '%exibirInclusaoRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirRota' WHERE RESI_DS LIKE '%incluirRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'associarPontosConsumoRota' WHERE RESI_DS LIKE '%associarPontosConsumoRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'excluirRota' WHERE RESI_DS LIKE '%excluirRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoRota' WHERE RESI_DS LIKE '%exibirDetalhamentoRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoRota' WHERE RESI_DS LIKE '%exibirAlteracaoRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'associarPontosConsumoRotaAlterar' WHERE RESI_DS LIKE '%associarPontosConsumoRotaAlterar.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'remanejarPontosConsumoRota' WHERE RESI_DS LIKE '%remanejarPontosConsumoRota.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'carregarPontoConsumoRotaRemanejamento' WHERE RESI_DS LIKE '%carregarPontoConsumoRotaRemanejamento.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarRota' WHERE RESI_DS LIKE '%alterarRota.do%';

commit;
