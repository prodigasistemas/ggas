INSERT INTO parametro_sistema (
    pmsi_cd,
    pmsi_cd_parametro,
    pmsi_ds_parametro,
    pmsi_vl_parametro,
    pmsi_ds_complemento,
    pmsi_cd_tipo_parametro,
    pmsi_nr_versao,
    pmsi_in_uso,
    pmsi_tm_ultima_alteracao,
    tabe_cd,
    mosi_cd
) VALUES (
    sq_pmsi_cd.NEXTVAL,
    'NUMERO_DIAS_CHECAGEM_CORTE',
    'Indica a quantidade de dias para checagem se um ponto de consumo foi cortado',
    '7',
    NULL,
    1,
    1,
    1,
    current_timestamp,
    NULL,
    14
);

--------------------------------------- ALTER TABLE - AVISO_CORTE ---------------------------------------
ALTER TABLE AVISO_CORTE ADD AVCO_NR_CHECAGEM_CORTE NUMBER(1,0) DEFAULT 0;

COMMIT;