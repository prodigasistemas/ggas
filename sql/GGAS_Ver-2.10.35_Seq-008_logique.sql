insert into recurso_sistema (resi_cd,resi_ds,resi_nr_versao,opsi_cd,resi_in_uso,resi_in_web,resi_tm_ultima_alteracao) values (SQ_RESI_CD.nextval,'exibirPesquisaChamadoAssunto',0,1623,1,1,CURRENT_TIMESTAMP);
delete from TABELA_COLUNA where taco_cd = 3538;
update TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'chamadoTipo' where taco_cd = 3524;
update TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'enviarEmailCadastro' where taco_cd = 4300;
update TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'enviarEmailTramitar' where taco_cd = 4301;
update TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'enviarEmailReiterar' where taco_cd = 4302;
update TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'enviarEmailEncerrar' where taco_cd = 4303;

UPDATE TABELA_COLUNA SET TACO_NM_PROPRIEDADE = 'telaAtualizacao' WHERE TACO_CD = 3742;

UPDATE TABELA_COLUNA SET TACO_NM_PROPRIEDADE = 'chamadoTipoPesquisa' WHERE TACO_CD = 3838;

UPDATE TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'nomeQuestionario' where taco_cd = 3814;
UPDATE TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'tipoServico' where taco_cd = 3815;

UPDATE TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'pergunta' where taco_cd = 3830;
UPDATE TABELA_COLUNA set TACO_NM_PROPRIEDADE = 'notaResposta' where taco_cd = 3831;

UPDATE tabela set  menu_cd = 191 where tabe_cd = 346;
UPDATE tabela set  menu_cd = 193 where tabe_cd = 333;
UPDATE tabela set  menu_cd = 196 where tabe_cd = 340;
UPDATE tabela set  menu_cd = 195 where tabe_cd = 349;
UPDATE tabela set  menu_cd = 196 where tabe_cd = 340;
UPDATE tabela set  menu_cd = 222 where tabe_cd = 360;

commit;
