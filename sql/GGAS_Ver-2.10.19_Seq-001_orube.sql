UPDATE MENU SET MENU_DS_URL = 'exibirPesquisarPeriodicidade' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=periodicidade';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisarPeriodicidade' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=periodicidade';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirPeriodicidade' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=periodicidade';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoPeriodicidade' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=periodicidade';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarPeriodicidade' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=periodicidade';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarPeriodicidade' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=periodicidade';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirPeriodicidade' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=periodicidade';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarPeriodicidade' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=periodicidade';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerPeriodicidade' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=periodicidade';

COMMIT;
