INSERT INTO CONSTANTE_SISTEMA VALUES (SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CONTEUDO'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento Ao Público'), 'C_URL_IMAGEM_LOGO_ALGAS', 
'Indica a url da imagem de logo da algas', 'https://i.postimg.cc/KzHYVZzL/logo-Algas.jpg',
NULL, 1, 1, CURRENT_TIMESTAMP); 

INSERT INTO CONSTANTE_SISTEMA VALUES (SQ_COST_CD.nextval, (SELECT TABE_CD FROM TABELA WHERE TABE_NM = 'ENTIDADE_CONTEUDO'), 
(SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS = 'Atendimento Ao Público'), 'C_URL_IMAGEM_INFORMACAO_PUBLICA', 
'Indica a url da imagem de logo de informação pública', 'https://i.postimg.cc/PNnHPpWx/informacao-publica.jpg',
NULL, 1, 1, CURRENT_TIMESTAMP); 

COMMIT;