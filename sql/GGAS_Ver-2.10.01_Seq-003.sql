UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaMedidor' WHERE MENU_DS_URL LIKE '%exibirPesquisaMedidor.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaMedidor' WHERE RESI_DS LIKE 'exibirPesquisaMedidor.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarMedidor' WHERE RESI_DS LIKE 'pesquisarMedidor.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoMedidor' WHERE RESI_DS LIKE 'exibirDetalhamentoMedidor.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarMedidor' WHERE RESI_DS LIKE 'exibirAtualizarMedidor.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarMedidor' WHERE RESI_DS LIKE 'alterarMedidor.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoMedidor' WHERE RESI_DS LIKE 'exibirInclusaoMedidor.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirMedidor' WHERE RESI_DS LIKE 'incluirMedidor.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerMedidor' WHERE RESI_DS LIKE 'removerMedidor.do%';

commit;