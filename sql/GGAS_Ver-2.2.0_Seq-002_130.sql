insert into documento_impressao_layout 
  select SQ_DOIL_CD.nextval,
  'Aviso de Corte',
  'Relatório de aviso de corte',
  'relatorioAvisoCorteBahiaGas.jasper',
  (select ENCO_CD from ENTIDADE_CONTEUDO where enco_ds like '%Aviso de Corte%'),1,1,current_timestamp
  FROM DUAL WHERE NOT EXISTS
  (select * from documento_impressao_layout  where documento_impressao_layout.DOIL_NM_ARQUIVO  =  'relatorioAvisoCorteBahiaGas.jasper');


 Insert into PARAMETRO_SISTEMA (PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select SQ_PMSI_CD.nextval,'PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO',
'Nome do Gerente Financeiro.','NOME DO GERENTE FINANCEIRO',null,'1','1','1',
current_timestamp,null,'4' from dual where not exists (select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO = 'PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO');
commit;