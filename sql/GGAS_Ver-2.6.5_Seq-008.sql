-- NOTIFICACAO DE CORTE
-- desabilita os registros inválidos
update documento_impressao_layout
set DOIL_IN_USO = 0
where ENCO_CD_DOCUMENTO_IMPRES_TIPO =
(select ENCO_CD from ENTIDADE_CONTEUDO where upper(enco_ds) like upper('%Notifica%o de Corte%'));
-- insere registro padrão
insert into documento_impressao_layout 
  select SQ_DOIL_CD.nextval,
  'Notificacao de Corte',
  'Relatorio de notificacao de corte',
  'relatorioNotificacaoCorteSergas.jasper',
  (select ENCO_CD from ENTIDADE_CONTEUDO where upper(enco_ds) like upper('%Notifica%o de Corte%')),1,1,current_timestamp
  FROM DUAL WHERE NOT EXISTS
  (select * from documento_impressao_layout where DOIL_NM_ARQUIVO  =  'relatorioNotificacaoCorteSergas.jasper');
-- este update serve para o script rodar duas vezes
update documento_impressao_layout 
set DOIL_IN_USO = 1
where DOIL_NM_ARQUIVO like 'relatorioNotificacaoCorteSergas.jasper';


-- AVISO DE CORTE
-- desabilita os registros inválidos
update documento_impressao_layout
set DOIL_IN_USO = 0
where ENCO_CD_DOCUMENTO_IMPRES_TIPO =
(select ENCO_CD from ENTIDADE_CONTEUDO where upper(enco_ds) like upper('%Aviso de Corte%'));
-- insere registro padrão
insert into documento_impressao_layout 
  select SQ_DOIL_CD.nextval,
  'Aviso de Corte',
  'Relatorio de aviso de corte',
  'relatorioAvisoCorteSergas.jasper',
  (select ENCO_CD from ENTIDADE_CONTEUDO where upper(enco_ds) like upper('%Aviso de Corte%')),1,1,current_timestamp
  FROM DUAL WHERE NOT EXISTS
  (select * from documento_impressao_layout where DOIL_NM_ARQUIVO like 'relatorioAvisoCorteSergas.jasper');
-- este update serve para o script rodar duas vezes
update documento_impressao_layout 
set DOIL_IN_USO = 1
where DOIL_NM_ARQUIVO like 'relatorioAvisoCorteSergas.jasper';

commit;