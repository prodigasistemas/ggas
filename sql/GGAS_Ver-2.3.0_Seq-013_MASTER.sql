Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm = 'LEITURA_TIPO'),(select mosi_cd from modulo_sistema where upper(mosi_DS) like 'MEDI%O'),
'C_TIPO_LEITURA_PLANILHA','Indica o tipo da leitura planilha',(select leti_cd from leitura_tipo where leti_ds like 'PLANILHA'),null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_TIPO_LEITURA_PLANILHA');

ALTER TABLE leitura_movimento MODIFY ( LEMO_NM_CLIENTE VARCHAR2(255 CHAR)) ;

update CONSTANTE_SISTEMA set COST_VL = (select COAN_CD from CONSUMO_ANORMALIDADE where coan_ds like 'PERIODO LEITURA MAIOR PERIODIC') where cost_nm like 'C_ANORMALIDADE_PERIODO_LEITURA_MAIOR';

commit;