  Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'SERVICO_TIPO'), (select mosi_cd from modulo_sistema where upper(mosi_ds) like 'COBRAN%A'),
'C_SERVICO_TIPO_CORTE','Indica se o tipo de serviço realizado é CORTE',
null, null,'1','1',current_timestamp
from dual where not exists (select * from constante_sistema where cost_nm = 'C_SERVICO_TIPO_CORTE');

commit;