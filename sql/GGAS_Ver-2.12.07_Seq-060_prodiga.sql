ALTER TABLE VEICULO DROP COLUMN TRAN_CD;	

CREATE TABLE TRANSPORTADORA_VEICULO (
	TRVE_CD NUMBER(10) PRIMARY KEY,
	TRAN_CD NUMBER(10)  NOT NULL,
	VEIC_CD NUMBER(10) NOT NULL,
	TRVE_IN_USO NUMBER(1) NOT NULL,
	TRVE_TM_ULTIMA_ALTERACAO TIMESTAMP NOT NULL,
	TRVE_NR_VERSAO NUMBER(5) NOT NULL
);

ALTER TABLE TRANSPORTADORA_VEICULO
ADD CONSTRAINT TRVE_TRAN_FK FOREIGN KEY (TRAN_CD) REFERENCES TRANSPORTADORA(TRAN_CD) ENABLE;

ALTER TABLE TRANSPORTADORA_VEICULO
ADD CONSTRAINT TRVE_VEIC_FK FOREIGN KEY (VEIC_CD) REFERENCES VEICULO(VEIC_CD) ENABLE;

INSERT INTO MENU VALUES(SQ_MENU_CD.nextval, 
(SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Cadastro' AND MENU_CD_PAI IS NULL),
'Veículo', 1, 1, CURRENT_TIMESTAMP, 0, NULL, NULL, 'exibirPesquisaVeiculo', 0);

INSERT INTO OPERACAO_SISTEMA VALUES(SQ_OPSI_CD.nextval, (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS like '%Cadastro%'), 'CONSULTAR', 0,0,0,1,
CURRENT_TIMESTAMP, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo'), 1);

INSERT INTO OPERACAO_SISTEMA VALUES(SQ_OPSI_CD.nextval, (SELECT MOSI_CD FROM MODULO_SISTEMA WHERE MOSI_DS like '%Cadastro%'), 'INCLUIR', 0,0,0,1,
CURRENT_TIMESTAMP, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo'), 1);

INSERT INTO PERMISSAO_SISTEMA VALUES(SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo'), 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo')),
0,0,(SELECT PASI_CD FROM PAPEL_SISTEMA WHERE UPPER(PASI_DS) = 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO PERMISSAO_SISTEMA VALUES(SQ_PESI_CD.nextval, (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo'), 
(SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'INCLUIR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo')),
0,0,(SELECT PASI_CD FROM PAPEL_SISTEMA WHERE UPPER(PASI_DS) = 'ADMINISTRADOR'), 1, CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'exibirPesquisaVeiculo', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'pesquisarVeiculo', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'exibirDetalhamentoVeiculo', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'CONSULTAR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'exibirIncluirVeiculo', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'INCLUIR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo')),
1,1,CURRENT_TIMESTAMP);

INSERT INTO RECURSO_SISTEMA VALUES(SQ_RESI_CD.nextval, 'incluirEquipe', 0, (SELECT OPSI_CD FROM OPERACAO_SISTEMA WHERE OPSI_DS = 'INCLUIR' AND MENU_CD = (SELECT MENU_CD FROM MENU WHERE MENU_DS_URL = 'exibirPesquisaVeiculo')),
1,1,CURRENT_TIMESTAMP);

ALTER TABLE EQUIPE ADD EQUI_IN_ESCALA_TODOS_DIAS NUMBER(1) DEFAULT 0;

commit;