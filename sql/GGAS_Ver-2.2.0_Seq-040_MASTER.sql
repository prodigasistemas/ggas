-- Ajuste para código de movimento da caixa
update documento_layout_campo set DOLC_NR_POSICAO_FINAL = 150, DOLC_NR_POSICAO_INICIAL = 150 where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador where banc_cd = 
(select banc_cd from banco where banc_nr=104)) and DOLA_DS = 'DEBITO AUTOMATICO' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Caixa' and DOLA_METODO_PROCESSAMENTO = 'processarCadastroDebitoAutomatico') 
and dolc_ds_campo = 'codigoMovimento';

update documento_layout_retorno_oco set DORO_IN_PROCESSA_RECEBIMENTO = 0, DORO_IN_PROCESSA_CADASTRO = 1 where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador where banc_cd = 
(select banc_cd from banco where banc_nr=104)) and DOLA_DS = 'DEBITO AUTOMATICO' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Caixa' and DOLA_METODO_PROCESSAMENTO = 'processarCadastroDebitoAutomatico') 
and doro_ds = 'EXCLUSÃO DO OPTANTE';

update documento_layout_retorno_oco set DORO_IN_PROCESSA_RECEBIMENTO = 0, DORO_IN_PROCESSA_CADASTRO = 1 where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador where banc_cd = 
(select banc_cd from banco where banc_nr=104)) and DOLA_DS = 'DEBITO AUTOMATICO' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Caixa' and DOLA_METODO_PROCESSAMENTO = 'processarCadastroDebitoAutomatico') 
and doro_ds = 'INCLUSÃO DO OPTANTE';

commit;