INSERT INTO MENU (MENU_CD,MENU_CD_PAI,MENU_DS,MENU_NR_VERSAO,MENU_IN_USO,MENU_TM_ULTIMA_ALTERACAO,MENU_IN_ALCADA,MENU_NR_ORDEM,RESI_CD,MENU_DS_URL)
VALUES (SQ_MENU_CD.nextval,(select menu_cd from menu where menu_ds like '%Atendimento%' and menu_cd_pai is null),
'Comunicação','1','1',CURRENT_TIMESTAMP,'0',
(SELECT MAX(MENU_NR_ORDEM) + 1 FROM menu),null,null);

UPDATE MENU SET MENU_CD_PAI = (SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Comunicação') WHERE 
MENU_DS LIKE 'Controle de Comunicação de Interrupção';

UPDATE MENU SET MENU_CD_PAI = (SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Comunicação') WHERE 
MENU_DS LIKE 'Acompanhamento de Comunicação de Interrupção';

UPDATE MENU SET MENU_CD_PAI = (SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Comunicação') WHERE 
MENU_DS LIKE 'Controle de Protocolo';

COMMIT;