ALTER TABLE USUARIO_SISTEMA ADD "USSI_NM_TOKEN_API" VARCHAR (200) NULL;
COMMENT ON COLUMN USUARIO_SISTEMA."USSI_NM_TOKEN_API" IS 'Token para autenticação do usuário para uso de API para escrita e leitura de medição';

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'USUARIO_SISTEMA'),'USSI_NM_TOKEN_API' , 'Token de autenticação do usuário','token',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'USUARIO_SISTEMA') and TACO_NM = 'USSI_NM_TOKEN_API');

COMMIT;
