ALTER TABLE TARIFA_VIGENCIA_FAIXA 
  ADD TAVF_VL_COMPRA NUMBER(15,8);

COMMENT ON COLUMN TARIFA_VIGENCIA_FAIXA.TAVF_VL_COMPRA
  IS 'Valor compra da faixa da tarifa';

Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'TARIFA_VIGENCIA_FAIXA'),
      'TAVF_VL_COMPRA',
      'Valor compra da faixa da tarifa',
      'valorCompra',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'TARIFA_VIGENCIA_FAIXA') and TACO_NM = 'TAVF_VL_COMPRA');
            
ALTER TABLE TARIFA_VIGENCIA_FAIXA 
  ADD TAVF_VL_MARGEM_VALOR_AGREGADO NUMBER(15,8);

COMMENT ON COLUMN TARIFA_VIGENCIA_FAIXA.TAVF_VL_MARGEM_VALOR_AGREGADO
  IS 'Margem de valor agregado da faixa da tarifa';

Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'TARIFA_VIGENCIA_FAIXA'),
      'TAVF_VL_MARGEM_VALOR_AGREGADO',
      'Margem de valor agregado da faixa da tarifa',
      'valorCompra',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'TARIFA_VIGENCIA_FAIXA') and TACO_NM = 'TAVF_VL_MARGEM_VALOR_AGREGADO');
            
commit;