--1463939
--select * from documento_impressao_layout;
insert into documento_impressao_layout 
  select SQ_DOIL_CD.nextval,
  'Notificação de Corte',
  'Relatório de notificação de corte',
  'relatorioNotificacaoCorteBahiaGas.jasper',
  (select ENCO_CD from ENTIDADE_CONTEUDO where upper(enco_ds) like upper('%Notifica%o de Corte%')),1,1,current_timestamp
  FROM DUAL WHERE NOT EXISTS
  (select * from documento_impressao_layout where DOIL_NM_ARQUIVO  =  'relatorioNotificacaoCorteBahiaGas.jasper');

commit;