UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaDevolucao' WHERE MENU_DS_URL LIKE '%exibirPesquisaDevolucao.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaDevolucao' WHERE RESI_DS LIKE 'exibirPesquisaDevolucao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarDevolucoes' WHERE RESI_DS LIKE 'pesquisarDevolucoes.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalheDevolucao' WHERE RESI_DS LIKE 'exibirDetalheDevolucao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoDevolucao' WHERE RESI_DS LIKE 'exibirInclusaoDevolucao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoDevolucao' WHERE RESI_DS LIKE 'exibirAlteracaoDevolucao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerDevolucao' WHERE RESI_DS LIKE 'removerDevolucao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarDebitos' WHERE RESI_DS LIKE 'pesquisarDebitos.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirDevolucao' WHERE RESI_DS LIKE 'incluirDevolucao.do';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarDevolucao' WHERE RESI_DS LIKE 'alterarDevolucao.do';

COMMIT;