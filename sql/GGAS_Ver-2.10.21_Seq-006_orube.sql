UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaCreditoDebito' WHERE MENU_DS_URL LIKE '%exibirPesquisaCreditoDebito.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaCreditoDebito' WHERE RESI_DS LIKE '%exibirPesquisaCreditoDebito.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoCreditoDebito' WHERE RESI_DS LIKE '%exibirInclusaoCreditoDebito.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarCreditoDebito' WHERE RESI_DS LIKE '%pesquisarCreditoDebito.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'cancelarLancamentoCreditoDebito' WHERE RESI_DS LIKE '%cancelarLancamentoCreditoDebito.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoCreditoDebito' WHERE RESI_DS LIKE '%exibirDetalhamentoCreditoDebito.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'autorizarCreditoDebito' WHERE RESI_DS LIKE '%autorizarCreditoDebito.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'salvarParcelasCreditoDebito' WHERE RESI_DS LIKE '%salvarParcelasCreditoDebito.do%';

commit;
