
ALTER TABLE TI_TITULO_RECEBER
ADD TITR_VL_ACRESCIMOS NUMBER(15,2) NULL
ADD TITR_VL_DESCONTOS NUMBER(15,2) NULL;

COMMENT ON COLUMN TI_TITULO_RECEBER.TITR_VL_ACRESCIMOS IS 'Valor referente ao somátrio dos débitos associado a fatura';
COMMENT ON COLUMN TI_TITULO_RECEBER.TITR_VL_DESCONTOS IS 'Valor referente ao somátrio dos créditos associado a fatura';

comment on column  CONTRATO.CONT_IND_PARTICIPA_ECARTAS is 'Indicador se o contrato participa do convênio e-cartas';
comment on column  CONTRATO.CONT_NR_CONTA_CORRENTE is 'Número da conta corrente do cliente para débito automatico';
comment on column  CONTRATO.CONT_NR_AGENCIA is 'Número da agência com dígito verificador  do cliente para débito automatico';
comment on column  CONTRATO.CONT_IN_EXIGE_APROVACAO is 'Indicador se o contrato exige aprovação';
comment on column  CONTRATO.ARCC_CD_DEBITO_AUTOMATICO is 'Chave primaria da tabela ARRECADADOR_CONTRATO_CONVENIO  para indicar o convênio para débito automático';
comment on column  CONTRATO.CONT_IN_PROPOSTA_APROVADA is 'Indicador se o contrato exige que a proposta seja aprovada';
comment on column  CONTRATO.ARCC_CD_IND_DEBITO_AUTOMATICO is 'Chave Primária da Tabela ARRECADADOR_CONTRATO_CONVENIO';

commit;
