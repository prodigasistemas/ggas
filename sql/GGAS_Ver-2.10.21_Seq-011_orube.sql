UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaUnidadeClasse' WHERE MENU_DS_URL LIKE '%unidadeClasse%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoUnidadeClasse' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%tela=unidadeClasse';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaUnidadeClasse' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%tela=unidadeClasse';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoUnidadeClasse' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%tela=unidadeClasse';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoUnidadeClasse' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%tela=unidadeClasse';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirUnidadeClasse' WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%tela=unidadeClasse';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarUnidadeClasse' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%tela=unidadeClasse';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarUnidadeClasse' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%tela=unidadeClasse';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerUnidadeClasse' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%tela=unidadeClasse';