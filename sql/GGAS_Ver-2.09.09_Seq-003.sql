INSERT INTO ENTIDADE_CLASSE VALUES (SQ_ENCL_CD.nextval, 'Motivos Alteração Prazo de Encerramento', NULL, 0, 1, current_timestamp);

INSERT INTO CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) VALUES 
	(SQ_COST_CD.nextval,'346','14','C_ENT_CLASSE_MOT_ALT_PRAZO_ENCERRAMENTO','Indica o código da entidade classe Motivos de Alteração do prazo de encerramento.',
	(select encl_cd from entidade_classe where encl_ds like 'Motivos Alteração Prazo de Encerramento'),null,'1','1',current_timestamp);
	
	

COMMIT;