UPDATE CONTRATO_ABA_ATRIBUTO SET COAA_IN_DEPEND_OBRIGATORIA = 0 WHERE COAA_NM_COLUNA = 'tempoValidadeRetiradaQPNRC';
UPDATE CONTRATO_ABA_ATRIBUTO SET COAA_IN_USO = 0 WHERE COAA_NM_COLUNA = 'consideraParadaNaoProgramadaC';
UPDATE CONTRATO_ABA_ATRIBUTO SET COAA_CD_DEPENDENCIA = 214 WHERE COAA_NM_COLUNA LIKE '%indicadorImpostoM%';

COMMIT;