CREATE TABLE TI_CONTRATO
(
  TICO_CD NUMBER(9,0) PRIMARY KEY,
  TICO_CD_ORIGEM NUMBER(9,0) NOT NULL,
  TICO_CD_SISTEMA_ORIGEM VARCHAR2(3 CHAR) NOT NULL,
  TICO_CD_SISTEMA_DESTINO VARCHAR2(3 CHAR) NOT NULL,
  TICO_CONT_PAI_CD NUMBER(9,0),
  TICO_COPC_CD NUMBER(9,0) NOT NULL,
  TICO_SEGM_CD NUMBER(9,0) NOT NULL,
  TICO_CD_MUNICIPIO_COB NUMBER(7,0),
  TICO_CD_MUNICIPIO_FAT NUMBER(7,0),
  TICO_CD_DESTINO NUMBER(9,0),
  TICO_CD_CLIENTE NUMBER(9,0) NOT NULL,
  TICO_CD_TIPO_CLIENTE NUMBER(9,0),
  TICO_NM_CLIE VARCHAR2(255 CHAR),
  TICO_NR_CPF VARCHAR2(11 BYTE),
  TICO_NR_CNPJ VARCHAR2(14 BYTE),
  TICO_TM_ULTIMA_ALTERACAO TIMESTAMP (6) DEFAULT CURRENT_TIMESTAMP NOT NULL ENABLE,
  TICO_DS_POCN VARCHAR2(100 BYTE),
  TICO_DS_LOGRADOURO_FAT VARCHAR2(130 BYTE),
  TICO_NR_END_FAT VARCHAR2(5 BYTE),
  TICO_DS_FAT_COMPLEMENTO VARCHAR2(255 CHAR),
  TICO_DS_BAIRRO_FAT VARCHAR2(60 BYTE),
  TICO_CD_CEP_FAT NUMBER(9,0),
  TICO_DS_MUNI_FAT_ORIGEM VARCHAR2(50 BYTE),
  TICO_UF_END_FAT_SG VARCHAR2(50 BYTE),
  TICO_PAIS_FAT_CD_BACEN NUMBER(4,0),
  TICO_DS_LOGRADOURO_COB VARCHAR2(130 BYTE),
  TICO_NR_END_COB VARCHAR2(5 BYTE),
  TICO_DS_COB_COMPLEMENTO VARCHAR2(255 CHAR),
  TICO_DS_BAIRRO_COB VARCHAR2(60 BYTE),
  TICO_DS_MUNI_COB_ORIGEM VARCHAR2(50 BYTE),
  TICO_UF_END_COB_SG VARCHAR2(50 BYTE),
  TICO_PAIS_COB_CD_BACEN NUMBER(4,0),
  TICO_DDD_NR_CLIE NUMBER(2,0),
  TICO_NR_CLIE NUMBER(10,0),
  TICO_NR_INSCRICAO_ESTADUAL VARCHAR2(20 BYTE),
  TICO_DS_EMAIL_PRINCIPAL VARCHAR2(80 BYTE),
  TICO_CD_OPERACAO VARCHAR2(1 CHAR) NOT NULL ENABLE,
  TICO_CD_SITUACAO VARCHAR2(2 CHAR) NOT NULL ENABLE,
  TICO_DS_ERRO VARCHAR2(2000 CHAR),
  TICO_NR_BANCO_DEB_ORIGEM NUMBER(9,0),
  TICO_NR_AGENCIA_DEB_ORIGEM VARCHAR2(35 BYTE),
  TICO_NR_CCORRENTE_DEB_ORIGEM VARCHAR2(35 BYTE),
  TICO_DS_TIPO_CONVENIO VARCHAR2(150 BYTE)
);

CREATE SEQUENCE  "SQ_TICO_CD" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

COMMENT ON COLUMN TI_CONTRATO.TICO_CD	IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_TICO_CD';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_ORIGEM	IS 'Indica o código do contrato gerado pelo sistema origem';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_SISTEMA_ORIGEM	IS	'Indica o código do sistema origem INTEGRAÇÃO_SISTEMA (PIR - Pirâmide, GAS - GGAS)';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_SISTEMA_DESTINO IS 'Indica o código do sistema destino INTEGRAÇÃO_SISTEMA (PIR - Pirâmide, GAS - GGAS)';
COMMENT ON COLUMN TI_CONTRATO.TICO_CONT_PAI_CD	IS	'Indica o código do contrato pai gerado pelo sistema origem';
COMMENT ON COLUMN TI_CONTRATO.TICO_COPC_CD	IS	'Indica o código do contrato ponto de consumo gerado pelo sistema origem';
COMMENT ON COLUMN TI_CONTRATO.TICO_SEGM_CD	IS	'Chave primária da tabela segmento';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_MUNICIPIO_COB	IS	'Indica o município do endereço de cobrança';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_MUNICIPIO_FAT	IS	'Indica o município do endereço de faturamento';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_DESTINO	IS	'Indica o código do contrato gerado pelo sistema destino';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_CLIENTE	IS	'Chave primária da tabela Cliente';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_TIPO_CLIENTE	IS	'Indica o tipo do cliente responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_NM_CLIE	IS	'Indica o nome do cliente responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_CPF	IS	'Indica o CPF do cliente do contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_CNPJ	IS	'Indica o CNPJ do cliente do contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_TM_ULTIMA_ALTERACAO	IS	'Indica a data e hora da realização da última alteração';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_POCN	IS	'Indica a descrição do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_LOGRADOURO_FAT	IS	'Indica o tipo, título e nome do logradouro do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_END_FAT	IS	'Indica o número do endereço do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_FAT_COMPLEMENTO	IS	'Indica o complemento do endereço do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_BAIRRO_FAT	IS	'Indica o nome do bairro do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_CEP_FAT	IS	'Indica o CEP do endereço (CEP) do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_MUNI_FAT_ORIGEM	IS	'Indica o município do endereço (MUNICIPIO) do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_UF_END_FAT_SG	IS	'Indica a sigla do Estado do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_PAIS_FAT_CD_BACEN	IS	'Indica o código do país do ponto de consumo';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_LOGRADOURO_COB	IS	'Indica o tipo, título e nome do logradouro do item de fatura';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_END_COB	IS	'Indica o número do endereço do item de fatura';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_COB_COMPLEMENTO	IS	'Indica o complemento do endereço do item de fatura';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_BAIRRO_COB	IS	'Indica o nome do bairro do item de fatura';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_MUNI_COB_ORIGEM	IS	'Indica o município do endereço (MUNICIPIO) do item de fatura';
COMMENT ON COLUMN TI_CONTRATO.TICO_UF_END_COB_SG	IS	'Indica a sigla do Estado do item de fatura';
COMMENT ON COLUMN TI_CONTRATO.TICO_PAIS_COB_CD_BACEN	IS	'Indica o código do país do item de fatura';
COMMENT ON COLUMN TI_CONTRATO.TICO_DDD_NR_CLIE	IS	'Indica o DDD do número do cliente responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_CLIE	IS	'Indica o número do cliente responsável pelo contrato.';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_INSCRICAO_ESTADUAL	IS	'Indica o número da Inscrição Estadual da pessoa jurídica responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_EMAIL_PRINCIPAL	IS	'Indica o e-mail principal do cliente responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_OPERACAO	IS	'Indica o código da operacao (‘I’= inclusão,‘A’= alteração, ‘E’ = exclusão)';
COMMENT ON COLUMN TI_CONTRATO.TICO_CD_SITUACAO	IS	'Indica a situação do processamento do registro por parte do Sistema Destino (‘P’ = processado, ‘NP’ = não processado,‘ER’ = erro no processamento e ‘AR’ = aguardando retorno)';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_ERRO	IS	'Indica a descrição do erro ocorrido no processamento do registro';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_BANCO_DEB_ORIGEM	IS	'Identifica o banco para débito em conta do cliente responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_AGENCIA_DEB_ORIGEM	IS	'Identifica a agência para débito em conta do cliente responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_NR_CCORRENTE_DEB_ORIGEM	IS	'Identifica a conta corrente para débito em conta do cliente responsável pelo contrato';
COMMENT ON COLUMN TI_CONTRATO.TICO_DS_TIPO_CONVENIO	IS	'Descreve o tipo de convênio de cobrança do cliente responsável pelo contrato';

-- Criação da função de integração
INSERT INTO INTEGRACAO_FUNCAO VALUES (7, 'Contrato', 'CONTR', 'Integração de Contratos', 402, 1, 1, sysdate, 396);

-- Criação da constante do sistema

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO)
values (SQ_COST_CD.nextval,'302','15','C_INTEGRACAO_CONTRATOS','Indica a função de Integração dos Contratos','7',null,'1','1',sysdate);

COMMIT;
