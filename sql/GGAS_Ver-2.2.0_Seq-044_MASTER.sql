--1508112
ALTER TABLE proposta ADD PROP_DT_ANO CHAR(4) NULL;
COMMENT ON COLUMN proposta.PROP_DT_ANO IS 'Ano que a proposta foi criada. Valor usado na montagem do número da proposta.';
INSERT INTO TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'PROPOSTA'),'PROP_DT_ANO','Ano que a proposta foi criada. Valor usado na montagem do número da proposta.','Ano que a proposta foi criada',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'PROPOSTA') and TACO_NM = 'PROP_DT_ANO');

ALTER TABLE proposta ADD PROP_VL_MEDIO_GN NUMBER(9, 2) NULL;
COMMENT ON COLUMN proposta.PROP_VL_MEDIO_GN IS 'Valor Médio do Gás Natural.';
INSERT INTO TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'PROPOSTA'),'PROP_VL_MEDIO_GN','Valor Médio do Gás Natural.','Valor Médio GN',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'PROPOSTA') and TACO_NM = 'PROP_VL_MEDIO_GN');

Insert into PARAMETRO_SISTEMA (PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,
PMSI_DS_COMPLEMENTO,PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD) 
select SQ_PMSI_CD.nextval,'SEQUENCIA_NUMERO_PROPOSTA',
'Sequencial Número Proposta.','0',null,'1','1','1',
current_timestamp,(select tabe_cd from tabela where tabela.TABE_NM = 'PARAMETRO_SISTEMA'),'4' from dual where not exists (select * from PARAMETRO_SISTEMA where PMSI_CD_PARAMETRO = 'SEQUENCIA_NUMERO_PROPOSTA');

COMMIT;