ALTER TABLE CHAMADO_ASSUNTO ADD "CHAS_IN_UNID_VISUALIZADORAS" NUMBER(1,0) DEFAULT 0;

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO'),'CHAS_IN_UNID_VISUALIZADORAS' , 'Indicador para saber se outra Unidade Organizacional pode visualizar o chamado com esse assunto','chavePrimaria',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO') and TACO_NM = 'CHAS_IN_UNID_VISUALIZADORAS');

CREATE SEQUENCE  "SQ_CAUO_CD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;
 
CREATE TABLE CHAMADO_ASSUNTO_UNIDADE
(
  CAUO_CD number(10) PRIMARY KEY,
  CHAS_CD number(10) not null,
  UNOR_CD number(10) not null,
  CAUO_IN_USO number(1) NOT NULL,
  CAUO_TM_ULTIMA_ALTERACAO timestamp NOT NULL,
  CAUO_NR_VERSAO number(5,0) NOT NULL,
  CONSTRAINT CAUO_CHAS_FK FOREIGN KEY (CHAS_CD) REFERENCES CHAMADO_ASSUNTO(CHAS_CD),
  CONSTRAINT CAUO_UNOR_FK FOREIGN KEY (UNOR_CD) REFERENCES UNIDADE_ORGANIZACIONAL(UNOR_CD)
);

COMMENT ON COLUMN CHAMADO_ASSUNTO_UNIDADE.CAUO_CD IS 'Chave primária gerada automaticamente e controlada pela sequence SQ_CAUO_CD ';
COMMENT ON COLUMN CHAMADO_ASSUNTO_UNIDADE.CHAS_CD IS 'Chave Estrangeira da tabela CHAMADO_ASSUNTO';
COMMENT ON COLUMN CHAMADO_ASSUNTO_UNIDADE.UNOR_CD IS 'Chave Estrangeira da tabela UNIDADE_ORGANIZACIONAL';
COMMENT ON COLUMN CHAMADO_ASSUNTO_UNIDADE.CAUO_NR_VERSAO IS 'Numero da versao do registro, utilizado para controle de concorrencia ';
COMMENT ON COLUMN CHAMADO_ASSUNTO_UNIDADE.CAUO_IN_USO IS 'Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)';
COMMENT ON COLUMN CHAMADO_ASSUNTO_UNIDADE.CAUO_TM_ULTIMA_ALTERACAO IS 'Data e hora da realizacao da ultima alteracao ';

Insert into TABELA (TABE_CD,TABE_NM,TABE_DS,TABE_NM_MNEMONICO,TABE_IN_ATRIBUTO_DINAMICO,TABE_NR_VERSAO,TABE_IN_USO,TABE_TM_ULTIMA_ALTERACAO,TABE_IN_CONSULTA_DINAMICA,TABE_NM_CLASSE,TABE_IN_AUDITAVEL,TABE_IN_ALCADA,MENU_CD,TABE_NR_MESES_DESCARTE,TABE_IN_INTEGRACAO,TABE_IN_MAPEAMENTO,TABE_IN_CONSTANTE) 
select SQ_TABE_CD.nextval,'CHAMADO_ASSUNTO_UNIDADE','Chamado assunto unidade organizacional','CAUO',0,0,1,current_timestamp,1,'br.com.ggas.atendimento.chamado.dominio.ChamadoAssuntoUnidadeOrganizacional',1,0,null,null,1,0,0 from dual where not exists (select * from TABELA where TABE_NM='CHAMADO_ASSUNTO_UNIDADE');

Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE'),'CAUO_CD' , 'Chave primária gerada automaticamente e controlada pela sequence SQ_CAUO_CD ','chavePrimaria',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE') and TACO_NM = 'CAUO_CD');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE'),'CHAS_CD' , 'Chave Estrangeira da tabela CHAMADO_ASSUNTO ','chavePrimaria',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE') and TACO_NM = 'CHAS_CD');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE'),'UNOR_CD' , 'Chave Estrangeira da tabela UNIDADE_ORGANIZACIONAL ','chavePrimaria',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE') and TACO_NM = 'UNOR_CD');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE'),'CAUO_NR_VERSAO','Numero da versao do registro, utilizado para controle de concorrencia','versao',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE') and TACO_NM = 'CAUO_NR_VERSAO');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE'),'CAUO_IN_USO','Indicador de que o registro esta ativo para o sistema (0-Nao, 1-Sim)','habilitado',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE') and TACO_NM = 'CAUO_IN_USO');
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE'),'CAUO_TM_ULTIMA_ALTERACAO','Data e hora da realizacao da última alteracao','ultimaAlteracao',0,0,1,current_timestamp,0,0,0,null from dual where not exists (select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CHAMADO_ASSUNTO_UNIDADE') and TACO_NM = 'CAUO_TM_ULTIMA_ALTERACAO');


COMMIT;