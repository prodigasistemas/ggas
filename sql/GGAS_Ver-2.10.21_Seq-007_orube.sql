UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoPessoa' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tipoCliente';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoPessoa' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tipoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoTipoPessoa' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=tipoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTipoPessoa' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=tipoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoTipoPessoa' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=tipoCliente';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTipoPessoa' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=tipoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirTipoPessoa' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=tipoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTipoPessoa' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=tipoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoPessoa' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=tipoCliente';

COMMIT;