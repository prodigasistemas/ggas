UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaCliente' WHERE MENU_DS_URL LIKE '%exibirPesquisaCliente.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarCliente' WHERE RESI_DS LIKE '%pesquisarCliente.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaCliente' WHERE RESI_DS LIKE '%exibirPesquisaCliente.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoCliente' WHERE RESI_DS LIKE '%exibirDetalhamentoCliente.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoCliente' WHERE RESI_DS LIKE 'exibirInclusaoCliente.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirCliente' WHERE RESI_DS LIKE 'incluirCliente.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoCliente' WHERE RESI_DS LIKE 'exibirAlteracaoCliente.do' AND OPSI_CD = 1039;
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarCliente' WHERE RESI_DS LIKE 'alterarCliente.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerCliente' WHERE RESI_DS LIKE 'removerCliente.do%';
COMMIT;