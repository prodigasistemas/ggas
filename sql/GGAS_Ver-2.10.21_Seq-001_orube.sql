UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoEndereco' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tipoEndereco';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoEndereco' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tipoEndereco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirTipoEndereco' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=tipoEndereco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTipoEndereco' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=tipoEndereco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarTipoEndereco' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=tipoEndereco';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirTipoEndereco' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=tipoEndereco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarTipoEndereco' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=tipoEndereco';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoEndereco' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=tipoEndereco';

COMMIT;