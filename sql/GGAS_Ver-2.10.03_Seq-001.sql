ALTER TABLE CHAMADO_ASSUNTO MODIFY CHAS_DS_ORIENTACAO VARCHAR2(800);
COMMENT ON COLUMN CHAMADO_ASSUNTO."CHAS_DS_ORIENTACAO" IS 'Texto de orientação ou instrução de trabalho que será mostrado no momento de abertura do chamado.';

COMMIT;