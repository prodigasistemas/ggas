INSERT INTO PARAMETRO_SISTEMA (
  PMSI_CD,
  PMSI_CD_PARAMETRO,
  PMSI_DS_PARAMETRO,
  PMSI_VL_PARAMETRO,
  PMSI_DS_COMPLEMENTO,
  PMSI_CD_TIPO_PARAMETRO,
  PMSI_NR_VERSAO,
  PMSI_IN_USO,
  PMSI_TM_ULTIMA_ALTERACAO,
  TABE_CD,
  MOSI_CD
)
VALUES (
  sq_pmsi_cd.NEXTVAL,
  'OBRIGATORIO_RETORNO_PROTOCOLO',
  'Indica se é obrigatório o retorno do protocolo no processo de cobrança',
  1,
  null,
  1,
  0,
  1,
  current_timestamp,
  null,
  3
);


COMMIT;