Insert into MENU (MENU_CD,MENU_CD_PAI,MENU_DS,MENU_NR_VERSAO,MENU_IN_USO,MENU_TM_ULTIMA_ALTERACAO,MENU_IN_ALCADA,MENU_NR_ORDEM,RESI_CD,MENU_DS_URL)
values (SQ_MENU_CD.nextval,(select menu_cd from menu where menu_ds like 'Cobrança' and menu_cd_pai is null),
'Negativação','1','1',CURRENT_TIMESTAMP,'0',
(SELECT MENU_NR_ORDEM FROM MENU WHERE MENU_DS like 'Débito'),null,null);


UPDATE MENU SET MENU_CD_PAI = (SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Negativação') WHERE 
MENU_DS LIKE 'Controle de Negativação';

UPDATE MENU SET MENU_CD_PAI = (SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Negativação') WHERE 
MENU_DS LIKE 'Agente Negativador';

UPDATE MENU SET MENU_CD_PAI = (SELECT MENU_CD FROM MENU WHERE MENU_DS = 'Negativação') WHERE 
MENU_DS LIKE 'Comando de Negativação';

COMMIT;