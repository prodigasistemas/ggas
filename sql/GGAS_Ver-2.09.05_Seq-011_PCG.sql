Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'SERVICO_PRESTADO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'CADASTRO'),
'C_SERVICO_PRESTADO_CONSTRUTORA','Indica o servico prestado construtora',(select sepr_cd from servico_prestado where sepr_ds like 'CONSTRUTORA'),null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_SERVICO_PRESTADO_CONSTRUTORA');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'SERVICO_PRESTADO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'CADASTRO'),
'C_SERVICO_PRESTADO_MEDICAO','Indica o servico prestado medicao',(select sepr_cd from servico_prestado where sepr_ds like 'MEDICAO'),null,'1','1',current_timestamp 
from dual where not exists (select * from constante_sistema where cost_nm like 'C_SERVICO_PRESTADO_MEDICAO');


COMMIT;