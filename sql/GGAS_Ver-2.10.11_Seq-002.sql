INSERT INTO PARAMETRO_SISTEMA
	(PMSI_CD,PMSI_CD_PARAMETRO,PMSI_DS_PARAMETRO,PMSI_VL_PARAMETRO,PMSI_DS_COMPLEMENTO,
	PMSI_CD_TIPO_PARAMETRO,PMSI_NR_VERSAO,PMSI_IN_USO,PMSI_TM_ULTIMA_ALTERACAO,TABE_CD,MOSI_CD)
VALUES (SQ_PMSI_CD.nextval,'P_PERGUNTA_QUESTIONARIO_SATISFACAO','Pergunta questionário satisfação',' ',null,'1','0','1',CURRENT_TIMESTAMP,null,NULL);

UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaUnidadeOrganizacional' WHERE MENU_DS_URL LIKE '%exibirPesquisaUnidadeOrganizacional.do%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoUnidadeOrganizacional' WHERE RESI_DS LIKE '%exibirDetalhamentoUnidadeOrganizacional.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirUnidadeOrganizacional' WHERE RESI_DS LIKE '%inserirUnidadeOrganizacional.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarUnidadeOrganizacional' WHERE RESI_DS LIKE '%exibirAtualizarUnidadeOrganizacional.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarUnidadeOrganizacional' WHERE RESI_DS LIKE '%atualizarUnidadeOrganizacional.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirRemocaoUnidadeOrganizacional' WHERE RESI_DS LIKE '%exibirRemocaoUnidadeOrganizacional.do%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaUnidadeOrganizacional' where RESI_DS LIKE '%exibirPesquisaUnidadeOrganizacional.do%';

commit;