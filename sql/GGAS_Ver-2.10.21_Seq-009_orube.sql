UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoSegmento' WHERE MENU_DS_URL LIKE '%tipoSegmento%';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInclusaoTipoSegmento' WHERE RESI_DS LIKE '%exibirInclusaoTabelaAuxiliar%tela=tipoSegmento';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoSegmento' WHERE RESI_DS LIKE '%exibirPesquisaTabelaAuxiliar%tela=tipoSegmento';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTipoSegmento' WHERE RESI_DS LIKE '%exibirDetalhamentoTabelaAuxiliar%tela=tipoSegmento';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAlteracaoTipoSegmento' WHERE RESI_DS LIKE '%exibirAlteracaoTabelaAuxiliar%tela=tipoSegmento';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirTipoSegmento' WHERE RESI_DS LIKE '%incluirTabelaAuxiliar%tela=tipoSegmento';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTipoSegmento' WHERE RESI_DS LIKE '%pesquisarTabelaAuxiliar%tela=tipoSegmento';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarTipoSegmento' WHERE RESI_DS LIKE '%alterarTabelaAuxiliar%tela=tipoSegmento';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoSegmento' WHERE RESI_DS LIKE '%removerTabelaAuxiliar%tela=tipoSegmento';