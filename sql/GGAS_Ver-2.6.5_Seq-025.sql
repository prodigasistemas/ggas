Insert into LEITURA_ANORMALIDADE (LEAN_CD,LEAN_DS,LEAN_DS_ABREVIADO,LEAC_CD_SEM_LEITURA,LEAC_CD_COM_LEITURA,LEAL_CD_SEM_LEITURA,LEAL_CD_COM_LEITURA,DOTI_CD,
LEAN_DS_MENSAGEM_LEITURA,LEAN_DS_MENSAGEM_MANUTENCAO,LEAN_DS_MENSAGEM_PREVENCAO,LEAN_NR_VEZES_CONSECUTIVAS,LEAN_IN_ACEITA_LEITURA,LEAN_IN_LISTAGEM,LEAN_IN_RETENCAO_CONTA,
LEAN_IN_ISENCAO,LEAN_IN_CREDITO_CONSUMO,LEAN_IN_FOTO,LEAN_IN_RELATIVO_MEDIDOR,LEAN_IN_IMOVEL_SEM_MEDIDOR,LEAN_IN_BLOQUEIA_FATURAMENTO,LEAN_IN_USO_SISTEMA,LEAN_NR_VEZES_BLOQUEIO,
LEAN_NR_VERSAO,LEAN_IN_USO,LEAN_TM_ULTIMA_ALTERACAO,SRTI_CD) 
select SQ_LEAN_CD.nextval,'Cons. entre leit.bloq. e ativ.',null,'6','3','5','1',null,null,null,null,null,'1',null,null,null,null,null,'1',null,'0','1',null,'4','1',
current_timestamp,null from dual where not exists (select * from leitura_anormalidade where upper(LEAN_DS) like 'CONS. ENTRE LEIT.BLOQ. E ATIV.' );

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval,(select tabe_cd from tabela where tabe_nm like 'LEITURA_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like 'MEDI%O'),
'C_ANORMALIDADE_LEITURA_ATIVACAO_DIF_BLOQUEIO','Indica que houve consumo entre a leitura de bloqueio e leitura de ativacao',
(select lean_cd from leitura_anormalidade where upper(lean_ds) like 'CONS. ENTRE LEIT.BLOQ. E ATIV.'),null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm = 'C_ANORMALIDADE_LEITURA_ATIVACAO_DIF_BLOQUEIO');

alter table NFE modify NFE_NR_TELEFONE VARCHAR2(12);
alter table NFE modify NFE_NR_TELEFONE_DESTINO VARCHAR2(12);

update constante_sistema set cost_vl = (select rubr_cd from rubrica where upper(rubr_ds) like 'DEVOLU%O PAGTO DUPLICIDADE%') 
where cost_nm like 'C_RUBRICA_CREDITO_PAGTO_MAIOR';

commit;