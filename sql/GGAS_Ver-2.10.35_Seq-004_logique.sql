insert into contrato_relatorio_tokens (cort_cd,cort_ds_descricao,cort_ds_valor,cort_ds_alias,cort_tm_ultima_alteracao) values (SQ_CORT_CD.nextval,'Prazo Contrato em Meses','prazoContratoMeses','getPrazoContratoMes',to_date('21/03/19','DD/MM/RR'));
ALTER TABLE CONSUMO_ANORMALIDADE
ADD COAN_DS_MENSAGEM_CONSUMO VARCHAR2(200 BYTE) NULL;
COMMENT ON COLUMN CONSUMO_ANORMALIDADE.COAN_DS_MENSAGEM_CONSUMO IS 'Mensagem a ser impressa na fatura caso a anormalidade tenha sido detectada';
commit;
