--------------------------- FATURAMENTO CRONOGRAMA ---------------------------------

ALTER TABLE FATURAMENTO_CRONOGRAMA ADD FAAC_CD NUMBER(9,0);
COMMENT ON COLUMN FATURAMENTO_CRONOGRAMA."FAAC_CD" IS 'Chave primária da tabela FATURAMENTO_ATIVIDADE_CRONOGRAMA que indica a atividade Registrar Leitura';