INSERT
INTO ENTIDADE_CLASSE
  (
    ENCL_CD,
    ENCL_DS,
    ENCL_IN_USO,
    ENCL_DS_ABREVIADO,
    ENCL_NR_VERSAO,
    ENCL_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_ENCL_CD.NEXTVAL,
  'Tipo Aplicacao Tributo Aliquota' ,
  1 ,
  'TpApTA' ,
  1 ,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM ENTIDADE_CLASSE
  WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota');
  
INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
    FROM ENTIDADE_CLASSE
    WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota'),
  'Percentual (%)',
  'Perc',
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  1
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Percentual (%)'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota'
    ));

INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM ENTIDADE_CLASSE
  WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota'),
  'Valor ($)',
  'Valr',
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  1
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Valor ($)'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota'
    ));

INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD FROM tabela WHERE upper(tabe_nm)=upper('ENTIDADE_CLASSE')),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS)=upper('Faturamento')),
  'C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA',
  'Esta relacionada ao tipo de aplicacao da aliquota do tributo.',
  (SELECT ENCL_CD
    FROM ENTIDADE_CLASSE
    WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota') ,
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM='C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA');

INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD FROM tabela WHERE upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS)=upper('Faturamento')),
  'C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_PERCENTUAL',
  'Esta relacionada a aplicacao em percentual da aliquota do tributo',
  (SELECT ENTIDADE_CONTEUDO.ENCO_CD
    FROM ENTIDADE_CONTEUDO
    WHERE ENCO_DS = 'Percentual (%)'
    AND ENCL_CD   =
      (SELECT ENCL_CD
      FROM entidade_classe
      WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota')) ,
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
    FROM CONSTANTE_SISTEMA
    WHERE COST_NM='C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_PERCENTUAL');

INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD FROM tabela WHERE upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS)=upper('Faturamento')),
  'C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_VALOR',
  'Esta relacionada a aplicacao do valor absoluto da aliquota do tributo',
  (SELECT ENTIDADE_CONTEUDO.ENCO_CD
    FROM ENTIDADE_CONTEUDO
    WHERE ENCO_DS = 'Valor ($)'
    AND ENCL_CD   =
      (SELECT ENCL_CD
      FROM entidade_classe
      WHERE ENCL_DS = 'Tipo Aplicacao Tributo Aliquota')) ,
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
    FROM CONSTANTE_SISTEMA
    WHERE COST_NM='C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_VALOR');

ALTER TABLE TRIBUTO_ALIQUOTA 
  ADD ENCO_CD_TIPO_APL_TRIB_ALIQ NUMBER(9,0)
  CONSTRAINT FK_TRAL_ENCO_TP_APL_TA REFERENCES ENTIDADE_CONTEUDO(ENCO_CD);
  
COMMENT ON COLUMN TRIBUTO_ALIQUOTA.ENCO_CD_TIPO_APL_TRIB_ALIQ 
  IS 'Chave primária da tabela ENTIDADE_CONTEUDO que indica o tipo de aplicacao da aliquota.';

INSERT
INTO tabela_coluna
  (
    TACO_CD,
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA
  )
SELECT SQ_TACO_CD.nextval,
  (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA'),
  'ENCO_CD_TIPO_APL_TRIB_ALIQ',
  'Tipo Aplicacao Tributo Aliquota',
  'tipoApliTribAliq',
  0,
  0,
  1,
  CURRENT_TIMESTAMP,
  0,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM tabela_coluna
  WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA')
    AND UPPER(TACO_NM)='ENCO_CD_TIPO_APL_TRIB_ALIQ');

update tributo_aliquota tral
  set 
    tral.enco_cd_tipo_apl_trib_aliq = 
      (select
        (case when tral2.TRAL_VL_ALIQUOTA > 0 
          then (SELECT cost_vl
            FROM CONSTANTE_SISTEMA
            WHERE COST_NM='C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_VALOR') 
          else (SELECT cost_vl
            FROM CONSTANTE_SISTEMA
            WHERE COST_NM='C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_PERCENTUAL') 
        end)
      FROM tributo_aliquota tral2
      where tral2.tral_cd = tral.tral_cd),
    tral.TRAL_VL_ALIQUOTA = 
      (select
        (case when tral2.TRAL_VL_ALIQUOTA > 0 
          then (TRAL_VL_ALIQUOTA) 
          else (TRAL_PR_ALIQUOTA * 100)
        end)
      FROM tributo_aliquota tral2
      where tral2.tral_cd = tral.tral_cd);

ALTER TABLE TRIBUTO_ALIQUOTA
  ADD CONSTRAINT NN01_TRAL_TIPO_APL_TRIB_ALIQ
  CHECK(ENCO_CD_TIPO_APL_TRIB_ALIQ IS NOT NULL);

ALTER TABLE TRIBUTO_ALIQUOTA DROP COLUMN TRAL_PR_ALIQUOTA;

delete tabela_coluna taco 
  where taco.tabe_cd = 
    (SELECT tabe.tabe_cd 
      FROM tabela tabe 
      where tabe.tabe_nm = 'TRIBUTO_ALIQUOTA')
    and taco_nm =  'TRAL_PR_ALIQUOTA';

INSERT
INTO ENTIDADE_CLASSE
  (
    ENCL_CD,
    ENCL_DS,
    ENCL_IN_USO,
    ENCL_DS_ABREVIADO,
    ENCL_NR_VERSAO,
    ENCL_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_ENCL_CD.NEXTVAL,
  'Tipo Aplicacao Reducao Base Calculo' ,
  1 ,
  'TpApRC' ,
  1 ,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM ENTIDADE_CLASSE
  WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo');

INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
    FROM ENTIDADE_CLASSE
    WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo'),
  'Percentual (%)',
  'Perc',
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  1
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Percentual (%)'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo'
    ));

INSERT
INTO entidade_conteudo
  (
    ENCO_CD,
    ENCL_CD,
    ENCO_DS,
    ENCO_DS_ABREVIADO,
    ENCO_IN_PADRAO,
    ENCO_NR_VERSAO,
    ENCO_IN_USO,
    ENCO_TM_ULTIMA_ALTERACAO,
    ENCO_NR_CODIGO
  )
SELECT SQ_ENCO_CD.nextval,
  (SELECT ENCL_CD
  FROM ENTIDADE_CLASSE
  WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo'),
  'Fator',
  'Fatr',
  0,
  1,
  1,
  CURRENT_TIMESTAMP,
  1
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM entidade_conteudo
  WHERE ENCO_DS = 'Fator'
  AND ENCL_CD   =
    (SELECT ENCL_CD
    FROM entidade_classe
    WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo'
    ));

INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD FROM tabela WHERE upper(tabe_nm)=upper('ENTIDADE_CLASSE')),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS)=upper('Faturamento')),
  'C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO',
  'Esta relacionada ao tipo de aplicacao da reducao da base de calculo.',
  (SELECT ENCL_CD
    FROM ENTIDADE_CLASSE
    WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo') ,
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM CONSTANTE_SISTEMA
  WHERE COST_NM='C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO');

INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD FROM tabela WHERE upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS)=upper('Faturamento')),
  'C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_PERCENTUAL',
  'Esta relacionada a aplicacao em percentual da reducao da base de calculo',
  (SELECT ENTIDADE_CONTEUDO.ENCO_CD
    FROM ENTIDADE_CONTEUDO
    WHERE ENCO_DS = 'Percentual (%)'
    AND ENCL_CD   =
      (SELECT ENCL_CD
      FROM entidade_classe
      WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo')) ,
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
    FROM CONSTANTE_SISTEMA
    WHERE COST_NM='C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_PERCENTUAL');

INSERT
INTO CONSTANTE_SISTEMA
  (
    COST_CD,
    TABE_CD,
    MOSI_CD,
    COST_NM,
    COST_DS,
    COST_VL,
    COST_CD_CLASSE,
    COST_NR_VERSAO,
    COST_IN_USO,
    COST_TM_ULTIMA_ALTERACAO
  )
SELECT SQ_COST_CD.nextval,
  (SELECT TABE_CD FROM tabela WHERE upper(tabe_nm)=upper('ENTIDADE_CONTEUDO')),
  (SELECT MOSI_CD FROM modulo_sistema WHERE upper(MOSI_DS)=upper('Faturamento')),
  'C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_FATOR',
  'Esta relacionada a aplicacao em fator da reducao da base de calculo',
  (SELECT ENTIDADE_CONTEUDO.ENCO_CD
    FROM ENTIDADE_CONTEUDO
    WHERE ENCO_DS = 'Fator'
    AND ENCL_CD   =
      (SELECT ENCL_CD
      FROM entidade_classe
      WHERE ENCL_DS = 'Tipo Aplicacao Reducao Base Calculo')) ,
  NULL,
  1,
  1,
  CURRENT_TIMESTAMP
FROM dual
WHERE NOT EXISTS
  (SELECT *
    FROM CONSTANTE_SISTEMA
    WHERE COST_NM='C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_FATOR');

ALTER TABLE TRIBUTO_ALIQUOTA 
  ADD TRAL_IN_RED_BASE_CALC NUMBER(1,0);
  
COMMENT ON COLUMN TRIBUTO_ALIQUOTA.TRAL_IN_RED_BASE_CALC 
  IS 'Indica se ha reducao da base de calculo.';

UPDATE TRIBUTO_ALIQUOTA 
  SET TRAL_IN_RED_BASE_CALC = 0;

ALTER TABLE TRIBUTO_ALIQUOTA
  ADD CONSTRAINT NN01_TRAL_IN_RED_BASE_CALC
  CHECK(TRAL_IN_RED_BASE_CALC IS NOT NULL);

ALTER TABLE TRIBUTO_ALIQUOTA 
  ADD ENCO_CD_TIPO_APL_RED_BASE_CALC NUMBER(9,0)
  CONSTRAINT FK_TRAL_ENCO_TP_APL_RBC REFERENCES ENTIDADE_CONTEUDO(ENCO_CD);
  
COMMENT ON COLUMN TRIBUTO_ALIQUOTA.ENCO_CD_TIPO_APL_RED_BASE_CALC 
  IS 'Chave primária da tabela ENTIDADE_CONTEUDO que indica o tipo de aplicacao da reducao de base de calculo.';

ALTER TABLE TRIBUTO_ALIQUOTA 
  ADD TRAL_VL_RED_BASE_CALC NUMBER(15,8);
  
COMMENT ON COLUMN TRIBUTO_ALIQUOTA.TRAL_VL_RED_BASE_CALC 
  IS 'Valor da reducao da base de calculo do tributo.';

INSERT
INTO tabela_coluna
  (
    TACO_CD,
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA
  )
SELECT SQ_TACO_CD.nextval,
  (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA'),
  'TRAL_IN_RED_BASE_CALC',
  'Indicador Reducao Base Calculo',
  'inRedBaseCalc',
  0,
  0,
  1,
  CURRENT_TIMESTAMP,
  0,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM tabela_coluna
  WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA')
    AND UPPER(TACO_NM)='TRAL_IN_RED_BASE_CALC');

INSERT
INTO tabela_coluna
  (
    TACO_CD,
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA
  )
SELECT SQ_TACO_CD.nextval,
  (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA'),
  'ENCO_CD_TIPO_APL_RED_BASE_CALC',
  'Tipo Aplicacao Reducao Base Calculo',
  'tipoApliRedBaseCalc',
  0,
  0,
  1,
  CURRENT_TIMESTAMP,
  0,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM tabela_coluna
  WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA')
    AND UPPER(TACO_NM)='ENCO_CD_TIPO_APL_RED_BASE_CALC');

INSERT
INTO tabela_coluna
  (
    TACO_CD,
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA
  )
SELECT SQ_TACO_CD.nextval,
  (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA'),
  'TRAL_VL_RED_BASE_CALC',
  'Valor Reducao Base Calculo',
  'vlRedBaseCalc',
  0,
  0,
  1,
  CURRENT_TIMESTAMP,
  0,
  0
FROM dual
WHERE NOT EXISTS
  (SELECT *
  FROM tabela_coluna
  WHERE tabe_cd = (SELECT TABE_CD FROM TABELA WHERE UPPER(TABE_NM) = 'TRIBUTO_ALIQUOTA')
    AND UPPER(TACO_NM)='TRAL_VL_RED_BASE_CALC');

commit;