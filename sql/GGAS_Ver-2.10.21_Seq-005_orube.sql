UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaTipoContato' WHERE MENU_DS_URL LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tipoContato';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaTipoContato' WHERE RESI_DS LIKE 'exibirPesquisaTabelaAuxiliar.do?acao=exibirPesquisaTabelaAuxiliar%tela=tipoContato';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirInserirTipoContato' WHERE RESI_DS LIKE 'exibirInclusaoTabelaAuxiliar.do?acao=exibirInclusaoTabelaAuxiliar%tela=tipoContato';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirDetalhamentoTipoContato' WHERE RESI_DS LIKE 'exibirDetalhamentoTabelaAuxiliar.do?acao=exibirDetalhamentoTabelaAuxiliar%tela=tipoContato';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirAtualizarTipoContato' WHERE RESI_DS LIKE 'exibirAlteracaoTabelaAuxiliar.do?acao=exibirAlteracaoTabelaAuxiliar%tela=tipoContato';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarTipoContato' WHERE RESI_DS LIKE 'pesquisarTabelaAuxiliar.do?acao=pesquisarTabelaAuxiliar%tela=tipoContato';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'inserirTipoContato' WHERE RESI_DS LIKE 'incluirTabelaAuxiliar.do?acao=incluirTabelaAuxiliar%tela=tipoContato';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'atualizarTipoContato' WHERE RESI_DS LIKE 'alterarTabelaAuxiliar.do?acao=alterarTabelaAuxiliar%tela=tipoContato';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'removerTipoContato' WHERE RESI_DS LIKE 'removerTabelaAuxiliar.do?acao=removerTabelaAuxiliar%tela=tipoContato';

COMMIT;