ALTER TABLE chamado_historico_anexo MODIFY (CHAX_NM VARCHAR2(100));

ALTER TABLE FATURA_ITEM
ADD (
  FAIT_VL_FIXO NUMBER(15,8),
  FAIT_VL_VARIAVEL NUMBER(15,8),
  FAIT_MD_INICIO NUMBER(17,0),
  FAIT_MD_FIM NUMBER(17,0)
);

COMMENT ON COLUMN "FATURA_ITEM"."FAIT_VL_FIXO" IS 'Valor fixo da faixa da tarifa';
COMMENT ON COLUMN "FATURA_ITEM"."FAIT_VL_VARIAVEL" IS 'Valor variável da faixa da tarifa';
COMMENT ON COLUMN "FATURA_ITEM"."FAIT_MD_INICIO" IS 'Volume inicial da faixa da tarifa';
COMMENT ON COLUMN "FATURA_ITEM"."FAIT_MD_FIM" IS 'Volume final da faixa da tarifa';

ALTER TABLE RAMO_ATIVIDADE
ADD SEGM_TIPO_CONSUMO_FATURAMENTO NUMBER(9,2);

COMMENT ON COLUMN "RAMO_ATIVIDADE"."SEGM_TIPO_CONSUMO_FATURAMENTO" IS 'Tipo de visualização da fatura';

UPDATE (
  SELECT r.segm_tipo_consumo_faturamento as old, s.segm_tipo_consumo_faturamento as new
  FROM ramo_atividade r
  INNER JOIN segmento s ON s.segm_cd = r.segm_cd
) t
SET t.old = t.new;

commit;
