ALTER TABLE TI_NOTA_FISCAL_ITEM ADD (TINI_CD_CEST VARCHAR2(7 BYTE));
COMMENT ON COLUMN TI_NOTA_FISCAL_ITEM.TINI_CD_CEST IS 'C�digo Especificador da Substitui��o Tribut�ria.';

COMMIT;