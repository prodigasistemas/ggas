-- insere a constante C_PROPOSTA_SITUACAO_APROVADA
Insert into CONSTANTE_SISTEMA(
        COST_CD,
        TABE_CD,
        MOSI_CD,
        COST_NM,
        COST_DS,
        COST_VL,
        COST_CD_CLASSE,
        COST_NR_VERSAO,
        COST_IN_USO,
        COST_TM_ULTIMA_ALTERACAO
)
select
        SQ_COST_CD.nextval,
        '215',
        '1',
        'C_PROPOSTA_SITUACAO_APROVADA',
        'Indica que a proposta foi aprovada',
        (select PRST_DS from proposta_situacao where PRST_DS like 'APROVADA'),
        NULL,
        '0',
        '1',
        current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where cost_nm like 'C_PROPOSTA_SITUACAO_APROVADA');

-- insere a constante C_PROPOSTA_SITUACAO_REJEITADA
Insert into CONSTANTE_SISTEMA(
        COST_CD,
        TABE_CD,
        MOSI_CD,
        COST_NM,
        COST_DS,
        COST_VL,
        COST_CD_CLASSE,
        COST_NR_VERSAO,
        COST_IN_USO,
        COST_TM_ULTIMA_ALTERACAO
)
select
        SQ_COST_CD.nextval,
        '215',
        '1',
        'C_PROPOSTA_SITUACAO_REJEITADA',
        'Indica que a proposta foi rejeitada',
        (select PRST_DS from proposta_situacao where PRST_DS like 'REJEITADA'),
        NULL,
        '0',
        '1',
        current_timestamp
from dual where not exists
(select * from CONSTANTE_SISTEMA where cost_nm like 'C_PROPOSTA_SITUACAO_REJEITADA');

COMMIT;