insert into NFE_PARAMETRO_SISTEMA (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_NM_CLASSE_ENTIDADE, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, EMPR_CD_EMPRESA)
Select SQ_PARA_CD_PARAMETRO.nextval, 'TIPO_ENVIO_SEFAZ', 'Identifica qual o tipo de envio da sefaz está sendo trabalhado, se é Assincrono ou Sincrono.', '0', '', 3, 1, 1, sysdate, EMPR_CD_EMPRESA
  From NFE_EMPRESA NE Where Not Exists(Select 1 From NFE_PARAMETRO_SISTEMA P Where P.EMPR_CD_EMPRESA = NE.EMPR_CD_EMPRESA And P.PMSI_CD_PARAMETRO = 'TIPO_ENVIO_SEFAZ');

insert into NFE_ENTIDADE_CLASSE (ENCL_CD, ENCL_DS, ENCL_DS_ABREVIADO, ENCL_NR_VERSAO, ENCL_IN_USO, ENCL_TM_ULTIMA_ALTERACAO)
values (83, 'Tipo de Envio para Sefaz', 'TPENVS', 1, 1, sysdate);

insert into NFE_ENTIDADE_CONTEUDO (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO)
values (255, 83, 'Assíncrono', 'Assinc', 1, 1, 1, sysdate, '0');

insert into NFE_ENTIDADE_CONTEUDO (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO)
values (256, 83, 'Síncrono', 'Sincro', 0, 1, 1, sysdate, '1');

insert into NFE_ENTIDADE_CONTEUDO (ENCO_CD, ENCL_CD, ENCO_DS, ENCO_DS_ABREVIADO, ENCO_IN_PADRAO, ENCO_NR_VERSAO, ENCO_IN_USO, ENCO_TM_ULTIMA_ALTERACAO, ENCO_NR_CODIGO)
values (257, 58, 'Versão 3.10', '3.10  ', 0, 1, 1, sysdate, '3.10');


-- UPDATEs NECESSÁRIOS

update constante_sistema set COST_VL = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE upper(ENCO_DS)  like upper('%Operação Tributável (base de cálculo = valor da operação (alíquota normal (cumulativo/não cumulativo))).%')), COST_CD_CLASSE = (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE upper(ENCL_DS) = upper('Situação Tributária PIS/COFINS')) WHERE COST_NM = 'C_ALIQUOTA_NORMAL_PIS_COFINS';
update constante_sistema set COST_VL = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE upper(ENCO_DS) like upper('%Operação Tributável (base de cálculo = valor da operação (alíquota diferenciada))                      %')), COST_CD_CLASSE = (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE upper(ENCL_DS) = upper('Situação Tributária PIS/COFINS')) WHERE COST_NM = 'C_ALIQUOTA_DIFERENCIADA_PIS_COFINS';
update constante_sistema set COST_VL = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE upper(ENCO_DS)  like upper('%Operação Isenta da Contribuição.                                                                        %')), COST_CD_CLASSE = (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE upper(ENCL_DS) = upper('Situação Tributária PIS/COFINS')) WHERE COST_NM = 'C_ALIQUOTA_ISENTO_PIS_COFINS';
update constante_sistema set COST_VL = (SELECT ENCO_CD FROM ENTIDADE_CONTEUDO WHERE upper(ENCO_DS)  like upper('%Operação Sem Incidência da Contribuição.                                                                %')), COST_CD_CLASSE = (SELECT ENCL_CD FROM ENTIDADE_CLASSE WHERE upper(ENCL_DS) = upper('Situação Tributária PIS/COFINS')) WHERE COST_NM = 'C_ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS';

update serie set SERI_IN_CONTINGENCIA_SCAN = 0 where SERI_DS like '%NOTA FISCAL ELETRONICA%';

Insert into nfe_empresa (EMPR_NM_FANTASIA,EMPR_CD_EMPRESA,EMPR_NM_EMPRESA,EMPR_DS_COMPL_EMPRESA,EMPR_NR_INSCR_EMPRESA,EMPR_NR_CNPJ_EMPRESA,EMPR_MM_LOGO,EMPR_IN_IMPRIME_LOGO,EMPR_NR_UF,EMPR_NR_VERSAO,EMPR_IN_USO,EMPR_TM_ULTIMA_ALTERACAO,LTNFS_CD,EMPR_NR_INSC_MUN,EMPR_DS_TEMPLATE_EMAIL) values ('ALGAS','1','ALGAS - GAS DE ALAGOAS SA',' ','242183131','69983484000132', EMPTY_BLOB(),'1','27','3','1',to_timestamp('08/06/15 15:57:18,824000000','DD/MM/RR HH24:MI:SS,FF'),null,null, EMPTY_BLOB());

update nfe_empresa set empr_nm_fantasia = 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO / SEM VALOR FISCAL', empr_nm_empresa = 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO / SEM VALOR FISCAL' where empr_nm_fantasia = 'ALGAS';

update NFE_PARAMETRO_SISTEMA set empr_cd_empresa = 1, pmsi_vl_parametro = 'C:\Procenge\Certificados\CONTAINER_CERTIFICADOS.jks' where pmsi_cd_parametro like '%CAMINHO_CERTIFICADO_ORGAOS%';

update NFE_PARAMETRO_SISTEMA set empr_cd_empresa = 1 where pmsi_cd_parametro like '%SENHA_CERTIFICADO_ORGAOS%';

update NFE_PARAMETRO_SISTEMA set pmsi_vl_parametro = 1, empr_cd_empresa = 1 where pmsi_cd_parametro like '%TIPO_ENVIO_SEFAZ%';

commit;