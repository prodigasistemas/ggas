-- Módulo Relatório --> ( Sub-Menu ) Casdastro 

	-- Clientes por Rotas

INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'GERAR',(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Clientes por Rota%'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Relatórios%'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Clientes por Rota%')) ;
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) values (SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Clientes por Rota%'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Clientes por Rota%')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP) ;
UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRelatorioRota' WHERE MENU_DS_URL LIKE '%exibirPesquisaRelatorioRota.do?acao=exibirPesquisaRelatorioRota%tela=relatorioRota%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaRelatorioRota' WHERE RESI_DS LIKE 'exibirPesquisaRelatorioRota.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirRelatorioRota' WHERE RESI_DS LIKE 'exibirRelatorioRota.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioRota', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Clientes por Rota%')) WHERE resi_ds LIKE 'gerarRelatorioRota.do';

	-- Número de Clientes e Número de Unidades Consumidoras

INSERT INTO OPERACAO_SISTEMA(OPSI_CD,OPSI_DS, MENU_CD,MOSI_CD,OPSI_IN_AUDITAVEL,OPSI_IN_EXIBIR,OPSI_IN_USO,OPSI_NR_TIPO,OPSI_NR_VERSAO,OPSI_TM_ULTIMA_ALTERACAO) SELECT SQ_OPSI_CD.nextval,'GERAR',(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Número de Clientes e Número de Unidades Consumidoras%'), (SELECT mosi_cd FROM modulo_sistema WHERE mosi_ds LIKE '%Relatórios%'),0,1,1,0,0,CURRENT_TIMESTAMP from dual where not exists (select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Número de Clientes e Número de Unidades Consumidoras%')) ;
INSERT INTO PERMISSAO_SISTEMA (PESI_CD,MENU_CD,OPSI_CD,PESI_NM_TIPO,PESI_NR_VERSAO,PASI_CD,PESI_IN_USO,PESI_TM_ULTIMA_ALTERACAO) values (SQ_PESI_CD.nextval,(SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Número de Clientes e Número de Unidades Consumidoras%'),(select opsi_cd from operacao_sistema WHERE opsi_ds LIKE 'GERAR' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Número de Clientes e Número de Unidades Consumidoras%')),'0','0',(SELECT pasi_cd FROM papel_sistema WHERE UPPER( pasi_ds ) LIKE 'ADMINISTRADOR'),'1', CURRENT_TIMESTAMP) ;
UPDATE MENU SET MENU_DS_URL = 'exibirRelatorioClienteUnidadeConsumidora' WHERE MENU_DS_URL LIKE '%exibirRelatorioClienteUnidadeConsumidora.do?acao=exibirRelatorioClienteUnidadeConsumidora%tela=relatorioClienteUnidadeConsumidora%';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirRelatorioClienteUnidadeConsumidora' WHERE RESI_DS LIKE 'exibirRelatorioClienteUnidadeConsumidora.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioClienteUnidadeConsumidora' WHERE RESI_DS LIKE 'gerarRelatorioClienteUnidadeConsumidora.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioClienteHistorico', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Número de Clientes e Número de Unidades Consumidoras%')) WHERE resi_ds LIKE 'gerarRelatorioClienteHistorico.do';

	-- Histórico do Cliente

UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaRelatorioHistoricoCliente' WHERE MENU_DS_URL LIKE 'exibirPesquisaRelatorioHistoricoCliente.do?acao=exibirPesquisaRelatorioHistoricoCliente';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'exibirPesquisaRelatorioHistoricoCliente' WHERE RESI_DS LIKE 'exibirPesquisaRelatorioHistoricoCliente.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioHistoricoCliente', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%GERAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Histórico do Cliente%')) WHERE resi_ds LIKE 'gerarRelatorioHistoricoCliente.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarPontoConsumoHistoricoCliente', opsi_cd = (SELECT opsi_cd FROM operacao_sistema WHERE opsi_ds LIKE '%PESQUISAR%' AND menu_cd = (SELECT menu_cd FROM menu WHERE menu_ds LIKE '%Histórico do Cliente%')) WHERE resi_ds LIKE 'pesquisarPontoConsumoHistoricoCliente.do';

COMMIT;
