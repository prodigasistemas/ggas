Insert into ENTIDADE_CLASSE (ENCL_CD,ENCL_DS,ENCL_DS_ABREVIADO,ENCL_NR_VERSAO,ENCL_IN_USO,ENCL_TM_ULTIMA_ALTERACAO) 
select SQ_ENCL_CD.nextval,'Corrigi PT',null,'1','1',current_timestamp from dual where not exists (select * from entidade_classe where encl_ds like 'Corrigi PT');

Insert into ENTIDADE_CONTEUDO (ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval,(select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'),'Sim (Coletor)',null,'0','1','1',current_timestamp,'0' 
from dual where not exists (select * from entidade_conteudo where enco_ds = 'Sim (Coletor)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'));

Insert into ENTIDADE_CONTEUDO (ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval,(select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'),'Sim (Contrato)',null,'0','1','1',current_timestamp,'0' 
from dual where not exists (select * from entidade_conteudo where enco_ds = 'Sim (Contrato)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'));

Insert into ENTIDADE_CONTEUDO (ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval,(select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'),'Nao',null,'0','1','1',current_timestamp,'0' 
from dual where not exists (select * from entidade_conteudo where enco_ds = 'Nao' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'));



Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CLASSE_CORRIGI_PT','Indica que corrigi PT', 
(select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'),
null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CLASSE_CORRIGI_PT');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_COLETOR','Indica que corrigi PT com dados do coletor', 
(select enco_cd from entidade_conteudo where enco_ds = 'Sim (Coletor)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT')),
(select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'),'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_COLETOR');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_CONTRATO','Indica que corrigi PT com dados do contrato', 
(select enco_cd from entidade_conteudo where enco_ds = 'Sim (Contrato)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT')),
(select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'),'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_CONTRATO');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO','Indica que nao corrigi PT', 
(select enco_cd from entidade_conteudo where enco_ds = 'Nao' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT')),
(select encl_cd from entidade_classe where encl_ds like 'Corrigi PT'),'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO');



Insert into ENTIDADE_CLASSE (ENCL_CD,ENCL_DS,ENCL_DS_ABREVIADO,ENCL_NR_VERSAO,ENCL_IN_USO,ENCL_TM_ULTIMA_ALTERACAO) 
select SQ_ENCL_CD.nextval,'Corrigi Z',null,'1','1',current_timestamp from dual where not exists (select * from entidade_classe where encl_ds like 'Corrigi Z');

Insert into ENTIDADE_CONTEUDO (ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval,(select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'),'Sim (Valor)',null,'0','1','1',current_timestamp,'0' 
from dual where not exists (select * from entidade_conteudo where enco_ds = 'Sim (Valor)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'));

Insert into ENTIDADE_CONTEUDO (ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval,(select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'),'Sim (Cromatografia)',null,'0','1','1',current_timestamp,'0' 
from dual where not exists (select * from entidade_conteudo where enco_ds = 'Sim (Cromatografia)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'));

Insert into ENTIDADE_CONTEUDO (ENCO_CD,ENCL_CD,ENCO_DS,ENCO_DS_ABREVIADO,ENCO_IN_PADRAO,ENCO_NR_VERSAO,ENCO_IN_USO,ENCO_TM_ULTIMA_ALTERACAO,ENCO_NR_CODIGO) 
select SQ_ENCO_CD.nextval,(select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'),'Nao',null,'0','1','1',current_timestamp,'0' 
from dual where not exists (select * from entidade_conteudo where enco_ds = 'Nao' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'));

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CLASSE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CLASSE_CORRIGI_Z','Indica que corrigi Z', 
(select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'),
null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CLASSE_CORRIGI_Z');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_VALOR','Indica que corrigi Z com valor', 
(select enco_cd from entidade_conteudo where enco_ds = 'Sim (Valor)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z')),
(select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'),'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_VALOR');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_CROMATOGRAFIA','Indica que corrigi Z com cromatografia', 
(select enco_cd from entidade_conteudo where enco_ds = 'Sim (Cromatografia)' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z')),
(select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'),'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_CROMATOGRAFIA');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'ENTIDADE_CONTEUDO'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ENTIDADE_CONTEUDO_CORRIGI_Z_NAO','Indica que nao corrigi Z', 
(select enco_cd from entidade_conteudo where enco_ds = 'Nao' and encl_cd in (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z')),
(select encl_cd from entidade_classe where encl_ds like 'Corrigi Z'),'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ENTIDADE_CONTEUDO_CORRIGI_Z_NAO');


--

alter table pressao_fornecimento_faixa ADD ENCO_CD_IN_CORRECAO_PT NUMBER(9) default 1 NOT NULL;
alter table pressao_fornecimento_faixa ADD CONSTRAINT FK_PRESSAO_IN_CORRECAO_PT FOREIGN KEY (ENCO_CD_IN_CORRECAO_PT) REFERENCES entidade_conteudo( ENCO_CD );
COMMENT ON COLUMN pressao_fornecimento_faixa.ENCO_CD_IN_CORRECAO_PT IS 'Indicador se corrigi PT (Sim (Coletor);Sim (Contrato);Nao)';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'PRESSAO_FORNECIMENTO_FAIXA'),
      'ENCO_CD_IN_CORRECAO_PT',
      'Indicador se corrigi PT (Sim (Coletor);Sim (Contrato);Nao)',
      'indicadorCorrecaoPT',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'PRESSAO_FORNECIMENTO_FAIXA') and TACO_NM = 'ENCO_CD_IN_CORRECAO_PT');
			
alter table pressao_fornecimento_faixa add ENCO_CD_IN_CORRECAO_Z NUMBER(9) default 1 NOT NULL;
alter table pressao_fornecimento_faixa ADD CONSTRAINT FK_PRESSAO_IN_CORRECAO_Z FOREIGN KEY (ENCO_CD_IN_CORRECAO_Z) REFERENCES entidade_conteudo( ENCO_CD );
COMMENT ON COLUMN pressao_fornecimento_faixa.ENCO_CD_IN_CORRECAO_Z IS 'Indicador se corrigi Z (Sim (Valor);Sim (Cromatografia);Nao)';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'PRESSAO_FORNECIMENTO_FAIXA'),
      'ENCO_CD_IN_CORRECAO_Z',
      'Indicador se corrigi Z (Sim (Valor);Sim (Cromatografia);Nao)',
      'indicadorCorrecaoZ',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'PRESSAO_FORNECIMENTO_FAIXA') and TACO_NM = 'ENCO_CD_IN_CORRECAO_Z');

update pressao_fornecimento_faixa set 
enco_cd_in_correcao_pt = (select enco_cd from entidade_conteudo where encl_cd = (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT') and enco_ds like 'Sim (Contrato)'),
enco_cd_in_correcao_z = (select enco_cd from entidade_conteudo where encl_cd = (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z') and enco_ds like 'Sim (Valor)')
where prff_in_correcao_ptz = 1;

update pressao_fornecimento_faixa set 
enco_cd_in_correcao_pt = (select enco_cd from entidade_conteudo where encl_cd = (select encl_cd from entidade_classe where encl_ds like 'Corrigi PT') and enco_ds like 'Nao'),
enco_cd_in_correcao_z = (select enco_cd from entidade_conteudo where encl_cd = (select encl_cd from entidade_classe where encl_ds like 'Corrigi Z') and enco_ds like 'Nao')
where prff_in_correcao_ptz = 0;


alter table pressao_fornecimento_faixa drop column prff_in_correcao_ptz;
delete from tabela_coluna where taco_nm = 'PRFF_IN_CORRECAO_PTZ';

delete from unidade_conversao where unid_cd_de = 19 and unid_cd_para = 16 and unid_ds_formula = 'return parametro * 0.97';


Insert into PRESSAO_FORNECIMENTO_FAIXA (PRFF_CD,PRFF_MD_MINIMO,PRFF_MD_MAXIMO,PRFF_NR_VERSAO,PRFF_NR_FATOR_Z,PRFF_IN_USO,PRFF_TM_ULTIMA_ALTERACAO,PRFF_NR_FATOR_CORRECAO_PTZ_PCS,
UNID_CD_PRESSAO,SEGM_CD,ENCO_CD_CLASSE_PRESSAO,ENCO_CD_IN_CORRECAO_PT,ENCO_CD_IN_CORRECAO_Z) 
select SQ_PRFF_CD.nextval,'0','99999','0',null,'1',current_timestamp,null,
(select unid_cd from unidade where upper(unid_ds) like upper('Atmosferas')),
(select segm_cd from segmento where upper(segm_ds) in ('INDUSTRIAL')),
(select enco_cd from entidade_conteudo where enco_ds like 'ALTA' and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) like 'CLASSE DA PRESS%FORNECIMENTO')),
(select enco_cd from entidade_conteudo where upper(enco_ds) like 'NAO' and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) in ('CORRIGI PT'))),
(select enco_cd from entidade_conteudo where upper(enco_ds) like 'NAO' and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) in ('CORRIGI Z')))
from dual where not exists (select * from pressao_fornecimento_faixa where 
unid_cd_pressao in (select unid_cd from unidade where upper(unid_ds) like upper('Atmosferas'))
and segm_cd in (select segm_cd from segmento where upper(segm_ds) in ('INDUSTRIAL')));

Insert into PRESSAO_FORNECIMENTO_FAIXA (PRFF_CD,PRFF_MD_MINIMO,PRFF_MD_MAXIMO,PRFF_NR_VERSAO,PRFF_NR_FATOR_Z,PRFF_IN_USO,PRFF_TM_ULTIMA_ALTERACAO,PRFF_NR_FATOR_CORRECAO_PTZ_PCS,
UNID_CD_PRESSAO,SEGM_CD,ENCO_CD_CLASSE_PRESSAO,ENCO_CD_IN_CORRECAO_PT,ENCO_CD_IN_CORRECAO_Z) 
select SQ_PRFF_CD.nextval,'0','99999','0',null,'1',current_timestamp,null,
(select unid_cd from unidade where upper(unid_ds) like upper('Atmosferas')),
(select segm_cd from segmento where upper(segm_ds) in ('VEICULAR')),
(select enco_cd from entidade_conteudo where enco_ds like 'ALTA' and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) like 'CLASSE DA PRESS%FORNECIMENTO')),
(select enco_cd from entidade_conteudo where upper(enco_ds) like 'NAO' and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) in ('CORRIGI PT'))),
(select enco_cd from entidade_conteudo where upper(enco_ds) like 'NAO' and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) in ('CORRIGI Z')))
from dual where not exists (select * from pressao_fornecimento_faixa where 
unid_cd_pressao in (select unid_cd from unidade where upper(unid_ds) like upper('Atmosferas'))
and segm_cd in (select segm_cd from segmento where upper(segm_ds) in ('VEICULAR')));




alter table LEITURA_MOVIMENTO ADD LEMO_MD_PRESSAO_INFORMADA NUMBER(17,8);
COMMENT ON COLUMN LEITURA_MOVIMENTO.LEMO_MD_PRESSAO_INFORMADA IS 'Pressao informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'LEITURA_MOVIMENTO'),
      'LEMO_MD_PRESSAO_INFORMADA',
      'Pressao informada',
      'pressaoInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'LEITURA_MOVIMENTO') and TACO_NM = 'LEMO_MD_PRESSAO_INFORMADA');
			
alter table LEITURA_MOVIMENTO ADD LEMO_MD_TEMPERATURA_INFORMADA NUMBER(17,8);
COMMENT ON COLUMN LEITURA_MOVIMENTO.LEMO_MD_TEMPERATURA_INFORMADA IS 'Temperatura informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'LEITURA_MOVIMENTO'),
      'LEMO_MD_TEMPERATURA_INFORMADA',
      'Temperatura informada',
      'temperaturaInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'LEITURA_MOVIMENTO') and TACO_NM = 'LEMO_MD_TEMPERATURA_INFORMADA');
			
alter table LEITURA_MOVIMENTO ADD UNID_CD_PRESSAO_INFORMADA  NUMBER(9);
alter table LEITURA_MOVIMENTO ADD CONSTRAINT FK_LEITURA_UNIDADE_PRESSAO FOREIGN KEY (UNID_CD_PRESSAO_INFORMADA) REFERENCES UNIDADE( UNID_CD );
COMMENT ON COLUMN LEITURA_MOVIMENTO.UNID_CD_PRESSAO_INFORMADA IS 'Unidade da pressao informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'LEITURA_MOVIMENTO'),
      'UNID_CD_PRESSAO_INFORMADA',
      'Unidade da pressao informada',
      'unidadePressaoInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'LEITURA_MOVIMENTO') and TACO_NM = 'UNID_CD_PRESSAO_INFORMADA');

			
alter table LEITURA_MOVIMENTO ADD UNID_CD_TEMPERATURA_INFORMADA  NUMBER(9);
alter table LEITURA_MOVIMENTO ADD CONSTRAINT FK_LEITURA_UNIDADE_TEMPERATURA FOREIGN KEY (UNID_CD_TEMPERATURA_INFORMADA) REFERENCES UNIDADE( UNID_CD );
COMMENT ON COLUMN LEITURA_MOVIMENTO.UNID_CD_TEMPERATURA_INFORMADA IS 'Unidade da temperatura informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'LEITURA_MOVIMENTO'),
      'UNID_CD_TEMPERATURA_INFORMADA',
      'Unidade da temperatura informada',
      'unidadeTemperaturaInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'LEITURA_MOVIMENTO') and TACO_NM = 'UNID_CD_TEMPERATURA_INFORMADA');
			
			
-- -------------------------------------------
alter table MEDICAO_HISTORICO ADD MEHI_MD_PRESSAO_INFORMADA NUMBER(17,8);
COMMENT ON COLUMN MEDICAO_HISTORICO.MEHI_MD_PRESSAO_INFORMADA IS 'Pressao informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'MEDICAO_HISTORICO'),
      'MEHI_MD_PRESSAO_INFORMADA',
      'Pressao informada',
      'pressaoInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'MEDICAO_HISTORICO') and TACO_NM = 'MEHI_MD_PRESSAO_INFORMADA');
			
alter table MEDICAO_HISTORICO ADD MEHI_MD_TEMPERATURA_INFORMADA NUMBER(17,8);
COMMENT ON COLUMN MEDICAO_HISTORICO.MEHI_MD_TEMPERATURA_INFORMADA IS 'Temperatura informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'MEDICAO_HISTORICO'),
      'MEHI_MD_TEMPERATURA_INFORMADA',
      'Temperatura informada',
      'temperaturaInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'MEDICAO_HISTORICO') and TACO_NM = 'MEHI_MD_TEMPERATURA_INFORMADA');
			
alter table MEDICAO_HISTORICO ADD UNID_CD_PRESSAO_INFORMADA  NUMBER(9);
alter table MEDICAO_HISTORICO ADD CONSTRAINT FK_MEDICAO_UNIDADE_PRESSAO FOREIGN KEY (UNID_CD_PRESSAO_INFORMADA) REFERENCES UNIDADE( UNID_CD );
COMMENT ON COLUMN MEDICAO_HISTORICO.UNID_CD_PRESSAO_INFORMADA IS 'Unidade da pressao informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'MEDICAO_HISTORICO'),
      'UNID_CD_PRESSAO_INFORMADA',
      'Unidade da pressao informada',
      'unidadePressaoInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'MEDICAO_HISTORICO') and TACO_NM = 'UNID_CD_PRESSAO_INFORMADA');

			
alter table MEDICAO_HISTORICO ADD UNID_CD_TEMPERATURA_INFORMADA  NUMBER(9);
alter table MEDICAO_HISTORICO ADD CONSTRAINT FK_MEDICAO_UNIDADE_TEMPERATURA FOREIGN KEY (UNID_CD_TEMPERATURA_INFORMADA) REFERENCES UNIDADE( UNID_CD );
COMMENT ON COLUMN MEDICAO_HISTORICO.UNID_CD_TEMPERATURA_INFORMADA IS 'Unidade da temperatura informada';
Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'MEDICAO_HISTORICO'),
      'UNID_CD_TEMPERATURA_INFORMADA',
      'Unidade da temperatura informada',
      'unidadeTemperaturaInformada',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'MEDICAO_HISTORICO') and TACO_NM = 'UNID_CD_TEMPERATURA_INFORMADA');

			
Insert into CONSUMO_ANORMALIDADE (COAN_CD,COAN_DS,COAN_DS_ABREVIADO,COAN_IN_BLOQUEIA_FATURAMENTO,COAN_NR_VERSAO,COAN_IN_USO,COAN_NR_VEZES_BLOQUEIO,
COAN_TM_ULTIMA_ALTERACAO,COAN_NR_OCOR_ANORMALIDADE) 
select SQ_COAN_CD.nextval,'PRESSAO NAO INF. MANUAL','PRNIM','1','0','1',null,current_timestamp,null from dual
where not exists (select * from CONSUMO_ANORMALIDADE where COAN_DS like 'PRESSAO NAO INF. MANUAL');

Insert into CONSUMO_ANORMALIDADE (COAN_CD,COAN_DS,COAN_DS_ABREVIADO,COAN_IN_BLOQUEIA_FATURAMENTO,COAN_NR_VERSAO,COAN_IN_USO,COAN_NR_VEZES_BLOQUEIO,
COAN_TM_ULTIMA_ALTERACAO,COAN_NR_OCOR_ANORMALIDADE) 
select SQ_COAN_CD.nextval,'TEMPERAT. NAO INF. MANUAL','TENIM','1','0','1',null,current_timestamp,null from dual
where not exists (select * from CONSUMO_ANORMALIDADE where COAN_DS like 'TEMPERAT. NAO INF. MANUAL');

Insert into CONSUMO_ANORMALIDADE (COAN_CD,COAN_DS,COAN_DS_ABREVIADO,COAN_IN_BLOQUEIA_FATURAMENTO,COAN_NR_VERSAO,COAN_IN_USO,COAN_NR_VEZES_BLOQUEIO,
COAN_TM_ULTIMA_ALTERACAO,COAN_NR_OCOR_ANORMALIDADE) 
select SQ_COAN_CD.nextval,'UN. PRESSAO NAO INF. MANUAL','UPNIM','1','0','1',null,current_timestamp,null from dual
where not exists (select * from CONSUMO_ANORMALIDADE where COAN_DS like 'UN. PRESSAO NAO INF. MANUAL');

Insert into CONSUMO_ANORMALIDADE (COAN_CD,COAN_DS,COAN_DS_ABREVIADO,COAN_IN_BLOQUEIA_FATURAMENTO,COAN_NR_VERSAO,COAN_IN_USO,COAN_NR_VEZES_BLOQUEIO,
COAN_TM_ULTIMA_ALTERACAO,COAN_NR_OCOR_ANORMALIDADE) 
select SQ_COAN_CD.nextval,'UN. TEMPERAT. NAO INF. MANUAL','UTNIM','1','0','1',null,current_timestamp,null from dual
where not exists (select * from CONSUMO_ANORMALIDADE where COAN_DS like 'UN. TEMPERAT. NAO INF. MANUAL');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'CONSUMO_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ANORMALIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE','Codigo da anormalidade de consumo pressao nao informada manualmente', 
(select COAN_CD from CONSUMO_ANORMALIDADE where COAN_DS like 'PRESSAO NAO INF. MANUAL'),
null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ANORMALIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'CONSUMO_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ANORMALIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE','Codigo da anormalidade de consumo temperatura nao informada manualmente', 
(select COAN_CD from CONSUMO_ANORMALIDADE where COAN_DS like 'TEMPERAT. NAO INF. MANUAL'),
null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ANORMALIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'CONSUMO_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ANORMALIDADE_UNIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE','Codigo da anormalidade de consumo unidade pressao nao informada manualmente', 
(select COAN_CD from CONSUMO_ANORMALIDADE where COAN_DS like 'UN. PRESSAO NAO INF. MANUAL'),
null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ANORMALIDADE_UNIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE');

Insert into CONSTANTE_SISTEMA (COST_CD,TABE_CD,MOSI_CD,COST_NM,COST_DS,COST_VL,COST_CD_CLASSE,COST_NR_VERSAO,COST_IN_USO,COST_TM_ULTIMA_ALTERACAO) 
select SQ_COST_CD.nextval, (select tabe_cd from tabela where tabe_nm like 'CONSUMO_ANORMALIDADE'),(select mosi_cd from modulo_sistema where upper(mosi_ds) like  upper('Medi%o%')),
'C_ANORMALIDADE_UNIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE','Codigo da anormalidade de consumo unidade temperatura nao informada manualmente', 
(select COAN_CD from CONSUMO_ANORMALIDADE where COAN_DS like 'UN. TEMPERAT. NAO INF. MANUAL'),
null,'1','1',current_timestamp from dual
where not exists (select * from constante_sistema where cost_nm like 'C_ANORMALIDADE_UNIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE');


commit;