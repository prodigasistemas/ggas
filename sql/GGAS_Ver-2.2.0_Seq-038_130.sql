
UPDATE constante_sistema
SET cost_cd_classe=
  (SELECT encl_cd
  FROM entidade_classe
  WHERE encl_ds='Dias Possiveis Vencimento'
  )
WHERE cost_cd =
  (SELECT cost_cd
  FROM constante_sistema
  WHERE cost_nm='C_DIAS_POSSIVEIS_VENCIMENTO'
  ); 
  
  
UPDATE constante_sistema
SET cost_cd_classe=
  (SELECT encl_cd
  FROM entidade_classe
  WHERE encl_ds='Motivos encerramento Chamado'
  )
WHERE cost_cd =
  (SELECT cost_cd
  FROM constante_sistema
  WHERE cost_nm='C_MOTIVO_ENCERRAMENTO_CHAMADO'
  ); 

insert into constante_sistema select sq_cost_cd.nextval, (select TABE_CD from tabela where tabe_nm in ('ENTIDADE_CONTEUDO')), 
  (SELECT MOSI_cD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) = UPPER('CONTRATO')), 'C_VENCIMENTO_APOS_LEITURA', 'Indica a opcao para calcular a data de vencimento apos a leitura', 
  (select enco_cd from entidade_conteudo where upper(enco_ds) like upper('LEITURA%') 
  and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) = upper('Fase referencial vencimento')))
  , NULL, 0, 1,CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM = 'C_VENCIMENTO_APOS_LEITURA');
  
  
insert into constante_sistema select sq_cost_cd.nextval, (select TABE_CD from tabela where tabe_nm in ('ENTIDADE_CONTEUDO')), 
  (SELECT MOSI_cD FROM MODULO_SISTEMA WHERE UPPER(MOSI_DS) = UPPER('CONTRATO')), 'C_VENCIMENTO_APOS_FATURAMENTO', 'Indica a opcao para calcular a data de vencimento apos o faturamento', 
  (select enco_cd from entidade_conteudo where upper(enco_ds) like upper('FATURAMENTO%') 
  and encl_cd in (select encl_cd from entidade_classe where upper(encl_ds) = upper('Fase referencial vencimento')))
  , NULL, 0, 1,CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT * FROM CONSTANTE_SISTEMA WHERE COST_NM = 'C_VENCIMENTO_APOS_FATURAMENTO');
  
  
ALTER TABLE CONSUMO_HISTORICO ADD COHI_IN_DRAWBACK NUMBER(1) default 0;
COMMENT ON COLUMN CONSUMO_HISTORICO.COHI_IN_DRAWBACK IS 'Indicador de consumo com drawback';
Insert into TABELA_COLUNA (TACO_CD,TABE_CD,TACO_NM,TACO_DS,TACO_NM_PROPRIEDADE,TACO_IN_CONSULTA_DINAMICA,TACO_NR_VERSAO,TACO_IN_USO,TACO_TM_ULTIMA_ALTERACAO,TACO_IN_AUDITAVEL,TACO_IN_ALCADA,TACO_IN_VARIAVEL,TACO_CD_VARIAVEL) 
select SQ_TACO_CD.nextval,(select tabe_cd from tabela where TABE_NM = 'CONSUMO_HISTORICO'),'COHI_IN_DRAWBACK' , 
'Indicador de consumo com drawback ','indicadorDrawback',0,0,1,current_timestamp,0,0,0,null from dual where not exists 
(select * from TABELA_COLUNA where TABE_CD = (SELECT TABE_CD from TABELA WHERE TABE_NM = 'CONSUMO_HISTORICO') and TACO_NM = 'COHI_IN_DRAWBACK');

commit;