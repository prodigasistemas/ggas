ALTER TABLE CONTRATO_MODELO 
  ADD COME_CD_VERSAO_ANTERIOR NUMBER (9);

COMMENT ON COLUMN CONTRATO_MODELO.COME_CD_VERSAO_ANTERIOR
  IS 'Chave primaria da tabela CONTRATO_MODELO para refereciar a versão anterior do contrato modelo';

ALTER TABLE CONTRATO_MODELO 
  ADD CONSTRAINT FK_COME_COME_VERSAO_ANTERIOR 
  FOREIGN KEY ( COME_CD_VERSAO_ANTERIOR ) 
  REFERENCES CONTRATO_MODELO ( COME_CD );

Insert into TABELA_COLUNA 
  (TACO_CD, 
    TABE_CD,
    TACO_NM,
    TACO_DS,
    TACO_NM_PROPRIEDADE,
    TACO_IN_CONSULTA_DINAMICA,
    TACO_NR_VERSAO,
    TACO_IN_USO,
    TACO_TM_ULTIMA_ALTERACAO,
    TACO_IN_AUDITAVEL,
    TACO_IN_ALCADA,
    TACO_IN_VARIAVEL,
    TACO_CD_VARIAVEL) 
  select SQ_TACO_CD.nextval,
    (select tabe_cd from tabela where TABE_NM = 'CONTRATO_MODELO'),
      'COME_CD_VERSAO_ANTERIOR',
      'Chave primaria da tabela CONTRATO_MODELO para refereciar a versão anterior do contrato modelo',
      'modeloContratoVersaoAnterior',
      0,
      0,
      1,
      current_timestamp,
      0,
      0,
      0,
      null from dual where not exists 
        (select * from TABELA_COLUNA where TABE_CD = 
          (SELECT TABE_CD from TABELA WHERE TABE_NM = 
            'CONTRATO_MODELO') and TACO_NM = 'COME_CD_VERSAO_ANTERIOR');
			
			commit;