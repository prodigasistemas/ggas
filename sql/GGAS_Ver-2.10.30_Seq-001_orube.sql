UPDATE MENU SET MENU_DS_URL = 'exibirPesquisaExcecaoLeituraFaturamento' WHERE MENU_DS_URL LIKE 'exibirPesquisaExcecaoLeituraFaturamento.do?acao=exibirPesquisaExcecaoLeituraFaturamento%tela=excecaoLeituraFaturamento';

UPDATE RECURSO_SISTEMA SET RESI_DS = 'incluirFaturaAnomalia' WHERE RESI_DS LIKE 'incluirFaturaAnomalia.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'alterarDadosAnomaliaFaturamento' WHERE RESI_DS LIKE 'alterarDadosAnomaliaFaturamento.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'gerarRelatorioAnaliseFatura' WHERE RESI_DS LIKE 'gerarRelatorioAnaliseFatura.do';
UPDATE RECURSO_SISTEMA SET RESI_DS = 'pesquisarExcecaoLeituraFatura' WHERE RESI_DS LIKE 'pesquisarExcecaoLeituraFatura.do';

COMMIT;