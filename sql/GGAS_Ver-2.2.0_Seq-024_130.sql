-- Ajuste para código de movimento da caixa
update documento_layout_campo set DOLC_NR_POSICAO_FINAL = 150, DOLC_NR_POSICAO_INICIAL = 150 where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador where banc_cd = 
(select banc_cd from banco where banc_nr=237)) and DOLA_DS = 'HEADER - DEBITO AUTOMATICO' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Bradesco' and DOLA_METODO_PROCESSAMENTO = 'processarCabecalho') 
and dolc_ds_campo = 'codigoMovimento';

commit;

-- Ajuste para versão do leiaute no cabeçalho do bradesco
update documento_layout_metodo_arg set DOLM_NR_ORDEM = 6 where dolc_cd  = (select dolc_cd from documento_layout_campo where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador 
where banc_cd = (select banc_cd from banco where banc_nr=237)) and DOLA_DS = 'HEADER - DEBITO AUTOMATICO' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Bradesco' and 
DOLA_METODO_PROCESSAMENTO = 'processarCabecalho') and dolc_ds_campo = 'versaoLeiaute');

-- Ajuste para código de remassa no cabeçalho do bradesco
update documento_layout_metodo_arg set DOLM_NR_ORDEM = 5 where dolc_cd  = (select dolc_cd from documento_layout_campo where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador 
where banc_cd = (select banc_cd from banco where banc_nr=237)) and DOLA_DS = 'HEADER - DEBITO AUTOMATICO' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Bradesco' and 
DOLA_METODO_PROCESSAMENTO = 'processarCabecalho') and dolc_ds_campo = 'codigoRemessa');

commit;

-- Ajuste para inclusao de argumento de total de registro para o registro de trilha do layout do bradesco
insert into documento_layout_metodo_arg select sq_dolm_cd.nextval, 
(select dolc_cd from documento_layout_campo where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador 
where banc_cd = 
(select banc_cd from banco where banc_nr=237)) and DOLA_DS = 'TRAILLER' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Bradesco' and 
DOLA_METODO_PROCESSAMENTO = 'processarTrilha') and dolc_ds_campo = 'totalRegistrosArquivo'), 
(select dola_cd from documento_layout where ARRE_CD = (select arre_cd from arrecadador where banc_cd = 
(select banc_cd from banco where banc_nr=237)) and DOLA_DS = 'TRAILLER' and DOLA_NR_TAMANHO_REGISTRO = 150 
and DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Bradesco' 
and DOLA_METODO_PROCESSAMENTO = 'processarTrilha'), 1, 1, 1, current_timestamp from dual;

-- Ajuste para inclusao de argumento de total de registro para o registro de trilha do layout do bradesco
insert into documento_layout_metodo_arg select sq_dolm_cd.nextval, 
(select dolc_cd from documento_layout_campo where dola_cd = 
(select dola_cd from documento_layout where ENCO_CD_FORMA_ARRECADACAO = 77 and ARRE_CD = (select arre_cd from arrecadador 
where banc_cd = 
(select banc_cd from banco where banc_nr=237)) and DOLA_DS = 'TRAILLER' and DOLA_NR_TAMANHO_REGISTRO = 150 and 
DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Bradesco' and 
DOLA_METODO_PROCESSAMENTO = 'processarTrilha') and dolc_ds_campo = 'valorTotalRegistrosArquivo'), 
(select dola_cd from documento_layout where ARRE_CD = (select arre_cd from arrecadador where banc_cd = 
(select banc_cd from banco where banc_nr=237)) and DOLA_DS = 'TRAILLER' and DOLA_NR_TAMANHO_REGISTRO = 150 
and DOLA_CLASSE_CONVERSOR_CAMPO = 'br.com.ggas.arrecadacao.remessa.conversor.DACNAB150Bradesco' 
and DOLA_METODO_PROCESSAMENTO = 'processarTrilha'), 2, 1, 1, current_timestamp from dual;

commit;

