insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD) 
values (SQ_PMSI_CD.nextval,'SERVIDOR_AUTENTICACAO_AD_HOST', 'Endere�o do servidor de autentica��o AD', 'Host', 1 , 0, 1, localtimestamp ,4);

insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD) 
values (SQ_PMSI_CD.nextval,'SERVIDOR_AUTENTICACAO_AD_PORTA', 'Porta do servidor de autentica��o AD', 'Porta', 1 , 0, 1, localtimestamp ,4);

insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD) 
values (SQ_PMSI_CD.nextval,'SERVIDOR_AUTENTICACAO_AD_DOMINIO', 'Dom�nio do servidor de autentica��o AD', 'Dom�nio', 1 , 0, 1, localtimestamp ,4);

insert into parametro_sistema (PMSI_CD, PMSI_CD_PARAMETRO, PMSI_DS_PARAMETRO, PMSI_VL_PARAMETRO, PMSI_CD_TIPO_PARAMETRO, PMSI_NR_VERSAO, PMSI_IN_USO, PMSI_TM_ULTIMA_ALTERACAO, MOSI_CD) 
values (SQ_PMSI_CD.nextval,'AUTENTICACAO_POR_AD_WINDOWS', 'Indica se a autentica��o pelo AD do Windows estar� habilidade', 2, 1 , 0, 1, localtimestamp ,4);

ALTER TABLE USUARIO_SISTEMA ADD CONSTRAINT usuario_dominio_unique UNIQUE (USSI_NM_USUARIO_DOMINIO);

commit;