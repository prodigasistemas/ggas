Insert into SUPER_MEDICAO_ANORMALIDADE values (28,'MEDIÇÃO COM CONSUMO NEGATIVO.','1','1','0',current_timestamp,'1');

Insert into SUPER_MEDICAO_ANORMALIDADE values (29,'VIRADA DE MEDIDOR.','1','1','0',current_timestamp,'0');

INSERT INTO PARAMETRO_SISTEMA (
  PMSI_CD,
  PMSI_CD_PARAMETRO,
  PMSI_DS_PARAMETRO,
  PMSI_VL_PARAMETRO,
  PMSI_DS_COMPLEMENTO,
  PMSI_CD_TIPO_PARAMETRO,
  PMSI_NR_VERSAO,
  PMSI_IN_USO,
  PMSI_TM_ULTIMA_ALTERACAO,
  TABE_CD,
  MOSI_CD
)
VALUES (
  sq_pmsi_cd.NEXTVAL,
  'DESATIVAR_PONTO_CONTRATO',
  'Indica se o ponto será desativado ao encerrar contrato',
  0,
  null,
  1,
  0,
  1,
  current_timestamp,
  null,
  3
);

COMMIT;