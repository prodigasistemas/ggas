<#-- Template de Emails para abertura de chamados em lote -->
<#setting number_format="computer">

<html>

<p>
    <strong>Chamados abertos em lote - ${nomeImovel}</strong>
</p>

<p>
    <strong>Data de Abertura: </strong> ${dataAbertura}
</p>

<p>
    <strong>Data de Previsão de Encerramento: </strong> ${dataPrevisaoEncerramento}
</p>

<p>
    <strong>Unidade Organizacional Responsável: </strong> ${unidadeOrganizacional}
</p>

<p>
    <strong>Status: </strong> ${status}
</p>

<p>
    <strong>Tipo - Assunto: </strong> ${tipoChamado} - ${assuntoChamado}
</p>

<p>
    <strong>Canal de Atendimento: </strong> ${canalAtendimento}
</p>

<p>
    <strong>Pontos de Consumo:</strong>
</p>


<table border='1' style='border-collapse: collapse;'>
    <thead>
    <tr>
        <th>Protocolo</th>
        <th>Ponto Consumo</th>
        <th>Cliente</th>
    </tr>
    <#list chamados as chamado>
        <tr>
            <td style="padding: 5px">${chamado.protocolo.numeroProtocolo}</td>
            <td style="padding: 5px">${chamado.pontoConsumo.descricao}</td>
            <td style="padding: 5px">
                <#if chamado.cliente?exists>${chamado.cliente.nome}</#if></td>
        </tr>
    </#list>
    </thead>
</table>


</html>
