<#-- Template de Emails para abertura de chamados em lote -->
<#setting number_format="computer">

<html>

Veja abaixo os chamados encerrados em lote: <br/><br/>

<table border='1' style='border-collapse: collapse;'>
    <thead>
    <tr>
        <th>Protocolo</th>
        <th>Cliente</th>
        <th>Ponto Consumo</th>
        <th>Solicitante</th>
    </tr>
    <#list chamados as chamado>
        <tr>
            <td style="padding: 5px">${chamado.protocolo.numeroProtocolo}</td>
            <td style="padding: 5px"><#if chamado.cliente?exists>${chamado.cliente.nome}</#if></td>
            <td style="padding: 5px"><#if chamado.pontoConsumo?exists>${chamado.pontoConsumo.descricao}</#if></td>
            <td style="padding: 5px"><#if chamado.nomeSolicitante?exists>${chamado.nomeSolicitante}</#if></td>
        </tr>
    </#list>
    </thead>
</table>


</html>
