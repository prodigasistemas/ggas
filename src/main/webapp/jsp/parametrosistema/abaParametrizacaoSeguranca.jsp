<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>

<script type="text/javascript">
$(document).ready(function() {

    var opt = {
            autoOpen: false,
            width: 550,
            modal: true,
            minHeight: 100,
            resizable: false
    }	
    
    $("#importarCertificadoPopup").dialog(opt)
    
    $("#botaoAbrirPopup").click(function () {
        exibirModalImportarCertificado();
    });
    
});

function exibirModalImportarCertificado() {
	
	$("#importarCertificadoPopup").dialog("open");
}

function fecharDialog() {
	$("#importarCertificadoPopup").dialog("close");
}

</script>

<style>
.certificado-label {
	font-size: 14px !important; /* Aumenta o tamanho da fonte */
	font-weight: bold !important; /* Torna o texto mais vis�vel */
}

.certificado-input {
	margin-top: 8px !important;
}

.botaoAdicionar {
	background-color: #437ea6 !important;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	border: 1px solid #376788;
	display: inline-block;
	color: #fff;
	font-family: arial;
	padding: 6px 24px;
	text-decoration: none;
	cursor: pointer;
	padding: 0 20px;
	margin-left: 10px;
	height: 25px;
	float: left;
}

.botaoAdicionar:hover {
	background-color: #2a4e67;
	border: 1px solid #376788;
}

.botaoAdicionar:active {
	background-color: #1197f4;
	border: 1px solid #376788;
}

</style>

<fieldset id="seguranca">
	<a class="linkHelp"
		href="<help:help>/abacadastroparametrizao.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>

	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<div id="importarCertificadoPopup" title="Importar Certificado">
				<label class="rotulo rotuloHorizontal certificado-label" id="rotuloArquivo"
					for="arquivo"><span class="campoObrigatorioSimbolo"></span>Certificado:</label> <input class="campoFile-sm campoHorizontal certificado-input"  type="file"
					id="arquivoCertificado" name="arquivoCertificado" title="Procurar" /> <input
					type="button" value="Limpar Arquivo" class="bottonRightCol2"
					id="limparArquivo"></br></br>
					<label class="rotulo rotuloHorizontal certificado-label" id="rotuloArquivo"
					for="arquivo"><span class="campoObrigatorioSimbolo"></span>Senha:</label> 
					<input class="campoTexto campo2Linhas" type="text"  name="senhaCertificado" value="" style="margin-top:6px;margin-left:30px;width: 150px;" >
					</br></br></br>
					<input name="Button" class="bottonRightCol2"
					id="botaoValidarPopup" value="Validar" type="button" tabindex="304">
					<input name="Button" class="bottonRightCol2"
					id="botaoFecharPopup" value="Fechar" type="button" tabindex="304" onclick="fecharDialog();">
			</div>
			<label class="rotulo">Certificado Digital:</label>
			<input class="campoTexto campo2Linhas" type="text" id="nomeCertificadoDigital" name="nomeCertificadoDigital" value="${nomeCertificadoDigital}"  >
			<input name="Button" class="botaoAdicionar"
				id="botaoAbrirPopup" value="Adicionar" type="button" tabindex="304">
		</fieldset>
	</fieldset>
</fieldset>
