<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<fieldset id="cobranca">
	<a class="linkHelp" href="<help:help>/abacobranaparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<fieldset class="conteinerBloco">
	
		<fieldset class="colunaFinal">
			
			<label class="rotulo">Parcelamento de d�bitos:</label>
			<select name="rubricaParcelamentoDebitoCobranca" id="tipoValorGeral" class="campoSelect">
				<c:forEach items="${listaRubricaEmUso}" var="tipoValor">
					<option value="<c:out value="${tipoValor.chavePrimaria}"/>" <c:if test="${rubricaParcelamentoDebitoCobranca == tipoValor.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoValor.descricao}"/>
					</option>
				</c:forEach>
			</select><br class="quebraLinha" />
			
			<label class="rotulo">Cr�dito d�bito Parcelado:</label>
			<select name="debitoSituacaoParceladaCobranca" id="tipoValorGeral" class="campoSelect" style="margin-bottom: 20px">
				<c:forEach items="${listaCreditoDebitoSituacao}" var="tipoValor">
					<option value="<c:out value="${tipoValor.chavePrimaria}"/>" <c:if test="${debitoSituacaoParceladaCobranca == tipoValor.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoValor.descricao}"/>
					</option>
				</c:forEach>
			</select><br class="quebraLinha" />

			<label class="rotulo rotulo2Linhas">Quantidade de clientes do relat�rio de declara��o de quita��o anual em lote:</label>
			<input class="campoTexto campo3Linhas" type="text" name="qtdClientesRelatorioLoteCobranca" maxlength="10" size="30"  value="${qtdClientesRelatorioLoteCobranca}" onkeypress="return formatarCampoInteiro(event,10);">
			<br class="quebraLinha" />
		
			<label class="rotulo rotulo2Linhas">Indica se a data de vencimento do documento de cobran�a � contra apresenta��o:</label>
			<input  class="campoRadio campoRadio3Linhas" type="radio" value="1" name="dataVencCobranca" <c:if test="${dataVencCobranca == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas" for="dataVencCobrancaCobranca">Sim</label>
			<input  class="campoRadio campoRadio3Linhas" type="radio" value="0" name="dataVencCobranca" <c:if test="${dataVencCobranca == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas" for="dataVencCobrancaCobranca">N�o</label>
			
			<label class="rotulo rotulo2Linhas">Enviar Relat�rio Aviso/Notifica��o de Corte por E-mail?</label>
			<input  class="campoRadio" type="radio" value="1" name="enviarEmailAvisoNotificacaoCorte" <c:if test="${enviarEmailAvisoNotificacaoCorte == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="enviarEmailAvisoNotificacaoCorte" <c:if test="${enviarEmailAvisoNotificacaoCorte == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			
			<label class="rotulo rotulo2Linhas">Obrigatoriedade do retorno do protocolo?</label>
			<input  class="campoRadio" type="radio" value="1" name="obrigatorioRetornoProtocolo" <c:if test="${obrigatorioRetornoProtocolo == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="obrigatorioRetornoProtocolo" <c:if test="${obrigatorioRetornoProtocolo == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>

			<label class="rotulo rotulo2Linhas">Mensagem de Aviso/Notifica��o de Corte na conta e, ou, notifica��o PDF?</label>
			<input  class="campoRadio" type="radio" value="1" id="exibirMensagemNotificacaoCorte0" name="exibirMensagemNotificacaoCorte" <c:if test="${exibirMensagemNotificacaoCorte == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Na conta</label>
			<input  class="campoRadio" type="radio" value="2" id="exibirMensagemNotificacaoCorte1" name="exibirMensagemNotificacaoCorte" <c:if test="${exibirMensagemNotificacaoCorte == '2'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Na notifica��o PDF</label>
			<input  class="campoRadio" type="radio" value="3" id="exibirMensagemNotificacaoCorte2" name="exibirMensagemNotificacaoCorte" <c:if test="${exibirMensagemNotificacaoCorte == '3'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Em ambos</label>

			<label class="rotulo rotulo2Linhas" style="margin-top: 5px">Participa do conv�nio e-Cartas?</label>
			<input  class="campoRadio" type="radio" value="1" name="participaECartas" <c:if test="${participaECartas == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="participaECartas" <c:if test="${participaECartas == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			<br class="quebraLinha" />
			<label class="rotulo rotulo2Linhas" style="margin-top: 15px">N�mero do contrato:</label>
			<input class="campoTexto campo3Linhas" type="text" id="numeroContratoECartas" name="numeroContratoECartas" maxlength="10" size="30"  value="${numeroContratoECartas}">
			<br class="quebraLinha" />
			<label class="rotulo rotulo2Linhas" style="margin-top: 15px">N�mero do cart�o postal:</label>
			<input class="campoTexto campo3Linhas" type="text" id="numeroCartaoPostalECartas" name="numeroCartaoPostalECartas" maxlength="10" size="30"  value="${numeroCartaoPostalECartas}">
			<br class="quebraLinha" />
			<label class="rotulo rotulo2Linhas" style="margin-top: 15px">Quantidade de faturas por lote:</label>
			<input class="campoTexto campo3Linhas" type="number" min="1" id="quantidadeFaturaPorLoteECartas" name="quantidadeFaturaPorLoteECartas" maxlength="10" size="30"  value="${quantidadeFaturaPorLoteECartas}">
			<br class="quebraLinha" />
			<hr class="linhaSeparadora1" />
			<fieldset class="conteinerBloco">
				<label class="rotulo rotulo2Linhas" for="mensagemNotificacaoCorte">Mensagem que ser� mostrado na nota da Fatura/Boleto quando uma fatura foi notificada para o corte:</label>
				<textarea class="campoTexto" name="mensagemNotificacaoCorte" id="mensagemNotificacaoCorte" rows="5" cols="80" > ${mensagemNotificacaoCorte}</textarea>
			</fieldset>
			<br />
		</fieldset>
	</fieldset>
</fieldset>
