<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<fieldset id="medicao">
	<a class="linkHelp" href="<help:help>/abamedioparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Padr�o de composi��o de consumo:</label>
			<select name="tipoComposicaoConsumoPadraoMedicao" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaTipoComposicaoConsumo}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${tipoComposicaoConsumoPadraoMedicao == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select><br />
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">N�mero m�ximo de d�gitos do mostrador do corretor de Vaz�o:</label>
			<select name="numeroDigitosCorretorVazao" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaNumeroDigitosCorretorVazao}" var="numero">
					<option value="<c:out value="${numero}"/>" <c:if test="${numeroDigitosCorretorVazao == numero}">selected="selected"</c:if>>
						<c:out value="${numero}"/>
					</option>
				</c:forEach>
			</select><br />
		</fieldset>
	</fieldset>
			
	<hr class="linhaSeparadora1" />	
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<fieldset class="pesquisarClienteFundo">
				<label class="rotulo rotulo2Linhas">Tipo de integra��o com o Sistema Supervis�rio:</label>
				<select name="tipoIntegracaoSistemaSupervisorio" class="campoSelect campo2Linhas">
			    	<c:forEach items="${listaTipoIntegracaoSistemaSupervisorio}" var="obj">
						<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${tipoIntegracaoSistemaSupervisorio == obj.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${obj.descricao}"/>
						</option>
					</c:forEach>
				</select>
				<br class="quebraLinha2" />
				
				<label class="rotulo rotulo2Linhas">Tipo de dado do Endere�o Remoto do Ponto de Consumo no Supervis�rio:</label>
				<select name="tipoDadoEnderecoRemoto" class="campoSelect campo2Linhas">
			    	<c:forEach items="${listaTipoDadoEnderecoRemoto}" var="obj">
						<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${tipoDadoEnderecoRemoto == obj.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${obj.descricao}"/>
						</option>
					</c:forEach>
				</select>
				
				<label class="rotulo rotulo2Linhas">Indica se o sistema exibir� as colunas: Leitura Corrigida, Leitura N�o Corrigida, Volume N�o Corrigido, Fator Z e Fator PTZ na tela do supervis�rio:</label>
				<input class="campoRadio campoRadio6BLinhas" type="radio" value="1" name="exibeColunasSupervisorio" <c:if test="${exibeColunasSupervisorio == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio rotuloRadio6BLinhas">Sim</label>
				<input class="campoRadio campoRadio6BLinhas" type="radio" value="0" name="exibeColunasSupervisorio" <c:if test="${exibeColunasSupervisorio == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio rotuloRadio6BLinhas">N�o</label><br class="quebraLinha2" />
				
				<label class="rotulo rotulo2Linhas">Indica se o sistema ir� atribuir o valor 1 para o Fator Z na tela do supervis�rio, quando n�o for atribuido nenhum valor:</label>
				<input class="campoRadio campoRadio6BLinhas" type="radio" value="1" name="supervisoriofatorzvalorfixo1" <c:if test="${supervisoriofatorzvalorfixo1 == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio rotuloRadio6BLinhas">Sim</label>
				<input class="campoRadio campoRadio6BLinhas" type="radio" value="0" name="supervisoriofatorzvalorfixo1" <c:if test="${supervisoriofatorzvalorfixo1 == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio rotuloRadio6BLinhas">N�o</label><br class="quebraLinha2" />
				
				<label class="rotulo rotulo2Linhas">Indica se o sistema obrigar� a informa��o de coment�rios relativos a todo ajuste/altera��o realizado em dados de medi��o di�ria e hor�ria vindos da integra��o com o supervis�rio:</label>
				<input class="campoRadio campoRadio4BLinhas" type="radio" value="1" name="comentarioMedicaoDiariaHoraria" <c:if test="${comentarioMedicaoDiariaHoraria == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio rotuloRadio4BLinhas">Sim</label>
				<input class="campoRadio campoRadio4BLinhas" type="radio" value="0" name="comentarioMedicaoDiariaHoraria" <c:if test="${comentarioMedicaoDiariaHoraria == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio rotuloRadio4BLinhas">N�o</label>
			</fieldset>
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Valor da press�o(atm) na condi��o de refer�ncia para ser utilizada na corre��o do consumo pelo fator de corre��o PTZ:</label>
			<input class="campoBTexto campo4Linhas" type="text" name="pressaoCondicaoReferenciaMedicao" maxlength="7" size="7" value="${pressaoCondicaoReferenciaMedicao}" onblur="aplicarMascaraNumeroDecimal(this,5);" onkeypress="return formatarCampoDecimalPositivo(event,this,7,5);">
		</fieldset>
		
		<fieldset class="colunaFinal">			
			<label class="rotulo rotulo2Linhas">Valor da temperatura(K) na condi��o de refer�ncia para ser utilizada na corre��o do consumo pelo fator de corre��o PTZ:</label>
			<input class="campoTexto campo5Linhas" type="text" name="temperaturaCondicaoReferenciaMedicao" maxlength="7" size="7" value="${temperaturaCondicaoReferenciaMedicao}" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,7,2);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Valor PCS refer�ncia para ser utilizada na corre��o do consumo pelo fator de corre��o PCS:</label>
			<input class="campoTexto campo4Linhas" type="text" name="pcsReferenciaMedicao" maxlength="7" size="7" value="${pcsReferenciaMedicao}" onkeypress="return formatarCampoInteiro(event);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Quantidade de casas para arredondamento para o Fator PCS:</label>
			<input class="campoTexto campo4Linhas" type="text" name="arredondamentoFatorPCS" maxlength="7" size="7" value="${arredondamentoFatorPCS}" onkeypress="return formatarCampoInteiro(event,7);">
					
			<label class="rotulo rotulo2Linhas">Unidade press�o medi��o:</label>
			<select name="unidadePressaoMedicao" class="campoSelect campo">
		    	<c:forEach items="${listaUnidadePressaoMedicao}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${unidadePressaoMedicao == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
			
			<label class="rotulo rotulo2Linhas">Unidade temperatura medi��o:</label>
			<select name="unidadeTemperaturaMedicao" class="campoSelect campo">
		    	<c:forEach items="${listaUnidadeTemperaturaMedicao}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${unidadeTemperaturaMedicao == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>
			
	<hr class="linhaSeparadora1" />
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Indica se o sistema obrigar� a informa��o de coment�rios relativos � todo ajuste/altera��o realizado em dados de Leitura de um ponto de consumo:</label>
			<input class="campoRadio campoRadio6BLinhas" type="radio" value="1" name="comentarioAnaliseExcecaoMedicao" <c:if test="${comentarioAnaliseExcecaoMedicao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio6BLinhas">Sim</label>
			<input class="campoRadio campoRadio6BLinhas" type="radio" value="0" name="comentarioAnaliseExcecaoMedicao" <c:if test="${comentarioAnaliseExcecaoMedicao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio6BLinhas">N�o</label><br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Indica se o sistema permitir� altera��o de dados de PCS de city gate para um ano/m�s faturado:</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="1" name="permiteAlteracaoPcsFaturadoMedicao" <c:if test="${permiteAlteracaoPcsFaturadoMedicao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">Sim</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="0" name="permiteAlteracaoPcsFaturadoMedicao" <c:if test="${permiteAlteracaoPcsFaturadoMedicao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">N�o</label>

			<label class="rotulo rotulo2Linhas">Indica se o sistema permitir� considerar rota de fiscaliza��o:</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="1" name="medirRotaFiscalizacao" <c:if test="${medirRotaFiscalizacao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">Sim</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="0" name="medirRotaFiscalizacao" <c:if test="${medirRotaFiscalizacao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">N�o</label>			
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Indica se o sistema permitir� altera��o de dados de PCS e Fator K do im�vel para um ano/m�s faturado:</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="1" name="permiteAlteracaoPcszMedicao" <c:if test="${permiteAlteracaoPcszMedicao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">Sim</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="0" name="permiteAlteracaoPcszMedicao" <c:if test="${permiteAlteracaoPcszMedicao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">N�o</label>
		</fieldset>
	</fieldset>
			
	<hr class="linhaSeparadora1" />
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			
			<label class="rotulo rotulo2Linhas">Tipo de arredondamento utilizado nas opera��es de Micromedi��o, como consumo m�dio.:</label>
			<select name="tipoArredondamentoConsumoMedicao" id="tipoArredondamentoConsumoMedicao" class="campoSelect">
				<c:forEach items="${listaMedidasArredondamento}" var="tipoValor">
					<option value="<c:out value="${tipoValor.chavePrimaria}"/>" <c:if test="${tipoArredondamentoConsumoMedicao == tipoValor.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoValor.descricao}"/>
					</option>
				</c:forEach>
			</select>			
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Diret�rio de arquivos de importa��o de leitura para upload:</label>
			<input class="campoTexto campo3Linhas" type="text" name="diretorioImportarLeituraUploadMedicao" maxlength="200" size="25" value="${diretorioImportarLeituraUploadMedicao}">
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Diretrio de arquivos dos registros de foto de leitura:</label>
			<input class="campoTexto campo3Linhas" type="text" name="diretorioImportarRegistroFotoLeitura" maxlength="200" size="25" value="${diretorioImportarRegistroFotoLeitura}">
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Caminho Completo de Falha no Carregamento dos Registros de Foto de Leitura:</label>
			<input class="campoTexto campo3Linhas" type="text" name="diretorioFalhaRegistroFotoLeitura" maxlength="200" size="25" value="${diretorioFalhaRegistroFotoLeitura}">
		</fieldset>
		
		<label class="rotulo rotulo2Linhas">Anormalidade de consumo zero:</label>
			<select name="anormalidadeConsumoZero" class="campoSelect campo3Linhas">
		    	<c:forEach items="${listaAnormalidadeConsumo}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${anormalidadeConsumoZero == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
	</fieldset>
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			
			<label class="rotulo rotulo2Linhas">Quantidade m�nima de medi��es hor�rias para o c�lculo da m�dia di�ria:</label>
			<input class="campoTexto campo4Linhas" type="text" name="quantidadeMinimaRegistrosMedicoesHora" maxlength="7" size="7" value="${quantidadeMinimaRegistrosMedicoesHora}" onkeypress="return formatarCampoInteiro(event,7);">
			
			<label class="rotulo rotulo2Linhas">Quantidade de medi��es hor�rias para o c�lculo da m�dia di�ria quando n�o houver a quantidade m�nima de medi��es hor�rias no dia:</label>
			<input class="campoTexto campo4Linhas" type="text" name="quantidadeMedicaoHorariaParaMedia" maxlength="7" size="7" value="${quantidadeMedicaoHorariaParaMedia}" onkeypress="return formatarCampoInteiro(event,7);">
			
			<label class="rotulo rotulo2Linhas">Indica se o contrato ser� validado:</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="1" name="validaContrato" <c:if test="${validaContrato == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">Sim</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="0" name="validaContrato" <c:if test="${validaContrato == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">N�o</label>
			
			<label class="rotulo rotulo2Linhas">Indica o registro de hist�rico de medi��o sem leitura retornada:</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="1" name="registraHistoricoMedicao" <c:if test="${registraHistoricoMedicao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">Sim</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="0" name="registraHistoricoMedicao" <c:if test="${registraHistoricoMedicao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">N�o</label>
			
			<label class="rotulo rotulo2Linhas">Considerar data da �ltima leitura para gerar a rota no GERAR DADOS:</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="1" name="consideraUltimaLeituraGerarDados" <c:if test="${consideraUltimaLeituraGerarDados == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">Sim</label>
			<input class="campoRadio campoRadio4BLinhas" type="radio" value="0" name="consideraUltimaLeituraGerarDados" <c:if test="${consideraUltimaLeituraGerarDados == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4BLinhas">N�o</label>			
				
		</fieldset>
	</fieldset>
			
</fieldset>