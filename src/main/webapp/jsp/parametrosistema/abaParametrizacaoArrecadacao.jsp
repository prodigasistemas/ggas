<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<script type="text/javascript">

$(document).ready(function(){
	$("#docCobrancaRecebMenorArrecadacaoSim").click(campoAcaoVisivel);
    $("#docCobrancaRecebMenorArrecadacaoNao").click(campoAcaoInvisivel);
});

animatedcollapse.addDiv('divAcaoRecebimentoAMenor');

function habilitarDesabilitarCamposAcao(campo) {
	if (campo.value == '0') {
		campoAcaoInvisivel();
	}else if (campo.value == '1') {
		campoAcaoVisivel();
	}
}

function init(){
	habilitarDesabilitarCamposAcao(document.parametroSistemaForm.docCobrancaRecebMenorArrecadacao);
}

function campoAcaoVisivel(){
	document.getElementById("divAcaoRecebimentoAMenor").style.display = "none";
}

function campoAcaoInvisivel(){
	document.getElementById("divAcaoRecebimentoAMenor").style.display = "block"; 
}

addLoadEvent(init);


</script>
<fieldset id="arrecadacao">
	<a class="linkHelp" href="<help:help>/abaarrecadaoparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	
	<fieldset class="conteinerBloco">
		<fieldset class="colunaFinal">
			<label class="rotulo">Recebimento docto inexistente:</label>
			<select name="recebimentoDoctoInexisArrecadacao" id="recebimentoDoctoInexisArrecadacao" class="campoSelect">
				<c:forEach items="${listaRecebimentoSituacao}" var="rubricaParcelamentoDebitos">
					<option value="<c:out value="${rubricaParcelamentoDebitos.chavePrimaria}"/>" <c:if test="${recebimentoDoctoInexisArrecadacao == rubricaParcelamentoDebitos.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${rubricaParcelamentoDebitos.descricao}"/>
					</option>
				</c:forEach>
			</select><br class="quebraLinha2" />
		</fieldset>
	</fieldset>
		
	<hr class="linhaSeparadora1" />
		
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			
			<label class="rotulo">C�digo da Empresa:</label>
			<input class="campoTexto" type="text" name="empresaFebrabanArrecadacao" maxlength="9" size="8" value="${empresaFebrabanArrecadacao}" onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Nome da empresa para gera��o para d�bito autom�tico:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="nomeEmpresaGeral" maxlength="60" size="30"  value="${nomeEmpresaGeral}">
			<br class="quebraLinha2" />			
			
			<label class="rotulo rotulo2Linhas">Quantidade de dias para vencimento do documento de cobran�a complementar:</label>
			<input class="campoTexto campo3Linhas" type="text"  name="qtdDiasVencDocComplementarArrecadacao" maxlength="9" size="8" value="${qtdDiasVencDocComplementarArrecadacao}"  onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Indica o valor m�nimo para gera��o do documento de cobran�a de valor pago a menor:</label>
			<input class="campoTexto campo4Linhas" type="text"  name="valorMinDocCobrancaRecMenorArrecadacao" maxlength="9" size="8" value="${valorMinDocCobrancaRecMenorArrecadacao}"  onkeypress="return formatarCampoInteiro(event,9);">
					
		</fieldset>
	
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Indica que o sistema dever� gerar um cr�dito com o valor pago a maior:</label>
			<input id="geraCreditoRecebMaiorDuplicidadeArrecadacao" class="campoRadio campoRadio3Linhas" type="radio" value="1" name="geraCreditoRecebMaiorDuplicidadeArrecadacao" <c:if test="${geraCreditoRecebMaiorDuplicidadeArrecadacao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas" for="geraCreditoRecebMaiorDuplicidadeArrecadacao">Sim</label>
			<input id="geraCreditoRecebMaiorDuplicidadeArrecadacao" class="campoRadio campoRadio3Linhas" type="radio" value="0" name="geraCreditoRecebMaiorDuplicidadeArrecadacao" <c:if test="${geraCreditoRecebMaiorDuplicidadeArrecadacao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas" for="naoGeraCreditoRecebMaiorDuplicidadeArrecadacao">N�o</label>
			<br class="quebraLinha2" />
			
			
			<label class="rotulo rotulo2Linhas">Indica que o sistema dever� gerar um documento de cobran�a com o valor pago a menor:</label>
			<input id="docCobrancaRecebMenorArrecadacaoSim" class="campoRadio campoRadio4Linhas" type="radio" value="1" name="docCobrancaRecebMenorArrecadacao" onclick="campoAcaoInvisivel();" <c:if test="${docCobrancaRecebMenorArrecadacao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4Linhas" for="docCobrancaRecebMenorArrecadacao">Sim</label>
			<input id="docCobrancaRecebMenorArrecadacaoNao" class="campoRadio campoRadio4Linhas" type="radio" value="0" name="docCobrancaRecebMenorArrecadacao" onclick="campoAcaoVisivel();" <c:if test="${docCobrancaRecebMenorArrecadacao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio4Linhas" for="naoDocCobrancaRecebMenorArrecadacao">N�o</label>
			<br class="quebraLinha2" />
			
			<!-- tag para corre��o de alinhamento -->
			<div id="divAcaoRecebimentoAMenor" style="margin-top:5px; width: 100%;">
				<fieldset id="conteinerAutenticacaoSMTP"  >
			
					<label class="rotulo rotulo2Linhas">A��o tratar recebimento a menor:</label>
					<select name="acaoRecebimentoAMenor" id="acaoRecebimentoAMenor" class="campoSelect">
						<c:forEach items="${listaBaixaRecebimento}" var="baixaRecebimento">
							<option value="<c:out value="${baixaRecebimento.chavePrimaria}"/>" <c:if test="${acaoRecebimentoAMenor == baixaRecebimento.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${baixaRecebimento.descricao}"/>
							</option>
						</c:forEach>
					</select>				

				</fieldset>
			</div>
			<!-- tag para corre��o de alinhamento -->
			
		</fieldset>
	</fieldset>
		
	<hr class="linhaSeparadora1" />
			
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotuloVertical">Diretorio de arquivos de arrecada��o a serem processados:</label>
			<input class="campoTexto campoVertical" type="text"  name="arquivosArrecadacao" maxlength="200" size="65" value="${arquivosArrecadacao}">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotuloVertical">Diret�rio de arquivos de remessa:</label>
			<input class="campoTexto campoVertical" type="text"  name="arquivosRemessaArrecadacao" maxlength="200" size="65" value="${arquivosRemessaArrecadacao}">
		</fieldset>
	
		<fieldset class="colunaFinalB">
			<label class="rotulo rotuloVertical">Diret�rio de arquivos de retorno:</label>
			<input class="campoTexto campoVertical" type="text"  name="arquivosRetornoArrecadacao" maxlength="200" size="65" value="${arquivosRetornoArrecadacao}">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotuloVertical">Diret�rio de arquivos retorno para upload:</label>
			<input class="campoTexto campoVertical" type="text"  name="arquivosUploadArrecadacao" maxlength="200" size="65" value="${arquivosUploadArrecadacao}">
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotuloVertical">Express�o n�o num�rica que vem no arquivo de retorno:</label>
			<input class="campoTexto campoVertical" type="text"  name="arrecadacaoCampoArquivoCodigoConvenio" 
			id="arrecadacaoCampoArquivoCodigoConvenio"
				maxlength="16" size="65" value="${arrecadacaoCampoArquivoCodigoConvenio}">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotuloVertical">C�digo Conv�nio do Contrato Conv�nio Arrecadador a ser usado:</label>
			<input class="campoTexto campoVertical" type="text"  name="arrecadacaoCodigoConvenio" 
			id="arrecadacaoCodigoConvenio" maxlength="10" size="65" value="${arrecadacaoCodigoConvenio}">
		</fieldset>
	</fieldset>
	
</fieldset>

<script>if (document.getElementById("docCobrancaRecebMenorArrecadacaoSim").checked == true) { document.getElementById("divAcaoRecebimentoAMenor").style.display = "none"; }</script>