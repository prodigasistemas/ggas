<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<fieldset id="faturamento">
	<a class="linkHelp" href="<help:help>/abafaturamentoparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>

	<fieldset class="conteinerBloco">
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Motivo de Refaturar:</label>
			<select name="motivoRefaturarFaturamento" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaEntidadeClasse}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${motivoRefaturarFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
		<br />
		<label class="rotulo rotulo2Linhas" name="lbGerenteFinanceiro">Gerente Financeiro:</label>
		<input class="campoTexto campo2Linhas" type="text" name="diretorFinanceiroTexto" id="diretorFinanceiroTexto" maxlength="255" size="70" value="${diretorFinanceiroTexto}">
	</fieldset>

	<hr class="linhaSeparadora1" />

  <fieldset class="conteinerBloco">
	<fieldset class="colunaFinal">
	<label class="rotulo rotulo2Linhas">Motivo de inclus�o quando chamado pelo faturar grupo:</label>
	<select name="motivoInclusaoFaturamento" class="campoSelect campo2Linhas">
    	<c:forEach items="${listaFormaInclusaoFatura}" var="obj">
			<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${motivoInclusaoFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${obj.descricao}"/>
			</option>
		</c:forEach>
	</select>
	</fieldset>

	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Motivo de cancelamento do cr�dito/d�bito:</label>
			<select name="debitoMotivoCancelamentoFaturamento" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaMotivoCancelamento}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${debitoMotivoCancelamentoFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Situa��o do Cr�dito D�bito Normal:</label>
			<select name="debitoSituacaoNormalFaturamento" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaCreditoDebitoSituacao}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${debitoSituacaoNormalFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<label class="rotulo rotulo2Linhas">Diret�rio de arquivos dos arquivos de fatura:</label>
		<input class="campoTexto campo2Linhas" type="text" name="diretorioArquivosFaturaFaturamento" maxlength="255" size="70" value="${diretorioArquivosFaturaFaturamento}">
		<br class="quebraLinha2" />

		<label class="rotulo rotulo2Linhas">Diret�rio de arquivos da simula��o de faturamento:</label>
		<input class="campoTexto campo2Linhas" type="text" name="diretorioArquivosSimulacaoFaturamento" maxlength="255" size="70" value="${diretorioArquivosSimulacaoFaturamento}">
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<label class="rotulo rotulo2Linhas" for="textoEmailFatura">Texto para email de envio de faturas:</label>
		<textarea class="campoTexto" name="textoEmailFatura" id="textoEmailFatura" rows="5" cols="80" > ${textoEmailFatura}</textarea>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Parametro para filtrar o combo na tela do preco de gas para n�o exibir o item de fatura margem de distribui��o:</label>
			<select name="filtroItemFaturaPrecoGasFaturamento" class="campoSelect campo4Linhas">
		    	<c:forEach items="${listaItemFatura}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${filtroItemFaturaPrecoGasFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>

		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">O sistema ir� gerar um cr�dito de volume ao alterar a leiturar anterior:</label>
			<input class="campoRadio campoRadio3Linhas" type="radio" value="1" name="creditoVolumeAutomaticamentoFaturamento" <c:if test="${creditoVolumeAutomaticamentoFaturamento == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas">Sim</label>
			<input class="campoRadio campoRadio3Linhas" type="radio" value="0" name="creditoVolumeAutomaticamentoFaturamento" <c:if test="${creditoVolumeAutomaticamentoFaturamento == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas">N�o</label><br class="quebraLinha2" />
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">M�todo de reajuste cascata proporcional complementar:</label>
			<select name="reajusteComplementarFaturamento" class="campoSelect campo2Linhas">
			   	<c:forEach items="${listaReajusteCascata}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${reajusteComplementarFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Quantidade de dias m�xima para prorroga��o da data de vencimento da fatura:</label>
			<input class="campoTexto campo3Linhas" type="text" name="numeroDiasMaximoProrrogacaoVencimentoFaturamento" maxlength="3" size="3" value="${numeroDiasMaximoProrrogacaoVencimentoFaturamento}" onkeypress="return formatarCampoInteiro(event,3);">
			<br class="quebraLinha2" />

			<label class="rotulo rotulo2Linhas">Quantidade de dias m�nima que ser� gerado uma fatura em rela��o ao melhor dia de vencimento do d�bito a cobrar:</label>
			<input class="campoTexto campo4Linhas" type="text" name="diasMinimoVencimentoFaturamento" maxlength="3" size="3" value="${diasMinimoVencimentoFaturamento}" onkeypress="return formatarCampoInteiro(event,3);">


			<label class="rotulo rotulo2Linhas">Obtem o intervalo m�ximo(meses) para cobran�a de uma fatura:</label>
			<input class="campoTexto campo3Linhas" type="text" name="intervaloMaximoCobrancaFaturaFaturamento" maxlength="3" size="3" value="${intervaloMaximoCobrancaFaturaFaturamento}" onkeypress="return formatarCampoInteiro(event,3);">
			<br class="quebraLinha2" />
			<label class="rotulo rotulo2Linhas">Quantidade de dias entre a data de apresenta��o e vencimento da fatura :</label>
			<input class="campoTexto campo3Linhas" type="text" name="quantidadeDiasApresentacaoVencimento" maxlength="2" size="3" value="${quantidadeDiasApresentacaoVencimento}" onkeypress="return formatarCampoInteiro(event,3);">
			<label class="rotulo rotulo2Linhas">Quantidade de meses para prescri��o de faturas :</label>
			<input class="campoTexto campo3Linhas" type="text" name="quantidadeMesesPrescricao" maxlength="2" size="3" value="${quantidadeMesesPrescricao}" onkeypress="return formatarCampoInteiro(event,3);">
		</fieldset>

		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Quantidade de dias para cancelamento da nota fiscal:</label>
			<input class="campoTexto campo2Linhas" type="text" name="qtdaDiasCancelamentoNotaFiscalFaturamento" maxlength="3" size="3" value="${qtdaDiasCancelamentoNotaFiscalFaturamento}" onkeypress="return formatarCampoInteiro(event,5);">
			<br class="quebraLinha2" />

			<label class="rotulo rotulo2Linhas">Quantidade de faturas que o sistema dever� gerar por lote:</label>
			<input class="campoTexto campo2Linhas" type="text" name="qtdaFaturaLoteFaturamento" maxlength="5" size="3" value="${qtdaFaturaLoteFaturamento}" onkeypress="return formatarCampoInteiro(event,5);">
			<br class="quebraLinha2" />

			<label class="rotulo rotulo2Linhas">Quantidade m�xima de faturamento consecutivo para o tipo de consumo: informado pelo cliente:</label>
			<input class="campoTexto campo4Linhas" type="text" name="qtdaMaxFaturamentoInformadoClienteFaturamento" maxlength="5" size="3" value="${qtdaMaxFaturamentoInformadoClienteFaturamento}" onkeypress="return formatarCampoInteiro(event,5);">
			<br class="quebraLinha2" />

			<label class="rotulo rotulo2Linhas">Quantidade m�xima de faturamento consecutivo para o tipo de consumo: m�dia:</label>
			<input class="campoTexto campo3Linhas" type="text" name="qtdaMaxFaturamentoMediaFaturamento" maxlength="5" size="3" value="${qtdaMaxFaturamentoMediaFaturamento}" onkeypress="return formatarCampoInteiro(event,5);">
			<br class="quebraLinha2" />

			<label class="rotulo rotulo2Linhas">Quantidade m�nima de cronogramas em aberto:</label>
			<input class="campoTexto campo2Linhas" type="text" name="qtdaMinCronogramasAbertoFaturamento" maxlength="5" size="3" value="${qtdaMinCronogramasAbertoFaturamento}" onkeypress="return formatarCampoInteiro(event,5);">
			<br class="quebraLinha2" />

			<label class="rotulo rotulo2Linhas">Quantidade m�xima de cronogramas em aberto:</label>
			<input class="campoTexto campo3Linhas" type="text" name="qtdaMaxCronogramasAbertosFaturamento" maxlength="5" size="3" value="${qtdaMaxCronogramasAbertosFaturamento}" onkeypress="return formatarCampoInteiro(event,5);">
			<br class="quebraLinha2" />

			<label class="rotulo rotulo2Linhas">Prazo m�nimo de anteced�ncia para entrega de fatura em rela��o � data de vencimento:</label>
			<input class="campoTexto campo2Linhas" type="text" name="prazoMinParaEntragaFaturaVencimento" maxlength="5" size="3" value="${prazoMinParaEntragaFaturaVencimento}" onkeypress="return formatarCampoInteiro(event,5);">
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Tamanho do campo da descri��o complementar na inclus�o de cr�dito e d�bito a realizar:</label>
			<input class="campoTexto campo4Linhas" type="text" name="tamanhoDescComplementarCreDebFaturamento" maxlength="3" size="3" value="${tamanhoDescComplementarCreDebFaturamento}" onkeypress="return formatarCampoInteiro(event,3);">
		</fieldset>

		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Tamanho do campo da descri��o complementar na inclus�o de uma nota de d�bito e cr�dito:</label>
			<input class="campoTexto campo4Linhas" type="text" name="tamanhoDescComplementarNotaFaturamento" maxlength="3" size="3" value="${tamanhoDescComplementarNotaFaturamento}" onkeypress="return formatarCampoInteiro(event,3);">
		</fieldset>

		<hr class="linhaSeparadora1" />

		<label class="rotulo rotulo2Linhas">Mensagem exibida na fatura para icms recolhido:</label>
		<input class="campoTexto campo2Linhas" type="text" name="textoSubstituicaoTributariaNotaFaturamento" maxlength="255" size="90" value="${textoSubstituicaoTributariaNotaFaturamento}">
		<label class="rotulo rotulo2Linhas">Mensagem exibida na fatura para isen��o de ICMS destacado:</label>
		<input class="campoTexto campo2Linhas" type="text" name="textoSubstituicaoTributariaNotaFaturamentoDestacado" maxlength="255" size="90" value="${textoSubstituicaoTributariaNotaFaturamentoDestacado}">

		<label class="rotulo rotulo2Linhas">Indica��o se o volume da fatura ser� somado na impress�o quando houver mudan�a de tarifa no ciclo:</label>
			<input class="campoRadio campoRadio3Linhas" type="radio" value="1" name="somarVolumeFaturaImpressao" <c:if test="${somarVolumeFaturaImpressao == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas">Sim</label>
			<input class="campoRadio campoRadio3Linhas" type="radio" value="0" name="somarVolumeFaturaImpressao" <c:if test="${somarVolumeFaturaImpressao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas">N�o</label><br class="quebraLinha2" />

	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="colunaFinal">
			<label class="rotulo">Cr�dito a Realizar:</label>
			<select name="tipoDocCreditoRealizarFaturamento" class="campoSelect">
		    	<c:forEach items="${listaTipoCreditoDebito}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${tipoDocCreditoRealizarFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select><br />
			<label class="rotulo rotulo2Linhas">Percentual do valor da fatura a ser utilizado como limite do desconto:</label>
			<input class="campoTexto campo5Linhas" type="text" name="percentualValorFaturaLimiteDesconto" maxlength="7" size="7" value="${percentualValorFaturaLimiteDesconto}" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,7,2);">
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Tipo do substituto para o calculo da substitui��o tribut�ria:</label>
			<select name="tipoSubstitutoFaturamento" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaTipoSubstituto}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${tipoSubstitutoFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Indica o valor m�nimo para gera��o do documento de cobran�a no momento de gera��o da fatura:</label>
			<input class="campoTexto campo4Linhas" type="text" name="valorMinGeracaoDocCobFaturamento" maxlength="3" size="3" value="${valorMinGeracaoDocCobFaturamento}" onkeypress="return formatarCampoInteiro(event,3);"><br />
		</fieldset>

		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Volume de refer�ncia para o c�lculo da tarifa m�dia para ToP:</label>
			<select name="volumeRefTarifaMediaFaturamento" class="campoSelect campo3Linhas">
		    	<c:forEach items="${listaVolumeReferenciaTarifaMediaTop}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${volumeRefTarifaMediaFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Ano e M�s de referi�ncia para o Faturamento:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="referenciaFaturamento" size="6"  maxlength="6" value="${referenciaFaturamento}" onkeypress="return formatarCampoInteiro(event,6);">
			<br class="quebraLinha2" />
		</fieldset>

		<fieldset class="colunaFinal">
			<label class="rotulo">Regime tribut�rio do ICMS:</label>
			<select name="regimeTributarioFaturamento" class="campoSelect">
		    	<c:forEach items="${listaRegimeTributario}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${regimeTributarioFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>

	<hr class="linhaSeparadora1" />

	<fieldset class="conteinerBloco">

		<fieldset class="colunaFinal">

			<label class="rotulo">Ambiente do Sistema da NFE:</label>
			<select name="indicadorAmbienteSistemaNFEFaturamento" class="campoSelect">
		    	<c:forEach items="${listaIndicadorAmbienteNFE}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${indicadorAmbienteSistemaNFEFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select><br />

			<label class="rotulo">Vers�o do xml da NFE:</label>
			<input class="campoTexto" type="text" name="nrVersaoXMLNFEFaturamento" maxlength="3" size="3" value="${nrVersaoXMLNFEFaturamento}" onkeypress="return formatarCampoInteiro(event,3);"><br /> <label class="rotulo">Emiss�o de S�rie Eletr�nica</br> para Cliente sem Contrato:</label> <input class="campoRadio" type="radio" name="emissaoSerieEletronica"
				id="1" value="1"
				<c:if test="${emissaoSerieEletronica eq '1'}">checked="checked"</c:if>> <label class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" name="emissaoSerieEletronica"
				id="0" value="0"
				<c:if test="${emissaoSerieEletronica eq '0'}">checked="checked"</c:if>> <label class="rotuloRadio">N�o</label>

			<fieldset class="">
				<label class="rotulo">Usu�rio:</label>
				<select name="usuarioFaturamento" class="campoSelect">
					<c:forEach items="${listaUsuarios}" var="obj">
						<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${usuarioFaturamento == obj.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${obj.login}"/>
						</option>
					</c:forEach>
				</select>
			</fieldset>
		 </fieldset>

		<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">

			<label class="rotulo rotulo2Linhas">Permite gerar fatura com valor 0:</label>
			<input  class="campoRadio" type="radio" value="1" name="faturaValorZero" <c:if test="${faturaValorZero == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="faturaValorZero">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="faturaValorZero" <c:if test="${faturaValorZero == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="faturaValorZero">N�o</label>
				
			<label class="rotulo rotulo2Linhas">Permite faturar com consumo 0:</label>
			<input  class="campoRadio" type="radio" value="1" name="faturaConsumoZero" <c:if test="${faturaConsumoZero == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="faturaConsumoZero">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="faturaConsumoZero" <c:if test="${faturaConsumoZero == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="faturaConsumoZero">N�o</label>

			<label class="rotulo rotulo2Linhas">Conceder desconto da tarifa para inadimplentes:</label>
			<input  class="campoRadio" type="radio" value="1" name="concedeDescontoInadimplente" <c:if test="${concedeDescontoInadimplente == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="concedeDescontoInadimplente">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="concedeDescontoInadimplente" <c:if test="${concedeDescontoInadimplente == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="concedeDescontoInadimplente">N�o</label>

			<label class="rotulo rotulo2Linhas">Indicador de obten��o da vig�ncia da substitui��o tribut�ria:</label>
			<select name="vigenciaSubstituicaoTributaria" class="campoSelect">
		    	<c:forEach items="${listaVigenciaSubstituicaoTributaria}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${vigenciaSubstituicaoTributaria == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>

		<hr class="linhaSeparadora1" />

		<fieldset class="conteinerBloco">
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo rotulo2Linhas">Caminho no Sistema para gravar os arquivos da SEFAZ:</label>
				<input class="campoTexto campo4Linhas" type="text" name="diretorioArquivosSefaz" value="${diretorioArquivosSefaz}">
			</fieldset>
		</fieldset>

		<hr class="linhaSeparadora1" />
		<fieldset class="conteinerBloco">
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo rotulo2Linhas">Utilizar Multiplos Ciclos para controle dos ciclos:</label>
				<input  class="campoRadio" type="radio" value="1" name="utilizarMultiplosCiclos" <c:if test="${utilizarMultiplosCiclos == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="utilizarMultiplosCiclos">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="utilizarMultiplosCiclos" <c:if test="${utilizarMultiplosCiclos == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="utilizarMultiplosCiclos">N�o</label>
				
				
				<label class="rotulo rotulo2Linhas">Indicador de execu��o do faturamento em paralelo:</label>
				<input  class="campoRadio" type="radio" value="1" name="faturamentoParalelo" <c:if test="${faturamentoParalelo == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="faturamentoParalelo">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="faturamentoParalelo" <c:if test="${faturamentoParalelo == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="faturamentoParalelo">N�o</label>
				
				<label class="rotulo rotulo2Linhas">Indicador de execu��o da contabiliza��o dos lan�amentos cont�beis durante o faturamento:</label>
				<input  class="campoRadio" type="radio" value="1" name="contabilizacaoLancamentos" <c:if test="${contabilizacaoLancamentos == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="contabilizacaoLancamentos">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="contabilizacaoLancamentos" <c:if test="${contabilizacaoLancamentos == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="contabilizacaoLancamentos">N�o</label>
				
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		<fieldset class="conteinerBloco">
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo rotulo2Linhas">Criar todos os cronogramas da Refer�ncia no Encerrar Ciclo?:</label>
				<input  class="campoRadio" type="radio" value="1" name="criarCronogramasEncerrar" <c:if test="${criarCronogramasEncerrar == '1'}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="criarCronogramasEncerrar">Sim</label>
					<input  class="campoRadio" type="radio" value="0" name="criarCronogramasEncerrar" <c:if test="${criarCronogramasEncerrar == '0'}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="criarCronogramasEncerrar">N�o</label>
					
				<label class="rotulo rotulo2Linhas">Calcular pela m�dia di�ria clientes com coletor de dados na tarifa Di�ria?:</label>
				<input  class="campoRadio" type="radio" value="1" name="calcularMediaDiariaColetor" <c:if test="${calcularMediaDiariaColetor == '1'}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="calcularMediaDiariaColetor">Sim</label>
					<input  class="campoRadio" type="radio" value="0" name="calcularMediaDiariaColetor" <c:if test="${calcularMediaDiariaColetor == '0'}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="calcularMediaDiariaColetor">N�o</label>	
			</fieldset>

			
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		<fieldset class="conteinerBloco">
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo rotulo2Linhas">Cancelar fatura ap�s o prazo?:</label>
				<input  class="campoRadio" type="radio" value="1" name="cancelaFaturaAposPrazo" <c:if test="${cancelaFaturaAposPrazo == '1'}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="cancelaFaturaAposPrazo">Sim</label>
					<input  class="campoRadio" type="radio" value="0" name="cancelaFaturaAposPrazo" <c:if test="${cancelaFaturaAposPrazo == '0'}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="cancelaFaturaAposPrazo">N�o</label>
			</fieldset>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo rotulo2Linhas">Quantidade de refer�ncias para c�lculo da m�dia para validar valores altos da fatura:</label>
				<input class="campoTexto campo4Linhas" type="text" name="quantidadeReferenciasCalculoMedia" maxlength="3" size="3" value="${quantidadeReferenciasCalculoMedia}" onkeypress="return formatarCampoInteiro(event,3);"><br />
			</fieldset>
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo rotulo2Linhas">Porcentagem para considerar faturamento elevado:</label>
				<input class="campoTexto campo4Linhas" type="text" name="porcentagemConsideradaFaturamentoElevado" maxlength="3" size="3" value="${porcentagemConsideradaFaturamentoElevado}" onkeypress="return formatarCampoInteiro(event,3);"><br />
			</fieldset>
		</fieldset>
		
</fieldset>
</fieldset>
