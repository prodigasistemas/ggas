<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<fieldset id="agencia">
    <a class="linkHelp" href="<help:help>/abacadastroparametrizao.htm</help:help>" target="right"
       onclick="exibirJDialog('#janelaHelp');"></a>

    <fieldset class="conteinerBloco">
        <fieldset class="coluna detalhamentoColunaLarga">

            <label class="rotulo rotulo2Linhas">Exibir consulta de notas fiscais/Segunda via</label>
            <input class="campoRadio" type="radio" id="exibirConsultaNotaFiscalSim" value="1" name="exibirMenuAgenciaVirtualConsultaNotaFiscal"
                   <c:if test="${exibirMenuAgenciaVirtualConsultaNotaFiscal == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirConsultaNotaFiscalSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirConsultaNotaFiscalNao" value="0" name="exibirMenuAgenciaVirtualConsultaNotaFiscal"
                   <c:if test="${exibirMenuAgenciaVirtualConsultaNotaFiscal == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirConsultaNotaFiscalNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir consulta de consumo por per�odo</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualConsumoPorPeriodoSim" value="1" name="exibirMenuAgenciaVirtualConsumoPorPeriodo"
                   <c:if test="${exibirMenuAgenciaVirtualConsumoPorPeriodo == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualConsumoPorPeriodoSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualConsumoPorPeriodoNao" value="0" name="exibirMenuAgenciaVirtualConsumoPorPeriodo"
                   <c:if test="${exibirMenuAgenciaVirtualConsumoPorPeriodo == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualConsumoPorPeriodoNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir consulta de declara��o de quita��o anual de d�bitos</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnualSim" value="1" name="exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnual"
                   <c:if test="${exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnual == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnualSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnualNao" value="0" name="exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnual"
                   <c:if test="${exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnual == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnualNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir autentica��o de declara��o de quita��o anual de d�bitos</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnualSim" value="1" name="exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnual"
                   <c:if test="${exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnual == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnualSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnualNao" value="0" name="exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnual"
                   <c:if test="${exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnual == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnualNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir consulta de contrato de ades�o</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualContratoAdesaoSim" value="1" name="exibirMenuAgenciaVirtualContratoAdesao"
                   <c:if test="${exibirMenuAgenciaVirtualContratoAdesao == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualContratoAdesaoSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualContratoAdesaoNao" value="0" name="exibirMenuAgenciaVirtualContratoAdesao"
                   <c:if test="${exibirMenuAgenciaVirtualContratoAdesao == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualContratoAdesaoNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir solicita��o de altera��o cadastral</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualAlteracaoCadastralSim" value="1" name="exibirMenuAgenciaVirtualAlteracaoCadastral"
                   <c:if test="${exibirMenuAgenciaVirtualAlteracaoCadastral == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualAlteracaoCadastralSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualAlteracaoCadastralNao" value="0" name="exibirMenuAgenciaVirtualAlteracaoCadastral"
                   <c:if test="${exibirMenuAgenciaVirtualAlteracaoCadastral == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualAlteracaoCadastralNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir altera��o de data de vencimento</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualAlteracaoDataVencimentoSim" value="1" name="exibirMenuAgenciaVirtualAlteracaoDataVencimento"
                   <c:if test="${exibirMenuAgenciaVirtualAlteracaoDataVencimento == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualAlteracaoDataVencimentoSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualAlteracaoDataVencimentoNao" value="0" name="exibirMenuAgenciaVirtualAlteracaoDataVencimento"
                   <c:if test="${exibirMenuAgenciaVirtualAlteracaoDataVencimento == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualAlteracaoDataVencimentoNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir solicita��o tempor�ria de interrup��o de fornecimento</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualInterrupcaoFornecimentoSim" value="1" name="exibirMenuAgenciaVirtualInterrupcaoFornecimento"
                   <c:if test="${exibirMenuAgenciaVirtualInterrupcaoFornecimento == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualInterrupcaoFornecimentoSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualInterrupcaoFornecimentoNao" value="0" name="exibirMenuAgenciaVirtualInterrupcaoFornecimento"
                   <c:if test="${exibirMenuAgenciaVirtualInterrupcaoFornecimento == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualInterrupcaoFornecimentoNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir solicita��o de religa��o de unidade consumidora</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualReligacaoUnidadeConsumidoraSim" value="1" name="exibirMenuAgenciaVirtualReligacaoUnidadeConsumidora"
                   <c:if test="${exibirMenuAgenciaVirtualReligacaoUnidadeConsumidora == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualReligacaoUnidadeConsumidoraSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualReligacaoUnidadeConsumidoraNao" value="0" name="exibirMenuAgenciaVirtualReligacaoUnidadeConsumidora"
                   <c:if test="${exibirMenuAgenciaVirtualReligacaoUnidadeConsumidora == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualReligacaoUnidadeConsumidoraNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir solicita��o de liga��o de g�s</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualLigacaoGasSim" value="1" name="exibirMenuAgenciaVirtualLigacaoGas"
                   <c:if test="${exibirMenuAgenciaVirtualLigacaoGas == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualLigacaoGasSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualLigacaoGasNao" value="0" name="exibirMenuAgenciaVirtualLigacaoGas"
                   <c:if test="${exibirMenuAgenciaVirtualLigacaoGas == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualLigacaoGasNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir programa��o de consumo (se cliente usa)</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualProgramacaoConsumoSim" value="1" name="exibirMenuAgenciaVirtualProgramacaoConsumo"
                   <c:if test="${exibirMenuAgenciaVirtualProgramacaoConsumo == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualProgramacaoConsumoSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualProgramacaoConsumoNao" value="0" name="exibirMenuAgenciaVirtualProgramacaoConsumo"
                   <c:if test="${exibirMenuAgenciaVirtualProgramacaoConsumo == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualProgramacaoConsumoNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir cromatografia</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualCromatografiaSim" value="1" name="exibirMenuAgenciaVirtualCromatografia"
                   <c:if test="${exibirMenuAgenciaVirtualCromatografia == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualCromatografiaSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualCromatografiaNao" value="0" name="exibirMenuAgenciaVirtualCromatografia"
                   <c:if test="${exibirMenuAgenciaVirtualCromatografia == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualCromatografiaNao">N�o</label>
            <br/><br/>

            <label class="rotulo rotulo2Linhas">Exibir fale conosco</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualFaleConoscoSim" value="1" name="exibirMenuAgenciaVirtualFaleConosco"
                   <c:if test="${exibirMenuAgenciaVirtualFaleConosco == '1'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualFaleConoscoSim">Sim</label>
            <input class="campoRadio" type="radio" id="exibirMenuAgenciaVirtualFaleConoscoNao" value="0" name="exibirMenuAgenciaVirtualFaleConosco"
                   <c:if test="${exibirMenuAgenciaVirtualFaleConosco == '0'}">checked="checked"</c:if>/>
            <label class="rotuloRadio" for="exibirMenuAgenciaVirtualFaleConoscoNao">N�o</label>
            <br/><br/>

        </fieldset>
    </fieldset>
</fieldset>			
