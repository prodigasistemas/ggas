<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<fieldset id="cadastro">
	<a class="linkHelp" href="<help:help>/abacadastroparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			
			<label class="rotulo rotulo2Linhas">Validar m�scara da inscri��o estadual?</label>
			<input  class="campoRadio" type="radio" value="1" name=indicadorValidarMascaraInscricaoEstadual <c:if test="${indicadorValidarMascaraInscricaoEstadual == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="indicadorValidarMascaraInscricaoEstadual">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="indicadorValidarMascaraInscricaoEstadual" <c:if test="${indicadorValidarMascaraInscricaoEstadual == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="indicadorValidarMascaraInscricaoEstadual">N�o</label>
			<br/>
			
			<label class="rotulo">M�scara da inscri��o estadual:</label>
			<input class="campoTexto campo2Linhas" type="text" name="mascaraInscricaoEstadualCadastro" maxlength="15" size="15" value="${mascaraInscricaoEstadualCadastro}"><br />
			
			<label class="rotulo rotulo2Linhas">Unidade Federa��o da CDL em que a aplica��o foi implantada:</label>
			<select name="unidadeFederacaoImplantacaoCadastro" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaUnidadeFederacao}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${unidadeFederacaoImplantacaoCadastro == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select><br />
			
		</fieldset>
		
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Tipo de endere�o correspond�ncia:</label>
			<select name="tipoEnderecoCorrespondenciaCadastro" class="campoSelect campo2Linhas">
		    	<c:forEach items="${listaTipoEndereco}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${tipoEnderecoCorrespondenciaCadastro == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select><br />
			
			<label class="rotulo">Esfera de Poder:</label>
			<select name="esferaPoderMinicipalCadastro" class="campoSelect">
		    	<c:forEach items="${listaEsferaPoder}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${esferaPoderMinicipalCadastro == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>
				</c:forEach>
			</select>
			<br/>
			
			<label class="rotulo rotulo2Linhas">Exige E-mail principal no cadastro de Pessoa?</label>
			<input  class="campoRadio" type="radio" value="1" name="exigeEmailPrincipalPessoa" <c:if test="${exigeEmailPrincipalPessoa == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="exigeEmailPrincipalPessoa" <c:if test="${exigeEmailPrincipalPessoa == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			
			
			<label class="rotulo">Exige Matr�cula do Funcion�rio?</label>
			<input  class="campoRadio" type="radio" value="1" name="exigeMatriculaFuncionario" <c:if test="${exigeMatriculaFuncionario == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="exigeMatriculaFuncionario">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="exigeMatriculaFuncionario" <c:if test="${exigeMatriculaFuncionario == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="exigeMatriculaFuncionario">N�o</label>
				
			<label class="rotulo rotulo2Linhas">Indica se o sistema obrigar� a inclus�o de Rota no cadastro do Ponto de Consumo:</label>
			<input  class="campoRadio" type="radio" value="1" name="aceitaInclusaoPontoConsumoRota" <c:if test="${aceitaInclusaoPontoConsumoRota == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="aceitaInclusaoPontoConsumoRota" <c:if test="${aceitaInclusaoPontoConsumoRota == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			
		</fieldset>
	</fieldset>
</fieldset>			