<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<script type="text/javascript">

$(document).ready(function(){
	
	$("#referenciaFaturamentoGeral").inputmask("m/y",{placeholder:"_"});
	$("#idServidorSmtpAuthGeralSim").click(campoVisivel);
    $("#idServidorSmtpAuthGeralNao").click(campoInvisivel);
	
});

animatedcollapse.addDiv('conteinerAutenticacaoSMTP');

function habilitarDesabilitarCampos(campo) {

	if (campo.value == '0') {
		
		animatedcollapse.hide('conteinerAutenticacaoSMTP');
	}else if (campo.value == '1') {
		
		animatedcollapse.show('conteinerAutenticacaoSMTP');
		
	}
}

function init(){

	habilitarDesabilitarCampos(document.parametroSistemaForm.servidorSmtpAuthGeral);
}

function campoInvisivel(){
	document.getElementById("campoUsuarioSmtp").value = "0";
	document.getElementById("campoSenhaSmtp").value = "0";
	document.getElementById("conteinerAutenticacaoSMTP").style.display = "none";
}

function campoVisivel(){
	var valorCampo = document.getElementById("campoUsuarioSmtp").value;
	if(valorCampo == 0){
		document.getElementById("campoUsuarioSmtp").value = "";     
		document.getElementById("campoSenhaSmtp").value = "";
	}
	document.getElementById("conteinerAutenticacaoSMTP").style.display = "block"; 

}



addLoadEvent(init);


</script>

<fieldset id="geral">
	<a class="linkHelp" href="<help:help>/abageralparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga" style="margin-right: 20px">
			
			<label class="rotulo rotulo2Linhas">Munic�pio onde o sistema est� implantado:</label>
			<select name="municipioImplantadoGeral" id="municipioImplantadoGeral" class="campoSelect campo2Linhas">
				<c:forEach items="${listaMunicipio}" var="municipioImplantado">
					<option value="<c:out value="${municipioImplantado.chavePrimaria}"/>" <c:if test="${municipioImplantadoGeral == municipioImplantado.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${municipioImplantado.descricao}"/>
					</option>
				</c:forEach>
			</select><br />
		
			<label class="rotulo">Hora que indica o in�cio do Dia:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="horaInicioDiaGeral" maxlength="9" size="8" value="${horaInicioDiaGeral}"  onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Quantidade de anos considerados no calend�rio:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="qtdAnosCalendarioGeral" maxlength="9" size="8"  value="${qtdAnosCalendarioGeral}" onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Quantidade de casas decimais utilizadas no c�lculo:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="qtdEscalaCalculoGeral" maxlength="9" size="8"  value="${qtdEscalaCalculoGeral}" onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Quantidade de casas decimais utilizadas para apresenta��o do consumo apurado:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="qtdDecimApresentConsumoApurado" maxlength="4" size="8"  value="${qtdDecimApresentConsumoApurado}" onkeypress="return formatarCampoInteiro(event,4);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Quantidade Anos Vig�ncia Feriado:</label>
			<input class="campoTexto campo2Linhas" type="text" name="quantidadeAnosVigenciaFeriadoGeral" maxlength="4" size="4"  value="${quantidadeAnosVigenciaFeriadoGeral}" onkeypress="return formatarCampoInteiro(event,4);">

			<label class="rotulo rotulo2Linhas">Refer�ncia para integra��o de nota e t�tulo</label>
			<select name="referenciaIntegracaoNotaTitulo" id="referenciaIntegracaoNotaTitulo" class="campoSelect campo2Linhas">
				<option value="1" <c:if test="${referenciaIntegracaoNotaTitulo == 1}">selected="selected"</c:if>>Cliente</option>
				<option value="2" <c:if test="${referenciaIntegracaoNotaTitulo == 2}">selected="selected"</c:if>>Contrato</option>
			</select><br />

			<label class="rotulo rotulo2Linhas">Tempo para atualiza��o autom�tica da lista de processos executados (em segundos):</label>
			<input class="campoTexto campo2Linhas" type="text"  name="tempoRefreshControleProcesso" maxlength="4" size="8" value="${tempoRefreshControleProcesso}" onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
		</fieldset>
		
		<fieldset id="conteinerSenha" class="colunaEsq">
			<fieldset class="conteinerDados" style="margin-bottom: 18px">
				<label class="rotulo">Ser� usado n�mero na senha:</label>
				<input  class="campoRadio" type="radio" value="1" name="caractereNumGeral" <c:if test="${caractereNumGeral == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caractereNum">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="caractereNumGeral" <c:if test="${caractereNumGeral == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caractereNum">N�o</label>
				<br class="quebraLinha" />
				
				<label class="rotulo">Ser� usado caractere min�sculo na senha:</label>
				<input  class="campoRadio" type="radio" value="1" name="caractereMinusculoGeral" <c:if test="${caractereMinusculoGeral == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caractereMinusculo">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="caractereMinusculoGeral" <c:if test="${caractereMinusculoGeral == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caractereMinusculo">N�o</label>
				<br class="quebraLinha" />
				
				<label class="rotulo">Ser� usado caractere mai�sculo na senha:</label>
				<input  class="campoRadio" type="radio" value="1" name="caractereMaiusculoGeral" <c:if test="${caractereMaiusculoGeral == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caractereMaiusculo">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="caractereMaiusculoGeral" <c:if test="${caractereMaiusculoGeral == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caractereMaiusculo">N�o</label>
				<br class="quebraLinha" />
				
				<label class="rotulo">Tamanho m�nimo da senha:</label>
				<input class="campoTexto" type="text"  name="tamMinSenhaGeral" maxlength="9" size="8"  value="${tamMinSenhaGeral}" onkeypress="return formatarCampoInteiro(event,9);">
				<br />
				
				<label class="rotulo">Tamanho m�ximo da senha:</label>
				<input class="campoTexto" type="text"  name="tamMaxSenhaGeral" maxlength="9" size="8"  value="${tamMaxSenhaGeral}" onkeypress="return formatarCampoInteiro(event,9);">
				<br />
				
				<label class="rotulo"> Dias para o usu�rio renovar a senha:</label>
				<input class="campoTexto" type="text"  name="periodicidadeSenhaRenovacaoGeral" maxlength="9" size="8"  value="${periodicidadeSenhaRenovacaoGeral}" onkeypress="return formatarCampoInteiro(event,9);">
			</fieldset>
			
			<label class="rotulo rotulo2Linhas">Tipo de arredondamento utilizado nas opera��es de c�lculo:</label>
			<select name="tipoArredondamentoCalculoGeral" id="tipoArredondamentoCalculoGeral" class="campoSelect">
				<c:forEach items="${listaMedidasArredondamento}" var="tipoValor">
					<option value="<c:out value="${tipoValor.chavePrimaria}"/>" <c:if test="${tipoArredondamentoCalculoGeral == tipoValor.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoValor.descricao}"/>
					</option>
				</c:forEach>
			</select><br />
			
			<label class="rotulo rotulo2Linhas">Aceita acentua��o em campos texto?</label>
				<input  class="campoRadio" type="radio" value="1" name="aceitaCaracterEspecial" <c:if test="${aceitaCaracterEspecial == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="aceitarAcentuacao">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="aceitaCaracterEspecial" <c:if test="${aceitaCaracterEspecial == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="aceitarAcentuacao">N�o</label>
			<br />
						
			<label class="rotulo rotulo2Linhas">Campos texto devem ser digitados apenas em CAIXA ALTA?</label>
				<input  class="campoRadio" type="radio" value="1" name="caixaAlta" <c:if test="${caixaAlta == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="aceitarAcentuacao">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="caixaAlta" <c:if test="${caixaAlta == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="aceitarAcentuacao">N�o</label>
			<br />
			
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadora1" />
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">		
		
		<label class="rotulo">E-mail padr�o:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="emailRemetenteDefaultGeral" maxlength="255" size="30"  value="${emailRemetenteDefaultGeral}">
			<br />
				
			<label class="rotulo">IP do servidor de Aplica��o:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="localHostGeral" maxlength="15" size="8"  value="${localHostGeral}" >
			<br />	
			
			<label class="rotulo">Servidor de Email Configurado:</label>
			<input id="servidorEmailConfigurado" class="campoRadio campoRadio2Linhas" type="radio" value="1" name="servidorEmailConfigurado"   <c:if test="${servidorEmailConfigurado == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="servidorEmailConfigurado">Sim</label>
			<input id="servidorEmailConfigurado" class="campoRadio campoRadio2Linhas" type="radio" value="0" name="servidorEmailConfigurado"  <c:if test="${servidorEmailConfigurado == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="servidorEmailConfigurado">N�o</label>
			<br />	

			<label class="rotulo rotulo2Linhas">Periodicidade de envio de email para chamados atrasados (em dias):</label>
			<input class="campoTexto campo2Linhas" type="text" onkeypress="apenasNumeros();" name="diasEmailChamadosAtrasados" maxlength="2" size="2"  value="${diasEmailChamadosAtrasados}" 
				style="margin-top: 15px;" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,2)');">
			<br />	

			<label class="rotulo" for="assinaturaEmail" style="text-align: left">Assinatura Email:</label>
			<textarea class="campoVertical" name="assinaturaEmail" id="assinaturaEmail" rows="5" cols="72" > ${assinaturaEmail}</textarea>

		</fieldset>
		
		<fieldset class="colunaFinal">
			
			<label class="rotulo">Porta do servidor de SMTP:</label>
			<input class="campoTexto" type="text"  name="servidorSmtpPortGeral" maxlength="9" size="8"  value="${servidorSmtpPortGeral}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,3)');">
			<br />
				
			<label class="rotulo">IP do servidor de SMTP:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="servidorSmtpHostGeral" maxlength="15" size="8"  value="${servidorSmtpHostGeral}" >
			<br />
			
			<label class="rotulo rotulo2Linhas">O servidor de SMTP requer autentica��o:</label>
			<input id="servidorSmptAuthGeral" class="campoRadio campoRadio2Linhas" type="radio" value="1" id="idServidorSmtpAuthGeralSim" name="servidorSmtpAuthGeral"   <c:if test="${servidorSmtpAuthGeral == '1'}">checked="checked"</c:if> onclick="campoVisivel();"/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="servidorSmptAuthGeral">Sim</label>
			<input id="servidorSmptAuthGeral" class="campoRadio campoRadio2Linhas" type="radio" value="0" id="idServidorSmtpAuthGeralNao" name="servidorSmtpAuthGeral" onclick="campoInvisivel();" <c:if test="${servidorSmtpAuthGeral == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="servidorSmptAuthGeral">N�o</label>
			
			<!-- tag para corre��o de alinhamento -->
			<div id="divConteinerAutenticacaoSMTP" style="margin-top:80px; width: 300px;"  >
				<fieldset id="conteinerAutenticacaoSMTP"  >
				
					<label id="usuarioSmtp" class="rotulo rotulo2Linhas" style="margin-right: 2px;">Usu�rio para acesso ao servidor de SMTP:</label>
					<input id="campoUsuarioSmtp"class="campoTexto campo2Linhas" type="text" name="servidorSmtpUserGeral" maxlength="9" size="8"  value="${servidorSmtpUserGeral}">
					
			
					<label id="senhaSmtp" class="rotulo rotulo2Linhas">Senha para acesso ao servidor de SMTP:</label>
					<input id="campoSenhaSmtp" class="campoTexto campo2Linhas" type="text"  name="servidorSmtpPassawordGeral" maxlength="9" size="8"  value="${servidorSmtpPassawordGeral}" >
				</fieldset>
			</div>
			<!-- tag para corre��o de alinhamento -->
			
			<label class="rotulo rotulo2Linhas">Ambiente � produ��o?:</label>
			<input id="indicadorAmbienteProducao" class="campoRadio campoRadio2Linhas" type="radio" value="1" id="idIndicadorAmbienteProducaoSim" name="indicadorAmbienteProducao"   <c:if test="${indicadorAmbienteProducao == '1'}">checked="checked"</c:if> onclick="campoVisivel();"/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="indicadorAmbienteProducao">Sim</label>
			<input id="indicadorAmbienteProducao" class="campoRadio campoRadio2Linhas" type="radio" value="0" id="idIndicadorAmbienteProducaoNao" name="indicadorAmbienteProducao" onclick="campoInvisivel();" <c:if test="${indicadorAmbienteProducao == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="indicadorAmbienteProducao">N�o</label>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadora1" />
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">			
			<label class="rotulo">Diret�rio dos Relat�rios Gerados:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="diretorioRelatorioPDF" maxlength="30" size="30"  value="${diretorioRelatorioPDF}">
			<br />
		
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo">Tamanho m�ximo de anexo (KB):</label>
			<input class="campoTexto" type="text"  name="tamanhoMaximoAnexo" maxlength="7" size="7"  value="${tamanhoMaximoAnexo}" onkeypress="return formatarCampoInteiro(event,7);">
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadora1" />
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas rotulo_auto_left">Envia SMS:</label>
			<input  class="campoRadio" type="radio" value="1" name="enviaSms" <c:if test="${enviaSms == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="enviaSms">Sim</label>
			<input  class="campoRadio" type="radio" value="2" name="enviaSms" <c:if test="${enviaSms == '2'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="enviaSms">N�o</label>

			<label class="rotulo rotulo_auto_left">Credencial Envio SMS :</label>
			<input class="campoTexto campo2Linhas" type="text"  name="credencialSms" maxlength="40" size="41"  value="${credencialSms}">
			<br />
			<label class="rotulo rotulo_auto_left">Projeto :</label>
			<input class="campoTexto campo2Linhas" type="text"  name="projetoSms" maxlength="30" size="30"  value="${projetoSms}">
			<br />
			<label class="rotulo rotulo_auto_left">Usuario Auxiliar :</label>
			<input class="campoTexto campo2Linhas" type="text"  name="usuarioSms" maxlength="30" size="30"  value="${usuarioSms}">
			<br />
			<label class="rotulo rotulo_auto_left">Mensagem :</label>
			<input class="campoTexto campo2Linhas" type="text"  name="mensagemSms" maxlength="100" size="41"  value="${mensagemSms}">
			<br />
			<label class="rotulo rotulo_auto_left">Proxy Host :</label>
			<input class="campoTexto campo2Linhas" type="text"  name="proxyHostSms" maxlength="15" size="8"  onkeypress="apenasNumeros();"  value="${proxyHostSms}">
			<br />
			<label class="rotulo rotulo_auto_left">Proxy Porta :</label>
			<input class="campoTexto campo2Linhas" type="text"  name="proxyPortaSms" maxlength="4" size="5"   onkeypress="apenasNumeros();" value="${proxyPortaSms}">
			<br />
		</fieldset>
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas rotulo_auto_left">Autentica��o por AD do Windows:</label>
			<input  class="campoRadio" type="radio" value="1" name="autenticacaoPorAD" <c:if test="${autenticacaoPorAD == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input  class="campoRadio" type="radio" value="2" name="autenticacaoPorAD" <c:if test="${autenticacaoPorAD == '2'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			<label class="rotulo rotulo_auto_left">Host :</label>
			<input class="campoTexto campo2Linhas campoAutAd" type="text"  name="servidorAutenticacaoAdHostGeral" maxlength="80" size="30"  value="${servidorAutenticacaoAdHostGeral}">
			<br />
			<label class="rotulo rotulo_auto_left">Porta :</label>
			<input class="campoTexto campo2Linhas campoAutAd" type="number"  id="servidorAutenticacaoAdPortaGeral" name="servidorAutenticacaoAdPortaGeral" maxlength="20" size="30"  value="${servidorAutenticacaoAdPortaGeral}">
			<br />
			<label class="rotulo rotulo_auto_left">Dom�nio :</label>
			<input class="campoTexto campo2Linhas campoAutAd" type="text"  name="servidorAutenticacaoAdDominioGeral" maxlength="80" size="30"  value="${servidorAutenticacaoAdDominioGeral}">
			<br />
		</fieldset>
	</fieldset>
	<hr class="linhaSeparadora1" />
	<label class="rotulo rotulo_auto_left">Latitude empresa:</label>
	<input class="campoTexto campo2Linhas campoAutAd" type="text"  name="latitudeEmpresa" maxlength="80" size="30"  value="${latitudeEmpresa}">
	<br />
	<label class="rotulo rotulo_auto_left">Longitude empresa :</label>
	<input class="campoTexto campo2Linhas campoAutAd" type="text"  name="longitudeEmpresa" maxlength="80" size="30"  value="${longitudeEmpresa}">
	<br />
	
</fieldset>
