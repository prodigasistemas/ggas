<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<h1 class="tituloInterno">Consultar Par�metros</h1>
<p class="orientacaoInicial">Selecione um par�metro da lista clicando no �cone <img id="iconeAlterar" src="<c:out value='${pageContext.request.contextPath}'/>/imagens/16x_editar.gif" /> respectivo.<br />
                 
<display:table class="dataTableGGAS" name="parametros" sort="list" id="parametro" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="consultarParametroSistema">
	<display:column property="codigo" sortable="true"  titleKey="PARAMETRO_SISTEMA_ROTULO_CODIGO" /> 
	<display:column property="descricao" sortable="true" titleKey="PARAMETRO_SISTEMA_ROTULO_DESCRICAO" /> 
	<display:column property="valor" sortable="true" titleKey="PARAMETRO_SISTEMA_ROTULO_VALOR"/> 
	<display:column property="tipoParametroSistema.descricao" sortable="true" titleKey="PARAMETRO_SISTEMA_ROTULO_TIPO"/>
	<display:column style="text-align: center;"  href="exibirFormAlteracaoParametroSistema" paramProperty="chavePrimaria" id="chavePrimaria"> 
		<img src="<c:url value="/imagens/16x_editar.gif"/>" border="0">
	</display:column>
</display:table>
 
