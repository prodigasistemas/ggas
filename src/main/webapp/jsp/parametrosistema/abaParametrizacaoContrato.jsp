<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<fieldset id="contrato">
	<a class="linkHelp" href="<help:help>/abacontratoparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Ano utilizado para numerar novos contratos:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="anoNovosContratos" maxlength="9" size="8" value="${anoNovosContratos}"  onkeypress="return formatarCampoInteiro(event,4);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas">Sequ�ncia da numera��o de contrato:</label>
			<input class="campoTexto campo2Linhas" type="text"  name="seqNumContrato" maxlength="9" size="8"  value="${seqNumContrato}" onkeypress="return formatarCampoInteiro(event,10);">
		</fieldset>
		
		<!-- NOVAS REGRAS CSS PARA AJUSTE NO IE9 -->
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Reiniciar a sequ�ncia da numera��o de contrato anualmente:</label>
			<input id="reiniciarSeqNumContratoAnualContrato" class="campo1Radio campoRadio10Linhas" type="radio" value="1" name="reiniciarSeqNumContratoAnualContrato" <c:if test="${reiniciarSeqNumContratoAnualContrato == '1'}">checked="checked"</c:if>/>
			<label class="rotulo1Radio rotuloRadio8Linhas" for="reiniciarSeqNumContratoAnualContrato">Sim</label>
			<input id="naoReiniciarSeqNumContratoAnualContrato" class="campo1Radio campoRadio10Linhas" type="radio" value="0" name="reiniciarSeqNumContratoAnualContrato" <c:if test="${reiniciarSeqNumContratoAnualContrato == '0'}">checked="checked"</c:if>/>
			<label class="rotulo1Radio rotuloRadio8Linhas" for="naoReiniciarSeqNumContratoAnualContrato">N�o</label>
		<br class="quebraLinha2" />
		<!-- NOVAS REGRAS CSS PARA AJUSTE NO IE9 -->
			
			<label class="rotulo">Ano Contratual Padr�o:</label>
			<input id="anoContratualMaiusculoContrato" class="campoRadio" type="radio" value="1" name="indicadorAnoContratualContrato" <c:if test="${indicadorAnoContratualContrato == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="anoContratualMaiusculoContrato">ANO(Fiscal)</label>
			<input id="anoContratualMinusculoContrato" class="campoRadio" type="radio" value="0" name="indicadorAnoContratualContrato" <c:if test="${indicadorAnoContratualContrato == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="anoContratualMinusculoContrato">ano(Calend�rio)</label>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadora1" />
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna2 detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Tempo de anteced�ncia em dias para alerta de renova��o contratual:</label>
			<input class="campoTexto campo3Linhas" type="text" name="diasAlertaRenovacaoContratualContrato" maxlength="9" size="8" value="${diasAlertaRenovacaoContratualContrato}" onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
			
		</fieldset>
		
		<fieldset class="colunaFinal2">
			<label class="rotulo rotuloVertical">Observa��o que ser� impressa com a nota de d�bito:</label>
			<textarea class="campoVertical" name="obsImpressaNotaDebitoContrato" rows="4" cols="72" > ${obsImpressaNotaDebitoContrato}</textarea> 
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadora1" />
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo rotulo2Linhas">Periodicidade de Referencia para valida��o do percentual de TOP e QPNR:</label>
			<input class="campoTexto campo3Linhas" type="text" id="idPeriodicidadeContrato" name="idPeriodicidadeContrato" maxlength="9" size="8" value="${idPeriodicidadeContrato}" onkeypress="return formatarCampoInteiro(event,9);">
			<br class="quebraLinha2" />
			
			<label class="rotulo rotulo2Linhas"> Per�odo de testes deve ser considerado:</label>
			<input  class="campoRadio campoRadio2Linhas" type="radio" value="1" name="considerarPeriodoTestesContrato" <c:if test="${considerarPeriodoTestesContrato == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="considerarPeriodoTestesContrato">Sim</label>
			<input  class="campoRadio campoRadio2Linhas" type="radio" value="0" name="considerarPeriodoTestesContrato" <c:if test="${considerarPeriodoTestesContrato == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio2Linhas" for="naoConsiderarPeriodoTestesContrato">N�o</label>
			<br class="quebraLinha2" />
				
		</fieldset>
			
		<fieldset class="colunaFinal">
			
			<label class="rotulo rotulo2Linhas"> Tarifa utilizada para cobran�a do valor devido pela utiliza��o da infra-estrutura de log�stica de entrega do G�S pela QR:</label>
			<select name="idTarifaRecCompromissoContrato" id="idTarifaRecCompromissoContrato" class="campoSelect campo4Linhas">
				<c:forEach items="${listaTarifas}" var="sitRecupContrato">
					<option value="<c:out value="${sitRecupContrato.chavePrimaria}"/>" <c:if test="${idTarifaRecCompromissoContrato == sitRecupContrato.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${sitRecupContrato.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>
				
	<hr class="linhaSeparadora1" />
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			
			<label class="rotulo rotulo2Linhas"> Motivo do fim do relacionamento entre o cliente e o contrato quando o mesmo for encerrado:</label>
			<select name="motivoFimRelacionamentoCliEncerrarContrato" id="motivoFimRelacionamentoCliEncerrarContrato" class="campoSelect campo3Linhas">
				<c:forEach items="${listaMotivoFimRelacionamentoClienteImovel}" var="sitRecupContrato">
					<option value="<c:out value="${sitRecupContrato.chavePrimaria}"/>" <c:if test="${motivoFimRelacionamentoCliEncerrarContrato == sitRecupContrato.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${sitRecupContrato.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotulo2Linhas">Enviar Email Ao Salvar Proposta?</label>
			<input  class="campoRadio" type="radio" value="1" name="enviaPropostaEmail" <c:if test="${enviaPropostaEmail == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="aceitarAcentuacao">Sim</label>
			<input  class="campoRadio" type="radio" value="0" name="enviaPropostaEmail" <c:if test="${enviaPropostaEmail == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio" for="aceitarAcentuacao">N�o</label>
			
			<label class="rotulo">Sequ�ncia Proposta:</label>
			<input class="campoTexto campo2Linhas" type="text" name="sequenciaProposta" id="sequenciaProposta" maxlength="15" size="15" value="${sequenciaProposta}"><br />
			
			<label class="rotulo rotulo2Linhas">Obrigat�rio Contato do Im�vel Para Inclus�o Proposta?:</label>
			<input class="campoRadio" type="radio" value="1" name="contatoImovelObrigatorio" <c:if test="${contatoImovelObrigatorio == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" value="0" name="contatoImovelObrigatorio" <c:if test="${contatoImovelObrigatorio == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			
			<label class="rotulo rotulo2Linhas">Ativar/Desativar Ponto de Consumo na Cria��o/Encerramento Contrato?</label>
			<input class="campoRadio" type="radio" value="1" name="desativarPontoContrato" <c:if test="${desativarPontoContrato == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" value="0" name="desativarPontoContrato" <c:if test="${desativarPontoContrato == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			
			<label class="rotulo rotulo2Linhas">Considerar Anomalia de Faturamento no Encerramento Contrato?</label>
			<input class="campoRadio" type="radio" value="1" name="considerarAnomaliaFaturamentoContrato" <c:if test="${considerarAnomaliaFaturamentoContrato == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" value="0" name="considerarAnomaliaFaturamentoContrato" <c:if test="${considerarAnomaliaFaturamentoContrato == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>			
			
	
		</fieldset>
		
	</fieldset>
</fieldset>