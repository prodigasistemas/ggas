<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>
	function salvar() {

		if (confirm('Confirma as altera��es ?')) {
			exibirIndicador();
			submeter('parametroSistemaForm', 'alterarParametros');
		}

	}

	function validarCriteriosParaCampo(campo, criterioCaixaAlta, criterioChar,
			funcao) {

		pos = getCaretPosition(campo);

		if (criterioCaixaAlta == "1") {
			campo.value = campo.value.toUpperCase();
		}
		if (criterioChar != "1") {
			campo.value = replaceSpecialChars(campo.value);
			campo.value = removerExtraEspacoInicialParametros(campo.value);
		}

		setCaretPosition(campo, pos);
	}

	function cancelar() {

		exibirIndicador();
		window.location = "exibirHome";

	}

	function selecionarAba(nomeAba) {

		document.forms[0].abaSelecionada.value = nomeAba;

	}

	function init() {

		var abaSelecionada = document.forms[0].abaSelecionada.value;

		if (abaSelecionada != '') {

			var $tabs = $('#tabs').tabs();
			$tabs.tabs('select', abaSelecionada);

		}

	}

	addLoadEvent(init);
</script>

<h1 class="tituloInterno">Parametriza��o<a class="linkHelp" href="<help:help>/controledosparmetros.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form id="formParametrizacao" method="post" name="parametroSistemaForm">
	<input name="abaSelecionada" type="hidden" value="${abaSelecionada}"/>
		    
	<fieldset id="tabs" style="display: none">
		
		<ul>
			<li><a href="#geral" onclick="selecionarAba('geral');">Geral</a></li>
			<li><a href="#cadastro" onclick="selecionarAba('cadastro');">Cadastro</a></li>
			<li><a href="#contrato" onclick="selecionarAba('contrato');">Contrato</a></li>
			<li><a href="#medicao" onclick="selecionarAba('medicao');">Medi��o</a></li>
			<li><a href="#faturamento" onclick="selecionarAba('faturamento');">Faturamento</a></li>
			<li><a href="#arrecadacao" onclick="selecionarAba('arrecadacao');">Arrecada��o</a></li>
			<li><a href="#cobranca" onclick="selecionarAba('cobranca');">Cobran�a</a></li>
			<li><a href="#contabilidade" onclick="selecionarAba('contabilidade');">Contabilidade</a></li>
			<li><a href="#atendimento" onclick="selecionarAba('atendimento');">Atendimento</a></li>
			<li><a href="#agencia" onclick="selecionarAba('agencia');">Ag�ncia Virtual</a></li>
			<li><a href="#seguranca" onclick="selecionarAba('seguranca');">Seguran�a</a></li>
		</ul>
				
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoGeral.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoCadastro.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoContrato.jsp"%>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoMedicao.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoFaturamento.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoArrecadacao.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoCobranca.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoContabilidade.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoAtendimento.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoAgenciaVirtual.jsp" %>
		<%@ include file="/jsp/parametrosistema/abaParametrizacaoSeguranca.jsp" %>
		
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Cancelar" type="button" onclick="cancelar();">
		<vacess:vacess param="alterarParametros.do">
			<input name="Button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="salvar();">
		</vacess:vacess>
	</fieldset>
<token:token></token:token>
</form:form>

<script src="${ctxWebpack}/dist/modulos/configuracao/parametrizacao/index.js"></script>
