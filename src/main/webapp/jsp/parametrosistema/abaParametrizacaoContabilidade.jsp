<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<fieldset id="contabilidade">
	<a class="linkHelp" href="<help:help>/abacadastroparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			
			<label class="rotulo">M�scara do n�mero da conta:</label>
			<input class="campoTexto campo2Linhas" type="text" name="mascaraNumeroConta" maxlength="20" size="20" value="${mascaraNumeroConta}"><br />
			
			<label class="rotulo rotulo2Linhas">Indica se o desconto da tarifa ser� contabilizado:</label>
			<input  class="campoRadio campoRadio3Linhas" type="radio" value="1" name="indContabilizaDescontoTarifa" <c:if test="${indContabilizaDescontoTarifa == '1'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas" for="indContabilizaDescontoTarifa">Sim</label>
			<input  class="campoRadio campoRadio3Linhas" type="radio" value="0" name="indContabilizaDescontoTarifa" <c:if test="${indContabilizaDescontoTarifa == '0'}">checked="checked"</c:if>/>
			<label class="rotuloRadio rotuloRadio3Linhas" for="indContabilizaDescontoTarifa">N�o</label>
			
			<fmt:setLocale value="pt_BR"/>
			<label class="rotulo rotulo2Linhas">Valor limite para caracterizar PDD no ano (R$):</label>
			<fmt:parseNumber value="${pddValorLimiteAnual}" parseLocale="pt_BR" var="pddAnual"/>
			<fmt:formatNumber value="${pddAnual}" pattern="#,##0.00" var="pddAnualFormatado"/>
			<input class="campoTexto campo2Linhas" name="pddValorLimiteAnual" type="text" maxlength="24" value="${pddAnualFormatado}" ><br />
			
			<label class="rotulo rotulo2Linhas">Valor limite para caracterizar PDD no semestre (R$):</label>
			<fmt:parseNumber value="${pddValorLimiteSemestral}" parseLocale="pt_BR" var="pddSemestral"/>
			<fmt:formatNumber value="${pddSemestral}" pattern="#,##0.00" var="pddSemestralFormatado"/>
			<input class="campoTexto campo2Linhas" name="pddValorLimiteSemestral" type="text" maxlength="24" value="${pddSemestralFormatado}" ><br />
			
			<label class="rotulo rotulo2Linhas">Ano e M�s da Refer�ncia Cont�bil:</label>
			<input class="campoTexto campo2Linhas" type="text" name="referenciaContabilGeral" maxlength="7" size="7" value="${referenciaContabilGeral}" onkeypress="return formatarCampoInteiro(event,7);" />
						
		</fieldset>
	</fieldset>
</fieldset>			