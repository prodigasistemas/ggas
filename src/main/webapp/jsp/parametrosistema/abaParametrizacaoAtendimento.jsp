<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<style>
	.questionarioSatisfacao {
		width: 300px;
	}
	.perguntaSatisfacao{
		width: 300px;
		margin-top: 20px;
	}
</style>

<script>
var perguntaId = undefined;
var questionarioId = undefined;
$(document).ready(function(){
	<c:if test="${ questionarioPerguntaSatisfacao ne null }">
		questionarioId = <c:out value="${ questionarioPerguntaSatisfacao.questionario.chavePrimaria }" /> ;
		perguntaId = <c:out value="${ questionarioPerguntaSatisfacao.chavePrimaria }" /> ;
		
		$("#questionarioSatisfacao option").each(function(){
			if ($(this).val() == questionarioId){
				$(this).attr("selected", "selected");
				listarPerguntas();
			}
		});
	</c:if>	
});

function listarPerguntas(){
	var questionarioId = $("#questionarioSatisfacao").val();
	var perguntas = $("#perguntas");

	perguntas.empty();

	if (questionarioId > 0){
		$("<label>").text("Pergunta do question�rio referente a satisfa��o: ").addClass("rotulo rotulo2Linhas questionarioSatisfacao").appendTo(perguntas);
		var select = $("<select name='perguntaSatisfacao' id='perguntaSatisfacao'>").addClass("perguntaSatisfacao").appendTo(perguntas);
		
		AjaxService.carregaListaPerguntas( questionarioId,{
       		callback:function(listaPerguntas) {

           		for (pergunta in listaPerguntas) {
               		var selected = "";
               		if ( listaPerguntas[pergunta]['id'] == perguntaId){
						selected = "selected='selected'"
               		}
               		var opt = $("<option value='"+listaPerguntas[pergunta]['id']+"' " + selected + " >" + listaPerguntas[pergunta]["descricao"] + "</option>").appendTo(select);
           		}
               	
        	}, 
        	async:false
       	});
	}
}
</script>

<fieldset id="atendimento">
	<a class="linkHelp" href="<help:help>/abacadastroparametrizao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	
	<fieldset class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			
			<label class="rotulo rotulo2Linhas">Gera��o autom�tica de Protocolo:</label>
			<input  class="campoRadio" type="radio" value="1" name="protocoloAutomatico" <c:if test="${protocoloAutomatico == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="protocoloAutomatico">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="protocoloAutomatico" <c:if test="${protocoloAutomatico == '0'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="protocoloAutomatico">N�o</label>
				<br/><br/>
				<label class="rotulo rotulo2Linhas">Modo de Acesso Agencia Por Contrato:</label>
			    <input  class="campoRadio" type="radio" value="1" name="acessoAgencia" <c:if test="${acessoAgencia == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="acessoAgencia">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="acessoAgencia" <c:if test="${acessoAgencia == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="acessoAgencia">N�o</label>	
				<br/><br/>
				<label class="rotulo">Tamanho maximo senha Agencia:</label>
			    <input class="campoTexto campo2Linhas" type="text" name="tamanhoMaxSenhaAg" maxlength="2" size="3" value="${tamanhoMaxSenhaAg}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,2)');">
			    <br/><br/>
				<label class="rotulo">Tamanho minimo senha Agencia:</label>
			    <input class="campoTexto campo2Linhas" type="text" name="tamanhoMinSenhaAg" maxlength="2" size="3" value="${tamanhoMinSenhaAg}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,2)');">
			    <br/><br/>
			    <label class="rotulo rotulo2Linhas">Caracter Maiusculo Senha Agencia:</label>
			    <input  class="campoRadio" type="radio" value="1" name="caracterMaiusculoAg" <c:if test="${caracterMaiusculoAg == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caracterMaiusculoAg">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="caracterMaiusculoAg" <c:if test="${caracterMaiusculoAg == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caracterMaiusculoAg">N�o</label>	
			    <br/><br/>
			    <label class="rotulo rotulo2Linhas">Caracter Minusculo Senha Agencia:</label>
			    <input  class="campoRadio" type="radio" value="1" name="caracterMinusculoAg" <c:if test="${caracterMinusculoAg == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caracterMinusculoAg">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="caracterMinusculoAg" <c:if test="${caracterMinusculoAg == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="caracterMinusculoAg">N�o</label>	
			    <br/><br/>
				<label class="rotulo rotulo2Linhas">Exige Manifesta��o Obrigatoria:</label>
			    <input  class="campoRadio" type="radio" value="1" name="manifestacao" <c:if test="${manifestacao == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="manifestacao">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="manifestacao" <c:if test="${manifestacao == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="manifestacao">N�o</label>
				<br/><br/>
				<label class="rotulo rotulo2Linhas">Exibe Passaporte:</label>
			    <input  class="campoRadio" type="radio" value="1" name="passaporte" <c:if test="${passaporte == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="passaporte">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="passaporte" <c:if test="${passaporte == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="passaporte">N�o</label>	
				<br/><br/>
				<label class="rotulo rotulo2Linhas">Obrigatoriedade do Campo Telefone Solicita��o de G�s:</label>
			    <input  class="campoRadio" type="radio" value="1" name="obrigatoriedadeTelefone" <c:if test="${obrigatoriedadeTelefone == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="passaporte">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="obrigatoriedadeTelefone" <c:if test="${obrigatoriedadeTelefone == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="passaporte">N�o</label>	
				
		</fieldset>
		 <fieldset style="padding-left: 20px">
			<label class="rotulo rotulo2Linhas" for="questionarioSatisfacao">Question�rio de satisfa��o:</label>
			
			<select name="questionarioSatisfacao" id="questionarioSatisfacao" onChange="listarPerguntas()">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaQuestionario}" var="questionario">
					<option value="${questionario.chavePrimaria}">${questionario.nomeQuestionario}</option>
				</c:forEach>
			</select>
			<br />
			<div id="perguntas">
				
			</div>
		</fieldset> 
		<hr class="linhaSeparadora1" />
		<fieldset class="colunaFinal">
		<label class="rotulo rotulo2Linhas">Quantidade de Meses Programa��o Consumo:</label>
			    <input class="campoTexto" type="text" name="quantidadeMesesProgramacaoConsumo" maxlength="2" size="3" value="${quantidadeMesesProgramacaoConsumo}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,2)');">
			    <br/><br/>
			    <label class="rotulo rotulo2Linhas">Dia limite Programa��o Consumo Mensal:</label>
			    <input class="campoTexto" type="text" name="diaLimiteProgramacaoConsumo" maxlength="2" size="3" value="${diaLimiteProgramacaoConsumo}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,2)');">
			    <br/><br/>
			    
			     <label class="rotulo rotulo2Linhas">Quantidade de Dias Limite para Aprova��o Mensal:</label>
			    <input class="campoTexto" type="text" name="diaLimiteAprovacaoProgramacaoConsumo" maxlength="2" size="3" value="${diaLimiteAprovacaoProgramacaoConsumo}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,2)');">
			    <br/><br/>
			    
				<label class="rotulo rotulo2Linhas">Mensagem de erro para QDS maior que o tolerado pelo QDC (Ag�ncia Virtual):</label>
				<textarea class="campoVertical" name="msgErroQdsMaiorQueQdcTolerado" 
					rows="4" cols="72" style="clear: none;" 
					maxlength="250"  onblur="this.value = removerEspacoInicialFinal(this.value);"
					onkeypress="return formatarCampoTextoLivreComLimite(event,this,250);" 
					onpaste="return formatarCampoTextoLivreComLimite(event,this,250);"
					>${msgErroQdsMaiorQueQdcTolerado}</textarea>
			    <br/><br/>
			    
			   <hr class="linhaSeparadora1" />
			   
			   <label class="rotulo rotulo2Linhas">Altera��o de Vencimento Autom�tica:</label>
			    <input  class="campoRadio" type="radio" value="1" name="vencimentoAutomatico" <c:if test="${vencimentoAutomatico == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="vencimentoAutomatico">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="vencimentoAutomatico" <c:if test="${vencimentoAutomatico == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="vencimentoAutomatico">N�o</label>	
			    <br/><br/>
				<label class="rotulo rotulo2Linhas">Altera��o Vencimento com D�bitos em Atraso:</label>
			    <input  class="campoRadio" type="radio" value="1" name="vencimentoDebitoAtraso" <c:if test="${vencimentoDebitoAtraso == '1'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="vencimentoDebitoAtraso">Sim</label>
				<input  class="campoRadio" type="radio" value="2" name="vencimentoDebitoAtraso" <c:if test="${vencimentoDebitoAtraso == '2'}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="vencimentoDebitoAtraso">N�o</label>
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Prazo para altera��o da data de vencimento:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="prazoAlteracaoVencimento" name="prazoAlteracaoVencimento" maxlength="5" size="7" value="${prazoAlteracaoVencimento}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
			   <br/><br/>
			   <label class="rotulo rotulo2Linhas"> Informe o n�mero m�ximo de dias para o religamento do g�s por desliga��o indevida:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="dataLimiteReligacaoGasPorDesligamentoIndevido" name="dataLimiteReligacaoGasPorDesligamentoIndevido" maxlength="5" size="7" value="${dataLimiteReligacaoGasPorDesligamentoIndevido}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
			   <br/><br/>
			   
			   <label class="rotulo rotulo2Linhas"> Informe o n�mero m�ximo de dias para o religamento do g�s:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="dataLimiteReligacaoGas" name="dataLimiteReligacaoGas" maxlength="5" size="7" value="${dataLimiteReligacaoGas}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');" >
			   <br/><br/>
			    <label class="rotulo rotulo2Linhas"> Informe o n�mero m�ximo de dias para a liga��o do g�s de baixa press�o:</label>
			     <input class="campoTexto campo3Linhas" type="text" id=dataLimiteLigacaoGasBaixaPressao name="dataLimiteLigacaoGasBaixaPressao" maxlength="5" size="7" value="${dataLimiteLigacaoGasBaixaPressao}" onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');" >
				<br/><br/>
				
			    <label class="rotulo rotulo2Linhas"> Informe o n�mero m�ximo de dias para a liga��o do g�s de media press�o:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="dataLimiteLigacaoGasMediaPressao" name="dataLimiteLigacaoGasMediaPressao" maxlength="5" size="7" value="${dataLimiteLigacaoGasMediaPressao}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Informe o n�mero m�ximo de dias para a liga��o do g�s de alta press�o:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="dataLimiteLigacaoGasAltaPressao" name="dataLimiteLigacaoGasAltaPressao" maxlength="5" size="7" value="${dataLimiteLigacaoGasAltaPressao}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Informe o n�mero de dias para checagem se um Ponto de Consumo foi cortado:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="numeroDiasChecagemCorte" name="numeroDiasChecagemCorte" maxlength="5" size="7" value="${numeroDiasChecagemCorte}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Informe o n�mero de dias m�nimo para o crit�rio da criticidade:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="prazoMinimoCriticidade" name="prazoMinimoCriticidade" maxlength="5" size="7" value="${prazoMinimoCriticidade}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Informe o n�mero de dias maximo para o crit�rio da criticidade:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="prazoMaximoCriticidade" name="prazoMaximoCriticidade" maxlength="5" size="7" value="${prazoMaximoCriticidade}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Informe o valor da criticidade para n�mero de dias abaixo do m�nimo para o crit�rio de criticidade:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="valorMinimoCriticidade" name="valorMinimoCriticidade" maxlength="5" size="7" value="${valorMinimoCriticidade}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Informe o valor da criticidade para n�mero de dias entre m�nimo e o m�ximo de dias para o crit�rio de criticidade:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="valorMedioCriticidade" name="valorMedioCriticidade" maxlength="5" size="7" value="${valorMedioCriticidade}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
				
				<label class="rotulo rotulo2Linhas"> Informe o valor da criticidade para n�mero de dias acima do m�ximo de dias para o crit�rio de criticidade:</label>
			     <input class="campoTexto campo3Linhas" type="text" id="valorMaximoCriticidade" name="valorMaximoCriticidade" maxlength="5" size="7" value="${valorMaximoCriticidade}"  onkeyup="return validarCriteriosParaCampo(this, '0', '0', 'formatarCampoInteiro(event,5)');">
				<br/><br/>
			   
		</fieldset>
	</fieldset>
</fieldset>			
