<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Alterar Par�metros</h1>
<p class="orientacaoInicial">Para alterar este registro, informe o Valor do par�metro no campo abaixo.<br />

<form:form method="post" action="alterarParametroSistema" > 
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="<c:out value='${parametroSistemaForm.map.chavePrimaria}'/>" />
	<input name="versao" type="hidden" id="versao" value="<c:out value='${parametroSistemaForm.map.versao}'/>" />
	<input name="acao" type="hidden" id="acao" value="alterarParametroSistema">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="habilitado" type="hidden" id="habilitado" value="<c:out value='${parametroSistemaForm.map.habilitado}'/>" />
	
	<fieldset id="alterarParametrosSistema" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo" id="rotuloCodigo1" for="codigo2">C�digo:</label>
		<input name="codigo" type="hidden" id="codigo" value="<c:out value='${parametroSistemaForm.map.codigo}'/>">
		<input class="campoTexto" name="codigo2" type="text" id="codigo2" disabled value="<c:out value='${parametroSistemaForm.map.codigo}'/>" size="30" maxlength="30"><br />
	
		<label class="rotulo" id="rotuloDescricao2" for="descricao2" >Descri��o:</label>
		<input  class="campoTexto"name="descricao" type="hidden" id="descricao" value="<c:out value='${parametroSistemaForm.map.descricao}'/>"/>
		<input  class="campoTexto"name="descricao2" type="text" id="descricao2" disabled value="<c:out value='${parametroSistemaForm.map.descricao}'/>" size="50" maxlength="255"><br />
		
		<label class="rotulo campoObrigatorio" id="rotuloValor" for="valor" ><span class="campoObrigatorioSimbolo">* </span>Valor:</label>
		<input  class="campoTexto"name="valor" type="text" id="valor" value="<c:out value='${parametroSistemaForm.map.valor}'/>" size="20" maxlength="255"><br />
		
		<label class="rotulo" id="rotuloDescricao2" for="descricao2" >Tipo:</label>
		<span class="itemDetalhamento"><c:out value='${parametro.tipoParametroSistema.descricao}'/></span><br />
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campo obrigat�rio para alterar a Configura��o do Par�metro.</p>
	</fieldset>
	
	              
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol" type="button" id="button" value="<fmt:message key="ACAO_CANCELAR"/>" onClick="history.back(-1);">
		<input type="submit" class="bottonRightCol2 botaoGrande1" name="Submit" value="<fmt:message key="ACAO_ALTERAR"/>">
	</fieldset>
</form:form> 