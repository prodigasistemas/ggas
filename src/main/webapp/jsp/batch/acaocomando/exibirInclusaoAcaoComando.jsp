<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">
$(document).ready(function(){
	$(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	habilitarDesabilitarModalidadeMedicao();
	var idCityGate = '${acaoComando.cityGate.chavePrimaria}';
	var idTronco = '${acaoComando.tronco.chavePrimaria}';
	carregarTronco(idCityGate, idTronco);
});
function habilitarDesabilitarModalidadeMedicao(){ 
	if(document.forms['acaoComandoForm'].indicadorCondominio[0].checked== '1'){
		$("#modalidadeMedicao").show();
	}else{
		//Para n�o ficar com valor quando for escondido
		$("#modalidadeMedicaoImovel").val(-1);
		$("#modalidadeMedicao").hide();
	}
}

function incluir() {
	submeter('acaoComandoForm','incluirAcaoComando');
}

function cancelar() {
	submeter('acaoComandoForm','exibirPesquisaAcaoComando');
}


function limparFormulario(){
}
function carregarTronco(idCityGate, idTronco) {	
	
  	var selectTronco = document.getElementById("tronco");

  	selectTronco.length=0;
  	var novaOpcao = new Option("Selecione","-1");
  	selectTronco.options[selectTronco.length] = novaOpcao;
  	  
  	if(idCityGate != '-1' && idCityGate != null && idCityGate!='' && idCityGate!='0'){	      		
       	AjaxService.listarTroncoPorCityGate( idCityGate, {
       		callback:function(listaTronco) {            		      		         		
       			for (tronco in listaTronco){
               		for (key in listaTronco[tronco]){
	                	var novaOpcao = new Option(listaTronco[tronco][key], key);
               			if(key==idTronco){
               				novaOpcao.selected="selected";
               			}
	                	selectTronco.options[selectTronco.length] = novaOpcao;
               		}
            	}
       			selectTronco.disabled = false;
            	$("#tronco").removeClass("campoDesabilitado");
        	}, async:false}
        );
    } else {	    	
    	selectTronco.disabled = true;
    	$("#tronco").addClass("campoDesabilitado");
    }          	
}

</script>
<style>
	.campoSelectAno {min-width:0px !important;}
 	.rotuloA {width: 6% !important; } 
 	.rotuloA2 {width: 2% !important; } 
 	#segmento{margin:0 0 0 0 !important;}
 	.conteinerComandoAcao{height: 170px !important;}
 	.conteinerComandoAcaoLinha2{height: 140px !important;}
</style>
<c:if test="${ fluxoAlteracao eq true }">
	<h1 class="tituloInterno">Alterar Comando de A��o</h1>
	<p class="orientacaoInicial">Altere os dados e clique em <span class="destaqueOrientacaoInicial">Salvar</span>
	    para alterar o comando a��o.<br/>Para cancelar clique em <span class="destaqueOrientacaoInicial">Cancelar</span>.<br />
	</p>
</c:if>

<c:if test="${ fluxoInclusao eq true }">
<h1 class="tituloInterno">Incluir Comando de A��o<a href="<help:help>/localidadeinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>
</c:if>

<form:form method="post" modelAttribute="AcaoComandoVO" action="incluirAcaoComando" id="acaoComandoForm" name="acaoComandoForm">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${acaoComando.chavePrimaria}">
	<input type="hidden" name="chavesPrimaria" id="chavesPrimaria" value="${acaoComando.chavePrimaria}">
	<input type="hidden" name="habilitado" id="habilitado" value="${acaoComando.habilitado}">
	<input type="hidden" name="versao" id="versao" value="${acaoComando.versao}">
	<fieldset id="pesquisaServicoTipoCol1" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<fieldset class="coluna">	
				<label class="rotulo campoObrigatorio" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
				<input class="campoTexto" type="text" name="nome" id="nome" maxlength="30" size="20" value="${acaoComando.nome}" onkeyup="letraMaiuscula(this);"/><br />
				<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
				<textarea  class="campoTexto" name="descricao" id="descricao" cols="35" rows="3" maxlength="150" onblur="this.value = removerEspacoInicialFinal(this.value);"
					onkeypress="return formatarCampoTextoLivreComLimite(event,this,100);" onpaste="return formatarCampoTextoLivreComLimite(event,this,1000);" onkeyup="letraMaiuscula(this);">${acaoComando.descricao}</textarea>
				<br/>
				<label class="rotulo campoObrigatorio" for="acaoPrecedente"><span class="campoObrigatorioSimbolo">* </span>A��o:</label>
		  		<select id="acao" class="campoSelect" name="acao">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaAcoes}" var="acaoPrecedente">
					<option value="<c:out value="${acaoPrecedente.chavePrimaria}"/>" <c:if test="${acaoComando.acao.chavePrimaria == acaoPrecedente.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${acaoPrecedente.descricao}"/>
					</option>
					</c:forEach>
				</select>
			<label class="rotulo" for="indicadorEquipamento"><span class="campoObrigatorioSimbolo">* </span>Atividade:</label>
			<input class="campoRadio" type="radio" name="indicadorSimulacao" id="indicadorSimulacao" value="false"   <c:if test="${acaoComando.indicadorSimulacao eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Gerar</label>
			<input class="campoRadio" type="radio" name="indicadorSimulacao" id="indicadorSimulacao" value="true"   <c:if test="${acaoComando.indicadorSimulacao eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Simular</label>
			
			</fieldset>
			</fieldset>
			
			<hr class="linhaSeparadora1" />
			
	 	<fieldset class="coluna">	 	
		<fieldset id="tarifaVigencia" class="conteinerDados conteinerComandoAcao">
	 		<label class="rotulo campoObrigatorio" for="acaoPrecedente">Grupo Faturamento:</label>
		  		<select id="grupoFaturamento" class="campoSelect" name="grupoFaturamento">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaGrupoFaturamento}" var="grupoFaturamento">
					<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${acaoComando.grupoFaturamento.chavePrimaria == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${grupoFaturamento.descricao}"/>
					</option>
					</c:forEach>
				</select>
			<label class="rotulo campoObrigatorio" for="localidade">Localidade:</label>
		  		<select id="localidade" class="campoSelect" name="localidade">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaLocalidade}" var="localidade">
					<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${acaoComando.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${localidade.descricao}"/>
					</option>
					</c:forEach>
				</select>
				
			<label class="rotulo campoObrigatorio" for="acaoPrecedente">Setor Comercial:</label>
		  		<select id="setorComercial" class="campoSelect" name="setorComercial">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSetorComercial}" var="setorComercial">
					<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${acaoComando.setorComercial.chavePrimaria == setorComercial.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${setorComercial.descricao}"/>
					</option>
					</c:forEach>
				</select>		

			<label class="rotulo campoObrigatorio" for="municipio">Municipio:</label>
		  		<select id="municipio" class="campoSelect" name="municipio">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaMunicipio}" var="municipio">
					<option value="<c:out value="${municipio.chavePrimaria}"/>" <c:if test="${acaoComando.municipio.chavePrimaria == municipio.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${municipio.descricao}"/>
					</option>
					</c:forEach>
				</select>		
		</fieldset>
	</fieldset>
	<fieldset class="colunaFinal colunaChamadoAssunto">	 	
		<fieldset id="tarifaVigencia" class="conteinerDados conteinerComandoAcao">
		 	<label class="rotulo campoObrigatorio" for="marcaMedidor">Marca Medidor:</label>
	  		<select id="marcaMedidor" class="campoSelect" name="marcaMedidor">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMarcaMedidor}" var="marcaMedidor">
				<option value="<c:out value="${marcaMedidor.chavePrimaria}"/>" <c:if test="${acaoComando.marcaMedidor.chavePrimaria == marcaMedidor.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${marcaMedidor.descricao}"/>
				</option>
				</c:forEach>
			</select>
	
		 	<label class="rotulo campoObrigatorio" for="modeloMedidor">Modelo Medidor:</label>
	  		<select id="modeloMedidor" class="campoSelect" name="modeloMedidor">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaModeloMedidor}" var="modeloMedidor">
				<option value="<c:out value="${modeloMedidor.chavePrimaria}"/>" <c:if test="${acaoComando.modeloMedidor.chavePrimaria == modeloMedidor.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modeloMedidor.descricao}"/>
				</option>
				</c:forEach>
			</select>
	
		 	<label class="rotulo campoObrigatorio" for="marcaCorretor">Marca Corretor de Vaz�o:</label>
	  		<select id="marcaCorretor" class="campoSelect" name="marcaCorretor">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMarcaCorretor}" var="marcaCorretor">
				<option value="<c:out value="${marcaCorretor.chavePrimaria}"/>" <c:if test="${acaoComando.marcaCorretor.chavePrimaria == marcaCorretor.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${marcaCorretor.descricao}"/>
				</option>
				</c:forEach>
			</select>
		 	<label class="rotulo campoObrigatorio" for="modeloCorretor">Modelo Corretor de Vaz�o:</label>
	  		<select id="modeloCorretor" class="campoSelect" name="modeloCorretor">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaModeloCorretor}" var="modeloCorretor">
				<option value="<c:out value="${modeloCorretor.chavePrimaria}"/>" <c:if test="${acaoComando.modeloCorretor.chavePrimaria == modeloCorretor.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modeloCorretor.descricao}"/>
				</option>
				</c:forEach>
			</select>
	 		
	
		 	<label class="rotulo campoObrigatorio">Ano de Fabrica��o do Medidor:</label>
	  		<select id="anoFabricacaoInicio" class="campoSelect campoSelectAno" name="anoFabricacaoInicio" >
				<option value="-1">Selecione</option>
				<c:forEach items="${listaAnoFabricacaoInicio}" var="anoFabricacaoInicio">
				<option value="<c:out value="${anoFabricacaoInicio}"/>" <c:if test="${acaoComando.anoFabricacaoInicio == anoFabricacaoInicio}">selected="selected"</c:if>>
					<c:out value="${anoFabricacaoInicio}"/>
				</option>
				</c:forEach>
			</select>
			<label class="rotuloHorizontal rotuloA">a</label> 
	  		<select id="anoFabricacaoFim" class="campoSelect campoSelectAno" name="anoFabricacaoFim">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaAnoFabricacaoFim}" var="anoFabricacaoFim">
				<option value="<c:out value="${anoFabricacaoFim}"/>" <c:if test="${acaoComando.anoFabricacaoFim == anoFabricacaoFim}">selected="selected"</c:if>>
					<c:out value="${anoFabricacaoFim}"/>
				</option>
				</c:forEach>
			</select>
			<label class="rotulo ajusteRotuloAs">Per�odo de Instala��o do Medidor: </label>
			<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataInstalacaoInicio" name="dataInstalacaoInicio" maxlength="10" value="${acaoComando.dataInstalacaoInicioFormatada}" onblur="validaData(this);">
			<label class="rotuloEntreCampos rotuloA2">a</label>
			<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataInstalacaoFim" name="dataInstalacaoFim" maxlength="10" value="${acaoComando.dataInstalacaoFimFormatada}" onblur="validaData(this);">
			<br />
		</fieldset>
	</fieldset>
	<hr class="linhaSeparadora1" />
			
	<fieldset class="coluna">	 	
		<fieldset id="tarifaVigencia" class="conteinerDados conteinerComandoAcaoLinha2">
			<label class="rotulo" for="indicadorEquipamento">Indicador de Condom�nio:</label>
			<input class="campoRadio" onclick="habilitarDesabilitarModalidadeMedicao();" type="radio" name="indicadorCondominio" id="indicadorCondominio" value="true"   <c:if test="${acaoComando.indicadorCondominio eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio" onclick="habilitarDesabilitarModalidadeMedicao();" type="radio" name="indicadorCondominio" id="indicadorCondominio" value="false"   <c:if test="${acaoComando.indicadorCondominio eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
			
			<div id="modalidadeMedicao">
				<label class="rotulo campoObrigatorio" id="rotuloModalidademMedicao" for="modalidadeMedicaoImovel">Modalidade de Medi��o:</label>
				<select name="modalidadeMedicaoImovel" id="modalidadeMedicaoImovel" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaModalidadeMedicaoImovel}" var="modalidade">
						<option value="<c:out value="${modalidade.codigo}"/>" <c:if test="${acaoComando.modalidadeMedicaoImovel.codigo == modalidade.codigo}">selected="selected"</c:if>>
							<c:out value="${modalidade.descricao}"/>
						</option>		
					</c:forEach>
				</select>
			</div>
			
	 		<label class="rotulo campoObrigatorio" for="situacaoConsumo">Situa��o de Liga��o:</label>
		  		<select id="situacaoConsumo" class="campoSelect" name="situacaoConsumo">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSituacaoConsumo}" var="situacaoConsumo">
					<option value="<c:out value="${situacaoConsumo.chavePrimaria}"/>" <c:if test="${acaoComando.situacaoConsumo.chavePrimaria == situacaoConsumo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${situacaoConsumo.descricao}"/>
					</option>
					</c:forEach>
				</select>

	 		<label class="rotulo campoObrigatorio" for="qtdPontoConsumo">Qtd.: Pontos de Consumo</label>
			<input class="campoTexto" type="text" name="qtdPontoConsumo" id="qtdPontoConsumo" maxlength="30" size="20" value="${acaoComando.qtdPontoConsumo}" onkeyup="letraMaiuscula(this);"/><br />

	 		<label class="rotulo campoObrigatorio" for="segmento">Segmento:</label>
		  		<select id="segmento" class="campoSelect" name="segmento">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${acaoComando.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>
					</c:forEach>
				</select>
				
				
				</fieldset>
		</fieldset>
		<fieldset class="colunaFinal colunaChamadoAssunto">	 	
			<fieldset id="tarifaVigencia" class="conteinerDados conteinerComandoAcaoLinha2">
			
		 		<label class="rotulo campoObrigatorio" for="cityGate">City Gate:</label>
		  		<select id="cityGate" class="campoSelect" name="cityGate" onchange="carregarTronco(this.value,null)">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaCityGateAcao}" var="cityGate">
					<option value="<c:out value="${cityGate.chavePrimaria}"/>" <c:if test="${acaoComando.cityGate.chavePrimaria == cityGate.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${cityGate.descricao}"/>
					</option>
					</c:forEach>
				</select>
		 		<label class="rotulo campoObrigatorio" for="tronco">Tronco:</label>
				<select name="tronco" id="tronco" class="campoSelect" <c:if test="${empty listaTronco}">disabled="disabled""</c:if>>
					<option value="-1">Selecione</option>
				</select>
		 		<label class="rotulo campoObrigatorio" for="zonaBloqueio">Zona de Bloqueio:</label>
				<select name="zonaBloqueio" id="zonaBloqueio" class="campoSelect" <c:if test="${empty listaZonaBloqueio}">disabled="disabled""</c:if>>
					<option value="-1">Selecione</option>
					<c:forEach items="${listaZonaBloqueio}" var="zonaBloqueio">
						<option value="<c:out value="${zonaBloqueio.chavePrimaria}"/>" <c:if test="${acaoComando.zonaBloqueio.chavePrimaria == zonaBloqueio.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${zonaBloqueio.descricao}"/>
						</option>		
					</c:forEach>
				</select>
				
			</fieldset>
		</fieldset>
	</fieldset>
</form:form>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
<%-- 	    <vacess:vacess param="incluirServicoTipo"> --%>
	    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit" onclick="incluir();">
<%-- 	    </vacess:vacess>	 --%>
 	</fieldset>
