<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">


function incluir() {
	submeter('acaoComandoForm','exibirInclusaoAcaoComando');
}

function pesquisar() {
	submeter('acaoComandoForm','pesquisarAcaoComando');
}

function detalharAcao(chave){
	document.forms[0].chavesPrimaria.value = chave;
	submeter('acaoComandoForm','exibirDetalhamentoAcaoComando');
}


function limparFormulario(){
	document.forms[0].nome.value="";
	document.forms[0].descricao.value="";
	
}

function removerAcaoComando(){
	
	var selecao = verificarSelecao();	
	
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('acaoComandoForm', 'removerAcaoComando');
		}
    }
}

function alterarAcaoComando() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms[0].chavesPrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('acaoComandoForm','exibirAlteracaoAcaoComando');
    }
	
}

</script>

<h1 class="tituloInterno">Pesquisar Comando de A��o</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" modelAttribute="AcaoComandoVO" action="incluirAcaoComando" id="acaoComandoForm" name="acaoComandoForm">
	<fieldset id="pesquisaServicoTipoCol1" class="conteinerPesquisarIncluirAlterar">
	
		<input name="chavesPrimaria" type="hidden" id="chavesPrimaria" > 	
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" > 	
		<fieldset class="conteinerBloco">
			<fieldset class="coluna">	
				<label class="rotulo campoObrigatorio" for="nome">Nome:</label>
				<input class="campoTexto" type="text" name="nome" id="nome" maxlength="30" size="20" value="${acaoComando.nome}" onkeyup="letraMaiuscula(this);"/><br />
				<label class="rotulo campoObrigatorio" for="descricao">Descri��o:</label>
				<textarea  class="campoTexto" name="descricao" id="descricao" cols="35" rows="3" maxlength="150" onblur="this.value = removerEspacoInicialFinal(this.value);"
					onkeypress="return formatarCampoTextoLivreComLimite(event,this,100);" onpaste="return formatarCampoTextoLivreComLimite(event,this,1000);" onkeyup="letraMaiuscula(this);">${acaoComando.descricao}</textarea>
				
				<label class="rotulo campoObrigatorio" for="acao">A��o:</label>
		  		<select id="acao" class="campoSelect" name="acao">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaAcoes}" var="acaoPrecedente">
					<option value="<c:out value="${acaoPrecedente.chavePrimaria}"/>" <c:if test="${acaoComando.acao.chavePrimaria == acaoPrecedente.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${acaoPrecedente.descricao}"/>
					</option>
					</c:forEach>
				</select>
				<label class="rotulo" for="habilitado">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${acaoComando.habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="indicadorUso">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${acaoComando.habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="indicadorUso">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitadoTodos" value="" <c:if test="${acaoComando.habilitado eq ''}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Todos</label>
			
			</fieldset>
		</fieldset>
	</fieldset>
		<fieldset class="conteinerBotoesPesquisarDirFixo"> 
	    	<input name="botaoPesquisar" class="bottonRightCol2" value="Pesquisar"  type="submit" onclick="pesquisar();">
	    	<input name="botaoLimpar" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
 	</fieldset>
	<c:if test="${listaAcaoComando ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaAcaoComando" sort="list" id="acao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${acao.chavePrimaria}">
	        </display:column>
			<display:column sortable="true" sortProperty="nome" title="Nome" style="text-align: center; width: 130px">
				<a href="javascript:detalharAcao(<c:out value='${acao.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${acao.nome}"/>
	            </a>
			</display:column>				
			<display:column sortable="true" sortProperty="descricao" title="Descri��o" maxLength="50">
				<a href="javascript:detalharAcao(<c:out value='${acao.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${acao.descricao}"/>
	            </a>
			</display:column>				
			<display:column sortable="true" sortProperty="acao" title="A��o de Cobran�a" style="width: 130px">
				<a href="javascript:detalharAcao(<c:out value='${acao.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${acao.acao.descricao}"/>
	            </a>
			</display:column>				
	    </display:table>	
	</c:if>
</form:form>
	
 	<fieldset class="conteinerBotoes"> 
		<input id="botaoAlterar" name="Button" class="bottonRightCol2" value="Alterar" type="button" onClick="alterarAcaoComando();">
	    <input id="botaoRemover" name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Remover" type="button" onclick="removerAcaoComando();">
<%-- 	    <vacess:vacess param="incluirServicoTipo"> --%>
	    	<input id="botaoIncluir" name="button" class="bottonRightCol2 botaoGrande1" value="Incluir"  type="submit" onclick="incluir();">
<%-- 	    </vacess:vacess>	 --%>
 	</fieldset>