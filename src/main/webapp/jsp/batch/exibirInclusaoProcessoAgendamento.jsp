<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>
	function carregarRotas(elem) {
		var codGrupoFaturamento = elem.value;
		carregarRotasPorGrupo(codGrupoFaturamento);
	}

	function carregarRotasPorGrupo(codGrupoFaturamento){
		if (codGrupoFaturamento > 0) {
			var selectRotas = document.getElementById("rotasSelecionadas");
			var rotasSelecionadas = [];
			<c:forEach items="${processoVO.rotasSelecionadas}" var="rota">
			rotasSelecionadas.push( parseInt(<c:out value = "${rota}"/>));
			</c:forEach>
			selectRotas.length=0;
			var novaOpcao =  null;
			selectRotas.options[selectRotas.length] = novaOpcao;
			AjaxService.listarRotaPorGrupoFaturamento(codGrupoFaturamento,
					function(rotas) {
						for (key in rotas){
							var novaOpcao = new Option(rotas[key], key);
							if ($.inArray(parseInt(key), rotasSelecionadas) >= 0){
								novaOpcao.selected = true;
							}
							selectRotas.options[selectRotas.length] = novaOpcao;
						}
						ordernarSelect(selectRotas)
					}
			);
		}
	}

	function limparFormulario() {
		document.getElementById('idModulo').value = '-1';
		document.getElementById('idOperacao').value = '-1';
		document.getElementById('idPeriodicidade').value = '-1';
		document.getElementById('diaNaoUtilNao').checked = true;
		document.getElementById('dataAgendada').value = '';
		document.getElementById('dataAgendadaFinal').value = '';
		document.getElementById('hora').value = '-1';
		document.getElementById('emailResponsavel').value = '';
		$('#parametrosCronogramaFaturamento').hide(400);
		$('#rotasSelecionadas').children().attr("selected", true);
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaProcesso"/>';
	}

	function carregarParametros(elem) {

		var emailResponsavel = document.getElementById("emailResponsavel");
		var idOperacao = null;
		if (elem.value != undefined) {
			idOperacao = elem.value;
		} else {
			idOperacao = elem;
		}
		if (idOperacao != "-1") {
			AjaxService.obterAtividadeCronogramaFaturamento(idOperacao, {
				callback : function(dados) {
					if (dados["horaInicio"] != undefined) {
						document.getElementById("horaInicio").value = dados["horaInicio"];
					}
					if (dados["horaFim"] != undefined) {
						document.getElementById("horaFim").value = dados["horaFim"];
					}
					if (dados["emailExecucao"] != undefined) {
						emailResponsavel.value = dados["emailExecucao"];
					} else {
						emailResponsavel.value = "";
					}
					if (dados["ehAtividadeCronograma"] == 'true') {
						$('#parametrosCronogramaFaturamento').show(400);
					} else {
						$('#parametrosCronogramaFaturamento').hide(400);
					}
					if (dados["isFaturarGrupo"] == 'true') {
						$('#parametrosCronogramaFaturamento').show(400);
					}

					restringirComboHora(dados["horaInicio"],
							dados["horaFim"]);

				}, async : false});
		} else {
			emailResponsavel.value = "";
			$('#parametrosCronogramaFaturamento').hide(400);
		}
	}

	function restringirComboHora(horaInicio, horaFim) {

		var selectHora = document.getElementById("hora")
		var idHora = "${processoVO.hora}";

		selectHora.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectHora[selectHora.length] = novaOpcao;

		if ((horaInicio != undefined) && (horaFim != undefined)) {

			for (var i = parseInt(horaInicio); i <= parseInt(horaFim); i++) {
				novaOpcao = new Option(i, i);
				if ((idHora != "") && (i == idHora)) {
					novaOpcao.selected = true;
				}
				selectHora[selectHora.length] = novaOpcao;
			}

		} else if ((horaInicio != undefined) && (horaFim == undefined)) {

			for (var i = parseInt(horaInicio); i <= 23; i++) {
				novaOpcao = new Option(i, i);
				if ((idHora != "") && (i == idHora)) {
					novaOpcao.selected = true;
				}
				selectHora[selectHora.length] = novaOpcao;
			}

		} else if ((horaInicio == undefined) && (horaFim != undefined)) {

			for (var i = 0; i <= parseInt(horaFim); i++) {
				novaOpcao = new Option(i, i);
				if ((idHora != "") && (i == idHora)) {
					novaOpcao.selected = true;
				}
				selectHora[selectHora.length] = novaOpcao;
			}

		} else {

			for (var i = 0; i <= 23; i++) {
				novaOpcao = new Option(i, i);
				if ((idHora != "") && (i == idHora)) {
					novaOpcao.selected = true;
				}
				selectHora[selectHora.length] = novaOpcao;
			}
		}
	}

	function carregarOperacoes(elem) {

		var idModulo = elem.value;

		var selectOperacoes = document.getElementById("idOperacao");
		var idOperacao = '${processo.operacao.chavePrimaria}';
		var emailResponsavel = document.getElementById("emailResponsavel");

		selectOperacoes.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectOperacoes[selectOperacoes.length] = novaOpcao;
		emailResponsavel.value = "";

		if (idModulo != "-1") {
			selectOperacoes.disabled = false;
			AjaxService.consultarOperacoesBatchPorModulo(idModulo,
					function(listaOperacoes) {
						for (key in listaOperacoes) {
							var novaOpcao = new Option(
									listaOperacoes[key], key);
							if (idOperacao == key) {
								novaOpcao.selected = true;
							}
							selectOperacoes.options[selectOperacoes.length] = novaOpcao;
						}
					});
		} else {
			selectOperacoes.disabled = true;
			emailResponsavel.value = "";
			$('#parametrosCronogramaFaturamento').hide(400);
		}
	}

	function carregarHoraExecucao(elem) {

		var idOperacao = null;
		if (elem.value != undefined) {
			idOperacao = elem.value;
		} else {
			idOperacao = elem;
		}

		if (idOperacao != "-1") {
			AjaxService
					.obterAtividadeCronogramaFaturamento(
							idOperacao,
							{
								callback : function(dados) {
									if (dados["horaInicio"] != undefined) {
										document.getElementById("horaInicio").value = dados["horaInicio"];
									}
									if (dados["horaFim"] != undefined) {
										document.getElementById("horaFim").value = dados["horaFim"];
									}
									restringirComboHora(dados["horaInicio"],
											dados["horaFim"]);

								},
								async : false
							}

					);
		}
	}

	function init() {

		var idModulo = document.getElementById("idModulo");
		var idOperacao = '${processo.operacao.chavePrimaria}';

		if (idModulo != undefined) {
			carregarOperacoes(idModulo);

			if (idOperacao != "") {
				carregarHoraExecucao(idOperacao);
				carregarParametros(idOperacao);
			}
		}
		if (idOperacao != undefined) {
			document.getElementById('idOperacao').disable = false;
		}
	}

	$(document).ready(function(){
		$(".campoData").datepicker({
			minDate : '+0d',
			changeYear : true,
			yearRange : '<c:out value="${intervaloAnosData}"/>',
			showOn : 'button',
			buttonImage: $('#imagemCalendario').val(),
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy'
		});

		init();
		carregarRotasPorGrupo(${processoVO.idGrupoFaturamento});
	});
	
	function selecionarRotasGrupoFaturamento() {
		
		var parametrosCronogramaFaturamento = $("#parametrosCronogramaFaturamento").attr("style");
		var rotasSelecionadas = $("#rotasSelecionadas option:selected").val();
		var idGrupoFaturamento = $("#idGrupoFaturamento").val();
			
		if( parametrosCronogramaFaturamento == "" ){
				
			if( idGrupoFaturamento > 0 && typeof rotasSelecionadas == 'undefined' ) {
					
				$('#rotasSelecionadas').children().attr("selected", true);
					
			}
		}
		
	}
	
</script>

<div class="bootstrap">
	<form:form method="post" action="incluirProcessoAgendamento">

		<input name="acao" type="hidden" id="acao" value="incluirProcessoAgendamento"/>
		<input name="versao" type="hidden" id="versao" value="<c:out value='${processo.versao}'/>" />
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="imagemCalendario" type="hidden" id="imagemCalendario" value="<c:url value="/imagens/calendario.png"/>">

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Agendar Processos</h5>
			</div>
			<div class="card-body">

				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i>
					Para agendar um processo, informe os dados abaixo e clique em <b>Agendar</b>
				</div>

				<div class="col-xl-12">
					<div class="row">
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label for="idModulo">M�dulo:<span class="text-danger">*</span></label>
							<select name="modulo" id="idModulo" class="form-control form-control-sm" onchange="carregarOperacoes(this);">
								<option value="-1">Selecione</option>
								<c:forEach items="${modulos}" var="modulo">
									<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${processoVO.modulo == modulo.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${modulo.descricao}"/>
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label for="idOperacao">Processo:<span class="text-danger">*</span></label>
							<select name="operacao" id="idOperacao" class="form-control form-control-sm" disabled="disabled" onchange="carregarParametros(this);" >
								<option value="-1">Selecione</option>
								<c:forEach items="${operacoesBatchs}" var="operacao">
									<option value="<c:out value="${operacao.chavePrimaria}"/>" <c:if test="${processo.operacao.chavePrimaria == operacao.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${operacao.descricao}"/>
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label for="idPeriodicidade">Periodicidade:<span class="text-danger">*</span></label>
							<select name="periodicidade" id="idPeriodicidade" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
								<c:forEach items="${periodicidades}" var="periodicidade">
									<option value="<c:out value="${periodicidade.codigo}"/>" <c:if test="${processo.periodicidade == periodicidade.codigo}">selected="selected"</c:if>>
										<c:out value="${periodicidade.descricao}"/>
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-xl-3 col-lg-6 col-md-12">
							<div class="row">
								<div class="col-md-12">
									<label>Permite dia n�o �til?</label>
								</div>
								<div class="col-md-12">
									<div class="custom-control custom-radio custom-control-inline">
										<input class="custom-control-input" type="radio" name="diaNaoUtil" id="diaNaoUtilSim" value="true"
											   <c:if test="${processo.diaNaoUtil eq 'true'}">checked</c:if>>
										<label class="custom-control-label" for="diaNaoUtilSim">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input class="custom-control-input" type="radio" name="diaNaoUtil" id="diaNaoUtilNao" value="false"
											   <c:if test="${processo.diaNaoUtil ne 'true'}">checked</c:if>>
										<label class="custom-control-label" for="diaNaoUtilNao">N�o</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label for="dataAgendada">Data In�cio Agendamento:<span class="text-danger">*</span></label>
							<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
								<input class="form-control form-control-sm campoData" type="text" id="dataAgendada"
									   name="dataInicioAgendamento" maxlength="10" value="${processoVO.dataInicioAgendamento}">
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label for="dataAgendadaFinal">Data Final Agendamento:</label>
							<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
								<input class="form-control form-control-sm campoData" type="text" id="dataAgendadaFinal"
									   name="dataFinalAgendamento" maxlength="10" value="${processoVO.dataFinalAgendamento}">
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label for="hora">Hora de execu��o:</label>
							<select id="hora" class="form-control form-control-sm" name="hora" <ggas:campoSelecionado campo="hora"/> >
								<option value="-1">Selecione</option>
								<c:forEach begin="0" end="23" var="hora">
									<option value="${hora}" <c:if test="${processoVO.hora == hora}">selected="selected"</c:if>><c:out value="${hora}"/></option>
								</c:forEach>
							</select>
							<small class="form-text text-muted">Intervalo permitido para o processo selecionado</small>
							<input type="hidden" id="horaInicio" name="horaInicio" value="${processoVO.horaInicio}">
							<input type="hidden" id="horaFim" name="horaFim" value="${processoVO.horaFim}">
						</div>
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label for="emailResponsavel">E-mail p/ envio resultado:</label>
							<textarea id="emailResponsavel" name="emailResponsavel" class="form-control form-control-sm" cols="50" rows="2"
									  onkeypress="return formatarCampoEmailMultiplo(event);">
								<c:out value="${processo.emailResponsavel}"/>
							</textarea><br>
							<small class="form-text text-muted">Separar e-mails por ponto e v�gula ( ; )</small>
						</div>
					</div>
					<div id="parametrosCronogramaFaturamento" style="display: none" class="row">
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label id="rotuloGrupoFaturamento" for="idGrupoFaturamento" style="white-space: nowrap;">Grupo de Faturamento:</label>
							<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="form-control form-control-sm" onchange="carregarRotas(this)">
								<option value="-1">Selecione</option>
								<c:forEach items="${gruposFaturamento}" var="grupo">
									<option value="<c:out value="${grupo.chavePrimaria}"/>"
											<c:if test="${processoVO.idGrupoFaturamento == grupo.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${grupo.descricao}"/>
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-xl-3 col-lg-6 col-md-12">
							<label id="rotuloRota" for="rotasSelecionadas">Rota:</label>
							<select name="rotasSelecionadas" id="rotasSelecionadas" class="form-control form-control-sm" multiple="multiple">
								<c:forEach items="${listaRotas}" var="rota">
									<option value="<c:out value="${rota.chavePrimaria}"/>"
											<c:if test="${processoVO.idRota == rota.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${rota.numeroRota}"/>
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<div class="form-row mt-2">
					<div class="col-md-12">
						<div class="row justify-content-between">
							<div class="col-md-6 col-sm-12 mt-1">
								<button name="button" class="btn btn-default btn-sm mb-1 mr-1" type="button" onclick="cancelar()">
									<i class="fa fa-reply"></i> Cancelar
								</button>
								<button name="button" class="btn btn-danger btn-sm mb-1 mr-1" type="button" onclick="limparFormulario()">
									<i class="fa fa-times"></i> Limpar
								</button>
							</div>
							<vacess:vacess param="incluirProcessoAgendamento">
								<div class="col-md-6 col-sm-12 mt-1 text-md-right">
									<button id="botaoSalvar" name="button" class="btn btn-primary btn-sm mb-1 mr-1" type="submit" onclick="selecionarRotasGrupoFaturamento()">
										<i class="fa fa-clock"></i> Agendar
									</button>
								</div>
							</vacess:vacess>
						</div>
					</div>
				</div>
			</div>
		</div>
		<token:token></token:token>
	</form:form>
</div>
