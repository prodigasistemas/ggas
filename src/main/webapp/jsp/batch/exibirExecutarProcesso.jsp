<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>
	function carregarRotas(elem) {
		var codGrupoFaturamento = elem.value;
		carregarRotasPorGrupo(codGrupoFaturamento);
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaProcesso"/>';
	}

	function limparFormulario(){
		document.getElementById('idModulo').value = '-1';
		document.getElementById('idOperacao').value = '-1';
		document.getElementById('emailResponsavel').value = '';
		document.getElementById('idGrupoFaturamento').value = '-1';
		document.getElementById('rotasSelecionadas').value = '-1';
		document.getElementById('idComando').value = '-1';
		$('.parametrosCronogramaFaturamento').hide(400);
		$('.cronogramaFaturamento2').hide(400);
		$('.parametrosRetornoNfe').hide(400);
	}

	function carregarRotasPorGrupo(codGrupoFaturamento){
		if (codGrupoFaturamento > 0) {
			var selectRotas = document.getElementById("rotasSelecionadas");
			var idRota = "${processoVO.idRota}";

			selectRotas.length=0;
			var novaOpcao =  null;
			selectRotas.options[selectRotas.length] = novaOpcao;
			AjaxService.listarRotaPorGrupoFaturamento(codGrupoFaturamento,
					function(rotas) {
						for (key in rotas){
							var novaOpcao = new Option(rotas[key], key);
							if (key == idRota){
								novaOpcao.selected = true;
							}
							selectRotas.options[selectRotas.length] = novaOpcao;
						}
						ordernarSelect(selectRotas)
					}
			);
		}
	}

	function carregarStatusNFE(){
		var selectStatusNFE = document.getElementById("idStatusNFE");
		var idStatusNFE = "${processoVO.idStatusNFE}";

		selectStatusNFE.length=0;
		var novaOpcao = new Option("Selecione","-1");
		selectStatusNFE.options[selectStatusNFE.length] = novaOpcao;
		AjaxService.listarStatusNfe(
				function(statusNFE) {
					for (key in statusNFE){
						var novaOpcao = new Option(statusNFE[key], key);
						if (key == idStatusNFE){
							novaOpcao.selected = true;
						}
						selectStatusNFE.options[selectStatusNFE.length] = novaOpcao;
					}
				}
		);
	}


	function carregarParametros(elem){
		var idOperacao = elem.value;
		var emailResponsavel = document.getElementById("emailResponsavel");

		if (idOperacao != "-1") {
			AjaxService.obterAtividadeCronogramaFaturamento( idOperacao, {
				callback: function(dados) {
					if (dados != null) {
						if (dados["ehAtividadeCronograma"] == 'true') {
							$('.parametrosCronogramaFaturamento').show(400);
						} else {
							$('.parametrosCronogramaFaturamento').hide(400);
						}
						if (dados["isFaturarGrupo"] == 'true') {
							$('.parametrosCronogramaFaturamento').show(400);
							$('.cronogramaFaturamento2').show(400);
						} else {
							$('.cronogramaFaturamento2').hide(400);
						}
						if (dados["emailExecucao"] != undefined) {
							emailResponsavel.value = dados["emailExecucao"];
						} else {
							emailResponsavel.value = "";
						}
						if (dados["isIntegrarNFE"] == 'true') {
							$('.parametrosRetornoNfe').show(400);
							carregarStatusNFE();
						}
						if(dados["isGerarArquivosSEFAZ"] == 'true') {
							$('.parametrosArquivosSefaz').show(400);
						}
						if(dados["isRetornoNfe"] === 'true'){
							$('.parametrosRetornoNfe').show(400);
						}

					}
				}, async:false}

			);
		} else {
			$('.cronogramaFaturamento2').hide(400);
			$('.parametrosCronogramaFaturamento').hide(400);
			$('.parametrosIntegracaoNFE').hide(400);
			$('.parametrosArquivosSefaz').hide(400);
			$('.parametrosRetornoNfe').hide(400);

			emailResponsavel.value = "";
		}
	}

	function carregarOperacoes(elem){

		var idModulo = elem.value;

		var selectOperacoes = document.getElementById("idOperacao");
		var idOperacao = '${processo.operacao.chavePrimaria}';

		var emailResponsavel = document.getElementById("emailResponsavel");

		selectOperacoes.length=0;
		var novaOpcao = new Option("Selecione","-1");
		selectOperacoes[selectOperacoes.length] = novaOpcao;

		emailResponsavel.value = "";

		if (idModulo != "-1") {
			selectOperacoes.disabled = false;
			AjaxService.consultarOperacoesBatchPorModulo( idModulo,
					function(listaOperacoes) {
						for (key in listaOperacoes){

							var novaOpcao = new Option(listaOperacoes[key], key);
							if (idOperacao == key) {
								novaOpcao.selected = true;
							}
							selectOperacoes.options[selectOperacoes.length] = novaOpcao;
						}

						//Ordena os processos em rela��o ao texto
						textoArray = new Array();
						valorArray = new Array();
						originalArray = new Array();

						textoArray[0] = "0. Selecione"
						valorArray[0] = -1;
						for(i=1; i<selectOperacoes.length; i++){
							textoArray[i] = selectOperacoes.options[i].text;
							valorArray[i] = selectOperacoes.options[i].value;
							originalArray[i] = selectOperacoes.options[i].text;
						}


						textoArray.sort();

						textoArray[0] = "Selecione";
						for(i=0; i<selectOperacoes.length; i++){
							selectOperacoes.options[i].text = textoArray[i];
							for(j=0; j<selectOperacoes.length; j++)
								if (textoArray[i] == originalArray[j])
									selectOperacoes.options[i].value = valorArray[j];

						}


						for (key in listaOperacoes){
							if (idOperacao == key) {
								$("#idOperacao option[value='" + idOperacao + "']").attr("selected","selected");
								carregarComando(idOperacao);
							}
						}

						carregarParametros(document.getElementById("idOperacao"));
					}
			);
		} else {
			selectOperacoes.disabled = true;
			$('.cronogramaFaturamento2').hide(400);
			$('.parametrosCronogramaFaturamento').hide(400);
			$('.parametrosIntegracaoNFE').hide(400);
			$('.parametrosArquivosSefaz').hide(400);
			$('.parametrosRetornoNfe').hide(400);
			emailResponsavel.value = "";
		}
		
	}
	function init() {
		$("#campoDataRetornoNfe" ).change(function() {
			$("#dataEmissao").val($("#campoDataRetornoNfe").val());
		});

		$('#campoDataRetornoNfe').datepicker({
			changeYear : true,
			maxDate : new Date(),
			yearRange : '<c:out value="${intervaloAnosData}"/>',
			showOn : 'button',
			buttonImage: $('#imagemCalendario').val(),
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy',
			onSelect : function(dateText) {
				$(this).change();
			}
		});

		$('#periodoEmissao').datepicker({
			changeYear : true,
			yearRange : '<c:out value="${intervaloAnosData}"/>',
			showOn : 'button',
			buttonImage: $('#imagemCalendario').val(),
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy'
		});

		$("#dataEmissao").datepicker({
			changeYear : true,
			maxDate : new Date(),
			yearRange : '<c:out value="${intervaloAnosData}"/>',
			showOn : 'button',
			buttonImage: $('#imagemCalendario').val(),
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy',
			onSelect : function(dateText) {
				$(this).change();
			}
		});

		$('#periodoEmissao').on("change", function() {
			var dataForm = ($('#periodoEmissao').val())
					.split("/");
			var dataHoje = new Date();
			var dataDeEmissao = new Date(dataForm[2],
					dataForm[1] - 1, dataForm[0]);
			if (dataHoje < dataDeEmissao) {
				alert("A data de emiss�o n�o pode ser maior do que a data de hoje. ");
				$('#periodoEmissao').val("");
			}
		});

		manterDadosTela();
		corrigirPosicaoDatepicker();
		
	}

	function manterDadosTela() {

		var emailResponsavelAlterado = document
				.getElementById('emailResponsavel').value;
		var emailResponsavelAtual = document
				.getElementById('emailResponsavel');

		var idGrupoFaturamento = "${processoVO.idGrupoFaturamento}";
		if (idGrupoFaturamento != "") {
			carregarRotasPorGrupo(idGrupoFaturamento);
		}

		var idModulo = "${processoVO.modulo}";
		if (idModulo != "") {
			var comboidModulo = document.getElementById("idModulo");
			if (comboidModulo != undefined) {
				carregarOperacoes(comboidModulo);
				emailResponsavelAtual.value = emailResponsavelAlterado;
			}
		}
	}

	function carregarComando(chaveComando) {
		var noCache = "noCache=" + new Date().getTime();
		$("#comboComandos").load("carregarComandos?chaveOperacao=" + chaveComando + "&" + noCache);
	}

	$(document).ready(function(){
		$("#anoMesFaturamento").inputmask("9999/99",{placeholder:"_"});
		$("#anoMesReferencia").inputmask("9999/99",{placeholder:"_"});
		init();
	});
	
	function selecionarRotasGrupoFaturamento() {
		
		var parametrosCronogramaFaturamento = $("#parametrosCronogramaFaturamento").attr("style");
		var rotasSelecionadas = $("#rotasSelecionadas option:selected").val();
		var idGrupoFaturamento = $("#idGrupoFaturamento").val();
			
		if( parametrosCronogramaFaturamento == "" ){
				
			if( idGrupoFaturamento > 0 && typeof rotasSelecionadas == 'undefined' ) {
					
				$('#rotasSelecionadas').children().attr("selected", true);
					
			}
		}
		
	}

</script>

<div class="bootstrap">
	<form:form method="post" action="processarExecucaoProcesso">

	<input name="acao" type="hidden" id="acao" value="processarExecucaoProcesso"/>
	<input name="postBack" type="hidden" id="postBack" value="${isPostBack}" />
	<input name="imagemCalendario" type="hidden" id="imagemCalendario" value="<c:url value="/imagens/calendario.png"/>">

	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Executar Processos</h5>
		</div>
		<div class="card-body">

			<div class="alert alert-primary fade show" role="alert">
				<i class="fa fa-question-circle"></i>
				Para executar um processo, informe os dados abaixo e clique em <b>Executar</b>
			</div>

			<div class="col-xl-12">
				<div class="row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<label for="idModulo">M�dulo:<span class="text-danger">*</span></label>
						<select name="modulo" id="idModulo" class="form-control form-control-sm" onchange="carregarOperacoes(this);">
							<option value="-1">Selecione</option>
							<c:forEach items="${modulos}" var="modulo">
								<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${processoVO.modulo == modulo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${modulo.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<label for="idOperacao">Processo:<span class="text-danger">* </span></label>
						<select name="operacao" id="idOperacao" class="form-control form-control-sm" disabled="disabled" onchange="carregarParametros(this);carregarComando(this.value);" >
							<option value="-1">Selecione</option>
							<c:forEach items="${operacoesBatchs}" var="operacao">
								<option value="<c:out value="${operacao.chavePrimaria}"/>" <c:if test="${processo.operacao.chavePrimaria == operacao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${operacao.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<label for="idOperacao">Comando de A��o:</label>
						<div id="comboComandos">
							<jsp:include page="/jsp/batch/acaocomando/comboComandos.jsp"></jsp:include>
						</div>
					</div>
					<div style="display: none" class="col-xl-4 col-lg-6 col-md-12 parametrosRetornoNfe">
						<label id="rotuloDataEmissaoRetornoNfe" for="campoDataRetornoNfe">Data de Emiss�o: </label>
						<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
							<input class="form-control form-control-sm campoData" type="text" id="campoDataRetornoNfe" name="campoDataRetornoNfe" maxlength="10"
								   value="${processoVO.dataEmissao}">
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12 parametrosIntegracaoNFE" style="display: none">
						<label id="rotuloStatusNFE" for="idStatusNFE">Status da NFE: </label>
						<select name="idStatusNFE" id="idStatusNFE" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaStatusNFE}" var="statusNFE">
								<option value="<c:out value="${statusNFE.chavePrimaria}"/>" <c:if test="${processoVO.idStatusNFE == statusNFE.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${statusNFE.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12 parametrosIntegracaoNFE" style="display: none">
						<label id="rotuloDataEmissao" for="rotuloDataEmissao">Data de Emiss�o: </label>
						<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
							<input class="form-control form-control-sm campoData" type="text" id="dataEmissao" name="dataEmissao" maxlength="10"
								   value="${processoVO.dataEmissao}">
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12 parametrosIntegracaoNFE" style="display: none">
						<label id="rotuloAnoMesFaturamento" for="anoMesFaturamento" style="white-space: nowrap;">Ano/M�s Faturamento: </label>
						<input type="text"  id="anoMesFaturamento" class="form-control form-control-sm" name="anoMesFaturamento" maxlength="7" value="${processoVO.anoMesFaturamento}" />
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12 parametrosIntegracaoNFE" style="display: none">
						<label id="rotuloCiclo" for="ciclo">Ciclo: </label>
						<input type="text" class="form-control form-control-sm" id="ciclo" name="ciclo" maxlength="1" value="${processoVO.ciclo}" />
					</div>
					<div style="display: none" class="col-xl-4 col-lg-6 col-md-12 parametrosArquivosSefaz">
						<label id="rotuloAnoMesReferencia" for="anoMesReferencia">Ano/M�s Refer�ncia: </label>
						<input type="text" id="anoMesReferencia" class="form-control form-control-sm" name="anoMesReferencia" maxlength="7" value="${processoVO.anoMesReferencia}" />
					</div>
					<div style="display: none" class="col-xl-4 col-lg-12 col-md-12 cronogramaFaturamento2">
						<label id="rotuloDataPeriodoEmissao" for="periodoEmissao">Data de Emiss�o: </label>
						<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
							<input class="form-control form-control-sm campoData" type="text" id="periodoEmissao" name="periodoEmissao" maxlength="10"
								   value="${periodoEmissao}">
						</div>
					</div>
					<div style="display: none" class="col-xl-4 col-lg-12 col-md-12 cronogramaFaturamento2">
						<div class="row">
							<div class="col-md-12">
								<label style="white-space: nowrap;">Data retroativa n�o v�lida:</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input class="custom-control-input" type="radio" name="tipoDataRetroativaInvalida" id="tipoDataRetroativaInvalida1" checked value="0">
									<label class="custom-control-label" for="tipoDataRetroativaInvalida1">Data atual</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input class="custom-control-input" type="radio" name="tipoDataRetroativaInvalida" id="tipoDataRetroativaInvalida2" value="1">
									<label class="custom-control-label" for="tipoDataRetroativaInvalida2" style="white-space: nowrap;">Pr�xima data v�lida</label>
								</div>
							</div>
						</div>
					</div>
					<div id="parametrosCronogramaFaturamento" style="display: none" class="col-xl-4 col-lg-6 col-md-12 parametrosCronogramaFaturamento">
						<label id="rotuloGrupoFaturamento" for="idGrupoFaturamento" style="white-space: nowrap;">Grupo de Faturamento:<span class="text-danger">*</span></label>
						<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="form-control form-control-sm" onchange="carregarRotas(this)">
							<option value="-1">Selecione</option>
							<c:forEach items="${gruposFaturamento}" var="grupo">
								<option value="<c:out value="${grupo.chavePrimaria}"/>"
										<c:if test="${processoVO.idGrupoFaturamento == grupo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${grupo.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>
					<div style="display: none" class="col-xl-4 col-lg-6 col-md-12 parametrosCronogramaFaturamento">
						<label id="rotuloRota" for="rotasSelecionadas">Rota:</label>
						<select name="rotasSelecionadas" id="rotasSelecionadas" class="form-control form-control-sm" multiple="multiple">
							<c:forEach items="${listaRotas}" var="rota">
								<option value="<c:out value="${rota.chavePrimaria}"/>"
										<c:if test="${processoVO.idRota == rota.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${rota.numeroRota}"/>
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-xl-4 col-lg-12 col-md-12">
						<label for="emailResponsavel">E-mail para envio do resultado:</label>
						<textarea id="emailResponsavel" name="emailResponsavel" class="form-control form-control-sm" cols="62" rows="2"
								  onkeypress="return formatarCampoEmailMultiplo(event)"><c:out value="${processo.emailResponsavel}"/></textarea><br class="quebraLinha2" />
						<small class="form-text text-muted">Separar e-mails por ponto e v�gula ( ; )</small>
					</div>
				</div>
			</div>

		</div>
		<div class="card-footer">
			<div class="form-row mt-2">
				<div class="col-md-12">
					<div class="row justify-content-between">
						<div class="col-md-6 col-sm-12 mt-1">
							<button name="button" class="btn btn-default btn-sm mb-1 mr-1" type="button" onclick="cancelar()">
								<i class="fa fa-reply"></i> Cancelar
							</button>
							<button name="button" class="btn btn-danger btn-sm mb-1 mr-1" type="button" onclick="limparFormulario()">
								<i class="fa fa-times"></i> Limpar
							</button>
						</div>
						<div class="col-md-6 col-sm-12 mt-1 text-md-right">
							<button id="botaoExecutar" name="button" class="btn btn-primary btn-sm mb-1 mr-1" type="submit" onclick="selecionarRotasGrupoFaturamento()">
								<i class="fa fa-play"></i> Executar
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<token:token></token:token>
</form:form>
</div>
