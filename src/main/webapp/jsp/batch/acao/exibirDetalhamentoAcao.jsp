<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Detalhar A��o<a href="<help:help>/cadastrodoclientedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script type="text/javascript">

function voltar(){
	submeter('acaoForm','pesquisarAcao');
}

function alterar(){
	submeter('acaoForm','exibirAlteracaoAcao');
}

</script>

<form method="post" action="alterarAcao" id="acaoForm">

<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${acao.chavePrimaria }">
<input type="hidden" name="chavesPrimaria" id="chavesPrimaria" value="${acao.chavePrimaria }">

<fieldset id="pesquisaServicoTipoCol1" class="conteinerPesquisarIncluirAlterar">
			<fieldset id="servicoTipoCol1" class="coluna">
				<label class="rotulo campoObrigatorio" for="acaoPrecedente">A��o predecessora:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.acaoPrecedente.descricao}"/></span><br />
				<label class="rotulo" for="mensagem">Descri��o da a��o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.descricao}"/></span><br />

			<label class="rotulo" for="mensagem">Opera��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.operacao.descricao}"/></span><br />
				
		    <label class="rotulo rotulo2Linhas" id="rotuloQuantidadeAgendamento" for="tipoNumeroDiasValidade">N�mero de dias de validade da a��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.numeroDiasValidade}"/></span><br />
			

		    <label class="rotulo rotulo2Linhas" id="rotuloQuantidadeAgendamento" for="tipoNumeroDiasVencimento">N�mero de dias para o vencimento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.numeroDiasVencimento}"/></span><br />	
			</fieldset>		
			<fieldset id="pesquisaServicoTipoCol2" class="colunaFinal">
			<label class="rotulo rotulo2Linhas" for="tipoDocumento">Tipo de documento a ser gerado:</label>
		  	<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.tipoDocumento.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloIndicadorGerarAutorizacaoServico">Gerar ordem de servi�o:</label>
				<c:choose>
			<c:when test="${acao.indicadorGerarAutorizacaoServico == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
				<label class="rotulo rotulo2Linhas" for="tipoDocumento">Tipo de Servi�o:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.servicoTipo.descricao}"/></span><br />
		  		<label class="rotulo rotulo2Linhas" for="tipoDocumento">Equipe:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.equipe.nome}"/></span><br />
			</c:when>
			<c:when test="${acao.indicadorGerarAutorizacaoServico == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
			
			<label class="rotulo" id="rotuloConsiderarDebitosAVencer">Considera</label>
				<c:choose>
			<c:when test="${acao.considerarDebitosAVencer == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${acao.considerarDebitosAVencer == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
			
			<label class="rotulo" id="rotuloBloquearDocumentosPagaveis">Bloquear emiss�o de documentos pag�veis:</label>
				<c:choose>
			<c:when test="${acao.bloquearDocumentosPagaveis == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${acao.bloquearDocumentosPagaveis == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>

			<label class="rotulo" id="rotuloIndicadorGerarChamadoPesquisa">Gerar chamado de pesquisa:</label>
				<c:choose>
			<c:when test="${acao.indicadorGerarChamadoPesquisa == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
				<label class="rotulo rotulo2Linhas" for="tipoDocumento">Tipo de Chamado:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.chamadoAssuntoPesquisa.chamadoTipo.descricao}"/></span><br />
		  		<label class="rotulo rotulo2Linhas" for="tipoDocumento">Assunto:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.chamadoAssuntoPesquisa.descricao}"/></span><br />
				<label class="rotulo rotulo2Linhas" for="tipoDocumento">Unidade Organizacional:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.unidadeOrganizacionalPesquisa.descricao}"/></span><br />
		  		<label class="rotulo rotulo2Linhas" for="tipoDocumento">Tipo de Servi�o:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.servicoTipoPesquisa.descricao}"/></span><br />
		  		<label class="rotulo rotulo2Linhas" for="tipoDocumento">Questionario:</label>
		  		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${acao.questionario.nomeQuestionario}"/></span><br />
			</c:when>
			<c:when test="${acao.indicadorGerarChamadoPesquisa == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
			
			</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Voltar" type="button" onClick="voltar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
<%-- 	    <vacess:vacess param="incluirServicoTipo"> --%>
	    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar"  type="submit" onclick="alterar();">
<%-- 	    </vacess:vacess>	 --%>
 	</fieldset>