<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

$(document).ready(function(){
	
	var indicadorGerarAutorizacaoServico = ${acao.indicadorGerarAutorizacaoServico};
	if(indicadorGerarAutorizacaoServico){
		atualizaCamposObrigatorios();
	}else{
		desabilitaCamposObrigatorios();	
	}
	
	var indicadorGeraPesquisa = "<c:out value="${acao.indicadorGerarChamadoPesquisa}" />";
	if(indicadorGeraPesquisa=='true') {
		$("#equipamentosServico").show();
		$( "#spanQuestionarioPesquisa" ).html('*&nbsp;');
		$( "#spanServicoTipoPesquisa" ).html('*&nbsp;');
		$( "#spanUnidadeOrganizacionalPesquisa" ).html('*&nbsp;');
		$( "#spanAssuntoPesquisa" ).html('*&nbsp;');
		$( "#spanChamadoTipoPesquisa" ).html('*&nbsp;');
		atualizaCamposObrigatorios();
	}
	
	var maxLength = 30;
	$('#servicoTipo > option').text(function(i, text) {
		text = text.trim();
	    if (text.length > maxLength) {
	        return text.substr(0, maxLength) + '...';  
	    }
	});
	
	var maxLength = 30;
	$('#servicoTipoPesquisa > option').text(function(i, text) {
		text = text.trim();
	    if (text.length > maxLength) {
	        return text.substr(0, maxLength) + '...';  
	    }
	});
});

function incluir() {
	submeter('acaoForm','incluirAcao');
}

function cancelar() {
	submeter('acaoForm','exibirPesquisaAcao');
}

function carregarOperacoes(chaveModulo){
	var noCache = "noCache=" + new Date().getTime();
	$("#comboOperacoes").load("carregarOperacoes?chaveModulo="+chaveModulo+"&"+noCache);
}

function atualizaCamposObrigatorios() {
	$( "#spanServicoTipo" ).html('*&nbsp;');
	$( "#spanEquipe" ).html('*&nbsp;');
}

function desabilitaCamposObrigatorios() {
	$( "#spanServicoTipo" ).html('');
	$( "#spanEquipe" ).html('');
}

function atualizaCamposObrigatoriosPesquisa() {
	$( "#spanQuestionarioPesquisa" ).html('*&nbsp;');
	$( "#spanServicoTipoPesquisa" ).html('*&nbsp;');
	$( "#spanUnidadeOrganizacionalPesquisa" ).html('*&nbsp;');
	$( "#spanAssuntoPesquisa" ).html('*&nbsp;');
	$( "#spanChamadoTipoPesquisa" ).html('*&nbsp;');
}

function desabilitaCamposObrigatoriosPesquisa() {
	$( "#spanQuestionarioPesquisa" ).html('');
	$( "#spanServicoTipoPesquisa" ).html('');
	$( "#spanUnidadeOrganizacionalPesquisa" ).html('');
	$( "#spanAssuntoPesquisa" ).html('');
	$( "#spanChamadoTipoPesquisa" ).html('');
}

function limparFormulario(){
	document.forms[0].acaoPrecedente.value = -1;
	document.forms[0].modulo.value = -1;
	document.forms[0].operacao.value = -1;
	document.forms[0].descricao.value = "";
	document.forms[0].tipoDocumento.value = -1;
	document.forms[0].tipoNumeroDiasValidade.value = -1;
	document.forms[0].tipoNumeroDiasVencimento.value = -1;
	document.forms[0].numeroDiasVencimento.value = "";
	document.forms[0].numeroDiasValidade.value = "";
	document.forms[0].indicadorGerarAutorizacaoServico[0].checked = true;
	document.forms[0].considerarDebitosAVencer[0].checked = true;
	document.forms[0].bloquearDocumentosPagaveis[0].checked = true;
	document.forms[0].indicadorGerarChamadoPesquisa[0].checked = true;
}

function exibirCamposPesquisa(chaveOperacao) {
	var operacaoPesquisa = "<c:out value="${operacaoPesquisa}" />";
	if(chaveOperacao==operacaoPesquisa) {
		$("#equipamentosServico").show();
		$( "#spanQuestionarioPesquisa" ).html('*&nbsp;');
		$( "#spanServicoTipoPesquisa" ).html('*&nbsp;');
		$( "#spanUnidadeOrganizacionalPesquisa" ).html('*&nbsp;');
		$( "#spanAssuntoPesquisa" ).html('*&nbsp;');
		$( "#spanChamadoTipoPesquisa" ).html('*&nbsp;');
		document.forms[0].indicadorGerarChamadoPesquisa.value = true;
	} else {
		$("#equipamentosServico").hide();
		document.forms[0].indicadorGerarChamadoPesquisa.value = false;
	}
}
</script>

<c:if test="${ fluxoAlteracao eq true }">
	<h1 class="tituloInterno">Alterar A��o</h1>
	<p class="orientacaoInicial">Altere os dados e clique em <span class="destaqueOrientacaoInicial">Salvar</span>
	    para alterar a a��o.<br/>Para cancelar clique em <span class="destaqueOrientacaoInicial">Cancelar</span>.<br />
	</p>
</c:if>


<c:if test="${ fluxoInclusao eq true }">
<h1 class="tituloInterno">Incluir A��o<a href="<help:help>/localidadeinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>
</c:if>
<form:form method="post" modelAttribute="Acao" action="incluirAcao" id="acaoForm" name="acaoForm">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${acao.chavePrimaria}">
	<input type="hidden" name="chavesPrimaria" id="chavesPrimaria" value="${acao.chavePrimaria}">
	<input type="hidden" name="habilitado" id="habilitado" value="${acao.habilitado}">
	<input type="hidden" name="versao" id="versao" value="${acao.versao}">
	<input type="hidden" name="indicadorGerarChamadoPesquisa" id="indicadorGerarChamadoPesquisa" value="${acao.indicadorGerarChamadoPesquisa}">
	<fieldset id="pesquisaServicoTipoCol1" class="conteinerPesquisarIncluirAlterar">
			<fieldset id="servicoTipoCol1" class="coluna">
				<label class="rotulo campoObrigatorio" for="acaoPrecedente">A��o predecessora:</label>
		  		<select id="acaoPrecedente" class="campoSelect" name="acaoPrecedente">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaPrecedente}" var="acaoPrecedente">
					<option value="<c:out value="${acaoPrecedente.chavePrimaria}"/>" <c:if test="${acao.acaoPrecedente.chavePrimaria == acaoPrecedente.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${acaoPrecedente.descricao}"/>
					</option>
					</c:forEach>
				</select>
				<label class="rotulo" for="mensagem"><span class="campoObrigatorioSimbolo">* </span>Descri��o da a��o:</label>
				<textarea  class="campoTexto" name="descricao" id="descricao" cols="35" rows="3" maxlength="100" onblur="this.value = removerEspacoInicialFinal(this.value);"
					onkeypress="return formatarCampoTextoLivreComLimite(event,this,100);" onpaste="return formatarCampoTextoLivreComLimite(event,this,1000);" onkeyup="letraMaiuscula(this);">${acao.descricao}</textarea>
				<br/>
				
				<label class="rotulo campoObrigatorio" for="idModulo"><span class="campoObrigatorioSimbolo">* </span>M�dulo:</label>
				<select name="modulo" id="modulo" class="campoSelect" onchange="carregarOperacoes(this.value);">
					<option value="-1">Selecione</option>
					<c:forEach items="${modulos}" var="modulo">
						<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${acao.modulo.chavePrimaria == modulo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${modulo.descricao}"/>
						</option>		
					</c:forEach>
				</select><br />
				
			<label class="rotulo campoObrigatorio" for="idOperacao"><span class="campoObrigatorioSimbolo">* </span>Opera��o:</label>
			<div id="comboOperacoes">	  
				<jsp:include page="/jsp/batch/acao/comboOperacoes.jsp"></jsp:include>		
			</div>	
				
		    <label class="rotulo rotulo2Linhas" id="rotuloQuantidadeAgendamento" for="tipoNumeroDiasValidade">N�mero de dias de validade da a��o:</label>
			<input class="campoTexto campoHorizontal" id="numeroDiasValidade" name="numeroDiasValidade" type="text" size="5" maxlength="3" onkeypress="return formatarCampoInteiro(event, this);" value="${acao.numeroDiasValidade}">
			<select class="campoSelect campoHorizontal" name="tipoNumeroDiasValidade" id="tipoNumeroDiasValidade">
		    	<option value="-1">Selecione</option>
					<c:forEach items="${listaDias}" var="dia">
					<option value="<c:out value="${dia.chavePrimaria}"/>" <c:if test="${acao.tipoNumeroDiasValidade.chavePrimaria == dia.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${dia.descricao}"/>
					</option>
					</c:forEach>
		    </select>

		    <label class="rotulo rotulo2Linhas" id="rotuloQuantidadeAgendamento" for="tipoNumeroDiasVencimento">N�mero de dias para o vencimento:</label>
			<input class="campoTexto campoHorizontal" id="numeroDiasVencimento" name="numeroDiasVencimento" type="text" size="5" maxlength="3" onkeypress="return formatarCampoInteiro(event, this);" value="${acao.numeroDiasVencimento}">	
			<select class="campoSelect campoHorizontal" name="tipoNumeroDiasVencimento" id="tipoNumeroDiasVencimento">
		    	<option value="-1">Selecione</option>
					<c:forEach items="${listaDias}" var="dia">
					<option value="<c:out value="${dia.chavePrimaria}"/>" <c:if test="${acao.tipoNumeroDiasVencimento.chavePrimaria == dia.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${dia.descricao}"/>
					</option>
					</c:forEach>
		    </select>
			</fieldset>		
			<fieldset id="pesquisaServicoTipoCol2" class="colunaFinal">
			<label class="rotulo rotulo2Linhas" for="tipoDocumento">Tipo de documento a ser gerado:</label>
		  		<select id="tipoDocumento" class="campoSelect" name="tipoDocumento">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoDocumento}" var="tipoDocumento">
					<option value="<c:out value="${tipoDocumento.chavePrimaria}"/>" <c:if test="${acao.tipoDocumento.chavePrimaria == tipoDocumento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoDocumento.descricao}"/>
					</option>
					</c:forEach>
				</select>
			<label class="rotulo" for="indicadorGerarAutorizacaoServico">Gerar ordem de servi�o:</label>
			<input class="campoRadio" type="radio" name="indicadorGerarAutorizacaoServico" id="indicadorGerarAutorizacaoServico" value="true" <c:if test="${acao.indicadorGerarAutorizacaoServico eq 'true'}">checked</c:if> onclick="atualizaCamposObrigatorios();">
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorGerarAutorizacaoServico" id="indicadorGerarAutorizacaoServico" value="false" <c:if test="${acao.indicadorGerarAutorizacaoServico eq 'false'}">checked</c:if> onclick="desabilitaCamposObrigatorios();">
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
	
			<label class="rotulo rotulo2Linhas" for="servicoTipo"><span id="spanServicoTipo" class="campoObrigatorioSimbolo"></span>Tipo de Servi�o:</label>
		  		<select id="servicoTipo" class="campoSelect" name="servicoTipo">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaServicoTipo}" var="servicoTipo">
					<option value="<c:out value="${servicoTipo.chavePrimaria}"/>" <c:if test="${acao.servicoTipo.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${servicoTipo.descricao}"/>
					</option>
					</c:forEach>
				</select>			
				
			<label class="rotulo rotulo2Linhas" for="equipe"><span id="spanEquipe" class="campoObrigatorioSimbolo"></span>Equipe:</label>
		  		<select id="equipe" class="campoSelect" name="equipe">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaEquipe}" var="equipe">
					<option value="<c:out value="${equipe.chavePrimaria}"/>" <c:if test="${acao.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${equipe.nome}"/>
					</option>
					</c:forEach>
				</select>			
			<label class="rotulo" for="considerarDebitosAVencer">Considera d�bitos a vencer:</label>
			<input class="campoRadio" type="radio" name="considerarDebitosAVencer" id="considerarDebitosAVencer" value="true" <c:if test="${acao.considerarDebitosAVencer eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio" type="radio" name="considerarDebitosAVencer" id="considerarDebitosAVencer" value="false" <c:if test="${acao.considerarDebitosAVencer eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
			
			<label class="rotulo" for="bloquearDocumentosPagaveis">Bloquear emiss�o de documentos pag�veis:</label>
			<input class="campoRadio" type="radio" name="bloquearDocumentosPagaveis" id="bloquearDocumentosPagaveis" value="true" <c:if test="${acao.bloquearDocumentosPagaveis eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio" type="radio" name="bloquearDocumentosPagaveis" id="bloquearDocumentosPagaveis" value="false" <c:if test="${acao.bloquearDocumentosPagaveis eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
		<fieldset id="equipamentosServico" class="conteinerDados">
			<label class="rotulo rotulo2Linhas" for="assunto"><span id="spanAssuntoPesquisa" class="campoObrigatorioSimbolo"></span>Assunto:</label>
		  		<select id="chamadoAssuntoPesquisa" class="campoSelect" name="chamadoAssuntoPesquisa">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaChamadoAssunto}" var="assunto">
					<option value="<c:out value="${assunto.chavePrimaria}"/>" <c:if test="${acao.chamadoAssuntoPesquisa.chavePrimaria == assunto.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${assunto.descricao}"/>
					</option>
					</c:forEach>
				</select>			
				
			<label class="rotulo rotulo2Linhas" for="unidadeOrganizacionalPesquisa"><span id="spanUnidadeOrganizacionalPesquisa" class="campoObrigatorioSimbolo"></span>Unidade organizacional:</label>
		  		<select id="unidadeOrganizacionalPesquisa" class="campoSelect" name="unidadeOrganizacionalPesquisa">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaUnidadeOrganizacional}" var="unidadeOrganizacional">
					<option value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>" <c:if test="${acao.unidadeOrganizacionalPesquisa.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidadeOrganizacional.descricao}"/>
					</option>
					</c:forEach>
				</select>								
			<label class="rotulo rotulo2Linhas" for="servicoTipoPesquisa"><span id="spanServicoTipoPesquisa" class="campoObrigatorioSimbolo"></span>Tipo de Servi�o:</label>
		  		<select id="servicoTipoPesquisa" class="campoSelect" name="servicoTipoPesquisa">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaServicoTipo}" var="servicoTipo">
					<option value="<c:out value="${servicoTipo.chavePrimaria}"/>" <c:if test="${acao.servicoTipoPesquisa.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${servicoTipo.descricao}"/>
					</option>
					</c:forEach>
				</select>								
			<label class="rotulo rotulo2Linhas" for="questionario"><span id="spanQuestionarioPesquisa" class="campoObrigatorioSimbolo"></span>Question�rio:</label>
		  		<select id="questionario" class="campoSelect" name="questionario">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaQuestionario}" var="questionario">
					<option value="<c:out value="${questionario.chavePrimaria}"/>" <c:if test="${acao.questionario.chavePrimaria == questionario.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${questionario.nomeQuestionario}"/>
					</option>
					</c:forEach>
				</select>	
				</fieldset>							
			</fieldset>
	</fieldset>
</form:form>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
<%-- 	    <vacess:vacess param="incluirServicoTipo"> --%>
	    	<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit" onclick="incluir();">
<%-- 	    </vacess:vacess>	 --%>
 	</fieldset>
