<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<c:if test="${empty (param.acao) == false && param.acao ne 'exibirPesquisaProcesso'}">
	<input type="hidden" class="recarregar" value="true">
</c:if>
<script>

	function carregarRotas(elem) {
		var codGrupoFaturamento = elem.value;
		carregarRotasPorGrupo(codGrupoFaturamento);
	}

	function carregarRotasPorGrupo(codGrupoFaturamento){
		if (codGrupoFaturamento > 0) {
			var selectRotas = document.getElementById("rotasSelecionadas");
			var rotasSelecionadas = [];
			<c:forEach items="${processoVO.rotasSelecionadas}" var="rota">
			rotasSelecionadas.push( parseInt(<c:out value = "${rota}"/>));
			</c:forEach>
			selectRotas.length=0;
			var novaOpcao =  null;
			selectRotas.options[selectRotas.length] = novaOpcao;
			AjaxService.listarRotaPorGrupoFaturamento(codGrupoFaturamento,
					function(rotas) {
						for (key in rotas){
							var novaOpcao = new Option(rotas[key], key);
							if ($.inArray(parseInt(key), rotasSelecionadas) >= 0){
								novaOpcao.selected = true;
							}
							selectRotas.options[selectRotas.length] = novaOpcao;
						}
						ordernarSelect(selectRotas)
					}
			);
		}
	}

	function limparFormulario(){
		document.forms[0].idModulo.value = "-1";
		document.forms[0].idOperacao.value = "-1";
		document.forms[0].idPeriodicidade.value = "-1";
		document.forms[0].idSituacao.value = "-1";
		document.forms[0].dataInicio.value = "";
		document.forms[0].dataFim.value = "";
		document.forms[0].dataAgendada.value = "";
		document.forms[0].dataAgendadaFinal.value = "";
		document.forms[0].indicadorAgendamento[2].checked = true;
		$('#idGrupoFaturamento option:eq(0)').prop('selected', true);
		$('#rotasSelecionadas').empty();
		$('#divGrupoFaturamento').hide(400);
		$('#divRota').hide(400);
	}

	function carregarOperacoes(elem){
		var idModulo = elem.value;
		var selectOperacoes = document.getElementById("idOperacao");
		var idOperacao = '${processo.operacao.chavePrimaria}';

		selectOperacoes.length=0;
		var novaOpcao = new Option("Selecione","-1");
		selectOperacoes[selectOperacoes.length] = novaOpcao;
		if (idModulo != "-1") {
			selectOperacoes.disabled = false;
			AjaxService.consultarOperacoesBatchPorModulo( idModulo,
					function(listaOperacoes) {
						for (key in listaOperacoes){
							var novaOpcao = new Option(listaOperacoes[key], key);
							if (idOperacao == key) {
								novaOpcao.selected = true;
								validarAtividade(idOperacao);
							}
							selectOperacoes.options[selectOperacoes.length] = novaOpcao;
						}
					}
			);
		} else {
			selectOperacoes.disabled = true;
			if ($('#divGrupoFaturamento').is(":visible")) {
				$('#idGrupoFaturamento option:eq(0)').prop('selected', true);
				$('#rotasSelecionadas').empty();
				$('#divGrupoFaturamento').hide(400);
				$('#divRota').hide(400);
			}
		}
	}

	function carregarParametros(elem) {
		var idOperacao = null;
		if (elem.value != undefined) {
			idOperacao = elem.value;
		} else {
			idOperacao = elem;
		}
		validarAtividade(idOperacao);
	}

	function validarAtividade(idOperacao) {
		if (idOperacao != "-1") {
			AjaxService.obterAtividadeCronogramaFaturamento(idOperacao, {
				callback : function(dados) {
					if (dados["ehAtividadeCronograma"] == 'true') {
						$('#divGrupoFaturamento').show(400);
						$('#divRota').show(400);
					} else {
						$('#idGrupoFaturamento option:eq(0)').prop('selected', true);
						$('#rotasSelecionadas').empty();
						$('#divGrupoFaturamento').hide(400);
						$('#divRota').hide(400);
					}
					if (dados["isFaturarGrupo"] == 'true') {
						$('#divGrupoFaturamento').show(400);
						$('#divRota').show(400);
					}
				}, async : false});
		} else {
			$('#idGrupoFaturamento option:eq(0)').prop('selected', true);
			$('#rotasSelecionadas').empty();
			$('#divGrupoFaturamento').hide(400);
			$('#divRota').hide(400);
		}
	}


	function reiniciarProcesso(chavePrimaria,versao) {
		document.forms[0].chavePrimaria.value = chavePrimaria;
		document.forms[0].versao.value = versao;
		var retorno = confirm("Deseja reiniciar o processo?");
		if(retorno == true) {
			submeter('formPesquisaProcesso', 'reiniciarProcesso');
		}

	}

	function excluirProcesso(chavePrimaria) {
		document.forms[0].chavePrimaria.value = chavePrimaria;
		var retorno = confirm("Deseja excluir o processo?");
		if(retorno == true) {
			submeter('formPesquisaProcesso', 'excluirProcesso');
		}
	}

	function agendar() {
		location.href = '<c:url value="exibirInclusaoProcessoAgendamento"/>';
	}

	function executar() {
		location.href = '<c:url value="exibirExecutarProcesso"/>';
	}

	function alterarProcesso() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('formPesquisaProcesso', 'exibirAlteracaoProcessoAgendamento');
		}
	}

	function cancelarAgendamento(){

		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_CANCELAR_AGENDAMENTO"/>');
			if(retorno == true) {
				submeter('formPesquisaProcesso', 'cancelarAgendamento');
			}
		}
	}

	function exibirDialogInfoExecucaoProcesso(chavePrimaria) {
		$("#modalDetalhamentoDadosExecucaoProcesso").modal('hide');
		AjaxService.exibirDialogInfoExecucaoProcesso(chavePrimaria,
			function(processoSistema) {
				$("#modalDetalhamentoDadosExecucaoProcesso #numeroExecucaoProcesso")
						.html(chavePrimaria);
				$("#modalDetalhamentoDadosExecucaoProcesso #nomeExecucaoProcesso")
						.html(processoSistema["descricaoProcesso"]);
				$("#modalDetalhamentoDadosExecucaoProcesso #usuarioExecucaoProcesso")
						.html(processoSistema["usuarioResponsavel"]);
				$("#modalDetalhamentoDadosExecucaoProcesso #unidadeOrganizacionalExecucaoProcesso")
						.html(processoSistema["unidadeOrganizacional"]);
				$("#modalDetalhamentoDadosExecucaoProcesso #dataInicioExecucaoProcesso")
						.html(processoSistema["dataInicioExecucaoProcesso"]);
				$("#modalDetalhamentoDadosExecucaoProcesso #fimExecucaoProcesso")
						.html(processoSistema["dataFimExecucaoProcesso"]);
				$("#modalDetalhamentoDadosExecucaoProcesso #unidadeOrganizacionalExecucaoProcesso")
						.html(processoSistema["unidadeOrganizacional"]);
				$("#modalDetalhamentoDadosExecucaoProcesso #duracaoExecucaoProcesso")
						.html(processoSistema["tempoExecucao"]);
				$("#modalDetalhamentoDadosExecucaoProcesso").modal('show');
			}
		);
	}

	function exibirRelatorioPopup(chavePrimaria){
		window.open('exibirRelatorioPopup?postBack=true&chavePrimaria=' + chavePrimaria,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function exibirLog(chavePrimaria, logErro) {

		AjaxService.exibirLog(chavePrimaria, logErro,
				function (log) {
					console.log(log);
					if (log[0].error) {
						console.log(log[0].error);
						$('.mensagensSpring').html('<div class="notification failure hideit"><p><strong>Erro: </strong>'+log[0].error+'</p></div>');
						$('.mensagensSpring').show();

						$('html, body').animate({scrollTop: $('.mensagensSpring').offset().top - 80}, 300);
						document.getElementById("breadCrumb").scrollIntoView();
					} else {
						$("#logErro").html(log[0].log.replace(/(?:\r\n|\r|\n)/g, '<br>'));

						$('#download_erro').hide();
						$('#download_sucesso').hide();
						if (logErro) {
							$("#dialogLogErro").dialog('option', 'title', 'Log de erro do processo: ' + chavePrimaria);
							$('#download_erro').show();
							$('#download_erro').attr('href', 'exibirLog?logErro=true&chavePrimaria=' + chavePrimaria);
						} else {
							$("#dialogLogErro").dialog('option', 'title', 'Log do processamento: ' + chavePrimaria);
							$('#download_sucesso').show();
							$('#download_sucesso').attr('href', 'exibirLog?logErro=false&chavePrimaria=' + chavePrimaria);
						}

						$("#dialogLogErro").dialog('open');
					}
				}
		);
	}

	function segundosParaHMS(d) {
		d = Number(d);
		var h = Math.floor(d / 3600);
		var m = Math.floor(d % 3600 / 60);
		var s = Math.floor(d % 3600 % 60);

		var hora, minuto, segundo;

		var valores = [];
		if (h > 0) {
			hora = h + (h == 1 ? " hora" : " horas");
			valores.push(hora);
		}
		if (m > 0) {
			minuto = m + (m == 1 ? " minuto" : " minutos");
			valores.push(minuto);
		}
		if (s > 0) {
			segundo = s + (s == 1 ? " segundo" : " segundos");
			valores.push(segundo);
		}
		if (valores.length == 3) {
			return valores[0] + ", " + valores[1] + " e " + valores[2];
		} else if(valores.length == 2) {
			return valores[0] + " e " + valores[1];
		} else {
			return valores[0];
		}
	}

	function init(tempoAtualizacao) {
		var idModulo = document.getElementById("idModulo");
		if (idModulo != undefined) {
			carregarOperacoes(idModulo);
		}
		var tempoExtenso = segundosParaHMS(tempoAtualizacao);
		$('.destaqueOrientacaoInicial').html(tempoExtenso);
	}

	$(document).ready(function() {
		$("#dialogLogErro").dialog({
			autoOpen: false,
			modal: true,
			width: 1000,
			resizeble: false,
			draggable: false
		});

		var tempoAtualizacao = parseInt($('#tempoAtualizacao').val());

		setInterval(function (){
			if ($("#dialogLogErro").dialog('isOpen') === false && $('.containerTabela').is(":visible")) {
				$('#botaoPesquisar').click();
			}
		}, tempoAtualizacao* 1000);

		$(".campoData").datepicker({
			changeYear: true,
			maxDate: '+0d',
			showOn: 'button',
			buttonImage: $('#imagemCalendario').val(),
			buttonImageOnly: true,
			buttonText: 'Exibir Calend�rio',
			dateFormat: 'dd/mm/yy'
		});

		init(tempoAtualizacao);
		carregarRotasPorGrupo(${processoVO.idGrupoFaturamento});
	});
</script>

<div class="bootstrap">
    <input name="imagemCalendario" type="hidden" id="imagemCalendario" value="<c:url value="/imagens/calendario.png"/>">
	<input name="tempoAtualizacao" type="hidden" id="tempoAtualizacao" value="${tempoAtualizacao}"/>
	<form id="formPesquisaProcesso">

		<input name="acao" type="hidden" id="acao" value="pesquisarProcesso"/>
		<input name="versao" type="hidden" id="versao" value="<c:out value='${processo.versao}'/>" />
		<input name="chavePrimaria" type="hidden" id="chavePrimaria"/>
		<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
		<input name="idAcao" type="hidden" id="IdAcao" value="${idAcao}"/>
		<input name="pesquisaAutomatica" type="hidden" id="pesquisaAutomatica" value="${pesquisaAutomatica}"/>

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Pesquisar Processo</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i>
					Para filtrar um processo, informe os dados abaixo e clique em <b>Pesquisar</b>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body bg-light">
								<h5>Filtrar processo</h5>
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idModulo">M�dulo:</label>
										<select name="modulo" id="idModulo" class="form-control form-control-sm" onchange="carregarOperacoes(this);">
											<option value="-1">Selecione</option>
											<c:forEach items="${modulos}" var="modulo">
												<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${processoVO.modulo == modulo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${modulo.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idPeriodicidade">Processo:</label>
										<select name="operacao" id="idOperacao" class="form-control form-control-sm" onchange="carregarParametros(this);">
											<option value="-1">Selecione</option>
											<c:forEach items="${operacoesBatchs}" var="operacao">
												<option value="<c:out value="${operacao.chavePrimaria}"/>" <c:if test="${processo.operacao.chavePrimaria == operacao.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${operacao.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idPeriodicidade">Periodicidade:</label>
										<select name="periodicidade" id="idPeriodicidade" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${periodicidades}" var="periodicidade">
												<option value="<c:out value="${periodicidade.codigo}"/>" <c:if test="${processo.periodicidade == periodicidade.codigo}">selected="selected"</c:if>>
													<c:out value="${periodicidade.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idSituacao">Situa��o:</label>
										<select name="situacao" id="idSituacao" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${situacoes}" var="situacao">
												<option value="<c:out value="${situacao.codigo}"/>" <c:if test="${processo.situacao == situacao.codigo}">selected="selected"</c:if>>
													<c:out value="${situacao.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="dataAgendada">Data In�cio Agendamento:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm campoData" type="text" id="dataAgendada" name="dataInicioAgendamento" maxlength="10"
												value="${processoVO.dataInicioAgendamento}">
										</div>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="dataAgendadaFinal">Data Final Agendamento:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm campoData" type="text" id="dataAgendadaFinal" name="dataFinalAgendamento" maxlength="10"
												   value="${processoVO.dataFinalAgendamento}">
										</div>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="dataInicio">Data In�cio Execu��o:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm campoData" type="text" id="dataInicio" name="dataInicio" maxlength="10"
												   value="${processoVO.dataInicio}">
										</div>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="dataFim">Data Fim Execu��o:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm campoData" type="text" id="dataFim" name="dataFim" maxlength="10"
												   value="${processoVO.dataFim}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label class="float-none">Foram Agendados: </label><br>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="indicadorAgendamento" id="agendadoSim" class="custom-control-input"
												   value="true" <c:if test="${processoVO.indicadorAgendamento eq 'true'}">checked</c:if>>
											<label class="custom-control-label" for="agendadoSim">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="indicadorAgendamento" id="agendadoNao" class="custom-control-input"
												   value="false" <c:if test="${processoVO.indicadorAgendamento eq 'false'}">checked</c:if>>
											<label class="custom-control-label" for="agendadoNao">N�o</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="indicadorAgendamento" id="agendadoTodos" class="custom-control-input"
												   value="" <c:if test="${empty processoVO.indicadorAgendamento}">checked</c:if>>
											<label class="custom-control-label" for="agendadoTodos">Todos</label>
										</div>
									</div>

									<div id="divGrupoFaturamento" style="display: none" class="col-xl-3 col-lg-6 col-md-12">
										<label id="rotuloGrupoFaturamento" for="idGrupoFaturamento">Grupo de Faturamento:</label>
										<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="form-control form-control-sm" onchange="carregarRotas(this)">
											<option value="-1">Selecione</option>
											<c:forEach items="${gruposFaturamento}" var="grupo">
												<option value="<c:out value="${grupo.chavePrimaria}"/>" <c:if test="${processoVO.idGrupoFaturamento == grupo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${grupo.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div id="divRota" style="display: none" class="col-xl-3 col-lg-6 col-md-12">
										<label id="rotuloRota" for="rotasSelecionadas">Rota:</label>
										<select name="rotasSelecionadas" id="rotasSelecionadas" class="form-control form-control-sm" multiple="multiple">
											<c:forEach items="${listaRotas}" var="rota">
												<option value="<c:out value="${rota.chavePrimaria}"/>" <c:if test="${processoVO.idRota == rota.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${rota.numeroRota}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mt-3">
					<div class="col-sm-12">
						<button type="button" class="btn btn-secondary btn-sm float-right" onclick="limparFormulario();">
							<i class="fa fa-times"></i> Limpar
						</button>
						<button id="botaoPesquisar" type="submit" class="btn btn-primary btn-sm float-right mr-1">
								<i class="fa fa-search"></i> Pesquisar
						</button>
						<div class="containerTabela" style="display: none">
							<label>Obs.: Essa tela ser� recarregada automaticamente a cada <span class="destaqueOrientacaoInicial">30</span>.</label>
						</div>
					</div>
				</div>
				<div class="containerTabela" style="display: none">
					<hr/>
					<div class="table-responsive pt-3">
						<table id="tableProcessos" class="table table-bordered table-striped table-hover" style="width: 100% !important;">
							<thead class="thead-ggas-bootstrap">
							<th scope="col" class="text-center">
								<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
									<input id="checkAllAuto" type="checkbox"  name="checkAllAuto" class="custom-control-input">
									<label class="custom-control-label p-0" for="checkAllAuto"></label>
								</div>
							</th>
							<th scope="col" class="text-center">Processo</th>
							<th scope="col" class="text-center">Descri��o</th>
							<th scope="col" class="text-center">Periodicidade</th>
							<th scope="col" class="text-center">In�cio do agendamento</th>
							<th scope="col" class="text-center">Fim do agendamento</th>
							<th scope="col" class="text-center">Dados de Execu��o</th>
							<th scope="col" class="text-center">Situa��o</th>
							<th scope="col" class="text-center">Log</th>
							<th scope="col" class="text-center">Relat�rio</th>
							<th scope="col" class="text-center"></th>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>

			</div>
			<div class="card-footer">
				<button id="btnCancelar" style="display: none;" type="button" class="btn btn-danger btn-sm float-left" onclick="cancelarAgendamento();">
					<i class="fa fa-times"></i> Cancelar Agendamento
				</button>
				<button id="botaoAgendar" type="button" class="btn btn-primary btn-sm float-right" onclick="agendar();">
					<i class="fas fa-clock"></i> Agendar
				</button>
				<button id="botaoExecutar" type="button" class="btn btn-primary btn-sm float-right mr-1" onclick="executar();">
					<i class="fas fa-play"></i> Executar
				</button>
			</div>
		</div>

		<div id="dialogLogErro" title="Log de erro" style="display: none">
			<div id="logErro">

			</div>

			<div id="downloadErro" style="margin-top: 10px">
				<a id="download_erro"><img alt="log de erro" title="Download log de erro (nova aba)" src="<c:url value="/imagens/icon-alert16_log.png"/>" border="0"> Baixar arquivo</a>
				<a id="download_sucesso"><img alt="log do processamento" title="Download log do processamento (nova aba)" src="<c:url value="/imagens/success_icon_log.png"/>" border="0"> Baixar arquivo</a>
			</div>
		</div>

	</form>
</div>

<jsp:include page="detalhamentoDadosExecucaoProcesso.jsp"/>

<script src="${ctx}/js/lib/moment/moment-with-locales.min.js"></script>
<script src="${ctx}/js/lib/daterangepicker/daterangepicker.js"></script>
<script src="${ctx}/js/lib/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="${ctxWebpack}/dist/modulos/batch/controleProcesso/pesquisarProcesso/index.js"></script>
