<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Alterar Processo Agendado</h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Alterar</span> para finalizar.</p>


<form:form method="post" action="alterarProcessoAgendamento">
	
<script>
	
	$(function(){
						
		// Datepicker
		$(".campoData").datepicker({ minDate: '+0d', changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
	});

	function limparFormulario(){
		document.getElementById('idModulo').value = '-1';
		document.getElementById('idOperacao').value = '-1';
		document.getElementById('idPeriodicidade').value = '-1';
		document.getElementById('diaNaoUtilNao').checked = true;
		document.getElementById('dataAgendada').value = '';
		document.getElementById('hora').value = '-1';
		document.getElementById('idSituacao').value = '-1';
		document.getElementById('emailResponsavel').value = '-1';
	}
	
	function cancelar() {	
		location.href = '<c:url value="/exibirPesquisaProcesso"/>';
	}

	function carregarOperacoes(elem){
		var idModulo = elem.value;
		var selectOperacoes = document.getElementById("idOperacao");
		var idOperacao = '${processo.operacao.chavePrimaria}';
	    
		selectOperacoes.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectOperacoes[selectOperacoes.length] = novaOpcao;
        
      	if (idModulo != "-1") {
      		selectOperacoes.disabled = false;      		
        	AjaxService.consultarOperacoesBatchPorModulo( idModulo, 
            	function(listaOperacoes) {            		      		         		
                	for (key in listaOperacoes){
                    	var novaOpcao = new Option(listaOperacoes[key], key);
                    	if (idOperacao == key) {
                    		novaOpcao.selected = true;
                    	}
                    	selectOperacoes.options[selectOperacoes.length] = novaOpcao;
                    }
                }
            );
      	} else {
      		selectOperacoes.disabled = true;      	
      	}
	}

	function init() {
	
		var idModulo = document.getElementById("idModulo");
		
		if (idModulo != undefined) {
			carregarOperacoes(idModulo);
		}	
		
	}

	addLoadEvent(init);
	
	
</script>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="<c:out value='${processo.chavePrimaria}'/>"">
	<input name="acao" type="hidden" id="acao" value="alterarProcessoAgendamento"/>
	<input name="versao" type="hidden" id="versao" value="<c:out value='${processo.versao}'/>" />
	<input name="postBack" type="hidden" id="postBack" value="false">
	
	<fieldset id="alterarProcesso" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
		<label class="rotulo" for="idModulo"><span class="campoObrigatorioSimbolo">* </span>M�dulo:</label>
		<select name="modulo" id="idModulo" class="campoSelect" onchange="carregarOperacoes(this);">
				<option value="-1">Selecione</option>
				<c:forEach items="${modulos}" var="modulo">
					<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${processoVO.modulo.chavePrimaria == modulo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modulo.descricao}"/>
					</option>		
				</c:forEach>
			</select><br />
			<label class="rotulo campoObrigatorio" for="idOperacao"><span class="campoObrigatorioSimbolo">* </span>Opera��o batch:</label>
			<select name="operacao" id="idOperacao" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${operacoesBatchs}" var="operacao">
					<option value="<c:out value="${operacao.chavePrimaria}"/>" <c:if test="${processo.operacao.chavePrimaria == operacao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${operacao.descricao}"/>
					</option>		
				</c:forEach>
			</select><br />
			
			<label class="rotulo campoObrigatorio" for="idPeriodicidade"><span class="campoObrigatorioSimbolo">* </span>Periodicidade:</label>
			<select name="periodicidade" id="idPeriodicidade" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${periodicidades}" var="periodicidade">
					<option value="<c:out value="${periodicidade.codigo}"/>" <c:if test="${processo.periodicidade == periodicidade.codigo}">selected="selected"</c:if>>
					<c:out value="${periodicidade.descricao}"/>
					</option>		
				</c:forEach>
			</select><br />
			
			<label class="rotulo" id="rotuloDiaNaoUtil">Permite dia n�o �til?</label>
			<input class="campoRadio" type="radio" value="true" name="diaNaoUtil" id="diaNaoUtilSim" <c:if test="${processo.diaNaoUtil eq 'true'}">checked</c:if>><label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" value="false" name="diaNaoUtil" id="diaNaoUtilNao" <c:if test="${processo.diaNaoUtil ne 'true'}">checked</c:if>><label class="rotuloRadio">N�o</label>
			<br class="quebraLinha2"/>
			
			<label class="rotulo" for="emailResponsavel">E-mail do Respons�vel:</label>			
			<input type="text" id="emailResponsavel" name="emailResponsavel" class="campoTexto"  onkeypress="return formatarCampoEmail(event)" size="40" maxlength="80" value="${processo.emailResponsavel}"/>		
		</fieldset>
		
		<fieldset  class="colunaFinal">
			<label class="rotulo campoObrigatorio" id="rotuloDataAgendada" for="dataAgendada"><span class="campoObrigatorioSimbolo">* </span>Data In�cio Agendamento:</label>
			<input type="text" id="dataAgendada" name="dataInicioAgendamento" class="campoData" size="10" maxlength="10" value="${processoVO.dataInicioAgendamento}"/><br />
			<label class="rotulo" id="rotuloDataAgendadaFinal" for="dataAgendadaFinal">Data Final Agendamento:</label>
			<input type="text" id="dataAgendadaFinal" name="dataFinalAgendamento" class="campoData" size="10" maxlength="10" value="${processoVO.dataFinalAgendamento}"/><br />

			<label class="rotulo" id="rotuloHora" for="hora">Hora do dia:</label>
			<select id="hora" class="campoTexto campoHorizontal" name="hora" <ggas:campoSelecionado campo="hora"/> >
				<option value="-1">Selecione</option>
			    <c:forEach begin="0" end="23" var="hora">
				  <option value="${hora}" <c:if test="${processoVO.hora == hora}">selected="selected"</c:if>><c:out value="${hora}"/></option>
				</c:forEach>
			</select>
			<label class="rotuloHorizontal rotuloInformativo" for="horaInicialDia">h</label><br class="quebraLinha2" />
			
			<label class="rotulo" for="idSituacao"><span class="campoObrigatorioSimbolo">* </span>Situa��o:</label>
			<select name="situacao" id="idSituacao" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${situacoes}" var="situacao">
					<option value="<c:out value="${situacao.codigo}"/>" <c:if test="${processo.situacao == situacao.codigo}">selected="selected"</c:if>>
					<c:out value="${situacao.descricao}"/>
					</option>		
				</c:forEach>
			</select>
		</fieldset>
		<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campo obrigat�rio apenas para selecionar a Opera��o batch.</p>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
	    <input name="button" class="bottonRightCol2 botaoGrande1" id="botaoAlterar" value="Alterar"  type="submit">
 	</fieldset>
 	<token:token></token:token>
</form:form>