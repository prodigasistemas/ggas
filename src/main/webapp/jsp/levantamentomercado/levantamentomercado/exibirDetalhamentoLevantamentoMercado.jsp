<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Detalhar Levantamento de Mercado<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script type="text/javascript">

function pesquisar() {
	submeter('agenteForm','pesquisarAgente');
}

function alterarAgente() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms["agenteForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('agenteForm','exibirAlteracaoAgente');
    }
	
}

function detalharAgente(chave){
	document.forms["agenteForm"].chavePrimaria.value = chave;
	submeter('agenteForm','exibirDetalhamentoAgente');
}

function limparFormulario(){
	document.getElementById('nome').value = "";
	document.forms['agenteForm'].habilitado[0].checked = true;
		
}

function alterar() {
	submeter('lmForm', 'exibirAlteracaoLevantamentoMercado');
}

function removerAgente(){
	
	var selecao = verificarSelecao();	
	document.getElementById('chavePrimaria').value = document.getElementById('chavesPrimarias').value;
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('agenteForm', 'removerAgente');
		}
    }
}

function cancelar(){
	submeter('lmForm', 'exibirPesquisaLevantamentoMercado');
}

function desabilitarCampoQuantidade(){
	document.forms["lmForm"].quantidadeUnidadesConsumidoras.value = "";
	document.forms["lmForm"].quantidadeUnidadesConsumidoras.disabled = true;
}

function habilitarCampoQuantidade(){
	document.forms[0].quantidadeUnidadesConsumidoras.disabled = false;
}

</script>

<form action="exibirDetalhamentoLevantamentoMercado" id="lmForm" name="lmForm"  modelAttribute="levantamentoMercado" >
	<fieldset class="conteinerPesquisarIncluirAlterar" class="margin-bottom: 50px">
		<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${ levantamentoMercado.chavePrimaria }" />
		<fieldset id="conteinerImovelLevantamentoMercado" title="Dados do Im�vel" class="conteinerDados">
			<div class="coluna detalhamentoColunaLarga ">
				<input type="hidden" id="idImovel" name="idImovel" value="${ idImovel }" />
				<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o Im�vel:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${levantamentoMercado.imovel.nome}"/></span><br />
				<label class="rotulo" id="rotuloEndereco" for="endereco" >Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ enderecoImovel }"/></span><br />
				<label class="rotulo" id="rotuloCondominioSimNao" for="condominioSimNao" >Condom�nio:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ condominio }"/></span><br />
			</div>	
			
			<div class="coluna2" style="float: right">
				<label class="rotulo" id="rotuloDescricao" for="descricao" >Cep:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ levantamentoMercado.imovel.quadraFace.endereco.cep.cep }"/></span><br />
				<label class="rotulo" id="rotuloEndereco" for="endereco" >Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ imovel.descricaoComplemento }"/></span><br />
				<label class="rotulo" id="rotuloCondominioSimNao" for="condominioSimNao" >Quadra:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ levantamentoMercado.imovel.quadraFace.quadra.numeroQuadra }"/></span><br />
				<label class="rotulo" id="rotuloCondominioSimNao" for="condominioSimNao" >Face da Quadra:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ imovel.quadraFace.endereco.numero }"/></span><br />
			</div>			
		</fieldset></br></br>
		
		<fieldset id="conteinerClienteLevantamentoMercado" style="margin-bottom: 30px" class="conteinerDados">
			<div class="coluna detalhamentoColunaLarga">
				<label class="rotulo" id="rotuloDescricao" for="descricao" >Nome:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ contato.nome }"/></span><br />
				<label class="rotulo" id="rotuloEndereco" for="endereco" >CPF/CNPJ:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ numeroDocumentoCliente }"/></span><br />
			</div>	
			<div class="colunaDir" style="float: right">
				<label class="rotulo" id="rotuloEndereco" for="endereco" >Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ enderecoCliente }"/></span><br />
				<label class="rotulo" id="rotuloEmail" for="email" >E-mail:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${ contato.email }"/></span><br />
			</div>			
		</fieldset>
		
		<fieldset  class="colunaEsq coluna detalhamentoColunaLarga" id="levantamentoMercado" >
				
				<label class="rotulo" id="rotuloAgente" for="agente" >Agente:</label>
				<span class="itemDetalhamento" style="margin-bottom: -20px" id="agente" name="agente" ><c:out value="${ imovel.agente.funcionario.nome }"/></span>
				
				<label class="rotulo" id="rotuloSegmento" for="agente" >Segmento:</label>
				<span class="itemDetalhamento" id="segmento" name="segmento" ><c:out value="${ levantamentoMercado.segmento.descricao }"/></span><br />
				
				<label class="rotulo" id="rotuloVigencia"  for="vigencia" >Vig�ncia:</label>
				<input tabindex="5" class="campoData campo2Linhas campoHorizontal" type="text" id="vigenciaInicio" name="vigenciaInicio"  style="margin-top: 8px" disabled="disabled" value="${ vigenciaInicio }">
				<label class="rotuloEntreCampos" style="margin-top: 4px">a</label>
				<input tabindex="6" class="campoData campo2Linhas campoHorizontal" type="text" id="vigenciaFim" name="vigenciaFim"  style="margin-top: 8px; padding-left: 8px;" disabled="disabled" value="${ vigenciaFim }" ><br/><br class="quebraLinha"/>
		</fieldset>
			
		<fieldset style="float: right;"  id="ImovelAbaIdLoc">
			<div class="alinhaDivDireita">
			
				<label class="rotulo" style="width: 100px;" id="rotuloGeradorEnergia" for="geradorEnergia" >Un. Consumidora:</label>					
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno" style="margin-left: 6px" id="unidades" name="unidades" ><c:out value="${ unidades }"/></span><br />
								
				<label class="rotulo" style="width: 106px" id="rotuloUsoGerador" for="usoGerador" >Quantidade:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"  id="quantidadeUnidadesConsumidoras" name="quantidadeUnidadesConsumidoras" ><c:out value="${ levantamentoMercado.quantidadeUnidadesConsumidoras }"/></span><br />
				
				<label class="rotulo" style="width: 106px" id="rotuloStatus" for="status" >Status:</label>
				<span class="itemDetalhamento itemDetalhamentoMedioPequeno"  id="status" name="status" ><c:out value="${ levantamentoMercado.status.descricao }"/></span><br />
				
				</br></br>
			</div>
		</fieldset>	
	</fieldset>	

	<div id="gridDetalhamentoTipoCombustivel">
		<jsp:include page="/jsp/levantamentomercado/levantamentomercado/gridDetalhamentoTipoCombustivelLevantamentoMercado.jsp"></jsp:include>	
	</div>
	
	<fieldset class="conteinerBotoes">
   		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
   		<input name="button" value="Alterar" class="bottonRightCol2 botaoGrande1" onclick="alterar()" type="button">
	</fieldset>
</form>