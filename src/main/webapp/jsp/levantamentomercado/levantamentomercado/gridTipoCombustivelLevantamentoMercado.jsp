<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
$(document).ready(function(){
	var indexList = document.forms[0].indexList.value;
	var idTipoCombustivel = document.forms[0].tipoCombustivel.value;
	var buttonAlterar = document.getElementById("buttonAlterar");
	
	if(indexList > -1 && idTipoCombustivel > -1){
		document.getElementById("buttonAlterar").disabled = false;
		document.getElementById("btnAdicionar").disabled = true;
		
	}else{
		document.getElementById("buttonAlterar").disabled = true;
		document.getElementById("btnAdicionar").disabled = false;
	}
	
});


	function incluirTipoCombustivel() {
		var idTipoCombustivel = document.forms[0].tipoCombustivel.value;
		var consumo = document.forms[0].consumo.value;
		var preco = document.forms[0].preco.value;
		var fornecedor = document.forms[0].fornecedor.value;
		var periodicidade = document.forms[0].periodicidade.value;
		var modalidadeUso = document.forms[0].modalidadeUso.value;
		var redeEstado = document.forms[0].redeEstado.value;
		var localizacaoCentral = document.forms[0].localizacaoCentral.value;
		var central = document.forms[0].central.value;
		var localizacaoAbrigo = document.forms[0].localizacaoAbrigo.value;
		var tipoCilindro = document.forms[0].tipoCilindro.value;
		var redeMaterial = document.forms[0].redeMaterial.value;
		var redeTempo = document.forms[0].redeTempo.value;
		var ventilacao = document.forms[0].ventilacao.value;
		var observacoes = document.forms[0].observacoes.value;
		

		var indexList = -1;
		var noCache = "noCache=" + new Date().getTime();

		var url = "adicionarTipoCombustivel?tipoCombustivel="
				+ idTipoCombustivel + "&consumo=" + consumo + "&preco=" + preco
				+ "&fornecedor=" + fornecedor + "&periodicidade="
				+ periodicidade + "&modalidadeUso=" + modalidadeUso
				+ "&redeEstado=" + redeEstado + "&localizacaoCentral="
				+ localizacaoCentral + "&central=" + central
				+ "&localizacaoAbrigo=" + localizacaoAbrigo + "&tipoCilindro="
				+ tipoCilindro + "&redeMaterial=" + redeMaterial
				+ "&tipoCilindro=" + tipoCilindro + "&redeMaterial="
				+ redeMaterial + "&redeTempo=" + redeTempo + "&ventilacao="
				+ ventilacao + "&observacoes=" + observacoes + "&indexList="
				+ indexList + "&" + noCache;
		
		carregarFragmento('gridTipoCombustivel', url);
		
		var buttonAlterar = document.getElementById("buttonAlterar");
		buttonAlterar.disabled = true;
	}

	function alterarTipoCombustivel() {
		var idTipoCombustivel = document.forms[0].tipoCombustivel.value;
		var consumo = document.forms[0].consumo.value;
		var preco = document.forms[0].preco.value;
		var fornecedor = document.forms[0].fornecedor.value;
		var periodicidade = document.forms[0].periodicidade.value;
		var modalidadeUso = document.forms[0].modalidadeUso.value;
		var redeEstado = document.forms[0].redeEstado.value;
		var localizacaoCentral = document.forms[0].localizacaoCentral.value;
		var central = document.forms[0].central.value;
		var localizacaoAbrigo = document.forms[0].localizacaoAbrigo.value;
		var tipoCilindro = document.forms[0].tipoCilindro.value;
		var redeMaterial = document.forms[0].redeMaterial.value;
		var redeTempo = document.forms[0].redeTempo.value;
		var ventilacao = document.forms[0].ventilacao.value;
		var observacoes = document.forms[0].observacoes.value;
		var indexList = document.forms[0].indexList.value;
		var noCache = "noCache=" + new Date().getTime();

		var url = "adicionarTipoCombustivel?tipoCombustivel="
				+ idTipoCombustivel + "&consumo=" + consumo + "&preco=" + preco
				+ "&fornecedor=" + fornecedor + "&periodicidade="
				+ periodicidade + "&modalidadeUso=" + modalidadeUso
				+ "&redeEstado=" + redeEstado + "&localizacaoCentral="
				+ localizacaoCentral + "&central=" + central
				+ "&localizacaoAbrigo=" + localizacaoAbrigo + "&tipoCilindro="
				+ tipoCilindro + "&redeMaterial=" + redeMaterial
				+ "&tipoCilindro=" + tipoCilindro + "&redeMaterial="
				+ redeMaterial + "&redeTempo=" + redeTempo + "&ventilacao="
				+ ventilacao + "&observacoes=" + observacoes + "&indexList="
				+ indexList + "&" + noCache;

		var sucesso = carregarFragmento('gridTipoCombustivel', url);
		

		if(sucesso){
			var buttonAdicionar = document.getElementById("btnAdicionar");
			var buttonAlterar = document.getElementById("buttonAlterar");
			buttonAdicionar.disabled = false;
			buttonAlterar.disabled = true;
		}
	}

	function exibirAlteracaoTipoCombustivel(indice, idTipoCombustivel,
			consumo, preco, idFornecedor, idPeriodicidade, idModalidade,
			idEstadoRede, central, localizacaoCentral, localizacaoAbrigo,
			idCilindro, idRedeMaterial, redeTempo, ventilacao, observacoes, listaTipoAparelho) {

		if(idTipoCombustivel == -2){
			$("#preco").addClass("campoDesabilitado").css("background","#DCDCDC").attr("disabled","disabled").val(""); 
			$("label[for=preco], label[for=preco] span.campoObrigatorioSimbolo").addClass("rotuloDesabilitado");
			$("#consumo").addClass("campoDesabilitado").css("background","#DCDCDC").attr("disabled","disabled").val(""); 
			$("label[for=consumo], label[for=consumo] span.campoObrigatorioSimbolo").addClass("rotuloDesabilitado");
		}
		
		document.forms[0].indexList.value = indice;
		document.forms[0].tipoCombustivel.value = idTipoCombustivel;
		document.forms[0].consumo.value = consumo.replace(".",",");
		document.forms[0].preco.value = preco.replace(".",",");
		document.forms[0].fornecedor.value = idFornecedor;
		document.forms[0].periodicidade.value = idPeriodicidade;
		document.forms[0].modalidadeUso.value = idModalidade;
		document.forms[0].redeEstado.value = idEstadoRede;
		document.forms[0].central.value = central;
		document.forms[0].localizacaoCentral.value = localizacaoCentral;
		document.forms[0].localizacaoAbrigo.value = localizacaoAbrigo;
		document.forms[0].tipoCilindro.value = idCilindro;
		document.forms[0].redeMaterial.value = idRedeMaterial;
		document.forms[0].redeTempo.value = redeTempo;
		document.forms[0].ventilacao.value = ventilacao;
		document.forms[0].observacoes.value = observacoes;

		var noCache = "noCache=" + new Date().getTime();

		var url = "atualizarGridTipoAparelho?idTipoCombustivel=" + idTipoCombustivel
				+ "&" + noCache;
		
		carregarFragmento('gridTipoAparelho', url);
		
		var buttonAdicionar = document.getElementById("btnAdicionar");
		var buttonAlterar = document.getElementById("buttonAlterar");
		buttonAdicionar.disabled = true;
		buttonAlterar.disabled = false;
	}

	function removerTipoCombustivel( indexLista ) {
		var noCache = "noCache=" + new Date().getTime();
		$("#gridTipoCombustivel").load(
				"removerTipoCombustivel?indexLista=" + indexLista
						+ "&" + noCache);
	}
	
	function desabilitarCamposConsumoPreco(){
		var opcaoSelecionada = $("#tipoCombustivel option:selected").val();
		if(opcaoSelecionada == -2){
			$("#preco").css('background-color', '#DCDCDC');
			$("#preco").css('background-image-color', 'red');
			$("#preco").attr("disabled","disabled").val("");
			$("label[for=preco], label[for=preco] span.campoObrigatorioSimbolo").addClass("rotuloDesabilitado");
			$("#consumo").addClass("campoDesabilitado").css("background","#DCDCDC").attr("disabled","disabled").val(""); 
			$("label[for=consumo], label[for=consumo] span.campoObrigatorioSimbolo").addClass("rotuloDesabilitado");
		}else{
			$("#preco").removeAttr("disabled");
			$("#preco").css("background-color","#FFFFFF");
			$("label[for=preco], label[for=preco] span.campoObrigatorioSimbolo").removeClass("rotuloDesabilitado");
			$("#consumo").removeClass("campoDesabilitado").removeAttr("disabled").css("background","#FFFFFF");
			$("label[for=consumo], label[for=consumo] span.campoObrigatorioSimbolo").removeClass("rotuloDesabilitado");
		}
	}
	
	
</script>
<fieldset class="conteinerBloco">
			<fieldset id="dadosCombustivelLM">
			<fieldset  class="colunaEsq detalhamentoColunaLarga" id="levantamentoMercado" >
			
				
				<label class="rotulo" id="rotuloTipoCombustivel" for="tipoCombustivel" ><span class="campoObrigatorioSimbolo">* </span>Tipo Combust�vel:</label>
				<select name="tipoCombustivel" id="tipoCombustivel" class="campoSelect" onChange="desabilitarCamposConsumoPreco();">
				<option value="-1">Selecione</option>
					<c:forEach items="${tiposCombustiveis}" var="combustivel">
						<option value="<c:out value="${combustivel.chavePrimaria}"/>" >
							<c:out value="${combustivel.descricao}"/>
						</option>
					</c:forEach>
				<option value="-2">N�o se Aplica</option>
				</select><br />
				
				<label class="rotulo" id="rotuloEndereco" for="consumo" ><span class="campoObrigatorioSimbolo">* </span>Consumo:</label>
				<input class="campoTexto" style="width: 217px;" type="text" id="consumo" name="consumo"  
				onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,17,2);" maxlength="17" size="5"  /><br />
				
				<label class="rotulo" id="rotuloPreco" for="preco" ><span class="campoObrigatorioSimbolo">* </span>Pre�o:</label>
				<input class="campoTexto campoValorReal campoTextoGrande" style="width: 194px;" type="text" id="preco"  name="preco"   maxlength="18" size="18" "
				onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" /><br />
				
				<label class="rotulo" id="rotuloFornecedor" for="fornecedor" ><span class="campoObrigatorioSimbolo">* </span>Fornecedor:</label>
				<select name="fornecedor" id="fornecedor" class="campoSelect" >
				<option value="-1">Selecione</option>
					<c:forEach items="${listaFornecedor}" var="fornecedor">
						<option value="<c:out value="${fornecedor.chavePrimaria}"/>" >
							<c:out value="${fornecedor.descricao}"/>
						</option>
					</c:forEach>
				<option value="-2">N�o se Aplica</option>
				</select><br />
						
				<label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>Periodicidade:</label>
	            <select name="periodicidade" class="campoSelect" id="periodicidade" >
	                <option value="-1">Selecione</option>
	                <c:forEach items="${listaPeriodicidades}" var="periodicidade">
	                    <option value="<c:out value="${periodicidade.chavePrimaria}"/>" >
	                        <c:out value="${periodicidade.descricao}"/>
	                    </option>       
	                </c:forEach>    
           		<option value="-2">N�o se Aplica</option>
           		</select><br />	
           		
				<label class="rotulo" id="rotuloModalidadeUso" for="modalidadeUso" ><span class="campoObrigatorioSimbolo">* </span>Modalidade de Uso:</label>
				<select name="modalidadeUso" id="modalidadeUso" class="campoSelect">
				<option value="-1">Selecione</option>
					<c:forEach items="${listaModalidadeUso}" var="modalidade">
						<option value="<c:out value="${modalidade.chavePrimaria}"/>" >
							<c:out value="${modalidade.descricao}"/>
						</option>
					</c:forEach>
				<option value="-2">N�o se Aplica</option>
				</select><br />
				
				<label class="rotulo" id="rotuloRedeEstado" for="redeEstado" ><span class="campoObrigatorioSimbolo">* </span>Estado da Rede:</label>
				<select name="redeEstado" id="redeEstado" class="campoSelect">
				<option value="-1">Selecione</option>
					<c:forEach items="${listaEstadoRede}" var="redeEstado">
						<option value="<c:out value="${redeEstado.chavePrimaria}"/>" >
							<c:out value="${redeEstado.descricao}"/>
						</option>
					</c:forEach>
				</select><br />
				
			</fieldset>
				
			<fieldset style="float: right;"  id="colunaDireitaLM" style="margin-left: 50px;">
					
				<label class="rotulo"      id="rotuloCentral" for="email" ><span class="campoObrigatorioSimbolo">*</span>Central:</label>
				<input class="campoRadio"  type="radio" name="central" id="centralSim"  value="true" >
				<label class="rotuloRadio" for="indicarCentralSim">Sim</label>
				<input class="campoRadio"  style="margin-left: 128px;" type="radio" name="central" id="centralNao"  value="false" checked="checked">
				<label class="rotuloRadio" for="indicadorCentralNao">N�o</label>
				
				<label class="rotulo" id="rotuloLocalizacao" for="localizacao" >Localiza��o Central:</label>
				<input class="campoTexto" type="text" id="localizacaoCentral" name="localizacaoCentral" style="width: 217px;" maxlength="255" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" "><br />
			
				<label class="rotulo" id="rotuloLocalizacaoAbrigo" for="localizacaoAbrigo" >Localiza��o Abrigo:</label>
				<input class="campoTexto" type="text" id="localizacaoAbrigo" name="localizacaoAbrigo" style="width: 217px;" maxlength="255" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" ><br />
				
				<label class="rotulo" id="rotuloCilindro" for="tipoCilindro" >Cilindro:</label>
				<select name="tipoCilindro" id="tipoCilindro" class="campoSelect">
				<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoCilindro}" var="cilindro">
						<option value="<c:out value="${cilindro.chavePrimaria}"/>" >
							<c:out value="${cilindro.descricao}"/>
						</option>
					</c:forEach>
				</select><br />
				
				<label class="rotulo" id="rotuloRedeMaterial" for="redeMaterial" ><span class="campoObrigatorioSimbolo">* </span>Material de Rede:</label>
				<select name="redeMaterial" id="redeMaterial" class="campoSelect">
				<option value="-1">Selecione</option>
					<c:forEach items="${listaMaterialRede}" var="material">
						<option value="<c:out value="${material.chavePrimaria}"/>" >
							<c:out value="${material.descricao}"/>
						</option>
					</c:forEach>
				<option value="-2">N�o se Aplica</option>
				</select><br />
				
				<label class="rotulo" id="rotuloRedeTempo" for="redeTempo" >Rede Tempo:</label>
				<input class="campoTexto" type="text" id="redeTempo" name="redeTempo" style="width: 217px;" maxlength="255" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" ><br />
				
				<label class="rotulo"      id="rotuloVentilacao" for="ventilacao" ><span class="campoObrigatorioSimbolo">* </span>Ventila��o:</label>					
				<input class="campoRadio"  type="radio" name="ventilacao" id="ventilacaoSim"  value="true" >
				<label class="rotuloRadio" for="indicaGeradorEnergiaSim">Sim</label>
				<input class="campoRadio"  style="margin-left: 128px;" type="radio" name="ventilacao" id="ventilacaoNao"  value="false" checked="checked">
				<label class="rotuloRadio" for="indicaGeradorEnergiaNao">N�o</label>
				
			</fieldset>
		</fieldset>
		<fieldset id="dadosAparelho" style="padding-top:20px; padding-bottom:20px;">
				
			<div id="gridTipoAparelho">
				<jsp:include page="/jsp/levantamentomercado/levantamentomercado/gridTipoAparelho.jsp"></jsp:include>	
			</div>
				
			<fieldset  id="colunaObservacoesTipoCombustivel" style="padding-top: 20px" >
				<label class="rotulo" style="width: 345px;" for="observacoesLM" >Observa��es:</label>
		    	<textarea  id="observacoes"   maxlength="250"  name="observacoes"  class="campoTexto campoTextoGrande" rows="3" cols="33" > </textarea>	
			</fieldset>
		</fieldset>
		
		<fieldset  id="dadosLMGRID">
			<input name="buttonLimpar" id="buttonLimpar" class="bottonRightCol" value="Limpar" type="button" onClick="limparCampos();">
			<input name="btnAdicionar" id="btnAdicionar" style="margin-bottom: 10px;" class="bottonRightCol" value="Adicionar" type="button" onClick="incluirTipoCombustivel();">
			<input name="buttonAlterar" id="buttonAlterar" style="margin-bottom: 10px;" class="bottonRightCol" value="Alterar" type="button" onClick="alterarTipoCombustivel();">
			
			<c:set var="i" value="0" />
			<display:table class="dataTableGGAS" name="sessionScope.listaTipoCombustivelLM" sort="list" id="levantamentoMercadoTipoCombustivel" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	
	 			<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.descricao" title="Combust�vel">
		        	<c:choose>
						<c:when test="${ levantamentoMercadoTipoCombustivel.tipoCombustivel.chavePrimaria == -2 }">
			            	<c:out value="N�o se Aplica"/>
						</c:when>
						<c:otherwise>
							<c:out value="${ levantamentoMercadoTipoCombustivel.tipoCombustivel.descricao }"/>
						</c:otherwise>
					</c:choose>
				</display:column>
		        
		        <display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.consumo" title="Consumo" >
		           <fmt:formatNumber
						value="${levantamentoMercadoTipoCombustivel.consumo}" maxFractionDigits="2"
						minFractionDigits="2"/>
				</display:column>
		        
		    	<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.preco" title="Pre�o" >
		            <fmt:formatNumber
						value="${levantamentoMercadoTipoCombustivel.preco}" maxFractionDigits="2"
						minFractionDigits="2"/>
				</display:column>
				
				<display:column style="text-align: center; width: 25px">
					<a href="javascript:exibirAlteracaoTipoCombustivel('${i}','${ levantamentoMercadoTipoCombustivel.tipoCombustivel.chavePrimaria }','${ levantamentoMercadoTipoCombustivel.consumo }',
					'${ levantamentoMercadoTipoCombustivel.preco }', '${ levantamentoMercadoTipoCombustivel.fornecedor.chavePrimaria }', '${ levantamentoMercadoTipoCombustivel.periodicidade.chavePrimaria }',
					'${ levantamentoMercadoTipoCombustivel.modalidadeUso.chavePrimaria }', '${ levantamentoMercadoTipoCombustivel.redeEstado.chavePrimaria }', '${ levantamentoMercadoTipoCombustivel.central }',
					'${ levantamentoMercadoTipoCombustivel.localizacaoCentral }', '${ levantamentoMercadoTipoCombustivel.localizacaoAbrigo }', '${ levantamentoMercadoTipoCombustivel.tipoBotijao.chavePrimaria }',
					'${ levantamentoMercadoTipoCombustivel.redeMaterial.chavePrimaria }', '${ levantamentoMercadoTipoCombustivel.redeTempo }', '${ levantamentoMercadoTipoCombustivel.ventilacao }', 
					'${ levantamentoMercadoTipoCombustivel.observacoes }');">
						<img title="Alterar Material" alt="Alterar Material"  src="<c:url value="/imagens/16x_editar.gif"/>">
					</a> 
				</display:column>
				
				<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
					<a onclick="return confirm('Deseja excluir o Tipo Combust�vel?');" href="javascript:removerTipoCombustivel('${ i }');">
						<img title="Excluir Tipo Combust�vel" alt="Excluir Tipo Combust�vel"  src="<c:url value="/imagens/deletar_x.png"/>">
					</a> 
				</display:column>
				<c:set var="i" value="${i+1}" />
		    </display:table>	
		</fieldset>	
</fieldset>