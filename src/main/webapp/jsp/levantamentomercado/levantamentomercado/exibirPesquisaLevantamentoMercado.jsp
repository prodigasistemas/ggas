<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Pesquisar Levantamento de Mercado<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">

	$(document).ready(function() {
		var valor = document.forms['lmForm'].matriculaImovelTexto.value;
		if (valor == 0) {
			document.forms['lmForm'].matriculaImovelTexto.value = "";
		}
		
		// Datepicker
		$(".campoData").datepicker({
			changeYear : true,
			showOn : 'button',
			buttonImage : '<c:url value="/imagens/calendario.gif"/>',
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy'
		});
		
		
		var valor = '${visitaAgendada}';
		if(valor == "todos"){
			$("#visitaAgendadaTodos").prop("checked", true);
		}
	});

	function pesquisar() {
		submeter('lmForm', 'pesquisarLevantamentoMercado');
	}

	function detalharLevantamentoMercado(chave) {
		document.forms["lmForm"].chavePrimaria.value = chave;
		submeter('lmForm', 'exibirDetalhamentoLevantamentoMercado');
	}

	function alterarLM() {
		var selecao = verificarSelecao();
		document.forms['lmForm'].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		if (selecao == true) {
			submeter('lmForm', 'exibirAlteracaoLevantamentoMercado');
		}
	}

	function incluir() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms[0].idImovel.value = obterValorUnicoCheckboxSelecionado();
			submeter("lmForm", "exibirInclusaoLevantamentoMercado");
		}
	}

	function exibirPopupPesquisaImovel() {
		popup = window
				.open(
						'exibirPesquisaImovelCompletoPopup?postBack=true',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado) {
		document.forms[0].idImovel.value = idSelecionado;
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");

		if (idSelecionado != '') {
			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {
										idImovel.value = imovel["chavePrimaria"];
										matriculaImovel.value = imovel["chavePrimaria"];
										nomeFantasia.value = imovel["nomeFantasia"];
										numeroImovel.value = imovel["numeroImovel"];
										cidadeImovel.value = imovel["cidadeImovel"];
										indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
									}
								},
								async : false
							}
					);
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
		}

		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if (indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	function limparCamposPesquisa() {
		document.getElementById('descricaoImovelTexto').value = "";
		document.getElementById('complementoTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('condominioAmbos').checked = "true";
		$(".campoSelect").val("");
		$(".campoData").val("");
		$(".campoTexto").val("");
		$("#visitaAgendadaTodos").prop("checked", true);
		

	}

	function agendarVisitaLM() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms['lmForm'].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('lmForm', 'exibirAgendaVisitaAgente');
		}
	}

	function pesquisarImovelLM2() {
		submeter('lmForm', 'pesquisarImoveis');
	}

	function gerarPropostaLM() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms['lmForm'].chavePrimariaLM.value = obterValorUnicoCheckboxSelecionado();
			submeter('lmForm', 'exibirInclusaoPropostaLevatamentoMercado');
		}
	}

	function removerLevantamentoMecado() {

		var selecao = verificarSelecao();

		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				document.forms['lmForm'].chavePrimariaLM.value = obterValorUnicoCheckboxSelecionado();
				submeter('lmForm', 'removerLevantamentoMercado');
			}
		}
	}
	
	function gerarEVTE() {

		var selecao = verificarSelecao();

		if (selecao == true) {
			document.forms['lmForm'].chavePrimariaLM.value = obterValorUnicoCheckboxSelecionado();
			submeter('lmForm', 'gerarEVTE');
		}
	}
	
</script>

<form action="pesquisarLevantamentoMercado" id="lmForm" name="lmForm" method="post" modelAttribute="levantamentoMercado" >
	<input name="chavePrimariaLM" type="hidden" id="chavePrimariaLM">
 	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarImovel" class="colunaDir">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${ levantamentoMercado.chavePrimaria }" />
				
				<input name="idImovel" type="hidden" id="idImovel" value="${ idImovel }">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${ nomeFantasiaImovel  }">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${ matriculaImovel  }">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${ numeroImovel  }">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${ cidadeImovel  }">
				<input name="condominio" type="hidden" id="condominio" value="${  condominio  }">

				<label class="rotulo" id="rotuloNomeFantasia" for="descricaoImovelTexto">Descri��o:</label>
				<input class="campoTexto" type="text" id="descricaoImovelTexto" name="descricaoImovelTexto" style="width: 168px;" maxlength="255" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${ imovel.nome }"><br />
				
				<label class="rotulo" id="rotuloBairro" for="descricaoBairro">Bairro:</label>
				<input class="campoTexto" type="text" id="descricaoBairro" name="descricaoBairro" style="width: 168px;" maxlength="255" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${ bairro }"><br />
				
				<label class="rotulo" id="rotuloRua" for="descricaoRua">Rua:</label>
				<input class="campoTexto" type="text" id="descricaoRua" name="descricaoRua" style="width: 168px;" maxlength="255" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${ rua }"><br />
				
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoTexto" onkeypress="return formatarCampoDecimal(event,this,10 ,10);" style="width: 168px;"  type="text" id="matriculaImovelTexto" name="matriculaImovelTexto" value="${ imovel.chavePrimaria }" /><br />		

				<label class="rotulo" id="rotuloComplemento" for="complemento">Complemento:</label>
				<input class="campoTexto" type="text" id="complementoTexto" name="complementoTexto" style="width: 168px;" maxlength="255"  onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${ imovel.descricaoComplemento }"><br />
				
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero do Imovel:</label>
				<input class="campoTexto" onkeypress="return formatarCampoDecimal(event,this,10 ,10);" style="width: 169px;"  type="text" id="numeroImovelTexto" name="numeroImovelTexto" value="${ imovel.numeroImovel }" /><br />		
				
				<label class="rotulo" id="rotuloImovelCondominio" for="condominioSim">O im�vel � condom�nio?</label>
				<input class="campoRadio" type="radio" value="true" name="indicadorCondominio" id="condominioSim" <c:if test="${ imovel.condominio eq 'true' }">checked</c:if> onclick="">
				<label for="condominioSim" class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" value="false" name="indicadorCondominio" id="condominioNao" <c:if test="${ imovel.condominio eq 'false' }">checked</c:if> onclick="">
				<label for="condominioNao" class="rotuloRadio">N�o</label>
				<input class="campoRadio" type="radio" value="ambos" name="indicadorCondominio" id="condominioAmbos" <c:if test="${ imovel.condominio ne 'true' && imovel.condominio ne 'false' }">checked</c:if> onclick="">
				<label for="condominioAmbos" class="rotuloRadio">Todos</label><br class="quebraRadio">
				
			</div>
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq true }">checked</c:if> >
				<label class="rotuloRadio" for="indicadorUso">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq false }">checked</c:if>>
				<label class="rotuloRadio" for="indicadorUso">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitadoTodos" value="todos">
				<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset id="pesquisarImovel2" class="colunaDir" >
				
				<label class="rotulo" id="rotuloStatus" for="idStatus" >Status:</label>
				<select name="idStatus" id="idStatus" class="campoSelect">
				<option value="-1">Selecione</option>
					<c:forEach items="${listaStatus}" var="status">
						<option value="<c:out value="${status.chavePrimaria}"/>" <c:if test="${idStatus == status.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${status.descricao}"/>
						</option>
					</c:forEach>
				</select><br />
				
				<label class="rotulo" id="rotuloAgente" for="agente" >Agente:</label>
				<select name="idAgente" id="idAgente" class="campoSelect">
				<option value="-1">Selecione</option>
					<c:forEach items="${listaAgentes}" var="agente">
						<option value="<c:out value="${agente.chavePrimaria}"/>" <c:if test="${idAgente == agente.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${agente.funcionario.nome}"/>
						</option>
					</c:forEach>
				</select><br />
				
				<label class="rotulo" id="rotuloVigencia"  for="dataVisita" >Data Visita:</label>
				<input tabindex="5" class="campoData campo2Linhas campoHorizontal" type="text" id="dataVisita" name="dataVisita"  style="margin-top: 8px" value="${ dataVisita }">
				
				<label class="rotulo" id="rotuloPeriodo"  for=""periodoVisitaFinal"" >Per�odo Visita:</label>
				<input tabindex="5" class="campoData campo2Linhas campoHorizontal" type="text" id="periodoVisitaInicial" name="periodoVisitaInicial"  style="margin-top: 8px" value="${ periodoVisitaInicial }">
				<label class="rotuloEntreCampos" style="margin-top: 4px; ">a</label>
				<input tabindex="6" class="campoData campo2Linhas campoHorizontal" type="text" id="periodoVisitaFinal" name="periodoVisitaFinal"  style="margin-top: 8px;" value="${ periodoVisitaFinal }"><br class="quebraLinha"/>
				
				<label class="rotulo" id="rotuloVisitaAgendada" for="visitaAgendada">Visita Agendada:</label>
				<input class="campoRadio" type="radio" value="true" name="visitaAgendada" id="visitaAgendadaSim" <c:if test="${visitaAgendada eq true }">checked</c:if>>
				<label for="visitaAgendada" class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" value="false" name="visitaAgendada" id="visitaAgendadaNao" <c:if test="${visitaAgendada eq false }">checked</c:if>>
				<label for="visitaAgendada" class="rotuloRadio">N�o</label>
				<input class="campoRadio" type="radio" value="todos" name="visitaAgendada" id="visitaAgendadaTodos" >
				<label for="visitaAgendada" class="rotuloRadio">Todos</label>
		</fieldset>
			
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			
			<input name="pesquisarImovel" class="bottonRightCol2" id="pesquisarImovel" type="button" value="Im�vel"  onclick="pesquisarImovelLM2();">
			
    		<input name="Button1" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" onclick="pesquisar();">
			
			<input name="Button2" class="bottonRightCol"  value="Limpar" type="button" onclick="limparCamposPesquisa();">		

		</fieldset>
	
	</fieldset>

	<c:if test="${ imoveis ne null }">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="imoveis" sort="list" id="imovel" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column style="width: 20px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="${ imovel.chavePrimaria }">
	        </display:column>
	        
	    	 <display:column  sortable="false" sortProperty="imovel.nome" title="Nome" >
	            	<c:out value="${ imovel.nome }"/>
	            </a>
			</display:column>
	    </display:table>
	    
	</c:if>
	
	<c:if test="${ listaLMVO ne null }">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaLMVO" sort="list" id="lmvo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column style="width: 20px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="${ lmvo.chavePrimaria }">
	        </display:column>
	        
	        <display:column style="width: 30px" title="Ativo">
				<c:choose>
					<c:when test="${lmvo.habilitado == true}">
						<img alt="Ativo" title="Ativo"
							src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo"
							src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        
	        <display:column  sortable="true" sortProperty="chavePrimaria" title="C�digo" style="width: 40px;">
				<a href="javascript:detalharLevantamentoMercado(<c:out value='${ lmvo.chavePrimaria }'/>);">
	            	<c:out value="${ lmvo.chavePrimaria }"/>
	            </a>
			</display:column>
	        
	    	 <display:column  sortable="true" sortProperty="imovel.nome" title="Nome" >
				<a href="javascript:detalharLevantamentoMercado(<c:out value='${ lmvo.chavePrimaria }'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${ lmvo.imovel.nome }"/>
	            </a>
			</display:column>

			<display:column sortable="true" sortProperty="contato" title="Contato">
				<a href="javascript:detalharLevantamentoMercado(<c:out value='${ lmvo.chavePrimaria }'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${ lmvo.contato }"/>
	            </a>
			</display:column>
			
			<display:column sortable="true" sortProperty="status" title="Status">
				<a href="javascript:detalharLevantamentoMercado(<c:out value='${ lmvo.chavePrimaria }'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${ lmvo.status }"/>
	            </a>
			</display:column>
			
			<display:column sortable="true" sortProperty="status" title="Visita Agendada">
				<c:choose>
					<c:when test="${lmvo.possuiAgendamento}">
						<a href="javascript:detalharLevantamentoMercado(<c:out value='${ lmvo.chavePrimaria }'/>);"><span class="linkInvisivel"></span>
	            			<c:out value="Sim"/>
	           			</a>
					</c:when>
					<c:otherwise>
						<a href="javascript:detalharLevantamentoMercado(<c:out value='${ lmvo.chavePrimaria }'/>);"><span class="linkInvisivel"></span>
	            			<c:out value="N�o"/>
	           			</a>
					</c:otherwise>
				</c:choose>
				
			</display:column>
			
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
  		<c:if test="${ not empty listaLMVO }">
  			<input name="ButtonAlterar"  class="bottonRightCol"   id="alterar" value="Alterar" type="button" onclick="alterarLM();">
  			
  			<input name="buttonAgendar"  class="bottonRightCol"   id="agendarVisita" value="Agendar" type="button" onclick="agendarVisitaLM();">
  			
  			<input name="buttonProposta" class="bottonRightCol"   id="gerarProposta" value="Gerar Proposta" type="button" onclick="gerarPropostaLM();">
  			
  			<input name="btnGerarEVTE" class="bottonRightCol"   id="btnGerarEVTE" value="Gerar EVTE" type="button" onclick="gerarEVTE();">

  			<input name="removerLM"  class="bottonRightCol"   id="removerLM" value="Remover" type="button" onclick="removerLevantamentoMecado();">
   		</c:if>
   		<c:if test="${ imoveis ne null }">
   			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
   		</c:if>
	</fieldset>
	<iframe id="download" src ="" width="0" height="0"></iframe>
</form>