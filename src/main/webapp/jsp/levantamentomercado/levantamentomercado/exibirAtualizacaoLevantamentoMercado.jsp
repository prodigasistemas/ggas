<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<h1 class="tituloInterno">Alterar Levantamento de Mercado<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em�<span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.<br /></p>

<script type="text/javascript">

$(document).ready(function() {
	
	$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

	var valor = document.forms['lmForm'].unidadesConsumidoras.value;
	if(valor == "false"){
		$("#quantidadeUnidadesConsumidoras").addClass("campoDesabilitado");
		$("label[for=quantidadeUnidadesConsumidoras], label[for=quantidadeUnidadesConsumidoras]").addClass("rotuloDesabilitado");
	}
	
	var indexList = document.forms[0].indexList.value;
	var idTipoCombustivel = document.forms[0].tipoCombustivel.value;
	var buttonAlterar = document.getElementById("buttonAlterar");
	
	if(indexList > -1 && idTipoCombustivel > -1){
		buttonAlterar.disabled = false;
	}else{
		buttonAlterar.disabled = "disabled";
	}
			
});/*-- FIM: Bloco JQuery -- */



function incluir() {
	submeter('lmForm', 'incluirLevantamentoMercado');
}


function alterarLevantamentoMercado() {
	submeter('lmForm', 'alterarLevantamentoMercado');
}


function cancelar(){
	submeter('lmForm', 'exibirPesquisaLevantamentoMercado');
}

function desabilitarCampoQuantidade(){
	$("#quantidadeUnidadesConsumidoras").addClass("campoDesabilitado").val("");   
	$("label[for=quantidadeUnidadesConsumidoras], label[for=quantidadeUnidadesConsumidoras]").addClass("rotuloDesabilitado");		
}

function habilitarCampoQuantidade(){
	$("#quantidadeUnidadesConsumidoras").removeClass("campoDesabilitado").removeAttr("disabled");	
	$("label[for=quantidadeUnidadesConsumidoras], label[for=quantidadeUnidadesConsumidoras]").removeClass("rotuloDesabilitado");
}

function adicionarTipoCombustivel(){
	submeter('lmForm', 'adicionarLevantamentoMercadoTipoCombustivel');
}

</script>

<form action="incluirLevantamentoMercado" id="lmForm" name="lmForm" method="post" modelAttribute="levantamentoMercado" >
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${levantamentoMercado.chavePrimaria}" />
		<input name="imovel" type="hidden" id="imovel" value="${ imovel.chavePrimaria }" />
		<input name="idAgente" type="hidden" id="idAgente" value="${ levantamentoMercado.imovel.agente.chavePrimaria }" />
		<input name="dataVisita" type="hidden" id="dataVisita" value="" />
		<input name="periodoVisitaInicial" type="hidden" id="periodoVisitaInicial" value="" />
		<input name="periodoVisitaFinal" type="hidden" id="periodoVisitaFinal" value="" />
		<input name="visitaAgendada" type="hidden" id="visitaAgendada" value="" />
		<input name="idStatus" type="hidden" id="idStatus" value="" />
		<input name="descricaoBairro" type="hidden" id="descricaoBairro" value="" />
		<input name="descricaoRua" type="hidden" id="descricaoRua" value="" />
		
		<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${ nomeFantasiaImovel  }" />
		<input type="hidden" id="descricaoImovelTexto" name="descricaoImovelTexto"  value="${ imovel.nome }" />
		<input type="hidden" id="complementoTexto" name="complementoTexto"  value="${ imovel.descricaoComplemento }" />
		<input type="hidden" id="matriculaImovelTexto" name="matriculaImovelTexto" value="${ imovel.chavePrimaria }" />
		<input type="hidden" id="numeroImovelTexto" name="numeroImovelTexto" value="${ imovel.numeroImovel }" />
		<input type="hidden" name="indicadorCondominio" id="condominioSim" value="${ imovel.condominio }" />
		<input name="chavePrimariaLM" type="hidden" id="chavePrimariaLM" value="${chavePrimariaLM}">
		<input type="hidden" name="indexList" id="indexList" >

	<fieldset class="conteinerPesquisarIncluirAlterar">

		<fieldset id="conteinerImovelLevantamentoMercado" title="Dados do Im�vel" class="conteinerDados">
			<div class="coluna detalhamentoColunaLarga ">
				<input type="hidden" id="idImovel" name="idImovel" value="${ imovel.chavePrimaria }" />
				<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o Im�vel:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ imovel.nome }"/></span><br />
				<label class="rotulo" id="rotuloEndereco" for="endereco" >Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ enderecoImovel }"/></span><br />
				<label class="rotulo" id="rotuloCondominioSimNao" for="condominioSimNao" >Condom�nio:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ condominio }"/></span><br />
			</div>	
			
			<div class="coluna2" style="float: right">
				<label class="rotulo" id="rotuloDescricao" for="descricao" >Cep:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ imovel.quadraFace.endereco.cep.cep }"/></span><br />
				<label class="rotulo" id="rotuloEndereco" for="endereco" >Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ imovel.descricaoComplemento }"/></span><br />
				<label class="rotulo" id="rotuloCondominioSimNao" for="condominioSimNao" >Quadra:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ imovel.quadraFace.quadra.numeroQuadra }"/></span><br />
				<label class="rotulo" id="rotuloCondominioSimNao" for="condominioSimNao" >Face da Quadra:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ imovel.quadraFace.endereco.numero }"/></span><br />
			</div>			
		</fieldset></br></br>
		
		<fieldset id="conteinerClienteLevantamentoMercado" style="margin-bottom: 30px" class="conteinerDados">
			<div class="coluna detalhamentoColunaLarga">
				<label class="rotulo" id="rotuloDescricao" for="descricao" >Nome:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"> <c:out value="${ contato.nome }"/></span> <br />
				<label class="rotulo" id="rotuloEndereco" for="endereco" >CPF/CNPJ:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"> <c:out value="${ numeroDocumentoCliente }"/></span><br />
			</div>	
			<div class="colunaDir" style="float: right">
				<label class="rotulo" id="rotuloEndereco" for="endereco" >Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ enderecoCliente }"/></span><br />
				<label class="rotulo" id="rotuloEmail" for="email" >E-mail:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${ contato.email }"/></span><br />
			</div>			
		</fieldset>
		
		<fieldset  class="colunaEsq coluna detalhamentoColunaLarga" id="dadosGerais" >
			
			<label class="rotulo" id="rotuloAgente" for="agente" >Agente:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="agente" name="agente" ><c:out value="${ imovel.agente.funcionario.nome }"/></span><br />
			
			
			<label class="rotulo" id="rotuloSegmento" for="segmento" ><span class="campoObrigatorioSimbolo">* </span>Segmento:</label>
			<select name="segmento" id="segmento" class="campoSelect" style="margin-bottom: 10px;">
			<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${levantamentoMercado.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>
				</c:forEach>
			</select><br />
		
			
			<label class="rotulo" id="rotuloVigencia"  for="inicioVigencia" ><span class="campoObrigatorioSimbolo">* </span>Vig�ncia inicial:</label>
			<input tabindex="5" class="campoData campo2Linhas campoHorizontal" type="text" id="inicioVigencia" name="inicioVigencia"  style="margin-top: 8px" value="${ vigenciaInicio }"><br/>
			<label class="rotulo" id="rotuloVigencia"  for="finalVigencia" >Vig�ncia Final:</label>
			<input tabindex="6" class="campoData campo2Linhas campoHorizontal" type="text" id="finalVigencia" name="finalVigencia"  style="margin-top: 8px;" value="${ vigenciaFim }" ><br/><br class="quebraLinha"/>
		</fieldset>
		
		<fieldset style="float: right;"  id="dadosGeraisDir" style="margin-left: 50px;">
			
			<label class="rotulo"      id="rotuloGeradorEnergia" for="geradorEnergia" ><span class="campoObrigatorioSimbolo">* </span>Un. Consumidora:</label>					
			<input class="campoRadio"  type="radio" name="unidadesConsumidoras" onClick="habilitarCampoQuantidade();" id="unidadesConsumidoras"  value="true" <c:if test="${ levantamentoMercado.unidadesConsumidoras == true }">checked</c:if>>
			<label class="rotuloRadio" for="indicaUnidadesConsumidorasSIm">Sim</label>
			<input class="campoRadio"  style="margin-left: 128px;" type="radio" name="unidadesConsumidoras" onClick="desabilitarCampoQuantidade();" id="unidadesConsumidoras"  value="false" <c:if test="${ levantamentoMercado.unidadesConsumidoras == false }">checked</c:if>>
			<label class="rotuloRadio" for="unidadesConsumidorasNao">N�o</label>
			
			<label class="rotulo" id="rotuloQuantidade" for="quantidadeUnidadesConsumidoras" ><span id="asterisco" class="camposObrigatoriosLM">* </span>Quantidade:</label>
			<input class="campoTexto campoTextoGrande" id="quantidadeUnidadesConsumidoras" onkeypress="return formatarCampoDecimal(event,this,10 ,10);"  type="text" id="quantidadeUnidadesConsumidoras" name="quantidadeUnidadesConsumidoras" value="${ levantamentoMercado.quantidadeUnidadesConsumidoras }" /><br />	
		
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${ levantamentoMercado.habilitado eq true }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" style="margin-left: 120px" value="false" <c:if test="${ levantamentoMercado.habilitado eq false }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>		
		</fieldset>

		<div id="gridTipoCombustivel">
			<jsp:include page="/jsp/levantamentomercado/levantamentomercado/gridTipoCombustivelLevantamentoMercado.jsp"></jsp:include>	
		</div>
				
	</fieldset>	
	</form>
		
	<fieldset class="conteinerBotoes">
   		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
   		<input name="button" value="Salvar" class="bottonRightCol2 botaoGrande1" onclick="alterarLevantamentoMercado()" type="button">
	</fieldset>

		