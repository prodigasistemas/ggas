<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">

function detalharTipoCombustivel( indexLista ){
	var chavePrimariaLM = document.forms[0].chavePrimariaLM.value;
	var noCache = "noCache=" + new Date().getTime();

	var url = "detalharTipoCombustivel?indexLista=" + indexLista 
			+ "&chavePrimariaLM=" + chavePrimariaLM
			+ "&" + noCache;
	
	carregarFragmento('gridDetalhamentoTipoCombustivel', url);
}
	
</script>
<fieldset class="conteinerBloco">
	<fieldset style="margin-top: 50px;">
		<input type="hidden" id="idTipoCombustivel" name="idTipoCombustivel"/>
		<input type="hidden" id="chavePrimariaLM" name="chavePrimariaLM" value="${ levantamentoMercado.chavePrimaria }" />
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS" name="sessionScope.listaTipoCombustivelLM" sort="list" id="levantamentoMercadoTipoCombustivel" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.descricao" title="Combust�vel">
 				<c:choose>
					<c:when test="${ levantamentoMercadoTipoCombustivel.tipoCombustivel.chavePrimaria == -2 }">
			    		<a href="javascript:detalharTipoCombustivel('${ i }' );">
			            	<c:out value="N�o se Aplica"/>
	           			</a>
					</c:when>
					<c:otherwise>
						<a href="javascript:detalharTipoCombustivel('${ i }' );">
							<c:out value="${ levantamentoMercadoTipoCombustivel.tipoCombustivel.descricao }"/>
						</a>
					</c:otherwise>
				</c:choose>
			</display:column>
	        
	        <display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.consumo" title="Consumo" >
	            <c:choose>
					<c:when test="${ levantamentoMercadoTipoCombustivel.consumo != null }">
			    		<a href="javascript:detalharTipoCombustivel('${ i }' );">
		            	<fmt:formatNumber value="${levantamentoMercadoTipoCombustivel.consumo}" maxFractionDigits="2" minFractionDigits="2"/>
						</a>
					</c:when>
				</c:choose>
	            
			</display:column>
	        
	    	<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.preco" title="Pre�o" >
	             <c:choose>
					<c:when test="${ levantamentoMercadoTipoCombustivel.preco != null }">
			    		<a href="javascript:detalharTipoCombustivel('${ i }' );">
		            	<fmt:formatNumber value="${levantamentoMercadoTipoCombustivel.preco}" maxFractionDigits="2" minFractionDigits="2"/>
						</a>
					</c:when>
				</c:choose>
			</display:column>
		<c:set var="i" value="${i+1}" />
		</display:table>	
	</fieldset>
	
	<fieldset id="dadosDetalhamentoGlobal" style="margin-top: 20px">
		
		<fieldset id="detalhamentoTipoCombustivelLevantamentoMercado1" style="float:left">
		
			<label class="rotulo" id="rotuloTipoCombustivel" for="tipoCombustivel" >Tipo de Combust�vel:</label>
			<c:if test="${ tipoCombustivelLevantamentoMercado.tipoCombustivel.chavePrimaria == -2 }">
				<span class="itemDetalhamento" style="margin-bottom: -20px" id="tipoCombustivel" name="tipoCombustivel" ><c:out value="N�o se Aplica"/></span>
			</c:if>
			
			<c:if test="${ tipoCombustivelLevantamentoMercado.tipoCombustivel.chavePrimaria != -2 }">
				<span class="itemDetalhamento" style="margin-bottom: -20px" id="tipoCombustivel" name="tipoCombustivel" ><c:out value="${ tipoCombustivelLevantamentoMercado.tipoCombustivel.chavePrimaria }"/></span>
			</c:if>
			
			<label class="rotulo" id="rotuloCOnsumo" for="consumo" >Consumo:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="Consumo" name="Consumo" ><c:out value="${ consumo }"/></span>
						
			<label class="rotulo" id="rotuloPreco" for="preco" >Pre�o:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="preco" name="preco" ><c:out value="${ preco }"/></span>
			
			<label class="rotulo" id="rotuloFornecedor" for="fornecedor" >Fornecedor:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="fornecedor" name="fornecedor" ><c:out value="${ tipoCombustivelLevantamentoMercado.fornecedor.descricao }"/></span>
			
			<label class="rotulo" id="rotuloPeriodicidade" for="periodicidade" >Periodicidade:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="periodicidade" name="periodicidade" ><c:out value="${ tipoCombustivelLevantamentoMercado.periodicidade.descricao }"/></span>
			
			<label class="rotulo" id="rotuloModalidadeUso" for="modalidadeUso" >Modalidade de Uso:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="modalidadeUso" name="modalidadeUso" ><c:out value="${ tipoCombustivelLevantamentoMercado.modalidadeUso.descricao }"/></span>
			
			<label class="rotulo" id="rotuloRedeEstado" for="redeEstado" >Estada da Rede:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="redeEstado" name="redeEstado" ><c:out value="${ tipoCombustivelLevantamentoMercado.redeEstado.descricao }"/></span>
			
		</fieldset>
		
		<fieldset id="detalhamentoTipoCombustivelLevantamentoMercado2" >
			
			<label class="rotulo" id="rotuloCentral" for="central" >Central:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="central" name="central" ><c:out value="${ central }"/></span>
			
			<label class="rotulo" id="rotuloLocalizacaoCentral" for="localizacaoCentral" >Localiza��o Central:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="localizacaoCentral" name="localizacaoCentral" ><c:out value="${ tipoCombustivelLevantamentoMercado.localizacaoCentral }"/></span>
						
			<label class="rotulo" id="rotuloLocalizacao" for="localizacaoAbrigo" >Localiza��o Abrigo:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="localizacaoAbrigo" name="localizacaoAbrigo" ><c:out value="${ tipoCombustivelLevantamentoMercado.localizacaoAbrigo }"/></span>
			
			<label class="rotulo" id="rotuloCilindro" for="cilindro" >Cilindro:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="cilindro" name="cilindro" ><c:out value="${ tipoCombustivelLevantamentoMercado.tipoBotijao.descricao }"/></span>
			
			<label class="rotulo" id="rotuloRedeMaterial" for="redeMaterial" >Material da Rede:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="redeMaterial" name="redeMaterial" ><c:out value="${ tipoCombustivelLevantamentoMercado.redeMaterial.descricao }"/></span>
			
			<label class="rotulo" id="rotuloRedeTempo" for="redeTempo" >Rede Tempo:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="redeTempo" name="redeTempo" ><c:out value="${ tipoCombustivelLevantamentoMercado.redeTempo }"/></span>
			
			<label class="rotulo" id="rotuloVentilacao" for="ventilacao" >Ventila��o:</label>
			<span class="itemDetalhamento" style="margin-bottom: -20px" id="ventilacao" name="ventilacao" ><c:out value="${ ventilacao }"/></span>
			
		</fieldset>
	
	</fieldset>
	
	<fieldset id="containerTipoAparelhoLevantamentoMercado" style="margin-bottom: 30px">
		
        <fieldset id="divAparelho" style="width: 300px; margin-top: 45px;" >
			<legend>Aparelhos</legend>
			<display:table class="dataTableGGAS" style="width: 300px;" name="listaTipoAparelho" sort="list" id="tipoAparelho" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

				<display:column  sortable="false" sortProperty="tipoAparelho.tipoAparelho.descricao" title="Descri��o" >
					<c:out value="${ tipoAparelho.tipoAparelho.descricao }"/>
				</display:column>

			</display:table>
   		</fieldset>
        
        <fieldset id="containerObservacoes" style="margin-left: 600px; margin-top: -80px; display: block">
			<label class="rotulo" id="rotuloObservacoes" for="observacoes" >Observa��es:</label>
			<span class="itemDetalhamento" id="observacoes" name="observacoes" ><c:out value="${ tipoCombustivelLevantamentoMercado.observacoes }"/></span>	
        </fieldset>
	</fieldset>
</fieldset>