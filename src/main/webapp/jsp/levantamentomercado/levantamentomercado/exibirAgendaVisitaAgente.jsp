<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Agendar Visita<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script type="text/javascript">

$(document).ready(function() {
	
	// Datepicker
	$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', minDate: '+0d'});

	//Validar e checar intervalos de datas para liberar o bot�o Gerar.
	$("#dataInicial,#dataFinal").bind('keypress blur focusin', function() {
		validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");
	});		
	validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");

			
	$("#horaVisita").inputmask("99:99");
		
});/*-- FIM: Bloco JQuery -- */


function cancelar(){
	location.href = '<c:url value="/exibirPesquisaLevantamentoMercado"/>';
}

function limparFormulario(){
	document.getElementById('horaVisita').value = "";
	document.getElementById('dataVisitaAgente').value = "";
	document.getElementById('observacoes').value = "";
}

function incluirAgenda(){
	var fluxoAlteracao = document.forms["lmForm"].fluxoAgendaAtualizacao.value;
	if(fluxoAlteracao == "false"){
		submeter('lmForm', 'incluirAgenda');
	}
	
	if(fluxoAlteracao == "true"){
		submeter('lmForm', 'atualizarAgendamento');
	}
}


</script>

<form method="post" action="incluirAgenda" id="lmForm" name="lmForm" modelAttribute="AgendaVisitaAgente">
<%-- 	<input type="hidden" id="chaveLM" name="chaveLM" value="${ levantamentoMercado.chavePrimaria }" > --%>
	<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${ levantamentoMercado.chavePrimaria }" >
	<input type="hidden" id="fluxoAgendaAtualizacao" name="fluxoAgendaAtualizacao" value="${ fluxoAgendaAtualizacao }" >
	<input type="hidden" id="chaveAgendamento" name="chaveAgendamento" value="${ agenda.chavePrimaria }" >
	
	<c:if test="${ fluxoAgendaAtualizacao eq false }">
		<fieldset class="conteinerPesquisarIncluirAlterar">
			<fieldset id="materialCol1" class="">
				<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" ><span class="campoObrigatorioSimbolo">* </span>Nome Agente:</label>
				<input class="campoTexto" type="text" id="nomeAgente" name="nomeAgente" style="width: 220px" value="${ levantamentoMercado.imovel.agente.funcionario.nome }" disabled="disabled">
				
				<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" ><span class="campoObrigatorioSimbolo">* </span>Data Visita:</label>
				<input tabindex="5" class="campoData campo2Linhas campoHorizontal" style="width: 80px" type="text" id="dataVisitaAgente" name="dataVisitaAgente"  style="margin-top: 8px" value="${ AgendaVisitaAgente.dataVisita }">
				
				<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" ><span class="campoObrigatorioSimbolo">* </span>Hora Visita:</label>
				<input class="campoTexto" maxlength="5" type="text" id="horaVisita" name="horaVisita" value="${ AgendaVisitaAgente.horaVisita }" style="width: 40px" >
			
				<label class="rotulo" for="observacoes" >Observa��es:</label>
		    	<textarea  id="observacoes" maxlength="1000"  name="observacoes"  class="campoTexto campoTextoGrande" rows="3" cols="33" > ${ AgendaVisitaAgente.observacoes } </textarea>	
			
			</fieldset>
		</fieldset>
	</c:if>
	
	<c:if test="${ fluxoAgendaAtualizacao eq true }">
		<fieldset class="conteinerPesquisarIncluirAlterar">
			<fieldset id="materialCol1" class="">
				<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" ><span class="campoObrigatorioSimbolo">* </span>Nome Agente:</label>
				<input class="campoTexto" type="text" id="nomeAgente" style="width: 220px" name="nomeAgente" value="${ levantamentoMercado.imovel.agente.funcionario.nome }" disabled="disabled">
				
				<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" ><span class="campoObrigatorioSimbolo">* </span>Data Visita:</label>
				<input tabindex="5" class="campoData campoHorizontal" style="width: 80px" type="text" id="dataVisitaAgente" name="dataVisitaAgente"  style="margin-top: 8px" value="${ dataVisitaAgente }">
				
				<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" ><span class="campoObrigatorioSimbolo">* </span>Hora Visita:</label>
				<input class="campoTexto" maxlength="5" type="text" id="horaVisita" name="horaVisita" value="${ horaVisita }" style="width: 40px" >
			
				<label class="rotulo" for="observacoes" >Observa��es:</label>
		    	<textarea  id="observacoes" maxlength="1000"  name="observacoes"  class="campoTexto campoTextoGrande" rows="3" cols="33" > ${ agenda.observacoes } </textarea>	
			
			</fieldset>
		</fieldset>
	</c:if>

	<fieldset class="conteinerBotoes">
	    <input name="button" value="Cancelar" class="bottonRightCol2 botaoGrande1" style="float: left; margin-right: 5px" onclick="cancelar()" type="button">
	    <input name="button" value="Limpar" class="bottonRightCol2 botaoGrande1"  style="float: left" onclick="limparFormulario()" type="button">
	    <input name="button" value="Salvar" class="bottonRightCol2 botaoGrande1" onclick="incluirAgenda()" type="button">
 	</fieldset>
</form>