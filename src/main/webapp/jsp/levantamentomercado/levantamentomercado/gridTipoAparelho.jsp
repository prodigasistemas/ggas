<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
$(document).ready(function(){
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	botaoAlterarMaterial.disabled = true;
});

function adicionarTipoAparelho() {
	var idAparelho = document.forms[0].idAparelho.value;
	var indexList = -1;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "adicionarTipoAparelho?idAparelho="+idAparelho
			+"&indexList="+indexList
			+"&"+noCache;
	
	if(idAparelho > 0){
		carregarFragmento('gridTipoAparelho',url);
	}else{
		alert('Selecione um Tipo de Aparelho!');
	}
}

function limparAparelhos() {
	var idAparelho = document.forms[0].idAparelho.value;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "limparGridAparelhos?idAparelho="+idAparelho
			+"&"+noCache;
	
	carregarFragmento('gridTipoAparelho',url);
}

function exibirAlteracaoAparelho(indice,idAparelho) {
	document.forms[0].indexList.value = indice;
	document.forms[0].idAparelho.value = idAparelho;
}

function alterarMaterial() {
	var chavePrimaria = document.forms[0].chavePrimaria.value;
	var chaveMaterial = document.forms[0].material.value;
	var quantidadeMaterial = document.forms[0].quantidadeMaterial.value;
	var indexList = document.forms[0].indexList.value;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "incluirMaterialServicoTipo?tipoCombustivel="+chaveMaterial
			+"&quantidadeMaterial="+quantidadeMaterial
			+"&chavePrimaria="+chavePrimaria
			+"&indexList="+indexList
			+"&"+noCache;
	
	if(chaveMaterial!="-1" && chaveMaterial != "" && quantidadeMaterial > 0) {
		carregarFragmento('gridMateriais',url);
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	botaoAlterarMaterial.disabled = true;
	} else if(chaveMaterial == "-1" || quantidadeMaterial == ""){
		alert('Os campos Descri��o do Material e Quantidade s�o de preenchimento obrigat�rio');
	} else if(quantidadeMaterial == 0){
		alert('O campo Quantidade deve ser maior que zero.');
	}
}

function limparMaterial() {
	document.forms[0].material.value = -1;
	document.forms[0].quantidadeMaterial.value = "";
}

function exibirAlteracaoMaterialServicoTipo(indice,idMaterial,quantidade) {
	document.forms[0].indexList.value = indice;
	document.forms[0].material.value = idMaterial;
	document.forms[0].quantidadeMaterial.value = quantidade;
	
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	var botaoLimparMaterial = document.getElementById("botaoLimparMaterial");	
	var botaoIncluirMaterial = document.getElementById("botaoIncluirMaterial");	
	botaoIncluirMaterial.disabled = true;
	botaoLimparMaterial.disabled = false;
	botaoAlterarMaterial.disabled = false;
}

function removerTipoAparelho(chavePrimaria) {
	var noCache = "noCache=" + new Date().getTime();
	$("#gridTipoAparelho").load("removerAparelho?chavePrimaria="+chavePrimaria+"&"+noCache);
}

</script>
				
<fieldset id="divServico" class="colunaEsq" >
	<div class="pesquisarClienteFundo" style="width: 450px;">
		<legend>Aparelhos</legend>
        <div style="margin: 5px">
        	<label class="rotulo" id="rotuloAparelho" for="aparelho" ><span class="campoObrigatorioSimbolo">* </span>Aparelho:</label>
			<select name="idAparelho" id="idAparelho" class="campoSelect" >
			<option value="-1">Selecione</option>
				<c:forEach items="${listaAparelho}" var="aparelho">
				<option value="<c:out value="${aparelho.chavePrimaria}"/>" >
				<c:out value="${aparelho.descricao}"/>
			</option>
			</c:forEach>
			</select>
	              
			<input name="buttonAdicionar" style="margin-top: 5px; margin-left: 2px" id="buttonAdicionar" type="button" class="botton" value="Adicionar" onclick="adicionarTipoAparelho();">
		
        </div>
           
		<div id="gridComponentesServicoTipo" style="height: 40px;" class="conteinerChamado">
			<display:table class="dataTableGGAS" name="sessionScope.listaTipoAparelho" sort="list" id="tipoAparelho" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

				<display:column  sortable="false" sortProperty="tipoAparelho.descricao" title="Descri��o" >
					<c:out value="${ tipoAparelho.descricao }"/>
				</display:column>

				<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
					<a onclick="return confirm('Deseja excluir o Aparelho?');" href="javascript:removerTipoAparelho('${tipoAparelho.chavePrimaria}');">
						<img title="Excluir Aparelho" alt="Excluir Aparelho"  src="<c:url value="/imagens/deletar_x.png"/>">
					</a> 
				</display:column>

			</display:table>	
   		</div>
 	</div>
</fieldset>
