<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

function alterar(){
	submeter('agenteForm','alterarAgente');
}

function cancelar() {
	location.href = '<c:url value="/exibirPesquisaAgente"/>';
}

function limparFormulario(){
	document.getElementById('idFuncionario').value = "";
	document.forms['agenteForm'].habilitado[0].checked = true;
	document.forms['agenteForm'].nomeCompletoFuncionario.value = "";
	document.forms['agenteForm'].empresaFuncionario.value = "";
	document.forms['agenteForm'].matriculaFuncionario.value = "";
	document.forms['agenteForm'].nomeFuncionarioTexto.value = "";
	document.forms['agenteForm'].matriculaFuncionarioTexto.value = "";
	document.forms['agenteForm'].empresaFuncionarioTexto.value = "";
}

function alertInativacao() {
	var habilitado = document.getElementById("habilitado").checked;
	var habilitadoOriginal = document.getElementById("habilitadoOriginal").value;
	
	if(habilitado == false && habilitadoOriginal == "true"){
		if(confirm('Deseja Inativar o Agente?')){
			return true;  
		}else{
			return false;
		} 	
	}	  
}

function removerEspacoInicio(elem){

	return elem.replace(/^\s{1}/,"");

}

function selecionarFuncionario(idSelecionado){
	var idFuncionario = document.getElementById("idFuncionario");
	var nomeCompletoFuncionario = document.getElementById("nomeCompletoFuncionario");
	var empresaFuncionario = document.getElementById("empresaFuncionario");
	var matriculaFuncionario = document.getElementById("matriculaFuncionario");
	
	if(idSelecionado != '') {				
		AjaxService.obterFuncionarioPorChave( idSelecionado, {
           	callback: function(funcionario) {	           		
           		if(funcionario != null){  	           			        		      		         		
	               	idFuncionario.value = funcionario["chavePrimaria"];
	               	matriculaFuncionario.value = funcionario["matricula"];
	               	nomeCompletoFuncionario.value = funcionario["nome"];
	               	empresaFuncionario.value = funcionario["nomeEmpresa"];
               	}
        	}, async:false}
        	
        );	        
    } else {
   		idFuncionario.value = "";
    	nomeCompletoFuncionario.value = "";
    	matriculaFuncionario.value = "";
    	empresaFuncionario.value = "";
   	}
    
    document.getElementById("nomeFuncionarioTexto").value = nomeCompletoFuncionario.value;
    document.getElementById("matriculaFuncionarioTexto").value = matriculaFuncionario.value;
    document.getElementById("empresaFuncionarioTexto").value = empresaFuncionario.value;
}

function exibirPopupPesquisaLeiturista() {
	popup = window.open('exibirPesquisaFuncionarioPopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}
</script>


<h1 class="tituloInterno">Alterar Agente<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form method="post" action="alterarAgente" id="agenteForm" name="agenteForm">

	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${ agente.chavePrimaria }">
	<input type="hidden" name="versao" id="versao" value="${ agente.versao }">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="materialCol1" class="coluna">
			<legend><span class="campoObrigatorioSimboloTabs">* </span>Pesquisar Funcion�rio</legend>
			<div class="conteinerDados coluna detalhamentoColunaLarga">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Funcion�rio</span> para selecionar um Funcion�rio.</p>
				<input name="idFuncionario" type="hidden" id="idFuncionario" value="${ agente.funcionario.chavePrimaria }">
				<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${ agente.chavePrimaria }">
				<input name="nomeCompletoFuncionario" type="hidden" id="nomeCompletoFuncionario" value="${leituristaForm.map.nomeCompletoFuncionario}">
				<input name="empresaFuncionario" type="hidden" id="empresaFuncionario" value="${leituristaForm.map.empresaFuncionario}">
				<input name="matriculaFuncionario" type="hidden" id="matriculaFuncionario" value="${leituristaForm.map.matriculaFuncionario}">
				<input name="Button" id="botaoPesquisarFuncionario" style="margin-left: 72px; margin-bottom: 10px;" class="bottonRightCol" title="Pesquisar Funcionario"  value="Pesquisar Funcionario" onclick="exibirPopupPesquisaLeiturista();" type="button"><br >
					
				<label class="rotulo" style="width: 70px;" id="rotuloCliente" for="nomeFuncionarioTexto">Funcion�rio:</label>
				<input class="campoDesabilitado" type="text" id="nomeFuncionarioTexto" name="nomeFuncionarioTexto"  maxlength="50" size="48" disabled="disabled" value="${ agente.funcionario.nome }"><br />
				<label class="rotulo" style="width: 70px;" id="rotuloCnpjTexto" for="matriculaFuncionarioTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaFuncionarioTexto" name="matriculaFuncionarioTexto"  maxlength="18" size="18" disabled="disabled" value="${ agente.funcionario.matricula }"><br />
				<label class="rotulo" style="width: 70px;" id="rotuloEmailClienteTexto" for="empresaFuncionarioTexto">Empresa:</label>
				<input class="campoDesabilitado" type="text" id="empresaFuncionarioTexto" name="empresaFuncionarioTexto"  maxlength="80" size="40" disabled="disabled" value="${ agente.funcionario.empresa.cliente.nome }"><br />	
			</div>
		</fieldset>		
		
		<fieldset id="localidadeCol2" class="materialCol2">	
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${ agente.habilitado eq true }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${ agente.habilitado eq false }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
		</fieldset>
	</fieldset>
	
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
	    <vacess:vacess param="alterarMaterial">
	    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit" onclick="alterar()">
	    </vacess:vacess>
 	</fieldset>
</form>
