<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Detalhar Agente<a href="<help:help>/cadastrodoclientedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script type="text/javascript">

function voltar(){
	submeter('agenteForm','pesquisarAgente');
}

function alterar(){
	submeter('agenteForm','exibirAlteracaoAgente');
}

</script>

<form method="post" action="alterarAgente" id="agenteForm">

<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${ agente.chavePrimaria }">
<input type="hidden" name="habilitado" id="habilitado" value="${ agente.habilitado }">

<fieldset class="detalhamento">
	<fieldset id="detalhamentoGeral" class="conteinerBloco">
		<input name="idFuncionario" type="hidden" id="idFuncionario" value="${ agente.funcionario.chavePrimaria }">
		<fieldset id="detalhamentoMaterial" class="colunaEsq">
			<legend><span class="campoObrigatorioSimboloTabs">* </span>Dados do Agente:</legend><br/>
			<label class="rotulo" id="rotuloMatr�cula">C�digo Agente:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${agente.chavePrimaria}"/></span><br />
			<label class="rotulo" id="rotuloNome">Matr�cula:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${agente.funcionario.matricula}"/></span><br />
			<label class="rotulo" id="rotuloNome">Funcion�rio:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${agente.funcionario.nome}"/></span><br />
			<label class="rotulo" id="rotuloNome">Empresa:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${agente.funcionario.empresa.cliente.nome}"/></span><br />
			
			<label class="rotulo" id="rotuloIndicador">Indicador de Uso:</label>
			<span class="itemDetalhamento" id="empresaServicoPrestado"> 
				<c:choose>
					<c:when test="${agente.habilitado == 'true'}">Ativo</c:when>
					<c:otherwise>Inativo</c:otherwise>
				</c:choose>
			</span>
		</fieldset>
		
	</fieldset>
		
</fieldset>	
	
</form>
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
		<vacess:vacess param="exibirAlteracaoMaterial">    
    		<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>
</fieldset>
