<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Pesquisar Agente<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">

function pesquisar() {
	submeter('agenteForm','pesquisarAgente');
}

function alterarAgente() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms["agenteForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('agenteForm','exibirAlteracaoAgente');
    }
	
}

function detalharAgente(chave){
	document.forms["agenteForm"].chavePrimaria.value = chave;
	submeter('agenteForm','exibirDetalhamentoAgente');
}

function limparFormulario(){
	document.getElementById('idFuncionario').value = "";
	document.forms['agenteForm'].habilitado[0].checked = true;
	document.forms['agenteForm'].nomeCompletoFuncionario.value = "";
	document.forms['agenteForm'].empresaFuncionario.value = "";
	document.forms['agenteForm'].matriculaFuncionario.value = "";
	document.forms['agenteForm'].nomeFuncionarioTexto.value = "";
	document.forms['agenteForm'].matriculaFuncionarioTexto.value = "";
	document.forms['agenteForm'].empresaFuncionarioTexto.value = "";
}

function incluir() {
	location.href = '<c:url value="/exibirInclusaoAgente"/>';
}

function removerAgente(){
	
	var selecao = verificarSelecao();	
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('agenteForm', 'removerAgente');
		}
    }
}

function selecionarFuncionario(idSelecionado){
	var idFuncionario = document.getElementById("idFuncionario");
	var nomeCompletoFuncionario = document.getElementById("nomeCompletoFuncionario");
	var empresaFuncionario = document.getElementById("empresaFuncionario");
	var matriculaFuncionario = document.getElementById("matriculaFuncionario");
	
	if(idSelecionado != '') {				
		AjaxService.obterFuncionarioPorChave( idSelecionado, {
           	callback: function(funcionario) {	           		
           		if(funcionario != null){  	           			        		      		         		
	               	idFuncionario.value = funcionario["chavePrimaria"];
	               	matriculaFuncionario.value = funcionario["matricula"];
	               	nomeCompletoFuncionario.value = funcionario["nome"];
	               	empresaFuncionario.value = funcionario["nomeEmpresa"];
               	}
        	}, async:false}
        	
        );	        
    } else {
   		idFuncionario.value = "";
    	nomeCompletoFuncionario.value = "";
    	matriculaFuncionario.value = "";
    	empresaFuncionario.value = "";
   	}
    
    document.getElementById("nomeFuncionarioTexto").value = nomeCompletoFuncionario.value;
    document.getElementById("matriculaFuncionarioTexto").value = matriculaFuncionario.value;
    document.getElementById("empresaFuncionarioTexto").value = empresaFuncionario.value;
}

function exibirPopupPesquisaLeiturista() {
	popup = window.open('exibirPesquisaFuncionarioPopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

</script>

<form action="pesquisarAgente" id="agenteForm" name="agenteForm" method="post" modelAttribute="agente">
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" > 	
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">	
		<fieldset id="materialCol1" class="coluna">
			<legend>Pesquisar Funcion�rio</legend>
				<div class="conteinerDados coluna2 detalhamentoColunaLarga" style="padding-right: 0px;">
					<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Funcion�rio</span> para selecionar um Funcion�rio.</p>
					<input name="idFuncionario" type="hidden" id="idFuncionario" value="${ agente.funcionario.chavePrimaria }">
					<input name="nomeCompletoFuncionario" type="hidden" id="nomeCompletoFuncionario" >
					<input name="empresaFuncionario" type="hidden" id="empresaFuncionario" >
					<input name="matriculaFuncionario" type="hidden" id="matriculaFuncionario" >
					<input name="Button" id="botaoPesquisarFuncionario" style="margin-left: 72px; margin-bottom: 10px;" class="bottonRightCol" title="Pesquisar Funcionario"  value="Pesquisar Funcionario" onclick="exibirPopupPesquisaLeiturista();" type="button"><br >
					
					<label class="rotulo" style="width: 70px;" id="rotuloCliente" for="nomeFuncionarioTexto">Funcion�rio:</label>
					<input class="campoDesabilitado" type="text" id="nomeFuncionarioTexto" name="nomeFuncionarioTexto"  maxlength="50" size="48" disabled="disabled" value="${ agente.funcionario.nome }"><br />
					<label class="rotulo" style="width: 70px;" id="rotuloCnpjTexto" for="matriculaFuncionarioTexto">Matr�cula:</label>
					<input class="campoDesabilitado" type="text" id="matriculaFuncionarioTexto" name="matriculaFuncionarioTexto"  maxlength="18" size="18" disabled="disabled" value="${ agente.funcionario.matricula }"><br />
					<label class="rotulo" style="width: 70px;" id="rotuloEmailClienteTexto" for="empresaFuncionarioTexto">Empresa:</label>
					<input class="campoDesabilitado" type="text" id="empresaFuncionarioTexto" name="empresaFuncionarioTexto"  maxlength="80" size="40" disabled="disabled" value="${ agente.funcionario.empresa.cliente.nome }"><br />	
				</div>
		</fieldset>
		
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="colunaFinalMaterial">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq true }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq false }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="todos" <c:if test="${habilitado ne false and habilitado ne true}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</br>
		</fieldset>	
		<fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" onclick="pesquisar();">
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>	
	
	<c:if test="${listaAgente ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaAgente" sort="list" id="agente" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column style="width: 20px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="${agente.chavePrimaria}">
	        </display:column>
	        
	        <display:column style="width: 30px" title="Ativo">
				<c:choose>
					<c:when test="${agente.habilitado == true}">
						<img alt="Ativo" title="Ativo"
							src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo"
							src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        
	    	<display:column  sortable="true" sortProperty="agente.funcionario.matricula" title="Matr�cula" style="width: 30px">
				<a href="javascript:detalharAgente(<c:out value='${agente.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${agente.funcionario.matricula}"/>
	            </a>
			</display:column>

			<display:column sortable="true" sortProperty="nome" title="Nome">
				<a href="javascript:detalharAgente(<c:out value='${agente.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${agente.funcionario.nome}"/>
	            </a>
			</display:column>
					
	    </display:table>	
	</c:if>
	
	
		
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaAgente}">
  			<vacess:vacess param="exibirAlteracaoMaterial">
  				<input value="Alterar" class="bottonRightCol2" onclick="alterarAgente()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerAgente">
				<input value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerAgente()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoAgente">
   			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>
</form>

	