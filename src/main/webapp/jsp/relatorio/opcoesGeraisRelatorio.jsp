<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script>
		
	$(document).ready(function(){		
		ativarTipoGrafico();
		$("#exibirGraficoCheck").click(function(){
			ativarTipoGrafico();
		});

		$("#botaoOK").click(function(){
			realizarAcoesPreSubmit();
			$("#exportarRelatorio").dialog("close");
		});
	});
	
	function ativarTipoGrafico(){
		if($("#exibirGraficoCheck").attr("checked")){
			$("#idTipoGraficoSelect").removeAttr("disabled");
			$("label[for=idTipoGraficoSelect]").removeClass("rotuloDesabilitado");
		}else{
			$("#idTipoGraficoSelect").attr("disabled","disabled");
			$("label[for=idTipoGraficoSelect]").addClass("rotuloDesabilitado");
		}
	}

	function realizarAcoesPreSubmit() {
		if($("#exibirFiltrosCheck").is(":checked")){
			$("#exibirFiltros").val("true");
		} else {
			$("#exibirFiltros").val("false");
		}

		<c:if test="${param.apenasXLS == null || !param.apenasXLS}">
			$(":radio[name=formatoImpressaoRadio]").each(function(){
				var radioFormatoImpressao = $(this).val();
				if($(this).is(":checked")){$("#formatoImpressao").val(radioFormatoImpressao);}
			});
		</c:if>

		<c:if test="${param.exibeAnaliticoSintetico}">
			$(":radio[name=tipoExibicaoRadio]").each(function(){
				var radioTipoExibicao = $(this).val();
				if($(this).is(":checked")){$("#tipoExibicao").val(radioTipoExibicao);}
			});
		</c:if>

		<c:if test="${param.exibeGrafico}">
			var checkExibirGraficos = $("#exibirGraficoCheck").val();
			if($("#exibirGraficoCheck").is(":checked")){$("#exibirGrafico").val(checkExibirGraficos);}

			var selectIdTipoGrafico = $("idTipoGraficoSelect").val();
			$("idTipoGrafico").val(selectIdTipoGrafico);
		</c:if>
		
		<c:if test="${param.exibeOpcoesTarifa}">
			var tarifasVigencia = document.getElementById('idVigenciasTarifa');
		    var idVigenciaTarifaSelecionada = tarifasVigencia.options[tarifasVigencia.selectedIndex].value;
			$("#chavePrimariaVigencia").val(idVigenciaTarifaSelecionada);
	
			var descontosVigencia = document.getElementById('idDescontosVigencia');
		    var idDescontoVigenciaSelecionado = descontosVigencia.options[descontosVigencia.selectedIndex].value;
			$("#chavePrimariaVigenciaDesconto").val(idDescontoVigenciaSelecionado);
		</c:if>
		
		gerar();
	}
	
	function carregarVigencias(codTarifa){
		var tarifasVigencia = document.getElementById("idVigenciasTarifa");
		tarifasVigencia.length = 0;
		AjaxService.listarTarifaVigenciaPorTarifa(codTarifa, 
	     	function(dados) {
				for (key in dados) {
					var item = dados[key];
					var novaOpcao = new Option(item[1], item[0]);
					tarifasVigencia.options[tarifasVigencia.length] = novaOpcao;
				}
				carregarVigenciaDescontos();
	        }
		);
	}
	
	function carregarVigenciaDescontos(){
		var selTarifaVigenciaDesconto = document.getElementById('idDescontosVigencia');
		selTarifaVigenciaDesconto.length = 0;

		var novaOpcao = new Option("Selecione","-1");
		selTarifaVigenciaDesconto.options[selTarifaVigenciaDesconto.length] = novaOpcao;

	    var tarifasVigencia = document.getElementById('idVigenciasTarifa');
	    var idVigenciaTarifaSelecionada = tarifasVigencia.options[tarifasVigencia.selectedIndex].value;
		
	 	if (idVigenciaTarifaSelecionada != "-1") {
			selTarifaVigenciaDesconto.disabled = false;
			AjaxService.obterTarifaVigenciaDescontoPorTarifaVigenciaOrdenada(idVigenciaTarifaSelecionada, {
            	callback:function(listaTarifaVigenciaDesconto) {
	             	for (key in listaTarifaVigenciaDesconto){
						var novaOpcao = new Option(listaTarifaVigenciaDesconto[key][1], listaTarifaVigenciaDesconto[key][0]);
						selTarifaVigenciaDesconto.options[selTarifaVigenciaDesconto.length] = novaOpcao;
	                 }
	             }, async:false}
	         );
		}
	}
	
</script>


<input name="<c:out value='${param.exibeAnaliticoSintetico}' default=""/>" type="hidden" id="<c:out value='${param.exibeAnaliticoSintetico}' default=""/>" value="${param.exibeAnaliticoSintetico}">
<input name="<c:out value='${param.exibeGrafico}' default=""/>" type="hidden" id="<c:out value='${param.exibeGrafico}' default=""/>" value="${param.exibeGrafico}">
<input name="exibirFiltros" type="hidden" id="exibirFiltros" value="${param.exibeGrafico}">
<input name="formatoImpressao" type="hidden" id="formatoImpressao" value="${relatorioForm.map.formatoImpressao}">
<input name="tipoExibicao" type="hidden" id="tipoExibicao" value="${relatorioForm.map.tipoExibicao}">
<input name="exibirGrafico" type="hidden" id="exibirGrafico" value="${relatorioForm.map.exibirGrafico}">
<input name="idTipoGrafico" type="hidden" id="idTipoGrafico" value="${relatorioForm.map.exibirGrafico}">
<input name="chavePrimariaVigencia" type="hidden" id="chavePrimariaVigencia">
<input name="chavePrimariaVigenciaDesconto" type="hidden" id="chavePrimariaVigenciaDesconto">

<div id="exportarRelatorio" title="Op��es de Exporta��o" style="display: none">
	
	<c:if test="${param.exibeAnaliticoSintetico}">
		<label class="rotulo" for="tipoExibicaoRadio">Tipo de Exibi��o:</label>
					
		<input class="campoRadio" type="radio" name="tipoExibicaoRadio" id="analitico" value="analitico" checked />
		<label class="rotuloRadio" for="analitico">Anal�tico</label>
		
		<input class="campoRadio" type="radio" name="tipoExibicaoRadio" id="sintetico" value="sintetico" 
			<c:if test="${relatorioForm.map.tipoExibicao eq 'sintetico'}">checked="checked"</c:if>>
		<label class="rotuloRadio" for="sintetico" style="margin-right: 0; padding-right: 0">Sint�tico</label>
		<br class="quebraLinha2" />
	</c:if>
	
	<c:if test="${!param.ocultarCheckFiltros}">
		<label class="rotulo" for="exibirFiltrosCheck">Exibir filtros no relat�rio:</label>
		<input type="checkbox" class="campoCheckbox" name="exibirFiltrosCheck" id="exibirFiltrosCheck" 
			<c:if test="${relatorioForm.map.exibirFiltros}">checked="checked"</c:if>>
		<br class="quebraLinha2" />
	</c:if>
	
	<c:if test="${param.exibeGrafico}">
		<label class="rotulo" for="exibirGraficoCheck">Exibir gr�fico:</label>
		<input type="checkbox" class="campoCheckbox" name="exibirGraficoCheck" id="exibirGraficoCheck" value="${relatorioForm.map.exibirGrafico}" 
			<c:if test="${relatorioForm.map.exibirGrafico}">checked="checked"</c:if> />
		<br class="quebraLinha2" />
		
		<c:if test="${param.exibeTipoGrafico}">
			<label class="rotulo" for="idTipoGraficoSelect">Tipo de Gr�fico:</label>
			<select name="idTipoGraficoSelect" id="idTipoGraficoSelect" class="campoSelect">
		    	<c:forEach items="${listaTipoGrafico}" var="tipoGrafico">
					<option value="<c:out value="${tipoGrafico}"/>" <c:if test="${relatorioForm.map.idTipoGrafico == tipoGrafico}">selected="selected"</c:if>>
						<c:out value="${tipoGrafico}"/>
					</option>		
			    </c:forEach>
		    </select>
			<br class="quebraLinha2" />
		</c:if>
	</c:if>
	
	<c:if test="${param.exibeOpcoesTarifa}">
		<label class="rotulo campoObrigatorio" for="idVigenciasTarifa"><span class="campoObrigatorioSimbolo">* </span>Vig�ncia(s):</label> 
	    <select name="idVigenciasTarifa" id="idVigenciasTarifa" tabindex="18" class="campoSelect campoHorizontal">
	    </select>
	    
	    <label class="rotulo" for="idDescontosVigencia" style="margin-bottom: 0px;">Descontos:</label>
		<select name="idDescontosVigencia" id="idDescontosVigencia" class="campoSelect campoHorizontal">
	   	</select>
	</c:if>
	
	<c:if test="${param.apenasXLS == null || !param.apenasXLS}">
	
		<input id="tipoExportacaoPDF" class="campoRadio" style="margin-top: 10px; margin-left: 5px" type="radio" name="formatoImpressaoRadio" id="pdf" value="${relatorioUtil.formatoImpressaoPDF}" 
			<c:if test="${relatorioForm.map.formatoImpressao eq relatorioUtil.formatoImpressaoPDF || relatorioForm.map.formatoImpressao eq null}">checked="checked"</c:if> />
		<label class="rotuloRadio" for="tipoExportacaoPDF"><img style="vertical-align: middle" src='<c:url value="/imagens/icone_pdf_peq.png"/>' />PDF</label>	
					
		<input id="tipoExportacaoRTF" class="campoRadio" style="margin-top: 10px" type="radio" name="formatoImpressaoRadio" id="tipoExportacaoRTF" value="${relatorioUtil.formatoImpressaoRTF}" 
			<c:if test="${relatorioForm.map.formatoImpressao eq relatorioUtil.formatoImpressaoRTF}">checked="checked"</c:if> />
		<label class="rotuloRadio" for="tipoExportacaoRTF"><img style="vertical-align: middle" src='<c:url value="/imagens/icone_word_peq.png"/>' />Word</label>	
		
		<input id="tipoExportacaoXLS" class="campoRadio" style="margin-top: 10px" type="radio" name="formatoImpressaoRadio" id="tipoExportacaoXLS" value="${relatorioUtil.formatoImpressaoXLS}" 
			<c:if test="${relatorioForm.map.formatoImpressao eq relatorioUtil.formatoImpressaoXLS}">checked="checked"</c:if> />
		<label class="rotuloRadio" style="margin-right: 0; padding-right: 0" for="tipoExportacaoXLS"><img style="vertical-align: middle; margin-right: 0; padding-right: 0" src='<c:url value="/imagens/icone_excel_peq.png"/>' />Excel</label>
	
	</c:if>
	
	<br class="quebraLinha2" />	
	<hr class="linhaSeparadoraPopup" />
	
	<input type="button" id="botaoOK" class="bottonRightCol" value="OK">
</div>