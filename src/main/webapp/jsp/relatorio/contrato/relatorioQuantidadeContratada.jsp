<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script type="text/javascript">

	$(document).ready(function(){
		$("#dataInicial,#dataFinal,#dataAssinatura,#dataVencimento").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});
	
		// Dialog			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
		
		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente --//
		//Estado Inicial desabilitado
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");

		//Validar e checar intervalos de datas para liberar o bot�o Gerar.
		$(".campoData").bind('keypress blur focusin', function() {
			validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");
		});		
		validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");
		
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();
		$(".conteinerPesquisarIncluirAlterar select").val("-1");
		$(".conteinerPesquisarIncluirAlterar :text").val("");
	}
	

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function gerar(){
		$(".mensagens").hide();
		submeter('0', 'gerarRelatorioQuantidadesContratadas');
	}

</script>
<h1 class="tituloInterno">Quantidades Contratadas<a class="linkHelp" href="<help:help>/gerandoorelatriodequantidadescontratadas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os filtros que ser�o utilizados e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" styleId="formRelatorio">
	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
	
	<fieldset class="conteinerPesquisarIncluirAlterar" id="conteinerRelatorioQuantidadeContratada">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoFormatadoCliente}"/>
				<jsp:param name="possuiRadio" value="false"/>
				<jsp:param name="funcaoParametro" value="ativarBotoes"/>
			</jsp:include>
		</fieldset>
		
		<fieldset class="colunaEsq2" style="padding-top: 15px">
			<label class="rotulo campoObrigatorio" id="rotuloPeriodo" for="dataInicial"><span class="campoObrigatorioSimbolo">* </span>Per�odo:</label>
			<input tabindex="5" class="campoData campoHorizontal" type="text" id="dataInicial" name="dataInicial" value="${relatorioContratoVO.dataInicial}">
			<label class="rotuloEntreCampos">a</label>
			<input tabindex="6" class="campoData campoHorizontal" type="text" id="dataFinal" name="dataFinal" value="${relatorioContratoVO.dataFinal}"><br class="quebraLinha2"/>
			
			<label class="rotulo">N�mero do Contrato:</label>
			<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="9" size="8" value="${relatorioContratoVO.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"><br />
			
			<label class="rotulo" for="idSegmento">Segmento:</label>
			<select name="idSegmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>				
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" 
					<c:if test="${relatorioContratoVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>
				</c:forEach>
			</select><br />	
			
			<label class="rotulo">Data Assinatura:</label>
			<input class="campoData" type="text" id="dataAssinatura" name="dataAssinatura" maxlength="10" value="${relatorioContratoVO.dataAssinatura}">
			<br />	
			
			<label class="rotulo">Data Vencimento:</label>
			<input class="campoData" type="text" id="dataVencimento" name="dataVencimento" maxlength="10" value="${relatorioContratoVO.dataVencimento}">
		</fieldset>
			
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="gerarRelatorioQuantidadesContratadas">
				<input name="button" id="botaoGerar" class="bottonRightCol2" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
			</vacess:vacess>
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>
</form:form>