<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script type="text/javascript">

	$(document).ready(function(){
		
		
		$("#dataInicial,#dataFinal").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 	
			dateFormat: 'dd/mm/yy'
		});
		// Dialog			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
		
// 		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	

	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};			
		
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	
	function limparFormulario(){
		document.relatorioForm.numeroContrato.value = "";
		document.relatorioForm.habilitado[0].checked = true;	
		
		
    	limparCamposPesquisa();
    	
    	document.relatorioForm.indicadorPesquisa[0].checked = false;
    	document.relatorioForm.indicadorPesquisa[1].checked = false;
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
 			limparCamposPesquisaClienteImovel();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisaClienteImovel();
			
		}	
	}
	
	function limparCamposPesquisa(){
		
		$(".campoData").val("");
		$("#recuperacao").attr("checked","checked");
		$("input[name=indicadorPesquisa]").removeAttr("checked");
		
		limparFormularioDadosCliente();		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		document.getElementById('situacaoContrato').value = "-1";
		
		
	}
	
	function limparCamposPesquisaClienteImovel(){	
		limparFormularioDadosCliente();		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		document.getElementById('situacaoContrato').value = "-1";
		
	}
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function gerar() {
		$(".mensagens").hide();
		submeter('0','gerarRelatorioCobrancaRecuperacaoPenalidades');
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function init() {
		<c:choose>
			<c:when test="${relatorioContratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${relatorioContratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
	}	
	
	addLoadEvent(init);

</script>
<h1 class="tituloInterno">Cobran�a e Recupera��o de Penalidades<a class="linkHelp" href="<help:help>/gerandoorelatriodequantidadescontratadas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os filtros que ser�o utilizados e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" styleId="formRelatorio">
	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
	
	<fieldset class="conteinerPesquisarIncluirAlterar" id="conteinerRelatorioQuantidadeContratada">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${relatorioContratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${relatorioContratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<label class="rotulo campoObrigatorio" id="rotuloPeriodo" for="dataInicial"><span class="campoObrigatorioSimbolo">* </span>Per�odo:</label>
			<input tabindex="5" class="campoData campoHorizontal" type="text" id="dataInicial" name="dataInicial" value="${relatorioContratoVO.dataInicial}">
			<label class="rotuloEntreCampos">a</label>
			<input tabindex="6" class="campoData campoHorizontal" type="text" id="dataFinal" name="dataFinal" value="${relatorioContratoVO.dataFinal}"><br class="quebraLinha2"/>
			
			<label class="rotulo" for="tipoExibicaoRadio" style="margin-bottom: 0px; margin-left: 20px;"> Modelo de Relat�rio:</label>
						
				<input class="campoRadio" type="radio" name="tipo" id="recuperacao" value="recuperacao" checked="checked"
				<c:if test="${relatorioContratoVO.tipo eq 'recuperacao'}">checked="checked"</c:if>>
				<label class="rotuloRadio" for="recuperacao">Recupera��o</label>
			
				<input class="campoRadio" type="radio" name="tipo" id="top" value="top" 
				<c:if test="${relatorioContratoVO.tipo eq 'top'}">checked="checked"</c:if>>
				<label class="rotuloRadio" for="top" style="margin-right: 0; padding-right: 0">Top/PCM</label>
			
		<fieldset class="conteinerBotoesPesquisarDirFixo">
<%-- 			<vacess:vacess param="gerarRelatorioCobrancaRecuperacaoPenalidade"> --%>
				<input name="button" id="botaoGerar" class="bottonRightCol2" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
<%-- 			</vacess:vacess> --%>
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>
</form:form>