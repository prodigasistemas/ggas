<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Autoriza��o de Servi�o</h1>
<p class="orientacaoInicial">Informe o filtro e em seguida clique em <span class="destaqueOrientacaoInicial"> Gerar.</span></p>

<script type="text/javascript">

$(document).ready(function(){
	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	
	$("#cpfCliente").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjCliente").inputmask("99.999.999/9999-99",{placeholder:"_"});	
	$("#cpfSolicitante").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjSolicitante").inputmask("99.999.999/9999-99",{placeholder:"_"});
		
	document.forms[0].servicoAtraso[2].checked=true;
	
	$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 170, maxHeight: 170, resizable: false});
	
	$( "#nomeCliente" ).autocomplete({
		 source: function( request, response ) {
			 
		 				$.ajax({
		 							url: "carregaNomesClientes?nome="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
			 											
		 											response( $.map( data, function( item ) {
		 											return {
		 													label:	item.nome,
		 													name:	item.nome + "," + item.rg + "," + item.cpf,
		 													}
		 											}));
		 										}
		 						});
		 		},
		 minLength: 3,
		 select: function( event, ui ) {

		 			var dados = ui.item.name.split(",");	 		
		 			carregarFormPessoaFisica(dados);
		 	
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
		 });
	 
	 $( "#nomeFantasiaCliente" ).autocomplete({
		 source: function( request, response ) {
			 
		 				$.ajax({
		 							url: "carregaNomesClientes?nomeFantasia="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
			 											
		 											response( $.map( data, function( item ) {
		 											return {
			 												label: 	item.nomeFantasia,
		 													name:	item.nomeFantasia + "," + item.cnpj,
		 													}
		 											}));
		 										}
		 						});
		 		},
		 minLength: 3,
		 select: function( event, ui ) {
			 
			 	var dados = ui.item.name.split(",");	 		
			 	carregarFormPessoaJuridica(dados);
		 		
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
		 });
	 $('.tooltip').tooltip();
});

function carregarFormPessoaFisica(dados){
	if(dados[0] != "null" && dados[0] != null){	
		$("#nomeCliente").val(dados[0]);
	}else{
		$("#nomeCliente").val(null);
	}
	if(dados[1] != "null" && dados[1] != null){		
		$("#rgCliente").val(dados[1]);
	}else{
		$("#rgCliente").val(null);
	}
	$("#cpfCliente").val(dados[2]);
}

function carregarFormPessoaJuridica(dados){	
	$("#nomeFantasiaCliente").val(dados[0]);
	$("#cnpjCliente").val(dados[1]);
}


function limparCampoCpfCnpj(obj){
	if(obj.value.length > 14 && obj.value.length < 18 ){
		obj.value ="";
	}else{
		if(obj.value.length < 14){
			obj.value ="";
		}
	}
	
}

function gerar(){	
	$(".mensagens").hide();
	var formatoImpressao = document.forms[0].formatoImpressao.value;
	var exibirFiltros = document.forms[0].exibirFiltros.value;
	var agrupamento = document.getElementById("agrupamento").value;
	submeter("servicoAutorizacaoForm","gerarRelatorioAutorizacaoServico?tipoRelatorio="+formatoImpressao+"&agrupamento ="+agrupamento+"&exibirFiltros ="+exibirFiltros);
	
}
//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
function exibirExportarRelatorio(){
	exibirJDialog("#exportarRelatorio");
}

function limparFormulario() {		
	submeter("servicoAutorizacaoForm","exibirPesquisaRelatorioAutorizacaoServico");
// 	limparFormularios(document.servicoAutorizacaoForm);
// 	document.forms[0].servicoAtraso[2].checked = true;
// 	document.forms[0].condominioImovel[2].checked = true;
	
}

animatedcollapse.addDiv('mostrarFormChamado', 'fade=0,speed=400,group=chamado,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormImovel', 'fade=0,speed=400,group=imovel,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormContrato', 'fade=0,speed=400,group=contrato,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormPessoaFisica', 'fade=0,speed=400,group=pessoaFisica,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormPessoaJuridica', 'fade=0,speed=400,group=pessoaJuridica,persist=1,hide=0');

function atualizarAssuntos(chave) {
	var noCache = "noCache=" + new Date().getTime();
	var chaveAssunto = document.forms[0].chaveAssunto.value;
	$("#divAssuntos").load("carregarAssuntosServico?chavePrimaria="+chave+"&chaveAssunto="+chaveAssunto+"&"+noCache);
}


</script>

<form:form action="gerarRelatorioAutorizacaoServico" id="servicoAutorizacaoForm" name="servicoAutorizacaoForm" method="post" modelAttribute="ServicoAutorizacaoVO">
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
	<input type="hidden" id="chaveAssunto" name="chaveAssunto" value="${chaveAssunto}">
	
<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset style="padding-bottom: 20px">
	<fieldset class="colunaServicoAutorizacao">

	            <label class="rotulo" style="margin-left: 10px;" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" >Data de Gera��o:</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataGeracaoInicio" name="dataGeracaoInicio" maxlength="10" value="${servicoAutorizacaoVO.dataGeracaoInicio}">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataGeracaoFim" name="dataGeracaoFim" maxlength="10" value="${servicoAutorizacaoVO.dataGeracaoFim}">
				<br />
				
				
				<label class="rotulo rotulo2Linhas" style="margin-left: 6px;" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" >Data Previs�o <br/>de Encerramento:</label>
				<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataPrevisaoInicio" name="dataPrevisaoInicio" maxlength="10" value="${servicoAutorizacaoVO.dataPrevisaoInicio}">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
				<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataPrevisaoFim" name="dataPrevisaoFim" maxlength="10" value="${servicoAutorizacaoVO.dataPrevisaoFim}">
				<br />
				
				<label class="rotulo rotulo2Linhas" style="margin-left: 23px;" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" >Data de<br/>Encerramento:</label>
				<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataEncerramentoInicio" name="dataEncerramentoInicio" maxlength="10" value="${servicoAutorizacaoVO.dataEncerramentoInicio}">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
				<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataEncerramentoFim" name="dataEncerramentoFim" maxlength="10" value="${servicoAutorizacaoVO.dataEncerramentoFim}">
				<br />
							
			<label class="rotulo rotuloVertical rotuloCampoList" style="margin-left: 15px" id="rotuloTipo" for="tipo">Tipo de Servi�o:</label>
			<select name="servicosTipos" id="servicosTipos" class="campoList" multiple="multiple" style="width: 270px;">
				<c:forEach items="${listaTipoServico}" var="servico">
					<option title="${servico.descricao}" value="<c:out value="${servico.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.servicoTipo.chavePrimaria == servico.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${servico.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
		
			<label class="rotulo" style="margin-left: 65px" id="rotuloMarca" for="equipe">Equipe:</label>
			<select name="equipe" id="equipe" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaEquipe}" var="equipe">
					<option value="<c:out value="${equipe.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${equipe.nome}"/>
					</option>		
			    </c:forEach>
			</select>	
	
   </fieldset>

    <fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			
			
			<label class="rotulo" id="rotuloAgrupar" for="agrupamento">Agrupar Por:</label>
			<select name="agrupamento" id="agrupamento" class="campoSelect">
				<option value="cliente">Cliente</option>
				<option value="equipe">Equipe</option>
			</select>
			
			
			<label class="rotulo rotulo2Linhas" id="rotuloTipoDeServico" for="statusSevicoAutorizacao">Status da Autoriza��o de Servi�o:</label>
			<input class="campoRadio" type="checkbox"  name="listaStatus" <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Aberta')}">checked="checked"</c:if> value="Aberta">
			<label class="rotuloRadio" id="rotuloTipoDeServico" for="statusSevicoAutorizacao">&nbsp;Aberta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<input class="campoRadio" type="checkbox"  name="listaStatus" <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Cancelada')}">checked="checked"</c:if>  value="Cancelada">
			<label class="rotuloRadio" id="rotuloTipoDeServico" for="statusSevicoAutorizacao">&nbsp;Cancelada</label>
			
			<input class="campoRadio" type="checkbox"  name="listaStatus"  <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Encerrada')}">checked="checked"</c:if> value="Encerrada">
			<label class="rotuloRadio" id="rotuloTipoDeServico" for="statusSevicoAutorizacao">&nbsp;Encerrada</label>
			<input class="campoRadio" type="checkbox"  name="listaStatus" <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Pendente de Atualizacao')}">checked="checked"</c:if>  value="Pendente de Atualizacao">
			<label class="rotuloRadio" id="rotuloTipoDeServico" for="statusSevicoAutorizacao">&nbsp;Pendente de Atualizacao</label>

			<input class="campoRadio" type="checkbox"  name="listaStatus"  <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Em execu��o')}">checked="checked"</c:if> value="Em execu��o">
			<label class="rotuloRadio" id="rotuloTipoDeServico" for="statusSevicoAutorizacao">&nbsp;Em execu��o</label>
			<input class="campoRadio" type="checkbox"  name="listaStatus" value="Todos">
			<label class="rotuloRadio" id="rotuloTipoDeServico" for="statusSevicoAutorizacao">&nbsp;Todos</label>
			
			
			<label class="rotulo rotulo2Linhas" for="habilitado">Autoriza��o de servi�o<br/>em atraso:</label>
			<input class="campoRadio" type="radio" name="servicoAtraso" id="servicoAtraso" value="true" <c:if test="${servicoAutorizacaoVO.servicoAtraso eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio" type="radio" name="servicoAtraso" id="servicoAtraso" value="false" <c:if test="${servicoAutorizacaoVO.servicoAtraso eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
			<input class="campoRadio" type="radio" name="servicoAtraso" id="servicoAtraso" value="" <c:if test="${servicoAutorizacaoVO.servicoAtraso eq null}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
			<br/>
		
			<label class="rotulo rotulo2Linhas" id="rotuloMarca" for="marca">Prioridade do<br/>Tipo de Servi�o:</label>
			<select name="servicoTipoPrioridade" id="servicoTipoPrioridade" class="campoSelect" >
				<option value="-1">Selecione</option>
				<c:forEach items="${listaServicoTipoPrioridade}" var="prioridade">
					<option value="<c:out value="${prioridade.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.servicoTipoPrioridade.chavePrimaria == prioridade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${prioridade.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />	

			<label class="rotulo rotulo2Linhas" for="encerradoAposPrazo"> Autoriza��o de servi�o encerrada ap�s o prazo previsto</label>
			<input class="campoRadio" type="radio" name="encerradoAposPrazo" id="encerradoAposPrazo" value="true" <c:if test="${servicoAutorizacaoVO.encerradoAposPrazo eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio" type="radio" name="encerradoAposPrazo" id="encerradoAposPrazo" value="false" <c:if test="${servicoAutorizacaoVO.encerradoAposPrazo eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
			<input class="campoRadio" type="radio" name="encerradoAposPrazo" id="encerradoAposPrazo" value="" <c:if test="${servicoAutorizacaoVO.encerradoAposPrazo eq null}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>

			<label class="rotulo rotulo2Linhas" for="encerradoAposPrazo"> Autoriza��o de servi�o encerrada ap�s o prazo previsto</label>
			<input class="campoRadio" type="radio" name="indicadorAssociado" id="indicadorAssociado" value="true" <c:if test="${servicoAutorizacaoVO.indicadorAssociado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Com Cliente Associado</label>
			<input class="campoRadio" type="radio" name="indicadorAssociado" id="indicadorAssociado" value="false" <c:if test="${servicoAutorizacaoVO.indicadorAssociado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sem Cliente Associado</label>
			<input class="campoRadio" type="radio" name="indicadorAssociado" id="indicadorAssociado" value="" <c:if test="${servicoAutorizacaoVO.indicadorAssociado eq null}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
			
			<label class="rotulo rotulo2Linhas" for="tipagemRelatorio"> Tipo de Relat�rio: </label>
			<input class="campoRadio" type="radio" name="tipagemRelatorio" id="tipagemRelatorio" value="relatorioServicoAutorizacaoAnalitico" checked />
			<label class="rotuloRadio" for="tipagemRelatorio">Anal�tico</label>
			<input class="campoRadio" type="radio" name="tipagemRelatorio" id="tipagemRelatorio" value="relatorioServicoAutorizacaoSintetico" >
			<label class="rotuloRadio" for="tipagemRelatorio">Sint�tico</label>					
			<input class="campoRadio" type="radio" name="tipagemRelatorio" id="tipagemRelatorio" value="relatorioServicoAutorizacaoResumo" >
			<label class="rotuloRadio" for="tipagemRelatorio">Resumo</label>					
			
			</fieldset>
		</fieldset>

</fieldset>

<fieldset id="pesquisarServicoAutorizacao" class="colunaEsq">


	<fieldset id="pesquisarImovel" class="fieldsChamados pesquisarServicoAutorizacaoLeft">
	<legend>Filtro Chamado</legend>
		<div class="pesquisarImovelFundo">
		<a class="linkPesquisaAvancada" id="pesquisaContrato" href="#" rel="toggle['mostrarFormChamado']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Chamado<img src="imagens/setaBaixo.png" border="0"></a><br />
		<fieldset id="mostrarFormChamado" class="fieldsChamados">
		
		<label class="rotulo rotulo2Linhas" id="rotuloNumeroProtocolo" for="numeroProtocolo">N�mero do Protocolo:</label>
		<input class="campoTexto" type="text" id="numeroProtocoloChamado" name="numeroProtocoloChamado"  maxlength="10" size="10" value="${servicoAutorizacaoVO.numeroProtocoloChamado}" onkeypress="return formatarCampoInteiro(event);"><br />

		<label class="rotulo" id="rotuloNomeSolicitante" for="nomeSolicitante">Nome do Solicitante:</label>
		<input class="campoTexto" type="text" id="nomeSolicitanteChamado" name="nomeSolicitanteChamado"  maxlength="30" size="30" value="${servicoAutorizacaoVO.nomeSolicitanteChamado}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"><br />
		<label class="rotulo rotulo2Linhas" id="rotuloCnpj" for="cpfCnpjSolicitante">CPF/CNPJ do Solicitante:</label>
		<input class="campoTexto" type="text" id="cpfCnpjSolicitanteChamado" name="cpfCnpjSolicitanteChamado" maxlength="18" size="18" value="${servicoAutorizacaoVO.cpfCnpjSolicitanteChamado}" onkeypress='mascaraCpfCnpj(this,cpfCnpj)' onblur="limparCampoCpfCnpj(this)"><br />
		
		<fieldset id="conteinerSegmentos" class="conteinerCampoList">
		<fieldset class="conteinerLista1">
		<label class="rotulo rotuloVertical rotuloCampoList" id="rotuloTipoChamado" for="tipoChamado">Tipo do Chamado:</label>
		<select id="idChamadoTipo"  class="campoList campoVertical" name="idChamadoTipo" multiple="multiple"  onclick="atualizarAssuntos(this.value);">
				<c:forEach items="${listaChamadoTipo}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.idChamadoTipo == tipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipo.descricao}"/>
					</option>
				</c:forEach>
		</select>
<!-- 		<label class="rotulo" id="rotuloAssuntoChamado" for="assuntoChamado">Assunto do Chamado:</label> -->
		</fieldset>
		<div id="divAssuntos">
			<jsp:include page="/jsp/relatorio/selectBoxAssunto.jsp"></jsp:include>
		</div>
		</fieldset>
		
<!-- 		<select id="chamadoAssunto" class="campoList campoVertical" multiple="multiple" name="chamadoAssunto"> -->
<!-- 				<option value="-1">Selecione</option> -->
<%-- 					<c:forEach items="${listaChamadoAssunto}" var="assunto"> --%>
<%-- 						<option value="<c:out value="${assunto.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.chamadoAssunto.chavePrimaria == assunto.chavePrimaria}">selected="selected"</c:if>> --%>
<%-- 							<c:out value="${assunto.descricao}"/> --%>
<!-- 						</option> -->
<%-- 					</c:forEach> --%>
<!-- 			</select><br/> -->
	
		
		<label class="rotulo" id="rotuloStatusChamado" for="statusChamado">Status Chamado:</label>
		<select id="statusChamado" class="campoSelect" name="statusChamado">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaStatusChamado}" var="status">
				<option value="<c:out value="${status.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.statusChamado.chavePrimaria == status.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${status.descricao}"/>
				</option>
			</c:forEach>
		</select><br/>
		
		
		 <label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Canal de Atendimento:</label>
					<select id="canalAtendimento" class="campoSelect" name="canalAtendimento">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaCanalAtendimento}" var="canal">
						<option value="<c:out value="${canal.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.canalAtendimento.chavePrimaria == canal.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${canal.descricao}"/>
						</option>
						</c:forEach>
					</select><br/>	
					<label class="rotulo" id="rotuloUnidadeOrganizacional" for="unidadeOrganizacional">Unidade Organizacional:</label>
					<select id="unidadeOrganizacionalChamado" class="campoSelect" name="unidadeOrganizacionalChamado" onclick="carregarResponsavel(this.value);">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeOrganizacional}" var="unidade">
						<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.unidadeOrganizacionalChamado.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${unidade.descricao}"/>
						</option>
						</c:forEach>
					</select><br/>
					<label class="rotulo" id="rotuloRespons�vel" for="respons�vel">Respons�vel:</label>
					<div id="divResponsavel">
						<select id="usuarioResponsavelChamado" class="campoSelect" name="usuarioResponsavelChamado">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaUsuarioResponsavel}" var="responsavel">
							<option value="<c:out value="${responsavel.usuario.chavePrimaria}"/>" <c:if test="${servicoAutorizacaoVO.usuarioResponsavelChamado.funcionario.chavePrimaria == responsavel.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${responsavel.nome}"/>
							</option>
							</c:forEach>
						</select><br/>		
					</div>
					</fieldset>
		</div>
	</fieldset>


	<fieldset id="pesquisarImovel" class="fieldsChamados pesquisarServicoAutorizacaoLeft">
		<legend>Filtro Cliente</legend>
		<div class="pesquisarImovelFundo">
		<a class="linkPesquisaAvancada" id="pesquisaContrato" href="#" rel="toggle['mostrarFormPessoaFisica']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pessoa F�sica <img src="imagens/setaBaixo.png" border="0"></a><br />
		<fieldset id="mostrarFormPessoaFisica" class="fieldsChamados">
			<label class="rotulo" id="rotuloCpf" for="cpfCliente">CPF:</label>
			<input class="campoTexto" type="text" id="cpfCliente" name="cpfCliente"  maxlength="14" size="14" value="${servicoAutorizacaoVO.cpfCliente}"><br />
			<label class="rotulo" id="rotuloRg" for="rgCliente">RG:</label>
			<input class="campoTexto" type="text" id="rgCliente" name="rgCliente"  maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event);" value="${servicoAutorizacaoVO.rgCliente}"><br />
			<label class="rotulo" id="rotuloNome" for="nomeCliente">Nome:</label>
			<input class="campoTexto" type="text" id="nomeCliente" name="nomeCliente"  maxlength="30" size="30" value="${servicoAutorizacaoVO.nomeCliente}" onkeyup="letraMaiuscula(this);" onblur="this.value = removerEspacoInicialFinal(this.value);"><br />
		</fieldset>
		</div>
	</fieldset>
	
	<fieldset id="pesquisarImovel" class="fieldsChamados colunaEsq">
		<div class="pesquisarImovelFundo">
		<a class="linkPesquisaAvancada" id="pesquisaContrato" href="#" rel="toggle['mostrarFormPessoaJuridica']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pessoa Jur�dica <img src="imagens/setaBaixo.png" border="0"></a><br />
		<fieldset id="mostrarFormPessoaJuridica" class="fieldsChamados">
			<label class="rotulo" id="rotuloCnpj" for="cnpjCliente">CNPJ:</label>
			<input class="campoTexto" type="text" id="cnpjCliente" name="cnpjCliente" maxlength="18" size="18" value="${servicoAutorizacaoVO.cnpjCliente}"><br />
			<label class="rotulo" id="rotuloNomeFantasia" for="nomeFantasiaCliente">Nome Fantasia:</label>
			<input class="campoTexto" type="text" id="nomeFantasiaCliente" name="nomeFantasiaCliente" maxlength="30" size="30" value="${servicoAutorizacaoVO.nomeFantasiaCliente}" onkeyup="letraMaiuscula(this);" onblur="this.value = removerEspacoInicialFinal(this.value);"><br />
		</fieldset>
		</div>
	</fieldset>
	

	
</fieldset>




<fieldset id="pesquisarCliente" class="fieldsChamados pesquisarServicoAutorizacaoDir">
	<fieldset id="pesquisarImovel" class="colunaDir">
		<legend>Filtro Im�vel</legend>
		<div class="pesquisarImovelFundo">
		<a class="linkPesquisaAvancada" id="pesquisaImovel" href="#" rel="toggle['mostrarFormImovel']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Im�vel <img src="imagens/setaBaixo.png" border="0"></a><br />
		<fieldset id="mostrarFormImovel" class="fieldsChamados">
			<fieldset class="exibirCep">
				<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
					<jsp:param name="cepObrigatorio" value="false"/>
					<jsp:param name="idCampoCep" value="cepImovel"/>
					<jsp:param name="numeroCep" value="${servicoAutorizacaoVO.cepImovel}"/>
				</jsp:include>
			</fieldset>
			<label class="rotulo" id="rotuloNumeroImovel" for="numeroImovel">N�mero do Im�vel:</label>
			<input class="campoTexto" type="text" id="numeroImovel" name="numeroImovel"  maxlength="9" size="9" onkeypress="return formatarCampoNumeroEndereco(event, this);" value="${servicoAutorizacaoVO.numeroImovel}"><br />
			<label class="rotulo" id="rotuloComplemento" for="descricaoComplemento">Complemento:</label>
			<input class="campoTexto" type="text" id="descricaoComplemento" name="descricaoComplemento"   maxlength="25" size="30" value="${servicoAutorizacaoVO.descricaoComplemento}" onkeyup="letraMaiuscula(this);" onblur="this.value = removerEspacoInicialFinal(this.value);"><br />
			<label class="rotulo" id="rotuloComplemento" for="nomeImovel">Nome Fantasia:</label>
			<input class="campoTexto" type="text" id="nomeImovel" name="nomeImovel"  maxlength="30" size="30" value="${servicoAutorizacaoVO.nomeImovel}" onkeyup="letraMaiuscula(this);" onblur="this.value = removerEspacoInicialFinal(this.value);"><br />
			<label class="rotulo" id="rotuloComplemento" for="matriculaImovel">Matr�cula:</label>
			<input class="campoTexto" type="text" id="matriculaImovel" name="matriculaImovel"  maxlength="9" size="9" onkeypress="return formatarCampoInteiro(event)" value="${servicoAutorizacaoVO.matriculaImovel}"><br />
		
			<label class="rotulo" for="habilitado">Imovel � Condom�nio:</label>
			<input class="campoRadio" type="radio" name="condominioImovel" id="condominioImovel" value="true" <c:if test="${ condominioImovel eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="condominio">Sim</label>
			<input class="campoRadio" type="radio" name="condominioImovel" id="condominioImovel" value="false" <c:if test="${ condominioImovel eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="condominio">N�o</label>
			<input class="campoRadio" type="radio" name="condominioImovel" id="condominioImovel" value="" <c:if test="${ condominioImovel eq null}">checked</c:if>>
			<label class="rotuloRadio" for="condominio">Todos</label>
			
		</fieldset>
		</div>
	</fieldset>
	
	<fieldset id="pesquisarImovel" class="fieldsChamados colunaDir">
		<legend>Filtro Contrato</legend>
		<div class="pesquisarImovelFundo">
		<a class="linkPesquisaAvancada" id="pesquisaContrato" href="#" rel="toggle['mostrarFormContrato']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Contrato <img src="imagens/setaBaixo.png" border="0"></a><br />
		<fieldset id="mostrarFormContrato" class="fieldsChamados">
			<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:</label>
			<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato"  maxlength="9" size="8" onkeypress="return formatarCampoInteiro(event);" value="${servicoAutorizacaoVO.numeroContrato}"><br />
		</fieldset>
		</div>
	</fieldset>
</fieldset>



</fieldset>

<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio()">	
		<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
</fieldset>

</form:form>
