<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<h1 class="tituloInterno">Relat�rios de Descontos Concedidos na Fatura<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos necess�rios abaixo e clique em<span class="destaqueOrientacaoInicial"> Gerar</span></p>
<script language="javascript">

$(document).ready(function(){
	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$("#cpfCliente").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjCliente").inputmask("99.999.999/9999-99",{placeholder:"_"});	
	$("#cpfSolicitante").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjSolicitante").inputmask("99.999.999/9999-99",{placeholder:"_"});
	
	$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 170, maxHeight: 170, resizable: false});	
	
	$('#numeroContrato').keyup(function() {
		if( $(this).val().length > 0 ) {
			$('#botaoPontoConsumo').attr('disabled', false);
		} else {
			$('#botaoPontoConsumo').attr('disabled', true);
		}
	});
});

function exibirPopupPesquisaImovel() {
	popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function selecionarImovel(idSelecionado){
	var idImovel = document.getElementById("idImovel");
	var matriculaImovel = document.getElementById("matriculaImovel");
	var nomeFantasia = document.getElementById("nomeFantasiaImovel");
	var numeroImovel = document.getElementById("numeroImovel");
	var cidadeImovel = document.getElementById("cidadeImovel");
	var indicadorCondominio = document.getElementById("condominio");
	
	if(idSelecionado != '') {				
		AjaxService.obterImovelPorChave( idSelecionado, {
           	callback: function(imovel) {	           		
           		if(imovel != null){  	           			        		      		         		
	               	idImovel.value = imovel["chavePrimaria"];
	               	matriculaImovel.value = imovel["chavePrimaria"];		               	
	               	nomeFantasia.value = imovel["nomeFantasia"];
	               	numeroImovel.value = imovel["numeroImovel"];
	               	cidadeImovel.value = imovel["cidadeImovel"];
	               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
               	}
        	}, async:false}
        	
        );	        
    } else {
   		idImovel.value = "";
    	matriculaImovel.value = "";
    	nomeFantasia.value = "";
		numeroImovel.value = "";
          	cidadeImovel.value = "";
          	indicadorCondominio.value = "";
   	}

	document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
	document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
	document.getElementById("numeroImovelTexto").value = numeroImovel.value;
	document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
	if(indicadorCondominio.value == 'true') {
		document.getElementById("indicadorCondominioImovelTexto1").checked = true;
	} else {
		document.getElementById("indicadorCondominioImovelTexto2").checked = true;
	}
	document.getElementById("botaoPontoConsumo").disabled = false;
}


function carregarFormPessoaFisica(dados){
	if(dados[0] != "null" && dados[0] != null){	
		$("#nomeCliente").val(dados[0]);
	}else{
		$("#nomeCliente").val(null);
	}
	if(dados[1] != "null" && dados[1] != null){		
		$("#rgCliente").val(dados[1]);
	}else{
		$("#rgCliente").val(null);
	}
	$("#cpfCliente").val(dados[2]);
}

function carregarFormPessoaJuridica(dados){	
	$("#nomeFantasiaCliente").val(dados[0]);
	$("#cnpjCliente").val(dados[1]);
}


function gerar(){	
	$(".mensagens").hide();
	var formatoImpressao = document.forms[0].formatoImpressao.value
	var exibirFiltros = document.forms[0].exibirFiltros.value;
	submeter("chamadoForm","gerarRelatorioDescontosConcedidosFatura?tipoRelatorio="+formatoImpressao+"&exibirFiltros ="+exibirFiltros);
	
}
//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
function exibirExportarRelatorio(){
	exibirJDialog("#exportarRelatorio");
}

function limparFormulario() {
	submeter("chamadoForm","exibirPesquisaRelatorioDescontosConcedidosFatura");	
}
function limparCampoCpfCnpj(obj){
	if(obj.value.length > 14 && obj.value.length < 18 ){
		obj.value ="";
	}else{
		if(obj.value.length < 14){
			obj.value ="";
		}
	}
	
}

function habilitaCliente(){
	$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
	$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado");
};
function habilitaImovel(){
	$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
	$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
};

function desabilitarPesquisaOposta(elem){
	limparFormularioDadosCliente();
	limparFormularioDadosImovel();		
	var selecao = getCheckedValue(elem);
	if(selecao == "indicadorPesquisaImovel"){
		pesquisarImovel(true);
		pesquisarCliente(false);
		habilitaImovel();
	}else{
		pesquisarImovel(false);
		pesquisarCliente(true);
		habilitaCliente();
	}	
}

function pesquisarPontoConsumo() {
	var idCliente = document.forms[0].idCliente.value;
	var idImovel = document.forms[0].idImovel.value;
	var numeroContrato = document.forms[0].numeroContrato.value;
	$("#gridPontosConsumo").load("pesquisarPontosConsumoDescontos?idImovel="+idImovel+"&idCliente="+idCliente+"&numeroContrato="+numeroContrato);
}

</script>

<form:form action="gerarRelatorioChamado" method="POST" id="chamadoForm" name="chamadoForm" modelAttribute="PesquisaRelatorioDescontosConcedidosFaturaVO" >

<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
<input type="hidden" id="chaveAssunto" name="chaveAssunto" value="${chaveAssunto}">
<input type="hidden" id="chaveCliente" value="${cliente.chavePrimaria}" />
<input type="hidden" id="cliente" />

<fieldset class="containerPesquisarIncluirAlterar">
	<fieldset id="conteinerRelatorioFaturamento" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${pesquisaRelatorioDescontosConcedidos.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${pesquisaRelatorioDescontosConcedidos.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${pesquisaRelatorioDescontosConcedidos.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${pesquisaRelatorioDescontosConcedidos.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${pesquisaRelatorioDescontosConcedidos.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${pesquisaRelatorioDescontosConcedidos.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
			<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${pesquisaRelatorioDescontosConcedidos.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${pesquisaRelatorioDescontosConcedidos.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${pesquisaRelatorioDescontosConcedidos.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${pesquisaRelatorioDescontosConcedidos.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${pesquisaRelatorioDescontosConcedidos.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${pesquisaRelatorioDescontosConcedidos.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${pesquisaRelatorioDescontosConcedidos.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirIndicador(); exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${pesquisaRelatorioDescontosConcedidos.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${pesquisaRelatorioDescontosConcedidos.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		<fieldset class="conteinerBloco3">
			<fieldset class="coluna detalhamentoColunaLarga">
			
				<fieldset class="conteinerLista1">
				<label class="rotulo" for="idSegmento">Grupo Faturamento:</label> 
				<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="campoSelect" >
					<option value="-1">Selecione</option>
					<c:forEach items="${listaGrupoFaturamento}" var="grupoFaturamento">
						<option value="<c:out value="${grupoFaturament.chavePrimaria}"/>" 
						<c:if test="${pesquisaRelatorioDescontosConcedidos.idGrupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${grupoFaturamento.descricao}"/>
						</option>
					</c:forEach>
				</select><br />
			<label class="rotulo rotuloVertical rotuloCampoList" for="chamadosTipo">Segmento:</label>
					<select id="segmentos" name="segmentos" class="campoList campoVertical" multiple="multiple"  >
						   	<c:forEach items="${listaSegmento}" var="segmento">
								<option value="<c:out value="${segmento.chavePrimaria}"/>" >
										<c:out value="${segmento.descricao}" />
								</option>		
						    </c:forEach>
					  </select>
		</fieldset>		
				
			</fieldset>
		
			<fieldset id="pesquisarImovel" class="colunaDir" style="float:none;">
			<label class="rotulo" for="numeroDocumentoInicio">Numero da Fatura:</label>
			<input class="campoTexto campoHorizontal" type="text" id="numeroFaturaInicial" name="numeroFaturaInicial" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${pesquisaRelatorioDescontosConcedidos.numeroFaturaInicial}">
			<label class="rotuloEntreCampos" for="numeroDocumentoFinal">a</label>
			<input class="campoTexto campoHorizontal" type="text" id="numeroFaturaFinal" name="numeroFaturaFinal" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${pesquisaRelatorioDescontosConcedidos.numeroFaturaFinal}"><br class="quebraLinha2" />		
			<label class="rotulo" for="numeroDocumentoInicio">Numero da NF:</label>
			<input class="campoTexto campoHorizontal" type="text" id="numeroNFInicial" name="numeroNFInicial" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${pesquisaRelatorioDescontosConcedidos.numeroNFInicial}">
			<label class="rotuloEntreCampos" for="numeroDocumentoFinal">a</label>
			<input class="campoTexto campoHorizontal" type="text" id="numeroNFFinal" name="numeroNFFinal" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${pesquisaRelatorioDescontosConcedidos.numeroNFFinal}"><br class="quebraLinha2" />		
			<label class="rotulo" for="numeroDocumentoInicio">N�mero do Contrato:</label>
			<input class="campoTexto campoHorizontal" type="text" id="numeroContrato" name="numeroContrato" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${pesquisaRelatorioDescontosConcedidos.numeroContrato}" >
			<label class="rotulo rotulo2Linhas" >Per�odo <br class="quebraLinhaTexto"/>de Emiss�o:</label>
			<input tabindex="5" class="campoHorizontal  campoData" type="text" id=dataEmissaoInicial name="dataEmissaoInicial" value="${pesquisaRelatorioDescontosConcedidos.dataEmissaoInicial}" style="margin-top: 8px" >
			<label class="rotuloEntreCampos" style="margin-top: 4px">a</label>
			<input tabindex="6" class="campoHorizontal campo2Linhas campoData" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" value="${pesquisaRelatorioDescontosConcedidos.dataEmissaoFinal}" style="margin-top: 8px" ><br/><br class="quebraLinha"/>
			<label class="rotulo rotulo2Linhas" >Intervalo <br class="quebraLinhaTexto"/>de Vencimento:</label>
			<input tabindex="5" class="campoHorizontal  campoData" type="text" id="vencimentoInicial" name="vencimentoInicial" value="${pesquisaRelatorioDescontosConcedidos.vencimentoInicial}" style="margin-top: 8px" >
			<label class="rotuloEntreCampos" style="margin-top: 4px">a</label>
			<input tabindex="6" class="campoHorizontal campo2Linhas campoData" type="text" id="vencimentoFinal" name="vencimentoFinal" value="${pesquisaRelatorioDescontosConcedidos.vencimentoFinal}" style="margin-top: 8px" ><br/><br class="quebraLinha"/>
			</fieldset>
		</fieldset>
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();" disabled="disabled">
		</fieldset>
	</fieldset>
	
	<div id="gridPontosConsumo">
			<jsp:include page="/jsp/relatorio/gridPontosConsumo.jsp" />
	</div>
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio()">	
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>
	
</fieldset>

</form:form>