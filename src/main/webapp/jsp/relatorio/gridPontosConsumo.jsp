<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 
 <%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadora1" />
		<fieldset class="conteinerBloco">
			<p class="orientacaoInicial">Selecione um Ponto de Consumo na lista abaixo. Para incluir ao filtro o Ponto de Consumo marque a caixa de sele��o � esquerda do Ponto de Consumo e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</p>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontosConsumoDescontos">
		        <display:column style="text-align: center; width: 25px">
		        	<input type="radio" name="idPontoConsumo" id="chavePontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"/>
		        </display:column>
		        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: left; padding-left: 10px">
		            <c:out value="${pontoConsumo.descricao}"/>
		        </display:column>
		        <display:column title="Segmento" style="width: 130px">
		            <c:out value="${pontoConsumo.segmento.descricao}"/>
		        </display:column>
		        <display:column title="Situa��o" style="width: 160px">
		            <c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
		        </display:column>
		    </display:table>
		</fieldset>
	</c:if>