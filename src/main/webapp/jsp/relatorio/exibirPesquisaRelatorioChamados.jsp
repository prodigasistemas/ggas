<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<h1 class="tituloInterno">Relat�rios dos Chamados <a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos necess�rios abaixo e clique em<span class="destaqueOrientacaoInicial"> Gerar</span></p>
<script language="javascript">

$(document).ready(function(){
	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$("#cpfCliente").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjCliente").inputmask("99.999.999/9999-99",{placeholder:"_"});	
	$("#cpfSolicitante").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjSolicitante").inputmask("99.999.999/9999-99",{placeholder:"_"});
	
	informacoesRelatorioAnalitico();
	
	$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 170, maxHeight: 170, resizable: false});	
	
	$( "#nomeCliente" ).autocomplete({
		 source: function( request, response ) {
			 
		 				$.ajax({
		 							url: "carregaNomesClientes?nome="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
			 											
		 											response( $.map( data, function( item ) {
		 											return {
		 													label:	item.nome,
		 													name:	item.nome + "," + item.rg + "," + item.cpf,
		 													}
		 											}));
		 										}
		 						});
		 		},
		 minLength: 3,
		 select: function( event, ui ) {

		 			var dados = ui.item.name.split(",");	 		
		 			carregarFormPessoaFisica(dados);
		 	
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
		 });
	 
	 $( "#nomeFantasiaCliente" ).autocomplete({
		 source: function( request, response ) {
			 
		 				$.ajax({
		 							url: "carregaNomesClientes?nomeFantasia="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
			 											
		 											response( $.map( data, function( item ) {
		 											return {
			 												label: 	item.nomeFantasia,
		 													name:	item.nomeFantasia + "," + item.cnpj,
		 													}
		 											}));
		 										}
		 						});
		 		},
		 minLength: 3,
		 select: function( event, ui ) {
			 
			 	var dados = ui.item.name.split(",");	 		
			 	carregarFormPessoaJuridica(dados);
		 		
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
		 });
	 
	 $(".periodoChamado").inputmask("99/9999",{placeholder:"_"});

	
});

function carregarFormPessoaFisica(dados){
	if(dados[0] != "null" && dados[0] != null){	
		$("#nomeCliente").val(dados[0]);
	}else{
		$("#nomeCliente").val(null);
	}
	if(dados[1] != "null" && dados[1] != null){		
		$("#rgCliente").val(dados[1]);
	}else{
		$("#rgCliente").val(null);
	}
	$("#cpfCliente").val(dados[2]);
}

function carregarFormPessoaJuridica(dados){	
	$("#nomeFantasiaCliente").val(dados[0]);
	$("#cnpjCliente").val(dados[1]);
}


function gerar(){	
	$(".mensagens").hide();
	var formatoImpressao = document.forms[0].formatoImpressao.value
	var exibirFiltros = document.forms[0].exibirFiltros.value;
	submeter("chamadoForm","gerarRelatorioChamado?tipoRelatorio="+formatoImpressao+"&exibirFiltros ="+exibirFiltros);
	
}
//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
// function exibirExportarRelatorio(){
	
// 	$("#chavesChamadoTipo").val( $("#listaChamadoTipos").val() );	
// 	$("#modeloRelatorio").val("sintetico");	
// 	exibirJDialog("#exportarRelatorio");
// }


// function exibirExportarRelatorioQuantitativo(){
	
// 	$("#chavesChamadoTipo").val( $("#listaChamadoTipos").val() );	
// 	$("#modeloRelatorio").val("quantitativo");	
// 	exibirJDialog("#exportarRelatorio");
// }

// function exibirExportarRelatorioAnalitico(){
	
// 	$("#chavesChamadoTipo").val( $("#listaChamadoTipos").val() );	
// 	$("#modeloRelatorio").val("analitico");	
// 	exibirJDialog("#exportarRelatorio");
// }

function exibirExportarRelatorioGenerico(tipoRelatorio){
		switch(tipoRelatorio){
			case "ARSAL":
				$("#modeloRelatorio").val("sintetico");	
				break;
			case "Quantitativo":
				$("#modeloRelatorio").val("quantitativo");	
				break;
			case "Anal�tico":
				$("#modeloRelatorio").val("analitico");	
				break;
			case "Resumo":
				$("#modeloRelatorio").val("resumo");	
				break;	
		}
		$("#chavesChamadoTipo").val( $("#listaChamadoTipos").val());
		exibirJDialog("#exportarRelatorio");
}

function limparFormulario() {
	submeter("chamadoForm","exibirPesquisaRelatorioChamado");	
}
function limparCampoCpfCnpj(obj){
	if(obj.value.length > 14 && obj.value.length < 18 ){
		obj.value ="";
	}else{
		if(obj.value.length < 14){
			obj.value ="";
		}
	}
	
}

animatedcollapse.addDiv('mostrarFormChamado', 'fade=0,speed=400,group=chamado,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormImovel', 'fade=0,speed=400,group=imovel,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormContrato', 'fade=0,speed=400,group=contrato,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormPessoaFisica', 'fade=0,speed=400,group=pessoaFisica,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormPessoaJuridica', 'fade=0,speed=400,group=pessoaJuridica,persist=1,hide=0');

function atualizarAssuntos(chave) {
	var noCache = "noCache=" + new Date().getTime();
	var chaveAssunto = document.forms[0].chaveAssunto.value;
	$("#divAssuntos").load("carregarAssuntosServico?chavePrimaria="+chave+"&chaveAssunto="+chaveAssunto+"&"+noCache);
}



function informacoesRelatorioAnalitico() {
    $('.relatorioOutro').hide();
    $('.relatorioAnalitico').show();
    $('.relatorioResumo').hide();
    $('#periodoCriacao').val("");
    $('#periodoPrevisaoEncerramento').val("")
    $('#periodoResolucao').val("");
    
}

function informacoesRelatorioOutros() {
    $('.relatorioAnalitico').hide();
    $('.relatorioResumo').hide();
    $('.relatorioOutro').show();
	formatarDatas();
}

function informacoesRelatorioResumo(){
	$('.relatorioOutro').hide();
	$('.relatorioAnalitico').hide();
	$("#relatorioAnaliticos").show();
	$('.relatorioResumo').show();
	formatarDatas();
}

function formatarDatas(){
    $('#dataInicioCriacao').val("");
    $('#dataFimCriacao').val("")
    $('#dataInicioEncerramento').val("");
    $('#dataFimEncerramento').val("");
    $('#dataInicioResolucao').val("")
    $('#dataFimResolucao').val("");
}

</script>

<form:form action="gerarRelatorioChamado" method="POST" id="chamadoForm" name="chamadoForm" modelAttribute="ChamadoVO" >

<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
<input type="hidden" id="chaveAssunto" name="chaveAssunto" value="${chaveAssunto}">
<input type="hidden" id="chaveCliente" value="${cliente.chavePrimaria}" />
<input type="hidden" id="cliente" />
<input type="hidden" id="modeloRelatorio" name="modeloRelatorio" />

<fieldset class="containerPesquisarIncluirAlterar">
<legend>Filtro Pesquisa</legend>

	<fieldset id="pesquisarCliente" class="colunaEsq">

	 <fieldset id="pesquisarImovel" class="colunaEsq" class="fieldsChamados">
		<label class="rotulo rotulo2Linhas" id="rotuloNumeroProtocolo" for="numeroProtocolo">N�mero do Protocolo:</label>
		<input class="campoTexto" type="text" id="numeroProtocolo" name="numeroProtocolo"  maxlength="10" size="10" value="${chamadoVO.numeroProtocolo}" onkeypress="return formatarCampoInteiro(event);"><br />
		<label class="rotulo rotulo2Linhas" id="rotuloNomeSolicitante" for="nomeSolicitante">Nome do Solicitante:</label>
		<input class="campoTexto" type="text" id="nomeSolicitante" name="nomeSolicitante"  maxlength="30" size="27" value="${chamadoVO.nomeSolicitante}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"><br />
		<label class="rotulo rotulo2Linhas" id="rotuloCnpj" for="cpfCnpjSolicitante">CPF/CNPJ do Solicitante:</label>
		<input class="campoTexto" type="text" id="cpfCnpjSolicitante" name="cpfCnpjSolicitante" maxlength="18" size="18" value="${chamadoVO.cpfCnpjSolicitante}" onkeypress='mascaraCpfCnpj(this,cpfCnpj)' onblur="limparCampoCpfCnpj(this)"><br />
		<br/>
		
		
		<fieldset id="conteinerSegmentos" class="conteinerCampoList">
		<fieldset class="conteinerLista1">
		<label class="rotulo rotuloVertical rotuloCampoList" id="rotuloTipoChamado" for="listaChamadoTipos">Tipo do Chamado:</label>
		<select id="listaChamadoTipos"  class="campoList campoVertical" style="width:426px;height:20px;" onclick="atualizarAssuntos(this.value);">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaChamadoTipo}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>">
						<c:out value="${tipo.descricao}"/>
					</option>
				</c:forEach>
		</select>
		</fieldset>
		
		<input type="hidden" value="" id="chavesChamadoTipo" name="chavesChamadoTipo">
		
		</fieldset>
		
			<div id="divAssuntos">
			<jsp:include page="/jsp/relatorio/selectBoxAssunto.jsp"></jsp:include>
			</div>		
		
		<label class="rotulo rotuloVertical rotuloCampoList" id="rotuloTipoDeServico" for="tipoDeServico">Tipo do Servi�o:</label>
		<select id="servicosTipo" class="campoList campoVertical" name="servicosTipo" multiple="multiple" style="width:100%;font-size:12px;" >
				<c:forEach items="${listaTipoServico}" var="servico">
					<option title="${servico.descricao}" value="<c:out value="${servico.chavePrimaria}"/>" <c:if test="${chamadoVO.idServicoTipo == servico.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${servico.descricao}"/>
					</option>
				</c:forEach>
		</select><br/>
	
	</fieldset>	
		
	<fieldset id="pesquisarImovel" class="fieldsChamados colunaEsq">
		<legend>Filtro Cliente</legend>
		<div class="pesquisarImovelFundo">
		<a class="linkPesquisaAvancada" id="pesquisaContrato" href="#" rel="toggle['mostrarFormPessoaFisica']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pessoa F�sica <img src="imagens/setaBaixo.png" border="0"></a><br />
		<fieldset id="mostrarFormPessoaFisica" class="fieldsChamados">
			<label class="rotulo" id="rotuloCpf" for="cpfCliente">CPF:</label>
			<input class="campoTexto" type="text" id="cpfCliente" name="cpfCliente"  maxlength="14" size="14" value="${chamadoVO.cpfCliente}"><br />
			<label class="rotulo" id="rotuloRg" for="rgCliente">RG:</label>
			<input class="campoTexto" type="text" id="rgCliente" name="rgCliente"  maxlength="13" size="14" onkeypress="return formatarCampoInteiro(event);" value="${chamadoVO.rgCliente}"><br />
			<label class="rotulo" id="rotuloNome" for="nomeCliente">Nome:</label>
			<input class="campoTexto" type="text" id="nomeCliente" name="nomeCliente"  maxlength="30" size="30" value="${chamadoVO.nomeCliente}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"><br />
			
		</fieldset>
		</div>
	</fieldset>
	
	<fieldset id="pesquisarImovel" class="fieldsChamados colunaEsq">
		<div class="pesquisarImovelFundo">
		<a class="linkPesquisaAvancada" id="pesquisaContrato" href="#" rel="toggle['mostrarFormPessoaJuridica']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pessoa Jur�dica <img src="imagens/setaBaixo.png" border="0"></a><br />
		<fieldset id="mostrarFormPessoaJuridica" class="fieldsChamados">
			<label class="rotulo" id="rotuloCnpj" for="cnpjCliente">CNPJ:</label>
			<input class="campoTexto" type="text" id="cnpjCliente" name="cnpjCliente" maxlength="18" size="18" value="${chamadoVO.cnpjCliente}"><br />
			<label class="rotulo" id="rotuloNomeFantasia" for="nomeFantasiaCliente">Nome Fantasia:</label>
			<input class="campoTexto" type="text" id="nomeFantasiaCliente" name="nomeFantasiaCliente" maxlength="30" size="30" value="${chamadoVO.nomeFantasiaCliente}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"><br />
		</fieldset>
		</div>
	</fieldset>
		

	</fieldset>
	
	<fieldset id="pesquisarCliente" style="width:470px !important;" class="colunaDir">
		
		<fieldset id="pesquisarImovel" class="colunaDir">
		
		<label class="rotulo" id="rotuloCanalAtendimento" for="listaCanalAtendimento">Canal de Atendimento:</label>
					<select id="listaCanalAtendimento" class="campoList " name="listaCanalAtendimento" style="width:220px;" multiple="multiple" >
						<c:forEach items="${listaCanalAtendimento}" var="canal">
							<option value="<c:out value="${canal.chavePrimaria}"/>" 
								<c:forEach items="${chamadoVO.listaCanalAtendimento}" var = "ca">
									<c:if test="${ca.chavePrimaria == canal.chavePrimaria}">selected="selected"</c:if>
					    		</c:forEach>
							>
								<c:out value="${canal.descricao}"/>
							</option>
						</c:forEach>
					</select><br/>	
					<label class="rotulo" id="rotuloUnidadeOrganizacional" for="unidadeOrganizacional">Unidade Organizacional:</label>
					<select id="unidadeOrganizacional" class="campoSelect" name="unidadeOrganizacional" onclick="carregarResponsavel(this.value);">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeOrganizacional}" var="unidade">
						<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${chamadoVO.unidadeOrganizacional.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${unidade.descricao}"/>
						</option>
						</c:forEach>
					</select><br/>
					<label class="rotulo" id="rotuloRespons�vel" for="respons�vel">Respons�vel:</label>
					<div id="divResponsavel">
						<select id="usuarioResponsavel" class="campoSelect" name="usuarioResponsavel">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaUsuarioResponsavel}" var="responsavel">
							<option value="<c:out value="${responsavel.usuario.chavePrimaria}"/>" <c:if test="${chamadoVO.usuarioResponsavel.funcionario.chavePrimaria == responsavel.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${responsavel.nome}"/>
							</option>
							</c:forEach>
						</select><br/>		
					</div>
					<label class="rotulo" id="rotuloRespons�vel" for="respons�vel">Segmento:</label>
					<div id="divSegmentos">
						<select id="idSegmentoChamado" class="campoSelect" name="idSegmentoChamado">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaSegmento}" var="segmento">
							<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${chamadoVO.idSegmentoChamado == segmento.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${segmento.descricao}"/>
							</option>
							</c:forEach>
						</select><br/>		
					</div>

					<label class="rotulo" id="rotuloCategoria" for="categoria">Categoria:</label>
					<div id="divCategorias">
						<select id="idCategoriaChamado" class="campoSelect" name="categoriaChamado">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCategoria}" var="categoria">
							<option value="<c:out value="${categoria.name}"/>" <c:if test="${chamadoVO.categoria.name eq categoria.name}">selected="selected"</c:if>>
								<c:out value="${categoria.descricao}"/>
							</option>
							</c:forEach>
						</select><br/>		
					</div>
					
				<label class="rotulo" id="rotuloTipoDeServico" for="statusChamado" >Status do Chamado:</label>
					<input style="margin-left: 5px;" class="campoRadio" type="checkbox"  name="listaStatus" <c:if test="${fn:contains(chamadoVO.listaStatus,'ABERTO')}">checked="checked"</c:if> value="ABERTO">
					<label class="" id="rotuloTipoDeServico" for="statusChamado">&nbsp;Aberto</label>
					<input style="margin-left: 5px;" class="campoRadio" type="checkbox"  name="listaStatus" <c:if test="${fn:contains(chamadoVO.listaStatus,'EM ANDAMENTO')}">checked="checked"</c:if>  value="EM ANDAMENTO">
					<label class="" id="rotuloTipoDeServico" for="statusChamado">&nbsp;Em atendimento</label>
					<input style="margin-left: 5px;" class="campoRadio" type="checkbox"  name="listaStatus" <c:if test="${fn:contains(chamadoVO.listaStatus,'FINALIZADO')}">checked="checked"</c:if>  value="FINALIZADO">
					<label class="" id="rotuloTipoDeServico" for="statusChamado">&nbsp;Finalizado</label>
					<input style="margin-left: 15px;" class="campoRadio" type="checkbox"  name="listaStatus"  <c:if test="${fn:contains(chamadoVO.listaStatus,'RASCUNHO')}">checked="checked"</c:if> value="RASCUNHO">
					<label class="" id="rotuloTipoDeServico" for="statusChamado">&nbsp;Rascunho</label>
					<input style="margin-left: 5px;" class="campoRadio" type="checkbox"  name="listaStatus" value="Todos">
					<label class="" id="rotuloTipoDeServico" for="statusChamado">&nbsp;Todos</label>
				
				<label class="rotulo" id="rotuloPrazo" for="canalAtendimento">Prazo:</label>
					<input style="margin-left: 5px;" class="campoRadio" id="prazoAtrasado" type="checkbox" <c:if test="${fn:contains(chamadoVO.listaPrazo,'ATRASADOS')}">checked="checked"</c:if> name="listaPrazo"  value="ATRASADOS">  
					<label class="" id="rotuloPrazoAtrasado" for="prazoAtrasado">&nbsp;Atrasados</label>
					<input style="margin-left: 5px;" class="campoRadio" id="prazoHoje" type="checkbox" <c:if test="${fn:contains(chamadoVO.listaPrazo,'HOJE')}">checked="checked"</c:if> name="listaPrazo" value="HOJE" >  
					<label class="" id="rotuloPrazoHoje" for="prazoHoje">&nbsp;a Vencer</label>
					<input style="margin-left: 5px;" class="campoRadio" id="prazoFuturos" type="checkbox" <c:if test="${fn:contains(chamadoVO.listaPrazo,'FUTUROS')}">checked="checked"</c:if> name="listaPrazo" value="FUTUROS">  
					<label class="" id="rotuloPrazoFuturos" for="prazoFuturos">&nbsp;no Prazo</label>
				<br />
				
			<label class="rotulo rotulo2Linhas" style="margin-top:5px;" for="tipoRelatorioAnalitico">Tipo de Relat�rio:</label>
			<input class="campoRadio" type="radio" name="tipoRelatorioImpressao" id="tipoRelatorioAnalitico" value="true" checked onclick="informacoesRelatorioAnalitico();">
			<label class="rotuloRadio" for="indicadorUso">Anal�tico </label>
			<input class="campoRadio" type="radio" name="tipoRelatorioImpressao" id="tipoRelatorioOutros" value="false" onclick="informacoesRelatorioOutros();">
			<label class="rotuloRadio" for="indicadorUso">Quantitativo ou ARSAL</label>
			<div style="width:80px;margin-left:158px;">
				<input class="campoRadio" type="radio" name="tipoRelatorioImpressao" id="tipoRelatorioResumo" value="resumo" onclick="informacoesRelatorioResumo();">
				<label class="rotuloRadio" for="indicadorUso">Resumo</label>			
			</div>						
			<br />		
			
			
			<fieldset id="relatorioOutros" class="relatorioOutro colunaDir">	
				<label class="rotulo rotulo2Linhas" ><span class="campoObrigatorioSimbolo">**</span> Per�odo de  Gera��o:</label>
	            <input class="campoTexto campoHorizontal periodoChamado" type="text" id="periodoCriacao" name="periodoCriacao" 
	                         value="<c:out value="${chamadoVO.periodoCriacao}"/>" >			
				<br/><br class="quebraLinha"/>
				<label class="rotulo rotulo2Linhas" ><span class="campoObrigatorioSimbolo">**</span> Per�odo de Previs�o de Encerramento:</label>
	            <input class="campoTexto campoHorizontal periodoChamado" type="text" id="periodoPrevisaoEncerramento" name="periodoPrevisaoEncerramento" 
	                         value="<c:out value="${chamadoVO.periodoPrevisaoEncerramento}"/>" >	<br/><br class="quebraLinha"/>
				<label class="rotulo" id="rotuloDataInicioEncerramento" for="dataInicioResolucao" ><span class="campoObrigatorioSimbolo">**</span> Per�odo de Resolu��o:</label>
	            <input class="campoTexto campoHorizontal periodoChamado" type="text" id="periodoResolucao" name="periodoResolucao" 
	                         value="<c:out value="${chamadoVO.periodoResolucao}"/>" >
            </fieldset>
            
            
            <fieldset id="relatorioAnaliticos" class="relatorioAnalitico colunaDir">	
	            <label class="rotulo rotulo2Linhas" ><span class="campoObrigatorioSimbolo">**</span> Per�odo de  Gera��o:</label>
				<input tabindex="35" class="campoData campo2Linhas campoHorizontal" type="text" id="dataInicioCriacao" name="dataInicioCriacao" value="${chamadoVO.dataInicioCriacao}" style="margin-top: 8px" >
				<label class="rotuloEntreCampos" style="margin-top: 4px">a</label>
				<input tabindex="36" class="campoData campo2Linhas campoHorizontal" type="text" id="dataFimCriacao" name="dataFimCriacao" value="${chamadoVO.dataFimCriacao}" style="margin-top: 8px" ><br/><br class="quebraLinha"/>
				<label class="rotulo rotulo2Linhas" ><span class="campoObrigatorioSimbolo">**</span> Per�odo de Previs�o de Encerramento:</label>
				<input tabindex="37" class="campoHorizontal  campoData" type="text" id="dataInicioEncerramento" name="dataInicioEncerramento" value="${chamadoVO.dataInicioEncerramento}" style="margin-top: 8px" >
				<label class="rotuloEntreCampos" style="margin-top: 4px">a</label>
				<input tabindex="38" class="campoHorizontal campo2Linhas campoData" type="text" id="dataFimEncerramento" name="dataFimEncerramento" value="${chamadoVO.dataFimEncerramento}" style="margin-top: 8px" ><br/><br class="quebraLinha"/>
				
				<label class="rotulo" id="rotuloDataInicioEncerramento" for="dataInicioResolucao" ><span class="campoObrigatorioSimbolo">**</span> Per�odo de Resolu��o:</label>
				<input tabindex="39" class="campoData campo2Linhas campoHorizontal" type="text" id="dataInicioResolucao" name="dataInicioResolucao" maxlength="10" value="${chamadoVO.dataInicioResolucao}">
				<label class="rotuloEntreCampos" for="dataFimResolucao">a</label>
				<input tabindex="30" class="campoData campo2Linhas campoHorizontal" type="text" id="dataFimResolucao" name="dataFimResolucao" maxlength="10" value="${chamadoVO.dataFimResolucao}">
            </fieldset>
            
			<br />
			
			<p class="legenda" style="margin-top: 0;">
				<span class="campoObrigatorioSimbolo">**</span>
				� necess�rio definir ao menos um intervalo para a gera��o do relat�rio
			</p>
		</fieldset>
		<fieldset id="pesquisarImovel" class="colunaDir">
			<legend>Filtro Im�vel</legend>
			<div class="pesquisarImovelFundo">
			<a class="linkPesquisaAvancada" id="pesquisaImovel" href="#" rel="toggle['mostrarFormImovel']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Im�vel <img src="imagens/setaBaixo.png" border="0"></a><br />
			<fieldset id="mostrarFormImovel" class="fieldsChamados">
				<fieldset class="exibirCep">
					<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
						<jsp:param name="cepObrigatorio" value="false"/>
						<jsp:param name="idCampoCep" value="cepImovel"/>
						<jsp:param name="numeroCep" value="${chamadoVO.cepImovel}"/>
					</jsp:include>
				</fieldset>
				<label class="rotulo" id="rotuloNumeroImovel" for="numeroImovel">N�mero do Im�vel:</label>
				<input class="campoTexto" type="text" id="numeroImovel" name="numeroImovel"  maxlength="9" size="9" onkeypress="return formatarCampoNumeroEndereco(event, this);" value="${chamadoVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloComplemento" for="descricaoComplemento">Complemento:</label>
				<input class="campoTexto" type="text" id="descricaoComplemento" name="descricaoComplemento"   maxlength="25" size="30" value="${chamadoVO.descricaoComplemento}"  onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"><br />
				<label class="rotulo" id="rotuloComplemento" for="nomeImovel">Nome Fantasia:</label>
				<input class="campoTexto" type="text" id="nomeImovel" name="nomeImovel"  maxlength="30" size="30" value="${chamadoVO.nomeImovel}"  onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"><br />
				<label class="rotulo" id="rotuloComplemento" for="matriculaImovel">Matr�cula:</label>
				<input class="campoTexto" type="text" id="matriculaImovel" name="matriculaImovel"  maxlength="9" size="9" onkeypress="return formatarCampoInteiro(event)" value="${chamadoVO.matriculaImovel}"><br />
			
				<label class="rotulo" for="habilitado">Imovel � Condom�nio:</label>
				<input class="campoRadio" type="radio" name="condominioImovel" id="condominioImovel" value="true" <c:if test="${condominioImovel eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="condominio">Sim</label>
				<input class="campoRadio" type="radio" name="condominioImovel" id="condominioImovel" value="false" <c:if test="${condominioImovel eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="condominio">N�o</label>
				<input class="campoRadio" type="radio" name="condominioImovel" id="condominioImovel" value="" <c:if test="${condominioImovel eq null}">checked</c:if>>
				<label class="rotuloRadio" for="condominio">Todos</label>
				
			</fieldset>
			</div>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="fieldsChamados colunaDir">
			<legend>Filtro Contrato</legend>
			<div class="pesquisarImovelFundo">
			<a class="linkPesquisaAvancada" id="pesquisaContrato" href="#" rel="toggle['mostrarFormContrato']" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Contrato <img src="imagens/setaBaixo.png" border="0"></a><br />
			<fieldset id="mostrarFormContrato" class="fieldsChamados">
				<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:</label>
				<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato"  maxlength="9" size="8" onkeypress="return formatarCampoInteiro(event);" value="${chamadoVO.numeroContrato}"><br />
			</fieldset>
			</div>
		</fieldset>


	</fieldset>
	
	<fieldset class="conteinerBotoesPesquisarDirFixo" style="margin-top: 10px;">
		 <fieldset class="relatorioAnalitico conteinerBotoesPesquisarDirFixo" >
			<input name="Button" class="bottonRightCol2 botaoTipoRelatorio" value="Anal�tico" type="button" onclick="exibirExportarRelatorioGenerico(this.value)">
		 </fieldset>
		 <fieldset class="relatorioOutro conteinerBotoesPesquisarDirFixo">
			<input name="Button" class="bottonRightCol2 botaoTipoRelatorio" value="Quantitativo" type="button" onclick="exibirExportarRelatorioGenerico(this.value)">	
			<input name="Button" class="bottonRightCol2 botaoTipoRelatorio" value="ARSAL" type="button" onclick="exibirExportarRelatorioGenerico(this.value)">	
		 </fieldset>
		 <fieldset class="relatorioResumo conteinerBotoesPesquisarDirFixo">
			<input name="Button" class="bottonRightCol2 botaoTipoRelatorio" value="Resumo" type="button" onclick="exibirExportarRelatorioGenerico(this.value)">	
		 </fieldset>
		<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>
	
</fieldset>

</form:form>