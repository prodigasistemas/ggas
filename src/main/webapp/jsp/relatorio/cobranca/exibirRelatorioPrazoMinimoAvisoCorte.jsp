<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script language="javascript">

	$(document).ready(function() {
		// Dialog
		$("#mesAno").inputmask("99/9999",{placeholder:"_"});
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400});
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function gerar(){
		exibirIndicador();
		$(".mensagens").hide();
		submeter('relatorioCobrancaForm', 'gerarRelatorioPrazoMinimoAvisoCorte');
	}

	function limparCampos(){		
		$(":text").val("");
		$("select").val("-1");
		$("#ativo").attr("checked","checked");
	}

	function init(){
		exibirRelatorio();
		configurarCamposRadio(document.relatorioCobrancaForm.exibirFiltros);	
		configurarCamposRadio(document.relatorioCobrancaForm.formatoImpressao);		
	}
	addLoadEvent(init);

	//Mant�m as op��es do relat�rio definidas
	function configurarCamposRadio(campo){ // exibir filtros
		if(campo.name == "exibirFiltros"){
			var valor = $(campo).val();
			if (campo.value == "true"){
				$("#exibirFiltrosCheck").attr("checked","checked");
			} else {
				$("#exibirFiltrosCheck").removeAttr("checked");
			}
			
		} else { // formatoimpressao
			var radio = $("input[name=formatoImpressaoRadio]");
			for(i = 0; i < radio.length; i++){
				if(radio[i].value==campo.value){
					radio[i].checked = true;
					break;
				}
			}
		}		
	}
	
	function exibirRelatorio(){
		<c:if test="${exibirRelatorio eq true}">
			limparCampos();
			var iframe = document.getElementById("download");
			iframe.src = 'exibirRelatorioPrazoMinimoAvisoCorte';
		</c:if>
	}
	
	
</script>

<h1 class="tituloInterno">Prazo M�nimo de Anteced�ncia para Envio do Aviso de Corte<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para gerar um relat�rio, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</br></p>

<form:form method="post" action="gerarRelatorioPrazoMinimoAvisoCorte" id="relatorioCobrancaForm" name="relatorioCobrancaForm">

	<input name="acao" type="hidden" id="acao" value="gerarRelatorioPrazoMinimoAvisoCorte"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">

	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaRotaCol1" class="coluna">
			<label class="rotulo" for="dataDevolucaoInicial"><span class="campoObrigatorioSimbolo">* </span>Per�odo de Refer�ncia:</label>
			<input class="campoData campoHorizontal" type="text" id="mesAno" name="mesAno" maxlength="7">	
		</fieldset>
		
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campo obrigat�rio.</p>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="gerarRelatorioPrazoMinimoAvisoCorte">
	   			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
	   		</vacess:vacess>		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCampos();">		
		</fieldset>
	</fieldset>
	
	<iframe id="download" src ="" width="0" height="0"></iframe>
</form:form>
