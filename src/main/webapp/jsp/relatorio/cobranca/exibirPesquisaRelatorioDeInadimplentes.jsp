<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%-- <jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" /> --%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/dhtmlxcombo.css">


<script type="text/javascript">

	$(document).ready(function(){			
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});	 	

		
		//Define a apar�ncia dos r�tulos da pesquisa de cliente e im�vel como desabilitados
		$("#pesquisarImovel .rotulo").addClass("rotuloDesabilitado");
		$("#pesquisarCliente .rotulo").addClass("rotuloDesabilitado");

		//Habilita a pesquisa de cliente e im�vel.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);

		//Exibe a janela de Op��es de exporta��o 			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
		
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}

	function init(){
		
		exibirRelatorio();		

		var idCliente = "${relatorioCobrancaVO.idCliente}";
		var idImovel = "${relatorioCobrancaVO.idImovel}";
		if ((idCliente != undefined && idCliente > 0)) {
			ativarBotaoPontoConsumo();
			document.getElementById('idSegmento').disabled = true;
			document.getElementById('idRamoAtividade').disabled = true;
		} else if (idImovel != undefined && idImovel > 0) {
			document.getElementById("botaoPontoConsumo").disabled = false;
			document.getElementById('idSegmento').disabled = true;
			document.getElementById('idRamoAtividade').disabled = true;
		}

		<c:choose>
			<c:when test="${relatorioCobrancaVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${relatorioCobrancaVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>
		</c:choose>
		
		<c:if test="${relatorioCobrancaVO.idImovel eq null}">
			$("input[name=indicadorCondominioImovelTexto]").removeAttr("checked");
		</c:if>
		
	}
	addLoadEvent(init);
	
	function limparPesquisa(){
		limparFormularioDadosCliente();
		limparFormularioDadosImovel()
		$("input[name=indicadorCondominioImovelTexto]").removeAttr("checked");
		$("#dataBase").val("");
// 		moveAllOptionsEspecial(document.relatorioCobrancaForm.idsSegmentosAssociados, document.relatorioCobrancaForm.idsSegmentosDisponiveis, true);
// 		document.relatorioCobrancaForm.formatoImpressao[0].checked = true;
// 		document.relatorioCobrancaForm.exibirFiltros[0].checked = true;

		$("#idSituacaoPontoConsumo").val("-1");
		$("#diasAtraso").val("");
		document.relatorioCobrancaForm.diasVencido[4].checked = true;
		document.relatorioCobrancaForm.tipoRelatorio[0].checked = true;
		document.relatorioCobrancaForm.faturaRevisao[1].checked = true;
		$(".campoList").val('');
	}

	function limparFormularioDadosImovel() {
		$("#pesquisarImovel :hidden, #pesquisarImovel :text").val("");
		$("#pesquisarImovelFundo :radio").removeAttr("checked");
		$("input[name=indicadorCondominioImovelTexto]").removeAttr("checked");
	}

	function pesquisarPontoConsumo(){
		document.relatorioCobrancaForm.pesquisaPontoConsumo.value = "true";
		submeter("relatorioCobrancaForm", "exibirPesquisaRelatorioDeInadimplentes");
	}
	
	function gerar(){
		$(".mensagens").hide();
		$("#botaoGerar, #botaoPontoConsumo, #botaoLimpar").attr('disabled','disabled');

// 		selectAllOptions(document.forms[0].idsSegmentosAssociados);
// 		selectAllOptions(document.forms[0].idsSegmentosDisponiveis);
		submeter("relatorioCobrancaForm", "gerarRelatorioDeInadimplentes");
	}

	function habilitaCliente(){
		$("#pesquisarImovel .rotulo").addClass("rotuloDesabilitado");
		$("pesquisarCliente .rotulo").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#pesquisarImovel .rotulo").removeClass("rotuloDesabilitado");
		$("pesquisarCliente .rotulo").addClass("rotuloDesabilitado")
	};

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			
			limparFormularioDadosCliente();
			limparFormularioDadosImovel();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			
			limparFormularioDadosCliente();
			limparFormularioDadosImovel();
		}	
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
	    } else {
	   		idImovel.value = "";
	    	matriculaImovel.value = "";
	    	nomeFantasia.value = "";
			numeroImovel.value = "";
	          	cidadeImovel.value = "";
	          	indicadorCondominio.value = "";
	   	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}	
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function exibirRelatorio(){
		<c:if test="${exibirRelatorio eq true}">
			var iframe = document.getElementById("download");
			iframe.src = 'exibirRelatorioDeInadimplentes';
		</c:if>	
	}
	
	
</script>
<h1 class="tituloInterno">Inadimplentes<a class="linkHelp" href="<help:help>/gerandoorelatriodeinadimplentes.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos necess�rios abaixo e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" action="/exibirPesquisaRelatorioDeInadimplentes" name="relatorioCobrancaForm" id="relatorioCobrancaForm">
	<input type="hidden" name="pesquisaPontoConsumo" value="<c:out value="${relatorioCobrancaVO.pesquisaPontoConsumo}"/>"/>
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${relatorioCobrancaVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${relatorioCobrancaVO.idCliente}"/>
				
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCompletoCliente}"/>
				
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoFormatado}"/>
				
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoFormatadoCliente}"/>
						
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${relatorioCobrancaVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${relatorioCobrancaVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.idImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirIndicador(); exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.idImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto3" name="indicadorCondominioImovelTexto" value="" disabled="disabled" <c:if test="${empty imovel.condominio}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto3">Todos</label>
			</div>
		</fieldset>

		<fieldset class="conteinerBloco" >
		<fieldset class="coluna">
				<label class="rotulo rotuloVertical">Segmento:</label><br /> <select
					class="campoList campoVertical" name="idsSegmentosAssociados" multiple="multiple">
					<c:forEach items="${listaSegmento}" var="segmento">
						<option title="<c:out value="${segmento.descricao}"/>"
							value="${segmento.chavePrimaria}" 
							<c:forEach items="${relatorioCobrancaVO.idsSegmentosAssociados}" var="idsSegmentosAssociados">
								${segmento.chavePrimaria eq idsSegmentosAssociados ? 'selected' : ''}
							</c:forEach> >
							<c:out value="${segmento.descricao}" />
						</option>
					</c:forEach>
		</select>
		</fieldset>
		<fieldset class="colunaFinal">
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Data de Refer�ncia:</label>
			<input class="campoData campoHorizontal" type="text" id="dataBase" name="dataBase" maxlength="10" value="${relatorioCobrancaVO.dataBase}" />
				
			<label class="rotulo">Dias de Atraso:</label>
			<input class="campoTexto campoHorizontal" type="text" maxlength="4" size="4" id="diasAtraso" name="diasAtraso" onkeypress="return formatarCampoInteiro(event);" <c:if test="${relatorioCobrancaVO.diasAtraso > 0}">value="${relatorioCobrancaVO.diasAtraso}"</c:if>>
			
			<label class="rotulo">Dias de vencimento:</label>
			<input class="campoRadio" type="radio" name="diasVencido" id="30Dias" value="30" ${relatorioCobrancaVO.diasVencido eq '30' ? 'checked' : ''} />
			<label class="rotuloRadio">30 Dias</label>	
		
			<input class="campoRadio" type="radio" name="diasVencido" id="60Dias" value="60" ${relatorioCobrancaVO.diasVencido eq '60' ? 'checked' : ''} />
			<label class="rotuloRadio">60 Dias</label>	
				
			<input class="campoRadio" type="radio" name="diasVencido" id="90Dias" value="90" ${relatorioCobrancaVO.diasVencido eq '90' ? 'checked' : ''} />
			<label class="rotuloRadio">90 Dias</label>
			
			<input class="campoRadio" type="radio" name="diasVencido" id="mais90Dias" value="mais90Dias" ${relatorioCobrancaVO.diasVencido eq 'mais90Dias' ? 'checked' : ''} />
			<label class="rotuloRadio">Mais de 90 Dias</label>	
			
			<input class="campoRadio" type="radio" name="diasVencido" id="todos" value="todos" ${relatorioCobrancaVO.diasVencido eq 'todos' ? 'checked' : ''} />
			<label class="rotuloRadio">Todos</label>			
		
		
			<label class="rotulo rotuloVertical">Situa��o:</label><br /> 
			<select name="idSituacaoPontoConsumo" id="idSituacaoPontoConsumo" class="campoSelect" >
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${listaSituacaoPontoConsumo}" var="situacaoPontoConsumo">
						<option value="<c:out value="${situacaoPontoConsumo.chavePrimaria}"/>" <c:if test="${relatorioCobrancaVO.idSituacaoPontoConsumo == situacaoPontoConsumo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${situacaoPontoConsumo.descricao}"/>
						</option>
					</c:forEach>
				</select>
		
		
			<label class="rotulo" for="tipoExibicaoRadio">  Modelo de Relat�rio:</label>
			<input class="campoRadio" type="radio" name="tipoRelatorio" id="analitico" value="analitico" checked="checked" 
				<c:if test="${relatorioCobrancaVO.tipoRelatorio eq 'analitico'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="analitico">Anal�tico</label>		
						
			<input class="campoRadio" type="radio" name="tipoRelatorio" id="sintetico" value="sintetico" 
				<c:if test="${relatorioCobrancaVO.tipoRelatorio eq 'sintetico'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="sintetico" style="margin-right: 0; padding-right: 0">Sint�tico</label>		
						
			<label class="rotulo">Faturas em Revis�o:</label>
		    <input class="campoRadio" type="radio" name="faturaRevisao" id="faturaRevisaoSim" value="true" <c:if test="${relatorioCobrancaVO.faturaRevisao == 'true'}">checked</c:if>><label class="rotuloRadio">Sim</label>
		   	<input class="campoRadio" type="radio" name="faturaRevisao" id="faturaRevisaoNao" value="false" <c:if test="${relatorioCobrancaVO.faturaRevisao == 'false'}">checked</c:if>><label class="rotuloRadio">N�o</label>
			
		</fieldset>

		</fieldset>

		<fieldset class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="exibirPesquisaRelatorioDeInadimplentes">
					<input class="bottonRightCol2 botaoPontosDeConsumo" type="button"
						id="botaoPontoConsumo" value="Pontos de Consumo"
						onclick="pesquisarPontoConsumo();">
				</vacess:vacess>
				<vacess:vacess param="gerarRelatorioDeInadimplentes">
					<input class="bottonRightCol2" type="button" id="botaoGerar"
						value="Gerar" onclick="exibirExportarRelatorio();">
				</vacess:vacess>
				<input class="bottonRightCol2 bottonRightColUltimo" id="botaoLimpar"
					value="Limpar" type="button" onclick="limparPesquisa();">
			</fieldset>
		</fieldset>

		<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<p class="orientacaoInicial">Selecione um ou mais Pontos de Consumo na lista abaixo e clique em Gerar Relat�rio</p>
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" id="pontoConsumo" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirPesquisaRelatorioDeInadimplentes">
	    	
	    	<display:column style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="idsPontoConsumo" value="${pontoConsumo.chavePrimaria}">
	     	</display:column>
	    			    	
	    	<display:column sortable="true" title="Ponto de Consumo" sortProperty="descricao" style="width: 370px">
	    		<c:out value="${pontoConsumo.descricao}"/>			    		
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Im�vel" sortProperty="imovel.nome" style="width: 370px">
	    		<c:out value="${pontoConsumo.imovel.nome}"/>			    	
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Segmento" sortProperty="segmento.descricao">
	    		<c:out value="${pontoConsumo.segmento.descricao}"/>			    	
	    	</display:column>
	    						    	
		</display:table>
	</c:if>
	
	<iframe id="download" src ="" width="0" height="0"></iframe>
	
</form:form>
