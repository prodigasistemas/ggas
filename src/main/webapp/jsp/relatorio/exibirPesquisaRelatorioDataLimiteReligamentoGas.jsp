<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div id="msg"></div>

<h1 class="tituloInterno">Relat�rio Data Limite para o Religamento do G�s</h1>
<p class="orientacaoInicial">
	Informe o filtro e em seguida clique em <span
		class="destaqueOrientacaoInicial"> Gerar.</span>
</p>

<script type="text/javascript">
	
	$(document).ready(function() {
		$(".campoData").datepicker({
					changeYear: true,
					yearRange: "c-20:c",
					showOn: 'button',
					buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
					buttonImageOnly: true,
					buttonText: 'Exibir Calend�rio', 
					dateFormat: 'dd/mm/yy'
				});

		$("#exportarRelatorio").dialog({
			autoOpen : false,
			width : 245,
			modal : true,
			minHeight : 100,
			maxHeight : 400,
			resizable : false
		});
	});
	
	
	function exibirExportarRelatorio() {
		
		var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
		"Erro: </strong>O(s) campo(s) Data In�cio e Data Final s�o de preenchimento obrigat�rio.</p></div>";
		
	
		if( $("#dataInicio").val() == "" ){
			$("#dataInicio").css('border', '1px solid rgb(255, 143, 143)');
			$("#dataInicio").css('background-color', 'rgb(255, 239, 239)');
			
			
			if( $("#dataFinal").val() == "" ){
				$("#dataFinal").css('border', '1px solid rgb(255, 143, 143)');
				$("#dataFinal").css('background-color', 'rgb(255, 239, 239)');
				
			}
			
			$("#msg").html( htmlErro );
			
		}else if( $("#dataFinal").val() != "" ){
			$("#msg").html( "" );
			exibirJDialog("#exportarRelatorio");
		}
	}

	function gerar() {
		
		var formatoImpressao = document.forms[0].formatoImpressao.value;
		var exibirFiltros = document.forms[0].exibirFiltros.value;
		var dataInicio = document.getElementById("dataInicio").value;
		var dataFinal = document.getElementById("dataFinal").value;
		console.log(dataFinal);
		
		submeter("gerarRelatorioDataLimiteReligamentoGas","gerarRelatorioDataLimiteReligamentoGas?tipoRelatorio="+ formatoImpressao + "&dataInicio =" + dataInicio + "&dataFinal" + dataFinal +
						+ "&exibirFiltros =" + exibirFiltros);
	}
	
	
	
</script>



<form:form action="gerarRelatorioDataLimiteReligamentoGas"
	id="gerarRelatorioDataLimiteReligamentoGas"
	name="gerarRelatorioDataLimiteReligamentoGas" method="post"
	modelAttribute="gerarRelatorioDataLimiteReligamentoGas">

	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset style="padding-bottom: 20px">
			<fieldset class="colunaServicoAutorizacao">

			<label class="rotulo" style="margin-left: 10px;" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" >Data In�cio: <span class="campoObrigatorioSimbolo">* </span></label>
			<input class="campo2Linhas campoHorizontal campoData" type="text" id="dataInicio" name="dataInicio" maxlength="10">
			
			<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">Data final <span class="campoObrigatorioSimbolo">* </span></label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFinal" name="dataFinal" maxlength="10"> <br />

			</fieldset>
			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
		</fieldset>
	</fieldset>


	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="Gerar"
			value="Gerar" type="button" onclick="exibirExportarRelatorio();">
		<input name="Button" class="bottonRightCol bottonRightColUltimo"
			value="Limpar" type="button" onclick="limparFormulario();">
	</fieldset>

</form:form>
