<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<h1 class="tituloInterno">Relat�rios de Previs�o e Capta��o <a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos necess�rios abaixo e clique em<span class="destaqueOrientacaoInicial"> Gerar</span></p>
<script language="javascript">

$(document).ready(function(){
	$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 170, maxHeight: 170, resizable: false});	
		
});

function gerar(){	
	$(".mensagens").hide();
	var formatoImpressao = document.forms[0].formatoImpressao.value
	var exibirFiltros = document.forms[0].exibirFiltros.value;
	submeter("chamadoForm","gerarRelatorioPrevisaoCaptacao?tipoRelatorio="+formatoImpressao+"&exibirFiltros ="+exibirFiltros);
	
}
//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
function exibirExportarRelatorio(){
	exibirJDialog("#exportarRelatorio");
}

function limparFormulario() {
	submeter("chamadoForm","exibirPesquisaRelatorioPrevisaoCaptacao");	
}
function limparCampoCpfCnpj(obj){
	if(obj.value.length > 14 && obj.value.length < 18 ){
		obj.value ="";
	}else{
		if(obj.value.length < 14){
			obj.value ="";
		}
	}
	
}


</script>

<form:form action="gerarRelatorioChamado" method="POST" id="chamadoForm" name="chamadoForm" modelAttribute="RelatorioPrevisaoCaptacaoVO" >

<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>

<fieldset class="containerPesquisarIncluirAlterar">
	<fieldset id="conteinerRelatorioFaturamento" class="conteinerPesquisarIncluirAlterar">
			<fieldset id="pesquisarImovel" class="colunaDir" style="float:none;">
			<label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Ano Refer�ncia:</label>
					<select id="anoReferencia" class="campoSelect" name="anoReferencia">
						<c:forEach items="${listaAnoReferencia}" var="anoReferencia">
						<option value="<c:out value="${anoReferencia}"/>" >
							<c:out value="${anoReferencia}"/>
						</option>
						</c:forEach>
					</select><br/>	
			<label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Segmento:</label>
					<select id="segmento" class="campoSelect" name="segmento">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaSegmento}" var="segmento">
						<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${relatorioControleAESVO.segmento == segmento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${segmento.descricao}"/>
						</option>
						</c:forEach>
					</select><br/>
		</fieldset>
		
		
	</fieldset>
	
	<fieldset id="pesquisarCliente" class="colunaDir">
		
	</fieldset>
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio()">	
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>
	
</fieldset>

</form:form>