<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">

	$(document).ready(function(){
		
		//Define a apar�ncia dos r�tulos da pesquisa de cliente e im�vel como desabilitados
		$("#pesquisarImovel .rotulo, #pesquisarCliente .rotulo").addClass("rotuloDesabilitado");

		// Exibe a janela de Op��es de exporta��o 			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
		
		$('#analitico, #sintetico').on('change', function() {
			
			if ($("#analitico").attr('checked') === 'checked') {
				$("#tipoExibicao").val("analitico");
			}else{
				$("#tipoExibicao").val("sintetico");
			}
			
		});
		
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}

	function limparFormularioDadosImovel() {
		$("#pesquisarImovel :hidden, #pesquisarImovel :text").val("");
		$("#pesquisarImovelFundo :radio").removeAttr("checked");
	}
	
	function limparCamposPesquisa() {
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
		habilitarBotaoGerarRelatorio();
		
		$("#idSegmento").removeAttr("disabled");
		$("#idSegmento,#idRamoAtividade,#ano,#mes").val("-1");		
		$(":checkbox,:radio").removeAttr("checked");
	}

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente() {
		$("#pesquisarImovel .rotulo").addClass("rotuloDesabilitado");
		$("#pesquisarCliente .rotulo").removeClass("rotuloDesabilitado");
	};
	function habilitaImovel() {
		$("#pesquisarImovel .rotulo").removeClass("rotuloDesabilitado");
		$("#pesquisarCliente .rotulo").addClass("rotuloDesabilitado");
	};

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function desabilitarPesquisaOposta(elem) {
		document.getElementById('idSegmento').disabled = false;
		var selecao = getCheckedValue(elem);
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
		if (selecao == "indicadorPesquisaImovel") {
			pesquisarImovel(true);
			pesquisarCliente(false);
			habilitaImovel();
		} else {
			pesquisarImovel(false);
			pesquisarCliente(true);
			habilitaCliente();
		}
	}

	function pesquisarImovel(valor) {
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor) {
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}

	function selecionarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");

		if (idSelecionado != '') {
			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {
										idImovel.value = imovel["chavePrimaria"];
										matriculaImovel.value = imovel["chavePrimaria"];
										nomeFantasia.value = imovel["nomeFantasia"];
										numeroImovel.value = imovel["numeroImovel"];
										cidadeImovel.value = imovel["cidadeImovel"];
										indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
									}
								},
								async : false
							}

					);
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
		}

		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if (indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
		ativarBotaoPontoConsumo();
	}

	function ativarBotaoPontoConsumo() {
		document.getElementById("botaoPontoConsumo").disabled = false;
	}

	function init() {
		
		$("#analitico").attr("checked", true);

		var idCliente = "${relatorioSubMenuMedicaoVO.idCliente}";
		var idImovel = "${relatorioSubMenuMedicaoVO.idImovel}";
		if ((idCliente != undefined && idCliente > 0)) {
			ativarBotaoPontoConsumo();
			document.getElementById('idSegmento').disabled = true;
			document.getElementById('idRamoAtividade').disabled = true;
		} else if (idImovel != undefined && idImovel > 0) {
			document.getElementById("botaoPontoConsumo").disabled = false;
			document.getElementById('idSegmento').disabled = true;
			document.getElementById('idRamoAtividade').disabled = true;
		}

		habilitarBotaoGerarRelatorio();
		
		<c:choose>
			<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>
		</c:choose>

	}
	addLoadEvent(init);

	function pesquisarPontoConsumo() {
		submeter(
			'relatorioSubMenuMedicaoForm','pesquisarPontoConsumoRelatorioResumoVolume'
		);
	}

	function gerar() {
		$(".mensagens").hide();
		submeter('relatorioSubMenuMedicaoForm','gerarRelatorioResumoVolume');
	}

	function carregarRamosAtividadePontoConsumo(elem) {
		var idSegmentoPontoConsumo = elem.value;
		var selectRamoAtividade = document.getElementById("idRamoAtividade");

		selectRamoAtividade.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;

		if (idSegmentoPontoConsumo != '-1') {
			AjaxService.listarRamoAtividadePorSegmento(idSegmentoPontoConsumo,{
				callback : function(ramosAtividade) {
					for (ramo in ramosAtividade){
	               		for (key in ramosAtividade[ramo]){
		                	var novaOpcao = new Option(ramosAtividade[ramo][key], key);
							selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
	               		}
					}
					selectRamoAtividade.disabled = false;
					$("#idRamoAtividade").removeClass("campoDesabilitado");
				},
				async : false
			});
		} else {
			selectRamoAtividade.disabled = true;
			$("#idRamoAtividade").addClass("campoDesabilitado");
		}
		verificaLarguraSelect();
	}

	function habilitarBotaoGerarRelatorio() {
		var mes = $("#mes").val();
		var ano = $("#ano").val();
		if (mes != -1 && mes != undefined && ano != -1 && ano != undefined) {
			$("input#botaoGerar").removeAttr("disabled");
		} else {
			$("input#botaoGerar").attr("disabled", "disabled");
		}
	}

	function habilitarInclusao() {
		var chavesPontoConsumo = document
				.getElementsByName('chavesPontoConsumo');
		var count = 0;

		if (chavesPontoConsumo != undefined && chavesPontoConsumo.length > 0) {
			for ( var i = 0; i < chavesPontoConsumo.length; i++) {
				if (chavesPontoConsumo[i].checked) {
					count = count + 1;
				}
			}
		}
	}
	
</script>

<h1 class="tituloInterno">Resumo de Volume<a class="linkHelp" href="<help:help>/gerandoorelatrioderesumodevolume.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" styleId="formRelatorio" id="relatorioSubMenuMedicaoForm" name="relatorioSubMenuMedicaoForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" >
	 	<jsp:param name="exibeAnaliticoSintetico" value="true"/>
	</jsp:include>
	
	<fieldset id="conteinerRelatorioResumoVolume" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPontoConsumo"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirIndicador(); exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<label class="rotulo campoObrigatorio" for="mes"><span class="campoObrigatorioSimbolo">* </span>M�s:</label>			
			<select name="mes" id="mes" class="campoSelect campoHorizontal" onchange="habilitarBotaoGerarRelatorio();">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMeses}" var="mes">
					<option value="<c:out value="${mes.key}"/>" <c:if test="${relatorioSubMenuMedicaoVO.mes == mes.key}">selected="selected"</c:if>>
						<c:out value="${mes.value}"/>
					</option>		
		    	</c:forEach>
			</select>
			
			<label class="rotulo campoObrigatorio rotuloHorizontal" for="ano"><span class="campoObrigatorioSimbolo">* </span>Ano:</label>			
			<select name="ano" id="ano" class="campoSelect campoHorizontal" onchange="habilitarBotaoGerarRelatorio();">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaAnos}" var="ano">
					<option value="<c:out value="${ano}"/>" <c:if test="${relatorioSubMenuMedicaoVO.ano == ano}">selected="selected"</c:if>>
						<c:out value="${ano}"/>
					</option>		
		    	</c:forEach>
			</select>	
		
			<label class="rotulo rotuloHorizontal" for="idSegmento">Segmento:</label>
			<select name="idSegmento" id="idSegmento" class="campoSelect campoHorizontal" onchange="carregarRamosAtividadePontoConsumo(this);">
		    	<option value="-1">Selecione</option>				
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${relatorioSubMenuMedicaoVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>
				</c:forEach>
			</select>
			
			<label class="rotulo rotuloHorizontal" for="idRamoAtividade">Ramo de Atividade:</label>
			<select name="idRamoAtividade" id="idRamoAtividade" class="campoSelect" <c:if test="${empty listaRamoAtividade}">disabled="disabled""</c:if>>
		    	<option value="-1">Selecione</option>				
				<c:forEach items="${listaRamoAtividade}" var="ramo">
					<option value="<c:out value="${ramo.chavePrimaria}"/>" <c:if test="${relatorioSubMenuMedicaoVO.idRamoAtividade == ramo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${ramo.descricao}"/>
					</option>
				</c:forEach>
			</select>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();" disabled="disabled">
			<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>		
	
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadora1" />
		<display:table class="dataTableGGAS" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoRelatorioResumoVolume">
	        <display:column style="text-align: center; width: 25px">
	        	<input type="checkBox" name="chavesPontoConsumo" id="chavePontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}" onclick="habilitarInclusao();"/>
	        </display:column>
	        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: left; padding-left: 10px">
	            <c:out value="${pontoConsumo.descricao}"/>
	        </display:column>
	        <display:column title="Segmento" style="width: 130px">
	            <c:out value="${pontoConsumo.segmento.descricao}"/>
	        </display:column>
	        <display:column title="Situa��o" style="width: 160px">
            	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
	        </display:column>
	    </display:table>
	</c:if>
 
	<fieldset class="conteinerBotoes">
		<input id="botaoGerar" name="button" class="bottonRightCol2 botaoGrande1" value="Gerar" type="button" onclick="exibirExportarRelatorio();" disabled="disabled">
	</fieldset>	
	
</form:form> 
