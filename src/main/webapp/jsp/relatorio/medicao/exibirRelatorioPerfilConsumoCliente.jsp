<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script type="text/javascript">

	$(document).ready(function(){
		//Datepicker
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio'});
		
		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		// Dialog
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400});

		//Verifica se o Ano foi selecionado e habilita o bot�o Gerar 
		habilitarGerar();
		$("#ano").bind('keyup blur change', function(){
			 habilitarGerar();
		});
		
	});

	//Verifica se o Ano foi selecionado e habilita o bot�o Gerar 
	function habilitarGerar(){
		if($("#ano").val() != -1){
			$("#botaoGerar").removeAttr("disabled");
		} else {
			$("#botaoGerar").attr("disabled","disabled");
		}
	}	

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}

	function limparPontoConsumo() {
		submeter('0', 'exibirRelatorioPerfilConsumoCliente');
	}

	function limparCamposPesquisa(){
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();

		document.getElementById('idSegmento').value = "-1";
		document.getElementById('idModalidadeMedicao').value = "-1";
		document.getElementById('numeroContrato').value = "";
		document.getElementById('dataAssinatura').value = "";
		document.getElementById('dataInicio').value = "";
		document.getElementById('dataEncerramento').value = "";
		document.getElementById('ano').value = "-1";

		habilitarGerar();
		
		<c:if test="${listaPontoConsumo ne null}">
			limparPontoConsumo();
		</c:if>
		document.getElementById("botaoPontoConsumo").disabled = true;
	}
	
	function limparFormularioDadosImovel(){
		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		
	}
	
	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};		

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparFormularioDadosCliente();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparFormularioDadosImovel();
			document.getElementById("botaoPontoConsumo").disabled = true;
			
		}	
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}	
		
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
		ativarBotaoPontoConsumo();
	}

	function ativarBotaoPontoConsumo() {
		document.getElementById("botaoPontoConsumo").disabled = false;
	}

	function gerarRelatorio(){
		submeter('0', 'gerarRelatorioPerfilConsumoCliente');
	}

	function init() {

		var idCliente = "${relatorioSubMenuMedicaoVO.idCliente}";
		var idImovel = "${relatorioSubMenuMedicaoVO.idImovel}";
		if ((idCliente != undefined && idCliente > 0)) {
			ativarBotaoPontoConsumo();
		} else if (idImovel != undefined && idImovel > 0) {
			document.getElementById("botaoPontoConsumo").disabled = false;
		}

		<c:choose>
		<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
			pesquisarImovel(false);
			habilitaCliente();
		</c:when>
		<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
			pesquisarCliente(false);
			habilitaImovel();
		</c:when>			
		<c:otherwise>
			pesquisarCliente(false);
			pesquisarImovel(false);
		</c:otherwise>				
	</c:choose>	
		
	}

	addLoadEvent(init);	

	function pesquisarPontoConsumo() {
		submeter('relatorioSubMenuMedicaoForm', 'pesquisarPontoConsumoRelatorioPerfilConsumoCliente');
	}

	function gerar(){

		gerarRelatorio();
		
	}
	
</script>

<h1 class="tituloInterno">Relat�rio de Perfil de Consumo<a class="linkHelp" href="<help:help>/gerandoorelatriodeperfildeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe o filtro e em seguida clique em <span class="destaqueOrientacaoInicial">Gerar Relat�rio</span>.</p>

<form:form method="post" styleId="formRelatorio" id="relatorioSubMenuMedicaoForm" name="relatorioSubMenuMedicaoForm">

	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
	
	<fieldset id="conteinerRelatorioPerfilConsumo" class="conteinerPesquisarIncluirAlterar">
		
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCompletoCliente}"/>
				
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoFormatado}"/>
				
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoFormatadoCliente}"/>
						
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		
		<fieldset class="conteinerBloco">
			<fieldset class="coluna">
				<label class="rotulo campoObrigatorio" for="ano"><span class="campoObrigatorioSimbolo">*</span>Ano:</label>			
				<select name="ano" id="ano" class="campoSelect campoHorizontal">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaAnos}" var="ano">
						<option value="<c:out value="${ano}"/>" <c:if test="${relatorioSubMenuMedicaoVO.ano == ano}">selected="selected"</c:if>>
							<c:out value="${ano}"/>
						</option>		
			    	</c:forEach>
				</select><br />
				<label class="rotulo" id="rotuloDataAssinatura" for="dataAssinatura">Data Assinatura:</label>
				<input class="campoData campoHorizontal" type="text" id="dataAssinatura" name="dataAssinatura" maxlength="10" value="${relatorioSubMenuMedicaoVO.dataAssinatura}"><br />
				
				<label class="rotulo" id="rotuloDataInicio" for="dataInicio">Data de In�cio do Consumo:</label>
				<input class="campoData campoHorizontal" type="text" id="dataInicio" name="dataInicio" maxlength="10" value="${relatorioSubMenuMedicaoVO.dataInicio}"><br />
				
				<label class="rotulo" id="rotuloDataEncerramento" for="dataEncerramento">Data Encerramento:</label>
				<input class="campoData campoHorizontal" type="text" id="dataEncerramento" name="dataEncerramento" maxlength="10" value="${relatorioSubMenuMedicaoVO.dataEncerramento}">
			</fieldset>
			
			<fieldset class="colunaFinal">
				<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:<!-- fix bug --></label>
				<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="9" size="8" value="${relatorioSubMenuMedicaoVO.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"><br />
				
				<label class="rotulo" id="rotuloIdModalidadeMedicao" for="idModalidadeMedicao">Modalidade de Medi��o:</label>
					<select name="idModalidadeMedicao" id="idModalidadeMedicao" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaModalidadeMedicao}" var="modalidade">
							<option value="<c:out value="${modalidade.codigo}"/>" <c:if test="${relatorioSubMenuMedicaoVO.idModalidadeMedicao == modalidade.codigo}">selected="selected"</c:if>>
								<c:out value="${modalidade.descricao}"/>
						</option>		
					</c:forEach>
					</select><br />
				
				<label class="rotulo" for="idSegmento">Segmento:</label>
				<select name="idSegmento" id="idSegmento" class="campoSelect" onchange="carregarRamosAtividadePontoConsumo(this);">
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${listaSegmento}" var="segmento">
						<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${relatorioSubMenuMedicaoVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${segmento.descricao}"/>
						</option>
					</c:forEach>
				</select>
			</fieldset>
				
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();" disabled="disabled">
				<input class="bottonRightCol bottonRightColUltimo" id="botaoLimpar" value="Limpar" type="button" onclick="limparCamposPesquisa();">
			</fieldset>
			
		</fieldset>
	</fieldset>
	
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadora1" />
		<fieldset class="conteinerBloco">
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoRelatorioPerfilConsumoCliente">
		        <display:column style="text-align: center; width: 25px">
		        	<input type="checkBox" name="chavesPontoConsumo" id="chavePontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}" onclick="habilitarInclusao();"/>
		        </display:column>
		        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: left; padding-left: 10px">
	            	<c:out value="${pontoConsumo.descricao}"/>
		        </display:column>
		        <display:column title="Segmento" style="width: 130px">
	            	<c:out value="${pontoConsumo.segmento.descricao}"/>
		        </display:column>
		        <display:column title="Situa��o" style="width: 160px">
		            <c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
		        </display:column>
		    </display:table>
		</fieldset>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<input id="botaoGerar" class="bottonRightCol2 botaoGrande1 botaoGerar" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
	</fieldset>
</form:form>
