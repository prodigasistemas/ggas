<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

	$(document).ready(function(){
		//Datepicker
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '/GGAS/imagens/calendario.gif', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
		$("#pesquisarImovel .rotulo, #pesquisarCliente .rotulo").addClass("rotuloDesabilitado");
		
		// Exibe a janela de Op��es de exporta��o 			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
		
		//Validar e checar intervalos de datas para liberar o bot�o Gerar.
		$(".campoData").bind('keypress blur focusin', function() {
			validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");
		});		
		validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");
		
		
	});
	
	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
		}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	
	function gerar(){		
		$(".mensagens").hide();
		$("#botaoGerar").attr('disabled','disabled');
		submeter('0', 'gerarRelatorioVolumesFaturados');
	}
	
	function pesquisarPontoConsumo() {
		$("#indicadorPesquisaPontos").val(true);
		submeter('relatorioSubMenuMedicaoForm', 'pesquisarPontoConsumoRelatorioVolumesFaturados');
	}
	
	function limparCamposPesquisa(){
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
		$("input[name=chavesPontoConsumo]").attr("checked",false);
		$("#idSegmento").val("-1");
		$(".campoData").val("");
		$("#botaoGerar").attr("disabled","disabled");
		$("input[name=indicadorCondominioImovelTexto]").removeAttr("checked");
	}
	
	function limparFormularioDadosImovel() {
		$("#pesquisarImovel :hidden, #pesquisarImovel :text").val("");
		$("#pesquisarImovelFundo :radio").removeAttr("checked");
	}
	
	function desabilitarPesquisaOposta(elem){
		limparFormularioDadosImovel()
		limparFormularioDadosCliente();
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
		} else {
			pesquisarImovel(false);
			pesquisarCliente(true);
		}	
	}
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function init() {

		<c:choose>
			<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>
		</c:choose>
	}
	addLoadEvent(init);
	
</script>

<h1 class="tituloInterno">Volumes Faturados<a class="linkHelp" href="<help:help>/gerandoorelatriodevolumesfaturados.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os filtros que ser�o utilizados e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" styleId="formRelatorio" id="relatorioSubMenuMedicaoForm" name="relatorioSubMenuMedicaoForm">

	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="pesquisarPontosConsumo" type="hidden" id="indicadorPesquisaPontos" value="">
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />
	
	<fieldset id="conteinerRelatorioVolumesFaturados" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset class="conteinerBloco" style="padding-bottom: 20px">
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Periodo de emiss�o:</label>
			<input class="campoData campoHorizontal" type="text" id="dataInicial" name="dataInicial" value="${relatorioSubMenuMedicaoVO.dataInicial}">
			<label class="rotuloEntreCampos">a</label>
			<input class="campoData campoHorizontal" type="text" id="dataFinal" name="dataFinal" value="${relatorioSubMenuMedicaoVO.dataFinal}">
			
			<label class="rotulo rotuloHorizontal" style="margin-left: 40px" for="idSegmento">Segmento:</label> 
			<select name="idSegmento" id="idSegmento" class="campoSelect campo2Linhas campoHorizontal">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>"
						<c:if test="${relatorioSubMenuMedicaoVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${segmento.descricao}" /></option>
				</c:forEach>
			</select>
		</fieldset>
		
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoFormatadoCliente}"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPesquisar"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>
		</fieldset>
	
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirIndicador(); exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();">
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>

	<c:if test="${listaPontoConsumo ne null && not empty listaPontoConsumo}">
		<hr class="linhaSeparadora1" />
		<p class="orientacaoInicial">Selecione um Ponto de Consumo na lista abaixo para gerar o relat�rio.</p>
		<display:table class="dataTableGGAS" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoFatura">
	        <display:column style="text-align: center; width: 25px">
	        	<input type="checkbox" name="chavesPontoConsumo" id="chavesPontoConsumo" value="${pontoConsumo.chavePrimaria}"/>
	        </display:column>
	        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: left; padding-left: 10px">
	          
	           <c:choose>
					<c:when test="${pontoConsumo.imovel.nome ne null}">
						<c:out value="${pontoConsumo.imovel.nome} - ${pontoConsumo.descricao}"/>
					</c:when>
					<c:otherwise>
						<c:out value="${pontoConsumo.descricao}"/>
					</c:otherwise>
				</c:choose>
	           
	        </display:column>
	        <display:column title="Segmento" style="width: 130px">
            	<c:out value="${pontoConsumo.segmento.descricao}"/>
	        </display:column>
	        <display:column title="Situa��o" style="width: 160px">
	            <c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
	        </display:column>
	    </display:table>
	</c:if>
		
	<fieldset class="conteinerBotoes">
		<vacess:vacess param="gerarRelatorioVolumesFaturados">
			<input name="button" id="botaoGerar" class="bottonRightCol2 botaoGrande1" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
		</vacess:vacess>
	</fieldset>		
</form:form>
