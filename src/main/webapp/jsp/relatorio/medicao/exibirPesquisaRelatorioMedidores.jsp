<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script type="text/javascript">
	
	$(document).ready(function() {
		$(".campoData").datepicker({
					changeYear: true,
					yearRange: "c-20:c",
					showOn: 'button',
					buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
					buttonImageOnly: true,
					buttonText: 'Exibir Calend�rio', 
					dateFormat: 'dd/mm/yy'
				});

		$("#exportarRelatorio").dialog({
			autoOpen : false,
			width : 245,
			modal : true,
			minHeight : 100,
			maxHeight : 400,
			resizable : false
		});
	});
	
	function exibirExportarRelatorio() {
	
// 		var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
// 		"Erro: </strong>O(s) campo(s) Data In�cio e Data Final s�o de preenchimento obrigat�rio.</p></div>";
		
	
// 		if( $("#dataInicio").val() == "" ){
// 			$("#dataInicio").css('border', '1px solid rgb(255, 143, 143)');
// 			$("#dataInicio").css('background-color', 'rgb(255, 239, 239)');
			
			
// 			if( $("#dataFinal").val() == "" ){
// 				$("#dataFinal").css('border', '1px solid rgb(255, 143, 143)');
// 				$("#dataFinal").css('background-color', 'rgb(255, 239, 239)');
				
// 			}
			
// 			$("#msg").html( htmlErro );
			
// 		}else if( $("#dataFinal").val() != "" ){
			
// 			$("#msg").html( "" );			
// 			exibirJDialog("#exportarRelatorio");
// 		}
		exibirJDialog("#exportarRelatorio");
	}

	function gerar() {
		
		var formatoImpressao = document.forms[0].formatoImpressao.value;
		var exibirFiltros = document.forms[0].exibirFiltros.value;
		var dataInicio = document.getElementById("dataInicio").value;
		var dataFinal = document.getElementById("dataFinal").value;		
		submeter("gerarRelatorioMedidores","gerarRelatorioMedidores?tipoRelatorio="+ formatoImpressao + "&dataInicio =" + dataInicio + "&dataFinal" + dataFinal +
						+ "&exibirFiltros =" + exibirFiltros);
	}
	
	function limparFormulario(){
		document.gerarRelatorioMedidores.numeroSerie.value = "";
		document.gerarRelatorioMedidores.modoUso.value = "";
		document.gerarRelatorioMedidores.tipoMedidor.value = "-1";
		document.gerarRelatorioMedidores.marcaMedidor.value = "-1";
		document.gerarRelatorioMedidores.modelo.value = "-1";
		document.gerarRelatorioMedidores.localArmazenagem.value = "-1";
		document.gerarRelatorioMedidores.modoUso[3].checked = true;
		document.forms[0].habilitado[0].checked = true;
		document.gerarRelatorioMedidores.dataInicio.value = "";
		document.gerarRelatorioMedidores.dataFinal.value = "";
		document.gerarRelatorioMedidores.anoFabricacao.value = "";
	}
	
	
</script>

<div id="msg"></div>


<h1 class="tituloInterno">Relat�rio Medidores</h1>
<p class="orientacaoInicial">
	Informe o(s) filtro(s) e em seguida clique em <span
		class="destaqueOrientacaoInicial"> Gerar.</span>
</p>



<form:form action="gerarRelatorioMedidores"
	id="gerarRelatorioMedidores"
	name="gerarRelatorioMedidores" method="post"
	modelAttribute="gerarRelatorioMedidores">

	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">

	<fieldset class="conteinerPesquisarIncluirAlterar">
			<fieldset id="pesquisaRelatorioMedidoresCol2" class="coluna">
				<label class="rotulo" id="rotuloMarca" style="width: 150px" for="marca">Marca:</label>
				<select name="marcaMedidor" id="marca" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaMarcaMedidor}" var="marca">
						<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${medidor.marcaMedidor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${marca.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />	
				
				<label class="rotulo" id="rotuloModelo" for="modelo" style="width: 150px">Modelo:</label>
				<select name="modelo" id="modeloMedidor" class="campoSelect">
					<option value="-1">Selecione</option> 
					<c:forEach items="${listaModeloMedidor}" var="modelo">
						<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${medidor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${modelo.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />
				<label class="rotulo" id="rotuloLocalArmazenagem" for="localArmazenagem" style="width: 150px">Local de Armazenagem:</label>
				<select name="localArmazenagem" id="localArmazenagem" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaLocalArmazenagem}" var="localArmazenagem">
					<option value="<c:out value="${localArmazenagem.chavePrimaria}"/>" <c:if test="${medidor.localArmazenagem.chavePrimaria == localArmazenagem.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${localArmazenagem.descricao}"/>
					</option>		
				    </c:forEach>
				</select>
				
				<label class="rotulo" id="rotuloAnoFabricacao" for="anoFabricacao" style="width: 150px" >Ano Fabrica��o:</label>
				<input class="campoTexto" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" name="anoFabricacao" id="anoFabricacao" maxlength="4" size="4" value="${medidor.anoFabricacao}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/><br />
				
				<label class="rotulo" for="habilitado" style="width: 150px">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="indicadorUso">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="indicadorUso">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Todos</label>
				
			</fieldset>			
			<fieldset class="pesquisaRelatorioMedidoresCol1" class="coluna">
			
				<label class="rotulo" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" style="width: 150px">Data Aquisi��o Inicial: <!--  <span class="campoObrigatorioSimbolo">* </span>--></label>
				<input class="campo2Linhas campoHorizontal campoData" type="text" id="dataInicio" name="dataInicio" maxlength="10">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">Data Aquisi��o Final <!--  <span class="campoObrigatorioSimbolo">* </span>--></label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFinal" name="dataFinal" maxlength="10"> <br />
				
				<label class="rotulo" id="rotuloNumeroSerie" for="numeroSerie" style="width: 150px" >N�mero de s�rie:</label>
				<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${medidor.numeroSerie}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/><br />
				
				<label class="rotulo" id="rotuloTipo" for="tipo" style="width: 150px">Tipo:</label>
				<select name="tipoMedidor" id="tipo" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoMedidor}" var="tipo">
						<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${medidor.tipoMedidor.chavePrimaria == tipo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipo.descricao}"/>
						</option>		
				    </c:forEach>
				</select>
				
				<label class="rotulo" id="modoUsoRotulo" for="modoUso" style="width: 150px">Perfil:</label>
				<input class="campoRadio" type="radio" name="modoUso" id="modoUso" value="${modoUsoNormal}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoNormal}">checked</c:if>>
				<label class="rotuloRadio" for="modoUso">Normal</label>
				<input class="campoRadio" type="radio" name="modoUso" id="modoUso" value="${modoUsoVirtual}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">checked</c:if>>
				<label class="rotuloRadio" for="modoUso">Virtual</label>
				<input class="campoRadio modoUso" type="radio" name="modoUso" id="modoUso" value="${modoUsoIndependente}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoIndependente}">checked</c:if>>
				<label class="rotuloRadio" for="modoUso">Independente</label>
				<input class="campoRadio modoUso" type="radio" name="modoUso" id="modoUso" value="null" <c:if test="${medidor.modoUso eq null || medidor.modoUso eq ''}">checked</c:if>>
				<label class="rotuloRadio" for="modoUso">Todos</label>
				
			</fieldset>
		

			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
	</fieldset>


	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="Gerar"
			value="Gerar" type="button" onclick="exibirExportarRelatorio();">
		<input name="Button" class="bottonRightCol bottonRightColUltimo"
			value="Limpar" type="button" onclick="limparFormulario();">
	</fieldset>

</form:form>
