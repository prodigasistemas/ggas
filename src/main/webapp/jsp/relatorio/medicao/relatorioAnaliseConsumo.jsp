<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script>

	$(document).ready(function() {
		$("#dataEmissaoInicial").inputmask("99/9999",{placeholder:"_"});
		$("#dataEmissaoFinal").inputmask("99/9999",{placeholder:"_"});
		// Dialog			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400});
		
		/*verifica se o campo M�s/Ano de dataEmissaoInicial e dataEmissaoFinal foram	
		preenchidos corretamente e habilita o bot�o Gerar*/

		$("#dataEmissaoInicial").bind('keypress blur focusin', function() {
			validarMesAnoLiberarBotao("dataEmissaoInicial", "dataEmissaoFinal","botaoGerar");
		});	

		$("#dataEmissaoFinal").bind('keypress blur focusin', function() {
			validarMesAnoLiberarBotao("dataEmissaoInicial", "dataEmissaoFinal","botaoGerar");
		});
		validarMesAnoLiberarBotao("dataEmissaoInicial", "dataEmissaoFinal","botaoGerar");
		
	});

	/*Verifica se o campo M�s/Ano de dataEmissaoInicial e dataEmissaoFinal foram
	preenchidos e habilita o bot�o Gerar*/
	function validarMesAnoLiberarBotao(idMesAnoInicial, idMesAnoFinal,idBotao){
		if($("#" + idMesAnoInicial).val() != "" && $("#" + idMesAnoInicial).val() != "__/____" && 
			/^(0[1-9]|1[012])[- /.](19|20)\d\d$/.test($("#" + idMesAnoInicial).val()) && 
			
			$("#" + idMesAnoFinal).val() != "" && $("#" + idMesAnoFinal).val() != "__/____" && 
					/^(0[1-9]|1[012])[- /.](19|20)\d\d$/.test($("#" + idMesAnoFinal).val()) ){

				$("#" + idBotao).removeAttr("disabled");
		} else {
			$("#" + idBotao).attr("disabled","disabled");
		}
	}
	

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function gerar() {
		
		if($("#dataEmissaoInicial").val() == "" || $("#dataEmissaoInicial").val() == "__/____" ||
				$("#dataEmissaoFinal").val() == "" || $("#dataEmissaoFinal").val() == "__/____"){
			alert("� preciso preencher o campo M�s/Ano de Refer�ncia");
		}else{
			$(".mensagens").hide();			
			submeter('relatorioSubMenuMedicaoForm', 'gerarRelatorioAnaliseConsumo');
			}
	}

	function limparCamposPesquisa() {
		limparFormularioDadosCliente();
		$("#dataEmissaoInicial").val("");
		$("#dataEmissaoFinal").val("");
		$("#botaoGerar").attr("disabled","disabled");
	}

</script>

<h1 class="tituloInterno">An�lise de Consumo</h1>
<p class="orientacaoInicial">Selecione um <span class="destaqueOrientacaoInicial">cliente</span>, um <span class="destaqueOrientacaoInicial">per�odo</span>, o <span class="destaqueOrientacaoInicial">formato de impress�o</span> e clique no bot�o <span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" styleId="formRelatorio" id="relatorioSubMenuMedicaoForm" name="relatorioSubMenuMedicaoForm">
	<fieldset id="pesquisarFatura" class="conteinerPesquisarIncluirAlterar">
		
		<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
		
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoFormatadoCliente}"/>				
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotoes"/>
			</jsp:include>
		</fieldset>
	
		<fieldset class="colunaEsq2" style="padding-top: 15px">
			<label class="rotulo campoObrigatorio" id="rotuloReferencia" for="dataEmissaoInicial"><span class="campoObrigatorioSimbolo">* </span>M�s/Ano de Refer�ncia:</label>
			<input class="campoTexto campoHorizontal" type="text" size="5" id="dataEmissaoInicial" name="dataEmissaoInicial" onkeypress="return formatarCampoInteiro(event,6);" value="${relatorioSubMenuMedicaoVO.dataEmissaoInicial}">
			<label class="rotuloEntreCampos" id="rotuloEntreCamposPeriodo" for="dataEmissaoFinal">�</label>
			<input class="campoTexto campoHorizontal" type="text" size="5" id="dataEmissaoFinal" name="dataEmissaoFinal" onkeypress="return formatarCampoInteiro(event,6);" value="${relatorioSubMenuMedicaoVO.dataEmissaoFinal}">
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="gerarRelatorioAnaliseConsumo">
				<input name="button" id="botaoGerar" class="bottonRightCol2" value="Gerar" type="button" disabled="disabled" onclick="exibirExportarRelatorio();">
			</vacess:vacess>
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
		
	</fieldset>
</form:form>
