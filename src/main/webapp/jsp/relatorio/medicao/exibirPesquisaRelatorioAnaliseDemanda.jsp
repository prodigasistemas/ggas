<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/dhtmlxcombo.css">


<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

		//Validar e checar intervalos de datas para liberar o bot�o Gerar.
		$(".campoData").bind('keypress blur focusin', function() {
			validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");
		});		
		validarDatasLiberarBotao("dataInicial","dataFinal","botaoGerar");
		
		$("#pesquisarImovel .rotulo, #pesquisarCliente .rotulo").addClass("rotuloDesabilitado");
		
		$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 200, maxHeight: 200, resizable: false});
		
	$('#exibirGraficoCheck').on('change', function() {
	
		if ($(this).attr('checked') === 'checked') {
			$("#exibirGrafico").val(true);
		} else {
			$("#exibirGrafico").val(false);
		}
	});

});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio() {
		exibirJDialog("#exportarRelatorio");
	}

	function gerar() {

		$(".mensagens").hide();

		$("#botaoGerar").attr('disabled', 'disabled');
		$("#botaoPontoConsumo").attr('disabled', 'disabled');
		$("#botaoLimpar").attr('disabled', 'disabled');

		selectAllOptions(document.forms[0].idsSegmentosAssociados);
		selectAllOptions(document.forms[0].idsSegmentosDisponiveis);

		selectAllOptions(document.forms[0].idsRamosAtividadesAssociados);
		selectAllOptions(document.forms[0].idsRamosAtividadesDisponiveis);

		submeter("relatorioSubMenuMedicaoForm", "gerarRelatorioAnaliseDemanda");

	}

	function limparPesquisa() {
		
		$("input[name=indicadorCondominioImovelTexto]").removeAttr("checked");
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
		$(".campoData").val("");
		$("#botaoGerar").attr('disabled', 'disabled');
		moveAllOptionsEspecial(
				document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados,
				document.relatorioSubMenuMedicaoForm.idsSegmentosDisponiveis,
				true);
		configurarRamosAtividades();
		document.relatorioSubMenuMedicaoForm.formatoImpressao[0].checked = true;
		document.relatorioSubMenuMedicaoForm.exibirFiltros[0].checked = true;
		document.relatorioSubMenuMedicaoForm.exibirGrafico[0].checked = true;
		document.relatorioSubMenuMedicaoForm.consumoProgramado[0].checked = true;
	}

	function limparFormularioDadosImovel() {
		$("#pesquisarImovel :hidden, #pesquisarImovel :text").val("");
		$("#pesquisarImovelFundo :radio").removeAttr("checked");
	}

	function pesquisarPontoConsumo() {

		selectAllOptions(document.forms[0].idsSegmentosAssociados);
		selectAllOptions(document.forms[0].idsSegmentosDisponiveis);

		selectAllOptions(document.forms[0].idsRamosAtividadesAssociados);
		selectAllOptions(document.forms[0].idsRamosAtividadesDisponiveis);

		$("#pesquisarPontosConsumo").val("true");

		submeter("relatorioSubMenuMedicaoForm",
				"exibirPesquisaRelatorioAnaliseDemanda");

	}

	function habilitaCliente() {
		$(
				"#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto")
				.addClass("rotuloDesabilitado");
		$(
				"#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto")
				.removeClass("rotuloDesabilitado")
	};
	function habilitaImovel() {
		$(
				"#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto")
				.removeClass("rotuloDesabilitado");
		$(
				"#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto")
				.addClass("rotuloDesabilitado")
	};

	function desabilitarPesquisaOposta(elem) {
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
		var selecao = getCheckedValue(elem);
		if (selecao == "indicadorPesquisaImovel") {
			pesquisarImovel(true);
			pesquisarCliente(false);
			habilitaImovel()
		} else {
			pesquisarImovel(false);
			pesquisarCliente(true);
			habilitaCliente()
		}
	}

	function selecionarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");

		if (idSelecionado != '') {
			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {
										idImovel.value = imovel["chavePrimaria"];
										matriculaImovel.value = imovel["chavePrimaria"];
										nomeFantasia.value = imovel["nomeFantasia"];
										numeroImovel.value = imovel["numeroImovel"];
										cidadeImovel.value = imovel["cidadeImovel"];
										indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
									}
								},
								async : false
							}

					);
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
		}

		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if (indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function pesquisarImovel(valor) {
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}

	function pesquisarCliente(valor) {
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}

	function exibirPopupPesquisaImovel() {
		popup = window
				.open(
						'exibirPesquisaImovelCompletoPopup?postBack=true',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function exibirRelatorio() {
		<c:if test="${exibirRelatorio eq true}">
		var iframe = document.getElementById("download");
		iframe.src = 'exibirRelatorioAnaliseDemanda';
		</c:if>
	}

	function configurarRamosAtividades() {
		var campoSegmento = document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados;
		var segmentosAssociados = new Array();

		for (i = 0; i < campoSegmento.length; i++) {
			segmentosAssociados.push(campoSegmento[i].value);
		}

		AjaxService
				.obterRamosAtividadePorSegmentos(
						segmentosAssociados,
						{
							callback : function(dados) {

								if (dados != null) {
									var idsRamosAtividadesDisponiveis = document.relatorioSubMenuMedicaoForm.idsRamosAtividadesDisponiveis;
									var idsRamosAtividadesAssociados = document.relatorioSubMenuMedicaoForm.idsRamosAtividadesAssociados;
									var idsAssociados = new Array();

									for (i = 0; i < idsRamosAtividadesAssociados.length; i++) {
										idsAssociados
												.push(idsRamosAtividadesAssociados[i].value);
									}

									idsRamosAtividadesDisponiveis.length = 0;
									idsRamosAtividadesAssociados.length = 0;

									for (key in dados) {
										var novaOpcao = new Option(dados[key],
												key);

										if (contemValor(idsAssociados, key)) {
											idsRamosAtividadesAssociados.options[idsRamosAtividadesAssociados.length] = novaOpcao;
										} else {
											idsRamosAtividadesDisponiveis.options[idsRamosAtividadesDisponiveis.length] = novaOpcao;
										}
									}
								}
							},
							async : true
						});
	}

	function contemValor(array, valor) {

		var retorno = false;

		for (i = 0; i < array.length; i++) {

			if (array[i] == valor) {
				retorno = true;
				break;
			}

		}

		return retorno;

	}

	function init() {
		
		$("#exibirGraficoCheck").attr("checked", true);
		
		<c:choose>
		<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
		pesquisarImovel(false);
		habilitaCliente();
		</c:when>
		<c:when test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
		pesquisarCliente(false);
		habilitaImovel();
		</c:when>
		<c:otherwise>
		pesquisarCliente(false);
		pesquisarImovel(false);
		</c:otherwise>
		</c:choose>

		exibirRelatorio();
		configurarCamposRadio(document.relatorioSubMenuMedicaoForm.exibirFiltros);
		configurarCamposRadio(document.relatorioSubMenuMedicaoForm.formatoImpressao);
	}
	addLoadEvent(init);

	//Mant�m as op��es do relat�rio definidas
	function configurarCamposRadio(campo) { // exibir filtros
		if (campo.name == "exibirFiltros") {
			var valor = $(campo).val();
			if (campo.value == "true") {
				$("#exibirFiltrosCheck").attr("checked", "checked");
			} else {
				$("#exibirFiltrosCheck").removeAttr("checked");
			}

		} else { // formatoimpressao
			var radio = $("input[name=formatoImpressaoRadio]");
			for (i = 0; i < radio.length; i++) {
				if (radio[i].value == campo.value) {
					radio[i].checked = true;
					break;
				}
			}
		}
	}
</script>
<h1 class="tituloInterno">An�lise de Demanda<a class="linkHelp" href="<help:help>/gerandoorelatriodeanlisededemanda.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos necess�rios abaixo e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" action="/exibirPesquisaRelatorioAnaliseDemanda" id="relatorioSubMenuMedicaoForm" name="relatorioSubMenuMedicaoForm">
	
	<input type="hidden" id="pesquisarPontosConsumo" name="pesquisarPontosConsumo" value="${relatorioSubMenuMedicaoVO.pesquisarPontosConsumo}"/>
	<input type="hidden" id="exibirGrafico" name="exibirGrafico" value=""/>
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp">
		<jsp:param name="exibeGrafico" value="true" />
		<jsp:param name="exibeTipoGrafico" value="false" />
	</jsp:include>
	
	<fieldset id="conteinerRelatorioAnaliseDemanda" class="conteinerPesquisarIncluirAlterar">
		
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCompletoCliente}"/>
				
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoFormatado}"/>
				
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoFormatadoCliente}"/>
						
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>	
				
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${relatorioSubMenuMedicaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<fieldset class="colunaEsq">						
				<label class="rotulo rotuloVertical">Segmento:</label><br />
				<select class="campoList campoVertical" style="width: 163px" name="idsSegmentosDisponiveis" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsSegmentosDisponiveis, document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados, true);configurarRamosAtividades();">
		   			<c:forEach items="${listaSegmento}" var="segmento">
		   				<c:choose>
		   					<c:when test="${not empty relatorioSubMenuMedicaoVO.idsSegmentosAssociados}">
		   						<c:forEach items="${relatorioSubMenuMedicaoVO.idsSegmentosDisponiveis}" var="idSegmentoDisponivel">
		   							<c:if test="${segmento.chavePrimaria eq idSegmentoDisponivel}">
										<option value="<c:out value="${segmento.chavePrimaria}"/>"><c:out value="${segmento.descricao}"/></option>
									</c:if>			   						
		   						</c:forEach>			   					
		   					</c:when>
		   					<c:otherwise>
		   						<option title="<c:out value="${segmento.descricao}"/>" value="<c:out value="${segmento.chavePrimaria}"/>"> 
									<c:out value="${segmento.descricao}"/>
								</option>			   					
		   					</c:otherwise>	
		   				</c:choose>
		   			</c:forEach>
		   		</select>	
		   															
				<fieldset class="conteinerBotoesCampoList1a">						
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" id="botaoDireita" onClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsSegmentosDisponiveis, document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados, true);configurarRamosAtividades();">
					<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos" onClick="moveAllOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsSegmentosDisponiveis, document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados, true);configurarRamosAtividades();">
					
					<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop" onClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados, document.relatorioSubMenuMedicaoForm.idsSegmentosDisponiveis, true);configurarRamosAtividades();">
					<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos" onClick="moveAllOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados, document.relatorioSubMenuMedicaoForm.idsSegmentosDisponiveis, true);configurarRamosAtividades();">
				</fieldset>				
				
				<select class="campoList" style="width: 163px" name="idsSegmentosAssociados" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsSegmentosAssociados, document.relatorioSubMenuMedicaoForm.idsSegmentosDisponiveis, true);configurarRamosAtividades();">
					<c:forEach items="${listaSegmento}" var="segmento">
						<c:forEach items="${relatorioSubMenuMedicaoVO.idsSegmentosAssociados}" var="idSegmentoAssociado">
							<c:if test="${segmento.chavePrimaria eq idSegmentoAssociado}">
								<option title="<c:out value="${segmento.descricao}"/>" value="<c:out value="${segmento.chavePrimaria}"/>">
									<c:out value="${segmento.descricao}"/>
								</option>
							</c:if>
						</c:forEach>
					</c:forEach>
				</select>  									
			</fieldset>
			
			<fieldset class="colunaDir" style="margin-right: 0px">
					
				<label class="rotulo rotuloVertical">Ramo de Atividade:</label><br />
				<select class="campoList campoVertical" style="width: 163px" name="idsRamosAtividadesDisponiveis" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsRamosAtividadesDisponiveis, document.relatorioSubMenuMedicaoForm.idsRamosAtividadesAssociados, true);">
		   			<c:forEach items="${listaRamoAtividade}" var="ramoAtividade">
		   				<c:choose>
		   					<c:when test="${not empty relatorioSubMenuMedicaoVO.idsRamosAtividadesAssociados}">
		   						<c:forEach items="${relatorioSubMenuMedicaoVO.idsRamosAtividadesDisponiveis}" var="idRamoAtividadeDisponivel">
		   							<c:if test="${ramoAtividade.chavePrimaria eq idRamoAtividadeDisponivel}">
										<option value="<c:out value="${ramoAtividade.chavePrimaria}"/>"><c:out value="${ramoAtividade.descricao}"/></option>
									</c:if>			   						
		   						</c:forEach>			   					
		   					</c:when>
		   					<c:otherwise>
		   						<option title="<c:out value="${ramoAtividade.descricao}"/>" value="<c:out value="${ramoAtividade.chavePrimaria}"/>"> 
									<c:out value="${ramoAtividade.descricao}"/>
								</option>			   					
		   					</c:otherwise>	
		   				</c:choose>
		   			</c:forEach>
		   		</select>	
		   															
				<fieldset class="conteinerBotoesCampoList1a">						
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" id="botaoDireita" onClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsRamosAtividadesDisponiveis, document.relatorioSubMenuMedicaoForm.idsRamosAtividadesAssociados, true);">
					<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos" onClick="moveAllOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsRamosAtividadesDisponiveis, document.relatorioSubMenuMedicaoForm.idsRamosAtividadesAssociados, true);">
					
					<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop" onClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsRamosAtividadesAssociados, document.relatorioSubMenuMedicaoForm.idsRamosAtividadesDisponiveis, true);">
					<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos" onClick="moveAllOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsRamosAtividadesAssociados, document.relatorioSubMenuMedicaoForm.idsRamosAtividadesDisponiveis, true);">
				</fieldset>				
				
				<select class="campoList" style="width: 163px" name="idsRamosAtividadesAssociados" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.relatorioSubMenuMedicaoForm.idsRamosAtividadesAssociados, document.relatorioSubMenuMedicaoForm.idsRamosAtividadesDisponiveis, true);">
					<c:forEach items="${listaRamoAtividade}" var="ramoAtividade">
						<c:forEach items="${relatorioSubMenuMedicaoVO.idsRamosAtividadesAssociados}" var="idRamoAtividadeAssociado">
							<c:if test="${ramoAtividade.chavePrimaria eq idRamoAtividadeAssociado}">
								<option title="<c:out value="${ramoAtividade.descricao}"/>" value="<c:out value="${ramoAtividade.chavePrimaria}"/>">
									<c:out value="${ramoAtividade.descricao}"/>
								</option>
							</c:if>
						</c:forEach>
					</c:forEach>
				</select>									
			</fieldset>
		</fieldset>
						
		<fieldset class="conteinerBloco" style="padding-bottom: 20px">
			<label class="rotulo">Exibir consumo programado:</label>
			<input class="campoRadio" type="radio" name="consumoProgramado" value="true" <c:if test="${relatorioSubMenuMedicaoVO.consumoProgramado eq 'true'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">Sim</label>	
			<input class="campoRadio" type="radio" name="consumoProgramado" value="false" <c:if test="${relatorioSubMenuMedicaoVO.consumoProgramado eq 'false'}">checked="checked"</c:if>/>
			<label class="rotuloRadio">N�o</label>
			
			<label class="rotulo rotuloHorizontal campoObrigatorio" style="margin-left: 40px" id="rotuloPeriodo" for="dataInicial"><span class="campoObrigatorioSimbolo">* </span>Periodo:</label>
			<input class="campoData campoHorizontal" type="text" id="dataInicial" name="dataInicial" maxlength="10" value="${relatorioSubMenuMedicaoVO.dataInicial}">
			<label class="rotuloEntreCampos" id="rotuloEntreCamposPeriodo" for="dataFinal">a</label>
			<input class="campoData campoHorizontal" type="text" id="dataFinal" name="dataFinal" maxlength="10" value="${relatorioSubMenuMedicaoVO.dataFinal}">
		</fieldset>
		
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para preenchimento.</p>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="exibirPesquisaRelatorioAnaliseDemanda">
				<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();">
			</vacess:vacess>
			<vacess:vacess param="gerarRelatorioAnaliseDemanda">
				<input class="bottonRightCol2" type="button" id="botaoGerar" value="Gerar" onclick="exibirExportarRelatorio();">
			</vacess:vacess>
			<input class="bottonRightCol bottonRightColUltimo" id="botaoLimpar" value="Limpar" type="button" onclick="limparPesquisa();">
		</fieldset>
	</fieldset>
	
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<p class="orientacaoInicial">Selecione um ou mais Pontos de Consumo na lista abaixo e clique em Gerar Relat�rio</p>
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" id="pontoConsumo" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirPesquisaRelatorioAnaliseDemanda">
	    	
	    	<display:column style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="idsPontoConsumo" value="${pontoConsumo.chavePrimaria}">
	     	</display:column>
	    			    	
	    	<display:column sortable="true" title="Ponto de Consumo" sortProperty="descricao" headerClass="tituloTabelaEsq" class="conteudoTabelaEsq2" style="width: 370px">
	    		<c:out value="${pontoConsumo.descricao}"/>			    		
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Im�vel" sortProperty="imovel.nome" headerClass="tituloTabelaEsq" class="conteudoTabelaEsq2" style="width: 370px">
	    		<c:out value="${pontoConsumo.imovel.nome}"/>			    	
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Segmento" sortProperty="segmento.descricao">
	    		<c:out value="${pontoConsumo.segmento.descricao}"/>			    	
	    	</display:column>
	    						    	
		</display:table>
	</c:if>
	
	<iframe id="download" src ="" width="0" height="0"></iframe> 
	
</form:form>
