<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script type="text/javascript">
	
	$(document).ready(function() {
		$(".campoData").datepicker({
					changeYear: true,
					yearRange: "c-20:c",
					showOn: 'button',
					buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
					buttonImageOnly: true,
					buttonText: 'Exibir Calend�rio', 
					dateFormat: 'dd/mm/yy'
				});

		$("#exportarRelatorio").dialog({
			autoOpen : false,
			width : 245,
			modal : true,
			minHeight : 100,
			maxHeight : 400,
			resizable : false
		});
	});
	
	function exibirExportarRelatorio() {
		var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
		"Erro: </strong>O(s) campo(s) Data In�cio e Data Final s�o de preenchimento obrigat�rio.</p></div>";
		
	
		if( $("#dataInicio").val() == "" ){
			$("#dataInicio").css('border', '1px solid rgb(255, 143, 143)');
			$("#dataInicio").css('background-color', 'rgb(255, 239, 239)');
			
			
			if( $("#dataFinal").val() == "" ){
				$("#dataFinal").css('border', '1px solid rgb(255, 143, 143)');
				$("#dataFinal").css('background-color', 'rgb(255, 239, 239)');
				
			}
			
			$("#msg").html( htmlErro );
			
		}else if( $("#dataFinal").val() != "" ){
			
			$("#msg").html( "" );			
			exibirJDialog("#exportarRelatorio");
		}
	}

	function gerar() {
		
		var formatoImpressao = document.forms[0].formatoImpressao.value;
		var exibirFiltros = document.forms[0].exibirFiltros.value;
		var dataInicio = document.getElementById("dataInicio").value;
		var dataFinal = document.getElementById("dataFinal").value;		
		submeter("gerarRelatorioSubstituicaoMedidor","gerarRelatorioSubstituicaoMedidor?tipoRelatorio="+ formatoImpressao + "&dataInicio =" + dataInicio + "&dataFinal" + dataFinal +
						+ "&exibirFiltros =" + exibirFiltros);
	}
	
	function limparFormulario(){
		document.gerarRelatorioSubstituicaoMedidor.dataInicio.value = "";
		document.gerarRelatorioSubstituicaoMedidor.dataFinal.value = "";
		document.querySelectorAll('input[name="operacao"]').forEach(r => r.checked = false);
		limparSelecao(document.getElementById('equipe'));
		limparSelecao(document.getElementById('servico'));
	}
	
    function limparSelecao(selectElement) {
        for (var i = 0; i < selectElement.options.length; i++) {
            selectElement.options[i].selected = false;
        }
    }
	
	
</script>

<div id="msg"></div>


<h1 class="tituloInterno">Relat�rio de Substitui��o/Retirada de Medidores</h1>
<p class="orientacaoInicial">
	Informe o(s) filtro(s) e em seguida clique em <span
		class="destaqueOrientacaoInicial"> Gerar.</span>
</p>



<form:form action="gerarRelatorioSubstituicaoMedidor"
	id="gerarRelatorioSubstituicaoMedidor"
	name="gerarRelatorioSubstituicaoMedidor" method="post"
	modelAttribute="gerarRelatorioSubstituicaoMedidor">

	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">

	<fieldset class="conteinerPesquisarIncluirAlterar">
			<fieldset id="pesquisaRelatorioMedidoresCol2" class="coluna">
				<label class="rotulo" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" style="width: 150px">Data de refer�ncia:  <span class="campoObrigatorioSimbolo">* </span></label>
				<input class="campo2Linhas campoHorizontal campoData" type="text" id="dataInicio" name="dataInicio" maxlength="10">
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFinal" name="dataFinal" maxlength="10"> <br />
			</fieldset>		
		
            <label for="equipe" style="width: 150px" class="rotulo">Equipe</label>
            <select name="equipe" id="equipe" class="coluna" multiple="multiple">
                <option value="-1">Selecione</option>
                <c:forEach items="${listaEquipe}" var="equipe">
                    <option value="<c:out value="${equipe.chavePrimaria}"/>"
                            <c:if test="${servicoAutorizacaoVO.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${equipe.nome}"/>
                    </option>
                </c:forEach>
            </select>
            <br/>
            <label for="servico" style="width: 150px" class="rotulo">Servi�o Tipo</label>
            <select id="servico" name="servico" class="coluna" multiple="multiple">
                <option value="-1">Selecione</option>
                <c:forEach items="${listaTipoServico}" var="servico">
                    <option value="<c:out value="${servico.chavePrimaria}"/>"
                            <c:if test="${chamadoVO.idServicoTipo == servico.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${servico.descricao}"/>
                    </option>
                </c:forEach>
            </select>            						
			
<!-- 			<label class="rotulo" for="habilitado" style="width: 150px">Opera��o</label> -->
<!-- 			<input class="campoRadio" type="radio" name="operacao" id="operacao" value="Substitui��o" > -->
<!-- 			<label class="rotuloRadio" for="operacaoTipo">Substitui��o</label> -->
<!-- 			<input class="campoRadio" type="radio" name="operacao" id="operacao" value="Retirada" > -->
<!-- 			<label class="rotuloRadio" for="operacaoTipo">Retirada</label> -->
			
            <label for="operacao" style="width: 150px" class="rotulo">Opera��o</label>
            <select id="operacao" name="operacao" class="coluna">
                <option value="">Selecione</option>
				<option value="Substitui��o">Substitui��o</option>
				<option value="Retirada">Retirada</option>
            </select>      			
				
			
			<label class="rotulo" for="tipoRelacaoRelatorio" style="width: 150px">Tipo de Relat�rio: <span class="campoObrigatorioSimbolo">* </span></label>
			<input class="campoRadio" type="radio" name="tipoRelacaoRelatorio" id="tipoRelacaoRelatorio" value="relacao" checked>
			<label class="rotuloRadio" for="tipoRelacaoRelatorio">Rela��o de Medidores</label>
			<input class="campoRadio" type="radio" name="tipoRelacaoRelatorio" id="tipoRelacaoRelatorio" value="listagem" >
			<label class="rotuloRadio" for="tipoRelacaoRelatorio">Listagem de Medidores</label>

			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>	
	</fieldset>


	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="Gerar"
			value="Gerar" type="button" onclick="exibirExportarRelatorio();">
		<input name="Button" class="bottonRightCol bottonRightColUltimo"
			value="Limpar" type="button" onclick="limparFormulario();">
	</fieldset>

</form:form>
