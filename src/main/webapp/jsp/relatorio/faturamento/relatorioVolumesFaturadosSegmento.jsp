<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<link href="/Content/ui.multiselect.css" rel="stylesheet" type="text/css" />

<script>
	$(document).ready(function() {
			
		// Datepicker
		$(".campoData").datepicker({
			changeYear : true,
			showOn : 'button',
			buttonImage : '<c:url value="/imagens/calendario.gif"/>',
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy'
		});
		$(".ui-datepicker-trigger").css("margin-top","5px");
		
		// Validar e checar intervalos de datas para liberar o bot�o Gerar.
		$("#dataInicial, #dataFinal").bind('keypress blur focusin', 
			function() {
				permissaoGerarRelatorio();
			}
		);

		// Dialog
		$("#exportarRelatorio").dialog({
			autoOpen : false,
			width : 245,
			modal : true,
			minHeight : 100,
			maxHeight : 400,
			resizable : false
		});
		
		$( "#dataInicial" ).focus();
		
	});

	// verifica se e possivel gerar relatorio 
	function permissaoGerarRelatorio(){
		var algumSegmentoEscolhido = $('#idsSegmentos').val() != null;
		var datasEstaoValidas = validarDatas("dataInicial", "dataFinal");
		var anoMesEstaValido = validarAnoMes("anoMesReferencia");
		if (anoMesEstaValido) {
			$("#numeroCiclo").removeAttr("disabled");
		} else {
			limparCicloSelect();
		}
		if (algumSegmentoEscolhido) {			
			if (datasEstaoValidas || anoMesEstaValido){
				$("#botaoGerar").removeAttr("disabled");
			} else {
				$("#botaoGerar").attr("disabled","disabled");
			}
		}
	}

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio() {
		$("#tipoExibicao").attr("name", "");
		exibirJDialog("#exportarRelatorio");
	}

	function gerar() {
		$(".mensagens").hide();
		submeter('0', 'gerarRelatorioVolumesFaturadosSegmento');
	}
	
	function limparCamposPesquisa() {
		$("#dataInicial").val("");
		$("#dataFinal").val("");
		$("#anoMesReferencia").val("");
		limparCicloSelect();
		$("#exibirValoresNulos").attr('checked', true);
		$("#exibirTambemContratosInativos").attr('checked', true);
		$("#analitico").attr('checked', true);
		$("#idsSegmentos").val("");
		$("#botaoGerar").attr("disabled","disabled");
	}
	
	function limparCicloSelect() {
		$('#numeroCiclo').val(-1);
		$("#numeroCiclo").attr("disabled", true);
	}
	
</script>

<h1 class="tituloInterno">Volumes Faturados por Segmento
	<a class="linkHelp" href="<help:help>#</help:help>" 
		target="right" onclick="exibirJDialog('#janelaHelp');">
	</a></h1>
	
<p class="orientacaoInicial">Informe os filtros abaixo e clique em 
	<span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" styleId="formRelatorio">
	<input name="acao" type="hidden" id="acao" value="gerarRelatorioTitulosAtraso" />
	<input name="postBack" type="hidden" id="postBack" value="false" />
	<input name="itensSelecionados" type="hidden" id="acao" />
    
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
		<label class="rotulo rotuloVertical" for="idsSegmentos" style="padding-top: 0px;">
			<span class="campoObrigatorioSimbolo">*</span>Segmento:</label>
		<select class="campoList campoVertica" multiple="multiple" 
			name="idsSegmentos" id="idsSegmentos" size="5" style="height: inherit; margin-bottom: 15px;"
			onchange="permissaoGerarRelatorio();">
		
			<c:forEach items="${listaSegmento}" var="segmento">
				<c:set var="selecionado" value="false" />
				<c:forEach items="${relatorioSubMenuFaturamentoVO.idsSegmentos}"
					var="idSegmento">
					<c:if test="${idSegmento eq segmento.chavePrimaria }">
						<c:set var="selecionado" value="true" />
					</c:if>
				</c:forEach>

				<option value="<c:out value="${segmento.chavePrimaria}"/>"
					<c:if test="${selecionado eq true}">
									selected="selected"	</c:if>>
					<c:out value="${segmento.descricao}" />
				</option>
			</c:forEach>
		</select>
	
		<label class="rotulo">
			<span class="campoObrigatorioSimbolo">**</span>Per�odo de Emiss�o:</label>
		<input class="campoData campoHorizontal" type="text" id="dataInicial" autocomplete="off"
			name="dataInicial" value="${relatorioSubMenuFaturamentoVO.dataInicial}">
		<label class="rotuloEntreCampos" style="margin-left: 0;">at�</label>
		<input class="campoData campoHorizontal" type="text" id="dataFinal" autocomplete="off"
			name="dataFinal" value="${relatorioSubMenuFaturamentoVO.dataFinal}">
		
		<label class="rotulo" id="rotuloAnoMesReferencia" for="anoMesReferencia">
			<span class="campoObrigatorioSimbolo">**</span>Ano / M�s de refer�ncia: </label> 
		<input class="campoTexto campoHorizontal" type="text" 
			id="anoMesReferencia"  name="anoMesReferencia" maxlength="7"
			onkeyup="permissaoGerarRelatorio();" autocomplete="off"
			value="<c:out value="${relatorioSubMenuFaturamentoVO.anoMesReferencia}"/>">
		<br />
		<label class="rotulo" id="rotuloCiclo" for="ciclo" 
			style="clear: none; margin-left: 5px;">Ciclo:</label>
		<select name="numeroCiclo" id="numeroCiclo" style="height: 19px;"
			class="campoSelect campo2Linhas campoHorizontal">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaCiclo}" var="ciclo">
				<option value="<c:out value="${ciclo}"/>"
					<c:if test="${relatorioSubMenuFaturamentoVO.numeroCiclo == ciclo}">selected="selected"</c:if>>
					<c:out value="${ciclo}" />
				</option>
			</c:forEach>
		</select>
		
		<label class="rotulo" id="rotuloExibirValoresNulos">Exibir valores nulos</label> 
		<input class="campoCheckbox" type="checkbox" id="exibirValoresNulos"
			name="exibirValoresNulos" style="margin-top: 8px;" value="true" ${relatorioSubMenuFaturamentoVO.exibirValoresNulos eq true ? 'checked' : ''} />
		
		<label class="rotulo" id="rotuloExibirValoresNulos">Exibir tamb�m contratos inativos</label> 
		<input class="campoCheckbox" type="checkbox" id="exibirTambemContratosInativos"
			name="exibirTambemContratosInativos" style="margin-top: 8px;" value="true" ${relatorioSubMenuFaturamentoVO.exibirTambemContratosInativos eq true ? 'checked' : ''} />
		
		<label class="rotulo" for="tipoExibicaoRadio"> Modelo de Relat�rio:</label>
				
		<input class="campoRadio" type="radio" name="tipoExibicao" id="analitico" value="analitico" ${relatorioSubMenuFaturamentoVO.tipoExibicao eq 'analitico' ? 'checked' : ''} />
		<label class="rotuloRadio" for="analitico">Anal�tico</label>
	
		<input class="campoRadio" type="radio" name="tipoExibicao" id="sintetico" value="sintetico" ${relatorioSubMenuFaturamentoVO.tipoExibicao eq 'sintetico' ? 'checked' : ''} />
		<label class="rotuloRadio" for="sintetico" style="margin-right: 0; padding-right: 0">Sint�tico</label>
		
		<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />

		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input name="button" id="botaoGerar" class="bottonRightCol2"
				value="Gerar" type="button" onclick="exibirExportarRelatorio();">
			<input class="bottonRightCol2 bottonRightColUltimo" 
				value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>	
		
	</fieldset>	
	
	<p class="legenda">
		<span class="campoObrigatorioSimbolo">*</span>
		Campo(s) obrigat�rio(s) para a gera��o do Relat�rio
	</p>
	<p class="legenda" style="margin-top: 0;">
		<span class="campoObrigatorioSimbolo">**</span>
		� necess�rio escolher ao menos um dos filtros para a gera��o do relat�rio
	</p>
</form:form>
  