<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script type="text/javascript">

	$(document).ready(function(){
		$("#dataEmissaoInicial,#dataEmissaoFinal, #dataVencimentoInicial, #dataVencimentoFinal").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);

		$("#anoMesReferencia").inputmask("9999/99",{placeholder:"_"});
	
		 // Dialog			
		$("#pontoConsumoPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
	
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
		
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}

	function pesquisar(idPontoConsumo) {
		document.getElementById('idPontoConsumo').value = idPontoConsumo;
	}

	function limparFormulario(){
		document.relatorioSubMenuFaturamentoForm.habilitado[0].checked = true;	
		
    	limparCamposPesquisa();

    	document.getElementById('idSegmento').value = "-1";		
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	
	function limparCamposPesquisa(){				
				
		limparFormularioDadosCliente();	
		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		document.getElementById('idContrato').value = "";
		document.getElementById('numeroNotaFiscalInicial').value = "";
		document.getElementById('numeroNotaFiscalFinal').value = "";
		document.getElementById('dataEmissaoInicial').value = "";
		document.getElementById('dataEmissaoFinal').value = "";
		document.getElementById('dataVencimentoInicial').value = "";
		document.getElementById('dataVencimentoFinal').value = "";
		document.getElementById('idSegmento').value = "-1";
		
		document.getElementById('anoMesReferencia').value = "";
		document.getElementById('numeroCiclo').value = "-1";
		document.getElementById('idGrupo').value = "-1";
		var selectRotas = document.getElementById("idRota");
	 	selectRotas.length = 0;
        var novaOpcaoSelecione = new Option("Selecione", -1);
        selectRotas.options[selectRotas.length] = novaOpcaoSelecione;        		
		
	}

	function limparPontoConsumo(){
		document.getElementById('idContrato').value = "";
		document.getElementById('numeroNotaFiscalInicial').value = "";
		document.getElementById('numeroNotaFiscalFinal').value = "";
		document.getElementById('dataEmissaoInicial').value = "";
		document.getElementById('dataEmissaoFinal').value = "";
		document.getElementById('dataVencimentoInicial').value = "";
		document.getElementById('dataVencimentoFinal').value = "";
		document.getElementById('idSegmento').value = "-1";
		
		submeter('relatorioSubMenuFaturamentoForm', 'exibirRelatorioFaturamento');
	}
	
	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};		

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}	
		
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
		
		ativarBotaoPontoConsumo();
		
	}

	function ativarBotaoPontoConsumo() {
		document.getElementById("botaoPontoConsumo").disabled = false;
	}

	function init() {

		var idCliente = "${relatorioSubMenuFaturamentoVO.idCliente}";
		var idImovel = "${relatorioSubMenuFaturamentoVO.idImovel}";
		if ((idCliente != undefined && idCliente > 0)) {
			ativarBotaoPontoConsumo();
		} else if (idImovel != undefined && idImovel > 0) {
			document.getElementById("botaoPesquisar").disabled = false;
			document.getElementById("botaoPontoConsumo").disabled = false;
		}

		<c:choose>
			<c:when test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
		
	}

	addLoadEvent(init);	

	function pesquisarPontoConsumo() {
		submeter('relatorioSubMenuFaturamentoForm', 'pesquisarPontoConsumoRelatorioFaturamento');
	}

	function preencherIdPontoConsumo() {		
		var idPontoConsumo = $("input[type=radio][name=chavesPontoConsumo]:checked ").val();		
		$("#idPontoConsumo").val(idPontoConsumo);
				
	}

	function gerar(){		
		$(".mensagens").hide();
		submeter('0', 'gerarRelatorioFaturamento');
	}
	
	function carregarRotas(elem) {
        var idGrupoFaturamento = elem.value;
        var selectRotas = document.getElementById("idRota");

        selectRotas.length = 0;

        var novaOpcaoSelecione = new Option("Selecione", -1);
        selectRotas.options[selectRotas.length] = novaOpcaoSelecione;
        
        AjaxService.listarRotaPorGrupoFaturamento(idGrupoFaturamento, function(
                rotas) {
            for (key in rotas) {
                var novaOpcao = new Option(rotas[key], key);
                selectRotas.options[selectRotas.length] = novaOpcao;
            }
            ordernarSelect(selectRotas);
        });

    }

</script>

	<h1 class="tituloInterno">Faturamento<a class="linkHelp" href="<help:help>/gerandoorelatriodefaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	
	<form:form method="post" styleId="formRelatorio" id="relatorioSubMenuFaturamentoForm" name="relatorioSubMenuFaturamentoForm">

	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value=""/>
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${relatorioSubMenuFaturamentoVO.idPontoConsumo}"/>
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>	
	
	<fieldset id="conteinerRelatorioFaturamento" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPontoConsumo"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<fieldset class="coluna">
				<label class="rotulo campoObrigatorio" for="dataEmissaoInicial">Per�odo de emiss�o:</label>
				<input class="campoData campoHorizontal" type="text" id="dataEmissaoInicial" name="dataEmissaoInicial" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataEmissaoInicial}">
				<label class="rotuloEntreCampos" for="dataEmissaoFinal">a</label>
				<c:choose>
					<c:when test="${dtFinal eq null}">
						<input class="campoData campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataEmissaoFinal}">	
					</c:when>
					<c:otherwise>
						<input class="campoData campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10" value="${dtFinal}">
					</c:otherwise>
				</c:choose>
				<br />
				
				<label class="rotulo campoObrigatorio" for="dataVencimentoInicial">Data de vencimento:</label>
				<input class="campoData campoHorizontal" type="text" id="dataVencimentoInicial" name="dataVencimentoInicial" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataVencimentoInicial}">
				<label class="rotuloEntreCampos" for="dataVencimentoFinal">a</label>
				<c:choose>
					<c:when test="${dtFinal eq null}">
						<input class="campoData campoHorizontal" type="text" id="dataVencimentoFinal" name="dataVencimentoFinal" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataVencimentoFinal}">	
					</c:when>
					<c:otherwise>
						<input class="campoData campoHorizontal" type="text" id="dataVencimentoFinal" name="dataVencimentoFinal" maxlength="10" value="${dtFinal}">
					</c:otherwise>
				</c:choose>
				<br />
				
				<label class="rotulo" for="numeroDocumentoInicio">N�mero da nota fiscal:</label>
				<input class="campoTexto campoHorizontal" type="text" id="numeroNotaFiscalInicial" name="numeroNotaFiscalInicial" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${relatorioSubMenuFaturamentoVO.numeroNotaFiscalInicial}">
				<label class="rotuloEntreCampos" for="numeroNotaFiscalFinal">a</label>
				<input class="campoTexto campoHorizontal" type="text" id="numeroNotaFiscalFinal" name="numeroNotaFiscalFinal" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${relatorioSubMenuFaturamentoVO.numeroNotaFiscalFinal}">
			</fieldset>
			
			<fieldset class="colunaFinal">	
			
				<label class="rotulo rotulo2Linhas" id="rotuloAnoMesReferencia" for="anoMesReferenciaLido">Ano / M�s de refer�ncia:</label> 
				<input class="campoTexto campoHorizontal" type="text" id="anoMesReferencia" name="anoMesReferencia"						
						value="<c:out value="${relatorioSubMenuFaturamentoVO.anoMesReferencia}"/>"><br /> 
				<label class="rotulo rotulo2Linhas" id="rotuloCiclo" for="ciclo">Ciclo:</label> 
				<select name="numeroCiclo" id="numeroCiclo" class="campoSelect campo2Linhas campoHorizontal">
					<option value="-1">Selecione</option>
						<c:forEach items="${listaCiclo}" var="ciclo">
							<option value="<c:out value="${ciclo}"/>"
								<c:if test="${relatorioSubMenuFaturamentoVO.numeroCiclo == ciclo}">selected="selected"</c:if>>
								<c:out value="${ciclo}" />
							</option>
						</c:forEach>
				</select>
				<br />
				
				<label class="rotulo" for="idRubrica">Grupo:</label>
				<select name="idGrupo" id="idGrupo" class="campoSelect" onchange="carregarRotas(this)">
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${listaGrupoFaturamento}" var="grupo">
						<option value="<c:out value="${grupo.chavePrimaria}"/>" <c:if test="${relatorioSubMenuFaturamentoVO.idGrupo == grupo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${grupo.descricao}"/>
						</option>
					</c:forEach>
				</select><br class="quebraLinha"/>	
				
				<label class="rotulo" for="idRota">Rota:</label>
				<select name="idRota" id="idRota" class="campoSelect" "  >
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${listaRota}" var="rota">
						<option value="<c:out value="${rota.chavePrimaria}"/>" <c:if test="${relatorioSubMenuFaturamentoVO.idRota == rota.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${rota.numeroRota}"/>
						</option>		
					</c:forEach>
				</select>
				<br class="quebraLinha"/>	
																					
				<label class="rotulo" for="idSegmento">Segmento:</label>
				<select name="idSegmento" id="idSegmento" class="campoSelect" >
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${listaSegmento}" var="segmento">
						<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${relatorioSubMenuFaturamentoVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${segmento.descricao}"/>
						</option>
					</c:forEach>
				</select><br class="quebraLinha"/>									
				
				<label class="rotulo" id="rotuloNumeroContrato" for="idContrato">N�mero do Contrato:<!-- fix bug --></label>
				<input class="campoTexto" type="text" id="idContrato" name="idContrato" maxlength="13" size="13" value="${relatorioSubMenuFaturamentoVO.idContrato}" onkeypress="return formatarCampoInteiro(event);"><br />
				
			</fieldset>
			
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();" disabled="disabled">
				<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();<c:if test="${listaPontoConsumo ne null}">limparPontoConsumo()</c:if>">
			</fieldset>
		</fieldset>
	</fieldset>
	
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoRelatorioResumoVolume">
	        <display:column style="text-align: center; width: 25px">
	        	<input type="radio" name="chavesPontoConsumo" id="chavePontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}" onclick="preencherIdPontoConsumo();"/>
	        </display:column>
	        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: left">
	            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="Segmento" style="width: 130px">
	            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.segmento.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="Situa��o" style="width: 160px">
	            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<vacess:vacess param="pesquisarPontoConsumoRelatorioFaturamento">
			<input name="button" class="bottonRightCol2 botaoGrande1" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
		</vacess:vacess>
	</fieldset>
</form:form>
