<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Posi��o de Contas a Receber por Per�odo<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para gerar um relat�rio, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</br></p>

<script language="javascript">


	$().ready(function(){
		$("input#diasAtraso").keyup(apenasNumeros).blur(apenasNumeros);
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
		// Exibe a janela de Op��es de exporta��o 			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
	});
	
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function gerar() {
		
		$(".mensagens").hide();
		submeter('relatorioSubMenuFaturamentoForm','gerarRelatorioPosicaoContasReceber');
	}
	
	function limparFormContasReceber(){
		
		limparFormularios(document);
		limparFormularioDadosCliente();
		unSelectAllOptions(document.getElementById("idsSegmentos"));
		
	}
	
	function pesquisarPontoConsumo(){
		$("#indicadorPesquisa").val("indicadorPesquisaCliente");
		submeter("relatorioSubMenuFaturamentoForm", "exibirPesquisaPontosConsumo");
	}
					
	</script>

<form:form method="post" styleId="formRelatorio" id="relatorioSubMenuFaturamentoForm" name="relatorioSubMenuFaturamentoForm"> 
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"/>
	<input name="chavePrimarias" type="hidden" id="chavePrimarias"/>
	<input name="idCliente" type="hidden" id="idCliente" value="${relatorioSubMenuFaturamentoVO.idCliente}"/>
	<input type="hidden" id="indicadorPesquisa" name="indicadorPesquisa" value="${relatorioSubMenuFaturamentoVO.indicadorPesquisa}"/>
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>	
	
	<fieldset id="pesquisaArrecadador" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisaCliente" <c:if test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}"></c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPontoConsumo"/>
			</jsp:include>		
		</fieldset>
			
		<fieldset id="pesquisarBanco" class="colunaDir">
			<label class="rotulo rotulo2Linhas" for="dataVencimentoInicial"><span class="campoObrigatorioSimbolo">* </span>Per�odo de Vencimento:</label>
			<input class="campoData campoHorizontal" type="text" id="dataVencimentoInicial" name="dataVencimentoInicial" maxlength="10"
				value="${relatorioSubMenuFaturamentoVO.dataVencimentoInicial}">
			<label class="rotuloEntreCampos" for="dataVencimentoFinal">a</label>
			<input class="campoData campoHorizontal" type="text" id="dataVencimentoFinal" name="dataVencimentoFinal" maxlength="10"
				value="${relatorioSubMenuFaturamentoVO.dataVencimentoFinal}"><br class="quebraLinha2" />
			<label class="rotulo rotulo2Linhas" for="dataReferencia">Data de Refer�ncia:</label>
			<input class="campoData campoHorizontal" type="text" id="dataReferencia" name="dataReferencia" maxlength="10"
				value="${relatorioSubMenuFaturamentoVO.dataReferencia}">
			<label class="rotulo rotulo2Linhas" id="rotuloDiasAtraso" for="diasAtraso">Quantidade de dias em atraso:</label>
            <input class="campoTexto campo2Linhas" type="text" id="diasAtraso" name="diasAtraso" size="20" maxlength="3"
	                <c:out value="${codigoConvenio}"/> value="${relatorioSubMenuFaturamentoVO.diasAtraso}"/>
	                
			<label class="rotulo rotuloVertical" id="rotuloListaSegmento" for="segmento">Segmento:</label>
			<select class="campoList campoVertica" multiple="multiple" name="idsSegmentos" id="idsSegmentos">
			
				<c:forEach items="${listaSegmento}" var="segmento">
					<c:set var="selecionado" value="false" />
					<c:forEach items="${relatorioSubMenuFaturamentoVO.idsSegmentos}"
						var="idSegmento">
						<c:if test="${idSegmento eq segmento.chavePrimaria }">
							<c:set var="selecionado" value="true" />
						</c:if>

					</c:forEach>

					<option value="<c:out value="${segmento.chavePrimaria}"/>"
						<c:if test="${selecionado eq true}">
										selected="selected"	</c:if>>
						<c:out value="${segmento.descricao}" />
					</option>
				</c:forEach>
			</select>			
			
		</fieldset>			
		
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>Campos obrigat�rios.</p>		
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="button" value="Gerar" class="bottonRightCol2 botaoGrande2" id="botaoGerar" type="button" onclick="exibirExportarRelatorio();">
		<input class="bottonRightCol2 botaoGrande2" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();">
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormContasReceber();">
	</fieldset>
	
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<p class="orientacaoInicial">Selecione um ou mais Pontos de Consumo na lista abaixo e clique em Gerar Relat�rio</p>
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" id="pontoConsumo" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirPesquisaPontosConsumo">
	    	
	    	<display:column style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="idsPontoConsumo" value="${pontoConsumo.chavePrimaria}">
	     	</display:column>
	    			    	
	    	<display:column sortable="true" title="Ponto de Consumo" sortProperty="descricao" style="width: 370px">
	    		<c:out value="${pontoConsumo.descricao}"/>			    		
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Im�vel" sortProperty="imovel.nome" style="width: 370px">
	    		<c:out value="${pontoConsumo.imovel.nome}"/>			    	
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Segmento" sortProperty="segmento.descricao">
	    		<c:out value="${pontoConsumo.segmento.descricao}"/>			    	
	    	</display:column>
	    						    	
		</display:table>
	</c:if>

</form:form> 
