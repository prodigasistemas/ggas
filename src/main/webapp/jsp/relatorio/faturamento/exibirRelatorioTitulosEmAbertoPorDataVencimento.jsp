<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">T�tulos Em Aberto por Data Vencimento<a class="linkHelp" href="<help:help>/consultadasfaturas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os filtros abaixo e clique em <span class="destaqueOrientacaoInicial">Gerar</span></p>

<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		 // Dialog	
		 
		 //Define a apar�ncia dos r�tulos da pesquisa de cliente e im�vel como desabilitados
// 		$("#pesquisarImovel .rotulo, #pesquisarCliente .rotulo").addClass("rotuloDesabilitado");
		
		// Exibe a janela de Op��es de exporta��o 			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
	});
	
	
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function gerar() {
		
		$(".mensagens").hide();
		submeter('relatorioSubMenuFaturamentoForm','gerarRelatorioTitulosEmAbertoPorDataVencimento');
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente() {
		
		$("#pesquisarImovel .rotulo").addClass("rotuloDesabilitado");
		$("#pesquisarCliente .rotulo").removeClass("rotuloDesabilitado");
	};
	function habilitaImovel() {
		$("#pesquisarImovel .rotulo").removeClass("rotuloDesabilitado");
		$("#pesquisarCliente .rotulo").addClass("rotuloDesabilitado");
	};

	function limparFormularioDadosImovel() {
		$("#pesquisarImovel :hidden, #pesquisarImovel :text").val("");
		$("#pesquisarImovelFundo :radio").removeAttr("checked");
	}
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);	
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			habilitaImovel();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			habilitaCliente();
			
		}	
	}
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function pesquisarPontoConsumo() {
		submeter('relatorioSubMenuFaturamentoForm', 'pesquisarPontoConsumo');
	}
	
	function ativarBotaoPontoConsumo() {
		document.getElementById("botaoPontoConsumo").disabled = false;
		document.getElementById("botaoGerar").disabled = false;
	}
	
	function ativarBotaoGerar() {
		document.getElementById("botaoGerar").disabled = false;
	}
	
	
	
	function limparFormulario(){

		document.getElementById('dataEmissaoInicial').value = "";
		document.getElementById('dataEmissaoFinal').value = "";
		document.getElementById('dataVencimentoInicial').value = "";
		document.getElementById('dataVencimentoFinal').value = "";
		document.relatorioSubMenuFaturamentoForm.situacaoDebito[2].checked = true;	
		document.relatorioSubMenuFaturamentoForm.indicadorPesquisa[0].checked = false;
    	document.relatorioSubMenuFaturamentoForm.indicadorPesquisa[1].checked = false;
		
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	
	
	function selecionarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");

		if (idSelecionado != '') {
			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {
										idImovel.value = imovel["chavePrimaria"];
										matriculaImovel.value = imovel["chavePrimaria"];
										nomeFantasia.value = imovel["nomeFantasia"];
										numeroImovel.value = imovel["numeroImovel"];
										cidadeImovel.value = imovel["cidadeImovel"];
										indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
									}
								},
								async : false
							}

					);
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
		}

		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if (indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
		ativarBotaoPontoConsumo();
		ativarBotaoGerar();
	}
	
	function init() {

		var idCliente = "${relatorioSubMenuFaturamentoVO.idCliente}";
		var idImovel = "${relatorioSubMenuFaturamentoVO.idImovel}";
		
		if ((idCliente != undefined && idCliente > 0)) {
			ativarBotaoPontoConsumo();
 			ativarBotaoGerar();
		} else if (idImovel != undefined && idImovel > 0) {
			document.getElementById("botaoPontoConsumo").disabled = false;
 			ativarBotaoGerar()
		}

		<c:choose>
			<c:when test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
		
	}

	addLoadEvent(init);	

	

</script>

<form:form method="post" styleId="formRelatorio" id="relatorioSubMenuFaturamentoForm" name="relatorioSubMenuFaturamentoForm">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" >
	 	<jsp:param name="exibeAnaliticoSintetico" value="true"/>
	</jsp:include>	
	
	<fieldset id="conteinerRelatorioFaturamento" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);" >			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPontoConsumo"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${relatorioSubMenuFaturamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirIndicador(); exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${imovel.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<fieldset class="colunaEsq">
				<label class="rotulo campoObrigatorio" for="dataEmissaoInicial">Per�odo de emiss�o:</label>
				<input class="campoData campoHorizontal" type="text" id="dataEmissaoInicial" name="dataEmissaoInicial" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataEmissaoInicial}">
				<label class="rotuloEntreCampos" for="dataEmissaoFinal">a</label>
				<c:choose>
					<c:when test="${dtFinal eq null}">
						<input class="campoData campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataEmissaoFinal}">	
					</c:when>
					<c:otherwise>
						<input class="campoData campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10" value="${dtFinal}">
					</c:otherwise>
				</c:choose>
				<br />
				
				<label class="rotulo campoObrigatorio" for="dataVencimentoInicial">Data de vencimento:</label>
				<input class="campoData campoHorizontal" type="text" id="dataVencimentoInicial" name="dataVencimentoInicial" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataVencimentoInicial}">
				<label class="rotuloEntreCampos" for="dataVencimentoFinal">a</label>
				<c:choose>
					<c:when test="${dtFinal eq null}">
						<input class="campoData campoHorizontal" type="text" id="dataVencimentoFinal" name="dataVencimentoFinal" maxlength="10" value="${relatorioSubMenuFaturamentoVO.dataVencimentoFinal}">	
					</c:when>
					<c:otherwise>
						<input class="campoData campoHorizontal" type="text" id="dataVencimentoFinal" name="dataVencimentoFinal" maxlength="10" value="${dtFinal}">
					</c:otherwise>
				</c:choose>
				<br />
			
				<label class="rotulo" for="situacaoDebito">Situa��o:</label>
				<input class="campoRadio" type="radio" name="situacaoDebito" id="situacaoDebito" value="true" <c:if test="${relatorioSubMenuFaturamentoVO.situacaoDebito eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="ativo">� vencer</label>
				<input class="campoRadio" type="radio" name="situacaoDebito" id="situacaoDebito" value="false" <c:if test="${relatorioSubMenuFaturamentoVO.situacaoDebito eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="inativo">Vencida</label>
				<input class="campoRadio" type="radio" name="situacaoDebito" id="situacaoDebito" value="" <c:if test="${empty relatorioSubMenuFaturamentoVO.situacaoDebito}">checked</c:if>>
				<label class="rotuloRadio" for="todos">Todos</label>	
			</fieldset>
			
		</fieldset>
			
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisarPontoConsumo();" disabled="disabled">
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
		</fieldset>
		 <br /><br /><br />
		 
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumo">       
	         
	       <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	        	<input type="checkBox" name="idsPontoConsumo" id="chavePontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}" onclick="preencherIdPontoConsumo();"/>
	        </display:column>
	        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: left">
	            	<c:out value="${pontoConsumo.descricao}"/>
	        </display:column>
	        <display:column title="Segmento" style="width: 130px">
	            	<c:out value="${pontoConsumo.segmento.descricao}"/>
	        </display:column>
	        <display:column title="Situa��o" style="width: 160px">
	            	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
	        </display:column>
	    </display:table>
	</c:if>
		
		<fieldset class="conteinerBotoes">
			<vacess:vacess param="pesquisarPontoConsumoRelatorioFaturamento">
				<input  id="botaoGerar" class="bottonRightCol2 botaoGrande1" value="Gerar" type="button" onclick="exibirExportarRelatorio();" disabled="disabled">
			</vacess:vacess>
		</fieldset>
	</fieldset>
	
</form:form>
