<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<h1 class="tituloInterno">Relat�rio Indicador de Emerg�ncia <a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos necess�rios abaixo e clique em<span class="destaqueOrientacaoInicial"> Gerar</span></p>
<script language="javascript">
$(document).ready(function(){
	$(".periodoChamado").inputmask("99/9999",{placeholder:"_"});
	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$("#cpfCliente").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjCliente").inputmask("99.999.999/9999-99",{placeholder:"_"});	
	$("#cpfSolicitante").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjSolicitante").inputmask("99.999.999/9999-99",{placeholder:"_"});
	
	
	$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 170, maxHeight: 170, resizable: false});	
	
	$( "#nomeCliente" ).autocomplete({
		 source: function( request, response ) {
			 
		 				$.ajax({
		 							url: "carregaNomesClientes?nome="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
			 											
		 											response( $.map( data, function( item ) {
		 											return {
		 													label:	item.nome,
		 													name:	item.nome + "," + item.rg + "," + item.cpf,
		 													}
		 											}));
		 										}
		 						});
		 		},
		 minLength: 3,
		 select: function( event, ui ) {

		 			var dados = ui.item.name.split(",");	 		
		 			carregarFormPessoaFisica(dados);
		 	
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
		 });
	 
	 $( "#nomeFantasiaCliente" ).autocomplete({
		 source: function( request, response ) {
			 
		 				$.ajax({
		 							url: "carregaNomesClientes?nomeFantasia="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
			 											
		 											response( $.map( data, function( item ) {
		 											return {
			 												label: 	item.nomeFantasia,
		 													name:	item.nomeFantasia + "," + item.cnpj,
		 													}
		 											}));
		 										}
		 						});
		 		},
		 minLength: 3,
		 select: function( event, ui ) {
			 
			 	var dados = ui.item.name.split(",");	 		
			 	carregarFormPessoaJuridica(dados);
		 		
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
		 });
	
});

function carregarFormPessoaFisica(dados){
	if(dados[0] != "null" && dados[0] != null){	
		$("#nomeCliente").val(dados[0]);
	}else{
		$("#nomeCliente").val(null);
	}
	if(dados[1] != "null" && dados[1] != null){		
		$("#rgCliente").val(dados[1]);
	}else{
		$("#rgCliente").val(null);
	}
	$("#cpfCliente").val(dados[2]);
}

function carregarFormPessoaJuridica(dados){	
	$("#nomeFantasiaCliente").val(dados[0]);
	$("#cnpjCliente").val(dados[1]);
}


function gerar(){	
	$(".mensagens").hide();
	var formatoImpressao = document.forms[0].formatoImpressao.value
	var exibirFiltros = document.forms[0].exibirFiltros.value;
	if($("#tipoRelatorioIndiceVazamentoGas").is(":checked")){
		submeter("indicadorEmergenciaForm","gerarRelatorioIndiceVazamento?tipoRelatorio="+formatoImpressao+"&exibirFiltros ="+exibirFiltros);
	}else{
		submeter("indicadorEmergenciaForm","gerarRelatorioIndicadorEmergencia?tipoRelatorio="+formatoImpressao+"&exibirFiltros ="+exibirFiltros);
	}
}
//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
function exibirExportarRelatorio(){
	exibirJDialog("#exportarRelatorio");
}

function limparFormulario() {
	submeter("indicadorEmergenciaForm","exibirPesquisaRelatorioIndicadorEmergencia");	
}
function limparCampoCpfCnpj(obj){
	if(obj.value.length > 14 && obj.value.length < 18 ){
		obj.value ="";
	}else{
		if(obj.value.length < 14){
			obj.value ="";
		}
	}
	
}


function exibirExportarRelatorioAnalitico(){
	
	$("#chavesChamadoTipo").val( $("#listaChamadoTipos").val() );	
	$("#modeloRelatorio").val("analitico");	
	exibirJDialog("#exportarRelatorio");
}


animatedcollapse.addDiv('mostrarFormChamado', 'fade=0,speed=400,group=chamado,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormImovel', 'fade=0,speed=400,group=imovel,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormContrato', 'fade=0,speed=400,group=contrato,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormPessoaFisica', 'fade=0,speed=400,group=pessoaFisica,persist=1,hide=0');
animatedcollapse.addDiv('mostrarFormPessoaJuridica', 'fade=0,speed=400,group=pessoaJuridica,persist=1,hide=0');

function atualizarAssuntos(chave) {
	var noCache = "noCache=" + new Date().getTime();
	var chaveAssunto = document.forms[0].chaveAssunto.value;
	$("#divAssuntos").load("carregarAssuntosServico?chavePrimaria="+chave+"&chaveAssunto="+chaveAssunto+"&"+noCache);
}

</script>

<form:form action="gerarRelatorioChamado" method="POST" id="indicadorEmergenciaForm" name="indicadorEmergenciaForm" modelAttribute="IndicadorEmergenciaVO" >

<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
<input type="hidden" id="chaveAssunto" name="chaveAssunto" value="${chaveAssunto}">
<input type="hidden" id="chaveCliente" value="${cliente.chavePrimaria}" />
<input type="hidden" id="cliente" />
<input type="hidden" id="modeloRelatorio" name="modeloRelatorio" />

<fieldset class="containerPesquisarIncluirAlterar">

		<fieldset id="pesquisarCliente" class="colunaEsq" style="width:800px;">
			
			<div>
				<label class="rotulo rotulo2Linhas" >Per�odo de  Gera��o:</label>
					    <input class="campoTexto campoHorizontal periodoChamado" type="text" id="periodoCriacao" name="periodoCriacao" 
					                 value="">			
				<br/>		
			</div>

			<label class="rotulo rotulo2Linhas" style="margin-top:5px;" for="redeLocal">� Rede Alg�s?</label>
			<input class="campoRadio" type="radio" name="redeLocal" id="localRedeAlgas" value="910" checked>
			<label style="margin-left:2px" class="rotuloRadio" for="redeLocal">Sim</label>
			<input class="campoRadio" type="radio" name="redeLocal" id="localRedeCliente" value="">
			<label style="margin-left:2px" class="rotuloRadio" for="redeLocal">N�o</label>
					
			<label class="rotulo rotulo2Linhas" style="margin-top:5px;" for="descricaoTipoServico">Tipo de Relat�rio:</label>
			<input class="campoRadio" type="radio" name="descricaoTipoServico" id="tipoRelatorioIndiceVazamentoGas" value="VAZAMENTO DE GAS" checked>
			<label style="margin-left:2px" class="rotuloRadio" for="descricaoTipoServico">IVAZ - Vazamentos no Sistema </label>
			<input class="campoRadio" type="radio" name="descricaoTipoServico" id="tipoRelatorioIndicadorEmergenciaVazamentoGas" value="VAZAMENTO DE GAS">
			<label style="margin-left:2px" class="rotuloRadio" for="descricaoTipoServico">TAE - Vazamento de G�s </label>
			<input class="campoRadio" type="radio" name="descricaoTipoServico" id="tipoRelatorioIndicadorEmergenciaFaltaGas" value="FALTA DE GAS">
			<label style="margin-left:2px" class="rotuloRadio" for="descricaoTipoServico">TAE - Falta de G�s</label>	
			<br />
		</fieldset>	
		
	<fieldset class="conteinerBotoesPesquisarDirFixo" style="margin-top: 10px;">
		 <fieldset class="relatorioAnalitico conteinerBotoesPesquisarDirFixo" >
			<input name="Button" class="bottonRightCol2" value="Gerar" type="button" onclick="exibirExportarRelatorio()">
		 </fieldset>
		<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>
	
</fieldset>

</form:form>