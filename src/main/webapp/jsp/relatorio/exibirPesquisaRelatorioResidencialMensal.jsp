<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<h1 class="tituloInterno">Relat�rios Residencial Mensal <a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos necess�rios abaixo e clique em<span class="destaqueOrientacaoInicial"> Gerar</span></p>
<script language="javascript">

$(document).ready(function(){
	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$("#cpfCliente").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjCliente").inputmask("99.999.999/9999-99",{placeholder:"_"});	
	$("#cpfSolicitante").inputmask("999.999.999-99",{placeholder:"_"});
	$("#cnpjSolicitante").inputmask("99.999.999/9999-99",{placeholder:"_"});
	
	$("#exportarRelatorio").dialog({autoOpen: false, width: 255, modal: true, minHeight: 170, maxHeight: 170, resizable: false});	
		
});

function carregarFormPessoaFisica(dados){
	if(dados[0] != "null" && dados[0] != null){	
		$("#nomeCliente").val(dados[0]);
	}else{
		$("#nomeCliente").val(null);
	}
	if(dados[1] != "null" && dados[1] != null){		
		$("#rgCliente").val(dados[1]);
	}else{
		$("#rgCliente").val(null);
	}
	$("#cpfCliente").val(dados[2]);
}

function carregarFormPessoaJuridica(dados){	
	$("#nomeFantasiaCliente").val(dados[0]);
	$("#cnpjCliente").val(dados[1]);
}

function exibirPopupPesquisaImovel() {
	popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function selecionarImovel(idSelecionado){
	var idImovel = document.getElementById("idImovel");
	var matriculaImovel = document.getElementById("matriculaImovel");
	var nomeFantasia = document.getElementById("nomeFantasiaImovel");
	var numeroImovel = document.getElementById("numeroImovel");
	var cidadeImovel = document.getElementById("cidadeImovel");
	var indicadorCondominio = document.getElementById("condominio");
	
	if(idSelecionado != '') {				
		AjaxService.obterImovelPorChave( idSelecionado, {
           	callback: function(imovel) {	           		
           		if(imovel != null){  	           			        		      		         		
	               	idImovel.value = imovel["chavePrimaria"];
	               	matriculaImovel.value = imovel["chavePrimaria"];		               	
	               	nomeFantasia.value = imovel["nomeFantasia"];
	               	numeroImovel.value = imovel["numeroImovel"];
	               	cidadeImovel.value = imovel["cidadeImovel"];
	               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
               	}
        	}, async:false}
        	
        );	        
    } else {
   		idImovel.value = "";
    	matriculaImovel.value = "";
    	nomeFantasia.value = "";
		numeroImovel.value = "";
          	cidadeImovel.value = "";
          	indicadorCondominio.value = "";
   	}

	document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
	document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
	document.getElementById("numeroImovelTexto").value = numeroImovel.value;
	document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
	if(indicadorCondominio.value == 'true') {
		document.getElementById("indicadorCondominioImovelTexto1").checked = true;
	} else {
		document.getElementById("indicadorCondominioImovelTexto2").checked = true;
	}
}

function gerar(){	
	$(".mensagens").hide();
	var formatoImpressao = document.forms[0].formatoImpressao.value
	var exibirFiltros = document.forms[0].exibirFiltros.value;
	submeter("chamadoForm","gerarRelatorioResidencialMensal?tipoRelatorio="+formatoImpressao+"&exibirFiltros ="+exibirFiltros);
	
}
//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
function exibirExportarRelatorio(){
	exibirJDialog("#exportarRelatorio");
}

function limparFormulario() {
	submeter("chamadoForm","exibirPesquisaRelatorioResidencialMensal");	
}
function limparCampoCpfCnpj(obj){
	if(obj.value.length > 14 && obj.value.length < 18 ){
		obj.value ="";
	}else{
		if(obj.value.length < 14){
			obj.value ="";
		}
	}
	
}

</script>

<form:form action="gerarRelatorioChamado" method="POST" id="chamadoForm" name="chamadoForm" modelAttribute="RelatorioComercialMensalVO" >

<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
<input type="hidden" id="chaveAssunto" name="chaveAssunto" value="${chaveAssunto}">
<input type="hidden" id="chaveCliente" value="${cliente.chavePrimaria}" />
<input type="hidden" id="cliente" />

<fieldset class="containerPesquisarIncluirAlterar">
	<fieldset id="conteinerRelatorioFaturamento" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq" >
					
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${relatorioForm.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${relatorioForm.map.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${relatorioForm.map.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${relatorioForm.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${relatorioForm.map.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPontoConsumo"/>
			</jsp:include>		
		</fieldset>
		
			<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${pesquisaRelatorioDescontosConcedidos.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${pesquisaRelatorioDescontosConcedidos.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${pesquisaRelatorioDescontosConcedidos.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${pesquisaRelatorioDescontosConcedidos.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${pesquisaRelatorioDescontosConcedidos.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${pesquisaRelatorioDescontosConcedidos.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${pesquisaRelatorioDescontosConcedidos.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirIndicador(); exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${pesquisaRelatorioDescontosConcedidos.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${pesquisaRelatorioDescontosConcedidos.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${pesquisaRelatorioDescontosConcedidos.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		<label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Ano Refer�ncia:</label>
					<select id="anoReferencia" class="campoSelect" name="anoReferencia">
						<c:forEach items="${listaAnoReferencia}" var="anoReferencia">
						<option value="<c:out value="${anoReferencia}"/>" >
							<c:out value="${anoReferencia}"/>
						</option>
						</c:forEach>
					</select><br/>	
			<label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Modalidade:</label>
					<select id="modalidadeMedicao" class="campoSelect" name="modalidadeMedicao">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaModalidadeMedicao}" var="modalidade">
						<option value="<c:out value="${modalidade.codigo}"/>" <c:if test="${relatorioComercialMensalVO.modalidadeMedicao == modalidade.codigo}">selected="selected"</c:if> >
							<c:out value="${modalidade.descricao}"/>
						</option>
						</c:forEach>
					</select><br/>
		
	</fieldset>
	
	<fieldset id="pesquisarCliente" class="colunaDir">
		
	</fieldset>
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio()">	
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>
	
</fieldset>

</form:form>