<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script type="text/javascript">

	$(document).ready(function(){
		// Datepicker
		$(".campoData").datepicker(
				{changeYear: true, showOn: 'button', 
					buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
					buttonImageOnly: true, buttonText: 'Exibir Calend�rio', 
					dateFormat: 'dd/mm/yy'});

		//Dialog
		$("#exportarRelatorio").dialog(
				{autoOpen: false, width: 245, modal: true, 
					minHeight: 100, maxHeight: 400, resizable: false});
		
		corrigirPosicaoDatepicker();
		
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function gerar(){
		$(".mensagens").hide();
		submeter("gerarRelatorioVolumeCompradoDistribuidoForm",
				"gerarRelatorioVolumeCompradoDistribuido");
	}

	function limparCamposPesquisa(){
		$(":text").val("");
		$("#botaoGerar").attr("disabled","disabled");
	}

</script>

<h1 class="tituloInterno">Volume Comprado x Volume Distribuido<a class="linkHelp" href="<help:help>/gerandoorelatoriodevolumecompradoxvolumedistribudo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	
<form method="post" id="gerarRelatorioVolumeCompradoDistribuidoForm" 
	name="gerarRelatorioVolumeCompradoDistribuidoForm">
	
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo campoObrigatorio" id="rotuloPeriodo" for="dataEmissaoInicial"><span class="campoObrigatorioSimbolo">* </span>Periodo Emiss�o:</label>
		<input class="campoData campoHorizontal" type="text" id="dataEmissaoInicial" name="dataEmissaoInicial" maxlength="10" value="${dataEmissaoInicial}">
		<label class="rotuloEntreCampos" id="rotuloEntreCamposPeriodo" for="dataFinal">a</label>
		<input class="campoData campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10" value="${dataEmissaoFinal}">
		
		<c:set var="listaEntidades" value="${listaSegmentos}" scope="request" />
		
		<div id="gridComponenteListaOrdenada">
			<jsp:include
				page="/jsp/comum/componentes/selecionarListaOrdenada.jsp">
				<jsp:param value="Segmentos" name="labelTitulo"/>
				<jsp:param value="Segmento" name="labelEntidade"/>
			</jsp:include>
		</div>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="gerarRelatorioVolumeCompradoDistribuido.do">
				<input name="button" id="botaoGerar" class="bottonRightCol2" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
			</vacess:vacess>
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>
	
</form>
