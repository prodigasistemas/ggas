<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script language="javascript">

	$(document).ready(function() {
		// Dialog			
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400});
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function gerar(){
		exibirIndicador();
		$(".mensagens").hide();
		submeter('relatorioCadastroForm', 'gerarRelatorioRota');
	}

	function limparCampos(){		
		$("#idPeriodicidade").val("");
		$("#idTipoLeitura").val("");
		$("#numeroRota").val("");
		$("#idGrupoFaturamento").val("");
		$("#idSetorComercial").val("");
		$("#idEmpresa").val("");
		$("#idLeiturista").val("");
		$('input[id=ativo]').attr('checked',true);
	}

	function init(){
		exibirRelatorio();
		configurarCamposRadio(document.relatorioCadastroForm.exibirFiltros);	
		configurarCamposRadio(document.relatorioCadastroForm.formatoImpressao);		
	}
	addLoadEvent(init);

	//Mant�m as op��es do relat�rio definidas
	function configurarCamposRadio(campo){ // exibir filtros
		if(campo.name == "exibirFiltros"){
			var valor = $(campo).val();
			if (campo.value == "true"){
				$("#exibirFiltrosCheck").attr("checked","checked");
			} else {
				$("#exibirFiltrosCheck").removeAttr("checked");
			}
			
		} else { // formatoimpressao
			var radio = $("input[name=formatoImpressaoRadio]");
			for(i = 0; i < radio.length; i++){
				if(radio[i].value==campo.value){
					radio[i].checked = true;
					break;
				}
			}
		}		
	}
	
	function exibirRelatorio(){
		<c:if test="${exibirRelatorio eq true}">
			var iframe = document.getElementById("download");
			iframe.src = 'exibirRelatorioRota';
		</c:if>
	}
	
	
</script>

<h1 class="tituloInterno">Clientes por Rota<a class="linkHelp" href="<help:help>/gerandoorelatriodeclientesporrota.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe o filtro e em seguida clique em <span class="destaqueOrientacaoInicial">Gerar</span>.</p>

<form:form method="post" action="gerarRelatorioRota" id = "relatorioCadastroForm" name="relatorioCadastroForm">

	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaRotaCol1" class="coluna">
			<label class="rotulo" id="rotuloPeriodicidade" for="idPeriodicidade">Periodicidade da Rota:</label>
			<select name="idPeriodicidade" id="idPeriodicidade" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaPeriodicidade}" var="periodicidade">
					<option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${relatorioCadastroVO.idPeriodicidade == periodicidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${periodicidade.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br/>
			
			<label class="rotulo" id="rotuloTipoLeitura" for="idTipoLeitura">Meio de Leitura:</label>
			<select name="idTipoLeitura" id="idTipoLeitura" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaTipoLeitura}" var="tipoLeiturista">
					<option value="<c:out value="${tipoLeiturista.chavePrimaria}"/>" <c:if test="${relatorioCadastroVO.idTipoLeitura == tipoLeiturista.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoLeiturista.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br/>
			
			<label class="rotulo" id="rotuloCodigoRota" for="numeroRota">C�digo da Rota:</label>
			<input class="campoTexto" type="text" name=numeroRota id="numeroRota" maxlength="30" size="30" value="${relatorioCadastroVO.numeroRota}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumericoSemHifen(event)');" style="text-transform: uppercase"/>
			<br/>
			
			<label class="rotulo" id="rotuloGrupoFaturamento" for="idGrupoFaturamento">Grupo de Faturamento:</label>
			<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaGrupoFaturamento}" var="grupo">
					<option value="<c:out value="${grupo.chavePrimaria}"/>" <c:if test="${relatorioCadastroVO.idGrupoFaturamento == grupo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${grupo.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
		</fieldset>
			
		<fieldset id="pesquisaRotaCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloSetorComercial" for="idSetorComercial">Setor Comercial:</label>
			<select name="idSetorComercial" id="idSetorComercial" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaSetorComercial}" var="setorComercial">
					<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${relatorioCadastroVO.idSetorComercial == setorComercial.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${setorComercial.descricao}"/>
					</option>
			    </c:forEach>
			</select><br />
			
			<label class="rotulo" id="rotuloEmpresa" for="idEmpresa">Empresa:</label>
			<select name="idEmpresa" id="idEmpresa" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaEmpresa}" var="empresa">
					<option value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${relatorioCadastroVO.idEmpresa == empresa.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${empresa.cliente.nome}"/>
					</option>
			    </c:forEach>
			</select><br/>
			
			<label class="rotulo" id="rotuloLeiturista" for="idLeiturista">Leiturista:</label>
			<select name="idLeiturista" id="idLeiturista" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaLeiturista}" var="leiturista">
					<option value="<c:out value="${leiturista.chavePrimaria}"/>" <c:if test="${relatorioCadastroVO.idLeiturista == leiturista.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${leiturista.funcionario.nome}"/>
					</option>		
				</c:forEach>
			</select><br/>
			
			<label class="rotulo">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="ativo" value="true" <c:if test="${relatorioCadastroVO.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="ativo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="inativo" value="false" <c:if test="${relatorioCadastroVO.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="inativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="todos" value="" <c:if test="${empty relatorioCadastroVO.habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="todos">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="gerarRelatorioRota">
	   			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
	   		</vacess:vacess>		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCampos();">		
		</fieldset>
	</fieldset>
	
	<iframe id="download" src ="" width="0" height="0"></iframe>
</form:form>