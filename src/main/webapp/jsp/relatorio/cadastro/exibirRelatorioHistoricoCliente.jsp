<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Hist�rico do Cliente<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para gerar um relat�rio, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Gerar</span>, ou clique em <span class="destaqueOrientacaoInicial">Ponto de Consumo</span> para listar<br />
pontos de consumo, ent�o marque o ponto de consumo a sua escolha para gerar o relat�rio do cliente do ponto de consumo escolhido.</br></p>

<script language="javascript">


	$().ready(function(){
		$("input#codigoPontoConsumo").keyup(apenasNumeros).blur(apenasNumeros);
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		// Dialog
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
	
	});

	function pesquisarPontoConsumoHistoricoCliente() {
		
		document.getElementById("idCliente").value;
		submeter("relatorioCadastroForm", "pesquisarPontoConsumoHistoricoCliente");
	}
	
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
	}
	
	function gerar(){
		$(".mensagens").hide();
		submeter('0', 'gerarRelatorioHistoricoCliente');
	}
	
	function limparHistoricoCliente(){

		limparFormularioDadosCliente();
		limparFormularios(relatorioCadastroForm);
		document.getElementsByName("situacao")[2].checked = true;
		
	}
	
</script>

<form:form method="post" action="pesquisarPontoConsumoHistoricoCliente" id="relatorioCadastroForm" name="relatorioCadastroForm">

	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />
 
 	<input name="acao" type="hidden" id="acao" value="gerarRelatorioClienteHistorico" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"/>
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias"/>
	<input type="hidden" id="exibirGridPontoConsumo" name="exibirGridPontoConsumo">
	
	<fieldset id="pesquisaArrecadador" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="isPesquisaObrigatoria" value="true"/>
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
			
		<fieldset id="pesquisarBanco" class="colunaDir">
			<label class="rotulo rotulo2Linhas" for="codigoPontoConsumo">C�digo do Ponto de Consumo:</label>
			<input class="campoTexto campo2Linhas" type="text" id="codigoPontoConsumo" name="codigoPontoConsumo" maxlength="5" size="5"
				<c:if test="${relatorioCadastroVO.codigoPontoConsumo ne 0}">value='${relatorioCadastroVO.codigoPontoConsumo}'</c:if>>
			<label class="rotulo" for="dataEmissaoInicial">Per�odo de Emiss�o:</label>
			<input class="campoData campoHorizontal" type="text" id="dataEmissaoInicial" name="dataEmissaoInicial" maxlength="10"
				value="${relatorioCadastroVO.dataEmissaoInicial}">
			<label class="rotuloEntreCampos" for="dataEmissaoFinal">a</label>
			<input class="campoData campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10"
				value="${relatorioCadastroVO.dataEmissaoFinal}"><br class="quebraLinha2" />
			<label class="rotulo" for="dataDevolucaoInicial">Per�odo de Vencimento:</label>
			<input class="campoData campoHorizontal" type="text" id="dataVencimentoInicial" name="dataVencimentoInicial" maxlength="10"
				value="${relatorioCadastroVO.dataVencimentoInicial}">
			<label class="rotuloEntreCampos" for="dataVencimentoFinal">a</label>
			<input class="campoData campoHorizontal" type="text" id="dataVencimentoFinal" name="dataVencimentoFinal" maxlength="10"
				value="${relatorioCadastroVO.dataVencimentoFinal}"><br class="quebraLinha2" />
			<label class="rotulo" for="situacao">Situa��o:</label>
			<input class="campoRadio" type="radio" name="situacao" id="situacao" value="true" <c:if test="${relatorioCadastroVO.situacao eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="ativo">A Vencer</label>
			<input class="campoRadio" type="radio" name="situacao" id="situacao" value="false" <c:if test="${relatorioCadastroVO.situacao eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="inativo">Vencidos</label>
			<input class="campoRadio" type="radio" name="situacao" id="situacao" value="" <c:if test="${empty relatorioCadastroVO.situacao}">checked</c:if>>
			<label class="rotuloRadio" for="todos">Todos</label>		
		</fieldset>	
	</fieldset>
	<c:if test="${listaPontoConsumo eq null}">
		<fieldset class="conteinerBotoesPesquisarDirFixo" style="width: 400px">
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparHistoricoCliente();"
			 style="margin-right: 5px;">
	<%-- 		<vacess:vacess param="pesquisarPontoConsumoHistoricoCliente"> --%>
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Ponto de Consumo" type="button" 
				onclick="pesquisarPontoConsumoHistoricoCliente();" style="margin-right: 5px;">
	<%-- 		</vacess:vacess> --%>
			<vacess:vacess param="gerarRelatorioClienteHistorico">
	   			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
			</vacess:vacess>
		</fieldset>			
		<br/>
	</c:if>	
	<c:if test="${listaPontoConsumo ne null}">
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Ponto de Consumo" type="button" onclick="pesquisarPontoConsumoHistoricoCliente();">
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparHistoricoCliente();">
		</fieldset>
		<br/>
	</c:if>
	<c:if test="${listaPontoConsumo ne null}">
	<hr class="linhaSeparadoraPesquisa"  id="linha"/>
	<div id="teste" style="margin-top: 70px">
		
		<display:table class="dataTableGGAS" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoHistoricoCliente">
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="chavesPrimarias" value="${pontoConsumo.chavePrimaria}">
	     	</display:column>
	     	 <display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${pontoConsumo.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
		     <display:column sortable="true" sortProperty="descricao" title="Ponto de Consumo">
				<c:out value='${pontoConsumo.descricao}'/>
		     </display:column>
		     <display:column sortable="true" title="Segmento" sortProperty="segmento.descricao">
				<c:out value='${pontoConsumo.segmento.descricao}'/>
	    	 </display:column>
		 	<display:column sortable="true" title="Situa��o" sortProperty="situacaoConsumo.descricao">
				<c:out value='${pontoConsumo.situacaoConsumo.descricao}'/>
	    	</display:column>
		</display:table>
		</div>
	</c:if>
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<c:if test="${listaPontoConsumo ne null}">
			<vacess:vacess param="gerarRelatorioClienteHistorico">
				<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Gerar" type="button" onclick="exibirExportarRelatorio();">
			</vacess:vacess>
		</c:if>
	</fieldset>
</form:form> 
	