<!--
 Copyright (C) <2011> GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$(".campoData").datepicker({
					changeYear: true,
					yearRange: "c-20:c",
					showOn: 'button',
					buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
					buttonImageOnly: true,
					buttonText: 'Exibir Calend�rio', 
					dateFormat: 'dd/mm/yy'
				});
	
		$("#exportarRelatorio").dialog({
			autoOpen : false,
			width : 245,
			modal : true,
			minHeight : 100,
			maxHeight : 400,
			resizable : false
		});
	});
	c
	
	function exibirExportarRelatorio() {
		
		var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
		"Erro: </strong>O(s) campo(s) Data Início e Data Final são de preenchimento obrigatório.</p></div>";
		
	
		if( $("#dataInicio").val() == ""  ||  $("#dataFinal").val() == "" ){
			
			if($("#dataInicio").val() == ""){
				$("#dataInicio").css('border', '1px solid rgb(255, 143, 143)');
				$("#dataInicio").css('background-color', 'rgb(255, 239, 239)');
			}
			
			if($("#dataFinal").val() == "" ){
				$("#dataFinal").css('border', '1px solid rgb(255, 143, 143)');
				$("#dataFinal").css('background-color', 'rgb(255, 239, 239)');
			}
			
			$("#msg").html( htmlErro );
			$("#msg").show();
			
			setTimeout(function (){
				$("#msg").fadeOut("slow");
			}, 3000);
			
			
		}else if( $("#dataFinal").val() != "" ){
			
			var dataAtual = (moment().add(1,'d')).format('YYYY-MM-DD');
			var dataInicio = $("#dataInicio").val();
			var dataFinal = $("#dataFinal").val();
			
			var isDataValida = true;
			
			if(dataInicio.includes("_") || dataFinal.includes("_")){
				isDataValida = false;
			}
			
			if(isDataValida){
				  var dia  = dataInicio.split("/")[0];
				  var mes  = dataInicio.split("/")[1];
				  var ano  = dataInicio.split("/")[2];
				  
				  var dataFormatadaInicio =  ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
				  
				  var dia  = dataFinal.split("/")[0];
				  var mes  = dataFinal.split("/")[1];
				  var ano  = dataFinal.split("/")[2];
				  
				  var dataFormatadaFinal =  ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);

				  if(isNaN(Date.parse(dataFormatadaInicio)) || isNaN(Date.parse(dataFormatadaFinal))){
					  isDataValida = false;
					  var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
						"Erro: </strong>Insira uma data válida.</p></div>";
				  }
				  
				  if(moment(dataFormatadaInicio).format('YYYY-MM-DD') > dataAtual && isDataValida){
					  isDataValida = false;
					  var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
						"Erro: </strong>Data inicial não pode ser maior que a data atual.</p></div>";
				  }
				  
				  if(moment(dataFormatadaFinal).format('YYYY-MM-DD') > dataAtual && isDataValida){
					  isDataValida = false;
					  var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
						"Erro: </strong>Data final não pode ser maior que a data atual.</p></div>";
				  }
				  
				  if(moment(dataFormatadaInicio).format('YYYY-MM-DD') > moment(dataFormatadaFinal).format('YYYY-MM-DD')
						  || moment(dataFormatadaFinal).format('YYYY-MM-DD') < moment(dataFormatadaInicio).format('YYYY-MM-DD')){
					  isDataValida = false;
					  var htmlErro = "<div class=\"notification failure hideit\"><p><strong>" +
						"Erro: </strong>Data inicial não pode ser maior que a data final e vice-versa.</p></div>";
				  }
				  
				 	  
			}
			
			
			if(isDataValida){
				$("#msg").html( "" );			
				exibirJDialog("#exportarRelatorio");
			}else{
				$("#msg").html( htmlErro );
				$("#msg").show();
				
				setTimeout(function (){
					$("#msg").fadeOut("slow");
				}, 3000);
			}
			
		}
	}

	function gerar() {
		
		var formatoImpressao = document.forms[0].formatoImpressao.value;
		var exibirFiltros = document.forms[0].exibirFiltros.value;
		var dataInicio = document.getElementById("dataInicio").value;
		var dataFinal = document.getElementById("dataFinal").value;
		
		submeter("gerarRelatorioClientesLigadosPrimeiraVez","gerarRelatorioClientesLigadosPrimeiraVez?tipoRelatorio="+ formatoImpressao + "&dataInicio =" + dataInicio + "&dataFinal" + dataFinal +
						+ "&exibirFiltros =" + exibirFiltros);
	}
	
	function limparFormulario(){
		document.gerarRelatorioClientesLigadosPrimeiraVez.dataInicio.value = "";
		document.gerarRelatorioClientesLigadosPrimeiraVez.dataFinal.value = "";
	}
</script>

<div id="msg"></div>
<h1 class="tituloInterno">Relatório Clientes Ligados Pela Primeira Vez</h1>
<p class="orientacaoInicial">
	Informe o filtro e em seguida clique em <span
		class="destaqueOrientacaoInicial"> Gerar.</span>
</p>
<form:form action="gerarRelatorioClientesLigadosPrimeiraVez" 
id="gerarRelatorioClientesLigadosPrimeiraVez" 
name="gerarRelatorioClientesLigadosPrimeiraVez" 
method="post" modelAttribute="gerarRelatorioClientesLigadosPrimeiraVez">

	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp" />

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset style="padding-bottom: 20px">
			<fieldset class="colunaServicoAutorizacao">

			<label class="rotulo" style="margin-left: 10px;" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" >Data Início: <span class="campoObrigatorioSimbolo">* </span></label>
				<input class="campo2Linhas campoHorizontal campoData" type="text" id="dataInicio" name="dataInicio" maxlength="10">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">Data final <span class="campoObrigatorioSimbolo">* </span></label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFinal" name="dataFinal" maxlength="10"> <br />

			</fieldset>
			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigatórios</p>
		</fieldset>
	</fieldset>


	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="Button" class="bottonRightCol2" id="Gerar"
			value="Gerar" type="button" onclick="exibirExportarRelatorio();">
		<input name="Button" class="bottonRightCol bottonRightColUltimo"
			value="Limpar" type="button" onclick="limparFormulario();">
	</fieldset>

</form:form>
