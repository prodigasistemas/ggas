<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
	integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"
	integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js"
	integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var logRelatorioComunicacao = "<c:out value="${sessionScope.relatorioComunicacaoInterrupacao}"/>";
		if(logRelatorioComunicacao != ""){
			submeter('comunicacaoForm', 'exibirRelatorioComunicacaoInterrupcao');
		}
		
		
		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});

		$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});
	});


	function gerarRelatorio() {
		submeter("comunicacaoForm",
				"gerarRelatorioSinteticoComunicacaoInterrupcao");
	}
	
	
	function limparFormulario(){		
		$('#servicoTipo option[value=-1]').attr('selected','selected');
		$('#dataGeracaoInicial').val('');
		$('#dataGeracaoFinal').val('');
		$('#dataPrevisaoInicial').val('');
		$('#dataPrevisaoFinal').val('');
		$('#dataExecucaoInicial').val('');
		$('#dataExecucaoFinal').val('');		
		$('#formatoImpressaoXLS').prop('checked', true);
	}
	
</script>

<div class="bootstrap">
	<form:form action="pesquisarComunicacao" id="comunicacaoForm"
		name="comunicacaoForm" method="post"
		modelAttribute="FiltroRelatorioComunicacaoInterrupcaoDTO">

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Relat�rio Sint�tico de Comunica��o
					de Interrup��o</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para gerar
					o rel�rio, clique em <b>Gerar</b> selecionar o formato e clique em
					<b>Confirmar</b> para gerar o rel�torio.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">

						<div class="row mb-2">
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="servicoTipo">Tipo de Servi�o:</label> <select
											onchange="verificarSelecaoServicoTipo();" name="servicoTipo"
											id="servicoTipo" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaServicoTipo}" var="servicoTipo">
												<option
													value="<c:out value="${servicoTipo.chavePrimaria}"/>"
													<c:if test="${comunicacao.servicoTipo.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${servicoTipo.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label>Per�odo de Gera��o do Protocolo:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
											style="padding-left: 0px !important;">
											<input class="form-control form-control-sm bootstrapDP"
												type="text" id="dataGeracaoInicial"
												name="dataGeracaoInicial" maxlength="10"
												value="${comunicacao.dataGeracaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm bootstrapDP"
												type="text" id="dataGeracaoFinal"
												name="dataGeracaoFinal" maxlength="10"
												value="${comunicacao.dataGeracaoFinal}">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label>Per�odo Previs�o Execu��o do Servi�o:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
											style="padding-left: 0px !important;">
											<input
												class="form-control form-control-sm campoData bootstrapDP"
												type="text" id="dataPrevisaoInicial"
												name="dataPrevisaoInicial" maxlength="10"
												value="${comunicacao.dataPrevisaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input
												class="form-control form-control-sm campoData bootstrapDP"
												type="text" id="dataPrevisaoFinal"
												name="dataPrevisaoFinal" maxlength="10"
												value="${comunicacao.dataPrevisaoFinal}">
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label>Per�odo Execu��o do Servi�o:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
											style="padding-left: 0px !important;">
											<input
												class="form-control form-control-sm campoData bootstrapDP"
												type="text" id="dataExecucaoInicial"
												name="dataExecucaoInicial" maxlength="10"
												value="${comunicacao.dataExecucaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input
												class="form-control form-control-sm campoData bootstrapDP"
												type="text" id="dataExecucaoFinal"
												name="dataExecucaoFinal" maxlength="10"
												value="${comunicacao.dataExecucaoFinal}">
										</div>
									</div>
								</div>
							</div>	
							
							<div class="col-md-6">
								<label for="autorizacaoGerada" class="col-md-12">Formato Impress�o:</label>
								<div class="col-md-10">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="formatoImpressaoPDF" name="formatoImpressao" class="custom-control-input" value="true"
											   <c:if test="${comunicacao.formatoImpressao eq 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="formatoImpressaoPDF">PDF</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="formatoImpressaoXLS" name="formatoImpressao" class="custom-control-input" value="false" 
										 	<c:if test="${comunicacao == null or comunicacao.formatoImpressao eq 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="formatoImpressaoXLS">XLS</label>
									</div>
								</div>
							</div>														
						</div>



						<br />
						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoPesquisar"
									type="button" onclick="gerarRelatorio();">
									Gerar
								</button>
								<button class="btn btn-secondary btn-sm" name="botaoLimpar"
									id="botaoLimpar" value="limparFormulario" type="button"
									onclick="limparFormulario();">
									<i class="far fa-trash-alt"></i> Limpar
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</form:form>
</div>


