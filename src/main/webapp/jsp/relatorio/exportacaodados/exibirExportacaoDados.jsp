<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>
	
	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaConsultaExportacao"/>';
	}
	
	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaConsultaExportacao"/>';
	}

	function exibirRelatorio(){
	
		<c:if test="${exibirRelatorio eq true}">
			var iframe = document.getElementById("download");
			iframe.src = 'exibirExportacaoDadosRelatorio';
		</c:if>				
					
	}

	function init(){
		exibirRelatorio();	
	}

	addLoadEvent(init);
	
</script>

<h1 class="tituloInterno">Executar Consulta de Exporta��o de Dados<a class="linkHelp" href="<help:help>/executarumaconsultadeexportaodedados.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os par�metros da pesquisa abaixo, se necess�rio altere o Separador e clique em <span class="destaqueOrientacaoInicial">Gerar</span> para iniciar a extra��o.</p>

<form:form method="post" action="exportarDados" onsubmit="return recuperarPickList(this);">
	
	<input type="hidden" name="acao" id="acao" value="exportarDados">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${consulta.chavePrimaria}">
	
	<fieldset id="conteinerExecutarConsultaExtracaoDados" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="cabecalho" class="conteinerBloco">
			<label class="rotulo">Nome da Consulta: </label>
			<label class="itemDetalhamento"><c:out value="${consulta.nome}"/></label><br />
			<label class="rotulo">Descri��o da Consulta: </label>
			<label class="itemDetalhamento"><c:out value="${consulta.descricao}"/></label>
		</fieldset>

		<hr class="linhaSeparadora1">
		
		<fieldset id="consulta" class="conteinerBloco">
			<jsp:include page="/jsp/relatorio/exportacaodados/parametrosExecucaoConsulta.jsp" />
			
			<hr class="linhaSeparadora1" />
			
			<label class="rotulo campoObrigatorio" id="rotuloSeparador" for="separador"><span class="campoObrigatorioSimbolo">* </span>Separador:</label>
			<input class="campoTexto campoHorizontal" style="text-align: center" type="text" id="separador" name="separador" maxlength="1" size="1" value=";">
			<label for="rotuloSeparador" class="rotuloInformativo2Linhas rotuloHorizontal" style="width: 250px; margin-top: -4px; margin-left: 5px">OBS: o separador padr�o para o formato CSV � o ponto e v�rgula( ; ).</label>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol" id="botaoCancelarParametro" name="botaoCancelarParametro" value="Cancelar" type="button" onclick="cancelar();">
	    <vacess:vacess param="exportarDados">
	    	<input name="Button" class="bottonRightCol2 botaoGrande1 botaoGerar" value="Gerar" type="submit">
	    </vacess:vacess>
	</fieldset>
		
	<iframe id="download" src ="" width="0" height="0"></iframe>

</form:form> 