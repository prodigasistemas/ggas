<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Consulta de Exporta��o de Dados<a class="linkHelp" href="<help:help>/pesquisandoconsultasdeexportaodedados.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para restringir a pesquisa de Consultas cadastradas selecione um m�dulo e/ou nome especifico e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>. Para exibir todas as Consultas apenas clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />

<form:form method="post" action="pesquisarConsultaExportacao" id="consultaForm" name="consultaForm"> 

	<script>

	function limparFormulario(){
		document.forms[0].nome.value = "";
		document.forms[0].idModulo.value = "";		
	}	
	
	function limparFormulario(){
		location.href = '<c:url value="/exibirPesquisaConsultaExportacao"/>';
	}	
	
	function incluir() {		
		location.href = '<c:url value="/exibirInclusaoConsultaExportacao"/>';
	}

	function processar(pagina, acao) {
		
		if (acao == 'copiar') {
			$("#isAcaoCopiar").val(true);
		}
		
		var selecao = verificarSelecaoApenasUm();
		
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("consultaForm", pagina);
	    }
	}	
	
	function excluir(){		
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('consultaForm', 'excluirConsultaExportacao');
			}
	    }
	}

	function detalhar(chavePrimaria)
	{
		document.forms[0].chavePrimaria.value = chavePrimaria;
		submeter("consultaForm", "exibirDetalhamentoConsultaExportacao");
	}
	
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarConsultaExportacao">
	<input name="isAcaoCopiar" type="hidden" id="isAcaoCopiar" value="false">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<!-- <input name="chavesPrimarias" type="hidden" id="chavesPrimarias"> -->
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="conteinerPesquisarConsultaExtracaoDados" class="conteinerPesquisarIncluirAlterar">
			<label class="rotulo" id="rotuloNome" for="nome">Nome:</label>
			<input class="campoTexto campoHorizontal" style="margin-right: 40px" type="text" name="nome" id="nome" maxlength="30" size="50" value="${exportacaoDadosVO.nome}"/>		
			<label class="rotulo rotuloHorizontal" id="rotuloModulo" for="idModulo">M�dulo:</label>
			<select name="idModulo" id="idModulo" class="campoSelect campoHorizontal">
				<option value="-1">Selecione</option>
				<c:forEach items="${modulos}" var="modulo">
					<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${exportacaoDadosVO.idModulo == modulo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modulo.descricao}"/>
					</option>
				</c:forEach>
		    </select>

		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarConsultaExportacao">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>		
	</fieldset>
	
	<c:if test="${listaConsulta ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<a name="pesquisaConsultaResultados"></a>
		<display:table class="dataTableGGAS" name="listaConsulta" sort="list"  id="consulta" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarConsultaExportacao">
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${consulta.chavePrimaria}">
	     	</display:column>
		 	<display:column sortable="false" title="Nome" style="text-align: left; padding-left: 5px; width: 450px" headerClass="tituloTabelaEsq">
				<a href="javascript:detalhar('${consulta.chavePrimaria}');"><span class="linkInvisivel"></span>
					<c:out value="${consulta.nome}"/>
				</a>
			</display:column>
		 	<display:column sortable="false" title="M�dulo" style="text-align: left; padding-left: 5px" headerClass="tituloTabelaEsq">
				<a href="javascript:detalhar('${consulta.chavePrimaria}');"><span class="linkInvisivel"></span>
					<c:out value="${consulta.modulo.descricao}"/>
				</a>
			</display:column>
	    </display:table>
	</c:if>
	 
	<fieldset class="conteinerBotoes">
		<c:if test="${listaConsulta ne null}">
			<vacess:vacess param="exibirAlteracaoConsultaExportacao">
				<input name="button" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="processar('exibirAlteracaoConsultaExportacao', 'alterar');" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirInclusaoConsultaExportacao">
				<input name="button" value="Copiar" class="bottonRightCol2 bottonLeftColUltimo botaoCopiar" onclick="processar('exibirAlteracaoConsultaExportacao', 'copiar');" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirExportacaoDados">
				<input name="button" value="Executar" class="bottonRightCol2 bottonLeftColUltimo botaoExecutar" onclick="processar('exibirExportacaoDados', 'executar');" type="button">
			</vacess:vacess>
			<vacess:vacess param="excluirConsultaExportacao">
				<input name="button" value="Excluir" class="bottonRightCol bottonLeftColUltimo" onclick="excluir();" type="button">
			</vacess:vacess>
		</c:if>
		
		<vacess:vacess param="exibirInclusaoConsultaExportacao">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>

	</fieldset>
<token:token></token:token>
</form:form> 