<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Consulta para Exporta��o de Dados<a class="linkHelp" href="<help:help>/incluiralterarumaconsultadeexportaodedados.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha o nome da consulta, selecione o M�dulo e informe a declara��o SQL. O campo Descri��o � opcional.</p>

<form:form method="post" action="alterarConsultaExportacao" id="consultaForm" name="consultaForm">

	<script>
	
		function limparFormulario(){
			document.forms[0].nome.value = "";
			document.forms[0].idModulo.value = "";
			document.forms[0].sql.value = "";
			document.forms[0].descrocap.value = "";
			
			limparParametro();
		}	
		
		function cancelarEdicao(){
			location.href = '<c:url value="/exibirPesquisaConsultaExportacao"/>';
		}
	
	</script>

	<input type="hidden" name="acao" id="acao" value="alterarConsulta">
	<input type="hidden" name="fluxo" id="fluxo" value="false">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="indexLista" type="hidden" id="indexLista" value="${exportacaoDadosVO.indexLista}">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${consulta.chavePrimaria}">
	
	<fieldset id="conteinerIncluirAlterarConsultaExportacao" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input class="campoTexto campoHorizontal" style="margin-right: 40px" type="text" id="nome" name="nome" maxlength="30" size="70" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${consulta.nome}">
		
		<label class="rotulo rotuloHorizontal campoObrigatorio" for="idModulo"><span class="campoObrigatorioSimbolo">* </span>M�dulo:</label>
		<select name="idModulo" id="idModulo" class="campoSelect" onchange="carregarOperacoes(this);">
			<option value="-1">Selecione</option>
			<c:forEach items="${modulos}" var="modulo">
				<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${consulta.modulo.chavePrimaria == modulo.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${modulo.descricao}"/>
				</option>
			</c:forEach>
		</select>
					
		<fieldset class="conteinerBloco" style="padding-top: 30px">
			<fieldset class="coluna2">
				<label class="rotulo rotuloVertical" id="rotuloSql" for="sql"><span class="campoObrigatorioSimbolo">* </span>Declara��o SQL:</label>
				<textarea id="sql" name="sql" id="sql" class="campoTexto campoVertical" cols="90" rows="10" style="text-transform: uppercase" onblur="toUpperCase(this)">${consulta.sql}</textarea>
				<label for="rotuloSql" class="rotuloInformativo rotuloInformativo2Linhas rotuloHorizontal" style="clear: left; margin-top: 0; padding-top: 0">OBS: apenas declara��es SELECT ser�o permitidas.</label>
			</fieldset>
			<fieldset class="colunaFinal" style="padding-left: 32px">
				<label class="rotulo rotuloVertical" for="descricao">Descri��o:</label>
				<textarea name="descricao" class="campoTexto campoVertical" cols="50" rows="10"">${consulta.descricao}</textarea><br />
			</fieldset>
		</fieldset>
		<hr class="linhaSeparadora1" />
	
	<p class="orientacaoInicial">Preencha os dados do par�metro relacionando cada identificador entre chaves ex: {CODIGO} na Declara��o SQL acima com o campo C�digo de cada par�metro abaixo. Preencha os dados restantes do par�metro e clique em <span class="destaqueOrientacaoInicial">Adicionar</span>.</p>	
		
	<fieldset id="conteinerConsultaExportacaoParametros" class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Par�metros:</legend>
		<jsp:include page="/jsp/relatorio/exportacaodados/includeParametroTelaIncluirAlterar.jsp" >
			 <jsp:param name="actionAdicionarParametro" value="adicionarParametroConsultaExportacaoFluxoInclusaoTelaAlterar" />
			 <jsp:param name="actionAlterarParametro" value="adicionarParametroConsultaExportacaoFluxoAlteracaoTelaAlterar" />
			<jsp:param name="actionRemoverParametro" value="removerParametroConsultaExportacaoFluxoAlteracao" />
		</jsp:include>
	</fieldset>
	</fieldset>	
	
	<fieldset class="conteinerBotoes">	 
	    <input name="Button" class="bottonRightCol" id="botaoCancelarParametro" name="botaoCancelarParametro" value="Cancelar" type="button" onclick="cancelarEdicao();">
	    <vacess:vacess param="alterarConsultaExportacao">
	    	<input name="Button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="submit">
	    </vacess:vacess>
	</fieldset>
<token:token></token:token>
</form:form> 