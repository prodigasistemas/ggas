<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>
	function init(){
		<c:if test="${sucessoManutencaoLista eq true}">
			limparParametro();
		</c:if>

		<c:if test="${parametroAlterado eq true}">
			estadoDosBotoes(false);
		</c:if>
		$("#paramQtdMaxCaracteres").val("");
		$("#paramIdClasseAssociadaObrigatorio").css("display","none");
	}
	addLoadEvent(init);


	// inclui o parametro informado
	function incluirParametro(form, actionAdicionarParametro) {
		 document.forms[0].indexLista.value = -1;
		 verificarTipoMascara(form);
		 submeter('consultaForm', 'adicionarParametroConsulta');
	}

	// altera o parametro especificado
	function alterarParametro(form, actionAlterarParametro) {
		verificarTipoMascara(form);	
		submeter('consultaForm', 'adicionarParametroConsulta');
	}

	// remove o parametro especificado
	function removerParametro(actionRemoverParametro, indice) {
		document.forms[0].indexLista.value = indice;
		submeter('consultaForm', 'removerParametroConsulta');
	}

	// limpa os campos do formul�rio de parametro
	function limparParametro() {	
		var form = document.forms[0];
		form.paramCodigo.value = "";
		form.paramNome.value = "";
		form.paramIdTipo.selectedIndex = 0;
		form.paramMascara.selectedIndex = 0;
		form.paramQtdMaxCaracteres.value = "";
		form.paramOrdemExibicao.value = "";
		
		form.paramIsChaveEntidade.checked = false;
		form.paramIsPermiteMultiplaSelecao.checked = false;
		$("#rotuloParamIsPermiteMultiplaSelecao").addClass("rotuloDesabilitado");
		form.paramIdEntidadeReferenciada.selectedIndex = 0;		
		form.paramIdClasseAssociada.selectedIndex = 0;

		document.forms[0].indexLista.value = -1;
		estadoDosBotoes(false);
		habilitarMultiplaSelecaoEntidades(form);
		exibirCampoClasse(form);	
	}

	// exibe os valores do parametro nos campos correspondentes para edi��o
	function exibirAlteracaoDoParametro(indice, codigo, nome, idTipo, caracteres, mascara, ordem, isChaveEntidade, isMultivalorado, idEntidadeReferenciada, classe) {
		if (indice != "") {
			var form  = document.forms[0];
	
			form.indexLista.value = indice;
			
			if (codigo != "") {
				form.paramCodigo.value = codigo;
			}
			if (nome != "") {
				form.paramNome.value = nome;
			}
			mudarValorCombo(form.paramIdTipo, idTipo);
			if (caracteres != "") {
				form.paramQtdMaxCaracteres.value = caracteres;
			}
			
			if (ordem != "") {
				form.paramOrdemExibicao.value = ordem;
			}
			form.paramIsChaveEntidade.checked = (isChaveEntidade == "true");
			form.paramIsPermiteMultiplaSelecao.checked = (isMultivalorado == "true");
			$("#rotuloParamIsPermiteMultiplaSelecao").removeClass("rotuloDesabilitado");
			mudarValorCombo(form.paramIdEntidadeReferenciada, idEntidadeReferenciada);		
			estadoDosBotoes(true);
			form.paramIsChaveEntidade.disabled = false;
			form.paramIsPermiteMultiplaSelecao.disabled = false;
			
			habilitarCampoMascaraQtdMax(form);
			mudarValorCombo(form.paramMascara, mascara);
			
			exibirCampoClasse(form);
			mudarValorCombo(form.paramIdClasseAssociada, classe);
		}
	
	}

	// rotina que seta o valor do select
	function mudarValorCombo(combo, valor){
		if (valor != "")
		{
		    for (var i = 0; i<combo.options.length; i++)
		    {       
		    	
	            if (combo.options[i].value == valor)
		            {
		                    combo.options[i].selected = true;
		                    break;
		            }
		    }
		}
	}

	// verifica o comportamento dos botoes de acordo com a opera��o 
	function estadoDosBotoes(edicao)
	{
		var botaoAlterarParametro = document.getElementById("botaoAlterarParametro");
		var botaoIncluirParametro = document.getElementById("botaoIncluirParametro");	
		botaoIncluirParametro.disabled = edicao;
		botaoAlterarParametro.disabled = !edicao;		
	}

	// verifica se habilita ou n�o os campos de mascara e quantidade de acordo com a op��o do tipo.
	function habilitarCampoMascaraQtdMax(form)
	{
		$("#paramMascara").removeClass("campoDesabilitado").removeAttr("disabled").removeAttr("readonly");
		$("#paramQtdMaxCaracteres").removeClass("campoDesabilitado").removeAttr("disabled").removeAttr("readonly");
		$("#paramIsChaveEntidade").removeClass("campoDesabilitado").removeAttr("disabled");
		
		//Op��o Boleano
		if (form.paramIdTipo.value == 223)
		{
			$("#paramMascara").addClass("campoDesabilitado").attr("disabled","disabled").val("");
			$("#paramQtdMaxCaracteres").addClass("campoDesabilitado").attr("disabled","disabled").val("");
			$("#paramIsChaveEntidade").addClass("campoDesabilitado").attr("disabled","disabled");
			$("#paramIsPermiteMultiplaSelecao").addClass("campoDesabilitado").attr("disabled","disabled");
		}
		//Op��o Data	
		if (form.paramIdTipo.value == 222)
		{
			$("#paramMascara").val("d/m/y").attr("disabled", "disabled");
			$("#paramQtdMaxCaracteres").val("10").attr("readonly", "readonly");
			$("#paramIsChaveEntidade").addClass("campoDesabilitado").attr("disabled","disabled");
			$("#paramIsPermiteMultiplaSelecao").addClass("campoDesabilitado").attr("disabled","disabled");			
		}
		carregarComboTipoMascara(form);
		habilitarMultiplaSelecaoEntidades(form);
	}

	// verifica se habilita ou nao o campo de multipla sele��o e entidades referenciadas
	function habilitarMultiplaSelecaoEntidades(form)
	{
		form.paramIsPermiteMultiplaSelecao.disabled = !form.paramIsChaveEntidade.checked;
		if (form.paramIsPermiteMultiplaSelecao.disabled)
		{
			$("#rotuloParamIsPermiteMultiplaSelecao").addClass("rotuloDesabilitado");
		}
		else
		{
			$("#rotuloParamIsPermiteMultiplaSelecao").removeClass("rotuloDesabilitado");
		}		
		form.paramIdEntidadeReferenciada.disabled = !form.paramIsChaveEntidade.checked;
		if (form.paramIsChaveEntidade.disabled)
		{
			form.paramIsPermiteMultiplaSelecao.disabled = true;
			$("#rotuloParamIsPermiteMultiplaSelecao").addClass("rotuloDesabilitado");
			form.paramIdEntidadeReferenciada.disabled = true;
		}
	}

	// verifica se habilita ou n�o o campo tipo de mascara
	function verificarTipoMascara(form)
	{
		if (form.paramIdTipo.value == 222)
		{
			form.paramMascara.disabled=false;
		}
	}

	// rotina que habilita ou n�o o campo classe
	function exibirCampoClasse(form)
	{
		var spanObrigatorio = document.getElementById('paramIdClasseAssociadaObrigatorio');
		// caso a entidade referenciada seja a opcao Conteudo Entidade(90) ou Unidade(272)				
		if (form.paramIdEntidadeReferenciada.value==90 || form.paramIdEntidadeReferenciada.value==272)
		{
			// formatar campo obrigat�rio e popula o combo de acordo com a opcao selecionada.
			form.paramIdClasseAssociada.disabled = false;
			exibirDadosClasse(form.paramIdEntidadeReferenciada.value, form);
			//spanObrigatorio.innerHTML="* ";
			$("#paramIdClasseAssociadaObrigatorio").css("display","inline");
			$("label[for=paramIdClasseAssociada]").addClass("campoObrigatorio");
		}else{
			// caso contr�rio, remove as op��es do combo e desabilita			
			limparCombo(form.paramIdClasseAssociada);
			form.paramIdClasseAssociada.disabled = true;
			//spanObrigatorio.innerHTML="";
			$("#paramIdClasseAssociadaObrigatorio").css("display","none");
			$("label[for=paramIdClasseAssociada]").removeClass("campoObrigatorio");
		}
	}

	//Conteudo Entidade -> Entidade_CLASSE (90)
	//Unidade -> UNIDADE_CLASSE (272)	
	function exibirDadosClasse(idTabela, form) {
		var combo = form.paramIdClasseAssociada;		
		limparCombo(combo);
		if (idTabela == 90 || idTabela == 272)
		{			
			if(idTabela == "90")
			{
				AjaxService.listarEntidadeClasse({
		    		callback: function(entidade) {            		      		         		
		            	for (key in entidade){
		            		adicionarOpcaoCombo(combo, key, entidade[key]);
		                }
		            }
		    		, async: false})
			}else
			{
				AjaxService.listarClasseUnidade({
		    		callback: function(entidade) {            		      		         		
		            	for (key in entidade){
		            		adicionarOpcaoCombo(combo, key, entidade[key]);
		                }
		            }
		    		, async: false})
			}
		}	
	}

	// adiciona um option no campo select (DOM)
	function adicionarOpcaoCombo(combo, id, texto)
	{
		var novaOpcao = document.createElement('option');
		novaOpcao.value = id;
		novaOpcao.text = texto;		
		try
		{
			combo.add(novaOpcao, null); // exceto ie
	    }
	    catch(ex)
	    {
	    	combo.add(novaOpcao); // para IE
	    }		
	}

	// remove todos os options de um select com exce��o a primeira op��o, o "Selecione". (DOM)
	function limparCombo(combo)
	{
		for (i = combo.length - 1; i>=0; i--)
		{
			if (i!=0){
	      		combo.remove(i);
			}
		}			
	}

	// carrega de acordo com a opcao de Tipo
	function carregarComboTipoMascara(form)
	{
		var combo = form.paramMascara;
		limparCombo(combo);
		// Data		
		if (form.paramIdTipo.value == 222)
		{
			<c:forEach items="${mascaras_d}" var="mascara">
			adicionarOpcaoCombo(combo, '<c:out value="${mascara.mascara}"/>', '<c:out value="${mascara.mascara}"/>');
			</c:forEach>
			combo.selectedIndex = 1;							
		}		
		// Alfanum�rico
		if (form.paramIdTipo.value == '221')
		{
			<c:forEach items="${mascaras_a}" var="mascara">
			adicionarOpcaoCombo(combo, '<c:out value="${mascara.mascara}"/>', '<c:out value="${mascara.mascara}"/>');
			</c:forEach>			
		}
		// Num�rico
		if (form.paramIdTipo.value == '220')
		{
			<c:forEach items="${mascaras_n}" var="mascara">
			adicionarOpcaoCombo(combo, '<c:out value="${mascara.mascara}"/>', '<c:out value="${mascara.mascara}"/>');
			</c:forEach>							
		}
	}
	
</script>

<fieldset id="consulta" class="conteinerBloco">
	<fieldset class="coluna">
	<input class="campoTexto campo2Linhas" type="hidden" id="paramIsAlterado" name="paramIsAlterado" value="${exportacaoDadosVO.paramIsAlterado}"><br />
		<label class="rotulo campoObrigatorio" id="rotuloParamCodigo" for="paramCodigo"><span class="campoObrigatorioSimbolo">* </span>C�digo:</label>
		<input class="campoTexto campo2Linhas" type="text" id="paramCodigo" name="paramCodigo" maxlength="30" size="32" value="${exportacaoDadosVO.paramCodigo}"><br />
		
		<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloParamNome" for="paramNome"><span class="campoObrigatorioSimbolo">* </span>Nome de exibi��o do par�metro:</label>
		<input class="campoTexto campo2Linhas" type="text" id="paramNome" name="paramNome" maxlength="40" size="44" value="${exportacaoDadosVO.paramNome}"><br />		

		<label class="rotulo campoObrigatorio" for="paramIdTipo"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
			<select name="paramIdTipo" id="paramIdTipo" class="campoSelect" onchange="javascript:habilitarCampoMascaraQtdMax(this.form);">
				<option value="-1">Selecione</option>
				<c:forEach items="${tipos}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${exportacaoDadosVO.paramIdTipo == tipo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipo.descricao}"/>
					</option>		
				</c:forEach>
			</select><br />		

		<label class="rotulo" for="paramMascara">Mascara:</label>
			<select name="paramMascara" id="paramMascara" class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${mascaras}" var="mascara">
					<option value="<c:out value="${mascara.mascara}"/>" <c:if test="${exportacaoDadosVO.paramMascara == mascara.mascara}">selected="selected"</c:if>>
					<c:out value="${mascara.mascara}"/>
					</option>		
				</c:forEach>
			</select><br />		

		<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloParamQtdMaxCaracteres" for="paramQtdMaxCaracteres"><span class="campoObrigatorioSimbolo">* </span>Quantidade m�xima de caracteres:</label>
		<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="paramQtdMaxCaracteres" name="paramQtdMaxCaracteres" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event,2);" value="${exportacaoDadosVO.paramQtdMaxCaracteres}">		

		<label class="rotulo rotuloHorizontal" id="rotuloParamOrdemExibicao" for="paramOrdemExibicao">Ordem de exibi��o:</label>
		<input class="campoTexto campoHorizontal" type="text" id="paramOrdemExibicao" name="paramOrdemExibicao" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event,2);" value="${exportacaoDadosVO.paramOrdemExibicao}">
	</fieldset>
	
	<fieldset class="colunaFinal">
		<legend>Entidade-chave:</legend>
		<fieldset class="conteinerDados">
			<label class="rotulo" id="rotuloParamIsChaveEntidade" for="paramIsChaveEntidade">� chave de entidade?</label>			
			<input class="campoCheckbox" type="checkbox" id="paramIsChaveEntidade" name="paramIsChaveEntidade" <c:if test="${exportacaoDadosVO.paramIsChaveEntidade eq 'on'}">checked</c:if> onclick="javascript:habilitarMultiplaSelecaoEntidades(this.form);"/><br class="quebraLinha2"/>
			<label class="rotulo <c:if test="${exportacaoDadosVO.paramIsChaveEntidade ne 'on'}">rotuloDesabilitado</c:if>" id="rotuloParamIsPermiteMultiplaSelecao" for="paramIsPermiteMultiplaSelecao">Permite m�ltipla sele��o?</label>
			<input class="campoCheckbox" type="checkbox" id="paramIsPermiteMultiplaSelecao" name="paramIsPermiteMultiplaSelecao" <c:if test="${exportacaoDadosVO.paramIsPermiteMultiplaSelecao eq 'on'}">checked</c:if> <c:if test="${exportacaoDadosVO.paramIsChaveEntidade ne 'on'}">disabled="disabled"</c:if> /><br class="quebraLinha2"/>
		
			<label class="rotulo" for="paramIdEntidadeReferenciada">Entidades Referenciadas:</label>
				<select name="paramIdEntidadeReferenciada" id="paramIdEntidadeReferenciada" class="campoSelect" onchange="javascript:exibirCampoClasse(this.form);">
					<option value="-1">Selecione</option>
					<c:forEach items="${entidadesReferenciadas}" var="entidade">
						<option value="<c:out value="${entidade.chavePrimaria}"/>" <c:if test="${exportacaoDadosVO.paramIdEntidadeReferenciada == entidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${entidade.descricao}"/>
						</option>		
					</c:forEach>
				</select><br />		
			
			<label class="rotulo" for="paramIdClasseAssociada"><span id="paramIdClasseAssociadaObrigatorio" class="campoObrigatorioSimbolo">* </span>Classe:</label>
			<select name="paramIdClasseAssociada" id="idParamClasseAssocidada" class="campoSelect" disabled="disabled">
				<option value="-1">Selecione</option>
			</select>
			
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input class="bottonRightCol2" id="botaoIncluirParametro" name="botaoIncluirParametro" value="Adicionar" type="button" onclick="incluirParametro(this.form,'<c:out value="${param['actionAdicionarParametro']}"/>');">		
	   	<input class="bottonRightCol2" id="botaoAlterarParametro" name="botaoAlterarParametro" disabled="disabled" value="Alterar" type="button" onclick="alterarParametro(this.form,'<c:out value="${param['actionAlterarParametro']}"/>');">
	   	<input class="bottonRightCol2 bottonRightColUltimo" id="botaoLimparParametro" name="botaoLimparParametro" value="Limpar" type="button" onclick="limparParametro();">
	</fieldset>	
	
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Consulta.</p>		
	
</fieldset>

<c:if test="${listaConsultaParametro ne null}">
	<hr class="linhaSeparadora1">
	<p class="orientacaoInicial">Para alterar um par�metro selecione-o na lista abaixo e seus dados ser�o carregados nos campos acima. Ao finalizar as altera��es no par�metro clique em <span class="destaqueOrientacaoInicial">Alterar</span>. Para encerrar clique em <span class="destaqueOrientacaoInicial">Salvar</span>.</p>

<c:set var="i" value="0" />
<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaConsultaParametro" sort="list" id="consultaParametro"  pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao">
		<display:column sortable="false" title="C�digo" style="text-align: left; padding-left: 5px" headerClass="tituloTabelaEsq" maxLength="40">
		<a href="javascript:exibirAlteracaoDoParametro('${i}','${consultaParametro.codigo}','${consultaParametro.nomeCampo}','${consultaParametro.entidadeConteudo.chavePrimaria}','${consultaParametro.numeroMaximoCaracteres}','${consultaParametro.mascara}','${consultaParametro.ordem}','${consultaParametro.fk}','${consultaParametro.multivalorado}','${consultaParametro.tabela.chavePrimaria}','${consultaParametro.chaveEntidadeComplementar}');"><span class="linkInvisivel"></span>			
			<c:out value="${consultaParametro.codigo}"/>
		</a>	
	</display:column>	
		<display:column sortable="false" title="Nome de exibi��o<br />do par�metro" style="text-align: left; padding-left: 5px; width: 200px" headerClass="tituloTabelaEsq" maxLength="40">
		<a href="javascript:exibirAlteracaoDoParametro('${i}','${consultaParametro.codigo}','${consultaParametro.nomeCampo}','${consultaParametro.entidadeConteudo.chavePrimaria}','${consultaParametro.numeroMaximoCaracteres}','${consultaParametro.mascara}','${consultaParametro.ordem}','${consultaParametro.fk}','${consultaParametro.multivalorado}','${consultaParametro.tabela.chavePrimaria}','${consultaParametro.chaveEntidadeComplementar}');"><span class="linkInvisivel"></span>
			<c:out value="${consultaParametro.nomeCampo}"/>
		</a>
	</display:column>
	
		<display:column sortable="false" title="Tipo"  style="width: 85px">
		<a href="javascript:exibirAlteracaoDoParametro('${i}','${consultaParametro.codigo}','${consultaParametro.nomeCampo}','${consultaParametro.entidadeConteudo.chavePrimaria}','${consultaParametro.numeroMaximoCaracteres}','${consultaParametro.mascara}','${consultaParametro.ordem}','${consultaParametro.fk}','${consultaParametro.multivalorado}','${consultaParametro.tabela.chavePrimaria}','${consultaParametro.chaveEntidadeComplementar}');"><span class="linkInvisivel"></span>
			<c:out value="${consultaParametro.entidadeConteudo.descricao}"/>
		</a>	
	</display:column>
	
		<display:column sortable="false" title="Quant.<br />Caracteres"  style="width: 70px">
		<a href="javascript:exibirAlteracaoDoParametro('${i}','${consultaParametro.codigo}','${consultaParametro.nomeCampo}','${consultaParametro.entidadeConteudo.chavePrimaria}','${consultaParametro.numeroMaximoCaracteres}','${consultaParametro.mascara}','${consultaParametro.ordem}','${consultaParametro.fk}','${consultaParametro.multivalorado}','${consultaParametro.tabela.chavePrimaria}','${consultaParametro.chaveEntidadeComplementar}');"><span class="linkInvisivel"></span>
			<c:out value="${consultaParametro.numeroMaximoCaracteres}"/>
		</a>
	</display:column>
 	
	 	<display:column sortable="false" title="M�scara"  style="width: 125px">
		<a href="javascript:exibirAlteracaoDoParametro('${i}','${consultaParametro.codigo}','${consultaParametro.nomeCampo}','${consultaParametro.entidadeConteudo.chavePrimaria}','${consultaParametro.numeroMaximoCaracteres}','${consultaParametro.mascara}','${consultaParametro.ordem}','${consultaParametro.fk}','${consultaParametro.multivalorado}','${consultaParametro.tabela.chavePrimaria}','${consultaParametro.chaveEntidadeComplementar}');"><span class="linkInvisivel"></span>
			<c:out value="${consultaParametro.mascara}"/>
		</a> 	
 	</display:column>

		<display:column sortable="false" title="Entidade" style="text-align: left; padding-left: 5px; width: 200px" headerClass="tituloTabelaEsq">
			<a style="line-height: 15px;" href="javascript:exibirAlteracaoDoParametro('${i}','${consultaParametro.codigo}','${consultaParametro.nomeCampo}','${consultaParametro.entidadeConteudo.chavePrimaria}','${consultaParametro.numeroMaximoCaracteres}','${consultaParametro.mascara}','${consultaParametro.ordem}','${consultaParametro.fk}','${consultaParametro.multivalorado}','${consultaParametro.tabela.chavePrimaria}','${consultaParametro.chaveEntidadeComplementar}');"><span class="linkInvisivel"></span>
				<span style="display: block">
					Chave: 	
		<c:choose>
	      <c:when test="${consultaParametro.fk ne null && consultaParametro.fk eq true}">
	      	Sim
	      </c:when>		
	      <c:otherwise>
	      	N�o
	      </c:otherwise>
	    </c:choose>
			    </span>
			   <span style="display: block">
		Multivalorado:  	
		<c:choose>
	      <c:when test="${consultaParametro.multivalorado ne null && consultaParametro.multivalorado eq true}">
	      	Sim
	      </c:when>		
	      <c:otherwise>
	      	N�o
	      </c:otherwise>
	    </c:choose>
				</span>
				<span style="display: block">
					Nome: <c:out value="${consultaParametro.tabela.descricao}"/>
				</span>
			</a>
			
	</display:column>
	
		<display:column style="text-align: center; width: 20px"> 
		<a onclick="return confirm('Deseja excluir o parametro?');" href="javascript:removerParametro('${param['actionRemoverParametro']}','${i}');"><img title="Exluir par�metro" alt="Exluir parametro" src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a> 
	</display:column>
	
	<c:set var="i" value="${i+1}" />	
</display:table>
</c:if>