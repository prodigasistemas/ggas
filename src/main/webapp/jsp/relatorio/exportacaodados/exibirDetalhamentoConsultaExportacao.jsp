<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhamento de Consulta para Exporta��o de Dados<a class="linkHelp" href="<help:help>/detalhamentodeumaconsultadeexportaodedados.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" id="consultaForm" name="consultaForm">
	 
	<script>
	
		function cancelarEdicao()
		{
			location.href = '<c:url value="/exibirPesquisaConsultaExportacao"/>';
		}
		function alterar() {
			submeter("consultaForm", "exibirAlteracaoConsultaExportacao");
		}	
	
	</script>
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${consulta.chavePrimaria}">
	
	<fieldset id="conteinerDetalhamentoConsultaExportacao" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo rotuloHorizontal">Nome:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo2" style="margin-right: 40px">${consulta.nome}</span>
		
		<label class="rotulo rotuloHorizontal" for="idModulo">M�dulo:</label>
		<span class="itemDetalhamento">${consulta.modulo.descricao}</span>
		
		<fieldset class="conteinerBloco" style="padding-top: 30px">
			<fieldset class="coluna2">
				<label class="rotulo rotuloVertical">Declara��o SQL:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo2">${consulta.sql}</span>
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo rotuloVertical">Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio">${consulta.descricao}</span>
			</fieldset>
		</fieldset>
		<hr class="linhaSeparadora1" />
	
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Par�metros:</legend>
			<jsp:include page="/jsp/relatorio/exportacaodados/detalhamentoParametroConsultaExportacao.jsp" />
		</fieldset>
		
		<fieldset class="conteinerBotoes">	 
		    <input name="Button" class="bottonRightCol" id="botaoCancelarParametro" name="botaoCancelarParametro" value="Voltar" type="button" onclick="cancelarEdicao();">
		    <vacess:vacess param="alterarConsultaExportacao">
		    	<input name="Button" class="bottonRightCol2 botaoGrande1" id="botaoAlterar" name="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
		    </vacess:vacess>
		</fieldset>

	</fieldset>
</form:form> 