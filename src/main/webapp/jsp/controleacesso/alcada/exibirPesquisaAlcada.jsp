<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Al�ada<a class="linkHelp" href="<help:help>/consultadasaladas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para restringir a pesquisa de Al�ada a um perfil e/ou Opera��o especifica, preencha os respectivos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>. Para exibir todas as Al�adas cadastradas apenas clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />

<form method="post" action="pesquisarAlcada" name="controleAlcadaForm" id="controleAlcadaForm"> 

	<script language="javascript">

	$(document).ready(function(){
		
		//Efeito mouseover nas tabelas de resultados de pesquisa (displaytag)
	   	$("tr.odd,tr.even").hover(
			function () {
				$(this).addClass("selectedRow");
			},
			function () {
				$(this).removeClass("selectedRow");
			}
		);
	});
	
	function limparFormulario(){
		document.forms[0].nome.value = "";
		document.forms[0].idModulo.value = "";		
	}	
	
	function limparFormulario(){
		document.forms[0].idMenu.value = "-1";	
	}	
	
	function definir() {		
		submeter("controleAlcadaForm", "exibirDefinicaoAlcada");
	}

	function detalhar(dadosAlcada){

		document.forms[0].listaAlcadasRemocao.value = dadosAlcada;		
		submeter("controleAlcadaForm", "exibirDetalharAlcada");
	}

	function alterar(){			
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			var dadosAlcada = obterValorUnicoCheckboxSelecionado();
			
			document.forms[0].listaAlcadasRemocao.value = dadosAlcada;
			
			submeter('controleAlcadaForm', 'exibirAlterarAlcada');
	    }
	}

	function remover(){
		
		var cont = 0;
		var teste = new Array();
		
		
		if (document.forms[0].chavesPrimarias != undefined) {
			var total = document.forms[0].chavesPrimarias.length;
			
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if(document.forms[0].chavesPrimarias[i].checked == true){
						
						teste[cont] = document.forms[0].chavesPrimarias[i].value; 
						cont++;
					}
				}
			}
		}

		var selecao = verificarSelecao();
		if (selecao == true) {
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {

					document.forms[0].listaAlcadasRemocao.value = teste;
					submeter('controleAlcadaForm', 'removerAlcada');
				}
			}
		}

	}
	
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarAlcada">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="funcionalidadeHidden" type="hidden" id="funcionalidadeHidden">
	<input name="papelHidden" type="hidden" id="papelHidden">
	<input name="dataInicialHidden" type="hidden" id="dataInicialHidden">
	<input name="dataFinalHidden" type="hidden" id="dataFinalHidden">
	<input name="listaAlcadasRemocao" type="hidden" id="listaAlcadasRemocao">
	
	<fieldset id="pesquisarOperacao" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo" id=rotuloOperacao for="idOperacao">Funcionalidade / Opera��o:</label>
		<select name="idMenu" id="idMenu" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${menus}" var="menu">
				<option value="<c:out value="${menu.chavePrimaria}"/>" <c:if test="${alcada.idMenu == menu.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${menu.descricao}"/>
				</option>
			</c:forEach>
	    </select>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaAlcadasConsultadas ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<a name="pesquisaConsultaResultados"></a>
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="${listaAlcadasConsultadas}" sort="list" id="alcada" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAlcada">
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='todos' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="<c:out value='${alcada.idMenu}'/>_<c:out value='${alcada.idPapel}'/>_<fmt:formatDate value="${alcada.dataInicial}" pattern="dd-MM-yyyy"/>_<fmt:formatDate value="${alcada.dataFinal}" pattern="dd-MM-yyyy"/>">
	        </display:column>
	        <display:column sortable="true" sortProperty="funcionalidade" title="Funcionalidade / Opera��o">
	        	<a href="javascript:detalhar('<c:out value='${alcada.idMenu}'/>_<c:out value='${alcada.idPapel}'/>_<fmt:formatDate value="${alcada.dataInicial}" pattern="dd-MM-yyyy"/>_<fmt:formatDate value="${alcada.dataFinal}" pattern="dd-MM-yyyy"/>')"><span class="linkInvisivel"></span>
            		<c:out value="${alcada.descricaoFuncionalidade}"/>
            		<input type="hidden" name="listaFuncionalidades" id="funcionalidade${i}" value="${alcada.descricaoFuncionalidade}"/>
            	</a>
	        </display:column>
	        <display:column sortable="true" sortProperty="papel" title="Perfil" style="width: 350px">
	        	<a href="javascript:detalhar('<c:out value='${alcada.idMenu}'/>_<c:out value='${alcada.idPapel}'/>_<fmt:formatDate value="${alcada.dataInicial}" pattern="dd-MM-yyyy"/>_<fmt:formatDate value="${alcada.dataFinal}" pattern="dd-MM-yyyy"/>')"><span class="linkInvisivel"></span>
            		<c:out value="${alcada.descricaoPapel}"/>
            		<input type="hidden" name="listaPapeis" id="papel${i}" value="${alcada.descricaoPapel}"/>
            	</a>
	        </display:column>
	        <display:column sortable="true" sortProperty="dataInicial" title="Validade da al�ada" style="width: 150px">
	        	<a href="javascript:detalhar('<c:out value='${alcada.idMenu}'/>_<c:out value='${alcada.idPapel}'/>_<fmt:formatDate value="${alcada.dataInicial}" pattern="dd-MM-yyyy"/>_<fmt:formatDate value="${alcada.dataFinal}" pattern="dd-MM-yyyy"/>')"><span class="linkInvisivel"></span>
	        		<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy"/> - <fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy" />
	        		<input type="hidden" name="listaDataInicial" id="dataInicial${i}" value='<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy"/>'/>
	        		<input type="hidden" name="listaDataFinal" id="dataFinal${i}" value='<fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy"/>'/> 
	        	</a>
	        </display:column>
	        <c:set var="i" value="${i+1}" />
		</display:table>
	</c:if>
	 
	<fieldset class="conteinerBotoes">
		<c:if test="${listaAlcadasConsultadas ne null}">
			<!-- Remover da vers�o final 
			<input name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterar();" type="button">
			<input name="buttonRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="remover();" type="button">
			-->
			
			<vacess:vacess param="exibirAlterarAlcada">
  				<input name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterar()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerAlcada">
				<input name="buttonRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="remover()" type="button">
			</vacess:vacess>
		</c:if>
		<!-- Remover da vers�o final
		<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="definir();" type="button">
		-->
		<vacess:vacess param="exibirDefinicaoAlcada">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="definir();" type="button">
		</vacess:vacess>
		
	</fieldset>

</form> 