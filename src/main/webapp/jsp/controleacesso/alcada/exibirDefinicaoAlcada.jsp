<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Incluir Al�ada<a class="linkHelp" href="<help:help>/inclusoalteraodasaladas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Selecione uma funcionalidade e seu(s) perfil(s) e clique em <span class="destaqueOrientacaoInicial">Exibir</span>.<br />

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/selectbox.js"></script>

<script language="javascript">
	
	$(document).ready(function(){
		$(".campoData").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});

		// Dialog			
		$("#detalheAlcada").dialog({
			autoOpen: false,
			width: 500,
			modal: true,
			minHeight: 120,
			resizable: false
		});
	});
		
	function habilitarExibir(){
		var idMenuSelecionado = document.forms[0].idMenu.value;
		if (idMenuSelecionado != undefined && idMenuSelecionado != -1){
			document.forms[0].btExibir.disabled = false;
		}else{
			document.forms[0].btExibir.disabled = true;
		}
		
	}

	function desabilitarIncluir(){
		var btnIncluir = document.forms[0].btIncluir;
		if (btnIncluir != undefined){
			document.forms[0].btIncluir.disabled = true;
		}
		
	}
	
	function habilitarIncluir(){
		var btnIncluir = document.forms[0].btIncluir;
		if (btnIncluir != undefined){
			document.forms[0].btIncluir.disabled = false;
		}
	}


	function exibir(){
		var lista = document.getElementById('listaSelecionados');
		if (lista != null){
			for (i=0; i<lista.length; i++){
				lista.options[i].selected = true;
			}
		}
		submeter("controleAlcadaForm", "exibirAlcada");
	}

	function incluir(){
		var lista = document.getElementById('listaSelecionados');
		if (lista != null){
			for (i=0; i<lista.length; i++){
				lista.options[i].selected = true;
			}
		}
		submeter("controleAlcadaForm", "incluirAlcada");
	}

	function cancelar(){
		submeter("controleAlcadaForm", "pesquisarAlcada");
	}

	function init(){
		habilitarExibir();
	}

	function detalharAlcada(descricaoPapel,dataInicial,dataFinal){
		
		if (descricaoPapel != '' && dataInicial != '' && dataFinal != ''){
			AjaxService.obterDetalhesAlcada(descricaoPapel, dataInicial, dataFinal , {
				callback: function(listaAlcadas) {	           		
		       		if(listaAlcadas != null){
		       			var cont = 0;
		       			var inner = '';
		       			$('#detalheAlcada').html('');
		       			inner = inner + '<label class="rotulo">Perfil:</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">'+ descricaoPapel + '</label>';

		       			$("#detalheAlcada").append(inner);
		       			
		       			for (key in listaAlcadas) {
			       			var alcada = listaAlcadas[cont];
			       			var valorInicial = parseFloat(alcada[1]);
			       			var valorFinal = parseFloat(alcada[2]);
			       			inner = '';	
			       			inner = inner + '<label class="rotulo">'+ alcada[0] + ':</label>';
							if(!isNaN(valorInicial)){
			       				inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">a partir de ' + alcada[1] + '</label>';
							}
							if(!isNaN(valorFinal)){
			       				inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">at� '+ alcada[2] + '</label>';
							}
			       			//inner = inner + '<label  id=rotuloCampo>'+ valorFinal + '</label>';
			       			$("#detalheAlcada").append(inner);
			       			
			       			cont++;
		       			}
		       			inner = '';
		       			inner = inner + '<label class="rotulo">Validade da al�ada:</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">a partir de ' + dataInicial + '</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">at� '+ dataFinal + '</label>';
		       			//inner = inner + '<label  id=rotuloCampo>'+ dataFinal + '</label>';
		       			$("#detalheAlcada").append(inner);
		           	}
	    		}, async:false}
    		);
		}
		$("#detalheAlcada").dialog('open');
		
	}

	addLoadEvent(init);
</script>



<form method="post" action="pesquisarAlcada" name="controleAlcadaForm" id="controleAlcadaForm"> 
	
	<div style="display: none" id="detalheAlcada" title="Al�ada" style="text-align: left;">

	</div>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarAlcada">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="descricaoPapel" type="hidden" id="descricaoPapel">
	<input name="dataInicial" type="hidden" id="dataInicial">
	<input name="dataFinal" type="hidden" id="dataFinal">
	
	<div id="indicadorPesquisa" style="display: none">
		<img src="<c:url value="/imagens/indicator2.gif"/>" width="132px" height="132px"/>
	</div>
	
	<fieldset id="conteinerIncluirAlterarAlcada" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<label class="rotulo" id=rotuloOperacao for="idOperacao">Funcionalidade / Opera��o:</label>
			<select name="idMenu" id="idMenu" class="campoSelect campoHorizontal" style="margin-right: 40px" onchange="habilitarExibir();desabilitarIncluir();">
				<option value="-1">Selecione</option>
				<c:forEach items="${menus}" var="menu">
					<option value="<c:out value="${menu.chavePrimaria}"/>" <c:if test="${alcada.idMenu == menu.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${menu.descricao}"/>
					</option>
				</c:forEach>
		    </select>
		
			<!--<vacess:vacess param="exibirAlcada">
				<input name="btExibir" id="btExibir" value="Exibir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="exibir();" type="button" disabled="disabled">
			</vacess:vacess>-->
			<input name="btExibir" id="btExibir" value="Exibir" class="bottonRightCol2 botaoIncluir" onclick="exibir();" type="button" disabled="disabled">
		</fieldset>
	
		<fieldset class="conteinerBloco">
			<c:if test="${listaCampos ne null}">
				<hr class="linhaSeparadora"/>
				<label class="rotulo" for="idPapel">Perfil:</label>
				<select name="idPapel" id="idPapel" class="campoSelect">
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${papeis}" var="papel">
						<option value="<c:out value="${papel.chavePrimaria}"/>" <c:if test="${alcada.idPapel == papel.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${papel.descricao}"/>
						</option>
					</c:forEach>
				</select><br />
					
				<c:forEach var="campo" items="${listaCampos}">
					<label class="rotulo" for="<c:out value='${campo.nomePropriedade}'/>Inicial" >
					<c:out value="${campo.descricao}"/>:</label>
					
					<label  for="<c:out value='${campo.nomePropriedade}'/>Inicial"> a partir de </label>
					
					<input class="campoTexto campoHorizontal" type="text" id="<c:out value="${campo.nomePropriedade}"/>Inicial" name="<c:out  value="${campo.nomePropriedade}"/>Inicial" maxlength="17" size="14" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,13,2);">
					
					<label  for="<c:out value="${campo.nomePropriedade}"/>Final" style="margin-left: 5px;"> at� </label>
					
					<input class="campoTexto campoHorizontal" type="text" id="<c:out value="${campo.nomePropriedade}"/>Final" name="<c:out value="${campo.nomePropriedade}"/>Final"  maxlength="17" size="14" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,13,2);">
				</c:forEach>
				
				<label class="rotulo" for="dataInicialAlcada">Validade da al�ada:</label>
				<label for="dataInicialAlcada"> a partir de </label>
				<input class="campoData campoHorizontal" type="text" id="dataInicialAlcada" name="dataInicialAlcada" maxlength="10" value="${alcada.dataInicial}">
				<label for="dataFinalAlcada"> at� </label>
				<input class="campoData campoHorizontal" type="text" id="dataFinalAlcada" name="dataFinalAlcada" maxlength="10" value="${alcada.dataFinal}">
			</c:if>
		</fieldset>
	
		<hr class="linhaSeparadoraPesquisa"/>
		
		<c:if test="${listaAlcadas ne null}">
			<fieldset class="conteinerBloco">
			    <legend class="conteinerBlocoTitulo">Al�adas</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="${listaAlcadas}" sort="list" id="alcada" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirAlcada">
			        <display:column sortable="true" sortProperty="papel" title="Papel">
			        	<a href="javascript:detalharAlcada('<c:out value='${alcada.descricaoPapel}'/>','<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy" />','<fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy" />')"><span class="linkInvisivel"></span>
		            		<c:out value="${alcada.descricaoPapel}"/>
		            	</a>
			        </display:column>
			        <display:column sortable="true" sortProperty="dataInicial" title="Validade da al�ada" style="width: 50%">
			        	<a href="javascript:detalharAlcada('<c:out value='${alcada.descricaoPapel}'/>','<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy" />','<fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy" />')"><span class="linkInvisivel"></span>
			        		<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy" /> - <fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy" /> 
			        	</a>
			        </display:column>
		    	</display:table>
		    </fieldset>
		</c:if>
	
		<fieldset class="conteinerBotoes">
			<vacess:vacess param="incluirAlcada">
				<input name="btIncluir" id="btIncluir" value="Salvar" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
			</vacess:vacess>
			<!-- Remover da vers�o final 
			<input name="btIncluir" id="btIncluir" value="Salvar" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
			-->
			<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
		</fieldset>
	</fieldset>
</form> 