<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Al�ada<a class="linkHelp" href="<help:help>/inclusoalteraodasaladas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span>.<br />

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/selectbox.js"></script>

<script lang="javascript">
	
	$(document).ready(function(){
		$(".campoData").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});

		// Dialog			
		$("#detalheAlcada").dialog({
			autoOpen: false,
			width: 500,
			modal: true,
			minHeight: 120,
			resizable: false
		});
	});
		

	function alterar(){

		document.forms[0].dataInicialHidden.value = document.forms[0].dataInicialAlcada.value;
		document.forms[0].dataFinalHidden.value = document.forms[0].dataFinalAlcada.value;
		document.forms[0].funcionalidadeHidden.value = '<c:out value="${alcadaVO.descricaoFuncionalidade}"/>';
		document.forms[0].papelHidden.value = '<c:out value="${alcadaVO.descricaoPapel}"/>';
		document.forms[0].idMenu.value = '<c:out value="${alcadaVO.idMenu}"/>';

		submeter("controleAlcadaForm", "alterarAlcada");
	}

	function cancelar(){
		submeter("controleAlcadaForm", "pesquisarAlcada");
	}
	
	function init(){
	}

	function detalharAlcada(descricaoPapel,dataInicial,dataFinal){

		if (descricaoPapel != '' && dataInicial != '' && dataFinal != ''){
			AjaxService.obterDetalhesAlcada(descricaoPapel, dataInicial, dataFinal , {
				callback: function(listaAlcadas) {	           		
		       		if(listaAlcadas != null){
		       			var cont = 0;
		       			var inner = '';
		       			$('#detalheAlcada').html('');
		       			inner = inner + '<label class="rotulo">Perfil:</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">'+ descricaoPapel + '</label>';

		       			$("#detalheAlcada").append(inner);
		       			
		       			for (key in listaAlcadas) {
			       			var alcada = listaAlcadas[cont];
			       			var valorInicial = parseFloat(alcada[1]);
			       			var valorFinal = parseFloat(alcada[2]);
			       			inner = '';	
			       			inner = inner + '<label class="rotulo">'+ alcada[0] + ':</label>';
			       			if(!isNaN(valorInicial)){
			       				inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">a partir de ' + alcada[1] + '</label>';
							}
							if(!isNaN(valorFinal)){
			       				inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">at� '+ alcada[2] + '</label>';
			       			}
			       			//inner = inner + '<label  id=rotuloCampo>'+ valorFinal + '</label>';
			       			$("#detalheAlcada").append(inner);
			       			
			       			cont++;
		       			}
		       			inner = '';
		       			inner = inner + '<label class="rotulo">Validade da al�ada:</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">a partir de ' + dataInicial + '</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">at� '+ dataFinal + '</label>';
		       			//inner = inner + '<label  id=rotuloCampo>'+ dataFinal + '</label>';
		       			$("#detalheAlcada").append(inner);
		           	}
	    		}, async:false}
    		);
		}
		$("#detalheAlcada").dialog('open');
		
	}

	addLoadEvent(init);
</script>



<form method="post" action="alterarAlcada" name="controleAlcadaForm" id="controleAlcadaForm"> 
	
	<div style="display: none" id="detalheAlcada" title="Al�ada" style="text-align: left;">

	</div>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarAlcada">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="descricaoPapel" type="hidden" id="descricaoPapel">
	<input name="dataInicial" type="hidden" id="dataInicial">
	<input name="dataFinal" type="hidden" id="dataFinal">
	<input name="funcionalidadeHidden" type="hidden" id="funcionalidadeHidden">
	<input name="papelHidden" type="hidden" id="papelHidden">
	<input name="dataInicialHidden" type="hidden" id="dataInicialHidden">
	<input name="dataFinalHidden" type="hidden" id="dataFinalHidden">
	<input name="idMenu" type="hidden" id="idMenu" value="${alcadaVO.idMenu}">
	
	<fieldset id="conteinerIncluirAlterarAlcada" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<label class="rotulo" id=rotuloOperacao for="idOperacao">Funcionalidade / Opera��o:</label>
			<label for="descricaoFuncionalidade"><c:out value="${alcadaVO.descricaoFuncionalidade}"/></label>
		</fieldset>
		
		<c:if test="${listaAlcadasAlteracao ne null}">
		<fieldset class="conteinerBloco">
			<hr class="linhaSeparadora"/>
			<label class="rotulo" for="idPapel">Perfil:</label>
			<label for="descricaoPapel"><c:out value="${alcadaVO.descricaoPapel}"/></label>
			<label class="rotulo" for="habilitado">Situa��o:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoAtivo" value="true" <c:if test="${alcada.habilitado eq 'true'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoInativo" value="false" <c:if test="${alcada.habilitado eq 'false'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoInativo">Inativo</label>			
			<br />
		
			<c:set var="i" value="0" />
			<c:forEach var="alcadaVO" items="${listaAlcadasAlteracao}">
				<label class="rotulo" for="valorInicial${i}" ><c:out value="${alcadaVO.coluna.descricao}"/>:</label>
				<label for="valorInicial${i}" > a partir de </label>
				<input class="campoText campoHorizontal" type="text"  id="valorInicial${i}" name="listaValoresIniciais" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,13,2);" maxlength="17" size="14" value="<c:out value="${alcadaVO.valorInicialFormatado}"/>">
				<label for="valorFinal${i}" style="margin-left: 5px;"> at� </label>
				<input class="campoText campoHorizontal" type="text" id="valorFinal${i}" name="listaValoresFinais" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,13,2);" maxlength="17" size="14" value="<c:out value="${alcadaVO.valorFinalFormatado}"/>">
				<c:set var="i" value="${i+1}"/>
			</c:forEach>
		
			<label class="rotulo" for="dataInicialAlcada" >Validade da al�ada:</label>
			<label for="dataInicialAlcada" > a partir de </label>
			<input class="campoData campoHorizontal" type="text" id="dataInicialAlcada" name="dataInicialAlcada" maxlength="10" value="<fmt:formatDate value='${alcada.dataInicial}' pattern='dd/MM/yyyy' />">
			<label for="dataFinalAlcada" > at� </label>
			<input class="campoData campoHorizontal" type="text" id="dataFinalAlcada" name="dataFinalAlcada" maxlength="10" value="<fmt:formatDate value='${alcada.dataFinal}' pattern='dd/MM/yyyy' />">
		</fieldset>
		</c:if>
	
		<c:if test="${listaAlcadas ne null}">
			<hr class="linhaSeparadoraPesquisa"/>
		    <fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Al�adas</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="${listaAlcadas}" sort="list" id="alcada" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirAlterarAlcada">
					<display:column sortable="true" sortProperty="papel" title="Papel">
						<a href="javascript:detalharAlcada('<c:out value='${alcada.descricaoPapel}'/>','<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy" />','<fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy" />')"><span class="linkInvisivel"></span>
		            		<c:out value="${alcada.descricaoPapel}"/>
		            	</a>
			        </display:column>
			        <display:column sortable="true" sortProperty="dataInicial" title="Validade da al�ada" style="width: 50%">
			        	<a href="javascript:detalharAlcada('<c:out value='${alcada.descricaoPapel}'/>','<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy" />','<fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy" />')"><span class="linkInvisivel"></span>
			        		<fmt:formatDate value="${alcada.dataInicial}" pattern="dd/MM/yyyy" /> - <fmt:formatDate value="${alcada.dataFinal}" pattern="dd/MM/yyyy" /> 
			        	</a>
			        </display:column>
		    	</display:table>
			</fieldset>
		</c:if>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
		
		<vacess:vacess param="exibirAlterarAlcada">
			<input name="btAlterar" id="btAlterar" value="Salvar" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="alterar();" type="button">
		</vacess:vacess>
	</fieldset>

</form> 