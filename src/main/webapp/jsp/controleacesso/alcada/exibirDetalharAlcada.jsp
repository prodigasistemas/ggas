<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Al�ada<a class="linkHelp" href="<help:help>/detalhamentodasaladas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span>.<br />

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/selectbox.js"></script>

<script language="javascript">
	
	$(document).ready(function(){
		$(".campoData").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});

		// Dialog			
		$("#detalheAlcada").dialog({
			autoOpen: false,
			width: 500,
			modal: true,
			minHeight: 120,
			resizable: false
		});
	});
		

	function alterar(){

		submeter("controleAlcadaForm", "exibirAlterarAlcada");
	}

	function init(){
	}

	function detalharAlcada(descricaoPapel,dataInicial,dataFinal){

		if (descricaoPapel != '' && dataInicial != '' && dataFinal != ''){
			AjaxService.obterDetalhesAlcada(descricaoPapel, dataInicial, dataFinal , {
				callback: function(listaAlcadas) {	           		
		       		if(listaAlcadas != null){
		       			var cont = 0;
		       			var inner = '';
		       			$('#detalheAlcada').html('');
		       			inner = inner + '<label class="rotulo" id=rotuloCampo>Perfil:</label>';
		       			inner = inner + '<label id=rotuloCampo style="margin-left: 5px;">'+ descricaoPapel + '</label>';

		       			$("#detalheAlcada").append(inner);

		       			for (key in listaAlcadas) {
			       			var alcada = listaAlcadas[cont];
			       			var valorInicial = parseFloat(alcada[1]);
			       			var valorFinal = parseFloat(alcada[2]);
			       			inner = '';	
			       			inner = inner + '<label class="rotulo">' + alcada[0] + ':</label>';
							if(!isNaN(valorInicial)){
			       				inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">a partir de ' + alcada[1] + '</label>';
							}
							if(!isNaN(valorFinal)){
			       				inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">at� '+ alcada[2] + '</label>';
							}
			       			//inner = inner + '<label  id=rotuloCampo>'+ valorFinal + '</label>';
			       			$("#detalheAlcada").append(inner);
			       			
			       			cont++;
		       			}
		       			
		       			inner = '';
		       			inner = inner + '<label class="rotulo">Validade da al�ada:</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">a partir de ' + dataInicial + '</label>';
		       			inner = inner + '<label class="itemDetalhamento" style="margin-left: 5px;">at� '+ dataFinal + '</label>';
		       			$("#detalheAlcada").append(inner);
		           	}
	    		}, async:false}
    		);
		}
		$("#detalheAlcada").dialog('open');
		
	}

	function voltar() {
		submeter("controleAlcadaForm", "pesquisarAlcada");
	}

	addLoadEvent(init);
</script>



<form method="post" action="pesquisarAlcada" name="controleAlcadaForm" id="controleAlcadaForm"> 
	
	<div style="display: none" id="detalheAlcada" title="Al�ada" style="text-align: left;">

	</div>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarAlcada">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="descricaoPapel" type="hidden" id="descricaoPapel">
	<input name="dataInicial" type="hidden" id="dataInicialAlcada" value="${dataInicialAlcada}">
	<input name="dataFinal" type="hidden" id="dataFinalAlcada" value="${dataFinalAlcada}">
	<input name="idMenu" type="hidden" id="idMenu" value="${alteracaoAlcadaVO.menu.chavePrimaria}">
	<input name="idPapel" type="hidden" id="idPapel" value="${alteracaoAlcadaVO.papel.chavePrimaria}">
	<input name="isDetalhar" type="hidden" id="isDetalhar" value="true">
	
	<fieldset id="conteinerDetalharAlcada" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<label class="rotulo" id=rotuloOperacao for="idOperacao">Funcionalidade / Opera��o:</label>
			<label for="descricaoFuncionalidade"><c:out value="${alteracaoAlcadaVO.menu.descricao}"/></label>
		</fieldset>
		
		<c:if test="${listaAlcadasAlteracao ne null}">
			<hr class="linhaSeparadora"/>
			<fieldset class="conteinerBloco">
				<label class="rotulo" for="idPapel">Perfil:</label>
				<label class="itemDetalhamento" for="descricaoPapel"><c:out value="${alteracaoAlcadaVO.papel.descricao}"/></label>
				<br />
				
				<c:set var="i" value="0" />
				<c:forEach var="alcadaVO" items="${listaAlcadasAlteracao}">
					<label class="rotulo"><c:out value="${alcadaVO.coluna.descricao}"/>:</label>
					<c:if test="${alcadaVO.alcada ne null and !empty alcadaVO.alcada.valorInicial}">
						<label class="itemDetalhamento" style="margin-left: 5px;">a partir de <c:out value="${alcadaVO.alcada.valorInicialFormatado}"/></label>
					</c:if>
					<c:if test="${alcadaVO.alcada ne null and !empty alcadaVO.alcada.valorFinal}">
						<label class="itemDetalhamento" style="margin-left: 5px;">at� <c:out value="${alcadaVO.alcada.valorFinalFormatado}"/></label>
					</c:if>
					<c:set var="i" value="${i+1}"/>
				</c:forEach>
				
				<label class="rotulo">Validade da al�ada:</label>
				<label class="itemDetalhamento" style="margin-left: 5px;">a partir de <fmt:formatDate value="${alteracaoAlcadaVO.alcada.dataInicial}" pattern="dd/MM/yyyy"/></label>
				<label class="itemDetalhamento" style="margin-left: 5px;">at� <fmt:formatDate value="${alteracaoAlcadaVO.alcada.dataFinal}" pattern="dd/MM/yyyy"/></label>
			</fieldset>
		</c:if>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<!-- Remover da vers�o final
		<input name="btVoltar" id="btVoltar" value="Cancelar" class="bottonRightCol bottonLeftColUltimo" onclick="voltar();" type="button">
		<input name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoGrande1 botaoAlterar" onclick="alterar();" type="button">
		-->
		
		<input name="btVoltar" id="btVoltar" value="Cancelar" class="bottonRightCol bottonLeftColUltimo" onclick="voltar();" type="button">
		
		<vacess:vacess param="exibirAlterarAlcada">
			<input name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterar()" type="button">
		</vacess:vacess>
	</fieldset>
</form> 