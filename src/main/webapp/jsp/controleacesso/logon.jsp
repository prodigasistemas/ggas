<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<script type="text/javascript">
	$(document).ready(function(){
	
		$("#login").focus();
		
		// Dialog			
		$("#recuperarSenha").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Enviar" : function() {
					document.forms[0].elements["loginHidden"].value = document.getElementById("loginRecuperarSenha").value;
					submeter("0", "recuperarSenha");
					
				}
			},
			resizeble: false,
			draggable: false
		});

		$("#alterarPrimeiraSenha").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Enviar" : function() {
					document.forms[0].elements["senhaAtualHidden"].value = document.getElementById("senhaAtual").value;
					document.forms[0].elements["senhaNovaHidden"].value = document.getElementById("novaSenha1").value;
					document.forms[0].elements["confirmacaoSenhaNovaHidden"].value = document.getElementById("novaSenha2").value;
					submeter("0", "alterarPrimeiraSenha");
					
				}
			},
			resizeble: false,
			draggable: false
		});
		
	});

	function init () {
		var primeiroAcesso = document.forms[0].elements["primeiroAcesso"].value;
		if (primeiroAcesso == "true"){
			exibirJDialog("#alterarPrimeiraSenha");
		}
	}

	function logarUsuarioDominio() {
		submeter("0", "logarUsuarioDominio");
	}
	addLoadEvent(init);
	

</script>

<div id="conteinerLogin">
	
	<div style="display: none" id="recuperarSenha" title="Recuperar Senha">
		<label for="login" class="labelLogin rotuloVertical" style = "margin-right: -40px;">Login:</label>
		<input name="loginRecuperarSenha" id="loginRecuperarSenha" class="campoTexto" size="20" maxlength="20" type="text">
	</div>
	
	<div style="display: none" id="alterarPrimeiraSenha" title="Alterar Senha" style="text-align: left;">
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Senha Atual:</label>
		<input class="campoTexto" type="password" id="senhaAtual" name="senhaAtual" maxlength="20" size="20" />
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Nova Senha:</label>
		<input class="campoTexto" type="password" id="novaSenha1" name="novaSenha1" maxlength="20" size="20" />
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Nova Senha:</label>
		<input class="campoTexto" type="password" id="novaSenha2" name="novaSenha2" maxlength="20" size="20" />
	</div>
	
	<c:if test="${not empty mensagemTela}">
   		<h1 class="mensagemErro"><c:out value='${mensagemTela}' /></h1>  
    </c:if>
	    
	
	<form method="post" action="efetuarLogon" style="">
	<input name="acao" type="hidden" id="acao" value="efetuarLogon">
			<input name="loginHidden" id="loginHidden" type="hidden">
			<input name="senhaAtualHidden" id="senhaAtualHidden" type="hidden">
			<input name="senhaNovaHidden" id="senhaNovaHidden" type="hidden">
			<input name="confirmacaoSenhaNovaHidden" id="confirmacaoSenhaNovaHidden" type="hidden">
			<input name="primeiroAcesso" id="primeiroAcesso" type="hidden" value="${primeiroAcesso}">
	<div class="ggas_login">
  <table style="width: 100%; padding: 0; border-spacing: 0;">
    <tr>
      <td align="center"><img src="images/logo_login.png" width="201" height="57" alt="GGAS" /></td>
    </tr>
    <tr>
      <td align="center" class="ggas_arial_bold_15">Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s</td>
    </tr>
    <tr>
      <td align="center"><form name="form1" method="post" action="">
      	  <table style="width: 100%; padding: 0; border-spacing: 0; text-align: center;">
            <tr>
              <td align="center"><img src="images/ico_perfil.png" class="ico_login_" width="30" height="30" />
                <label for="textfield3"></label>
                <input type="text" name="login" id="login" value="<c:out value='${usuario.login}'/>"></td>
              <td align="center"><label for="senha"></label>
                <img src="images/ico_pass.png" width="30" height="30" class="ico_login_" />
                <input type="password" name="senha" id="senha" value="<c:out value='${usuario.senha}'/>"></td>
            </tr>
            <tr>
            	<td colspan="2" align="center" class="ggas_space_bt_login">
              		<input type="submit" name="button3" id="button3" value="Logon" class="ggas_botao">
<!--               		<input type="button" name="usurDominio" id="usurDominio" value="Login integrado" class="ggas_botao" onclick="logarUsuarioDominio()"> -->
           		</td>
            </tr>
          	<tr>
              <td colspan="2" align="center"><a onclick="exibirJDialog('#recuperarSenha')" href="#">esqueci a senha</a></td>
            </tr>
          </table>
        </form></td>
    </tr>
  </table>
</div>
		

	</form> 
</div>