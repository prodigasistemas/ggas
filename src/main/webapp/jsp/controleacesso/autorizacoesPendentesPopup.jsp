<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<jsp:useBean id="controladorAlcada" class="br.com.ggas.controleacesso.impl.ControladorAlcadaImpl" />

<script>
		
	$(document).ready(function(){		
		<c:if test="${exibirPendencias eq true}">
		exibirPaginaPendencias();
	    </c:if>
	});

	function exibirPaginaPendencias() {
		var idUsuario = '<c:out value="${sessionScope.usuarioLogado.chavePrimaria}"/>';
		var conteiner = "conteinerPendencias";
		var exibirPendenciasPopup = '<c:out value="${sessionScope.exibirPendenciasPopup}"/>';
		exibirPendencias(idUsuario, conteiner, exibirPendenciasPopup);
		
		<c:set var="exibirPendenciasPopup" value="true" scope="session" />  

	}

	function exibirPendencias(idUsuario, conteiner, exibirPendenciasPopup) {
		exibirIndicador();
		listarAutorizacoesPendentes(idUsuario, conteiner);
		esconderIndicador();
		if(exibirPendenciasPopup != "false"){
			exibirJDialog("#pendenciasPopup");
		}
		
	}

	function listarAutorizacoesPendentes(idUsuario, conteiner) {
		AjaxService.obterListaAutorizacoesPendentes(idUsuario,  
			{ 
	       		callback: function(listaPendencias) {
					// Limpa o conte�do anterior da div "conteinerPendencias"
					$("#"+conteiner).html('');
					var inner = '<p class="orientacaoInicial">Nenhuma pend�ncia foi encontrada.</p>';
					
					if(listaPendencias != null && listaPendencias.length > 0){
		       			var cont = 0;
		       			var pendenciaLinha = "odd"
		       			for (key in listaPendencias) {
			       			var pendencia = listaPendencias[cont];	
			       			inner = '<p>';
			       			inner = inner + '<a class="'+pendenciaLinha+'" href="javascript:' + pendencia[2] + '(\'' + pendencia[3] + '\'';

							if(pendencia[4] != "" && pendencia[5] != ""){
								inner = inner + ',\'' + pendencia[4] + '\',\'' + pendencia[5] + '\'';
							}
			       			
				       		inner = inner + ');">' + pendencia[0] + ' ' + pendencia[1] + '</a>';
				       		inner += '</p>';
							
				       		$("#"+conteiner).append(inner);
			       			cont++;
			       			if(pendenciaLinha == "odd"){
			       				pendenciaLinha = "even"
							} else {
								pendenciaLinha = "odd"
							}
		       			}
		       			var containerAutorizacaoPendente = $("#"+conteiner);
		       			var paragrafosAutorizacaoPendente = containerAutorizacaoPendente.children('p');
		       			paragrafosAutorizacaoPendente.css('margin-bottom','5px');
		           	}else{
		           		$("#"+conteiner).append(inner);
		           	}
	   	  		}, 
	   	 		async: false
	   		}
	   	);
	}
	
	function exibirTarifa(chavesPrimarias){
		$("#chavesPrimarias").val(chavesPrimarias);
		submeter('autorizacoesPendentesForm', 'pesquisarTarifa');
	}

	function exibirParcelamento(chavesPrimarias, dataInicial, dataFinal){
		$("#chavesPrimarias").val(chavesPrimarias);
		$("#dataInicial").val(dataInicial);
		$("#dataFinal").val(dataFinal);		
		submeter('autorizacoesPendentesForm', 'pesquisarParcelamentoDebitos');
	}

	function exibirCreditoDebito(chavesPrimarias){
		$("#chavesPrimarias").val(chavesPrimarias);
		var form = document.getElementById('autorizacoesPendentesForm');
		form.action = 'pesquisarCreditoDebito';
		form.submit();
	}	
	
	function exibirSupervisorioMedicaoDiaria(chavesPrimarias){
		$("#chavesPrimarias").val(chavesPrimarias);
		document.getElementById('tipoSupervisorio').value = 'diaria';
		var form = document.getElementById('autorizacoesPendentesForm');
		form.action = 'pesquisarMedicaoSupervisorioPendentes#ancoraPesquisaMedicaoSupervisorio';
		form.submit();
	}
	
	
	
	
	
	function exibirSupervisorioMedicaoHoraria(chavesPrimarias){
		$("#chavesPrimarias").val(chavesPrimarias);
		document.getElementById('tipoSupervisorio').value = 'horaria';
		var form = document.getElementById('autorizacoesPendentesForm');
		form.action = 'pesquisarMedicaoSupervisorioPendentes#ancoraPesquisaMedicaoSupervisorio';
		form.submit();
	}
	
	function exibirSolicitacaoConsumo(){		
		
		submeter('autorizacoesPendentesForm', 'exibirPesquisaProgramacaoConsumo');
	}
	
</script>

<div id="pendenciasPopup" title="" style="display: none">
	
	<form method="post" id="autorizacoesPendentesForm" name="autorizacoesPendentesForm">
		<input name="isAutorizacao" type="hidden" id="isAutorizacao" value="true"/>
		<input name="status" type="hidden" id="status" value="${controladorAlcada.codigoStatusPendente}"/>
		<input name="habilitado" type="hidden" id="habilitado" value="true"/>
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value=""/>	
		<input name="tipoSupervisorio" type="hidden" id="tipoSupervisorio" value=""/>	
		<input name="dataInicial" type="hidden" id="dataInicial" value=""/>
		<input name="dataFinal" type="hidden" id="dataFinal" value=""/>
	
		<div id="conteinerPendencias"></div>
    </form>
	
</div>