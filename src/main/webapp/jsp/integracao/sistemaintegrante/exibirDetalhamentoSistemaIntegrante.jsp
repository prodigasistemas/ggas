<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Sistema Integrante<a href="<help:help>/cadastrodoclientedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form method="post" action="exibirDetalhamentoSistemaIntegrante" enctype="multipart/form-data"  id="sistemaIntegranteForm" name="sistemaIntegranteForm">

<script>
	
	function voltar(){	
		submeter("sistemaIntegranteForm",  "pesquisarSistemaIntegrante");
	}
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter('sistemaIntegranteForm', 'exibirAlteracaoSistemaIntegrante');
	}
	
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoSistemaIntegrante">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${integracaoVO.chavePrimaria}">
<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${integracaoVO.chavesPrimarias}">

<fieldset class="detalhamento">
	<fieldset id="detalhamentoGeral" class="conteinerBloco">
		
		<fieldset id="detalhamentoSistemaIntegrante" class="colunaEsq">
			<label class="rotulo" id="rotuloSigla">Sigla:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${integracaoVO.sigla}"/></span><br />
			<label class="rotulo" id="rotuloNome">Nome:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${integracaoVO.nome}"/></span><br />
			<label class="rotulo" id="rotuloDescricao">Descri��o:</label>
			<span class="itemDetalhamento"><c:out value="${integracaoVO.descricao}"/></span><br />
			<label class="rotulo" id="rotuloIndicador">Indicador de Uso:</label>
			<span class="itemDetalhamento" id="empresaServicoPrestado"> 
				<c:choose>
					<c:when test="${integracaoVO.habilitado == 'true'}">Ativo</c:when>
					<c:otherwise>Inativo</c:otherwise>
				</c:choose>
			</span>
		</fieldset>
		
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Integra��es</legend>		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaIntegracoes" sort="list" id="integracao" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	        
	        <display:column property="integracaoFuncao.nome" sortable="false" title="Fun��es" style="width: 250px"/> 
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${integracao.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
						
	   	</display:table>	   	
	</fieldset>
</fieldset>	
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAlteracaoSistemaIntegrante">    
    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>

</form:form>
