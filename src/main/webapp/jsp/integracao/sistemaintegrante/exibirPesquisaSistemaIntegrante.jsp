<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Pesquisar Sistema Integrante<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarSistemaIntegrante" id="sistemaIntegranteForm" name="sistemaIntegranteForm">

	<script language="javascript">
	
	function removerSistemaIntegrante(){
		var selecao = verificarSelecao();
		
		
		if (selecao == true) {	
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('sistemaIntegranteForm', 'removerSistemaIntegrante');
			}
	    }
	}
	
	function alterarSistemaIntegrante(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('sistemaIntegranteForm', 'exibirAlteracaoSistemaIntegrante');
	    }

	}
	
	function detalharSistemaIntegrante(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("sistemaIntegranteForm", "exibirDetalhamentoSistemaIntegrante");
	}
	
	function incluir() {
		location.href = '<c:url value="/exibirInclusaoSistemaIntegrante"/>';
	}
	
	function pesquisar() {
		location.href = '<c:url value="/pesquisarSistemaIntegrante"/>';
		
	}
	
	function limparFormulario(){
		document.sistemaIntegranteForm.sigla.value = "";
		document.sistemaIntegranteForm.nome.value = "";
		document.forms[0].habilitado[0].checked = true;
	}
	
</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarSistemaIntegrante">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="funcionarioCol2" class="coluna">
			<label class="rotulo" id="rotuloSigla" for="sigla" >Sigla:</label>
			<input class="campoTexto" type="text" name="sigla" id="sigla" maxlength="3" size="10" value="${integracaoVO.sigla}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/><br />
				
			<label class="rotulo" id="rotuloNome" for="nome" >Nome:</label>
			<input class="campoTexto" type="text" name="nome" id="nome" maxlength="30" size="30" value="${integracaoVO.nome}" onkeyup="letraMaiuscula(this);"/><br />
						
		</fieldset>
		
		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
									
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${integracaoVO.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${integracaoVO.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty integracaoVO.habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarSistemaIntegrante">		
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" onclick="pesquisar()" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${sistemasIntegrantes ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="sistemasIntegrantes" sort="list" id="sistemaIntegrante" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarSistemaIntegrante">
	        <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" id="chavesPrimariasCheckbox" name="chavesPrimarias" value="${sistemaIntegrante.chavePrimaria}">
	        </display:column>
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${sistemaIntegrante.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        <display:column sortable="true" title="Sigla" sortProperty="sigla" style="text-align: center;">
	        	<a href="javascript:detalharSistemaIntegrante(<c:out value='${sistemaIntegrante.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${sistemaIntegrante.sigla}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Nome" sortProperty="nome" style="text-align: center;">
	        	<a href="javascript:detalharSistemaIntegrante(<c:out value='${sistemaIntegrante.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${sistemaIntegrante.nome}"/>
	            </a>
	        </display:column>
	        
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty sistemasIntegrantes}">
  			<vacess:vacess param="exibirAlteracaoSistemaIntegrante">
  				<input id="botaoAlterar" name="botaoAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarSistemaIntegrante()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerSistemaIntegrante">
				<input id="botaoRemover" name="botaoRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerSistemaIntegrante()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoSistemaIntegrante">
   			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>
	
</form:form>
