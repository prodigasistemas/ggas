<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %> 

<script>
$().ready(function(){
    $("input#sigla").keyup(removeExtra).blur(removeExtra);
});

function limparFormulario(){

	//Sistema Integrante - dados comuns
	document.sistemaIntegranteForm.sigla.value = "";
	document.sistemaIntegranteForm.nome.value = "";
	document.sistemaIntegranteForm.descricao.value = "";
	document.getElementById("descricao").value = "";
}

function cancelar(){
	location.href = '<c:url value="/exibirPesquisaSistemaIntegrante"/>';
}

</script>

<h1 class="tituloInterno">Incluir Sistema Integrante<a href="<help:help>/cadastroclienteinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form:form method="post" action="incluirSistemaIntegrante" id="sistemaIntegranteForm" name="sistemaIntegranteForm"> 
	<input name="acao" type="hidden" id="acao" value="incluirSistemaIntegrante"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${integracaoVO.chavePrimaria}"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	
	<fieldset id="conteinerSistemaIntegrante" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="clienteCol1" class="coluna">
			<label class="rotulo campoObrigatorio" id="rotuloSigla" for="sigla"><span class="campoObrigatorioSimbolo">* </span>Sigla:</label>
			<input class="campoTexto" type="text" id="sigla" name="sigla" maxlength="3" size="10" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${integracaoVO.sigla}"><br />
			
			<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
			<input class="campoTexto" type="text" id="nome" name="nome" maxlength="30" size="30"  onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${integracaoVO.nome}"><br />
			
		</fieldset>
		
		<fieldset id="clienteCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o:</label>
			<textarea class="campoTexto" id="descricao" name="descricao" rows="2" cols="37" maxlength="255" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" >${integracaoVO.descricao}</textarea><br />
		</fieldset>
	</fieldset>


<fieldset class="conteinerBotoes"> 
	<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="incluirSistemaIntegrante">
    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit">
    </vacess:vacess>
 </fieldset>
</form:form> 