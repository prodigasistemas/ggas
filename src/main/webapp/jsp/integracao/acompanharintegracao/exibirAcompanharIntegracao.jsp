<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">
	
$(document).ready(function(){
	mudarStatusFuncao();
});

	
function detalharTabela(chavePrimaria,nome) {
	document.forms[0].chavePrimaria.value = chavePrimaria;
	submeter("acompanharIntegracaoForm", "exibirDetalhamentoAcompanharIntegracao");
}

function carregarFuncoes(elem) {	
	var codSistema = elem.value;
	carregarFuncoesPorSistema(codSistema);
	mudarStatusFuncao();
}

function mudarStatusFuncao() {
	var idSistema= document.getElementById("idIntegracaoSistema");	

	if (idSistema.value != -1) {
		document.getElementById("idFuncao").disabled = false;
	} else {
		document.getElementById("idFuncao").disabled = true;
	}
}


function carregarFuncoesPorSistema(codSistema){
  	var selectFuncao = document.getElementById("idFuncao");
  	var idFuncao = "${integracaoVO.idFuncao}";		  	

  	selectFuncao.length=0;
  	var novaOpcao = new Option("Selecione","-1");
  	selectFuncao.options[selectFuncao.length] = novaOpcao;
   	AjaxService.listarFuncaoPorSistema(codSistema,
   			function(funcoes) {            		      		         		
           	for (key in funcoes){
                var novaOpcao = new Option(funcoes[key], key);
                if (key == idFuncao){
                	novaOpcao.selected = true;
                }
                selectFuncao.options[selectFuncao.length] = novaOpcao;		            			            	
        	}
        	ordernarSelect(selectFuncao)
    	}
    );	  	
}







	
function pesquisaAcompanharIntegracao() {
	$("#botaoPesquisar").attr('enabled','enabled');
	submeter('acompanharIntegracaoForm', 'pesquisaAcompanharIntegracao');
}	
	
	

	
</script>

<h1 class="tituloInterno">Acompanhar Integra��o<a href="<help:help>/consultadosclientes.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para acompanhar a integra��o, selecione o <span class="destaqueOrientacaoInicial">Sistema integrante</span> e sua respectiva <span class="destaqueOrientacaoInicial">Fun��o</span>, se existe erro ou n�o e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />


<form:form method="post" action="pesquisaAcompanharIntegracao" name="acompanharIntegracaoForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="chave" type="hidden" id="chave" value="">
	<input name="nomeTabela" type="hidden" id="nomeTabela" value="">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="funcionarioCol2" class="coluna">
				<label class="rotulo campoObrigatorio" for="idIntegracaoSistema"><span class="campoObrigatorioSimbolo">*</span>Sistema Integrante:</label>
				<select name="idIntegracaoSistema" id="idIntegracaoSistema" class="campoSelect campo2Linhas" onchange="carregarFuncoes(this)">
			    	<option value="-1">Selecione</option>
					<c:forEach items="${listarIntegracaoSistema}" var="acaoIntegacaoSistema">
						<option value="<c:out value="${acaoIntegacaoSistema.chavePrimaria}"/>" <c:if test="${integracaoVO.idIntegracaoSistema == acaoIntegacaoSistema.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${acaoIntegacaoSistema.nome}"/>
						</option>		
				    </c:forEach>		
			    </select><br />
			    
			    <label class="rotulo campoObrigatorio" id="rotuloEnderecoReferencia" for="idFuncao">Fun��o:</label>
				<select name="idFuncao" id="idFuncao" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listarIntegracaoSistemaFuncao}" var="integracaoSistemaFuncao">
						<option value="<c:out value="${integracaoSistemaFuncao.chavePrimaria}"/>" <c:if test="${integracaoVO.idFuncao == integracaoSistemaFuncao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${integracaoSistemaFuncao.integracaoFuncao.descricao}"/>
						</option>		
					</c:forEach>		
				</select>
				<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campo obrigat�rio para Acompanhar Integra��o.</p>
			</fieldset>
			
			
			<fieldset class="colunaFinal">
				<label class="rotulo" id="apenasComErro">Apenas com erro?</label>
					<input class="campoRadio" type="radio" value="true" name="indicadorCondominioAmbos" id="indicadorCondominioAmbos" <c:if test="${integracaoVO.indicadorCondominioAmbos}">checked</c:if>>
					<label for="apenasComErroSim" class="rotuloRadio">Sim</label>
					<input class="campoRadio" type="radio" value="false" name="indicadorCondominioAmbos" id="indicadorCondominioAmbos" <c:if test="${!integracaoVO.indicadorCondominioAmbos}">checked</c:if>>
					<label for="apenasComErroNao" class="rotuloRadio">N�o</label>			
			</fieldset>
		</fieldset>	
			
		<input name="acao" type="hidden" id="acao" value="pesquisaAcompanharIntegracao"/>	
			
		<fieldset class="conteinerBotoesPesquisarDir">
 			<vacess:vacess param="pesquisaAcompanharIntegracao">
			  <input  name="button" value="Pesquisar" class="bottonRightCol2 botaoGrande1" type="submit">
 		    </vacess:vacess>
	    </fieldset>	
	  
    
		<c:if test="${listaTabela ne null && not empty listaTabela}">
			<hr class="linhaSeparadora1" />
			<fieldset class="conteinerBloco">
				<p class="orientacaoInicial">Selecione um dos itens da <span class="destaqueOrientacaoInicial">Tabela Integra��o</span> para acompanhar seus dados.</p>
				  <legend>Tabelas de Integra��o</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaTabela" sort="list" id="listaTab" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisaAcompanharIntegracao">
			        
			        
			        <display:column title="Nome da tabela" headerClass="tituloTabelaEsq" media="html" style="text-align: left">
			            <a href="javascript:detalharTabela(${listaTab.tabela.chavePrimaria})"><span class="linkInvisivel"></span>
			            	<c:out value="${listaTab.tabela.nome}"/>
			            </a>
			        </display:column>

			        <display:column title="Descri��o da Tabela" headerClass="tituloTabelaEsq" media="html"  style="text-align: left">
			            <a href="javascript:detalharTabela(${listaTab.tabela.chavePrimaria})"><span class="linkInvisivel"></span>
			            	<c:out value="${listaTab.tabela.descricao}"/>
			            </a>
			        </display:column>
			       	
			      
			    </display:table>
			</fieldset>
		</c:if>
	
</form:form> 
