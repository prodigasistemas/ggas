<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">
	
	$(document).ready(function(){
		init();
	});

	function voltar(){

		submeter("acompanharIntegracaoForm","pesquisaAcompanharIntegracao");
	} 	
	
	function salvarAlteracao() {

		submeter("acompanharIntegracaoForm","salvarAlteracao");
	}

	function exportar() {

		submeter("acompanharIntegracaoForm","exportarTabela");
	}
	
	function mudarImagem(chavePrimaria, nomeCampoAlterado, valorCampoAlterado, tipoColuna){

		document.getElementById('imagem' + chavePrimaria).src = '<c:url value="imagens/icon-alert16.gif"/>';
		document.getElementById('imagem' + chavePrimaria).style.visibility='visible';	
		AjaxService.alterarAcompanhamentoIntegracao(chavePrimaria, nomeCampoAlterado, valorCampoAlterado.value, tipoColuna);
	}
	
	function init() {

		var registrosAlterados = [
  		  			                <c:forEach items="${sessionScope.registrosAlterados}" var="item" varStatus="loopStatus">
  		  			                '${item}'<c:if test="${!loopStatus.last}">, </c:if>
  		  			                </c:forEach>
  		  			                ];
  		  			
  		var quantidadeRegistrosAlterados = registrosAlterados.length;
  		
  		if(quantidadeRegistrosAlterados > 0){
  			
  			for (var i = 0; i< quantidadeRegistrosAlterados; i++) {
  				
  				document.getElementById('imagem' + registrosAlterados[i]).src = '<c:url value="imagens/icon-alert16.gif"/>';
  				document.getElementById('imagem' + registrosAlterados[i]).style.visibility='visible';
  			}
  		}
      		
      	var registrosSalvos = [
   	  			                <c:forEach items="${sessionScope.registrosSalvos}" var="item" varStatus="loopStatus">
   	  			                '${item}'<c:if test="${!loopStatus.last}">, </c:if>
   	  			                </c:forEach>
   	  			                ];
   		  			
   		var quantidadeRegistrosSalvos = registrosSalvos.length;
   		if(quantidadeRegistrosSalvos > 0){
   			for (var i = 0; i< quantidadeRegistrosSalvos; i++) {
   				document.getElementById('imagem' + registrosSalvos[i]).src = '<c:url value="imagens/check1.gif"/>';
   				document.getElementById('imagem' + registrosSalvos[i]).style.visibility='visible';
   			}
   		}
	}
	
	function limpar() {

        submeter("acompanharIntegracaoForm","exibirDetalhamentoAcompanharIntegracao");              
    }

	function filtrarTabela() {

		submeter("acompanharIntegracaoForm","filtrarTabela");			
	}
	
	function atualizarLista(campo, valor, valor2){

		AjaxService.atualizarFiltroPesquisaAcompanhementoTabelaIntegracao(campo, valor.value, valor2.value);
	}

	
</script>

<h1 class="tituloInterno">Filtro da Tabela de Integra��o<a href="<help:help>/consultadosclientes.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<form:form method="post" name="acompanharIntegracaoForm" action="exibirDetalhamentoAcompanharIntegracao">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${integracaoVO.chavePrimaria}"/>
	<input name="idIntegracaoSistema" type="hidden" id="idIntegracaoSistema" value="${integracaoVO.idIntegracaoSistema}"/>
	<input name="idFuncao" type="hidden" id="idFuncao" value="${integracaoVO.idFuncao}"/>
		
	<!-- -------------------------------------------------------- -->
	<c:if test="${sessionScope.listaFiltros ne null}">
	<div style="height:150px; overflow:auto">		
		<fieldset class="conteinerBloco">			
				<display:table class="dataTableGGAS" name="${sessionScope.listaFiltros}" sort="list" id="coluna" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoAcompanharIntegracaoPopup" >
					
					<display:column title="Coluna" media="html" style="text-align: left">
					    <c:out value="${coluna.nomeColuna}"/>
					</display:column>					
					<display:column sortable="false" title="Valor Inicial">
						<input class="campoTexto" type="text" value="<c:out value="${coluna.valorColuna}"/>" onchange="atualizarLista('${coluna.nomeColuna}',this,'');">
					</display:column>		
					<display:column sortable="false" title="Valor Final">
						<input class="campoTexto" type="text" value="<c:out value="${coluna.valorColuna2}"/>" onchange="atualizarLista('${coluna.nomeColuna}','',this);">
					</display:column>
			
				</display:table>			
		</fieldset>
	</div>
	</c:if>
	<fieldset class="conteinerBotoes">
		<div>
        	<input class="bottonRightCol2 botaoGrande1" value="Aplicar Filtro" type="button" onclick="filtrarTabela();">
            <input class="bottonRightCol2 botaoGrande1" value="Limpar" type="button" onclick="limpar();">
		</div>
		<br/>                          
    </fieldset>   
    
	
	<c:if test="${sessionScope.listaTabela ne null && not empty sessionScope.listaTabela}">
	
		<h1 class="tituloInternoAcompanharIntegracao">Tabela de Integra��o</h1>
		
		<div style="height:390px; overflow:auto">
			<fieldset class="conteinerBloco">			
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas consultaTabela"  name="${sessionScope.listaTabela}" sort="list" id="tabela" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoAcompanharIntegracao">
				 	<display:column title="Status" style="width: 30px">
						<img id="imagem<c:out value="${tabela[0].valorColuna}"/>"  border="0" style="visibility:hidden">
			 		</display:column>
				 	<c:forEach var="i" begin="0" end="${fn:length(tabela)>0?fn:length(tabela)-1:fn:length(tabela)}">
			         	<display:column sortable="false" title="${tabela[i].nomeColuna}" style="text-align:center">
			         	
			         		<c:choose>
							  <c:when test="${tabela[i].nomePropriedade eq 'chavePrimaria' || tabela[i].nomePropriedade eq 'versao' || tabela[i].nomePropriedade eq 'ultimaAlteracao'}">
									<c:out value="${tabela[i].valorColuna}"/>
							  </c:when>
							  <c:otherwise>
							  		<input style="width: 200px; text-align: right" class="campoTexto" type="text" value="${tabela[i].valorColuna}" onchange="mudarImagem(<c:out value='${tabela[0].valorColuna}'/>,'${tabela[i].nomePropriedade}',this, '${tabela[i].tipoColuna}');"/>
							  </c:otherwise>
							</c:choose>
																	         					            	
			        	</display:column>
			      	</c:forEach>
			    </display:table>		    
			</fieldset>				
		</div>
	</c:if>
	
	 <fieldset class="conteinerBotoes">
		<input name="botaoProcessar" value="Processar" class="bottonRightCol2 botaoGrande1 botaoProcessar" onclick="" type="button">
		<input name="botaoExportar" value="Exportar" class="bottonRightCol2 botaoGrande1 botaoExportar" onclick="exportar();" type="button">
		<input name="botaoIncluir" value="Salvar" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="salvarAlteracao();" type="button">
	 <input name="ButtonVoltar" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
	</fieldset>
	
</form:form> 
