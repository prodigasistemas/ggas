<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script  type="text/javascript">

$(document).ready(function() {

	$("#anoMesReferenciaLido").inputmask("9999/99",{placeholder:"_"});
	$("#anoMesReferenciaLidoMI").inputmask("9999/99",{placeholder:"_"});


	$(".campoData").datepicker({
		changeYear : true,
		showOn : 'button',
		buttonImage: '<c:url value="/imagens/calendario.png"/>',
		buttonImageOnly : true,
		buttonText : 'Exibir Calend�rio',
		dateFormat : 'dd/mm/yy'
	});

	var indicadorPesquisa = '${validaMedicaoSupervisorioVO.indicadorPesquisa}';
	
});

function pesquisar() {
    submeter(
            "medicaoSupervisorioForm", "pesquisarMedicaoSupervisorio");
}



function transferirMedicaoSupervisorioParcial() {
	var selecao = verificarSelecao();
	if (selecao == true) {  
		$('#dataCorte').modal('show');
	}
}

function transferirMedicaoSupervisorio() {
	var selecao = verificarSelecao();
	
	if (selecao == true) {
		$("#chavesPrimarias").attr("name", "");
		submeter("medicaoSupervisorioForm", "transferirMedicaoSupervisorio");
	}
}

function carregarRotas(elem) {
	var idGrupoFaturamento = elem.value;
	var selectRotas = document.getElementById("idsRota");

	selectRotas.length = 0;

	AjaxService.listarRotaPorGrupoFaturamento(idGrupoFaturamento, function(
			rotas) {
		for (key in rotas) {
			var novaOpcao = new Option(rotas[key], key);
			selectRotas.options[selectRotas.length] = novaOpcao;
		}
		ordernarSelect(selectRotas);
	});

}

function carregarOcorrenciasPC(elem) {
	
	var isOcorrencia = elem.value;	
	
	if(isOcorrencia === undefined){
		isOcorrencia = elem;
	}
	
	var selectOcorrenciasPC = document.getElementById("idsOcorrenciaMedicaoPC");
	selectOcorrenciasPC.length = 0;

	AjaxService
	.listarSupervisorioMedicaoAnormalidade(
			isOcorrencia,
			function(ocorrencias) {
				for (key in ocorrencias) {
					var novaOpcao = new Option(ocorrencias[key], key);
					selectOcorrenciasPC.options[selectOcorrenciasPC.length] = novaOpcao;
				}
				ordernarSelect(selectOcorrenciasPC);
			});

}

function carregarOcorrenciasMI(elem) {
	
	var isOcorrencia = elem.value;
	
	if(isOcorrencia === undefined){
		isOcorrencia = elem;
	}
	var selectOcorrenciasMI = document.getElementById("idsOcorrenciaMedicaoMI");
	selectOcorrenciasMI.length = 0;

	AjaxService
	.listarSupervisorioMedicaoAnormalidade(
			isOcorrencia,
			function(ocorrencias) {
				for (key in ocorrencias) {
					var novaOpcao = new Option(ocorrencias[key], key);
					selectOcorrenciasMI.options[selectOcorrenciasMI.length] = novaOpcao;
				}
				ordernarSelect(selectOcorrenciasMI);
			});

}

function carregarSetoresComerciais(elem) {

	var idLocalidade = elem.value;
	if(idLocalidade=="-1") {
		enableAndDisableSetorComercial(true);
		return true;
	} else {
		enableAndDisableSetorComercial(false);
	}
	var selectsetoresComerciais = document.getElementById("idSetorComercial");

	selectsetoresComerciais.length = 0;

	AjaxService.listarSetoresComerciaisPorLocalidade(idLocalidade, function(
			setores) {
		for (key in setores) {
			var novaOpcao = new Option(setores[key], key);
			selectsetoresComerciais.options[selectsetoresComerciais.length] = novaOpcao;
		}
		var novaOpcao = new Option("Selecione", "-1");
		selectsetoresComerciais.options[selectsetoresComerciais.length] = novaOpcao;
		selectsetoresComerciais.value="-1";
		ordernarSelect(selectsetoresComerciais);
	});

}

function enableAndDisableSetorComercial(value) {
	document.getElementById('idSetorComercial').disabled = value;
}

function detalhar(codigoSupervisorioPontoConsumo, anoMesLeitura, numeroCiclo) {             
	document.forms[0].enderecoRemotoParametro.value = codigoSupervisorioPontoConsumo;
	document.forms[0].anoMesReferenciaParametro.value = anoMesLeitura;
	document.forms[0].idCicloParametro.value = numeroCiclo;     
	document.forms[0].consultaBancoTelaSupervisorioDiario.value = true;

	submeter(
			"medicaoSupervisorioForm", "exibirDetalhamentoMedicaoSupervisorioDiario");
}

function desfazerSupervisorio() {
	var selecao = verificarSelecao();
	if (selecao == true) {
		$("#chavesPrimarias").attr("name", "");
		submeter("medicaoSupervisorioForm", "desfazerSupervisorio");
	}
}

function desfazerConsolidarSupervisorio() {
	var selecao = verificarSelecao();
	if (selecao == true) {
		$("#chavesPrimarias").attr("name", "");
		submeter("medicaoSupervisorioForm", "desfazerConsolidarSupervisorio");
	}
}

function pesquisaPontoConsumo(){
	$("#indicadorPesquisa").val("medidorIndependente");
}

function pesquisaMedidorIndependente(){
	
	$("#indicadorPesquisa").val("pontoConsumo");
}

function limparFormulario() {

	//limparFormularioDadosCliente();
	$(":text").val("");
	$("select").val("-1");
	document.getElementById("condominioSim").checked = false;
	document.getElementsByName("indicadorCondominio")[2].checked = true;
	document.getElementById('idSetorComercial').disabled = true;
	var ocorrenciasMedicao = document.getElementById('idsOcorrenciaMedicaoPC');
	
	for (i = 0; i < ocorrenciasMedicao.length; i++) {
		ocorrenciasMedicao.options[i].selected = false;
	}
	
	document.getElementsByName('ocorrenciaPC')[2].checked = true;
	document.getElementById('analisadaNao').checked = true;
	document.getElementById('transferenciaNao').checked = true;
	document.getElementById('ativoSim').checked = true;
	document.getElementById('statusTodas').checked = true;
	
	document.getElementById('idsRota').length = '0';
	

	var ocorrenciasMedicaoMI = document.getElementById('idsOcorrenciaMedicaoMI');
	
	for (i = 0; i < ocorrenciasMedicaoMI.length; i++) {
		ocorrenciasMedicaoMI.options[i].selected = false;
	}
	
	document.getElementsByName('ocorrenciaMI')[2].checked = true;
	document.getElementById('analisadaNaoMI').checked = true;
	document.getElementById('transferenciaNaoMI').checked = true;
	document.getElementById('ativoSimMI').checked = true;
	document.getElementById('statusMI').checked = true;
	

	carregarOcorrenciasPC("");
	carregarOcorrenciasMI("");
	
}

function init() {
	
	var ocorrenciaPCelement = '${validaMedicaoSupervisorioVO.ocorrenciaPC}';
	var ocorrenciaMIelement = '${validaMedicaoSupervisorioVO.ocorrenciaMI}';
	
	if(ocorrenciaPCelement == ''){
		carregarOcorrenciasPC(ocorrenciaPCelement);
	}
	if(ocorrenciaMIelement == ''){
		carregarOcorrenciasMI(ocorrenciaMIelement);
	}
	
	if(document.getElementById('idLocalidade').value=="-1") {
		enableAndDisableSetorComercial(true);
	}
}

addLoadEvent(init);

</script>

<div class="bootstrap">
	<form:form method="post" action="pesquisarMedicaoSupervisorio" id="medicaoSupervisorioForm">
		<div class="card">
			<div class="card-header">
            	<h5 class="card-title mb-0">Validar Medi��es do Supervis�rio</h5>
        	</div>
        	<div class="card-body">
        		<input name="acao" type="hidden" id="acao" value="pesquisarMedicaoSupervisorio"/>
   				<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
    			<input name="chavePrimaria" type="hidden" id="chavePrimaria">
    			<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
    			<input name="consultaBancoTelaSupervisorioDiario" type="hidden" id="consultaBancoTelaSupervisorioDiario">
    			<input name="indicadorPesquisa" type="hidden" id="indicadorPesquisa" value="${validaMedicaoSupervisorioVO.indicadorPesquisa}"/>
        			
    			<input name="enderecoRemotoParametro" type="hidden" id="enderecoRemotoParametro" value="${validaMedicaoSupervisorioVO.enderecoRemotoParametro}">
    			<input name="anoMesReferenciaParametro" type="hidden" id="anoMesReferenciaParametro" value="${validaMedicaoSupervisorioVO.anoMesReferenciaParametro}">
    			<input name="idCicloParametro" type="hidden" id="idCicloParametro" value="${validaMedicaoSupervisorioVO.idCicloParametro}">
    			<input name="indicadorIntegrado" type="hidden" id="indicadorIntegrado" value="${validaMedicaoSupervisorioVO.indicadorIntegrado}"/>
    			
    			<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em 
         			Pesquisar, ou clique apenas em Pesquisar para exibir todos.
   			 	</div>
   			 	<div class="row">  
   			 		<div class="col-md-12 mt-2">		
   			 			<div id="accordion">	 			
							<jsp:include page="/jsp/integracao/supervisorio/camposPesquisaMedicaoSupervisorioPC.jsp"/>
							<jsp:include page="/jsp/integracao/supervisorio/camposPesquisaMedicaoSupervisorioMI.jsp"/>
						</div>
					</div>
   			 	</div>
   			 	<div class="row mt-3">
                    <div class="col align-self-end text-right">
                    	<vacess:vacess param="pesquisarMedicaoSupervisorio">
                        	<button class="btn btn-primary btn-sm" id="botaoPesquisar" type="button"
                        		onclick="pesquisar();">
                            	<i class="fa fa-search"></i> Pesquisar
                        	</button>
                        </vacess:vacess>
                        <button class="btn btn-secondary btn-sm" id="botaoLimpar" type="button"
                                onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                    </div>
                </div>
                
                <div class="row">
                	<div class="col-md-12 mt-1">
                	
                		<a name="ancoraPesquisaMedicaoSupervisorio"></a>    
                	
						<jsp:include page="/jsp/integracao/supervisorio/gridMedicaoSupervisorioPC.jsp"/>
				 		<jsp:include page="/jsp/integracao/supervisorio/gridMedicaoSupervisorioMI.jsp"/> 
   	             	</div>
                </div>
   			 	
        	</div><!--  fim do card-body -->
        	<div class="card-footer">
        		<div class="row">
        			<div class="col-sm-12">
        				<vacess:vacess param="desfazerIntegracaoSupervisorio">
							<button type="button" class="btn btn-sm btn-primary float-left ml-1 mt-1" 
							name="botaoDesfazerIntegracaoSupervisorio"
							onclick="desfazerSupervisorio();">Desfazer Integra��o</button>
						</vacess:vacess>
            			<vacess:vacess param="desfazerConsolidarSupervisorio">
							<button type="button" class="btn btn-sm btn-primary float-left ml-1 mt-1"
							name="botaoDesfazerConsolidarSupervisorio" 
							onclick="desfazerConsolidarSupervisorio();">Desfazer Consolidar</button>
						</vacess:vacess> 
        				<vacess:vacess param="transferirMedicaoSupervisorio">
                			<button type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-right"	
                    		onclick="transferirMedicaoSupervisorio();" >Integrar</button>
            			</vacess:vacess>                    
           				<vacess:vacess param="transferirMedicaoSupervisorio">
                			<button type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-right"
                    		onclick="transferirMedicaoSupervisorioParcial();">Integrar Parcial</button>
            			</vacess:vacess>        

        			</div>
        		</div>
        	</div><!-- Fim do card-footer -->
		</div><!-- fim do card -->
		<!-- Modal -->
	<div class="modal fade" id="dataCorte" tabindex="-1" role="dialog" aria-labelledby="dataCorte" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content">
      	<div class="modal-header">
        	<h5 class="modal-title" id="exampleModalCenterTitle">Per�odo de Integra��o</h5>
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
         	 <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
		<div class="input-group input-group-sm">
            <input type="text" class="form-control form-control-sm campoData"
             id="dataIntegracaoInicial" name="dataIntegracaoInicial" maxlength="10"
             value="${validaMedicaoSupervisorioVO.dataIntegracaoInicial}" > 
              <div class="input-group-prepend">
                  <span class="input-group-text">at�</span>
              </div>
              <input type="text" class="form-control form-control-sm campoData"
               id="dataIntegracaoFinal" name="dataIntegracaoFinal" maxlength="10"
               value="${validaMedicaoSupervisorioVO.dataIntegracaoFinal}">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="transferirMedicaoSupervisorio();">Integrar</button>
      </div>
    </div>
  </div>
</div>
	</form:form>
</div>

