<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="mt-3">
	<p>Para pesquisar por Pessoa ou Im�vel, utilize o bot�o <strong>Pesquisa Avan�ada</strong>.</p>
    <button class="btn btn-primary" data-toggle="collapse" type="button" 
     data-target="#linkExibirDetalhes" aria-expanded="false" aria-controls="linkExibirDetalhes">
     Pesquisa Avan�ada
    </button>

    <div id="linkExibirDetalhes" class="collapse">
 		<div class="card card-body mt-2">
  		 <div class="row" id="pesquisaAvancadaPesquisaMedicaoSupervisorio">
           <div class="col-md-6" id="pesquisarCliente">
               <jsp:include page="/jsp/cadastro/cliente/dadosClienteNovo.jsp">
                   <jsp:param name="idCampoIdCliente" value="idCliente" />
                   <jsp:param name="idCliente"
                       value="${validaMedicaoSupervisorioVO.idCliente}" />
                   <jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente" />
                   <jsp:param name="nomeCliente"
                       value="${cliente.nome}" />
                   <jsp:param name="idCampoDocumentoFormatado"
                       value="documentoFormatado" />
                   <jsp:param name="documentoFormatadoCliente"
                       value="${cliente.numeroDocumentoFormatado}" />
                   <jsp:param name="idCampoEmail" value="emailCliente" />
                   <jsp:param name="emailCliente"
                       value="${cliente.emailPrincipal}" />
                   <jsp:param name="idCampoEnderecoFormatado"
                       value="enderecoFormatadoCliente" />
                   <jsp:param name="enderecoFormatadoCliente"
                       value="${cliente.enderecoPrincipal.enderecoFormatado}" />
               </jsp:include>
           </div>

           <div class="col-md-6" id="pesquisarImovel">
               <h5>Pesquisar Im�vel</h5>
               
               <div class="form-row mt-1">
    				<label for="nomeImovel" class="col-sm-3 col-form-label">Nome Fantasia: </label>
    				<div class="col-sm-9">
      				<input type="text" class="form-control form-control-sm" id="nomeImovel"
      				name="nomeImovel" maxlength="50" size="37"
                       value="${validaMedicaoSupervisorioVO.nomeImovel}"
                       onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 
                       'formatarCampoNome(event)');">
    				</div>
 				</div>
 				 <div class="form-row mt-1">
    				<label  for="complementoImovel" class="col-sm-3 col-form-label">Complemento:</label>
    				<div class="col-sm-9">
      				<input type="text" class="form-control form-control-sm" id="complementoImovel"
      				   name="complementoImovel" maxlength="25" size="30"
                       value="${validaMedicaoSupervisorioVO.complementoImovel}"
                       onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 
                       'formatarCampoNome(event)');">
    				</div>
 				</div>
               
               	<div class="form-row mt-1">
    				<label for="matriculaImovel" class="col-sm-3 col-form-label">Matr�cula:</label>
    				<div class="col-sm-9">
      				<input type="text" class="form-control form-control-sm" id="matriculaImovel"
      				   name="matriculaImovel" maxlength="9" size="9"
                       value="${validaMedicaoSupervisorioVO.matriculaImovel}"
                       onkeypress="return formatarCampoInteiro(event)">
    				</div>
 				</div>
 				<div class="form-row mt-1">
    				<label for="numeroImovel" class="col-sm-3 col-form-label">N�mero do Im�vel:</label>
    				<div class="col-sm-4">
      				<input type="text" class="form-control form-control-sm" id="numeroImovel"
      				   name="numeroImovel" maxlength="9" size="9"
                       value="${validaMedicaoSupervisorioVO.numeroImovel}"
                       onkeypress="return formatarCampoNumeroEndereco(event, this);">
    				</div>
 				</div>
              	<div class="form-row mt-1">
              	<div class="col-sm-12">
    	  			<label for="condominioSim">O im�vel � condom�nio? </label>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="indicadorCondominio" id="condominioSim"
                 		value="true" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.indicadorCondominio eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="condominioSim">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="indicadorCondominio" id="condominioNao"
                 		value="false" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.indicadorCondominio eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="condominioNao">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="indicadorCondominio" id="condominioAmbos"
                 		value="" class="custom-control-input"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.indicadorCondominio}">checked</c:if>>
                      	<label class="custom-control-label" for="condominioAmbos">Todos</label>
                	</div>
    			</div>
    		</div>
    		<div class="form-row">
                   <div class="exibirCep col-sm-9">
                        <jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
                           <jsp:param name="cepObrigatorio" value="false" />
                           <jsp:param name="idCampoCep" value="cepImovel" />
                           <jsp:param name="numeroCep"
                               value="${validaMedicaoSupervisorioVO.cepImovel}" />
                       </jsp:include>
                   </div>
               </div>
           </div>
		</div>
		</div>
	</div>
</div>
