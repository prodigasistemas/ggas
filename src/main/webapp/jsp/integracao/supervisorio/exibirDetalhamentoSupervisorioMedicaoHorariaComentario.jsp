<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInternoPopup">Coment�rios das Altera��es Realizadas<a class="linkHelp" href="<help:help>/comentriosdasalteraesrealizadas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<script>

	window.opener.esconderIndicador();

	$(document).ready(function(){	
		//Adiciona o atributo title �s c�lulas e joga o valor da pr�pria c�lula como valor do atributo.
		$("table.dataTableMenor tr td").each(function(){
			var conteudoTabela = $(this).text();
			$(this).attr({'title': conteudoTabela});
		});
	});


</script>

<form>
	<fieldset>
		
		<display:table id="medicaoSupervisorioMedicaoComentario" class="dataTableGGAS dataTablePopup dataTableCabecalho2Linhas" export="false" name="listaSupervisorioMedicaoHorariaComentario" sort="list" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoSupervisorioMedicaoHorariaComentario">
	     	
			<display:column title="Data<br />Leitura" style="width: 75px">
				<fmt:formatDate value="${medicaoSupervisorioMedicaoComentario.supervisorioMedicaoHoraria.dataRealizacaoLeitura}" pattern="dd/MM/yyyy"/>
			</display:column>
	
			<display:column title="Descri��o">
				<div style="word-wrap: break-word; width: 580px; text-align: left; overflow: hidden">
					<c:out value='${medicaoSupervisorioMedicaoComentario.descricao}'/>
				</div>
			</display:column>
			
			<display:column title="Usu�rio" style="width: 170px">
				<c:choose>
				  <c:when test="${medicaoSupervisorioMedicaoComentario.usuario.funcionario ne null}">
					<c:out value='${medicaoSupervisorioMedicaoComentario.usuario.funcionario.nome}'/>
				  </c:when>
				  <c:otherwise>
				    <c:out value='${medicaoSupervisorioMedicaoComentario.usuario.login}'/>
				  </c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Data<br />Altera��o" style="width: 75px">
				<fmt:formatDate value="${medicaoSupervisorioMedicaoComentario.ultimaAlteracao}" pattern="dd/MM/yyyy"/> 
			</display:column>		
			    
	    </display:table>
    </fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2 botaoGrande1" value="Fechar" type="button" onclick="window.close();">
 	</fieldset>
</form>