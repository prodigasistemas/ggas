<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="card">
 	 <div class="card-header" id="medidor">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" id="expandirPesquisaMedidorIndependente" type="button" onclick="pesquisaPontoConsumo();" data-toggle="collapse" data-target="#linkMedidor" aria-expanded="false" aria-controls="linkMedidor">
          Pesquisa por Medidor Independente
        </button>
      </h5>
    </div>
    <div id="linkMedidor" class="collapse" aria-labelledby="medidor" data-parent="#accordion">
 		<div class="card-body">
 			<div class="row">
 				<div class="col-md-6">
 					<div class="form-row">
             		<div class="col-md-6">
             			<label for="anoMesReferenciaLidoMI">Ano/M�s de Refer�ncia:</label>
                         <input class="form-control form-control-sm" type="text" id="anoMesReferenciaLidoMI" name="anoMesReferenciaLidoMI" 
                         value="<c:out value="${validaMedicaoSupervisorioVO.anoMesReferenciaLidoMI}"/>" >
             			</div>
             		 	<div class="col-md-4">
             				<label for="idCicloMI">Ciclo:</label>
							<select name="idCicloMI" id="idCicloMI" class="form-control form-control-sm" >
								<option value="-1">Selecione</option>
								<c:forEach items="${listaCiclo}" var="ciclo">
								<option value="<c:out value="${ciclo}"/>"
									<c:if test="${validaMedicaoSupervisorioVO.idCicloMI == ciclo}">selected="selected"</c:if>>
									<c:out value="${ciclo}" />
								</option>
							</c:forEach>
							</select>
           		  		</div>
 				</div>
 				<div class="form-row">
             		 <div class="col-md-10">
             		 	 <label for="enderecoRemotoLidoMI">Endere�o Remoto do Medidor:</label>
                               <input class="form-control form-control-sm" type="text" id="enderecoRemotoLidoMI" 
                               name="enderecoRemotoLidoMI" value="<c:out value="${validaMedicaoSupervisorioVO.enderecoRemotoLidoMI}"/>" >
             		 	</div>
             	</div>
             	<div class="form-row">
             	 <div class="col-md-10">
             		<label>Per�odo de Leitura:</label>
             		<div class="input-group input-group-sm">
            			<input type="text" class="form-control form-control-sm campoData"
             			id="dataLeituraInicialMI" name="dataLeituraInicialMI" 
             			value="${validaMedicaoSupervisorioVO.dataLeituraInicialMI}" onblur="verificarFiltroPesquisa();" > 
             			 <div class="input-group-prepend">
                  		<span class="input-group-text">at�</span>
             		 </div>
              			<input type="text" class="form-control form-control-sm campoData"
              			 id="dataLeituraFinalMI" name="dataLeituraFinalMI"
              			 value="${validaMedicaoSupervisorioVO.dataLeituraFinalMI}" onblur="verificarFiltroPesquisa();" >
       				 </div>
       				 </div>
             	</div>
             	<div class="form-row">
             	 <div class="col-md-10">
             		<label>Per�odo de Inclus�o:</label>
             		<div class="input-group input-group-sm">
            			<input type="text" class="form-control form-control-sm campoData"
             			id="dataInclusaoInicialMI" name="dataInclusaoInicialMI"
             			value="${validaMedicaoSupervisorioVO.dataInclusaoInicialMI}" onblur="verificarFiltroPesquisa();" > 
             			 <div class="input-group-prepend">
                  		<span class="input-group-text">at�</span>
             		 </div>
              			<input type="text" class="form-control form-control-sm campoData"
              			 id="dataInclusaoFinalMI" name="dataInclusaoFinalMI"
              			 value="${validaMedicaoSupervisorioVO.dataInclusaoFinalMI}" onblur="verificarFiltroPesquisa();" >
       				 </div>
       				 </div>
             	</div>
             	<div class="form-row mt-2">
    	  			<label>Ocorr�ncias de Medi��es:</label>
    	  			<div class="col-md-10">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="ocorrenciaMI" id="ocorrenciaSimMI"
                 		value="true" class="custom-control-input"
		        		onchange="carregarOcorrenciasMI(this);"
                      	<c:if test="${validaMedicaoSupervisorioVO.ocorrenciaMI eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="ocorrenciaSimMI">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                		<input type="radio" name="ocorrenciaMI" id="ocorrenciaNaoMI"
                 		value="false" class="custom-control-input"
		        		onchange="carregarOcorrenciasMI(this);"
                      	<c:if test="${validaMedicaoSupervisorioVO.ocorrenciaMI  eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="ocorrenciaNaoMI">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                		<input type="radio" name="ocorrenciaMI" id="ocorrenciaTodasMI"
                 		value="" class="custom-control-input"
		        		onchange="carregarOcorrenciasMI(this);"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.ocorrenciaMI}">checked</c:if>>
                      	<label class="custom-control-label" for="ocorrenciaTodasMI">Todas</label>
                	</div>
    			</div>
    		</div>
    		<div class="form-row">
    			<div class="col-md-10">
    			<select multiple name="idsOcorrenciaMedicao" id="idsOcorrenciaMedicaoMI" class="form-control form-control-sm" >
				<c:forEach items="${listaOcorrenciaMedicao}"
			            var="ocorrenciaMedicao">
			            <c:set var="selecionado" value="false" />
			            <c:forEach
			                items="${validaMedicaoSupervisorioVO.idsOcorrenciaMedicaoMI}"
			                var="idOcorrenciaMedicao">
			                <c:if
			                    test="${idOcorrenciaMedicaoMI eq ocorrenciaMedicao.chavePrimaria}">
			                    <c:set var="selecionado" value="true" />
			                </c:if>
			            </c:forEach>
			            <option
			                value="<c:out value="${ocorrenciaMedicao.chavePrimaria}"/>"
			                <c:if test="${selecionado eq true}"> selected="selected"</c:if>
			                title="<c:out value="${ocorrenciaMedicao.descricao}"/>">
			                <c:out value="${ocorrenciaMedicao.descricao}" />
			            </option>
			        </c:forEach>
				</select>	
			</div>	
    		</div>
 		</div><!-- fim da primeira coluna -->
 		<div class="col-md-6">
 			<div class="form-row mt-2">
    	  		<label>Medi��o Integrada:</label>
    	  			<div class="col-md-10">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="analisadaMI" id="analisadaSimMI"
                 		value="true" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.analisadaMI eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="analisadaSimMI">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="analisadaMI" id="analisadaNaoMI"
                 		value="false" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.analisadaMI eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="analisadaNaoMI">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="analisadaMI" id="analisadaTodasMI" 
                 		value="" class="custom-control-input"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.analisadaMI}">checked</c:if>>
                      	<label class="custom-control-label" for="analisadaTodasMI">Todas</label>
                	</div>
    			</div>
    		</div>
    		
    		<div class="form-row mt-2">
    	  		<label for="comOcorrenciaMI">Transfer�ncia:</label>
    	  		<div class="col-md-12">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="transferenciaMI" id="transferenciaSimMI"
                 		value="true" class="custom-control-input"
                      	 <c:if test="${validaMedicaoSupervisorioVO.transferenciaMI eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="transferenciaSimMI">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="transferenciaMI" id="transferenciaNaoMI"
                 		value="false" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.transferenciaMI  eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="transferenciaNaoMI">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="transferenciaMI" id="transferenciaTodasMI"
                 		value="" class="custom-control-input"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.transferenciaMI}">checked</c:if>>
                      	<label class="custom-control-label" for="transferenciaTodasMI">Todas</label>
                	</div>
    			</div>
    		</div>
    		
    		<div class="form-row mt-2">
    	  		<label>Ativos:</label> 
    	  		<div class="col-md-12">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="ativoMI" id="ativoSimMI"
                 		value="true" class="custom-control-input"
                      	 <c:if test="${validaMedicaoSupervisorioVO.ativoMI eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="ativoSimMI">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="ativoMI" id="ativoNaoMI"
                 		value="false" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.ativoMI eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="ativoNaoMI">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="ativoMI" id="ativoTodasMI"
                 		value="" class="custom-control-input"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.ativoMI}">checked</c:if>>
                      	<label class="custom-control-label" for="ativoTodasMI">Todas</label>
                	</div>
    			</div>
    		</div>
    		
    		<div class="form-row mt-2">
    	  		<label>Al�ada: </label>
    	  		<div class="col-md-12">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusMI"  id="statusAutorizadoMI"
                 		value="${statusAutorizado}"  class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.statusMI eq statusAutorizado}">checked</c:if>>
                      	<label class="custom-control-label" for="statusAutorizadoMI">Autorizado</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusMI" id="statusNaoAutorizadoMI" 
                 		value="${statusNaoAutorizado}"  class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.statusMI eq statusNaoAutorizado}">checked</c:if>>
                      	<label class="custom-control-label" for="statusNaoAutorizadoMI">N�o Autorizado</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusMI" id="statusPendenteMI" 
                 		value="${statusPendente}"  class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.statusMI eq statusPendente}">checked</c:if>>
                      	<label class="custom-control-label" for="statusPendenteMI">Pendente</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusMI" id="statusMI"
                 		value="0"  class="custom-control-input"
                      	 <c:if test="${empty validaMedicaoSupervisorioVO.statusMI or validaMedicaoSupervisorioVO.statusMI eq 0}">checked</c:if>>
                      	<label class="custom-control-label" for="statusMI">Todas</label>
                	</div>
    			</div>
    		</div>
 				
 				</div><!-- fim da segunda coluna -->
 			
 			</div><!-- fim da row principal -->	
 		</div>
 	</div>
</div>
