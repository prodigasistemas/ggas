<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script>

    $(document).ready(function(){
        
    	iniciarDatatable('#supervisorioMedicao');
    	

        $('input[name$="dataReferencia"]').inputmask("99/9999-9",{placeholder:"_"}); 
        $('input[name$="dataRealizacaoLeitura"]').inputmask("99/99/9999",{placeholder:"_"}); 
        
        $('input[name=volumeNaoCorrigido]').each(function() {
            var input = $(this),
            text = input.val().replace(".", ",");
            input.val(text);
        });
        
        $('input[name=volumeCorrigido]').each(function() {
            var input = $(this),
            text = input.val().replace(".", ",");
            input.val(text);
        });
        
        $('input[name=pressao]').each(function() {
            var input = $(this),
            text = input.val().replace(".", ",");
            input.val(text);
        });
        
        $('input[name=temperatura]').each(function() {
            var input = $(this),
            text = input.val().replace(".", ",");
            input.val(text);
        });
        
        $('input[name=fatorZ]').each(function() {
            var input = $(this),
            text = input.val().replace(".", ",");
            input.val(text);
        });
        
        $('input[name=fatorPTZ]').each(function() {
            var input = $(this),
            text = input.val().replace(".", ",");
            input.val(text);
        });
        

    
    });
    
    
    function alterarStatusRegistroSupervisorioDiario(chavePrimaria, nomeCampoAlterado, valorCampoAlterado, codigoPontoConsumoSupervisorio){
        
            AjaxService.alterarStatusRegistroSupervisorioDiario(chavePrimaria, nomeCampoAlterado, valorCampoAlterado.value, codigoPontoConsumoSupervisorio,
                function(listaSupervisorio) { 
                
                for (key in listaSupervisorio){
                	
                	var element = document.getElementById("registro" + chavePrimaria);
                	element.classList.add("table-warning");
                    
                    if(chavePrimaria>0){
                    	document.getElementById('horaria' + chavePrimaria).style.visibility='hidden';
                    }
                }
                
                }
             ); 

    }
    
    //Preenchimento do Modal de detalhes
    function detalhaSupervisorio(temperatura, pressao, medicao, status){
    	
    	document.getElementById('temperatura').value = temperatura;
    	document.getElementById('pressao').value = pressao;
    	document.getElementById('medicaoHoraria').value = medicao;
    	
    	if(status == null || status == ""){
    		status = "N�o possui Status de Autoriza��o cadastrado.";
    	}
    	
    	document.getElementById('status').value = status;
    	
    	$('#detalhamento').modal('show');
    }

    
    function salvarSupervisorioMedicaoDiaria(){
        
        var exigeComentario = document.getElementById("exigeComentario");
        var comentario = document.getElementById("comentario");
                        
        if(exigeComentario.value == 1 && comentario.value == ""){
            alert ('<fmt:message key="CAMPOS_OBRIGATORIOS"/>');
            return false;
        }
    
        submeter("medicaoSupervisorioForm", "salvarSupervisorioMedicaoDiaria");
            
    }
    
    function exibirDetalhamentoSupervisorioMedicaoDiariaComentario(chavePrimaria){
        var popupSupervisorioMedicaoComentario;
        document.forms[0].chavePrimaria.value = chavePrimaria;
        var popupSupervisorioMedicaoComentario = window.open('about:blank','popupSupervisorioMedicaoComentario','height=600,width=980,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0 ,modal=1');
        if (window.focus) {popupSupervisorioMedicaoComentario.focus()}
        
        submeter("medicaoSupervisorioForm", "exibirDetalhamentoSupervisorioMedicaoDiariaComentario", "popupSupervisorioMedicaoComentario");
    }
    
    
    
    function exibirDetalhamentoSupervisorioMedicaoHorario(chavePrimaria, indicadorProcessado, indicadorIntegrado){
        
        var popupSupervisorioMedicaoHorario;
        document.forms[0].chavePrimariaDiaria.value = chavePrimaria;
        document.forms[0].indicadorProcessado.value = indicadorProcessado;
        document.forms[0].indicadorIntegrado.value = indicadorIntegrado;
        document.forms[0].consultaBanco.value = true;
        var popupSupervisorioMedicaoHorario = window.open('about:blank','popupSupervisorioMedicaoHorario','height=600,width=980,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0 ,modal=1');
        if (window.focus) {popupSupervisorioMedicaoHorario.focus()}
        submeter("medicaoSupervisorioForm", "exibirDetalhamentoMedicaoSupervisorioHorario", "popupSupervisorioMedicaoHorario");
    }
    
    function adicionarSupervisorioMedicaoDiaria(chavePrimaria, codigoPontoConsumoSupervisorio){
    	
        document.forms[0].chavePrimaria.value = chavePrimaria;
        document.forms[0].enderecoRemotoParametro.value = codigoPontoConsumoSupervisorio;
        submeter('medicaoSupervisorioForm', 'adicionarRegistroSupervisorioDiario');
    }
    
    function removerSupervisorioMedicaoDiaria(chavePrimaria, codigoPontoConsumoSupervisorio){
        
        if(chavePrimaria > 0){
            var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_DESABILITAR"/>');
            if(retorno == true) {
                document.forms[0].chavePrimaria.value = chavePrimaria;
                document.forms[0].enderecoRemotoParametro.value = codigoPontoConsumoSupervisorio;
                
                submeter('medicaoSupervisorioForm', 'removerRegistroSupervisorioDiario');
            }
        }else{
            document.forms[0].chavePrimaria.value = chavePrimaria;
            document.forms[0].enderecoRemotoParametro.value = codigoPontoConsumoSupervisorio;
            
            submeter('medicaoSupervisorioForm', 'removerRegistroSupervisorioDiario');
        }
        
            
    }

    
    function cancelar(){    
        submeter('medicaoSupervisorioForm', 'exibirPesquisaMedicaoSupervisorio');
    }
    
    function voltar(){    
        submeter('medicaoSupervisorioForm', 'pesquisarMedicaoSupervisorio');
    }
    
    function autorizarSupervisorioMedicaoDiaria(chavePrimaria){
        
        var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_AUTORIZAR"/>');
        if(retorno == true) {
            document.forms[0].chavePrimaria.value = chavePrimaria;
            submeter('medicaoSupervisorioForm', 'autorizarSupervisorioMedicaoDiaria');
        }
    }
    
    function naoAutorizarSupervisorioMedicaoDiaria(chavePrimaria, codigoPontoConsumoSupervisorio){
        
        var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_NAO_AUTORIZAR"/>');
        if(retorno == true) {
            document.forms[0].chavePrimaria.value = chavePrimaria;
            document.getElementById("dataRealizacaoLeitura").value = document.getElementById("dataRealizacaoLeitura" + chavePrimaria).value;
            document.getElementById("enderecoRemotoParametro").value = codigoPontoConsumoSupervisorio;
            
            submeter('medicaoSupervisorioForm', 'naoAutorizarSupervisorioMedicaoDiaria');
        }
    }
    
    function transferirMedicaoSupervisorioPorDia() {
    	var selecao = verificarSelecao();
    	
    	if (selecao == true) {
    		submeter("medicaoSupervisorioForm", "transferirMedicaoDiariaPorDia");
    	}
    }
    
    function init() {
        
        var registrosAlterados = [
                                    <c:forEach items="${sessionScope.registrosAlterados}" var="item" varStatus="loopStatus">
                                    '${item}'<c:if test="${!loopStatus.last}">, </c:if>
                                    </c:forEach>
                                    ];
                    
        var quantidadeRegistrosAlterados = registrosAlterados.length;
        
        if(quantidadeRegistrosAlterados > 0){
            
            for (var i = 0; i< quantidadeRegistrosAlterados; i++) {
         
            	if(document.getElementById("registro" + registrosAlterados[i]) != null) {
            		var alt = document.getElementById("registro" + registrosAlterados[i]);
            		alt.classList.add("table-warning");
            	}
                
                if(registrosAlterados[i]>0){
                	if(document.getElementById('horaria' + registrosAlterados[i])!=null){
                		document.getElementById('horaria' + registrosAlterados[i]).style.visibility='hidden';
                	}
                }
            }
        }
        
        var registrosIndicadorProcessado = [
                                    <c:forEach items="${sessionScope.registrosIndicadorProcessado}" var="item" varStatus="loopStatus">
                                    '${item}'<c:if test="${!loopStatus.last}">, </c:if>
                                    </c:forEach>
                                    ];
                    var quantidadeRegistrosIndicadorProcessadoa = registrosIndicadorProcessado.length;
                    
                    if(quantidadeRegistrosIndicadorProcessadoa > 0){
                        
                        for (var i = 0; i< quantidadeRegistrosIndicadorProcessadoa; i++) {
                            
                        	if(document.getElementById('dataReferencia'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('dataReferencia'+ registrosIndicadorProcessado[i]).disabled = true;
                        	}
                            
                        	if(document.getElementById('dataRealizacaoLeitura'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('dataRealizacaoLeitura'+ registrosIndicadorProcessado[i]).disabled = true;
                        	}
                            
                            if(document.getElementById('leituraNaoCorrigida'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('leituraNaoCorrigida'+ registrosIndicadorProcessado[i]).disabled = true;
                        	}
                            
                            if(document.getElementById('leituraCorrigida'+ registrosIndicadorProcessado[i])!=null){
                            	document.getElementById('leituraCorrigida'+ registrosIndicadorProcessado[i]).disabled = true;
                            }
                            
                            if(document.getElementById('volumeNaoCorrigido'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('volumeNaoCorrigido'+ registrosIndicadorProcessado[i]).disabled = true;
                            }
                            
                            if(document.getElementById('volumeCorrigido'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('volumeCorrigido'+ registrosIndicadorProcessado[i]).disabled = true;
                            }
                            
                            if(document.getElementById('fatorZ'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('fatorZ'+ registrosIndicadorProcessado[i]).disabled = true;
                            }
                            
                            if(document.getElementById('fatorPTZ'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('fatorPTZ'+ registrosIndicadorProcessado[i]).disabled = true;
                            }
                            
                            if(document.getElementById('botaoAdicionar'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('botaoAdicionar'+ registrosIndicadorProcessado[i]).disabled = true;
                            }
                            if(document.getElementById('botaoRemover'+ registrosIndicadorProcessado[i])!=null) {
                            	document.getElementById('botaoRemover'+ registrosIndicadorProcessado[i]).disabled = true;
                            }
                            
                        }
                    }
        
        var registrosRemocaoLogica = [
                        <c:forEach items="${sessionScope.registrosRemocaoLogica}" var="item" varStatus="loopStatus">
                        '${item}'<c:if test="${!loopStatus.last}">, </c:if>
                        </c:forEach>
                        ];
        
        var quantidadeRegistrosRemocaoLogica = registrosRemocaoLogica.length;
        
        if(quantidadeRegistrosRemocaoLogica > 0){
            
            for (var i = 0; i< quantidadeRegistrosRemocaoLogica; i++) {
          
            	if(document.getElementById('dataReferencia'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('dataReferencia'+ registrosRemocaoLogica[i]).disabled = true;
            	}
                
            	if(document.getElementById('dataRealizacaoLeitura'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('dataRealizacaoLeitura'+ registrosRemocaoLogica[i]).disabled = true;
            	}
                
            	if(document.getElementById('leituraNaoCorrigida'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('leituraNaoCorrigida'+ registrosRemocaoLogica[i]).disabled = true;
            	}
                
            	if(document.getElementById('leituraCorrigida'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('leituraCorrigida'+ registrosRemocaoLogica[i]).disabled = true;
        		}
                
            	if(document.getElementById('volumeNaoCorrigido'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('volumeNaoCorrigido'+ registrosRemocaoLogica[i]).disabled = true;
            	}
                
                if(document.getElementById('volumeCorrigido'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('volumeCorrigido'+ registrosRemocaoLogica[i]).disabled = true;
            	}
                
                if(document.getElementById('fatorZ'+ registrosRemocaoLogica[i])!=null){
                	document.getElementById('fatorZ'+ registrosRemocaoLogica[i]).disabled = true;
                }
                
                if(document.getElementById('fatorPTZ'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('fatorPTZ'+ registrosRemocaoLogica[i]).disabled = true;
            	}
                
            	if(document.getElementById('botaoAdicionar'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('botaoAdicionar'+ registrosRemocaoLogica[i]).disabled = true;
            	}
                
                if(document.getElementById('botaoRemover'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('botaoRemover'+ registrosRemocaoLogica[i]).disabled = true;
                }
                
                if(document.getElementById('botaoComentario'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('botaoComentario'+ registrosRemocaoLogica[i]).disabled = true;
                }
                
                if(document.getElementById('botaoDetalhamento'+ registrosRemocaoLogica[i])!=null) {
                	document.getElementById('botaoDetalhamento'+ registrosRemocaoLogica[i]).disabled = true;
                }
                
            }
        }
        
        var registrosSalvos = [
                                <c:forEach items="${sessionScope.registrosSalvos}" var="item" varStatus="loopStatus">
                                '${item}'<c:if test="${!loopStatus.last}">, </c:if>
                                </c:forEach>
                                ];
                    
        var quantidadeRegistrosSalvos = registrosSalvos.length;
        
        if(quantidadeRegistrosSalvos > 0){
            for (var i = 0; i< quantidadeRegistrosSalvos; i++) {
            	if(document.getElementById("registro" + registrosSalvos[i])!= null) {
            		var salvo = document.getElementById("registro" + registrosSalvos[i]);
            		salvo.classList.add("table-success");
            	}
            }
        }
        
        var registrosSomenteLeitura = [
                                        <c:forEach items="${sessionScope.registrosSomenteLeitura}" var="item" varStatus="loopStatus">
                                        '${item}'<c:if test="${!loopStatus.last}">, </c:if>
                                        </c:forEach>
                                        ];
                
        var quantidadeRegistrosSomenteLeitura = registrosSomenteLeitura.length;
        if(quantidadeRegistrosSomenteLeitura > 0){
                    
            for (var i = 0; i< quantidadeRegistrosSomenteLeitura; i++) {
                        
            	if(document.getElementById('leituraNaoCorrigida'+ registrosSomenteLeitura[i])!=null) {
                	document.getElementById('leituraNaoCorrigida'+ registrosSomenteLeitura[i]).disabled = true;
            	}
                   
            	if(document.getElementById('leituraCorrigida'+ registrosSomenteLeitura[i])!=null) {
                	document.getElementById('leituraCorrigida'+ registrosSomenteLeitura[i]).disabled = true;
            	}  
                
                if(document.getElementById('volumeNaoCorrigido'+ registrosSomenteLeitura[i])!=null) {
                	document.getElementById('volumeNaoCorrigido'+ registrosSomenteLeitura[i]).disabled = true;
                }
                
                if(document.getElementById('pressao'+ registrosSomenteLeitura[i])!=null) {
                	document.getElementById('pressao'+ registrosSomenteLeitura[i]).disabled = true;
                }
                
                if(document.getElementById('temperatura'+ registrosSomenteLeitura[i])!=null) {
                	document.getElementById('temperatura'+ registrosSomenteLeitura[i]).disabled = true;
                }
                
				if(document.getElementById('fatorPTZ'+ registrosSomenteLeitura[i])!=null) {
                	document.getElementById('fatorPTZ'+ registrosSomenteLeitura[i]).disabled = true;
            	}
            }
        }
        
    }
    
    addLoadEvent(init);
    

</script>

<div class="bootstrap">
	<form:form method="post" action="exibirDetalhamentoMedicaoSupervisorioDiario" id="medicaoSupervisorioForm" name="medicaoSupervisorioForm">
		<%-- <input name="pageRequest" type="hidden" id="pageRequest" value="${medicaoSupervisorioForm.map.pageRequest}"> --%>
	 	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${validaMedicaoSupervisorioVO.chavePrimaria}" />
		<input name="chavePrimariaDiaria" type="hidden" id="chavePrimariaDiaria" value="" />
		<input name="indicadorProcessado" type="hidden" id="indicadorProcessado" value="${validaMedicaoSupervisorioVO.indicadorProcessado}" />
		<input name="indicadorIntegrado" type="hidden" id="indicadorIntegrado" value="${validaMedicaoSupervisorioVO.indicadorIntegrado}" />
		<input name="exigeComentario" type="hidden" id="exigeComentario" value="${validaMedicaoSupervisorioVO.exigeComentario}">
		<input name="nomeCampoAlterado" type="hidden" id="campoAlterado" value="${nomeCampoAlterado}">
		<input name="valorCampoAlterado" type="hidden" id="campoAlterado" value="${valorCampoAlterado}">
		<input name="dataRealizacaoLeituraParametro" type="hidden" id="dataRealizacaoLeitura" value="${validaMedicaoSupervisorioVO.dataRealizacaoLeitura}">
		<input name="enderecoRemotoParametro" type="hidden" id="enderecoRemotoParametro" value="${validaMedicaoSupervisorioVO.enderecoRemotoParametro}">
		<input name="anoMesReferenciaParametro" type="hidden" id="anoMesReferenciaParametro" value="${validaMedicaoSupervisorioVO.anoMesReferenciaParametro}">
		<input name="idCicloParametro" type="hidden" id="idCicloParametro" value="${validaMedicaoSupervisorioVO.idCicloParametro}">
		<input name="idCicloPC" type="hidden" value="${validaMedicaoSupervisorioVO.idCicloPC}">
		<input name="idCicloMI" type="hidden" value="${validaMedicaoSupervisorioVO.idCicloMI}">
		<input name="quantidadeRegistrosAtivos" type="hidden" id="quantidadeRegistrosAtivos" value="${quantidadeRegistrosAtivos}">
		<input name="totalVolumesCorrigidosAtivos" type="hidden" id="totalVolumesCorrigidosAtivos" value="${totalVolumesCorrigidosAtivos}">
		<input name="totalVolumesNaoCorrigidosAtivos" type="hidden" id="totalVolumesNaoCorrigidosAtivos" value="${totalVolumesNaoCorrigidosAtivos}">
		<input name="consultaBanco" type="hidden" id="consultaBanco">
		
		<input type="hidden" name="idLocalidade" id="idLocalidade" value="${validaMedicaoSupervisorioVO.idLocalidade}" />
		<input type="hidden" name="idSetorComercial" id="idSetorComercial" value="${validaMedicaoSupervisorioVO.idSetorComercial}" />
		<input type="hidden" name="idGrupoFaturamento" id="idGrupoFaturamento" value="${validaMedicaoSupervisorioVO.idGrupoFaturamento}" />
		<input type="hidden" name="idsRota" id="idsRota" value="${validaMedicaoSupervisorioVO.idsRota}" />

		<input type="hidden" name="ocorrenciaPC" id="ocorrenciaPC" value="${validaMedicaoSupervisorioVO.ocorrenciaPC}" />
		<input type="hidden" name="ocorrenciaMI" id="ocorrenciaMI" value="${validaMedicaoSupervisorioVO.ocorrenciaMI}" />
		<input type="hidden" name="analisadaPC" id="analisadaPC" value="${validaMedicaoSupervisorioVO.analisadaPC}" />
		<input type="hidden" name="analisadaMI" id="analisadaMI" value="${validaMedicaoSupervisorioVO.analisadaMI}" />
		<input type="hidden" name="transferenciaPC" id="transferenciaPC" value="${validaMedicaoSupervisorioVO.transferenciaPC}" />
		<input type="hidden" name="transferenciaMI" id="transferenciaMI" value="${validaMedicaoSupervisorioVO.transferenciaMI}" />
		<input type="hidden" name="ativoPC" id="ativoPC" value="${validaMedicaoSupervisorioVO.ativoPC}" />
		<input type="hidden" name="ativoMI" id="ativoMI" value="${validaMedicaoSupervisorioVO.ativoMI}" />
		<input type="hidden" name="statusPC" id="statusPC" value="${validaMedicaoSupervisorioVO.statusPC}" />
		<input type="hidden" name="statusMI" id="statusMI" value="${validaMedicaoSupervisorioVO.statusMI}" />
		
		<input type="hidden" name="anoMesReferenciaLidoPC" id="anoMesReferenciaLidoPC" value="${validaMedicaoSupervisorioVO.anoMesReferenciaLidoPC}" />
		<input type="hidden" name="anoMesReferenciaLidoMI" id="anoMesReferenciaLidoMI" value="${validaMedicaoSupervisorioVO.anoMesReferenciaLidoMI}" />
		<input type="hidden" name="enderecoRemotoLidoPC" id="enderecoRemotoLidoPC" value="${validaMedicaoSupervisorioVO.enderecoRemotoLidoPC}" />
		<input type="hidden" name="enderecoRemotoLidoMI" id="enderecoRemotoLidoMI" value="${validaMedicaoSupervisorioVO.enderecoRemotoLidoMI}" />
		<input type="hidden" name="dataLeituraInicialPC" id="dataLeituraInicialPC" value="${validaMedicaoSupervisorioVO.dataLeituraInicialPC}" />
		<input type="hidden" name="dataLeituraInicialMI" id="dataLeituraInicialMI" value="${validaMedicaoSupervisorioVO.dataLeituraInicialMI}" />
		<input type="hidden" name="dataLeituraFinalPC" id="dataLeituraFinalPC" value="${validaMedicaoSupervisorioVO.dataLeituraFinalPC}" />
		<input type="hidden" name="dataLeituraFinalMI" id="dataLeituraFinalMI" value="${validaMedicaoSupervisorioVO.dataLeituraFinalMI}" />
		<input type="hidden" name="dataInclusaoInicialPC" id="dataInclusaoInicialPC" value="${validaMedicaoSupervisorioVO.dataInclusaoInicialPC}" />
		<input type="hidden" name="dataInclusaoInicialMI" id="dataInclusaoInicialMI" value="${validaMedicaoSupervisorioVO.dataInclusaoInicialMI}" />
		<input type="hidden" name="dataInclusaoFinalPC" id="dataInclusaoFinalPC" value="${validaMedicaoSupervisorioVO.dataInclusaoFinalPC}" />
		<input type="hidden" name="dataInclusaoFinalMI" id="dataInclusaoFinalMI" value="${validaMedicaoSupervisorioVO.dataInclusaoFinalMI}" />
		<input type="hidden" name="idsOcorrenciaMedicaoPC" id="idsOcorrenciaMedicaoPC" value="${validaMedicaoSupervisorioVO.idsOcorrenciaMedicaoPC}" />
		<input type="hidden" name="idsOcorrenciaMedicaoMI" id="idsOcorrenciaMedicaoMI" value="${validaMedicaoSupervisorioVO.idsOcorrenciaMedicaoMI}" />

		<input type="hidden" name="idCliente" id="idCliente" value="${validaMedicaoSupervisorioVO.idCliente}" />
		<input type="hidden" name="nomeImovel" id="nomeImovel" value="${validaMedicaoSupervisorioVO.nomeImovel}" />
		<input type="hidden" name="complementoImovel" id="complementoImovel" value="${validaMedicaoSupervisorioVO.complementoImovel}" />
		<input type="hidden" name="matriculaImovel" id="matriculaImovel" value="${validaMedicaoSupervisorioVO.matriculaImovel}" />
		<input type="hidden" name="numeroImovel" id="numeroImovel" value="${validaMedicaoSupervisorioVO.numeroImovel}" />
		<input type="hidden" name="indicadorCondominio" id="indicadorCondominio" value="${validaMedicaoSupervisorioVO.indicadorCondominio}" />
		<input type="hidden" name="cepImovel" id="cepImovel" value="${validaMedicaoSupervisorioVO.cepImovel}" />
		<input type="hidden" name="clientePrincipalNome" value="${clientePrincipal.nome}" />
		<%-- <input type="hidden" name="consultaBancoTelaSupervisorioDiario" value="${validaMedicaoSupervisorioVO.consultaBancoTelaSupervisorioDiario}" /> --%>
		
		<div class="card">
			<div class="card-header">
            	<h5 class="card-title mb-0">Validar Medi��es Di�rias do Supervis�rio</h5>
        	</div>
        	<div class="card-body">
        		<div class="row">
        			
        			<div class="col-md-6">
  						<c:choose>
							<c:when test="${pontoConsumo.imovel.nome ne null}">
								<p>Descri��o: <span class="font-weight-bold" id="descricao">
  								<c:out  value="${pontoConsumo.imovel.nome} - ${pontoConsumo.descricao}"/></span></p>
							</c:when>
							<c:otherwise>
								<p>Descri��o: <span class="font-weight-bold" id="descricao">
  								<c:out  value="${pontoConsumo.descricao}"/></span></p>
							</c:otherwise>
						</c:choose>
						
  						
  						<p>Cliente: <span class="font-weight-bold" id="cliente">
  						<c:out value="${clientePrincipal.nome eq null ? clientePrincipalNome : clientePrincipal.nome}"/></span></p>
  						
  						<p>Endere�o: <span class="font-weight-bold" id="endereco">
  						<c:out value="${pontoConsumo.enderecoFormatado}"/></span></p>
  						
  						<p>Segmento: <span class="font-weight-bold" id="segmento">
  						<c:out value="${pontoConsumo.segmento.descricao}"/></span></p>
  						
        			</div>
        			<div class="col-md-6">
        				<p>Situa��o: <span class="font-weight-bold" id="situacao">
  						<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/></span></p>
  						
  						<p>Matr�cula do Im�vel: <span class="font-weight-bold" id="matriculaImovel">
  						<c:out value="${pontoConsumo.imovel.chavePrimaria}"/></span></p>
  						
  						<p>C�digo do Supervis�rio: <span class="font-weight-bold" id="codSupervisorio">
  						<c:out value="${pontoConsumo.codigoPontoConsumoSupervisorio}"/></span></p>
  						
        			</div>
        		</div>
        		<hr/>
        		<c:if test="${sessionScope.listaSupervisorioMedicaoDiariaVO ne null}">
        			<div class="alert alert-primary fade show" role="alert">
         				<i class="fa fa-info-circle"></i>
         					Utilize os bot�es de A��o para Adicionar, Remover e Detalhar o Registro de Medi��o Di�ria.<br/>
         					Ao alterar algum registro ele ficar� destacado at� ser salvo.
   			 			</div>
        			<div class="table-responsive">
        				<table class="table table-bordered table-striped table-hover" id="supervisorioMedicao" width="100%">
        					<thead class="thead-ggas-bootstrap">
        						<tr>
									<th>
										<div
											class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
												class="custom-control-input"> <label
												class="custom-control-label p-0" for="checkAllAuto"></label>
										</div>
									</th>
									<th scope="col" class="text-center" data-orderable="false">C�digo</th>
                   			 		<th scope="col" class="text-center">M�s/Ano</th>
                    				<th scope="col" class="text-center">Data</th>
                    				<c:choose>
										<c:when test="${sessionScope.parametroExibeColunasSupervisorio}">
											<th scope="col" class="text-center">Leitura N�o Corrigida</th>
                    						<th scope="col" class="text-center">Leitura Corrigida</th>
                    						<th scope="col" class="text-center">Volume N�o Corrigido</th>
										</c:when>
									</c:choose>
                    				<th scope="col" class="text-center">Volume Corrigido</th>
                    				<c:choose>
										<c:when test="${sessionScope.parametroExibeColunasSupervisorio}">
											<th scope="col" class="text-center">Press�o M�dia</th>
                    						<th scope="col" class="text-center">Temperatura</th>
											<th scope="col" class="text-center">Fator Z</th>
                    						<th scope="col" class="text-center">Fator PTZ</th>
                    						<th scope="col" class="text-center">Al�ada</th>
										</c:when>
									</c:choose>
                    				<th scope="col" class="text-center">Anormalidade</th>
                    				<th scope="col" class="text-center">Integrado</th>
                    				<th scope="col" class="text-center">Transferido</th>
                    				<th scope="col" class="text-center">A��es</th>
        						</tr>
        					</thead>
        					<tbody>
        						<c:forEach items="${sessionScope.listaSupervisorioMedicaoDiariaVO}" var="supervisorioMedicaoDiariaVO">
        						<tr id = "registro<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>">

										<td>
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
												data-identificador-check="chk${supervisorioMedicaoDiariaVO.chavePrimaria}">
												<input id="chk${supervisorioMedicaoDiariaVO.chavePrimaria}" type="checkbox"
													name="chavesPrimarias" class="custom-control-input"
													value="${supervisorioMedicaoDiariaVO.chavePrimaria}"> <label
													class="custom-control-label p-0"
													for="chk${supervisorioMedicaoDiariaVO.chavePrimaria}"></label>
											</div>
										</td>

										<td class="text-center">
        							 	<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}" />
        							</td>
        							
        							<td>
        								 <input type="text" class="form-control form-control-sm"
                                          id="dataReferencia<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
                                          onblur="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'dataReferencia', this);"
                                          name="dataReferencia" size="8"
                                          value="<c:out value="${supervisorioMedicaoDiariaVO.dataReferenciaFormatado}"/>">
        							</td>
        							<td>
        								<fmt:formatDate pattern="dd/MM/yyyy" value="${supervisorioMedicaoDiariaVO.dataRealizacaoLeitura}" var="formattedStatusDate" />
										<input type="text" class="form-control form-control-sm"
											id="dataRealizacaoLeitura<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
											name="dataRealizacaoLeitura" value="<c:out value="${formattedStatusDate}"/>" size="8"
											onblur="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'dataRealizacaoLeitura', this);">
        							</td>
        							
        							<c:choose>
										<c:when test="${sessionScope.parametroExibeColunasSupervisorio}">
										<td>
											<input class="form-control form-control-sm"
											id="leituraNaoCorrigida<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
											name="leituraNaoCorrigida" maxlength="9" size="9"
											onkeypress="return formatarCampoInteiro(event)"
											onblur="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'leituraSemCorrecaoFatorPTZ', this);"
											type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.leituraSemCorrecaoFatorPTZ}"/>" />
										</td>

										<td>
							 				<input class="form-control form-control-sm"
											id="leituraCorrigida<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
											name="leituraCorrigida" maxlength="9" size="9"
											onkeypress="return formatarCampoInteiro(event)"
											onblur="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'leituraComCorrecaoFatorPTZ', this);"
											type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.leituraComCorrecaoFatorPTZ}"/>" />
										</td>

					
										<td>
										<c:choose>
											<c:when test="${supervisorioMedicaoDiariaVO.consumoSemCorrecaoFatorPTZ == 0}">
												<input class="form-control form-control-sm"
												id="volumeNaoCorrigido<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
												name="volumeNaoCorrigido" 
												onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
												onkeypress="return formatarCampoDecimal(event,this,7,8);"
												onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'consumoSemCorrecaoFatorPTZ', this);"
												type="text" value="0,00000000" />
											</c:when>
											<c:otherwise>
												<input class="form-control form-control-sm"
												id="volumeNaoCorrigido<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
												name="volumeNaoCorrigido" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
												onkeypress="return formatarCampoDecimal(event,this,7,8);"
												onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'consumoSemCorrecaoFatorPTZ', this);"
												type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.consumoSemCorrecaoFatorPTZ}"/>" />
											</c:otherwise>
										</c:choose>
									</td>
								</c:when>
								</c:choose>
								
       							<td>
       							
       								<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.consumoComCorrecaoFatorPTZ == 0}">
										<input class="form-control form-control-sm"
										id="volumeCorrigido<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
										name="volumeCorrigido"
										onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
										onkeypress="return formatarCampoDecimal(event,this,9,8);" size="10"
										onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'consumoComCorrecaoFatorPTZ', this);"
										type="text" value="0,00000000" />
									</c:when>
									<c:otherwise>
										<input class="form-control form-control-sm"
										id="volumeCorrigido<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
										name="volumeCorrigido" size="10"
										onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
										onkeypress="return formatarCampoDecimal(event,this,9,8);" 
										onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'consumoComCorrecaoFatorPTZ', this);"
										type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.consumoComCorrecaoFatorPTZ}"/>" />
									</c:otherwise>
								</c:choose>
        						</td>
        						
        						
        						
        						<c:choose>
									<c:when test="${sessionScope.parametroExibeColunasSupervisorio}">
        							<td>
        								<c:choose>
											<c:when test="${supervisorioMedicaoDiariaVO.pressao == 0}">
												<input class="form-control form-control-sm" 
												id="pressao<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
												name="pressao" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
												onkeypress="return formatarCampoDecimal(event,this,5,8);" size="5"
												onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'pressao', this);"
												type="text" value="<c:out value="0,00000000"/>" />
											</c:when>
											<c:otherwise>
												<input class="form-control form-control-sm" 
												id="pressao<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
												name="pressao" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
												onkeypress="return formatarCampoDecimal(event,this,5,8);"  size="5" 
												onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'pressao', this);"
												type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.pressao}"/>" />
											</c:otherwise>
										</c:choose>
        							</td>
        							<td>
        							<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.temperatura == 0}">
											<input class="form-control form-control-sm" 
											id="temperatura<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
											name="temperatura" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
											onkeypress="return formatarCampoDecimal(event,this,5,8);"  size="5"
											onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'temperatura', this);"
											type="text" value="<c:out value='0,00000000'/>" />
										</c:when>
							 			 <c:otherwise>
							  				<input class="form-control form-control-sm" 
							  				id="temperatura<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
											name="temperatura" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
											onkeypress="return formatarCampoDecimal(event,this,5,8);"  size="5"
											onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'temperatura', this);"
											type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.temperatura}"/>" />
							  			</c:otherwise>
							 			 </c:choose>
        					 		 </td>
        					  
									<td>
									<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.fatorZ == 0}">
										<input class="form-control form-control-sm"
										id="fatorZ<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
										name="fatorZ" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
										onkeypress="return formatarCampoDecimal(event,this,2,8);" size="5"
										onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'fatorZ', this);"
										type="text" value="0,00000000" />
										</c:when>
									<c:otherwise>
										<input class="form-control form-control-sm"
										id="fatorZ<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
										name="fatorZ" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
										onkeypress="return formatarCampoDecimal(event,this,2,8);" size="5"
										onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'fatorZ', this);"
										type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.fatorZ}"/>" />
									</c:otherwise>
									</c:choose>
								</td>

								<td>
								<c:choose>
									<c:when test="${supervisorioMedicaoDiariaVO.fatorPTZ == 0}">
										<input class="form-control form-control-sm" disabled="disabled"
										id="fatorPTZ<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
										name="fatorPTZ" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
										onkeypress="return formatarCampoDecimal(event,this,2,8);" size="5"
										onchange="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'fatorPTZ', this);"
										type="text" value="<c:out value="0,00000000"/>" />
									</c:when>
									<c:otherwise>
										<input class="form-control form-control-sm" disabled="disabled"
										id="fatorPTZ<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
										name="fatorPTZ" onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,8);"
										onkeypress="return formatarCampoDecimal(event,this,2,8);" size="5"
										onblur="alterarStatusRegistroSupervisorioDiario(<c:out value='${supervisorioMedicaoDiariaVO.chavePrimaria}'/>, 'fatorPTZ', this);"
										type="text" value="<c:out value="${supervisorioMedicaoDiariaVO.fatorPTZ}"/>" />
									</c:otherwise>
								</c:choose>
								</td>

								
        						
        						<td class="text-center">
        							<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.possuiAutorizacaoAlcada == true}">

										<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.isStatusPendente == true}">
											<button type="button" class="btn btn-info btn-sm" title="Autorizar Al�ada" 
											id="botaoAutorizar<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
											onClick="autorizarSupervisorioMedicaoDiaria(<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>);">
        										<i class="far fa-thumbs-up"></i></button>
        										
        									<button type="button" class="btn btn-secondary btn-sm" title=" N�o Autorizar Al�ada" 
											id="botaoNaoAutorizar<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>"
											onClick="naoAutorizarSupervisorioMedicaoDiaria(<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>);">
        										<i class="far fa-thumbs-down"></i></button>
											
										</c:when>
										<c:otherwise>
											<c:out value="${supervisorioMedicaoDiariaVO.status.descricao}" />
										</c:otherwise>
										</c:choose>

									</c:when>
									<c:otherwise>
										<c:out value="${supervisorioMedicaoDiariaVO.status.descricao}" />
									</c:otherwise>
								</c:choose>
									</td>
								</c:when>
								</c:choose>
        							
        					
        						<td class="text-center">
        							<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.supervisorioMedicaoAnormalidade ne null}">

										<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.supervisorioMedicaoAnormalidade.indicadorImpedeProcessamento == false}">
											<img id="imagem"
											alt="<c:out value="${supervisorioMedicaoDiariaVO.supervisorioMedicaoAnormalidade.descricao}"/>"
											title="<c:out value="${supervisorioMedicaoDiariaVO.supervisorioMedicaoAnormalidade.descricao}"/>"
											src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
										</c:when>
										<c:otherwise>
											<img id="imagem" alt="<c:out value="${supervisorioMedicaoDiariaVO.supervisorioMedicaoAnormalidade.descricao}"/>"
											title="<c:out value="${supervisorioMedicaoDiariaVO.supervisorioMedicaoAnormalidade.descricao}"/>"
											src="<c:url value="/imagens/circle_red.png"/>" border="0">
										</c:otherwise>
										</c:choose>

										</c:when>
										<c:otherwise>
											<img id="imagem" alt="Ativo" src="<c:url value="/imagens/circle_green.png"/>" border="0">
					 					</c:otherwise>
									</c:choose>
        						</td>
        						<td class="text-center">
        							<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.indicadorIntegrado}">Sim</c:when>
										<c:otherwise>N�o</c:otherwise>
									</c:choose>
        						</td>
        						<td class="text-center">
        							<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.indicadorProcessado}">Sim</c:when>
										<c:otherwise>N�o</c:otherwise>
									</c:choose>
        							
        						</td>
        					
        						<td class="text-center">
        							<c:choose>
										<c:when test="${supervisorioMedicaoDiariaVO.chavePrimaria > 0 && sessionScope.parametroTipoIntegracaoSupervisorios}">
											<button type="button" class="btn btn-primary btn-sm" title="Medi��o Hor�ria" onClick="javascript:exibirDetalhamentoSupervisorioMedicaoHorario('<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>', '<c:out value="${supervisorioMedicaoDiariaVO.indicadorProcessado}"/>', '<c:out value="${supervisorioMedicaoDiariaVO.indicadorIntegrado}"/>');">
        									<i class="fas fa-clock"></i></button>
        								</c:when>
        							</c:choose>
        						
        							<button type="button" class="btn btn-info btn-sm" id="botaoComentario<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>" 
        								title="Coment�rio" onClick="javascript:exibirDetalhamentoSupervisorioMedicaoDiariaComentario('<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>');">
        							<i class="fas fa-pen-square"></i></button>
        							
        							<button type="button" class=" btn btn-primary btn-sm"  id="botaoAdicionar<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>" 
        								title="Adicionar" onClick="adicionarSupervisorioMedicaoDiaria('<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>', '<c:out value="${supervisorioMedicaoDiariaVO.codigoPontoConsumoSupervisorio}"/>');">
        							<i class="fas fa-plus-square"></i></button>
        							
        							<button type="button"class="btn btn-danger btn-sm"  id="botaoRemover<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>" 
        								title="Remover" onClick="removerSupervisorioMedicaoDiaria('<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>', '<c:out value="${supervisorioMedicaoDiariaVO.codigoPontoConsumoSupervisorio}"/>');" >
        							<i class="fas fa-minus-square"></i></button>
        							
        							<button type="button"class="btn btn-secondary btn-sm"  id="botaoDetalhamento<c:out value="${supervisorioMedicaoDiariaVO.chavePrimaria}"/>" 
        								title="Detalhamento" data-target="#detalhamento" onClick="javascript:detalhaSupervisorio('<c:out value="${supervisorioMedicaoDiariaVO.temperatura}"/>', 
        								'<c:out value="${supervisorioMedicaoDiariaVO.pressao}"/>', '<c:out value="${supervisorioMedicaoDiariaVO.quantidadeSupervisorioMedicaoHoraria}"/>', '<c:out value="${supervisorioMedicaoDiariaVO.status.descricao}"/>');">
        								<i class="fas fa-file-invoice"></i></button>
        						</td>
        						</tr>
        					
        						</c:forEach>
        						
        					</tbody>
        				</table>
        			</div>
        			<br/>
        			<p>Valores Corrigidos: <c:out value='${ totalVolumesCorrigidosAtivos }' />	</p>
        		</c:if>
        		
        	
        
        	<div class="form-group col-md-6">
    			<label for="comentario">Coment�rio: <span class="text-danger">*</span></label>
   				<textarea class="form-control"  id="comentario" name="comentario" rows="5" 
   				onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoTextoComLimite(event,this,70)');">
   				${supervisorioMedicaoDiariaVO.comentario eq null ? comentario : supervisorioMedicaoDiariaVO.comentario}</textarea>
 			 </div>

        	</div>
        	<div class="card-footer">
        		<button type="button" class="btn btn-danger btn-sm ml-1 mt-1 float-left"
            				onclick="javascript:cancelar();"><i class="fa fa-times"></i> Cancelar</button>
            	<button type="button" class="btn btn-secondary btn-sm ml-1 mt-1 float-left"
            				onclick="javascript:voltar();"><i class="fas fa-undo-alt"></i> Voltar</button>
            	<button type="button" class="btn btn-success btn-sm ml-1 mt-1 float-right"
            				onclick="javascript:salvarSupervisorioMedicaoDiaria();"><i class="fas fa-save"></i> Salvar</button>
				<button type="button"
					class="btn btn-primary btn-sm ml-1 mt-1 float-right"
					onclick="javascript:transferirMedicaoSupervisorioPorDia();">Integrar</button>

			</div>
		</div>
			
	</form:form>
	
	<!-- Modal -->
<div class="modal fade" id="detalhamento" tabindex="-1" role="dialog" aria-labelledby="detalhamento" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tituloDetalhamento">Detalhes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form:form>
  			<div class="form-group row">
    			<label for="staticEmail" class="col-sm-4 col-form-label">Temperatura: </label>
    			<div class="col-sm-8">
      				<input type="text" readonly class="form-control-plaintext" id="temperatura">
    			</div>
 			</div>
 			<div class="form-group row">
    			<label for="staticEmail" class="col-sm-4 col-form-label">Press�o: </label>
    			<div class="col-sm-8">
      				<input type="text" readonly class="form-control-plaintext" id="pressao">
    			</div>
 			</div>
 			<div class="form-group row">
    			<label for="staticEmail" class="col-sm-4 col-form-label">Quantidade de Medi��es Hor�rias:</label>
    			<div class="col-sm-8">
      				<input type="text" readonly class="form-control-plaintext" id="medicaoHoraria">
    			</div>
 			</div>
 			<div class="form-group row">
    			<label for="staticEmail" class="col-sm-4 col-form-label">Status:</label>
    			<div class="col-sm-8">
      				<input type="text" readonly class="form-control-plaintext" id="status">
    			</div>
 			</div>
        </form:form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm ml-1 mt-1 float-left"
            				data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
      </div>
    </div>
  </div>
</div>
	
</div>

