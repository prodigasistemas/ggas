<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

	window.opener.esconderIndicador();
	$(document).ready(function(){
		//Estilos iniciais para os bot�es de Adicionar e Remover Medi��es Hor�rias 
		$("input.botaoAdicionarFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
		$("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
	
		$("input.botaoAdicionarFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			}
		);
	
		$("input.botaoRemoverFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			}
		);
		
		$('input[name$="horaRealizacaoLeitura"]').inputmask("99:99",{placeholder:"_"}); 
		
		$('input[name=pressao]').each(function() {
		    var input = $(this),
		    text = input.val().replace(".", ",");
		    input.val(text);
		});
		
		$('input[name=temperatura]').each(function() {
		    var input = $(this),
		    text = input.val().replace(".", ",");
		    input.val(text);
		});		
	});

	animatedcollapse.addDiv('conteinerDadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=1');

	function mudarImagem(chavePrimaria, nomeCampoAlterado, valorCampoAlterado){
		
		document.forms[0].chavePrimaria.value = chavePrimaria;
		document.forms[0].nomeCampoAlterado.value = nomeCampoAlterado;
		document.forms[0].valorCampoAlterado.value = valorCampoAlterado.value;
		
		var dataHoraRealizacaoLeitura;
		
		if(nomeCampoAlterado == 'dataRealizacaoLeitura'){
			
			dataHoraRealizacaoLeitura = document.getElementById('horaRealizacaoLeitura'+ chavePrimaria).value;
		}else if(nomeCampoAlterado == 'horaRealizacaoLeitura'){
			
			dataHoraRealizacaoLeitura = document.getElementById('dataRealizacaoLeitura'+ chavePrimaria).value;
		}
		document.forms[0].dataHoraRealizacaoLeitura.value = dataHoraRealizacaoLeitura;
		
				AjaxService.alterarStatusRegistroSupervisorioHorario(chavePrimaria, nomeCampoAlterado, valorCampoAlterado.value, dataHoraRealizacaoLeitura,
	              	function(listaSupervisorio) { 
				
				for (key in listaSupervisorio){
					
					document.getElementById('imagem' + chavePrimaria).src = '<c:url value="imagens/icon-alert16.gif"/>';
					document.getElementById('imagem' + chavePrimaria).style.visibility='visible';
					
				}
				
	          	}
		     );	
		
	}
	
	function salvarSupervisorioMedicaoHoraria(){
				
		var exigeComentario = document.getElementById("exigeComentario");
		var comentario = document.getElementById("comentario");
		document.forms[0].comentario.value = comentario.value;
		if(exigeComentario.value == 1 && comentario.value == ""){
			alert ('<fmt:message key="CAMPOS_OBRIGATORIOS"/>');
			return false;
		}
		
		submeter('medicaoSupervisorioForm', 'salvarRegistroSupervisorioHorario');
	}
	
	function exibirDetalhamentoSupervisorioMedicaoComentario(chavePrimaria){
		exibirIndicador();
		var popupSupervisorioMedicaoHorariaComentario;
		document.forms[0].chavePrimaria.value = chavePrimaria;
		var popupSupervisorioMedicaoHorariaComentario = window.open('about:blank','popupSupervisorioMedicaoHorariaComentario','height=600,width=980,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0 ,modal=1');
		if (window.focus) {popupSupervisorioMedicaoHorariaComentario.focus()}
		submeter("medicaoSupervisorioForm", "exibirDetalhamentoSupervisorioMedicaoHorariaComentario", "popupSupervisorioMedicaoHorariaComentario");
	}
	
	function adicionarSupervisorioMedicaoHoraria(chavePrimaria, codigoPontoConsumoSupervisorio){
		
		document.forms[0].chavePrimaria.value = chavePrimaria;
		document.forms[0].enderecoRemotoParametro.value = codigoPontoConsumoSupervisorio;
		
		submeter('medicaoSupervisorioForm', 'adicionarRegistroSupervisorioHorario');
	}
	
	function removerSupervisorioMedicaoHoraria(chavePrimaria, codigoPontoConsumoSupervisorio){
		
		if(chavePrimaria > 0){
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_DESABILITAR"/>');
			if(retorno == true) {
				document.forms[0].chavePrimaria.value = chavePrimaria;
				document.forms[0].enderecoRemotoParametro.value = codigoPontoConsumoSupervisorio;
				
				submeter('medicaoSupervisorioForm', 'removerRegistroSupervisorioHorario');
			}
		}else{
			document.forms[0].chavePrimaria.value = chavePrimaria;
			document.forms[0].enderecoRemotoParametro.value = codigoPontoConsumoSupervisorio;
			
			submeter('medicaoSupervisorioForm', 'removerRegistroSupervisorioHorario');
		}
		
	}
	
	function autorizarSupervisorioMedicaoHoraria(chavePrimaria){
		
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_AUTORIZAR"/>');
		if(retorno == true) {
			document.forms[0].chavePrimaria.value = chavePrimaria;
			submeter('medicaoSupervisorioForm', 'autorizarSupervisorioMedicaoHoraria');
		}
	}
	
	function naoAutorizarSupervisorioMedicaoHoraria(chavePrimaria){
		
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_NAO_AUTORIZAR"/>');
		if(retorno == true) {
			document.forms[0].chavePrimaria.value = chavePrimaria;
			document.getElementById("dataRealizacaoLeitura").value = document.getElementById("dataRealizacaoLeitura" + chavePrimaria).value;
			document.getElementById("horaRealizacaoLeitura").value = document.getElementById("horaRealizacaoLeitura" + chavePrimaria).value;
			submeter('medicaoSupervisorioForm', 'naoAutorizarSupervisorioMedicaoHoraria');
		}
	}
	
	function init() {
		
		var registrosAlterados = [
		  			                <c:forEach items="${sessionScope.registrosAlterados}" var="item" varStatus="loopStatus">
		  			                '${item}'<c:if test="${!loopStatus.last}">, </c:if>
		  			                </c:forEach>
		  			                ];
		  			
		var quantidadeRegistrosAlterados = registrosAlterados.length;
		
		if(quantidadeRegistrosAlterados > 0){
			
			for (var i = 0; i< quantidadeRegistrosAlterados; i++) {
				
				if(document.getElementById('imagem' + registrosAlterados[i])!=null) {
					document.getElementById('imagem' + registrosAlterados[i]).src = '<c:url value="imagens/icon-alert16.gif"/>';
					document.getElementById('imagem' + registrosAlterados[i]).style.visibility='visible';
				}
			}
		}
		
		var registrosRemocaoLogica = [
		                <c:forEach items="${sessionScope.registrosRemocaoLogicaHoraria}" var="item" varStatus="loopStatus">
		                '${item}'<c:if test="${!loopStatus.last}">, </c:if>
		                </c:forEach>
		                ];
		
		var quantidadeRegistrosRemocaoLogica = registrosRemocaoLogica.length;
			
		if(quantidadeRegistrosRemocaoLogica > 0){
				
				for (var i = 0; i< quantidadeRegistrosRemocaoLogica; i++) {
					
					if(document.getElementById('imagem' + registrosRemocaoLogica[i])!=null) {
						document.getElementById('imagem' + registrosRemocaoLogica[i]).src = '<c:url value="imagens/deletar_x.png"/>';
						document.getElementById('imagem' + registrosRemocaoLogica[i]).style.visibility='visible';
					}
					
					document.getElementById('dataRealizacaoLeitura'+ registrosRemocaoLogica[i]).className = 'campoDesabilitado';
					document.getElementById('dataRealizacaoLeitura'+ registrosRemocaoLogica[i]).disabled = true;
					
					document.getElementById('horaRealizacaoLeitura'+ registrosRemocaoLogica[i]).className = 'campoDesabilitado';
					document.getElementById('horaRealizacaoLeitura'+ registrosRemocaoLogica[i]).disabled = true;
					
					document.getElementById('leituraNaoCorrigida'+ registrosRemocaoLogica[i]).className = 'campoDesabilitado';
					document.getElementById('leituraNaoCorrigida'+ registrosRemocaoLogica[i]).disabled = true;
					
					document.getElementById('leituraCorrigida'+ registrosRemocaoLogica[i]).className = 'campoDesabilitado';
					document.getElementById('leituraCorrigida'+ registrosRemocaoLogica[i]).disabled = true;
					
					document.getElementById('pressao'+ registrosRemocaoLogica[i]).className = 'campoDesabilitado';
					document.getElementById('pressao'+ registrosRemocaoLogica[i]).disabled = true;
					
					document.getElementById('temperatura'+ registrosRemocaoLogica[i]).className = 'campoDesabilitado';
					document.getElementById('temperatura'+ registrosRemocaoLogica[i]).disabled = true;
					
					document.getElementById('botaoAdicionar'+ registrosRemocaoLogica[i]).style.visibility='hidden';
					document.getElementById('botaoRemover'+ registrosRemocaoLogica[i]).style.visibility='hidden';
					
				}
			}
		
			var registrosIndicadorProcessado = [
	      		                <c:forEach items="${sessionScope.registrosIndicadorProcessadoHoraria}" var="item" varStatus="loopStatus">
	      		                '${item}'<c:if test="${!loopStatus.last}">, </c:if>
	      		                </c:forEach>
	      		                ];
	      		
	      		var quantidadeRegistrosIndicadorProcessado = registrosIndicadorProcessado.length;
	      			
	      		if(quantidadeRegistrosIndicadorProcessado > 0){
	      				
	      				for (var i = 0; i< quantidadeRegistrosIndicadorProcessado; i++) {
	      					
	      					if(document.getElementById('imagem' + registrosIndicadorProcessado[i])!=null){
	      						document.getElementById('imagem' + registrosIndicadorProcessado[i]).src = '<c:url value="imagens/deletar_x.png"/>';
	      					}
	      					
	      					if(document.getElementById('dataRealizacaoLeitura'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('dataRealizacaoLeitura'+ registrosIndicadorProcessado[i]).className = 'campoDesabilitado';
	      						document.getElementById('dataRealizacaoLeitura'+ registrosIndicadorProcessado[i]).disabled = true;
	      					}
	      					
	      					if(document.getElementById('horaRealizacaoLeitura'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('horaRealizacaoLeitura'+ registrosIndicadorProcessado[i]).className = 'campoDesabilitado';
	      						document.getElementById('horaRealizacaoLeitura'+ registrosIndicadorProcessado[i]).disabled = true;
	      					}
	      					
	      					if(document.getElementById('leituraNaoCorrigida'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('leituraNaoCorrigida'+ registrosIndicadorProcessado[i]).className = 'campoDesabilitado';
	      						document.getElementById('leituraNaoCorrigida'+ registrosIndicadorProcessado[i]).disabled = true;
	      					}
	      					
	      					if(document.getElementById('leituraCorrigida'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('leituraCorrigida'+ registrosIndicadorProcessado[i]).className = 'campoDesabilitado';
	      						document.getElementById('leituraCorrigida'+ registrosIndicadorProcessado[i]).disabled = true;
	      					}
	      					
	      					if(document.getElementById('pressao'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('pressao'+ registrosIndicadorProcessado[i]).className = 'campoDesabilitado';
	      						document.getElementById('pressao'+ registrosIndicadorProcessado[i]).disabled = true;
	      					}
	      					
	      					if(document.getElementById('temperatura'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('temperatura'+ registrosIndicadorProcessado[i]).className = 'campoDesabilitado';
	      						document.getElementById('temperatura'+ registrosIndicadorProcessado[i]).disabled = true;
	      					}
	      					
	      					if(document.getElementById('botaoAdicionar'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('botaoAdicionar'+ registrosIndicadorProcessado[i]).style.visibility='hidden';
	      					}
	      					
	      					if(document.getElementById('botaoRemover'+ registrosIndicadorProcessado[i])!=null) {
	      						document.getElementById('botaoRemover'+ registrosIndicadorProcessado[i]).style.visibility='hidden';
	      					}
	      					
	      				}
	      			}
	      			var registrosSalvos = [
	   	  			                <c:forEach items="${sessionScope.registrosSalvos}" var="item" varStatus="loopStatus">
	   	  			                '${item}'<c:if test="${!loopStatus.last}">, </c:if>
	   	  			                </c:forEach>
	   	  			                ];
	   		  			
	   				var quantidadeRegistrosSalvos = registrosSalvos.length;
	   				if(quantidadeRegistrosSalvos > 0){
	   					for (var i = 0; i< quantidadeRegistrosSalvos; i++) {
	   						if(document.getElementById('imagem' + registrosSalvos[i])!=null) {
	   							document.getElementById('imagem' + registrosSalvos[i]).src = '<c:url value="imagens/check1.gif"/>';
	   							document.getElementById('imagem' + registrosSalvos[i]).style.visibility='visible';
	   						}
	   					}
	   				}
	      		}
	
	addLoadEvent(init);
	
</script>


<h1 class="tituloInternoPopup">Validar Medi��es Hor�rias do Supervis�rio<a class="linkHelp" href="<help:help>/validarmedieshorriasdosupervisrio.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="exibirDetalhamentoMedicaoSupervisorioHorario" id="medicaoSupervisorioForm" name="medicaoSupervisorioForm">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${validaMedicaoSupervisorioVO.chavePrimaria}"/>
<input name="chavePrimariaDiaria" type="hidden" id="chavePrimariaDiaria" value="${chavePrimariaDiaria}"/>
<input name="indicadorProcessado" type="hidden" id="indicadorProcessado" value="${validaMedicaoSupervisorioVO.indicadorProcessado}"/>
<input name="indicadorIntegrado" type="hidden" id="indicadorIntegrado" value="${validaMedicaoSupervisorioVO.indicadorIntegrado}"/>
<input name="exigeComentario" type="hidden" id="exigeComentario" value="${validaMedicaoSupervisorioVO.exigeComentario}">
<input name="enderecoRemotoParametro" type="hidden" id="enderecoRemotoParametro" value="${validaMedicaoSupervisorioVO.enderecoRemotoParametro}">
<input name="nomeCampoAlterado" type="hidden" id="campoAlterado" value="${nomeCampoAlterado}">
<input name="valorCampoAlterado" type="hidden" id="campoAlterado" value="${valorCampoAlterado}">
<input name="dataHoraRealizacaoLeitura" type="hidden" id="dataHoraRealizacaoLeitura" value="">
<input name="dataRealizacaoLeitura" type="hidden" value="${validaMedicaoSupervisorioVO.dataRealizacaoLeitura}">
<input name="horaRealizacaoLeitura" type="hidden" value="${validaMedicaoSupervisorioVO.horaRealizacaoLeitura}">
<input name="quantidadeRegistrosAtivos" type="hidden" id="quantidadeRegistrosAtivos" value="${validaMedicaoSupervisorioVO.quantidadeRegistrosAtivos}">

<input type="hidden" name="idLocalidade" id="idLocalidade" value="${validaMedicaoSupervisorioVO.idLocalidade}" />
<input type="hidden" name="idSetorComercial" id="idSetorComercial" value="${validaMedicaoSupervisorioVO.idSetorComercial}" />
<input type="hidden" name="idGrupoFaturamento" id="idGrupoFaturamento" value="${validaMedicaoSupervisorioVO.idGrupoFaturamento}" />
<input type="hidden" name="idsRota" id="idsRota" value="${validaMedicaoSupervisorioVO.idsRota}" />

<input type="hidden" name="ocorrenciaPC" id="ocorrenciaPC" value="${validaMedicaoSupervisorioVO.ocorrenciaPC}" />
<input type="hidden" name="ocorrenciaMI" id="ocorrenciaMI" value="${validaMedicaoSupervisorioVO.ocorrenciaMI}" />
<input type="hidden" name="analisadaPC" id="analisadaPC" value="${validaMedicaoSupervisorioVO.analisadaPC}" />
<input type="hidden" name="analisadaMI" id="analisadaMI" value="${validaMedicaoSupervisorioVO.analisadaMI}" />
<input type="hidden" name="transferenciaPC" id="transferenciaPC" value="${validaMedicaoSupervisorioVO.transferenciaPC}" />
<input type="hidden" name="transferenciaMI" id="transferenciaMI" value="${validaMedicaoSupervisorioVO.transferenciaMI}" />
<input type="hidden" name="ativoPC" id="ativoPC" value="${validaMedicaoSupervisorioVO.ativoPC}" />
<input type="hidden" name="ativoMI" id="ativoMI" value="${validaMedicaoSupervisorioVO.ativoMI}" />
<input type="hidden" name="statusPC" id="statusPC" value="${validaMedicaoSupervisorioVO.statusPC}" />
<input type="hidden" name="statusMI" id="statusMI" value="${validaMedicaoSupervisorioVO.statusMI}" />

<input type="hidden" name="anoMesReferenciaLidoPC" id="anoMesReferenciaLidoPC" value="${validaMedicaoSupervisorioVO.anoMesReferenciaLidoPC}" />
<input type="hidden" name="anoMesReferenciaLidoMI" id="anoMesReferenciaLidoMI" value="${validaMedicaoSupervisorioVO.anoMesReferenciaLidoMI}" />
<input type="hidden" name="enderecoRemotoLidoPC" id="enderecoRemotoLidoPC" value="${validaMedicaoSupervisorioVO.enderecoRemotoLidoPC}" />
<input type="hidden" name="enderecoRemotoLidoMI" id="enderecoRemotoLidoMI" value="${validaMedicaoSupervisorioVO.enderecoRemotoLidoMI}" />
<input type="hidden" name="dataLeituraInicialPC" id="dataLeituraInicialPC" value="${validaMedicaoSupervisorioVO.dataLeituraInicialPC}" />
<input type="hidden" name="dataLeituraInicialMI" id="dataLeituraInicialMI" value="${validaMedicaoSupervisorioVO.dataLeituraInicialMI}" />
<input type="hidden" name="dataLeituraFinalPC" id="dataLeituraFinalPC" value="${validaMedicaoSupervisorioVO.dataLeituraFinalPC}" />
<input type="hidden" name="dataLeituraFinalMI" id="dataLeituraFinalMI" value="${validaMedicaoSupervisorioVO.dataLeituraFinalMI}" />
<input type="hidden" name="dataInclusaoInicialPC" id="dataInclusaoInicialPC" value="${validaMedicaoSupervisorioVO.dataInclusaoInicialPC}" />
<input type="hidden" name="dataInclusaoInicialMI" id="dataInclusaoInicialMI" value="${validaMedicaoSupervisorioVO.dataInclusaoInicialMI}" />
<input type="hidden" name="dataInclusaoFinalPC" id="dataInclusaoFinalPC" value="${validaMedicaoSupervisorioVO.dataInclusaoFinalPC}" />
<input type="hidden" name="dataInclusaoFinalMI" id="dataInclusaoFinalMI" value="${validaMedicaoSupervisorioVO.dataInclusaoFinalMI}" />
<input type="hidden" name="idsOcorrenciaMedicaoPC" id="idsOcorrenciaMedicaoPC" value="${validaMedicaoSupervisorioVO.idsOcorrenciaMedicaoPC}" />
<input type="hidden" name="idsOcorrenciaMedicaoMI" id="idsOcorrenciaMedicaoMI" value="${validaMedicaoSupervisorioVO.idsOcorrenciaMedicaoMI}" />

<input type="hidden" name="idCliente" id="idCliente" value="${validaMedicaoSupervisorioVO.idCliente}" />
<input type="hidden" name="nomeImovel" id="nomeImovel" value="${validaMedicaoSupervisorioVO.nomeImovel}" />
<input type="hidden" name="complementoImovel" id="complementoImovel" value="${validaMedicaoSupervisorioVO.complementoImovel}" />
<input type="hidden" name="matriculaImovel" id="matriculaImovel" value="${validaMedicaoSupervisorioVO.matriculaImovel}" />
<input type="hidden" name="numeroImovel" id="numeroImovel" value="${validaMedicaoSupervisorioVO.numeroImovel}" />
<input type="hidden" name="indicadorCondominio" id="indicadorCondominio" value="${validaMedicaoSupervisorioVO.indicadorCondominio}" />
<input type="hidden" name="cepImovel" id="cepImovel" value="${validaMedicaoSupervisorioVO.cepImovel}" />

<a name="blocoSupervisorioMedicaoHoraria"></a>
<fieldset id="listaMedicoesHorarias" class="conteinerBloco">	
	
	<c:if test="${sessionScope.listaSupervisorioMedicaoHorariaVO ne null}">
				<display:table class="dataTableGGAS dataTablePopup dataTableCabecalho2Linhas" name="${sessionScope.listaSupervisorioMedicaoHorariaVO}" sort="list" id="supervisorioMedicaoHorariaVO" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoMedicaoSupervisorioHorario">
		  			
		  			<display:column title="Status" style="width: 30px">
						<img id="imagem<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>"  border="0" style="visibility:hidden">
			 		 </display:column>
		  			
		  			 <display:column title="Ativo" style="width: 30px">
				     	<c:choose>
							<c:when test="${supervisorioMedicaoHorariaVO.habilitado == true}">
								<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
							</c:otherwise>
						</c:choose>
					</display:column>
		  			
			     	<display:column title="Data" style="width: 60px">
						<fmt:formatDate pattern="dd/MM/yyyy" value="${supervisorioMedicaoHorariaVO.dataRealizacaoLeitura}" var="formattedStatusDate" />
			     		<input style="width: 80px; text-align: center" type="text" class="campoData" id="dataRealizacaoLeitura<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" name="dataRealizacaoLeitura<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" value="<c:out value="${formattedStatusDate}"/>" maxlength="10" size="10" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'dataRealizacaoLeitura', this);"/>
				    </display:column>
					
				    <display:column title="Hora"  style="width: 60px">
			     		<fmt:formatDate pattern="HH:mm" value="${supervisorioMedicaoHorariaVO.dataRealizacaoLeitura}" var="formattedStatusHour" />
			     		<input style="width: 80px; text-align: center" type="text" class="campoTexto" id="horaRealizacaoLeitura<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" name="horaRealizacaoLeitura<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onkeypress="aplicarMascaraCampoHora(this);" onblur="aplicarMascaraCampoHora(this);" value="<c:out value="${formattedStatusHour}"/>" maxlength="5" size="5" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'horaRealizacaoLeitura', this);"/>
				    </display:column>

					    <display:column sortable="false" title="Leitura n�o Corrigida"  style="width: 60px">
								<input style="width: 80px; text-align: right"  class="campoTexto" id= "leituraNaoCorrigida<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onkeypress="return formatarCampoInteiro(event)" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'leituraSemCorrecaoFatorPTZ', this);" type="text"  value="<c:out value="${supervisorioMedicaoHorariaVO.leituraSemCorrecaoFatorPTZ}"/>" />
					    </display:column>	
			     
					    <display:column title="Leitura Corrigida"  style="width: 60px">
								<input style="width: 80px; text-align: right" class="campoTexto" id= "leituraCorrigida<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onkeypress="return formatarCampoInteiro(event)" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'leituraComCorrecaoFatorPTZ', this);" type="text"  value="<c:out value="${supervisorioMedicaoHorariaVO.leituraComCorrecaoFatorPTZ}"/>" />
					    </display:column>

				    <display:column title="Press�o"  style="width: 60px">
				    
				     <c:choose>
                            <c:when test="${supervisorioMedicaoHorariaVO.pressao == 0}">
                                <input style="width: 80px; text-align: right" class="campoTexto" name="pressao" id= "pressao<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onkeypress="return formatarCampoDecimal(event,this,5,8);" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'pressao', this);" type="text"  value="<c:out value="0,00000000"/>" />
                            </c:when>
                            <c:otherwise>
                                <input style="width: 80px; text-align: right" class="campoTexto" name="pressao" id= "pressao<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onkeypress="return formatarCampoDecimal(event,this,5,8);" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'pressao', this);" type="text"  value="<c:out value="${supervisorioMedicaoHorariaVO.pressao}"/>" />
                            </c:otherwise>
                        </c:choose>
							
				    </display:column>
				    
				    <display:column title="Temperatura"  style="width: 60px">
				    
				     <c:choose>
                            <c:when test="${supervisorioMedicaoHorariaVO.temperatura == 0}">
                                <input style="width: 80px; text-align: right" class="campoTexto" name="temperatura" id= "temperatura<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onkeypress="return formatarCampoDecimal(event,this,5,8);" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'temperatura', this);" type="text"  value="<c:out value="0,00000000"/>" />
                            </c:when>
                            <c:otherwise>
                                <input style="width: 80px; text-align: right" class="campoTexto" name="temperatura" id= "temperatura<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onkeypress="return formatarCampoDecimal(event,this,5,8);" onchange="mudarImagem(<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>, 'temperatura', this);" type="text"  value="<c:out value="${supervisorioMedicaoHorariaVO.temperatura}"/>" />
                            </c:otherwise>
                        </c:choose>
				    
							
				    </display:column>
				    
				    <display:column title="Al�ada" style="width: 60px">
						<c:choose>
							  <c:when test="${supervisorioMedicaoHorariaVO.possuiAutorizacaoAlcada == true}">
							  
								  <c:choose>
								  <c:when test="${supervisorioMedicaoHorariaVO.isStatusPendente == true}">
									<span>Autorizar?</span>
									<input type="button" class="bottonRightCol3"   id="botaoAutorizar<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" name="botaoAutorizar<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onClick='autorizarSupervisorioMedicaoHoraria(<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>);' value="Sim"/>
		        					<input type="button" class="bottonRightCol3"  id="botaoNaoAutorizar<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" name="botaoNaoAutorizar<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onClick="naoAutorizarSupervisorioMedicaoHoraria('<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>')" value="N�o"/>
								  </c:when>
								  <c:otherwise>
								   	<c:out value="${supervisorioMedicaoHorariaVO.status.descricao}"/>
								  </c:otherwise>
								</c:choose>
							  
							  </c:when>
							  <c:otherwise>
							   	<c:out value="${supervisorioMedicaoHorariaVO.status.descricao}"/>
							  </c:otherwise>
							</c:choose>
					</display:column>
				    
				    <display:column title="Anormalidade"  style="width: 60px">
				    	<c:choose>
							  <c:when test="${supervisorioMedicaoHorariaVO.supervisorioMedicaoAnormalidade ne null}">
							  
								  <c:choose>
								  <c:when test="${supervisorioMedicaoHorariaVO.supervisorioMedicaoAnormalidade.indicadorImpedeProcessamento == false}">
									<img id="imagem"  alt="<c:out value="${supervisorioMedicaoHorariaVO.supervisorioMedicaoAnormalidade.descricao}"/>" title="<c:out value="${supervisorioMedicaoHorariaVO.supervisorioMedicaoAnormalidade.descricao}"/>" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
								  </c:when>
								  <c:otherwise>
								   <img id="imagem"  alt="<c:out value="${supervisorioMedicaoHorariaVO.supervisorioMedicaoAnormalidade.descricao}"/>" title="<c:out value="${supervisorioMedicaoHorariaVO.supervisorioMedicaoAnormalidade.descricao}"/>" src="<c:url value="/imagens/circle_red.png"/>" border="0">
								  </c:otherwise>
								</c:choose>
							  
							  </c:when>
							  <c:otherwise>
							    <img id="imagem"  alt="Ativo" src="<c:url value="/imagens/circle_green.png"/>" border="0">
							  </c:otherwise>
							</c:choose>
				    </display:column>
					 
					 <display:column  style="width: 60px">
							<a href="javascript:exibirDetalhamentoSupervisorioMedicaoComentario('<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
					 </display:column>
					 
					 <display:column style="width: 60px" >
		        		<input type="button" class="botaoAdicionarFaixas botaoAdicionar" id="botaoAdicionar<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" name="botaoAdicionar<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onClick="adicionarSupervisorioMedicaoHoraria('<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>', '<c:out value="${supervisorioMedicaoHorariaVO.codigoPontoConsumoSupervisorio}"/>');" title="Adicionar" />
		        		<input type="button" class="botaoRemoverFaixas botaoRemover" id="botaoRemover<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" name="botaoRemover<c:out value="${supervisorioMedicaoHorariaVO.chavePrimaria}"/>" onClick="removerSupervisorioMedicaoHoraria('<c:out value='${supervisorioMedicaoHorariaVO.chavePrimaria}'/>', '<c:out value="${supervisorioMedicaoHorariaVO.codigoPontoConsumoSupervisorio}"/>')" title="Remover"/>
				    </display:column>
				</display:table>
				
				<c:if test="${ validaMedicaoSupervisorioVO.quantidadeRegistrosAtivos == 0 }"> <div class="pagebanner pagebannerbold">N�o foram encontrados registros ativos.</div> </c:if>
                <c:if test="${ validaMedicaoSupervisorioVO.quantidadeRegistrosAtivos == 1 }"> <div class="pagebanner pagebannerbold"> Um registro ativo encontrado. </div> </c:if>
                <c:if test="${ validaMedicaoSupervisorioVO.quantidadeRegistrosAtivos > 1 }"> <div class="pagebanner pagebannerbold"> <c:out value='${ validaMedicaoSupervisorioVO.quantidadeRegistrosAtivos }'/> registros ativos encontrados. Exibindo todos os registros. </div> </c:if>		
				
				<br />
				
				<c:choose>
					<c:when test="${validaMedicaoSupervisorioVO.exigeComentario == 1}">
						<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Coment�rio sobre a altera��o da medi��o hor�ria:</label>
					</c:when>
					<c:otherwise>
						<label class="rotulo" for="observacao">Coment�rio sobre a altera��o da medi��o hor�ria:</label>
					</c:otherwise>
				</c:choose>
				<textarea id="comentario" name="comentario" class="campoTexto campoVertical" rows="5" cols="80" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoTextoComLimite(event,this,70)');"></textarea>
			</c:if>
	</fieldset>
					
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="javascript:salvarSupervisorioMedicaoHoraria();">
 	</fieldset>
	
</form:form>