<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script>
$(document).ready(function(){
	iniciarDatatable('#supervisorioMedicaoDiaria');
	
	$('#checkAllAutoMI').on('click', function(){
         var nodes =  $('#supervisorioMedicaoDiaria').dataTable().fnGetNodes(); 
         if($('#checkAllAutoMI').is(':checked')){
        	 $('.checkboxIdMI', nodes).prop('checked', true).attr('checked', 'checked');
         }else{ 
        	 $('.checkboxIdMI', nodes).prop('checked', false).removeAttr('checked');
         }
         
	 });
	
});
	

</script>

<c:if test="${listaSupervisorioMedicaoDiariaMI ne null}">
	<div class="table-responsive mt-3">
		<table class="table table-bordered table-striped table-hover" id="supervisorioMedicaoDiaria" width="100%">
			 <thead class="thead-ggas-bootstrap">
                 <tr>
                 	 <th class="text-center" data-orderable="false">
                     	<input type='checkbox' name='checkAllAuto' id='checkAllAutoMI'/>
                     </th>
                    <th scope="col" class="text-center">Anormalidade</th>
                    <th scope="col" class="text-center">Ponto de Consumo</th>
                    <th scope="col" class="text-center">Endere�o Remoto</th>
                    <th scope="col" class="text-center">Refer�ncia-Ciclo</th>
                    <th scope="col" class="text-center">Transferido</th>
                    <th scope="col" class="text-center">Integrado</th>
                    <th scope="col" class="text-center">Dias</th>
                  </tr>
               </thead>
               <tbody>
               		<c:forEach items="${listaSupervisorioMedicaoDiariaMI}" var="supervisorioMedicaoDiaria">
               			<tr>
               				<td class="text-center">
               					 <c:choose>
                					<c:when test="${supervisorioMedicaoDiaria.indicadorProcessado == true}">
                  					 	<input type="checkbox" disabled="disabled" alt="Esse item j� foi transferido" title="Esse item j� foi transferido" class="checkboxDesable" name="chavesPrimarias" value="${supervisorioMedicaoDiaria.chavePrimaria}">
                					</c:when>
                					<c:otherwise>
                    					<input id="checkboxChavesPrimarias" type="checkbox" id="chaves" name="chavesPrimarias" class="checkboxIdMI" value="${supervisorioMedicaoDiaria.chavePrimaria}">
                					</c:otherwise>
            					</c:choose>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalhar('<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>' , '<c:out value="${supervisorioMedicaoDiaria.dataReferencia}"/>', '<c:out value="${supervisorioMedicaoDiaria.numeroCiclo}"/>');"><span class="linkInvisivel"></span>
            					<c:choose>
                					<c:when test="${supervisorioMedicaoDiaria.anormalidade eq null}">
                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
               						</c:when>
                					<c:when test="${supervisorioMedicaoDiaria.anormalidade == 0}">
                    					<img id="imagem" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
                					</c:when>
                					<c:otherwise>
                    					<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
                					</c:otherwise>
            					</c:choose>
            					</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalhar('<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>' , '<c:out value="${supervisorioMedicaoDiaria.dataReferencia}"/>', '<c:out value="${supervisorioMedicaoDiaria.numeroCiclo}"/>');"><span class="linkInvisivel"></span>
                					<c:out value="${supervisorioMedicaoDiaria.pontoconsumo}"/>
            					</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalhar('<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>' , '<c:out value="${supervisorioMedicaoDiaria.dataReferencia}"/>', '<c:out value="${supervisorioMedicaoDiaria.numeroCiclo}"/>');"><span class="linkInvisivel"></span>
                					<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>
           						 </a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalhar('<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>' , '<c:out value="${supervisorioMedicaoDiaria.dataReferencia}"/>', '<c:out value="${supervisorioMedicaoDiaria.numeroCiclo}"/>');"><span class="linkInvisivel"></span>
                					<c:out value="${supervisorioMedicaoDiaria.dataReferenciaFormatado}"/>
            					</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalhar('<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>' , '<c:out value="${supervisorioMedicaoDiaria.dataReferencia}"/>', '<c:out value="${supervisorioMedicaoDiaria.numeroCiclo}"/>');"><span class="linkInvisivel"></span>
                					<c:out value="${supervisorioMedicaoDiaria.transferido}"/>
            					</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalhar('<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>' , '<c:out value="${supervisorioMedicaoDiaria.dataReferencia}"/>', '<c:out value="${supervisorioMedicaoDiaria.numeroCiclo}"/>');"><span class="linkInvisivel"></span>
                					<c:out value="${supervisorioMedicaoDiaria.integrado}"/>
            					</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalhar('<c:out value="${supervisorioMedicaoDiaria.codigoPontoConsumoSupervisorio}"/>' , '<c:out value="${supervisorioMedicaoDiaria.dataReferencia}"/>', '<c:out value="${supervisorioMedicaoDiaria.numeroCiclo}"/>');"><span class="linkInvisivel"></span>
                					<fmt:formatNumber value="${supervisorioMedicaoDiaria.qtdMedicao}" maxFractionDigits="4"/>
           						 </a>
               				</td>
               			</tr>
               		</c:forEach>
               </tbody>
		</table>
	</div>
</c:if>
