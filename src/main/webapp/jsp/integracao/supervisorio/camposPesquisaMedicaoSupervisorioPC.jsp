<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="card">
 	 <div class="card-header" id="pontoConsumo">
      <h5 class="mb-0">
        <button class="btn btn-link pontoConsumo" id="expandirPesquisaPontoConsumo" data-toggle="collapse" type="button" onclick="pesquisaMedidorIndependente();" data-target="#linkPontoConsumo" aria-expanded="true" aria-controls="linkPontoConsumo">
          Pesquisa por Ponto de Consumo
        </button>
      </h5>
    </div>
    <div id="linkPontoConsumo" class="collapse show" aria-labelledby="pontoConsumo" data-parent="#accordion">
 		<div class="card-body">
 		<div class="row">
 			<div class="col-md-6">
 				<div class="form-row">
             		 <div class="col-md-10">
             			<label for="idLocalidade">Localidade:</label>
						<select name="idLocalidade" id="idLocalidade" class="form-control form-control-sm" onchange="carregarSetoresComerciais(this);">
							<option value="-1">Selecione</option>
								<c:forEach items="${listaLocalidade}" var="localidade">
                        			<option value="<c:out value="${localidade.chavePrimaria}"/>"
                            		<c:if test="${validaMedicaoSupervisorioVO.idLocalidade == localidade.chavePrimaria}">selected="selected"</c:if>>
                            		<c:out value="${localidade.descricao}" />
                       				</option>
                    			</c:forEach>
							</select>
           		  		 </div>
          			</div>
          			
          			<div class="form-row">
             		 <div class="col-md-10">
             			<label for="idSetorComercial">Setor Comercial:</label>
						<select name="idSetorComercial" id="idSetorComercial" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
								<c:forEach items="${listaSetorComercial}" var="setorComercial">
                        			<option value="<c:out value="${setorComercial.chavePrimaria}"/>"
                            		<c:if test="${validaMedicaoSupervisorioVO.idSetorComercial == setorComercial.chavePrimaria}">selected="selected"</c:if>>
                            		<c:out value="${setorComercial.descricao}" />
                       				</option>
                    			</c:forEach>
							</select>
           		  		 </div>
          			</div>
          			<div class="form-row">
             		 <div class="col-md-10">
             			<label for="idGrupoFaturamento">Grupo de Faturamento:</label>
						<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="form-control form-control-sm" onchange="carregarRotas(this);">
							<option value="-1">Selecione</option>
								<c:forEach items="${listaGrupoFaturamento}" var="grupoFaturamento">
                       				 <option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>"
                            		 <c:if test="${validaMedicaoSupervisorioVO.idGrupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
                           			 <c:out value="${grupoFaturamento.descricao}" />
                       				 </option>
                   				</c:forEach>
							</select>
           		  		 </div>
          			</div>
          			
          			<div class="form-row">
             		 <div class="col-md-10">
             			<label for="idsRota">Rota:</label>
						<select multiple name="idsRota" id="idsRota" class="form-control form-control-sm">
							<c:forEach items="${listaRota}" var="rota">
								<c:set var="selecionado" value="false"/>
								<c:forEach items="${validaMedicaoSupervisorioVO.idsRota}" var="idRota">
									<c:if test="${idRota eq rota.chavePrimaria}">
										<c:set var="selecionado" value="true" />
									</c:if>
								</c:forEach>
								<option value="<c:out value="${rota.chavePrimaria}"/>"<c:if test="${selecionado eq true}"> selected="selected"</c:if> title="<c:out value="${rota.numeroRota}"/>">
									<c:out value="${rota.numeroRota}"/>
								</option>		
							</c:forEach>
						</select>
           		  		</div>
          			</div>
          			
          		<div class="form-row mt-2">
    	  			<label>Ocorr�ncias de Medi��es:</label>
    	  			<div class="col-md-10">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="ocorrenciaPC" id="ocorrenciaSim"
                 		value="true" class="custom-control-input"
		        		onchange="carregarOcorrenciasPC(this);"
                      	<c:if test="${validaMedicaoSupervisorioVO.ocorrenciaPC eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="ocorrenciaSim">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                		<input type="radio" name="ocorrenciaPC" id="ocorrenciaNao"
                 		value="false" class="custom-control-input"
		        		onchange="carregarOcorrenciasPC(this);"
                      	<c:if test="${validaMedicaoSupervisorioVO.ocorrenciaPC eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="ocorrenciaNao">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                		<input type="radio" name="ocorrenciaPC" id="ocorrenciaTodas"
                 		value="" class="custom-control-input"
		        		onchange="carregarOcorrenciasPC(this);"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.ocorrenciaPC}">checked</c:if>>
                      	<label class="custom-control-label" for="ocorrenciaTodas">Todas</label>
                	</div>
    			</div>
    		</div>
    		<div class="form-row">
    			<div class="col-md-10">
    			<select multiple name="idsOcorrenciaMedicaoPC" id="idsOcorrenciaMedicaoPC" class="form-control form-control-sm">
				<c:forEach items="${listaOcorrenciaMedicao}"
			            var="ocorrenciaMedicao">
			            <c:set var="selecionado" value="false" />
			            <c:forEach
			                items="${validaMedicaoSupervisorioVO.idsOcorrenciaMedicaoPC}"
			                var="idOcorrenciaMedicao">
			                <c:if
			                    test="${idOcorrenciaMedicao eq ocorrenciaMedicao.chavePrimaria}">
			                    <c:set var="selecionado" value="true" />
			                </c:if>
			            </c:forEach>
			            <option
			                value="<c:out value="${ocorrenciaMedicao.chavePrimaria}"/>"
			                <c:if test="${selecionado eq true}"> selected="selected"</c:if>
			                title="<c:out value="${ocorrenciaMedicao.descricao}"/>">
			                <c:out value="${ocorrenciaMedicao.descricao}" />
			            </option>
			        </c:forEach>
				</select>	
			</div>	
    		</div>
    		<div class="form-row">
             	<div class="col-md-6">
             		<label for="anoMesReferenciaLido">Ano/M�s de Refer�ncia:</label>
                         <input class="form-control form-control-sm" type="text" id="anoMesReferenciaLido" name="anoMesReferenciaLidoPC" 
                         value="<c:out value="${validaMedicaoSupervisorioVO.anoMesReferenciaLidoPC}"/>">
             		</div>
             		 <div class="col-md-4">
             			<label for="idCiclo">Ciclo:</label>
						<select name="idCicloPC" id="idCiclo" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCiclo}" var="ciclo">
							<option value="<c:out value="${ciclo}"/>"
								<c:if test="${validaMedicaoSupervisorioVO.idCicloPC == ciclo}">selected="selected"</c:if>>
								<c:out value="${ciclo}" />
							</option>
							</c:forEach>
						</select>
           		  	</div>
          		</div>	
 			</div>
 			<div class="col-md-6">

          		<div class="form-row">
             		 <div class="col-md-10">
             		 	 <label for="enderecoRemotoLido">Endere�o Remoto do Ponto de Consumo:</label>
                               <input class="form-control form-control-sm" type="text" id="enderecoRemotoLido" 
                               name="enderecoRemotoLidoPC" value="<c:out value="${validaMedicaoSupervisorioVO.enderecoRemotoLidoPC}"/>">
             		 	</div>
             	</div>
             	<div class="form-row">
             	 <div class="col-md-10">
             		<label>Per�odo de Leitura:</label>
             		<div class="input-group input-group-sm">
            			<input type="text" class="form-control form-control-sm campoData"
             			id="dataLeituraInicial" name="dataLeituraInicialPC" 
             			value="${validaMedicaoSupervisorioVO.dataLeituraInicialPC}" onblur="verificarFiltroPesquisa();"> 
             			 <div class="input-group-prepend">
                  		<span class="input-group-text">at�</span>
             		 </div>
              			<input type="text" class="form-control form-control-sm campoData"
              			 id="dataLeituraFinal" name="dataLeituraFinalPC"
              			 value="${validaMedicaoSupervisorioVO.dataLeituraFinalPC}" onblur="verificarFiltroPesquisa();">
       				 </div>
       				 </div>
             	</div>
             	<div class="form-row">
             	 <div class="col-md-10">
             		<label>Per�odo de Inclus�o:</label>
             		<div class="input-group input-group-sm">
            			<input type="text" class="form-control form-control-sm campoData"
             			id="dataInclusaoInicial" name="dataInclusaoInicialPC"
             			value="${validaMedicaoSupervisorioVO.dataInclusaoInicialPC}" onblur="verificarFiltroPesquisa();"> 
             			 <div class="input-group-prepend">
                  		<span class="input-group-text">at�</span>
             		 </div>
              			<input type="text" class="form-control form-control-sm campoData"
              			 id="dataInclusaoFinal" name="dataInclusaoFinalPC"
              			 value="${validaMedicaoSupervisorioVO.dataInclusaoFinalPC}" onblur="verificarFiltroPesquisa();">
       				 </div>
       				 </div>
             	</div>
             
             	<div class="form-row mt-2">
    	  			<label for="analisadaTodas">Medi��o Integrada:</label>
    	  			<div class="col-md-10">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="analisadaPC" id="analisadaSim"
                 		value="true" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.analisadaPC eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="analisadaSim">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="analisadaPC" id="analisadaNao"
                 		value="false" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.analisadaPC eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="analisadaNao">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="analisadaPC" id="analisadaTodas" 
                 		value="" class="custom-control-input"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.analisadaPC}">checked</c:if>>
                      	<label class="custom-control-label" for="analisadaTodas">Todas</label>
                	</div>
    			</div>
    		</div>
    		
    		
    		<div class="form-row mt-2">
    	  		<label for="comOcorrenciaTodas">Transfer�ncia:</label>
    	  		<div class="col-md-12">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="transferenciaPC" id="transferenciaSim"
                 		value="true" class="custom-control-input"
                      	 <c:if test="${validaMedicaoSupervisorioVO.transferenciaPC eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="transferenciaSim">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="transferenciaPC" id="transferenciaNao"
                 		value="false" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.transferenciaPC eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="transferenciaNao">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="transferenciaPC" id="transferenciaTodas"
                 		value="" class="custom-control-input"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.transferenciaPC}">checked</c:if>>
                      	<label class="custom-control-label" for="transferenciaTodas">Todas</label>
                	</div>
    			</div>
    		</div>
    		
    		<div class="form-row mt-2">
    	  		<label>Ativos:</label> 
    	  		<div class="col-md-12">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="ativoPC" id="ativoSim"
                 		value="true" class="custom-control-input"
                      	 <c:if test="${validaMedicaoSupervisorioVO.ativoPC eq true}">checked</c:if>>
                      	<label class="custom-control-label" for="ativoSim">Sim</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
            			<input type="radio" name="ativoPC" id="ativoNao"
                 		value="false" class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.ativoPC eq false}">checked</c:if>>
                      	<label class="custom-control-label" for="ativoNao">N�o</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="ativoPC" id="ativoTodas"
                 		value="" class="custom-control-input"
                      	<c:if test="${empty validaMedicaoSupervisorioVO.ativoPC}">checked</c:if>>
                      	<label class="custom-control-label" for="ativoTodas">Todas</label>
                	</div>
    			</div>
    		</div>
    		
    		<div class="form-row mt-2">
    	  		<label>Al�ada: </label>
    	  		<div class="col-md-12">
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusPC"  id="statusAutorizado"
                 		value="${statusAutorizado}"  class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.statusPC eq statusAutorizado}">checked</c:if>>
                      	<label class="custom-control-label" for="statusAutorizado">Autorizado</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusPC" id="statusNaoAutorizado" 
                 		value="${statusNaoAutorizado}"  class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.statusPC eq statusNaoAutorizado}">checked</c:if>>
                      	<label class="custom-control-label" for="statusNaoAutorizado">N�o Autorizado</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusPC" id="statusPendente" 
                 		value="${statusPendente}"  class="custom-control-input"
                      	<c:if test="${validaMedicaoSupervisorioVO.statusPC eq statusPendente}">checked</c:if>>
                      	<label class="custom-control-label" for="statusPendente">Pendente</label>
                	</div>
                	<div class="custom-control custom-radio custom-control-inline">
                 		<input type="radio" name="statusPC" id="statusTodas"
                 		 value="0"  class="custom-control-input"
                      	 <c:if test="${empty validaMedicaoSupervisorioVO.statusPC or validaMedicaoSupervisorioVO.statusPC eq 0}">checked</c:if>>
                      	<label class="custom-control-label" for="statusTodas">Todas</label>
                	</div>
    			</div>
    		</div>
	 	
 			</div>
 		</div>
 		<jsp:include page="/jsp/integracao/supervisorio/camposPesquisaMedicaoSupervisorioPA.jsp"/>
 		</div><!-- fim do card -->
 	</div>

</div>


