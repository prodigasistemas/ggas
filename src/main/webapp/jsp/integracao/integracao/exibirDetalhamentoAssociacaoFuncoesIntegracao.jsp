<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script language="javascript">
		
	function exibirTabelaItemMapeamento(chaveTabela) {
		document.getElementById('chavePrimariaItemMapeamento').value = chaveTabela;
		document.getElementById('operacao').value = "exibir";
		submeter('manterIntegracaoForm', 'exibirAbaMapeamentoDetalhamento?#integracaoAbaMapeamento');
	}

	function alterar(chavePrimaria) {
		document.forms[0].chavePrimaria.value = chavePrimaria;
		document.getElementById('operacao').value = "limpar";
		submeter('manterIntegracaoForm', 'exibirAlterarAssociacaoFuncoesIntegracao');	
	}
	
	function voltar(){		
		location.href = '<c:url value="/exibirPesquisaIntegracao"/>';
	}
	
</script>

<h1 class="tituloInterno">
	Detalhar Associa��o das Fun��es � Integra��o <a class="linkHelp"
		href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para alterar o registro clique em <span class="destaqueOrientacaoInicial">Alterar</span>,
	ou clique em <span class="destaqueOrientacaoInicial">Voltar</span><br /> 
</p>

<form:form method="post" action="exibirAbaMapeamentoDetalhamento" id="manterIntegracaoForm" name="manterIntegracaoForm">
	
	<token:token></token:token>
	<input name="acao" type="hidden" id="acao"	value="exibirAbaMapeamentoDetalhamento" />
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${integracaoSistemaFuncao.chavePrimaria}" />
	<input name="integracaoSistema" type="hidden" id="integracaoSistema">
	<input name="integracaoFuncao" type="hidden" id="integracaoFuncao">
	<input name="nome" type="hidden" id="nome"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="operacao" type="hidden" id="operacao" value="">
	<input name="isAbaMapeamento" type="hidden" id="isAbaMapeamento" value="${abaMapeamento}">
	<input name="idIntegracaoFuncao" type="hidden" id="idIntegracaoFuncao" value="${integracaoVO.idIntegracaoFuncao}"/>
	<input name="idIntegracaoSistema" type="hidden" id="idIntegracaoSistema" value="${integracaoVO.idIntegracaoSistema}"/>

	 <fieldset>
	 
	    <fieldset id="associacaoSistemaFuncao" class="colunaEsq">
	        <label class="rotulo " id="integracaoSistema"
	            for="integracaoSistemaFuncao">Sistema de integra��o:</label>
	        <span class="itemDetalhamento itemDetalhamentoLargo">
	        <c:out value="${integracaoSistemaFuncao.integracaoSistema.nome}"/></span>
	        <br/>
	        <label class="rotulo " id="integracaoSistema"
	            for="integracaoSistemaFuncao">Fun��o:</label>
	        <span class="itemDetalhamento itemDetalhamentoLargo">
	        <c:out value="${integracaoSistemaFuncao.integracaoFuncao.nome}"/></span>
	        <br/>
	      	<label class="rotulo" id="envioEmail" for="envioEmail" >Envio de e-mail?</label>
	      	<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:choose>
					<c:when test="${integracaoSistemaFuncao.indicadorEnviaEmailErro == 1}">
						<c:out value=" Sim (${integracaoSistemaFuncao.destinatariosEmail})"/>
					</c:when>
					<c:otherwise>
						<c:out value=" N�o"/>
					</c:otherwise>
				</c:choose>
			</span><br/>
	
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:choose>
					<c:when test="${integracaoSistemaFuncao.habilitado == true}">
						<c:out value=" Ativo"/>
					</c:when>
					<c:otherwise>
						<c:out value=" Inativo"/>
					</c:otherwise>
				</c:choose>
			</span><br/>	
	        
	    </fieldset>
	    
		<hr class="linhaSeparadoraPesquisa" />
	    
    </fieldset>
    
    <fieldset id="tabs" style="display: none">
		
		<ul>
			<c:if test="${not empty listaParametros || not empty listaParametrosAlteracao}">
				<li><a href="#integracaoAbaParametro">
				<c:choose><c:when test="${detalhamento != 1}">
					<span class="campoObrigatorioSimboloTabs">* </span>
				</c:when></c:choose>
				Par�metro</a></li>
			</c:if>
			<c:if test="${not empty listaMapeamentos}">
				<li><a href="#integracaoAbaMapeamento">Mapeamento</a></li>
			</c:if>
		</ul>
		
		<c:if test="${not empty listaParametros || not empty listaParametrosAlteracao}">
			<fieldset id="integracaoAbaParametro">
				<jsp:include page="/jsp/integracao/integracao/abaParametro.jsp">
					<jsp:param name="actionAdicionarParametro" value="adicionarParametroDoSistemaFluxoInclusao" />				
				</jsp:include>
			</fieldset>
		</c:if>
		
		<c:if test="${not empty listaMapeamentos}">
			<fieldset id="integracaoAbaMapeamento">
				<jsp:include page="/jsp/integracao/integracao/abaMapeamento.jsp">
					<jsp:param name="actionAdicionarMapeamento" value="adicionarMapeamentoIntegracao" />				
				</jsp:include>
			</fieldset>
		</c:if>
	
	</fieldset>
		
	<hr class="linhaSeparadoraPesquisa" />
	<input name="button" value="Voltar" class="bottonRightCol2" onclick="voltar()" type="button" />
	<input name="button3" value="Alterar" class="bottonRightCol2 botaoGrande1" onclick="alterar(<c:out value='${integracaoSistemaFuncao.chavePrimaria}'/>)" type="button" />
				
</form:form>
