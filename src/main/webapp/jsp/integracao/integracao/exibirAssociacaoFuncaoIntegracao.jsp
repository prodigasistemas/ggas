<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script language="javascript">    

	function setarIndicadorEnviaEmailErro(obj){	
		document.forms[0].indicadorEnviaEmailErro.value = obj.value;	
		carregarCampoEmail();
	}
	
	function carregarCampoEmail(){
		if(document.forms[0].indicadorEnviaEmailErro[0].checked == true){
	        document.manterIntegracaoForm.destinatariosEmail.disabled = false;
	        document.manterIntegracaoForm.destinatariosEmail.style.display = 'inline';
	   }else{
	        document.manterIntegracaoForm.destinatariosEmail.disabled = true;
	        document.manterIntegracaoForm.destinatariosEmail.style.display = 'none';
	   }
	}
	
	function aplicarFuncao(form, actionAdicionarFuncao) {
		if(document.getElementById("integracaoSistema").value==-1){
			alert("Selecione um Sistema de Integra��o!");
			document.getElementById("integracaoSistema").focus();
			document.getElementById("integracaoFuncao").value = -1;
		} else {
			if(document.getElementById("integracaoFuncao").value==-1){
				alert("Uma fun��o deve ser selecionada.");
			}
			submeter('manterIntegracaoForm','exibirAssociacaoFuncaoIntegracao');
		}
	}
	
	function exibirTabelaItemMapeamento(chaveTabela) {
		document.getElementById('chavePrimariaItemMapeamento').value = chaveTabela;
		document.getElementById('operacao').value = "exibir";
		document.getElementById('isMapeamentoTrocado').value = true;
		submeter('manterIntegracaoForm', 'exibirAbaMapeamento?#integracaoAbaMapeamento');
	}

 	function removerMapeamento(chave, codigo1, codigo2){
		document.getElementById('codigoEntidadeSistemaGgasRemocao').value = codigo1;
		document.getElementById('codigoEntidadeSistemaIntegranteRemocao').value = codigo2;
		document.getElementById('operacao').value = "removerMapeamento";
		submeter('manterIntegracaoForm', 'exibirAbaMapeamento?#integracaoAbaMapeamento');

 	}
	
	function adicionarMapeamento(chave){
		submeter('manterIntegracaoForm', 'exibirAbaMapeamento?#integracaoAbaMapeamento');
	}

	function salvar() {
        submeter('manterIntegracaoForm', 'salvarIntegracao');      
    }
    
    function cancelar(){        
        location.href = '<c:url value="/exibirPesquisaIntegracao"/>';      
    }
		
	function init() {
		carregarCampoEmail();
	}	
	
    function limpar(){
    	$("#integracaoSistema").val("-1");
    	$("#integracaoFuncao").val("-1");
    	$("input[name=indicadorEnviaEmailErro]").val("1");
    	$("#destinatariosEmail").val("");
		document.getElementById('operacao').value = "limpar";
		submeter('manterIntegracaoForm', 'exibirAssociacaoFuncaoIntegracao');
    }

	addLoadEvent(init);
    
</script>

<h1 class="tituloInterno">
    Associar Fun��o � Integra��o <a class="linkHelp"
        href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
        target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
    Para associar uma fun��o a um sistema de integra��o, selecione o sistema integrante
    e em seguida escolha uma fun��o, preencha as informa��es de par�metros e os mapeamentos, se houver, e clique em <span class="destaqueOrientacaoInicial">Salvar</span>
    para salvar a associa��o.<br/>Para cancelar clique em <span class="destaqueOrientacaoInicial">Cancelar</span>.<br />
</p>

<form:form method="post" action="exibirAssociacaoFuncaoIntegracao" id="manterIntegracaoForm" name="manterIntegracaoForm">

	<token:token></token:token>
    <input name="acao" type="hidden" id="acao"  value="exibirAssociacaoFuncaoIntegracao" />
    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
    <input name="chavePrimaria" type="hidden" id="chavePrimaria">    
    <input name="idSistema" type="hidden" id="idSistema">
    <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
    <input name="mapeamentos" type="hidden" id="mapeamentos" value="${mapeamentos}">
    <input name="operacao" type="hidden" id="operacao" value="">
    <input name="isMapeamentoTrocado" type="hidden" id="isMapeamentoTrocado" value=""/>

	<fieldset id="integracao" class="conteinerBloco">
	
      <fieldset id="associacaoSistemaFuncao" class="colunaEsq" style="padding-right: 15px;">
          <label class="rotulo" id="rotuloAssociarSistemaFuncao"             
              for="integracaoSistema">Sistema de integra��o:</label> 
          <select name="integracaoSistema" id="integracaoSistema" class="campoSelect campo2Linhas">                
              <option value="-1">Selecione</option>
              <c:forEach items="${listaIntegracaoSistema}" var="integracaoSistema">
                  <option value="<c:out value="${integracaoSistema.chavePrimaria}"/>"
                      <c:if test="${integracaoVO.integracaoSistema == integracaoSistema.chavePrimaria}">selected="selected"</c:if>>
                      <c:out value="${integracaoSistema.nome}" />                     
                  </option>
              </c:forEach>
          </select>
          <br/>
      </fieldset>
       <fieldset id="associacaoSistemaFuncao" class="colunaEsq" style="padding-right: 15px;">
         <label class="rotulo" id="rotuloAssociarSistemaFuncao"             
	              for="integracaoSistema">Fun��o:</label> 
         <select name="integracaoFuncao" id="integracaoFuncao" class="campoSelect" 
	          	onchange="aplicarFuncao(this.form,'<c:out value="${param['actionAdicionarFuncao']}"/>');" >
			<option value="-1">Selecione</option>
			<c:forEach items="${listaIntegracaoFuncao}" var="integracaoFuncao">
				<option value="<c:out value="${integracaoFuncao.chavePrimaria}"/>" title="<c:out value="${integracaoFuncao.nome}"/>" <c:if test="${integracaoVO.integracaoFuncao == integracaoFuncao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${integracaoFuncao.nome}"/>
				</option>
			</c:forEach>
		</select>
      </fieldset> 
	<hr class="linhaSeparadora" />
 	<fieldset id="associacaoSistemaFuncao" class="colunaEsq" style="padding-right: 15px;">
		<label class="rotulo" id="envioEmail" for="envioEmail" ><span class="campoObrigatorioSimbolo2">* </span>Envio de e-mail?</label>
		<input class="campoRadio" type="radio" id="indicadorEnviaEmailErro" name="indicadorEnviaEmailErro" onclick="setarIndicadorEnviaEmailErro(this)" value="1" <c:if test="${integracaoVO.indicadorEnviaEmailErro == 1}">checked="checked"</c:if>>
		<label class="rotuloRadio" for="indicadorEnvio">Sim</label>
		<input class="campoRadio" type="radio" id="indicadorEnviaEmailErro" name="indicadorEnviaEmailErro" onclick="setarIndicadorEnviaEmailErro(this)" value="0" <c:if test="${integracaoVO.indicadorEnviaEmailErro == 0}">checked="checked"</c:if>>
		<label class="rotuloRadio" for="indicadorEnvio">N�o</label>
		<input class="campoTexto campoHorizontal" type="text" name="destinatariosEmail" id="destinatariosEmail" size="40" value="${integracaoVO.destinatariosEmail}"/><br />
		<span class="legenda" style="margin-top: -3px; ">Obs: separe os e-mails por v�rgula</span>
	</fieldset>
	
    <c:if test="${not empty listaIntegracaoSistemaFuncao}">   
    <hr class="linhaSeparadora" />                          
     <fieldset id="tabelaAssociacaoFuncao" class="coluna2">      
         <hr class="linhaSeparadoraPesquisa" />
             <display:table class="dataTableGGAS" name="${listaIntegracaoSistemaFuncao}"
                 sort="list" id="integracaoSistemaFuncao"
                 decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
                 pagesize="15"
                 excludedParams="org.apache.struts.taglib.html.TOKEN acao"
                 requestURI="pesquisarAssociacaoSistemaFuncao">
                 
             <display:column style="text-align: center;" title="Fun��o"
                     sortable="true" sortProperty="chavePrimaria" >                  
                 <c:out value='${integracaoSistemaFuncao.integracaoFuncao.nome}'/>                   
             </display:column>
             <display:column title="Indicadores" style="width: 30px">
                     <c:choose>
                         <c:when test="${integracaoSistemaFuncao.indicadorEnviaEmailErro == 1}">
                             <c:out value="Sim"/>
                         </c:when>
                         <c:otherwise>
                             <c:out value="N�o"/>
                         </c:otherwise>
                     </c:choose>
             </display:column>
             <display:column style="text-align: center; " title="E-mail"
                 sortable="false">
                 <c:out value='${integracaoSistemaFuncao.destinatariosEmail}'/>
             </display:column>               
             <display:column title="Par�metro" style="width: 30px"></display:column>
             <display:column title="Mapeamento" style="width: 30px"></display:column>
         </display:table>
	</fieldset>
	</c:if>
	</fieldset>
	
	<hr class="linhaSeparadora" />
			
	<fieldset id="tabs" style="display: none">
		
		<ul>
			<c:if test="${not empty listaParametros}">
				<li><a href="#integracaoAbaParametro"><span class="campoObrigatorioSimboloTabs">* </span>Par�metro</a></li>
			</c:if>
			<c:if test="${not empty listaMapeamentos}">
				<li><a href="#integracaoAbaMapeamento">Mapeamento</a></li>
			</c:if>
		</ul>
		
		<c:if test="${not empty listaParametros}">
			<fieldset id="integracaoAbaParametro">
				<jsp:include page="/jsp/integracao/integracao/abaParametro.jsp">
					<jsp:param name="actionAdicionarParametro" value="adicionarParametroDoSistemaFluxoInclusao" />				
				</jsp:include>
			</fieldset>
		</c:if>
		
		<c:if test="${not empty listaMapeamentos}">
			<fieldset id="integracaoAbaMapeamento">
				<jsp:include page="/jsp/integracao/integracao/abaMapeamento.jsp">
					<jsp:param name="actionAdicionarMapeamento" value="adicionarMapeamentoIntegracao" />				
				</jsp:include>
			</fieldset>
		</c:if>
	
	</fieldset>

	<script language="javascript">carregarCampoEmail();</script>
	
	<fieldset class="conteinerBotoes">
	    <input name="button" value="Cancelar" class="bottonRightCol2" onclick="cancelar()" type="button" />                 
	    <input name="Button" value="Limpar" class="bottonRightCol botaoRemover" id="Limpar" onclick="limpar()" type="button">     
	    <input name="button" value="Salvar" class="bottonRightCol2 botaoGrande1" onclick="salvar()" type="button" />
	</fieldset>

</form:form>