<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>

//Fun��o para aceitar apenas n�meros.
function apenasNumeros() {
    var initVal = $(this).val();
    outputVal = initVal.replace(/\D/g,"");    
    if (initVal != outputVal) {
        $(this).val(outputVal);
    }
}

	//Estilos iniciais para os bot�es de Adicionar e Remover Faixas de Tarifas 
	$(document).ready(function(){
    
		$("input.botaoAdicionarFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
		$("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
	
		$("input.botaoAdicionarFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			}
		);
	
		$("input.botaoRemoverFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			}
		);
	});

</script>

<input name="mapeamentoExclusao" type="hidden" id="mapeamentoExclusao">
<input name="chavePrimariaItemMapeamento" type="hidden" id="chavePrimariaItemMapeamento" value="${integracaoVO.chavePrimariaItemMapeamento}">
<input name="chavePrimariaItemMapeamentoAnterior" type="hidden" id="chavePrimariaItemMapeamentoAnterior" value="${integracaoVO.chavePrimariaItemMapeamento}">
<input name="codigoEntidadeSistemaGgasRemocao" type="hidden" id="codigoEntidadeSistemaGgasRemocao">
<input name="codigoEntidadeSistemaIntegranteRemocao" type="hidden" id="codigoEntidadeSistemaIntegranteRemocao">

 <fieldset id="funcaoIntegracao" > 
	<display:table class="dataTableGGAS dataTableAba" name="listaMapeamentos" sort="list" id="mapeamento"  requestURI="#integracaoAbaMapeamento" >
		<display:column style="width: 30px">
			<c:choose>
				<c:when test="${mapeamento.tabela.chavePrimaria == integracaoVO.chavePrimariaItemMapeamento}">
					<img alt="Ativo" title="Ativo"
						src="<c:url value="/imagens/success_icon.png"/>" border="0">
				</c:when>
			</c:choose>
		</display:column>	
		<display:column sortable="false"  title="Nome" >
			<a href="javascript:exibirTabelaItemMapeamento('<c:out value='${mapeamento.tabela.chavePrimaria}'/>');" >
	 			<span class="linkInvisivel"></span><c:out value='${mapeamento.tabela.nome}'/></a>
		</display:column>
		<display:column sortable="false" title="Descricao" >
			<a href="javascript:exibirTabelaItemMapeamento('<c:out value='${mapeamento.tabela.chavePrimaria}'/>');" >
	 			<span class="linkInvisivel"></span><c:out value='${mapeamento.tabela.descricao}'/></a>
		</display:column> 
		<c:set var="i" value="${i+1}" />
	</display:table>
</fieldset> 
<br/>
<c:if test="${listaMapeamento ne null}">
<fieldset id="blocoMapeamento">
	<c:set var="i" value="0" />	
		<display:table class="dataTableGGAS" name="listaMapeamento" sort="list" id="mapeamento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#integracaoAbaMapeamento" style="width: 870px">
	        <display:column title="GGAS" style="width: 50%">
	        	<input type="hidden" id="chaveMapeamento" name="chaveMapeamento" value="${mapeamento.chavePrimaria}"/>
	        	
	        	<c:choose>
					<c:when test="${detalhamento != 1}">
						<input type="text" class="campoTexto" id="codigoEntidadeSistemaGgas" name="codigoEntidadeSistemaGgas" size="10" maxlength="9" value="${mapeamento.codigoEntidadeSistemaGgas}" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');" onkeypress="return formatarCampoInteiro(event);" />				
					</c:when>
					<c:otherwise>
			       		<c:out value="${mapeamento.codigoEntidadeSistemaGgas}"/>
					</c:otherwise>
				</c:choose>
	        	
	        </display:column>
	        <display:column title="Sistema Integrado" >
	        
	        	<c:choose>
					<c:when test="${detalhamento != 1}">
						<input type="text" class="campoTexto" id="codigoEntidadeSistemaIntegrante" name="codigoEntidadeSistemaIntegrante" size="10" maxlength="9" value="${mapeamento.codigoEntidadeSistemaIntegrante}" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');" onkeypress="return formatarCampoInteiro(event);" />
					</c:when>
					<c:otherwise>
			       		<c:out value="${mapeamento.codigoEntidadeSistemaIntegrante}"/>
					</c:otherwise>
				</c:choose>
				
	        </display:column>
	        
	   		<c:choose>
				<c:when test="${detalhamento != 1}">

			        <display:column title="Adicionar/Remover" >
				        <c:choose>
				        	<c:when test="${fn:length(listaMapeamento)-1 == i}">
				        		<input type="button" class="botaoAdicionarFaixas botaoAdicionar" onClick="adicionarMapeamento('<c:out value='${mapeamento.chavePrimaria}'/>');" title="Adicionar" />
				        	</c:when>
				        	<c:otherwise>
				        		<input type="button" class="botaoAdicionarFaixas botaoAdicionar" onClick="adicionarMapeamento('<c:out value='${mapeamento.chavePrimaria}'/>');" title="Adicionar" />
				        		<input type="button" class="botaoRemoverFaixas botaoRemover" onClick="removerMapeamento('<c:out value='${mapeamento.chavePrimaria}'/>','<c:out value='${mapeamento.codigoEntidadeSistemaGgas}'/>','<c:out value='${mapeamento.codigoEntidadeSistemaIntegrante}'/>')" title="Remover"/>
				        	</c:otherwise>
				        </c:choose>
				    </display:column>
				   
				</c:when>
			</c:choose>
				    
	       <c:set var="i" value="${i+1}" />		
	    </display:table>
</fieldset>
</c:if>