<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<c:if test="${listaParametros ne null}">
	<display:table class="dataTableGGAS dataTableAba" name="listaParametros" sort="list" id="parametro" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
        <display:column sortable="false" title="C�digo" sortProperty="codigo" style="text-align: center;">	
   			<input type="hidden" id="chaveParametro" name="chaveParametro" value="${parametro.chavePrimaria}"/>        	
           	<c:out value="${parametro.parametroSistema.codigo}"/>	            
        </display:column>
		<display:column sortable="false"  title="Valor">
			<input id="valor" class="campoTexto" type="text" name="valor" value="<c:out value="${parametro.parametroSistema.valor}"/>">		
		</display:column>
        <display:column sortable="false" title="Descri��o" sortProperty="descricao" style="text-align: center;">	        	
            <c:out value="${parametro.parametroSistema.descricao}"/>	            
        </display:column>
	</display:table>
</c:if>
<c:if test="${listaParametrosAlteracao ne null}">
	<display:table class="dataTableGGAS dataTableAba" name="listaParametrosAlteracao" sort="list" id="parametro" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
        <display:column sortable="false" title="C�digo" sortProperty="codigo" style="text-align: center;">	
   			<input type="hidden" id="chaveParametro" name="chaveParametro" value="${parametro.chavePrimaria}"/>        	
           	<c:out value="${parametro.codigo}"/>	            
        </display:column>
		<display:column sortable="false"  title="Valor">
			<c:choose>
				<c:when test="${detalhamento == 1}">
					<c:out value="${parametro.valor}"/>
				</c:when>
				<c:otherwise>
					<input id="valor" class="campoTexto" type="text" maxlength="255" name="valor" value="<c:out value="${parametro.valor}"/>">	
				</c:otherwise>
			</c:choose>
		</display:column>
        <display:column sortable="false" title="Descri��o" sortProperty="descricao" style="text-align: center;">	        	
            <c:out value="${parametro.descricao}"/>	            
        </display:column>
	</display:table>
</c:if>

