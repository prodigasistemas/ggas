<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<h1 class="tituloInterno">
	Pesquisar Integra��o<a class="linkHelp"
		href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para pesquisar um registro espec�fico, informe os dados nos campos
	abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>,
	ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>
	para exibir todos.<br /> Para associar um novo registro clique em <span
		class="destaqueOrientacaoInicial">Associar</span>
</p>

<form:form method="post" action="pesquisarIntegracao" id="manterIntegracaoForm" name="manterIntegracaoForm">

<script language="javascript">	
	
	function associarSistemaFuncao() {		
		location.href = '<c:url value="exibirAssociacaoFuncaoIntegracao"/>';

	}
	
	function alterarSistemaFuncao() {
		
		var verficaSelecao = verificarSelecaoApenasUm();	
		
		if (verficaSelecao == true) {
			
			document.getElementById('habilitado').value = document.forms[0].habilitado.value;
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			
			var checkbox = $('input[name="chavesPrimarias"]:checked');
			
			var integracaoFuncao = checkbox.attr("maxlength");
			var integracaoSistema = checkbox.attr("size");
			
			document.forms[0].idIntegracaoFuncao.value = integracaoFuncao;
			document.forms[0].idIntegracaoSistema.value = integracaoSistema;
			
			submeter('manterIntegracaoForm', 'exibirAlterarAssociacaoFuncoesIntegracao');
		}
 		
	}
	
	function detalharSistemaFuncao(integracaoFuncao, integracaoSistema) {
		document.forms[0].idIntegracaoFuncao.value = integracaoFuncao;
		document.forms[0].idIntegracaoSistema.value = integracaoSistema;
		submeter('manterIntegracaoForm', 'exibirDetalhamentoAssociacaoFuncoesIntegracao'); 		
 		
	}
	
	function removerSistemaFuncao(){
		
		var selecao = verificarSelecao();		
				
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('manterIntegracaoForm', 'removerSistemaFuncao');
				}
		    }
		}
// 		var selecao = verificarSelecao();
//  		var chaves = document.getElementById("chavesPrimarias");
// 		if (selecao == true) {	
//  			document.forms[0].chavesPrimarias.value = obterValorUnicoCheckboxSelecionadoGeral(chaves);
// 			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
// 			if(retorno == true) {
// 				submeter('manterIntegracaoForm', 'removerSistemaFuncao');
// 			}
// 	    }
	
	
	function limparFormulario(){
		
		document.getElementById('integracaoSistema').value = "-1";	
		document.getElementById('integracaoFuncao').value = "-1";		
		document.forms[0].habilitado[0].checked = true;
		
	}
	
</script>

	<input name="acao" type="hidden" id="acao"	value="pesquisarIntegracao" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"/>
	
	<input name="nome" type="hidden" id="nome"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true"/>
	
	<input name="idIntegracaoFuncao" type="hidden" id="idIntegracaoFuncao"/>
	<input name="idIntegracaoSistema" type="hidden" id="idIntegracaoSistema" />

	<fieldset class="conteinerPesquisarIncluirAlterar">

		<fieldset id="funcionarioCol2" class="coluna">
			<label class="rotulo" id="rotuloIntegracaoSistema"
				for="integracaoSistema">Sistema de integra��o:</label> <select
				name="integracaoSistema" id="integracaoSistema"
				class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaIntegracaoSistema}" var="integracaoSistema">
					<option value="<c:out value="${integracaoSistema.chavePrimaria}"/>"
						<c:if test="${integracaoVO.integracaoSistema == integracaoSistema.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${integracaoSistema.nome}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo campoObrigatorio" id="rotuloFuncao"
				for="integracaoFuncao">Fun��o:</label> <select name="integracaoFuncao" id="integracaoFuncao"
				class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaIntegracaoFuncao}" var="integracaoFuncao">
					<option value="<c:out value="${integracaoFuncao.chavePrimaria}"/>"
						<c:if test="${integracaoVO.integracaoFuncao == integracaoFuncao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${integracaoFuncao.nome}" />
					</option>
				</c:forEach>
			</select>
		</fieldset>

		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${integracaoVO.habilitado eq true}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${integracaoVO.habilitado eq false}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty integracaoVO.habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Todos</label>

		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDir">	

			<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
					value="Pesquisar" type="submit">			
			<input name="Button" class="bottonRightCol bottonRightColUltimo" id="Limpar" 
				value="Limpar" type="button" onclick="limparFormulario();">		
							
		</fieldset>
		
	</fieldset><br/>
		
		
			<c:if test="${listaIntegracaoSistemaFuncaoGrid ne null}">
				<hr class="linhaSeparadoraPesquisa" />
				<display:table class="dataTableGGAS" name="listaIntegracaoSistemaFuncaoGrid"
					sort="list" id="integracaoSistemaFuncao"
					decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
					pagesize="15"
					excludedParams="org.apache.struts.taglib.html.TOKEN acao"
					requestURI="pesquisarIntegracao">

				<display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
      				<input type="checkbox" name="chavesPrimarias" value="${integracaoSistemaFuncao.chavePrimaria}" maxlength="${integracaoSistemaFuncao.integracaoFuncao.chavePrimaria}" size="${integracaoSistemaFuncao.integracaoSistema.chavePrimaria}">
				</display:column>

				<display:column title="Ativo" style="width: 30px">
						<c:choose>
							<c:when test="${integracaoSistemaFuncao.habilitado == true}">
								<img alt="Ativo" title="Ativo"
									src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img alt="Inativo" title="Inativo"
									src="<c:url value="/imagens/cancel16.png"/>" border="0">
							</c:otherwise>
						</c:choose>
				</display:column>	
				<display:column style="text-align: center;" title="Sistema Integrante"
 						sortable="true" sortProperty="integracaoSistema.nome" >				
 						<a href='javascript:detalharSistemaFuncao(<c:out value='${integracaoSistemaFuncao.integracaoFuncao.chavePrimaria}'/>,
 							<c:out value='${integracaoSistemaFuncao.integracaoSistema.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
 						<c:out value='${integracaoSistemaFuncao.integracaoSistema.nome}'/></a>
 				</display:column>
 				<display:column style="text-align: center;" title="Fun��o"
 						sortable="true" sortProperty="integracaoFuncao.nome" >
						<a href='javascript:detalharSistemaFuncao(<c:out value='${integracaoSistemaFuncao.integracaoFuncao.chavePrimaria}'/>,
 							<c:out value='${integracaoSistemaFuncao.integracaoSistema.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${integracaoSistemaFuncao.integracaoFuncao.nome}'/></a>
				</display:column>
				<display:column style="text-align: center;" title="Pend�ncia" >
							<c:out value='${integracaoSistemaFuncao.pendencia}'/>
				</display:column>
			</display:table>
		</c:if>	
	
	
	<c:if test="${empty listaIntegracaoSistemaFuncaoGrid}">
		<fieldset class="conteinerBotoes">		
					
			<input name="button" value="Associar" class="bottonRightCol2 botaoGrande1" onclick="associarSistemaFuncao()" type="button">		
							
		</fieldset>
	</c:if>	
	<c:if test="${not empty listaIntegracaoSistemaFuncaoGrid}">
		<fieldset class="conteinerBotoes">
			
			<input name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarSistemaFuncao()" type="button">
			<input name="buttonRemover" value="Remover" class="bottonRightCol botaoRemover" id="Remover" onclick="removerSistemaFuncao()" type="button">		
			<input name="button" value="Associar" class="bottonRightCol2 botaoGrande1" onclick="associarSistemaFuncao()" type="button">		
							
		</fieldset>
	</c:if>


</form:form>
