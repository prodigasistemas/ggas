<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script language="javascript">

	function setarIndicadorEnviaEmailErro(obj){	
		document.forms[0].indicadorEnviaEmailErro.value = obj.value;	
		carregarCampoEmail();
	}
	
	function carregarCampoEmail(){
		if(document.forms[0].indicadorEnviaEmailErro[0].checked == true){
	        document.manterIntegracaoForm.destinatariosEmail.disabled = false;
	        document.manterIntegracaoForm.destinatariosEmail.style.display = 'inline';
	   }else{
	        document.manterIntegracaoForm.destinatariosEmail.disabled = true;
	        document.manterIntegracaoForm.destinatariosEmail.style.display = 'none';
	   }
	}

	function exibirTabelaItemMapeamento(chaveTabela) {
		document.getElementById("chavePrimariaItemMapeamento").value = chaveTabela;
		document.getElementById('operacao').value = "exibir";
		document.getElementById('isMapeamentoTrocado').value = true;
		submeter('manterIntegracaoForm', 'exibirAbaMapeamento?#integracaoAbaMapeamento');

	}
	
	function removerMapeamento(chave, codigo1, codigo2){
		document.getElementById('codigoEntidadeSistemaGgasRemocao').value = codigo1;
		document.getElementById('codigoEntidadeSistemaIntegranteRemocao').value = codigo2;
		document.getElementById('operacao').value = "removerMapeamento";
		submeter('manterIntegracaoForm', 'exibirAbaMapeamento?#integracaoAbaMapeamento');
	}
	
	function adicionarMapeamento(chave){
		submeter('manterIntegracaoForm', 'exibirAbaMapeamento?#integracaoAbaMapeamento');
	}

	var habilitado = null;
	
	function habilitar(habilitar){		
		document.getElementById('habilitado').value = habilitar;
	}
	
	function salvar() {
    	document.getElementById('operacao').value = "salvarAlteracao";
    	submeter('manterIntegracaoForm', 'salvarAlteracaoIntegracao');
	}
	
    function cancelar(){        
        location.href = '<c:url value="/exibirPesquisaIntegracao"/>';      
    }
    
	function init() {
		carregarCampoEmail();
	}	

	addLoadEvent(init);
    
</script>

<h1 class="tituloInterno">
    Alterar Associa��o das Fun��es � Integra��o<a class="linkHelp"
        href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
        target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
    Altere os dados que estiverem dispon�veis para altera��o e clique em <span class="destaqueOrientacaoInicial">Salvar</span>
    para alterar a associa��o.<br/>Para cancelar clique em <span class="destaqueOrientacaoInicial">Cancelar</span>.<br />
</p>

<form:form method="post" action="exibirAlterarAssociacaoFuncoesIntegracao" id="manterIntegracaoForm" name="manterIntegracaoForm">

	<token:token></token:token>
    <input name="acao" type="hidden" id="acao"  value="exibirAlterarAssociacaoFuncoesIntegracao" />
    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" />
    <input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${integracaoVO.integracaoSistemaFuncao}" />
    <input name="nome" type="hidden" id="nome"/>
    <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true" />
    <input name="idIntegracaoSistemaFuncao" type="hidden" id="idIntegracaoSistemaFuncao" value="${integracaoVO.integracaoSistemaFuncao}"/>
    <input name="operacao" type="hidden" id="operacao" value="">
    <input name="idIntegracaoFuncao" type="hidden" id="idIntegracaoFuncao" value="${integracaoVO.idIntegracaoFuncao}"/>
	<input name="idIntegracaoSistema" type="hidden" id="idIntegracaoSistema" value="${integracaoVO.idIntegracaoSistema}"/>
	<input name="isMapeamentoTrocado" type="hidden" id="isMapeamentoTrocado" value=""/>
    
    <fieldset>
    
        <fieldset id="associacaoSistemaFuncao" class="colunaEsq">
            <label class="rotulo " id="integracaoSistema"
                for="integracaoSistemaFuncao">Sistema de integra��o:</label>
            <span class="itemDetalhamento itemDetalhamentoLargo">
            <c:out value="${integracaoSistemaFuncao.integracaoSistema.nome}"/></span>
            <br/>
            <label class="rotulo " id="integracaoSistema"
                for="integracaoSistemaFuncao">Fun��o:</label>
            <span class="itemDetalhamento itemDetalhamentoLargo">
            <c:out value="${integracaoSistemaFuncao.integracaoFuncao.nome}"/></span>
            <br/>
           	<label class="rotulo" id="envioEmail" for="envioEmail" ><span class="campoObrigatorioSimbolo2">* </span>Envio de e-mail?</label>
			<input class="campoRadio" type="radio" id="indicadorEnviaEmailErro" name="indicadorEnviaEmailErro" onclick="setarIndicadorEnviaEmailErro(this)" value="1" <c:if test="${integracaoVO.indicadorEnviaEmailErro == 1}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="indicadorEnvio">Sim</label>
			<input class="campoRadio" type="radio" id="indicadorEnviaEmailErro" name="indicadorEnviaEmailErro" onclick="setarIndicadorEnviaEmailErro(this)" value="0" <c:if test="${integracaoVO.indicadorEnviaEmailErro == 0}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="indicadorEnvio">N�o</label>
			<input class="campoTexto campoHorizontal" type="text" name="destinatariosEmail" id="destinatariosEmail" size="40" value="${integracaoVO.destinatariosEmail}"/><br />
			<span class="legenda" style="margin-top: -3px; ">Obs: separe os e-mails por v�rgula</span>
			<br/> <br/>   <br/><br/>			
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" onclick="habilitar(this.value)" <c:if test="${integracaoVO.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" onclick="habilitar(this.value)" <c:if test="${integracaoVO.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>            
        </fieldset>
            
    <hr class="linhaSeparadoraPesquisa" />
        
    </fieldset>
    
    	<fieldset id="tabs" style="display: none">
		
		<ul>
			<c:if test="${not empty listaParametros || not empty listaParametrosAlteracao}">
				<li><a href="#integracaoAbaParametro"><span class="campoObrigatorioSimboloTabs">* </span>Par�metro</a></li>
			</c:if>
			<c:if test="${not empty listaMapeamentos}">
				<li><a href="#integracaoAbaMapeamento">Mapeamento</a></li>
			</c:if>
		</ul>
		
		<c:if test="${not empty listaParametros || not empty listaParametrosAlteracao}">
			<fieldset id="integracaoAbaParametro">
				<jsp:include page="/jsp/integracao/integracao/abaParametro.jsp">
					<jsp:param name="actionAdicionarParametro" value="adicionarParametroDoSistemaFluxoInclusao" />				
				</jsp:include>
			</fieldset>
		</c:if>
		
		<c:if test="${not empty listaMapeamentos}">
			<fieldset id="integracaoAbaMapeamento">
				<jsp:include page="/jsp/integracao/integracao/abaMapeamento.jsp">
					<jsp:param name="actionAdicionarMapeamento" value="adicionarMapeamentoIntegracao" />				
				</jsp:include>
			</fieldset>
		</c:if>
	
	</fieldset>
	
	<script language="javascript">carregarCampoEmail();</script>
	
    <fieldset class="conteinerBotoes">
        <input name="buttonCancelar" value="Cancelar" class="bottonRightCol2" onclick="cancelar()" type="button" />
        <input name="buttonSalvar" value="Salvar" class="bottonRightCol2 botaoGrande1" onclick="salvar()" type="button" />
    </fieldset>

</form:form>
