<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Evento Comercial<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos abaixo para realizar a altera��o.</p>

<script type="text/javascript">
	

$(document).ready(function(){
	document.getElementById('botaoConfigurar').disabled=true;
	
	$("table#eventoComercial input:radio").click(function(){
		$("table#eventoComercial tr").removeClass("selectedRow");
		$(this).closest("tr").addClass("selectedRow");
	});
	
});

	
	function setarStatus(chave,descricao){
		document.getElementById('checkEventoComercial').value = chave;
		document.getElementById('botaoConfigurar').disabled=false;
		document.getElementById('descricao').value = descricao;
	}
	
		
	function configurar(){
		
		$("#acao").val(true);
		
	    submeter("contabilForm", "configurarEventoComercial");
	}

	function limparFormulario(){
				
		document.getElementById('eventoComercialDescricao').value = "";
		document.getElementById('descricaoHistorico').value = "";
		document.getElementById('descricaoContaAuxiliarDebito').value = "";
		document.getElementById('descricaoContaAuxiliarCredito').value = "";
				
		$("#idSegmento").val("-1");		 		 		 			
		document.forms[0].idModulo.value = "-1";
		document.forms[0].idCredito.value = "-1";
		document.forms[0].idDebito.value = "-1";
		document.forms[0].idTributo.value = "-1";
		document.forms[0].idItemContabil.value = "-1";
		
		document.forms[0].idModulo.focus();
	}

</script>	


<form:form method="post" action="pesquisarEventoComercial" id="contabilForm" name="contabilForm">

	<input type="hidden" name="acao" id="acao" value="false"/>
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${contabilVO.chavePrimaria}"/>
	<input type="hidden" name="chavesPrimarias" id="chavesPrimarias" value="${contabilVO.chavesPrimarias}">
	<input type="hidden" name="descricao" id="descricao" value="${contabilVO.descricao}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="clienteCol1" class="coluna">

			<label class="rotulo" for="modulo" >M�dulo:</label>
			<select name="idModulo" id="idModulo" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaModulo}" var="modulo">
					<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${contabilVO.idModulo == modulo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${modulo.descricao}"/>						
					</option>		
			    </c:forEach>
		    </select><br />
				
		    <label class="rotulo" id="rotuloNome" for="eventoComercialDescricao">Descri��o:</label>
			<input class="campoTexto" type="text" id="eventoComercialDescricao" name="eventoComercialDescricao" value="${contabilVO.eventoComercialDescricao}"  maxlength="30" size="30" onkeyup="letraMaiuscula(this),formatarCampoNome(event);">
			<br />
				
				
			<label class="rotulo" for="segmento" >Segmento:</label>
			<select name="idSegmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${contabilVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />				
				
				
			<label class="rotulo" for="debito" >Conta D�bito:</label>
			<select name="idDebito" id="idDebito" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaContabil}" var="debito">
					<option value="<c:out value="${debito.chavePrimaria}"/>" <c:if test="${contabilVO.idDebito == debito.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${debito.numeroConta} - ${debito.nomeConta}"/>
					</option>		
			    </c:forEach>
		    </select><br />
		
		
		    <label class="rotulo" for="credito" >Conta Cr�dito:</label>
		 	<select name="idCredito" id="idCredito" class="campoSelect">
		     	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaContabil}" var="credito">
					<option value="<c:out value="${credito.chavePrimaria}"/>" <c:if test="${contabilVO.idCredito == credito.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${credito.numeroConta} - ${credito.nomeConta}"/>
					</option>		
			    </c:forEach>
		      </select><br />
		    
		    <label class="rotulo" for="tributo" >Tributo:</label>
			<select name="idTributo" id="idTributo" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaTributo}" var="tributo">
					<option value="<c:out value="${tributo.chavePrimaria}"/>" <c:if test="${contabilVO.idTributo == tributo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tributo.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />
		
			<label class="rotulo" for="idItemContabil" >Item Cont�bil:</label>
			<select name="idItemContabil" id="idItemContabil" class="campoSelect" >
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaItemContabil}" var="itemContabil">
					<option value="<c:out value="${itemContabil.chavePrimaria}"/>" <c:if test="${contabilVO.idItemContabil == itemContabil.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${itemContabil.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />
		</fieldset>    
		 		    
		    <fieldset id="clienteCol2" class="colunaFinal">
		    
		     <label class="rotulo descricaoRotulo1" for="descricaoHistorico" >Hist�rico:</label>
			  <input class="campoTexto descricaoAlinhamento1" type="text" id="descricaoHistorico" name="descricaoHistorico" maxlength="30" size="30" value="${contabilVO.descricaoHistorico}">
             <br />
		    
		     <label class="rotulo descricaoRotulo2" for="descricaoContaAuxiliar">Conta Auxiliar D�bito:</label>
		      <input class="campoTexto descricaoAlinhamento2" type="text" id="descricaoContaAuxiliarDebito" name="descricaoContaAuxiliarDebito" maxlength="30" size="30" value="${contabilVO.descricaoContaAuxiliarDebito}">
             <br />
             
		     <label class="rotulo descricaoRotulo3" for="descricaoContaAuxiliar">Conta Auxiliar Cr�dito:</label>
		      <input class="campoTexto descricaoAlinhamento3" type="text" id="descricaoContaAuxiliarCredito" name="descricaoContaAuxiliarCredito" maxlength="30" size="30" value="${contabilVO.descricaoContaAuxiliarCredito}">
		    
		    </fieldset>
		
		<br/><br/><br/><br/><br/><br/><br/><br/><br/>
		
		<fieldset class="conteinerBotoesPesquisarDir">
            <vacess:vacess param="pesquisarEventoComercial"> 
	    			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
			</vacess:vacess>				
			<input name="Button" class="bottonRightCol bottonRightColUltimo" type="button" value="Limpar"  onclick="limparFormulario();">
		</fieldset>
		
		<fieldset class="conteinerBotoes">
			<vacess:vacess param="pesquisarEventoComercial">		
				<input name="Button" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Configurar" id="botaoConfigurar" type="button" onclick="configurar();">
			</vacess:vacess>
		</fieldset>
	
	
	<c:if test="${listaEventoComercial ne null}">
			<hr class="linhaSeparadoraPesquisa" />
			<fieldset class="conteinerBloco">
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaEventoComercial"  sort="list" id="eventoComercial"   pagesize="15"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarEventoComercial">
			        
			    <display:column style="width: 25px"> 
		    		<input type="radio" name="idEventoComercial" id="checkEventoComercial" value="<c:out value="${eventoComercial.chavePrimaria}"/>"  onclick="javascript:setarStatus('<c:out value="${eventoComercial.chavePrimaria}"/>','<c:out value="${eventoComercial.descricao}"/>')"/>
		    	</display:column>
			        
			        <display:column sortable="true" title="Descri��o" headerClass="tituloTabelaEsq" sortProperty="descricao" property="descricao"  style="text-align: left; padding-left: 10px" />

			        <display:column sortable="true" title="M�dulo" headerClass="tituloTabelaEsq" sortProperty="modulo.descricao" property="modulo.descricao" style="text-align: left; padding-left: 10px" />
			       	
			       	<display:column sortable="true" title="Complemento" headerClass="tituloTabelaEsq" sortProperty="indicadorComplemento" property="indicadorComplemento" style="text-align: left; padding-left: 10px" />
			       	
			       	<display:column sortable="true" title="Unidade" headerClass="tituloTabelaEsq" sortProperty="descricaoValor" property="descricaoValor" style="text-align: left; padding-left: 10px" />
			
			    </display:table>
			</fieldset>
		</c:if>
	

</form:form> 