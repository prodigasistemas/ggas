<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Configurar Evento Comercial<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Preencha os campos abaixo para realizar a altera��o.</p>

<script type="text/javascript">
	
$(document).ready(function(){
	
	// Dialog			
	$("#historicoPopup").dialog({
		autoOpen: false,
		draggable: false,
		width: 580,
		modal: true,
		minHeight: 110,
		position: "center",
		resizable: false
	});

	$("#contaAuxiliarPopupDebito").dialog({
		autoOpen: false,
		draggable: false,
		width: 580,
		modal: true,
		minHeight: 110,
		position: "center",
		resizable: false
	});	
	
	$("#contaAuxiliarPopupCredito").dialog({
		autoOpen: false,
		draggable: false,
		width: 580,
		modal: true,
		minHeight: 110,
		position: "center",
		resizable: false
	});	
	
	//limparCombos();	
	if(document.forms[0].idDebito.value == "-1" || document.forms[0].idDebito.value == ""){
		document.getElementById('botaoAlterar').disabled=true;
	}else{		
		document.getElementById('botaoAlterar').disabled=false;
		document.getElementById('botaoAdicionar').disabled=true;
	}
	
});

function exibirPopup(popup,id,elemento) {
	exibirJDialog("#"+popup);
	document.getElementById(elemento).value=document.getElementById(id).value;
}
	
function closePopup(popup){
	$("#"+popup).dialog('close');
}
	
function okPopup(id,elemento,popup){
	document.getElementById(id).value=document.getElementById(elemento).value;
	$("#"+popup).dialog('close');
}	

function alterar(){
	
	submeter("contabilForm","adicionarLancamentoComercial");
}

function cancelar(){
	
	limparCombos();
	
	$("#acao").val(true);
	
	submeter("contabilForm","pesquisarEventoComercial");
}

function adicionar(){	
	document.forms[0].indexLista.value = -1;
	
	var descricaoHistorico = $("#idHistorico").val();
	var descricaoContaAuxiliarDebito = $("#idContaAuxiliarDebito").val();
	var descricaoContaAuxiliarCredito = $("#idContaAuxiliarCredito").val();
	
	$("#descricaoHistorico").val(descricaoHistorico);
	$("#descricaoContaAuxiliarDebito").val(descricaoContaAuxiliarDebito);
	$("#descricaoContaAuxiliarCredito").val(descricaoContaAuxiliarCredito);
	
	submeter("contabilForm","adicionarLancamentoComercial");
	document.getElementById('botaoSalvar').disabled=false;
}


function exibirAlteracao(indice) {
	
	var descricaoHistorico = $("#idHistorico").val();
	var descricaoContaAuxiliarDebito = $("#idContaAuxiliarDebito").val();
	var descricaoContaAuxiliarCredito = $("#idContaAuxiliarCredito").val();
	
	$("#descricaoHistorico").val(descricaoHistorico);
	$("#descricaoContaAuxiliarDebito").val(descricaoContaAuxiliarDebito);
	$("#descricaoContaAuxiliarCredito").val(descricaoContaAuxiliarCredito);
	
	if (indice != "") {									
		document.forms[0].indexLista.value=indice;
		
		submeter("contabilForm","exibirLancamentoComercial");		
	}
	document.getElementById('botaoAlterar').disabled=false;
	document.getElementById('botaoAdicionar').disabled=true;
}

function carregarColunas(elem,coluna) {	
	var codTabela = elem.value;
	carregarColunasTabela(codTabela,coluna);
}


function carregarColunasTabela(codTabela,coluna){
  	var selectFuncao = document.getElementById(coluna);

  	selectFuncao.length=0;
  	var novaOpcao = new Option();
  	// selectFuncao.options[selectFuncao.length-1] = novaOpcao;
   	AjaxService.listarColunasTabela(codTabela,
   			function(funcoes) {            		      		         		
           	for (key in funcoes){
                var novaOpcao = new Option(funcoes[key], key);
                if (key == coluna){
                	novaOpcao.selected = true;
                }
                selectFuncao.options[selectFuncao.length] = novaOpcao;		            			            	
        	}
        	ordernarSelect(selectFuncao)
    	}
    );	  	
}
	
	function limparCombos(){
		document.forms[0].idDebito.value = "-1";
		document.forms[0].idCredito.value = "-1";
		document.forms[0].idSegmento.value = "-1";		 		 		 			
		document.forms[0].idItemContabil.value = "-1";
		document.forms[0].idTributo.value = "-1";
		
		document.forms[0].idHistorico.value = "";
		document.forms[0].idContaAuxiliarDebito.value = "";
		document.forms[0].idContaAuxiliarCredito.value = "";
		
		document.forms[0].idDebito.focus();
	}
	
	
	function limparCancelar(){
		limparCombos();
		document.getElementById('botaoAlterar').disabled=true;
		document.getElementById('botaoAdicionar').disabled=false;
	}


function remover(chave,indice){
	document.forms[0].chavePrimaria.value=chave;
	document.forms[0].indexLista.value = indice;
	submeter('contabilForm','removerEventoComercialLancamento');
}
	
	
function salvar(){
	
	$("#acao").val(true);
	
	submeter("contabilForm","salvarAlteracoesConfiguracao");
}
	
	
function moverColuna(elemento,colunaFuncao) {
	coluna=document.getElementById(colunaFuncao);
	var texto;
	var simbolo="#";
		
	for(var i=0; i<coluna.options.length; i++) {
		if (coluna.options[i].selected && coluna.options[i].value != "-1") {
			var no = new Option();
			no.text = coluna.options[i].text;
					
			texto=document.getElementById(elemento).value;
			document.getElementById(elemento).value=texto+' '+simbolo+no.text+simbolo;
				
			   }
			}
		}

</script>	

<form:form method="post" action="/configurarEventoComercial" id="contabilForm" name="contabilForm">

	<input type="hidden" name="acao" id="acao" value="false"/>
	<input type="hidden" name="idEventoComercial" id="idEventoComercial" value="${contabilVO.idEventoComercial}"/>
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${contabilVO.chavePrimaria}"/>
	<input type="hidden" name="chavesPrimarias" id="chavesPrimarias" value="${contabilVO.chavesPrimarias}">
	
	<input type="hidden" id="descricaoHistorico" name="descricaoHistorico" value="" >
	<input type="hidden" id="descricaoContaAuxiliarDebito" name="descricaoContaAuxiliarDebito" value="" >
	<input type="hidden" id="descricaoContaAuxiliarCredito" name="descricaoContaAuxiliarCredito" value="" >
	
	<input type="hidden" name="indexLista" id="indexLista" value="${contabilVO.indexLista}">
	
	<div id="historicoPopup" title="Hist�rico">
			<select name="idTabela" id="idTabela" class="campoSelect" onchange="carregarColunas(this,'idColunaHistorico')">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaTabela}" var="tabela">
					<option value="<c:out value="${tabela.chavePrimaria}"/>" <c:if test="${contabilVO.tabela == tabela.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tabela.nome}"/>
					</option>		
			    </c:forEach>
		    </select><br /><br />
		   
			<select id="idColunaHistorico" class="campoList listaCurta2a campoVertical" style="width: 170px" name="idColunaHistorico" multiple="multiple" 
				onDblClick="moverColuna('textoHistorico','idColunaHistorico');">
				<c:forEach items="${listaColuna}" var="coluna">
					<option value="<c:out value="${coluna.idColunaHistorico}"/>" title="<c:out value="${coluna.nome}"/>"></option>
				</c:forEach>
			</select>
		 
		<textarea id="textoHistorico" name="textoHistorico" class="campoTexto" rows="8" cols="50"></textarea>
		    <br/>

		 <br/>
		<hr class="linhaSeparadoraPopup" />
		<br/>
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Ok" type="button" onclick="okPopup('idHistorico','textoHistorico','historicoPopup');">
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Cancelar" type="button" onclick="closePopup('historicoPopup')">
	</div>
	
	
	<div id="contaAuxiliarPopupDebito" title="Conta Auxiliar D�bito">
			<select name="idTabelaD" id="idTabelaD" class="campoSelect" onchange="carregarColunas(this,'idColunaContaAuxiliarDebito')">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaTabela}" var="tabela">
					<option value="<c:out value="${tabela.chavePrimaria}"/>" <c:if test="${contabilVO.tabela == tabela.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tabela.nome}"/>
					</option>		
			    </c:forEach>
		    </select><br /><br />

			<select id="idColunaContaAuxiliarDebito" class="campoList listaCurta2a campoVertical" style="width: 170px" name="idColunaContaAuxiliarDebito" multiple="multiple" 
				onDblClick="moverColuna('textoContaAuxiliarDebito','idColunaContaAuxiliarDebito');">
				<c:forEach items="${listaColuna}" var="coluna">
					<option value="<c:out value="${coluna.idColuna}"/>" title="<c:out value="${coluna.nome}"/>"></option>
				</c:forEach>
			</select>
		 
		<textarea id="textoContaAuxiliarDebito" name="textoContaAuxiliarDebito" class="campoTexto" rows="8" cols="50"></textarea>
		    <br/>
		 <br/>
		<hr class="linhaSeparadoraPopup" />
		<br/>
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Ok" type="button" onclick="okPopup('idContaAuxiliarDebito','textoContaAuxiliarDebito','contaAuxiliarPopupDebito');">
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Cancelar" type="button" onclick="closePopup('contaAuxiliarPopupDebito');">
	</div>
	
	
	<div id="contaAuxiliarPopupCredito" title="Conta Auxiliar Cr�dito">
			<select name="idTabelaC" id="idTabelaC" class="campoSelect" onchange="carregarColunas(this,'idColunaContaAuxiliarCredito')">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaTabela}" var="tabela">
					<option value="<c:out value="${tabela.chavePrimaria}"/>" <c:if test="${contabilVO.tabela == tabela.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tabela.nome}"/>
					</option>		
			    </c:forEach>
		    </select><br /><br />
		   
			<select id="idColunaContaAuxiliarCredito" class="campoList listaCurta2a campoVertical" style="width: 170px" name="idColunaContaAuxiliarCredito" multiple="multiple" 
				onDblClick="moverColuna('textoContaAuxiliarCredito','idColunaContaAuxiliarCredito');">
				<c:forEach items="${listaColuna}" var="coluna">
					<option value="<c:out value="${coluna.idColuna}"/>" title="<c:out value="${coluna.nome}"/>"></option>
				</c:forEach>
			</select>
		 
		<textarea id="textoContaAuxiliarCredito" name="textoContaAuxiliarCredito" class="campoTexto" rows="8" cols="50"></textarea>
		    <br/>
		 <br/>
		<hr class="linhaSeparadoraPopup" />
		<br/>
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Ok" type="button" onclick="okPopup('idContaAuxiliarCredito','textoContaAuxiliarCredito','contaAuxiliarPopupCredito');">
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Cancelar" type="button" onclick="closePopup('contaAuxiliarPopupCredito');">
	</div>
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="clienteCol1" class="coluna">

			<label class="rotulo" for="descricao" >Evento Comercial:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${contabilVO.descricao}"/></span><br /><br /> <br />

							
			<label class="rotulo" for="debito" ><span class="campoObrigatorioSimbolo">*</span>Conta D�bito:</label>
			<select name="idDebito" id="idDebito" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaContabil}" var="debito">
					<option value="<c:out value="${debito.chavePrimaria}"/>" <c:if test="${contabilVO.idDebito == debito.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${debito.numeroConta} - ${debito.nomeConta}"/>
					</option>		
			    </c:forEach>
		    </select><br />
			
		    <label class="rotulo rotulo2Linhas" for="credito" ><span class="campoObrigatorioSimbolo">*</span>Conta Cr�dito:</label>
		 	<select name="idCredito" id="idCredito" class="campoSelect">
		     	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaContabil}" var="credito">
					<option value="<c:out value="${credito.chavePrimaria}"/>" <c:if test="${contabilVO.idCredito == credito.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${credito.numeroConta} - ${credito.nomeConta}"/>
					</option>		
			    </c:forEach>
		      </select><br />
			
			<label class="rotulo" for="segmento" >Segmento: </label>
			<select name="idSegmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaSegmento}" var="segmento">
					<option <c:if test="${contabilVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if> value="<c:out value="${segmento.chavePrimaria}"/>">
						<c:out value="${segmento.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />			
				
		
			<label class="rotulo" for="idItemContabil" >Item Cont�bil:</label>
			<select name="idItemContabil" id="idItemContabil" class="campoSelect" >
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaItemContabil}" var="itemContabil">
					<option value="<c:out value="${itemContabil.chavePrimaria}"/>" <c:if test="${contabilVO.idItemContabil == itemContabil.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${itemContabil.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />
		    
		    <label class="rotulo" for="idTributo" >Tributo:</label>
			<select name="idTributo" id="idTributo" class="campoSelect" >
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaTributo}" var="tributo">
					<option value="<c:out value="${tributo.chavePrimaria}"/>" <c:if test="${contabilVO.idTributo == tributo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tributo.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />
		
		</fieldset>    
		 		    
		    <fieldset id="clienteCol2" class="colunaFinal">
		    <br /><br /><br />
             <label class="rotulo" for="lancamentoItemContabil" >Hist�rico: </label>
			  <input class="campoTexto" type="text" id="idHistorico" name="idHistorico" readonly="readonly" value="${contabilVO.idHistorico}" onclick="exibirPopup('historicoPopup','idHistorico','textoHistorico');" size="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
             <br />
             
		     <label class="rotulo rotulo2Linhas" for="lancamentoItemContabil">Conta Auxiliar D�bito:</label>
		       <input class="campoTexto" type="text" id="idContaAuxiliarDebito" name="idContaAuxiliarDebito" readonly="readonly" value="${contabilVO.idContaAuxiliarDebito}" onclick="exibirPopup('contaAuxiliarPopupDebito','idContaAuxiliarDebito','textoContaAuxiliarDebito');" size="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
		     <br />
		     
		     <label class="rotulo rotulo2Linhas" for="lancamentoItemContabil">Conta Auxiliar Cr�dito:</label>
		       <input class="campoTexto" type="text" id="idContaAuxiliarCredito" name="idContaAuxiliarCredito" readonly="readonly" value="${contabilVO.idContaAuxiliarCredito}" onclick="exibirPopup('contaAuxiliarPopupCredito','idContaAuxiliarCredito','textoContaAuxiliarCredito');" size="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
		     <br />
		     
		     <label class="rotulo" for="habilitado">Regime Contabil:</label>
			 <input class="campoRadio" type="radio" name="indicadorRegimeContabilCompetencia" id="indicadorRegimeContabilCompetencia" value="true" <c:if test="${contabilVO.indicadorRegimeContabilCompetencia eq 'true'}">checked</c:if>>
			 <label class="rotuloRadio" >Competencia</label>
			 <input class="campoRadio" type="radio" name="indicadorRegimeContabilCompetencia" id="indicadorRegimeContabilCompetencia" value="false" <c:if test="${contabilVO.indicadorRegimeContabilCompetencia eq 'false'}">checked</c:if>>
			 <label class="rotuloRadio">Caixa</label>
		    
		    </fieldset>
		
		<fieldset class="conteinerBotoes">
			<vacess:vacess param="pesquisarEventoComercial"> 
	    			<input name="Button" class="bottonRightCol bottonLeftColUltimo" id="botaoAdicionar" value="Adicionar" type="button" onclick="adicionar();">
	    			<input name="Button" class="bottonRightCol bottonLeftColUltimo" id="botaoAlterar" value="Alterar" type="button" onclick="alterar('<c:out value="${i}"/>');">
			</vacess:vacess>		
			<input class="bottonRightCol bottonLeftColUltimo" type="button" value="Limpar" onclick="limparCombos();" >
		</fieldset>
	
		
	</fieldset>
	<c:if test="${listaLancamentoEventoComercial ne null}">
	<c:set var="i" value="0"/>
			<fieldset class="conteinerBloco">
			<hr class="linhaSeparadora2" />
								
				<display:table class="dataTableGGAS" name="listaLancamentoEventoComercial" sort="list" id="lancamentoComercial" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="configurarEventoComercial">
			        
			         <display:column media="html" title="Conta D�bito">
			            	<c:out value="${lancamentoComercial.contaContabilDebito.numeroConta}"/>
			        </display:column>

			         <display:column title="Conta Cr�dito" media="html">
			            	<c:out value="${lancamentoComercial.contaContabilCredito.numeroConta }"/>
			        </display:column>
			       	
			       	 <display:column title="Conta Auxiliar D�bito" media="html">
			            	<c:out value="${lancamentoComercial.descricaoContaAuxiliarDebito}"/>
			        </display:column>
			        
			        <display:column title="Conta Auxiliar Cr�dito" media="html">
			            	<c:out value="${lancamentoComercial.descricaoContaAuxiliarCredito}"/>
			        </display:column>
			       	
			       	 <display:column title="Hist�rico" media="html">
			            	<c:out value="${lancamentoComercial.descricaoHistorico}"/>
			        </display:column>
			      
			       	 <display:column title="Segmento" media="html">
			            	<c:out value="${lancamentoComercial.segmento.descricao}"/>
			        </display:column>
			      
			       	 <display:column title="Item Cont�bil" media="html">
			            	<c:out value="${lancamentoComercial.lancamentoItemContabil.descricao}"/>
			        </display:column>			      

			       	 <display:column title="Tributo"  media="html">
			            	<c:out value="${lancamentoComercial.tributo.descricao}"/>
			        </display:column>
			        
			        <display:column title="Regime" media="html">	     	
				     	<c:choose>
							<c:when test="${lancamentoComercial.indicadorRegimeContabilCompetencia == true}">
								<c:out value="Competencia"/>
							</c:when>
							<c:otherwise>
								<c:out value="Caixa"/>
							</c:otherwise>				
						</c:choose>
					</display:column>
		      
		      		<display:column title=""  media="html">
                        <a href="javascript:exibirAlteracao('${i}');"><img alt="Alterar Lan�amento" title="Alterar Lan�amento" src="<c:url value="/imagens/16x_editar.gif"/>" border="0"/></a>
						<span class="linkInvisivel"></span>
			        </display:column>
			        
			        <display:column title=""  media="html">
                        <a onclick="return confirm('Deseja excluir o registro?');" href="javascript:remover('<c:out value="${lancamentoComercial.chavePrimaria}"/>','<c:out value="${i}"/>');"><img title="Excluir Lan�amento" alt="Excluir Lan�amento"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
			        </display:column>
		      	<c:set var="i" value="${i+1}" />
			    </display:table>
			</fieldset>
		</c:if>
	
	
		<fieldset class="conteinerBotoes">
			<vacess:vacess param="pesquisarEventoComercial"> 
	    			<input name="Button" class="bottonRightCol2 botaoGrande1 botaoIncluir" id="botaoSalvar" value="Salvar" type="button" onclick="salvar();">
			</vacess:vacess>				
					<input name="buttonRemover" value="Cancelar" class="bottonRightCol2 botaoAlterar" onclick="cancelar();" type="button">					
					<input value="Limpar" class="bottonRightCol bottonRightColUltimo" onclick="limparCancelar();" type="button"/>					
		</fieldset>
</form:form>