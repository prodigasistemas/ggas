<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<h1 class="tituloInterno">Detalhar Lan�amento Cont�bil<a class="linkHelp" href="<help:help>/detalhamentodoslancamentoscontabeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	
	<script>	
		
		function voltar(){	
			submeter("contabilForm",  "pesquisarLancamentoContabil");
		}
	
	</script>
	
<form:form method="post" action="exibirDetalhamentoLancamentoContabil" enctype="multipart/form-data"  id="contabilForm" name="contabilForm">

	<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoLancamentoContabil">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contabilVO.chavePrimaria}">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${contabilVO.chavesPrimarias}">
	
	<input name="dataIntervaloContabilInicial" type="hidden" value="${contabilVO.dataIntervaloContabilInicial}">
	<input name="dataIntervaloContabilFinal" type="hidden" value="${contabilVO.dataIntervaloContabilFinal}">
	<input name="idSegmento" type="hidden" value="${contabilVO.idSegmento}">
	<input name="numeroNotaFiscalInicial" type="hidden" value="${contabilVO.numeroNotaFiscalInicial}">
	<input name="numeroNotaFiscalFinal" type="hidden" value="${contabilVO.numeroNotaFiscalFinal}">
	<input name="idModulo" type="hidden" value="${contabilVO.idModulo}">
	<input name="idEventoComercial" type="hidden" value="${contabilVO.idEventoComercial}">
	<input name="idLancamentoItemContabil" type="hidden" value="${contabilVO.idLancamentoItemContabil}">
	
	<fieldset class="detalhamento">
		<fieldset class="conteinerBloco">		
			<fieldset class="coluna">
				<label class="rotulo" id="rotuloNome">Data Cont�bil:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${contabilVO.dataContabil}"/></span><br />
				<label class="rotulo" id="rotuloNome">EventoComercial:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${contabilVO.eventoComercialDescricao}"/></span><br />
				<label class="rotulo" id="rotuloNomeAbreviado">Segmento:</label>
				<span class="itemDetalhamento"><c:out value="${contabilVO.segmentoDescricao}"/></span><br />
			</fieldset>
			
			<fieldset class="colunaFinal">				
				<label class="rotulo" id="rotuloTipo">Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${contabilVO.itemContabilDescricao}"/></span><br />
				<label class="rotulo" id="rotuloTipo">Conta D�bito:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${contabilVO.contaDebitoDetalhamento}"/></span><br />
				<label class="rotulo" id="rotuloTipo">Conta Cr�dito:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${contabilVO.contaCreditoDetalhamento}"/></span><br />
			</fieldset>		
		</fieldset>
	</fieldset>
					
	<display:table class="dataTableGGAS" style="margin-top: 20px" name="listaLancamentoContabilDetalhamentoVO" sort="list" id="lancamentoContabilAnalitico" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoLancamentoContabil" >
	    <display:column sortable="false" title="Cliente" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
	    	<c:out value='${lancamentoContabilAnalitico.cliente}'/>
	    </display:column> 
	    <display:column sortable="false" title="Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left; width: 210px; padding-left: 10px">
	    	<c:out value='${lancamentoContabilAnalitico.pontoConsumo}'/>
	    </display:column>
		<display:column sortable="false" title="Objeto" style="width: 180px">
			<c:out value='${lancamentoContabilAnalitico.objeto}'/>
		</display:column>
		<display:column sortable="false" title="Complemento" style="width: 100px">
			<c:out value='${lancamentoContabilAnalitico.complemento}'/>
		</display:column>
		<display:column sortable="false" title="Valor" style="width: 130px; text-align: right; padding-right: 5px">
			<c:if test="${lancamentoContabilAnalitico.descricaoValor eq 'R$'}">
						<c:out value='${lancamentoContabilAnalitico.descricaoValor}'/>
			</c:if>	
			<c:out value='${lancamentoContabilAnalitico.valor}'/>
			<c:if test="${lancamentoContabilAnalitico.descricaoValor eq 'm3'}">
						<c:out value='${lancamentoContabilAnalitico.descricaoValor}'/>
			</c:if>
		</display:column>				
	</display:table>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">        
	</fieldset>

</form:form>
