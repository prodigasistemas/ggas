<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Lan�amentos Cont�beis<a class="linkHelp" href="<help:help>/consultadoslancamentoscontabeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<script type="text/javascript">
	
	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#cnpj").inputmask("99.999.999/9999-99",{placeholder:"_"});
	    $("input#numeroNotaFiscalInicial").keyup(apenasNumeros).blur(apenasNumeros);
	    $("input#numeroNotaFiscalFinal").keyup(apenasNumeros).blur(apenasNumeros);
		
	});				

	function pesquisar() {
		
		submeter('contabilForm', 'pesquisarLancamentoContabil');
		
	}

	function limparFormulario() {
		
		document.contabilForm.numeroNotaFiscalInicial.value = "";
		document.contabilForm.numeroNotaFiscalFinal.value = "";

		document.getElementById('idEventoComercial').disabled = true;
		document.getElementById('idLancamentoItemContabil').disabled = true;
		
		document.forms[0].idModulo.value = "-1";
		document.forms[0].idEventoComercial.value = "-1";
		document.forms[0].idLancamentoItemContabil.value = "-1";
		document.forms[0].idSegmento.value = "-1";		 		 		 			
  		
		document.getElementById('dataIntervaloContabilInicial').value = "";
		document.getElementById('dataIntervaloContabilFinal').value = "";
		document.getElementById('cnpj').value = "";
		
	}

	function exibirDetalhamentoContabil(chave) {
		
		document.forms[0].chavePrimaria.value = chave;
		submeter("contabilForm", 'exibirDetalhamentoLancamentoContabil');
	}	

	function carregarEventoComercial() {
		idModulo = document.getElementById("idModulo").value;
		submeter("contabilForm", "carregarEventoComercial");
	}

	function carregarComplemento() {
		idEventoComercial = document.getElementById("idEventoComercial").value;
		submeter("contabilForm", "carregarComplemento");
	}	

	function habilitarDesabilitarModuloEventoComercial(){

		idModulo = document.getElementById("idModulo").value;

		if(idModulo != -1){
			document.getElementById("idEventoComercial").disabled = false;
		}else{
			document.getElementById("idEventoComercial").disabled = true;
		}

		idEventoComercial = document.getElementById("idEventoComercial").value;
		if(idEventoComercial != '-1'){
			document.getElementById("idLancamentoItemContabil").disabled = false;
		}else{
			document.getElementById("idLancamentoItemContabil").disabled = true;
		}
	}

	function init() {
		
		habilitarDesabilitarModuloEventoComercial();
	}
	
	addLoadEvent(init);

</script>	

<form:form method="post" action="PesquisarLancamentoContabil" id="contabilForm" name="contabilForm">
	<input type="hidden" name="acao" id="acao" value="pesquisarLancamentoContabil"/>
	<input type="hidden" name="fluxoPesquisa" id="fluxoPesquisa" value="true">	
	<input type="hidden" name="chavePrimaria" id="chavePrimaria">
	<input type="hidden" name="chavePrimarias" id="chavePrimarias">
	
	<fieldset id="conteinerCliente" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="clienteCol1" class="coluna">
	<label class="rotulo" for="dataIntervaloContabilInicial">Data Cont�bil:</label>
			<input class="campoData campoHorizontal" type="text" id="dataIntervaloContabilInicial" name="dataIntervaloContabilInicial" maxlength="10" value="${contabilVO.dataIntervaloContabilInicial}">
			<label class="rotuloEntreCampos" for="dataIntervaloContabilFinal">a</label>
			<input class="campoData campoHorizontal" type="text" id="dataIntervaloContabilFinal" name="dataIntervaloContabilFinal" maxlength="10" value="${contabilVO.dataIntervaloContabilFinal}"><br class="quebraLinha2" />
			
			<label class="rotulo" for="segmento" >Segmento:</label>
			<select name="idSegmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${contabilVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />
		    
		    <label class="rotulo" for="codigoIntervaloNotaFiscal">Nota Fiscal:</label>
		    <input class="campoTexto campoHorizontal" type="text" id="numeroNotaFiscalInicial" name="numeroNotaFiscalInicial" maxlength="8" size="6" value="${contabilVO.numeroNotaFiscalInicial}">
		    <label class="rotuloEntreCampos" for="codigoIntervaloNotaFiscal">a</label>
		    <input class="campoTexto campoHorizontal" type="text" id="numeroNotaFiscalFinal" name="numeroNotaFiscalFinal" maxlength="8" size="6" value="${contabilVO.numeroNotaFiscalFinal}">
		    
		</fieldset>
		
		<fieldset id="clienteCol2" class="colunaFinal">
			<label class="rotulo" for="modulo" >Modulo:</label>
			<select name="idModulo" id="idModulo" class="campoSelect" onchange="carregarEventoComercial()">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaModulo}" var="modulo">
					<option value="<c:out value="${modulo.chavePrimaria}"/>" <c:if test="${contabilVO.idModulo == modulo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${modulo.descricao}"/>						
					</option>		
			    </c:forEach>
		    </select><br class="quebraLinha"/>		    
		    
		    <label class="rotulo" for="eventoComercial" >Evento Comercial:</label>
			<select name="idEventoComercial" id="idEventoComercial" class="campoSelect" onchange="carregarComplemento()" >
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaEventoComercial}" var="eventoComercial">
					<option value="<c:out value="${eventoComercial.chavePrimaria}"/>" <c:if test="${contabilVO.idEventoComercial == eventoComercial.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${eventoComercial.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br />
		
			<label class="rotulo" for="lancamentoItemContabil" >Complemento:</label>
			<select name="idLancamentoItemContabil" id="idLancamentoItemContabil" class="campoSelect" >
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaLancamentoItemContabil}" var="lancamentoItemContabil">
					<option value="<c:out value="${lancamentoItemContabil.chavePrimaria}"/>" <c:if test="${contabilVO.idLancamentoItemContabil == lancamentoItemContabil.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${lancamentoItemContabil.descricao}"/>
					</option>		
			    </c:forEach>
		    </select><br /><br />
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarLancamentoContabil">
				<input class="bottonRightCol2" id="botaoPesquisar" type="button" value="Pesquisar" onclick="pesquisar();">
			</vacess:vacess>		
			<input class="bottonRightCol bottonRightColUltimo" type="button" value="Limpar"  onclick="limparFormulario();">
		</fieldset>
	</fieldset>		
	
	<c:if test="${listaLancamentoContabilSintetico ne null && empty listaLancamentoContabilSinteticoVO}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaLancamentoContabilSintetico" sort="list" id="lancamentoContabilSintetico" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarLancamentoContabil">
		    <display:column sortable="true" sortProperty="dataContabil" title="Data Cont�bil" style="width: 70px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>		      		
		      		<fmt:formatDate value="${lancamentoContabilSintetico.dataContabil}" pattern="dd/MM/yyyy"/>
		      	</a>
		    </display:column>
		    <display:column sortable="true" sortProperty="contaContabilDebito" title="Conta D�bito" style="width: 70px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSintetico.contaContabilDebito.nomeConta}'/>		      		
		      	</a>
            </display:column>
            <display:column sortable="true" sortProperty="contaContabilCredito" title="Conta Cr�dito" style="width: 70px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSintetico.contaContabilCredito.nomeConta}'/>
		      	</a>
            </display:column>	
		    <display:column sortable="true" sortProperty="eventoComercial.modulo" title="Modulo" style="width: 85px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSintetico.eventoComercial.modulo.descricao}'/>
		      	</a>
            </display:column>
            <display:column sortable="true" sortProperty="eventoComercial.descricao" title="Evento Comercial" headerClass="tituloTabelaEsq" style="width: 300px; text-align: left" >
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSintetico.eventoComercial.descricao}'/>
				</a>
	    	</display:column>
	    	<display:column sortable="true" sortProperty="segmento" title="Segmento" style="width: 100px; text-align: left" >
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSintetico.segmento.descricao}'/>
				</a>
	    	</display:column>	    	
	    	<display:column title="Complemento" style="width: 135px">
	    		<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>
	    			<c:if test="${lancamentoContabilSintetico.lancamentoItemContabil ne null}">
	    				<c:out value='${lancamentoContabilSintetico.lancamentoItemContabil.descricao}'/>
	    			</c:if>
	    			<c:if test="${lancamentoContabilSintetico.lancamentoItemContabil eq null}">
	    				<c:out value='${lancamentoContabilSintetico.tributo.descricao}'/>
	    			</c:if>
				</a>
	    	</display:column>	    		    			    	    		    		        		      	      		     
	    	<display:column title="Valor" style="width: 90px; text-align: right">
	    		<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSintetico.chavePrimaria}'/>);'>
					<c:if test="${lancamentoContabilSintetico.eventoComercial.descricaoValor eq 'R$'}">
						<c:out value='${lancamentoContabilSintetico.eventoComercial.descricaoValor}'/>
					</c:if>	    			    			
					<fmt:formatNumber value="${lancamentoContabilSintetico.valor}" minFractionDigits="2"/>
					<c:if test="${lancamentoContabilSintetico.eventoComercial.descricaoValor eq 'm3'}">
						<c:out value='${lancamentoContabilSintetico.eventoComercial.descricaoValor}'/>
					</c:if>
				</a>
	    	</display:column>	    	
		</display:table>		
	</c:if>		
	
	
	<c:if test="${ not empty listaLancamentoContabilSinteticoVO}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaLancamentoContabilSinteticoVO" sort="list" id="lancamentoContabilSinteticoVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarLancamentoContabil">
		    <display:column sortable="true" sortProperty="numeroNotaFiscal" title="Nota Fiscal" style="width: 70px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>		      		
		      		<c:out value='${lancamentoContabilSinteticoVO.numeroNotaFiscal}'/>
		      	</a>
		    </display:column>
		    <display:column sortable="true" sortProperty="dataContabil" title="Data Cont�bil" style="width: 70px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>		      		
		      		<fmt:formatDate value="${lancamentoContabilSinteticoVO.dataContabil}" pattern="dd/MM/yyyy"/>
		      	</a>
		    </display:column>
		    <display:column sortable="true" sortProperty="contaContabilDebito" title="Conta D�bito" style="width: 70px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSinteticoVO.contaContabilDebito.nomeConta}'/>		      		
		      	</a>
            </display:column>
            <display:column sortable="true" sortProperty="contaContabilCredito" title="Conta Cr�dito" style="width: 70px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSinteticoVO.contaContabilCredito.nomeConta}'/>
		      	</a>
            </display:column>	
		    <display:column sortable="true" sortProperty="eventoComercial.modulo" title="Modulo" style="width: 85px">
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSinteticoVO.eventoComercial.modulo.descricao}'/>
		      	</a>
            </display:column>
            <display:column sortable="true" sortProperty="eventoComercial.descricao" title="Evento Comercial" headerClass="tituloTabelaEsq" style="width: 300px; text-align: left" >
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSinteticoVO.eventoComercial.descricao}'/>
				</a>
	    	</display:column>
	    	<display:column sortable="true" sortProperty="segmento" title="Segmento" style="width: 100px; text-align: left" >
		      	<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>
		      		<c:out value='${lancamentoContabilSinteticoVO.segmento.descricao}'/>
				</a>
	    	</display:column>	    	
	    	<display:column title="Complemento" style="width: 135px">
	    		<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>
	    			<c:if test="${lancamentoContabilSinteticoVO.lancamentoItemContabil ne null}">
	    				<c:out value='${lancamentoContabilSinteticoVO.lancamentoItemContabil.descricao}'/>
	    			</c:if>
	    			<c:if test="${lancamentoContabilSinteticoVO.lancamentoItemContabil eq null}">
	    				<c:out value='${lancamentoContabilSinteticoVO.tributo.descricao}'/>
	    			</c:if>
				</a>
	    	</display:column>	    		    			    	    		    		        		      	      		     
	    	<display:column title="Valor" style="width: 90px; text-align: right">
	    		<a href='javascript:exibirDetalhamentoContabil(<c:out value='${lancamentoContabilSinteticoVO.chavePrimaria}'/>);'>
					<c:if test="${lancamentoContabilSinteticoVO.eventoComercial.descricaoValor eq 'R$'}">
						<c:out value='${lancamentoContabilSinteticoVO.eventoComercial.descricaoValor}'/>
					</c:if>	    			    			
					<fmt:formatNumber value="${lancamentoContabilSinteticoVO.valor}" minFractionDigits="2"/>
					<c:if test="${lancamentoContabilSinteticoVO.eventoComercial.descricaoValor eq 'm3'}">
						<c:out value='${lancamentoContabilSinteticoVO.eventoComercial.descricaoValor}'/>
					</c:if>
				</a>
	    	</display:column>	    	
		</display:table>		
	</c:if>
	
	
</form:form> 