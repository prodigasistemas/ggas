<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Tipo de Documento<a class="linkHelp" href="<help:help>/consultadasdevoluesfinanceiras.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar um Tipo de Documento, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.

<script type="text/javascript">
	
	function cancelar() {		
		location.href = '<c:url value="exibirPesquisaTipoDocumento"/>';
	}
	
	function limparFormulario() {
		document.getElementById('descricao').value = "";
		document.getElementById('descricaoAbreviada').value = "";
		document.forms[0].indicadorCodigoBarras[0].checked = true;
		document.forms[0].indicadorPagavel[0].checked = true;
		document.forms[0].habilitado[0].checked = true;
	}

</script>	

<form:form method="post" action="alterarTipoDocumento">
	<input type="hidden" name="acao" id="acao" value="alterarTipoDocumento"/>
	<input type="hidden" name="fluxoPesquisa" id="fluxoPesquisa" value="true">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${tipoDocumento.chavePrimaria}">
	
	<fieldset id="conteinerPesquisarTipoDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisaTipoDocumentoCol1" class="coluna">
			<label class="rotulo numeroPosicao" id="rotuloNumero" for="descricao" ><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${tipoDocumento.descricao}" onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)"/>
			<br class="quebraLinha"/>		
			<label class="rotulo descricaoPosicao" id="rotuloDescricao" for="descricaoAbreviada" ><span class="campoObrigatorioSimbolo">* </span>Descri��o Abreviada:</label>
			<input class="campoTexto descricaoAbreviada" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="10" size="10"
					onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" value="${tipoDocumento.descricaoAbreviada}"/>
			<br class="quebraLinha"/>
		</fieldset>
		
		<fieldset id="pesquisaTipoDocumentoCol2" class="colunaFinal">
			<label class="rotulo" for="habilitado">Pag�vel: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorPagavel" id="indicadorPagavel" value="1" <c:if test="${tipoDocumento.indicadorPagavel eq '1'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Sim</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorPagavel" id="indicadorPagavel" value="0" <c:if test="${tipoDocumento.indicadorPagavel eq '0'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">N�o</label>
			<label class="rotulo" for="habilitado">C�digo de Barra: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorCodigoBarras" id="indicadorCodigoBarras" value="1" <c:if test="${tipoDocumento.indicadorCodigoBarras eq '1'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Sim</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorCodigoBarras" id="indicadorCodigoBarras" value="0" <c:if test="${tipoDocumento.indicadorCodigoBarras eq '0'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">N�o</label>
			<label class="rotulo" for="habilitado">Indicador de Uso: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${tipoDocumento.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Ativo</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${tipoDocumento.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Inativo</label>			
		</fieldset>
		
	</fieldset>
	
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios.</p>
	
	<c:if test="${listaDocumentoModelo ne null}">
	
		<hr class="linhaSeparadoraPesquisa" />
		
		<display:table class="dataTableGGAS" name="listaDocumentoModelo" sort="list" id="documentoModelo" pagesize="15"  requestURI="exibirAlterarTipoDocumento">
		
			<display:column media="html" sortable="false" class="selectedRowColumn" title="">
      			<input type="radio" name="modelo" id="modelo" value="${documentoModelo.chavePrimaria}" <c:if test="${tipoDocumento.chavePrimaria eq documentoModelo.chavePrimaria}">checked</c:if>>
			</display:column>
			
			<display:column style="text-align: center;" title="Modelo do Layout">
	        	<a href="exibirModeloTipoDocumento/${documentoModelo.chavePrimaria}" target="_blank">
	            	<c:out value="${documentoModelo.descricao}"/>
	            </a>
 			</display:column>
 			
		</display:table>
		
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonRemover" value="Cancelar" class="bottonRightCol2" id="botaoAlterar" onclick="cancelar()" type="button">
		<input name="buttonLimpar" value="Limpar" class="bottonRightCol2 bottonLeftColUltimo" onclick="limparFormulario()" type="button">
		 <input name="buttonSalvar" value="Salvar" class="bottonRightCol2 botaoGrande1" type="submit" />
	</fieldset>
	
</form:form> 