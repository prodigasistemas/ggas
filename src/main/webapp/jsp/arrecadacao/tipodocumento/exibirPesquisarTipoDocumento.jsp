<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Tipo de Documento<a class="linkHelp" href="<help:help>/consultadasdevoluesfinanceiras.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span> ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos os registros. Para incluir um novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span>.

<script type="text/javascript">
	
	$(document).ready(function() {
		//alert( JSON.stringify($('#indicadorHabilitado').serialize()));
		if ($("#indicadorHabilitado").val() == "") {
			document.forms[0].habilitado[0].checked = true;
		}
		if ($('#indPagavel').val() == "") {
			document.forms[0].indicadorPagavel[0].checked = true;
		}
		if ($('#indCodigoBarras').val() == "") {
			document.forms[0].indicadorCodigoBarras[0].checked = true;
		}
	});

	function pesquisar() {
		submeter('tipoDocumentoForm', 'pesquisarTipoDocumento');
	}

	function detalhar(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter('tipoDocumentoForm', 'exibirDetalharTipoDocumento');
	}

	function alterar(chave) {
		var selecao = verificarSelecaoApenasUmSemMensagem();
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('tipoDocumentoForm', 'exibirAlterarTipoDocumento');
		} else {
			alert("Selecione apenas um registro para realizar a opera��o!");
		}
	}

	function incluir() {
		location.href = '<c:url value="/exibirIncluirTipoDocumento"/>';
	}

	function remover() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				submeter('tipoDocumentoForm', 'removerTipoDocumento');
			}
		}
	}

	function limparFormulario() {
		document.getElementById('descricao').value = "";
		document.getElementById('descricaoAbreviada').value = "";
		document.forms[0].habilitado[0].checked = true;
		document.forms[0].indicadorCodigoBarras[0].checked = true;
		document.forms[0].indicadorPagavel[0].checked = true;
	}
</script>	

<form:form method="post" action="exibirPesquisaTipoDocumento" id="tipoDocumentoForm" name="tipoDocumentoForm">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria">
	<input type="hidden" name="chavePrimarias" id="chavePrimarias">
	<input type="hidden" id="indicadorHabilitado" value="${habilitado}" />
	<input type="hidden" id="indPagavel" value="${tipoDocumento.indicadorPagavel}" />
	<input type="hidden" id="indCodigoBarras" value="${tipoDocumento.indicadorCodigoBarras}" />
	
	<fieldset id="conteinerPesquisarTipoDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisaTipoDocumentoCol1" class="coluna">
			<label class="rotulo numeroPosicao" id="rotuloNumero" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${tipoDocumento.descricao}" onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)"/>
			<br class="quebraLinha"/>		
			<label class="rotulo descricaoPosicao" id="rotuloDescricao" for="descricaoAbreviada" >Descri��o Abreviada:</label>
			<input class="campoTexto descricaoAbreviada" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="10" size="10" value="${tipoDocumento.descricaoAbreviada}" onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)"/>
			<br class="quebraLinha"/>
		</fieldset>
		
		<fieldset id="pesquisaTipoDocumentoCol2" class="colunaFinal">
			<label class="rotulo" for="habilitado">Pag�vel: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorPagavel" id="indicadorPagavel" value="1" <c:if test="${tipoDocumento.indicadorPagavel eq '1'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Sim</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorPagavel" id="indicadorPagavel" value="0" <c:if test="${tipoDocumento.indicadorPagavel eq '0'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">N�o</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorPagavel" id="indicadorPagavel" value="null" <c:if test="${tipoDocumento.indicadorPagavel eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Todos</label>
			<label class="rotulo" for="habilitado">C�digo de Barra: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorCodigoBarras" id="indicadorCodigoBarras" value="1" <c:if test="${tipoDocumento.indicadorCodigoBarras eq '1'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Sim</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorCodigoBarras" id="indicadorCodigoBarras" value="0" <c:if test="${tipoDocumento.indicadorCodigoBarras eq '0'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">N�o</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorCodigoBarras" id="indicadorCodigoBarras" value="null" <c:if test="${tipoDocumento.indicadorCodigoBarras eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Todos</label>
			<label class="rotulo" for="habilitado">Indicador de uso: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label><br/>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2" id="botaoPesquisar" type="button" value="Pesquisar" onclick="pesquisar();">
			<input class="bottonRightCol bottonRightColUltimo" type="button" value="Limpar"  onclick="limparFormulario();">
		</fieldset>
		
	</fieldset>
	
	<c:if test="${listaTipoDocumento ne null}">
	
		<hr class="linhaSeparadoraPesquisa" />
		
		<display:table class="dataTableGGAS" name="listaTipoDocumento" sort="list" id="tipoDocumento" pagesize="15" requestURI="pesquisarTipoDocumento">
		
			<display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
      			<input type="checkbox" name="chavesPrimarias" id="chavesPrimarias" value="${tipoDocumento.chavePrimaria}">
			</display:column>
			
			<display:column title="Ativo" style="width: 30px">
				<c:choose>
					<c:when test="${tipoDocumento.habilitado == true}">
						<img alt="Ativo" title="Ativo"
							src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo"
							src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column style="text-align: center;" title="Descri��o" sortable="true" sortProperty="descricao" >
	        	<a href="javascript:detalhar(<c:out value='${tipoDocumento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${tipoDocumento.descricao}"/>
	            </a>
 			</display:column>
 			
 			<display:column style="text-align: center;" title="Descri��o Abreviada" sortable="true" sortProperty="descricaoAbreviada" >
	        	<a href="javascript:detalhar(<c:out value='${tipoDocumento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${tipoDocumento.descricaoAbreviada}"/>
	            </a>
 			</display:column>
 			
 			<display:column style="text-align: center;" title="Pag�vel">
	 			<a href="javascript:detalhar(<c:out value='${tipoDocumento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>				
					<c:out value='${tipoDocumento.indicadorPagavelFormatado}'/>
				</a>
			</display:column>
			
			<display:column style="text-align: center;" title="C�digo de Barras">
	 			<a href="javascript:detalhar(<c:out value='${tipoDocumento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${tipoDocumento.indicadorCodigoBarrasFormatado}'/>
				</a>	
			</display:column>
			
		</display:table>
		
	</c:if>
	
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaTipoDocumento}">
	  		<%--<vacess:vacess param="exibirAlterarTipoDocumento">--%>
				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" id="buttonAlterar" onclick="alterar()" type="button">
			<%--</vacess:vacess>--%>
			<%--<vacess:vacess param="removerTipoDocumento">--%>
				<input name="buttonRemover" value="Excluir" class="bottonRightCol2 bottonLeftColUltimo" onclick="remover()" type="button">
			<%--</vacess:vacess>--%>
   		</c:if>
   		<%--<vacess:vacess param="exibirIncluirTipoDocumento">--%>
   			<input name="buttonIncluir" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluir()" type="button">
		<%--</vacess:vacess>--%>
	</fieldset>
	
</form:form> 