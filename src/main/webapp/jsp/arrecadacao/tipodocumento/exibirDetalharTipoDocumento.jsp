<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Tipo de Documento<a class="linkHelp" href="<help:help>/consultadasdevoluesfinanceiras.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span>.

<script type="text/javascript">
	
	function cancelar() {		
		location.href = '<c:url value="exibirPesquisaTipoDocumento"/>';
	}
	
	function alterar(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter('tipoDocumentoForm','exibirAlterarTipoDocumento');
	}
	
</script>	

<form:form method="post" action="exibirDetalharTipoDocumento" id="tipoDocumentoForm" name="tipoDocumentoForm">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${tipoDocumento.chavePrimaria}">
	<input type="hidden" name="chavePrimarias" id="chavePrimarias">
	
	<fieldset id="conteinerPesquisarTipoDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisaTipoDocumentoCol1" class="coluna">
			<label class="rotulo numeroPosicao" id="rotuloNumero" for="numero" >Descri��o:</label>
			<span class="itemDetalhamento">
			<c:out value="${tipoDocumento.descricao}"/></span><br/>
			
			<label class="rotulo numeroPosicao" id="rotuloNumero" for="numero" >Descricao Abreviada:</label>
			<span class="itemDetalhamento">
			<c:out value="${tipoDocumento.descricaoAbreviada}"/></span><br/>
		</fieldset>
		
		<fieldset id="pesquisaTipoDocumentoCol2" class="colunaFinal">
			<label class="rotulo" for="habilitado">Pag�vel: &nbsp;</label>
			<span class="itemDetalhamento">
			<c:out value="${tipoDocumento.indicadorPagavelFormatado}"/></span><br/>
			
			<label class="rotulo" for="habilitado">C�digo de Barra: &nbsp;</label>
			<span class="itemDetalhamento">
			<c:out value="${tipoDocumento.indicadorCodigoBarrasFormatado}"/></span><br/>
			
			<label class="rotulo" for="habilitado">Indicador de uso: &nbsp;</label>
			<span class="itemDetalhamento">
			<c:if test="${tipoDocumento.habilitado eq 'true'}">Ativo</c:if>
			<c:if test="${tipoDocumento.habilitado eq 'false'}">Inativo</c:if>
			</span><br/>
		</fieldset>
		
	</fieldset>
	
	<fieldset id="exibirLogoDetalhar">
		<c:if test="${tipoDocumento.modelos ne 'null'}">
			<center><img id="imagemLogoDetalhar" src="<c:url value="/exibirModeloTipoDocumento=${tipoDocumento.modelos}"/>" width="800px" /></center>	
		</c:if>
	</fieldset>	
	
	<fieldset class="conteinerBotoes">		
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
		<%--<vacess:vacess param="exibirAlterarTipoDocumento">--%>
			<input name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoGrande1" onclick="alterar(${tipoDocumento.chavePrimaria})" type="button">
		<%--</vacess:vacess>--%>
	</fieldset>
	
</form:form> 