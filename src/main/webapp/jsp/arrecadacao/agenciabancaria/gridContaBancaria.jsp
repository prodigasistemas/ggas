<!--
 Copyright (C) <2011> GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

 Este programa  um software livre; voc pode redistribu-lo e/ou
 modific-lo sob os termos de Licena Pblica Geral GNU, conforme
 publicada pela Free Software Foundation; verso 2 da Licena.

 O GGAS  distribudo na expectativa de ser til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
 COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
 Consulte a Licena Pblica Geral GNU para obter mais detalhes.

 Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
 junto com este programa; se no, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">

function removerContaBancaria(index){
	var noCache = "noCache=" + new Date().getTime();
	$("#gridContaBancaria").load("removerContaBancaria?indexListaContaBancaria="+index+"&"+noCache);
}

</script>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<div class="bootstap">
	<c:set var="i" value="0" />
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover"
				id="agenciaBancaria" style="width: 100%">
				<thead class="thead-ggas-bootstrap">
					<tr>
						<th class="text-center">Descri��o</th>
						<th class="text-center">N�mero da Conta</th>
						<th class="text-center">D�gito Verificador</th>
						<th class="text-center">Alterar</th>
						<th class="text-center">Remover</th>
					</tr>

				</thead>
				<tbody>
					<c:forEach items="${listaContaBancaria}" var="contaBancaria">
						<tr>
							<td class="text-center"><c:out
									value="${contaBancaria.descricao}" /></td>

							<td class="text-center"><c:out
									value="${contaBancaria.numeroConta}" /></td>

							<td class="text-center"><c:out
									value="${contaBancaria.numeroDigito}" /></td>
							<td class="text-center"><a
								href="javascript:exibirAlteracaoContaBancaria('${i}', '${contaBancaria.descricao}' ,'${contaBancaria.numeroConta}' ,'${contaBancaria.numeroDigito}');">
									<i class="fas fa-pencil-alt"></i>
							</a></td>

							<td class="text-center"><a
								onclick="javascript:removerContaBancaria(<c:out value="${i}"/>);">
									<i class="far fa-trash-alt"></i>
							</a></td>
						</tr>
					</c:forEach>

				</tbody>

			</table>
			<c:set var="i" value="${i + 1}" />
		</div>
	</div>
</div>
