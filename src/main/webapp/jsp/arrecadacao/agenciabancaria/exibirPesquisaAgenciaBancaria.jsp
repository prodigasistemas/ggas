<!--
 Copyright (C) <2011> GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

 Este programa  um software livre; voc pode redistribu-lo e/ou
 modific-lo sob os termos de Licena Pblica Geral GNU, conforme
 publicada pela Free Software Foundation; verso 2 da Licena.

 O GGAS  distribudo na expectativa de ser til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
 COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
 Consulte a Licena Pblica Geral GNU para obter mais detalhes.

 Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
 junto com este programa; se no, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<script type="text/javascript">
	$(document).ready(function() {
		$("input[type=text]").attr("onpaste", "return false;");
		$("#codigo").inputmask({mask:["9999", "9999-9"]},{placeholder:"_"});
	});

	function pesquisar() {
		submeter('agenciaBancariaForm', 'pesquisarAgenciaBancaria');
	}

	function alterarAgenciaBancaria() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms["agenciaBancariaForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('agenciaBancariaForm', 'exibirAlteracaoAgenciaBancaria');
		}

	}

	function detalharAgenciaBancaria(chave) {
		document.forms["agenciaBancariaForm"].chavePrimaria.value = chave;
		submeter('agenciaBancariaForm', 'exibirDetalhamentoAgenciaBancaria');
	}

	function limparFormulario() {
		limparFormularios(document.agenciaBancariaForm);
		document.forms['agenciaBancariaForm'].habilitado[0].checked = true;
	}

	function incluir() {
		location.href = '<c:url value="/exibirInclusaoAgenciaBancaria"/>';
	}

	function removerAgenciaBancaria() {

		var selecao = verificarSelecao();

		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				submeter('agenciaBancariaForm', 'removerAgenciaBancaria');
			}
		}
	}
</script>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<div class="bootstrap">
	<form action="pesquisarAgenciaBancaria" id="agenciaBancariaForm"
		name="agenciaBancariaForm" method="post">

		<div class="card">
			<!--- Inicio do Card --->
			<div class="card-header">
				<h5 class="card-title mb-0">Pesquisar Ag�ncia Banc�ria</h5>
			</div>
			<div class="card-body">
				<!--- Inicio do Card-Body--->

				<input name="chavePrimaria" type="hidden" id="chavePrimaria">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Para pesquisar um registro
					especfico, informe os dados nos campos abaixo e clique em <strong>Pesquisar</strong>,
					ou clique apenas em <strong>Pesquisar</strong> para exibir todos.
					Para incluir um novo registro clique em <strong>Incluir</strong>.
				</div>
				<div class="row">
					<!--- Inicio da Linha de Pesquisa--->
					<div class="col-md-6">

						<div class="form-row">
							<div class="col-md-10">
								<label for="banco">Banco:</label> <select name="chaveBanco"
									id="chaveBanco" class="form-control form-control-sm">
									<option value="">Selecione</option>
									<c:forEach items="${listaBanco}" var="banco">
										<option value="<c:out value="${banco.chavePrimaria}"/>"
											<c:if test="${agenciaBancariaVO.chaveBanco == banco.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${banco.nome}" />
										</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="form-row">
							<div class="col-md-10">
								<label for="codigo">C�digo da Ag�ncia:</label> <input
									class="form-control form-control-sm" type="text" id="codigo"
									name="codigo" maxlength="6" size="6"
									value="${agenciaBancariaVO.codigo}">
							</div>

						</div>

						<div class="form-row">
							<div class="col-md-10">
								<label for="nome">Nome da Ag�ncia:</label> <input
									class="form-control form-control-sm" type="text" id="nome"
									name="nome" maxlength="40" size="40"
									value="${agenciaBancariaVO.nome}">
							</div>

						</div>
					</div>
					<div class="col-md-6">
						<div class="form-row">
							<label for="habilitado" class="col-md-12">Indicador de
								Uso:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoAtivo" name="habilitado"
										class="custom-control-input" value="true"
										<c:if test="${habilitado eq 'true'}">checked</c:if>> <label
										class="custom-control-label" for="habilitadoAtivo">Ativo</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoInativo" name="habilitado"
										class="custom-control-input" value="false"
										<c:if test="${habilitado eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="habilitadoInativo">Inativo</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoTodos" name="habilitado"
										class="custom-control-input" value=""
										<c:if test="${habilitado eq ''}">checked</c:if>> <label
										class="custom-control-label" for="habilitadoTodos">Todos</label>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--- Fim da Linha de Pesquisa--->

				<div class="row mt-3">
					<div class="col align-self-end text-right">
						<button class="btn btn-primary btn-sm" id="botaoPesquisar"
							type="submit" onclick="pesquisar();">
							<i class="fa fa-search"></i> Pesquisar
						</button>

						<button class="btn btn-secondary btn-sm" id="botaoLimpar"
							type="button" onclick="limparFormulario();">
							<i class="far fa-trash-alt"></i> Limpar
						</button>
					</div>

				</div>

				<c:if test="${listaAgenciaBancaria ne null}">
					<hr />
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover"
							id="agenciaBancaria" style="width: 100%">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th class="text-center" data-orderable="false"><input
										type="checkbox" name="checkAllAuto" id="checkAllAuto" disabled />
									</th>
									<th class="text-center">Ativo</th>
									<th class="text-center">Banco</th>
									<th class="text-center">C�digo da Ag�ncia Bancria</th>
									<th class="text-center">Nome da Agncia Bancria</th>
								</tr>

							</thead>
							<tbody>
								<c:forEach items="${listaAgenciaBancaria}" var="agenciaBancaria">
									<tr>
										<td class="text-center"><input
											id="checkboxChavesPrimarias${agenciaBancaria.chavePrimaria}"
											type="checkbox" name="chavesPrimarias"
											value="${agenciaBancaria.chavePrimaria}"></td>

										<td class="text-center"><c:choose>
												<c:when test="${agenciaBancaria.habilitado == true}">
													<i class="fas fa-check-circle text-success" title="Ativo"></i>
												</c:when>
												<c:otherwise>
													<i class="fas fa-ban text-danger" title="Inativo"></i>
												</c:otherwise>
											</c:choose></td>
										<td class="text-center"><a
											href="javascript:detalharAgenciaBancaria(<c:out value='${agenciaBancaria.chavePrimaria}'/>);">
												<c:out value="${agenciaBancaria.banco.nome}" />
										</a></td>

										<td class="text-center"><a
											href="javascript:detalharAgenciaBancaria(<c:out value='${agenciaBancaria.chavePrimaria}'/>);">
												<c:out value="${agenciaBancaria.codigo}" />
										</a></td>

										<td class="text-center"><a
											href="javascript:detalharAgenciaBancaria(<c:out value='${agenciaBancaria.chavePrimaria}'/>);">
												<c:out value="${agenciaBancaria.nome}" />
										</a></td>
									</tr>
								</c:forEach>
							</tbody>

						</table>
					</div>
				</c:if>

			</div>
			<!--- Fim do Card-Body--->
		<div class="card-footer">
			<div class="row">
				<div class="col-sm-12">

				
					<button type="button" id="buttonIncluir" value="Incluir" 
					class="btn btn-sm btn-primary float-right ml-1 mt-1" onclick="incluir()">
					<i class="fa fa-plus"></i> Incluir
					</button>

					<c:if test="${not empty listaAgenciaBancaria}">

						<vacess:vacess param="exibirAlteracaoAgenciaBancaria">
							<button id="buttonAlterar" value="Alterar"
								class="btn btn-sm btn-primary float-right ml-1 mt-1"
								onclick="alterarAgenciaBancaria();">
								<i class="fas fa-pencil-alt"></i> Alterar
							</button>
						</vacess:vacess>

						<vacess:vacess param="removerAgenciaBancaria">
							<button id="buttonRemover" value="Remover"
								class="btn btn-danger btn-sm ml-1 mt-1 float-left"
								onclick="removerAgenciaBancaria();">
								<i class="far fa-trash-alt"></i> Remover
							</button>
						</vacess:vacess>
					</c:if>
				</div>
			</div>
		</div>
		
		</div>
		<!--- Fim do Card --->

		
	</form>

</div>
