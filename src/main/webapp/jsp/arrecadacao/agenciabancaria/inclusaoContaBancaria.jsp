<!--
 Copyright (C) <2011> GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

 Este programa  um software livre; voc pode redistribu-lo e/ou
 modific-lo sob os termos de Licena Pblica Geral GNU, conforme
 publicada pela Free Software Foundation; verso 2 da Licena.

 O GGAS  distribudo na expectativa de ser til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
 COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
 Consulte a Licena Pblica Geral GNU para obter mais detalhes.

 Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
 junto com este programa; se no, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<%-- <h1 class="tituloInterno">Incluir Agncia Bancria<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1> --%>
<!-- <p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p> -->

<script type="text/javascript">
	function adicionarContaBancaria() {
		var noCache = "noCache=" + new Date().getTime();
		var descricao = $("#descricao").val();
		var numeroConta = $("#numeroConta").val();
		var numeroDigito = $("#numeroDigito").val();
		var url = "adicionarContaBancaria?descricao=" + descricao
				+ "&numeroConta=" + numeroConta + "&numeroDigito="
				+ numeroDigito + "&" + noCache;

		var sucesso = carregarFragmento("gridContaBancaria", url);

		if (sucesso) {
			limparFormContaBancaria();
		}

	}

	function exibirAlteracaoContaBancaria(index, descricao, numeroConta,
			numeroDigito) {

		$("#indexListaContaBancaria").val(index);
		$("#descricao").val(descricao);
		$("#numeroConta").val(numeroConta);
		$("#numeroDigito").val(numeroDigito);

		$("#botaoAlterarContaBancaria").removeAttr("disabled", "disabled");
		$("#botaoIncluirContaBancaria").attr("disabled", "disabled");

	}

	function alterarContaBancaria() {
		var noCache = "noCache=" + new Date().getTime();
		var descricao = $("#descricao").val();
		var numeroConta = $("#numeroConta").val();
		var numeroDigito = $("#numeroDigito").val();
		var indexLista = $("#indexListaContaBancaria").val();

		var url = "alterarContaBancaria?descricao=" + descricao
				+ "&numeroConta=" + numeroConta + "&numeroDigito="
				+ numeroDigito + "&indexListaContaBancaria=" + indexLista + "&"
				+ noCache;

		var sucesso = carregarFragmento("gridContaBancaria", url);

		if (sucesso) {
			limparFormContaBancaria();
			$("#botaoAlterarContaBancaria").attr("disabled", "disabled");
			$("#botaoIncluirContaBancaria").removeAttr("disabled", "disabled");
		}

	}

	function limparFormulario() {
		limparFormularios(document.agenciaBancariaForm);
	}

	function limparFormContaBancaria() {
		$("#descricao").val(null);
		$("#numeroConta").val(null);
		$("#numeroDigito").val(null);
	}
</script>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>


<div class="bootstrap">
	<form method="post" id="contaBancariaForm" name="contaBancariaForm"	modelAttribute="ContaBancariaVO">
		
		
							
		<div class="card-body">
			<hr/>
			<h5 class="card-title mb-0">Contas Banc�rias</h5>
			
			</br>
			<input type="hidden" id="indexListaContaBancaria"
				name="indexListaContaBancaria">
			<div class="row">
				<div class="col-md-4">
					<div class="form-row">
						<div class="col-md-10">
							<label for="descricao">Descri��o:<span
								class="text-danger">*</span>
							</label> <input class="form-control form-control-sm" type="text"
								id="descricao" name="descricao" maxlength="30" size="30"
								onkeyup="letraMaiuscula(this);"
								value="${contaBancariaVO.descricao}">
						</div>

					</div>
				</div>

				<div class="col-md-4">
					<div class="form-row">
						<div class="col-md-10">
							<label for="numeroConta">N�mero da Conta:<span
								class="text-danger">*</span>
							</label> <input class="form-control form-control-sm" type="text"
								id="numeroConta" name="numeroConta" maxlength="20" size="20"
								onkeypress="return formatarCampoInteiro(event);"
								value="${contaBancariaVO.numeroConta}">
						</div>

					</div>
				</div>

				<div class="col-md-4">
					<div class="form-row">
						<div class="col-md-5">
							<label for="fax">D�gito Verificador:<span
								class="text-danger">*</span>
							</label> <input class="form-control form-control-sm" type="text"
								id="numeroDigito" name="numeroDigito" maxlength="2" size="2"
								onkeypress="return formatarCampoInteiro(event);"
								value="${contaBancariaVO.numeroDigito}">
						</div>
					</div>
				</div>
			</div>
			</br>
			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-secondary btn-sm float-right ml-1 mt-1"
						type="button" onclick="limparFormContaBancaria();" value="Limpar">
						<i class="far fa-trash-alt"></i> Limpar
					</button>

					<button id="botaoIncluirContaBancaria"
						class="btn btn-sm btn-primary btn-sm ml-1 mt-1 float-right"
						type="button" onclick="adicionarContaBancaria();"
						value="Adicionar Conta">
						<i class="fa fa-plus"></i> Adicionar Conta
					</button>

					<button id="botaoAlterarContaBancaria"
						class="btn btn-primary btn-sm ml-1 mt-1 float-right" type="button"
						onclick="alterarContaBancaria();" disabled="disabled"
						value="Alterar Conta" type="button">
						<i class="fas fa-pencil-alt"></i>Alterar Conta
					</button>
				</div>
			</div>


		</div>

		<div id="gridContaBancaria">
			<jsp:include
				page="/jsp/arrecadacao/agenciabancaria/gridContaBancaria.jsp"></jsp:include>
		</div>
	
	</form>
	
	
</div>
