<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script type="text/javascript">

$(document).ready(function(){
	$("input[type=text]").attr("onpaste","return false;");
	$("#codigo").inputmask({mask:["9999","9999-9"]},{placeholder:"_"});
});

function cancelar(){
	location.href = '<c:url value="/exibirPesquisaAgenciaBancaria"/>';
}

function voltar() {	
	submeter("agenciaBancariaForm", "pesquisarAgenciaBancaria");
}

function limparFormulario(){
	limparFormularios(document.agenciaBancariaForm);	
}

function incluirAgenciaBancaria(){
	var fluxoInclusao = $("#fluxoInclusao").val();
	var fluxoAlteracao = $("#fluxoAlteracao").val();
	
	if(fluxoInclusao != ""){
		submeter('agenciaBancariaForm', 'incluirAgenciaBancaria');
	}

	if(fluxoAlteracao != ""){
		submeter('agenciaBancariaForm', 'alterarAgenciaBancaria');
	}	
}

</script>

<script type='text/javascript' src='<c:out value=' ${pageContext.request.contextPath}' />/dwr/util.js'></script>

<script type='text/javascript' src='<c:out value=' ${pageContext.request.contextPath}' />/dwr/interface/AjaxService.js'>
</script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<div class="bootstrap">
    <form method="post" id="agenciaBancariaForm" name="agenciaBancariaForm" modelAttribute="AgenciaBancariaVO">
        <div class="card">
			
			<c:if test="${ fluxoInclusao eq true }">
				<div class="card-header">
					<h5 class="card-title mb-0">Incluir Ag�ncia Banc�ria</h5>
				</div>
			</c:if>

			<c:if test="${ fluxoAlteracao eq true }">
				<div class="card-header">
					<h5 class="card-title mb-0">Alterar Ag�ncia Banc�ria</h5>
				</div>
			</c:if>

            <div class="card-body">
                <input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${agenciaBancariaVO.chavePrimaria}">
                <input type="hidden" id="fluxoInclusao" name="fluxoInclusao" value="${fluxoInclusao}">
                <input type="hidden" id="fluxoAlteracao" name="fluxoAlteracao" value="${fluxoAlteracao}">
                <input type="hidden" id="fluxoVoltar" name="fluxoVoltar">

				<c:if test="${ fluxoInclusao eq true }">
					<div class="alert alert-primary fade show" role="alert">
						<i class="fa fa-question-circle"></i> Informe os dados abaixo e clique em <strong>Salvar</strong>
					</div>
				</c:if>

				<c:if test="${ fluxoAlteracao eq true }">
					<div class="alert alert-primary fade show" role="alert">
						<i class="fa fa-question-circle"></i> Para modificar as informaes deste registro clique em <strong>Alterar</strong>
					</div>
				</c:if>



				<div class="row">
                    <div class="col-md-6">
                        <div class="form-row">
                            <div class="col-md-10">
                                <label for="banco">Banco:</label>
                                <select name="chaveBanco" id="chaveBanco" class="form-control form-control-sm">
                                    <option value="">Selecione</option>
                                    <c:forEach items="${listaBanco}" var="banco">
                                        <option value="<c:out value="${banco.chavePrimaria}"/>"
                                            <c:if test="${agenciaBancariaVO.chaveBanco == banco.chavePrimaria}">
                                                selected="selected"</c:if>>
                                            <c:out value="${banco.nome}" />

                                        </option>

                                    </c:forEach>
                                </select>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-10">
                                <label for="codigo">C�digo da Ag�ncia:</label>
                                <input class="form-control form-control-sm" type="text" id="codigo" name="codigo"  maxlength="6" size="6"
                                    value="${agenciaBancariaVO.codigo}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-10">
                                <label for="nome">Nome da Ag�ncia:</label>
                                <input class="form-control form-control-sm" type="text" name="nome" id="nome" maxlength="40" size="40"
                                    value="${agenciaBancariaVO.nome}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="form-row">
                            <div class="col-md-10">
                                <label for="telefone">Numero de Telefone:</label>
                                <input class="form-control form-control-sm" type="text" id="telefone" name="telefone" maxlength="9" size="9"
                                    value="${agenciaBancariaVO.telefone}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-10">
                                <label for="fax">N�mero do Fax:</label>
                                <input class="form-control form-control-sm" type="text" id="fax" name="fax" maxlength="9" size="9"
                                    value="${agenciaBancariaVO.fax}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-10">
                                <label for="telefone">E-mail:</label>
                                <input class="form-control form-control-sm" type="text" id="email" name="email" maxlength="40" size="40"
                                    value="${agenciaBancariaVO.email}">
                            </div>
                        </div>

                        <c:if test="${fluxoAlteracao eq true}">
                            <div class="form-row">
                                <label for="habilitado" class="col-md-12">Indicador de Uso:</label>
                                <div class="col-md-12">

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="habilitado" id="habilitadoAtivo" class="custom-control-input" value="true"
                                        <c:if test="${agenciaBancariaVO.habilitado eq 'true'}">checked</c:if> >
                                        <label class="custom-control-label" for="habilitadoAtivo">Ativo</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="habilitado" id="habilitadoInativo" class="custom-control-input" value="false"
                                        <c:if test="${agenciaBancariaVO.habilitado eq 'false'}">checked</c:if> >
                                        <label class="custom-control-label" for="habilitadoInativo">Inativo</label>
                                    </div>
                                </div>
                            </div>

                        </c:if>
                    </div>

                </div>
            </div>

            <jsp:include page="/jsp/arrecadacao/agenciabancaria/inclusaoContaBancaria.jsp"></jsp:include>

            <div class="card-footer">

                <div class="row">
                    <div class="col-sm-12">

                        <c:if test="${ fluxoInclusao eq true }">
                            <button class="btn btn-secondary btn-sm" value="Cancelar" type="button"
                                onclick="cancelar();">
                                <i class="fa fa-chevron-circle-left"></i> Cancelar
                            </button>
														
							 <vacess:vacess param="exibirInclusaoAgenciaBancaria">
                                <button class="btn btn-primary btn-sm btn-sm ml-1 mt-1 float-right" id="buttonSalvar" name="button" value="Salvar" type="button"
                                    onclick="incluirAgenciaBancaria();">
                                    <i class="fa fa-save"></i> Salvar
                                </button>
                            </vacess:vacess>
							
                            <button class="btn btn-secondary btn-sm btn-sm ml-1 mt-1 float-right" value="Limpar" type="button"
                                onclick="limparFormulario();">
                                <i class="fa fa-times"></i> Limpar
                            </button>
                          
                        </c:if>
                        
                        <c:if test="${ fluxoAlteracao eq true }">
                            <button class="btn btn-secondary btn-sm" value="Cancelar" type="button"
                                onclick="cancelar();">
                                <i class="fa fa-chevron-circle-left"></i> Cancelar
                            </button>
														
							 <vacess:vacess param="exibirAlteracaoAgenciaBancaria">
                                <button class="btn btn-primary btn-sm btn-sm ml-1 mt-1 float-right" id="buttonSalvar" name="button" value="Salvar" type="button"
                                    onclick="incluirAgenciaBancaria();">
                                    <i class="fa fa-save"></i> Salvar
                                </button>
                            </vacess:vacess>
							
                            <button class="btn btn-secondary btn-sm btn-sm ml-1 mt-1 float-right" value="Limpar" type="button"
                                onclick="limparFormulario();">
                                <i class="fa fa-times"></i> Limpar
                            </button>
                          
                        </c:if>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
