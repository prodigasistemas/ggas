<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

function voltar() {	
	submeter("agenciaBancariaForm", "pesquisarAgenciaBancaria");
}

</script>

<script type='text/javascript'
src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>

<script type='text/javascript'
src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<div class="bootstrap">
	<form action="exibirAlteracaoAgenciaBancaria" id="agenciaBancariaForm" name="agenciaBancariaForm" method="post">
		<div class="card"> <!--- Inicio do Card --->
			<div class="card-header">
				<h5 class="card-title mb-0">Detalhar Ag�ncia Banc�ria</h5>
			</div>



			<div class="card-body"><!--- Inicio do Card-Body--->

				<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${agencia.chavePrimaria}">

				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Para modificar as informa��es deste registro clique em <strong>Alterar</strong>.
				</div>



				<div class="row">

					<div class="col-md-6">

						<ul class="list-group">

							<li class="list-group-item">
								Banco: <span class="font-weight-bold">
									<c:out value='${agencia.banco.nome}'/></span>
								</li>

								<li class="list-group-item">
									C�digo da Ag�ncia: <span class="font-weight-bold">
										<c:out value='${agencia.codigo}'/></span>
									</li>

									<li class="list-group-item">
										Nome da Ag�ncia: <span class="font-weight-bold">
											<c:out value='${agencia.nome}'/></span>
										</li>
									</ul>
								</div>

								<div class="col-md-6">

									<ul class="list-group">

										<li class="list-group-item">
											N�mero de Telefone: <span class="font-weight-bold">
												<c:out value='${agencia.telefone}'/></span>
											</li>

											<li class="list-group-item">
												N�mero do Fax: <span class="font-weight-bold">
													<c:out value='${agencia.fax}'/></span>
												</li>

												<li class="list-group-item">
													E-mail: <span class="font-weight-bold">
														<c:out value='${agencia.email}'/></span>
													</li>
												</ul>
											</div>

										</div>

										<hr/>

										<div class="table-responsive">
											<table class="table table-bordered table-striped table-hover" id="contaBancaria" style="width: 100%">

												<thead class="thead-ggas-bootstrap">
													<tr>
														<th class="text-center">Descri��o</th>
														<th class="text-center">N�mero da Conta</th>
														<th class="text-center">D�gito Verificador</th>
													</tr>

												</thead>
												<tbody >
													<c:forEach items="${listaContaBancaria}" var="contaBancaria">
													<tr>
														<td class="text-center">
															<c:out value="${contaBancaria.descricao}"/>

														</td>

														<td class="text-center">
															<c:out value="${contaBancaria.numeroConta}"/>

														</td>

														<td class="text-center">
															<c:out value="${contaBancaria.numeroDigito}"/>

														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div><!--- Fim do Card-Body--->

								<div class="card-footer">
									<div class="row">
										<div class="col-sm-12">
											<button type="button" id="buttonVoltar" value="Voltar" name="Button"
											class="btn btn-secondary btn-sm float-left ml-1 mt-1" onclick="voltar();">
											<i class="fa fa-chevron-circle-left"></i> Voltar
										</button>

										<button id="buttonAlterar" value="Alterar" name="button"
										class="btn btn-sm btn-primary btn-sm ml-1 mt-1 float-right"
										type="submit">
										<i class="fas fa-pencil-alt"></i> Alterar
									</button>
								</div>

							</div>

						</div>
					</div> <!---Fim do Card --->


				</form>
			</div>