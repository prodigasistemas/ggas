<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>


<h1 class="tituloInterno">Alterar Arrecadador<a href="<help:help>/arrecadadorinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form:form method="post" action="alterarArrecadador" enctype="multipart/form-data" id="arrecadadorForm" name="arrecadadorForm" modelAttribute="ArrecadadorImpl">

<script>

	$().ready(function(){
		$("input#codigoAgente").keyup(apenasNumeros).blur(apenasNumeros);
		var idCliente = $("#cliente").val();
		if(idCliente!=""){
			selecionarCliente(idCliente);
		}		
	});

	function voltar() {
		location.href = '<c:url value="/exibirPesquisaArrecadador"/>';
	}
	function limparFormArrecadador(){
		
		limparFormularios(arrecadadorForm);
		document.getElementById("habilitado").checked = true;
		limparFormularioDadosCliente();
	}
	
	function salvar(){
		$("#cliente").val( $("#idCliente").val() );
		$("#arrecadadorForm").submit();
	}

</script>

	<input name="acao" type="hidden" id="acao" value="alterarArrecadador"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${arrecadador.chavePrimaria}"/>
	<input name="cliente" type="hidden" id="cliente" value="<c:if test="${arrecadador.cliente ne null}"></c:if><c:out value="${arrecadador.cliente.chavePrimaria}"/>"/> 
	<%-- 
	--%>
	
	<fieldset id="pesquisaArrecadador" class="conteinerPesquisarIncluirAlterar">	
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${arrecadador.cliente.chavePrimaria}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${arrecadador.cliente.nome}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente"	value="${arrecadador.cliente.getNumeroDocumentoFormatado()}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${arrecadador.cliente.emailPrincipal}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${arrecadador.cliente.getEnderecoPrincipal().getEnderecoFormatado()}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
	
		<fieldset id="pesquisarBanco" class="colunaDir">			
			<label class="rotulo campoObrigatorio" id="rotuloBanco" for="banco"><span class="campoObrigatorioSimbolo">* </span>Banco:</label>
			<select class="campoSelect" id="banco" name="banco">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaBanco}" var="banco">
					<option value="<c:out value="${banco.chavePrimaria}"/>" <c:if test="${arrecadador.banco.chavePrimaria == banco.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${banco.nome}"/>
					</option>		
		    	</c:forEach>
			</select>
			<br/>
			<label class="rotulo campoObrigatorio" id="rotuloCodigoAgente" for="codigoAgente" ><span class="campoObrigatorioSimbolo">* </span>C�digo Agente:</label>
			<input class="campoTexto" type="text" id="codigoAgente" name="codigoAgente" maxlength="5" size="5" value="${arrecadador.codigoAgente}">
			<label class="rotulo rotuloRadio" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${arrecadador.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="ativo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${arrecadador.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="inativo">Inativo</label>
		</fieldset>
	</fieldset>
	<br/>
	<fieldset class="conteinerBotoes">
	    <input name="buttonCancelar" class="bottonRightCol" value="Cancelar" type="button" onclick="voltar();">
	    <input name="buttonLimpar" class="bottonRightCol bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limparFormArrecadador();">
	<%--     <vacess:vacess param="incluirBanco.do"> --%>
	    	<input id="buttonSalvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="salvar()">
	<%--     </vacess:vacess> --%>
	</fieldset>
</form:form> 