<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%--
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
 --%>

<h1 class="tituloInterno">Pesquisar Arrecadador<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script language="javascript">

	$(document).ready(function(){
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
		$("input#codigoAgente").keyup(apenasNumeros).blur(apenasNumeros);
		var idCliente = $("#cliente").val();
		selecionarCliente(idCliente);
		
	});
	
	function pesquisar(){	
		$("#cliente").val( $("#idCliente").val() );
		$("#arrecadadorForm").submit();
	}
			
	function incluir() {		
		location.href = '<c:url value="/exibirInclusaoArrecadador"/>';
	}

	function removerArrecadador(){
		var selecao = verificarSelecao();
		if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('arrecadadorForm', 'removerArrecadador');
				}
	    }
	} 	

	function detalharArrecadador(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("arrecadadorForm", "exibirDetalhamentoArrecadador");
	}
	
	function alterarArrecadador(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("arrecadadorForm", "exibirAlteracaoArrecadador");
		}
	}
	
	function limparFormArrecadador(){
		
		limparFormularios(arrecadadorForm);
		document.getElementById("habilitado").checked = true;
		limparFormularioDadosCliente();
		
	}
	
					
	</script>

<form:form method="post" action="pesquisarArrecadador" id="arrecadadorForm" name="arrecadadorForm" modelAttribute="ArrecadadorImpl"> 
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"/>
	<input name="chavePrimarias" type="hidden" id="chavePrimarias"/>
	<input name="cliente" type="hidden" id="cliente" value="${arrecadador.cliente.chavePrimaria}"/>
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
<fieldset id="pesquisaArrecadador" class="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisarCliente" class="colunaEsq">
		<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
			<jsp:param name="idCampoIdCliente" value="idCliente"/>
			<jsp:param name="idCliente" value="${arrecadadorForm.map.idCliente}"/>
			<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
			<jsp:param name="nomeCliente" value="${arrecadadorForm.map.nomeCliente}"/>
			<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
			<jsp:param name="documentoFormatadoCliente" value="${arrecadadorForm.map.documentoCliente}"/>
			<jsp:param name="idCampoEmail" value="emailCliente"/>
			<jsp:param name="emailCliente" value="${arrecadadorForm.map.emailCliente}"/>
			<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
			<jsp:param name="enderecoFormatadoCliente" value="${arrecadadorForm.map.enderecoCliente}"/>		
			<jsp:param name="dadosClienteObrigatorios" value="false"/>
			<jsp:param name="possuiRadio" value="true"/>
		</jsp:include>		
	</fieldset>
		
	<fieldset id="pesquisarBanco" class="colunaDir">			
		<label class="rotulo" id="rotuloBanco" for="banco">Banco:</label>
		<select class="campoSelect" id="banco" name="banco">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaBanco}" var="banco">
				<option value="<c:out value="${banco.chavePrimaria}"/>" <c:if test="${arrecadador.banco.chavePrimaria == banco.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${banco.nome}"/>
				</option>		
	    	</c:forEach>
		</select>
		<br/>
		<label class="rotulo" for="codigoAgente">C�digo Agente:</label>
		<input class="campoTexto" type="text" id="codigoAgente" name="codigoAgente" maxlength="5" size="5"
			value="${arrecadador.codigoAgente}">
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="ativo">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="inativo">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
		<label class="rotuloRadio" for="todos">Todos</label>		
	</fieldset>	
</fieldset>
	<fieldset class="conteinerBotoesPesquisarDirFixo">		
		 <vacess:vacess param="exibirPesquisarArrecadador">
	   		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisar()">
	 	</vacess:vacess>
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormArrecadador();">
	</fieldset>
			<br/>	
	<c:if test="${listaArrecadador ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaArrecadador" sort="list" id="arrecadador" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15" requestURI="pesquisarArrecadador">
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${arrecadador.chavePrimaria}">
	     	</display:column>
	     	 <display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${arrecadador.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
		     <display:column sortable="true" sortProperty="cliente.nome" title="Nome do Cliente" headerClass="tituloTabelaEsq">
		     	<a href='javascript:detalharArrecadador(<c:out value='${arrecadador.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${arrecadador.cliente.nome}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" title="Banco" sortProperty="banco.nome">
				<a href='javascript:detalharArrecadador(<c:out value='${arrecadador.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${arrecadador.banco.nome}'/>
	        	</a>
	    	 </display:column>
		 	<display:column sortable="true" title="C�digo do Agente" style="width: 110px" sortProperty="codigoAgente">
		 		<a href='javascript:detalharArrecadador(<c:out value='${arrecadador.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${arrecadador.codigoAgente}'/>
				</a>
	    	</display:column>
		</display:table>
	</c:if>
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaArrecadador}">
			<vacess:vacess param="exibirPesquisarArrecadador">
				<input name="buttonAlterar" id="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarArrecadador();" type="button">
			 </vacess:vacess>
			<vacess:vacess param="removerArrecadador">
			</vacess:vacess>
				<input name="buttonRemover" id="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerArrecadador()" type="button">
		</c:if>
		<vacess:vacess param="exibirInclusaoArrecadador">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" type="button" onclick="javascript:incluir();">
		</vacess:vacess>
		
	</fieldset>
</form:form> 

