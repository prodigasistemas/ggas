<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<script>

	function alterarDetalharArrecadador(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("arrecadadorForm", "exibirAlteracaoArrecadador");		
	}
	
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaArrecadador"/>';
	}
	
	
</script>

<h1 class="tituloInterno">Detalhamento de Arrecadador<a class="linkHelp" href="<help:help>/detalhararrecadador.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="exibirDetalhamentoArrecadador" enctype="multipart/form-data" id="arrecadadorForm" name="arrecadadorForm" modelAttribute="ArrecadadorImpl">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${arrecadador.cliente.chavePrimaria}"/>	
	<input name="postBack" type="hidden" id="postBack" value="false">
	
	<fieldset class="detalhamento">
		<fieldset id="" class="conteinerBloco">					
			<fieldset id="pesquisarCliente" class="colunaEsq">
				<legend>Dados do Cliente</legend>
				<div class="conteinerDados">											
					<label class="rotulo" id="rotuloCliente">Cliente:</label>
					<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${arrecadador.cliente.nome}"/></span><br />
			     	<c:choose>
						<c:when test="${arrecadador.cliente.cpf ne null}">
							<label class="rotulo" id="rotuloCnpjTexto">CPF:</label>
							<span class="itemDetalhamento"><c:out value="${arrecadador.cliente.cpf}"/></span><br />
						</c:when>
						<c:otherwise>
							<label class="rotulo" id="rotuloCnpjTexto">CNPJ:</label>
							<span class="itemDetalhamento"><c:out value="${arrecadador.cliente.cnpj}"/></span><br />
						</c:otherwise>				
					</c:choose>
					<label class="rotulo" id="rotuloEnderecoTexto">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${arrecadador.cliente.getEnderecoPrincipal().getEnderecoFormatado()}"/></span><br />
					<label class="rotulo" id="rotuloEnderecoTexto">E-mail:</label>
					<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${arrecadador.cliente.emailPrincipal}"/></span><br />
				</div>
			</fieldset>
			<fieldset id="pesquisarArrecadador" class="colunaDir">			
				<label class="rotulo" id="rotuloBanco" for="banco">Banco:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio" id="Banco"><c:out value="${arrecadador.banco.nome}"/></span><br />
				<br/>
				<label class="rotulo" for="codigoAgente">C�digo Agente:</label>
				<span class="itemDetalhamento" id="codigoAgente"><c:out value="${arrecadador.codigoAgente}"/></span><br />
				<br />
				<label class="rotulo" for="habilitado">Indicador de Uso:</label>
				<span class="itemDetalhamento" id="habilitado"> 
					<c:choose>
						<c:when test="${arrecadador.banco.habilitado == 'true'}">Ativo</c:when>
						<c:otherwise>Inativo</c:otherwise>
					</c:choose>
				</span>
			</fieldset>
		</fieldset>
		<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="voltar();">	    
	     <%--<vacess:vacess param="incluirBanco">--%>
	    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Alterar" type="button" 
	    		onclick="alterarDetalharArrecadador(<c:out value='${arrecadador.chavePrimaria}'/>);">
	     <%--</vacess:vacess>--%>
		</fieldset>	
</form:form> 