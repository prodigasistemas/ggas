<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Devolu��o Financeira<a class="linkHelp" href="<help:help>/detalhamentodedevoluofinanceira.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<form method="post" action="exibirDetalheDevolucao" name="devolucaoForm" id="devolucaoForm"> 

<script>

	$(document).ready(function(){
		$("#pontoConsumoPopup").dialog({
			autoOpen: false,			
			width: 370,
			modal: true,
			minHeight: 90,
			resizable: false
		});
	});
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter("devolucaoForm", "exibirAlteracaoDevolucao");
	}
	
	function voltar() {
		submeter("devolucaoForm", "pesquisarDevolucoes");
	}

	function exibirDadosPontoConsumo(idPontoConsumo) {
		
		var descricao = '';
		var endereco = '';
		var cep = '';
		var complemento = '';
		
		if(idPontoConsumo != ""){
	       	AjaxService.obterPontoConsumoPorChave( idPontoConsumo, { 
	           	callback: function(pontoConsumo) {
					descricao = pontoConsumo['descricao'];
					endereco = pontoConsumo['endereco'];
					cep = pontoConsumo['cep'];
					complemento = pontoConsumo['complemento'];
	       	  }
	       	 , async: false}
	       	);

			document.getElementById('descricaoPopup').innerHTML = descricao;
			document.getElementById('enderecoPopup').innerHTML = endereco;
			document.getElementById('cepPopup').innerHTML = cep;
			document.getElementById('complementoPopup').innerHTML = complemento;
		}
		
		exibirJDialog("#pontoConsumoPopup");
	}

</script>

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${devolucaoVO.chavePrimaria}"/>
<input name="versao" type="hidden" id="versao" value="${devolucaoVO.versao}">
<input name="postBack" type="hidden" id="postBack" value="true">

<div id="pontoConsumoPopup" title="Ponto de Consumo">
	<label class="rotulo">Descri��o:</label>
	<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="descricaoPopup"></span><br />
	<label class="rotulo">Endere�o:</label>
	<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="enderecoPopup"></span><br />
	<label class="rotulo">Cep:</label>
	<span class="itemDetalhamento" id="cepPopup"></span><br />
	<label class="rotulo">Complemento:</label>
	<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="complementoPopup"></span><br /><br />
</div>

<fieldset id="detalharDevolucao" class="conteinerPesquisarIncluirAlterar detalharPerfilParcelamento">
	<fieldset class="coluna detalhamentoColunaLarga">
		<label class="rotulo" for="descricao">Cliente:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno2"><c:out value="${devolucaoVO.nomeCompletoCliente}"/></span><br />
		<label class="rotulo" for="descricaoAbreviada">Ponto de Consumo:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno2"><c:out value="${devolucaoVO.descricaoPontoConsumo}"/></span><br />
		<label class="rotulo" for="idAmortizacao">Observa��o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno2"><c:out value="${devolucaoVO.observacaoDevolucao}"/></span><br />
	</fieldset>
	<fieldset class="colunaFinal">
		<label class="rotulo" for="maximoParcelas">Data da Devolu��o:</label>
		<span class="itemDetalhamento"><c:out value="${devolucaoVO.dataDevolucao}"/></span><br />
		<label class="rotulo" for="idAmortizacao">Valor da Devolu��o:</label>
		<span class="itemDetalhamento"><c:out value="${devolucaoVO.valorDevolucao}"/></span><br />
		<label class="rotulo" for="idAmortizacao">Tipo da Devolu��o:</label>
		<span class="itemDetalhamento"><c:out value="${devolucaoVO.tipoDevolucaoDetalhamento}"/></span><br />
	</fieldset>
	
	<c:if test="${listaFatura ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Notas de Cr�dito:</legend>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaFatura" sort="list" id="fatura" pagesize="15" excludedParams="" requestURI="#" >
				<display:column property="chavePrimaria" title="N� do Documento" style="width: 130px"/>
				<display:column property="tipoDocumento.descricao" title="Tipo" />
		   		<display:column sortable="false" title="Data de<br />Emiss�o" style="width: 75px">
		   			<fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy" />
		   		</display:column>
		   		<display:column sortable="false" title="Vencimento" style="width: 75px">
		   			<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy" />
		   		</display:column>
		   		<display:column sortable="false" title="Valor (R$)" style="width: 130px" >
		   			<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2" type="currency"/>
		   		</display:column>
		   		<display:column property="situacaoPagamento.descricao" sortable="false" title="Situa��o de Pagamento" style="width: 150px"/>
		   		<c:if test="${clientePopupVO.idCliente ne null && clientePopupVO.idCliente > 0}">
			   		<display:column title="Ponto de<br />Consumo" style="width: 60px">
						<a href="javascript:exibirDadosPontoConsumo('<c:out value="${fatura.pontoConsumo.chavePrimaria}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
					</display:column>	
				</c:if>   		
		   	</display:table>
		</fieldset>
	</c:if>
	<c:if test="${listaCreditoDebito ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Cr�ditos a Realizar:</legend>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" id="creditoDebito" pagesize="15" excludedParams="" requestURI="#" >
		   	<display:column property="chavePrimaria" title="N� do Cr�dito" style="width: 130px"/>
			   	<c:if test="${creditoDebitoNegociado.rubrica ne null}">
				   			<display:column title="Rubrica">
       				   			<c:out value="${creditoDebitoNegociado.rubrica.descricao}"/>
							</display:column>
				</c:if>
			   	<display:column sortable="false" title="In�cio da Cobran�a" style="width: 75px">
		   			<fmt:formatDate value="${creditoDebitoNegociado.dataInicioCobranca}" pattern="dd/MM/yyyy" />
		   		</display:column>
		   		<display:column sortable="false" title="Vencimento" style="width: 75px">
		   			<fmt:formatDate value="${creditoDebito.dataVencimento}" pattern="dd/MM/yyyy" />
		   		</display:column>
		   		<display:column title="Valor (R$)" style="width: 130px" >
		   			<fmt:formatNumber value="${creditoDebito.valor}" minFractionDigits="2" type="currency"/>
		   		</display:column>
		   		<display:column property="creditoDebitoSituacao.descricao" title="Situa��o do Cr�dito" style="width: 150px"/>
			   		<c:if test="${clientePopupVO.idCliente ne null && clientePopupVO.idCliente > 0}">
			   			<display:column title="Ponto de<br />Consumo">
							<a href="javascript:exibirDadosPontoConsumo('<c:out value="${creditoDebitoNegociado.pontoConsumo.chavePrimaria}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
						</display:column>
			   		</c:if>
		   	</display:table>
		</fieldset>
	</c:if>
	<c:if test="${listaRecebimento ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Recebimentos:</legend>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaRecebimento" sort="list" id="recebimento" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		        <display:column title="Cliente" >
		            <c:out value="${recebimento.cliente.nome}"/>
		        </display:column>
		         <display:column title="Documento" >
		        	<c:choose>
			        	<c:when test="${recebimento.documentoCobrancaItem != null}">
						    <c:out value="${recebimento.documentoCobrancaItem.chavePrimaria}"/>
						</c:when>
						<c:when test="${recebimento.faturaGeral != null}">
						    <c:out value="${recebimento.faturaGeral.chavePrimaria}"/>
						</c:when>
						<c:otherwise>
							--
						</c:otherwise>	
					</c:choose>
		        </display:column>
		        <display:column title="Data do<br />Recebimento" style="width: 75px">
		   			<fmt:formatDate value="${recebimento.dataRecebimento}" pattern="dd/MM/yyyy" />
		   		</display:column>
		   		<display:column title="Valor do<br />Documento (R$)" >
		        	<c:choose>
			            <c:when test="${recebimento.documentoCobrancaItem != null}">
						    <c:out value="${recebimento.documentoCobrancaItem.valorFormatado}"/>
						</c:when>
						<c:when test="${recebimento.faturaGeral != null}">
						    <c:out value="${recebimento.faturaGeral.faturaAtual.valorTotal}"/>
						</c:when>
						<c:otherwise>
							--
						</c:otherwise>
					</c:choose>
		        </display:column>
		        <display:column title="Valor do<br />Recebimento (R$)" >
		        	<fmt:formatNumber value="${recebimento.valorRecebimento}" minFractionDigits="2" type="currency"/>
		        </display:column>
		        <display:column title="Valor <br />Excedente (R$)" >
		        	<fmt:formatNumber value="${recebimento.valorExcedente}" minFractionDigits="2" type="currency"/>
		        </display:column>
		        <display:column title="Situa��o" >
		            <c:out value="${recebimento.recebimentoSituacao.descricao}"/>
		        </display:column>
		    </display:table>
		</fieldset>
	</c:if>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAlteracaoDevolucao">
    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>

</form> 