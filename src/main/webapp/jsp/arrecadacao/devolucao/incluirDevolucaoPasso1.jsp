
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Incluir Devolu��es Financeiras<a class="linkHelp" href="<help:help>/devoluofinanceirainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para incluir a devolu��o financeira selecione um cliente clicando em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span>, ou um ponto de consumo atrav�s do <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span></p>

<script type="text/javascript">
	
	$(document).ready(function(){

		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};
	
	function desabilitarPesquisaOposta(elem){
		
		document.getElementById('botaoPesquisar').disabled = false;
		
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
		}	
	}
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCliente').value = "";
		document.getElementById('documentoCliente').value = "";
		document.getElementById('enderecoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		limparCamposCondominio();
	}
	
	function limparCamposCondominio() {
		var idImovel = document.getElementById("idImovel").value;
		if(!(idImovel && idImovel > 0)) {
			document.getElementById('condominio').value = "";
			document.getElementById('indicadorCondominioImovelTexto1').checked = false;
			document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		}
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaDevolucao"/>';
	}

	function pesquisar() {
		
		if (document.getElementById("indicadorPesquisaImovel").checked) {
			submeter('devolucaoForm', 'pesquisarPontosConsumoDevolucaoTelaInclusao');
		} else {
			submeter('devolucaoForm', 'pesquisarDebitos');
		}
	}

	function limparFormularioPesquisa() {
		
		document.forms[0].idTipoDevolucao.value = "-1";

		document.getElementById('indicadorPesquisaCliente').checked = false;
		document.getElementById('indicadorPesquisaImovel').checked = false;

		pesquisarImovel(false);
		pesquisarCliente(false);
		limparCamposPesquisa();
	}

	function pesquisarDevolucoesPorPontoConsumo(idPonto) {
		
		document.getElementById('idCliente').value = "";
		document.getElementById('idPontoConsumo').value = idPonto;
		submeter('devolucaoForm', 'pesquisarDebitos');
	}

	function init() {
		limparCamposCondominio();
		<c:choose>
			<c:when test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaCliente();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
	}	
	
	addLoadEvent(init);	

</script>	

<form method="post" action="incluirDevolucao" name="devolucaoForm" id="devolucaoForm">
	<token:token></token:token>
	
	<input type="hidden" name="acao" id="acao" value="pesquisarDevolucoes"/>
	<input type="hidden" name="fluxoPesquisa" id="fluxoPesquisa" value="true">
	<input type="hidden" name="idPontoConsumo" id="idPontoConsumo">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria">
	<input type="hidden" name="chavePrimarias" id="chavePrimarias">
	
	<fieldset id="incluirDevolucoesPasso1" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente" />
				<jsp:param name="idCliente" value="${clientePopupVO.idCliente}" />
				<jsp:param name="idCampoNomeCliente" value="nomeCliente" />
				<jsp:param name="nomeCliente" value="${clientePopupVO.nomeCliente}" />
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente" />
				<jsp:param name="documentoFormatadoCliente"
					value="${clientePopupVO.documentoCliente}" />
				<jsp:param name="idCampoEmail" value="emailCliente" />
				<jsp:param name="emailCliente"
					value="${clientePopupVO.emailCliente}" />
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente" />
				<jsp:param name="enderecoFormatadoCliente"
					value="${clientePopupVO.enderecoCliente}" />
				<jsp:param name="possuiRadio" value="true" />
			</jsp:include>
		</fieldset>
	
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovelPopupVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovelPopupVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovelPopupVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovelPopupVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovelPopupVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovelPopupVO.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovelPopupVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovelPopupVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovelPopupVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovelPopupVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovelPopupVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovelPopupVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarDebitos">		
				<input class="bottonRightCol2" id="botaoPesquisar" type="button" value="Pesquisar" onclick="pesquisar();" disabled="disabled">
			</vacess:vacess>		
			<input class="bottonRightCol bottonRightColUltimo" type="button" value="Limpar"  onclick="limparFormularioPesquisa();">
		</fieldset>
	</fieldset>	
	
	<c:if test="${listaContratoPontoConsumos ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaContratoPontoConsumos" sort="list" id="contratoPontoConsumo" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontosConsumo">
		     <display:column sortable="true" title="Contrato" style="width: 120px">
				<a href='javascript:pesquisarDevolucoesPorPontoConsumo(<c:out value='${contratoPontoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contratoPontoConsumo.contrato.numero}'/> 
	        	</a>
	    	 </display:column>
		     <display:column sortable="true" title="Im�vel - Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left">
		     	<a href='javascript:pesquisarDevolucoesPorPontoConsumo(<c:out value='${contratoPontoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contratoPontoConsumo.pontoConsumo.imovel.nome}'/><c:out value=" - "/><c:out value="${contratoPontoConsumo.pontoConsumo.descricao}"/>
				</a>
		     </display:column>
		 	<display:column sortable="false" title="Situa��o" style="width: 100px">
		 		<a href='javascript:pesquisarDevolucoesPorPontoConsumo(<c:out value='${contratoPontoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:choose>
						<c:when test="${contratoPontoConsumo.pontoConsumo.habilitado eq true}">
							<c:out value='Ativo'/>
						</c:when>
						<c:otherwise>
							<c:out value='Inativo'/>
						</c:otherwise>
					</c:choose>
				</a>
	    	</display:column>
	    	<display:column sortable="false" title="Segmento" style="width: 100px">
	    		<a href='javascript:pesquisarDevolucoesPorPontoConsumo(<c:out value='${contratoPontoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${contratoPontoConsumo.pontoConsumo.segmento.descricao}"/>
				</a>
	    	</display:column>
	    	<display:column sortable="false" title="Ramo de Atua��o" style="width: 150px">
	    		<a href='javascript:pesquisarDevolucoesPorPontoConsumo(<c:out value='${contratoPontoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${contratoPontoConsumo.pontoConsumo.ramoAtividade.descricao}"/>
				</a>
	    	</display:column>
		</display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<input class="bottonRightCol2" type="button" value="Cancelar" onclick="cancelar();">
	</fieldset>
	
</form> 