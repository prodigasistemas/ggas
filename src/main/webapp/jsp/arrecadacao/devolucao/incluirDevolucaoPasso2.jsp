<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<h1 class="tituloInterno">Incluir Devolu��es Financeiras<a class="linkHelp" href="<help:help>/devoluofinanceirainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para incluir a devolu��o financeira selecione um cliente clicando em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span>, ou um ponto de consumo atrav�s do <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span></p>

<script type="text/javascript">

	$(document).ready(function(){
	
		$('#todosFatura').click(function(){
			var checked = $('#todosFatura').is(':checked');
			var checkFatura = document.getElementsByName('chavesFatura');
			for (var i = 0; i < checkFatura.length; i++) {
				checkFatura[i].checked = checked;
			}
		});
		$('#todosDebito').click(function(){
			var checked = $('#todosDebito').is(':checked');
			var checkDebito = document.getElementsByName('chavesCreditoDebito');
			for (var i = 0; i < checkDebito.length; i++) {
				checkDebito[i].checked = checked;
			}
		});
		
		// Dialog			
		$("#pontoConsumoPopup").dialog({
			autoOpen: false,			
			width: 370,
			modal: true,
			minHeight: 90,
			resizable: false
		});
	});

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaDevolucao"/>';
	}

	function atualizarValorDevolucao(obj, paramValor) {

		var inputValorDevolucao = document.getElementById('valorDevolucao');
		
		if (inputValorDevolucao.value == '') {
			inputValorDevolucao.value = 0;
		}

		var valorDevolucao = parseFloat(inputValorDevolucao.value);

		var valor = 0;
		if (paramValor != '') {
			valor = parseFloat(paramValor);
		}
		
		if (obj.checked) {
			valorDevolucao = valorDevolucao + valor;
		} else {
			valorDevolucao = valorDevolucao - valor;
		}
		var btIncluirDevolucao = document.getElementById('btIncluirDevolucao');
		if (valorDevolucao == 0) {
			btIncluirDevolucao.disabled = true;
		} else {
			btIncluirDevolucao.disabled = false;
		}
		
		inputValorDevolucao.value = valorDevolucao.toFixed(2);
		var temp = valorDevolucao.toFixed(2).replace(".",",");
		if (valorDevolucao != "0"){
			document.getElementById('valorDevolucaoAux').innerHTML = "R$ " + temp;
		} else {
			document.getElementById('valorDevolucaoAux').innerHTML = "";
		}
	}

	function incluirDevolucao() {
		document.getElementById('btIncluirDevolucao').disabled = true;
		submeter('devolucaoForm', 'incluirDevolucao');
	}

	function verificarSelecaoFaturas() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.chavesFatura != undefined) {
			var total = form.chavesFatura.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form.chavesFatura[i].checked == true){
						flag++;
					}
				}
			} else {
				if(form.chavesFatura.checked == true){
					flag++;
				}
			}
		
			if (flag <= 0) {
				return false;
			}
			
		} else {
			return false;
		}
		
		return true;
	}
	
	function verificarSelecaoCreditoDebitos() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.chavesCreditoDebito != undefined) {
			var total = form.chavesCreditoDebito.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form.chavesCreditoDebito[i].checked == true){
						flag++;
					}
				}
			} else {
				if(form.chavesCreditoDebito.checked == true){
					flag++;
				}
			}
		
			if (flag <= 0) {
				alert ("Selecione um ou mais registros para realizar a opera��o!");
				return false;
			}
			
		} else {
			return false;
		}
		
		return true;
	}

	function exibirDadosPontoConsumo(idPontoConsumo) {		
		var descricao = '';
		var endereco = '';
		var cep = '';
		var complemento = '';
		
		if(idPontoConsumo != ''){
	       	AjaxService.obterPontoConsumoPorChave( idPontoConsumo, { 
	           	callback: function(pontoConsumo) {
					descricao = pontoConsumo['descricao'];
					endereco = pontoConsumo['endereco'];
					cep = pontoConsumo['cep'];
					complemento = pontoConsumo['complemento'];
	       	  }
	       	 , async: false}
	       	);

		document.getElementById('descricaoPopup').innerHTML = descricao;
		document.getElementById('enderecoPopup').innerHTML = endereco;
		document.getElementById('cepPopup').innerHTML = cep;
		document.getElementById('complementoPopup').innerHTML = complemento;

		} else {
			document.getElementById('descricaoPopup').innerHTML = '';
			document.getElementById('enderecoPopup').innerHTML = '';
			document.getElementById('cepPopup').innerHTML = '';
			document.getElementById('complementoPopup').innerHTML = '';
		}
			
		exibirJDialog("#pontoConsumoPopup");
	}

	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=0');

	function manterDadosCheckBox(valor){

		<c:forEach items="${devolucaoVO.chavesFatura}" var="fatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>').checked = valor;
			}	
		</c:forEach>

		<c:forEach items="${devolucaoVO.chavesRecebimento}" var="recebimento">
			if(document.getElementById('chaveRecebimento'+'<c:out value="${recebimento}"/>') != undefined){
				document.getElementById('chaveRecebimento'+'<c:out value="${recebimento}"/>').checked = valor;
			}	
		</c:forEach>

		<c:forEach items="${devolucaoVO.chavesCreditoDebito}" var="creditoDebito">
			if(document.getElementById('chaveCreditoDebito'+'<c:out value="${creditoDebito}"/>') != undefined){
				document.getElementById('chaveCreditoDebito'+'<c:out value="${creditoDebito}"/>').checked = valor;
			}	
		</c:forEach>

	}

	function init(){

		var chavesFatura = "${devolucaoVO.chavesFatura}";
		var chavesRecebimento = "${devolucaoVO.chavesRecebimento}";
		var chavesCreditoDebito = "${devolucaoVO.chavesCreditoDebito}";

		if(chavesFatura != null || chavesRecebimento != null || chavesCreditoDebito != null){
			document.getElementById('btIncluirDevolucao').disabled = false;
		}
			
		manterDadosCheckBox(true);
	}
	
	addLoadEvent(init);
	
</script>	

<form method="post" action="incluirDevolucao" id="devolucaoForm" name="devolucaoForm">
	<token:token></token:token>
	<input name="acao" type="hidden" id="acao" value="incluirDevolucao"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${devolucaoVO.idPontoConsumo}">
	<input name="idCliente" type="hidden" id="idCliente" value="${clientePopupVO.idCliente}">
	
	
	<div id="pontoConsumoPopup" title="Ponto de Consumo">
		<label class="rotulo">Descri��o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="descricaoPopup"></span><br />
		<label class="rotulo">Endere�o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="enderecoPopup"></span><br />
		<label class="rotulo">Cep:</label>
		<span class="itemDetalhamento" id="cepPopup"></span><br />
		<label class="rotulo">Complemento:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="complementoPopup"></span><br /><br />
	</div>
	
	<fieldset id="incluirDevolucoesPasso2" class="conteinerPesquisarIncluirAlterar">
		<c:choose>
			<c:when test="${clientePopupVO.idCliente ne null && clientePopupVO.idCliente > 0}">
				<label class="rotulo">Nome do Cliente:</label>
				<span class="itemDetalhamento"><c:out value="${devolucaoVO.nomeCompletoCliente}"/></span>
			</c:when>
			<c:otherwise>
				<a id="linkDadosPontoConsumo" class="linkPesquisaAvancada" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
					<fieldset class="coluna">
						<label class="rotulo">Descricao:</label>
						<span class="itemDetalhamento"><c:out value="${devolucaoVO.descricaoPontoConsumo}"/></span><br />
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento"><c:out value="${devolucaoVO.enderecoPontoConsumo}"/></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">CEP:</label>
						<span class="itemDetalhamento"><c:out value="${devolucaoVO.cepPontoConsumo}"/></span><br />
						<label class="rotulo">Complemento:</label>
						<span class="itemDetalhamento"><c:out value="${devolucaoVO.complementoPontoConsumo}"/></span><br />
					</fieldset>
				</fieldset>
			</c:otherwise>
		</c:choose>
		<hr class="linhaSeparadora" />
		<c:if test="${listaFatura ne null}">
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Notas de Cr�dito:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaFatura" sort="list" id="fatura" pagesize="15" excludedParams="" requestURI="#">
				   	<display:column style="text-align: center; width: 25px" sortable="false" >
				   		<input type="checkbox" name="chavesFatura" value="${fatura.chavePrimaria}" id="chaveFatura${fatura.chavePrimaria}" onclick="javascript:atualizarValorDevolucao(this, '<c:out value="${fatura.valorTotal}"/>')">
				   	</display:column>
					<display:column property="chavePrimaria" title="N� do Documento" style="width: 130px"/>
					<display:column property="tipoDocumento.descricao" title="Tipo" />
			   		<display:column sortable="false" title="Data de<br />Emiss�o" style="width: 75px">
			   			<fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy" />
			   		</display:column>
			   		<display:column sortable="false" title="Vencimento" style="width: 75px">
			   			<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy" />
			   		</display:column>
			   		<display:column sortable="false" title="Valor (R$)" style="width: 130px" >
			   			<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2" type="currency"/>
			   		</display:column>
			   		<c:if test="${clientePopupVO.idCliente ne null && clientePopupVO.idCliente > 0}">
				   		<display:column title="Ponto de<br />Consumo" style="width: 60px">
							<a href="javascript:exibirDadosPontoConsumo('<c:out value="${fatura.pontoConsumo.chavePrimaria}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
						</display:column>	
					</c:if>   		
			   	</display:table>
			</fieldset>
			<hr class="linhaSeparadoraDetalhamento" />
		</c:if>
		<c:if test="${listaCreditoDebito ne null}">
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Cr�ditos a Realizar:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" id="creditoDebito" pagesize="15" excludedParams="" requestURI="#" >
				   	<display:column style="text-align: center; width: 25px" sortable="false">
				   		<input type="checkbox" name="chavesCreditoDebito" id="chaveCreditoDebito${creditoDebito.chavePrimaria}" value="${creditoDebito.chavePrimaria}" onclick="javascript:atualizarValorDevolucao(this, '<c:out value="${creditoDebito.valor}"/>')">
				   	</display:column>
				   	<display:column property="chavePrimaria" title="N� do Cr�dito" style="width: 130px"/>
				   	
				   	<display:column property="creditoDebitoNegociado.rubrica.descricao" sortable="false" title="R�brica" style="width: 75px"/>
				   	
				   	<display:column property="creditoDebitoNegociado.dataIncicioCobrancaFormatada" sortable="false" title="In�cio da Cobran�a" style="width: 75px"/>
				   	
			   		<display:column sortable="false" sortProperty="getDataVencimentoFormatada"  title="Vencimento" style="width: 75px">
			   			<c:out value="${creditoDebito.dataVencimentoFormatada}"/>
			   		</display:column>
			   		<display:column title="Valor (R$)" style="width: 130px" >
			   		<c:out value="${creditoDebito.valor}"/>
			   		</display:column>
			   		<display:column property="creditoDebitoSituacao.descricao" title="Situa��o do Cr�dito" style="width: 150px"/>
				   		<c:if test="${clientePopupVO.idCliente ne null && clientePopupVO.idCliente > 0}">
				   			<display:column title="Ponto de<br />Consumo" style="width: 60px">
							<a href="javascript:exibirDadosPontoConsumo('<c:out value="${creditoDebitoNegociado.pontoConsumo.chavePrimaria}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
							</display:column>
				   		</c:if>
			   	</display:table>
			</fieldset>
			
			<hr class="linhaSeparadora" />
			
		</c:if> 
		<c:if test="${listaRecebimento ne null}"> 	
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Recebimentos:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaRecebimento" sort="list" id="recebimento" pagesize="15" excludedParams="" requestURI="#">
			        <display:column style="text-align: center;" sortable="false">
			         	<input type="checkbox" name="chavesRecebimento" id="chaveRecebimento${recebimento.chavePrimaria}" value="${recebimento.chavePrimaria}" onclick="javascript:atualizarValorDevolucao(this, '<c:out value="${recebimento.valorExcedente}"/>')">
			        </display:column>
			        <display:column title="Cliente" >
			            <c:out value="${recebimento.cliente.nome}"/>
			        </display:column>
			         <display:column title="Documento" >
			        	<c:choose>
				        	<c:when test="${recebimento.documentoCobrancaItem != null}">
							    <c:out value="${recebimento.documentoCobrancaItem.chavePrimaria}"/>
							</c:when>
							<c:when test="${recebimento.faturaGeral != null}">
							    <c:out value="${recebimento.faturaGeral.chavePrimaria}"/>
							</c:when>
							<c:otherwise>
								--
							</c:otherwise>	
						</c:choose>
			        </display:column>
			        <display:column title="Data do<br />Recebimento" style="width: 75px">
			   			<fmt:formatDate value="${recebimento.dataRecebimento}" pattern="dd/MM/yyyy" />
			   		</display:column>
			   		<display:column title="Valor do<br />Documento (R$)" >
			        	<c:choose>
				            <c:when test="${recebimento.documentoCobrancaItem != null}">
				            	<fmt:formatNumber value="${recebimento.documentoCobrancaItem.valor}" minFractionDigits="2" type="currency"/>
							</c:when>
							<c:when test="${recebimento.faturaGeral != null}">
								<fmt:formatNumber value="${recebimento.faturaGeral.faturaAtual.valorTotal}" minFractionDigits="2" type="currency"/>
							</c:when>
							<c:otherwise>
								--
							</c:otherwise>
						</c:choose>
			        </display:column>
			        <display:column title="Valor do<br />Recebimento (R$)" >
						<fmt:formatNumber value="${recebimento.valorRecebimento}" minFractionDigits="2" type="currency"/>
			        </display:column>
			        <display:column title="Valor <br />Excedente (R$)" >
			            <fmt:formatNumber value="${recebimento.valorExcedente}" minFractionDigits="2" type="currency"/>
			        </display:column>
			        <display:column title="Situa��o" >
			            <c:out value="${recebimento.recebimentoSituacao.descricao}"/>
			        </display:column>
			    </display:table>
			    <hr class="linhaSeparadoraDetalhamento" />
			</fieldset>
		</c:if>
		
		<fieldset id="dadosDevolu��o" class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Dados da Devolu��o:</legend>
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo campoObrigatorio" for="valorDevolucao"><span class="campoObrigatorioSimbolo">* </span>Valor:</label>
				<input type="text" class="campoTexto" id="valorDevolucao" name="valorDevolucao" value="${devolucaoVO.valorDevolucao}">
				<span class="itemDetalhamento" id="valorDevolucaoAux"><c:if test="${not empty (devolucaoVO.valorDevolucao)}">R$ </c:if><c:out value="${devolucaoVO.valorDevolucao}"/></span><br class="quebraLinha" />
				
				<label class="rotulo campoObrigatorio" for="tipoDocumentoGerado"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
				<select name="idTipoDevolucao" id="idTipoDevolucao" class="campoSelect">
			    	<option value="-1">Selecione</option>
			    	<c:forEach items="${listaTipoDevolucao}" var="tipoDevolucao">
						<option value="<c:out value="${tipoDevolucao.chavePrimaria}"/>" <c:if test="${devolucaoVO.idTipoDevolucao == tipoDevolucao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoDevolucao.descricao}"/>
						</option>		
				    </c:forEach>
			    </select>
			</fieldset>
			
			<fieldset class="colunaFinal">
				<label class="rotulo" for="observacao">Observa��o:</label>
				<textarea id="observacaoDevolucao" name="observacaoDevolucao">${devolucaoVO.observacaoDevolucao}</textarea>
			</fieldset>
		</fieldset>
	</fieldset>	
	
	<fieldset id="conteinerBotoesPesquisarDirPesquisarExtratoDebito" class="conteinerBotoes">
		<input class="bottonRightCol2" type="button" value="Cancelar" onclick="cancelar();">
		<vacess:vacess param="incluirDevolucao">
			<input class="bottonRightCol2 botaoGrande1" type="button" value="Incluir" id="btIncluirDevolucao" onclick="incluirDevolucao();" disabled="disabled">
		</vacess:vacess>
	</fieldset>
</form> 
