<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script type="text/javascript" src="<c:url value="/js/selectbox.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.filestyle.mini.js"/>"></script>



<h1 class="tituloInterno">Alterar Banco<a href="<help:help>/bancoalterarincluir.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar um Banco, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form:form method="post" action="alterarBanco" id="bancoForm" name="bancoForm"  modelAttribute="BancoImpl" enctype="multipart/form-data"  >

<script>

	$().ready(function(){
		$("input#nome").keyup(removeExtraEspacoInicial).blur(removeExtraEspacoInicial);
		$("input#nomeAbreviado").keyup(removeExtraManterEspaco, removeExtraEspacoInicial).blur(removeExtraManterEspaco, removeExtraEspacoInicial);
		$("input#codigoBanco").keyup(apenasNumeros);
	});
	
	function removerLogoTipo(chave) {
		document.forms[0].chavePrimaria.value = chave;
		document.forms[0].removaLogo.value = true;
		submeter('bancoForm', 'exibirAlteracaoBanco');
		}
	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisarBanco"/>';
	}
	
	function alterar(){
		submeter('bancoForm', 'alterarBanco');
	}
	
	function limpar(form){
		limparFormularios(form);
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="alterarBanco">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${banco.chavePrimaria}">
<input name="removaLogo" type="hidden" id="removaLogo" value="null">
<input name="versao" type="hidden" id="versao" value="${banco.versao}"> 

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisarBancoCol2" class="colunaEsq">
		<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" ><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input type="text" class="campoTexto" name="nome" id="nome" maxlength="40" size="40"
			onkeyup="return letraMaiuscula(this);" onkeypress="return letraMaiuscula(this);" 
			value="${banco.nome}" style="width: 280px;"/><br />	
		<label class="rotulo campoObrigatorio" id="rotuloCodigoBanco"for="codigoBanco"><span class="campoObrigatorioSimbolo">* </span>C�digo do Banco:</label>
		<input type="text" class="campoTexto" id="codigoBanco" name=codigoBanco maxlength="10" size="10"
			onkeyup="return validarCampoInteiroCopiarColar(this);" value="${banco.codigoBanco}" /><br />			
		<label class="rotulo" id="rotuloNomeAbreviado"for="nomeAbreviado">Nome Abreviado:</label>
		<input type="text" class="campoTexto" name="nomeAbreviado" id="nomeAbreviado" maxlength="10" size="10"
			onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" value="${banco.nomeAbreviado}" /><br />
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${banco.habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${banco.habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Inativo</label>
	</fieldset>
	
	<fieldset id="pesquisarBanco" class="colunaFinal">
		<label class="rotulo" id="rotuloLogo" for="logoBanco">Logotipo:</label>
		<input class="campoFile" type="file" id="logoBanco" name="logoBanco" title="Procurar"  value=""/>
		<input type="button" value="Limpar Arquivo" class="bottonRightCol" id="limparArquivo">
		<c:if test="${not empty banco.logoBanco}">
			<img id="imagemLogoAlterar" class="imagemGgas"
					src="<c:url value="/exibirLogoMarcaBanco/${banco.chavePrimaria} "/>" 
			/>
			<fieldset class="botaoRemoverLogo">				
				<input type="button" value="Remover Logotipo" class="bottonRightCol2" id="removerLogotipoButton" onclick="removerLogoTipo(<c:out value='${banco.chavePrimaria}'/>);">
			</fieldset>			
		</c:if>	
		
	</fieldset>
	
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Banco.</p>
	
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limpar(this.form);">
<%--     <vacess:vacess param="alterarBanco.do"> --%>
    	<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="alterar();">
<%--     </vacess:vacess> --%>
</fieldset>
</form:form>
