<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<%--   <%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>  --%>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>

<h1 class="tituloInterno">Incluir Banco<a href="<help:help>/bancoinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form:form method="post" action="incluirBanco" id="bancoForm" name="bancoForm" modelAttribute="BancoImpl"
			 enctype="multipart/form-data" >

<script>

	$().ready(function(){
		$("input#nome").keyup(removeExtraEspacoInicial).blur(removeExtraEspacoInicial);
		$("input#nomeAbreviado").keyup(removeExtraManterEspaco, removeExtraEspacoInicial).blur(removeExtraManterEspaco, removeExtraEspacoInicial);
		$("input#codigoBanco").keyup(apenasNumeros);
		
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
		
	});
	function voltar() {
		location.href = '<c:url value="/exibirPesquisarBanco"/>';
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="incluirBanco">
<input name="indicador" type="hidden" id="indicador" value="${habilitado}">

<fieldset class="conteinerPesquisarIncluirAlterar">	
	<fieldset id="pesquisarBancoCol2" class="colunaEsq">		
		<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome" >
			<span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input type="text" class="campoTexto" name="nome" id="nome" maxlength="40" size="40"
			onkeyup="return letraMaiuscula(this);" onkeypress="return letraMaiuscula(this);" 
			value="${banco.nome}" style="width: 280px;"/><br />		
		<label class="rotulo campoObrigatorio" id="rotuloCodigoBanco"for="codigoBanco">
		<span class="campoObrigatorioSimbolo">* </span>C�digo do Banco:</label>
		<input type="text" class="campoTexto" id="codigoBanco" name=codigoBanco maxlength="10" size="10"
			onkeyup="return validarCampoInteiroCopiarColar(this);" value="${banco.codigoBanco}" /><br />			
		<label class="rotulo" id="rotuloNomeAbreviado"for="nomeAbreviado">Nome Abreviado:</label>
		<input type="text" class="campoTexto" name="nomeAbreviado" id="nomeAbreviado" maxlength="10" size="10"
			onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" value="${banco.nomeAbreviado}" /><br />
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Inativo</label>
	</fieldset>
	
	<fieldset id="pesquisarBanco" class="colunaFinal">
		<label class="rotulo" id="rotuloLogo" for="logoBanco">Logotipo:</label>
		<input class="campoFile" type="file" id="logoBanco" name="logoBanco" title="Procurar" value="${banco.logoBanco}"/>
		<input type="button" value="Limpar Arquivo" class="bottonRightCol" id="limparArquivo">		
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Banco.</p>
</fieldset>

<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="voltar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limparFormularios(this.form);">
	<vacess:vacess param="incluirBanco">
    	<input id="buttonIncluir" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Incluir" type="submit">
	</vacess:vacess>
</fieldset>
</form:form>
