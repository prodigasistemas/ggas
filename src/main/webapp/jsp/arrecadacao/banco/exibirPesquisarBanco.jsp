<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%-- <%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %> --%>

<h1 class="tituloInterno">Pesquisar Banco<a href="<help:help>/consultadasbanco.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarBanco" id="bancoForm" name="bancoForm" modelAttribute="bancoImpl" enctype="multipart/form-data">
	
	<script>
	
	$().ready(function(){
		$("input#nome").keyup(removeExtraEspacoInicial).blur(removeExtraEspacoInicial);
		$("input#nomeAbreviado").keyup(removeExtraManterEspaco, removeExtraEspacoInicial).blur(removeExtraManterEspaco, removeExtraEspacoInicial);
		$("input#codigoBanco").keyup(apenasNumeros).blur(apenasNumeros);
		
		if($("#indicador").val() ==""){
			document.forms[0].habilitado[0].checked = true;
		}
	});
	
	
	function removerBanco(){
		
		var selecao = verificarSelecao();
		if (selecao == true) {	
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('bancoForm', 'removerBanco');
			}
	    }
	}
	
	function alterarBanco(){
		
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("bancoForm", "exibirAlteracaoBanco");
	    }
	}
	
	function detalharBanco(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("bancoForm", "exibirDetalhamentoBanco");
	}
	
	function incluirBanco() {
			location.href = '<c:url value="/exibirInclusaoBanco"/>';
	}
	
	function limparFormulario(form){
		limparFormularios(form);
		document.forms[0].habilitado[0].checked = true;
	}
	
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarBanco">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	<fieldset id="pesquisarBanco" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" id="rotuloNome"for="nome">Nome:</label>
			<input type="text" class="campoTexto" name="nome" id="nome" maxlength="40" size="40"
				onkeyup="return letraMaiuscula(this);" onkeypress="return letraMaiuscula(this);" value="${banco.nome}" /><br />
		
			<label class="rotulo" id="rotuloCodigoBanco"for="codigoBanco">C�digo do Banco:</label>
			<input type="text" class="campoTexto" id="codigoBanco" name="codigoBanco" maxlength="10" size="10" value="${banco.codigoBanco}"><br />				
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo" id="rotuloNomeAbreviado"for="nomeAbreviado">Nome Abreviado:</label>
			<input type="text" class="campoTexto" name="nomeAbreviado" id="nomeAbreviado" maxlength="10" size="10"
				onkeyup="return letraMaiuscula(this);" onkeypress="return letraMaiuscula(this);" value="${banco.nomeAbreviado}" /><br />
		
			<label class="rotulo">Indicador de Uso:</label>
		    <input class="campoRadio" type="radio" name="habilitado" id="indicadorUsoAtivo" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicadorUsoInativo" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoInativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
		<!-- 	<vacess:vacess param="pesquisarSegmentos.do">  -->
		    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
		<!--   </vacess:vacess>	 -->	
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario(this.form);">		
		</fieldset>
	</fieldset>
		
		
	</fieldset>
	
	<c:if test="${listaBanco ne null}">
		<hr class="linhaSeparadoraPesquisa" />
	    <display:table class="dataTableGGAS" name="listaBanco" sort="list" id="banco"
	    decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" 
	     requestURI="pesquisarBanco">    
	      	<display:column style="text-align: center; width: 25px" sortable="false" title="
	      	<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="chavesPrimarias" value="${banco.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${banco.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        <display:column sortable="true" title="Nome">
	        	<a href="javascript:detalharBanco(<c:out value='${banco.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${banco.nome}"/>
	            </a>
	            
	        </display:column>
			<display:column title="Nome Abreviado"  style="width: 150px">
	        	<a href="javascript:detalharBanco(<c:out value='${banco.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	        		<c:out value="${banco.nomeAbreviado}"/>
	            </a>
	        </display:column>
	        <display:column title="Codigo do Banco"  style="width: 150px">
	        	<a href="javascript:detalharBanco(<c:out value='${banco.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	        		<c:out value="${banco.codigoBanco}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty banco}">
		 	<vacess:vacess param="exibirAlteracaoBanco"> 
				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2  botaoAlterar" onclick="alterarBanco()" type="button">
			</vacess:vacess> 
			<vacess:vacess param="removerBanco"> 
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerBanco()" type="button">
			</vacess:vacess> 
		</c:if>
		<vacess:vacess param="exibirInclusaoBanco"> 
			<input id="buttonIncluir" name="button" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Incluir" onclick="incluirBanco()" type="button">
		</vacess:vacess>
	</fieldset>
</form:form>
