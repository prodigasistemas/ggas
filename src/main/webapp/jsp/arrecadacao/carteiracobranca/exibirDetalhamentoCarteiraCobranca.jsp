<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Carteira de Cobran�a</h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script type="text/javascript">

function voltar() {	
	submeter("carteiraCobrancaForm", "pesquisarCarteiraCobranca");
}

</script>

<form method="post" action="exibirAlteracaoCarteiraCobranca" id="carteiraCobrancaForm" name="carteiraCobrancaForm" modelAttribute="CarteiraCobrancaVO">

	<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${carteiraCobranca.chavePrimaria}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaServicoTipoCol1" class="coluna">
		
			<label class="rotulo">Descri��o:</label>
			<span class="itemDetalhamento"><c:out value="${carteiraCobranca.descricao}"/></span><br />		
			
			<label class="rotulo">N�mero:</label>
			<span class="itemDetalhamento"><c:out value="${carteiraCobranca.numero}"/></span><br />		
			
			<label class="rotulo">Arrecadador:</label>
			<span class="itemDetalhamento"><c:out value="${carteiraCobranca.arrecadador.banco.codigoBanco} - ${carteiraCobranca.arrecadador.banco.nome}"/></span><br />		
			
			<label class="rotulo">C�digo Carteira:</label>
			<span class="itemDetalhamento"><c:out value="${carteiraCobranca.codigoCarteira.descricao}"/></span><br />
			
			<label class="rotulo">Tipo Carteira:</label>
			<span class="itemDetalhamento"><c:out value="${carteiraCobranca.tipoCarteira.descricao}"/></span><br />
						
		</fieldset>
		
		<fieldset id="pesquisaServicoTipoCol2" class="colunaFinal">			
		
			<label class="rotulo">Faixa para Nosso N�mero:</label>
			<c:choose>
			<c:when test="${carteiraCobranca.indicadorFaixaNossoNumero == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${carteiraCobranca.indicadorFaixaNossoNumero == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
			
			<label class="rotulo">Nosso N�mero Livre para Empresa:</label>
			<c:choose>
			<c:when test="${carteiraCobranca.indicadorNossoNumeroLivre == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${carteiraCobranca.indicadorNossoNumeroLivre == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
			
			<label class="rotulo">Banco deve emitir boleto:</label>
			<c:choose>
			<c:when test="${carteiraCobranca.indicadorEmissao == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
				<label class="rotulo">Layout da Fatura Impressa:</label>
				<span class="itemDetalhamento"><c:out value="${carteiraCobranca.arquivoLayoutFatura}"/></span><br />
			</c:when>
			<c:when test="${carteiraCobranca.indicadorEmissao == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
			
			<label class="rotulo">Permite protesto:</label>
			<c:choose>
			<c:when test="${carteiraCobranca.indicadorProtesto == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${carteiraCobranca.indicadorProtesto == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
		
			<label class="rotulo">Realiza a entrega do documento:</label>
			<c:choose>
			<c:when test="${carteiraCobranca.indicadorEntrega == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${carteiraCobranca.indicadorEntrega == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>
			
			<label class="rotulo">Indicador de Uso:</label>
			<c:choose>
			<c:when test="${carteiraCobranca.habilitado == 'true'}">
				<span class="itemDetalhamento"><c:out value="Ativo"/></span><br />
			</c:when>
			<c:when test="${carteiraCobranca.habilitado == 'false'}">
				<span class="itemDetalhamento"><c:out value="Inativo"/></span><br />
			</c:when>
			</c:choose>
			
		</fieldset>						
	</fieldset>
					
	</fieldset>
	
<fieldset class="conteinerBotoes"> 		
	<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onClick="voltar();">	
	
	<vacess:vacess param="exibirAlteracaoCarteiraCobranca">
		<input id="buttonAlterar" name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="submit">
	</vacess:vacess>
		
</fieldset>
</form>
