<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Pesquisar Carteira de Cobran�a<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">

$(document).ready(function(){
	$("input[type=text]").attr("onpaste","return false;");
	$("input#descricao").keyup(removeExtraEspacoInicial).blur(removeExtraEspacoInicial);
	$("input#numero").keyup(apenasNumeros).blur(apenasNumeros);
});



function pesquisar() {
	submeter('carteiraCobrancaForm','pesquisarCarteiraCobranca');
}

function alterarCarteiraCobranca() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms["carteiraCobrancaForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('carteiraCobrancaForm','exibirAlteracaoCarteiraCobranca');
    }
	
}

function detalharCarteiraCobranca(chave){
	document.forms["carteiraCobrancaForm"].chavePrimaria.value = chave;
	submeter('carteiraCobrancaForm','exibirDetalhamentoCarteiraCobranca');
}

function limparFormulario(){
	limparFormularios(document.carteiraCobrancaForm);		
	
	document.forms["carteiraCobrancaForm"].indicadorFaixaNossoNumero[2].checked = true;
	document.forms['carteiraCobrancaForm'].indicadorEntrega[2].checked = true;
	document.forms['carteiraCobrancaForm'].indicadorNossoNumeroLivre[2].checked = true;
	document.forms['carteiraCobrancaForm'].indicadorEmissao[2].checked = true;
	document.forms['carteiraCobrancaForm'].indicadorProtesto[2].checked = true;
	document.forms['carteiraCobrancaForm'].indicadorEntrega[2].checked = true;
	document.forms['carteiraCobrancaForm'].habilitado[0].checked = true;	
}

function incluir() {
	location.href = '<c:url value="/exibirInclusaoCarteiraCobranca"/>';
}

function removerCarteiraCobranca(){
	
	var selecao = verificarSelecao();	
	
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('carteiraCobrancaForm', 'removerCarteiraCobranca');
		}
    }
}

</script>

<form action="pesquisarCarteiraCobranca" id="carteiraCobrancaForm" name="carteiraCobrancaForm" method="post" modelAttribute="CarteiraCobrancaVO" >
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" > 	
				
		<fieldset id="pesquisaServicoTipoCol1" class="coluna">
		
			<label class="rotulo" id="rotuloDescricao" for="descricao">Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="40" size="40" value="${carteiraCobrancaVO.descricao}" onkeyup="letraMaiuscula(this);"/><br />
		
			<label class="rotulo">N�mero:</label>
			<input class="campoTexto" type="text" id="numero" name="numero" maxlength="3" size="3" onkeypress="return formatarCampoInteiro(event);" value="${carteiraCobrancaVO.numero}">	
				
			<label class="rotulo">Arrecadador:</label>
			<select class="campoSelect" id="chaveArrecadador" name="chaveArrecadador">
				<option value="">Selecione</option>
				<c:forEach items="${listaArrecadador}" var="arrecada">
					<option value="<c:out value="${arrecada.chavePrimaria}"/>" <c:if test="${carteiraCobrancaVO.chaveArrecadador == arrecada.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${arrecada.banco.codigoBanco} - ${arrecada.banco.nome}"/>
					</option>		
		    	</c:forEach>
			</select>	
		
			<label class="rotulo">C�digo Carteira:</label>
			<select class="campoSelect" id="chaveCodigoCarteira" name="chaveCodigoCarteira">
				<option value="">Selecione</option>
				<c:forEach items="${listaCodigoCarteira}" var="carteira">
					<option value="<c:out value="${carteira.chavePrimaria}"/>" <c:if test="${carteiraCobrancaVO.chaveCodigoCarteira == carteira.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${carteira.descricao}"/>
					</option>		
		    	</c:forEach>
			</select>	
			
			<label class="rotulo">Tipo Carteira:</label>
			<select class="campoSelect" id="chaveTipoCarteira" name="chaveTipoCarteira">
				<option value="">Selecione</option>
				<c:forEach items="${listaTipoCarteira}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${carteiraCobrancaVO.chaveTipoCarteira == tipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipo.descricao}"/>
					</option>		
		    	</c:forEach>
			</select>	
					
		</fieldset>
		
		<fieldset id="pesquisaServicoTipoCol2" class="colunaFinalMaterial">
		
			<label class="rotulo">Faixa para Nosso N�mero:</label>
			<input class="campoRadio" type="radio" name="indicadorFaixaNossoNumero" id="indicadorFaixaNossoNumero" value="true" <c:if test="${carteiraCobrancaVO.indicadorFaixaNossoNumero eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorFaixaNossoNumero" id="indicadorFaixaNossoNumero" value="false" <c:if test="${carteiraCobrancaVO.indicadorFaixaNossoNumero eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorFaixaNossoNumero" id="indicadorFaixaNossoNumero" value="" <c:if test="${carteiraCobrancaVO.indicadorFaixaNossoNumero eq null}">checked</c:if>>
			<label class="rotuloRadio">Todos</label>
			
			<label class="rotulo">Nosso N�mero Livre para Empresa:</label>
			<input class="campoRadio" type="radio" name="indicadorNossoNumeroLivre" id="indicadorNossoNumeroLivre" value="true" <c:if test="${carteiraCobrancaVO.indicadorNossoNumeroLivre eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" >Sim</label>
			<input class="campoRadio" type="radio" name="indicadorNossoNumeroLivre" id="indicadorNossoNumeroLivre" value="false" <c:if test="${carteiraCobrancaVO.indicadorNossoNumeroLivre eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" >N�o</label>
			<input class="campoRadio" type="radio" name="indicadorNossoNumeroLivre" id="indicadorNossoNumeroLivre" value="" <c:if test="${carteiraCobrancaVO.indicadorNossoNumeroLivre eq null}">checked</c:if>>
			<label class="rotuloRadio">Todos</label>
			
			<label class="rotulo">Banco deve emitir boleto:</label>
			<input class="campoRadio" type="radio" name="indicadorEmissao" id="indicadorEmissao" value="true" <c:if test="${carteiraCobrancaVO.indicadorEmissao eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorEmissao" id="indicadorEmissao" value="false" <c:if test="${carteiraCobrancaVO.indicadorEmissao eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorEmissao" id="indicadorEmissao" value="" <c:if test="${carteiraCobrancaVO.indicadorEmissao eq null}">checked</c:if>>
			<label class="rotuloRadio">Todos</label>
			
			<label class="rotulo">Permite protesto:</label>
			<input class="campoRadio" type="radio" name="indicadorProtesto" id="indicadorProtesto" value="true" <c:if test="${carteiraCobrancaVO.indicadorProtesto eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorProtesto" id="indicadorProtesto" value="false" <c:if test="${carteiraCobrancaVO.indicadorProtesto eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorProtesto" id="indicadorProtesto" value="" <c:if test="${carteiraCobrancaVO.indicadorProtesto eq null}">checked</c:if>>
			<label class="rotuloRadio">Todos</label>
			
			
			<label class="rotulo">Realiza a entrega do documento:</label>
			<input class="campoRadio" type="radio" name="indicadorEntrega" id="indicadorEntrega" value="true" <c:if test="${carteiraCobrancaVO.indicadorEntrega eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorEntrega" id="indicadorEntrega" value="false" <c:if test="${carteiraCobrancaVO.indicadorEntrega eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorEntrega" id="indicadorEntrega" value="" <c:if test="${carteiraCobrancaVO.indicadorEntrega eq null}">checked</c:if>>
			<label class="rotuloRadio">Todos</label>
						
			<label class="rotulo">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${carteiraCobrancaVO.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" >Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${carteiraCobrancaVO.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" >Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${carteiraCobrancaVO.habilitado eq null}">checked</c:if>>
			<label class="rotuloRadio" >Todos</label>
			
		</br>
		</fieldset>	
		<fieldset class="conteinerBotoesPesquisarDirFixo">
<%-- 			<vacess:vacess param="pesquisarCarteiraCobranca">		 --%>
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" onclick="pesquisar();">
<%-- 	    	</vacess:vacess>		 --%>
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>	
	
	<c:if test="${listaCarteiraCobranca ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaCarteiraCobranca" sort="list" id="carteiraCobranca" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="${carteiraCobranca.chavePrimaria}">
	        </display:column>
	        
	        <display:column title="Ativo" style="width: 10px">
		     	<c:choose>
					<c:when test="${carteiraCobranca.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column sortable="true" sortProperty="descricao" title="Descri��o" style="width: 130px">
	        	<a href="javascript:detalharCarteiraCobranca(<c:out value='${carteiraCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${carteiraCobranca.descricao}"/>
	            </a>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="numero" title="N�mero" style="width: 130px">
	        	<a href="javascript:detalharCarteiraCobranca(<c:out value='${carteiraCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${carteiraCobranca.numero}"/>
	            </a>
	        </display:column>
			
			<display:column  sortable="true" sortProperty="arrecadador.cliente.nome" title="Arrecadador" style="width: 130px">
				<a href="javascript:detalharCarteiraCobranca(<c:out value='${carteiraCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${carteiraCobranca.arrecadador.banco.codigoBanco} - ${carteiraCobranca.arrecadador.banco.nome}"/>
	            </a>
			</display:column>
			
			<display:column sortable="true" sortProperty="codigoCarteira.descricao" title="C�digo Carteira" style="width: 130px">
				<a href="javascript:detalharCarteiraCobranca(<c:out value='${carteiraCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${carteiraCobranca.codigoCarteira.descricao}"/>
	            </a>
			</display:column>			
	        
	        <display:column sortable="true" sortProperty="tipoCarteira.descricao" title="Tipo Carteira" style="width: 130px">
	        	<a href="javascript:detalharCarteiraCobranca(<c:out value='${carteiraCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${carteiraCobranca.tipoCarteira.descricao}"/>
	            </a>
	        </display:column>
	        	        
	    </display:table>	
	</c:if>
	
</form>
		
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaCarteiraCobranca}">
  			<vacess:vacess param="exibirAlteracaoCarteiraCobranca">
  				<input id="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarCarteiraCobranca()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerCarteiraCobranca">
				<input id="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerCarteiraCobranca()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoCarteiraCobranca">
   			<input id="buttonIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>

	