<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<c:if test="${ fluxoInclusao eq true }">
	<h1 class="tituloInterno">Incluir Carteira de Cobran�a<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>
</c:if>

<c:if test="${ fluxoAlteracao eq true }">
	<h1 class="tituloInterno">Alterar Carteira de Cobran�a<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
</c:if>

<script type="text/javascript">

$(document).ready(function(){
	$("input[type=text]").attr("onpaste","return false;");
	$("input#descricao").keyup(removeExtraEspacoInicial).blur(removeExtraEspacoInicial);
	$("input#numero").keyup(apenasNumeros).blur(apenasNumeros);

    var $indicadorEmissaoSim = $('#indicadorEmissaoSim');
    var $indicadorEmissaoNao = $('#indicadorEmissaoNao');
	var $idLayoutFatura = $("#fieldLayoutFatura");

    $indicadorEmissaoSim.change(function () {
		$idLayoutFatura.show();
    });

    $indicadorEmissaoNao.change(function () {
		$idLayoutFatura.hide();
    });

});

function verificarCampoLayout() {
    var $indicadorEmissaoNao = $('#indicadorEmissaoNao')[0];
    if ($indicadorEmissaoNao.checked) {
        $("#layoutFaturaImpressa").val("");
    }
}

function cancelar(){
	location.href = '<c:url value="/exibirPesquisaCarteiraCobranca"/>';
}

function voltar() {	
	submeter("carteiraCobrancaForm", "pesquisarCarteiraCobranca");
}

function limparFormulario(){
	$("#descricao").val("");	
	$("#numero").val("");
	$("#chaveTipoCarteira").val("");
	$("#chaveCodigoCarteira").val("");
	$("#chaveArrecadador").val("");
	$("#layoutFaturaImpressa").val("");

}

function incluirCarteiraCobranca(){
	var fluxoInclusao = $("#fluxoInclusao").val();
	var fluxoAlteracao = $("#fluxoAlteracao").val();

    verificarCampoLayout();

	if(fluxoInclusao != ""){
		submeter('carteiraCobrancaForm', 'incluirCarteiraCobranca');
	}

	if(fluxoAlteracao != ""){
		submeter('carteiraCobrancaForm', 'alterarCarteiraCobranca');
	}	
}

</script>

<form method="post" id="carteiraCobrancaForm" name="carteiraCobrancaForm" modelAttribute="CarteiraCobrancaVO">

	<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${carteiraCobrancaVO.chavePrimaria}">
	<input type="hidden" id="fluxoInclusao" name="fluxoInclusao" value="${fluxoInclusao}"> 	
	<input type="hidden" id="fluxoAlteracao" name="fluxoAlteracao" value="${fluxoAlteracao}">
	<input type="hidden" id="fluxoVoltar" name="fluxoVoltar">	
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaServicoTipoCol1" class="coluna">
		
			<label class="rotulo" id="rotuloDescricao" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="40" size="40" value="${carteiraCobrancaVO.descricao}" onkeyup="letraMaiuscula(this);"/><br />
		
			<label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>N�mero:</label>
			<input class="campoTexto" type="text" id="numero" name="numero" maxlength="3" size="3" onkeypress="return formatarCampoInteiro(event);" value="${carteiraCobrancaVO.numero}">	
		
		
			<label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>Arrecadador:</label>
			<select class="campoSelect" id="chaveArrecadador" name="chaveArrecadador">
				<option value="">Selecione</option>
				<c:forEach items="${listaArrecadador}" var="arrecada">
					<option value="<c:out value="${arrecada.chavePrimaria}"/>" <c:if test="${carteiraCobrancaVO.chaveArrecadador == arrecada.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${arrecada.banco.codigoBanco} - ${arrecada.banco.nome}"/>
					</option>		
		    	</c:forEach>
			</select>	
			
			<label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>C�digo Carteira:</label>
			<select class="campoSelect" id="chaveCodigoCarteira" name="chaveCodigoCarteira">
				<option value="">Selecione</option>
				<c:forEach items="${listaCodigoCarteira}" var="carteira">
					<option value="<c:out value="${carteira.chavePrimaria}"/>" <c:if test="${carteiraCobrancaVO.chaveCodigoCarteira == carteira.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${carteira.descricao}"/>
					</option>		
		    	</c:forEach>
			</select>	
			
			<label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>Tipo Carteira:</label>
			<select class="campoSelect" id="chaveTipoCarteira" name="chaveTipoCarteira">
				<option value="">Selecione</option>
				<c:forEach items="${listaTipoCarteira}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${carteiraCobrancaVO.chaveTipoCarteira == tipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipo.descricao}"/>
					</option>		
		    	</c:forEach>
			</select>
					
		</fieldset>
		
		<fieldset id="pesquisaServicoTipoCol2" class="colunaFinal">	
			
			<label class="rotulo">Faixa para Nosso N�mero:</label>
			<input class="campoRadio" type="radio" name="indicadorFaixaNossoNumero" id="indicadorFaixaNossoNumero" value="true" <c:if test="${carteiraCobrancaVO.indicadorFaixaNossoNumero eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorFaixaNossoNumero" id="indicadorFaixaNossoNumero" value="false" <c:if test="${carteiraCobrancaVO.indicadorFaixaNossoNumero eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			
			<label class="rotulo">Nosso N�mero Livre para Empresa:</label>
			<input class="campoRadio" type="radio" name="indicadorNossoNumeroLivre" id="indicadorNossoNumeroLivre" value="true" <c:if test="${carteiraCobrancaVO.indicadorNossoNumeroLivre eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" >Sim</label>
			<input class="campoRadio" type="radio" name="indicadorNossoNumeroLivre" id="indicadorNossoNumeroLivre" value="false" <c:if test="${carteiraCobrancaVO.indicadorNossoNumeroLivre eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" >N�o</label>

            <div>
			<label class="rotulo">Banco deve emitir boleto:</label>
			<input class="campoRadio" type="radio" name="indicadorEmissao" id="indicadorEmissaoSim" value="true" <c:if test="${carteiraCobrancaVO.indicadorEmissao eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorEmissao" id="indicadorEmissaoNao" value="false" <c:if test="${carteiraCobrancaVO.indicadorEmissao eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>

				<div id="fieldLayoutFatura" <c:if test="${carteiraCobrancaVO.indicadorEmissao eq 'false'}">style="display:none;"</c:if>>

                    <label class="rotulo">Layout da Fatura Impressa:</label>
                    <input class="campoTexto" type="text" id="layoutFaturaImpressa" name="arquivoLayoutFatura" size="40" maxlength="255" value="${carteiraCobrancaVO.arquivoLayoutFatura}"/>
                </div>
            </div>

			<label class="rotulo">Permite protesto:</label>
			<input class="campoRadio" type="radio" name="indicadorProtesto" id="indicadorProtesto" value="true" <c:if test="${carteiraCobrancaVO.indicadorProtesto eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorProtesto" id="indicadorProtesto" value="false" <c:if test="${carteiraCobrancaVO.indicadorProtesto eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			
			
			<label class="rotulo">Realiza a entrega do documento:</label>
			<input class="campoRadio" type="radio" name="indicadorEntrega" id="indicadorEntrega" value="true" <c:if test="${carteiraCobrancaVO.indicadorEntrega eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorEntrega" id="indicadorEntrega" value="false" <c:if test="${carteiraCobrancaVO.indicadorEntrega eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			
			<c:if test="${ fluxoAlteracao eq true }">
				<label class="rotulo">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${carteiraCobrancaVO.habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" >Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${carteiraCobrancaVO.habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" >Inativo</label>
			</c:if>
			
		</fieldset>						
	</fieldset>	
				
	</fieldset>
</form>
	
	<fieldset class="conteinerBotoes"> 
	
		<c:if test="${ fluxoInclusao eq true }">
			<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
			<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
			<vacess:vacess param="exibirInclusaoCarteiraCobranca">
				<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" onclick="incluirCarteiraCobranca();"  type="button">
			</vacess:vacess>
		</c:if>
		
		<c:if test="${ fluxoAlteracao eq true }">
			<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onClick="voltar();">
			<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
			<vacess:vacess param="exibirAlteracaoCarteiraCobranca">
				<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" onclick="incluirCarteiraCobranca();"  type="button">
			</vacess:vacess>
		</c:if>
				
 	</fieldset>
