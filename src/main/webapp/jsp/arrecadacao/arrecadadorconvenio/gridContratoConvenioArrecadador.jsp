<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

function atualizarPadrao(chaveArrecadadorConvenioContrato){
	var confirmacao = confirm('Deseja tornar padr�o este contrato conv�nio arrecadador?');
	if(confirmacao == true){
		document.forms[0].chavePrimaria.value = chave;
		submeter("arrecadadorConvenioForm", 
				"atualizarPadrao");
	}	
}

</script>

<div id="gridContratoConvenioArrecadador">
	<fieldset>
	<%-- 	<c:if test="${listaArrecadadorConvenio ne null}"> --%>
			<hr class="linhaSeparadoraPesquisa" />	
			
	<%-- 	<c:set var="indexListaArrecadadorConvenio" value="0" /> --%>
				<display:table class="dataTableGGAS" name="listaArrecadadorConvenio" sort="list" id="arrecadadorConvenio"
					pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarArrecadadorConvenio">
					
					<display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	    					<input type="checkbox" name="chavesPrimarias"  value="${arrecadadorConvenio.chavePrimaria}">
			        </display:column>
			        
			        <display:column title="Ativo" style="width: 30px">
						<c:choose>
							<c:when test="${arrecadadorConvenio.habilitado == true}">
								<img id="img_ativo_${arrecadadorConvenio.chavePrimaria}" alt="Ativo" title="Ativo"
									src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img id="img_ativo_${arrecadadorConvenio.chavePrimaria}" alt="Inativo" title="Inativo"
									src="<c:url value="/imagens/cancel16.png"/>" border="0"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column title="Padr�o" style="width: 80px">
			  			<span id="span_indicador_padrao_${arrecadadorConvenio.chavePrimaria}">${arrecadadorConvenio.indicadorPadraoString}</span>
					</display:column>
					
			        <display:column sortable="true" sortProperty="arrecadadorContrato.numeroContrato" title="N�mero do Contrato do Arrecadador" >
			            <a href='javascript:detalharArrecadadorConvenio(<c:out value='${arrecadadorConvenio.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			            	<c:out value="${arrecadadorConvenio.arrecadadorContrato.numeroContrato}"/>
			            </a>
			        </display:column>

			        <display:column sortable="true" sortProperty="arrecadadorContrato.arrecadador" title="Arrecadador" >
			            <a href='javascript:detalharArrecadadorConvenio(<c:out value='${arrecadadorConvenio.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			            	<c:out value="${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.codigoBanco} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nome}"/>
			            </a>
			        </display:column>			        
			        
			        <display:column sortable="true" sortProperty="codigoConvenio"   title="C�digo do Conv�nio" >
			            <a href='javascript:detalharArrecadadorConvenio(<c:out value='${arrecadadorConvenio.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			            	<c:out value="${arrecadadorConvenio.codigoConvenio}"/>
			            </a>
			        </display:column>
			        
			    	<display:column sortable="true" sortProperty="contaConvenio.numeroConta"  title="Conta Dep�sito" >
			            <a href='javascript:detalharArrecadadorConvenio(<c:out value='${arrecadadorConvenio.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				            <c:if test="${arrecadadorConvenio.contaConvenio.numeroConta ne 'null'}">
								<c:out value="${arrecadadorConvenio.contaConvenio.numeroConta} - ${arrecadadorConvenio.contaConvenio.numeroDigito}"/>
							</c:if>
			            </a>
			        </display:column>
			        
			        <display:column sortable="true" sortProperty="contaCredito.numeroConta"  title="Conta Cr�dito" >
			            <a href='javascript:detalharArrecadadorConvenio(<c:out value='${arrecadadorConvenio.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			            	<c:if test="${arrecadadorConvenio.contaCredito.numeroConta ne 'null'}">
								<c:out value="${arrecadadorConvenio.contaCredito.numeroConta} - ${arrecadadorConvenio.contaCredito.numeroDigito}"/>
							</c:if>
			            </a>
			        </display:column>
			        
			        <display:column sortable="true" sortProperty="tipoConvenio.descricao"  title="Tipo Conv�nio" >
			            <a href='javascript:detalharArrecadadorConvenio(<c:out value='${arrecadadorConvenio.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			            	<c:out value="${arrecadadorConvenio.tipoConvenio.descricao}"/>
			            </a>
			        </display:column>
			        
			    </display:table>
	<%-- 	<c:set var="indexListaComponentesEquipe" value="${indexListaComponentesEquipe + 1}" /> --%>
	<%-- 	</c:if> --%>
	</fieldset>
</div>
