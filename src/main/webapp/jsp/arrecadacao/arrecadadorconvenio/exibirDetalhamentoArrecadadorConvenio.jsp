<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Contrato Conv�nio Arrecadador<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>"
    target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
    Para alterar uma Contrato Conv�nio Arrecadador clique em <span class="destaqueOrientacaoInicial">Alterar</span>.<br/>
    Para voltar para tela de pesquisa clique em <span class="destaqueOrientacaoInicial">Voltar</span>.<br />
</p>

<form:form method="post" action="exibirAlteracaoArrecadadorConvenio">

	<script language="javascript">
		
		function limparFormularioArrecadadorConvenio(){
			limparFormularios(arrecadadorConvenioForm);
		}
		
		function cancelarArrecadadorConvenio() {
			location.href = '<c:url value="/exibirPesquisaArrecadadorConvenio"/>';
		}
		
	</script>
	
		<input name="acao" type="hidden" id="acao" value="exibirAlteracaoArrecadadorConvenio">
		<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${arrecadadorConvenio.chavePrimaria}">
	
		<fieldset id="pesquisaRecebimento" class="conteinerPesquisarIncluirAlterar">		
			<fieldset class="coluna">	
				<label class="rotulo" id="listaArrecadadorContrato" for="arrecadadorContrato">Contrato:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="arrecadadorContrato">
					<c:out value="${arrecadadorConvenio.arrecadadorContrato.numeroContrato} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nome}"/>
				</span>
				<br />
				
				<label class="rotulo" id="rotuloCodigoConvenio" for="codigoConvenio">C�digo Conv�nio:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="codigoConvenio">
					<c:out value="${arrecadadorConvenio.codigoConvenio}"/>
				</span>	
				<br/>
				
				<label class="rotulo" id="rotuloTipoConvenio" for="tipoConvenio">Tipo Conv�nio:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="tipoConvenio">
					<c:out value="${arrecadadorConvenio.tipoConvenio.descricao}"/>
				</span>
				<br />
				
				<label class="rotulo" id="rotuloCarteiraCobranca" for="arrecadadorCarteiraCobranca">Carteira de Cobran�a:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="arrecadadorCarteiraCobranca">
					<c:out value="${arrecadadorConvenio.arrecadadorCarteiraCobranca.descricao}"/>
				</span>	
				<br />
				
				<label class="rotulo" id="rotuloCarteiraCobranca" for="arrecadadorCarteiraCobranca">Leiaute Banc�rio:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="arrecadadorCarteiraCobranca">
					<c:out value="${arrecadadorConvenio.leiaute.descricao}"/>
				</span>
				
				<label class="rotulo" id="indicadorPadrao" for="indicadorPadrao">Padr�o:</label>
				<span id="span_indicador_padrao" class="itemDetalhamento">${arrecadadorConvenio.indicadorPadraoString}</span>
				<br />
				
				<label class="rotulo" for="habilitado">Indicador de uso: &nbsp;</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="habilitado">
					<c:choose>
						<c:when test="${arrecadadorConvenio.habilitado == true}">
							<c:out value="Ativo"/>		
						</c:when>
						<c:otherwise>
							<c:out value="Inativo"/>
						</c:otherwise>
					</c:choose>
				</span>
									
			</fieldset>
			<fieldset class="colunaFinal">
				
				<label class="rotulo" id="rotulocontaConvenio" for="contaConvenio">Conta Dep�sito:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="contaConvenio">
					<c:if test="${arrecadadorConvenio.contaConvenio ne null}">
						<c:out value="${arrecadadorConvenio.contaConvenio.numeroConta}-${arrecadadorConvenio.contaConvenio.numeroDigito}"/>
					</c:if>
				</span>
				<br />
				<label class="rotulo" id="rotulocontaCredito" for="contaCredito">Conta Cr�dito:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="contaCredito">
					<c:if test="${arrecadadorConvenio.contaCredito ne null}">
						<c:out value="${arrecadadorConvenio.contaCredito.numeroConta}-${arrecadadorConvenio.contaCredito.numeroDigito}"/>
					</c:if>
				</span>
				<br />
				<label class="rotulo rotulo2Linhas">N� Sequ�ncia Cobran�a Gerado:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="codigoConvenio">
					<c:out value="${arrecadadorConvenio.sequenciaCobrancaGerado}"/>
				</span>	
				<br/>
				<label class="rotulo rotulo2Linhas">N� Sequ�ncia Cobran�a In�cio:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="codigoConvenio">
					<c:out value="${arrecadadorConvenio.sequencialCobrancaInicio}"/>
				</span>	
				<br/>
				<label class="rotulo rotulo2Linhas">N� Sequ�ncia Cobran�a Fim:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="codigoConvenio">
					<c:out value="${arrecadadorConvenio.sequencialCobrancaFim}"/>
				</span>	
				<br/>
				
			</fieldset>
			<fieldset class="colunaFinal">

				<label class="rotulo rotulo2Linhas">Layout da Fatura Digital:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo" id="codigoConvenio">
					<c:out value="${arrecadadorConvenio.arquivoLayoutFatura}"/>
				</span>

			</fieldset>
		</fieldset>
		<fieldset class="conteinerBotoes">
			<input name="Button" class="bottonRightCol" value="Voltar" type="button" onClick="cancelarArrecadadorConvenio();">			
				<input name="Button" class="bottonRightCol2 botaoGrande1 bottonLeftColUltimo" id="botaoAlterar" value="Alterar" type="submit">
			<vacess:vacess param="exibirDetalhamentoArrecadadorConvenio">
			</vacess:vacess>
		</fieldset>
	
</form:form>
