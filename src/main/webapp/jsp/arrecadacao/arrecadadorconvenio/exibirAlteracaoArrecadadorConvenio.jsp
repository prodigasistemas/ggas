<%@page import="br.com.ggas.arrecadacao.impl.ArrecadadorContratoConvenioImpl"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<input name="idTipoConvenioDebitoAutomatico" type="hidden" id="idTipoConvenioDebitoAutomatico" value="${idTipoConvenioDebitoAutomatico}" >

<h1 class="tituloInterno">Alterar Contrato Conv�nio Arrecadador<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>"
    target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
    Para alterar uma Contrato Conv�nio Arrecadador, altere os dados desejados, mantendo preenchidos todos os campos obrigat�rios, e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.<br/>
    Para cancelar clique em <span class="destaqueOrientacaoInicial">Cancelar</span>.<br />
</p>

<form:form method="post" action="alterarArrecadadorConvenio" name="arrecadadorConvenio"
		   id="arrecadadorConvenio" modelAttribute="ArrecadadorContratoConvenioImpl" onsubmit="verificarCampoLayout()">

	<script language="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/arrecadacao/index.js"></script>
		<input type="hidden" name="indicadorPadrao" value="<c:out value="${arrecadadorConvenio.indicadorPadraoString == 'Sim'}"/>">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${arrecadadorConvenio.chavePrimaria}">

		<fieldset id="pesquisaRecebimento" class="conteinerPesquisarIncluirAlterar">		
			<fieldset class="coluna">	
				<label class="rotulo" id="listaArrecadadorContrato" for="arrecadadorContrato"><span class="campoObrigatorioSimbolo">* </span>Contrato:</label>
				<select name="arrecadadorContrato" id="arrecadadorContrato" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaArrecadadorContrato}" var="arrecadadorContrato">
						<option value="<c:out value="${arrecadadorContrato.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.arrecadadorContrato.chavePrimaria == arrecadadorContrato.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${arrecadadorContrato.numeroContrato} - ${arrecadadorContrato.arrecadador.banco.nome}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
				
				<label class="rotulo rotulo2Linhas" id="rotuloCodigoConvenio" for="codigoConvenio"><span class="campoObrigatorioSimbolo">* </span>C�digo Conv�nio:</label>
	            <input class="campoTexto" type="text" id="codigoConvenio" name="codigoConvenio" size="20" maxlength="20" <c:out value="${codigoConvenio}"/> value="${arrecadadorConvenio.codigoConvenio}"/>
				<br />
				
				<label class="rotulo" id="rotuloTipoConvenio" for="tipoConvenio"><span class="campoObrigatorioSimbolo">* </span>Tipo Conv�nio:</label>
				<select name="tipoConvenio" id="tipoConvenio" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoConvenio}" var="tipoConvenio">
						<option value="<c:out value="${tipoConvenio.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.tipoConvenio.chavePrimaria == tipoConvenio.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${tipoConvenio.descricao}"/>
						</option>
				    </c:forEach>
				</select>
				<br />

				<label class="rotulo" id="rotuloCarteiraCobranca" for="arrecadadorCarteiraCobranca"><span class="campoObrigatorioSimbolo2">* </span>Carteira de Cobran�a:</label>
				<select name="arrecadadorCarteiraCobranca" id="arrecadadorCarteiraCobranca" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaCarteiraCobranca}" var="carteiraCobranca">
						<option value="<c:out value="${carteiraCobranca.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.arrecadadorCarteiraCobranca.chavePrimaria == carteiraCobranca.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${carteiraCobranca.descricao}"/>
						</option>
				    </c:forEach>
				</select>			
				<br />
				<label class="rotulo" id="rotuloleiaute" for="leiaute"><span class="campoObrigatorioSimbolo">* </span>Leiaute Banc�rio:</label>
				<select name="leiaute" id="leiaute" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaLeiautes}" var="leiaute">
						<option value="<c:out value="${leiaute.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.leiaute.chavePrimaria == leiaute.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${leiaute.descricao} - ${leiaute.formaArrecadacao.descricao}"/>
						</option>
				    </c:forEach>
				</select>
				
				<br />				
				<label class="rotulo" id="indicadorPadrao" for="indicadorPadrao">Padr�o:</label>
				<span id="span_indicador_padrao" class="itemDetalhamento">${arrecadadorConvenio.indicadorPadraoString}</span>
					                
	            <br />
				<label class="rotulo" for="habilitado">Indicador de uso: &nbsp;</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${arrecadadorConvenio.habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${arrecadadorConvenio.habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Inativo</label>
				
			</fieldset>			
			<fieldset class="colunaFinal">

				<label class="rotulo" id="rotulocontaConvenio" for="contaConvenio">Conta Dep�sito:</label>
				<select name="contaConvenio" id="contaConvenio" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaContaConvenio}" var="contaConvenio">
						<option value="<c:out value="${contaConvenio.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.contaConvenio.chavePrimaria == contaConvenio.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${contaConvenio.numeroConta}-${contaConvenio.numeroDigito}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
				<label class="rotulo" id="rotulocontaCredito" for="contaCredito">Conta Cr�dito:</label>
				<select name="contaCredito" id="contaCredito" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaContaCredito}" var="contaCredito">
						<option value="<c:out value="${contaCredito.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.contaCredito.chavePrimaria == contaCredito.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${contaCredito.numeroConta}-${contaCredito.numeroDigito}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
				<label class="rotulo" id="rotuloCarteiraCobranca" for="arrecadadorCarteiraCobranca">C�digo Carteira:</label>
				<select name="arrecadadorCarteiraCobranca" id="arrecadadorCarteiraCobranca" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaCarteiraCobranca}" var="carteiraCobranca">
						<option value="<c:out value="${carteiraCobranca.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.arrecadadorCarteiraCobranca.chavePrimaria == carteiraCobranca.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${carteiraCobranca.descricao}"/>
						</option>
				    </c:forEach>
				</select>			
				<br />
				<label class="rotulo" id="indicadorPadrao" for="indicadorPadrao">Padr�o:</label>
				<span id="span_indicador_padrao" name="indicadorPadrao" value="<c:if test="${arrecadadorConvenio.indicadorPadraoString == 'Sim'}"></c:if><c:out value="${arrecadadorConvenio.indicadorPadraoString == 'Sim'}"/>" class="itemDetalhamento">${arrecadadorConvenio.indicadorPadraoString}</span>
			</fieldset>			
			<fieldset class="colunaFinal">								
	                
	            <label class="rotulo rotulo2Linhas">N� Sequ�ncia Cobran�a Gerado:</label>
	            <input class="campoTexto" type="text" id="sequenciaCobrancaGerado" name="sequenciaCobrancaGerado" size="15" maxlength="15" <c:if test="${arrecadadorConvenio.sequenciaCobrancaGerado > 0}">value="${arrecadadorConvenio.sequenciaCobrancaGerado}"</c:if>/>
				
				<label class="rotulo rotulo2Linhas">N� Sequ�ncia Cobran�a In�cio:</label>
	            <input class="campoTexto" type="text" id="sequencialCobrancaInicio" name="sequencialCobrancaInicio" size="15" maxlength="15" <c:if test="${arrecadadorConvenio.sequencialCobrancaInicio > 0}"> value="${arrecadadorConvenio.sequencialCobrancaInicio}"</c:if>/>
				
				<label class="rotulo rotulo2Linhas">N� Sequ�ncia Cobran�a Fim:</label>
	            <input class="campoTexto" type="text" id="sequencialCobrancaFim" name="sequencialCobrancaFim" size="15" maxlength="15" <c:if test="${arrecadadorConvenio.sequencialCobrancaFim > 0}"> value="${arrecadadorConvenio.sequencialCobrancaFim}"</c:if>/>

			</fieldset>
			<fieldset class="colunaFinal" id="fieldLayoutFatura" style="display: none;">

				<label class="rotulo rotulo2Linhas">Layout da Fatura Digital:</label>
				<input class="campoTexto" type="text" id="layoutFaturaDigital" name="arquivoLayoutFatura" size="50" maxlength="255" value="${arrecadadorConvenio.arquivoLayoutFatura}"/>

			</fieldset>
		</fieldset>
		<fieldset class="conteinerBotoes">
			<input name="ButtonCancelar" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelarArrecadadorConvenio('<c:url value="/exibirPesquisaArrecadadorConvenio"/>');">
			<input name="ButtonLimpar" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormularioArrecadadorConvenio();">			
				<input name="ButtonAlterar" class="bottonRightCol2 botaoGrande1 bottonLeftColUltimo" id="botaoAlterar" value="Salvar" type="submit">
			<vacess:vacess param="exibirAlteracaoArrecadadorConvenio">
			</vacess:vacess>
		</fieldset>
	
</form:form>
