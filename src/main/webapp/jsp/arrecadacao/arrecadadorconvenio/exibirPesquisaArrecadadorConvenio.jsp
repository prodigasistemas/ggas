<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="br.com.ggas.arrecadacao.impl.ArrecadadorContratoConvenioImpl"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	



<h1 class="tituloInterno">Pesquisar Contrato Conv�nio Arrecadador<a class="linkHelp" href="<help:help>/consultadosrecebimentos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarArrecadadorConvenio" name="arrecadadorConvenioForm" id="arrecadadorConvenio" modelAttribute="ArrecadadorContratoConvenioImpl">

	<script language="javascript">
		$().ready(function(){
			$("input#codigoConvenio").keyup(apenasNumeros);
			if($("#indicador").val()==""){
				document.forms[0].habilitado[0].checked = true;
			}
		});
				
		function alterarArrecadadorConvenio(){
			
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('arrecadadorConvenio', 'exibirAlteracaoArrecadadorConvenio');
		    }
		}
		
		
		function detalharArrecadadorConvenio(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("arrecadadorConvenio", "exibirDetalhamentoArrecadadorConvenio");
		}
		
		function incluirArrecadadorConvenio() {
			location.href = '<c:url value="/exibirInclusaoArrecadadorConvenio"/>';
		}
		
		function limparFormularioArrecadadorConvenio(){
			limparFormularios(arrecadadorConvenioForm);
			document.forms[0].habilitado[0].checked = true;
		}
		
		function removerArrecadadorConvenio(){
			
			var selecao = verificarSelecao();
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('arrecadadorConvenio', 'removerConvenioArrecadador');
				}
		    }
		}
		
		function funcaoAlterarArrecadadorConvenioPadrao(){
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {
				var unicaChavePrimaria = obterValorUnicoCheckboxSelecionado();
				if ($("#img_ativo_"+unicaChavePrimaria).attr("title") == "Ativo") {
					var naoArrecadadorConvenioPadrao = $("#span_indicador_padrao_"+unicaChavePrimaria).text() == 
						"<%=ArrecadadorContratoConvenioImpl.INDICADOR_PADRAO_STRING_NAO%>";
					if(naoArrecadadorConvenioPadrao){
						var retorno = confirm('<fmt:message key="PERGUNTA_ARRECADADOR_CONVENIO_PADRAO"/>');
						if(retorno == true) {
							document.forms[0].chavePrimaria.value = unicaChavePrimaria;
							submeter('arrecadadorConvenio', 
									'alterarArrecadadorConvenioPadrao');
						}
				    } else {
						alert('<fmt:message key="ERRO_ARRECADADOR_CONVENIO_PADRAO"/>')
					}	
				} else {
					alert('<fmt:message key="ERRO_ARRECADADOR_CONVENIO_PADRAO_INATIVO"/>')
				}
			}
		}
		
	</script>
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
		<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
		<fieldset id="pesquisaRecebimento" class="conteinerPesquisarIncluirAlterar">		
			<fieldset class="coluna">	
				<label class="rotulo" id="listaArrecadadorContrato" for="arrecadadorContrato">Contrato:</label>
				<select name="arrecadadorContrato" id="arrecadadorContrato" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaArrecadadorContrato}" var="arrecadadorContrato">
						<option value="<c:out value="${arrecadadorContrato.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.arrecadadorContrato.chavePrimaria == arrecadadorContrato.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${arrecadadorContrato.numeroContrato}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
				<label class="rotulo" id="rotuloTipoConvenio" for="tipoConvenio">Tipo Conv�nio:</label>
				<select name="tipoConvenio" id="tipoConvenio" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoConvenio}" var="tipoConvenio">
						<option value="<c:out value="${tipoConvenio.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.tipoConvenio.chavePrimaria == tipoConvenio.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${tipoConvenio.descricao}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
				<label class="rotulo" id="rotulocontaConvenio" for="contaConvenio">Conta Dep�sito:</label>
				<select name="contaConvenio" id="contaConvenio" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaContaConvenio}" var="contaConvenio">
						<option value="<c:out value="${contaConvenio.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.contaConvenio.chavePrimaria == contaConvenio.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${contaConvenio.numeroConta}-${contaConvenio.numeroDigito}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
				<label class="rotulo" id="rotuloleiaute" for="leiaute">Leiaute Banc�rio:</label>
				<select name="leiaute" id="leiaute" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaLeiautes}" var="leiaute">
						<option value="<c:out value="${leiaute.chavePrimaria}"/>" <c:if test="${arrecadadorConvenioForm.map.leiaute == leiaute.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${leiaute.descricao} - ${leiaute.formaArrecadacao.descricao}"/>
						</option>
				    </c:forEach>
				</select>
			</fieldset>	
					
			<fieldset class="colunaFinal">
				<label class="rotulo" id="rotulocontaCredito" for="contaCredito">Conta Cr�dito:</label>
				<select name="contaCredito" id="contaCredito" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaContaCredito}" var="contaCredito">
						<option value="<c:out value="${contaCredito.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.contaCredito.chavePrimaria == contaCredito.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${contaCredito.numeroConta}-${contaCredito.numeroDigito}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
				<label class="rotulo" id="rotuloCarteiraCobranca" for="arrecadadorCarteiraCobranca">Carteira de Cobran�a:</label>
				<select name="arrecadadorCarteiraCobranca" id="arrecadadorCarteiraCobranca" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaCarteiraCobranca}" var="carteiraCobranca">
						<option value="<c:out value="${carteiraCobranca.chavePrimaria}"/>" <c:if test="${arrecadadorConvenio.arrecadadorCarteiraCobranca.chavePrimaria == carteiraCobranca.chavePrimaria}">selected="selected"</c:if>>
						    <c:out value="${carteiraCobranca.descricao}"/>
						</option>
				    </c:forEach>
				</select>				
				<br />		
				<label class="rotulo" id="rotuloCodigoConvenio" for="codigoConvenio">C�digo Conv�nio:</label>
	            <input class="campoTexto" type="text" id="codigoConvenio" name="codigoConvenio" size="20"
	                 value="${arrecadadorConvenio.codigoConvenio}">
				<br />
				<label class="rotulo" for="habilitado">Indicador de uso: &nbsp;</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null' or habilitado eq ''}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Todos</label><br/>
			</fieldset>
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="exibirPesquisaArrecadadorConvenio">
	    			<input name="ButtonPesquisar" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
		    	</vacess:vacess>	
				<input name="ButtonLimpar" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormularioArrecadadorConvenio();">		
			</fieldset>
		</fieldset>
		<c:if test="${listaArrecadadorConvenio ne null}">
			<jsp:include page="/jsp/arrecadacao/arrecadadorconvenio/gridContratoConvenioArrecadador.jsp"></jsp:include>		
		</c:if>
		<fieldset class="conteinerBotoes">
			<c:if test="${not empty listaArrecadadorConvenio}">
				<vacess:vacess param="exibirAlteracaoArrecadadorConvenio">
					<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" id="botaoAlterar" onclick="alterarArrecadadorConvenio()" type="button">
				</vacess:vacess>
				
				<vacess:vacess param="exibirAlteracaoArrecadadorConvenio">
					<input name="buttonAlterarPadrao" value="Alterar Padr�o" class="bottonRightCol2" 
						onclick="funcaoAlterarArrecadadorConvenioPadrao()" type="button">
				</vacess:vacess>
				
				<vacess:vacess param="removerArrecadadorConvenio">
					<input name="buttonRemover" value="Remover" class="bottonRightCol2 botaoGrande1 bottonLeftColUltimo" 
					onclick="removerArrecadadorConvenio()" type="button" style="margin-right: 7px;">
				</vacess:vacess>
			</c:if>
			<vacess:vacess param="exibirInclusaoArrecadadorConvenio">
				<input name="buttonIncluir" value="Incluir" class="bottonRightCol2 botaoGrande1 bottonLeftColUltimo" style="margin-right: 7px;"
				id="botaoIncluir" onclick="incluirArrecadadorConvenio()" type="button">
			</vacess:vacess>
		</fieldset>
	
</form:form>
