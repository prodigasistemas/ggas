<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



<h1 class="tituloInterno">Pesquisar Contrato Arrecadador<a href="<help:help>/consultadasempresas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>


	<script type="text/javascript">
	
		$(document).ready(function() {
			
			$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
			document.getElementById("habilitadoTodos").value = '';

		});
		
		function pesquisar() {
			submeter('contratoArrecadadorForm', 'pesquisarContratoArrecadador');
		}

		function limparFormulario(ele){
// 			limparFormularios(ele);
			document.getElementById("numeroContrato").value = "";
			document.getElementById("dataInicio").value = "";
			document.getElementById("dataFim").value = "";
			document.getElementById("chaveArrecadador").value ="";
			
		}
		
		function incluir() {
			submeter('contratoArrecadadorForm', 'exibirInclusaoContratoArrecadador');
		}
		
		function detalharContratoArrecadador(valorChave) {
			document.forms[0].chavePrimaria.value = valorChave;
			submeter('contratoArrecadadorForm', 'exibirDetalharContratoArrecadador');
		}
		
		function exibirAlterarContratoArrecadadores() {
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('contratoArrecadadorForm', 'exibirAlteracaoContratoArrecadador');
			}

		}
		
		function removerContratoArrecadadores() {

			var selecao = verificarSelecao();

			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if (retorno == true) {
					submeter('contratoArrecadadorForm', 'removerContratoArrecadador');
				}
			}
		}
		
		
		
	</script>

<form:form method="post" action="pesquisarContratoArrecadador" id="contratoArrecadadorForm" name="contratoArrecadadorForm" modelAttribute="ContratoArrecadadorVO">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
	<fieldset id="materialCol1" class="coluna">
	
	<label class="rotulo" >N�mero Contrato:</label>
		<input class="campoTexto" type="text" name="numeroContrato" id="numeroContrato" maxlength="20" size="30" value="${contratoArrecadador.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"/><br />
	
	<label class="rotulo">Arrecadador:</label>
			<select class="campoSelect" id="chaveArrecadador" name="chaveArrecadador">
				<option value="">Selecione</option>
				<c:forEach items="${listaArrecadador}" var="arrecada">
					<option value="<c:out value="${arrecada.chavePrimaria}"/>" <c:if test="${carteiraCobrancaVO.chaveArrecadador == arrecada.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${arrecada.banco.codigoBanco} - ${arrecada.banco.nome}"/>
					</option>		
		    	</c:forEach>
			</select>	
			
	</fieldset>
																		
<fieldset id="materialCol2" class="colunaFinal">		
  
  <label class="rotulo"  >Per�odo Contrato Arrecadador:</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataInicio" name="dataInicio" maxlength="10" style=" margin-left: 65px;" value="${contratoArrecadador.dataInicio}">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFim" name="dataFim" maxlength="10" value="${contratoArrecadador.dataFim}">	
			
			
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${contratoArrecadadorVO.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${contratoArrecadadorVO.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoTodos" value="" checked="checked" <c:if test="${contratoArrecadadorVO.habilitado eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>

		
		 </fieldset>	


<fieldset class="conteinerBotoesPesquisarDirFixo">
	   		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisar();">
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario(this.form);">		
		</fieldset>
	     
	     <c:if test="${listaContratoArrecadadores ne null}">
  <hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaContratoArrecadadores"
			sort="list" id="contratoArrecadador"
			decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="pesquisarContratoArrecadador">
			
			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="${contratoArrecadador.chavePrimaria}">
	        </display:column>
	        
	        <display:column title="Ativo" style="width: 10px">
		     	<c:choose>
					<c:when test="${contratoArrecadador.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column sortable="true" sortProperty="numeroContrato" title="N�mero Contrato" style="width: 130px">
	        	<a href="javascript:detalharContratoArrecadador(<c:out value='${contratoArrecadador.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${contratoArrecadador.numeroContrato}"/>
	            </a>
	        </display:column>

			<display:column sortable="true" sortProperty="arrecadador" title="Arrecadador" style="width: 190px">
	        	<a href="javascript:detalharContratoArrecadador(<c:out value='${contratoArrecadador.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${contratoArrecadador.arrecadador.banco.codigoBanco} - ${contratoArrecadador.arrecadador.banco.nome}"/>
	            </a>
	        </display:column>	        
	        
	        <display:column sortable="true"  title="Data In�cio" style="width: 100px">
	        	<a href="javascript:detalharContratoArrecadador(<c:out value='${contratoArrecadador.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	        	<fmt:formatDate value="${contratoArrecadador.dataInicio}" pattern="dd/MM/yyyy"/>
	            </a>
	        </display:column>
	        
	        <display:column sortable="true"  title="Data Fim" style="width: 100px" >
	        	<a href="javascript:detalharContratoArrecadador(<c:out value='${contratoArrecadador.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<fmt:formatDate value="${contratoArrecadador.dataFim}" pattern="dd/MM/yyyy"/>
	            </a>
	        </display:column>

		</display:table>
</c:if>

		<hr class="linhaSeparadoraPesquisa" />

	<fieldset class="conteinerBotoesPesquisarDir">
<%-- 		<vacess:vacess param="exibirInclusaoQuestionario"> --%>
			<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
				value="Incluir" type="button" onclick="incluir()">
<%-- 		</vacess:vacess> --%>
	</fieldset>
	<c:if test="${not empty listaContratoArrecadadores}">

		<fieldset class="conteinerBotoesPesquisarEsq">
<%-- 			<vacess:vacess param="exibirAlteracaoQuestionario"> --%>
				<input name="Button" class="bottonRightCol2" id="botaoAlterar"
					value="Alterar" type="button" onclick="exibirAlterarContratoArrecadadores()">
<%-- 			</vacess:vacess> --%>
<%-- 			<vacess:vacess param="removerQuestionario"> --%>
				<input name="Button" class="bottonRightCol2" id="botaoRemover"
					value="Remover" type="button" onclick="removerContratoArrecadadores()">
<%-- 			</vacess:vacess> --%>
		</fieldset>


	</c:if>
	</fieldset>
</form:form>
