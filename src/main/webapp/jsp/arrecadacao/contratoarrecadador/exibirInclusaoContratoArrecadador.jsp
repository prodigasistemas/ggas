<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:if test="${ acao eq 'incluir' }">
	<h1 class="tituloInterno">Incluir Contrato Arrecadador<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>
</c:if>

<c:if test="${ acao eq 'alterar' }">
	<h1 class="tituloInterno">Alterar Contrato Arrecadador<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
</c:if>

<script type="text/javascript">

$(document).ready(function(){
	$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	
	$("input[type=text]").attr("onpaste","return false;");
});

function cancelar(){
	location.href = '<c:url value="/exibirPesquisaContratoArrecadador"/>';
}

function voltar() {	
	submeter("contratoArrecadadorForm", "pesquisarContratoArrecadador");
}

function limparFormulario(){
	$("#descricao").val("");	
	$("#numero").val("");
	$("#chaveTipoCarteira").val("");
	$("#chaveCodigoCarteira").val("");
	$("#chaveArrecadador").val("");
	
}

function incluir(){
	
		submeter('contratoArrecadadorForm', 'incluirContratoArrecadador');

}

function alterar(){
	submeter('contratoArrecadadorForm', 'alterarContratoArrecadador');
}

</script>


<form:form method="post" id="contratoArrecadadorForm" name="contratoArrecadadorForm" modelAttribute="ContratoArrecadador" enctype="multipart/form-data">
	<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${contratoArrecadador.chavePrimaria}">
	<input type="hidden" id="fluxoVoltar" name="fluxoVoltar">	
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="materialCol1" class="coluna">
		
			<label class="rotulo" id="rotuloDescricao" for="numeroContrato"><span class="campoObrigatorioSimbolo">* </span>N�mero Contrato:</label>
			<input class="campoTexto" type="text" name="numeroContrato" id="numeroContrato" maxlength="20" size="40" value="${contratoArrecadador.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"/><br />
		
			<label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>Arrecadador:</label>
			<select class="campoSelect" id="chaveArrecadador" name="chaveArrecadador">
				<option value="">Selecione</option>
				<c:forEach items="${listaArrecadador}" var="arrecada">
					<option value="<c:out value="${arrecada.chavePrimaria}"/>" <c:if test="${contratoArrecadador.arrecadador.chavePrimaria == arrecada.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${arrecada.banco.codigoBanco} - ${arrecada.banco.nome}"/>
					</option>		
		    	</c:forEach>
			</select>	
			
			</fieldset>	
			
			<fieldset id="materialCol2" class="colunaFinal">	
			<label class="rotulo" id="rotuloDescricao" for="dataInicio"><span class="campoObrigatorioSimbolo">* </span>Data In�cio:</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataInicial" name="dataInicial" maxlength="10" value="<fmt:formatDate value="${contratoArrecadador.dataInicio}" pattern="dd/MM/yyyy"/>" >
			
			<label class="rotulo" id="rotuloDescricao" for="dataFim">Data Fim:</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFinal" name="dataFinal" maxlength="10"  value="<fmt:formatDate value="${contratoArrecadador.dataFim}" pattern="dd/MM/yyyy"/>">
			</fieldset>	
			
			<c:if test="${ acao eq 'alterar' }">
				<label class="rotulo">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${contratoArrecadador.habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" >Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${contratoArrecadador.habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" >Inativo</label>
			</c:if>
			
							
	</fieldset>	
				
	
	<fieldset class="conteinerBotoes"> 
	
		<c:if test="${ acao eq 'incluir' }">
			<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
			<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
<%-- 			<vacess:vacess param="exibirInclusaoCarteiraCobranca"> --%>
				<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" onclick="incluir();"  type="button">
<%-- 			</vacess:vacess> --%>
		</c:if>
		
		<c:if test="${ acao eq 'alterar' }">
			<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onClick="voltar();">
			<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
<%-- 			<vacess:vacess param="exibirAlteracaoCarteiraCobranca"> --%>
				<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" onclick="alterar();"  type="button">
<%-- 			</vacess:vacess> --%>
		</c:if>
				
 	</fieldset>
</form:form>