<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<h1 class="tituloInterno">Incluir Recebimento<a class="linkHelp" href="<help:help>/inclusoalteraodosrecebimentos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para exibir os documentos e d�bitos selecione um Cliente clicando em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span>,
	ou um Im�vel clicando em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span> para listar Ponto(s) de Consumo e selecione um dos Pontos</p>

<script type="text/javascript">
	
	$(document).ready(function() {

	   	$(".campoData").datepicker({
	   		changeYear: true, 
	   		yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', 
	   		showOn: 'button', 
	   		buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
	   		buttonImageOnly: true, 
	   		buttonText: 'Exibir Calend�rio', 
	   		dateFormat: 'dd/mm/yy',
	   		onSelect: function (stringData, objeto) {
	   			atualizarValoresCorrigidos(stringData);
	   	    }});
	   	
	   	AjaxService
		.getDataHoje({callback: function(stringDataHojeServidor) {
		   	$('#dataRecebimentoInclusao')
		   		.datepicker("option", "maxDate", stringDataHojeServidor)
		   		.datepicker("setDate", stringDataHojeServidor);
		} , async: false });
	   	
	   	$(".campoValorReal[disabled='disabled']").addClass("campoValorRealDesabilitado");

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto,#rotuloPontoConsumoLegado").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#faturaVO").chromatable({
			width: "891px"
		});
		
		corrigirPosicaoDatepicker();
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
		}	
		document.getElementById('botaoPesquisar').disabled = true;
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('pontoConsumoLegado').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		document.getElementById('botaoPesquisar').disabled = true;
		
		<c:if test="${listaFatura ne null}">
			document.getElementById('idArrecadador').value = "-1";
			document.getElementById('formaArrecadacao').value = "-1";
			document.getElementById('observacao').value = "";
			document.getElementById("bancoArrecadadorTexto").value = "";
			document.getElementById("agenciaContaArrecadadorTexto").value = "";
// 			document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";

			document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
			document.getElementById('bancoArrecadadorTexto').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
			
			document.getElementById("dataRecebimentoInclusao").value = "";
			document.getElementById("valorRecebimento").value = "";
		</c:if>
		
		var checkBox = document.getElementsByName("chavesPrimariasFaturas");
		for( var i = 0 ; i < checkBox.length ; i++){
			checkBox[i].checked = false;
		} 		
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
			document.getElementById('botaoPesquisar').disabled = false;
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
            document.getElementById('botaoPesquisar').disabled = true;
        }
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function pesquisar() {
		submeter('recebimentoForm', 'exibirFaturasRecebimento');
	}

	function pesquisarFaturasPontoConsumo(chave) {
		document.forms[0].idPontoConsumo.value = chave;
		submeter('recebimentoForm', 'exibirFaturasRecebimento');
	}

	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaRecebimento"/>';
	}

	function exibirRecebimentosPontoConsumo(idPonto) {
		document.getElementById('idCliente').value = "";
		document.getElementById('idPontoConsumo').value = idPonto;
		submeter('recebimentoForm', 'exibirRecebimentos');
	}

	function carregarTiposConvenio(elem) {	
		if (elem.value == '-1' || elem.value == ''){
// 			document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";

			document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
			document.getElementById('bancoArrecadadorTexto').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
		}	
		var idArrecadador = elem.value;
		listarTiposConvenioPorArrecadador(idArrecadador);
	}
	
	function listarTiposConvenioPorArrecadador(idArrecadador){
	  	var selectTiposConvenio = document.getElementById("formaArrecadacao");
	  	var idTipoConvenio = "${recebimentoVO.idFormaArrecadacao}";
	  	selectTiposConvenio.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectTiposConvenio.options[selectTiposConvenio.length] = novaOpcao;
       	AjaxService.listarTiposConvenioPorArrecadador(idArrecadador,
           	function(tiposConvenio){            		      		         		
               	for (key in tiposConvenio){
	                var novaOpcao = new Option(tiposConvenio[key], key);
	                if (key == idTipoConvenio){
	                	novaOpcao.selected = true;
	                }
	                selectTiposConvenio.options[selectTiposConvenio.length] = novaOpcao;		            			            	
            	}
               	mostrarACTipoConvenio();
        	}
        );
    	
	}

	function mostrarACTipoConvenio(){
		var idArrecadador = document.getElementById("idArrecadador").value;
		var idFormaArrecadacao = document.getElementById("formaArrecadacao").value;
		var bancoArrecadadorTexto = document.getElementById("bancoArrecadadorTexto");
		var agenciaContaArrecadadorTexto = document.getElementById("agenciaContaArrecadadorTexto");

		if((idArrecadador != '' && idArrecadador != '-1') && (idFormaArrecadacao != '' && idFormaArrecadacao != '-1')){
			AjaxService.consultarContaArrecadadorSelecionado(idArrecadador,idFormaArrecadacao, {
				callback: function(arrecadador) {	           		
		       		if(arrecadador != null){
		       			bancoArrecadadorTexto.value = arrecadador["nomeBanco"];
		       			agenciaContaArrecadadorTexto.value = arrecadador["contaBancaria"];
// 		       			document.getElementById('bancoAgenciaContaArrecadador').style.display = "block";

		    			document.getElementById('bancoArrecadadorTextoLabel').style.display = "block";
		    			document.getElementById('bancoArrecadadorTexto').style.display = "block";
		    			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "block";
		    			document.getElementById('agenciaContaArrecadadorTexto').style.display = "block";
		           	}
		    		else {
		    			document.getElementById("bancoArrecadadorTexto").value = '';
		    			document.getElementById("agenciaContaArrecadadorTexto").value = '';
// 		    			document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";

		    			document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
		    			document.getElementById('bancoArrecadadorTexto').style.display = "none";
		    			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
		    			document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
		    			
			    	} }, async:false}
	    		);
		} else {
			document.getElementById("bancoArrecadadorTexto").value = '';
			document.getElementById("agenciaContaArrecadadorTexto").value = '';
// 			document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";
			
			document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
			document.getElementById('bancoArrecadadorTexto').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
			
		}
	}
	
	
	function gerarRelatorioReciboQuitacao() {
		<c:if test="${not empty gerarRelatorioReciboQuitacao && gerarRelatorioReciboQuitacao == true}">
			submeter("recebimentoForm", "gerarRelatorioReciboQuitacao");
		</c:if>
	}

	function manterDadosCheckBox(valor){

		<c:forEach items="${chavesPrimariasFaturas}" var="fatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>').checked = valor;
			}	
		</c:forEach>

	}

	function ativarBotaoPesquisar() {
		document.getElementById("botaoPesquisar").disabled = false;
	}
	
	function incluirRecebimento(){
		
	}

	function init() {
		var elem = document.getElementById('idArrecadador');
		
		if(elem != null){
		  carregarTiposConvenio(elem);
		}
		
		
		manterDadosCheckBox(true);
		
		<c:choose>
			<c:when test="${recebimentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				document.getElementById('botaoPesquisar').disabled = false;
				habilitaCliente();
			</c:when>
			<c:when test="${recebimentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				document.getElementById('botaoPesquisar').disabled = false;
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
				document.getElementById('botaoPesquisar').disabled = true;
			</c:otherwise>
		</c:choose>	

		var idCliente = '${recebimentoVO.idCliente}';
		var idImovel = '${recebimentoVO.idImovel}';
		
		if(idCliente != ''){
			document.getElementById('indicadorPesquisaCliente').checked = true;
			selecionarCliente(idCliente);
		}
		
		if(idImovel != ''){
			document.getElementById('indicadorPesquisaImovel').checked = true;
			selecionarImovel(idImovel);
		}
	}

	function atualizarValoresCorrigidos(stringData) {
		
		var chavesPrimariasFaturas = new Array();
		var checkboxes = $('input[name=chavesPrimariasFaturas]');
		checkboxes.each(function() {
			var codigoFatura = $(this).val();
			chavesPrimariasFaturas.push(codigoFatura);
		});
		
		AjaxService.atualizarValoresCorrigidos( 
			chavesPrimariasFaturas, stringData, false, {
				callback: function(listaFaturas) {
					
					for (var i = 0; i < listaFaturas.length; i++) {
						var fatura = listaFaturas[i];
						
						var chavePrimaria = fatura['chavePrimaria'];
						var saldoCorrigido = fatura['saldoCorrigido'];
						
						$("#fatura_saldo_corrigido_" + chavePrimaria).text(saldoCorrigido);
						
					}
				} , async: false
			}
		);
		
	}
	
	addLoadEvent(init);
	addLoadEvent(gerarRelatorioReciboQuitacao);

</script>	

<form:form method="post" action="incluirRecebimento" name="recebimentoForm">
	<input name="acao" type="hidden" id="acao" value="incluirRecebimento">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${recebimentoVO.idPontoConsumo}">
	<input name="fluxoBaixaPorDacao" type="hidden" id="fluxoBaixaPorDacao" value="${recebimentoVO.fluxoBaixaPorDacao}">

	<fieldset id="incluirRecebimentoPesquisa" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<fieldset id="pesquisarCliente" class="colunaEsq">
				<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${recebimentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
				<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
					<jsp:param name="idCampoIdCliente" value="idCliente"/>
					<jsp:param name="idCliente" value="${recebimentoForm.map.idCliente}"/>
					<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
					<jsp:param name="nomeCliente" value="${recebimentoForm.map.nomeCompletoCliente}"/>
					<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
					<jsp:param name="documentoFormatadoCliente" value="${recebimentoForm.map.documentoFormatado}"/>
					<jsp:param name="idCampoEmail" value="emailCliente"/>
					<jsp:param name="emailCliente" value="${recebimentoForm.map.emailCliente}"/>
					<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
					<jsp:param name="enderecoFormatadoCliente" value="${recebimentoForm.map.enderecoFormatadoCliente}"/>
					<jsp:param name="funcaoParametro" value="ativarBotaoPesquisar"/>
					<jsp:param name="possuiRadio" value="true"/>
				</jsp:include>
			</fieldset>
		
			<fieldset id="pesquisarImovel" class="colunaDir">
				<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${recebimentoForm.map.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
				<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
				<div class="pesquisarImovelFundo">
					<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
					<input name="idImovel" type="hidden" id="idImovel" value="${recebimentoVO.idImovel}">
					<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${recebimentoForm.map.nomeFantasiaImovel}">
					<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${recebimentoForm.map.matriculaImovel}">
					<input name="numeroImovel" type="hidden" id="numeroImovel" value="${recebimentoForm.map.numeroImovel}">
					<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${recebimentoForm.map.cidadeImovel}">
					<input name="condominio" type="hidden" id="condominio" value="${recebimentoForm.map.condominio}">
	
					<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
					<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Nome Fantasia:</label>
					<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${recebimentoForm.map.nomeFantasiaImovel}"><br />
					<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
					<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${recebimentoForm.map.matriculaImovel}"><br />
					<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
					<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${recebimentoForm.map.numeroImovel}"><br />
					<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
					<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${recebimentoForm.map.cidadeImovel}"><br />
					<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${recebimentoForm.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${recebimentoForm.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
				</div>
			</fieldset>
		
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<input class="bottonRightCol2" id="botaoPesquisar" type="button"   disabled="disabled" value="Pesquisar" onclick="pesquisar();">
				<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
			</fieldset>
		</fieldset>
		
		<c:if test="${listaContratoPontoConsumos ne null}">
		<hr class="linhaSeparadora1" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaContratoPontoConsumos" sort="list" id="contratoPontoConsumo" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirFaturasRecebimento">
		     <display:column sortable="true" title="Matr�cula do Imovel" style="width: 120px">
				<a href='javascript:pesquisarFaturasPontoConsumo(<c:out value='${contratoPontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contratoPontoConsumo.imovel.chavePrimaria}'/> 
	        	</a>
	    	 </display:column>
		     <display:column sortable="true" title="Im�vel - Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left">
		     	<a href='javascript:pesquisarFaturasPontoConsumo(<c:out value='${contratoPontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contratoPontoConsumo.codigoPontoConsumo} - '/> <c:out value="${contratoPontoConsumo.descricao}"/>
				</a>
		     </display:column>
		 	<display:column sortable="false" title="Situa��o" style="width: 100px">
		 		<a href='javascript:pesquisarFaturasPontoConsumo(<c:out value='${contratoPontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:choose>
						<c:when test="${contratoPontoConsumo.habilitado eq true}">
							<c:out value='Ativo'/>
						</c:when>
						<c:otherwise>
							<c:out value='Inativo'/>
						</c:otherwise>
					</c:choose>
				</a>
	    	</display:column>
	    	<display:column sortable="false" title="Segmento" style="width: 100px">
	    		<a href='javascript:pesquisarFaturasPontoConsumo(<c:out value='${contratoPontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${contratoPontoConsumo.segmento.descricao}"/>
				</a>
	    	</display:column>
	    	<display:column sortable="false" title="Ramo de Atua��o" style="width: 150px">
	    		<a href='javascript:pesquisarFaturasPontoConsumo(<c:out value='${contratoPontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${contratoPontoConsumo.ramoAtividade.descricao}"/>
				</a>
	    	</display:column>
<%-- 			<display:column sortable="false" title="C�digo</br>Legado"> --%>
<%-- 	    		<a href='javascript:pesquisarRecebimentosPontoConsumo(<c:out value='${pontoConsumoContrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span> --%>
<%-- 					<c:out value="${contratoPontoConsumo.codigoLegado}"/> --%>
<!-- 				</a> -->
<%-- 	    	</display:column> --%>
		</display:table>
	</c:if>
	
	<c:if test="${listaFatura ne null}">
	
		<hr class="linhaSeparadora1" />			
		
		<fieldset id="recebimentoDados" class="conteinerBloco">
			<fieldset class="coluna">			
				<label class="rotulo campoObrigatorio" id="rotuloArrecador" for="modelo"><span class="campoObrigatorioSimbolo">* </span>Arrecadador:</label>
				<select name="idArrecadador" id="idArrecadador" class="campoSelect" onchange="carregarTiposConvenio(this); mostrarACTipoConvenio();">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaArrecadador}" var="arrecadador">
						<option title="<c:choose><c:when test="${arrecadador.banco != null}"><c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.banco.nome}"/></c:when><c:when test="${arrecadador.cliente != null}"><c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.cliente.nome}"/></c:when></c:choose>" value="<c:out value="${arrecadador.chavePrimaria}"/>" <c:if test="${recebimentoVO.idArrecadador == arrecadador.chavePrimaria}">selected="selected"</c:if>>
							<c:choose>
								<c:when test="${arrecadador.banco != null}">
								    <c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.banco.nome}"/>
								</c:when>
								<c:when test="${arrecadador.cliente != null}">
								    <c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.cliente.nome}"/>
								</c:when>
							</c:choose>
						</option>
				    </c:forEach>
				</select>
				<br />

				<label class="rotulo campoObrigatorio" id="rotuloFormaArrecadacao" for="formaArrecadacao"><span class="campoObrigatorioSimbolo">* </span>Tipo de Conv�nio:</label>
				<select name="idFormaArrecadacao" id="formaArrecadacao" class="campoSelect" onchange="mostrarACTipoConvenio();">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaFormaArrecadacao}" var="formaArrecadacao">
						<option value="<c:out value="${formaArrecadacao.chavePrimaria}"/>" <c:if test="${recebimentoVO.idFormaArrecadacao == formaArrecadacao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${formaArrecadacao.descricao}"/>
						</option>
				    </c:forEach>
				</select>
				
				<label class="rotulo" for="bancoArrecadadorTexto" id="bancoArrecadadorTextoLabel">Banco:</label>
				<input class="campoDesabilitado" type="text" id="bancoArrecadadorTexto" name="bancoArrecadadorTexto"  maxlength="30" size="30" disabled="disabled" value="${recebimentoForm.map.bancoArrecadadorTexto}">
				<label class="rotulo" for="agenciaContaArrecadadorTexto" id="agenciaContaArrecadadorTextoLabel">Ag/CC:</label>
				<input class="campoDesabilitado" type="text" id="agenciaContaArrecadadorTexto" name="agenciaContaArrecadadorTexto"  maxlength="12" size="12" disabled="disabled" value="${recebimentoForm.map.agenciaContaArrecadadorTexto}">
				
				
				<label class="rotulo campoObrigatorio" id="rotulodataRecebimentoInclusao" for="dataRecebimentoInclusao" ><span class="campoObrigatorioSimbolo">* </span>Data do Recebimento:</label>
				<input readonly="readonly" class="campoData campoHorizontal" type="text" id="dataRecebimentoInclusao" name="dataRecebimentoInclusao" maxlength="10" value="${recebimentoVO.dataRecebimentoInclusao}">
				
				<label class="rotulo campoObrigatorio" id="rotuloValorRecebimento" for="valorRecebimento" ><span class="campoObrigatorioSimbolo">* </span>Valor do Recebimento:</label>
				<input class="campoTexto campoValorReal" type="text" id="valorRecebimento" name="valorRecebimento" size="13" maxlength="13" value="${recebimentoVO.valorRecebimento}"
						onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
						style="margin: 0px"/>
			</fieldset>
			
			<fieldset class="colunaFinal">
				<label class="rotulo rotuloVertical" for="observacao">Observa��o:</label>
				<textarea id="observacao" name="observacao" maxlength="300"
				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
				class="campoTexto campoVertical" rows="4" onkeypress="return formatarCampoTextoLivreComLimite(event,this,300);"><c:out value="${recebimentoVO.observacao}"/></textarea>
				<br />
			</fieldset>
		</fieldset>

			<hr class="linhaSeparadora1" />
			<fieldset class="conteinerBloco">
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaFatura" sort="list" id="faturaVO" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirFaturasRecebimento">
					 <display:column style="text-align: center; width: 25px" sortable="false" > 
	         			<input type="checkbox" name="chavesPrimariasFaturas" id="chaveFatura${faturaVO.fatura.chavePrimaria}" value="${faturaVO.fatura.chavePrimaria}">
	        		 </display:column>
				     <display:column title="Documento">
						<c:out value='${faturaVO.fatura.chavePrimaria}'/>
			    	 </display:column>
				     <display:column title="Ciclo/<br />Refer�ncia" style="width: 75px">
						<c:out value='${faturaVO.fatura.cicloReferenciaFormatado}'/>
				     </display:column>
				 	<display:column sortable="false" title="Valor (R$)" style="width: 150px">
						<fmt:formatNumber value="${faturaVO.fatura.valorTotal}" minFractionDigits="2" maxFractionDigits="2" />
			    	</display:column>
			    	<display:column sortable="false" title="Saldo (R$)" style="width: 150px">
			   			<fmt:formatNumber value="${faturaVO.saldo}" minFractionDigits="2" maxFractionDigits="2" />
			   		</display:column>
			    	<display:column sortable="false" title="Saldo Corrigido (R$)" style="width: 150px">
			    		<span id="fatura_saldo_corrigido_${faturaVO.fatura.chavePrimaria}">
				   			<fmt:formatNumber value="${faturaVO.saldoCorrigido}" minFractionDigits="2" maxFractionDigits="2" />
			    		</span>
			   		</display:column>
				 	<display:column sortable="false" title="Data de<br />Emiss�o" style="width: 75px">
						<c:out value='${faturaVO.fatura.dataEmissaoFormatada}'/>
			    	</display:column>
			    	<display:column sortable="false" title="Data de<br />Vencimento" style="width: 75px">
						<c:out value='${faturaVO.fatura.dataVencimentoFormatada}'/>
			    	</display:column>
			    	<display:column sortable="false" title="Tipo de Documento" style="width: 150px">
						<c:out value="${faturaVO.fatura.tipoDocumento.descricao}"/>
			    	</display:column>
				</display:table>
			</fieldset>
		</c:if>
	</fieldset>
	
				
		<fieldset class="conteinerBotoes"> 
			<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
			<c:if test="${listaFatura ne null}">
				<vacess:vacess param="incluirRecebimento">
			    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" value="Incluir"  onclick="incluirRecebimento()" type="submit">
			    </vacess:vacess>
		  	</c:if>
		</fieldset>
		
	<token:token></token:token>
	
</form:form> 