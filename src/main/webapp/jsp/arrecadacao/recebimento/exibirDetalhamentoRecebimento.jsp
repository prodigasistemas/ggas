<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<h1 class="tituloInterno">Detalhar Recebimentos<a class="linkHelp" href="<help:help>/detalhamentodorecebimento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Detalhes do Recebimento <span class="destaqueOrientacaoInicial"></span></p>
<form:form method="post" action="exibirDetalhamentoRecebimento" enctype="multipart/form-data" id="recebimentoForm" name="recebimentoForm">

<script>
	
	function cancelar(){	
		submeter("recebimentoForm", "pesquisarRecebimento");
	}
	
</script>

<input type="hidden" id="acao" name="acao" value="exibirDetalhamentoRecebimento">
<input type="hidden" id="postBack" name="postBack" value="true">
<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="${recebimentoVO.chavePrimaria}">
<input type="hidden" id="dataRecebimentoInicial" name="dataRecebimentoInicial" value="${recebimentoVO.dataRecebimentoInicial}">
<input type="hidden" id="dataRecebimentoFinal" name="dataRecebimentoFinal" value="${recebimentoVO.dataRecebimentoFinal}">
<input type="hidden" id="idCliente" name="idCliente" value="${recebimentoVO.idCliente}">
<input type="hidden" id="idImovel" name="idImovel" value="${recebimentoVO.idImovel}">
<%-- <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${recebimentoVO.chavesPrimarias}"> --%>

<fieldset id="detalharRecebimento" class="detalhamento">
	<fieldset class="conteinerBloco">
		<fieldset class="colunaEsq">
			<div class="pesquisarClienteFundo">
				<label class="rotulo" id="rotuloCliente">Cliente:</label>				
				<span class="itemDetalhamento" id="itemDetalhamentoCliente"><c:out value="${cliente.nome}"/></span><br />
				<label class="rotulo" id="rotuloCpfCnpjTexto">CPF/CNPJ:</label>
				<span class="itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
				<label class="rotulo" id="rotuloEnderecoTexto">Endere�o:</label>
				<span class="itemDetalhamento" id="enderecoFormatadoTexto"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
				<label class="rotulo" id="rotuloEmailClienteTexto">E-mail:</label>
				<span class="itemDetalhamento"><c:out value="${cliente.emailPrincipal}"/></span>
			</div>		
		</fieldset>
		
		<fieldset class="colunaEsq2">
			<label class="rotulo" for="principal1">Arrecadador:</label>
			<span class="itemDetalhamento" id="recebimentoArrecadador">
				<c:out value="${descricaoArrecadador}"/>
			</span><br />
			
			<label class="rotulo">Data do Recebimento:</label>
			<span class="itemDetalhamento" id="recebimentoData"> 
				<c:out value="${dataRecebimentoInclusao}"/>
			</span><br />
			
			<label class="rotulo">Devolvido:</label>
			<span class="itemDetalhamento" id="recebimentoTipoDevolucao"> 
				<c:if test="${not empty descricaoTipoDevolucao}">
					<c:out value="Sim"/>
				</c:if>
				<c:if test="${empty descricaoTipoDevolucao}">
					<c:out value="N�o"/>
				</c:if>					
			</span><br />
			
			<label class="rotulo">Aviso Banc�rio:</label>
			<span class="itemDetalhamento" id="recebimentoAvisoBancario"> 
				<c:out value="${descricaoAvisoBancario}"/>
			</span><br />
			
			<label class="rotulo">Forma de Arrecada��o:</label>
			<span class="itemDetalhamento" id="recebimentoFormaArrecadacao"> 
				<c:out value="${formaArrecadacao.descricao}"/>
			</span><br />
				
			<label class="rotulo">Valor Recebimento (R$):</label>
			<span class="itemDetalhamento" id="recebimentoValor"> 
				<c:out value="${valorRecebimento}"/>
			</span>
			
			<label class="rotulo">Situa��o:</label>
			<span class="itemDetalhamento" id="recebimentoDescricaoRecebimentoSituacao"> 
				<c:out value="${descricaoRecebimentoSituacao}"/>
			</span>
			
			<label class="rotulo">Observa��o:</label>
			<span class="itemDetalhamento" id="recebimentoObservacao"> 
				<c:out value="${observacao}"/>
			</span>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
	
	<fieldset id="detalharRecebimentoDetalhesDocumento" class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Detalhes do Documento:</legend>
		<fieldset class="coluna detalhamentoColunaLarga">		
			<label class="rotulo" for="principal1">N� Documento:</label>
			<span class="itemDetalhamento" id="documentoNumero"><c:out value="${documentoNumero}"/></span><br />
			
			<label class="rotulo">Ciclo / Refer�ncia:</label>
			<span class="itemDetalhamento" id="documentoCicloReferencia"><c:out value="${documentoCicloReferencia}"/></span><br />
			
			<label class="rotulo">Valor (R$):</label>
			<span class="itemDetalhamento" id="documentoValor"><c:out value="${documentoValor}"/></span><br />
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo">Data Emiss�o:</label>
			<span class="itemDetalhamento" id="documentoDataEmissao"><c:out value="${documentoDataEmissao}"/></span><br />
			
			<label class="rotulo">Vencimento:</label>
			<span class="itemDetalhamento" id="documentoVencimento"><c:out value="${documentoVencimento}"/></span><br />
				
			<label class="rotulo">Tipo Documento:</label>
			<span class="itemDetalhamento" id="documentoTipo"><c:out value="${documentoTipo}"/></span><br />
			
			<label class="rotulo">Ponto de Consumo:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno2" id="documentoPontoConsumo">
				<c:out value="${pontoConsumoDescricaoFormatada}"/>
			</span><br />
		</fieldset>
	</fieldset>
</fieldset>

<hr class="linhaSeparadoraDetalhamento">
	
	<c:if test="${listaCreditoDebitoNegociado ne null and not empty listaCreditoDebitoNegociado}">
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Cr�ditos d�bitos gerados: </legend>	
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebitoNegociado" sort="list" id="obj" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			   	
			   	<display:column title="Tipo de Lan�amento" style="width: 250px">
					<c:choose>
						<c:when test="${obj.creditoOrigem eq null}">D�bito</c:when>
						<c:otherwise>Cr�dito</c:otherwise>
					</c:choose>
				</display:column>
			   	
			   	<display:column property="rubrica" title="Rubrica" style="width: 250px"/>    
			   	
			   	<display:column title="Valor (R$)" style="width: 250px">
			   		<fmt:formatNumber value="${obj.valor}" minFractionDigits="2"/>
			   	</display:column>
			   	
			   	<display:column title="Situa��o">
					<c:out value="${obj.situacao}"/>
				</display:column>
			   			   		
		    </display:table>
		    			    
		</fieldset>
	</c:if>
	
	

<fieldset id="detalhamentoRecebimentoDocumento">
		
			<br />
			
		</fieldset>
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">    
</fieldset>

</form:form>
