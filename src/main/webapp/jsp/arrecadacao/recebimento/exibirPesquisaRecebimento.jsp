<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	

<h1 class="tituloInterno">Pesquisar Recebimento<a class="linkHelp" href="<help:help>/consultadosrecebimentos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo, o intervalo de emiss�o de Recebimentos e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarRecebimento" id="recebimentoForm" name="recebimentoForm">

	<script language="javascript">
	
	$(document).ready(function(){
			
			$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

			//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
			//Estado Inicial desabilitado
			$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
			//Dispara o evento no click do radiobutton.
			$("#indicadorPesquisaCliente").click(habilitaCliente);
			$("#indicadorPesquisaImovel").click(habilitaImovel);

			$("#dataRecebimentoInicial,#dataRecebimentoFinal").bind('keypress blur focusin', function() {
				validarDatasLiberarBotao("dataRecebimentoInicial","dataRecebimentoFinal","botaoPesquisar");
			});
						
		});		

		//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
		function habilitaCliente(){
			$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
			$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
		};
		function habilitaImovel(){
			$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
			$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
		};
		
		function estornarRecebimento(){
			
			var selecao = verificarSelecao();
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_ESTORNAR"/>');
				if(retorno == true) {
					submeter('recebimentoForm', 'estornarRecebimento');
				}
		    }
		}
		
		function alterarRecebimento(){
			
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('recebimentoForm', 'exibirAlteracaoRecebimento');
		    }
		}
		
		function detalharRecebimento(chave){
			
			$("#recebimentoInicial").prop("name", "dataRecebimentoInicial");
			$("#recebimentoFinal").prop("name", "dataRecebimentoFinal");
			$("#dataRecebimentoInicial").prop("name", "");
			$("#dataRecebimentoFinal").prop("name", "");
			
			document.forms[0].chavePrimaria.value = chave;
			submeter("recebimentoForm", "exibirDetalhamentoRecebimento");
		}
	
		function pesquisarRecebimentosPontoConsumo(chave) {
			document.forms[0].idPontoConsumo.value = chave;
			submeter("recebimentoForm", "pesquisarRecebimento");
		}
		
		function incluir() {
			location.href = '<c:url value="/exibirInclusaoRecebimento"/>';
		}
		
		function baixaPorDacao() {
			document.forms[0].fluxoBaixaPorDacao.value = true;
			submeter("recebimentoForm", "exibirInclusaoRecebimento");
		}

		function alterar(chave) {
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("recebimentoForm", "exibirAlteracaoRecebimento");
		    }
		}
	
		function desabilitarPesquisaOposta(elem){
			var selecao = getCheckedValue(elem);
			if(selecao == "indicadorPesquisaImovel"){
				pesquisarImovel(true);
				pesquisarCliente(false);
				limparCamposPesquisa();
			}else{
				pesquisarImovel(false);
				pesquisarCliente(true);
				limparCamposPesquisa();
			}	
		}
		
		function pesquisarImovel(valor){
			document.getElementById('botaoPesquisarImovel').disabled = !valor;
		}
		
		function pesquisarCliente(valor){
			document.getElementById('botaoPesquisarCliente').disabled = !valor;
		}
		
		function limparCamposPesquisa(){
			document.getElementById('idCliente').value = "";
			document.getElementById('nomeCompletoCliente').value = "";
			document.getElementById('documentoFormatado').value = "";
			document.getElementById('enderecoFormatadoCliente').value = "";
			document.getElementById('emailCliente').value = "";
			
			document.getElementById('nomeClienteTexto').value = "";
			document.getElementById('documentoFormatadoTexto').value = "";
			document.getElementById('enderecoFormatadoTexto').value = "";
			document.getElementById('emailClienteTexto').value = "";
			
			document.getElementById('idImovel').value = "";
			document.getElementById('nomeFantasiaImovel').value = "";
			document.getElementById('matriculaImovel').value = "";
			document.getElementById('numeroImovel').value = "";
			document.getElementById('cidadeImovel').value = "";
			document.getElementById('condominio').value = "";
			
			document.getElementById('nomeImovelTexto').value = "";
			document.getElementById('matriculaImovelTexto').value = "";
			document.getElementById('numeroImovelTexto').value = "";
			document.getElementById('cidadeImovelTexto').value = "";
			document.getElementById('indicadorCondominioImovelTexto1').checked = false;
			document.getElementById('indicadorCondominioImovelTexto2').checked = false;
	
		}
	
		function limparFormulario() {
			
			var recebimentoInicial = document.getElementById('dataRecebimentoInicial').value;
			var recebimentoFinal = document.getElementById('dataRecebimentoFinal').value;
			$("#recebimentoInicial").val(recebimentoInicial);
			$("#recebimentoFinal").val(recebimentoFinal);
			
			document.getElementById('arrecadador').value = "-1";
			document.getElementById('anoMesContabilInicial').value = "";
			document.getElementById('anoMesContabilFinal').value = "";
			document.getElementById('dataRecebimentoInicial').value = "";
			document.getElementById('dataRecebimentoFinal').value = "";
			document.getElementById('dataDocumentoInicial').value = "";
			document.getElementById('dataDocumentoFinal').value = "";
			//animatedcollapse.hide('pesquisarClienteImovel');
			$( "#pesquisarClienteImovel" ).slideUp();
			document.getElementById('recebimentoSituacao').value = "-1";
			document.getElementById('tipoDocumento').value = "-1";
			limparCamposPesquisa();
		}
		
		function exibirPopupPesquisaImovel() {
			popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
		}
		
		function selecionarImovel(idSelecionado){
			var idImovel = document.getElementById("idImovel");
			var matriculaImovel = document.getElementById("matriculaImovel");
			var nomeFantasia = document.getElementById("nomeFantasiaImovel");
			var numeroImovel = document.getElementById("numeroImovel");
			var cidadeImovel = document.getElementById("cidadeImovel");
			var indicadorCondominio = document.getElementById("condominio");
			
			if(idSelecionado != '') {				
				AjaxService.obterImovelPorChave( idSelecionado, {
		           	callback: function(imovel) {	           		
		           		if(imovel != null){  	           			        		      		         		
			               	idImovel.value = imovel["chavePrimaria"];
			               	matriculaImovel.value = imovel["chavePrimaria"];		               	
			               	nomeFantasia.value = imovel["nomeFantasia"];
			               	numeroImovel.value = imovel["numeroImovel"];
			               	cidadeImovel.value = imovel["cidadeImovel"];
			               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	}
		        	}, async:false}
		        	
		        );	        
		    } else {
		   		idImovel.value = "";
		    	matriculaImovel.value = "";
		    	nomeFantasia.value = "";
				numeroImovel.value = "";
		          	cidadeImovel.value = "";
		          	indicadorCondominio.value = "";
		   	}
		
			document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
			document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
			document.getElementById("numeroImovelTexto").value = numeroImovel.value;
			document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
			if(indicadorCondominio.value == 'true') {
				document.getElementById("indicadorCondominioImovelTexto1").checked = true;
			} else {
				document.getElementById("indicadorCondominioImovelTexto2").checked = true;
			}
		}
		
		function exibirRecebimentosPontoConsumo(idPonto) {
			document.getElementById('idCliente').value = "";
			document.getElementById('idPontoConsumo').value = idPonto;
			submeter('recebimentoForm', 'exibirRecebimento');
		}
		
		function init() {
			<c:choose>
				<c:when test="${recebimentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
					animatedcollapse.show('pesquisarClienteImovel');
					pesquisarImovel(false);
					habilitaCliente();
				</c:when>
				<c:when test="${recebimentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
					animatedcollapse.show('pesquisarClienteImovel');
					pesquisarCliente(false);
					habilitaImovel();
				</c:when>
				<c:otherwise>
					/* animatedcollapse.hide('pesquisarClienteImovel'); */
					animatedcollapse.addDiv('pesquisarClienteImovel', 'fade=0,speed=400,hide=1');
					pesquisarCliente(false);
					pesquisarImovel(false);
				</c:otherwise>
			</c:choose>
			var idCliente = '${recebimentoVO.idCliente}';
			var idImovel = '${recebimentoVO.idImovel}';
			
			if(idCliente != ''){
				document.getElementById('indicadorPesquisaCliente').checked = true;
				selecionarCliente(idCliente);
			}
			if(idImovel != ''){
				document.getElementById('indicadorPesquisaImovel').checked = true;
				selecionarImovel(idImovel);
			}
			
			validarDatasLiberarBotao("dataRecebimentoInicial","dataRecebimentoFinal","botaoPesquisar");
		}	
	
		function imprimirReciboQuitacao(idRecebimento) {
			document.forms[0].chavePrimaria.value = idRecebimento;
			submeter("recebimentoForm", "imprimirReciboQuitacao");
		}
	
		function gerarRelatorioReciboQuitacao() {
			<c:if test="${not empty gerarRelatorioReciboQuitacao && gerarRelatorioReciboQuitacao == true}">
				/* location.href = '<c:url value="/gerarRelatorioReciboQuitacao"/>'; */
				submeter("recebimentoForm", "gerarRelatorioReciboQuitacao");
			</c:if>
		}
		
		function showHidePesquisaClienteImovel(){
			$( "#pesquisarClienteImovel" ).slideToggle();
		}
		
		addLoadEvent(init);
		addLoadEvent(gerarRelatorioReciboQuitacao);
		
		//animatedcollapse.addDiv('pesquisarClienteImovel', 'fade=0,speed=400,hide=1');
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarRecebimento">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<!-- <input name="chavesPrimarias" type="text" id="chavesPrimarias" > -->
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="fluxoBaixaPorDacao" type="hidden" id="fluxoBaixaPorDacao">
	<input type="hidden" id="recebimentoInicial" name="" value="${recebimentoVO.dataRecebimentoInicial}">
	<input type="hidden" id="recebimentoFinal" name="" value="${recebimentoVO.dataRecebimentoFinal}">
	
		<fieldset id="pesquisaRecebimento" class="conteinerPesquisarIncluirAlterar">		
			<fieldset class="coluna">	
				<label class="rotulo" id="rotuloArrecador" for="modelo">Arrecadador:</label>
				<select name="idArrecadador" id="arrecadador" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaArrecadador}" var="arrecadador">
						<option value="<c:out value="${arrecadador.chavePrimaria}"/>" <c:if test="${recebimentoVO.idArrecadador == arrecadador.chavePrimaria}">selected="selected"</c:if>>
							<c:choose>
								<c:when test="${arrecadador.banco != null}">
								    <c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.banco.nome}"/>
								</c:when>
								<c:when test="${arrecadador.cliente != null}">
								    <c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.cliente.nome}"/>
								</c:when>
							</c:choose>
						</option>
				    </c:forEach>
				</select>
				<br />
				
				<label class="rotulo rotulo2Linhas" id="rotuloAnoMesContabil" for="anoMesContabil" >Per�odo Refer�ncia Cont�bil:</label>
				<input class="campoTexto campo2Linhas campoHorizontal" type="text" name="anoMesContabilInicial" id="anoMesContabilInicial" maxlength="6" size="6" value="${recebimentoVO.anoMesContabilInicial}" onkeypress="return formatarCampoInteiro(event,6);"/>
				<label class="rotuloEntreCampos" for="anoMesContabilFinal">a</label>
				<input class="campoTexto campo2Linhas campoHorizontal" type="text" name="anoMesContabilFinal" id="anoMesContabilFinal" maxlength="6" size="6" value="${recebimentoVO.anoMesContabilFinal}" onkeypress="return formatarCampoInteiro(event,6);"/>
				<label class="rotuloInformativo rotuloInformativo2Linhas rotuloHorizontal" for="anoMesContabilFinal">mmaaaa</label>
				<br />
				
				<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" ><span class="campoObrigatorioSimbolo">* </span>Intervalo de Emiss�o de Recebimentos:</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataRecebimentoInicial" name="dataRecebimentoInicial" maxlength="10" value="${recebimentoVO.dataRecebimentoInicial}">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataRecebimentoFinal" name="dataRecebimentoFinal" maxlength="10" value="${recebimentoVO.dataRecebimentoFinal}">
				<br />
				
				<label class="rotulo rotulo2Linhas" id="rotuloIntervaloEmissaoDocumentos" for="intervaloEmissaoDocumentos" >Intervalo de Emiss�o de Documentos:</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataDocumentoInicial" name="dataDocumentoInicial" maxlength="10" value="${recebimentoVO.dataDocumentoInicial}">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloEmissaoDocumentos" for="dataParcelamentoFinal">a</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataDocumentoFinal" name="dataDocumentoFinal" maxlength="10" value="${recebimentoVO.dataDocumentoFinal}">
			</fieldset>
			
			<fieldset class="colunaFinal">
				<label class="rotulo" id="rotuloRecebimentoSitucao" for="modelo">Situa��o:</label>
				<select name="idRecebimentoSituacao" id="recebimentoSituacao" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaRecebimentoSituacao}" var="recebimentoSituacao">
						<option value="<c:out value="${recebimentoSituacao.chavePrimaria}"/>" <c:if test="${recebimentoVO.idRecebimentoSituacao == recebimentoSituacao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${recebimentoSituacao.descricao}"/>
						</option>
				    </c:forEach>
				</select>
				<br />
		
				<label class="rotulo" id="rotuloTipoDocumento" for="modelo">Tipo de Documento:</label>
				<select name="idTipoDocumento" id="tipoDocumento" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoDocumento}" var="tipoDocumento">
						<option value="<c:out value="${tipoDocumento.chavePrimaria}"/>" <c:if test="${recebimentoVO.idTipoDocumento == tipoDocumento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoDocumento.descricao}"/>
						</option>
				    </c:forEach>
				</select>
		
		<!-- 	<br />
				<label class="rotulo" id="xxx" for="modelo">Faixa de Valor Pago:</label>
				<select name="idxxx" id="xxx" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaxxx}" var="xxx">
						<option value="<c:out value="${xxx.chavePrimaria}"/>" <c:if test="${recebimentoForm.map.idxxx == xxx.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${xxx.descricao}"/>
						</option>
				    </c:forEach>
				</select><br /> -->
			</fieldset>
			
			<a class="linkPesquisaAvancada" onclick="showHidePesquisaClienteImovel();" href="#" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pesquisar por Cliente ou Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
			
			<fieldset id="pesquisarClienteImovel" class="conteinerBloco">			
				<input name="acao" type="hidden" id="acao" value="pesquisarPontosConsumo">
				<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
				<input name="idPontoConsumo" type="hidden" id="idPontoConsumo">
				
				<fieldset id="pesquisarCliente" class="colunaEsq">
					<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${recebimentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
					<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
						<jsp:param name="idCampoIdCliente" value="idCliente"/>
						<jsp:param name="idCliente" value="${recebimentoVO.idCliente}"/>
						<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
						<jsp:param name="nomeCliente" value="${cliente.nome}"/>
						<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
						<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
						<jsp:param name="idCampoEmail" value="emailCliente"/>
						<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
						<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
						<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>
						<jsp:param name="possuiRadio" value="true"/>
					</jsp:include>
				</fieldset>
			
				<fieldset id="pesquisarImovel" class="colunaDir">
					<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${recebimentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
					<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
					<div class="pesquisarImovelFundo">
						<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
						<input name="idImovel" type="hidden" id="idImovel" value="${recebimentoVO.idImovel}">
						<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nome}">
						<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.chavePrimaria}">
						<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
						<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.quadraFace.endereco.cep.municipio.descricao}">
						<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">
		
						<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
						<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
						<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
						<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
						<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
						<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
						<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
						<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
						<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.municipio.descricao}"><br />
						<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
						<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
						<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
					</div>
				</fieldset>		
			</fieldset>
			
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="pesquisarRecebimento">
		    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
		    	</vacess:vacess>		
				<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
			</fieldset>
		</fieldset>
	
	<c:if test="${listaContratoPontoConsumos ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaContratoPontoConsumos" sort="list" id="pontoConsumoContrato" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontosConsumo">
		     <display:column title="Matr�cula do Imovel" style="width: 120px">
				<a href='javascript:pesquisarRecebimentosPontoConsumo(<c:out value='${pontoConsumoContrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${pontoConsumoContrato.imovel.chavePrimaria}'/> 
	        	</a>
	    	 </display:column>
		     <display:column title="Im�vel - Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left">
		     	<a href='javascript:pesquisarRecebimentosPontoConsumo(<c:out value='${pontoConsumoContrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${pontoConsumoContrato.chavePrimaria}"/><c:out value=" - "/><c:out value='${pontoConsumoContrato.imovel.nome}'/><c:out value=" - "/><c:out value="${pontoConsumoContrato.descricao}"/>
				</a>
		     </display:column>
		 	<display:column sortable="false" title="Situa��o" style="width: 100px">
		 		<a href='javascript:pesquisarRecebimentosPontoConsumo(<c:out value='${pontoConsumoContrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:choose>
						<c:when test="${pontoConsumoContrato.habilitado eq true}">
							<c:out value='Ativo'/>
						</c:when>
						<c:otherwise>
							<c:out value='Inativo'/>
						</c:otherwise>
					</c:choose>
				</a>
	    	</display:column>
	    	<display:column sortable="false" title="Segmento" style="width: 100px">
	    		<a href='javascript:pesquisarRecebimentosPontoConsumo(<c:out value='${pontoConsumoContrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${pontoConsumoContrato.segmento.descricao}"/>
				</a>
	    	</display:column>
	    	<display:column sortable="false" title="Ramo de Atua��o" style="width: 150px">
	    		<a href='javascript:pesquisarRecebimentosPontoConsumo(<c:out value='${pontoConsumoContrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${pontoConsumoContrato.ramoAtividade.descricao}"/>
				</a>
	    	</display:column>
		</display:table>
	</c:if>

	<c:if test="${listaRecebimento ne null}">
		<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaRecebimento" sort="list" id="recebimentoVO" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarRecebimento">
	        <display:column style="width: 25px; text-align: center;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	         	<input type="checkbox" name="chavesPrimarias" value="${recebimentoVO.recebimento.chavePrimaria}">
	        </display:column>
	        <display:column sortable="true" sortProperty="recebimento.cliente.nome" style="text-align: left" headerClass="tituloTabelaEsq" title="Cliente" >
	            <a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	            	<c:out value="${recebimentoVO.recebimento.cliente.nome}"/>
	            </a>
	        </display:column>
	        <display:column style="width: 80px" title="Documento" >
	        	<a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	        		<c:choose>
						<c:when test="${recebimentoVO.recebimento.faturaGeral != null and recebimentoVO.recebimento.recebimentoSituacao.descricao != 'ESTORNADO' }">
					    	<c:out value="${recebimentoVO.recebimento.faturaGeral.faturaAtual.chavePrimaria}"/>
						</c:when>
						<c:otherwise>
							--
						</c:otherwise>	
					</c:choose>
				</a>
	        </display:column>
	        <display:column sortable="true" sortProperty="recebimento.anoMesContabilFormatado" style="width: 75px" title="Refer�ncia" >
	            <a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	            	<c:choose>
						<c:when test="${recebimentoVO.recebimento.faturaGeral != null and recebimentoVO.recebimento.recebimentoSituacao.descricao != 'ESTORNADO'}">
							<c:out value="${recebimentoVO.recebimento.faturaGeral.faturaAtual.cicloReferenciaFormatado}"/>
						</c:when>
						<c:otherwise>
							--
						</c:otherwise>	
	            	</c:choose>
	            	
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="recebimento.dataRecebimentoFormatada" style="width: 80px" title="Data do<br />Recebimento" >
	            <a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	            	<c:out value="${recebimentoVO.recebimento.dataRecebimentoFormatada}"/>
	            </a>
	        </display:column>
	        <display:column style="width: 100px; text-align: right" title="Valor do<br />Documento (R$)" >
	        	<a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	        		<c:choose>
						<c:when test="${recebimentoVO.recebimento.faturaGeral != null and recebimentoVO.recebimento.recebimentoSituacao.descricao != 'ESTORNADO'}">
					    	<c:set var="valorDocumento" value="${recebimentoVO.recebimento.faturaGeral.faturaAtual.valorTotal}"/>
					    	<fmt:formatNumber value="${recebimentoVO.recebimento.faturaGeral.faturaAtual.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
						</c:when>
						<c:otherwise>
							<c:set var="valorDocumento" value=""/>
							--
						</c:otherwise>
					</c:choose>
				</a>
	        </display:column>
	        <display:column style="width: 100px; text-align: right" title="Valor do<br />Recebimento (R$)" >
	            <a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	            	<c:if test="${not empty valorDocumento and valorDocumento lt recebimentoVO.recebimento.valorRecebimento}">
	            		(+)	            
	            	</c:if>
	            	<c:if test="${not empty valorDocumento and valorDocumento gt recebimentoVO.recebimento.valorRecebimento}">
	            		(-)	            
	            	</c:if>
	            	<fmt:formatNumber value="${recebimentoVO.recebimento.valorRecebimento}" minFractionDigits="2" maxFractionDigits="2"/>
	            </a>
	        </display:column>
	        <display:column style="width: 100px; text-align: right" title="Saldo Total (R$)" >
	            <a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	                <fmt:formatNumber value="${recebimentoVO.saldo}" minFractionDigits="2"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="recebimento.recebimentoSituacao.descricao" style="width: 160px" title="Situa��o" >
	            <a href='javascript:detalharRecebimento(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	            	<c:out value="${recebimentoVO.recebimento.recebimentoSituacao.descricao}"/>
	            </a>
	        </display:column>
	        <display:column style="width: 40px" title="Recibo" >
	        	<a href='javascript:imprimirReciboQuitacao(<c:out value='${recebimentoVO.recebimento.chavePrimaria}'/>);'>
		    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir Recibo de Quita��o" title="Imprimir Recibo de Quita��o" border="0">
		    	</a>
	        </display:column>
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaRecebimento}">
			<vacess:vacess param="exibirAlteracaoRecebimento">
				<input name="button" value="Alterar" class="bottonRightCol2" id="botaoAlterar" onclick="alterar()" type="button">
			</vacess:vacess>
			<vacess:vacess param="estornarRecebimento">
				<input name="buttonRemover" value="Estornar" class="bottonRightCol bottonLeftColUltimo" onclick="estornarRecebimento()" type="button">
			</vacess:vacess>
		</c:if>

		<vacess:vacess param="exibirInclusaoRecebimento">
			<input name="buttonIncluir" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluir()" type="button" />
			<input name="buttonBaixaPorDacao" value="Baixa Por Da��o" class="bottonRightCol2 botaoGrande1" id="botaoBaixaPorDacao" onclick="baixaPorDacao()" type="button" style="margin-right: 7px;" />
		</vacess:vacess>

	</fieldset>
	
</form:form>
