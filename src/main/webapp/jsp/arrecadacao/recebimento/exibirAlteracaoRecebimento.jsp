<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Recebimento<a class="linkHelp" href="<help:help>/inclusoalteraodosrecebimentos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script type="text/javascript">
	
	$(document).ready(function() {

	   	$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	   	

	   	$(".campoValorReal[disabled='disabled']").addClass("campoValorRealDesabilitado");

		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#faturaVO").chromatable({
			width: "891px"
		});
		
	});

	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idArrecadador').value = "-1";
		document.getElementById('formaArrecadacao').value = "-1";
		document.getElementById('observacao').value = "";
		document.getElementById("bancoArrecadadorTexto").value = "";
		document.getElementById("agenciaContaArrecadadorTexto").value = "";
// 		document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";
		
		document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
		document.getElementById('bancoArrecadadorTexto').style.display = "none";
		document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
		document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
		
		document.getElementById("dataRecebimentoInclusao").value = "";
		document.getElementById("valorRecebimento").value = "";
	}

	function pesquisar() {
		submeter('recebimentoForm', 'exibirFaturasRecebimento');
	}

	function pesquisarFaturasPontoConsumo(chave) {
		document.forms[0].idPontoConsumo.value = chave;
		submeter('recebimentoForm', 'exibirFaturasRecebimento');
	}

	function cancelar(){
		
		$("#idArrecadador").prop("name", "arrecadador");
		$("#arrecadador").val("-1");
		$("#indicadorPesquisa").val("");
		$("#idCliente").val("");
		$("#idImovel").val("");
		submeter('recebimentoForm', 'exibirPesquisaRecebimento');
	}

	function exibirRecebimentosPontoConsumo(idPonto) {
		document.getElementById('idCliente').value = "";
		document.getElementById('idPontoConsumo').value = idPonto;
		submeter('recebimentoForm', 'exibirRecebimentos');
	}

	function carregarTiposConvenio(elem) {	
		if(elem.value == '-1' || elem.value == ''){
// 			document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";
			
			document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
			document.getElementById('bancoArrecadadorTexto').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
		}	
		var idArrecadador = elem.value;
		listarTiposConvenioPorArrecadador(idArrecadador);
	}

	function listarTiposConvenioPorArrecadador(idArrecadador){
	  	var selectTiposConvenio = document.getElementById("formaArrecadacao");
	  	var idTipoConvenio = "${recebimentoVO.idFormaArrecadacao}";
	  	selectTiposConvenio.length=0;
	  	
      	var novaOpcao = new Option("Selecione","-1");
      	selectTiposConvenio.options[selectTiposConvenio.length] = novaOpcao;
       	AjaxService.listarTiposConvenioPorArrecadador(idArrecadador,
           	function(tiposConvenio) {            		      		         		
               	for (key in tiposConvenio){
	                var novaOpcao = new Option(tiposConvenio[key], key);
	                if (key == idTipoConvenio){
	                	novaOpcao.selected = true;
	                }
	                selectTiposConvenio.options[selectTiposConvenio.length] = novaOpcao;		            			            	
            	}
        	}
        );
	}

	function mostrarACTipoConvenio(){
		var idArrecadador = document.getElementById("idArrecadador").value;
		var idFormaArrecadacao = document.getElementById("formaArrecadacao").value;
		var bancoArrecadadorTexto = document.getElementById("bancoArrecadadorTexto");
		var agenciaContaArrecadadorTexto = document.getElementById("agenciaContaArrecadadorTexto");
		
		if((idArrecadador != '' && idArrecadador != '-1') && (idFormaArrecadacao != '' && idFormaArrecadacao != '-1')){
			AjaxService.consultarContaArrecadadorSelecionado(idArrecadador,idFormaArrecadacao, {
				callback: function(arrecadador) {	           		
		       		if(arrecadador != null){
		       			bancoArrecadadorTexto.value = arrecadador["nomeBanco"];
		       			agenciaContaArrecadadorTexto.value = arrecadador["contaBancaria"];
// 		       			document.getElementById('bancoAgenciaContaArrecadador').style.display = "block";
		       			
		       			document.getElementById('bancoArrecadadorTextoLabel').style.display = "block";
		    			document.getElementById('bancoArrecadadorTexto').style.display = "block";
		    			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "block";
		    			document.getElementById('agenciaContaArrecadadorTexto').style.display = "block";
		           	}
		    		else {
		    			document.getElementById("bancoArrecadadorTexto").value = '';
		    			document.getElementById("agenciaContaArrecadadorTexto").value = '';
// 		    			document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";
		    			
		    			document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
		    			document.getElementById('bancoArrecadadorTexto').style.display = "none";
		    			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
		    			document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
		    			
			    	} }, async:false}
	    		);
		} else {
			document.getElementById("bancoArrecadadorTexto").value = '';
			document.getElementById("agenciaContaArrecadadorTexto").value = '';
// 			document.getElementById('bancoAgenciaContaArrecadador').style.display = "none";
			
			document.getElementById('bancoArrecadadorTextoLabel').style.display = "none";
			document.getElementById('bancoArrecadadorTexto').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTextoLabel').style.display = "none";
			document.getElementById('agenciaContaArrecadadorTexto').style.display = "none";
		}
	}

	function recarregaDadosCliente(idCliente) {
		submeter('recebimentoForm', 'exibirAlteracaoRecebimento?idCliente='+idCliente);	
	}

	function salvar() {
		<c:forEach items="${recebimentoForm.map.radioPontoConsumo}" var="contratoPontoConsumo">
			if(document.getElementById('chaveContratoPontoConsumo'+'<c:out value="${contratoPontoConsumo}"/>') != undefined){
				document.getElementById('chaveContratoPontoConsumo'+'<c:out value="${contratoPontoConsumo}"/>').checked = valor;
			}	
		</c:forEach>

		var checkFatura = document.getElementsByName('chavesFatura');
		for (var i = 0; i < checkFatura.length; i++) {
			checkFatura[i].checked = checked;
		}		

		<c:forEach items="${chavesPrimariasFaturas}" var="fatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>').checked = valor;
			}	
		</c:forEach>
		
		alert('A altera��o de um recebimento n�o implica em rec�lculo ou cancelamento de acr�scimos por impontualidade gerados. \nATEN��O: Se Multa e Juros forem indevidos, acessar tela de "Cr�dito/D�bito a Realizar" para exclus�o ou Corre��o.');
		//submeter('recebimentoForm', 'alterarRecebimento');

	}

	function verificaFatura(campo) {
		var retorno = 'exibirAlteracaoRecebimento';
		if (document.getElementById("idPontoConsumo").value == campo.value){
			document.getElementById("idPontoConsumo").value = '';
		} else {
			document.getElementById("idPontoConsumo").value = campo.value;
			retorno = retorno + '&idPontoConsumo=' + campo.value ;
		}
		submeter('recebimentoForm', retorno);
	}

	function verificaValorLimite(campo){
		var valorTotalSelecoes = document.getElementById("valorTotalSelecoes");
		var soma = parseFloat(0);
		if (valorTotalSelecoes.value!=''){
			soma = soma + parseFloat(valorTotalSelecoes.value);
		}
		if (campo.checked){
			var valorRecebimento = document.getElementById("valorRecebimento");
			valorRecebimento = valorRecebimento.value.replace('.', '');
			valorRecebimento = valorRecebimento.replace(',', '.'); 
			if (soma > parseFloat(valorRecebimento)){
				campo.checked = false;
				alert('A soma dos valores das fatura selecionadas ultrapassa o valor do recebimento.');
			} else {
				soma = soma + parseFloat(document.getElementById(campo.value).value);
			}
		} else {
			soma = soma - parseFloat(document.getElementById(campo.value).value);
		}
		valorTotalSelecoes.value = soma;
	}
	
	function init() {

		var elem = document.getElementById('idArrecadador');
		if(elem != null){
			carregarTiposConvenio(elem);
			mostrarACTipoConvenio();			
		}	

	}
		
	addLoadEvent(init);	

</script>	

<form:form method="post" action="alterarRecebimento" id="recebimentoForm" name="">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${recebimentoVO.chavePrimaria}">
	<input name="acao" type="hidden" id="acao" value="alterarRecebimento">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${recebimentoForm.map.idPontoConsumo}">
	<input name="valorTotalSelecoes" type="hidden" id="valorTotalSelecoes" value="">
	<input name="dataRecebimento" type="hidden" id="dataRecebimento" value="${recebimentoForm.map.dataRecebimentoInclusao}">
	<input name="arrecadador" type="hidden" id="arrecadador" value="">
	<input name="idCliente" type="hidden" id="idCliente"  value="${recebimentoVO.idCliente}">
	<input name="idImovel" type="hidden" id="idImovel" value="${recebimentoVO.idImovel}">
	<input name="indicadorPesquisa" type="hidden" id="indicadorPesquisa" value="${recebimentoVO.indicadorPesquisa}">


	<fieldset id="altera��oRecebimento" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaRecebimento" class="conteinerPesquisarIncluirAlterar">
			<fieldset id="pesquisarCliente" class="colunaEsq">
				<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
					<jsp:param name="idCampoIdCliente" value="idCliente"/>
					<jsp:param name="idCliente" value="${recebimentoVO.idCliente}"/>
					<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
					<jsp:param name="nomeCliente" value="${cliente.nome}"/>
					<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
					<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
					<jsp:param name="idCampoEmail" value="emailCliente"/>
					<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
					<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
					<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>
					<jsp:param name="possuiRadio" value="true"/>
					<jsp:param name="funcaoParametro" value="recarregaDadosCliente"/>
				</jsp:include>
			</fieldset>

			<fieldset class="colunaFinal">
			<fieldset id="pesquisaRecebimento" class="conteinerPesquisarIncluirAlterar">		
				<label class="rotulo campoObrigatorio" id="rotuloArrecador" for="idArrecadador"><span class="campoObrigatorioSimbolo">* </span>Arrecadador:</label>
				<select name="idArrecadador" id="idArrecadador" class="campoSelect campo2Linhas" onchange="carregarTiposConvenio(this);mostrarACTipoConvenio();">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaArrecadador}" var="arrecadador">
						<option title="<c:choose><c:when test="${arrecadador.banco != null}"><c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.banco.nome}"/></c:when><c:when test="${arrecadador.cliente != null}"><c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.cliente.nome}"/></c:when></c:choose>" value="<c:out value="${arrecadador.chavePrimaria}"/>" <c:if test="${recebimentoVO.idArrecadador == arrecadador.chavePrimaria}">selected="selected"</c:if>>
							<c:choose>
								<c:when test="${arrecadador.banco != null}">
								    <c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.banco.nome}"/>
								</c:when>
								<c:when test="${arrecadador.cliente != null}">
								    <c:out value="${arrecadador.codigoAgenteFormatado} - ${arrecadador.cliente.nome}"/>
								</c:when>
							</c:choose>
						</option>
				    </c:forEach>
				</select>
				<br />
	
				<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloFormaArrecadacao" for="formaArrecadacao"><span class="campoObrigatorioSimbolo">* </span>Forma de Arrecada��o:</label>
				<select name="idFormaArrecadacao" id="formaArrecadacao" class="campoSelect campo2Linhas" onchange="mostrarACTipoConvenio();" >
					<option value="-1">Selecione</option>
					<c:forEach items="${listaFormaArrecadacao}" var="formaArrecadacao">
						<option value="<c:out value="${formaArrecadacao.chavePrimaria}"/>" <c:if test="${recebimentoVO.idFormaArrecadacao == formaArrecadacao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${formaArrecadacao.descricao}"/>
						</option>
				    </c:forEach>
				</select>
				
				<label class="rotulo" for="bancoArrecadadorTexto" id="bancoArrecadadorTextoLabel">Banco:</label>
				<input class="campoDesabilitado" type="text" id="bancoArrecadadorTexto" name="bancoArrecadadorTexto"  maxlength="30" size="30" disabled="disabled" value="${recebimentoForm.map.bancoArrecadadorTexto}">
				<label class="rotulo" for="agenciaContaArrecadadorTexto" id="agenciaContaArrecadadorTextoLabel">Ag/CC:</label>
				<input class="campoDesabilitado" type="text" id="agenciaContaArrecadadorTexto" name="agenciaContaArrecadadorTexto"  maxlength="12" size="12" disabled="disabled" value="${recebimentoForm.map.agenciaContaArrecadadorTexto}">

				<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotulodataRecebimentoInclusao" for="dataRecebimentoInclusao" ><span class="campoObrigatorioSimbolo">* </span>Data do Recebimento:</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataRecebimentoInclusao" name="dataRecebimentoInclusao" maxlength="10" value="${dataRecebimentoInclusao eq null ? recebimentoVO.dataRecebimentoInclusao : dataRecebimentoInclusao}">
<!-- 				<br class="quebraLinha2" /> -->
				<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloValorRecebimento" for="valorRecebimento" ><span class="campoObrigatorioSimbolo">* </span>Valor do Recebimento:</label>
				<input class="campoTexto campo2Linhas" type="text" id="valorRecebimento" name="valorRecebimento" size="13" maxlength="13"
						onkeypress="return formatarCampoDecimal(event,this,11,2);" onblur="aplicarMascaraNumeroDecimal(this,2);" value="${recebimentoVO.valorRecebimento}">
				<br />
				
				<label class="rotulo" for="observacao">Observa��o:</label>
				<textarea id="observacao" name="observacao" maxlength="300" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>',
					'<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" class="campoTexto" rows="4"
					onkeypress="return formatarCampoTextoLivreComLimite(event,this,300);">
					<c:out value="${recebimentoVO.observacao}"/>
				</textarea>
			</fieldset>
		</fieldset>
	
		<hr class="linhaSeparadora1" />
		
		<fieldset class="conteinerBloco">
			<display:table class="dataTableGGAS" name="listaContratoPontoConsumos" sort="list" id="contratoPontoConsumo" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirAlteracaoRecebimento">
				<display:column sortable="true" title="Matr�cula do Imovel" style="width: 120px">
						<c:out value='${contratoPontoConsumo.pontoConsumo.imovel.chavePrimaria}'/> 
				</display:column>
				<display:column sortable="true" title="Im�vel - Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left">
					<c:out value='${contratoPontoConsumo.pontoConsumo.codigoLegado}'/><c:out value=" - "/><c:out value='${contratoPontoConsumo.pontoConsumo.imovel.nome}'/><c:out value=" - "/><c:out value="${contratoPontoConsumo.pontoConsumo.descricao}"/>
				</display:column>
				<display:column sortable="false" title="Situa��o" style="width: 100px">
					<c:choose>
						<c:when test="${contratoPontoConsumo.pontoConsumo.habilitado eq true}">
							<c:out value='Ativo'/>
						</c:when>
						<c:otherwise>
							<c:out value='Inativo'/>
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column sortable="false" title="Segmento" style="width: 100px">
					<c:out value="${contratoPontoConsumo.pontoConsumo.segmento.descricao}"/>
				</display:column>
				<display:column sortable="false" title="Ramo de Atua��o" style="width: 150px">
					<c:out value="${contratoPontoConsumo.pontoConsumo.ramoAtividade.descricao}"/>
				</display:column>
			</display:table>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset class="conteinerBloco">	
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaFatura" sort="list" id="faturaVO" pagesize="15" excludedParams="" requestURI="#">
				<display:column title="Documento"  style="width: 180px">
					<c:out value='${faturaVO.fatura.chavePrimaria}'/>
				</display:column>
				<display:column title="Ciclo/<br />Refer�ncia" style="width: 75px">
					<c:out value='${faturaVO.fatura.cicloReferenciaFormatado}'/>
				</display:column>
				<display:column sortable="false" title="Valor (R$)">
					<fmt:formatNumber value="${faturaVO.fatura.valorTotal}" minFractionDigits="2" maxFractionDigits="2" />
					<input type="hidden" id="${faturaVO.fatura.chavePrimaria}" value="${faturaVO.fatura.valorTotal}">
				</display:column>
				<display:column sortable="false" title="Saldo (R$)" >
			   			<fmt:formatNumber value="${faturaVO.saldo}" minFractionDigits="2" maxFractionDigits="2" />
			   	</display:column>
				<display:column sortable="false" title="Data de<br />Emiss�o" style="width: 75px">
					<c:out value='${faturaVO.fatura.dataEmissaoFormatada}'/>
				</display:column>
				<display:column sortable="false" title="Data de<br />Vencimento" style="width: 75px">
					<c:out value='${faturaVO.fatura.dataVencimentoFormatada}'/>
				</display:column>
				<display:column sortable="false" title="Tipo de Documento" style="width: 250px">
					<c:out value="${faturaVO.fatura.tipoDocumento.descricao}"/>
				</display:column>
			</display:table>
		</fieldset>
		
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
		<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar"  type="button" onclick="limparCamposPesquisa();">
		<vacess:vacess param="alterarRecebimento">
			<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="submit" onclick="salvar()">
		</vacess:vacess>
	</fieldset>
	
	<token:token></token:token>

</form:form> 