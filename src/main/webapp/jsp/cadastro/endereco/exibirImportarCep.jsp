<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Importar CEP<a href="<help:help>/importaodosceps.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para importar o CEP dos correios, clique em <span class="destaqueOrientacaoInicial">Procurar</span> e busque o arquivo de importa��o de CEPs, clicando em <span class="destaqueOrientacaoInicial">Abrir</span> (na janela) para finalizar a sele��o.<br />
Em seguida clique em <span class="destaqueOrientacaoInicial">Importar:</span> para inserir o(s) CEP(s) no sistema.</p>
<form method="post" enctype="multipart/form-data" action="importarCep"> 
<input name="acao" type="hidden" id="acao" value="importarCep">

<fieldset id="importarCEP" class="conteinerPesquisarIncluirAlterar">
	<label class="rotulo campoObrigatorio" id="rotuloArquivo" for="nome"><span class="campoObrigatorioSimbolo">* </span>Arquivo:</label>
	<input class="campoFile" type="file" id="arquivo" name="arquivo" title="Procurar" />
	<input type="button" value="Limpar Arquivo" class="bottonRightCol2" id="limparArquivo" style="margin-left:110px;margin-top: -3px;">
	
	<c:if test="${sucessoImportacao}">
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset id="resumoCEP">
			<legend>Resumo da Importa��o</legend>			
			<fieldset id="resultadoCEP">
				<label class="rotulo" for="nome">Itens Inclu�dos:</label>
				<span class="itemDetalhamento"><c:out value="${qtdIncluidas}"/></span><br />
				
				<label class="rotulo" for="nome">Itens Alterados:</label>	
				<span class="itemDetalhamento"><c:out value="${qtdAlteradas}"/></span><br />
				
				<label class="rotulo" for="nome">Itens Rejeitados:</label>	
				<span class="itemDetalhamento"><c:out value="${qtdRejeitadas}"/></span>
			</fieldset>
		</fieldset>
	</c:if>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para importa��o de CEP.</p>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>Formato de arquivo suportado: .txt</p>
</fieldset>

<fieldset class="conteinerBotoes">
	<vacess:vacess param="importarCep">
    	<input name="Button" type="submit" class="bottonRightCol2" value="Importar">
    </vacess:vacess>
</fieldset>

</form> 