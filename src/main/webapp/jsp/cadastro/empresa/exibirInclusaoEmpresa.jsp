<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>

<script>
	$(function() {
		// limpa todos os campos <input type="file"... 
		$("input#limparFormulario").click(function() {
			$("#nomeClienteTexto,#cnpjFormatadoTexto,#emailClienteTexto,#enderecoFormatadoTexto,#idCliente,#nomeCompletoCliente,#cnpjFormatado,#emailCliente,#enderecoFormatado,.file").attr({value: "" });
			$("#principal2").attr({checked: "true" });
			moveAllOptions(document.forms[0].idServicosPrestados,document.forms[0].servicosPrestadosDisponiveis,true);
		});
			
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
		
		$("#principal2").attr({checked: "true" });
		
	});
	
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaEmpresa"/>';
	}
	
	
	function incluir(){
		var lista = document.getElementById('idServicosPrestados');	
		
		for (i=0; i<lista.length; i++){
			lista.options[i].selected = true;
		}
		
		submeter("EmpresaForm", "incluirEmpresa"); 
	}
	
	var popup;

	function exibirPopupPesquisaCliente() {
		popup = window.open('exibirPesquisaClientePopup?pessoaJuridica=true' ,'popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document.getElementById("nomeCompletoCliente");
		var cnpjFormatado = document.getElementById("cnpjFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document.getElementById("enderecoFormatado");		
		
		if(idSelecionado != '') {				
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];		               	
		               	if(cliente["cnpj"] != undefined ){
		               		cnpjFormatado.value = cliente["cnpj"];
		               	} else {
			               	cnpjFormatado.value = "";			               	
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	cnpjFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
        
        document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
        document.getElementById("cnpjFormatadoTexto").value = cnpjFormatado.value;
        document.getElementById("emailClienteTexto").value = emailCliente.value;
        document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
	}
	
	
</script>


<h1 class="tituloInterno">Incluir Empresa<a href="<help:help>/empresainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form:form method="post" action="incluirEmpresa"  id="EmpresaForm" name="EmpresaForm"  enctype="multipart/form-data" >

<input name="acao" type="hidden" id="acao" value="incluirEmpresa">
<input name="indicador" type="hidden" id="indicador" value="${habilitado}">

<fieldset class="conteinerPesquisarIncluirAlterar">	
	<fieldset id="pesquisarCliente" class="colunaEsq">
		<legend>Pesquisar Pessoa Jur�dica</legend>
		<div class="pesquisarClienteFundo">			
			<input name="cliente" type="hidden" id="idCliente" value="${empresaForm.map.idCliente}">
			<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${empresaForm.map.nomeCompletoCliente}">
			<input name="cnpjFormatado" type="hidden" id="cnpjFormatado" value="${empresaForm.map.cnpjFormatado}">
			<input name="enderecoFormatado" type="hidden" id="enderecoFormatado" value="${empresaForm.map.enderecoFormatado}">
			<input name="emailCliente" type="hidden" id="emailCliente" value="${empresaForm.map.emailCliente}">
			
			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Pessoa Jur�dica</span> para selecionar a Pessoa Jur�dica Respons�vel.</p>
			<input name="Button" id="botaoPesquisarClientePesssoaJuridica" class="bottonRightCol2" title="Pesquisar Pessoa Jur�dica"  value="Pesquisar Pessoa Jur�dica" onclick="exibirPopupPesquisaCliente();" type="button"><br >
			<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloCliente" for="nomeClienteTexto"><span class="campoObrigatorioSimbolo">* </span>Pessoa Jur�dica:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" size="50" disabled="disabled" value="${empresa.cliente.nome}"><br />
			<label class="rotulo campoObrigatorio" id="rotuloCnpjTexto" for="cnpjFormatadoTexto"><span class="campoObrigatorioSimbolo">* </span>CNPJ:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="cnpjFormatadoTexto" name="cnpjFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${empresa.cliente.cnpjFormatado}"><br />	
			<label class="rotulo campoObrigatorio" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto"><span class="campoObrigatorioSimbolo">* </span>Endere�o:</label>
			<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoTexto" rows="2" cols="37" disabled="disabled">${empresa.cliente.enderecoPrincipal.enderecoFormatado}</textarea><br />
			<label class="rotulo" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${empresa.cliente.emailPrincipal}"><br />
		</div>
	</fieldset>
	
	<fieldset id="exibirLogo">
		<p class="orientacaoInterna"style="padding-left: 4em; background-position: 2em 2px;">No campo <span class="destaqueOrientacaoInicial">
		Logotipo</span> insira arquivos no(s) formato(s) ${formatosAceitosLogoEmpresa} apenas.</p>
		<label class="rotulo" id="rotuloLogo" for="logo">Logotipo:</label>
		<input class="campoFile" type="file" id="logo" name="logoEmpresa" title="Procurar" value="" style="width: 40px; padding: 4px; font-size: 10pt; " >
		<input type="button" value="Limpar Arquivo" class="bottonRightCol2" id="limparArquivo" style="width: 110px; padding: 4px; text-align: left; font-size: 10pt; ">
		
		<label class="rotulo campoObrigatorio" for="principal1"><span class="campoObrigatorioSimbolo">* </span>CDL:</label>
		<input class="campoRadio" id="principal1" name="principal" value="true" type="radio" <c:if test="${empresa.principal eq 'true'}">checked</c:if>><label class="rotuloRadio" for="principal1">Sim</label>
		<input class="campoRadio" id="principal2" name="principal" value="false" type="radio" <c:if test="${empresa.principal eq 'false'}">checked</c:if>><label class="rotuloRadio" for="principal2">N�o</label><br >
		
		<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloServicoPrestado" for="servicosPrestadosDisponiveis" ><span class="campoObrigatorioSimbolo">* </span>�rea de atua��o:</label>		
		<select id="servicosPrestadosDisponiveis" class="campoList" multiple="multiple"
			onDblClick="moveSelectedOptions(document.forms[0].servicosPrestadosDisponiveis,document.forms[0].idServicosPrestados,true)">
		   	<c:forEach items="${listaServicoPrestado}" var="servico">
				<option value="<c:out value="${servico.chavePrimaria}"/>"> <c:out value="${servico.descricao}"/></option>		
		    </c:forEach>
	  	</select>
	  	<fieldset class="conteinerBotoesCampoList">
			<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol2" 
		   		onClick="moveSelectedOptions(document.forms[0].servicosPrestadosDisponiveis,document.forms[0].idServicosPrestados,true)">
		  	<input id="botaoMoverTodosDireita" type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7"
		   		onClick="moveAllOptions(document.forms[0].servicosPrestadosDisponiveis,document.forms[0].idServicosPrestados,true)">
		  	<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
		   		onClick="moveSelectedOptions(document.forms[0].idServicosPrestados,document.forms[0].servicosPrestadosDisponiveis,true)">
		  	<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" 
		   		onClick="moveAllOptions(document.forms[0].idServicosPrestados,document.forms[0].servicosPrestadosDisponiveis,true)">
		</fieldset>
	  	<select id="idServicosPrestados" name="idServicosPrestados" multiple="multiple" class="campoList campoList2"
	   		onDblClick="moveSelectedOptions(document.forms[0].idServicosPrestados,document.forms[0].servicosPrestadosDisponiveis,true)">
		   	<c:forEach items="${listaServicoPrestadoSelecionados}" var="servico">
				<option value="<c:out value="${servico.chavePrimaria}"/>"> <c:out value="${servico.descricao}"/></option>		
		    </c:forEach>
	  	</select>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Empresas.</p>
</fieldset>

<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="voltar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button">
    <vacess:vacess param="incluirEmpresa.do">
    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="incluir();">
    </vacess:vacess>
</fieldset>
</form:form>
