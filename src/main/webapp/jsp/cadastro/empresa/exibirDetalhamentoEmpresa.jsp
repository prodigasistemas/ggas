<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<h1 class="tituloInterno">Detalhar Empresa<a href="<help:help>/cadastrodaempresadetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form id="empresaForm" name="empresaForm" method="post" action="exibirDetalhamentoEmpresa" enctype="multipart/form-data">

<script>
	
	function voltar(){	
		location.href = '<c:url value="/exibirPesquisaEmpresa"/>';
	}
	
	function alterar(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("empresaForm", "exibirAtualizarEmpresa");
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoEmpresa">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${empresa.chavePrimaria}">

<fieldset class="detalhamento">

	<fieldset id="detalhamentoEmpresaCol1" class="colunaEsq">
		<div class="pesquisarClienteFundo">
			<label class="rotulo rotulo2Linhas" id="rotuloCliente">Pessoa Jur�dica:</label>
			<span id="itemDetalhamentoPessoaJuridica" class="itemDetalhamento2Linhas"><c:out value="${empresa.cliente.nome}"/></span><br />
			<label class="rotulo" id="rotuloCnpjTexto">CNPJ:</label>
			<span class="itemDetalhamento"><c:out value="${empresa.cliente.cnpjFormatado}"/></span><br />
			<label class="rotulo" id="rotuloEnderecoTexto">Endere�o:</label>
			<span class="itemDetalhamento" id="enderecoFormatadoTexto"><c:out value="${empresa.cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
			<label class="rotulo" id="rotuloEmailClienteTexto">E-mail:</label>
			<span class="itemDetalhamento"><c:out value="${empresa.cliente.emailPrincipal}"/></span>
		</div>		
		<fieldset id="camposInferioresDetalhar">
			<label class="rotulo" for="principal1">CDL:</label>
			<span class="itemDetalhamento">
				<c:choose>
					<c:when test="${empresa.principal eq 'true'}">Sim</c:when>
					<c:otherwise>N�o</c:otherwise>
				</c:choose>
			</span>
			<br />
			<label class="rotulo">Indicador de Uso:</label>
			<span class="itemDetalhamento" id="empresaServicoPrestado"> 
				<c:choose>
					<c:when test="${empresa.habilitado eq'true'}">Sim</c:when>
					<c:otherwise>N�o</c:otherwise>
				</c:choose>
			</span>
			<br />
			<label class="rotulo" for="servicosPrestadosDisponiveis" >�rea de atua��o:</label>		
		   	<span class="itemDetalhamento" id="empresaServicoPrestado">
		   		<c:forEach items="${empresa.servicosPrestados}" var="servico2">
		   			<c:out value="${servico2.descricao}"/>
		   			<c:forEach items="${listaServicoPrestadoSelecionados}" var="servico">
	   					<c:if test="${servico2.chavePrimaria eq servico.chavePrimaria}">
	   						&nbsp;&nbsp;&nbsp;<img src="<c:url value="/imagens/check2.gif" />" border="0">
	   					</c:if>
				    </c:forEach>
				    <br />
			    </c:forEach> 
		    </span>
		</fieldset>
	</fieldset>
	
	<fieldset id="exibirLogoDetalhar">
		<label class="rotulo" id="rotuloLogo" for="logo">Logotipo:</label>
		<c:choose>
			<c:when test="${not empty empresa.logoEmpresa }">
				<img id="imagemLogoDetalhar" class="imagemGgas"
					src="<c:url value="/exibirLogoEmpresa/${empresa.chavePrimaria}" />" 
					/>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
	</fieldset>	
	
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAtualizarEmpresa.do">    
    	<input id="botaoAlterar" name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" value="Alterar" type="button" 
    	onclick="alterar(<c:out value='${empresa.chavePrimaria}'/>);">
    </vacess:vacess>
</fieldset>
</form:form>
