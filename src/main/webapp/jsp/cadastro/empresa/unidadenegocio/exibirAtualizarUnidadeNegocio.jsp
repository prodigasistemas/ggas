<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Alterar Unidade de Neg�cio<a href="<help:help>${paginaHelp}</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>

    $(document).ready(function(){
        var max = 0;
  
        $('.rotulo').each(function(){
                if ($(this).width() > max)
                   max = $(this).width();   
            });
        $('.rotulo').width(max);
    });
    
    function salvar(){
    	submeter('unidadeNegocioForm', 'atualizarUnidadeNegocio');
    }

    function cancelar(){    
    	location.href='exibirPesquisaUnidadeNegocio';
    }
 
	function limparCampos() {

		var habilitado = document.unidadeNegocioForm.habilitado.value;
		limparFormularios(unidadeNegocioForm);
		document.unidadeNegocioForm.habilitado.value = habilitado;

	}
	
</script>

<form method="post" enctype="multipart/form-data" action="atualizarUnidadeNegocio" id="unidadeNegocioForm" name="unidadeNegocioForm">
<input name="postBack" type="hidden" id="postBack" value="true">

<input type="hidden" id="habilitadoOriginal" value="${unidadeNegocio.habilitado}" />	

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${unidadeNegocio.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${unidadeNegocio.versao}"> 

<fieldset class="conteinerPesquisarIncluirAltualizar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
	        <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o	:</label>
	        <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${unidadeNegocio.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/>
	        <br />

        	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o Abreviada:</label>
        	<input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="5" size="6" value="${unidadeNegocio.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/>
        
        <label class="rotulo campoObrigatorio" id="rotuloGerenciaRegional"><span class="campoObrigatorioSimbolo">* </span>Ger�ncia Regional:</label>
            <select name="gerenciaRegional" class="campoSelect" id="gerenciaRegional" >
                <option value="-1">Selecione</option>
                <c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
                    <option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${unidadeNegocio.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${gerenciaRegional.nome}"/>
                    </option>       
                </c:forEach>    
            </select>
        
    </fieldset>
    
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal colunaFinalAjuste">
           <br />
          
        <label class="rotulo" for="habilitado">Indicador de Uso:</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${unidadeNegocio.habilitado eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Ativo</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${unidadeNegocio.habilitado eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Inativo</label><br />
        </fieldset>
    
    <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
</fieldset>
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="javascript:limparCampos();">
    
    <vacess:vacess param="atualizarUnidadeNegocio">
        <input name="button" id="botaoSalvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="javascript:salvar();">
    </vacess:vacess>
</fieldset>
</form>
