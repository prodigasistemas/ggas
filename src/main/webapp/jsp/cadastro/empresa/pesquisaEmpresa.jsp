<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Pesquisar Empresa<a href="<help:help>/consultadasempresas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarEmpresa" id="empresaForm" name="empresaForm" modelAttribute="EmpresaImpl">
	
	<script language="javascript">
	
	$(document).ready(function(){
		
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
		
		$("#cnpjFormatado").inputmask("99.999.999/9999-99",{placeholder:"_"});  

	});
	
	
	function removerEmpresa(){
		
		var selecao = verificarSelecao();
		if (selecao == true) {	
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('empresaForm', 'excluirEmpresa');
			}
	    }
	}
	
	function alterarEmpresa(){
		
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("empresaForm", "exibirAtualizarEmpresa");
	    }
	}
	
	function detalharEmpresa(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("empresaForm", "exibirDetalhamentoEmpresa");
	}
	
	
	function incluir() {
			location.href = '<c:url value="/exibirInclusaoEmpresa"/>';
	}
	
	function limparFormulario(){
		document.getElementById('nomeCliente').value = "";	
		document.getElementById('cnpjFormatado').value = "";	
		document.getElementById('idServicoPrestado').value = "-1";
		document.forms[0].habilitado[0].checked = true;
	}
	
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarEmpresas">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarEmpresaCol1" class="coluna">
			<label class="rotulo" id="rotuloNomeEmpresa" for="nomeCompletoCliente">Nome:</label>
			<input class="campoTexto" type="text" name="nomeCliente" id="nomeCliente" maxlength="50" size="50" value="${empresa.cliente.nome}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/><br />
			
			<label class="rotulo" id="rotuloCNPJ" for="cnpjFormatado">CNPJ:</label>
			<input class="campoTexto" type="text" name="cnpjFormatado" id="cnpjFormatado" maxlength="18" size="18" value="${empresa.cliente.cnpjFormatado}"/>
		</fieldset>
		
		<fieldset id="pesquisarEmpresaCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloServicoPrestado" for="servicoPrestado">�rea de atua��o:</label>
			<select name="idServicoPrestado" id="idServicoPrestado" class="campoSelect" >
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaServicoPrestado}" var="servicoPrestado">
					<option value="<c:out value="${servicoPrestado.chavePrimaria}"/>" 
						<c:if test="${idServicoPrestado == servicoPrestado.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${servicoPrestado.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
				
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
			
			<fieldset class="conteinerBotoesPesquisarDir">
				<vacess:vacess param="pesquisarEmpresa.do">
		    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
		    	</vacess:vacess>
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
			</fieldset>
		</fieldset>
		
		
	</fieldset>
	
	<c:if test="${empresas ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="empresas" sort="list" id="empresa" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15" requestURI="pesquisarEmpresa">
	      	<display:column style="text-align: center; width: 25px" sortable="false" 
	      		title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${empresa.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${empresa.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        <display:column sortable="true" title="Nome">
	        	<a href="javascript:detalharEmpresa(<c:out value='${empresa.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${empresa.cliente.nome}"/>
	            </a>
	        </display:column>
			<display:column title="CNPJ"  style="width: 150px">
	        	<a href="javascript:detalharEmpresa(<c:out value='${empresa.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	        		<c:out value="${empresa.cliente.cnpjFormatado}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty empresas}">
			<vacess:vacess param="exibirAtualizarEmpresa.do">
				<input id="botaoAlterar" name="buttonAlterar" value="Alterar" class="bottonRightCol2  botaoAlterar" onclick="alterarEmpresa()" type="button">
			</vacess:vacess>
			<vacess:vacess param="excluirEmpresa.do">
				<input id="botaoRemover" name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerEmpresa()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInclusaoEmpresa.do">
			<input id="botaoIncluir" name="button" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Incluir" onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>
</form:form>
