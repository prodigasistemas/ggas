<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Inserir Unidade Organizacional<a class="linkHelp" href="<help:help>/unidadeorganizacionalinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form method="post" action="inserirUnidadeOrganizacional" name="unidadeOrganizacionalForm" id="unidadeOrganizacionalForm">

<script>
$(document).ready(function(){
   
	$("#inicioExpediente,#fimExpediente").inputmask("99:99",{placeholder:"_"});
   
	if($("#valorIndicadorAbreChamado").val() == "" && $("#valorIndicadorCentralAtendimento").val() == "" &&
			$("#valorIndicadorChat").val() == "" && $("#valorIndicadorTramite").val() == "" 
				&& $("#valorIndicadorEmail").val() == ""){
		
				document.forms[0].indicadorTramite[1].checked = "checked";
				document.forms[0].indicadorAbreChamado[1].checked = "checked";
				document.forms[0].indicadorCentralAtendimento[1].checked = "checked";
				document.forms[0].indicadorChat[1].checked = "checked";
				document.forms[0].indicadorEmail[1].checked = "checked";
		}
   
});

	function carregarCamposPorTipo(elem){
		carregarUnidadeSuperiorTipo(elem);
		carregarEmpresasPorTipo(elem);
		carregarTipoDaUnidadeTipo(elem)
	}
	
	function carregarCamposPorGerenciaRegional(elem){
		carregarLocalidades(elem);
		var unidade = document.getElementById("idUnidadeTipo");
		var codTipo = unidade.value;
		
		if (codTipo != "-1") {      		
        	AjaxService.obterUnidadeTipo(codTipo, 
            	function(unidadeTipo) {
           			var tipo = unidadeTipo["tipo"];
           			if(tipo != "L"){
           				preencherHabilitarCampoDescricaoSigla(elem);
           			}
           		}
           	);
        }
		
	}
	
	function carregarCamposPorLocalidade(elem){
		carregarUnidadeSuperiorLocalidade(elem);
		carregarUnidadeRepavimentadora(elem);
		preencherHabilitarCampoDescricaoSiglaPorLocalidade(elem);
	}

	function carregarCampoLocalidades(codGerenciaRegional){
		var selectLocalidades = document.getElementById("idLocalidade");
		var idLocalidade = "${unidadeOrganizacional.localidade.chavePrimaria}";
		
      	selectLocalidades.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
        
      	if (codGerenciaRegional != "-1") {      		
        	AjaxService.consultarLocalidadesPorGerenciaRegional( codGerenciaRegional, {
            	callback: function(localidades) {            		      		         		
                	for (key in localidades){
                    	var novaOpcao = new Option(localidades[key], key);
                        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
                    }
                }, async:false}
            );
            if (selectLocalidades.disabled == false) {
	            if (idLocalidade != "-1") {
	            	selectLocalidades.value = idLocalidade;
	            }
	        }
      	}
	}
	
	function preencherHabilitarCampoDescricaoSigla(codGerenciaRegional){
		var idGR = codGerenciaRegional.value;
		preencherCamposDescricaoSiglaPorGR(idGR);
	}
	
	function preencherCamposDescricaoSiglaPorGR(idGR){
		var descricao = document.getElementById("descricao");
      	var sigla = document.getElementById("sigla");
      	if (idGR != "-1") { 
	      	AjaxService.obterGerenciaRegional( idGR, 
	           	function(gerenciaRegional) {
	           		descricao.value = gerenciaRegional["nome"];
          			descricao.disabled = true;
          			sigla.value = gerenciaRegional["sigla"];
          			sigla.disabled = true;
	           	}
			);
		}else{
			//descricao.value = "";
          	descricao.disabled = false;
          	//sigla.value = "";
          	sigla.disabled = false;
		}
	}
	
	function preencherHabilitarCampoDescricaoSiglaPorLocalidade(codLocalidade){
		var idLocalidade = codLocalidade.value;
		carregarCamposDescricaoSigla(idLocalidade, false);

	}
	
	function carregarCamposDescricaoSigla(idLocalidade, isManter){
		var descricao = document.getElementById("descricao");
      	var sigla = document.getElementById("sigla");
      	if (idLocalidade != "-1") { 
	      	AjaxService.obterLocalidade( idLocalidade, 
	           	function(localidade) {
	           		descricao.value = localidade["descricao"];
          			descricao.disabled = true;
          			sigla.value = "";
          			sigla.disabled = true;
	           	}
			);
		}else{
			if(!isManter){
				descricao.value = "";
          		descricao.disabled = false;
          		sigla.value = "";
          		sigla.disabled = false;
			}
		}
	}

	function carregarLocalidades(elem) {	
		var codGerenciaRegional = elem.value;	
		carregarCampoLocalidades(codGerenciaRegional);
	}
	
	function carregarCampoUSPorLocalidade(codLocalidade){
		var idTipo = document.getElementById("idUnidadeTipo");
      	var selectUnidadeSuperior = document.getElementById("idUnidadeSuperior");
      	    
      	selectUnidadeSuperior.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectUnidadeSuperior.options[selectUnidadeSuperior.length] = novaOpcao;
        
      	if (codLocalidade != "-1") {      		
        	AjaxService.consultarUnidadeSuperiorPorLocalidadeNivel(codLocalidade, idTipo.value,
            	function(unidades) {            		      		         		
                	for (key in unidades){
                    	var novaOpcao = new Option(unidades[key], key);
                        selectUnidadeSuperior.options[selectUnidadeSuperior.length] = novaOpcao;
                    }
                }
            );
      	}		
	}
	
	function carregarUnidadeSuperiorLocalidade(elem) {	
		var codLocalidade = elem.value;	
	}
	
	function carregarUnidadeSuperiorTipo(elem) {	
		var codTipo = elem.value;
		carregarUnidadeSuperior(codTipo);					
	}
	
	function carregarUnidadeSuperior(codTipo){
      	var selectUnidadeSuperior = document.getElementById("idUnidadeSuperior");
      	var idUnidadeSuperior = "${unidadeOrganizacional.unidadeSuperior.chavePrimaria}";

      	selectUnidadeSuperior.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectUnidadeSuperior.options[selectUnidadeSuperior.length] = novaOpcao;
        
      	if (codTipo != "-1") { 
        	AjaxService.consultarUnidadeSuperiorPorLocalidadeNivel(codTipo, {
	           	callback: function(unidades) {
                	for (key in unidades){
                    	var novaOpcao = new Option(unidades[key], key);
                        selectUnidadeSuperior.options[selectUnidadeSuperior.length] = novaOpcao;
                    }
                }, async:false}
            );
                        
            if (idUnidadeSuperior != "-1") {
            	selectUnidadeSuperior.value = idUnidadeSuperior;
            }
      	}
	}
	
	function carregarRepavimentadora(codLocalidade){
	    var selectUnidadeRepavimentadora = document.getElementById("idUnidadeRepavimentadora");
	    var idUnidadeRepavimentadora = "${unidadeOrganizacional.unidadeRepavimentadora.chavePrimaria}";
      	    
      	selectUnidadeRepavimentadora.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectUnidadeRepavimentadora.options[selectUnidadeRepavimentadora.length] = novaOpcao;
        
      	if (codLocalidade != "-1") {      		
        	AjaxService.consultarUnidadeRepavimentadora(codLocalidade, {
            	callback: function(unidades) {            		      		         		
                	for (key in unidades){
                    	var novaOpcao = new Option(unidades[key], key);
                        selectUnidadeRepavimentadora.options[selectUnidadeRepavimentadora.length] = novaOpcao;
                    }
                }, async:false}
            );
            if (idUnidadeRepavimentadora != "-1") {
            	selectUnidadeRepavimentadora.value = idUnidadeRepavimentadora;
            }
      	}
	}
	
	function carregarUnidadeRepavimentadora(elem) {	
		var codLocalidade = elem.value;
		carregarRepavimentadora(codLocalidade);			
	}
	
	function carregarEmpresasPorTipo(elem) {	
		var codTipo = elem.value;
		carregarEmpresa(codTipo);	
	}
	
	function carregarEmpresa(codTipo){
		var selectEmpresa = document.getElementById("idEmpresa");
		var idEmpresa = "${unidadeOrganizacional.empresa.chavePrimaria}";
      	    
      	selectEmpresa.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectEmpresa.options[selectEmpresa.length] = novaOpcao;
        
      	if (codTipo != "-1") {      		
        	AjaxService.consultarEmpresasPorTipo(codTipo,
            	function(unidades) { 
            	   var tamanho = 0;       		         		
                   for (key in unidades){
	                   var novaOpcao = new Option(unidades[key], key);
	                   selectEmpresa.options[selectEmpresa.length] = novaOpcao;
	                   tamanho++;
                   }

                   if (tamanho == 1) {
	                   selectEmpresa.selectedIndex = 1;
	                   document.unidadeOrganizacionalForm.chaveEmpresa.value =  selectEmpresa.options[selectEmpresa.selectedIndex].value;
	                   selectEmpresa.disabled = true;
                    } else {
                       selectEmpresa.disabled = false;
	                   if(idEmpresa != "-1"){
	                   		selectEmpresa.value = idEmpresa;
	                   	}
                    }
                }
            );
      	}
	}
		
	function carregarTipoDaUnidadeTipo(elem) {	
		var codTipo = elem.value;
		carregarTipo(codTipo);
			
	}
	
	function carregarTipo(codTipo){
		var gerenciaRegional = document.getElementById("idGerenciaRegional");
		var localidade = document.getElementById("idLocalidade");
		var unidadeCentralizadora = document.getElementById("idUnidadeCentralizadora");
		var descricao = document.getElementById("descricao");
		var sigla = document.getElementById("sigla");
      	if (codTipo != "-1") {      		
        	AjaxService.obterUnidadeTipo(codTipo, 
            	function(unidadeTipo) {
           			var tipo = unidadeTipo["tipo"];
           			if(tipo == "L"){
           				gerenciaRegional.disabled = false;
           				localidade.disabled = false;
           				descricao.disabled = false;
           				descricao.value = "";
           				sigla.value = "";
           				sigla.disabled = true;
           			}else if(tipo == "G"){
           				
           				localidade.disabled = true;
           				localidade.value = "-1";
           				gerenciaRegional.disabled = false;
           				unidadeCentralizadora.disabled = false;
           				sigla.disabled = false;
           				verificarGerencia(gerenciaRegional);
           			}else if(tipo == "C"){
           				
           				gerenciaRegional.value = "-1";
           				gerenciaRegional.disabled = true;
           				localidade.disabled = true;
           				localidade.value = "-1";
           				unidadeCentralizadora.disabled = true;
           				unidadeCentralizadora.value = "-1";
           				descricao.disabled = false;
           				sigla.disabled = false;
           				verificarGerencia(gerenciaRegional);
           			}else if(tipo == "T"){
           				
           				gerenciaRegional.value = "-1";	
           				gerenciaRegional.disabled = true;
           				localidade.disabled = true;
           				localidade.value = "-1";
           				unidadeCentralizadora.disabled = false;
           				sigla.disabled = false;
           				descricao.disabled = false;
           				verificarGerencia(gerenciaRegional);
           			}else{
           			
           				gerenciaRegional.disabled = false;
           				descricao.disabled = false;
           				sigla.disabled = false;
           				localidade.disabled = false;
           				localidade.value = "-1";
           				unidadeCentralizadora.disabled = false;
           				verificarGerencia(gerenciaRegional);
           			}
            	}
            );
      	}
	}
	
	function verificarGerencia(gerenciaRegional){
		var idGerenciaRegional = gerenciaRegional.value;
		if(idGerenciaRegional != "-1"){
			preencherCamposDescricaoSiglaPorGR(idGerenciaRegional);
		}
	}
	
	function incluirUnidadeOrganizacional(){
		
		
		
		var descricao = document.getElementById("descricao");
		descricao.disabled = false;
			
		submeter("unidadeOrganizacionalForm", "inserirUnidadeOrganizacional");
		
		descricao.disabled = true;
	}
		
	function limparFormulario(){
		document.forms[0].idUnidadeTipo.value = "-1";
		document.forms[0].idEmpresa.value = "-1";
		carregarCamposPorTipo(-1);
		document.forms[0].idGerenciaRegional.value = "-1";
		document.forms[0].idLocalidade.value = "-1";
		document.forms[0].idMunicipio.value = "-1";
		document.forms[0].idCanalAtendimento.value = "-1";
		document.forms[0].descricao.value = "";
		document.forms[0].emailContato.value = "";
		document.forms[0].sigla.value = "";
		document.forms[0].inicioExpediente.value = "";
		document.forms[0].fimExpediente.value = "";
		document.forms[0].idUnidadeSuperior.value = "-1";
		document.forms[0].idUnidadeCentralizadora.value = "-1";
		document.forms[0].idUnidadeRepavimentadora.value = "-1";		
		document.forms[0].indicadorTramite[1].checked = "checked";
		document.forms[0].indicadorAbreChamado[1].checked = "checked";
		document.forms[0].indicadorCentralAtendimento[1].checked = "checked";
		document.forms[0].indicadorChat[1].checked = "checked";
		document.forms[0].indicadorEmail[1].checked = "checked";	
	}
	
	function manterDadosTela() {
		var idUnidadeTipo = "${unidadeOrganizacional.unidadeTipo.chavePrimaria}";
		
		if (idUnidadeTipo != "") {
			carregarUnidadeSuperior(idUnidadeTipo);
			carregarEmpresa(idUnidadeTipo);
			//carregarTipo(idUnidadeTipo);
		}
		
		var idLocalidade = "${unidadeOrganizacional.localidade.chavePrimaria}";
		if(idLocalidade != ""){
			carregarRepavimentadora(idLocalidade);
			carregarCampoUSPorLocalidade(idLocalidade);
			carregarCamposDescricaoSigla(idLocalidade, true);
		}			
		
		var idGerenciaRegional = "${unidadeOrganizacional.gerenciaRegional.chavePrimaria}";
		if(idGerenciaRegional != ""){
			carregarCampoLocalidades(idGerenciaRegional);
			preencherCamposDescricaoSiglaPorGR(idGerenciaRegional)
			//document.getElementById("idUnidadeSuperior").selected=selected;
		}
	}
	
	function init () {
		manterDadosTela();
	}
	
	addLoadEvent(init);
	
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaUnidadeOrganizacional"/>';
	}
	
</script>

<input type="hidden" name="acao" id="acao" value="inserirUnidadeOrganizacional">
<input type="hidden" id="descricaoUnidade" value="${unidadeOrganizacional.unidadeTipo.descricao}"/>
<input name="empresa" type="hidden" id="chaveEmpresa" value="${unidadeOrganizacional.empresa.chavePrimaria}">
<input name="postBack" type="hidden" id="postBack" value="true">
<input type="hidden" id="valorIndicadorAbreChamado" value="${indicadorAbreChamado}" />
<input type="hidden" id="valorIndicadorCentralAtendimento" value="${unidadeOrganizacional.indicadorCentralAtendimento}" />
<input type="hidden" id="valorIndicadorEmail" value="${unidadeOrganizacional.indicadorEmail}" />
<input type="hidden" id="valorIndicadorTramite" value="${unidadeOrgazinacional.indicadorTramite}" />
<input type="hidden" id="valorIndicadorChat" value ="${unidadeOrganizacional.indicadorChat}" />

<!-- Campos do Formul�rio -->
<fieldset id="unidadeOrganizacional" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna">
		<label class="rotulo campoObrigatorio" for="idUnidadeTipo"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
		<select name="unidadeTipo" id="idUnidadeTipo" class="campoSelect" onchange="carregarCamposPorTipo(this)">
	   	<option value="-1">Selecione</option>
		<c:forEach items="${listaUnidadeTipo}" var="tipo">
			<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.unidadeTipo.chavePrimaria == tipo.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${tipo.descricao}"/>
			</option>		
	    </c:forEach>	
	    </select><br />
		
		<label class="rotulo campoObrigatorio" for="idEmpresa"><span class="campoObrigatorioSimbolo">* </span>Empresa:</label>
		<select name="empresa" id="idEmpresa" class="campoSelect">
	   	<option value="-1">Selecione</option>
		<c:forEach items="${empresas}" var="empresa">
			<option value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${empresa.cliente.nome}"/>
			</option>		
	    </c:forEach>	
	    </select><br />
			
		<label class="rotulo" for="idGerenciaRegional">Ger&ecirc;ncia Regional:</label>
		<select name="gerenciaRegional" id="idGerenciaRegional" class="campoSelect" onchange="carregarCamposPorGerenciaRegional(this)">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
				<option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${gerenciaRegional.nome}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
	    				
		<label class="rotulo" for="idLocalidade">Localidade:</label>
			<select name="localidade" id="idLocalidade" class="campoSelect" onchange="carregarCamposPorLocalidade(this)">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaLocalidade}" var="localidade">
				<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localidade.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
	    
		<label class="rotulo" for="idMunicipio">Munic�pio:</label>
	   	<select name="municipio" id="idMunicipio" class="campoSelect">
	   	<option value="-1">Selecione</option>
		<c:forEach items="${listaMunicipio}" var="municipio">
			<option value="<c:out value="${municipio.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.municipio.chavePrimaria == municipio.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${municipio.descricao}"/>
			</option>		
	    </c:forEach>	
	    </select><br />
	    
		<label class="rotulo" for="idCanalAtendimento">Canal de Atendimento:</label>
	   	<select name="canalAtendimento" id="idCanalAtendimento" class="campoSelect">
	   	<option value="-1">Selecione</option>
		<c:forEach items="${listaCanalAtendimento}" var="canalAtendimento">
			<option value="<c:out value="${canalAtendimento.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.canalAtendimento.chavePrimaria == canalAtendimento.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${canalAtendimento.descricao}"/>
			</option>		
	    </c:forEach>	
	    </select><br />
	    	
	    <label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>						
		<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="250" size="22" value="${unidadeOrganizacional.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />						
		<label class="rotulo" for="sigla">Sigla:</label>
		<input class="campoTexto" type="text" id="sigla" name="sigla" maxlength="5" size="5" value="${unidadeOrganizacional.sigla}"  onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		
		<label class="rotulo" for="idUnidadeSuperior">Unidade Superior:</label>
	   	<select name="unidadeSuperior" id="idUnidadeSuperior" class="campoSelect">
	   	<option value="-1">Selecione</option>
		<c:forEach items="${listaUnidadeSuperior}" var="unidadeSuperior">
			<option value="<c:out value="${unidadeSuperior.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.unidadeSuperior.chavePrimaria == unidadeSuperior.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${unidadeSuperior.descricao}"/>
			</option>		
	    </c:forEach>	
	    </select><br />
		<label class="rotulo" for="idUnidadeCentralizadora">Unidade Centralizadora:</label>
	   	<select name="unidadeCentralizadora" id="idUnidadeCentralizadora" class="campoSelect">
	   	<option value="-1">Selecione</option>
		<c:forEach items="${listaUnidadeCentralizadora}" var="unidadeCentralizadora">
			<option value="<c:out value="${unidadeCentralizadora.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.unidadeCentralizadora.chavePrimaria == unidadeCentralizadora.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${unidadeCentralizadora.descricao}"/>
			</option>		
	    </c:forEach>	
	    </select><br />
		<label class="rotulo" for="idUnidadeRepavimentadora">Unidade Repavimentadora:</label>
	   	<select name="unidadeRepavimentadora" id="idUnidadeRepavimentadora" class="campoSelect">
	   	<option value="-1">Selecione</option>
		<c:forEach items="${listaUnidadeRepavimentadora}" var="unidadeRepavimentadora">
			<option value="<c:out value="${unidadeRepavimentadora.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.unidadeRepavimentadora.chavePrimaria == unidadeRepavimentadora.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${unidadeRepavimentadora.descricao}"/>
			</option>		
	    </c:forEach>	
	    </select>
	    
	    <label class="rotulo" id="rotuloEmailContato" for="emailContato">E-mail de Contato:</label>
		<input class="campoTexto" type="text" id="emailContato" name="emailContato" maxlength="80" onkeypress="return formatarCampoEmail(event)" value="${unidadeOrganizacional.emailContato}">
	</fieldset>
	
	<fieldset class="colunaFinal">
	    <label class="rotulo campoObrigatorio" for="inicioExpediente"><span class="campoObrigatorioSimbolo">* </span>Hora de in�cio do expediente:</label>
		<input class="campoTexto" type="text" id="inicioExpediente" name="inicioExpediente" maxlength="5" size="5" value="${inicioExpediente}"><br />
	    
	    <label class="rotulo campoObrigatorio" for="fimExpediente"><span class="campoObrigatorioSimbolo">* </span>Hora do final do expediente:</label>
		<input class="campoTexto" type="text" id="fimExpediente" name="fimExpediente" maxlength="5" size="5" value="${fimExpediente}"><br />	    
		
		<label class="rotulo">Unidade Aceita Tramita&ccedil;&atilde;o?</label>
		<input class="campoRadio" id="indicadorTramite1" name="indicadorTramite" value="true" type="radio" <c:if test="${unidadeOrganizacional.indicadorTramite == true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorTramite1">Sim</label>
		<input class="campoRadio" id="indicadorTramite2" name="indicadorTramite" value="false" type="radio" <c:if test="${unidadeOrganizacional.indicadorTramite == false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorTramite2">N�o</label><br />
		
		<label class="rotulo">Unidade Abre Chamado?</label>
		<input class="campoRadio" id="indicadorAbreChamado1"  name="indicadorAbreChamado" value="true" type="radio" <c:if test="${unidadeOrganizacional.indicadorAbreChamado == true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorAbreChamado1">Sim</label>
		<input class="campoRadio" id="indicadorAbreChamado2" name="indicadorAbreChamado" value="false" type="radio" <c:if test="${unidadeOrganizacional.indicadorAbreChamado == false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorAbreChamado2">N�o</label><br />
		
		<label class="rotulo">Unidade Central de Atendimento:</label>
		<input class="campoRadio" id="indicadorCentralAtendimento1" name="indicadorCentralAtendimento" value="true" type="radio" <c:if test="${unidadeOrganizacional.indicadorCentralAtendimento == true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorCentralAtendimento1">Sim</label>
		<input class="campoRadio" id="indicadorCentralAtendimento2" name="indicadorCentralAtendimento" value="false" type="radio" <c:if test="${unidadeOrganizacional.indicadorCentralAtendimento == false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorCentralAtendimento2">N�o</label><br />
		
		<label class="rotulo">Chat:</label>
		<input class="campoRadio" id="indicadorChat1" name="indicadorChat" value="true" type="radio" <c:if test="${unidadeOrganizacional.indicadorChat == true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorChat1">Sim</label>
		<input class="campoRadio" id="indicadorChat2" name="indicadorChat" value="false" type="radio" <c:if test="${unidadeOrganizacional.indicadorChat == false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorChat2">N�o</label><br />
	
		<label class="rotulo">Indicador de Uso de E-mail:</label>
		<input class="campoRadio" id="indicadorEmail1" name="indicadorEmail" value="true" type="radio" <c:if test="${unidadeOrganizacional.indicadorEmail == true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorEmail1">Sim</label>
		<input class="campoRadio" id="indicadorEmail2" name="indicadorEmail" value="false" type="radio" <c:if test="${unidadeOrganizacional.indicadorEmail == false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorEmail2">N�o</label>
		
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Unidade Organizacional.</p>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="voltar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="inserirUnidadeOrganizacional">   
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="button" onclick="incluirUnidadeOrganizacional();">
    </vacess:vacess>  
</fieldset>
  

</form> 