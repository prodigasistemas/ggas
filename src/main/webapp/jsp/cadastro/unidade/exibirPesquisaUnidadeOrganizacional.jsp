<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Pesquisar Unidade Organizacional<a class="linkHelp" href="<help:help>/consultadasunidadesorganizacionais.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" action="pesquisarUnidadeOrganizacional" name="unidadeOrganizacionalForm" id="unidadeOrganizacionalForm"> 
	
	<script>
	
	function incluir() {		
		location.href = '<c:url value="/exibirInserirUnidadeOrganizacional"/>';
	}
	
	function alterarUnidadeOrganizacional() {
		
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("unidadeOrganizacionalForm", "exibirAtualizarUnidadeOrganizacional");
	    }
	}
	
	function remover() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
	  		if (retorno == true) {
	  			submeter('unidadeOrganizacionalForm', 'removerUnidadeOrganizacional');
	  		}
		}
	}
	
	
	function limparFormulario(){
		document.forms[0].idUnidadeTipo.value = "-1";
		document.forms[0].descricao.value = "";
		document.forms[0].emailContato.value = "";
		document.forms[0].sigla.value = "";
		document.forms[0].nivelTipoUnidade.value = "";
		document.forms[0].idEmpresa.value = "-1";
		document.forms[0].idGerenciaRegional.value = "-1";
		document.forms[0].idLocalidade.value = "-1";
		document.forms[0].idUnidadeCentralizadora.value = "-1";
		document.forms[0].idUnidadeSuperior.value = "-1";
		document.forms[0].idCanalAtendimento.value = "-1";
		
		document.forms[0].indicadorTramite3.checked = "checked";
		document.forms[0].indicadorAbreChamado3.checked = "checked";
		document.forms[0].habilitado1.checked = "checked";
	
	}
	
	function detalharUnidadeOrganizacional(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("unidadeOrganizacionalForm", "exibirDetalhamentoUnidadeOrganizacional");
	}
	
	function carregarLocalidades(elem) {	
			var codGerenciaRegional = elem.value;	
	      	var selectLocalidades = document.getElementById("idLocalidade");
	    
	      	selectLocalidades.length=0;
	      	var novaOpcao = new Option("Selecione","-1");
	        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
	        
	      	if (codGerenciaRegional != "-1") {      		
	        	AjaxService.consultarLocalidadesPorGerenciaRegional( codGerenciaRegional, 
	            	function(localidades) {            		      		         		
	                	for (key in localidades){
	                    	var novaOpcao = new Option(localidades[key], key);
	                        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
	                    }
	                }
	            );
	      	}
				
		}	
		animatedcollapse.addDiv('pesquisaAvancadaUnidadeOrganizacional', 'fade=0,speed=400,persist=1,hide=0');
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarUnidadeOrganizacional">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="">
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaUnidadeOrganizacionalCol1" class="coluna">
			<label class="rotulo" id="rotuloTipo" for="idUnidadeTipo">Tipo:</label>
			<select name="unidadeTipo" class="campoSelect" id="idUnidadeTipo">
	    		<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadeTipo}" var="unidadeTipo">
					<option value="<c:out value="${unidadeTipo.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.unidadeTipo.chavePrimaria == unidadeTipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidadeTipo.descricao}"/>
					</option>		
			    </c:forEach>	
			</select><br />
			<label class="rotulo" id="rotuloNivel" for="nivelTipoUnidade">N�vel Hier�rquico:</label>
			<input class="campoTexto" name="niveisUnidadeTipo" id="nivelTipoUnidade" maxlength="3" size="5" type="text" value="${niveisUnidadeTipo}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/><br />
		</fieldset>
		
		<fieldset id="pesquisaUnidadeOrganizacionalCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloDescricao" for="descricao">Descri&ccedil;&atilde;o:</label>
			<input class="campoTexto" name="descricao" id="descricao" maxlength="80" size="40" type="text" value="${unidadeOrganizacional.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/><br /><br />
			<br/>
			<label class="rotulo" id="rotuloSigla" for="sigla" >Sigla:</label>
			<input class="campoTexto" name="sigla" id="sigla" maxlength="10" size="5" type="text" value="${unidadeOrganizacional.sigla}" 
				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>',
				 'formatarCampoNome(event)');"/>
				
			<label class="rotulo rotulo1" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" id="habilitado1" name="habilitado" value="true" type="radio" <c:if test="${habilitado eq true}">checked="checked"</c:if>><label class="rotuloRadio" for="habilitado1">Ativo</label>
			<input class="campoRadio" id="habilitado2" name="habilitado" value="false" type="radio" <c:if test="${habilitado eq false}">checked="checked"</c:if>><label class="rotuloRadio" for="habilitado2">Inativo</label>
			<input class="campoRadio" id="habilitado3" name="habilitado" value="" type="radio" <c:if test="${empty habilitado}">checked="checked"</c:if>><label class="rotuloRadio" for="habilitado3">Todos</label>
		</fieldset>
		<fieldset id="conteinerPesquisaAvancadaUnidadeOrganizacional" class="conteinerBloco">
			<a class="linkPesquisaAvancada" href="#" rel="toggle[pesquisaAvancadaUnidadeOrganizacional]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pesquisa Avan�ada <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="pesquisaAvancadaUnidadeOrganizacional">
				<fieldset id="pesquisaAvancadaUnidadeOrganizacionalCol1" class="colunaEsq">		
					<label class="rotulo" for="idEmpresa">Empresa:</label>
					<select name="empresa" class="campoSelect" id="idEmpresa">
				    	<option title="Selecione" value="-1">Selecione</option>
						<c:forEach items="${empresas}" var="empresa">
							<option title="<c:out value="${empresa.cliente.nome}"/>"  value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${empresa.cliente.nome}"/>
							</option>
					    </c:forEach>
					</select><br />
					
					<label class="rotulo" for="idGerenciaRegional">Ger&ecirc;ncia Regional:</label>
					<select name="gerenciaRegional" class="campoSelect" id="idGerenciaRegional" onchange="carregarLocalidades(this)">
				    	<option title="Selecione" value="-1">Selecione</option>
						<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
							<option title="<c:out value="${gerenciaRegional.nome}"/>" value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${gerenciaRegional.nome}"/>
							</option>
					    </c:forEach>
					</select><br />
					
					<label class="rotulo" for="idLocalidade">Localidade:</label>
					<select title="Selecione" name="localidade" id="idLocalidade" class="campoSelect">
				    	<option value="-1">Selecione</option>
						<c:forEach items="${listaLocalidade}" var="localidade">
							<option title="<c:out value="${localidade.descricao}"/>" value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${localidade.descricao}"/>
							</option>
					    </c:forEach>
				    </select><br />
				    
				    <label class="rotulo" for="idCanalAtendimento">Canal de Atendimento:</label>
				   	<select title="Selecione" name="canalAtendimento" id="idCanalAtendimento" class="campoSelect">
					   	<option value="-1">Selecione</option>
						<c:forEach items="${listaCanalAtendimento}" var="canalAtendimento">
							<option title="<c:out value="${canalAtendimento.descricao}"/>" value="<c:out value="${canalAtendimento.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.canalAtendimento.chavePrimaria == canalAtendimento.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${canalAtendimento.descricao}"/>
							</option>		
					    </c:forEach>	
				    </select>
				    
				    <label class="rotulo" id="rotuloEmailContato" for="emailContato">E-mail de Contato:</label>
					<input class="campoTexto" type="text" id="emailContato" name="emailContato" maxlength="80" onkeypress="return formatarCampoEmail(event)" value="${unidadeOrganizacional.emailContato}">
			
				</fieldset>	
				
				
				<fieldset  id="pesquisaAvancadaUnidadeOrganizacionalCol2">
					 <label class="rotulo" for="idUnidadeCentralizadora">Unidade Centralizadora:</label>
				   	<select name="unidadeCentralizadora" id="idUnidadeCentralizadora" class="campoSelect">
					   	<option title="Selecione" value="-1">Selecione</option>
						<c:forEach items="${listaCentralizadoras}" var="unidadeCentralizadora">
							<option title="<c:out value="${unidadeCentralizadora.descricao}"/>" value="<c:out value="${unidadeCentralizadora.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.unidadeCentralizadora.chavePrimaria == unidadeCentralizadora.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidadeCentralizadora.descricao}"/>
							</option>		
					    </c:forEach>	
				    </select><br />
				    
				    <label class="rotulo" for="idUnidadeSuperior">Unidade Superior:</label>
				   	<select name="unidadeSuperior" id="idUnidadeSuperior" class="campoSelect">
					   	<option title="Selecione" value="-1">Selecione</option>
						<c:forEach items="${listaSuperiores}" var="unidadeSuperior">
							<option title="<c:out value="${unidadeSuperior.descricao}"/>" value="<c:out value="${unidadeSuperior.chavePrimaria}"/>" <c:if test="${unidadeOrganizacional.unidadeSuperior.chavePrimaria == unidadeSuperior.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidadeSuperior.descricao}"/>
							</option>		
					    </c:forEach>	
				    </select><br />
				    				
					<label class="rotulo rotulo1" for="indicadorTramite1">Unidade Aceita Tramita&ccedil;&atilde;o?</label>
					<input class="campoRadio" id="indicadorTramite1" name="indicadorTramite" value="true" type="radio" <c:if test="${indicadorTramite eq 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorTramite1">Sim</label>
					<input class="campoRadio" id="indicadorTramite2" name="indicadorTramite" value="false" type="radio" <c:if test="${indicadorTramite eq 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorTramite2">N�o</label>
					<input class="campoRadio" id="indicadorTramite3" name="indicadorTramite" value="" type="radio" <c:if test="${empty indicadorTramite}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorTramite3">Todos</label><br />
					
					<label class="rotulo rotulo1" for="indicadorAbreChamado1">Unidade Abre Chamado?</label>
					<input class="campoRadio" id="indicadorAbreChamado1" name="indicadorAbreChamado" value="true" type="radio" <c:if test="${indicadorAbreChamado eq 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorAbreChamado1">Sim</label>
					<input class="campoRadio" id="indicadorAbreChamado2" name="indicadorAbreChamado" value="false" type="radio" <c:if test="${indicadorAbreChamado eq 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorAbreChamado2">N�o</label>
					<input class="campoRadio" id="indicadorAbreChamado3" name="indicadorAbreChamado" value="" type="radio" <c:if test="${empty indicadorAbreChamado}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorAbreChamado3">Todos</label>				
				</fieldset>		
				
								
			</fieldset>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarUnidadeOrganizacional">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>		
	</fieldset>	
	
	<c:if test="${listaUnidadeOrganizacional ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaUnidadeOrganizacional" sort="list" id="unidadeOrganizacional" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarUnidadeOrganizacional?acao=pesquisarUnidadeOrganizacional">
			<display:column media="html" style="text-align: center; width: 25px" sortable="false" title="&nbsp;<input type='checkbox' name='checkAllAuto' id='checkAllAuto'"> 
				<input type="checkbox" name="chavesPrimarias" id="chavesPrimarias" value="${unidadeOrganizacional.chavePrimaria}">
			</display:column>
			<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${unidadeOrganizacional.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
			<display:column  sortProperty="sigla" sortable="true" title="Sigla" style="width: 35px">
				<a href='javascript:detalharUnidadeOrganizacional(<c:out value='${unidadeOrganizacional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${unidadeOrganizacional.sigla}'></c:out>
				</a>
			</display:column> 
			<display:column sortable="true" titleKey="UNIDADE_ORGANIZACIONAL_DESCRICAO" sortProperty="descricao">
				<a href='javascript:detalharUnidadeOrganizacional(<c:out value='${unidadeOrganizacional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${unidadeOrganizacional.descricao}'/>
				</a>
			</display:column>
			<display:column sortable="true" titleKey="UNIDADE_ORGANIZACIONAL_EMPRESA" sortProperty="empresa.cliente.nome" style="width: 260px">
				<a href='javascript:detalharUnidadeOrganizacional(<c:out value='${unidadeOrganizacional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${unidadeOrganizacional.empresa.cliente.nome}'></c:out>
				</a>
			</display:column>
			<display:column sortable="false" title="Aceita<br />Tramita��o" style="width: 85px">
				<a href='javascript:detalharUnidadeOrganizacional(<c:out value='${unidadeOrganizacional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:choose>
			      		<c:when test="${unidadeOrganizacional.indicadorTramite}">
			      			Sim
			      		</c:when>
			      		<c:otherwise>
			      			N�o
			      		</c:otherwise>
					</c:choose>
				</a>
			</display:column>     
			<display:column sortable="false" title="Abre<br />Chamado" style="width: 85px">
				<a href='javascript:detalharUnidadeOrganizacional(<c:out value='${unidadeOrganizacional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:choose>
						<c:when test="${unidadeOrganizacional.indicadorAbreChamado}">
							Sim
						</c:when>
						<c:otherwise>
							N�o
						</c:otherwise>
					</c:choose>
				</a>
			</display:column> 
			<display:column sortable="false" title="Central de<br />Atendimento" style="width: 85px">
				<a href='javascript:detalharUnidadeOrganizacional(<c:out value='${unidadeOrganizacional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:choose>
						<c:when test="${unidadeOrganizacional.indicadorCentralAtendimento}">
							Sim
			      		</c:when>
			      		<c:otherwise>
							N�o
						</c:otherwise>
					</c:choose>
				</a>
			</display:column>   
		</display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaUnidadeOrganizacional}">
			<vacess:vacess param="exibirAtualizarUnidadeOrganizacional">
				<input name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarUnidadeOrganizacional()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerUnidadeOrganizacional">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="remover();" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInserirUnidadeOrganizacional">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>
</form> 
