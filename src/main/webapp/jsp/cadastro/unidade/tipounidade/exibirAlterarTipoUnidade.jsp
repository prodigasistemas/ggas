<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Alterar Tipo de Unidade<a href="<help:help>/cadastrodotipodeunidadeorganizacionalinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>

    $(document).ready(function(){
        var max = 0;
  
        $('.rotulo').each(function(){
                if ($(this).width() > max)
                   max = $(this).width();   
            });
        $('.rotulo').width(max);
    });
    
    function salvar(){
        submeter('tipoUnidadeForm', 'alterarTipoUnidade');
    }

    function cancelar(){    
    	location.href='exibirPesquisaTipoUnidade';
    }

    function limparCampos(){

    	$('#descricao').val("");
    	$('#descricaoAbreviada').val("");
    	$('#nivel').val("");
    	$('#tipo').val("-1");
    }
        
</script>

<form:form method="post" enctype="multipart/form-data" action="alterarTipoUnidade" id="tipoUnidadeForm" name="tipoUnidadeForm">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="tela" type="hidden" id="tela" value="tipoUnidade">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tipoUnidade.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${tipoUnidade.versao}"> 

<fieldset class="conteinerPesquisarIncluirAltualizar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
	        <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o	:</label>
	        <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${tipoUnidade.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
	        <br />

        <label class="rotulo campoObrigatorio">Descri��o Abreviada:</label>
        <input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${tipoUnidade.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        
         <label class="rotulo" id="rotuloNivel">N�vel:</label>
        <input class="campoTexto campoHorizontal" type="text" name="nivel" id="nivel" maxlength="3" size="3" value="${tipoUnidade.nivel}" onkeypress="return formatarCampoInteiro(event)"/>
        <br /><br /><br /><br />
        
        <label class="rotulo campoObrigatorio" id="rotuloCodigoCnae"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
        <select name="tipo" class="campoSelect" id="tipo">
        <option value="-1">Selecione</option>
        <c:forEach items="${arrayTipoUnidadeTipo}" var="tipo">
            <option value="<c:out value="${tipoUnidade.tipo}"/>" <c:if test="${tipoUnidade.tipo == tipo}">selected="selected"</c:if>>
                <c:out value="${tipo}"/>
            </option>       
        </c:forEach>    
        </select><br />
    </fieldset>
    
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal colunaFinalAjuste">
           <br />
          
        <label class="rotulo" for="habilitado">Indicador de Uso:</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${tipoUnidade.habilitado eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Ativo</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${tipoUnidade.habilitado eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Inativo</label><br />
        </fieldset>
    
    <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
</fieldset>
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="javascript:limparCampos();">
    
    <vacess:vacess param="alterarTipoUnidade">
        <input name="button" id="botaoSalvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="salvar();">
    </vacess:vacess>
</fieldset>
</form:form>
