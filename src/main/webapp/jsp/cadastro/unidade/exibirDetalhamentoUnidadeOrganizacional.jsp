<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Unidade Organizacional<a class="linkHelp" href="<help:help>/unidadeorganizacionaldetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form method="post" action="exibirDetalhamentoUnidadeOrganizacional" name="unidadeOrganizacionalForm" id="unidadeOrganizacionalForm" enctype="multipart/form-data">

<script>
	
	function voltar(){	
		submeter('unidadeOrganizacionalForm', 'pesquisarUnidadeOrganizacional');
	}
	
	function alterar(){
		submeter('unidadeOrganizacionalForm', 'exibirAtualizarUnidadeOrganizacional');
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoUnidadeOrganizacional">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${unidadeOrganizacional.chavePrimaria}">
<input name="habilitado" type="hidden" value="true">
<fieldset id="unidadeOrganizacional" class="detalhamento">
	<fieldset class="coluna">
		<label class="rotulo" for="unidadeTipo">Tipo:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.unidadeTipo.descricao}"/></span><br />
		<label class="rotulo" for="empresa">Empresa:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.empresa.cliente.nome}"/></span><br />
		<label class="rotulo" for="gerenciaRegional">Ger�ncia Regional:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.gerenciaRegional.nome}"/></span><br />
		<label class="rotulo" for="localidade">Localidade:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.localidade.descricao}"/></span><br />
		<label class="rotulo" for="municipio">Munic�pio:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.municipio.descricao}"/></span><br />
		<label class="rotulo" for="canalAtendimento">Canal de Atendimento:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.canalAtendimento.descricao}"/></span><br />
		<label class="rotulo" for="descricao">Descri��o:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.descricao}"/></span><br />
		<label class="rotulo" for="sigla">Sigla:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.sigla}"/></span><br />
		<label class="rotulo" for="unidadeSuperior">Unidade Superior:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.unidadeSuperior.descricao}"/></span><br />
		<label class="rotulo" for="unidadeCentralizadora">Unidade Centralizadora:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.unidadeCentralizadora.descricao}"/></span><br />
		<label class="rotulo" for="unidadeRepavimentadora">Unidade Repavimentadora:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.unidadeRepavimentadora.descricao}"/></span><br />
		<label class="rotulo" for="emailContato">Email de Contato:</label>
		<span class="itemDetalhamento"><c:out value="${unidadeOrganizacional.emailContato}"/></span><br />
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo" for="inicioExpediente">Hora de in�cio do expediente:</label>
		<span class="itemDetalhamento"><c:out value="${inicioExpediente}"/></span><br />
		
		<label class="rotulo" for="fimExpediente">Hora do final do expediente:</label>
		<span class="itemDetalhamento"><c:out value="${fimExpediente}"/></span><br />
		
		<label class="rotulo" for="indicadorTramite">Unidade Aceita Tramita��o:</label>
		<span class="itemDetalhamento">
		<c:choose>
			<c:when test="${unidadeOrganizacional.indicadorTramite == 'true'}">
				<c:out value="Sim"/>
			</c:when>
			<c:otherwise>
				<c:out value="N�o"/>
			</c:otherwise>
		</c:choose>
		</span><br />
		
		<label class="rotulo" for="indicadorAbreChamado">Unidade Abre Chamado:</label>
		<span class="itemDetalhamento">
		<c:choose>
			<c:when test="${unidadeOrganizacional.indicadorAbreChamado == 'true'}">
				<c:out value="Sim"/>
			</c:when>
			<c:otherwise>
				<c:out value="N�o"/>
			</c:otherwise>
		</c:choose>
		</span><br />
		
		<label class="rotulo" for="indicadorCentralAtendimento">Unidade Central de Atendimento:</label>
		<span class="itemDetalhamento">
		<c:choose>
			<c:when test="${unidadeOrganizacional.indicadorCentralAtendimento == 'true'}">
				<c:out value="Sim"/>
			</c:when>
			<c:otherwise>
				<c:out value="N�o"/>
			</c:otherwise>
		</c:choose>
		</span><br />
		
		<label class="rotulo" for="indicadorChat">Chat:</label>
		<span class="itemDetalhamento">
		<c:choose>
			<c:when test="${unidadeOrganizacional.indicadorChat == 'true'}">
				<c:out value="Sim"/>
			</c:when>
			<c:otherwise>
				<c:out value="N�o"/>
			</c:otherwise>
		</c:choose>
		</span><br />
		
		<label class="rotulo" for="indicadorEmail">Email:</label>
		<span class="itemDetalhamento">
		<c:choose>
			<c:when test="${unidadeOrganizacional.indicadorEmail == 'true'}">
				<c:out value="Sim"/>
			</c:when>
			<c:otherwise>
				<c:out value="N�o"/>
			</c:otherwise>
		</c:choose>	
		</span><br />
		
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<span class="itemDetalhamento">
		<c:choose>
			<c:when test="${unidadeOrganizacional.habilitado == 'true'}">
				<c:out value="Sim"/>
			</c:when>
			<c:otherwise>
				<c:out value="N�o"/>
			</c:otherwise>
		</c:choose>	
		</span><br />
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAtualizarUnidadeOrganizacional">    
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>

</form>
