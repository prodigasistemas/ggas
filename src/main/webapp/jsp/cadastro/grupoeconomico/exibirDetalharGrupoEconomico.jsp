<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Detalhar Grupo Econ�mico<a class="linkHelp" href="<help:help>/incluindogrupoeconomico.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar um Grupo Econ�mico clique em <span class="destaqueOrientacaoInicial">Alterar</span>.</p>

<script language="javascript">

	function cancelar() {				
		location.href = '<c:url value="/exibirPesquisaGrupoEconomico"/>';
	}
	
	function alterarGrupoEconomico(){		
		submeter("grupoEconomicoForm", "exibirAlterarGrupoEconomico");
	}
	
</script>

<form:form method="post" action="alterarGrupoEconomico" id="grupoEconomicoForm" name="grupoEconomicoForm"> 
	<input name="acao" type="hidden" id="acao" value="alterarGrupoEconomico"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${grupoEconomico.chavePrimaria}"/>
	
<fieldset id="pesquisaArrecadador" class="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisarBanco" class="coluna">			

		<label class="rotulo" for="descricao">Nome:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${grupoEconomico.descricao}"/></span>
		<br />
		<label class="rotulo" id="rotuloTipoSegmento" for="idSegmento">Segmento:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${segmento.descricao}"/></span>
		<br/>
		<label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
		<span class="itemDetalhamento" id="habilitado">
			<c:choose>
				<c:when test="${grupoEconomico.habilitado == 'true'}">Ativo</c:when>
				<c:otherwise>Inativo</c:otherwise>
			</c:choose>
		</span>
	</fieldset>	
</fieldset>
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input name="buttonRemover" value="Cancelar" class="bottonRightCol2 botaoAlterar" onclick="cancelar();" type="button">
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Alterar" type="button" onclick="alterarGrupoEconomico();">
	</fieldset>
			<br/>	
	<c:if test="${listaCliente ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaCliente" sort="list" id="cliente" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalharGrupoEconomico">
	     	<display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${cliente.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
 		     <display:column sortable="true" sortProperty="chavePrimaria" title="C�digo" style="center">
				<c:out value='${cliente.chavePrimaria}'/>
		     </display:column>
   		     <display:column title="C�digo Pai" style="center">
				<c:out value='${cliente.clienteResponsavel.chavePrimaria}'/>
		     </display:column>			 
		     <display:column sortable="true" sortProperty="nome" title="Nome do Cliente" style="center">
				<c:out value='${cliente.nome}'/>
		     </display:column>
		     <display:column sortable="true" sortProperty="nomeFantasia" title="Nome Fantasia" style="center">
				<c:out value='${cliente.nomeFantasia}'/>
		     </display:column>
		     <display:column title="CPF/CNPJ" style="center">
		     	<c:choose>
					<c:when test="${cliente.cpf ne null}">
						<c:out value='${cliente.cpf}'/>
					</c:when>
					<c:otherwise>
						<c:out value='${cliente.cnpj}'/>
					</c:otherwise>				
				</c:choose>
		     </display:column>
		     <display:column sortable="true" sortProperty="tipoCliente" title="Tipo Cliente" style="center">
				<c:out value='${cliente.tipoCliente.descricao}'/>
		     </display:column>
		</display:table>
	</c:if>
</form:form> 
