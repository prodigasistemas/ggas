<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Incluir Grupo Econ�mico<a class="linkHelp" href="<help:help>/incluindogrupoeconomico.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para incluir Clientes no Grupo Econ�mico, clique em, <span class="destaqueOrientacaoInicial">Pesquisar Pessoa</span>, ou apenas informe os dados obrigat�rios e clique em <span class="destaqueOrientacaoInicial">Incluir</span>.<br /></p>

<script language="javascript">

	$().ready(function(){
		$('input[name=tela]').val("incluir");
		$("input#descricao").keyup(removeExtraEspacoInicial).blur(removeExtraEspacoInicial);
	});
	
	function cancelar() {		
		location.href = '<c:url value="/exibirPesquisaGrupoEconomico"/>';
	}
	
	function removerClienteGrupoEconomico(idCliente){
		document.forms[0].idCliente.value = idCliente;
		document.getElementById('idCliente').value = idCliente;
		submeter("grupoEconomicoForm", "removerClienteGrupoEconomicoIncluir");
	}
	
	function exibirPopupPesquisaClienteMultSelect(){
		popup = window.open('exibirPesquisaClientePopupMultSelect','popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function incluirListaCliente(listaIdSelecionado) {
		if (listaIdSelecionado != '' && listaIdSelecionado != undefined) {
			document.forms[0].listaIdCliente.value = listaIdSelecionado;
			document.getElementById('listaIdCliente').value = document.forms[0].listaIdCliente.value;
			submeter('grupoEconomicoForm', 'incluirClienteGrupoEconomicoIncluir');	
		}
	}
	
	function limparFormGrupoEconomico(form){
		
		limparFormularios(form);
		document.getElementById("habilitado").checked = true;
		limparFormularioDadosCliente();
	}
	
</script>

<form:form method="post" action="incluirGrupoEconomico" id="grupoEconomicoForm" name="grupoEconomicoForm"> 
	<input name="acao" type="hidden" id="acao" value="incluirGrupoEconomico"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"/>
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${pesquisaCliente.chavesPrimarias}"/>
	<input name="listaIdCliente" type="hidden" id="listaIdCliente" value="${listaIdCliente}"/>
	<input name="idCliente" type="hidden" id="idCliente"/>
	<input name="habilitado" type="hidden" id="habilitado" value="true"/>
	<input name="tela" type="hidden" id="tela"/>
	
<fieldset id="pesquisaArrecadador" class="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisarBanco" class="coluna">			

		<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="30" size="30" value="${grupoEconomico.descricao}"
		onkeyup="return letraMaiuscula(this);" onkeypress="return letraMaiuscula(this);">
		
			<label class="rotulo" id="rotuloTipoSegmento" for="idSegmento">Segmento:</label>
			<select class="campoSelect" id="idSegmento" name="segmento">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${grupoEconomico.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>
		    	</c:forEach>
			</select>
		<br/>		
	</fieldset>	
</fieldset>
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<input id="pesquisarPessoa" name="Button" class="bottonRightCol bottonRightColUltimo" value="Pesquisar Pessoa" type="button" onclick="exibirPopupPesquisaClienteMultSelect();">
		<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormGrupoEconomico(this.form);">
	</fieldset>
			<br/>	
	<c:if test="${sessionScope.listaCliente ne null && not empty sessionScope.listaCliente}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="${sessionScope.listaCliente}" sort="list" id="cliente" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirIncluirGrupoEconomico">
	     	<display:column style="width: 30px" title="Ativo">
	     		<input type="hidden" id="chavesPrimarias" name="chavesPrimarias" value="${cliente.chavePrimaria}">
		     	<c:choose>
					<c:when test="${cliente.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
 		     <display:column sortable="true" sortProperty="chavePrimaria" title="C�digo Pai" style="center">
				<c:out value='${cliente.clienteResponsavel.chavePrimaria}'/>
		     </display:column>			 
		     <display:column sortable="true" sortProperty="nome" title="Nome Cliente" style="center">
				<c:out value='${cliente.nome}'/>
		     </display:column>
		     <display:column sortable="true" sortProperty="nomeFantasia" title="Nome Fantasia" style="center">
				<c:out value='${cliente.nomeFantasia}'/>
		     </display:column>
		     <display:column title="CPF/CNPJ" style="center">
		     	<c:choose>
					<c:when test="${cliente.cpf ne null}">
						<c:out value='${cliente.cpf}'/>
					</c:when>
					<c:otherwise>
						<c:out value='${cliente.cnpj}'/>
					</c:otherwise>				
				</c:choose>
		     </display:column>
			<display:column title="Remover" style="center">
				<a href='javascript:removerClienteGrupoEconomico(<c:out value='${cliente.chavePrimaria}'/>);'>
					<img  alt="Remover" title="Remover" src="<c:url value="/imagens/deletar_x.png"/>" border="0">
				</a>
			 </display:column>
		</display:table>
	</c:if>
	<fieldset class="conteinerBotoes">
		<input name="buttonRemover" value="Cancelar" class="bottonRightCol2 botaoAlterar" onclick="cancelar();" type="button">
			<input name="Button" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" type="submit">
	</fieldset>
</form:form> 
