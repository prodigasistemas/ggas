<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Pesquisar Grupo Econ�mico<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span>. Ou para alterar, ap�s pesquisar, selecione um registro e clique em <span class="destaqueOrientacaoInicial">Alterar</span>.
Para detalhamento de um registro clique no mesmo.</p>

<script language="javascript">

	$().ready(function(){
		$("input#descricao").keyup(removeExtraEspacoInicial).blur(removeExtraEspacoInicial);
		
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
	});

	function incluirGrupoEconomico() {		
		location.href = '<c:url value="/exibirIncluirGrupoEconomico" />';
	}

	function removerGrupoEconomico(){
		var selecao = verificarSelecao();
		if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('grupoEconomicoForm', 'removerGrupoEconomico');
				}
	    }
	} 	

	function detalharGrupoEconomico(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("grupoEconomicoForm", "exibirDetalharGrupoEconomico");
	}
	
	function alterarGrupoEconomico(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("grupoEconomicoForm", "exibirAlterarGrupoEconomico");
		}
	}
	
	function limparFormGrupoEconomico(form){
		
		limparFormularios(form);
		document.getElementById("habilitado").checked = true;
		limparFormularioDadosCliente();
	}
	
</script>

<form:form method="post" action="pesquisarGrupoEconomico" id="grupoEconomicoForm" name="grupoEconomicoForm"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarGrupoEconomico"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value=""/>
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
<!-- 	<input name="idCliente" type="hidden" id="idCliente"/> -->
	<fieldset id="pesquisaArrecadador" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.chavePrimaria}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nome}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${endereco}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
			
		<fieldset id="pesquisarBanco" class="colunaDir">			
	
			<label class="rotulo" for="descricao">Nome:</label>
			<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="30" size="30" value="${grupoEconomico.descricao}"
				onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)">
			
			<label class="rotulo" id="rotuloTipoSegmento" for="idSegmento">Segmento:</label>
			<select class="campoSelect" id="idSegmento" name="segmento">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${grupoEconomico.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>
		    	</c:forEach>
			</select>
			
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="ativo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="inativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="todos">Todos</label>
			<br/>		
		</fieldset>	
	</fieldset>
	<fieldset class="conteinerBotoesPesquisarDirFixo">		
	   		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
		<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormGrupoEconomico(this.form);">
	</fieldset>	
	<br/>	
	<c:if test="${listaGrupoEconomico ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaGrupoEconomico" sort="list" id="grupoEconomico" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarGrupoEconomico">
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="chavesPrimarias" value="${grupoEconomico.chavePrimaria}">
	     	</display:column>
	     	<display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${grupoEconomico.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
		     <display:column sortable="true" sortProperty="descricao" title="Nome do Grupo Econ�mico">
		     	<a href='javascript:detalharGrupoEconomico(<c:out value='${grupoEconomico.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${grupoEconomico.descricao}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" sortProperty="qtdCliente" title="Quantidade de Clientes">
		     	<a href='javascript:detalharGrupoEconomico(<c:out value='${grupoEconomico.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${grupoEconomico.qtdCliente}'/>
				</a>
		     </display:column>
		</display:table>
	</c:if>
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaGrupoEconomico}">
				<input id="buttonAlterar" name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarGrupoEconomico();" type="button">
				<input id="buttonRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerGrupoEconomico()" type="button">
		</c:if>
			<input id="buttonIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluirGrupoEconomico();" type="button">
	</fieldset>
</form:form>