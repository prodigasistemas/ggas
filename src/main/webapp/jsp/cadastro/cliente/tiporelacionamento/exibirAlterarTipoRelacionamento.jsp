<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Alterar Tipo de Relacionamento<a href="<help:help>/tiposdereainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>

    $(document).ready(function(){
        var max = 0;
  
        $('.rotulo').each(function(){
                if ($(this).width() > max)
                   max = $(this).width();   
            });
        $('.rotulo').width(max);
         
    });
    
    function salvar(){
    	 submeter('relacionamentoForm', 'alterarTipoRelacionamento');
    }
    	
    function cancelar(){    
    	submeter('relacionamentoForm', 'exibirPesquisaTipoRelacionamento');
    }

    function limparCampos(){
    	document.relacionamentoForm.descricao.value = "";
    	document.forms[0].indicadorPrincipal[0].checked = true;
    }

    /* function init() {
    	input.maxLength = 50;
    }

    addLoadEvent(init); */
</script>

<form:form method="post" enctype="multipart/form-data" action="alterarTipoRelacionamento" id="relacionamentoForm" name="relacionamentoForm">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tipoRelacionamento.chavePrimaria}">

<fieldset class="conteinerPesquisarIncluirAlterar">
     <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
        
	        <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
	        <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${tipoRelacionamento.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
	        <br /><br />
        	<label class="rotulo">Principal:</label>
            <input class="campoRadio" type="radio" name="principal" id="indicadorPrincipal" value="true"  <c:if test="${tipoRelacionamento.principal eq 'true'}">checked</c:if>>
            <label class="rotuloRadio" for="indicadorPrincipal">Sim</label>
            <input class="campoRadio" type="radio" name="principal" id="indicadorPrincipal" value="false"  <c:if test="${tipoRelacionamento.principal eq 'false'}">checked</c:if>>
            <label class="rotuloRadio" for="indicadorPrincipal">N�o</label>
    </fieldset>
    
    <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal colunaFinalAjuste">
        
        <label class="rotulo" for="habilitado">Indicador de Uso:</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${tipoRelacionamento.habilitado eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Ativo</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${tipoRelacionamento.habilitado eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Inativo</label><br />
    </fieldset>
            
    
    <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
</fieldset>

	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limparCampos();">
    
    <vacess:vacess param="alterarTronco">
        <input name="button" id="botaoSalvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="salvar();">
    </vacess:vacess>
     
	</fieldset>
</form:form>
