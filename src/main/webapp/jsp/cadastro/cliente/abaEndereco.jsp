<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script>

$(document).ready(function(){
	
	$('#botaoAlterarEndereco').hide();
	$('#botaoAlterarAnexoAbaEndereco').hide();	
	
	 $('table.tabelaEndereco').DataTable( {
    	 "responsive": true,
    	 "paging":   false,
         "ordering": false,
         "info": false,
         "searching": false,
    	 "language": {
             "zeroRecords": "Nenhum registro foi encontrado"
    	 }
    } );
 	
});


function alterarAnexoAbaEndereco(form, actionAdicionarAnexoAbaEndereco) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	submeter('clienteForm', 'adicionarAnexoAbaEndereco');
}

function incluirAnexoAbaEndereco(form, actionAdicionarAnexoAbaEndereco) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	document.forms[0].indexListaAnexoAbaEndereco.value = -1;
	submeter('clienteForm', 'adicionarAnexoAbaEndereco');
}

function removerAnexoAbaEndereco(actionRemoverAnexoAbaEndereco, indice) {
	var retorno = confirm('Deseja excluir o registro?');
	if(retorno == true){
		document.forms[0].status.value = true;
		document.forms[0].postBack.value = true;
		document.forms[0].indexListaAnexoAbaEndereco.value = indice;
		submeter('clienteForm', 'removerAnexoAbaEndereco');
	}
}

function limparAnexoAbaEndereco() {
	document.forms[0].indexListaAnexoAbaEndereco.value = -1;
	document.forms[0].idTipoAnexo.selectedIndex = 0;
	document.forms[0].descricaoAnexo.value = "";
	
}

function alterarEndereco() {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	submeter('clienteForm', 'adicionarEnderecoDoCliente');
}

function incluirEndereco(form) {
	$('#status').val(true);
	$('#postBack').val(true);
	document.getElementById('indexLista').value = -1;
	submeter('clienteForm', 'adicionarEnderecoDoCliente');
}

function removerEndereco(indice) {
	var retorno = confirm('Deseja excluir o endere�o?');
	if(retorno == true){
		document.forms[0].status.value = true;
		document.forms[0].postBack.value = true;
		document.getElementById('indexLista').value = indice;
		submeter('clienteForm', 'removerEnderecoDoCliente');
	}
}

function atualizarCorrespondencia(indice) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	document.getElementById('indexLista').value = indice;
	submeter('clienteForm', 'atualizarCorrespondenciaDoCliente');
}

function limparEndereco() {
	document.getElementById('indexLista').value = -1;
	document.forms[0].idTipoEndereco.selectedIndex = 0;
	document.forms[0].cep.value = "";
	document.forms[0].chaveCep.value = "";
	document.forms[0].numeroEndereco.value = "";
	document.forms[0].complementoEndereco.value = "";
	document.forms[0].referenciaEndereco.value = "";
	document.forms[0].caixaPostal.value = "";
	document.forms[0].indexLista.disabled = false;
	document.forms[0].idTipoEndereco.disabled = false;
	document.forms[0].cep.disabled = false;
	document.forms[0].numeroEndereco.disabled = false;
	document.forms[0].complementoEndereco.disabled = false;
	document.forms[0].referenciaEndereco.disabled = false;
	document.forms[0].caixaPostal.disabled = false;
	var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
	removeAllOptions(selectCep);

	$("#pesquisarCepcep").hide()
}

function exibirAlteracaoDoEndereco(indice,idTipoEndereco,cep,numeroEndereco,complementoEndereco,
		referenciaEndereco,caixaPostal,chaveCep,chavePrimariaEndereco, indicadorPrincipal) {
	if (indice != "") {
		document.getElementById('indexLista').value = indice;
		if (idTipoEndereco != "") {
			var tamanho = document.forms[0].idTipoEndereco.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idTipoEndereco.options[i];
				if (idTipoEndereco == opcao.value) {
					opcao.selected = true;
				}
			}
		}
		if (cep != "") {
			document.forms[0].cep.value = cep;
		} else {
			document.forms[0].cep.value = "";
		}
		if (numeroEndereco != "") {
			document.forms[0].numeroEndereco.value = numeroEndereco;
		} else {
			document.forms[0].numeroEndereco.value = '';
		}
		if (complementoEndereco != "") {
			document.forms[0].complementoEndereco.value = complementoEndereco;
		}
		if (referenciaEndereco != "") {
			document.forms[0].referenciaEndereco.value = referenciaEndereco;
		} else {
			document.forms[0].referenciaEndereco.value = "";
		}
		if (caixaPostal != "") {
			document.forms[0].caixaPostal.value = caixaPostal;
		}
		if (chaveCep != "") {
			document.forms[0].chaveCep.value = chaveCep;
		}
		if (chavePrimariaEndereco != ""){
			document.forms[0].chavePrimariaEndereco.value = chavePrimariaEndereco;
		}
		if (indicadorPrincipal != ""){
			if(indicadorPrincipal == "0"){
				document.getElementById("indicadorPrincipalNao").checked = true;
			}else{
				document.getElementById("indicadorPrincipalSim").checked = true;
			}			
		}
	}
}

function exibirIndexDoAnexo(indexListaAnexoAbaEndereco,chavePrimariaAnexo,descricaoAnexo,idTipoAnexo) {
	if (indexListaAnexoAbaEndereco != "") {
		document.forms[0].indexListaAnexoAbaEndereco.value = indexListaAnexoAbaEndereco;
		if (idTipoAnexo != "") {
			var tamanho = document.forms[0].idTipoAnexo.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idTipoAnexo.options[i];
				if (idTipoAnexo == opcao.value) {
					opcao.selected = true;
					break;
				}
			}
		}
		
		if (descricaoAnexo != "") {
			$("#descricaoAnexo").val(descricaoAnexo) 
		}else{
			$("#descricaoAnexo").val(""); 
		}
		
		if (chavePrimariaAnexo != ""){
			document.forms[0].chavePrimariaAnexo.value = chavePrimariaAnexo;
		}
		
	}
}

	function visualizarAnexoAbaEndereco(){
		submeter('clienteForm', 'visualizarAnexoAbaEnderecoDoCliente');
	}

	function onloadEndereco() {
		
		<c:choose>
			<c:when test="${sucessoManutencaoLista}">
				limparEndereco();
			</c:when>
		</c:choose>
		
		<c:choose>
		
		<c:when test="${sucessoManutencaoLista}">
			limparAnexoAbaEndereco();
		</c:when>
	</c:choose>
	}


<c:if test="${abaId == '1'}">
	addLoadEvent(onloadEndereco);
	
	$('#identificacao-tab').removeClass('active');
	$('#clienteAbaIdentificacao').removeClass('active show');
	$('#endereco-tab').addClass('active');
	$('#clienteAbaEndereco').addClass('active show');
	
	
</c:if>

function habilitarBotaoAlterarEndereco(){
	$('#botaoAlterarEndereco').show();
	$('#botaoIncluirEndereco').hide();
}

function habilitarBotaoAlterarAnexoAbaEndereco(){ 
	$('#botaoAlterarAnexoAbaEndereco').show();
	$('#botaoIncluirAnexoAbaEndereco').hide();
}

</script>

<div class="row">
	<h5 class="col-md-12 mt-2">Dados do Endere�o</h5>
	<div class="col-md-6">
		<div class="form-row mb-1">
			<label id="rotuloIndicadorPrincipal" class="col-md-12">Endere�o Principal: <span class="text-danger">*</span></label>
			<div class="col-md-12">
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" class="custom-control-input" name="indicadorPrincipal" id="indicadorPrincipalSim" value="1"
					<c:if test="${clienteEnderecoEnd.indicadorPrincipal == 1 or empty clienteEnderecoEnd.indicadorPrincipal}">checked="checked"</c:if>>
					<label class="custom-control-label" for="indicadorPrincipalSim">Sim</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" class="custom-control-input" name="indicadorPrincipal" id="indicadorPrincipalNao" value="0"
					<c:if test="${clienteEnderecoEnd.indicadorPrincipal == 0}">checked="checked"</c:if>>
					<label class="custom-control-label" for="indicadorPrincipalNao">N�o</label>
				</div>
			</div>
		</div>
		
		<div class="form-row">
			<div class="col-md-7">
				<label id="rotuloTipoEndereco" for="idTipoEndereco">Tipo de Endere�o: <span class="text-danger">*</span></label>
             	<select name="tipoEndereco" id="idTipoEndereco" class="form-control form-control-sm"> 
             		<option value="-1">Selecione</option>
					<c:forEach items="${listaTiposEndereco}" var="tipoEndereco">
						<option value="<c:out value="${tipoEndereco.chavePrimaria}"/>" <c:if test="${clienteEnderecoEnd.tipoEndereco.chavePrimaria == tipoEndereco.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoEndereco.descricao}"/>
						</option>		
					</c:forEach>
           		</select>
           	</div>
           	
           	<div class="col-md-5">
        		<input type="hidden" id="chavePrimariaEndereco" name="chavePrimariaEndereco" />
        		<label id="rotuloNumeroImovel" for="numeroEndereco">N�mero: <span class="text-danger">*</span></label>
        		<input type="text" id="numeroEndereco" name="numero" class="form-control form-control-sm" onkeypress="return formatarCampoNumeroEndereco(event, this);" maxlength="5" size="3" value="${clienteEnderecoEnd.numero}">
        	</div>
		</div>
		
		<div class="mt-4">
		 	<jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
           	 	<jsp:param name="cepObrigatorio" value="true"/>
            	<jsp:param name="idCampoCep" value="cep"/>
            	<jsp:param name="numeroCep" value="${clienteEnderecoEnd.cep.cep}"/>
            	<jsp:param name="chaveCep" value="${clienteEnderecoEnd.cep.chavePrimaria}"/>
        	</jsp:include>
    	</div>
    	<p><span class="text-danger">* </span>Campos Obrigat�rios para Cadastrar Endere�os.</p>
	</div><!-- fim da coluna -->
	
	<div class="col-md-6">
		<div class="form-row">
        	<div class="col-md-7">
        		<label id="rotuloComplemento" for="complementoEndereco">Complemento:</label>
        		<input type="text"id="complementoEndereco" name="complemento" class="form-control form-control-sm" 
                   onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"
                    maxlength="255" size="30" value="${clienteEnderecoEnd.complemento}">
        	</div>
        	
        	<div class="col-md-5">
        		<label for="caixaPostal">Caixa Postal:</label>
        		<input type="text" id="caixaPostal" name="caixaPostal" class="form-control form-control-sm" 
                    onkeypress="return formatarCampoInteiro(event);" value="${clienteEnderecoEnd.caixaPostal}"
                    maxlength="5" size="5" value="${clienteEnderecoEnd.enderecoReferencia}">
        	</div>
    	</div>
		
		<div class="form-row">
			<div class="col-md-12">
        		<label for="referenciaEndereco">Refer�ncia:</label>
        		<input type="text" id="referenciaEndereco" name="enderecoReferencia" class="form-control form-control-sm" 
                    onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" 
                    maxlength="30" size="30" value="${clienteEnderecoEnd.enderecoReferencia}">
    		</div>
    	</div>
    	
    	
    	<div class="form-row">
			<div class="col-md-12 align-self-center text-center mt-4">
				<button class="btn btn-primary btn-sm" name="botaoIncluirEndereco" id="botaoIncluirEndereco"
          			type="button"  onclick="incluirEndereco(this.form);">
                	<i class="fas fa-plus"></i> Adicionar
          		</button>
          		<button class="btn btn-success btn-sm" name="botaoAlterarEndereco" id="botaoAlterarEndereco"
          			type="button" onclick="alterarEndereco(this.form);">
                	<i class="fas fa-save"></i> Alterar
          		</button>
				<button class="btn btn-secondary btn-sm" name="botaoLimparEndereco" id="botaoLimparEndereco" 
					type="button" onclick="limparEndereco();">
              		<i class="far fa-trash-alt"></i> Limpar
          		</button>
			</div>
		</div>
	</div>
</div>



<c:if test="${listaClienteEndereco ne null}">

<div class="row mt-2">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover tabelaEndereco" id="endereco"  style="width:100%">
				 <thead class="thead-ggas-bootstrap">
				 	<tr>
				 		<th class="text-center">Tipo de Endere�o</th>
				 		<th class="text-center">Endere�o</th>
				 		<th class="text-center">CEP</th>
				 		<th class="text-center">Refer�ncia</th>
				 		<th class="text-center">Caixa Postal</th>
				 		<th class="text-center">Correspond�ncia</th>
				 		<th class="text-center">Principal</th>
				 		<th class="text-center">A��es</th>
				 	</tr>
				 </thead>
				 <tbody>
				 	<c:set var="i" value="0" />
				 	<c:forEach items="${listaClienteEndereco}" var="endereco">
				 		<tr>
				 			<td class="text-center">
				 				<c:out value="${endereco.tipoEndereco.descricao}"/>
				 			</td>
				 			<td class="text-center">
				 				<c:out value="${endereco.cep.tipoLogradouro}"/>
								<c:out value="${endereco.cep.logradouro}"/>,&nbsp;
								<c:out value="${endereco.numero}"/>,&nbsp;
								<c:out value="${endereco.cep.bairro}"/>,&nbsp;
								<c:out value="${endereco.cep.nomeMunicipio}"/>&nbsp;-&nbsp;
								<c:out value="${endereco.cep.uf}"/>&nbsp;-&nbsp;
								<c:out value="${endereco.complemento}"/>
				 			</td>
				 			<td class="text-center">
				 				<c:out value="${endereco.cep.cep}"/>
				 			</td>
				 			<td class="text-center">
				 				<c:out value="${endereco.enderecoReferencia}"/>
				 			</td>
				 			<td class="text-center">
				 				<c:out value="${endereco.caixaPostal}"/>
				 			</td>
				 			<td class="text-center">
				 				<c:choose>
	  								<c:when test="${endereco.correspondencia}">
	  									Sim
	  								</c:when>
	  							<c:otherwise>
	  								N�o
	  							</c:otherwise>
	  							</c:choose>
				 			</td>
				 			<td class="text-center">
				 				  <c:choose>
	  								<c:when test="${endereco.indicadorPrincipal==1}">
	  									Sim
	  								</c:when>
	  								<c:when test="${endereco.indicadorPrincipal==0}">
	  									N�o
	  								</c:when>
	  							</c:choose>
				 			</td>
				 			<td class="text-center">
				 				<c:choose>
				 					<c:when test="${endereco.correspondencia == false}">
				 					<button type="button" class="btn btn-primary btn-sm" title="Tornar Correspond�ncia" onClick="atualizarCorrespondencia(<c:out value="${i}"/>);">
				 						<i class="fas fa-envelope"></i>
				 					</button>
				 					</c:when>
				 				</c:choose>
				 				<button type="button" class="btn btn-info btn-sm" title="Alterar Endere�o" id="idBotaoAlterarEndereco"
        						onClick="exibirAlteracaoDoEndereco('${i}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}','${endereco.indicadorPrincipal}');habilitarBotaoAlterarEndereco()">
        						<i class="fas fa-pen-square"></i></button>
        						<button class="btn btn-secondary btn-sm" type="button" title="Remover Endere�o"
        						onClick="removerEndereco(<c:out value="${i}"/>);">
              					<i class="far fa-trash-alt"></i> 
          						</button>
				 			</td>
				 		</tr>
				 	<c:set var="i" value="${i+1}" />
				 	</c:forEach>
				 </tbody>
			</table>
		</div>
	</div>
</div>

</c:if>

<hr/>

<div class="row">

	<input type="hidden" id="chavePrimariaAnexo" name="chavePrimariaAnexo" />
	<h5 class="col-md-12">Documentos</h5>
	<div class="col-md-10">
		 <div class="form-row">
		 	<div class="col-md-8">
        		<label id="rotuloDescricaoDocumento" for="descricaoDocumento">Descri��o: <span> *</span></label>
        			<input type="text" class="form-control form-control-sm" maxlength="30" id="descricaoAnexo" name="descricaoAnexoEndVO" value="${ClienteAnexoEnderecoVO.descricaoAnexo}"/>
        	</div>
        	
        	<div class="col-md-4">
    		 	<label id="rotuloTipoDocumento" for="idTipoDocumento">Tipo de Anexo: <span>*</span></label>
             		<select name="tipoAnexoDeEndereco" id="idTipoAnexo" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoAnexo}" var="tipoAnexo">
								<option value="<c:out value="${tipoAnexo.chavePrimaria}"/>" <c:if test="${ClienteAnexoEnderecoVO.tipoAnexo.chavePrimaria == tipoAnexo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoAnexo.descricao}"/>
								</option>		
							</c:forEach>
           			</select>
           	</div>
		</div>
		 
		<div class="form-row">
			<div class="col-md-8">
				 <label id="rotuloDocumentoAnexo" for="anexo">Anexo em Formato PDF: <span>*</span></label>
    			 <input type="file" class="form-control-file" id="anexo" name="anexo" value="${anexo}">
			</div>
			<div class="col-md-4 align-self-center text-center mt-4">
				<button class="btn btn-primary btn-sm" name="botaoIncluirAnexoAbaEndereco" id="botaoIncluirAnexoAbaEndereco"
          			type="button" onclick="incluirAnexoAbaEndereco(this.form);">
                	<i class="fas fa-plus"></i> Adicionar
          		</button>
          		<button class="btn btn-success btn-sm" name="botaoAlterarAnexoAbaEndereco" id="botaoAlterarAnexoAbaEndereco"
          			type="button" onclick="alterarAnexoAbaEndereco(this.form,'<c:out value="${param['actionAdicionarAnexoAbaEndereco']}"/>');">
                	<i class="fas fa-save"></i> Alterar
          		</button>
				<button class="btn btn-secondary btn-sm" name="botaoLimparAnexoAbaEndereco" id="botaoLimparAnexoAbaEndereco"
					type="button" onclick="limparAnexoAbaEndereco();">
              		<i class="far fa-trash-alt"></i> Limpar
          		</button>
			</div>
		</div>
	</div> 
	
</div>

<c:if test="${listaClienteAnexoAbaEndereco ne null}">

<div class="row mt-2">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover tabelaEndereco" id="anexo" style="width:100%">
				<thead class="thead-ggas-bootstrap">
					<tr>
						<th class="text-center">Descri��o</th>
				 		<th class="text-center">Tipo</th>
				 		<th class="text-center">Data de Inclus�o</th>
				 		<th class="text-center">A��es</th>
					</tr>
				</thead>
				<tbody>
					<c:set var="j" value="0" />
					<c:forEach items="${listaClienteAnexoAbaEndereco}" var="anexo">
						<tr>
							<td class="text-center">
								${anexo.descricaoAnexo}
							</td>
							<td class="text-center">
								${anexo.tipoAnexo.descricao}
							</td>
							<td class="text-center">
								<fmt:formatDate pattern="dd/MM/yyyy" value="${anexo.ultimaAlteracao}" var="formattedStatusDate" />
								<c:out value="${formattedStatusDate}"/>
					
								<fmt:formatDate pattern="HH:mm" value="${anexo.ultimaAlteracao}" var="formattedStatusHour" />
								<c:out value="${formattedStatusHour}"/>
							</td>
							<td class="text-center">
								<button type="button" class="btn btn-primary btn-sm" title="Download de Anexo"
	        						onClick="exibirIndexDoAnexo('${j}','${anexo.chavePrimaria}','${anexo.descricaoAnexo}','${anexo.tipoAnexo.chavePrimaria}');visualizarAnexoAbaEndereco();" id="botaoVisualizarAnexo">
	        						<i class="fas fa-download"></i></button>
	        							
					 		    <button type="button" class="btn btn-info btn-sm" title="Alterar Anexo" id="idExibirIndexEndereco"
	        						onClick="exibirIndexDoAnexo('${j}','${anexo.chavePrimaria}','${anexo.descricaoAnexo}','${anexo.tipoAnexo.chavePrimaria}');habilitarBotaoAlterarAnexoAbaEndereco()">
	        						<i class="fas fa-pen-square"></i></button>
	        						
	        					<button type="button" class="btn btn-secondary btn-sm" title="Remover Anexo"
	        						onClick="removerAnexoAbaEndereco('<c:out value="${param['actionRemoverAnexoAbaEndereco']}"/>',<c:out value="${j}"/>);">
	        						<i class="far fa-trash-alt"></i></button>
							</td>
						</tr>
						<c:set var="j" value="${j+1}" />
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

</c:if>	
