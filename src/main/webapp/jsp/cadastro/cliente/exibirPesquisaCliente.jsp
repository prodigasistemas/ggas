<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<style>
	th a,th a:visited {
		color: #fff;
	}
	th a{
		color: #ffffff !important;
	}
</style>

<div class="bootstrap">
	<form:form method="post" action="pesquisarCliente" id="clienteForm" name="clienteForm" modelAttribute="ClienteImpl">
	        	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
				<input name="chavePrimarias" type="hidden" id="chavePrimarias">
				<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
				<input type="hidden" name="status" id="status">
				<input name="comAS" type="hidden" id="comAS" value="false">
		
		<div class="card">
			<div class="card-header">
            	<h5 class="card-title mb-0">Pesquisar Pessoa</h5>
        	</div>
        	<div class="card-body">
				
				<div class="alert alert-primary fade show" role="alert">
         			<i class="fa fa-question-circle"></i>
         			Para pesquisar um registro espec�fico, informe os dados nos campos abaixo ou nos campos de pesquisa espec�fica (Pessoa F�sica ou Pessoa Jur�dica) e clique em
        			<strong>Pesquisar</strong>, ou clique apenas em <strong>Pesquisar</strong> para exibir todos. Para incluir um novo
        			 registro clique em <strong>Incluir</strong>
   			 	</div>
   			 	
   			 	<div class="row mb-2">
   			 		<div class="col-md-6">
   			 			<div class="form-row mb-2">
             		 			 <label for="nome" class="col-sm-3 mb-1">Nome</label>
             		 			 <div class="col-sm-8">
                                 <input class="form-control form-control-sm" type="text" 
                                         id="nome" name="nome" value="${clienteForm.nome}" maxlength="50" size="30"
                                        onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
             		 		</div>
             		 	</div>
             		 	<div class="form-row mb-2">
    	  					<label class="col-sm-3 mb-1">Tipo de Pessoa</label>
            				<div class="col-sm-8">
                				<div class="custom-control custom-radio custom-control-inline">
                 					<input type="radio" name="tipoCliente" id="codigoTipoPessoa1" class="custom-control-input" value="1"
                      				<c:if test="${clienteForm.tipoCliente.tipoPessoa.codigo eq 1}">checked="checked"</c:if>>
                      				<label class="custom-control-label" for="codigoTipoPessoa1">F�sica</label>
                				</div>
                				<div class="custom-control custom-radio custom-control-inline">
                					<input type="radio" name="tipoCliente" id="codigoTipoPessoa2" class="custom-control-input" value="2"
                      				<c:if test="${clienteForm.tipoCliente.tipoPessoa.codigo eq 2}">checked="checked"</c:if>>
                     				<label class="custom-control-label" for="codigoTipoPessoa2">Jur�dica</label>
                				</div>
                				<div class="custom-control custom-radio custom-control-inline">
                					<input type="radio"  name="tipoCliente" id="codigoTipoPessoaAmbos" class="custom-control-input" value="0"
                      				<c:if test="${clienteForm.tipoCliente.tipoPessoa.codigo ne 1 && clienteForm.tipoCliente.tipoPessoa.codigo ne 2}">checked="checked"</c:if>>
                      				<label class="custom-control-label" for="codigoTipoPessoaAmbos">Todos</label>
               		 			</div>
    						</div>
    					</div>
    					
    					<div class="form-row mb-2">
    	  					<label for="habilitado" class="col-sm-3 mb-1">Indicador de Uso</label>
            				<div class="col-sm-8">
                				<div class="custom-control custom-radio custom-control-inline">
                 					<input type="radio" id="habilitadoAtivo" name="habilitado" class="custom-control-input" value="true"
                      				<c:if test="${clienteForm.habilitado eq 'true' or empty habilitado}">checked="checked"</c:if>>
                      				<label class="custom-control-label" for="habilitadoAtivo">Ativo</label>
                				</div>
                				<div class="custom-control custom-radio custom-control-inline">
                					<input type="radio" id="habilitadoInativo" name="habilitado" class="custom-control-input" value="false"
                      				<c:if test="${clienteForm.habilitado eq 'false'}">checked="checked"</c:if>>
                     				<label class="custom-control-label" for="habilitadoInativo">Inativo</label>
                				</div>
                				<div class="custom-control custom-radio custom-control-inline">
                					<input type="radio" id="habilitadoTodos" name="habilitado" class="custom-control-input" value="null"
                      				<c:if test="${clienteForm.habilitado eq 'null'}">checked="checked"</c:if>>
                      				<label class="custom-control-label" for="habilitadoTodos">Todos</label>
               		 			</div>
    						</div>
    					</div>
    					
   			 		</div>
   			 		<div class="col-md-6">
   			 			<div class="col-md-10">
   			 				<jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
            					<jsp:param name="cepObrigatorio" value="false"/>
            					<jsp:param name="idCampoCep" value="cep"/>
            					<jsp:param name="numeroCep" value="${cep}"/>
        					</jsp:include>
   			 			 
   			 			</div><!-- componente de cep -->
   			 		</div>
   			 	</div><!-- fim da primeira row -->
   			 	
   			 	<div class="row">
   			 		<div class="col-md-12 mt-2">	
   			 			<div id="accordion">	
   			 				<div class="card">
   			 					<div class="card-header" id="pessoaFisica">
     								<h5 class="mb-0">
       								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#linkPessoaFisica" aria-expanded="false" aria-controls="linkPessoaFisica">
          								Pesquisa F�sica
        							</button></h5>
    							</div>
    							<div id="linkPessoaFisica" class="collapse" aria-labelledby="pessoaFisica" data-parent="#accordion">
 									<div class="card-body">
 										<div class="row">
 											<div class="col-md-6">
 											<div class="form-row col-md-10">
             		 			 				<label for="rotuloCPF">CPF:</label>
                                 					<input class="form-control form-control-sm" type="text"
                                        			 id="cpf" name="cpf" value="${clienteForm.cpf}" maxlength="14" size="14"
                                       				 onkeyup="return 'formatarCampoNome(event)');"/>
             		 						</div>
             		 					
 											</div>
 											<div class="col-md-6">
 											<div class="form-row col-md-10">
             		 			 				<label for="passaporte">Passaporte:</label>
                                 					<input class="form-control form-control-sm" type="text"
                                        		 	id="passaporte" name="numeroPassaporte" value="${clienteForm.numeroPassaporte}" maxlength="13" size="13"
                                       				 onkeyup="return 'formatarCampoNome(event)');"/>
             		 						</div>
             		 						</div>
 										</div>
 									</div>
 								</div>
   			 				</div><!--  Primeiro card -->
   			 				<div class="card">
   			 					<div class="card-header" id="pessoaJuridica">
     								<h5 class="mb-0">
       								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#linkPessoaJuridica" aria-expanded="false" aria-controls="linkPessoaJuridica">
          								Pesquisa Jur�dica
        							</button></h5>
    							</div>
    							<div id="linkPessoaJuridica" class="collapse" aria-labelledby="pessoaJuridica" data-parent="#accordion">
 									<div class="card-body">
 										<div class="row">
 											<div class="col-md-6">
 												<div class="form-row">
             		 								<div class="col-md-10">
             		 			 						<label for="nomeFantasia">Nome Fantasia:</label>
                                 						<input class="form-control form-control-sm" type="text"
                                         				id="nomeFantasia" name="nomeFantasia" value="${clienteForm.nomeFantasia}" maxlength="50" size="40"
                                        				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
             		 								</div>
             		 							</div>
             		 							
             		 							<div class="form-row">
             		 								<div class="col-md-10">
             		 			 						<label for="cnpj">CNPJ:</label>
                                 						<input class="form-control form-control-sm" type="text"
                                         				id="cnpj" name="cnpj" value="${clienteForm.cnpj}" maxlength="50" size="40"/>
             		 								</div>
             		 							</div>
             		 							
             		 							<div class="form-row">
             		 								<div class="col-md-10">
             		 								<label id="rotuloAtividadeEconomia" for="descricaoCompletaAtividadeEconomica">Atividade Econ�mica:</label>
             		 								<div class="input-group">
  														<input type="hidden" name="atividadeEconomia" id="idAtividadeEconomia"
  												 		value="${clienteForm.atividadeEconomica.chavePrimaria}"/>
  														<input type="hidden" id="descricaoAtividadeEconomica"
  														name="descricaoAtividadeEconomica" value="${clienteForm.atividadeEconomica.descricao}"/>
  														<textarea class="form-control" rows="2" id="descricaoCompletaAtividadeEconomica"
  														name="descricaoCompletaAtividadeEconomica" disabled></textarea>
 													 <div class="input-group-append">
   											 			<button class="btn btn-primary mb-0" type="button" id="botaoPesquisarAtividadeEconomica"
   											 			onclick="exibirPopupPesquisaAtividadeEconomica();"><i class="fas fa-search"></i></button>
   											 		</div>
 												 </div>
 												 </div>
             		 						 </div>
             		 							
 											</div>
 											<div class="col-md-6">
 												<div class="form-row">
             		 								<div class="col-md-10">
             		 			 						<label for="inscricaoEstadual">Inscri��o Estadual:</label>
                                 						<input class="form-control form-control-sm" type="text"
                                         				id="inscricaoEstadual" name="inscricaoEstadual" value="${clienteForm.inscricaoEstadual}" maxlength="25" size="25"/>
             		 								</div>
             		 							</div>
             		 							
             		 							<div class="form-row">
             		 								<div class="col-md-10">
             		 			 						<label for="inscricaoMunicipal">Inscri��o Municipal:</label>
                                 						<input class="form-control form-control-sm" type="text"
                                         				id="inscricaoMunicipal" name="inscricaoMunicipal" value="${clienteForm.inscricaoMunicipal}" maxlength="25" size="25"/>
             		 								</div>
             		 							</div>
             		 							
             		 							<div class="form-row">
             		 								<div class="col-md-10">
             		 			 						<label for="inscricaoRural">Inscri��o Rural:</label>
                                 						<input class="form-control form-control-sm" type="text"
                                         				id="inscricaoRural" name="inscricaoRural" value="${clienteForm.inscricaoRural}" maxlength="25" size="25"/>
             		 								</div>
             		 							</div>
 											</div>
 										
 										</div>
 									</div>
 								</div>
   			 				</div><!-- Segundo card -->
   			 			</div>	
   			 		</div>
   			 	</div><!-- fim da segunda linha -->
   			 	
   			 	 <div class="row mt-3">
                    <div class="col align-self-end text-right">
                    	<vacess:vacess param="pesquisarCliente">
                       	 	<button class="btn btn-primary btn-sm" id="botaoPesquisar" type="submit">
                            	<i class="fa fa-search"></i> Pesquisar
                        	</button>
                        </vacess:vacess>
                        <button class="btn btn-secondary btn-sm" name="botaoLimpar" id="botaoLimpar" value="limparFormulario" type="button"
                                onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                    </div>
                </div>
                
				<c:if test="${listaCliente ne null}">
				    <hr/>
				
				    <div class="text-center loading">
				        <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
				    </div>
				
				    <div class="table-responsive">
				       <table class="table table-bordered table-striped table-hover" id="table-clientes" width="100%"
                               style="display: none;">
				            <thead class="thead-ggas-bootstrap">
				            <tr>
				                <th>
				                    <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
				                        <input id="checkAllAuto" type="checkbox"  name="checkAllAuto"
				                               class="custom-control-input">
				                        <label class="custom-control-label p-0" for="checkAllAuto"></label>
				                    </div>
				                </th>
				                <th scope="col" class="text-center">Ativo</th>
				                <th scope="col" class="text-center">C�digo</th>
				                <th scope="col" class="text-center">Pessoa</th>
				                <th scope="col" class="text-center">Nome Fantasia</th>
				                <th scope="col" class="text-center">CPF/CNPJ</th>
				                <th scope="col" class="text-center">Tipo</th>
				                <th scope="col" class="text-center">Situa��o</th>
				                <th scope="col" class="text-center">Negativado</th>
				            </tr>
				            </thead>
				            <tbody>
				            <c:forEach items="${listaCliente}" var="cliente">
				                <tr>
				                    <td>
				                        <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1" data-identificador-check="chk${cliente.chavePrimaria}">
				                            <input id="chk${cliente.chavePrimaria}" type="checkbox"  name="chavesPrimarias"
				                                   class="custom-control-input"
				                                   value="${cliente.chavePrimaria}">
				                            <label class="custom-control-label p-0" for="chk${cliente.chavePrimaria}"></label>
				                        </div>
				                    </td>
				                    <td class="text-center">
				                    		<a title="Habilitado">
				                    		<c:choose>
												<c:when test="${cliente.habilitado == true}">
													<i class="fas fa-check-circle text-success" title="Ativo"></i>
												</c:when>
												<c:otherwise>
													<i class="fas fa-ban text-danger" title="Inativo"></i>
												</c:otherwise>
											</c:choose>
											</a>
				                    </td>
				                    <td>
				                        <a href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                class="linkInvisivel"></span>
				                            <c:out value="${cliente.chavePrimaria}"/>
				                        </a>
				                    </td>
				                    <td>
				                        <a href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                class="linkInvisivel"></span>
				                            <c:out value="${cliente.nome}"/>
				                        </a>
				                    </td>
				                    <td>
				                        <a href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                class="linkInvisivel"></span>
				                            <c:out value="${cliente.nomeFantasia}"/>
				                        </a>
				                    </td>
				                    <td>
				                        <a href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                class="linkInvisivel"></span>
				                            <c:choose>
				                                <c:when test="${cliente.cpf eq null || cliente.cpf eq ''}">
				                                    <a class='comAS'
				                                       href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                            class="linkInvisivel"></span>
				                                        <c:out value="${cliente.cnpjFormatado}"/>
				                                    </a>
				                                </c:when>
				                                <c:otherwise>
				                                    <a class='comAS'
				                                       href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                            class="linkInvisivel"></span>
				                                        <c:out value='${cliente.cpfFormatado}' />
				                                    </a>
				                                </c:otherwise>
				                            </c:choose>
				                        </a>
				                    </td>
				                    <td>
				                        <a href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                class="linkInvisivel"></span>
				                            <c:out value="${cliente.tipoCliente.descricao}"/>
				                        </a>
				                    </td>
				                    <td>
				                        <a href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                class="linkInvisivel"></span>
				                            <c:out value="${cliente.clienteSituacao.descricao}"/>
				                        </a>
				                    </td>
									<td class="text-center">
				                        <a href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                class="linkInvisivel"></span>
				                            <c:choose>
				                                <c:when test="${cliente.indicadorNegativado ne null && cliente.indicadorNegativado eq 'true'}">
				                                    <a 
				                                       href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                            class="linkInvisivel"></span>
				                                        Sim
				                                    </a>
				                                </c:when>
				                                <c:otherwise>
				                                    <a 
				                                       href="javascript:detalharCliente(<c:out value='${cliente.chavePrimaria}'/>);"><span
				                                            class="linkInvisivel"></span>
				                                        N�o
				                                    </a>
				                                </c:otherwise>
				                            </c:choose>
				                        </a>
				                    </td>				                    
				                </tr>
				            </c:forEach>
				            </tbody>
				        </table>
				    </div>
				</c:if>
   	</div><!--  fim do card-body -->
   	
        	<div class="card-footer">
        		<c:if test="${not empty listaCliente}">
        			<vacess:vacess param="removerCliente">
        				<button id="buttonRemover" value="removerCliente" type="button" class="btn btn-danger btn-sm ml-1 mt-1 float-right" onclick="removerCliente();">
        					<i class="far fa-trash-alt"></i> Remover
    					</button>
        			</vacess:vacess>
        			<vacess:vacess param="exibirAlteracaoCliente">
        				<button id="buttonAlterar" value="alterarCliente" type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-right" onclick="alterarCliente();">
        					<i class="fas fa-pencil-alt"></i> Alterar
    					</button>
        			</vacess:vacess>
        		</c:if>
        		<vacess:vacess param="exibirInclusaoCliente">
                  	<button id="buttonIncluir" value="Incluir" class="btn btn-sm btn-primary float-right ml-1 mt-1" onclick="incluir();">
                    	<i class="fa fa-plus"></i> Incluir
                   	</button>
             	</vacess:vacess>
        	</div>
		</div>
	</form:form>
</div>

<script src="${ctxWebpack}/dist/modulos/cadastro/cliente/pesquisarCliente/exibirPesquisaCliente/index.js"></script>
<script src="${pageContext.request.contextPath}/js/cadastro/cliente/exibirPesquisaCliente/index.js"
        type="application/javascript"></script>
