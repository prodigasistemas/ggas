<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script>

$(document).ready(function(){

	 $('table.table').DataTable( {
    	 "responsive": true,
    	 "paging":   false,
         "ordering": false,
         "info": false,
         "searching": false,
    	 "language": {
             "zeroRecords": "Nenhum registro foi encontrado"
    	 }
    } );
 	
});
	
	var tiposPessoaFisica = new Array();
	
	function carregarArrays() {
		<c:forEach items="${tiposClientes}" var="tipoCliente">
		 <c:if test="${tipoCliente.tipoPessoa.codigo == tipoPessoaFisica}">
		 tiposPessoaFisica[tiposPessoaFisica.length] = '${tipoCliente.chavePrimaria}';
		 </c:if>
		</c:forEach>
	}
	
	function voltar(){
		if($("#isTelaChamado").val() == 'true') {
			submeter('clienteForm','visualizarInformacoesPontoConsumo');
		} else {
			submeter("clienteForm",  "pesquisarCliente");
		}
	}
	
	function alterar(){
		document.forms[0].status.value = false;
		document.forms[0].postBack.value = true;
		submeter('clienteForm', 'exibirAlteracaoCliente');
	}
	
	function exibirCamposDocumentacao(idTipoCliente) {
		var exibirPessoaFisica = false;

		if (idTipoCliente != "" && idTipoCliente != -1) {
			for (var i=0; i<tiposPessoaFisica.length; i++) {
				if (tiposPessoaFisica[i] == idTipoCliente) {
					exibirPessoaFisica = true;
					break;
				}
			}
			
			if (exibirPessoaFisica == true) {
				exibirElemento('detalhamentoPessoaFisica');
				ocultarElemento('detalhamentoPessoaJuridica');
			} else {
				exibirElemento('detalhamentoPessoaJuridica');
				ocultarElemento('detalhamentoPessoaFisica');
			}	
		} else {
			ocultarElemento('detalhamentoPessoaJuridica');
			ocultarElemento('detalhamentoPessoaFisica');
		}
	}
	
	function manterExibicaoDocumentacaoNecessaria() {
		var idTipoCliente = "${clienteForm.tipoCliente.chavePrimaria}";
		if (idTipoCliente != "") {
			exibirCamposDocumentacao(idTipoCliente);	
		}
	}
	
	function visualizarAnexoAbaEndereco(indexAbaEndereco){
		document.forms[0].indexListaAnexoAbaEndereco.value = indexAbaEndereco;
		submeter('clienteForm', 'visualizarAnexoAbaEnderecoDoCliente');
	}
	
	function visualizarAnexoAbaIdentificacao(indexAbaIdentificacao){
		document.forms[0].indexListaAnexoAbaIdentificacao.value = indexAbaIdentificacao;
		submeter('clienteForm', 'visualizarAnexoAbaIdentificacao');
	}
	
	function init () {
		carregarArrays();
		manterExibicaoDocumentacaoNecessaria();
	}
	
	addLoadEvent(init);

</script>

<div class="bootstrap">
	<form:form method="post" action="exibirDetalhamentoCliente" enctype="multipart/form-data" id="clienteForm" name="clienteForm">
		<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoCliente">
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${clienteForm.chavePrimaria}">
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${clienteForm}">
		<input name="indexListaAnexoAbaEndereco" type="hidden" id="indexLista" value="${clienteForm}">
		<input name="indexListaAnexoAbaIdentificacao" type="hidden" id="indexLista" value="${clienteForm}">
		<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">
		<input type="hidden" name="status" id="status">
		<input type="hidden" id="isTelaChamado" name="isTelaChamado" value="${isTelaChamado}"/>
		<input type="hidden" id="idPontoConsumo" name="idPontoConsumo" value="${idPontoConsumo}"/>
		
		
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">
					Detalhar Pessoa
				</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
         			<i class="fa fa-question-circle"></i>
         			Para modificar as informa��es deste registro clique em <strong>Alterar</strong>.
   			 	</div>
   			 	<div class="row">
   			 		<h5 class="col-md-12 mt-2">Dados Gerais</h5>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					C�digo: <span class="font-weight-bold">
								<c:out value='${clienteForm.chavePrimaria}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Nome: <span class="font-weight-bold">
  								<c:out value='${clienteForm.nome}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					Nome Abreviado: <span class="font-weight-bold">
  								<c:out value='${clienteForm.nomeAbreviado}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					Conta auxiliar: <span class="font-weight-bold">
  								<c:out value='${clienteForm.contaAuxiliar}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					E-mail principal: <span class="font-weight-bold">
  								<c:out value='${clienteForm.emailPrincipal}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					E-mail secund�rio: <span class="font-weight-bold">
  								<c:out value='${clienteForm.emailSecundario}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					Situa��o: <span class="font-weight-bold">
  								<c:out value='${clienteForm.clienteSituacao.descricao}'/></span>
   			 				</li>
  							<li class="list-group-item">
   			 					Grupo Econ�mico: <span class="font-weight-bold">
  								<c:out value='${clienteForm.grupoEconomico.descricao}'/></span>
   			 				</li>
						</ul>
   			 		</div>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					Funcion�rio Fiscal: <span class="font-weight-bold">
								<c:out value='${clienteForm.funcionarioFiscal.nome}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Funcion�rio Vendedor: <span class="font-weight-bold">
  								<c:out value='${clienteForm.funcionarioVendedor.nome}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					Segmento: <span class="font-weight-bold">
  								<c:out value='${clienteForm.segmento.descricao}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					Tipo: <span class="font-weight-bold">
  								<c:out value='${clienteForm.tipoCliente.descricao}'/></span>
   			 				</li>
							<li class="list-group-item">
   			 					Denegado: <span class="font-weight-bold">
  								<c:if test="${clienteForm.indicadorDenegado eq true}">Sim</c:if>
								<c:if test="${clienteForm.indicadorDenegado eq false}">N�o</c:if>
								</span>
   			 				</li>
							<li class="list-group-item">
   			 					Tipo de Cliente: <span class="font-weight-bold">
  								<c:choose>
		      					<c:when test="${clienteForm.clientePublico eq true}">
		      						P�blico
		      					</c:when>
		      					<c:otherwise>
		      						Particular
		      					</c:otherwise>
		      					</c:choose></span>
   			 				</li>
							<li class="list-group-item">
   			 					Data de Envio de Brindes: <span class="font-weight-bold">
  								<fmt:formatDate  value="${clienteForm.dataEnvioBrindes}" pattern="dd/MM/yyyy" /></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Negativado: <span class="font-weight-bold">
  								<c:if test="${clienteForm.indicadorNegativado eq true}">Sim</c:if>
  								<c:if test="${clienteForm.indicadorNegativado eq false}">N�o</c:if>
   			 				</li>
  							
						</ul>

   			 		</div>
   			 	</div><!-- fim da primeira linha -->
   			 
   			 	<div class="row" id="detalhamentoPessoaFisica" style="display: none;">
   			 		<h5 class="col-md-12 mt-2 pb-2">Identifica��o de Pessoa F�sica</h5>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					Nacionalidade: <span class="font-weight-bold">
  								<c:out value='${clienteForm.nacionalidade.descricao}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					CPF: <span class="font-weight-bold">
  								<c:out value='${clienteForm.cpfFormatado}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					RG: <span class="font-weight-bold">
  								<c:out value='${clienteForm.rg}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Data de Emiss�o: <span class="font-weight-bold">
  								<fmt:formatDate value="${clienteForm.dataEmissaoRG}" pattern="dd/MM/yyyy" /></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Org�o Emissor: <span class="font-weight-bold">
  								<c:out value='${clienteForm.orgaoExpedidor.descricao}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					UF: <span class="font-weight-bold">
  								<c:out value='${clienteForm.unidadeFederacao.descricao}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Passaporte: <span class="font-weight-bold">
  								<c:out value='${clienteForm.numeroPassaporte}'/></span>
   			 				</li>
   			 			</ul>
		
   			 		</div>
   			 		
   			 		<div class="col-md-6">
   			 			
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					Data de Nascimento: <span class="font-weight-bold">
  								<fmt:formatDate value="${clienteForm.dataNascimento}" pattern="dd/MM/yyyy" /></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Profiss�o: <span class="font-weight-bold">
  								<c:out value='${clienteForm.profissao.descricao}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Sexo: <span class="font-weight-bold">
  								<c:out value='${clienteForm.sexo.descricao}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Nome da M�e: <span class="font-weight-bold">
  								<c:out value='${clienteForm.nomeMae}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Nome do Pai: <span class="font-weight-bold">
  								<c:out value='${clienteForm.nomePai}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Renda Familiar: <span class="font-weight-bold">
  								<fmt:formatNumber value="${clienteForm.rendaFamiliar.valorFaixaInicio}" minFractionDigits="2" type="currency"/> 
								<c:if test="${clienteForm.rendaFamiliar.valorFaixaFim ne ''}"> - </c:if>
								<fmt:formatNumber value="${clienteForm.rendaFamiliar.valorFaixaFim}" minFractionDigits="2" type="currency"/>
								</span>
   			 				</li>
   			 			</ul>

   			 		</div>
   			 		
   			 	</div>
   			 	<hr />
   			 	<div class="row" id="detalhamentoPessoaJuridica" style="display: none;">
   			 		<h5 class="col-md-12 mt-2 pb-2">Identifica��o de Pessoa Jur�dica</h5>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					Nome Fantasia: <span class="font-weight-bold">
  								<c:out value='${clienteForm.nomeFantasia}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					CNPJ: <span class="font-weight-bold">
  								<c:out value='${clienteForm.cnpjFormatado}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Inscri��o Estadual: <span class="font-weight-bold">
  								<c:out value='${clienteForm.inscricaoEstadual}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Isento de Inscri��o Estadual: <span class="font-weight-bold">
  								<c:choose>
		      						<c:when test="${clienteForm.regimeRecolhimento eq 'true'}">
		      							Sim
		      						</c:when>
		      					<c:otherwise>
		      						N�o
		      					</c:otherwise>
		      				</c:choose></span>
   			 				</li>
   			 			</ul>
   			 		</div>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					Inscri��o Municipal: <span class="font-weight-bold">
  								<c:out value='${clienteForm.inscricaoMunicipal}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Inscri��o Rural: <span class="font-weight-bold">
  								<c:out value='${clienteForm.inscricaoRural}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Atividade Econ�mica: <span class="font-weight-bold">
  								<c:out value='${clienteForm.atividadeEconomica.descricao}'/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Cliente Superior: <span class="font-weight-bold">
  								<c:out value='${clienteForm.clienteResponsavel.nome}'/></span>
   			 				</li>
   			 			</ul>

   			 		</div>
   			 	</div>
				<hr />
				<div class="row">
					<div class="col-md-12">
						<h5>Anexos da Identifica��o</h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="anexoAbaIdentificacao" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
   			 							<th scope="col" class="text-center">Descri��o do Documento</th>
   			 							<th scope="col" class="text-center">Data de Inclus�o</th>
   			 						</tr>
								</thead>
								<tbody>
									<c:set var="j" value="0" />	
									<c:forEach items="${listaClienteAnexoAbaIdentificacao}" var="anexoAbaIdentificacao">
										<tr>
											<td class="text-center">
												<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${j}'/>);'>
													<c:out value='${anexoAbaIdentificacao.descricaoAnexo}'/>
												</a>
											</td>
											<td class="text-center">
												<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusDate" />
												<c:out value="${formattedStatusDate}"/>
					
												<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusHour" />
												<c:out value="${formattedStatusHour}"/>
											</td>
										</tr>
										<c:set var="j" value="${j+1}" />
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div><!-- fim da row de anexo -->
				<hr />
				<div class="row">
					<div class="col-md-12">
						<c:if test="${listaClienteEndereco ne null}">
						<h5>Endere�os</h5>
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover" id="endereco" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
   			 							<th scope="col" class="text-center">Tipo de Endere�o</th>
   			 							<th scope="col" class="text-center">Endere�o</th>
   			 							<th scope="col" class="text-center">Refer�ncia</th>
   			 							<th scope="col" class="text-center">Caixa Postal</th>
   			 							<th scope="col" class="text-center">Principal</th>
   			 						</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaClienteEndereco}" var="endereco">
										<tr>
											<td class="text-center">
												 <c:out value="${endereco.tipoEndereco.descricao}"/>
											</td>
											<td class="text-center">
												<c:out value="${endereco.enderecoFormatado}"/>
											</td>
											<td class="text-center">
												<c:out value="${endereco.enderecoReferencia}"/>
											</td>
											<td class="text-center">
												<c:out value="${endereco.caixaPostal}"/>
											</td>
											<td class="text-center">
												<c:choose>
		      									<c:when test="${endereco.correspondencia}">
		      										Sim
		      									</c:when>
		      									<c:otherwise>
		      										N�o
		      									</c:otherwise>
		      									</c:choose>
											</td>
										</tr>
									</c:forEach>
								</tbody>
								</table>
							</div>
						</c:if>
					</div>
				</div>
				<hr />
				
				<div class="row">
					<div class="col-md-12">
						<h5>Anexos do Endere�o</h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="anexoAbaEndereco" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
   			 							<th scope="col" class="text-center">Descri��o do Documento</th>
   			 							<th scope="col" class="text-center">Data de Inclus�o</th>
   			 						</tr>
								</thead>
								<tbody>
									<c:set var="x" value="0" />	
									<c:forEach items="${listaClienteAnexoAbaEndereco}" var="anexoAbaEndereco">
										<tr>
											<td class="text-center">
												<a href='javascript:visualizarAnexoAbaEndereco(<c:out value='${x}'/>);'>
													<c:out value='${anexoAbaEndereco.descricaoAnexo}'/>
												</a>
											</td>
											<td class="text-center">
												<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaEndereco.ultimaAlteracao}" var="formattedStatusDate" />
												<c:out value="${formattedStatusDate}"/>
					
												<fmt:formatDate pattern="HH:mm" value="${anexoAbaEndereco.ultimaAlteracao}" var="formattedStatusHour" />
												<c:out value="${formattedStatusHour}"/>
											</td>
										</tr>
										<c:set var="x" value="${x+1}" />
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div><!-- fim da row de anexo -->
				<hr />
				<div class="row">
					<div class="col-md-12">
					<h5>Telefones</h5>
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" id="telefone" style="width:100%">
							<thead class="thead-ggas-bootstrap">
								<tr>
   			 						<th scope="col" class="text-center">Tipo de Telefone</th>
   			 						<th scope="col" class="text-center">DDD</th>
   			 						<th scope="col" class="text-center">N�mero</th>
   			 						<th scope="col" class="text-center">Ramal</th>
   			 						<th scope="col" class="text-center">Principal</th>
   			 					</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaClienteFone}" var="telefone">
									<tr>
										<td class="text-center">
											 <c:out value="${telefone.tipoFone.descricao}"/>
										</td>
										<td class="text-center">
											 <c:out value="${telefone.codigoDDD}"/>
										</td>
										<td class="text-center">
											 <c:out value="${telefone.numero}"/>
										</td>
										<td class="text-center">
											 <c:out value="${telefone.ramal}"/>
										</td>
										<td class="text-center">
											 <c:choose>
		      									<c:when test="${telefone.indicadorPrincipal}">
		      										Sim
		      									</c:when>
		      									<c:otherwise>
		      										N�o
		      									</c:otherwise>
		      								</c:choose>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					
					</div>
				</div>
				<hr />
				
				<div class="row">
					<div class="col-md-12">
					<h5>Contatos</h5>
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" id="contato" style="width:100%">
							<thead class="thead-ggas-bootstrap">
								<tr>
   			 						<th scope="col" class="text-center">Tipo de Contato</th>
   			 						<th scope="col" class="text-center">DDD</th>
   			 						<th scope="col" class="text-center">N�mero</th>
   			 						<th scope="col" class="text-center">Ramal</th>
   			 						<th scope="col" class="text-center">Cargo</th>
   			 						<th scope="col" class="text-center">�rea</th>
   			 						<th scope="col" class="text-center">E-mail</th>
   			 						<th scope="col" class="text-center">Principal</th>
   			 					</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaContato}" var="contato">
									<tr>
										<td class="text-center">
											 <c:out value="${contato.tipoContato.descricao}"/>
										</td>
										<td class="text-center">
											 <c:out value="${contato.codigoDDD}"/>
										</td>
										<td class="text-center">
											 <c:out value="${contato.fone}"/>
										</td>
										<td class="text-center">
											 <c:out value="${contato.ramal}"/>
										</td>
										<td class="text-center">
											 <c:out value="${contato.profissao.descricao}"/>
										</td>
										<td class="text-center">
											 <c:out value="${contato.descricaoArea}"/>
										</td>
										<td class="text-center">
											 <c:out value="${contato.email}"/>
										</td>
										<td class="text-center">
											 <c:choose>
		      									<c:when test="${contato.principal}">
		      										Sim
		      									</c:when>
		      									<c:otherwise>
		      										N�o
		      									</c:otherwise>
		      								</c:choose>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					</div>
				</div>
				
				<hr />
				
				<div class="row">
					<div class="col-md-12">
						<h5>Im�veis</h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="clienteImovel" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
   			 							<th scope="col" class="text-center">Matr�cula do Im�vel</th>
   			 							<th scope="col" class="text-center">Descri��o</th>
   			 							<th scope="col" class="text-center">Tipo de Relacionamento</th>
   			 							<th scope="col" class="text-center">In�cio da Rela��o</th>
   			 						</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaClienteImovel}" var="clienteImovel">
										<tr>
											<td class="text-center">
											 	<c:out value="${clienteImovel.imovel.chavePrimaria}"/>
											</td>
											<td class="text-center">
											 	<c:out value="${clienteImovel.imovel.nome}"/>
											</td>
											<td class="text-center">
											 	<c:out value="${clienteImovel.tipoRelacionamentoClienteImovel.descricao}"/>
											</td>
											<td class="text-center">
											 	<fmt:formatDate value="${clienteImovel.relacaoInicio}" type="both" pattern="dd/MM/yyyy" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- fim do card-body -->
			<div class="card-footer">
				<div class="row">
      				<div class="col-sm-12">
      					<button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="voltar();">
                            <i class="fas fa-undo-alt"></i> Voltar
                        </button>
                        <vacess:vacess param="exibirAlteracaoCliente"> 
                        	<button id="buttonAlterar" class="btn btn-sm btn-primary float-right ml-1 mt-1"  type="button" onclick="alterar();">
                       			<i class="fas fa-edit"></i> Alterar
                   			</button>
                        </vacess:vacess>   
      				</div>
      			</div>
      		</div>
		</div><!-- fim do card -->
		
	</form:form>
</div>

