
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Incluir Tipo de Pessoa<a href="<help:help>/cadastrodotipodeclienteinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script lang="javascript">


    $(document).ready(function(){
         var max = 0;

          $('.rotulo').each(function(){
                  if ($(this).width() > max)
                     max = $(this).width();   
              });
          $('.rotulo').width(max);
                    
        });

   

    function salvar(){
        
        submeter('tipoPessoaForm', "incluirTipoPessoa");

    }

    function cancelar(){    
        
        submeter('tipoPessoaForm', "exibirPesquisaTipoPessoa");
    }

    function limparCampos(){

    	document.getElementById('descricao').value = "";
    	document.getElementById('idTipoPessoa').value = "-1";
        document.getElementById('idEsferaPoder').value = "-1";
    }
    

    
</script>

<form method="post" id="tipoPessoaForm" name="tipoPessoaForm" 
	enctype="multipart/form-data" action="incluirTipoPessoa">
	
<input name="postBack" type="hidden" id="postBack" value="true">

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">

			<label class="rotulo campoObrigatorio"><span
				class="campoObrigatorioSimbolo">* </span>Descrição:</label> <input
				class="campoTexto campoHorizontal" type="text" name="descricao"
				id="descricao" maxlength="50" size="30"
				value="${tipoCliente.descricao}"
				onblur="this.value = removerEspacoInicialFinal(this.value);"
				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
			<br /> <br /> <label class="rotulo">Esfera de Poder:</label> <select
				name="esferaPoder" class="campoSelect" id="idEsferaPoder">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaEsferaPoder}" var="esferaPoder">
					<option value="<c:out value="${esferaPoder.chavePrimaria}"/>"
						<c:if test="${tipoCliente.esferaPoder.chavePrimaria == esferaPoder.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${esferaPoder.descricao}" />
					</option>
				</c:forEach>
			</select><br />

		</fieldset>

		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">

			<label class="rotulo campoObrigatorio"><span
				class="campoObrigatorioSimbolo">* </span>Tipo Pessoa:</label> <select
				name="tipoPessoa" class="campoSelect" id="idTipoPessoa">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoPessoa}" var="tipoPessoa">
					<option value="<c:out value="${tipoPessoa.key}"/>"
						<c:if test="${tipoCliente.tipoPessoa.codigo == tipoPessoa.key}">selected="selected"</c:if>>
						<c:out value="${tipoPessoa.value}" />
					</option>
				</c:forEach>
			</select><br />
			<br />

		</fieldset>

		<p class="legenda">
			<span class="campoObrigatorioSimbolo">* </span>campos obrigatórios
		</p>

	</fieldset>


	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
    
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="javascript:limparCampos();">
    
     <input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" id="botaoSalvar" onclick="javascript:salvar();">
   
</fieldset>
</form>
