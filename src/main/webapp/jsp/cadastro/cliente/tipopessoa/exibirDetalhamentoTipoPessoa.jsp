
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Detalhar Tipo de Pessoa<a href="<help:help>/cadastrodotipodeclientedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informações deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>
		$(document).ready(function(){
			  var max = 0;
		
		    $('.rotulo').each(function(){
		            if ($(this).width() > max)
		               max = $(this).width();   
		        });
		    $('.rotulo').width(max);
			
		});
		
		function voltar(){	
			submeter('tipoPessoaForm', "exibirPesquisaTipoPessoa");
		}
		
		function alterar(){
			submeter('tipoPessoaForm', "exibirAlteracaoTipoPessoa");
		}


</script>

<form method="post" enctype="multipart/form-data" id="tipoPessoaForm"
	name="tipoPessoaForm">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"
		value="${tipoCliente.chavePrimaria}">
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">

			<label class="rotulo">Descrição:</label> <span
				class="itemDetalhamento"><c:out
					value="${tipoCliente.descricao}" /></span> <br />
			<br /> <label class="rotulo">Esfera de Poder:</label> <span
				class="itemDetalhamento"><c:out
					value="${tipoCliente.esferaPoder.descricao}" /></span><br />


		</fieldset>

		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">


			<label class="rotulo">Tipo Pessoa:</label> <span
				class="itemDetalhamento"><c:out
					value="${tipoCliente.tipoPessoa.descricao}" /></span><br /> <br />
		</fieldset>
	</fieldset>



	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Voltar"
			type="button" onclick="voltar();">


		<vacess:vacess param="exibirAlteracaoTipoPessoa">
			<input name="button"
				class="bottonRightCol2 botaoGrande1 botaoAlterar" id="botaoAlterar"
				value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>

	</fieldset>
</form>
