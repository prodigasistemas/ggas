<!--
 Copyright (C) <2019> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Softwaregit add 
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %> 

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>

	
var tiposPessoaFisica = new Array();
var nacionalidadesEstrangeira = new Array();

function carregarArrays() {
	//Tipos de clientes
	<c:forEach items="${tiposClientes}" var="tipoCliente">
	 <c:if test="${tipoCliente.tipoPessoa.codigo == tipoPessoaFisica}">
	 tiposPessoaFisica[tiposPessoaFisica.length] = '${tipoCliente.chavePrimaria}';
	 </c:if>
	</c:forEach>
	//Nacionalidades
	<c:forEach items="${nacionalidades}" var="nacionalidade">
	 <c:if test="${nacionalidade.estrangeira == true}">
	 nacionalidadesEstrangeira[nacionalidadesEstrangeira.length] = '${nacionalidade.chavePrimaria}';
	 </c:if>
	</c:forEach>
}

function limparFormulario(){

	$(":text").val("");
	$("select").val("-1");
	
	exibirDocumentacaoNecessaria(document.clienteForm.idTipoCliente);
}

function cancelar(){
	location.href = '<c:url value="/exibirPesquisaCliente"/>';
}
	
function exibirDocumentacaoNecessaria(idTipoCliente) {
	exibirCamposDocumentacao(idTipoCliente.value);
	habilitarDocumentacaoNecessariaNacionalidade(document.forms[0].idNacionalidade);	
}

function incluirCliente(){
	submeter("clienteForm","incluirCliente");
}

function exibirCamposDocumentacao(idTipoCliente) {
	var exibirPessoaFisica = false;
	
	if (idTipoCliente != "" && idTipoCliente != -1) {
		for (var i=0; i<tiposPessoaFisica.length; i++) {
			if (tiposPessoaFisica[i] == idTipoCliente) {
				exibirPessoaFisica = true;
				break;
			}
		}
		
		if (exibirPessoaFisica == true) {
			exibirElemento('pessoaFisica');
			ocultarElemento('pessoaJuridica');
		} else {
			exibirElemento('pessoaJuridica');
			ocultarElemento('pessoaFisica');
		}	
	} else {
		ocultarElemento('pessoaJuridica');
		ocultarElemento('pessoaFisica');
	}
}

function habilitarDocumentacaoNecessariaNacionalidade(idNacionalidade) {
	exibirDocumentacaoNecessariaNacionalidadeHabilitado(idNacionalidade.value);	
}

function exibirDocumentacaoNecessariaNacionalidadeHabilitado(idNacionalidade) {
	var desabilidarDadosNacionalidadeBrasileira = false;
	
	if (idNacionalidade != "" && idNacionalidade != -1) {
		for (var i=0; i<nacionalidadesEstrangeira.length; i++) {
			if (nacionalidadesEstrangeira[i] == idNacionalidade) {
				desabilidarDadosNacionalidadeBrasileira = true;
				break;
			}
		}
		
		if (desabilidarDadosNacionalidadeBrasileira == true) {
			
			//Desabilitar os campos da nascionalidade brasileira;
			document.clienteForm.cpf.disabled = false;
			document.clienteForm.rg.disabled = true;
			document.clienteForm.dataEmissaoRG.disabled = true;
			document.clienteForm.idOrgaoEmissor.disabled = true;
			document.clienteForm.idOrgaoEmissorUF.disabled = true;
			document.clienteForm.passaporte.disabled = false;
			document.getElementById("obrigatorioPassaporte").style.display = "";
			document.getElementById("obrigatorioCPF").style.display = "none";

		} else {
			//habilitar os campos da nascionalidade estrangeira;
			document.clienteForm.cpf.disabled = false;
			document.clienteForm.rg.disabled = false;
			document.clienteForm.dataEmissaoRG.disabled = false;
			document.clienteForm.idOrgaoEmissor.disabled = false;
			document.clienteForm.idOrgaoEmissorUF.disabled = false;
			document.clienteForm.passaporte.disabled = true;
			document.getElementById("obrigatorioPassaporte").style.display = "none";
			document.getElementById("obrigatorioCPF").style.display = "";
			
		}	
	} else {
		//Desabilitar os campos da pessoa da nascionalidade brasileira e estrangeira
		document.clienteForm.cpf.disabled = true;
		document.clienteForm.rg.disabled = true;
		document.clienteForm.dataEmissaoRG.disabled = true;
		document.clienteForm.idOrgaoEmissor.disabled = true;
		document.clienteForm.idOrgaoEmissorUF.disabled = true;
		document.clienteForm.passaporte.disabled = true;
		document.getElementById("obrigatorioPassaporte").style.display = "none";
		document.getElementById("obrigatorioCPF").style.display = "none";

		
	}
}

function manterExibicaoDocumentacaoNecessaria() {
	
	var idTipoCliente = "${clienteForm.tipoCliente.chavePrimaria}";
	var idNacionalidade = "${clienteForm.nacionalidade.chavePrimaria}";
	if (idTipoCliente != "") {
		exibirCamposDocumentacao(idTipoCliente);	
	}
	if (idNacionalidade != "") {
		exibirDocumentacaoNecessariaNacionalidadeHabilitado(idNacionalidade);	
	}
}

function init(){
	carregarArrays();
	manterExibicaoDocumentacaoNecessaria();
}

//abilita campos desabilitados para poder pegar o valor na action
function abilitarCamposDesabilitados(){
	abilitarInscricaoEstadual();
}

function carregarComboTipoContato(combo){
	var idTipoCliente = combo.value;

	var selectTipoContato = document.clienteForm.idTipoContato;
	selectTipoContato.disabled = false;
	selectTipoContato.length = 0;
	selectTipoContato.options[selectTipoContato.length] = new Option('Selecione', '-1');
		
	AjaxService.obterListaTipoContatoPorTipoPessoa( idTipoCliente, {
       	callback:function(pessoas) {  

       		for(key in pessoas){
       			var novaOpcao = new Option(pessoas[key], key);
       			selectTipoContato.options[selectTipoContato.length] = novaOpcao;
       			
       		}
    	}, async:false}
    );	
}

function identificarLabelTipoCliente(component){
	//tipo 1 pessoa f�sica
	//tipo 2 pessoa jur�dica
	
	var idTipoCliente = component.value;
	var rotuloArrayNome = $("#rotuloNome");
	
	if (idTipoCliente == -1){
		$(rotuloArrayNome).html("Nome:");
		return;
	}
	
   	AjaxService.obterTipoClientePorIdTipoCliente( idTipoCliente, {
       	callback:function(tipoCliente) {            		      		         		
       		var idTipoPessoa = tipoCliente["cdTipoPessoa"];       		
       		var rotuloArrayNome = $("#rotuloNome"); 
       		
       		if (idTipoPessoa == 1){
       			$(rotuloArrayNome).html("Nome:");
       		}else if(idTipoPessoa == 2){
       			$(rotuloArrayNome).html("Raz�o Social:")
       		}else{
       			$(rotuloArrayNome).html("Nome:");
       		}
       		
    	}, async:false}
    );	
}

function carregarClientePublico (component){
	//tipo 1 pessoa f�sica
	//tipo 2 pessoa jur�dica
	var idTipoCliente = component.value;

	
   	AjaxService.obterTipoClientePorIdTipoCliente( idTipoCliente, {
       	callback:function(tipoCliente) {            		      		         		
       		var idTipoPessoa = tipoCliente["cdTipoPessoa"];       		
       		
       		if (idTipoPessoa == 1){

       			$("#clientePublico1").attr("disabled","disabled");
	   			$("#clientePublico2").attr("disabled","disabled");
	   			document.clienteForm.clientePublico.value = "false"
       		}else if(idTipoPessoa == 2){
	   			$("#clientePublico1").removeAttr("disabled");
	   			$("#clientePublico2").removeAttr("disabled");
       		}else{
	   			$("#clientePublico1").removeAttr("disabled");
	   			$("#clientePublico2").removeAttr("disabled");
	   			document.clienteForm.clientePublico.value = "false"
       		}
       		
    	}, async:false}
    );	
}

$(document).ready(function() {
	init();
});

</script>
<div class="bootstrap">
	<form:form method="post" action="incluirCliente" id="clienteForm" name="clienteForm" onsubmit="abilitarCamposDesabilitados()" enctype="multipart/form-data">
	
		<input name="fluxo" type="hidden" id="fluxo" value="inclusao"> 
		<input name="acao" type="hidden" id="acao" value="incluirCliente"/>
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${clienteForm.chavePrimaria}"/>
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
		<input name="indexLista" type="hidden" id="indexLista" value="${-1}">
		<input name="indexListaAnexoAbaEndereco" type="hidden" id="indexLista" value="${indexListaAnexoAbaEndereco}">
		<input name="indexListaAnexoAbaIdentificacao" type="hidden" id="indexLista" value="${indexListaAnexoAbaIdentificacao}">
		<input name="indicadorIntegracaoCadastroCliente" type="hidden" id="indicadorIntegracaoCadastroCliente" value="${indicadorIntegracaoCadastroCliente}">
		<input type="hidden" name="status" id="status">
	
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Incluir Pessoa</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Informe os dados abaixo e clique em <strong>Incluir</strong> para finalizar.
   			 	</div>
   			 	<div class="row">
   			 		<div class="col-md-6">
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 					<label id="rotuloTipo" for="idTipoCliente">Tipo: <span class="text-danger">*</span></label>
             					<select name="tipoCliente" id="idTipoCliente" class="form-control form-control-sm" 
             						 onchange="identificarLabelTipoCliente(this); exibirDocumentacaoNecessaria(this); carregarComboTipoContato(this); carregarClientePublico(this);">
                  					<option value="-1">Selecione</option>
                  					<c:forEach items="${tiposClientes}" var="tipoCliente">
										<option value="<c:out value="${tipoCliente.chavePrimaria}"/>" title="<c:out value="${tipoCliente.descricao}"/>" <c:if test="${clienteForm.tipoCliente.chavePrimaria == tipoCliente.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${tipoCliente.descricao}"/>
										</option>
									</c:forEach>
           						</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="nome"><span id="rotuloNome">Nome:</span><span class="text-danger"> *</span></label>
        		 				<input type="text" id="nome" name="nome" class="form-control form-control-sm"
                            	onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" 
                            	maxlength="255" value="${clienteForm.nome}">
        		 			</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="nomeAbreviado">Nome Abreviado:</label>
        		 				<input type="text" id="nomeAbreviado" name="nomeAbreviado" class="form-control form-control-sm" 
                            	onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" 
                            	maxlength="20" value="${clienteForm.nomeAbreviado}">
        		 			</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="contaAuxiliar">Conta auxiliar:</label>
        		 				<input type="text"  id="contaAuxiliar" name="contaAuxiliar" class="form-control form-control-sm" disabled
                            	placeholder="Exclusivo para Integra��o Pir�mide" maxlength="2000" size="50" value="${clienteForm.contaAuxiliar}">
        		 			</div>
        		 		</div>
        		 		
   			 			<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="emailPrincipal">E-mail Principal: <c:if test="${exigeEmailPrincialObrigatorio == 1}"><span class="text-danger">*</span></c:if></label>
        		 				<input type="text" id="emailPrincipal" name="emailPrincipal" class="form-control form-control-sm" 
                            	maxlength="80" onkeypress="return formatarCampoEmailMultiplo(event);" value="${clienteForm.emailPrincipal}">
        		 			</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="emailSecundario">E-mail Secund�rio: </label>
        		 				<input type="text" id="emailSecundario"  name="emailSecundario" class="form-control form-control-sm" 
                            	maxlength="80" onkeypress="return formatarCampoEmailMultiplo(event);" value="${clienteForm.emailSecundario}">
        		 			</div>
        		 		</div>
        		 		
        		 		
        		 		
   			 		</div>
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 					<label for="idSituacaoCliente">Situa��o: </label>
             					<select name="clienteSituacao" id="idClienteSituacao" class="form-control form-control-sm">
                  					<option value="-1">Selecione</option>
                  					<c:forEach items="${situacoesClientes}" var="clienteSituacao">
										<option value="<c:out value="${clienteSituacao.chavePrimaria}"/>" <c:if test="${clienteForm.clienteSituacao.chavePrimaria == clienteSituacao.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${clienteSituacao.descricao}"/>
										</option>	
									</c:forEach>
           						</select>
           					</div>
        		 		</div>
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 					<label for="grupoEconomico">Grupo Econ�mico: </label>
             					<select  name="grupoEconomico" id="idGrupoEconomico" class="form-control form-control-sm"> 
                  					<option value="-1">Selecione</option>
                  					<c:forEach items="${listaGrupoEconomico}" var="grupoEconomico">
										<option value="<c:out value="${grupoEconomico.chavePrimaria}"/>" <c:if test="${clienteForm.grupoEconomico.chavePrimaria == grupoEconomico.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${grupoEconomico.descricao}"/>
										</option>		
									</c:forEach>
           						</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
             				<div class="col-md-10">
    		 					<label for="idFuncionarioFiscal">Funcion�rio Fiscal: </label>
             					<select name="funcionarioFiscal" id="idFuncionarioFiscal" class="form-control form-control-sm"> 
                  					<option value="-1">Selecione</option>
                  					<c:forEach items="${listaFuncionarioFiscal}" var="funcionarioFiscal">
										<option value="<c:out value="${funcionarioFiscal.chavePrimaria}"/>" <c:if test="${clienteForm.funcionarioFiscal.chavePrimaria == funcionarioFiscal.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${funcionarioFiscal.nome}"/>
										</option>		
									</c:forEach>
           						</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
             				<div class="col-md-10">
    		 					<label for="idFuncionarioVendedor">Funcion�rio Vendedor:</label>
             					<select name="FuncionarioVendedor" id="idFuncionarioVendedor" class="form-control form-control-sm"> 
                  					<option value="-1">Selecione</option>
                  					<c:forEach items="${listaFuncionarioVendedor}" var="funcionarioVendedor">
										<option value="<c:out value="${funcionarioVendedor.chavePrimaria}"/>" <c:if test="${clienteForm.funcionarioVendedor.chavePrimaria == funcionarioVendedor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${funcionarioVendedor.nome}"/>
										</option>		
									</c:forEach>
           						</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
             				<div class="col-md-10">
    		 					<label for="idSegmentoCliente">Segmento:<span class="text-danger"> *</span></label>
             					<select name="segmento" id="idSegmentoCliente" class="form-control form-control-sm"> 
                  					<option value="-1">Selecione</option>
									<c:forEach items="${listaSegmento}" var="segmento">
										<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${clienteForm.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${segmento.descricao}"/>
										</option>		
									</c:forEach>
           						</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
							<label for="clientePublico">Tipo de Cliente:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="clientePublico1" name="clientePublico" class="custom-control-input" value="true"
										   <c:if test="${clienteForm.clientePublico == true}">checked</c:if>>
									<label class="custom-control-label" for="clientePublico1">P�blico</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="clientePublico2" name="clientePublico" class="custom-control-input" value="false"
										  <c:if test="${clienteForm.clientePublico == false or empty clienteForm.clientePublico}">checked</c:if>>
									<label class="custom-control-label" for="clientePublico2">Particular</label>
								</div>
							</div>
						</div>
						
   			 		</div>
   			 	</div><!-- fim da primeira row -->
   			 	
   			 
   			 	<div class="card mt-3">
                    <div class="card-header">
   			 		<ul class="nav nav-tabs card-header-tabs" id="cadastro" role="cadastroLista">
  						<li class="nav-item">
   						 	<a class="nav-link active" id="identificacao-tab" data-toggle="tab" href="#clienteAbaIdentificacao" role="tab" 
   						 	aria-controls="clienteAbaIdentificacao"><i class="fa fa-user-tie"></i><strong> Identifica��o</strong> <span class="text-danger">*</span></a>
  						</li>
 						<li class="nav-item">
    						<a class="nav-link" id="endereco-tab" data-toggle="tab" href="#clienteAbaEndereco" role="tab"
    						 aria-controls="clienteAbaEndereco"><i class="fas fa-address-card"></i><strong> Endere�o</strong> <span class="text-danger">*</span></a>
  						</li>
  						<li class="nav-item">
   							 <a class="nav-link" id="telefone-tab" data-toggle="tab" href="#clienteAbaTelefone" role="tab"
   							  aria-controls="clienteAbaTelefone"><i class="fas fa-phone"></i><strong> Telefone </strong><span class="text-danger">*</span></a>
  						</li>
  						<li class="nav-item">
   							 <a class="nav-link" id="contatos-tab" data-toggle="tab" href="#clienteAbaContatos" role="tab"
   							  aria-controls="clienteAbaContatos"><i class="fas fa-address-book"></i><strong> Contatos</strong></a>
  						</li>
					</ul>
					</div>
					<div class="card-body">
					<div class="tab-content" id="cliente">
  						<div class="tab-pane fade active show" id="clienteAbaIdentificacao" role="tabpanel" aria-labelledby="identificacao-tab">
  							<jsp:include page="/jsp/cadastro/cliente/abaIdentificacao.jsp">
			
								<jsp:param name="actionAdicionarAnexoAbaIdentificacao" value="adicionarAnexoAbaIdentificacaoFluxoInclusao" />
								<jsp:param name="actionRemoverAnexoAbaIdentificacao" value="removerAnexoAbaIdentificacaoFluxoInclusao" />
							</jsp:include>
  						</div>
  						<div class="tab-pane fade" id="clienteAbaEndereco" role="tabpanel" aria-labelledby="endereco-tab">
  							<jsp:include page="/jsp/cadastro/cliente/abaEndereco.jsp">
								<jsp:param name="actionAdicionarEndereco" value="adicionarEnderecoDoCliente" />
								<jsp:param name="actionRemoverEndereco" value="removerEnderecoDoClienteFluxoInclusao" />
								<jsp:param name="actionAtualizarCorrespondencia" value="atualizarCorrespondenciaDoClienteFluxoInclusao" />
				
								<jsp:param name="actionAdicionarAnexoAbaEndereco" value="adicionarAnexoAbaEnderecoFluxoInclusao" />
								<jsp:param name="actionRemoverAnexoAbaEndereco" value="removerAnexoAbaEnderecoFluxoInclusao" />
				
							</jsp:include>
  						</div>
  						<div class="tab-pane fade" id="clienteAbaTelefone" role="tabpanel" aria-labelledby="telefone-tab">
  							<jsp:include page="/jsp/cadastro/cliente/abaTelefone.jsp">
								<jsp:param name="actionAdicionarTelefone" value="adicionarTelefoneDoClienteFluxoInclusao" />
								<jsp:param name="actionRemoverTelefone" value="removerTelefoneDoClienteFluxoInclusao" />
								<jsp:param name="actionAtualizarTelefonePrincipal" value="atualizarTelefonePrincipalDoClienteFluxoInclusao" />
							</jsp:include>
  						</div>
  						<div class="tab-pane fade" id="clienteAbaContatos" role="tabpanel" aria-labelledby="contatos-tab">
  							<jsp:include page="/jsp/cadastro/cliente/abaContato.jsp">
								<jsp:param name="actionAdicionarContato" value="adicionarContatoDoClienteFluxoInclusao" />
								<jsp:param name="actionRemoverContato" value="removerContatoDoClienteFluxoInclusao" />
								<jsp:param name="actionAtualizarContatoPrincipal" value="atualizarContatoPrincipalDoClienteFluxoInclusao" />
							</jsp:include>
						</div>
					</div>
   			 	
   			 	</div>
   	
   			 	</div><!-- fim da segunda linha -->
   			 	
			</div><!-- fim do card-body -->
			
			<div class="card-footer">
				<div class="row">
      				<div class="col-sm-12">
                        <button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="cancelar();">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                        <button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                        <vacess:vacess param="incluirCliente">
                  		<button id="buttonIncluir" value="Incluir" class="btn btn-sm btn-primary float-right ml-1 mt-1"  type="button" onclick="incluirCliente();">
                       		<i class="fa fa-plus"></i> Incluir                   		
                       	</button>
             		 	</vacess:vacess>      				
      				</div>
      			</div>
			</div>
		</div><!-- fim do card -->
		
	</form:form>
</div>
