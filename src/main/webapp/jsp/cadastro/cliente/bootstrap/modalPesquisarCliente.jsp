<!--
Copyright (C) <2018> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2018 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->




<%--

    JSP respons�vel por gerenciar a sele��o de clientes atrav�s de um modal.

    Para utiliz�-la importe: web/componentes/pesquisarCliente/modal.ts e
    adicione esta JSP em sua p�gina com <jsp:include/>

    OBS: N�o Inclua esta JSP dentro de um <form> j� existente, caso contr�rio n�o ir� funcionar corretamente

    Verifique o c�digo fonte 'modal.ts' para mais informa��es sobre o uso desse componente

    @author jose.victor@logiquesistemas.com.br

--%>

<div id="modalPesquisarCliente"  class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pesquisar Pessoa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="alert alert-primary" role="alert">
                    <i class="fa fa-lg fa-fw fa-info-circle"></i>
                    Para pesquisar um cliente, informe os dados abaixo e clique em Pesquisar
                </div>

                <form id="modalPesquisarCliente_form">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_nome">Nome:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_nome" />
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_nomeFantasia">Nome Fantasia:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_nomeFantasia" />
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_cpf">CPF:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_cpf" />
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_cnpj">CNPJ:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_cnpj" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_passaporte">Passaporte:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_passaporte" />
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_inscricaoEstadual">Inscri��o Estadual:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_inscricaoEstadual" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_inscricaoMunicipal">Inscri��o Municipal:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_inscricaoMunicipal" />
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalPesquisarCliente_inscricaoRural">Inscri��o Rural:</label>
                                <input type="text" class="form-control form-control-sm"
                                       id="modalPesquisarCliente_inscricaoRural" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
                                <jsp:param name="cepObrigatorio" value="false"/>
                                <jsp:param name="inline" value="true"/>
                                <jsp:param name="idCampoCep" value="cepPessoa"/>
                                <jsp:param name="numeroCep" value=""/>
                            </jsp:include>
                        </div>
                    </div>

                    <hr/>
                    <div class="row">
                        <div class="col-sm-12">
                            <button data-style="expand-left" name="modalPesquisarCliente_pesquisar" type="submit"
                                    class="btn ladda-button btn-primary btn-sm float-right col-sm-12 col-md-6">
                                <i class="fa fa-search fa-fw"></i>
                                <span class="ladda-label">Pesquisar</span>
                            </button>
                        </div>
                    </div>

                </form>

                <div class="row mt-3" id="modalPesquisarCliente_containerTabela">
                    <div class="col-sm-12">
                        <table id="modalPesquisarCliente_tabelaPessoa" class="table table-bordered table-striped table-hover" width="100%">
                            <thead class="thead-ggas-bootstrap">
                            <tr>
                                <th scope="col" class="text-center">Nome</th>
                                <th scope="col" class="text-center">Nome Fantasia</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary float-left" data-dismiss="modal">
                    <i class="fa fa-times fa-fw"></i>Cancelar
                </button>
            </div>
        </div>
    </div>
</div>