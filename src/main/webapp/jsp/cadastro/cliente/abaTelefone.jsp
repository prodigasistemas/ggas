<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script>

$(document).ready(function(){
	
 	$('#botaoAlterarTelefone').hide();
 	
 	 $('#telefone').DataTable( {
    	 "responsive": true,
    	 "paging":   false,
         "ordering": false,
         "info": false,
         "searching": false,
    	 "language": {
             "zeroRecords": "Nenhum registro foi encontrado"
    	 }
    } );
 	
});

	
function incluirTelefone(form) {
	document.getElementById('indexLista').value = -1;
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	submeter('clienteForm', 'adicionarTelefoneDoCliente');
}

function alterarTelefone(form, actionAdicionarTelefone) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	submeter('clienteForm', 'adicionarTelefoneDoCliente');
}


function removerTelefone(indice) {
	var retorno = confirm('Deseja excluir o registro?');
	if(retorno == true){
		document.forms[0].status.value = true;
		document.forms[0].postBack.value = true;
		document.getElementById('indexLista').value = indice;
		submeter('clienteForm', 'removerTelefoneDoCliente');
	}
}

function atualizarTelefonePrincipal(actionAtualizarTelefonePrincipal, indice) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	document.getElementById('indexLista').value = indice;
	submeter('clienteForm', 'atualizarTelefonePrincipalDoCliente');
}

function limparTelefone() {
	document.getElementById('indexLista').value = -1;
	document.forms[0].idTipoTelefone.selectedIndex = 0;
	document.forms[0].dddTelefone.value = "";
	document.forms[0].numeroTelefone.value = "";
	document.forms[0].ramalTelefone.value = "";
	
}

function exibirAlteracaoDoTelefone(indice,idTipoTelefone,dddTelefone,numeroTelefone,ramalTelefone,chavePrimariaTelefone) {
	if (indice != "") {
		document.getElementById('indexLista').value = indice;
		if (idTipoTelefone != "") {
			var tamanho = document.forms[0].idTipoTelefone.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idTipoTelefone.options[i];
				if (idTipoTelefone == opcao.value) {
					opcao.selected = true;
				}
			}
		}
		if (dddTelefone != "") {
			document.forms[0].dddTelefone.value = dddTelefone;
		}
		if (numeroTelefone != "") {
			document.forms[0].numeroTelefone.value = numeroTelefone;
		}
		if (ramalTelefone != "") {
			document.forms[0].ramalTelefone.value = ramalTelefone;
		} else {
			document.forms[0].ramalTelefone.value = "";
		}
		if (chavePrimariaTelefone != "") {
			document.forms[0].chavePrimariaTelefone.value = chavePrimariaTelefone;
		}
	}
}

function onloadTelefone() {
	<c:choose>
		<c:when test="${sucessoManutencaoLista}">
			limparTelefone();
		</c:when>
	</c:choose>
}


<c:if test="${abaId == '2'}">
	addLoadEvent(onloadTelefone);

	$('#identificacao-tab').removeClass('active');
	$('#clienteAbaIdentificacao').removeClass('active show');
	$('#telefone-tab').addClass('active');
	$('#clienteAbaTelefone').addClass('active show');
	
</c:if>

function habilitarBotaoAlterarIndexTelefone(){ 
	$('#botaoAlterarTelefone').show();
	$('#botaoIncluirTelefone').hide();
}

</script>

<div class="row">
	<h5 class="col-md-12 mt-2">Dados do Telefone</h5>
	<div class="col-md-3">
		<input type="hidden" id="chavePrimariaTelefone" name="chavePrimariaTelefone" />
		<div class="form-row">
			<label id="rotTipoTelefone" for="idTipoTelefone">Tipo de Telefone: <span class="text-danger">*</span></label>
            <select id="idTipoTelefone" name="tipoFone" class="form-control form-control-sm"> 
			<option value="" <c:if test="${clienteFone.tipoFone.chavePrimaria eq null}">checked</c:if>>Selecione</option>
            <c:forEach items="${listaTiposTelefone}" var="tipoTelefone">
			<option value="<c:out value="${tipoTelefone.chavePrimaria}"/>" <c:if test="${clienteFone.tipoFone.chavePrimaria == tipoTelefone.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${tipoTelefone.descricao}"/>
			</option>		
			</c:forEach>
           </select>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-row">
			<label id="rotuloCodigoDDD" for="dddTelefone">DDD: <span class="text-danger">*</span></label>
        	<input type="text" class="form-control form-control-sm" id="dddTelefone" name="codigoDDD"
        	onkeypress="return formatarCampoInteiro(event)"  value="${clienteFone.codigoDDD}" maxlength="2">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-row">
			<label id="rotuloNumeroTelefone" for="numeroTelefone">N�mero: <span class="text-danger">*</span></label>
        	<input type="text" class="form-control form-control-sm" id="numeroTelefone" name="numeroFone"
        	onkeypress="return formatarCampoInteiro(event)" value="${clienteFone.numero}" maxlength="9" size="9">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-row">
			<label  id="rotuloRamalTelefone" for="ramalTelefone">Ramal:</label>
        	<input type="text" class="form-control form-control-sm" id="ramalTelefone" name="ramal"
        	onkeypress="return formatarCampoInteiro(event)" value="${clienteFone.ramal}" maxlength="4" size="4">
		</div>
	</div>
	
	</div>
	<div class="form-row">
	
		<div class="col-md-6 mt-1">
			<p><span class="text-danger">* </span>
			Campos Obrigat�rios apenas para Cadastrar Telefones.</p>
		</div>
	
		<div class="col-md-6 align-self-center text-center mt-4">
		<button class="btn btn-primary btn-sm" name="botaoIncluirTelefone" id="botaoIncluirTelefone"
          	type="button" onclick="incluirTelefone(this.form);">
            <i class="fas fa-plus"></i> Adicionar
        </button>
        <button class="btn btn-success btn-sm" name="botaoAlterarTelefone" id="botaoAlterarTelefone"
        	type="button" onclick="alterarTelefone(this.form,'<c:out value="${param['actionAdicionarTelefone']}"/>');">
            <i class="fas fa-save"></i> Alterar
        </button>
		<button class="btn btn-secondary btn-sm" name="botaoLimparTelefone" id="botaoLimparTelefone"
			type="button" onclick="limparTelefone();">
            <i class="far fa-trash-alt"></i> Limpar
        </button>
	</div>
	
	
</div>

<c:if test="${listaClienteFone ne null}">
	<div class="row mt-2">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover" id="telefone" style="width:100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th class="text-center">Descri��o</th>
				 			<th class="text-center">DDD</th>
				 			<th class="text-center">N�mero</th>
				 			<th class="text-center">Ramal</th>
				 			<th class="text-center">Principal</th>
				 			<th class="text-center">A��es</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="i" value="0" />
						<c:forEach items="${listaClienteFone}" var="telefone">
							<tr>
								<td class="text-center">
									${telefone.tipoFone.descricao}
								</td>
								<td class="text-center">
									${telefone.codigoDDD}
								</td>
								<td class="text-center">
									${telefone.numero}
								</td>
								<td class="text-center">
									${telefone.ramal}
								</td>
								<td class="text-center">
									<c:choose>
										<c:when test="${telefone.indicadorPrincipal}">Sim</c:when>
      									<c:otherwise>N�o</c:otherwise>
									</c:choose>
								</td>
								<td class="text-center">
				 				<c:choose>
				 					<c:when test="${telefone.indicadorPrincipal == false}">
				 					<button type="button" class="btn btn-primary btn-sm" title="Tornar Principal" onClick="atualizarTelefonePrincipal('<c:out value="${param['actionAtualizarTelefonePrincipal']}"/>',<c:out value="${i}"/>);">
				 						<i class="fas fa-phone"></i>
				 					</button>
				 					</c:when>
				 				</c:choose>
				 				<button type="button" class="btn btn-info btn-sm" title="Alterar Telefone" id="idExibirIndexTelefone"
        						onClick="exibirAlteracaoDoTelefone('${i}','${telefone.tipoFone.chavePrimaria}','${telefone.codigoDDD}','${telefone.numero}','${telefone.ramal}','${telefone.chavePrimaria}');habilitarBotaoAlterarIndexTelefone()">
        						<i class="fas fa-pen-square"></i></button>
        						<button class="btn btn-secondary btn-sm" type="button" title="Remover Telefone"
        						 onClick="removerTelefone(<c:out value="${i}"/>);">
              					<i class="far fa-trash-alt"></i> 
          						</button>
				 			</td>
							</tr>
						<c:set var="i" value="${i+1}" />
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</c:if>
