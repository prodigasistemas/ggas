<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<script>
$(document).ready(function(){

   $('#botaoAlterarAnexoAbaIdentificacao').hide();
   
   $(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
   $("#cpf").inputmask("999.999.999-99",{placeholder:"_"});
   $("#cnpj,#cnpjClienteSuperior").inputmask("99.999.999/9999-99",{placeholder:"_"});
   $("#inscricaoEstadual").inputmask("${mascaraInscricaoEstadual}",{placeholder:"_"});  
  
	
    $('#idAnexoAbaIdentificacao').DataTable( {
    	 "responsive": true,
    	 "paging":   false,
         "ordering": false,
         "info": false,
         "searching": false,
    	 "language": {
             "zeroRecords": "Nenhum registro foi encontrado"
    	 }
    } );
	
   checarInscricaoEstadualAbilitada();
   $("input#inscricaoEstadual").keyup(function() {
	   onchangeInscricaoEstadual();
	});
});

var popup;

function onchangeInscricaoEstadual(){
	var inscricaoEstadual = document.getElementById("inscricaoEstadual");
	var checkBoxInscricaoEstadual = document.getElementById("regimeRecolhimento");
	var naoPossuiIndicador = document.getElementById("naoPossuiIndicador");
	
	var texto = inscricaoEstadual.value;
	
	while(texto.indexOf("_") > -1 || texto.indexOf("-") > -1){
		texto = texto.replace("_","");
		texto = texto.replace("-","");
	}
	
	if (texto.length == 0){		
		checkBoxInscricaoEstadual.disabled = false;
		naoPossuiIndicador.disabled = false;
	}else{
		checkBoxInscricaoEstadual.disabled = true;
		naoPossuiIndicador.disabled = true;
	}
}

function abilitarInscricaoEstadual(){
	$("#inscricaoEstadual").removeAttr("disabled");
	$("#naoPossuiIndicador").removeAttr("disabled");
	$("#regimeRecolhimento").removeAttr("disabled");
	
	$("#obrigatorioInscricao").html("* ");
	
	return true;
}

function exibirPopupPesquisaClienteSuperior() {
	popup = window.open('exibirPesquisaClientePopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function checarInscricaoEstadualAbilitada(){
	var isRegimeRecolhimento = $("#regimeRecolhimento").attr("checked");
	var isNaoPossuiIndicador = $("#naoPossuiIndicador").attr("checked");
	
	if (isRegimeRecolhimento == true || isRegimeRecolhimento == "checked") {
		$("#inscricaoEstadual").val("");
		$("#inscricaoEstadual").attr("disabled", "disabled");
		
		$("#obrigatorioInscricao").html("");
		
		
		$("#naoPossuiIndicador").attr("disabled", "disabled");
	} else if (isNaoPossuiIndicador == true || isNaoPossuiIndicador == "checked" ){
		$("#inscricaoEstadual").val("");
		$("#inscricaoEstadual").attr("disabled", "disabled");
		
		$("#obrigatorioInscricao").html("");
		
		
		$("#regimeRecolhimento").attr("disabled", "disabled");
	} else{
		abilitarInscricaoEstadual();
	}
}

function exibirPopupPesquisaAtividadeEconomica() {
	popup = window.open('exibirPesquisaAtividadeEconomicaPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function selecionarAtividadeEconomica(idSelecionado){
	var idAtividadeEconomica = document.getElementById("idAtividadeEconomica");
	var descricaoAtividadeEconomica = document.getElementById("descricaoAtividadeEconomica");
	
	if(popup != null){
		popup.close;
	}
	
	if(idSelecionado != '') {
		AjaxService.obterAtividadeEconomicaPorChave( idSelecionado, {
           	callback: function(atividade) {	           		
           		if(atividade != null){  	           			        		      		         		
	               	idAtividadeEconomica.value = atividade["chavePrimaria"];
	               	descricaoAtividadeEconomica.value = atividade["descricao"];		               	
               	}
        	}, async:false}
        	
        );	        
    } else {       		
       	idAtividadeEconomica.value = "";
       	descricaoAtividadeEconomica.value = "";		
	}
	document.getElementById("idDescricaoCompletaAtividadeEconomica").value = idAtividadeEconomica.value + ' - ' + descricaoAtividadeEconomica.value;
}

function selecionarCliente(idSelecionado) {
	var chavePrimariaClienteSuperior;

	var idResponsavelSuperior = document.getElementById("idResponsavelSuperior");
	var nomeResponsavelSuperior = document.getElementById("nomeResponsavelSuperior");
	var nomeResponsavelSuperiorDes = document.getElementById("nomeResponsavelSuperiorDes");

	if (popup != undefined) {
		chavePrimariaClienteSuperior = idSelecionado;
		if (chavePrimariaClienteSuperior != '' && idResponsavelSuperior != undefined && 
		    nomeResponsavelSuperior != undefined && nomeResponsavelSuperiorDes != undefined) {
		    
			AjaxService.obterClientePorChave( chavePrimariaClienteSuperior, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){
	           			idResponsavelSuperior.value = cliente["chavePrimaria"];  	           			        		      		         		
	           			nomeResponsavelSuperior.value =  cliente["nome"];
	        			nomeResponsavelSuperiorDes.value =  nomeResponsavelSuperior.value;
	               	}
	        	}, async:false}
	        	
	        );
		}	

	} 
}

function alterarAnexoAbaIdentificacao(form) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	submeter('clienteForm', 'adicionarAnexoAbaIdentificacao');
}

function incluirAnexoAbaIdentificacao(form) {
	document.forms[0].indexListaAnexoAbaIdentificacao.value = -1;
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	submeter('clienteForm', 'adicionarAnexoAbaIdentificacao');

}

function removerAnexoAbaIdentificacao(indice) {
	var retorno = confirm('Deseja excluir o registro?');
	if(retorno == true){
		document.forms[0].status.value = true;
		document.forms[0].postBack.value = true;
		document.forms[0].indexListaAnexoAbaIdentificacao.value = indice;
		submeter('clienteForm', 'removerAnexoAbaIdentificacao');
	}
}

function limparAnexoAbaIdentificacao() {
	document.forms[0].idTipoAnexoAbaIdentificacao.selectedIndex = 0;
	document.forms[0].descricaoAnexoAbaIdentificacao.value = "";
	
	document.forms[0].indexListaAnexoAbaIdentificacao.value = -1;

}

function exibirIndexIdentificacao(indexListaAnexoAbaIdentificacao,chavePrimariaAnexoAbaIdentificacao,descricaoAnexoAbaIdentificacao,idTipoAnexoAbaIdentificacao) {
	if (indexListaAnexoAbaIdentificacao != "") {
		document.forms[0].indexListaAnexoAbaIdentificacao.value = indexListaAnexoAbaIdentificacao;
		if (idTipoAnexoAbaIdentificacao != "") {
			var tamanho = document.forms[0].idTipoAnexoAbaIdentificacao.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idTipoAnexoAbaIdentificacao.options[i];
				if (idTipoAnexoAbaIdentificacao == opcao.value) {
					opcao.selected = true;
					break;
				}
			}
		}
		
		if (descricaoAnexoAbaIdentificacao != "") {
			document.forms[0].descricaoAnexoAbaIdentificacao.value = descricaoAnexoAbaIdentificacao;
		}else{
			document.forms[0].descricaoAnexoAbaIdentificacao.value = "";
		}
		
		if (chavePrimariaAnexoAbaIdentificacao != ""){
			document.forms[0].chavePrimariaAnexoAbaIdentificacao.value = chavePrimariaAnexoAbaIdentificacao;
		}
	
	}
}

	function visualizarAnexoAbaIdentificacao(){
		submeter('clienteForm', 'visualizarAnexoAbaIdentificacao');
	}
	
	function somenteNumeros(num) {
        var er = /[^0-9.]/;
        er.lastIndex = 0;
        var campo = num;
        if (er.test(campo.value)) {
          campo.value = "";
          alert("O campo RG s� permite preenchimento com n�meros");
        }
    }
	
	function habilitarBotaoAlterarAnexoAbaIdentificacao(){
		$('#botaoAlterarAnexoAbaIdentificacao').show();
		$('#botaoIncluirAnexoAbaIdentificacao').hide();
	}

</script>

<div class="row" id="pessoaFisica" style="display: none">
	<h5 class="col-md-12 mt-2">Dados de Pessoa F�sica</h5>
	<div class="col-md-6">
		<div class="form-row">
             <div class="col-md-12">
    		 	<label id="labelNascionalidade" for="idNacionalidade">Nacionalidade: <span class="text-danger">*</span></label>
             		<select name="nacionalidade" id="idNacionalidade" class="form-control form-control-sm" onchange="habilitarDocumentacaoNecessariaNacionalidade(this);"> 
             			<option value="-1">Selecione</option>
							<c:forEach items="${nacionalidades}" var="nacionalidade">
							<option value="<c:out value="${nacionalidade.chavePrimaria}"/>" <c:if test="${clienteForm.nacionalidade.chavePrimaria == nacionalidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${nacionalidade.descricao}"/>
							</option>		
							</c:forEach>
           			</select>
           	</div>
       	</div>
       	
       	<div class="form-row">
        	<div class="col-md-12">
        		<label id="rotuloCPF" for="cpf">CPF: <span id="obrigatorioCPF" class="text-danger">*</span></label>
        			<input type="text" id="cpf" name="cpf"  class="form-control form-control-sm" 
                     maxlength="14" size="14" value="${clienteForm.cpf}">
        	</div>
    	</div>
    	
    	<div class="form-row">
        	<div class="col-md-6">
        		<label id="rotuloRG" for="rg">RG: </label>
        			<input type="text" id="rg" name="rg" class="form-control form-control-sm"  
        			maxlength="20" size="10" value="${clienteForm.rg}" onkeyup="return letraMaiuscula(this);">
        	</div>
    	
        	<div class="col-md-6">
        		<label id="rotuloDataEmissaoRG" for="dataEmissaoRG" >Data de Emiss�o:</label>
        		<div class="input-group input-group-sm" id="dataEmissao">
                   <input type="text" aria-label="Data de Emiss�o"
                     class="form-control form-control-sm campoData"
                     id="dataEmissaoRG" name="dataEmissaoRG" 
                      value="<fmt:formatDate pattern="dd/MM/yyyy" value="${clienteForm.dataEmissaoRG}" />" maxlength="10">
               </div>
        	</div>
        </div>
    	
    	<div class="form-row">
             <div class="col-md-5">
    		 	<label id="rotuloOrgaoEmissorRG" for="idOrgaoEmissor">�rg�o emissor:</label>
             		<select name="orgaoExpedidor" id="idOrgaoEmissor" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
						<c:forEach items="${orgaosEmissores}" var="orgaoEmissor">
							<option value="<c:out value="${orgaoEmissor.chavePrimaria}"/>" <c:if test="${clienteForm.orgaoExpedidor.chavePrimaria == orgaoEmissor.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${orgaoEmissor.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
           			</select>
           	</div>
       
             <div class="col-md-7">
    		 	<label id="rotuloOrgaoEmissorUF" for="idOrgaoEmissorUF">UF:</label>
             		<select name="unidadeFederacao" id="idOrgaoEmissorUF" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
						<c:forEach items="${unidadesFederacao}" var="unidadeFederacao">
							<option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${clienteForm.unidadeFederacao.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidadeFederacao.descricao}"/>
							</option>		
						</c:forEach>
           			</select>
           	</div>
           	
       	</div>
       	
       	<div class="form-row">
        	<div class="col-md-12">
        		<label id="rotuloPassaporte" for="passaporte">Passaporte: <span  id="obrigatorioPassaporte" class="text-danger">*</span></label>
        			<input type="text" id="passaporte" name="numeroPassaporte" class="form-control form-control-sm" 
                     maxlength="13" size="13" value="${clienteForm.numeroPassaporte}">
        	</div>
    	</div>
       	
    	<div class="form-row mb-1">
			<label id="rotuloSexo" class="col-md-12">Sexo: <span class="text-danger">*</span></label>
			<div class="col-md-12">
				<c:forEach items="${sexos}" var="sexo">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" class="custom-control-input" name="sexo" id="idSexo${sexo.chavePrimaria}" value="${sexo.chavePrimaria}"
						 <c:if test="${clienteForm.sexo.chavePrimaria eq sexo.chavePrimaria}">checked="checked"</c:if>>
						 <label class="custom-control-label" for="idSexo${sexo.chavePrimaria}">${sexo.descricao}</label>
					</div>
				</c:forEach>
			</div>
		</div>
	
	</div>
	<div class="col-md-6">
		
		
		<div class="form-row">
        	<div class="col-md-10">
        		<label id="rotuloDataNascimento" for="dataNascimento">Data de Nascimento:</label>
        		<div class="input-group input-group-sm" id="dataNasc">
                   <input type="text" aria-label="Data de Nascimento"
                     class="form-control form-control-sm campoData"
                     id="dataNascimento" name="dataNascimento" 
                      value="<fmt:formatDate pattern="dd/MM/yyyy" value="${clienteForm.dataNascimento}" />" maxlength="10">
               </div>
        	</div>
        </div>
    	
    	<div class="form-row">
             <div class="col-md-12">
    		 	<label id="rotuloProfissao" for="idProfissao">Profiss�o: </label>
             		<select name="profissao" id="idProfissao" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
							<c:forEach items="${profissoes}" var="profissao">
							<option value="<c:out value="${profissao.chavePrimaria}"/>" <c:if test="${clienteForm.profissao.chavePrimaria == profissao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${profissao.descricao}"/>
							</option>		
							</c:forEach>
           			</select>
           	</div>
       	</div>
	
		<div class="form-row">
        	<div class="col-md-12">
        		<label id="rotuloNomeMae" for="nomeMae">Nome da M�e: </label>
        			<input type="text" id="nomeMae" name="nomeMae" class="form-control form-control-sm" 
                      maxlength="50" size="50"  value="${clienteForm.nomeMae}"
                      onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
        	</div>
        </div>
        
        <div class="form-row">
        	<div class="col-md-12">
        		<label id="rotuloNomePai" for="nomePai">Nome do Pai: </label>
        			<input type="text" id="nomePai" name="nomePai" class="form-control form-control-sm" 
                      maxlength="50" size="50" value="${clienteForm.nomePai}"
                      onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" >
        	</div>
        </div>
        
        <div class="form-row">
             <div class="col-md-12 mb-2">
    		 	<label id="rotuloRendaFamiliar" for="idFaixaRendaFamiliar">Renda Familiar: </label>
             		<select name="rendaFamiliar" id="idFaixaRendaFamiliar" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
							<c:forEach items="${faixasRenda}" var="renda">
							<option value="<c:out value="${renda.chavePrimaria}"/>" <c:if test="${clienteForm.rendaFamiliar.chavePrimaria == renda.chavePrimaria}">selected="selected"</c:if>>
							<c:if test="${renda.valorFaixaInicio ne renda.valorFaixaFim}">
								<fmt:formatNumber value="${renda.valorFaixaInicio}" minFractionDigits="2" type="currency"/> - <fmt:formatNumber value="${renda.valorFaixaFim}" minFractionDigits="2" type="currency"/>
							</c:if>
							<c:if test="${renda.valorFaixaInicio eq renda.valorFaixaFim}">
								Acima de <fmt:formatNumber value="${renda.valorFaixaInicio}" minFractionDigits="2" type="currency"/>
							</c:if>
						</option>		
						</c:forEach>
           			</select>
           	</div>
           	<p><span class="text-danger">* </span>Campos Obrigat�rios para Inclus�o de Clientes.</p>
       	</div>
		
	</div>
	
</div>

<div class="row" id="pessoaJuridica" style="display: none">
	<h5 class="col-md-12 mt-2">Dados de Pessoa Jur�dica</h5>
	<div class="col-md-6">
		<div class="form-row">
        	<div class="col-md-12">
        		<label id="rotuloNomeFantasia" for="nomeFantasia">Nome Fantasia: <span class="text-danger"> *</span></label>
        			<input type="text" id="nomeFantasia" name="nomeFantasia" class="form-control form-control-sm" 
                      maxlength="50" size="50" value="${clienteForm.nomeFantasia}"
                      onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
        	</div>
        </div>
        
        <div class="form-row">
        	<div class="col-md-8">
        		<label  id="rotuloCNPJ" for="cnpj">CNPJ: <span class="text-danger"> *</span></label>
        			<input type="text" id="cnpj" name="cnpj" class="form-control form-control-sm" 
                      maxlength="18" size="18" value="${clienteForm.cnpj}"/>
        	</div>
        </div>
        
         <div class="form-row">
         	<label class="col-md-12" id="rotuloInscricaoEstadual" 
         		for="inscricaoEstadual">Inscri��o Estadual: <span class="text-danger" id="obrigatorioInscricao"> *</span></label>
        	<div class="col-md-5">
        		<input type="text" id="inscricaoEstadual" name="inscricaoEstadual" class="form-control form-control-sm" 
                     maxlength="14" size="25" value="${clienteForm.inscricaoEstadual}"/>
        	</div>
        	<div class="col-md-3 mt-2">
        		<div class="custom-control custom-checkbox custom-checkbox-inline">
  					<input class="custom-control-input" type="checkbox" id="regimeRecolhimento"
  					 name="regimeRecolhimento" value="1" <c:if test="${clienteForm.regimeRecolhimento eq '1'}">checked="checked"</c:if> onchange="checarInscricaoEstadualAbilitada()"/>
  					<label class="custom-control-label" id="rotuloRegimeRecolhimento" for="regimeRecolhimento">Isento</label>
				</div>
        	</div>
        	<div class="col-md-4 mt-2">
        		<div class="custom-control custom-checkbox custom-checkbox-inline">
  					<input class="custom-control-input" type="checkbox"  id="naoPossuiIndicador" 
  					 name="regimeRecolhimento" value="2" <c:if test="${clienteForm.regimeRecolhimento eq '2'}">checked="checked"</c:if> onchange="checarInscricaoEstadualAbilitada()"/>
  					<label class="custom-control-label" id="naoPossui" for="naoPossuiIndicador">N�o possui</label>
				</div>
        	</div>
        </div>
         <div class="form-row">
        	<div class="col-md-6">
        		<label id="rotuloInscricaoMunicipal" for="inscricaoMunicipal">Inscri��o Municipal:</label>
        			<input id="inscricaoMunicipal" name="inscricaoMunicipal" class="form-control form-control-sm"  maxlength="14" size="25" 
        			value="${clienteForm.inscricaoMunicipal}" onkeypress="return formatarCampoInteiro(event)"/>
        	</div>
        	
        	<div class="col-md-6">
        		<label id="rotuloInscricaoRural" for="inscricaoRural">Inscri��o Rural:</label>
        			<input id="inscricaoRural" name="inscricaoRural" class="form-control form-control-sm"  maxlength="14" size="25"
        			 value="${clienteForm.inscricaoRural}" onkeypress="return formatarCampoInteiro(event)"/>
        	</div>
        </div>   
        
	</div>
	
	
	<div class="col-md-6">
	
	
	<div class="form-row">
        <div class="col-md-12">
            <label id="rotuloAtividadeEconomica" for="descricaoCompletaAtividadeEconomica">Atividade Econ�mica:</label>
             	<div class="input-group">
  					<input type="hidden" name="atividadeEconomica" id="idAtividadeEconomica" value="${clienteForm.atividadeEconomica.chavePrimaria}"/>
  					<input type="hidden" id="descricaoAtividadeEconomica" name="descricaoAtividadeEconomica" value="${clienteForm.atividadeEconomica.descricao}"/>
  					<textarea class="form-control" id="idDescricaoCompletaAtividadeEconomica" name="descricaoCompletaAtividadeEconomica" disabled>
  					${clienteForm.atividadeEconomica.descricao}
  					</textarea>
 					<div class="input-group-append">
   						<button class="btn btn-primary mb-0" type="button" id="botaoPesquisarAtividadeEconomica"
   								onclick="exibirPopupPesquisaAtividadeEconomica();"><i class="fas fa-search"></i></button>
   				</div>
 			 </div>
 			</div>
     </div>	
     
     <div class="form-row mb-2">
        	<div class="col-md-12">
            <label id="rotuloResponsavelSuperior" for="nomeResponsavelSuperiorDes">Cliente Superior:</label>
             	<div class="input-group">
  					<input name="clienteResponsavel" type="hidden" id="idResponsavelSuperior" value="${clienteForm.clienteResponsavel.chavePrimaria}">
  					<input name="nomeResponsavelSuperior" type="hidden" id="nomeResponsavelSuperior" value="${clienteForm.clienteResponsavel.nome}">
  					<input class="form-control" id="nomeResponsavelSuperiorDes" name="nomeResponsavelSuperiorDes" readonly value="${clienteForm.clienteResponsavel.nome}">
 					<div class="input-group-append">
   						<button class="btn btn-primary" type="button" id="botaoPesquisaClienteSuperior"
   								onclick="exibirPopupPesquisaClienteSuperior();"><i class="fas fa-search"></i></button>
   				</div>
 			 </div>
 			</div>
        </div>
     	<p><span class="text-danger">* </span>Campos Obrigat�rios para Inclus�o de Clientes.</p>
	</div>
	
</div>
<hr />

<div class="row">
	<input type="hidden" id="chavePrimariaAnexoAbaIdentificacao" name="chavePrimariaAnexoAbaIdentificacao" />
	<h5 class="col-md-12">Documentos</h5>
	<div class="col-md-10">
		 <div class="form-row">
		 	<div class="col-md-8">
        		<label id="rotuloDescricaoDocumentoAbaIdentificacao" for="descricaoDocumentoAbaIdentificacao">Descri��o: <span> *</span></label>
        			<input type="text" class="form-control form-control-sm" maxlength="30" id="descricaoAnexoAbaIdentificacao" name="descricaoAnexoIdentificacao" value="${clienteAnexoID.descricaoAnexo}"/>
        	</div>
        	
        	<div class="col-md-4">
    		 	<label  id="rotuloTipoDocumentoAbaIdentificacao" for="idTipoDocumentoAbaIdentificacao">Tipo de Anexo: <span>*</span></label>
             		<select name="tipoAnexo" id="idTipoAnexoAbaIdentificacao" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoAnexo}" var="tipoAnexo">
							<option value="<c:out value="${tipoAnexo.chavePrimaria}"/>" <c:if test="${clienteAnexoID.tipoAnexo.chavePrimaria == tipoAnexo.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoAnexo.descricao}"/>
							</option>		
							</c:forEach>
           			</select>
           	</div>
		 </div>
		 

		<div class="form-row">
			<div class="col-md-8">
				 <label for="anexoAbaIdentificacao">Anexo em formato PDF: <span>*</span></label>
    			 <input type="file" class="form-control-file" id="anexoAbaIdentificacao" name="anexoAbaIdentificacao" value="${abaAnexoAbaIdentificacao}">
			</div>
			<div class="col-md-4 align-self-center text-center mt-4">
				<button class="btn btn-primary btn-sm" name="botaoIncluirAnexoAbaIdentificacao" id="botaoIncluirAnexoAbaIdentificacao" 
          			type="button"  onclick="incluirAnexoAbaIdentificacao(this.form);">
                	<i class="fas fa-plus"></i> Adicionar
          		</button>
          		<button class="btn btn-success btn-sm" name="botaoAlterarAnexoAbaIdentificacao" id="botaoAlterarAnexoAbaIdentificacao"
          			type="button" onclick="alterarAnexoAbaIdentificacao(this.form);">
                	<i class="fas fa-save"></i> Alterar
          		</button>
				<button class="btn btn-secondary btn-sm" name="botaoLimparAnexoAbaIdentificacao" id="botaoLimparAnexoAbaIdentificacao" 
					type="button" onclick="limparAnexoAbaIdentificacao();">
              		<i class="far fa-trash-alt"></i> Limpar
          		</button>
          
			</div>
		</div>

	</div>

</div>

<c:if test="${listaClienteAnexoAbaIdentificacao ne null}">
<div class="row mt-2">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover" id="idAnexoAbaIdentificacao" style="width:100%">
				 <thead class="thead-ggas-bootstrap">
				 	<tr>
				 		<th class="text-center">Descri��o</th>
				 		<th class="text-center">Tipo</th>
				 		<th class="text-center">Data de Inclus�o</th>
				 		<th class="text-center">A��es</th>
				 	</tr>
				 </thead>
				 <tbody>
				 	<c:set var="x" value="0" />
				 	<c:forEach items="${listaClienteAnexoAbaIdentificacao}" var="anexoAbaIdentificacao">
				 		<tr>
				 			<td class="text-center">
				 				${anexoAbaIdentificacao.descricaoAnexo}
				 			</td>
				 			<td class="text-center">
				 				${anexoAbaIdentificacao.tipoAnexo.descricao}
				 			</td>
				 			<td class="text-center">
				 				<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusDate" />
								<c:out value="${formattedStatusDate}"/>
				
								<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusHour" />
								<c:out value="${formattedStatusHour}"/>
				 			</td>
				 			<td class="text-center">
				 				<button type="button" class="btn btn-primary btn-sm" title="Download de Anexo"
        							onClick="exibirIndexIdentificacao('${x}','${anexoAbaIdentificacao}','${anexoAbaIdentificacao.descricaoAnexo}','${anexoAbaIdentificacao.tipoAnexo.chavePrimaria}');visualizarAnexoAbaIdentificacao();" id="botaoVisualizarAnexoAbaIdentificacao">
        							<i class="fas fa-download"></i></button>
        							
				 				<button type="button" class="btn btn-info btn-sm" title="Alterar Anexo" id="idExibirIndexIdentificacao"
        							onClick="exibirIndexIdentificacao('${x}','${anexoAbaIdentificacao}','${anexoAbaIdentificacao.descricaoAnexo}','${anexoAbaIdentificacao.tipoAnexo.chavePrimaria}');habilitarBotaoAlterarAnexoAbaIdentificacao()">
        							<i class="fas fa-pen-square"></i></button>
        						
        						<button type="button" class="btn btn-secondary btn-sm" title="Remover Anexo"
        							onClick="removerAnexoAbaIdentificacao(<c:out value="${x}"/>);">
        							<i class="far fa-trash-alt"></i></button>
				 			</td>
				 		</tr>
				 		<c:set var="x" value="${x+1}" />
				 	</c:forEach>
				 </tbody>
			</table>
		</div>
	</div>
</div>
</c:if>


	

