<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script language="javascript">	
	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("<c:out value='${param.idCampoIdCliente}' default=""/>");
		var nomeCompletoCliente = document.getElementById("<c:out value='${param.idCampoNomeCliente}' default=""/>");
		var documentoFormatado = document.getElementById("<c:out value='${param.idCampoDocumentoFormatado}' default=""/>");
		var emailCliente = document.getElementById("<c:out value='${param.idCampoEmail}' default=""/>");
		var enderecoFormatado = document.getElementById("<c:out value='${param.idCampoEnderecoFormatado}' default=""/>");		
		  if( document.getElementById("exibirGridPontoConsumo")){
			document.getElementById("exibirGridPontoConsumo").value = "";
		  }
		if(idSelecionado != '') {		
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];
		               	if(cliente["cnpj"] != undefined ){
		               		documentoFormatado.value = cliente["cnpj"];
		               	} else {
			               	if(cliente["cpf"] != undefined ){
			               		documentoFormatado.value = cliente["cpf"];
			               	} else {
			               		documentoFormatado.value = "";
			               	}
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
		               	$('form[name="notaDebitoCreditoForm"]').change();
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	documentoFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
		document.getElementById("idCliente").value = idCliente.value;
        document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
        document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
        document.getElementById("emailClienteTexto").value = emailCliente.value;
        document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
        if( document.getElementById("pontoConsumo")){
        	teste = document.getElementById("teste")

        	if(teste!=null){
        		teste.hidden = true;
            }
        	
        	linha = document.getElementById("linha");

        	if(linha!=null){
        		linha.hidden = true;
            }

        	exibirGridPontoConsumo = document.getElementById("exibirGridPontoConsumo");

        	if(exibirGridPontoConsumo!=null){
        		exibirGridPontoConsumo.value = "nao";
            }
        }
		//Verifica se a fun��o foi passada por par�metro e se ela � realmente v�lida antes de execut�-la.
        if (document.getElementById("funcaoParametro")!= null && document.getElementById("funcaoParametro").value != "") {
			var funcao = document.getElementById("funcaoParametro").value;
			if (funcaoExiste(funcao)) {
				funcao = funcao + '(\'' + idCliente.value + '\')';
				eval(funcao);
			}
        }
	}
	
	function exibirPopupPesquisaCliente() {	

		var restricaoTipoPessoa = '';

		<c:choose>
			<c:when test="${param.consultarPessoaFisica && not param.consultarPessoaJuridica}">
				restricaoTipoPessoa = '&pessoaFisica=true';
			</c:when>
			<c:when test="${not param.consultarPessoaFisica && param.consultarPessoaJuridica}">
				restricaoTipoPessoa = '&pessoaJuridica=true';
			</c:when>
		</c:choose>

		funcionalidade = "<c:out value='${param.funcionalidade}'/>";
		if(funcionalidade!=""){
			funcionalidade = "&funcionalidade=" + funcionalidade;
		}
		popup = window.open('exibirPesquisaClientePopup?' + funcionalidade + restricaoTipoPessoa,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

	}

	function limparFormularioDadosCliente(){
		document.getElementById('<c:out value='${param.idCampoIdCliente}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoNomeCliente}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoDocumentoFormatado}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoEnderecoFormatado}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoEmail}' default=""/>').value = "";
        document.getElementById('nomeClienteTexto').value = "";
        document.getElementById('documentoFormatadoTexto').value = "";
        document.getElementById('emailClienteTexto').value = "";
        document.getElementById('enderecoFormatadoTexto').value = "";
	}

	function desabilitarPesquisaCliente(){
		document.getElementById('botaoPesquisarCliente').disabled = true;
	}
	
	function habilitarPesquisaCliente(){
		document.getElementById('botaoPesquisarCliente').disabled = false;
	}

</script>

<c:choose>
	<c:when test="${not empty param.nomeComponente}">
		<legend <c:if test='${param.possuiRadio ne null && param.possuiRadio eq true}'>class="legendIndicadorPesquisa"</c:if>> <c:out value='${param.nomeComponente}'/>  </legend>
	</c:when>
	<c:otherwise>
		<legend <c:if test='${param.possuiRadio ne null && param.possuiRadio eq true}'>class="legendIndicadorPesquisa"</c:if>><c:if test='${param.isPesquisaObrigatoria}'><span id="spanPesquisarPessoa" class="campoObrigatorioSimbolo">* </span></c:if>Pesquisar Pessoa</legend>
	</c:otherwise>

</c:choose>
<div class="pesquisarClienteFundo" style="height:250px;">
	<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Pessoa</span> para selecionar a Pessoa.</p>
	<input name="<c:out value='${param.idCampoIdCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoIdCliente}' default=""/>" value="${param.idCliente}">
	<input name="<c:out value='${param.idCampoNomeCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoNomeCliente}' default=""/>" value="${param.nomeCliente}">
	<input name="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" value="${param.documentoFormatadoCliente}">
	<input name="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" value="${param.enderecoFormatadoCliente}">
	<input name="<c:out value='${param.idCampoEmail}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEmail}' default=""/>" value="${param.emailCliente}">
	<input name="funcaoParametro" type="hidden" id="funcaoParametro" value="${param.funcaoParametro}">
	
	<input name="Button" id="botaoPesquisarCliente" class="bottonRightCol2" title="Pesquisar Pessoa"  value="Pesquisar Pessoa" onclick="exibirPopupPesquisaCliente(); exibirIndicador();" type="button"><br />
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloCliente" for="nomeClienteTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Pessoa:</label>
	<input class="campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" disabled="disabled" value="${param.nomeCliente}"><br />
	<c:choose>
		<c:when test="${param.consultarPessoaFisica && not param.consultarPessoaJuridica}">
			<label class="rotulo<c:if test="${param.dadosClienteObrigatorios}"> campoObrigatorio</c:if>" id="rotuloCnpjTexto" for="documentoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>CPF:</label>
		</c:when>
		<c:when test="${not param.consultarPessoaFisica && param.consultarPessoaJuridica}">
			<label class="rotulo<c:if test="${param.dadosClienteObrigatorios}"> campoObrigatorio</c:if>" id="rotuloCnpjTexto" for="documentoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>CNPJ:</label>
		</c:when>
		<c:otherwise>
			<label class="rotulo<c:if test="${param.dadosClienteObrigatorios}"> campoObrigatorio</c:if>" id="rotuloCnpjTexto" for="documentoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>CPF/CNPJ:</label>
		</c:otherwise>
	</c:choose>
	<input class="campoDesabilitado" type="text" id="documentoFormatadoTexto" name="documentoFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${param.documentoFormatadoCliente}"><br />	
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Endere�o:</label>
	<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoClienteTexto" disabled="disabled">${param.enderecoFormatadoCliente}</textarea><br />
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
	<input class="campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${param.emailCliente}"><br />
</div>
