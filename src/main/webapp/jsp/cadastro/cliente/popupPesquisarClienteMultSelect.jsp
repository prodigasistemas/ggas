<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script language="javascript">
	
	$(document).ready(function(){
		$('input[name=multSelect]').val("true");
		$("#cpf").inputmask("999.999.999-99",{placeholder:"_"});
		$("#cnpj").inputmask("99.999.999/9999-99",{placeholder:"_"});
		
		//Coloca o cursor no primeiro campo de pesquisa
		$("#nome").focus();
	
		/*Executa a pesquisa quando a tecla ENTER � pressionada, 
		caso o cursor esteja em um dos campos do formul�rio*/
		$('#pesquisarClientePessoaJuridicaPopup > :text').keyup(function(event) {
			if (event.which == '13') {
				exibirIndicador();
				pesquisarCliente();
			}
	   	});
		
	});
	
	
// 	function selecionar(elem){
// 		document.getElementById('idSelecionado').value = elem.value;	
// 	}
	
// 	function selecionarCliente() {
// 		window.opener.selecionarCliente(document.getElementById('idSelecionado').value);
// 		window.close();
// 	}
	function selecionarMultiplosClientes(){
		var selecao = verificarSelecao();
		if (selecao == true) {
			var cont = 0;
			var chavesPrimarias = [];
            $('input[name="chavesPrimarias"]:checked').each(function() {
                     chavesPrimarias[cont] = this.value;
                     cont++;                     
             });
			window.opener.incluirListaCliente(chavesPrimarias);
			window.close();
	    }
	} 	
	
// 	function selecionarNoLink(id) {
// 		var funcionalidade = document.getElementById('funcionalidade').value;		
// 		window.opener.selecionarCliente(id);
// 		if (funcionalidade != null && funcionalidade == 'paradaProgramada') {
// 			window.opener.selecionarClienteParadaProgramada(id);
// 		}
// 		window.close();
// 	}
	
	function limparDados(){
		document.getElementById('nome').value = '';
		<c:if test="${pessoaJuridica eq true}">
		document.getElementById('nomeFantasia').value = '';
		document.getElementById('cnpj').value = '';
		document.getElementById('inscricaoEstadual').value = '';
		document.getElementById('inscricaoMunicipal').value = '';
		document.getElementById('inscricaoRural').value = '';
		document.getElementById('cep').value = '';
		</c:if>
		<c:if test="${pessoaFisica eq true}">
		document.getElementById('cpf').value = '';
		document.getElementById('passaporte').value = '';
		</c:if>
	}
	

	function init() {
		<c:if test="${clientesEncontrados ne null}">
			$.scrollTo($('#cliente'),800);
		</c:if>
	}
	addLoadEvent(init);

</script>

<c:choose>
	<c:when test="${pessoaJuridica eq true && pessoaFisica ne true}">
		<h1 class="tituloInternoPopup">Pesquisar Pessoa Jur�dica<a href="<help:help>/consultandopessoajurdica.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
		<p class="orientacaoInicialPopup">Para pesquisar uma Pessoa Jur�dica, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>
	</c:when>
	<c:when test="${pesquisaClienteForm.map.pessoaFisica eq true && pesquisaClienteForm.map.pessoaJuridica ne true}">
		<h1 class="tituloInternoPopup">Pesquisar Pessoa F�sica<a href="<help:help>/consultandopessoafsica.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
		<p class="orientacaoInicialPopup">Para pesquisar uma Pessoa F�sica, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>
	</c:when>
	<c:otherwise>
		<h1 class="tituloInternoPopup">Pesquisar Pessoa<a href="<help:help>/consultandocliente.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
		<p class="orientacaoInicialPopup">Para pesquisar um cliente, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>
	</c:otherwise>
</c:choose>

<form:form method="post" action="pesquisarClientePopup" id="pesquisarClientPopupMultSelectForm"> 

	<input name="multSelect" type="hidden" id="multSelect" value='true'/>
	<input name="comGrupoEconomico" type="hidden" id="comGrupoEconomico" value='true'/>
	<input name="acao" type="hidden" id="acao" value="pesquisarClientePopupMultSelect"/>
	<input name="idSelecionado" type="hidden" id="idSelecionado" value=""/>
	<input name="pessoaFisica" type="hidden" id="pessoaFisica" value="${pessoaFisica}"/>
	<input name="pessoaJuridica" type="hidden" id="pessoaJuridica" value="${pessoaJuridica}"/>
	<input name="habilitado" type="hidden" id="habilitado" value="true"/>
	<input type="hidden" name="funcionalidade" id="funcionalidade" value="${funcionalidade}" />
	<input name="listaIdCliente" type="hidden" id="listaIdCliente" value="${chavesPrimarias}"/>
	
	<fieldset 
		<c:choose>
			<c:when test="${pessoaFisica eq true && pessoaJuridica ne true}">
				id="pesquisarClientePessoaFisicaPopup"
			</c:when>
			<c:otherwise>
				id="pesquisarClientePessoaJuridicaPopup"
			</c:otherwise>
		</c:choose>>
		<label class="rotulo" id="rotuloNome" for="nome">Nome:</label>
		<input class="campoTexto" type="text" id="nome" name="nome" maxlength="50" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${cliente.nome}"><br />
		<c:if test="${pessoaJuridica eq true}">
		<label class="rotulo" id="rotuloNomeFantasia" for="nomeFantasia">Nome fantasia:</label>
			<input class="campoTexto" type="text" id="nomeFantasia" name="nomeFantasia" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" maxlength="50" size="50" value="${cliente.nomeFantasia}"><br />
			<label class="rotulo" id="rotuloCNPJ" for="cnpj">CNPJ:</label>
			<input class="campoTexto" type="text" id="cnpj" name="cnpj" maxlength="18" size="18" value="${cliente.cnpj}"><br />
		</c:if>
		<c:if test="${pessoaFisica eq true}">
		<label class="rotulo" id="rotuloCPF" for="cpf">CPF:</label>
		<input class="campoTexto" type="text" id="cpf" name="cpf"  maxlength="14" size="14" value="${cliente.cpf}"><br />
		<label class="rotulo" for="passaporte">Passaporte:</label>
		<input class="campoTexto" type="text" id="passaporte" name="passaporte" maxlength="13" size="13" value="${cliente.numeroPassaporte}" onkeypress="return formatarCampoInteiro(event);"><br />
		</c:if>
		<c:if test="${pessoaJuridica eq true}">
		<label class="rotulo" id="rotuloInscricaoEstadual" for="inscricaoEstadual">Inscri��o estadual:</label>
		<input class="campoTexto" type="text" id="inscricaoEstadual" name="inscricaoEstadual" maxlength="25" size="25" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${cliente.inscricaoEstadual}"><br />
		<label class="rotulo" id="rotuloInscricaoMunicipal" for="inscricaoMunicipal">Inscri��o municipal:</label>
		<input class="campoTexto" type="text" id="inscricaoMunicipal" name="inscricaoMunicipal" maxlength="25" size="25" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${cliente.inscricaoMunicipal}"><br />
		<label class="rotulo" id="rotuloInscricaoRural" for="inscricaoRural">Inscri��o rural:</label>
		<input class="campoTexto" type="text" id="inscricaoRural" name="inscricaoRural" maxlength="25" size="25" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${cliente.inscricaoRural}"><br />
		</c:if>
		<fieldset class="exibirCep">
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="idCampoCep" value="cep"/>
				<jsp:param name="cepObrigatorio" value="false"/>
				<jsp:param name="numeroCep" value="${numeroCep}"/>
			</jsp:include>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparDados();">
		<c:if test="${clientesEncontrados ne null}">
	    	<input id="buttonSelecionar" name="button" class="bottonRightCol2 botaoGrande1" value="Selecionar"  type="button" onclick="selecionarMultiplosClientes();">
	    </c:if>
	    <input name="button" id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Pesquisar"  onclick="$('input:checkbox').removeAttr('checked');" type="submit"style="margin-right:10px;">
	 </fieldset>
	
	
	<c:if test="${clientesEncontrados ne null}">
		<display:table class="dataTableGGAS dataTablePopup" name="clientesEncontrados" sort="list" id="cliente" pagesize="15"  requestURI="pesquisarClientePopup">
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="chavesPrimarias" value="${cliente.chavePrimaria}">
	     	</display:column>
			<display:column titleKey="CLIENTE_NOME" sortable="true" sortProperty="nome" style="text-align: left">
				<a href='javascript:selecionarNoLink(<c:out value='${cliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${cliente.nome}'/>
				</a>
			</display:column>
		</display:table>
	</c:if>
 	<a name="pesquisaClienteResultados"></a>
</form:form> 
