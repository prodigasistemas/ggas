<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script language="javascript">

	function selecionarContatoCliente(idSelecionado) {
		
		<c:choose>
			<c:when test="${sessionScope.listaContatoModificada ne null && sessionScope.listaContatoModificada == true}" >
				alert('Para selecionar um contato salve as altera��es realizadas!');
			</c:when>
			<c:otherwise>			
				window.opener.selecionarContatoCliente(idSelecionado);
				window.close();
			</c:otherwise>
		</c:choose>
	}

	
	function salvarContatosCliente(){
		submeter('clienteForm', 'salvarContatosCliente');
	}
	
	function fechar() {
		window.close();
	}
		
</script>

<h1 class="tituloInternoPopup">Contatos do Cliente: <c:out value="${cliente.nome}"/></h1>
<p class="orientacaoInicialPopup">Adicione ou altere um contato e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para atualizar os contatos do cliente. Clique no �cone <img title="Selecionar Contato" alt="Selecionar Contato"  src="<c:url value="/imagens/check2.gif"/>" border="0" /> selecionar o contato como solicitante do chamado.</p>

<form:form method="post" action="salvarContatosCliente" id="clienteForm">
 	
 	<input name="acao" type="hidden" id="acao" value="alterarCliente"/>
 	<input name="nome" type="hidden" id="nome" value="${cliente.nome}"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cliente.chavePrimaria}"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="indexLista" type="hidden" id="indexLista" value="${cliente.indexLista}">
	
<fieldset id="tabs" style="display: none">
	<ul>
		<li><a href="#clienteAbaContatos">Contatos</a></li>
	</ul>

	<fieldset id="clienteAbaContatos">
		<jsp:include page="/jsp/cadastro/cliente/abaContato.jsp">
			<jsp:param name="actionAdicionarContato" value="adicionarContatoDoClienteFluxoPopupContatos" />
			<jsp:param name="actionRemoverContato" value="false" />
			<jsp:param name="actionAtualizarContatoPrincipal" value="atualizarContatoPrincipalDoClienteFluxoPopupContatos" />
			<jsp:param name="popupContatos" value="true" />
		</jsp:include>
	</fieldset>
	
	<input name="Button" class="bottonRightCol" value="Fechar" type="button" onClick="fechar();">
	<input name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit" onclick="salvarContatosCliente()">
	
</fieldset>

</form:form>
 
