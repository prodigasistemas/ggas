<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<script language="javascript">

	$(document).ready(function(){	

	//Coloca o cursor no primeiro campo de pesquisa
	$("#codigo").focus();

	$("#codigo").inputmask("a9999-9/99",{placeholder:"_"});  
	$("#codigoCNAE").inputmask("9999999",{placeholder:"_"});  
	
	
	/*Executa a pesquisa quando a tecla ENTER � pressionada, 
	caso o cursor esteja em um dos campos do formul�rio*/
	$(':text').keypress(function(event) {
		if (event.keyCode == '13') {
			pesquisarAtividadeEconomica();
		}
	});
	
	});
	
	function selecionarAtividadeEconomica(idSelecionado) {
		window.opener.selecionarAtividadeEconomica(idSelecionado);
		window.close();
	}
	
	function limparCamposAtividadesEconomicas(){
		document.getElementById('codigo').value = '';
		document.getElementById('descricao').value = '';
		document.getElementById('codigoCNAE').value = '';
	}
	
	function pesquisarAtividadeEconomica() {
		$("#botaoPesquisar").attr('disabled','disabled');
		submeter('pesquisaAtividadeEconomicaForm', 'pesquisarAtividadeEconomicaPopup');
	}

	function init() {
		<c:if test="${listaAtividadesEconomicas ne null}">
			$.scrollTo($('#atividade'),800);
		</c:if>
	}	

	addLoadEvent(init);

</script>

	<h1 class="tituloInternoPopup">Pesquisar Atividade Econ�mica<a href="<help:help>/consultadasatividadeseconmicas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	<p class="orientacaoInicialPopup">Para pesquisar uma Atividade Econ�mica, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form:form method="post" action="pesquisarAtividadeEconomicaPopup" id="pesquisaAtividadeEconomicaForm" name="pesquisaAtividadeEconomicaForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarAtividadeEconomicaPopup">
	
	<fieldset id="pesquisarAtividadeEconomicaPopup">
		<label class="rotulo" id="rotuloCodigo" for="codigo">CNAE:</label>
		<input class="campoTexto" name="codigo" id="codigo" maxlength="10" size="10" type="text" value="${atividadeEconomica.codigo}"><br />
		
		<label class="rotulo" id="rotuloCodigoCNAE" for="codigoCNAE">C�digo:</label>
		<input class="campoTexto" name="codigoOriginal" id="codigoCNAE" maxlength="10" size="10" type="text" value="${atividadeEconomica.codigoOriginal}"><br />
		
		<label class="rotulo" id="rotuloDescricao" for="descricao">Descri��o:</label>
		<input class="campoTexto" name="descricao" id="descricao" maxlength="50" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" type="text" value="${atividadeEconomica.descricao}" />
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparCamposAtividadesEconomicas();">
	    <input name="button" class="bottonRightCol2 botaoGrande1" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarAtividadeEconomica()">
	</fieldset>
	
	<c:if test="${listaAtividadesEconomicas ne null}">
		<display:table class="dataTableGGAS dataTablePopup" name="listaAtividadesEconomicas" sort="list" id="atividade" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAtividadeEconomicaPopup">
			<display:column title="CNAE" style="width: 85px">  
		     	<a href='javascript:selecionarAtividadeEconomica(<c:out value='${atividade.chavePrimaria}'/>);'>
					<c:out value='${atividade.codigo}'/>
				</a>
			</display:column>	
		    <display:column title="Descri��o" style="text-align: left">
		    	<a href='javascript:selecionarAtividadeEconomica(<c:out value='${atividade.chavePrimaria}'/>);'>
					<c:out value='${atividade.descricao}'/>
				</a>
		    </display:column>   
		</display:table>
	</c:if>
 
</form:form>
