<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>

$(document).ready(function() {
	$("#cpfContato").inputmask("999.999.999-99", {
		placeholder : "_"
	});
	
	$('#botaoAlterarContato').hide();
 	
 	 $('#contato').DataTable( {
    	 "responsive": true,
    	 "paging":   false,
         "ordering": false,
         "info": false,
         "searching": false,
    	 "language": {
             "zeroRecords": "Nenhum registro foi encontrado"
    	 }
    } );
 	
	
});

function alterarContato(indice) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	submeter('clienteForm', 'adicionarContatoDoCliente');
}

function incluirContato(form, actionAdicionarContato) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	$("#indexLista").val(-1);
	submeter('clienteForm', 'adicionarContatoDoCliente');
}
   
function removerContato(indice) {
	var retorno = confirm('Deseja excluir o registro?');
	if(retorno == true){
		document.forms[0].status.value = true;
		document.forms[0].postBack.value = true;
		$("#indexLista").val(indice);
		submeter('clienteForm', 'removerContatoDoCliente');
	}
}

function atualizarContatoPrincipal(indice) {
	document.forms[0].status.value = true;
	document.forms[0].postBack.value = true;
	$("#indexLista").val(indice);
	submeter('clienteForm', 'atualizarContatoPrincipalDoCliente');
}

function limparContato() {
	$("#indexLista").val(-1);
	document.forms[0].idTipoContato.selectedIndex = 0;
	document.forms[0].areaContato.value = "";
	document.forms[0].emailContato.value = "";
	document.forms[0].idProfissaoContato.value = -1;	
	document.forms[0].nomeContato.value = "";
	document.forms[0].idDddContato.value = "";
	document.forms[0].idTelefoneContato.value = "";
	document.forms[0].ramalContato.value = "";
	document.forms[0].dddContato2.value = "";
	document.forms[0].telefoneContato2.value = "";
	document.forms[0].ramalContato2.value = "";
	document.forms[0].cpfContato.value= "";
	document.forms[0].rgContato.value= "";
	document.forms[0].indexLista.disabled = false;
	document.forms[0].idTipoContato.disabled = false;
	document.forms[0].nomeContato.disabled = false;
	document.forms[0].idDddContato.disabled = false;
	document.forms[0].idTelefoneContato.disabled = false;
	document.forms[0].ramalContato.disabled = false;
	document.forms[0].dddContato2.disabled = false;
	document.forms[0].telefoneContato2.disabled = false;
	document.forms[0].ramalContato2.disabled = false;	
	document.forms[0].idProfissaoContato.disabled = false;
	document.forms[0].areaContato.disabled = false;
	document.forms[0].emailContato.disabled = false;
	document.forms[0].cpfContato.disabled = false;
	document.forms[0].rgContato.disabled = false;
	
}

function exibirAlteracaoDoContato(indice,idTipoContato,nomeContato,idDddContato,idTelefoneContato,ramalContato,
		dddContato2,telefoneContato2,ramalContato2,cargoContato,areaContato,emailContato,chavePrimariaContato, cpf, rg) {
	if (indice != "") {
		$("#indexLista").val(indice);
		if (idTipoContato != "") {
			var tamanho = document.forms[0].idTipoContato.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idTipoContato.options[i];
				if (idTipoContato == opcao.value) {
					opcao.selected = true;
					break;
				}
			}
		}
		
		if (cargoContato != "") {
			var tamanho = document.forms[0].idProfissaoContato.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idProfissaoContato.options[i];
				if (cargoContato == opcao.value) {
					opcao.selected = true;
					break;
				}
			}
		}		
		
		
		if (nomeContato != "") {
			document.forms[0].nomeContato.value = nomeContato;
		}
		if (idDddContato != "") {
			document.forms[0].idDddContato.value = idDddContato;
		} else {
			document.forms[0].idDddContato.value = "";
		}
		if (idTelefoneContato != "") {
			document.forms[0].idTelefoneContato.value = idTelefoneContato;
		} else {
			document.forms[0].idTelefoneContato.value = "";
		}
		if (ramalContato != "") {
			document.forms[0].ramalContato.value = ramalContato;
		} else {
			document.forms[0].ramalContato.value = "";
		}
		if (dddContato2 != "") {
			document.forms[0].dddContato2.value = dddContato2;
		} else {
			document.forms[0].dddContato2.value = "";
		}
		if (telefoneContato2 != "") {
			document.forms[0].telefoneContato2.value = telefoneContato2;
		} else {
			document.forms[0].telefoneContato2.value = "";
		}
		if (ramalContato2 != "") {
			document.forms[0].ramalContato2.value = ramalContato2;
		} else {
			document.forms[0].ramalContato2.value = "";
		}
		if (areaContato != "") {
			document.forms[0].areaContato.value = areaContato;
		} else {
			document.forms[0].areaContato.value = "";
		}
		if (emailContato != "") {
			document.forms[0].emailContato.value = emailContato;
		} else {
			document.forms[0].emailContato.value = "";
		}
		if (cpf != "") {
			$("#cpfContato").val(cpf).inputmask("999.999.999-99", {
				placeholder : "_"
			});
		} else {
			document.forms[0].cpfContato.value = "";
		}
		if (rg != "") {
			document.forms[0].rgContato.value = rg;
		} else {
			document.forms[0].rgContato.value = "";
		}
		if (chavePrimariaContato != ""){
			document.forms[0].chavePrimariaContato.value = chavePrimariaContato;
		}
		
	}
}

function onloadContato() {
	<c:choose>
		<c:when test="${sucessoManutencaoLista}">
			limparContato();
		</c:when>
	</c:choose>
}

<c:if test="${abaId == '3'}">
	addLoadEvent(onloadContato);
	
	$('#identificacao-tab').removeClass('active');
	$('#clienteAbaIdentificacao').removeClass('active show');
	$('#contatos-tab').addClass('active');
	$('#clienteAbaContatos').addClass('active show');
	
	
</c:if>

function habilitarBotaoAlterarContato(){
	$('#botaoAlterarContato').show();
	$('#botaoIncluirContato').hide();
}

</script>

<div class="row">
 	<h5 class="col-md-12 mt-2">Dados do Contato</h5>
	<div class="col-md-6">
		<div class="form-row">
			<input type="hidden" id="chavePrimariaContato" name="chavePrimariaContato" />
			<div class="col-md-12">
				<label id="rotuloTipoContato" for="idTipoContato">Tipo de Contato: <span class="text-danger">*</span></label>
             		<select name="tipoContato" id="idTipoContato" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
						<c:forEach items="${listaTiposContato}" var="tipoContato">
						<option value="<c:out value="${tipoContato.chavePrimaria}"/>" <c:if test="${contatoCliente.tipoContato.chavePrimaria == tipoContato.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoContato.descricao}"/>
						</option>		
					</c:forEach>
           			</select>
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-12">
				<label id="rotuloNomeContato" for="nomeContato">Nome do Contato:<span class="text-danger">*</span></label>
        			<input type="text" id="nomeContato" name="nomeContato" class="form-control form-control-sm" 
                    onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
                    maxlength="50" value="${contatoCliente.nome}">
			</div>
		</div>
		
		<div class="form-row">
			<div class="col-md-3">
				<label id="rotuloDDDContato" for="idDddContato">DDD: </label>
        			<input type="text" id="idDddContato" name="codigoDDDContato" class="form-control form-control-sm" 
                    onkeypress="return formatarCampoInteiro(event);" maxlength="2" value="${contatoCliente.codigoDDD}">
			</div>
			<div class="col-md-5">
				<label id="rotuloNumeroContato" for="telefoneContato">Telefone:</label>
        			<input type="text" id="idTelefoneContato" name="foneContato" class="form-control form-control-sm" 
                    maxlength="9" onkeypress="return formatarCampoInteiro(event)" value="${contatoCliente.fone}">
			</div>
			<div class="col-md-4">
				<label id="rotuloRamalContato" for="ramalContato">Ramal:</label>
        			<input type="text" id="idRamalContato" name="ramalContato" class="form-control form-control-sm" 
                    maxlength="4" onkeypress="return formatarCampoInteiro(event)" value="${contatoCliente.ramal}">
			</div>
		</div>
		
		<div class="form-row">
			<div class="col-md-3">
				<label id="rotuloDDDContato2" for="dddContato2">DDD: (opcional)</label>
        			<input type="text" id="dddContato2" name="codigoDDD2Contato" class="form-control form-control-sm" 
                   onkeypress="return formatarCampoInteiro(event);" maxlength="2" value="${contatoCliente.codigoDDD2}">
			</div>
			<div class="col-md-5">
				<label id="rotuloNumeroContato2" for="telefoneContato2">Telefone: (opcional)</label>
        			<input type="text" id="telefoneContato2" name="fone2Contato" class="form-control form-control-sm" 
                    maxlength="9" onkeypress="return formatarCampoInteiro(event)" value="${contatoCliente.fone2}">
			</div>
			<div class="col-md-4">
				<label id="rotuloRamalContato2" for="ramalContato2">Ramal: (opcional)</label>
        			<input type="text" id="ramalContato2" name="ramal2Contato" class="form-control form-control-sm" 
                    maxlength="4" onkeypress="return formatarCampoInteiro(event)" value="${contatoCliente.ramal2}">
			</div>
		</div>
		
	</div>
	<div class="col-md-6">
		<div class="form-row">
			<div class="col-md-7">
				<label for="cargoContato">Cargo:</label>
             		<select name="profissaoContato" id="idProfissaoContato" class="form-control form-control-sm"> 
             			<option value="-1">Selecione</option>
						<c:forEach items="${profissoes}" var="profissao">
						<option value="<c:out value="${profissao.chavePrimaria}"/>" <c:if test="${contatoCliente.profissao.chavePrimaria == profissao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${profissao.descricao}"/>
						</option>		
						</c:forEach>
           		</select>
			</div>
			
			<div class="col-md-5">
				<label for="areaContato">�rea:</label>
        			<input type="text" id="areaContato" name="descricaoAreaContato" class="form-control form-control-sm"  maxlength="50"
                     onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${contatoCliente.descricaoArea}">
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-12">
				<label id="rotuloEmailContato" for="emailContato">E-mail do contato:</label>
        			<input type="text" id="emailContato" name="emailContato" class="form-control form-control-sm" 
                    maxlength="80" onkeypress="return formatarCampoEmail(event)" value="${contatoCliente.email}">
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-6">
				<label id="rotuloCpfContato" for="cpfContato">CPF:</label>
        			<input type="text" id="cpfContato" name="cpfContato" class="form-control form-control-sm" 
                   maxlength="14" size="14" value="${contatoCliente.cpf}">
			</div>
			<div class="col-md-6">
				<label id="rotuloEmailContato" for="emailContato">RG:</label>
        			<input type="text" id="rgContato" name="rgContato" class="form-control form-control-sm" 
                    maxlength="20" size="10" value="${contatoCliente.rg}">
			</div>
		</div>
		
		
		<p class = "mt-1"><span class="text-danger">* </span>Campos Obrigat�rios apenas para Cadastrar Contatos.</p>
		
		<div class="form-row">
			<div class="col-md-12 align-self-center text-center">
				<button class="btn btn-primary btn-sm" name="botaoIncluirContato" id="botaoIncluirContato"
          			type="button" onclick="incluirContato(this.form,'<c:out value="${param['actionAdicionarContato']}"/>');">
                	<i class="fas fa-plus"></i> Adicionar
          		</button>
          		<button class="btn btn-success btn-sm" name="botaoAlterarContato" id="botaoAlterarContato"
          			type="button" onclick="alterarContato(this.form);">
               		<i class="fas fa-save"></i> Alterar
          		</button>
				<button class="btn btn-secondary btn-sm" name="botaoLimparContato" id="botaoLimparContato"
					type="button" onclick="limparContato();">
            		<i class="far fa-trash-alt"></i> Limpar
          		</button>
          	</div>
		</div>
	</div>
</div>

<c:if test="${listaContato ne null}">
	<div class="row mt-2">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover" id="contato" style="width:100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th class="text-center">Descri��o</th>
				 			<th class="text-center">Nome</th>
				 			<th class="text-center">DDD</th>
				 			<th class="text-center">Telefone</th>
				 			<th class="text-center">Ramal</th>
				 			<th class="text-center">Cargo</th>
				 			<th class="text-center">�rea</th>
				 			<th class="text-center">E-mail</th>
				 			<th class="text-center">A��es</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="i" value="0" />
						<c:forEach items="${listaContato}" var="contato">
						<tr>
							<td class="text-center">
								${contato.tipoContato.descricao}
							</td>
							<td class="text-center">
								${contato.nome}
							</td>
							<td class="text-center">
								${contato.codigoDDD}
							</td>
							<td class="text-center">
								${contato.fone}
							</td>
							<td class="text-center">
								${contato.ramal}
							</td>
							<td class="text-center">
								${contato.profissao.descricao}
							</td>
							<td class="text-center">
								${contato.descricaoArea}
							</td>
							<td class="text-center">
								${contato.email}
							</td>
							<td class="text-center">
								<c:choose>
				 					<c:when test="${contato.principal == false}">
				 					<button type="button" class="btn btn-primary btn-sm" title="Tornar Principal" onClick="atualizarContatoPrincipal(<c:out value="${i}"/>);">
				 						<i class="fas fa-address-card"></i>
				 					</button>
				 					</c:when>
				 				</c:choose>
				 				<button type="button" class="btn btn-info btn-sm" title="Alterar Contato" id="idExibirIndexContato"
        						onClick="exibirAlteracaoDoContato('${i}','${contato.tipoContato.chavePrimaria}','${contato.nome}','${contato.codigoDDD}','${contato.fone}','${contato.ramal}',
        							'${contato.codigoDDD2}','${contato.fone2}','${contato.ramal2}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.email}','${contato.chavePrimaria}', '${contato.cpf}', '${contato.rg}');habilitarBotaoAlterarContato()">
        						<i class="fas fa-pen-square"></i></button>
        						
        						<c:choose>
        							<c:when test="${ param['popupContatos']}">
        							<button class="btn btn btn-light btn-sm" type="button" title="Selecionar Contato"
        							 onClick="selecionarContatoCliente('${contato.chavePrimaria}');">
              						<i class="fas fa-mouse-pointer"></i></button>
        						</c:when>
        						<c:otherwise>
        							<button class="btn btn-secondary btn-sm" type="button" title="Remover Contato"
        							 onClick="removerContato(<c:out value="${i}"/>);">
              						<i class="far fa-trash-alt"></i> 
          							</button>
          						</c:otherwise>
          						</c:choose>
							</td>
						</tr>
						<c:set var="i" value="${i+1}" />
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</c:if>

		
