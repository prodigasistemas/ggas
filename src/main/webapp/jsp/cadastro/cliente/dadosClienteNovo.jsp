<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>	
	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("<c:out value='${param.idCampoIdCliente}' default=""/>");
		var nomeCompletoCliente = document.getElementById("<c:out value='${param.idCampoNomeCliente}' default=""/>");
		var documentoFormatado = document.getElementById("<c:out value='${param.idCampoDocumentoFormatado}' default=""/>");
		var emailCliente = document.getElementById("<c:out value='${param.idCampoEmail}' default=""/>");
		var enderecoFormatado = document.getElementById("<c:out value='${param.idCampoEnderecoFormatado}' default=""/>");		
		  if( document.getElementById("exibirGridPontoConsumo")){
			document.getElementById("exibirGridPontoConsumo").value = "";
		  }
		if(idSelecionado != '') {		
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];
		               	if(cliente["cnpj"] != undefined ){
		               		documentoFormatado.value = cliente["cnpj"];
		               	} else {
			               	if(cliente["cpf"] != undefined ){
			               		documentoFormatado.value = cliente["cpf"];
			               	} else {
			               		documentoFormatado.value = "";
			               	}
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
		               	$('form[name="notaDebitoCreditoForm"]').change();
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	documentoFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
		document.getElementById("idCliente").value = idCliente.value;
        document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
        document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
        document.getElementById("emailClienteTex").value = emailCliente.value;
        document.getElementById("enderecoFormatadoClienteTexto").value = enderecoFormatado.value;
        if( document.getElementById("pontoConsumo")){
        	teste = document.getElementById("teste")

        	if(teste!=null){
        		teste.hidden = true;
            }
        	
        	linha = document.getElementById("linha");

        	if(linha!=null){
        		linha.hidden = true;
            }

        	exibirGridPontoConsumo = document.getElementById("exibirGridPontoConsumo");

        	if(exibirGridPontoConsumo!=null){
        		exibirGridPontoConsumo.value = "nao";
            }
        }
		//Verifica se a fun��o foi passada por par�metro e se ela � realmente v�lida antes de execut�-la.
        if (document.getElementById("funcaoParametro")!= null && document.getElementById("funcaoParametro").value != "") {
			var funcao = document.getElementById("funcaoParametro").value;
			if (funcaoExiste(funcao)) {
				funcao = funcao + '(\'' + idCliente.value + '\')';
				eval(funcao);
			}
        }
	}
	
	function exibirPopupPesquisaCliente() {	

		var restricaoTipoPessoa = '';

		<c:choose>
			<c:when test="${param.consultarPessoaFisica && not param.consultarPessoaJuridica}">
				restricaoTipoPessoa = '&pessoaFisica=true';
			</c:when>
			<c:when test="${not param.consultarPessoaFisica && param.consultarPessoaJuridica}">
				restricaoTipoPessoa = '&pessoaJuridica=true';
			</c:when>
		</c:choose>

		funcionalidade = "<c:out value='${param.funcionalidade}'/>";
		if(funcionalidade!=""){
			funcionalidade = "&funcionalidade=" + funcionalidade;
		}
		popup = window.open('exibirPesquisaClientePopup?' + funcionalidade + restricaoTipoPessoa,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

	}

	function limparFormularioDadosCliente(){
		document.getElementById('<c:out value='${param.idCampoIdCliente}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoNomeCliente}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoDocumentoFormatado}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoEnderecoFormatado}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoEmail}' default=""/>').value = "";
        document.getElementById('nomeClienteTexto').value = "";
        document.getElementById('documentoFormatadoTexto').value = "";
        document.getElementById('emailClienteTexto').value = "";
        document.getElementById('enderecoFormatadoClienteTexto').value = "";
	}

	function desabilitarPesquisaCliente(){
		document.getElementById('botaoPesquisarCliente').disabled = true;
	}
	
	function habilitarPesquisaCliente(){
		document.getElementById('botaoPesquisarCliente').disabled = false;
	}

</script>

<h5>Pesquisar Pessoa</h5>

<input name="<c:out value='${param.idCampoIdCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoIdCliente}' default=""/>" value="${param.idCliente}">
<input name="<c:out value='${param.idCampoNomeCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoNomeCliente}' default=""/>" value="${param.nomeCliente}">
<input name="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" value="${param.documentoFormatadoCliente}">
<input name="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" value="${param.enderecoFormatadoCliente}">
<input name="<c:out value='${param.idCampoEmail}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEmail}' default=""/>" value="${param.emailCliente}">
<input name="funcaoParametro" type="hidden" id="funcaoParametro" value="${param.funcaoParametro}">

 <div class="form-row mt-1">
    <label for="nomeClienteTexto" class="col-sm-2 col-form-label">Pessoa: <c:if test="${param.dadosClienteObrigatorios}"><span class="text-danger">* </span></c:if></label>
    <div class="col-sm-10">
    	<div class="input-group">
  			<input type="text" class="form-control form-control-sm" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" value="${param.nomeCliente}" disabled>
  			<div class="input-group-append">
    			<button class="btn btn-primary btn-sm" type="button" title="Pesquisar Pessoa" onclick="exibirPopupPesquisaCliente(); exibirIndicador();"><i class="fas fa-search"></i></button>
  			</div>
		</div>
    </div>
</div>

<div class="form-row mt-1">
	<label for="enderecoFormatadoClienteTexto" class="col-sm-2 col-form-label">Endere�o: <c:if test="${param.dadosClienteObrigatorios}"><span class="text-danger">* </span></c:if></label>
	<div class="col-sm-10">
      	<input type="text" class="form-control form-control-sm" id="enderecoFormatadoClienteTexto"
      		name="enderecoFormatadoClienteTexto" value="${param.enderecoFormatadoCliente}" disabled>
    </div>
</div>

<div class="form-row mt-1">
    <c:choose>
		<c:when test="${param.consultarPessoaFisica && not param.consultarPessoaJuridica}">
			<label for="documentoFormatadoTexto" class="col-sm-2 col-form-label">CPF: <c:if test="${param.dadosClienteObrigatorios}"><span class="text-danger">* </span></c:if></label>
		</c:when>
		<c:when test="${not param.consultarPessoaFisica && param.consultarPessoaJuridica}">
			<label for="documentoFormatadoTexto" class="col-sm-2 col-form-label">CNPJ: <c:if test="${param.dadosClienteObrigatorios}"><span class="text-danger">* </span></c:if></label>
		</c:when>
		<c:otherwise>
			<label for="documentoFormatadoTexto" class="col-sm-2 col-form-label">CPF/CNPJ: <c:if test="${param.dadosClienteObrigatorios}"><span class="text-danger">* </span></c:if></label>
		</c:otherwise>
	</c:choose>
    <div class="col-sm-9">
      	<input type="text" class="form-control form-control-sm" id="documentoFormatadoTexto"
      		name="documentoFormatadoTexto" maxlength="18" size="18" disabled
            value="${param.documentoFormatadoCliente}">
    </div>
</div>


<div class="form-row mt-1">
	<label for="emailClienteTex" class="col-sm-2 col-form-label">E-mail: <c:if test="${param.dadosClienteObrigatorios}"><span class="text-danger">* </span></c:if></label>
	<div class="col-sm-9">
      	<input type="text" class="form-control form-control-sm" id="emailClienteTex" maxlength="80" size="40" 
      	name="emailClienteTexto" disabled value="${param.emailCliente}">
    </div>
</div>
