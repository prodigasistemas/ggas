<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<h1 class="tituloInterno">Detalhar Tipo de Contato 
	<a class="linkHelp"
		href="<help:help>/consultadostiposdecontato.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para modificar as informa��es deste registro clique em <span
		class="destaqueOrientacaoInicial">Alterar</span>
</p>

<script>

	$(document).ready(function(){
	    
	    var max = 0;
	
	      $('.rotulo').each(function(){
	              if ($(this).width() > max)
	                 max = $(this).width();   
	          });
	      $('.rotulo').width(max);
	});
	
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaTipoContato"/>';
	}

	function alterar() {
		submeter('tipoContatoForm', 'exibirAtualizarTipoContato');
	}
	
</script>

<form:form method="post" action="exibirDetalhamentoTipoContato" id="tipoContatoForm" name="tipoContatoForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria"value="${tipoContato.chavePrimaria}">
	<input name="descricao" type="hidden" id="descricao" value="${tipoContato.descricao}">
	<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">

	<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
		<label class="rotulo">Descri��o:</label>
		<span class="itemDetalhamento">${tipoContato.descricao}</span>
		<br /><br />
	</fieldset>
	
	<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
	
	<br />
	</fieldset>

	</fieldset>

	<fieldset class="conteinerBotoes">
		<input id="botaoVoltar" name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    	<vacess:vacess param="exibirDetalhamentoTipoContato">
    		<input id="botaoAlterar" name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>
	</fieldset>
</form:form>