<!--
 Copyright (C) <2011> GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

 Este programa  um software livre; voc pode redistribu-lo e/ou
 modific-lo sob os termos de Licena Pblica Geral GNU, conforme
 publicada pela Free Software Foundation; verso 2 da Licena.

 O GGAS  distribudo na expectativa de ser til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
 COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
 Consulte a Licena Pblica Geral GNU para obter mais detalhes.

 Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
 junto com este programa; se no, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
	integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css">


<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"
	integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js"
	integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">

	
	$(document).ready(function() {
		iniciarDatatable('#veiculos');
		
	});

	function pesquisarVeiculo() {
		submeter("veiculoForm", "pesquisarVeiculo");
	}
	
	function exibirIncluirVeiculo() {
		submeter("veiculoForm", "exibirIncluirVeiculo");
	}
	
	function exibirAlterarVeiculo() {
		var selecao = verificarSelecaoApenasUm();
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		if (selecao == true) {
			submeter("veiculoForm", "exibirAlterarVeiculo");
		}
	}
	
	function limparFormulario(){		
		$('#uf option[value=-1]').attr('selected','selected');
		
		$('#placa').val('');
		$('#modelo').val('');
		$('input[name="habilitado"][value="null"]').prop('checked', true);
	}
	
</script>

<div class="bootstrap">

	<form:form action="pesquisarVeiculo" id="veiculoForm"
		name="veiculoForm" method="post" modelAttribute="Veiculo">
		<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="-1"/>

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Pesquisar Veiculo</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para
					pesquisar o ve�culo, clique em <b>Pesquisar</b> para visualizar os
					ve�culos.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">

						<div class="row mb-2">

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Modelo:</label> 
												<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
													style="padding-left: 0px !important;">
													<input class="form-control form-control-sm" type="text"
														id="modelo" name="modelo" maxlength="32"
														value="${veiculo.modelo}">
												</div>
											<select hidden="hidden" name="uf" id="uf"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${unidadesFederacao}"
												var="unidadeFederacao">
												<option
													value="<c:out value="${unidadeFederacao.chavePrimaria}"/>"
													<c:if test="${veiculo.uf.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${unidadeFederacao.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label>Placa:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
											style="padding-left: 0px !important;">
											<input class="form-control form-control-sm" type="text"
												id="placa" name="placa" maxlength="7"
												value="${veiculo.placa}">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
											<label>Indicador de Usos:</label>
											<div class="input-group input-group-sm col-sm-12 pl-1 pr-0">
												<div class="form-check form-check-inline">
												  <input class="form-check-input" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
												  <label class="form-check-label" for="habilitado">Ativo</label>
												</div>
												<div class="form-check form-check-inline">
												  <input class="form-check-input" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
												  <label class="form-check-label" for="habilitado">Inativo</label>
												</div>
												<div class="form-check form-check-inline">
												  <input class="form-check-input" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null' || habilitado eq null}">checked</c:if>>
												  <label class="form-check-label" for="habilitado">Todos</label>
												</div>
											</div>																		
									</div>
								</div>
							</div>

						</div>
						
						<br />
						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoPesquisar"
									type="button" onclick="pesquisarVeiculo();">Pesquisar</button>
								<button class="btn btn-primary btn-sm" id="botaoPesquisar"
									type="button" onclick="exibirIncluirVeiculo();">Incluir</button>
								<button class="btn btn-secondary btn-sm" name="botaoLimpar"
									id="botaoLimpar" value="limparFormulario" type="button"
									onclick="limparFormulario();">
									<i class="far fa-trash-alt"></i> Limpar
								</button>
							</div>
						</div>
												
						<c:if test="${listaVeiculos ne null}">
							<br>
							<hr class="linhaSeparadoraPesquisa" />
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover"  style="width:100%"
									id="veiculos">
									<thead class="thead-ggas-bootstrap">
										<tr>
											<th>
												<div
													class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
													<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
														class="custom-control-input"> <label
														class="custom-control-label p-0" for="checkAllAuto"></label>
												</div>
											</th>
											<th scope="col" class="text-center">Modelo</th>										
											<th scope="col" class="text-center">Placa</th>
											<th scope="col" class="text-center">Status</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listaVeiculos}" var="veiculoResultado">
											<tr>
												<td>
													<div
														class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
														data-identificador-check="chk${veiculo.chavePrimaria}">
														<input id="chk${veiculoResultado.chavePrimaria}"
															type="checkbox" name="chavesPrimarias"
															class="custom-control-input"
															value="${veiculoResultado.chavePrimaria}"> <label
															class="custom-control-label p-0"
															for="chk${veiculoResultado.chavePrimaria}"></label>
													</div>
												</td>
												<td class="text-center">
													<c:out value="${veiculoResultado.modelo}" />
												</td>											
												<td class="text-center"><c:out value="${veiculoResultado.placa}" />
												</td>
												<td class="text-center">    
													<c:choose>
												        <c:when test="${veiculoResultado.habilitado eq true}">
												            Ativo
												        </c:when>
												        <c:otherwise>
												            Inativo
												        </c:otherwise>
												    </c:choose>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>

						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoAlterar"
									type="button" onclick="exibirAlterarVeiculo();">Alterar</button>
								</button>
							</div>
						</div>
						</c:if>

					</div>

				</div>
			</div>
		</div>
	</form:form>

</div>