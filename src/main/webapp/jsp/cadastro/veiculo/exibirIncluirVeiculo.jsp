<!--
 Copyright (C) <2011> GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

 Este programa  um software livre; voc pode redistribu-lo e/ou
 modific-lo sob os termos de Licena Pblica Geral GNU, conforme
 publicada pela Free Software Foundation; verso 2 da Licena.

 O GGAS  distribudo na expectativa de ser til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
 COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
 Consulte a Licena Pblica Geral GNU para obter mais detalhes.

 Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
 junto com este programa; se no, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
	integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css">


<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"
	integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js"
	integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">

	function alterarVeiculo() {
		submeter("veiculoForm", "alterarVeiculo");
	}
	
	function incluirVeiculo() {
		submeter("veiculoForm", "incluirVeiculo");
	}
	
	function voltar(){
		submeter("veiculoForm","pesquisarVeiculo");
	}
	
</script>

<div class="bootstrap">

	<form:form action="pesquisarVeiculo" id="veiculoForm"
		name="veiculoForm" method="post" modelAttribute="Veiculo">
		<c:choose>
        	<c:when test="${ fluxoAlteracao eq true }"><h5 class="card-title mb-0"><input name="chavePrimaria" name="chavePrimaria" type="hidden" value="${ veiculo.chavePrimaria }"></h5></c:when>
        </c:choose>	

		<div class="card">
			<div class="card-header">
 				<c:choose>
                    <c:when test="${ fluxoInclusao eq true }"><h5 class="card-title mb-0">Incluir Ve�culo</h5></c:when>
                    <c:when test="${ fluxoAlteracao eq true }">Alterar Ve�culo</c:when>
                </c:choose>			
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
				
				
					<i class="fa fa-question-circle"></i> 
						<c:choose>
                            <c:when test="${ fluxoInclusao eq true }">
                               Preencha os campos e clique em <b>Incluir</b>
                                para incluir um Ve�culo.

                            </c:when>
                            <c:when test="${ fluxoAlteracao eq true || fluxoAlteracaoRascunho eq true}">
                                Para modificar as informa��es deste registro clique em <b>Alterar</b>.
                            </c:when>
                        </c:choose>
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">

						<div class="row mb-2">

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Modelo:<span
											class="text-danger">*</span></label>
											
											<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
												style="padding-left: 0px !important;">
												<input class="form-control form-control-sm" type="text"
													id="modelo" name="modelo" maxlength="32"
													value="${veiculo.modelo}">
											</div>
											
											<select hidden="hidden" name="uf" id="uf"
											class="form-control form-control-sm">
											<option value="2">Selecione</option>
											<c:forEach items="${unidadesFederacao}"
												var="unidadeFederacao">
												<option
													value="<c:out value="${unidadeFederacao.chavePrimaria}"/>"
													<c:if test="${veiculo.uf.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${unidadeFederacao.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label>Placa:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
											style="padding-left: 0px !important;">
											<input class="form-control form-control-sm" type="text"
												id="placa" name="placa" maxlength="7"
												value="${veiculo.placa}">
										</div>
									</div>
								</div>
							</div>

								<c:if test = "${ fluxoAlteracao eq true }">

									<div class="col-md-6">
										<div class="form-row">
											<div class="col-md-10">
											<label>Indicador de Uso:</label>
												<div class="input-group input-group-sm col-sm-12 pl-1 pr-0">
													<div class="form-check form-check-inline">
													  <input class="form-check-input" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${veiculo.habilitado eq true}">checked</c:if>>
													  <label class="form-check-label" for="habilitado">Ativo</label>
													</div>
													<div class="form-check form-check-inline">
													  <input class="form-check-input" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${veiculo.habilitado eq false}">checked</c:if>>
													  <label class="form-check-label" for="habilitado">Inativo</label>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</c:if>
								
								<c:if test = "${ fluxoInclusao eq true }">

									<div class="col-md-6">
										<div class="form-row">
											<div class="col-md-10">
												<label>Indicador de Uso:</label>
												<div class="input-group input-group-sm col-sm-12 pl-1 pr-0">
													<div class="form-check form-check-inline">
													  <input class="form-check-input" type="radio" name="habilitado" id="habilitado" value="true" checked>
													  <label class="form-check-label" for="habilitado">Ativo</label>
													</div>
													<div class="form-check form-check-inline">
													  <input class="form-check-input" type="radio" name="habilitado" id="habilitado" value="false">
													  <label class="form-check-label" for="habilitado">Inativo</label>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</c:if>

						</div>

						<br />
						<div class="row mt-3">
							<div class="col align-self-end text-left">
									<button class="btn btn-secondary btn-sm" id="botaoVoltar"
										type="button" onclick="voltar();">
										<i class="fa fa-chevron-circle-left"></i>
										Voltar</button>
										
									</button>
							</div>
							
							<div class="col align-self-end text-right">
							 <c:if test="${ fluxoAlteracao eq true }">
								<button class="btn btn-primary btn-sm" id="botaoAlterar"
									type="button" onclick="alterarVeiculo();">Alterar</button>
							 </c:if>
							  <c:if test="${ fluxoInclusao eq true }">		
								<button class="btn btn-primary btn-sm" id="botaoIncluir"
									type="button" onclick="incluirVeiculo();">Incluir</button>
								</button>
							  </c:if>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</form:form>

</div>