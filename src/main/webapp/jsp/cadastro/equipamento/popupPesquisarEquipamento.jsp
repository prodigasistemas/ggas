<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script language="javascript">

	$(document).ready(function(){		
		
		//Coloca o cursor no primeiro campo de pesquisa
		$("#descricao").focus();
	
		/*Executa a pesquisa quando a tecla ENTER � pressionada, 
		caso o cursor esteja em um dos campos do formul�rio*/
		$('#pesquisarEquipamentoPopup > :text').keyup(function(event) {
			if (event.which == '13') {
				pesquisarEquipamento();
			}
	   	});
		
	});

	function selecionarEquipamento(idSelecionado) {
		window.opener.selecionarEquipamento(idSelecionado);
		window.close();
	}

	function limparDados(){
		document.pesquisaEquipamentoForm.descricao.value = "";
		document.pesquisaEquipamentoForm.idSegmento.value = "-1";
	}

	
	function pesquisarEquipamento() {
		//$("#botaoPesquisar").attr('disabled','disabled');
		
		$("#pesquisarEquipamentoPopup").submit();
		//submeter('pesquisaEquipamentoForm', 'pesquisarEquipamentoPopup');
		
	}
	
	function init() {
		<c:if test="${equipamentosEncontrados ne null}">
			$.scrollTo($('#equipamento'),800);
		</c:if>
	}

	addLoadEvent(init);
		
</script>

<h1 class="tituloInternoPopup">Pesquisar Equipamento</h1>
<p class="orientacaoInicialPopup">Para pesquisar um equipamento, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form method="post" action="pesquisarEquipamentoPopup" id="pesquisarEquipamentoPopup">  

	<input name="idSelecionado" type="hidden" id="idSelecionado" value="">

	
	<fieldset id="pesquisarEquipamentoPopup">
			<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="30" size="20" value="${equipamento.descricao}" onkeypress="return formatarCampoAlfaNumericoSemCaracteresEspeciais(event)"
			 onkeyup="return validarCriteriosParaCampo(this, '1', '1', 'formatarCampoNome(event)');"><br />
			
			<label class="rotulo" for="idLocalidade" >Segmento:</label>
			<select class="campoSelect" name="idSegmento" id="idSegmento">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" 
					<c:if test="${equipamento.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>">
						<c:out value="${segmento.descricao}"/>
					</option>
			    </c:forEach>			    		
		    </select>
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparDados();">
	    <input name="button" id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Pesquisar"  type="button" onclick="pesquisarEquipamento()">
	 </fieldset>
	
	
	<c:if test="${equipamentos ne null}">
	<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTablePopup" name="equipamentos" sort="list" id="equipamento" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarEquipamentoPopup">
			<display:column title="Descri��o" sortable="true" sortProperty="descricao">  
				<a href='javascript:selecionarEquipamento(<c:out value='${equipamento.chavePrimaria}'/>);'>
					<c:out value='${equipamento.descricao}'/>
				</a>
			</display:column>
			<display:column title="Segmento" sortable="true" sortProperty="segmento.descricao">  
				<a href='javascript:selecionarEquipamento(<c:out value='${equipamento.chavePrimaria}'/>);'>
					<c:out value='${equipamento.segmento.descricao}'/>
				</a>
			</display:column>
		</display:table>
	</c:if>
	<a name="pesquisaEquipamentoResultados"></a>
</form> 