<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<h1 class="tituloInterno">Detalhar Equipamento</h1>
<p class="orientacaoInicial">
	Para modificar as informa��es deste registro clique em <span
		class="destaqueOrientacaoInicial">Alterar</span>
</p>
<form:form method="post" action="exibirDetalhamentoEquipamento" enctype="multipart/form-data" id="equipamentoForm" name="equipamentoForm">

	<script>
		function voltar() {
			//submeter("equipamentoForm", "pesquisarEquipamentos");
			location.href = '<c:url value="pesquisarEquipamentos"/>';
		}

		function alterar() {
			submeter('equipamentoForm', 'exibirAtualizarEquipamento');
		}
</script>
	


	<input name="acao" type="hidden" id="acao"
		value="exibirDetalhamentoEquipamento">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"
		value="${equipamento.chavePrimaria}">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias"
		value="${equipamento}">

	<fieldset class="detalhamento" id="exibirDetalhamentoEquipamento">
		<legend class="conteinerBlocoTitulo">Dados do Equipamento</legend>
		<fieldset id="detalhamentoDadosCol1" class="coluna">
			<label class="rotulo" id="rotuloDescricaoDetEquipamento"
				for="descricao">Descri��o:</label> <span id="descricao"
				class="itemDetalhamento itemDetalhamentoLargo"><c:out
					value="${equipamento.descricao}" /></span><br /> <label
				class="rotulo" id="rotuloSegmentoDetEquipamento"
				for="segmentoDescricao">Segmento:</label> <span
				id="segmentoDescricao"
				class="itemDetalhamento itemDetalhamentoLargo"><c:out
					value="${equipamento.segmento.descricao}" /></span><br />

		</fieldset>

		<fieldset id="detalhamentoDadosCol2" class="colunaFinal">
			
			<c:if test="${equipamento.segmento.indicadorEquipamentoPotenciaFixa eq null or equipamento.segmento.indicadorEquipamentoPotenciaFixa eq false}">
				<label class="rotulo" id="rotuloPotenciaPadrao for="potenciaFixaAlta">Pot�ncia Padr�o:</label>
				<span id="potenciaFixaAlta" class="itemDetalhamento itemDetalhamentoLargo">
				<fmt:formatNumber pattern="#,###.00" minIntegerDigits="1" value="${equipamento.potenciaPadrao}"/> kCal/h </span>
			</c:if>
			
			<c:if test="${equipamento.segmento.indicadorEquipamentoPotenciaFixa ne null or equipamento.segmento.indicadorEquipamentoPotenciaFixa eq true}">
				<label class="rotulo" id="rotuloVazaoAlta for="potenciaFixaAlta">Pot�ncia
					Alta:</label>
				<span id="potenciaFixaAlta"
					class="itemDetalhamento itemDetalhamentoLargo">
					<fmt:formatNumber pattern="#,###.00" minIntegerDigits="1" value="${equipamento.potenciaFixaAlta}"/> kCal/h </span>
				<label class="rotulo" id="rotuloVazaoMedia" for="potenciaFixaMedia">Pot�ncia
					M�dia:</label>
				<span id="potenciaFixaMedia"
					class="itemDetalhamento itemDetalhamentoLargo">
					<fmt:formatNumber pattern="#,###.00" minIntegerDigits="1" value="${equipamento.potenciaFixaMedia}"/> kCal/h </span>
				<label class="rotulo" id="rotuloVazaoBaixa" for="potenciaFixaBaixa">Pot�ncia
					Baixa:</label>
				<span id="potenciaFixaBaixa"
					class="itemDetalhamento itemDetalhamentoLargo">
					<fmt:formatNumber pattern="#,###.00" minIntegerDigits="1" value="${equipamento.potenciaFixaBaixa}"/> kCal/h </span>
			</c:if>

		</fieldset>
	</fieldset>

	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Voltar"
			type="button" onclick="voltar();">
			<input name="button" class="bottonRightCol2 botaoGrande1"
				value="Alterar" type="button" onclick="alterar();">
	</fieldset>
</form:form>