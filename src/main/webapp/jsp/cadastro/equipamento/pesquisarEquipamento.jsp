<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Equipamento</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarEquipamentos" id="equipamentoForm" name="equipamentoForm"> 

	<script language="javascript">
	$(document).ready(function(){
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
	});
	
	function alterarFuncionario(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("equipamentoForm", "pesquisarEquipamentos");
	}
	
	function limparFormulario(){
		document.equipamentoForm.descricao.value = "";
		document.equipamentoForm.idSegmento.value = "-1";
	    document.forms[0].habilitado[0].checked = true;
	}
	
	function incluir() {		
		location.href = '<c:url value="/exibirInserirEquipamento"/>';
	}
	
	function removerEquipamento(){
		
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('equipamentoForm', 'removerEquipamento');
			}
	    }
	}
	
	function alterarEquipamento(){
		
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('equipamentoForm', 'exibirAtualizarEquipamento');
	    }
	}
	
	function detalharEquipamento(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("equipamentoForm", 'exibirDetalhamentoEquipamento');
	}
	  
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarEquipamentos">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="equipamentoCol1" class="coluna">
			<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="30" size="20" value="${equipamento.descricao}" onkeypress="return formatarCampoAlfaNumericoSemCaracteresEspeciais(event)"
			onkeyup="return validarCriteriosParaCampo(this, '1', '1', 'formatarCampoNome(event)');" ><br />
			
			<label class="rotulo" for="idLocalidade" >Segmento:</label>
			<select class="campoSelect" name="segmento" id="idSegmento">
		    <option value="-1">Selecione</option>
			<c:forEach items="${listaSegmento}" var="segmento">
				<option value="${segmento.chavePrimaria}" ${equipamento.segmento.chavePrimaria == segmento.chavePrimaria ? 'selected="selected"': ''}>
					${segmento.descricao}
				</option>
		    </c:forEach>	
		    </select>
		</fieldset>
		
		<fieldset id="equipamentoCol2" class="colunaFinal">
		    <label class="rotulo" for="habilitado">Indicador de Uso:</label>
		    <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDir">
	    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" onclick="submeter('equipamentoForm', 'pesquisarEquipamentos');">	
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
		
	<c:if test="${equipamentos ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="equipamentos" sort="list" id="equipamento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarEquipamentos">
		      <display:column media="html" style="text-align: center;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		  <input type="checkbox" name="chavesPrimarias" value="${equipamento.chavePrimaria}">
	     	  </display:column>
	     	  <display:column title="Ativo">
		     	<c:choose>
					<c:when test="${equipamento.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
		      <display:column sortProperty="descricao" sortable="true" titleKey="EQUIPAMENTO_DESCRICAO" >
		      	<a href='javascript:detalharEquipamento(<c:out value='${equipamento.chavePrimaria}'/>);'>
						<c:out value='${equipamento.descricao}'/>
					</a>
		      </display:column> 
	         <display:column property="segmento.descricao" sortable="true" titleKey="EQUIPAMENTO_SEGMENTO" />
		</display:table>		
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty equipamentos}">
				<input id="botaoAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarEquipamento()" type="button">
				<input id="botaoRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerEquipamento()" type="button">
		</c:if>

		<input id="botaoIncluir" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir();" type="button">
		
	</fieldset>
	
</form:form>