<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<fmt:setLocale value="pt-BR"/>

<h1 class="tituloInterno">Incluir Equipamento</h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>
 
<form:form method="post" action="inserirEquipamento" id="equipamentoForm" name="equipamentoForm" ModelAttribute="EquipamentoImpl">

<script language="javascript">

	$().ready(function(){
		alterarOpcaoPotencia("#idSegmento");
	});
	
	function verificarTamanho( obj ){
		if( obj.value.length > 8){
			 obj.value = "";
			 alert( "Quantidade de d�gitos � maior que o permitido.")
		}	
	}
	
	function verificarDrop( obj ){	
		alert( "N�o � permitido arrastar e soltar.")
		obj.value = "";	
	}
	
	function limparFormulario() {	
		document.equipamentoForm.descricao.value = "";
		document.equipamentoForm.idSegmento.value = "-1";
		document.equipamentoForm.potenciaFixaAlta.value = "";
		document.equipamentoForm.potenciaFixaMedia.value = "";
		document.equipamentoForm.potenciaFixaBaixa.value = "";
		document.equipamentoForm.potenciaPadrao.value = "";
		$("#potenciaFixaAlta").val("").attr("disabled","disabled").addClass("campoDesabilitado").attr("style","");
		$("#potenciaFixaMedia").val("").attr("disabled","disabled").addClass("campoDesabilitado").attr("style","");
		$("#potenciaFixaBaixa").val("").attr("disabled","disabled").addClass("campoDesabilitado").attr("style","");
		$("#potenciaPadrao").val("").attr("disabled","disabled").addClass("campoDesabilitado").attr("style","");
	}
	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaEquipamento"/>';
	}
	
	function alterarOpcaoPotencia(){

		var idSegmento = $("#idSegmento option:selected").val();
		$.get("equipamento/opcoes?idSegmento=" + idSegmento)
			.done(function(json) {
				
				var segmentoResidencial = json.indicadorEquipamentoPotenciaFixa;
				if(segmentoResidencial == "true") {
	   				$("#potenciaFixaAlta").removeAttr("disabled").removeClass("campoDesabilitado");
	   				$("#potenciaFixaMedia").removeAttr("disabled").removeClass("campoDesabilitado");
	   				$("#potenciaFixaBaixa").removeAttr("disabled").removeClass("campoDesabilitado");
	   				$("#potenciaPadrao").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   			}
	   			else {
	   				$("#potenciaFixaAlta").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   				$("#potenciaFixaMedia").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   				$("#potenciaFixaBaixa").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   				$("#potenciaPadrao").removeAttr("disabled").removeClass("campoDesabilitado");
	   			}
					
			})
	}

addLoadEvent(alterarOpcaoPotencia);	

</script>

<input name="acao" type="hidden" id="acao" value="inserirEquipamento">

<fieldset class="conteinerPesquisarIncluirAlterar">
	
	<fieldset id="equipamentoCol1" class="coluna">
		<legend class="conteinerBlocoTitulo">Dados do Equipamento</legend>
		<label class="rotulo campoObrigatorio" id="rotuloDescricao" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
		<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="50" size="40" value="${equipamento.descricao}" onkeyup="return validarCriteriosParaCampo(this, '1', '1', 'formatarCampoNome(event)');"><br />
		<label class="rotulo campoObrigatorio" for="idLocalidade" ><span class="campoObrigatorioSimbolo">* </span>Segmento:</label>
		<select class="campoSelect" name="segmento" id="idSegmento" onchange="alterarOpcaoPotencia(this);">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaSegmento}" var="segmento">
				<option value="${segmento.chavePrimaria}" ${equipamento.segmento.chavePrimaria == segmento.chavePrimaria ? 'selected="selected"': ''}>
					${segmento.descricao}
				</option>
		    </c:forEach>	
	    </select>
		
		<label class="rotulo campoObrigatorio2" id="labelPotenciaPadrao" for="potenciaPadrao"><span class="campoObrigatorioSimbolo2">* </span>Pot�ncia Padr�o:</label>
		<input class="campoTexto campoDesabilitado" type="text" id="potenciaPadrao" name="potenciaPadrao" maxlength="10" size="10"
		 value="${equipamento.potenciaPadrao}" onkeypress="return formatarCampoDecimalPositivo(event, this, 4, 2);" 
		 onblur="aplicarMascaraNumeroDecimal(this, 2); verificarTamanho(this)" disabled="disabled" ondrop="verificarDrop(this)"/>
		<label class="rotuloHorizontal rotuloInformativo" for="potenciaPadrao">kCal/h</label>
	</fieldset>
	
	<fieldset id="equipamentoCol2">
		<legend class="conteinerBlocoTitulo">Valores de Pot�ncia Fixos</legend>
		<label class="rotulo campoObrigatorio2" id="labelpotenciaFixaAlta" for="potenciaFixaAlta"><span class="campoObrigatorioSimbolo2">* </span>Pot�ncia Alta:</label>
		<input class="campoTexto campoDesabilitado" type="text" id="potenciaFixaAlta" name="potenciaFixaAlta" maxlength="10" size="10" value="${potenciaFixaAlta}" onkeypress="return formatarCampoDecimalPositivo(event, this, 4, 2);" 
			onblur="aplicarMascaraNumeroDecimal(this, 2); verificarTamanho(this)" disabled="disabled" ondrop="verificarDrop(this)"/>
		<label class="rotuloHorizontal rotuloInformativo" for="potenciaFixaAlta">kCal/h</label>
		<label class="rotulo campoObrigatorio2" id="labelpotenciaFixaMedia" for="potenciaFixaMedia"><span class="campoObrigatorioSimbolo2">* </span>Pot�ncia Media:</label>
		<input class="campoTexto campoDesabilitado" type="text" id="potenciaFixaMedia" name="potenciaFixaMedia" maxlength="10" size="10" value="${potenciaFixaMedia}" onkeypress="return formatarCampoDecimalPositivo(event, this, 4, 2);"
			onblur="aplicarMascaraNumeroDecimal(this, 2); verificarTamanho(this)" disabled="disabled" ondrop="verificarDrop(this)"/>
		<label class="rotuloHorizontal rotuloInformativo" for="potenciaFixaMedia">kCal/h</label>
		<label class="rotulo campoObrigatorio2" id="labelpotenciaFixaBaixa" for="potenciaFixaBaixa"><span class="campoObrigatorioSimbolo2">* </span>Pot�ncia Baixa:</label>
		<input class="campoTexto campoDesabilitado" type="text" id="potenciaFixaBaixa" name="potenciaFixaBaixa" maxlength="10" size="10" value="${potenciaFixaBaixa}" onkeypress="return formatarCampoDecimalPositivo(event, this, 4, 2);" 
			onblur="aplicarMascaraNumeroDecimal(this, 2); verificarTamanho(this)" disabled="disabled" ondrop="verificarDrop(this)"/>
		<label class="rotuloHorizontal rotuloInformativo" for="potenciaFixaBaixa">kCal/h</label>
		
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o do equipamento.</p>
	<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios para inclus�o do equipamento quando habilitados.</p>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input id="botaoCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input id="botaoLimpar" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <input id="botaoSalvar" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button">
</fieldset>

<script>

$("#botaoSalvar").click(function(event){
	
	event.preventDefault();
	
	$("#potenciaPadrao").val( $("#potenciaPadrao").val().replace(".","").replace(",",".") );
	$("#potenciaFixaAlta").val( $("#potenciaFixaAlta").val().replace(".","").replace(",",".") );
	$("#potenciaFixaMedia").val( $("#potenciaFixaMedia").val().replace(".","").replace(",",".") );
	$("#potenciaFixaBaixa").val( $("#potenciaFixaBaixa").val().replace(".","").replace(",",".") );
	
	$("#equipamentoForm").submit();
})

</script>

</form:form> 