<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
 

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>


<h1 class="tituloInterno">Pesquisar Unidade Federativa<a href="<help:help>/consultasdasunidadesfederativas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. 
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarUnidadeFederacao" name="unidadeFederacaoForm">
    
    <script language="javascript">

    $(document).ready(function(){

    var max = 0;

      $('.rotulo').each(function(){
              if ($(this).width() > max)
                 max = $(this).width();   
          });
      $('.rotulo').width(max);
        
    });

    function removerUnidadeFederacao(){
    	 var selecao = verificarSelecao();
         if (selecao == true) {  
             var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
             if(retorno == true) {
                submeter('unidadeFederacaoForm', 'removerUnidadeFederacao');
            }
        }
    }

    
    function alterarUnidadeFederacao(){
        
        var selecao = verificarSelecaoApenasUm();
        if (selecao == true) {  
            document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
            
            submeter('unidadeFederacaoForm', 'exibirAlterarUnidadeFederacao');
        }
    }
    
    function detalharUnidadeFederacao(chave){

        document.forms[0].chavePrimaria.value = chave;
        submeter('unidadeFederacaoForm', 'exibirDetalharUnidadeFederacao');
        
    }
    
    function incluirUnidadeFederacao() {
        
        submeter('unidadeFederacaoForm', 'exibirIncluirUnidadeFederacao');
    }

    
    function limparFormulario(){
        
       document.getElementById('descricao').value = "";
       document.getElementById('sigla').value = "";
       document.getElementById('codigoIBGE').value = "";
       document.getElementById('idPais').value = "-1";
       document.forms[0].habilitado[0].checked = true;
    
    }

    </script>
    
    <fieldset class="conteinerPesquisarIncluirAlterar">
    	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
             <label class="rotulo" id="rotuloDescricao" for="descricao">Descri��o:</label>
             <input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${unidadeFederacao.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/><br />
            
            <label class="rotulo">Pa�s:</label>
            <select name="pais" class="campoSelect" id="idPais" >
                <option value="-1">Selecione</option>
                <c:forEach items="${listaPaises}" var="pais">
                    <option value="<c:out value="${pais.chavePrimaria}"/>" <c:if test="${unidadeFederacao.pais.chavePrimaria == pais.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${pais.nome}"/>
                    </option>       
                </c:forEach>    
            </select><br /><br />
            
        </fieldset>
        
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
        	<label class="rotulo">Sigla:</label>
            <input class="campoTexto campoHorizontal" type="text" id="sigla" name="sigla" maxlength="2" value="${unidadeFederacao.sigla}" onkeyup="verificaApenasLetras (this)"> <br />
            
            <label class="rotulo">C�digo IBGE:</label>
            <input class="campoTexto campoHorizontal" type="text" name="unidadeFederativaIBGE" id="codigoIBGE" maxlength="2" value="${unidadeFederacao.unidadeFederativaIBGE}" onkeypress="return formatarCampoInteiro(event)"> <br />
            
            <label class="rotulo" for="habilitado">Indicador de Uso:</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorUso">Ativo</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorUso">Inativo</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
                <label class="rotuloRadio" for="habilitado">Todos</label><br />
           
           <br /><br />
                <fieldset class="conteinerBotoesPesquisarDirFixo">
                    <vacess:vacess param="exibirPesquisaUnidadeFederacao">
                        <input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
                    </vacess:vacess>
                    <input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="javascript:limparFormulario();">
                </fieldset>
        </fieldset>
            
        </fieldset>
        
       <c:if test="${listaUnidadeFederacao ne null}">
       <hr class="linhaSeparadoraPesquisa" />
             <display:table class="dataTableGGAS" name="listaUnidadeFederacao" sort="list" id="unidadeFederacao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarUnidadeFederacao">
                    <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
                        <input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${unidadeFederacao.chavePrimaria}">
                    </display:column>
                    
                    <display:column title="Ativo" style="width: 30px">
                        <c:choose>
                            <c:when test="${unidadeFederacao.habilitado == true}">
                                <img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
                            </c:when>
                            <c:otherwise>
                                <img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
                            </c:otherwise>
                        </c:choose>
                    </display:column>
                
                <display:column sortable="true" sortProperty="descricao" title="Descri��o">
                     <a href="javascript:detalharUnidadeFederacao(<c:out value='${unidadeFederacao.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
                        <c:out value="${unidadeFederacao.descricao}"/>
                    </a>
                </display:column>
                
            </display:table>
        </c:if>
    
    <fieldset class="conteinerBotoes">
        <c:if test="${not empty listaUnidadeFederacao}">
            <vacess:vacess param="exibirAlterarUnidadeFederacao">
                <input name="buttonAlterar" id="botaoAlterar" value="Alterar" class="bottonRightCol2  botaoAlterar" onclick="javascript:alterarUnidadeFederacao();" type="button">
            </vacess:vacess>
            <vacess:vacess param="removerUnidadeFederacao">
                <input name="buttonRemover" id="botaoRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="javascript:removerUnidadeFederacao();" type="button">
            </vacess:vacess>
        </c:if>
        
        <vacess:vacess param="exibirIncluirUnidadeFederacao">
            <input id="buttonIncluir" name="buttonIncluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Incluir" onclick="javascript:incluirUnidadeFederacao();" type="button">
        </vacess:vacess>
    
    </fieldset>
</form:form>
