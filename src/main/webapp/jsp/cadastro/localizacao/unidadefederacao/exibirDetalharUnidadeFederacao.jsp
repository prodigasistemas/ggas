<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Detalhar Unidade Federativa<a href="<help:help>/cadastrodeunidadedafederaodetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>
		$(document).ready(function(){
			  var max = 0;
		
		    $('.rotulo').each(function(){
		            if ($(this).width() > max)
		               max = $(this).width();   
		        });
		    $('.rotulo').width(max);
			
		});


		function voltar(){	
			location.href = '<c:url value="/exibirPesquisaUnidadeFederacao"/>';
		}
		
		function alterar(){
			submeter('unidadeFederacaoForm', 'exibirAlterarUnidadeFederacao');
		}


</script>

<form:form method="post" enctype="multipart/form-data" name="unidadeFederacaoForm">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${unidadeFederacao.chavePrimaria}">
<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">

			<label class="rotulo">Descri��o:</label>
			<span class="itemDetalhamento"><c:out value="${unidadeFederacao.descricao}"/></span>
			<br /><br />
			<label class="rotulo">Sigla:</label>
		    <span class="itemDetalhamento"><c:out value="${unidadeFederacao.sigla}"/></span>
		
		</fieldset>
	
		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">

			<label class="rotulo">Pa�s:</label>
			<span class="itemDetalhamento"><c:out value="${unidadeFederacao.pais.nome}"/></span><br />
		    
		    <label class="rotulo">C�digo IBGE:</label>
		    <span class="itemDetalhamento"><c:out value="${unidadeFederacao.unidadeFederativaIBGE}"/></span><br />

		</fieldset>
</fieldset>

	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    
    <vacess:vacess param="exibirAlterarUnidadeFederacao">    
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
   
</fieldset>
</form:form>
