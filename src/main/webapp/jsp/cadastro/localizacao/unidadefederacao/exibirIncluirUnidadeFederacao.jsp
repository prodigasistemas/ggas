<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Incluir Unidade Federativa<a href="<help:help>/cadastrodaunidadedafederaoinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script language="javascript">

    $(document).ready(function(){
         var max = 0;

          $('.rotulo').each(function(){
                  if ($(this).width() > max)
                     max = $(this).width();   
              });
          $('.rotulo').width(max);
         
        });


    function salvar(){

        submeter('unidadeFederacaoForm', 'incluirUnidadeFederacao');
    }

    function cancelar(){   
    	location.href = '<c:url value="/exibirPesquisaUnidadeFederacao"/>';
    }

    function limparCampos(){
            document.getElementById('descricao').value = "";
            document.getElementById('sigla').value = "";
            document.getElementById('codigoIBGE').value = "";
            document.getElementById('idPais').value = "-1";
        }

</script>

<form:form method="post" enctype="multipart/form-data" action="incluirUnidadeFederacao" name="unidadeFederacaoForm">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="habilitado" type="hidden" id="habilitado" value="${true}">

<fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
        	<input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${unidadeFederacao.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        	<br />
            
              <label class="rotulo">Sigla:</label>
            <input class="campoTexto campoHorizontal" type="text" id="sigla" name="sigla" maxlength="2" value="${unidadeFederacao.sigla}" onkeyup="verificaApenasLetras (this),letraMaiuscula(this);"> <br />
        
		</fieldset>
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
        	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Pa�s:</label>
            <select name="pais" class="campoSelect" id="idPais">
                <option value="-1">Selecione</option>
                <c:forEach items="${listaPaises}" var="pais">
                    <option value="<c:out value="${pais.chavePrimaria}"/>"<c:if test="${unidadeFederacao.pais.chavePrimaria == pais.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${pais.nome}"/>
                    </option>       
                </c:forEach>    
            </select><br />
            
            <label class="rotulo">C�digo IBGE:</label>
            <input class="campoTexto campoHorizontal" type="text" id="codigoIBGE" name="unidadeFederativaIBGE" maxlength="2" value="${unidadeFederacao.unidadeFederativaIBGE}" onkeypress="return formatarCampoInteiro(event)"> <br /><br />
                    	
        </fieldset>
		
   	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
</fieldset>	
	<fieldset class="conteinerBotoes">
	    
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="javascript:limparCampos();">
	    <input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" id="botaoSalvar" onclick="javascript:salvar();">
	   
	</fieldset>
</form:form>
