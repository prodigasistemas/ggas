<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Alterar Microrregião<a href="<help:help>/cadastrodemicrorregioinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>

    $(document).ready(function(){
        var max = 0;
  
        $('.rotulo').each(function(){
                if ($(this).width() > max)
                   max = $(this).width();   
            });
        $('.rotulo').width(max);
    });
    
    function salvar(){
        submeter('microrregiaoForm', 'alterarMicrorregiao');
    }

    function cancelar(){    
    	location.href='exibirPesquisaMicrorregiao';
    }

    function limparCampos(){
    	$(":text").val("");
    	$("select").val("-1");
    	document.microrregiaoForm.habilitado[0].checked = true;
    }
        
</script>

<form:form method="post" enctype="multipart/form-data" action="alterarMicrorregiao" id="microrregiaoForm" name="microrregiaoForm">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="tela" type="hidden" id="tela" value="microrregiao">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${microrregiao.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${microrregiao.versao}"> 

<fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
	        <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descrição:</label>
	        <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${microrregiao.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
	        <br />
    </fieldset>
    
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal colunaFinalAjuste">
        	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Região:</label>
        <select name="regiao" class="campoSelect" id="idRegiao">
            <option value="-1">Selecione</option>
            <c:forEach items="${listaRegiao}" var="regiao">
                <option value="<c:out value="${regiao.chavePrimaria}"/>" <c:if test="${microrregiao.regiao.chavePrimaria == regiao.chavePrimaria}">selected="selected"</c:if>>
                    <c:out value="${regiao.descricao}"/>
                </option>       
            </c:forEach>    
        </select>
        <label class="rotulo" for="habilitado">Indicador de Uso:</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${microrregiao.habilitado eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Ativo</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${microrregiao.habilitado eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Inativo</label><br />
        </fieldset>
    
    <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigatórios</p>
</fieldset>
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="javascript:limparCampos();">
    
    <vacess:vacess param="alterarMicrorregiao">
        <input name="button" id="botaoSalvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="salvar();">
    </vacess:vacess>
</fieldset>
</form:form>
