<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



<h1 class="tituloInterno">Pesquisar Transportadora<a href="<help:help>/consultadasempresas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>


	<script type="text/javascript">
	
		$(document).ready(function() {
			
			limparFormulario();

		});

		function exibirPopupPesquisaCliente() {
			popup = window
					.open(
							'exibirPesquisaClientePopup?pessoaJuridica=true',
							'popup',
							'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
		}

		function exibirAlterarTransportadora() {
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('transportadoraForm', 'exibirAlteracaoTransportadora');
			}

		}

		function limparFormulario(){
			document.forms[0].placaVeiculo.value = '';
			document.forms[0].nomeMotorista.value = '';
		}
		
		function pesquisar() {
			submeter('transportadoraForm', 'pesquisarTransportadora');
		}

		function incluir() {
			submeter('transportadoraForm', 'exibirInclusaoTransportadora');
		}

		function removerTransportadora() {

			var selecao = verificarSelecao();

			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if (retorno == true) {
					submeter('transportadoraForm', 'removerTransportadora');
				}
			}
		}

		function detalharTransportadora(valorChave) {
			document.forms[0].chavePrimaria.value = valorChave;
			submeter('transportadoraForm', 'exibirDetalharTransportadora');
		}

		var popup;
		function exibirPopupPesquisaCliente() {
			popup = window
					.open(
							'exibirPesquisaClientePopup?pessoaJuridica=true',
							'popup',
							'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
		}

		function selecionarCliente(idSelecionado) {
			var idCliente = document.getElementById("idCliente");
			var nomeCompletoCliente = document
					.getElementById("nomeCompletoCliente");
			var cnpjFormatado = document.getElementById("cnpjFormatado");
			var emailCliente = document.getElementById("emailCliente");
			var enderecoFormatado = document
					.getElementById("enderecoFormatado");

			if (idSelecionado != '') {
				AjaxService
						.obterClientePorChave(
								idSelecionado,
								{
									callback : function(cliente) {
										if (cliente != null) {
											idCliente.value = cliente["chavePrimaria"];
											nomeCompletoCliente.value = cliente["nome"];
											if (cliente["cnpj"] != undefined) {
												cnpjFormatado.value = cliente["cnpj"];
											} else {
												cnpjFormatado.value = "";
											}
											emailCliente.value = cliente["email"];
											enderecoFormatado.value = cliente["enderecoFormatado"];
										}
									},
									async : false
								}

						);
			} else {
				idCliente.value = "";
				nomeCompletoCliente.value = "";
				cnpjFormatado.value = "";
				emailCliente.value = "";
				enderecoFormatado.value = "";
			}

			document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
			document.getElementById("cnpjFormatadoTexto").value = cnpjFormatado.value;
			document.getElementById("emailClienteTexto").value = emailCliente.value;
			document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
		}
	</script>

<form:form method="post" action="pesquisarTransportadora" id="transportadoraForm" name="transportadoraForm" modelAttribute="TransportadoraVO">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
	<fieldset id="pesquisarTransportadora" class="coluna2">
	
			
	<fieldset id="pesquisarCliente" class="colunaEsq">
		<legend>Pesquisar Pessoa Jur�dica</legend>
		<div class="pesquisarClienteFundo">			
			<input name="idCliente" type="hidden" id="idCliente" value="${empresaForm.map.idCliente}">
			<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${empresaForm.map.nomeCompletoCliente}">
			<input name="cnpjFormatado" type="hidden" id="cnpjFormatado" value="${empresaForm.map.cnpjFormatado}">
			<input name="enderecoFormatado" type="hidden" id="enderecoFormatado" value="${empresaForm.map.enderecoFormatado}">
			<input name="emailCliente" type="hidden" id="emailCliente" value="${empresaForm.map.emailCliente}">
			
			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Pessoa Jur�dica</span> para selecionar a Pessoa Jur�dica Respons�vel.</p>
			<input name="Button" id="botaoPesquisarClientePesssoaJuridica" class="bottonRightCol2" title="Pesquisar Pessoa Jur�dica"  value="Pesquisar Pessoa Jur�dica" onclick="exibirPopupPesquisaCliente();" type="button"><br >
			<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloCliente" for="nomeClienteTexto">Pessoa Jur�dica:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" size="50" disabled="disabled" value="${empresaForm.map.nomeCompletoCliente}"><br />
			<label class="rotulo campoObrigatorio" id="rotuloCnpjTexto" for="cnpjFormatadoTexto">CNPJ:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="cnpjFormatadoTexto" name="cnpjFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${empresaForm.map.cnpjFormatado}"><br />	
			<label class="rotulo campoObrigatorio" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto">Endere�o:</label>
			<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoTexto" rows="2" cols="37" disabled="disabled">${empresaForm.map.enderecoFormatado}</textarea><br />
			<label class="rotulo" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${empresaForm.map.emailCliente}"><br />
		</div>
	</fieldset>
			
			
</fieldset>
																		
  <fieldset id="pesquisarQuestionario2" class="colunaDir" style="margin-top:20px">			
			
<!-- 			<label class="rotulo" id="rotuloNomeMotorista" for="nomeMotorista">Nome Motorista:</label> -->
<%-- 			<input class="campoTexto" type="text" id="nomeMotorista" onkeyup="letraMaiuscula(this);" maxlength="30" size="30" value="${transportadoraVO.nomeMotorista}"><br /> --%>
			
<!-- 			<label class="rotulo" id="rotuloComplemento" for="placaVeiculo">Placa Ve�culo:</label> -->
<%-- 			<input class="campoTexto" type="text" id="placaVeiculo" onkeyup="letraMaiuscula(this);" maxlength="9" size="9" value="${transportadoraVO.placaVeiculo}"><br /> --%>
			
			
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${transportadoraVO.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${transportadoraVO.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoTodos" value="" checked="checked" <c:if test="${transportadoraVO.habilitado eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>

		
		 </fieldset>	


<fieldset class="conteinerBotoesPesquisarDirFixo">
	   		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisar();">
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormularios(this.form);">		
		</fieldset>
	     
	     <c:if test="${not empty listaTransportadoras}">
  <hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaTransportadoras"
			sort="list" id="transportadora"
			decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="pesquisarTransportadora">

			<display:column style="width: 25px" sortable="false"
				title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> <input type="checkbox" name="chavesPrimarias"
					value="${transportadora.chavePrimaria}">
			</display:column>
			<display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${transportadora.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
			<display:column sortable="true" title="Nome Transportadora" style="text-align: center;">
			<a href="javascript:detalharTransportadora(<c:out value='${transportadora.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value='${transportadora.cliente.nome}' />
				 </a>
			</display:column>
			
			<display:column sortable="true" title="CNPJ" style="text-align: center;">
	        	<a href="javascript:detalharTransportadora(<c:out value='${transportadora.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value='${transportadora.cliente.cnpj}' />
	            </a>
	        </display:column>

		</display:table>
</c:if>

		<hr class="linhaSeparadoraPesquisa" />

	<fieldset class="conteinerBotoesPesquisarDir">
<%-- 		<vacess:vacess param="exibirInclusaoQuestionario"> --%>
			<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
				value="Incluir" type="button" onclick="incluir()">
<%-- 		</vacess:vacess> --%>
	</fieldset>
	<c:if test="${not empty listaTransportadoras}">

		<fieldset class="conteinerBotoesPesquisarEsq">
<%-- 			<vacess:vacess param="exibirAlteracaoQuestionario"> --%>
				<input name="Button" class="bottonRightCol2" id="botaoAlterar"
					value="Alterar" type="button" onclick="exibirAlterarTransportadora()">
<%-- 			</vacess:vacess> --%>
<%-- 			<vacess:vacess param="removerQuestionario"> --%>
				<input name="Button" class="bottonRightCol2" id="botaoRemover"
					value="Remover" type="button" onclick="removerTransportadora()">
<%-- 			</vacess:vacess> --%>
		</fieldset>


	</c:if>
	</fieldset>
</form:form>
