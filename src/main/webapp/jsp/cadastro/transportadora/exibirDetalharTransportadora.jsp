<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Detalhar Transportadora</h1>
 <p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script type="text/javascript">

function exibirAlterarTransportadora(){
	submeter('formTransportadora','exibirAlteracaoTransportadora');
}

function cancelar(){
	submeter('formTransportadora','exibirPesquisaTransportadora');
}	
</script>


<form:form method="post" id="formTransportadora" name="formTransportadora" modelAttribute="Transportadora">

<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${transportadora.chavePrimaria}">

<fieldset class="conteinerPesquisarIncluirAlterar" disabled="disabled">

 
 <fieldset id="pesquisarTransportadora" class="coluna2">
	     	<label class="rotulo"  id="rotuloDescricaoQuesqionario" for="nomeTransportadora" > <span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Nome da Transportadora:</label>
	     	<input type="text" id="nomeTransportadora" name="nomeTransportadora" class="compoTexto" value="${transportadora.cliente.nome}" size="50" maxlength="250" style="margin-top: 10px"><br />
	     	
	     	<label class="rotulo"  id="rotuloDescricaoQuesqionario" for="cnpjTransportadora" > <span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>CNPJ da Transportadora:</label>
	     	<input type="text" id="cnpjTransportadora" name="cnpjTransportadora" class="compoTexto" value="${transportadora.cliente.cnpj}" size="50" maxlength="250" style="margin-top: 18px"><br />
            
	     </fieldset>
	     <br> <br><br>
	     
<%-- 		<c:if test="${!empty listaPerguntas}"> --%>
	       <fieldset id="gridPerguntasQuestionario" class="conteinerBloco" disabled="disabled"  style="width: 95.8%">
	       		<jsp:include page="/jsp/cadastro/transportadora/gridVeiculos.jsp"></jsp:include>
	       </fieldset>
	       
	        <fieldset id="gridPerguntasQuestionario" class="conteinerBloco" disabled="disabled"  style="width: 95.8%">
	       		<jsp:include page="/jsp/cadastro/transportadora/gridMotoristas.jsp"></jsp:include>
	       </fieldset>
<%-- 	   </c:if> --%>
	      
	      
	      <hr class="linhaSeparadoraPesquisa" />
	      
	 
</fieldset>
<fieldset  class="conteinerBotoesPesquisarEsq">
	<input name="Button" class="bottonLeftRotulo" id="botaoCancelar" value="Voltar" type="button" onclick="cancelar()">
</fieldset>
<fieldset class="conteinerBotoesPesquisarDir" >
<vacess:vacess param="exibirAlteracaoTransportadora">
	<input name="Button" class="bottonRightCol2" id="botaoAlterar" value="Alterar" type="button" onclick="exibirAlterarTransportadora()">
	</vacess:vacess>      	
</fieldset>


</form:form>