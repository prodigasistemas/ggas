<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 
<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:set var="indexVeiculo" value="0" />
 <display:table class="dataTableGGAS" sort="list" name="sessionScope.listaVeiculos" id="veiculo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

				<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${veiculo.habilitado}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	          <display:column sortable="false"  title="Placa Ve�culo" style="text-align: center;" maxLength="50" >
	            	<c:out value="${veiculo.placa}"/>
	        </display:column>
	        <display:column title="UF" sortable="false" style="text-align: center;  width: 30px">
	            	<c:out value="${veiculo.uf.descricao}"/>
	        </display:column>
	        <c:if test="${acao eq null || acao ne 'detalhar'}" >
	          <display:column sortable="false" style="text-align: center; ">
	           	<a href="javascript:carregarParaAlterarVeiculo('${indexVeiculo}','${veiculo.placa}','${veiculo.uf.chavePrimaria}','${veiculo.habilitado}');"><span class="linkInvisivel"></span>
	            	<img title="Alterar Ve�culo" alt="Alterar Ve�culo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0">
	            	</a>
	        </display:column>
	          <display:column sortable="false" style="text-align: center;  ">
	           	<a onclick="return confirm('Deseja excluir o Ve�culo?');" href="javascript:excluirVeiculo(<c:out value='${indexVeiculo}'/>);"><span class="linkInvisivel"></span>
	            	<img title="Excluir Ve�culo" alt="Excluir Ve�culo"  src="<c:url value="/imagens/deletar_x.png"/>" border="0">
	            </a>
	        </display:column>
	        </c:if>
	        <c:set var="indexVeiculo" value="${indexVeiculo+1}" />
</display:table>

