<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>


 	<c:if test="${ acao eq 'alterar' }">
 		<h1 class="tituloInterno">Alterar Transportadora</h1>
 		<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>
 	</c:if>
 	
 	<c:if test="${ acao eq 'incluir' }">
 		<h1 class="tituloInterno">Incluir Transportadora</h1>
 		<p class="orientacaoInicial">Informe os dados abaixo e clice em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar</p>
 		
 	</c:if>

<script type="text/javascript">
$(document).ready(function(){
	 $("#cpf").inputmask("999.999.999-99",{placeholder:"_"});
});
	
function adicionarMotorista(){
	var chavePrimaria = document.getElementById('chavePrimaria').value;
	var nomeMotorista = document.getElementById("nomeMotorista").value;
	
	var cpf = document.getElementById("cpf").value;
	var indexList = document.forms[0].indexList.value;
	
	
	var habilitado = true;
	var acao = "<c:out value="${acao}" />"
	if(acao != 'incluir'){
		if(!document.forms[0].habilitado3[0].checked){
			habilitado = false;
		}
	}

	
	var url = "carregarAdicionarMotorista?chavePrimariaTransportadora="+chavePrimaria
			+"&"+"nomeMotorista="+nomeMotorista
			+"&"+"cpf="+cpf
			+"&"+"indexList="+indexList
			+"&"+"habilitado="+habilitado;
	 var isSucesso = carregarFragmento('gridMotoristas',url);
	 
	 if(isSucesso){ 
		document.getElementById("nomeMotorista").value ='';
		document.getElementById("cpf").value ='';
	 	document.getElementById("indexList").value = null;
	 	document.getElementById("botaoAlterarMotorista").disabled=true;
    	document.getElementById("botaoAdicionarMotorista").disabled=false;
	 }	 
}

function adicionarVeiculos(){
	var chavePrimaria = document.getElementById('chavePrimaria').value;
	var placaVeiculo = document.getElementById("placaVeiculo").value;
	
	var idOrgaoEmissorUF = document.getElementById("idOrgaoEmissorUF").value;
	var indexList = document.forms[0].indexList.value;
	
	
	var habilitado = true;
	var acao = "<c:out value="${acao}" />"
	if(acao != 'incluir'){
		if(!document.forms[0].habilitado3[0].checked){
			habilitado = false;
		}
	}

	
	var url = "carregarAdicionarVeiculo?chavePrimariaTransportadora="+chavePrimaria
			+"&"+"placaVeiculo="+placaVeiculo
			+"&"+"idOrgaoEmissorUF="+idOrgaoEmissorUF
			+"&"+"indexList="+indexList
			+"&"+"habilitado="+habilitado;
	 var isSucesso = carregarFragmento('gridVeiculos',url);
	 
	 if(isSucesso){ 
		document.getElementById("placaVeiculo").value ='';
		document.getElementById("idOrgaoEmissorUF").value ='';
	 	document.getElementById("indexList").value = null;
	 	document.getElementById("botaoAlterarVeiculo").disabled=true;
    	document.getElementById("botaoAdicionarVeiculo").disabled=false;
	 }	 
}

function desalibitarHabilitarCamposNotas(valor){
	document.getElementById("notaMinima").value = '';
	document.getElementById("notaMaxima").value = '';
	document.getElementById("notaMinima").disabled = valor;
	document.getElementById("notaMaxima").disabled = valor;
}

function salvar(){
	
	 submeter('formTransportadora','incluirTransportadora');
}

function alterarTransportadora(){
	submeter('formTransportadora','alterarTransportadora');
}

function habilidarDesabilidarCampoIndicadorUso(valor){
	if(document.forms[0].chavePrimaria.value!=0) {
		document.forms[0].habilitado3[0].disabled = valor;
		document.forms[0].habilitado3[1].disabled = valor;
	}
}

function selecionarCliente(idSelecionado){
	var idCliente = document.getElementById("idCliente");
	var nomeCompletoCliente = document.getElementById("nomeCompletoCliente");
	var cnpjFormatado = document.getElementById("cnpjFormatado");
	var emailCliente = document.getElementById("emailCliente");
	var enderecoFormatado = document.getElementById("enderecoFormatado");		
	
	if(idSelecionado != '') {				
		AjaxService.obterClientePorChave( idSelecionado, {
           	callback: function(cliente) {	           		
           		if(cliente != null){  	           			        		      		         		
	               	idCliente.value = cliente["chavePrimaria"];
	               	nomeCompletoCliente.value = cliente["nome"];		               	
	               	if(cliente["cnpj"] != undefined ){
	               		cnpjFormatado.value = cliente["cnpj"];
	               	} else {
		               	cnpjFormatado.value = "";			               	
	               	}
	               	emailCliente.value = cliente["email"];
	               	enderecoFormatado.value = cliente["enderecoFormatado"];
               	}
        	}, async:false}
        	
        );	        
    } else {
   		idCliente.value = "";
    	nomeCompletoCliente.value = "";
    	cnpjFormatado.value = "";
    	emailCliente.value = "";
    	enderecoFormatado.value = "";
   	}
    
    document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
    document.getElementById("cnpjFormatadoTexto").value = cnpjFormatado.value;
    document.getElementById("emailClienteTexto").value = emailCliente.value;
    document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
}


function limparCampos(){
	submeter('formTransportadora','exibirInclusaoTransportadora');
}

function carregarParaAlterarMotorista(indexMotorista, nome, cpf, habilitado) {
	var botaoAlterarMotorista = document
			.getElementById("botaoAlterarMotorista");
	botaoAlterarMotorista.disabled = false;

	var botaoAdicionarMotorista = document
			.getElementById("botaoAdicionarMotorista");
	botaoAdicionarMotorista.disabled = true;
	habilidarDesabilidarCampoIndicadorUso(false);
	document.getElementById("nomeMotorista").value = nome;
	document.getElementById("cpf").value = cpf;

	var acao = "<c:out value="${acao}" />"
	if (acao == 'alterar') {
		if (habilitado) {
			document.forms[0].habilitado3[0].checked = true;
		} else {
			document.forms[0].habilitado3[1].checked = true;
		}
	}

	document.forms[0].indexList.value = indexMotorista;

}

var popup;
function exibirPopupPesquisaCliente() {
	popup = window.open('exibirPesquisaClientePopup?pessoaJuridica=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}


function carregarParaAlterarVeiculo(indexVeiculo, placa, idOrgaoEmissorUF, habilitado) {
		var botaoAlterarVeiculo = document
				.getElementById("botaoAlterarVeiculo");
		botaoAlterarVeiculo.disabled = false;

		var botaoAdicionarVeiculo = document
				.getElementById("botaoAdicionarVeiculo");
		botaoAdicionarVeiculo.disabled = true;
		habilidarDesabilidarCampoIndicadorUso(false);
		document.getElementById("placaVeiculo").value = placa;
		document.getElementById("idOrgaoEmissorUF").value = idOrgaoEmissorUF;

		var acao = "<c:out value="${acao}" />"
		if (acao == 'alterar') {
			if (habilitado) {
				document.forms[0].habilitado3[0].checked = true;
			} else {
				document.forms[0].habilitado3[1].checked = true;
			}
		}

		document.forms[0].indexList.value = indexVeiculo;

}

function excluirVeiculo(valorChave) {
		carregarFragmento('gridVeiculos', "removerVeiculo?indexVeiculo="
				+ valorChave);
	}

function excluirMotorista(valorChave) {
		carregarFragmento('gridMotoristas', "removerMotorista?indexMotorista="
				+ valorChave);
	}
	
function cancelar(){
	submeter('formTransportadora','exibirPesquisaTransportadora');
}	
</script>

<form:form method="post" id="formTransportadora" name="formTransportadora" modelAttribute="Transportadora">

<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${Transportadora.chavePrimaria}">
<input type="hidden" name="versao" id="versao" value="${Transportadora.versao}">
 <input type="hidden" name="indexList" id="indexList" >
  <input type="hidden" name="habilitadoPergunta" id="habilitado" >
 

<fieldset class="conteinerPesquisarIncluirAlterar">

 <fieldset id="pesquisarTransportadora" class="coluna2">
 
 
 
 <fieldset id="pesquisarCliente" class="colunaEsq">
		<legend><span class="campoObrigatorioSimbolo">* </span>Pesquisar Pessoa Jur�dica</legend>
		<div class="pesquisarClienteFundo">			
			<input name="idCliente" type="hidden" id="idCliente" value="${Transportadora.cliente.chavePrimaria}">
			<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${Transportadora.cliente.nome}">
			<input name="cnpjFormatado" type="hidden" id="cnpjFormatado" value="${Transportadora.cliente.cnpj}">
			<input name="enderecoFormatado" type="hidden" id="enderecoFormatado" value="${Transportadora.cliente.enderecoPrincipal.enderecoFormatado}">
			<input name="emailCliente" type="hidden" id="emailCliente" value="${Transportadora.cliente.emailPrincipal}">
			
			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Pessoa Jur�dica</span> para selecionar a Pessoa Jur�dica Respons�vel.</p>
			<input name="Button" id="botaoPesquisarClientePesssoaJuridica" class="bottonRightCol2" title="Pesquisar Pessoa Jur�dica"  value="Pesquisar Pessoa Jur�dica" onclick="exibirPopupPesquisaCliente();" type="button"><br >
			<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloCliente" for="nomeClienteTexto">Pessoa Jur�dica:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" size="50" disabled="disabled" value="${Transportadora.cliente.nome}"><br />
			<label class="rotulo campoObrigatorio" id="rotuloCnpjTexto" for="cnpjFormatadoTexto">CNPJ:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="cnpjFormatadoTexto" name="cnpjFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${Transportadora.cliente.cnpj}"><br />	
			<label class="rotulo campoObrigatorio" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto">Endere�o:</label>
			<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoTexto" rows="2" cols="37" disabled="disabled">${Transportadora.cliente.enderecoPrincipal.enderecoFormatado}</textarea><br />
			<label class="rotulo" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
			<input class="campoTexto campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${Transportadora.cliente.emailPrincipal}"><br />
		</div>
	</fieldset>
 
 
            <c:if test="${ acao eq 'alterar' }">
            <br />
	  			<label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
			    <input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${Transportadora.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
			   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${Transportadora.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
             	<br />
            </c:if>
            
	     </fieldset>
	     <br> <br><br>
	     <hr class="linhaSeparadora2" />
	      <fieldset class="conteinerBloco">
		      	<legend class="conteinerBlocoTitulo">Ve�culos</legend>
	     <fieldset class="pesquisarChamadoAssunto" style="float: left; margin:0 0 20px 0;background-color: #FFF">
		       	
		       	<fieldset class="colunaFinal">
			       	<label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloPlacaVeiculo" for="placaVeiculo" style="margin-top: 11px;" ><span id="spanDescricao" class="campoObrigatorioSimbolo" >* </span>Placa:</label>
			       	<input type="text" id="placaVeiculo" name="placaVeiculo" class="campoTexto campoHorizontal" value="${veiculo.placa}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" maxlength="7" style="margin-top: 21px"> <br /> 
			       	
			       	<label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloOrgaoEmissorUF" style="margin-left: 30px;"
					for="idOrgaoEmissorUF" ><span class="campoObrigatorioSimbolo">* </span>UF:</label>
					
					 <select name="idOrgaoEmissorUF"
					id="idOrgaoEmissorUF" class="campoSelect campoHorizontal">
					<option value="-1">Selecione</option>
					<c:forEach items="${unidadesFederacao}" var="unidadeFederacao">
						<option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>"
							<c:if test="${veiculo.uf.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${unidadeFederacao.descricao}" />
						</option>
					</c:forEach>
				</select><br />

				<c:if test="${ acao eq 'alterar' }">
				       	<label class="rotulo" id="rotuloHabilitado" for="habilitado3">Indicador de Uso:</label>
			    		<input class="campoRadio" type="radio" name="habilitado3" id="habilitado3" value="true" <c:if test="${veiculo.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
			   			<input class="campoRadio" type="radio" name="habilitado3" id="habilitado3" value="false" <c:if test="${veiculo.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
			       	</c:if>
			       	  	<br />
					<hr class="linhaSeparadoraPesquisa" />
					
					<fieldset class="conteinerBotoesPesquisarEsq">
			   			<input name="Button" class="bottonRightCol bottonLeftColUltimo" id="botaoAdicionarVeiculo" value="Adicionar Ve�culo" type="button" onclick="adicionarVeiculos()">
			   			<input name="Button" class="bottonRightCol bottonLeftColUltimo" id="botaoAlterarVeiculo" value="Alterar Ve�culo" type="button" onclick="adicionarVeiculos()" disabled="disabled">
					</fieldset>
				</fieldset>
				<div id="gridVeiculos">
					<jsp:include page="/jsp/cadastro/transportadora/gridVeiculos.jsp"></jsp:include>
				</div>
			</fieldset>
	      
	       </fieldset>
	      
	       <fieldset>
			<legend class="conteinerBlocoTitulo">Motoristas</legend>
	      <fieldset class="pesquisarChamadoAssunto" style="float: left; margin:0 0 20px 0; background-color: #FFF">
		       	
		       	<fieldset class="colunaFinal">
			       	<label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloNomeMotorista" for="nomeMotorista" style="margin-top: 11px;"><span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Nome:</label>
			       	<input type="text" id="nomeMotorista" name="nomeMotorista" class="campoTexto campoHorizontal" maxlength="30" style="margin-top: 21px;"  onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${motorista.nome}" > <br />
			       	
			       	<label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloCNPJ" for="cpf"><span class="campoObrigatorioSimbolo" style="margin-left: 30px;">* </span>CPF:</label>
					<input class="campoTexto campoHorizontal" type="text"  id="cpf" name="cpf" maxlength="18" size="18" value="${motorista.cpf}"><br />  
					

				<c:if test="${ acao eq 'alterar' }">
				       	<label class="rotulo" id="rotuloHabilitado" for="habilitado3">Indicador de Uso:</label>
			    		<input class="campoRadio" type="radio" name="habilitado3" id="habilitado3" value="true" <c:if test="${motorista.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
			   			<input class="campoRadio" type="radio" name="habilitado3" id="habilitado3" value="false" <c:if test="${motorista.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
			       	</c:if>
			       	  	<br />
					<hr class="linhaSeparadoraPesquisa" />
					
					<fieldset class="conteinerBotoesPesquisarEsq">
			   			<input name="Button" class="bottonRightCol bottonLeftColUltimo" id="botaoAdicionarMotorista" value="Adicionar Motorista" type="button" onclick="adicionarMotorista()">
			   			<input name="Button" class="bottonRightCol bottonLeftColUltimo" id="botaoAlterarMotorista" value="Alterar Motorista" type="button" onclick="adicionarMotorista()" disabled="disabled">
					</fieldset>
				</fieldset>	  
				<div id="gridMotoristas">     
	       		<jsp:include page="/jsp/cadastro/transportadora/gridMotoristas.jsp"></jsp:include>
	       		</div>
	       </fieldset>
	      </fieldset>
	      
	      <hr class="linhaSeparadoraPesquisa" />
	      
	      <fieldset class="conteinerBotoesPesquisarDir" >
	      	  <c:if test="${ acao eq 'incluir' }">
<%-- 	      	  <vacess:vacess param="incluirTransportadora"> --%>
	      	<input name="Button" class="bottonRightCol2" id="botaoIncluir" value="Salvar" type="button" onclick="salvar()">
<%-- 	      	</vacess:vacess> --%>
	      	</c:if>
	      	<c:if test="${ acao eq 'alterar' }">
<%-- 	      	<vacess:vacess param="alterarTransportadora"> --%>
	      	<input name="Button" class="bottonRightCol2" id="botaoAlterar" value="Salvar" type="button" onclick="alterarTransportadora()">
<%-- 	      	</vacess:vacess> --%>
	      	</c:if>
	      </fieldset>
	      <fieldset  class="conteinerBotoesPesquisarEsq">
	      	<input name="Button" class="bottonLeftRotulo" id="botaoCancelar" value="Cancelar" type="button" onclick="cancelar()">
	      	<input name="Button" class="bottonLeftRotulo" id="botaoLimpar" value="Limpar" type="button" onclick="limparCampos()">
	      </fieldset>
	      
	      
	       


</fieldset>



</form:form>