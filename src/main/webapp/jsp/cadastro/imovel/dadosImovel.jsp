<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script language="javascript">	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("<c:out value='${param.idCampoIdImovel}' default=''/>");
		var matriculaImovel = document.getElementById("<c:out value='${param.idCampoMatriculaImovel}' default=''/>");
		var nomeFantasia = document.getElementById("<c:out value='${param.idCampoNomeFantasiaImovel}' default=''/>");
		var numeroImovel = document.getElementById("<c:out value='${param.idCampoNumeroImovel}' default=''/>");
		var cidadeImovel = document.getElementById("<c:out value='${param.idCampoCidadeImovel}' default=''/>");
		var indicadorCondominio = document.getElementById("<c:out value='${param.idCampoCondominio}' default=''/>");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
	    } else {
	   		idImovel.value = "";
	    	matriculaImovel.value = "";
	    	nomeFantasia.value = "";
			numeroImovel.value = "";
	        cidadeImovel.value = "";
	        indicadorCondominio.value = "";
	   	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	/*
	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("<c:out value='${param.idCampoIdCliente}' default=""/>");
		var nomeCompletoCliente = document.getElementById("<c:out value='${param.idCampoNomeCliente}' default=""/>");
		var documentoFormatado = document.getElementById("<c:out value='${param.idCampoDocumentoFormatado}' default=""/>");
		var emailCliente = document.getElementById("<c:out value='${param.idCampoEmail}' default=""/>");
		var enderecoFormatado = document.getElementById("<c:out value='${param.idCampoEnderecoFormatado}' default=""/>");		
		
		if(idSelecionado != '') {				
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];
		               	if(cliente["cnpj"] != undefined ){
		               		documentoFormatado.value = cliente["cnpj"];
		               	} else {
			               	if(cliente["cpf"] != undefined ){
			               		documentoFormatado.value = cliente["cpf"];
			               	} else {
			               		documentoFormatado.value = "";
			               	}
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	documentoFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
        
        document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
        document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
        document.getElementById("emailClienteTexto").value = emailCliente.value;
        document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;

		//Verifica se a fun��o foi passada por par�metro e se ela � realmente v�lida antes de execut�-la.
        if (document.getElementById("funcaoParametro")!= null && document.getElementById("funcaoParametro").value != "") {
			var funcao = document.getElementById("funcaoParametro").value;
			if (funcaoExiste(funcao)) {
				funcao = funcao + '(\'' + idCliente.value + '\')';
				eval(funcao);
			}
        }
	}
	*/
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function limparFormularioDadosImovel(){		
       	$("#pesquisarImovelFundo :text, #pesquisarImovelFundo :hidden").val("");
       	$("#pesquisarImovelFundo :radio").removeAttr("checked");
	}

	function desabilitarPesquisaImovel(){
		$("#botaoPesquisarImovel").attr("disabled","disabled");
	}
	
	function habilitarPesquisaImovel(){
		$("#botaoPesquisarImovel").removeAttr("disabled");
	}

</script>

<c:choose>
	<c:when test="${not empty param.nomeComponente}">
		<legend <c:if test='${param.possuiRadio ne null && param.possuiRadio eq true}'>class="legendIndicadorPesquisa"</c:if>> <c:out value='${param.nomeComponente}'/>  </legend>
	</c:when>
	<c:otherwise>
		<legend <c:if test='${param.possuiRadio ne null && param.possuiRadio eq true}'>class="legendIndicadorPesquisa"</c:if>>Pesquisar Im�vel</legend>
	</c:otherwise>
</c:choose>

<div class="pesquisarImovelFundo">
	<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
	
	<input type="hidden" name="<c:out value='${param.idCampoIdImovel}' default=''/>" id="<c:out value='${param.idCampoIdImovel}' default=''/>" value="${param.idImovel}">
	<input type="hidden" name="<c:out value='${param.idCampoNomeFantasiaImovel}' default=''/>" id="<c:out value='${param.idCampoNomeFantasiaImovel}' default=''/>" value="${param.nomeFantasiaImovel}">
	<input type="hidden" name="<c:out value='${param.idCampoMatriculaImovel}' default=''/>" id="<c:out value='${param.idCampoMatriculaImovel}' default=''/>" value="${param.matriculaImovel}">
	<input type="hidden" name="<c:out value='${param.idCampoNumeroImovel}' default=''/>" id="<c:out value='${param.idCampoNumeroImovel}' default=''/>" value="${param.numeroImovel}">
	<input type="hidden" name="<c:out value='${param.idCampoCidadeImovel}' default=''/>" id="<c:out value='${param.idCampoCidadeImovel}' default=''/>" value="${param.cidadeImovel}">
	<input type="hidden" name="<c:out value='${param.idCampoCondominio}' default=''/>" id="<c:out value='${param.idCampoCondominio}' default=''/>" value="${param.condominio}">
	<!-- 
	<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${contratoForm.map.nomeFantasiaImovel}">
	<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${contratoForm.map.matriculaImovel}">
	<input name="numeroImovel" type="hidden" id="numeroImovel" value="${contratoForm.map.numeroImovel}">
	<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${contratoForm.map.cidadeImovel}">	
	<input name="condominio" type="hidden" id="condominio" value="${contratoForm.map.condominio}">
 	-->
 	
	<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirIndicador(); exibirPopupPesquisaImovel();" type="button"><br /><br />
	
	<label class="rotulo<c:if test="${param.dadosImovelObrigatorios}"> campoObrigatorio</c:if>" id="rotuloNomeFantasia" for="nomeImovelTexto">
		<c:if test="${param.dadosImovelObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Descri��o:
	</label>
	<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${param.nomeFantasiaImovel}"><br />
	
	<label class="rotulo<c:if test="${param.dadosImovelObrigatorios}"> campoObrigatorio</c:if>" id="rotuloMatriculaTexto" for="matriculaImovelTexto">
		<c:if test="${param.dadosImovelObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Matr�cula:
	</label>
	<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${param.matriculaImovel}"><br />
	
	<label class="rotulo<c:if test="${param.dadosImovelObrigatorios}"> campoObrigatorio</c:if>" id="rotuloNumeroTexto" for="numeroImovelTexto">
		<c:if test="${param.dadosImovelObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>N�mero:
	</label>
	<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${param.numeroImovel}"><br />
	
	<label class="rotulo<c:if test="${param.dadosImovelObrigatorios}"> campoObrigatorio</c:if>" id="rotuloCidadeTexto" for="cidadeImovelTexto">
		<c:if test="${param.dadosImovelObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Cidade:
	</label>
	<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${param.cidadeImovel}"><br />
	
	<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
	<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${param.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
	<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${param.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
</div>

<!-- Remover exemplo abaixo 

<div class="pesquisarClienteFundo">
	<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span> para selecionar o Cliente.</p>
	
	<input name="<c:out value='${param.idCampoIdCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoIdCliente}' default=""/>" value="${param.idCliente}">
	<input name="<c:out value='${param.idCampoNomeCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoNomeCliente}' default=""/>" value="${param.nomeCliente}">
	<input name="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" value="${param.documentoFormatadoCliente}">
	<input name="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" value="${param.enderecoFormatadoCliente}">
	<input name="<c:out value='${param.idCampoEmail}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEmail}' default=""/>" value="${param.emailCliente}">
	<input name="funcaoParametro" type="hidden" id="funcaoParametro" value="${param.funcaoParametro}">
	
	<input name="Button" id="botaoPesquisarCliente" class="bottonRightCol" title="Pesquisar Cliente"  value="Pesquisar Cliente" onclick="exibirPopupPesquisaCliente();" type="button"><br />
	
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloCliente" for="nomeClienteTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Cliente:</label>
	<input class="campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" disabled="disabled" value="${param.nomeCliente}"><br />
	
	<c:choose>
		<c:when test="${param.consultarPessoaFisica && not param.consultarPessoaJuridica}">
			<label class="rotulo<c:if test="${param.dadosClienteObrigatorios}"> campoObrigatorio</c:if>" id="rotuloCnpjTexto" for="documentoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>CPF:</label>
		</c:when>
		<c:when test="${not param.consultarPessoaFisica && param.consultarPessoaJuridica}">
			<label class="rotulo<c:if test="${param.dadosClienteObrigatorios}"> campoObrigatorio</c:if>" id="rotuloCnpjTexto" for="documentoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>CNPJ:</label>
		</c:when>
		<c:otherwise>
			<label class="rotulo<c:if test="${param.dadosClienteObrigatorios}"> campoObrigatorio</c:if>" id="rotuloCnpjTexto" for="documentoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>CPF/CNPJ:</label>
		</c:otherwise>
	</c:choose>
	<input class="campoDesabilitado" type="text" id="documentoFormatadoTexto" name="documentoFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${param.documentoFormatadoCliente}"><br />	
	
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Endere�o:</label>
	<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoClienteTexto" disabled="disabled">${param.enderecoFormatadoCliente}</textarea><br />
	
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
	<input class="campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${param.emailCliente}"><br />
</div>

-->