<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript">
	
	$(document).ready(function(){
		var operacaoExecutada = $("#operacaoExecutada").val();
		if(operacaoExecutada){ 
			limparSubsTributaria();
		}
		
		$('input[name=valorSubstituto]').change(function() {
	        var input = $(this),
	        text = input.val().replace(",", ".");
	        input.val(text);
	    });
		
		$('input[name=percentualSubstituto]').change(function() {
	        var input = $(this),
	        text = input.val().replace(",", ".");
	        input.val(text);
	    });
		
	});

	function adicionarSubsTributaria(){
		
		selecionarListasRamoAtividade();
		document.getElementById('idRamoAtividade').value = document.forms[0].idRamoAtividade.value;
		document.getElementById('operacaoSubstituicao').value = "adicionarSubsTributaria";
		document.forms[0].indexLista.value = document.getElementById("indexLista").value;
		
		submeter('segmentoForm', 'popupAlterarRamoAtividade');
	}
	
	function atualizarSubsTributaria(){
		
		selecionarListasRamoAtividade();
		var index = document.forms[0].indexLista.value;
		var idSubsTributaria = $('#idSubsTributaria').val();
		document.forms[0].operacao.value = "alterarSubsTributaria";
		document.getElementById('idRamoAtividade').value = document.forms[0].idRamoAtividade.value;
		document.getElementById('operacaoSubstituicao').value = "alterarSubsTributaria";
		document.getElementById('ramoAtividadeSubstituicaoTributaria').value = document.forms[0].indexLista.value;
		submeter('segmentoForm', 'popupAlterarRamoAtividade?idSubsTributaria='+idSubsTributaria);		
	}
	
	function removerSubsTributaria(idSubsTributaria) {
		selecionarListasRamoAtividade();
		document.getElementById('operacaoSubstituicao').value = "removerSubsTributaria";
		submeter('segmentoForm', 'popupAlterarRamoAtividade?idSubsTributaria='+idSubsTributaria);
	}
	
	function alterarSubsTributaria(idSubsTributaria, tipoSubsTributaria, valorSubstituto, percentualSubstituto, dataInicioVigencia, dataFimVigencia) {
		
		if (idSubsTributaria != "") {
			document.getElementById("idSubsTributaria").value = idSubsTributaria;
			document.forms[0].botaoIncluirSubsTributaria.disabled = true;
			document.forms[0].botaoAlterarSubsTributaria.disabled = false;	
			document.forms[0].operacao.value = "alterarSubsTributaria";
				
			$(document).ready(function(){
				
				$("select#tipoSubsTributaria").attr('value',tipoSubsTributaria);
				$("input#dataInicioVigencia").val(dataInicioVigencia);
				$("input#dataFimVigencia").val(dataFimVigencia);
				$("select#tipoSubsTributaria").trigger('onchange');
				$("input#valorSubstituto").val(valorSubstituto);
				$("input#percentualSubstituto").val(percentualSubstituto);
			});
			carregarAbaMedicao();
		}
	}
	
	function limparSubsTributaria(){
		$(document).ready(function(){
 			$("select#tipoSubsTributaria").attr('value',-1);
			$("input#dataInicioVigencia").val("");
			$("input#dataFimVigencia").val("");
			$("input#valorSubstituto").val("");
			$("input#percentualSubstituto").val("");
		});
	}
	function onloadRamoAtividade() {
		
		<c:choose>
			<c:when test="${indexLista > -1 && empty param['operacaoLista'] }">
				var botaoAlterarRamoAtividade = document.getElementById("botaoAlterarSubsTributaria");
				var botaoLimparRamoAtividade = document.getElementById("botaoLimparSubsTributaria");	
				var botaoIncluirRamoAtividade = document.getElementById("botaoIncluirSubsTributaria");	
				botaoIncluirSubsTributaria.disabled = true;
				botaoLimparSubsTributaria.disabled = false;
				botaoAlterarSubsTributaria.disabled = false;
			</c:when>
			<c:when test="${sucessoManutencaoLista}">
// 				limparSubsTributaria();
			</c:when>
		</c:choose>
	}
	
	function onChangeTipoSubstituicaoTributaria() {
		if($('#tipoSubsTributaria').val() == -1) {
              desabilitarCampo($('#valorSubstituto'));
              limparCampo($('#valorSubstituto'));
              desabilitarCampo($('#percentualSubstituto'));
              limparCampo($('#percentualSubstituto'));
        } else if($('#tipoSubsTributaria').val() == ${sessionScope.subsTributMVA}) {
            desabilitarCampo($('#valorSubstituto'));
            limparCampo($('#valorSubstituto'));
            habilitarCampo($('#percentualSubstituto'));
        } else if($('#tipoSubsTributaria').val() == ${sessionScope.subsTributPrecoMedioPondAConsFinal}) {
            habilitarCampo($('#valorSubstituto'));
            desabilitarCampo($('#percentualSubstituto'));
            limparCampo($('#percentualSubstituto'));
        }
    }
	

    function limparCampo(campo) {
           campo.val("");
    }

    function desabilitarCampo(campo) {
           campo.prop('readonly', true).addClass("campoDesabilitado");
    }

    function habilitarCampo(campo) {
           campo.prop('readonly', false).removeClass("campoDesabilitado");
    }


</script>
<fieldset class="conteinerPesquisarIncluirAlterar">
	
	<input type="hidden" name="ramoAtividadeSubstituicaoTributaria" id="ramoAtividadeSubstituicaoTributaria"/>
	<input type="hidden" name="idSubsTributaria" id="idSubsTributaria"/>
	
	<fieldset class="conteinerBloco">
		<label class="rotulo campoObrigatorio" for="tipoConsumoFaturamento"><span class="campoObrigatorioSimbolo">* </span>Exibir tipo consumo:</label>
		<select class="campoSelect" name="tipoConsumoFaturamento" id="tipoConsumoFaturamento">
			<c:forEach items="${listaTipoConsumoFaturamento}" var="consumoFaturamento">
				<option value="<c:out value="${consumoFaturamento.chavePrimaria}"/>"
						<c:if test="${ramoAtividade.tipoConsumoFaturamento.chavePrimaria == consumoFaturamento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${consumoFaturamento.descricao}"/>
				</option>
			</c:forEach>
		</select></br>
		<label class="rotulo" for="periodicidade"><span class="campoObrigatorioSimbolo2">* </span>Tipo de Substitui��o Tribut�ria:</label>
		<select class="campoSelect" name="tipoSubstituicao" id="tipoSubsTributaria" onchange="onChangeTipoSubstituicaoTributaria()">
	    <option value="-1">Selecione</option>
			<c:forEach items="${listaTipoSubsTributaria}" var="tipoSubsTributaria">
				<option value="<c:out value="${tipoSubsTributaria.chavePrimaria}"/>"
					<c:if test="${substTributaria.tipoSubstituicao.chavePrimaria == tipoSubsTributaria.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoSubsTributaria.descricao}"/>
				</option>
		    </c:forEach>
	    </select></br>
	    	
		<label class="rotulo" id="rotuloDataInicioVigencia" for="dataInicioVigencia"><span class="campoObrigatorioSimbolo2">* </span>Vig�ncia:</label>
		<input class="campoData campoHorizontal" type="text" id="dataInicioVigencia" name="dataInicioVigencia" maxlength="10" value="\<fmt:formatDate value="${substTributaria.dataInicioVigencia}" pattern="dd/MM/yyyy"/>" style="margin: 0px" />
		
		<label class="rotuloEntreCampos" id="rotuloDataFimVigencia" for="dataFimVigencia">a</label>
		<input class="campoData campoHorizontal" type="text" id="dataFimVigencia" name="dataFimVigencia" maxlength="10" value="\<fmt:formatDate value="${substTributaria.dataFimVigencia}" pattern="dd/MM/yyyy"/>" style="margin: 0px" />

	    <label class="rotulo" for="valorSubstituto"><span class="valorSubstituto">* </span>Valor: R$</label>
		<input class="campoTexto campoHorizontal campoDesabilitado" name="valorSubstituto" type="text" size="15" maxlength="15" id="valorSubstituto"
		value="${substTributaria.valorSubstituto}" onkeypress="return formatarCampoDecimal(event, this, 15, 8);" readonly>
			
		<label class="rotuloEntreCampos" id="rotuloEntreCamposPeriodo" for="percentualSubstituto"><span class="campoObrigatorioSimbolo2">* </span>Percentual:</label>
		<input class="campoTexto campoHorizontal campoDesabilitado" id="percentualSubstituto" name="percentualSubstituto" type="text" size="9" maxlength="9"
		 value="${substTributaria.percentualSubstituto}" onkeypress="return formatarCampoDecimal(event, this, 9, 6);" readonly><label class="rotuloInformativo" for="percentualSubstituto">%</label></br>	

<!-- 		<vacess:vacess param="inserirSegmento.do"> -->
<!-- 			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="adicionarSubsTributaria();" type="button"> -->
<!-- 		</vacess:vacess> -->
		<fieldset class="conteinerBotoesInserir1">
			<input class="bottonRightCol2" name="botaoLimparSubsTributaria" id="botaoLimparSubsTributaria" value="Limpar" type="button" onclick="limparSubsTributaria();">
			<input class="bottonRightCol2" name="botaoIncluirSubsTributaria" id="botaoIncluirSubsTributaria" value="Adicionar" type="button" onclick="document.forms[0].indexLista.value = -1; adicionarSubsTributaria();">	
			<input class="bottonRightCol2" disabled="disabled"  name="botaoAlterarSubsTributaria" id="botaoAlterarSubsTributaria" value="Alterar" type="button" onclick="atualizarSubsTributaria();">
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBloco">
			
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS" name="${sessionScope.listaSubsTributaria}" sort="list" id="listaSubsTributaria" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			 pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" style="text-align: center; width: 880px" >
			
			<display:column property="tipoSubstituicao.descricao" sortable="false" title="Descri��o" />			
			<display:column sortable="false" title="Vig�ncia Inicial">
				<fmt:formatDate value="${listaSubsTributaria.dataInicioVigencia}" pattern="dd/MM/yyyy" />
			</display:column>							
			<display:column sortable="false" title="Vig�ncia Final">
				<fmt:formatDate value="${listaSubsTributaria.dataFimVigencia}" pattern="dd/MM/yyyy" />
			</display:column>	
			<display:column sortable="false" title="Valor (R$)">
				<fmt:formatNumber value="${listaSubsTributaria.valorSubstituto}" type="number"/>
			</display:column>	
			<display:column property="percentualSubstituto" sortable="false" title="Percentual (%)"/>
			<display:column style="text-align: center;">																									
				<a href="javascript:alterarSubsTributaria('${listaSubsTributaria.chavePrimaria}','${listaSubsTributaria.tipoSubstituicao.chavePrimaria}', 
							'<fmt:formatNumber value="${listaSubsTributaria.valorSubstituto}" type="number"/>','<fmt:formatNumber value="${listaSubsTributaria.percentualSubstituto}"/>',
							'<fmt:formatDate value="${listaSubsTributaria.dataInicioVigencia}" pattern="dd/MM/yyyy" />',
							'<fmt:formatDate value="${listaSubsTributaria.dataFimVigencia}" pattern="dd/MM/yyyy" />');">
					<img title="Alterar tributo do ponto de consumo" alt="Alterar tributo do ponto de consumo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0">
				</a>				 
			</display:column>			
			<display:column style="text-align: center;">
				<a onclick="return confirm('Deseja excluir a substitui��o tribut�ria?');" href="javascript:removerSubsTributaria('<c:out value="${listaSubsTributaria.chavePrimaria}"/>');">
					<img title="Excluir substitui��o Tributaria" alt="Excluir substitui��o tributaria"  src="<c:url value="/imagens/deletar_x.png"/>">
				</a> 
			</display:column>
			<c:set var="i" value="${i+1}" />
		</display:table>	
	</fieldset>

</fieldset>

<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar substitui��o tribut�ria.</p>
