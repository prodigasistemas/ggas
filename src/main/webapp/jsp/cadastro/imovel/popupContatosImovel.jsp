<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script lang="javascript">
	
	function salvarContatosImovel(){
		submeter('imovelForm','salvarContatosImovel?postBack=false');
	}

	function selecionarContatoImovel(idContato){
		<c:choose>
			<c:when test="${sessionScope.listaContatoModificada ne null && sessionScope.listaContatoModificada}">
				alert("As altera��es ainda n�o foram salvas");
			</c:when>
			<c:otherwise>
				window.opener.selecionarContatoImovel(idContato);
				window.close();
			</c:otherwise>
		</c:choose>   			
	}
		
</script>

<h1 class="tituloInternoPopup">Contatos do Im�vel: <c:out value="${imovelVO.nome}" /> </h1>
<p class="orientacaoInicialPopup">Adicione ou altere um contato e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para atualizar os contatos do im�vel. Clique no �cone <img title="Selecionar contato" alt="Selecionar contato"  src="<c:url value="/imagens/check2.gif"/>" border="0"> para selecion�-lo como solicitante do chamado.</p>

<form method="post" action="alterarImovel" id="imovelForm" name="imovelForm">
<token:token></token:token>
<input name="acao" type="hidden" id="acao" value="alterarImovel"/>
<input name="nome" type="hidden" id="nomeImovel" value="${imovelVO.nome}"/>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${imovelVO.chavePrimaria}"/>
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="indexLista" type="hidden" id="indexLista" value="${imovelVO.indexLista}">
 	
<fieldset id="tabs" style="display: none">
	<ul>
		<li><a href="#imovelAbaContatos">Contatos</a></li>
	</ul>

	<fieldset id="imovelAbaContatos">
		<jsp:include page="/jsp/cadastro/imovel/abaContatoImovel.jsp">
			<jsp:param name="actionAdicionarContato" value="adicionarContatoDoImovelFluxoPopup" />			
			<jsp:param name="actionRemoverContato" value="false" />
			<jsp:param name="actionAtualizarContatoPrincipal" value="atualizarContatoDoImovelFluxoPopup" />
		</jsp:include>
	</fieldset>
	
</fieldset>

<fieldset>
	<input name="button" class="bottonRightCol2" value="Salvar" type="button" onclick="salvarContatosImovel();" style="float: right; margin: 15px;">
	<input name="button" class="bottonRightCol2" value="Fechar" type="button" onclick="window.close()" style="float: left; margin: 10px;">		
</fieldset>

</form>
 
