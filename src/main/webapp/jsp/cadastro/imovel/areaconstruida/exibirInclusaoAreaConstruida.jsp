<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">
	Incluir �rea Constru�da<a
		href="<help:help><%=session.getAttribute("paginaHelp")%></help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Informe os dados abaixo e clique em <span
		class="destaqueOrientacaoInicial">Salvar</span> para finalizar.
</p>

<script lang="javascript">


    $(document).ready(function(){
      

         var max = 0;

          $('.rotulo').each(function(){
                  if ($(this).width() > max)
                     max = $(this).width();   
              });
          $('.rotulo').width(max);
         
      
            $("input.botaoAdicionarFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px', 'margin-left' :'5px','cursor':'pointer'});
            $("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-right': '-12px' ,'cursor':'pointer'});

            $("input.botaoAdicionarFaixas").hover(
                function () {
                    $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'5px','cursor':'pointer'});
                },
                function () {
                    $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'5px','cursor':'pointer'});
                }
            );

            $("input.botaoRemoverFaixas").hover(
                function () {
                    $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px', 'margin-right': '-12px','cursor':'pointer'});
                },
                function () {
                    $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'-1px','cursor':'pointer'});
                }
            );
            

            $("input[name=maiorFaixa]").each(function(){
                aplicarMascaraNumeroInteiro(this);
            });
            $("input[name=menorFaixa]").each(function(){
                aplicarMascaraNumeroInteiro(this);
            });    
        });

        function adicionarFaixaTarifa( chave ){
            document.getElementById('faixaFimAdicao').value = chave; 

            submeter('areaConstruidaForm', 'incluirFaixaAreaConstruida');
        }

        function removerFaixaTarifa(chave){
            var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
            if(retorno == true) {
                document.getElementById('faixaInicialExclusao').value = chave; 
                submeter('areaConstruidaForm', 'removerFaixaAreaConstruida');
            }
        }
        
        function faixasTarifaClasses(){
            $("#tarifaFaixas .dataTable tr:nth-child(odd)").addClass("odd").removeClass("even");
            $("#tarifaFaixas .dataTable tr:nth-child(even)").addClass("even").removeClass("odd");
        }

	    function salvar(){
	        submeter('areaConstruidaForm', 'incluirAreaConstruida');
	    }

	    function cancelar(){   
	        submeter('areaConstruidaForm', 'exibirPesquisaAreaConstruida');
	    }

    
	    function init() {
	    	faixasTarifaClasses();
	    	verificarDesabilitarAreaConstruida();
	    }

	    function verificarDesabilitarAreaConstruida(){
	         var listaImovel = '<%=session.getAttribute("listaImovel")%>';
	            if(listaImovel != "null"){
	                    //document.getElementById("botaoRemover").disabled = true;
	                    //document.getElementById("botaoAdicionar").disabled = true;
	                    //document.getElementById("tarifaFaixas").disabled = true;
	                    //document.getElementById("botaoSalvar").disabled = true;
	            }
	    }

    
	    function mostrarMensagemDeAviso(){
	        alert('<fmt:message key="MENSAGEM_AREA_CONSTRUIDA_VINCULADA"/>');
	    }

    	addLoadEvent(init);
    
</script>

<form method="post" enctype="multipart/form-data"
	action="incluirAreaConstruida" id="areaConstruidaForm"
	name="areaConstruidaForm">
	<input name="habilitado" type="hidden" id="habilitado" value="${true}"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="faixaInicialExclusao" type="hidden"
		id="faixaInicialExclusao"> <input name="faixaFimAdicao"
		type="hidden" id="faixaFimAdicao"> <input name="ultimaFaixa"
		type="hidden" id="ultimaFaixa"> <input name="indexLista"
		type="hidden" id="indexLista">

	<fieldset class="conteinerPesquisarIncluirAlterar">

		<fieldset id="tarifaFaixas" class="conteinerBloco">

			<c:set var="i" value="0" />
			<display:table class="dataTable" name="sessionScope.listaTabelaAux"
				sort="list" id="dadosFaixa"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				requestURI="">

				<display:column style="text-align: center; width: 100px"
					sortable="false"
					title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>&nbsp;&nbsp;&nbsp;Habilitado">
					<input type="checkbox" id="faixasHabilitadas"
						name="faixasHabilitadas" value='${dadosFaixa.chavePrimaria}'
						<c:if test="${dadosFaixa.habilitado == true}">checked="checked"</c:if>>
				</display:column>

				<display:column style="width: 25px" title="Faixa Inicial (m�)">
					<input type="text" class="campoTexto menorFaixa" name="menorFaixa"
						id="menorFaixa" disabled="disabled"
						value="${dadosFaixa.menorFaixa}"
						onblur="aplicarMascaraNumeroInteiro(this);"
						onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);">
					<input type="hidden" name="menorFaixaEscondido"
						value="<c:out value="${dadosFaixa.menorFaixa}"/>">
				</display:column>
				<display:column style="width: 25px" title="Faixa Final (m�)">
					<input type="text" class="campoTexto maiorFaixa" id="maiorFaixa"
						name="maiorFaixa" value="${dadosFaixa.maiorFaixa}"
						onblur="aplicarMascaraNumeroInteiro(this);"
						onkeypress="return formatarCampoInteiro(event,17);">
				</display:column>
				<display:column style="width: 25px" title="Faixas">
					<c:choose>
						<c:when test="${fn:length(listaTabelaAux) > 1 && i > 0}">
							<input type="button" class="botaoAdicionarFaixas botaoAdicionar"
								id="botaoAdicionar"
								onClick='adicionarFaixaTarifa(<c:out value="${dadosFaixa.menorFaixa}"/>);'
								title="Adicionar" />

							<c:if
								test="${ not empty dadosFaixa.maiorFaixa && faixasSemImoveis[i] == 0 }">
								<input type="button" class="botaoRemoverFaixas botaoRemover"
									id="botaoRemover" onclick="mostrarMensagemDeAviso();"
									title="Remover" />
							</c:if>

							<c:if test="${ faixasSemImoveis[i] != 0 }">
								<input type="button" class="botaoRemoverFaixas botaoRemover"
									id="botaoRemover"
									onClick="removerFaixaTarifa('<c:out value='${dadosFaixa.menorFaixa}'/>')"
									title="Remover" />
							</c:if>

						</c:when>
						<c:otherwise>
							<input type="button" class="botaoAdicionarFaixas botaoAdicionar"
								onClick="adicionarFaixaTarifa();" title="Adicionar" />
						</c:otherwise>
					</c:choose>
				</display:column>
				<c:set var="i" value="${i+1}" />
			</display:table>
		</fieldset>
	</fieldset>


	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Cancelar"
			type="button" onclick="javascript:cancelar();"> <input
			name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar"
			value="Salvar" type="button" id="botaoSalvar"
			onclick="javascript:salvar();">

	</fieldset>
</form>