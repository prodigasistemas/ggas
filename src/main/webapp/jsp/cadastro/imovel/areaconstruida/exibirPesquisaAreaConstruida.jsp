<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
 

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/DataParadaProgramadaPrototype.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick-pt-BR.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>



<h1 class="tituloInterno">Pesquisar �rea Constru�da<a href="<help:help><%= session.getAttribute("paginaHelp") %></help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. 
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" action="pesquisarAreaConstruida" id="areaConstruidaForm" name="areaConstruidaForm">
    
   
    
    <script lang="javascript">

    $(document).ready(function(){
    	
   	  var max = 0;

      $('.rotulo').each(function(){
              if ($(this).width() > max)
                 max = $(this).width();   
          });
      $('.rotulo').width(max);
    });

    function incluirAreaConstruida() {
        
        submeter('areaConstruidaForm', 'exibirInclusaoAreaConstruida');
        
    }
    
    function limparFormulario(){
        
    	document.forms[0].habilitado[0].checked = true;
    	document.getElementById('faixa').value = "";
    }
    
    function habilitarDesabilitarBotaoAdicionar() {
		
		document.forms[0].habilitado[0].checked = true;
		document.getElementById('faixa').value = "";
	}
	

    </script>

    <input name="chavePrimaria" type="hidden" id="chavePrimaria" >
    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
    <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">

	<fieldset class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">

			<label class="rotulo" id="rotuloFaixa" for="faixa">Faixa:</label> <input
				class="campoTexto" type="text" name="faixa" id="faixa" maxlength="5"
				size="5" value="${faixa}"
				onkeypress="return formatarCampoInteiro(event)" /> <br />
		</fieldset>
		
		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">

			<label class="rotulo" for="habilitado">Indicador de Uso:</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value="true"
				<c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value="false"
				<c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value=""
				<c:if test="${habilitado eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label><br /> <br />
			<br />
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess
					param="exibirPesquisaAreaConstruida">
					<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
						value="Pesquisar" type="submit">
				</vacess:vacess>
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo"
					value="Limpar" type="button"
					onclick="javascript:limparFormulario();">
			</fieldset>
		</fieldset>
	</fieldset>


	<c:if test="${listaAreaConstruida ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaAreaConstruida"
			sort="list" id="areaConstruida"
			decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="pesquisarAreaConstruida">

			<display:column title="Ativo" style="width: 30px">
				<c:choose>
					<c:when test="${areaConstruida.habilitado == true}">
						<img alt="Ativo" title="Ativo"
							src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo"
							src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>



			<display:column sortable="true" sortProperty="menorFaixa"
				title="Faixa Inicial">
				<c:out value="${areaConstruida.menorFaixa}" />
			</display:column>
			<display:column sortable="true" sortProperty="maiorFaixa"
				title="Faixa Final">
				<c:out value="${areaConstruida.maiorFaixa}" />
			</display:column>
		</display:table>
	</c:if>

	<fieldset class="conteinerBotoes">
		<vacess:vacess
			param="exibirInclusaoAreaConstruida">
			<input id="incluir" name="button"
				class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Incluir"
				onclick="javascript:incluirAreaConstruida();" type="button">
		</vacess:vacess>
	</fieldset>
</form>
