<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script language="javascript">
	
	$(document).ready(function(){
		
	});

	function alterarLogradouroImoveisFilhos(){
		window.opener.setarCampoHidden();
		window.submeter("imovelForm", "alterarLogradouroImoveisFilhos");
		setInterval("window.close()", 30);
	} 	
	
</script>

<h1 class="tituloInternoPopup">Alterar Situa��o dos Im�veis<a href="<help:help>/consultandocliente.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicialPopup">Para alterar a situa��o dos im�veis, escolha os im�veis abaixo e clique em <span class="destaqueOrientacaoInicial">Alterar Situa��o Im�veis</span></p>

<form method="post" action="alterarLogradouroImoveisFilhos" id="imovelForm" name="imovelForm">

	<input name="acao" type="hidden" id="acao" value="alterarLogradouroImoveisFilhos"/>
	<input name="idQuadraFace" type="hidden" id="idQuadraFace" value="${imovelVO.idQuadraFace}"/>
	
	<fieldset class="conteinerBotoesPopup"> 
	    <input name="button" id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Alterar Situa��o Im�veis" type="button" onClick="alterarLogradouroImoveisFilhos();" style="margin-right:10px;">
	 </fieldset>
		
	<c:if test="${sessionScope.listaImovelFilhoFactivel ne null}">
		<legend class="conteinerBlocoTitulo">Mudar para Fact�vel:</legend>
			<display:table class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="sessionScope.listaImovelFilhoFactivel" id="imovel" pagesize="20" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirPopupAlteracaoLogradouro">
	     	
	     		<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      			<input type="checkbox" checked="checked" name="chavesPrimarias" value="${imovel.chavePrimaria}">
	     		</display:column>	     	
	     	
		     	<display:column style="width: 70px" title="Matr�cula" sortable="false" sortProperty="chavePrimaria">
						<c:out value='${imovel.chavePrimaria}'/>
			    </display:column>
			    <display:column sortable="false" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq">
						<c:out value='${imovel.nome}'/>
			    </display:column>		     
			    <display:column style="width: 80px" title="Situa��o" sortable="false" sortProperty="situacaoImovel">
			    		<c:out value='${imovel.situacaoImovel.descricao}'/>
			    </display:column>
<%-- 			    <display:column sortable="false" title="Endere�o" sortProperty="enderecoFormatado" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq"> --%>
<%-- 			    		<c:out value='${imovel.enderecoFormatado}'/> --%>
<%-- 			    </display:column> --%>
			</display:table>
		</c:if>
	<hr class="linhaSeparadora1" />
		<c:if test="${sessionScope.listaImovelFilhoPotencial ne null}">
		<legend class="conteinerBlocoTitulo">Mudar para Potencial:</legend>
			<display:table class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="sessionScope.listaImovelFilhoPotencial" id="imovel" pagesize="20" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirPopupAlteracaoLogradouro">
	     		
	     		<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      			<input type="checkbox" checked="checked" name="chavesPrimarias" value="${imovel.chavePrimaria}">
	     		</display:column>	     	
	     	
		     	<display:column style="width: 70px" title="Matr�cula" sortable="false" sortProperty="chavePrimaria">
						<c:out value='${imovel.chavePrimaria}'/>
			    </display:column>
			    <display:column sortable="false" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq">
						<c:out value='${imovel.nome}'/>
			    </display:column>		     
			    <display:column style="width: 80px" title="Situa��o" sortable="false" sortProperty="situacaoImovel">
			    		<c:out value='${imovel.situacaoImovel.descricao}'/>
			    </display:column>
<%-- 			    <display:column sortable="false" title="Endere�o" sortProperty="enderecoFormatado" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq"> --%>
<%-- 			    		<c:out value='${imovel.enderecoFormatado}'/> --%>
<%-- 			    </display:column> --%>
			</display:table>
		</c:if>
	
</form>
