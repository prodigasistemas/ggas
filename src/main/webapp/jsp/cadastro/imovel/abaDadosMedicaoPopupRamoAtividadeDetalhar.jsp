<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>

</script>
	
	<label class="rotulo" for="estouroConsumo">Volume m�dio para estouro de consumo:</label>
		<c:if test="${segmentoForm.consumoEstouro ne null}">
			<span class="itemDetalhamento">
				<c:out value="${segmentoForm.consumoEstouro}"/>
			</span>
		</c:if>
		<c:if test="${segmentoForm.consumoEstouro eq null}">	
			<span class="itemDetalhamento">
				
			</span>			
		</c:if>
	<label class="rotuloInformativo" for="consumoAlto">m<span class="expoente">3</span></label><br class="quebraLinha2" />
			
	<fieldset class="conteinerBloco">
	
    	<fieldset class="conteinerLista1">
			<a name="listaRamoAtividadeAmostragemPCS"></a>
			<label class="rotulo rotuloVertical" id="rotuloRamoAtividadeAmostragemPCS" for="ramoAtividadeAmostragemPCS">Local de Amostragem do PCS:</label><br />
			<select id="listaRamoAtividadeAmostragemPCS" class="campoList listaCurta1a campoVertical" name="listaRamoAtividadeAmostragemPCS" multiple="multiple" disabled="disabled" readOnly="true">
				<c:forEach items="${listaRamoAtividadeAmostragemPCSSelecionados}" var="amostragemPCS">
					<option value="<c:out value="${amostragemPCS.localAmostragemPCS.chavePrimaria}"/>" title="<c:out value="${amostragemPCS.localAmostragemPCS.descricao}"/>"> <c:out value="${amostragemPCS.localAmostragemPCS.descricao}"/></option>		
				</c:forEach>
			</select>
		</fieldset>
		
		<fieldset class="conteinerLista2">
			<a name="listaSegmentoIntervaloPCS"></a>
			<label class="rotulo rotuloVertical" id="rotuloIntervaloPCS" for="intervaloPCSs">Intervalo de Recupera��o do PCS:</label><br />
			<select id="listaRamoAtividadeIntervaloPCS" class="campoList listaCurta1a campoVertical" name="listaRamoAtividadeIntervaloPCS" multiple="multiple" disabled="disabled" readOnly="true">
				<c:forEach items="${listaRamoAtividadeIntervaloPCSSelecionados}" var="ramoAtividadeIntervaloPCS">
					<option value="<c:out value="${ramoAtividadeIntervaloPCS.intervaloPCS.chavePrimaria}"/>" title="<c:out value="${ramoAtividadeIntervaloPCS.intervaloPCS.chavePrimaria}"/>"> <c:out value="${ramoAtividadeIntervaloPCS.intervaloPCS.descricao}"/></option>		
				</c:forEach>
			</select>			
		</fieldset>
		<label class="rotulo" for="tamanhoReducao">Tamanho da redu��o:</label>
		<c:if test="${tamanhoReducao ne null}">
			<span id="tamanhoReducao" class="itemDetalhamento">
				<c:out value="${tamanhoReducao}"/>
			</span>
		</c:if>
		<c:if test="${tamanhoReducao eq null}">	
			<span id="tamanhoReducao" class="itemDetalhamento">
				
			</span>			
		</c:if>
		<label class="rotuloInformativo" for="consumoAlto">dias</label>
	</fieldset>