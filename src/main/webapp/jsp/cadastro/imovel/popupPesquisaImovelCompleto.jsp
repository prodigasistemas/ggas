<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script language="javascript">

	$(document).ready(function(){

		window.opener.esconderIndicador();
		
		//Coloca o cursor no primeiro campo de pesquisa
		$("#nomeFantasia").focus();

		/*Executa a pesquisa quando a tecla ENTER � pressionada, 
		caso o cursor esteja em um dos campos do formul�rio*/
		$('#pesquisarImovelCompletoPopup>:text, #pesquisarImovelCompletoPopup>:radio').keypress(function(event) {
			if (event.keyCode == '13') {
				pesquisarImovel();
			}
		});		
	
	});
	
	function selecionarNoLink(id) {
		window.opener.selecionarImovel(id);
		window.close();
	}
	
	function selecionar(elem){
		document.getElementById('idSelecionado').value = elem.value;	
	}
	
	function selecionarImovel() {
		window.opener.selecionarImovel(document.getElementById('idSelecionado').value);
		window.close();
	}
	
	function limparDados(){
		document.getElementById('nomeFantasia').value = '';
		document.getElementById('complementoImovel').value = '';
		document.getElementById('matriculaImovel').value = '';
		document.getElementById('numeroImovel').value = '';
		document.getElementById('cepImovel').value = '';
		document.getElementById('pontoConsumoLegado').value = '';
		document.forms[0].indicadorCondominioAmbos[2].checked = true;
		document.forms[0].habilitado[0].checked = true;
	}
	
	function pesquisarImovel(){
		$("#botaoPesquisar").attr('disabled','disabled');
		submeter('pesquisaImovelForm', 'pesquisarImovelCompletoPopup');
	}

	function init() {
		<c:if test="${listaImoveis ne null}">
			$.scrollTo($('#imovel'),800);
		</c:if>
	}
	addLoadEvent(init);

</script>



<h1 class="tituloInternoPopup">Pesquisar Im�vel<a class="linkHelp" href="<help:help>/consultandoimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicialPopup">Para pesquisar um im�vel, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form method="post" action="pesquisarImovelCompletoPopup" name="pesquisaImovelForm" id="pesquisaImovelForm">
	<input name="idSelecionado" type="hidden" id="idSelecionado" value="">
	
	<fieldset id="pesquisarImovelCompletoPopup">
		<label class="rotulo" id="rotuloNomeFantasia" for="nomeFantasia">Descri��o:</label>
		<input class="campoTexto" type="text" id="nomeFantasia" name="nomeFantasia" maxlength="50" size="37" value="${imovelPesquisaVO.nomeFantasia}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br class="quebraLinha2" />
		<fieldset id="exibirCep">
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="cepObrigatorio" value="false"/>
				<jsp:param name="idCampoCep" value="cepImovel"/>
				<jsp:param name="numeroCep" value="${imovelPesquisaVO.cepImovel}"/>
			</jsp:include>		
		</fieldset>
		<label class="rotulo" id="rotuloComplemento" for="complementoImovel">Complemento:</label>
		<input class="campoTexto" type="text" id="complementoImovel" name="complementoImovel" maxlength="25" size="30" value="${imovelPesquisaVO.complementoImovel}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"><br class="quebraLinha2" />
		<label class="rotulo" id="rotuloMatricula" for="matriculaImovel">Matr�cula:</label>
		<input class="campoTexto" type="text" id="matriculaImovel" name="matriculaImovel" maxlength="9" size="9" value="${imovelPesquisaVO.matriculaImovel}" onkeypress="return formatarCampoInteiro(event)"><br class="quebraLinha2" />
		<label class="rotulo" id="rotuloMatricula" for="numeroImovel">N�mero do im�vel:</label>
		<input class="campoTexto campo2Linhas" type="text" id="numeroImovel" name="numeroImovel" maxlength="9" size="9" value="${imovelPesquisaVO.numeroImovel}" onkeyup="this.value = this.value.toUpperCase();" onkeyup="this.value = this.value.toUpperCase();"><br class="quebraLinha2" />
		<label class="rotulo" id="rotuloPontoConsumoLegado" for="pontoConsumoLegado">C&oacute;digo do Ponto <br/>de Consumo:</label>
		<input class="campoTexto" type="text" id="pontoConsumoLegado" name="pontoConsumoLegado"  maxlength="18" size="18" value="${imovelPesquisaVO.pontoConsumoLegado}"><br />
		<label class="rotulo" id="rotuloImovelCondominio" for="condominioSim">O im�vel � condom�nio?</label>
		<input class="campoRadio" type="radio" value="true" name="indicadorCondominioAmbos" id="condominioSim" <c:if test="${imovelPesquisaVO.indicadorCondominioAmbos eq true}">checked</c:if>><label class="rotuloRadio" for="condominioSim">Sim</label>
		<input class="campoRadio" type="radio" value="false" name="indicadorCondominioAmbos" id="condominioNao" <c:if test="${imovelPesquisaVO.indicadorCondominioAmbos eq false}">checked</c:if>><label class="rotuloRadio" for="condominioNao">N�o</label>
		<input class="campoRadio" type="radio" value="" name="indicadorCondominioAmbos" id="condominioAmbos" <c:if test="${empty imovelPesquisaVO.indicadorCondominioAmbos}">checked</c:if>><label class="rotuloRadio" for="condominioAmbos">Todos</label><br class="quebraLinha2" />
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitadoImovelSim" value="true" <c:if test="${imovelPesquisaVO.habilitado eq true}">checked</c:if>>
		<label class="rotuloRadio" for="habilitadoImovelSim">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitadoImovelNao" value="false" <c:if test="${imovelPesquisaVO.habilitado eq false}">checked</c:if>>
		<label class="rotuloRadio" for="habilitadoImovelNao">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitadoImovelTodos" value="" <c:if test="${empty imovelPesquisaVO.habilitado}">checked</c:if>>
		<label class="rotuloRadio" for="habilitadoImovelTodos">Todos</label><br class="quebraLinha2" />
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparDados();">
	    <input name="button" id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Pesquisar" type="button" onclick="pesquisarImovel();">
	 </fieldset>
	
	<c:if test="${listaImoveis ne null}">
		<display:table class="dataTableGGAS dataTablePopup" name="listaImoveis" id="imovel" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarImovelCompletoPopup#ancoraResultados">
	     	<display:column title="Matr�cula" sortable="true" sortProperty="chavePrimaria" style="width: 120px">
	     		<a href='javascript:selecionarNoLink(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	     			<c:out value='${imovel.chavePrimaria}'/>
	     		</a>
	     	</display:column>
	     	
	     	<display:column titleKey="IMOVEL_NOME" sortable="true" sortProperty="nome" style="text-align: left">
				<a href='javascript:selecionarNoLink(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${imovel.nome}'/>
				</a>
			</display:column>
			
			<display:column title="Endere�o" sortable="true" sortProperty="enderecoFormatado" style="width: 200px; text-align: left">
				<a href='javascript:selecionarNoLink(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${imovel.enderecoFormatado}'/>
				</a>
			</display:column>
			<display:column sortable="true" title="Tipo Medi��o" sortProperty="modalidadeMedicaoImovel" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq" style="width: 100px">
		    	<a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    		<c:choose>
		    			<c:when test="${imovel.imovelCondominio ne null}">
				    		<c:out value='${imovel.imovelCondominio.modalidadeMedicaoImovel.descricao}'/>
		    			</c:when>
		    			<c:otherwise>
				    		<c:out value='${imovel.modalidadeMedicaoImovel.descricao}'/>
		    			</c:otherwise>
		    		</c:choose>
		    	</a>
		    </display:column>       
		</display:table>
	</c:if>
 

<input class="campoTexto"  type="text" style="display:none" id="pesquisaImovelPai" name="pesquisaImovelPai"  value="${imovelPesquisaVO.pesquisaImovelPai}"  />
  </form>
<c:if test="${not empty param.pesquisaImovelPai}">
<script>
$("#condominioSim").attr('checked','true')
$("#condominioNao").attr('disabled', 'true')
$("#condominioAmbos").attr('disabled', 'true')
$("#pesquisaImovelPai").val("true")
</script>
</c:if>

<c:if test="${not empty imovelPesquisaVO.pesquisaImovelPai}">
<script>
$("#condominioSim").attr('checked','true')
$("#condominioNao").attr('disabled', 'true')
$("#condominioAmbos").attr('disabled', 'true')
$("#pesquisaImovelPai").val("true")
</script>
</c:if>
 