<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript">

</script>
	
	<label class="rotulo" for="estouroConsumo">Volume m�dio para estouro de consumo:</label>
	<input class="campoTexto campoHorizontal" id="consumoAlto" name="volumeMedioEstouroRamo" type="text" size="8" maxlength="8" value="${ramoAtividadeAuxVO.volumeMedioEstouroRamo}" onkeypress="return formatarCampoInteiro(event);";">
	<label class="rotuloInformativo" for="consumoAlto">m<span class="expoente">3</span></label><br class="quebraLinha2" />
			
	<fieldset class="conteinerBloco">
	
    	<fieldset class="conteinerLista1">
		    <legend class="conteinerBlocoTitulo">Local de Amostragem do PCS</legend>
			<a name="listaRamoAtividadeAmostragemPCSDisponiveis"></a>
			<label class="rotulo rotuloVertical" id="rotuloLocalAmostragemPCS" for="listaRamoAtividadeAmostragemPCSDisponiveis">Dispon�veis:</label><br />
			<select id="listaRamoAtividadeAmostragemPCSDisponiveis" class="campoList listaCurta1a campoVertical" name="listaRamoAtividadeAmostragemPCSDisponiveis" multiple="multiple"  
			onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeAmostragemPCSDisponiveis,document.forms[0].listaRamoAtividadeAmostragemPCSSelecionados,true)">
				<c:forEach items="${listaRamoAtividadeAmostragemPCSDisponiveis}" var="amostragemPCS">
					<option value="<c:out value="${amostragemPCS.chavePrimaria}"/>" title="<c:out value="${amostragemPCS.chavePrimaria}"/>"> <c:out value="${amostragemPCS.descricao}"/></option>		
				</c:forEach>
			</select>
			
			<fieldset class="conteinerBotoesCampoList1a">
				<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol2" id="botaoDireita" 
					onClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeAmostragemPCSDisponiveis,document.forms[0].listaRamoAtividadeAmostragemPCSSelecionados,true);">
				<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].listaRamoAtividadeAmostragemPCSDisponiveis,document.forms[0].listaRamoAtividadeAmostragemPCSSelecionados,true);">
				<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
					onClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeAmostragemPCSSelecionados,document.forms[0].listaRamoAtividadeAmostragemPCSDisponiveis,true);">
				<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].listaRamoAtividadeAmostragemPCSSelecionados,document.forms[0].listaRamoAtividadeAmostragemPCSDisponiveis,true);">
			</fieldset>
		</fieldset>
		
		<fieldset class="conteinerLista2" id="camposConteinerLocalAmostragemPCSSelecionados">
			<label id="rotoloListaRamoAtividadeAmostragemPCSSelecionados" class="rotulo rotuloListaLonga2" for="listaRamoAtividadeAmostragemPCSSelecionados">Selecionados: <span class="itemContador" id="quantidadePontosAssociados"></span></label><br />
			<select id="listaRamoAtividadeAmostragemPCSSelecionados" class="campoList listaCurta2a campoVertical" name="ramoAtividadeAmostragemPCS" multiple="multiple" 
				onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeAmostragemPCSSelecionados,document.forms[0].listaRamoAtividadeAmostragemPCSDisponiveis,true);">
				<c:forEach items="${listaRamoAtividadeAmostragemPCSSelecionados}" var="ramoAtividadeAmostragemPCS">
					<option value="<c:out value="${ramoAtividadeAmostragemPCS.chavePrimaria}"/>" title="<c:out value="${ramoAtividadeAmostragemPCS.chavePrimaria}"/>"> <c:out value="${ramoAtividadeAmostragemPCS.descricao}"/></option>
				</c:forEach>
			</select>
						
			<fieldset class="conteinerBotoesCampoList2">
				<input type="button" class="bottonRightCol botoesLargosIE7" value="Para cima" onclick="moveOptionUp(this.form['listaRamoAtividadeAmostragemPCSSelecionados']);">
				<input type="button" class="bottonRightCol botoesLargosIE7" value="Para baixo" onclick="moveOptionDown(this.form['listaRamoAtividadeAmostragemPCSSelecionados']);">
			</fieldset>
			
		</fieldset>
		
		<br/><br/>
		
		<fieldset class="conteinerLista1">
		    <legend class="conteinerBlocoTitulo">Intervalo de Recupera��o do PCS</legend>
			<a name="listaIntervaloPCS"></a>
			<label class="rotulo rotuloVertical" id="rotulolistaRamoAtividadeIntervaloPCSDisponiveis" for="listaRamoAtividadeIntervaloPCSDisponiveis">Dispon�veis:</label><br />
			<select id="listaRamoAtividadeIntervaloPCSDisponiveis" class="campoList listaCurta1a campoVertical" name="listaRamoAtividadeIntervaloPCSDisponiveis" multiple="multiple"  
			onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeIntervaloPCSDisponiveis,document.forms[0].listaRamoAtividadeIntervaloPCSSelecionados,true);">
				<c:forEach items="${listaRamoAtividadeIntervaloPCSDisponiveis}" var="intervaloPCS">
					<option value="<c:out value="${intervaloPCS.chavePrimaria}"/>" title="<c:out value="${intervaloPCS.chavePrimaria}"/>"> <c:out value="${intervaloPCS.descricao}"/></option>		
				</c:forEach>
			</select>
			
			<fieldset class="conteinerBotoesCampoList1a">
				<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol2" id="botaoDireita" 
					onClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeIntervaloPCSDisponiveis,document.forms[0].listaRamoAtividadeIntervaloPCSSelecionados,true);">
				<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].listaRamoAtividadeIntervaloPCSDisponiveis,document.forms[0].listaRamoAtividadeIntervaloPCSSelecionados,true);">
				<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
					onClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeIntervaloPCSSelecionados,document.forms[0].listaRamoAtividadeIntervaloPCSDisponiveis,true);">
				<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].listaRamoAtividadeIntervaloPCSSelecionados,document.forms[0].listaRamoAtividadeIntervaloPCSDisponiveis,true);">
			</fieldset>
		</fieldset>
		
		<fieldset class="conteinerLista2" id="camposConteinerIntervaloPCSSelecionados">
			<label id="rotuloIntervaloPCSSelecionados" class="rotulo rotuloListaLonga2" for="listaRamoAtividadeIntervaloPCSSelecionados">Selecionados: <span class="itemContador" id="quantidadeIntervaloPCSSelecionados"></span></label><br />
			<select id="listaRamoAtividadeIntervaloPCSSelecionados" class="campoList listaCurta2a campoVertical" name="ramoAtividadeIntervaloPCS" multiple="multiple" 
				onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaRamoAtividadeIntervaloPCSSelecionados,document.forms[0].listaRamoAtividadeIntervaloPCSDisponiveis,true)">
				<c:forEach items="${listaRamoAtividadeIntervaloPCSSelecionados}" var="ramoAtividadeIntervaloPCS">
					<option value="<c:out value="${ramoAtividadeIntervaloPCS.chavePrimaria}"/>" title="<c:out value="${ramoAtividadeIntervaloPCS.chavePrimaria}"/>"> <c:out value="${ramoAtividadeIntervaloPCS.descricao}"/></option>
				</c:forEach>
			</select>
			
			<fieldset class="conteinerBotoesCampoList2">
				<input type="button" class="bottonRightCol botoesLargosIE7" value="Para cima" onclick="moveOptionUp(this.form['listaRamoAtividadeIntervaloPCSSelecionados']);">
				<input type="button" class="bottonRightCol botoesLargosIE7" value="Para baixo" onclick="moveOptionDown(this.form['listaRamoAtividadeIntervaloPCSSelecionados']);">
			</fieldset>
			</br>
			
		</fieldset>
	
	</fieldset>
	
	<label class="rotulo" for="tamanhoReducao">Tamanho da redu��o:</label>
	<input class="campoTexto campoHorizontal" id="consumoAlto" name="tamanhoReducaoRamo" type="text" size="4" maxlength="4" value="${ramoAtividadeAuxVO.tamanhoReducaoRamo}" onkeypress="return formatarCampoInteiro(event);" />
	<label class="rotuloInformativo" for="consumoAlto">dias</label><br class="quebraLinha2" />
