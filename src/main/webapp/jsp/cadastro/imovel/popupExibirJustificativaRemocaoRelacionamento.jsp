<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script>
$(document).ready(function(){				
	
	// Datepicker
	$(".campoData").datepicker({maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

	//Coloca o cursor no primeiro campo de pesquisa
	$("#idMotivoFimRelacionamentoClienteImovel").focus();

	/*Executa a pesquisa quando a tecla ENTER � pressionada, 
	caso o cursor esteja em um dos campos do formul�rio*/
	$(':text,select').keypress(function(event) {
		if (event.keyCode == '13') {
			confirmarRemocao();
		}
   	});
});	

function confirmarRemocao(){
	if(document.getElementById('idMotivoFimRelacionamentoClienteImovel').value == '-1' || document.getElementById('relacaoFim').value == ''){
		var camposNaoInformados = '';
      	var separador = '';
      	if(document.getElementById('idMotivoFimRelacionamentoClienteImovel').value == '-1'){
      		camposNaoInformados += 'Motivo do t�rmino da rela��o';
      		separador = ', ';
      	}
      	if(document.getElementById('relacaoFim').value == ''){
      		camposNaoInformados += separador + 'Fim do Relacionamento';
      		separador = ', ';      		
      	}    
      	
      	alert('<fmt:message key="ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS"><fmt:param>' + camposNaoInformados + '</fmt:param> </fmt:message>');  
	} else {
		window.opener.confirmarRemocaoCliente(document.getElementById('idMotivoFimRelacionamentoClienteImovel').value, document.getElementById('relacaoFim').value);
		window.close();
	}
}

</script>
	<h1 class="tituloInternoPopup">Justificativa de Fim de Relacionamento</h1>
 
<input name="acao" type="hidden" id="acao" value="pesquisarClienteSuperior">

<fieldset>
	<fieldset>
	<label class="rotulo" id="rotuloMotivoTermino" for="idMotivoFimRelacionamentoClienteImovel"><span class="campoObrigatorioSimbolo2">* </span>Motivo do t�rmino da rela��o:</label>
	<select name="idMotivoFimRelacionamentoClienteImovel" id="idMotivoFimRelacionamentoClienteImovel" class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMotivoFimRelacionamentoClienteImovel}" var="motivoFimRelacionamento">
			<option value="<c:out value="${motivoFimRelacionamento.chavePrimaria}"/>" <c:if test="${imovelForm.map.idMotivoFimRelacionamentoClienteImovel == motivoFimRelacionamento.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${motivoFimRelacionamento.descricao}"/>
			</option>		
		</c:forEach>
	</select>								
	<label class="rotulo campoObrigatorio" for="relacaoFim"><span class="campoObrigatorioSimbolo2">* </span>Fim do Relacionamento:</label><br/>
	<input type="text" id="relacaoFim" name="relacaoFim" class="campoData" value="${imovelForm.map.relacaoFim}" maxlength="10"/>
</fieldset>	
	<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para justificar o t�rmino do relacionamento.</p>
</fieldset>

<fieldset class="conteinerBotoesPopup">
    <input name="button" class="bottonRightCol2 botaoGrande1" value="Confirmar" type="button" onclick="confirmarRemocao();">
 </fieldset>