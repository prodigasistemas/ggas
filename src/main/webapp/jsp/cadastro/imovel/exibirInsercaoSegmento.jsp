<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Incluir Segmento<a class="linkHelp" href="<help:help>/incluiralterarsegmento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para incluir um Segmento, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form:form method="post" action="inserirSegmento" id="segmentoForm" name="segmentoForm">
	
	<script>
		$(document).ready(function(){			
			var mascara = formatarMascarasJSP("${mascaraNumeroConta}");
			$("input#contaContabil").inputmask(mascara,{placeholder:"_"});
		});
	
		function retornarTela(){
			$('#retornou').val(true);
			$('#operacao').val('incluir');
			submeter('segmentoForm', 'retornarTela');
		}
		
		function inserirSegmento(){
			selecionarListas();
			$('#operacao').val('incluir');
			submeter('segmentoForm', 'inserirSegmento');
		}
		
		function limparFormulario(){
			var form = document.segmentoForm;
			limparFormularios(form);
		}

		function cancelar() {
			location.href = '<c:url value="/pesquisarSegmentos"/>';
		}
		
		function selecionarContaContabil(idContaContabil, numeroConta){			
			var texto = numeroConta;
			document.getElementById("idContaContabil").value = idContaContabil;
			document.getElementById("contaContabil").value = texto;
			$(document).ready(function(){
				var mascara = formatarMascarasJSP("${mascaraNumeroConta}");
				$("input#contaContabil").inputmask(mascara,{placeholder:"_"});
			});
		}
		
	    function adicionarContaContabil() {
        	popup = window.open('exibirPopupPesquisaContaContabilSegmento','popup','height=500,width=750,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	    }
	    
		function selecionarListas() {
			var listaSegmentoAmostragemPCSDisponiveis = document.getElementById('listaSegmentoAmostragemPCSDisponiveis');		
			if (listaSegmentoAmostragemPCSDisponiveis != undefined) {
				for (i=0; i<listaSegmentoAmostragemPCSDisponiveis.length; i++){
					listaSegmentoAmostragemPCSDisponiveis.options[i].selected = true;
				}
			}
			var listaSegmentoAmostragemPCSSelecionados = document.getElementById('listaSegmentoAmostragemPCSSelecionados');		
			if (listaSegmentoAmostragemPCSSelecionados != undefined) {
				for (i=0; i<listaSegmentoAmostragemPCSSelecionados.length; i++){
					listaSegmentoAmostragemPCSSelecionados.options[i].selected = true;
				}
			}
			var listaSegmentoIntervaloPCSDisponiveis = document.getElementById('listaSegmentoIntervaloPCSDisponiveis');		
			if (listaSegmentoIntervaloPCSDisponiveis != undefined) {
				for (i=0; i<listaSegmentoIntervaloPCSDisponiveis.length; i++){
					listaSegmentoIntervaloPCSDisponiveis.options[i].selected = true;
				}
			}
			var listaSegmentoIntervaloPCSSelecionados = document.getElementById('listaSegmentoIntervaloPCSSelecionados');		
			if (listaSegmentoIntervaloPCSSelecionados != undefined) {
				for (i=0; i<listaSegmentoIntervaloPCSSelecionados.length; i++){
					listaSegmentoIntervaloPCSSelecionados.options[i].selected = true;
				}
			}
		}
		
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="inserirSegmento">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">
	<input type="hidden" name="idContaContabil" id="idContaContabil"/>
	<input type="hidden" name="idRamoAtividade" id="idRamoAtividade"/>
	<input type="hidden" name="operacao" id="operacao" />
	<input type="hidden" name="operacaoRamoAtividade" id="operacaoRamoAtividade" />
	<input type="hidden" name="executado" id="executado" value="false"/>	
	<input type="hidden" name="retornou" id="retornou" value="true" />
	
<fieldset id="conteinerSegmento" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<fieldset class="coluna detalhamentoColunaLarga">
				<legend class="conteinerBlocoTitulo">Dados Gerais</legend>
				<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
				<input class="campoTexto" id="descricao" name="descricao" type="text" size="15" maxlength="15" 
					onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" value="${segmentoForm.descricao}"><br />
				<label class="rotulo campoObrigatorio" for="descricaoAbreviada"><span class="campoObrigatorioSimbolo">* </span>Descri��o Abreviada:</label>
				<input class="campoTexto" id="descricaoAbreviada" name="descricaoAbreviada" type="text" size="3" maxlength="3"
					onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" value="${segmentoForm.descricaoAbreviada}"><br />
				<label class="rotulo campoObrigatorio" for="idTipoSegmento"><span class="campoObrigatorioSimbolo">* </span>Tipo do Segmento:</label>
				<select class="campoSelect" id="idTipoSegmento" name="tipoSegmento" >
			    	<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoSegmento}" var="tipoSegmento">
						<option value="<c:out value="${tipoSegmento.chavePrimaria}"/>" <c:if test="${segmentoForm.tipoSegmento.chavePrimaria == tipoSegmento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoSegmento.descricao}"/>
						</option>
				    </c:forEach>
			    </select>
			</fieldset>
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo rotulo2Linhas">Usa Programa��o de Consumo:</label>
			    <input  class="campoRadio" type="radio" value="1" name="usaProgramacaoConsumo" <c:if test="${segmentoForm.usaProgramacaoConsumo eq true}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="usaProgramacaoConsumo">Sim</label>
				<input  class="campoRadio" type="radio" value="0" name="usaProgramacaoConsumo" <c:if test="${segmentoForm.usaProgramacaoConsumo eq false or empty segmentoForm.usaProgramacaoConsumo}">checked="checked"</c:if>/>
				<label class="rotuloRadio" for="usaProgramacaoConsumo">N�o</label>	
			</fieldset>
		</fieldset>
			
		<fieldset id="tabs" style="display: none">
			<ul>
				<li><a href="#dadosFaturamento">Dados de Faturamento</a></li>
				<li><a href="#dadosMedicao">Dados de Medi��o</a></li>
				<li><a href="#ramoAtividade">Ramo de Atividade</a></li>		
			</ul>
			<fieldset class="conteinerAba" id="dadosFaturamento">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosFaturamentoSegmentoIncluir.jsp"></jsp:include>
			</fieldset>	
			<fieldset class="conteinerAba" id="dadosMedicao">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosMedicaoSegmentoIncluir.jsp"></jsp:include>
			</fieldset>	
			<fieldset class="conteinerAba" id="ramoAtividade">
				<jsp:include page="/jsp/cadastro/imovel/abaRamoAtividadeSegmentoIncluir.jsp"></jsp:include>			
			</fieldset>	
		</fieldset>	
		
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Segmento.</p>
		
		<fieldset class="conteinerBotoes">
			<input type="button" onclick="cancelar()" value="Cancelar" class="bottonRightCol2" name="Button">
		    <input type="button" onclick="limparFormulario();" value="Limpar" class="bottonRightCol2 bottonLeftColUltimo" name="Button">
		    <vacess:vacess param="inserirSegmento">
		    	<input id="botaoSalvar" type="button" value="Salvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" name="Button" onclick="inserirSegmento();">
		    </vacess:vacess>
		</fieldset>
	</fieldset>
</form:form>