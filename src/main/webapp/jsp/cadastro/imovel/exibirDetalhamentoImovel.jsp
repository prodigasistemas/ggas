<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Detalhar Im�vel<a href="<help:help>/cadastrodoimveldetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form method="post" action="exibirDetalhamentoImovel" enctype="multipart/form-data" id="imovelForm" name="imovelForm">

	<script type="text/javascript">

		function voltar(){	
			<% request.getSession().setAttribute("imovelPesquisaSessao", request.getAttribute("imovelPesquisa")); %>
			submeter("imovelForm", "voltarImovel");
		}
				
		function alterar(){		
			document.forms[0].postBack.value = false;		
			submeter('imovelForm', 'exibirAlteracaoImovel');
		}

		$(document).ready(function(){
			
			// Dialog			
			$("#cityGatePopup").dialog({
				autoOpen: false,				
				width: 333,
				modal: true,
				minHeight: 90,
				resizable: false
			});

			$("#tributoPopup").dialog({
				autoOpen: false,				
				width: 450,
				modal: true,
				minHeight: 90,
				resizable: false
			});	

		});
		
		function closePopup(){
			$("#cityGatePopup").dialog('close');
		}

		function closeTributoPopup(){
			$("#tributoPopup").dialog('close');
		}		

		function exibirCityGatePercentual(idPontoConsunmo) {

			if(idPontoConsunmo != ""){	      		
		       	AjaxService.obterPontoConsumoCityGate( idPontoConsunmo, { 
		           	callback: function(pontoConsumoCityGate) {
		           		var inner = '';
		           		var param = '';
		           		var div = 2;
		           		var start = 2;

		           		$('#corpoPercentualParticipacaoCityGate').html('');
		           		for(var key=0; key<pontoConsumoCityGate.length; key++) {
			           	
			               	if(start % div == 0){
								param = "odd";
				            }else{
								param = "even";
					        }
		               		inner = inner + '<tr class='+param+'><td>'+pontoConsumoCityGate[key][0]+'</td>';
		               		inner = inner + '<td>'+pontoConsumoCityGate[key][1]+' %</td>';
		               		inner = inner + '<td>'+pontoConsumoCityGate[key][2]+'</td></tr>';
		               		start = start + 1;					
		            	}
		               	$("#corpoPercentualParticipacaoCityGate").prepend(inner);
		        	}, async: false }
		        );
		    }
			exibirJDialog("#cityGatePopup");
		}

		function exibirTributos(idPontoConsunmo) {

			if(idPontoConsunmo != ""){	  
		       	AjaxService.obterPontoConsumoTributo( idPontoConsunmo, { 
		           	callback: function(pontoConsumoTributo) {
		           		var inner = '';
		           		var param = '';
		           		var div = 2;
		           		var start = 2;

		           		$('#corpoTributo').html('');
		               	for (key in pontoConsumoTributo){
			               	if(start % div == 0){
								param = "odd";
				            }else{
								param = "even";
					        }
					        
		               		inner = inner + '<tr class='+param+'>'
		               		inner = inner + '<td>' + pontoConsumoTributo[key][0] + '</td>';

							if (pontoConsumoTributo[key][1] == 'true') {
								inner = inner + '<td>Sim</td>';
							} else {
								inner = inner + '<td>N�o</td>';
							}

							if (pontoConsumoTributo[key][2] != 'null') {
								var strPorcentagem = pontoConsumoTributo[key][2];
								inner = inner + '<td>' +  strPorcentagem.replace(".", ",") + '%</td>';
							} else {
								inner = inner + '<td></td>';
							}

							if (pontoConsumoTributo[key][3] != 'null') {
								inner = inner + '<td>' + pontoConsumoTributo[key][3] + '</td>';
							} else {
								inner = inner + '<td></td>';
							}							

							if (pontoConsumoTributo[key][4] != 'null') {
								inner = inner + '<td>' + pontoConsumoTributo[key][4] + '</td>';
							} else {
								inner = inner + '<td></td>';
							}							

		               		inner = inner + '</tr>';
		               		start = start + 1;					
		            	}
		               	$("#corpoTributo").prepend(inner);
		        	}, async: false }
		        );
		    }
			exibirJDialog("#tributoPopup");
		}

		function gerarGrafico(chavePontoConsumo) {

			popup = window.open('exibirGraficoVolumeGSA?chavePontoConsumo=' + chavePontoConsumo, 'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

		}

		function irParaGoogleMaps(){
			var latitude = $("#latitude").val().replace(",", ".");
			var longitude = $("#longitude").val().replace(",", ".");

			if(latitude != "" && longitude != "") {
				var url = "https://www.google.com/maps/search/?api=1&query="+latitude+"%2C"+longitude;
				window.open(url, '_blank').focus();
			} else {
				alert("N�o existe latitude ou longitude cadastrada para o ponto de consumo.");
			}
		}		
	
	</script>

	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${imovel.chavePrimaria}">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="">
	<input name="latitude" type="hidden" id="latitude" value="${latitude}">
	<input name="longitude" type="hidden" id="longitude" value="${longitude}">
	
	<div id="cityGatePopup" title="City Gate">
		<table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDialog">
			<thead>
				<tr>
					<th>City Gate</th>
					<th style="width: 85px">Participa��o</th>
					<th style="width: 75px">In�cio da Vig�ncia</th>
				</tr>
			</thead>
			<tbody id="corpoPercentualParticipacaoCityGate"></tbody>
		</table>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Fechar" type="button" onclick="closePopup()">
	</div>
	
	<div id="tributoPopup" title="Exce��es Tribut�rias">
		<table class="dataTableGGAS dataTableDialog">
			<thead>
				<tr>
					<th>Tributo</th>
					<th style="width: 50px">Isento</th>
					<th style="width: 95px">Porcentagem</th>
					<th style="width: 95px">Data de In�cio</th>
					<th style="width: 95px">Data de Fim</th>
				</tr>
			</thead>
			<tbody id="corpoTributo"></tbody>
		</table>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Fechar" type="button" onclick="closeTributoPopup()">
	</div>	
	
	<fieldset class="detalhamento">
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Identifica��o e Localiza��o</legend>
			<fieldset id="detalhamentoImovelAbaIdLoc" class="coluna detalhamentoColunaExtraLarga">
				<label class="rotulo" id="rotuloNome">Matr�cula:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.chavePrimaria}"/></span><br />
				<label class="rotulo" id="rotuloNome">Descri��o:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.nome}"/></span><br />
				<label class="rotulo" id="rotuloNome">Endere�o:</label>
				<span class="itemDetalhamento"><c:out value="${enderecoFormatado}"/></span>
				<a href="javascript:irParaGoogleMaps();"><img class="alinhamentoGoogleMaps" style="display:inline-block;height: 50px;float: right;" border="0" src="<c:url value="/imagens/google-maps.png"/>"/></a>				
				<br />				
				<label class="rotulo" id="rotuloCnpjTexto">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.cep}"/></span><br />
				<label class="rotulo" id="rotuloNome">Complemento:</label>
				<span id="detalhamentoComplemento" class="itemDetalhamento"><c:out value="${imovel.descricaoComplemento}"/></span><br />
				<label class="rotulo">Refer�ncia:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.enderecoReferencia}"/></span>
				<label class="rotulo rotulo2Linhas">Previs�o de Encerramento:</label>
				<span class="itemDetalhamento"><c:out value="${dataPrevisaoEncerramentoObra}"/></span>
			</fieldset>	
			<fieldset id="detalhamentoImovelAbaIdLoc2" class="colunaFinal">
				<label class="rotulo">Setor:</label>
				<span class="itemDetalhamento"><c:out value="${requestScope.descricaoSetor}"/></span><br />
				<label class="rotulo">Quadra:</label>
				<span class="itemDetalhamento"><c:out value="${requestScope.numeroQuadra}"/></span><br />
				<label class="rotulo">Face da Quadra:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.quadraFaceFormatada}"/></span><br />
				<label class="rotulo" id="rotuloNumeroLote">N�mero do Lote:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.numeroLote}"/></span><br />
				<label class="rotulo" id="rotuloNumeroSublote">N�mero do Sublote:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.numeroSublote}"/></span><br />
				<label class="rotulo" id="rotuloTestadaLote">Testada do Lote:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.numeroTestada}"/></span><br />
				<label class="rotulo">Situa��o do Im�vel:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.situacaoImovel.descricao}"/></span>
				<label class="rotulo">Rota Prevista:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.rota}"/></span>
				<label class="rotulo">Agente:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.agente.funcionario.nome}"/></span>
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Caracter�sticas</legend>
			<fieldset id="detalhamentoCaracteristicasCol1" class="coluna detalhamentoColunaExtraLarga">
				<label class="rotulo" id="rotuloTipoPavimentoCalcada">Pavimento da Cal�ada:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.pavimentoCalcada.descricao}"/></span><br />
				<label class="rotulo" id="rotuloTipoPavimentoRua">Pavimento da Rua:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.pavimentoRua.descricao}"/></span><br />
				<label class="rotulo" id="rotuloPadraoConstrucao">Padr�o de constru��o:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.padraoConstrucao.descricao}"/></span><br />
				<label class="rotulo" id="rotuloImovelCondominio">O im�vel � condom�nio?</label>
				<c:choose>
					<c:when test="${imovel.condominio eq true}">
						<span class="itemDetalhamento">Sim</span>
					</c:when>
					<c:otherwise>
						<span class="itemDetalhamento">N�o</span>
					</c:otherwise>
				</c:choose>
				<label class="rotulo" id="rotuloTipoCombustivel">Tipo de Combust�vel</label>
				<span class="itemDetalhamento"><c:out value="${imovel.tipoCombustivel.descricao}"/></span>
				<c:if test="${imovel.condominio}">
				<fieldset id="detalhamentoDadosCondominio">
					<legend>Dados do Condom�nio</legend>
					<div class="conteinerDados">											
						<label class="rotulo" id="rotuloModalidademMedicao">Modalidade de Medi��o:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.modalidadeMedicaoImovel.descricao}"/></span><br />
						<label class="rotulo" id="rotuloTipoBotijao">Tipo de Cilindro:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.tipoBotijao.descricao}"/></span><br />
						<label class="rotulo" id="rotuloNumeroBlocos">N� de Blocos/Torres:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.quantidadeBloco}"/></span><br />
						<label class="rotulo" id="rotuloNumeroAndares">N� de Andares:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.quantidadeAndar}"/></span><br />
						<label class="rotulo" id="rotuloNumeroAptoPorAndar">N� de Apartamentos por Andar:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.quantidadeApartamentoAndar}"/></span><br />
						<label class="rotulo" id="rotuloExisteValvulaBloqueio">Existe v�lvula de bloqueio?</label>
						<span class="itemDetalhamento"><c:out value="${existeValvulaBloqueio}"/></span><br />
						<label class="rotulo" id="rotuloDataEntrega">Previs�o de Entrega:</label>						
						<span class="itemDetalhamento"><c:out value="${dataEntrega}"/></span><br />
						<label class="rotulo" id="rotuloConstrutora">Construtora:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.empresa.cliente.nome}"/></span><br />
						<label class="rotulo" id="rotuloPossuiPortaria">Condom�nio Possui portaria?</label>
						<span class="itemDetalhamento"><c:out value="${possuiPortaria}"/></span><br />
					</div>
				</fieldset>
			</c:if>
			</fieldset>
			<fieldset id="detalhamentoCaracteristicasCol2" class="colunaFinal">				
				<label class="rotulo" id="rotuloFaixaAreaConstruda">Faixa de �rea Constru�da:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.areaConstruidaFaixa.menorFaixa}"/> - <c:out value="${imovel.areaConstruidaFaixa.maiorFaixa}"/></span><br />
				<label class="rotulo" id="rotuloPerfilImovel">Perfil do Im�vel:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.perfilImovel.descricao}"/></span>
				
				<label class="rotulo" id="rotuloQtdBanheiro">Quantidade de banheiros:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.quantidadeBanheiro}"/></span>
				
				<label class="rotulo">Obra feita em im�vel novo:</label>
				<c:choose>
					<c:when test="${imovel.indicadorObraTubulacao eq true}">
						<span class="itemDetalhamento">Sim</span>
					</c:when>
					<c:otherwise>
						<span class="itemDetalhamento">N�o</span>
					</c:otherwise>
				</c:choose>
				
				<label class="rotulo rotulo2Linhas">Possui Restri��o de Servi�o? </label>
				<c:choose>
					<c:when test="${imovel.indicadorRestricaoServico eq true}">
						<span class="itemDetalhamento">Sim</span>
					</c:when>
					<c:otherwise>
						<span class="itemDetalhamento">N�o</span>
					</c:otherwise>
				</c:choose>
				
				<label class="rotulo" id="rotuloExisteRedeInterna">Im�vel possui rede interna?</label>
				<c:choose>
					<c:when test="${imovel.redePreexistente eq true}">
						<span class="itemDetalhamento">Sim</span>
					</c:when>
					<c:otherwise>
						<span class="itemDetalhamento">N�o</span>
					</c:otherwise>
				</c:choose>
				<c:if test="${imovel.redePreexistente}">
					<fieldset id="detalhamentoDadosRedeInterna">
						<legend>Dados da Rede Interna</legend>
						<div class="conteinerDados">							
							<label class="rotulo" id="rotuloMaterialRede">Material da rede:</label>
							<span class="itemDetalhamento"><c:out value="${imovel.redeInternaImovel.redeMaterial.descricao}"/></span><br />
							<label class="rotulo" id="rotuloQuantPrumadas">Quantidade de prumadas:</label>
							<span class="itemDetalhamento"><c:out value="${imovel.redeInternaImovel.quantidadePrumada}"/></span><br />
							<label class="rotulo rotulo2Linhas" id="rotuloDiamRedeTrechoPrimarioPrumada">Di�metro da rede no trecho prim�rio da prumada:</label>
							<span class="itemDetalhamento2Linhas"><c:out value="${imovel.redeInternaImovel.redeDiametroCentral.descricao}"/></span><br />
							<label class="rotulo rotulo2Linhas" id="rotuloDiamRedePrumadas">Di�metro da rede nas prumadas:</label>
							<span class="itemDetalhamento2Linhas"><c:out value="${imovel.redeInternaImovel.redeDiametroPrumada.descricao}"/></span><br />
							<label class="rotulo" id="rotuloDiametroRedeSecundaria">Di�metro da rede secund�ria:</label>
							<span class="itemDetalhamento"><c:out value="${imovel.redeInternaImovel.redeDiametroPrumadaApartamento.descricao}"/></span><br />
							<label class="rotulo rotulo2Linhas" id="rotuloQuantReguladoresPressao">Quantidade de<br />reguladores de press�o:</label>
							<span class="itemDetalhamento2Linhas"><c:out value="${imovel.redeInternaImovel.quantidadeReguladorHall.toString()}"/></span><br />
							<label class="rotulo" id="rotuloHallPossuiVentilacao">O hall possui ventila��o?</label>
							<span class="itemDetalhamento"><c:out value="${possuiVentilacaoHall}"/></span><br />
							<label class="rotulo" id="rotuloVentilacaoPermanente">A ventila��o � permanente?</label>
							<span class="itemDetalhamento"><c:out value="${possuiVentilacaoApartamento}"/></span><br />
						</div>
				</fieldset>
				</c:if>	
			</fieldset>			
		</fieldset>
		
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Relacionamentos</legend>
			<display:table class="dataTableGGAS" name="relacionamentosCliente" sort="list" id="clienteImovel" pagesize="15" excludedParams="" requestURI="#" >
				<display:column property="cliente.nome" sortable="false" title="Cliente" style="width: 247px" />
				<display:column sortable="false" title="CNPJ/CPF" style="width: 125px">
				<c:choose>
			      		<c:when test="${clienteImovel.cliente.cpf ne null && clienteImovel.cliente.cpf ne ''}">
			      			${clienteImovel.cliente.cpfFormatado}
					</c:when>
					<c:otherwise>
			      			${clienteImovel.cliente.cnpjFormatado}
					</c:otherwise>
				</c:choose>
		   		</display:column>
		   		
		   			   		
		   			<display:column sortable="false" title="Endere�o" style="width: 125px">
				<c:choose>
			      		<c:when test="${clienteImovel.cliente.enderecoPrincipal ne null && clienteImovel.cliente.enderecoPrincipal ne ''}">
			      			${clienteImovel.cliente.enderecoPrincipal.enderecoFormatado}
					</c:when>
					<c:otherwise>
			      			
					</c:otherwise>
				</c:choose>
		   		</display:column>
		   		
		   		<display:column property="tipoRelacionamentoClienteImovel.descricao" sortable="false" title="Relacionamento" style="width: 130px" />
		   		<display:column sortable="false" title="Data Inicial" style="width: 75px">
		   			<fmt:formatDate value="${clienteImovel.relacaoInicio}" type="date" pattern="dd/MM/yyyy" />
		   		</display:column>
		   		<display:column sortable="false" title="Data Final" style="width: 75px">
		   			<c:if test="${clienteImovel.relacaoFim ne null}">
		   				<fmt:formatDate value="${clienteImovel.relacaoFim}" type="date" pattern="dd/MM/yyyy" />
		   			</c:if>
		   		</display:column>
		   	</display:table>
		</fieldset>
		
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Contatos</legend>
			<display:table class="dataTableGGAS" name="contatos" sort="list" id="contato" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
				<display:column property="tipoContato.descricao" sortable="false"  title="Tipo" />
				<display:column property="profissao.descricao" sortable="false"  title="Profissao" />
				<display:column property="nome" sortable="false"  title="Nome" />
				<display:column property="codigoDDD" sortable="false"  title="DDD" />
				<display:column property="fone" sortable="false"  title="Telefone" />
				<display:column property="ramal" sortable="false"  title="Ramal" />
				<display:column property="codigoDDD2" sortable="false"  title="DDD(Opcional)" />
				<display:column property="fone2" sortable="false"  title="Telefone(Opcional)" />
				<display:column property="ramal2" sortable="false"  title="Ramal(Opcional)" />		
				<display:column property="descricaoArea" sortable="false"  title="�rea" />
				<display:column property="email" sortable="false"  title="Email" />
		        
		        <display:column sortable="false"  title="Principal" >
		   			<c:choose>
			      		<c:when test="${contato.principal}">
			      			Sim
			      		</c:when>
			      		<c:otherwise>
			      			N�o
			      		</c:otherwise>
			      	</c:choose>
		   		</display:column>	   		
		   	</display:table>
		</fieldset>
		
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Unidades Consumidoras</legend>
			<display:table class="dataTableGGAS" name="unidadesConsumidoras" sort="list" id="imovelRamoAtividade" pagesize="15" excludedParams="" requestURI="#" >
				<display:column property="ramoAtividade.segmento.descricao" sortable="false" title="Segmento" />
				<display:column property="ramoAtividade.descricao" sortable="false" title="Ramo de atividade" />
				<display:column property="quantidadeEconomia" sortable="false" title="Quantidade" />
		   	</display:table>
		</fieldset>
		
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Pontos de Consumo</legend>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="pontosDeConsumo" sort="list" id="pontoConsumo" pagesize="15" excludedParams="" requestURI="#" >
				<display:column sortable="false" title="Descri��o">
					<div style="width: 170px; word-wrap: break-word; text-align: left; padding-left: 5px; padding-right: 5px; overflow: hidden">
						<c:out value="${pontoConsumo.chavePrimaria}"/><c:out value=" - "/><c:out value="${pontoConsumo.descricao}"/>
					</div>
				</display:column>
				<display:column property="segmento.descricao" sortable="false" title="Segmento" style="width: 100px" />
				<display:column property="ramoAtividade.descricao" sortable="false" title="Ramo de<br />Atividade"  style="width: 100px" />
				<display:column property="cep.cep" sortable="false" title="Cep" style="width: 75px" />
				<display:column property="numeroImovel" sortable="false" title="N�mero" style="width: 50px" />
				<display:column sortable="false" title="Rota">
					<div style="width: 115px; word-wrap: break-word; text-align: center; padding-left: 5px; padding-right: 5px; overflow: hidden"><c:out value="${pontoConsumo.rota.numeroRota}"></c:out></div>
				</display:column>
				<display:column style="width: 100px; text-align: center;" sortable="false" title="C�digo �nico">
					<div><c:out value="${pontoConsumo.codigoLegado}"></c:out></div>
				</display:column>	
				<display:column sortable="false" title="Endere�o Remoto">
					<div style="width: 80px; word-wrap: break-word; text-align: left; padding-left: 5px; padding-right: 5px; overflow: hidden"><c:out value="${pontoConsumo.codigoPontoConsumoSupervisorio}"></c:out></div>
				</display:column>				
				<display:column title="City Gate" style="width: 30px">
					<a href="javascript:exibirCityGatePercentual('<c:out value="${pontoConsumo.chavePrimaria}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
				</display:column>				
				<display:column title="Tributos" style="width: 60px">
					<a href="javascript:exibirTributos('<c:out value="${pontoConsumo.chavePrimaria}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
				</display:column>
				<display:column title="Gr�fico</br>Volume Faturado" style="width: 100px">
					<a href="javascript:gerarGrafico(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
				</display:column>
				<display:column sortable="false" title="Indicador de uso">
						<c:if test="${pontoConsumo.habilitado eq 'true'}">ATIVO</c:if>
						<c:if test="${pontoConsumo.habilitado eq 'false'}">INATIVO</c:if>
				</display:column>
				<c:if test="${imovel.condominio}">
				<display:column style="width: 115px; text-align: center;" sortable="false" title="Situa��o<br/>Ponto de Consumo">
					<div><c:out value="${pontoConsumo.situacaoConsumo.descricao}"></c:out></div>
				</display:column>
				</c:if>					
		   	</display:table>
		</fieldset>	
		
		<c:if test="${imovel.condominio}">
			<hr class="linhaSeparadoraDetalhamento" />
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Im�veis Associados:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="imoveisAssociados" sort="list" id="imovelAssociado" pagesize="15" excludedParams="" requestURI="#" >
					<display:column property="chavePrimaria" sortable="false" title="Matricula" style="width: 130px" />
					<display:column property="nome" sortable="false" title="Descri��o" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px" />
					<display:column property="numeroImovel" sortable="false" title="N�mero" style="width: 90px" />
					<display:column property="situacaoImovel.descricao" sortable="false" title="Situa��o Im�vel" style="width: 130px" />
			   	</display:table>
			</fieldset>	
		</c:if>
	
	</fieldset>
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
	    
	    <vacess:vacess param="exibirAlteracaoImovel">
	    	<c:if test="${locked == false}">
	    	</c:if>
	    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
	    </vacess:vacess>
	</fieldset>
	
</form:form>
