<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>


</script>
<fieldset class="conteinerPesquisarIncluirAlterar">

	<fieldset class="coluna colunaMedia">
	<label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Ano Refer�ncia:</label>
					<select id="anoReferencia" class="campoSelect" name="anoReferencia">
						<c:forEach items="${listaAnoReferencia}" var="anoReferencia">
						<option value="<c:out value="${anoReferencia}"/>" >
							<c:out value="${anoReferencia}"/>
						</option>
						</c:forEach>
					</select><br/>	
		<label class="rotulo" for="numeroCiclos">Previs�o Janeiro:</label>
        <input class="campoTexto" id="previsaoJaneiro" name="previsaoJaneiro" type="text" size="4" maxlength="4" value="${segmento.previsaoJaneiro}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Fevereiro:</label>
        <input class="campoTexto" id="previsaoFevereiro" name="previsaoFevereiro" type="text" size="4" maxlength="4" value="${segmento.previsaoFevereiro}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Mar�o:</label>
        <input class="campoTexto" id="previsaoMarco" name="previsaoMarco" type="text" size="4" maxlength="4" value="${segmento.previsaoMarco}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Abril:</label>
        <input class="campoTexto" id="previsaoAbril" name="previsaoAbril" type="text" size="4" maxlength="4" value="${segmento.previsaoAbril}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Maio:</label>
        <input class="campoTexto" id="previsaoMaio" name="previsaoMaio" type="text" size="4" maxlength="4" value="${segmento.previsaoMaio}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Junho:</label>
        <input class="campoTexto" id="previsaoJunho" name="previsaoJunho" type="text" size="4" maxlength="4" value="${segmento.previsaoJunho}" onkeypress="return formatarCampoInteiro(event);"><br/>
	</fieldset>
	<fieldset class="colunaFinal ">
	<label class="rotulo" for="numeroCiclos">Previs�o Julho:</label>
        <input class="campoTexto" id="previsaoJulho" name="previsaoJulho" type="text" size="4" maxlength="4" value="${segmento.previsaoJulho}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Agosto:</label>
        <input class="campoTexto" id="previsaoAgosto" name="previsaoAgosto" type="text" size="4" maxlength="4" value="${segmento.previsaoAgosto}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Setembro:</label>
        <input class="campoTexto" id="previsaoSetembro" name="previsaoSetembro" type="text" size="4" maxlength="4" value="${segmento.previsaoSetembro}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Outubro:</label>
        <input class="campoTexto" id="previsaoOutubro" name="previsaoOutubro" type="text" size="4" maxlength="4" value="${segmento.previsaoOutubro}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Novembro:</label>
        <input class="campoTexto" id="previsaoNovembro" name="previsaoNovembro" type="text" size="4" maxlength="4" value="${segmento.previsaoNovembro}" onkeypress="return formatarCampoInteiro(event);"><br/>
		<label class="rotulo" for="numeroCiclos">Previs�o Dezembro:</label>
        <input class="campoTexto" id="previsaoDezembro" name="previsaoDezembro" type="text" size="4" maxlength="4" value="${segmento.previsaoDezembro}" onkeypress="return formatarCampoInteiro(event);"><br/>
		
	</fieldset>
</fieldset>