<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script language="javascript">

	$(document).ready(function(){	

		window.opener.esconderIndicador();
		
		//Coloca o cursor no primeiro campo de pesquisa
		$("#nomeCliente").focus();

		/*Executa a pesquisa quando a tecla ENTER � pressionada, 
		caso o cursor esteja em um dos campos do formul�rio*/
		$('#pesquisarImovelCompletoPopup>:text, #pesquisarImovelCompletoPopup>:radio').keypress(function(event) {
			if (event.keyCode == '13') {
				pesquisarCliente();
			}
	   	});
	});
	
	function selecionarNoLink(id) {
		window.opener.selecionarImovel(id);
		window.close();
	}
	
	function selecionar(elem){
		document.getElementById('idSelecionado').value = elem.value;	
	}
	
	function selecionarImovel() {
		window.opener.selecionarImovel(document.getElementById('idSelecionado').value);
		window.close();
	}
	
	function limparFormulario() {
		document.getElementById('nomeCliente').value = '';
		document.getElementById('nomeFantasia').value = '';
		document.getElementById('cepImovel').value = '';	
	}

	function pesquisarImovel() {
		$("#botaoPesquisar").attr('disabled','disabled');
		submeter('pesquisaImovelForm', 'pesquisarImovelPopup');
  	}

	function init() {
		<c:if test="${listaImoveis ne null}">
			$.scrollTo($('#imovel'),800);
		</c:if>
	}
	addLoadEvent(init);

</script>

	<h1 class="tituloInternoPopup">Pesquisar Im�vel</h1>
	<p class="orientacaoInicialPopup">Para pesquisar um Im�vel, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form method="post" action="pesquisarImovelPopup" id="pesquisaImovelForm" name="pesquisaImovelForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarImovelPopup">
	<input name="idSelecionado" type="hidden" id="idSelecionado" value="">
	
	<fieldset id="pesquisarImovelPopup">
		<label class="rotulo" id="rotuloNomeCliente" for="nomeCliente">Nome do Cliente:</label>
		<input class="campoTexto" name="nomeCliente" id="nomeCliente" maxlength="50" size="50" type="text" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${imovelPesquisaVO.nomeCliente}"><br />
		<label class="rotulo" id="rotuloNomeFantasia" for="nomeFantasia">Descri��o:</label>
		<input class="campoTexto" name="nomeFantasia" id="nomeFantasia" maxlength="50" size="50" type="text" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"  value="${imovelPesquisaVO.nomeFantasia}" /><br />
		<fieldset class="exibirCep">
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="cepObrigatorio" value="false"/>
				<jsp:param name="idCampoCep" value="cepImovel"/>
				<jsp:param name="numeroCep" value="${imovelPesquisaVO.cepImovel}"/>
			</jsp:include>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparFormulario();">
	    <input name="button" class="bottonRightCol2 botaoGrande1" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarImovel();">
	 </fieldset>
	
	 <p>&nbsp;</p>
	 
	<c:if test="${listaImoveis ne null}">
		<display:table class="dataTableGGAS dataTablePopup" name="listaImoveis" id="imovel" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarImovelPopup">
	        <display:column style="width: 70px" title="Matr�cula" sortable="true" sortProperty="chavePrimaria">
	     		<a href='javascript:selecionarNoLink(<c:out value='${imovel.chavePrimaria}'/>);'>
					<c:out value='${imovel.chavePrimaria}'/>
				</a>
		    </display:column>
	     	<display:column style="width: 230px" sortable="true" sortProperty="nome" title="Descri��o" class="colunaConteudoExtenso">
    			<a href='javascript:selecionarNoLink(<c:out value='${imovel.chavePrimaria}'/>);'>
					<c:out value='${imovel.nome}'/>
       			</a>
		    </display:column>
			<display:column sortable="true" title="Endere�o" sortProperty="enderecoFormatado">
		    	<a href='javascript:selecionarNoLink(<c:out value='${imovel.chavePrimaria}'/>);'>
		    		<c:out value='${imovel.enderecoFormatado}'/>
		    	</a>
		    </display:column>
		    <display:column sortable="true" title="Tipo Medi��o" sortProperty="modalidadeMedicaoImovel" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq" style="width: 100px">
		    	<a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    		<c:choose>
		    			<c:when test="${imovel.imovelCondominio ne null}">
				    		<c:out value='${imovel.imovelCondominio.modalidadeMedicaoImovel.descricao}'/>
		    			</c:when>
		    			<c:otherwise>
				    		<c:out value='${imovel.modalidadeMedicaoImovel.descricao}'/>
		    			</c:otherwise>
		    		</c:choose>
		    	</a>
		    </display:column>
	  	</display:table>
	 </c:if>
 	<a name="pesquisaImovelResultados"></a>
</form>
