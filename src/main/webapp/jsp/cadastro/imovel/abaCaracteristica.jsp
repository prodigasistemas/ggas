<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>

<style>
#dataEntrega+.ui-datepicker-trigger {
	position: absolute;
	top: 387px;
	left: 260px;
}
</style>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'>
	
</script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'>
	
</script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<script>
var desabilitaCamposPai = false
	$(document).ready(function() {
		<c:choose>
		<c:when test="${imovelVO.condominio ne true}">
		$("#ImovelAbaCaracteristicas3").hide();
		</c:when>
		<c:otherwise>
		$("#ImovelAbaCaracteristicas3").show();
		</c:otherwise>
		</c:choose>

		<c:choose>
		<c:when test="${imovelVO.redePreexistente ne true}">
		$("#dadosRedeInterna").hide();
		</c:when>
		<c:otherwise>
		exibirEsconderDeslizante("dadosRedeInterna", "exibir");
		</c:otherwise>
		</c:choose>

		<c:choose>
		<c:when test="${imovelVO.indicadorRestricaoServico ne true}">
		$("#divRestricoesServicoTipo").hide();
		</c:when>
		<c:otherwise>
		exibirEsconderDeslizante("divRestricoesServicoTipo", "exibir");
		</c:otherwise>
		</c:choose>

	});

	function esconderDadosCondominio() {
		exibirEsconderDeslizante("ImovelAbaCaracteristicas3", "esconder");
	}

	function exibirDadosCondominio() {
		exibirEsconderDeslizante("ImovelAbaCaracteristicas3", "exibir");
		desmarcaImovelPai()
		
        
        $('#imovelPaiNao').attr('checked','true')
        $('#imovelPaiSim').removeAttr('checked')
	}

	function esconderDadosRedeInterna() {
		exibirEsconderDeslizante("dadosRedeInterna", "esconder");
	}

	function exibirDadosRedeInterna() {
		exibirEsconderDeslizante("dadosRedeInterna", "exibir");
	}

	function exibirRestricaoServicoTipo() {
		exibirEsconderDeslizante("restricoesServicoTipo", "exibir");
	}

	function esconderRestricaoServicoTipo() {
		exibirEsconderDeslizante("restricoesServicoTipo", "esconder");
	}

	function limparCaracteristicas() {
		$("#caracteristicas select").val("-1");
		$("#caracteristicas :text").val("");
		$("#condominioNao, #redePreexistenteNao").attr("checked", "checked");
		esconderDadosCondominio();
		esconderDadosRedeInterna();		
		esconderRestricaoServicoTipo();
		document.forms[0].indicadorRestricaoServico[1].checked = true;		
		moveAllOptions(document.forms[0].idsServicoTipoRestricaoSalvos,document.forms[0].idsServicoTipo,true);
		
	}
	
	

	function selecionarImovel(idSelecionado) {
		if (idSelecionado != '') {
			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {
										$('#matriculaImovelPai').val(imovel["matriculaImovel"]);
										$('#nomeFantasiaImovelPai').val(imovel["nomeFantasia"]);
										$('#numeroImovelPai').val(imovel["numeroImovel"]);
										$('#cidadeImovelPai').val(imovel["cidadeImovel"]);

										if ($('#cepImovel').val() != imovel["cepImovel"]) {
											$('#idQuadraImovel').val('')
											$('#idQuadraFace').val('')
										}

										$('#cepImovel').val(imovel["cepImovel"]);
										$('#cepPontosConsumo').val(imovel["cepImovel"]);
										
										$('#numeroImovel').val(imovel["numeroImovel"]);
										$('#numeroImovelPontoConsumo').val(imovel["numeroImovel"]);
										
										if (desabilitaCamposPai){
											$("#cepImovel").prop("readonly", true);
											$('#numeroImovel').prop("readonly",true);
											$('#cepImovel').addClass("campoDesabilitado")
											$('#numeroImovel').addClass("campoDesabilitado")
											// $('#condominioSim').attr('disabled', 'true')
											//  $('#condominioNao').attr('disabled', 'true')
										}
										
										if (imovel["indicadorCondominioAmbos"] == 'true') {
											$("#indicadorCondominioImovelTexto1").attr('checked', 'true')
											$("#indicadorCondominioImovelTexto2").removeAttr('checked')
											$("#indicadorCondominioImovelPai").val('true')

											$("#condominioNao").attr('checked','true')
											$('#condominioSim').removeAttr('checked')
											// $('#areaImovelCondominio').hide()
											//    $('#legendaCaracteristica').hide()

										} else {
											$("#indicadorCondominioImovelTexto1").removeAttr('checked')
											$("#indicadorCondominioImovelTexto2").attr('checked', 'true')
											$('#indicadorCondominioImovelPai').val('false')

											$("#condominioNao").removeAttr('checked')
											$('#condominioSim').attr('checked','true')
											// $('#areaImovelCondominio').hide()
											//    $('#legendaCaracteristica').hide()
										}
										var cep = $('#cepImovel').val()
										var selectQuadras = document.getElementById("idQuadraImovel");
										
										if ((cep != undefined)
												&& (trim(cep) != '')
												&& (cep[8] != '_')) {

											
											var selectQuadras = document.getElementById("idQuadraImovel");
											var selectQuadrasPontoConsumo = document.getElementById("idQuadraPontoConsumo");
											var selectQuadraFacePontoConsumo = document.getElementById("idQuadraFacePontoConsumo");
											selectQuadrasPontoConsumo.disabled = false;
											selectQuadraFacePontoConsumo.disabled = false;
									      	$("#idQuadraPontoConsumo").removeClass("campoDesabilitado");    
									    	$("#idQuadraFacePontoConsumo").removeClass("campoDesabilitado");   
									      	
									        
											var selectQuadraFace = document.getElementById("idQuadraFace");
											var selectSituacaoImovel = document.getElementById("idSituacao");
											
											if (desabilitaCamposPai){
										      	$("#idQuadraImovel").addClass("campoDesabilitado");
										      	$("#idQuadraImovel").prop("readonly", true);
										      	//$("#idQuadraImovel").removeAttr("disabled");
										      	$("#idQuadraFace").addClass("campoDesabilitado");
										      	$("#idQuadraFace").prop("readonly", true);
										      	//$("#idQuadraFace").removeAttr("disabled");
										      	
										      	$("#selectSituacaoImovel").addClass("campoDesabilitado");
										      	$("#selectSituacaoImovel").prop("readonly", true);
										      	//$("#selectSituacaoImovel").removeAttr("disabled");
											}else{
												$("#idQuadraImovel").removeClass("campoDesabilitado");
										      	$("#idQuadraImovel").prop("readonly", false);
										      	$("#idQuadraImovel").removeAttr("disabled");
										       	$("#idQuadraFace").removeClass("campoDesabilitado");
										      	$("#idQuadraFace").prop("readonly", true);
										      	$("#idQuadraFace").removeAttr("disabled");
										      	
										      	$("#selectSituacaoImovel").removeClass("campoDesabilitado");
										      	$("#selectSituacaoImovel").prop("readonly", true);
										      	$("#selectSituacaoImovel").removeAttr("disabled");
											}
									      	
									    	
									      	
											AjaxService.consultarQuadrasPorIdCep(imovel["�dCepImovel"],{
																callback : function(
																		quadras) {
																	for (key in quadras) {
																		var novaOpcao = new Option(
																				quadras[key],
																				key);
																		var novaOpcaoPontoConsumo = new Option(
																				quadras[key],
																				key);
																		selectQuadras.options[selectQuadras.length] = novaOpcao;
																		selectQuadrasPontoConsumo.options[selectQuadrasPontoConsumo.length] = novaOpcaoPontoConsumo;
																	}
																 
																	 
																	$("#idQuadraImovel").val(imovel['idQuadraImovel'])
																	$("#idQuadraPontoConsumo").val(imovel['idQuadraImovel'])
																	
																	AjaxService.consultarFacesQuadraPorQuadraIdCep(imovel['idQuadraImovel'],imovel["�dCepImovel"],{
																		callback : function(
																				listaQuadraFace) {
																			for (key in listaQuadraFace) {
																				var novaOpcao = new Option(
																						listaQuadraFace[key],
																						key);
																				var novaOpcaoPontoConsumoQuadraFace = new Option(
																						listaQuadraFace[key],
																						key);
																				selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;
																				selectQuadraFacePontoConsumo.options[selectQuadraFacePontoConsumo.length] = novaOpcaoPontoConsumoQuadraFace;
																			}
																		$("#idQuadraFace").val(imovel['idQuadraFace'])
																		$("#idQuadraFacePontoConsumo").val(imovel['idQuadraFace'])
																		 
																		var selectChavePrimaria = document.imovelForm.chavePrimaria.value;
																		AjaxService.consultarSitucaoImovelPorFacesQuadra(imovel['idQuadraFace'],selectChavePrimaria,{
																				callback : function(listaSitucaoImovel) {
																					
																					for (var key in listaSitucaoImovel) {
																						var achou = false;
																						var qtdOptions = selectSituacaoImovel.options.length;

																						// Evita que coloque a mesma situa��o mais de uma vez
																						for (var j = 0; j < qtdOptions; j++) {
																							if(selectSituacaoImovel.options[j].value == key) { // verifica se o id � o mesmo
																								achou = true;
																								break;
																							}
																						}
																						if(!achou) {
																							var novaOpcao = new Option(
																									listaSitucaoImovel[key],
																									key);
																							selectSituacaoImovel.options[selectSituacaoImovel.length] = novaOpcao;
																						}
																					}

																			      	if(!desabilitaCamposPai && imovel['idSituacao'] > 0) {
																				      	$("#idSituacao").removeClass("campoDesabilitado");
																				      	$("#idSituacao").removeAttr("disabled");
																			      	}
																					 
																					$("#idSituacao").val(imovel['idSituacao'])
																				},
																				async : false
																			});

																		},
																		async : false
																	});
																},
																async : false
															}

													);
										} else {
											$("#idQuadraImovel").addClass(
													"campoDesabilitado");
											selectQuadras.disabled = true;
										}
									}
								},
								async : false
							}

					);
		} else {
			$('#matriculaImovelPai').val("");
			$('#nomeFantasiaImovelPai').val("");
			$('#numeroImovelPai').val("");
			$('#cidadeImovelPai').val("");
			$('#indicadorCondominioImovelTexto1').removeAttr('checked')
			$('#indicadorCondominioImovelTexto2').removeAttr('checked')
			$("#cepImovel").prop("readonly", false);
			$("#numeroImovel").prop("readonly", false);
			$('#numeroImovel').removeClass('campoDesabilitado')
			$('#cepImovel').removeClass('campoDesabilitado')
			$('#numeroImovel').removeAttr('disabled')
			$('#condominioSim').removeAttr('disabled')
			$('#condominioNao').removeAttr('disabled')
			$('#idQuadraImovel').attr('checked', 'true')
			$('#indicadorCondominioImovelPai').val('')
			// $('#areaImovelCondominio').show()
			//    $('#legendaCaracteristica').show()
		}

	}
	function exibirPopupPesquisaImovel() {
		popup = window
				.open(
						'exibirPesquisaImovelCompletoPopup?postBack=true&pesquisaImovelPai=S',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function exibePesquisaImovelPai() {
		$('#PesquisarImovelPai').show()
		esconderDadosCondominio()

		$('#condominioNao').attr('checked', 'true')
		$('#condominioSim').removeAttr('checked')
	}

	function desmarcaImovelPai() {
		$('#PesquisarImovelPai').hide()

		$("#cepImovel").prop("readonly", false);
		$('#numeroImovel').removeClass('campoDesabilitado')
		$('#cepImovel').removeClass('campoDesabilitado')

		$('#numeroImovel').removeAttr('disabled')
		//  $('#cepImovel').val('')
		//  $('#numeroImovel').val('')
		//  $('#condominioSim').removeAttr('disabled')
		//  debugger
		// $('#condominioNao').removeAttr('disabled')
		// $('#condominioNao').attr('checked','true')
		$('#matriculaImovelPai').val('');
		$('#nomeFantasiaImovelPai').val('');
		$('#numeroImovelPai').val('');
		$('#cidadeImovelPai').val('');
		//  $('#indicadorCondominioImovelTexto1').removeAttr('checked')
		//  $('#indicadorCondominioImovelTexto2').removeAttr('checked')
		//   $('#idQuadraImovel').attr('checked','true')
		$('#indicadorCondominioImovelPai').val('false')
		// $('#areaImovelCondominio').show()
		//   $('#legendaCaracteristica').show()

	}

	function configuraIndicadorCondominio(tipo) {
		if (tipo == 'true') {
			$('#indicadorCondominioImovelTexto1').attr('checked', 'true')
			$('#indicadorCondominioImovelTexto2').removeAttr('checked')
			$('#indicadorCondominioImovelPai').val('true')

			$("#cepImovel").prop("readonly", true);
			$('#cepImovel').addClass('campoDesabilitado')
			$("#numeroImovel").prop("readonly", true);
			$('#numeroImovel').addClass('campoDesabilitado')
			/*
			$('#condominioSim').attr('checked', 'false')
			$('#condominioNao').attr('checked', 'true')
			$('#condominioSim').removeAttr('checked')
			$('#condominioSim').attr('disabled', 'true')
			$('#condominioNao').attr('disabled', 'true')
			 */

			// $('#condominioNao').attr('checked','true')
			// $('#condominioSim').removeAttr('checked')
			// $('#areaImovelCondominio').hide()
			//     $('#legendaCaracteristica').hide()
		} else if (tipo == 'false') {
			$('#indicadorCondominioImovelTexto1').removeAttr('checked')
			$('#indicadorCondominioImovelTexto2').attr('checked', 'true')
			$('indicadorCondominioImovelPai').val('false')

			$("#cepImovel").prop("readonly", false);
			$('#cepImovel').removeClass('campoDesabilitado')
			$("#numeroImovel").prop("readonly", false);
			$('#numeroImovel').removeClass('campoDesabilitado')

			// $('#condominioNao').attr('checked', 'true')
			//  $('#condominioSim').removeAttr('checked')

			$('#PesquisarImovelPai').hide()
			//  $('#legendaCaracteristica').hide()
		} else {
			// $('#condominioNao').attr('checked', 'true')
			// $('#condominioSim').removeAttr('checked')
			$('#PesquisarImovelPai').hide()
		}

	}
</script>

<input style="display:none" type="text" id="idImovelCondominio" name="idImovelCondominio"  value="${imovelVO.idImovelCondominio}"> 
 
 
<c:if test="${imovelVO.idImovelCondominio eq ''}">
  <script>
  $('#PesquisarImovelPai').hide()
  $('#legendaCaracteristica').hide()
  </script>
</c:if>

<c:if test="${empty imovelVO.imovelPai}">
  <script>setTimeout("$('#PesquisarImovelPai').hide();",100)</script>
</c:if>

<c:if test="${imovelVO.imovelPai == 'true'}">
  <script>setTimeout("$('#PesquisarImovelPai').show()",100)</script>
</c:if>


 





<a class="linkHelp"
	href="<help:help>/abacaractersticascadastrodoimvel.htm</help:help>"
	target="right" onclick="exibirJDialog('#janelaHelp');"></a>
<fieldset id="ImovelAbaCaracteristicas" class="colunaEsq">
	<label class="rotulo" id="rotuloTipoPavimentoCalcada"
		for="idPavimentoCalcada"><span class="campoObrigatorioSimbolo"></span>Pavimento
		da Cal�ada:</label> <select name="idPavimentoCalcada" id="idPavimentoCalcada"
		class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaPavimentoCalcada}" var="pavimentoCalcada">
			<option value="<c:out value="${pavimentoCalcada.chavePrimaria}"/>"
				<c:if test="${imovelVO.idPavimentoCalcada == pavimentoCalcada.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${pavimentoCalcada.descricao}" />
			</option>
		</c:forEach>
	</select><br /> <label class="rotulo" id="rotuloTipoPavimentoRua"
		for="idPavimentoRua"><span class="campoObrigatorioSimbolo"></span>Pavimento
		da Rua:</label> <select name="idPavimentoRua" id="idPavimentoRua"
		class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaPavimentoRua}" var="pavimentoRua">
			<option value="<c:out value="${pavimentoRua.chavePrimaria}"/>"
				<c:if test="${imovelVO.idPavimentoRua == pavimentoRua.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${pavimentoRua.descricao}" />
			</option>
		</c:forEach>
	</select><br /> <label class="rotulo" id="rotuloPadraoConstrucao"
		for="idPadraoConstrucao">Padr�o de constru��o:</label> <select
		name="idPadraoConstrucao" id="idPadraoConstrucao" class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaPadraoConstrucao}" var="padraoConstrucao">
			<option value="<c:out value="${padraoConstrucao.chavePrimaria}"/>"
				<c:if test="${imovelVO.idPadraoConstrucao == padraoConstrucao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${padraoConstrucao.descricao}" />
			</option>
		</c:forEach>
	</select><br /> <label class="rotulo" id="rotuloTipoBotijao"
		for="idTipoBotijao">Tipo de Cilindro:</label> <select
		name="idTipoBotijao" id="idTipoBotijao" class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaTipoBotijao}" var="tipoBotijao">
			<option value="<c:out value="${tipoBotijao.chavePrimaria}"/>"
				<c:if test="${imovelVO.idTipoBotijao == tipoBotijao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${tipoBotijao.descricao}" />
			</option>
		</c:forEach>
	</select><br /> <label class="rotulo" id="rotuloTipoCombustivel"
		for="idTipoCombustivel">Tipo de Combust�vel:</label> <select
		name="idTipoCombustivel" id="idTipoCombustivel" class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaTipoCombustivel}" var="tipoCombustivel">
			<option value="<c:out value="${tipoCombustivel.chavePrimaria}"/>"
				<c:if test="${imovelVO.idTipoCombustivel == tipoCombustivel.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${tipoCombustivel.descricao}" />
			</option>
		</c:forEach>
	</select><br /> <label class="rotulo" id="rotuloExisteValvulaBloqueio"
		for="valvulaBloqueioSim">Existe v�lvula de bloqueio?</label> <input
		class="campoRadio" type="radio" value="true" name="valvulaBloqueio"
		id="valvulaBloqueioSim"
		<c:if test="${imovelVO.valvulaBloqueio eq 'true'}">checked</c:if>><label
		class="rotuloRadio">Sim</label> <input class="campoRadio" type="radio"
		value="false" name="valvulaBloqueio" id="valvulaBloqueioNao"
		<c:if test="${imovelVO.valvulaBloqueio ne 'true'}">checked</c:if>><label
		class="rotuloRadio">N�o</label><br /> <br /> 
		
		
		
		  
    <label class="rotulo" id="rotuloInformarImovelPai" style="width: 184px;"  for="imovelPaiSim"><span
      class="campoObrigatorioSimbolo">* </span> Unidade Interna Condominio?</label>
    <input class="campoRadio" type="radio" value="true" name="imovelPai"
      id="imovelPaiSim"
      <c:if test="${imovelVO.imovelPai eq 'true'}">checked</c:if>
      onclick="exibePesquisaImovelPai()">
    <label class="rotuloRadio">&nbsp;Sim</label>
    <input class="campoRadio" type="radio" value="false" name="imovelPai"
      id="imovelPaiNao"
      <c:if test="${empty imovelVO.imovelPai or imovelVO.imovelPai eq 'false'}">checked</c:if>
      onclick="desmarcaImovelPai()">
    <label class="rotuloRadio">&nbsp;N�o</label>
    <fieldset id="PesquisarImovelPai">
      <legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
      <div class="pesquisarImovelFundo">
        <p class="orientacaoInterna">
          Clique em <span class="destaqueOrientacaoInicial">Pesquisar
            Imovel</span> para selecionar um Im�vel.
        </p>

        <input name="Button" id="botaoPesquisarImovel"
          class="bottonRightCol" title="Pesquisar Imovel"
          value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();"
          type="button"><br />
        <br /> <label class="rotulo" id="rotuloNomeFantasia"
          for="nomeFantasiaImovelPai">Descri��o:</label> <input
          class="campoTexto" type="text" onfocus="this.blur();"
          id="nomeFantasiaImovelPai" name="nomeFantasiaImovelPai"
          maxlength="50" value="${imovelVO.nomeFantasiaImovelPai}"><br />
        <label class="rotulo" id="rotuloMatriculaTexto"
          for="matriculaImovelPai">Matr�cula:</label> <input
          class="campoTexto" type="text" onfocus="this.blur();"
          id="matriculaImovelPai" name="matriculaImovelPai" maxlength="18"
          size="18" value="${imovelVO.matriculaImovelPai}"><br /> <label
          class="rotulo" id="rotuloNumeroTexto" for="numeroImovelPai">N�mero:</label>
        <input class="campoTexto" type="text" onfocus="this.blur();"
          id="numeroImovelPai" name="numeroImovelPai" maxlength="18"
          size="18" value="${imovelVO.numeroImovelPai}"><br /> <label
          class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelPai">Cidade:</label>
        <input class="campoTexto" type="text" id="cidadeImovelPai"
          name="cidadeImovelPai" maxlength="18" size="18"
          onfocus="this.blur();" value="${imovelVO.cidadeImovelPai}"><br />
        

      </div>

    </fieldset>

 
  
		
		
		
		
		
		    <span id="areaImovelCondominio">
						<label class="rotulo campoObrigatorio" id="rotuloImovelCondominio" for="condominioSim">
						  <span class="campoObrigatorioSimbolo">*</span>O im�vel � condom�nio?
						</label> 
						<input class="campoRadio" type="radio" value="true" name="condominio" id="condominioSim"
						<c:if test="${imovelVO.condominio eq 'true'}">checked</c:if>
						onclick="exibirDadosCondominio()"><label class="rotuloRadio">Sim</label> <input class="campoRadio" type="radio" value="false" name="condominio"
						id="condominioNao"
						<c:if test="${imovelVO.condominio ne 'true'}">checked</c:if>
						onclick="esconderDadosCondominio()"><label class="rotuloRadio">N�o</label>
		
	<fieldset id="ImovelAbaCaracteristicas3">
		<legend>Dados do Condom�nio</legend>
		<div id="dadosCondominioFundo">
			<label class="rotulo" id="rotuloModalidademMedicao"
				for="codigoModalidadeMedicao"><span class="campoObrigatorioSimbolo2">* </span>Modalidade de Medi��o:</label> <select
				name="codigoModalidadeMedicao" id="codigoModalidadeMedicao"
				class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaModalidadeMedicaoImovel}" var="modalidade">
					<option value="<c:out value="${modalidade.codigo}"/>"
						<c:if test="${imovelVO.codigoModalidadeMedicao == modalidade.codigo}">selected="selected"</c:if>>
						<c:out value="${modalidade.descricao}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo"
				id="rotuloQuantidadeBloco" for="quantidadeBlocos"><span class="campoObrigatorioSimbolo2">* </span>N� de Blocos/Torres:</label><input
				class="campoTexto" type="text" id="quantidadeBloco"
				name="quantidadeBloco" maxlength="3" size="1"
				value="${imovelVO.quantidadeBloco}"
				onkeypress="return formatarCampoInteiro(event);"><br /><label
				class="rotulo" id="rotuloQuantidadeAndar"
				for="quantidadeAndar"><span class="campoObrigatorioSimbolo2">* </span>N� de Andares:</label>
			<c:if test="${requestScope.imovelAssociadoVerifado}">
				<input type="hidden" id="quantidadeAndar" name="quantidadeAndar"
					value="${imovelVO.quantidadeAndar}">
			</c:if>
			<input class="campoTexto" type="text" id="${param.qtdAndar}"
				name="${param.qtdAndar}" maxlength="2" size="1"
				value="${imovelVO.quantidadeAndar}"
				onkeypress="return formatarCampoInteiro(event);"
				<c:if test="${param.alteracao && requestScope.imovelAssociadoVerifado}">disabled=disabled</c:if>><br />

			<label class="rotulo"
				id="rotuloQuantidadeApartamentoAndar"
				for="quantidadeApartamentoAndar"><span class="campoObrigatorioSimbolo2">* </span>N� de Unidades por Andar:</label>
			<c:if test="${requestScope.imovelAssociadoVerifado}">
				<input type="hidden" id="quantidadeApartamentoAndar"
					name="quantidadeApartamentoAndar"
					value="${imovelVO.quantidadeApartamentoAndar}">
			</c:if>
			<input class="campoTexto" type="text"
				id="${param.qtdApartamentoAndar}"
				name="${param.qtdApartamentoAndar}" maxlength="2" size="1"
				value="${imovelVO.quantidadeApartamentoAndar}"
				onkeypress="return formatarCampoInteiro(event);"
				<c:if test="${param.alteracao && requestScope.imovelAssociadoVerifado}">disabled=disabled</c:if>><br />

			<label class="rotulo" id="rotuloDataEntrega" for="dataEntrega">Previs�o
				de Entrega:</label> <input type="text" id="dataEntrega" name="dataEntrega"
				class="campoData" value="${imovelVO.dataEntrega}" /><br /> <label
				class="rotulo" id="rotuloConstrutora" for="idEmpresa">Construtora:</label>
			<select name="idEmpresa" id="idEmpresa" class="campoSelect"
				style="width: 220px">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaEmpresa}" var="empresa">
					<option value="<c:out value="${empresa.chavePrimaria}"/>"
						<c:if test="${imovelVO.idEmpresa == empresa.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${empresa.cliente.nome}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo" id="rotuloPossuiPortaria"
				for="portariaSim">Condom�nio Possui portaria?</label> <input
				class="campoRadio" type="radio" value="true" name="portaria"
				id="portariaSim"
				<c:if test="${imovelVO.portaria eq 'true' || empty imovelVO.portaria}">checked</c:if>><label
				class="rotuloRadio">Sim</label> <input class="campoRadio"
				type="radio" value="false" name="portaria" id="portariaNao"
				<c:if test="${imovelVO.portaria eq 'false'}">checked</c:if>><label
				class="rotuloRadio">N�o</label>
		</div>
	</fieldset>
	
	</span>
	
	

	
	
	
</fieldset>
<fieldset id="ImovelAbaCaracteristicas2">
	<label class="rotulo" id="rotuloFaixaAreaConstruda"
		for="idAreaConstruidaFaixa">Faixa de �rea Constru�da:</label> <select
		name="idAreaConstruidaFaixa" id="idAreaConstruidaFaixa"
		class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaAreaConstruidaFaixa}"
			var="areaConstruidaFaixa">
			<option value="<c:out value="${areaConstruidaFaixa.chavePrimaria}"/>"
				<c:if test="${imovelVO.idAreaConstruidaFaixa == areaConstruidaFaixa.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${areaConstruidaFaixa.menorFaixa}" /> -
				<c:out value="${areaConstruidaFaixa.maiorFaixa}" />
			</option>
		</c:forEach>
	</select><br /> <label class="rotulo" id="rotuloPerfilImovel"
		for="idPerfilImovel">Perfil do Im�vel:</label> <select
		name="idPerfilImovel" id="idPerfilImovel" class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaPerfilImovel}" var="perfilImovel">
			<option value="<c:out value="${perfilImovel.chavePrimaria}"/>"
				<c:if test="${imovelVO.idPerfilImovel == perfilImovel.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${perfilImovel.descricao}" />
			</option>
		</c:forEach>
	</select><br /> <label class="rotulo" id="rotuloQuantidadeBanheiro"
		for="quantidadeBanheiro">Quantidade de banheiros:</label> <input
		class="campoTexto" type="text" id="quantidadeBanheiro"
		name="quantidadeBanheiro"
		onkeypress="return formatarCampoInteiro(event);" maxlength="4"
		size="5" value="${imovelVO.quantidadeBanheiro}" /><br /> <label
		class="rotulo">Obra feita em im�vel novo?</label> <input
		class="campoRadio" type="radio" value="true"
		name="indicadorObraTubulacao" id="indicadorObraTubulacaoSim"
		<c:if test="${imovelVO.indicadorObraTubulacao eq 'true'}">checked</c:if>><label
		class="rotuloRadio">Sim</label> <input class="campoRadio" type="radio"
		value="false" name="indicadorObraTubulacao"
		id="indicadorObraTubulacaoNao"
		<c:if test="${imovelVO.indicadorObraTubulacao ne 'true'}">checked</c:if>><label
		class="rotuloRadio">N�o</label> </br> 
		
		<label class="rotulo">Possui
		restri��o de servi�o?</label>		
		 <input class="campoRadio" type="radio"
		value="true" name="indicadorRestricaoServico"
		id="indicadorRestricaoServico"
		<c:if test="${imovelVO.indicadorRestricaoServico eq 'true'}">checked</c:if>
		onclick="exibirRestricaoServicoTipo()">
		
		<label class="rotuloRadio">Sim</label> 
		<input class="campoRadio" type="radio"
		value="false" name="indicadorRestricaoServico"
		id="indicadorRestricaoServico"
		<c:if test="${imovelVO.indicadorRestricaoServico ne 'true'}">checked</c:if>
		onclick="esconderRestricaoServicoTipo()">
		<label
		class="rotuloRadio">N�o</label> <br>
	<fieldset id="restricoesServicoTipo">
		<div id="divRestricoesServicoTipo">

			<label class="rotulo rotuloVertical1 rotuloCampoList "
				for="idsServicoTipoRestricao" id="RotuloIdsServicoTipoRestricao" >Servi�os dispon�veis:</label> 
			
			<label
				class="rotulo rotuloHorizontal2 rotuloCampoList"
				for="idsServicoTipoRestricaoSalvos" id="RotuloIdsServicoTipoRestricaoSalvos" >Servi�os restritos:</label> 

			<select
				id="idsServicoTipoRestricao" name="idsServicoTipo"
				class="campoList campoVertical" multiple="multiple"
				onDblClick="moveSelectedOptions(document.forms[0].idsServicoTipo,document.forms[0].idsServicoTipoRestricaoSalvos,true)">				
				<c:forEach items="${listaServicoTipo}" var="servicoTipo">
					<c:set var="r" value="false" />
					<c:forEach items="${imovelVO.idsServicoTipoRestricao}" var="idServicoTipoRestricao">						
						<c:if test="${idServicoTipoRestricao eq servicoTipo.chavePrimaria}">
							<c:set var="r" value="true" />					
						</c:if>						
				 	</c:forEach>
				 		<c:if test='${r eq "false" }'>
				 			<option value="<c:out value="${servicoTipo.chavePrimaria}" />" title="<c:out value="${servicoTipo.descricao}"/>">
				 				<c:out value="${servicoTipo.descricao}" />
				 			</option>
				 		</c:if>
				</c:forEach>
			</select>

			<fieldset class="conteinerBotoesCampoList">
				<input type="button" name="right" value="&gt;&gt;"
					class="bottonRightCol2"
					onClick="moveSelectedOptions(document.forms[0].idsServicoTipo,document.forms[0].idsServicoTipoRestricaoSalvos,true)">
				<input type="button" name="right" value="Todos &gt;&gt;"
					class="bottonRightCol botoesLargosIE7"
					onClick="moveAllOptions(document.forms[0].idsServicoTipo,document.forms[0].idsServicoTipoRestricaoSalvos,true)">
				<input type="button" name="left" value="&lt;&lt;"
					class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
					onClick="moveSelectedOptions(document.forms[0].idsServicoTipoRestricaoSalvos,document.forms[0].idsServicoTipo,true)">
				<input type="button" name="left" value="Todos &lt;&lt;"
					class="bottonRightCol botoesCampoListDir botoesLargosIE7"
					onClick="moveAllOptions(document.forms[0].idsServicoTipoRestricaoSalvos,document.forms[0].idsServicoTipo,true)">
			</fieldset>

			<select id="idsServicoTipoRestricaoSalvos"
				name="idsServicoTipoRestricao" multiple="multiple"
				class="campoList campoList2"
				onDblClick="moveSelectedOptions(document.forms[0].idsServicoTipoRestricaoSalvos,document.forms[0].idsServicoTipo,true)">
				<c:forEach items="${listaServicoTipo}" var="servicoTipo">
					<c:set var="r" value="false" />
					<c:forEach items="${imovelVO.idsServicoTipoRestricao}" var="idServicoTipoRestricao">						
								<c:if test="${idServicoTipoRestricao eq servicoTipo.chavePrimaria}">
									<c:set var="r" value="true" />					
								</c:if> 
				 	</c:forEach>
				 		<c:if test='${r eq "true" }'>
				 			<option value="<c:out value="${servicoTipo.chavePrimaria}"/>" title="<c:out value="${servicoTipo.descricao}"/>">
				 				<c:out value="${servicoTipo.descricao}" />
				 			</option>
				 		</c:if>
				</c:forEach>
			</select>		
					
		</div>
	</fieldset>

	</br> <label class="rotulo" id="rotuloExisteRedeInterna"
		for="redePreexistenteSim">Im�vel possui rede interna?</label> <input
		class="campoRadio" type="radio" value="true" name="redePreexistente"
		id="redePreexistenteSim"
		<c:if test="${imovelVO.redePreexistente eq 'true'}">checked</c:if>
		onclick="exibirDadosRedeInterna()"><label class="rotuloRadio">Sim</label>
	<input class="campoRadio" type="radio" value="false"
		name="redePreexistente" id="redePreexistenteNao"
		<c:if test="${imovelVO.redePreexistente ne 'true'}">checked</c:if>
		onclick="esconderDadosRedeInterna();"><label
		class="rotuloRadio">N�o</label>
	<fieldset id="dadosRedeInterna">
		<legend>Dados da Rede Interna</legend>
		<div id="dadosRedeInternaFundo">
			<label class="rotulo" id="rotuloMaterialRede" for="idRedeMaterial">Material
				da rede:</label> <select name="idRedeMaterial" id="idRedeMaterial"
				class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaRedeMaterial}" var="redeMaterial">
					<option value="<c:out value="${redeMaterial.chavePrimaria}"/>"
						<c:if test="${imovelVO.idRedeMaterial == redeMaterial.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${redeMaterial.descricao}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo" id="rotuloQuantPrumadas"
				for="quantidadePrumada">Quantidade de prumadas:</label> <input
				class="campoTexto" type="text" id="quantidadePrumada"
				name="quantidadePrumada"
				onkeypress="return formatarCampoInteiro(event);" maxlength="3"
				size="5" value="${imovelVO.quantidadePrumada}"><br />
			<label class="rotulo rotulo2Linhas"
				id="rotuloDiamRedeTrechoPrimarioPrumada" for="idRedeDiametroCentral">Di�metro
				da rede no trecho prim�rio da prumada:</label> <select
				name="idRedeDiametroCentral" id="idRedeDiametroCentral"
				class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaRedeDiametro}" var="redeDiametro">
					<option value="<c:out value="${redeDiametro.chavePrimaria}"/>"
						<c:if test="${imovelVO.idRedeDiametroCentral == redeDiametro.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${redeDiametro.descricao}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo rotulo2Linhas"
				id="rotuloDiamRedePrumadas" for="idRedeDiametroPrumada">Di�metro
				da rede nas prumadas:</label> <select name="idRedeDiametroPrumada"
				id="idRedeDiametroPrumada" class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaRedeDiametro}" var="redeDiametro">
					<option value="<c:out value="${redeDiametro.chavePrimaria}"/>"
						<c:if test="${imovelVO.idRedeDiametroPrumada == redeDiametro.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${redeDiametro.descricao}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo" id="rotuloDiametroRedeSecundaria"
				for="idRedeDiametroPrumadaApartamento">Di�metro da rede
				secund�ria:</label> <select name="idRedeDiametroPrumadaApartamento"
				id="idRedeDiametroPrumadaApartamento" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaRedeDiametro}" var="redeDiametro">
					<option value="<c:out value="${redeDiametro.chavePrimaria}"/>"
						<c:if test="${imovelVO.idRedeDiametroPrumadaApartamento == redeDiametro.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${redeDiametro.descricao}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo rotulo2Linhas"
				id="rotuloQuantReguladoresPressao" for="quantidadeReguladorHall">Quantidade
				de<br />reguladores de press�o:
			</label> <input class="campoTexto" type="text" id="quantidadeReguladorHall"
				name="quantidadeReguladorHall"
				onkeypress="return formatarCampoInteiro(event);" maxlength="3"
				size="5" value="${imovelVO.quantidadeReguladorHall}"><br />
			<label class="rotulo" id="rotuloHallPossuiVentilacao"
				for="ventilacaoHallSim">O hall possui ventila��o?</label> <input
				class="campoRadio" type="radio" value="true" name="ventilacaoHall"
				id="ventilacaoHallSim"
				<c:if test="${imovelVO.ventilacaoHall eq 'true'}">checked</c:if>><label
				class="rotuloRadio">Sim</label> <input class="campoRadio"
				type="radio" value="false" name="ventilacaoHall"
				id="ventilacaoHallNao"
				<c:if test="${imovelVO.ventilacaoHall ne 'true'}">checked</c:if>><label
				class="rotuloRadio">N�o</label><br /> <label class="rotulo"
				id="rotuloVventilacaoPermanente" for="ventilacaoApartamentoSim">A
				ventila��o � permanente?</label> <input class="campoRadio" type="radio"
				value="true" name="ventilacaoApartamento"
				id="ventilacaoApartamentoSim"
				<c:if test="${imovelVO.ventilacaoApartamento eq 'true'}">checked</c:if>><label
				class="rotuloRadio">Sim</label> <input class="campoRadio"
				type="radio" value="false" name="ventilacaoApartamento"
				id="ventilacaoApartamentoNao"
				<c:if test="${imovelVO.ventilacaoApartamento ne 'true'}">checked</c:if>><label
				class="rotuloRadio">N�o</label>
		</div>
	</fieldset>
</fieldset>


<p class="legenda" id="legendaCaracteristica">
	<span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para
	Inclus�o de Im�veis.
</p>



  <c:choose>
              <c:when test="${imovelVO.imovelPai eq 'true'}">
                <script>
                  setTimeout("configuraIndicadorCondominio('true')",100)
                 </script>
              </c:when>
               <c:when test="${imovelVO.imovelPai eq 'false'}">
                <script>
                   setTimeout("configuraIndicadorCondominio('false')",100)
                 </script>
              </c:when>
              <c:otherwise>
                <script>
              setTimeout("configuraIndicadorCondominio('')",100)
                </script>
              </c:otherwise>
            </c:choose>
