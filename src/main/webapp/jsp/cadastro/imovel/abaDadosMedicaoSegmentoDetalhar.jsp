<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>

function onloadRamoAtividade() {
	
	<c:choose>
		<c:when test="${indexLista > -1 && empty param['operacaoLista'] }">
			var botaoAlterarRamoAtividade = document.getElementById("botaoAlterarRamoAtividade");
			var botaoLimparRamoAtividade = document.getElementById("botaoLimparRamoAtividade");	
			var botaoIncluirRamoAtividade = document.getElementById("botaoIncluirRamoAtividade");	
			botaoIncluirRamoAtividade.disabled = true;
			botaoAlterarRamoAtividade.disabled = false;
			botaoLimparRamoAtividade.disabled = false;
		</c:when>
		<c:when test="${sucessoManutencaoLista}">
			limparRamoAtividade();
		</c:when>
	</c:choose>
}


<c:if test="${abaId == '1'}">
	addLoadEvent(onloadRamoAtividade);
</c:if>

</script>
	
	<label class="rotulo" for="estouroConsumo">Volume m�dio para estouro de consumo:</label>
		<span class="itemDetalhamento">
			<c:out value="${segmentoForm.volumeMedioEstouro}"/>
		</span>
	<label class="rotuloInformativo" for="consumoAlto">m<span class="expoente">3</span></label><br class="quebraLinha2" />
			
	<fieldset class="conteinerBloco">
	
    	<fieldset class="conteinerLista1">

			<label class="rotulo rotuloVertical" id="rotuloLocalAmostragemPCS" for="listaSegmentoAmostragemPCSSelecionados">Local de Amostragem do PCS:</label><br />
			<select id="listaSegmentoAmostragemPCS" class="campoList listaCurta1a campoVertical" name="listaSegmentoAmostragemPCS" multiple="multiple" disabled="disabled" readOnly="true">
				<c:forEach items="${listaSegmentoAmostragemPCSSelecionados}" var="segmentoAmostragemPCS">
					<option value="<c:out value="${segmentoAmostragemPCS.chavePrimaria}"/>" title="<c:out value="${segmentoAmostragemPCS.localAmostragemPCS.descricao}"/>"> <c:out value="${segmentoAmostragemPCS.localAmostragemPCS.descricao}"/></option>		
				</c:forEach>
			</select>
		</fieldset>
		
		<fieldset class="conteinerLista2">

			<label class="rotulo rotuloVertical" id="rotuloIntervaloPCS" for="intervaloPCSs">Intervalo de Recupera��o do PCS:</label><br />
			<select id="listaSegmentoIntervaloPCS" class="campoList listaCurta1a campoVertical" name="listaSegmentoIntervaloPCS" multiple="multiple" disabled="disabled" readOnly="true">
				<c:forEach items="${listaSegmentoIntervaloPCSSelecionados}" var="segmentoIntervaloPCS">
					<option value="<c:out value="${segmentoIntervaloPCS.chavePrimaria}"/>" title="<c:out value="${segmentoIntervaloPCS.intervaloPCS.descricao}"/>"> <c:out value="${segmentoIntervaloPCS.intervaloPCS.descricao}"/></option>		
				</c:forEach>
			</select>			
		</fieldset>
		<label class="rotulo" for="tamanhoReducao">Tamanho da redu��o:</label>
			<span id="tamanhoReducao" class="itemDetalhamento">
				<c:out value="${segmentoForm.tamanhoReducao}"/>
			</span>
		<label class="rotuloInformativo" for="consumoAlto">dias</label>
	</fieldset>