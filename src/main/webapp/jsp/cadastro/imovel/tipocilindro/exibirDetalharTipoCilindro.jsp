<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Detalhar Tipo de Cilindro<a href="<help:help>/Tiposdebotijodetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informações deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>
		$(document).ready(function(){
			  var max = 0;
		
		    $('.rotulo').each(function(){
		            if ($(this).width() > max)
		               max = $(this).width();   
		        });
		    $('.rotulo').width(max);
			
		});

		function voltar(){	

			submeter('tipoCilindroForm', 'exibirPesquisaTipoCilindro');
		}
		
		function alterar(){
			
			submeter('tipoCilindroForm', 'exibirAlteracaoTipoCilindro');
		}

		addLoadEvent(init);
</script>

<form:form method="post" enctype="multipart/form-data" id="tipoCilindroForm" name="tipoCilindroForm">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tipoCilindro.chavePrimaria}">
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
		
		<label class="rotulo">Descrição:</label>
		<span class="itemDetalhamento"><c:out value="${tipoCilindro.descricao}"/></span>
		<br /><br />
	    
	    </fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    
    <vacess:vacess param="exibirAlteracaoTipoCilindro">    
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
   
</fieldset>
</form:form>
