<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<script> 
	var codIcms = null;
	
	function alterarPontoConsumo(form, actionAdicionarPontoConsumo) {
		checarIndicadorMensagemAdicional();
		submeter('imovelForm', actionAdicionarPontoConsumo + '#pontosConsumo');
	}	
	
	$(document).ready(function(){
		
		AjaxService
		.isTipoSubstitutoPetrobras(function(isPetrobras) {
					if (isPetrobras == true) {
						animatedcollapse.show('conteinerICMSSubstituido');		
					} else {
						animatedcollapse.hide('conteinerICMSSubstituido');
					}
				});
		//tabula��o campos
		$("input#pontoConsumoLegado").attr('tabindex',1000);
		$("input#descricaoPontoConsumo").attr('tabindex',1001);	
		$("select#idSegmentoPontoConsumo").attr('tabindex',1002);	
		$("select#idRamoAtividadePontoConsumo").attr('tabindex',1003);
		$("select#idModalidadeUso").attr('tabindex',1004);
		$("input#cepPontosConsumo").attr('tabindex',1005);
		$("input#numeroImovelPontoConsumo").attr('tabindex',1006);
		$("input#descricaoComplementoPontoConsumo").attr('tabindex',1007);
		$("input#enderecoReferenciaPontoConsumo").attr('tabindex',1008);
		$("select#idQuadraPontoConsumo").attr('tabindex',1009);
		$("select#idQuadraFacePontoConsumo").attr('tabindex',1010);
		$("select#idRota").attr('tabindex',1011);
		$("input#latitudeGrau").attr('tabindex',1012);
		$("input#longitudeGrau").attr('tabindex',1013);
		$("input#enderecoRemotoPontoConsumo").attr('tabindex',1014);
		$("input#dataInicioVigenciaCityGate").attr('tabindex',1015);
		$("select#idPontoConsumoTributo").attr('tabindex',1016);
		$("input#radioIsentoSim").attr('tabindex',1017);
		$("input#radioIsentoNao").attr('tabindex',1018);
		$("input#radioDestaqueNotaSim").attr('tabindex',1019);
		$("input#radioDestaqueNotaNao").attr('tabindex',1020);
		$("input#percentualAliquota").attr('tabindex',1021);
		$("input#dataInicioVigencia").attr('tabindex',1022);
		$("input#dataFimVigencia").attr('tabindex',1023);
		
		$("input#latitudeGrau").mask('S0Z,000000000000',
			{ translation:  {
				'S': {pattern: /[+-]/, optional: true},
				'Z': {pattern: /[0-9]/, optional: true}
			}
		});
		$("input#longitudeGrau").mask('S0ZZ,000000000000',
			{ translation:  {
				'S': {pattern: /[+-]/, optional: true},
				'Z': {pattern: /[0-9]/, optional: true}
			}
		});
		
	    $("#dataInicioVigencia,.dataInicioVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});   
	
	    $("#dataFimVigencia,.dataFImVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

	    $("#dataInicioVigenciaCityGate,.dataInicioVigenciaCityGate").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});	 
	    
	    AjaxService.obterIdConstanteIcms(
      			{callback: function(idIcms) {            		      		         		
      				codIcms = idIcms;
                }, async: false}
        );	    	
	    
		//Comportamento dos campos de endere�o de Dados do Ponto de Consumo
		document.getElementById('numeroImovel').onchange = carregarNumeroImovelPontoConsumo;
		document.getElementById('complementoImovel').onchange = carregarComplementoImovelPontoConsumo;
	    
		
		//Comportamento dos campos de Exce��es Tribut�rias no Ponto de Consumo
	  	if($("input#radioIsentoSim").attr("checked")==true){	  		
			$("#percentualAliquota").addClass("campoDesabilitado");
			$("label[for=percentualAliquota], label[for=percentualAliquota] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
		} else {			
			$("#percentualAliquota").removeClass("campoDesabilitado");
			$("label[for=percentualAliquota], label[for=percentualAliquota] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");
		}
		
	  //Comportamento dos campos de Exce��es Tribut�rias no Ponto de Consumo
	  	if($("input#radioDrawbackSim").attr("checked")==true){	  		
	  		$("#valorDrawback").removeClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback]").removeClass("rotuloDesabilitado");
		} else {
			$("#valorDrawback").addClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback]").addClass("rotuloDesabilitado");			
		}
		
	  	
			
// 	  	$("input#indicadorIcmsSubstitutoTarifa").click(function(){			
// 			$("#conteinerAssociarTributosPontoConsumo label[for=percentualAliquota], #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] span.campoObrigatorioSimbolo3, label[for=percentualAliquota]").addClass("rotuloDesabilitado");
// 			$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").addClass("campoDesabilitado").attr("disabled","disabled").val("");   			
// 		});
		$("input#radioIsentoSim").click(function(){			
			$("#conteinerAssociarTributosPontoConsumo label[for=percentualAliquota], #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] span.campoObrigatorioSimbolo3, label[for=percentualAliquota]").addClass("rotuloDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").addClass("campoDesabilitado").attr("disabled","disabled").val("");   			
		});
		$("input#radioIsentoNao").click(function(){			
			$("#conteinerAssociarTributosPontoConsumo label[for=percentualAliquota], #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] span.campoObrigatorioSimbolo3, label[for=percentualAliquota]").removeClass("rotuloDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").removeClass("campoDesabilitado").removeAttr("disabled");			
		});
		
		$("input#radioDrawbackNao").click(function(){			
			$("#valorDrawback").addClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback] label[for=valorDrawback]").addClass("rotuloDesabilitado");
			$("#valorDrawback").attr("disabled","disabled").addClass("campoDesabilitado").val("");   			
		});
		$("input#radioDrawbackSim").click(function(){			
			$("#valorDrawback").removeClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback] label[for=valorDrawback]").removeClass("rotuloDesabilitado");
			$("#valorDrawback").removeAttr("disabled");	
			
		});
		

		if($("#idPontoConsumoTributo").val()!="-1"){
			$("#conteinerAssociarTributosPontoConsumo .campoData").removeAttr("disabled").removeClass("campoDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo input[type=radio]").removeAttr("disabled");
			$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo .campoRadio + label.rotuloRadio, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo img.ui-datepicker-trigger").css("display","block");
		} else {
			$("input#radioIsentoSim").attr("checked","checked");
			$("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #percentualAliquota").attr("disabled","disabled").addClass("campoDesabilitado").val("");
			$("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #valorDrawback").attr("disabled","disabled").addClass("campoDesabilitado").val("");
			$("#conteinerAssociarTributosPontoConsumo input[type=radio]").attr("disabled","disabled");
			$("#conteinerAssociarTributosPontoConsumo #indicadorIcmsSubstitutoTarifa").attr("disabled","disabled"); 
			$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo label.rotuloRadio, #conteinerAssociarTributosPontoConsumo label.rotuloInformativo, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota], #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=valorDrawback]").addClass("rotuloDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo img.ui-datepicker-trigger").css("display","none");
		}
		
		$("#idPontoConsumoTributo").change(function(){			
			if($(this).val() != -1){
				$("#conteinerAssociarTributosPontoConsumo .campoData").removeAttr("disabled").removeClass("campoDesabilitado");
				$("#conteinerAssociarTributosPontoConsumo input[type=radio]").removeAttr("disabled");
				$("#conteinerAssociarTributosPontoConsumo #creditoIcms").removeAttr("disabled");
				$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo .campoRadio + label.rotuloRadio, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");
				$("#conteinerAssociarTributosPontoConsumo img.ui-datepicker-trigger").css("display","block");
			} else {
				$("input#radioIsentoSim").attr("checked","checked");
				$("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #percentualAliquota").attr("disabled","disabled").addClass("campoDesabilitado").val("");
				$("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #valorDrawback").attr("disabled","disabled").addClass("campoDesabilitado").val("");
				$("#conteinerAssociarTributosPontoConsumo input[type=radio]").attr("disabled","disabled");   
				$("#conteinerAssociarTributosPontoConsumo #indicadorIcmsSubstitutoTarifa").attr("disabled","disabled");  
				$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo label.rotuloRadio, #conteinerAssociarTributosPontoConsumo label.rotuloInformativo, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota], #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=valorDrawback]").addClass("rotuloDesabilitado");
				$("#conteinerAssociarTributosPontoConsumo img.ui-datepicker-trigger").css("display","none");
			}
			
			
		});
	    //FIM: Comportamento dos campos de Exce��es Tribut�rias no Ponto de Consumo
		
	    var indicadorRotaObrigatoria = "<c:out value="${indicadorRotaObrigatoria}"/>";
	    if(indicadorRotaObrigatoria == '0'){
	    	$("#spanRotaPontoConsumo").html('');
	    }

	    var pontoConsumoLegado = $("input#pontoConsumoLegado").val();

	    if(pontoConsumoLegado == "") {
		    AjaxService.obterProximoCodigoLegadoPontoContumo(
	      			{callback: function(codigoLegado) {            		      		         		
	      				if(codigoLegado != "") {
	      					$("input#pontoConsumoLegado").val(codigoLegado);
	          			}
	                }, async: false}
	        );
	    }	
	    
	    
		if($("#cepPontosConsumo").val() != '' && ($("#idQuadraPontoConsumo").val() == -1 || $("#idQuadraPontoConsumo").val() == null)) {
			exibirEnderecocepPontosConsumo(true, false, false, this);
		}
		
		
		if($("#idQuadraPontoConsumo").val() != -1) {
			carregarCampoRotaPorQuadra(document.getElementById('idQuadraPontoConsumo'));
			obterRota();
		}
				
	    
	});
	animatedcollapse.addDiv('conteinerICMSSubstituido', 'fade=0,speed=400,hide=1');
	
	function carregarNumeroImovelPontoConsumo(){	
		if (document.getElementById('numeroImovel') != null) {
			document.getElementById('numeroImovelPontoConsumo').value = document.getElementById('numeroImovel').value; 
		} 
	}

	function carregarComplementoImovelPontoConsumo(){	
		if (document.getElementById('complementoImovel') != null) {
			document.getElementById('descricaoComplementoPontoConsumo').value = document.getElementById('complementoImovel').value; 
		}	
	}
	
	function carregarCampoRotaPorQuadra(elem){
		var codQuadra = elem.value;
		var rotaSelect = document.getElementById("idRota");
		var total = rotaSelect.length;
		var itemsParaLimpar = new Array();
		
		if (codQuadra == -1){
			document.getElementById("idRota").disabled = true;
		}else{
			document.getElementById("idRota").disabled = false;
		}
		
		for(var x = 0; x < total; x++){
			if (x > 0){
				itemsParaLimpar.push(rotaSelect.options[x]);				
			}
		}
		
		for(var x = 0; x < itemsParaLimpar.length; x++){
			rotaSelect.removeChild(itemsParaLimpar[x]);
		}
		
		if (codQuadra != -1){
	    	AjaxService.listarRotasPorSetorComercialDaQuadra(codQuadra,
	      			{callback: function(listaRota) {            		      		         		
	                	for (key in listaRota){
	                    	var novaOpcao = new Option(listaRota[key], key);
	                    	rotaSelect.options[rotaSelect.length] = novaOpcao;
	                    }
	                }, async: false}
	        );			
		}	
	}
	
	function carregarRamosAtividadePontoConsumo2(elem) {	
		
		var idSegmentoPontoConsumo = elem.value;
      	var selectRamoAtividade = document.getElementById("idRamoAtividadePontoConsumo");
    
      	selectRamoAtividade.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
      	  
      	if(idSegmentoPontoConsumo != '-1'){	      		
	       	AjaxService.listarRamoAtividadePorSegmento( idSegmentoPontoConsumo, {
	       		callback:function(ramosAtividade) {            		      		         		
	       			for (ramo in ramosAtividade){
	               		for (key in ramosAtividade[ramo]){
		                	var novaOpcao = new Option(ramosAtividade[ramo][key], key);
		            		selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
	               		}
	            	}
	            	selectRamoAtividade.disabled = false;
	            	$("#idRamoAtividadePontoConsumo").removeClass("campoDesabilitado");
	        	}, async:false}
	        );
	    } else {	    	
	    	selectRamoAtividade.disabled = true;
	    	$("#idRamoAtividadePontoConsumo").addClass("campoDesabilitado");
	    }          	
	}
	
	function exibirAlteracaoPontoConsumo(actionExibirAlteracaoPontoConsumo , idPontoConsumo,indice) {
		if (indice != "") {
			document.forms[0].indexLista.value = indice;
			submeter('imovelForm',  actionExibirAlteracaoPontoConsumo + '#pontosConsumo');
		}
	}

	function exibirAlteracaoPontoConsumoTributoAliquota(idPontoConsumoTributoAliquota,indice, tributo, indicadorIsencao, porcentagemAliquota, dataInicioVigencia, dataFimVigencia, destaqueIndicadorNota,creditoIcms,indicadorIcmsSubstitutoTarifa, indicadorDrawback, valorDrawback) {
		if (indice != "") {
			document.forms[0].indexListaPontoConsumoTributo.value = indice;
			document.getElementById("indexListaPontoConsumoTributo").value = indice;
			document.forms[0].botaoIncluirPontoConsumoTributoAliquota.disabled = true;
			document.forms[0].botaoAlterarPontoConsumoTributoAliquota.disabled = false;			

			var tamanho = document.forms[0].idPontoConsumoTributo.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idPontoConsumoTributo.options[i];
				if (tributo == opcao.value) {
					opcao.selected = true;
				}
			}	

			document.forms[0].dataInicioVigencia.value = dataInicioVigencia;
			document.forms[0].dataFimVigencia.value = dataFimVigencia;	

			if (indicadorIsencao == "true") {
				document.forms[0].radioIsentoNao.checked = false;
				document.forms[0].radioIsentoSim.checked = true;
				
				document.forms[0].percentualAliquota.disabled = true;
				document.forms[0].percentualAliquota.value = '';
				
			} else {
				document.forms[0].percentualAliquota.value = porcentagemAliquota;
				
				document.forms[0].radioIsentoNao.checked = true;
				document.forms[0].radioIsentoSim.checked = false;
								
				document.forms[0].percentualAliquota.disabled = false;
			}
			
			if (destaqueIndicadorNota == "true"){
				document.forms[0].radioDestaqueNotaSim.checked = true;
				document.forms[0].radioDestaqueNotaNao.checked = false;				
			}else{
				document.forms[0].radioDestaqueNotaNao.checked = true;
				document.forms[0].radioDestaqueNotaSim.checked = false;				
			}
			
			if(creditoIcms == "true"){
				document.forms[0].creditoIcms.checked = creditoIcms;
				document.forms[0].indicadorIcmsSubstitutoTarifa.disabled = false;
				document.forms[0].percentualAliquota.disabled = true;
				document.forms[0].percentualAliquota.value = '';
			}else{
				document.forms[0].creditoIcms.checked = false;
			}
			
			
			if(indicadorIcmsSubstitutoTarifa == "true"){
				document.forms[0].indicadorIcmsSubstitutoTarifa.checked = indicadorIcmsSubstitutoTarifa;
			}else{
				document.forms[0].indicadorIcmsSubstitutoTarifa.checked = false;
			}
			
			
			if (indicadorDrawback == "true") {
				document.forms[0].radioDrawbackNao.checked = false;
				document.forms[0].radioDrawbackSim.checked = true;
				
				document.forms[0].valorDrawback.disabled = false;
				document.forms[0].valorDrawback.value= valorDrawback;
				
				
			} else {
				document.forms[0].valorDrawback.value= '';				
				
				document.forms[0].radioDrawbackNao.checked = true;
				document.forms[0].radioDrawbackSim.checked = false;
				
				document.forms[0].valorDrawback.disabled = true;												
			}
			
		}

		if($("input#radioIsentoSim").attr("checked")==true){
			$("#percentualAliquota").addClass("campoDesabilitado");
			$("label[for=percentualAliquota], label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
		} else {
			$("#percentualAliquota").removeClass("campoDesabilitado");
			$("label[for=percentualAliquota], label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");
		}
		$("input#radioIsentoSim").click(function(){			
			$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal:nth-child(odd), #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").addClass("campoDesabilitado").attr("disabled","disabled").val("");   			
		});
		$("input#radioIsentoNao").click(function(){			
			$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal:nth-child(odd), #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").removeClass("campoDesabilitado").removeAttr("disabled");			
			
		});
		
		if($("input#radioDrawbackSim").attr("checked")=="checked"){
			$("#valorDrawback").removeClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback] label[for=valorDrawback]").removeClass("rotuloDesabilitado");
						
		} else {
			$("#valorDrawback").addClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback] label[for=valorDrawback]").addClass("rotuloDesabilitado");
			
		}
		$("input#radioDrawbackSim").click(function(){			
			$("#valorDrawback").removeClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback] label[for=valorDrawback]").removeClass("rotuloDesabilitado");			   			
		});
		$("input#radioDrawbackNao").click(function(){			
			$("#valorDrawback").addClass("campoDesabilitado");
			$("label[for=valorDrawback], label[for=valorDrawback] label[for=valorDrawback]").addClass("rotuloDesabilitado");			
			
		});

		if($("#idPontoConsumoTributo").val()!="-1"){
			$("#conteinerAssociarTributosPontoConsumo .campoData").removeAttr("disabled").removeClass("campoDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo input[type=radio]").removeAttr("disabled");
			$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal:nth-child(even), #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");
		} else {
			$("input#radioIsentoSim").attr("checked","checked");
			$("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #percentualAliquota").attr("disabled","disabled").addClass("campoDesabilitado").val("");
			$("#conteinerAssociarTributosPontoConsumo input[type=radio]").attr("disabled","disabled");    
			$("#conteinerAssociarTributosPontoConsumo #indicadorIcmsSubstitutoTarifa").attr("disabled","disabled");  
			$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
		}
		$("#idPontoConsumoTributo").change(function(){
			if($(this).val() != -1){
				$("#conteinerAssociarTributosPontoConsumo .campoData").removeAttr("disabled").removeClass("campoDesabilitado");
				$("#conteinerAssociarTributosPontoConsumo input[type=radio]").removeAttr("disabled");
				$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal:nth-child(even), #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");
		} else {
	      $("input#radioIsentoSim").attr("checked","checked");
	      $("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #percentualAliquota").attr("disabled","disabled").addClass("campoDesabilitado").val("");
	      $("#conteinerAssociarTributosPontoConsumo input[type=radio]").attr("disabled","disabled");
	      $("#conteinerAssociarTributosPontoConsumo #indicadorIcmsSubstitutoTarifa").attr("disabled","disabled");  
	      $("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
	     }			
		});
				
	}

	function removerPontoConsumo(actionRemoverPontoConsumo, indice) {
		document.forms[0].indexLista.value = indice;
		submeter('imovelForm', actionRemoverPontoConsumo + '?operacaoLista=true#pontosConsumo#pontosConsumo');
	}

	function limparPontoConsumoTributoAliquota() {

		// Tributo
		document.forms[0].idPontoConsumoTributo.value = -1;
		document.forms[0].indexListaPontoConsumoTributo.value = -1;	
		
		document.forms[0].radioIsentoNao.checked = false;
		document.forms[0].radioIsentoSim.checked = true;

		document.forms[0].percentualAliquota.disabled = true;
		document.forms[0].percentualAliquota.value = '';

		document.forms[0].dataInicioVigencia.value = '';
		document.forms[0].dataFimVigencia.value = '';

		document.forms[0].botaoIncluirPontoConsumoTributoAliquota.disabled = false;
		document.forms[0].botaoAlterarPontoConsumoTributoAliquota.disabled = true;

		$("#percentualAliquota").addClass("campoDesabilitado");
		$("label[for=percentualAliquota], label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");

		$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal:nth-child(odd), #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
		
		$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").addClass("campoDesabilitado").attr("disabled","disabled").val("");   

		$("input#radioIsentoSim").attr("checked","checked");
		$("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #percentualAliquota").attr("disabled","disabled").addClass("campoDesabilitado").val("");
		$("#conteinerAssociarTributosPontoConsumo input[type=radio]").attr("disabled","disabled");    
		$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");

      	$("input#radioIsentoSim").attr("checked","checked");
      	$("#conteinerAssociarTributosPontoConsumo .campoData,#conteinerAssociarTributosPontoConsumo #percentualAliquota").attr("disabled","disabled").addClass("campoDesabilitado").val("");
      	$("#conteinerAssociarTributosPontoConsumo input[type=radio]").attr("disabled","disabled");    
      	$("#conteinerAssociarTributosPontoConsumo label.rotuloHorizontal, #conteinerAssociarTributosPontoConsumo label.rotuloEntreCampos, #conteinerAssociarTributosPontoConsumo label[for=percentualAliquota] label[for=percentualAliquota] span.campoObrigatorioSimbolo3, #conteinerAssociarTributosPontoConsumo label[for=dataInicioVigencia] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
	}

	function limparPontoConsumo(limparTributoEquipamento) {

		document.forms[0].indexLista.value = -1;
		document.forms[0].idSegmentoPontoConsumo.value = "-1";
		document.forms[0].idRamoAtividadePontoConsumo.value = "-1";
		document.forms[0].idModalidadeUso.value = "-1";
		document.forms[0].enderecoReferenciaPontoConsumo.value = "";
		document.forms[0].descricaoPontoConsumo.value = "";
		document.forms[0].numeroSequenciaLeitura.value = "";
		document.forms[0].numeroImovelPontoConsumo.value = "";
		document.forms[0].descricaoComplementoPontoConsumo.value = "";
		document.forms[0].cepPontosConsumo.value = "";
		document.forms[0].observacaoPontoConsumo.value = "";
		//document.forms[0].idRamoAtividadePontoConsumo.disabled = true;
		document.forms[0].idQuadraPontoConsumo.value = "-1";
		document.forms[0].idQuadraPontoConsumo.disabled = true;
		document.forms[0].idQuadraFacePontoConsumo.value = "-1";
		document.forms[0].idQuadraFacePontoConsumo.disabled = true;
		document.forms[0].enderecoRemotoPontoConsumo.value = "";
		document.forms[0].pontoConsumoLegado.value = "";
		
		document.forms[0].latitudeGrau.value = "";
		document.forms[0].longitudeGrau.value = "";
		document.forms[0].idRota.value = "-1";
		document.forms[0].idRota.disabled = true;
		document.forms[0].numeroCep.value = "";
		document.forms[0].latitudeGrau.value = "";
		document.forms[0].longitudeGrau.value = "";			

		var botaoAlterarPontoConsumo = document.getElementById("botaoAlterarPontoConsumo");
		var botaoLimparPontoConsumo = document.getElementById("botaoLimparPontoConsumo");	
		var botaoIncluirPontoConsumo = document.getElementById("botaoIncluirPontoConsumo");	
		
		botaoAlterarPontoConsumo.disabled = true;
		botaoLimparPontoConsumo.disabled = false;

		if (botaoIncluirPontoConsumo != null) 
			botaoIncluirPontoConsumo.disabled = false;

		limparPontoConsumoTributoAliquota();
		limparPontoConsumoEquipamento();

		<c:if test="${abaId == '5'}">
			if (limparTributoEquipamento) {
				submeter('imovelForm','<c:out value="${param['actionLimparPontoConsumoTributoEquipamento']}"/>' + '#pontosConsumo');
			}
		</c:if>
	}

	function removerPontoConsumoTributoAliquota(actionRemoverPontoConsumoTributoAliquota, indice) {
		document.getElementById("indexListaPontoConsumoTributo").value = indice;
		document.forms[0].indexListaPontoConsumoTributo.value = indice;
		submeter('imovelForm',actionRemoverPontoConsumoTributoAliquota + '?operacaoListaTributo=true#pontosConsumo');
	}	
	
	function adicionarPontoConsumoTributoAliquota(form, actionAdicionarPontoConsumoTributoAliquota) {
		document.getElementById("indexListaPontoConsumoTributo").value = -1;
		submeter('imovelForm', actionAdicionarPontoConsumoTributoAliquota + '#pontosConsumo');
	}

	function alterarPontoConsumoTributoAliquota(form, actionAlterarPontoConsumoTributoAliquota) {
		document.getElementById("indexListaPontoConsumoTributo").value = document.getElementById("indexListaPontoConsumoTributo").value;
		submeter('imovelForm', actionAlterarPontoConsumoTributoAliquota + '?operacaoListaTributo=true#pontosConsumo');
	}	
	
	function incluirPontoConsumo(form, actionAdicionarPontoConsumo) {
		checarIndicadorMensagemAdicional();
		document.forms[0].indexLista.value = -1;
		submeter('imovelForm', actionAdicionarPontoConsumo + '#pontosConsumo');
	}
	
	function carregarCityGate(form, actionCarregarCityGate) {
		document.getElementById("botaoIncluirPontoConsumo").disabled = true;
		submeter('imovelForm', actionCarregarCityGate + '#pontosConsumo');
	}

	function onloadPontoConsumo() {
		
		var indicadorMensagemAdicional = $("input[name=indicadorMensagemAdicional]:checked").val();
		if(indicadorMensagemAdicional == "false") {
			$("#mensagemAdicional").css("display", "none");
		}

		var botaoIncluirPontoConsumoTributoAliquota = document.getElementById("botaoIncluirPontoConsumoTributoAliquota");
		var botaoAlterarPontoConsumoTributoAliquota = document.getElementById("botaoAlterarPontoConsumoTributoAliquota");

		var botaoIncluirPontoConsumoEquipamento = document.getElementById("botaoIncluirPontoConsumoEquipamento");
		var botaoAlterarPontoConsumoEquipamento= document.getElementById("botaoAlterarPontoConsumoEquipamento");

		<c:choose>
			<c:when test="${estadoAlteracaoPontoConsumoTributo}">
				botaoIncluirPontoConsumoTributoAliquota.disabled = true;
				botaoAlterarPontoConsumoTributoAliquota.disabled = false;
			</c:when>

			<c:when test="${empty estadoAlteracaoPontoConsumoTributo}">
				botaoIncluirPontoConsumoTributoAliquota.disabled = false;
				botaoAlterarPontoConsumoTributoAliquota.disabled = true;
			</c:when>	
		</c:choose>	

		<c:choose>
			<c:when test="${estadoAlteracaoPontoConsumoEquipamento}">
				botaoIncluirPontoConsumoEquipamento.disabled = true;
				botaoAlterarPontoConsumoEquipamento.disabled = false;
			</c:when>
	
			<c:when test="${empty estadoAlteracaoPontoConsumoEquipamento}">
				botaoIncluirPontoConsumoEquipamento.disabled = false;
				botaoAlterarPontoConsumoEquipamento.disabled = true;
			</c:when>	
		</c:choose>	

		<c:choose>
			<c:when test="${sucessoManutencaoLista}">
				<c:if test="${imovelVO.indexLista == -1}">
					limparPontoConsumo(false);
										
					var botaoAlterarPontoConsumo = document.getElementById("botaoAlterarPontoConsumo");
					var botaoLimparPontoConsumo = document.getElementById("botaoLimparPontoConsumo");
					var botaoIncluirPontoConsumo = document.getElementById("botaoIncluirPontoConsumo"); 
			
					botaoIncluirPontoConsumo.disabled = false;
					botaoAlterarPontoConsumo.disabled = true;	
					botaoLimparPontoConsumo.disabled = false;
				</c:if>

				<c:if test="${imovelVO.indexLista > -1 && empty param['operacaoLista']}">
					var botaoAlterarPontoConsumo = document.getElementById("botaoAlterarPontoConsumo");
					var botaoLimparPontoConsumo = document.getElementById("botaoLimparPontoConsumo");	
					var botaoIncluirPontoConsumo = document.getElementById("botaoIncluirPontoConsumo");	
					botaoAlterarPontoConsumo.disabled = false;
					botaoLimparPontoConsumo.disabled = false;
					botaoIncluirPontoConsumo.disabled = true;
				</c:if>	

				<c:if test="${imovelVO.indexLista > -1 && requestScope.paramAlteracao}">
					limparPontoConsumo(false);
				</c:if>

				if (document.getElementById("idQuadraFacePontoConsumo") != null && document.getElementById("idQuadraFacePontoConsumo").value != -1) {
					var objPontoConsumoCityGate = document.getElementById('pontoConsumoCityGate');
					if (objPontoConsumoCityGate != undefined) {
						objPontoConsumoCityGate.style.display = "";
					}
				}				

			</c:when>
			<c:when test="${sucessoManutencaoListaTributo}">
				<c:if test="${imovelPontoConsumoVO.indexListaPontoConsumoTributo == -1 && empty param['operacaoLista']}">
					limparPontoConsumoTributoAliquota();
				</c:if>
				
				<c:if test="${imovelPontoConsumoVO.indexListaPontoConsumoTributo > -1 && param['operacaoListaTributo']}">
					limparPontoConsumoTributoAliquota();
				</c:if>
				
				var botaoAlterarPontoConsumo = document.getElementById("botaoAlterarPontoConsumo");
				var botaoLimparPontoConsumo = document.getElementById("botaoLimparPontoConsumo");	
				var botaoIncluirPontoConsumo = document.getElementById("botaoIncluirPontoConsumo");
				botaoLimparPontoConsumo.disabled = false;
				botaoAlterarPontoConsumo.disabled = true;
				botaoIncluirPontoConsumo.disabled = false;
				
				<c:if test="${estadoAlteracaoPontoConsumo}">
					botaoAlterarPontoConsumo.disabled = false;
					botaoIncluirPontoConsumo.disabled = true;
				</c:if>

			</c:when>	
			<c:when test="${sucessoManutencaoListaEquipamento}">
				<c:if test="${imovelPontoConsumoVO.indexListaPontoConsumoEquipamento == -1 && empty param['operacaoLista']}">
					limparPontoConsumoEquipamento();
				</c:if>
				
				<c:if test="${imovelPontoConsumoVO.indexListaPontoConsumoEquipamento > -1 && param['operacaoListaEquipamento']}">
					limparPontoConsumoEquipamento();
				</c:if>
				
				var botaoAlterarPontoConsumo = document.getElementById("botaoAlterarPontoConsumo");
				var botaoLimparPontoConsumo = document.getElementById("botaoLimparPontoConsumo");	
				var botaoIncluirPontoConsumo = document.getElementById("botaoIncluirPontoConsumo");
				botaoLimparPontoConsumo.disabled = false;
				botaoAlterarPontoConsumo.disabled = true;
				botaoIncluirPontoConsumo.disabled = false;
				
				<c:if test="${estadoAlteracaoPontoConsumo}">
					botaoAlterarPontoConsumo.disabled = false;
					botaoIncluirPontoConsumo.disabled = true;
				</c:if>
			</c:when>			
			<c:when test="${estadoAlteracaoPontoConsumo}">
				var botaoAlterarPontoConsumo = document.getElementById("botaoAlterarPontoConsumo");
				var botaoLimparPontoConsumo = document.getElementById("botaoLimparPontoConsumo");	
				var botaoIncluirPontoConsumo = document.getElementById("botaoIncluirPontoConsumo");

				botaoLimparPontoConsumo.disabled = false;
				botaoAlterarPontoConsumo.disabled = false;
				botaoIncluirPontoConsumo.disabled = true;							

			</c:when>			
		</c:choose>
	}

	<c:if test="${abaId == '5'}">
		addLoadEvent(onloadPontoConsumo);	
	</c:if>

	function limparDiv(elem){
		var codQuadra = elem.value;

		if(codQuadra == '-1'){
			var objPontoConsumoCityGate = document.getElementById('pontoConsumoCityGate');
			if (objPontoConsumoCityGate != undefined) {
				objPontoConsumoCityGate.style.display = "none";
			}		
		} 
	}
	

	function habilitarAliquotaIcmsSubstituto(obj){
		
		 if (obj.checked == true){
			$("#percentualAliquota").addClass("campoDesabilitado");
			$("label[for=percentualAliquota], label[for=percentualAliquota] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
			$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").addClass("campoDesabilitado").attr("disabled","disabled").val("");
		}else{
			if($("input#radioIsentoSim:checked").val()=='true'){	  		
				$("#percentualAliquota").addClass("campoDesabilitado");
				$("label[for=percentualAliquota], label[for=percentualAliquota] span.campoObrigatorioSimbolo3").addClass("rotuloDesabilitado");
				$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").addClass("campoDesabilitado").attr("disabled","disabled").val("");
			} else {			
				$("#percentualAliquota").removeClass("campoDesabilitado");
				$("label[for=percentualAliquota], label[for=percentualAliquota] span.campoObrigatorioSimbolo3").removeClass("rotuloDesabilitado");  
				$("#conteinerAssociarTributosPontoConsumo #percentualAliquota").removeClass("campoDesabilitado").removeAttr("disabled");
			}
		}
			
	}			
	
	function desabilitarIcmsSubstituto(obj){
		
		if (obj.checked == true){
			$("#conteinerAssociarTributosPontoConsumo #indicadorIcmsSubstitutoTarifa").removeAttr("disabled");
			
		}else{
			$("#conteinerAssociarTributosPontoConsumo #indicadorIcmsSubstitutoTarifa").attr("disabled","disabled");  
			
		}
	}

	function carregarQuadraFacePontoConsumo() {

		if (document.getElementById('idQuadraFace') != '') {
			var objPontoConsumoCityGate = document.getElementById('pontoConsumoCityGate');
			if (objPontoConsumoCityGate != undefined) {
				objPontoConsumoCityGate.style.display = "none";
			}		
	
			document.forms[0].idQuadraFacePontoConsumo.disabled = false;
			document.forms[0].idQuadraFacePontoConsumo.value = document.forms[0].idQuadraFace.value;
			document.getElementById("botaoIncluirPontoConsumo").disabled = true;
			console.log('${param['actionCarregarCityGate']}')
			submeter('imovelForm', '${param['actionCarregarCityGate']}');
		}

		$("#cepImovel").removeClass("campoDesabilitado");
		$("#cepImovel").removeAttr("readonly");
		$("#numeroImovel").removeClass("campoDesabilitado");
		$("#numeroImovel").removeClass("campoDesabilitado");

	}	

	function exibirPopupPesquisaEquipamento() {
		popup = window.open('exibirPesquisaEquipamentoPopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarEquipamento(idEquipamento) {

		var idEquipamentoForm = document.getElementById("idEquipamento");
		var descricaoEquipamento = document.getElementById("descricaoEquipamento");
		var descricaoSegmento = document.getElementById("descricaoSegmento");
		
		var potenciaFixaAlta = document.getElementById("potenciaFixaAlta");
		var potenciaFixaMedia = document.getElementById("potenciaFixaMedia");
		var potenciaFixaBaixa = document.getElementById("potenciaFixaBaixa");
		
		var potencia = document.getElementById("potencia");
		
		if(idEquipamento != '') {				
			AjaxService.obterEquipamentoPorChave( idEquipamento, {
	           	callback: function(equipamento) {	           		
	           		if(equipamento != null){
	           			           			
	           			idEquipamentoForm.value = equipamento["chavePrimaria"];           			        		      		         		
	           			descricaoEquipamento.value = equipamento["descricao"];
	           			descricaoSegmento.value = equipamento["segmentoDescricao"];
	           			
	           			potenciaFixaAlta.value = equipamento["potenciaFixaAlta"];      
	           			aplicarMascaraNumeroDecimal(potenciaFixaAlta, 2);
	           			potenciaFixaMedia.value = equipamento["potenciaFixaMedia"];
	           			aplicarMascaraNumeroDecimal(potenciaFixaMedia, 2);
	           			potenciaFixaBaixa.value = equipamento["potenciaFixaBaixa"];
	           			aplicarMascaraNumeroDecimal(potenciaFixaBaixa, 2);
	           			
	           			potencia.value = equipamento["potenciaPadrao"];
	           			aplicarMascaraNumeroDecimal(potencia, 2);
	           				           			
	           			
	           			var indPotenciaFixa = equipamento["indPotenciaFixa"];	           			
	           			var idSegmento = Number(equipamento["idSegmento"]);
	           			
	        			if(indPotenciaFixa == "true" ) {
	        				
	        				$("#idPotenciaFixa").removeAttr("disabled").removeClass("campoDesabilitado");
	        	   			$("#potencia").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	        	   			$("#horasPorDia").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	        	   			$("#diasPorSemana").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	        	   			
	        			} else {
	        				
	        	   			$("#potencia").removeAttr("disabled").removeClass("campoDesabilitado");
	        	   			$("#horasPorDia").removeAttr("disabled").removeClass("campoDesabilitado");
	        	   			$("#diasPorSemana").removeAttr("disabled").removeClass("campoDesabilitado");    
	        	   			$("#idPotenciaFixa").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	        	   			$("#valorPotenciaFixa").val("");
	        	   			
	        			}
	        			
	           			$("#qtdeEquipamentos").removeAttr("disabled").removeClass("campoDesabilitado");            	
	               	}
	        	}, async:false}
	        	
	        );
	        
        } 
	}

	function limparPontoConsumoEquipamento() {

		// Equipamento
		document.getElementById("idEquipamento").value = -1;
		document.getElementById("potencia").value = '';	
		document.getElementById("horasPorDia").value = '';
		document.getElementById("diasPorSemana").value = '';
		document.getElementById("qtdeEquipamentos").value = '';
		document.getElementById("idPotenciaFixa").value = -1;
		document.getElementById("potencia").value = '';
		document.getElementById("potenciaFixaAlta").value = '';
		document.getElementById("potenciaFixaMedia").value = '';
		document.getElementById("potenciaFixaBaixa").value = '';

		document.getElementById("descricaoEquipamento").value = '';
		document.getElementById("descricaoSegmento").value = '';

		$("#potencia").attr("disabled","disabled").addClass("campoDesabilitado");
		$("#horasPorDia").attr("disabled","disabled").addClass("campoDesabilitado");
		$("#diasPorSemana").attr("disabled","disabled").addClass("campoDesabilitado");
		$("#qtdeEquipamentos").attr("disabled","disabled").addClass("campoDesabilitado");
		$("#idPotenciaFixa").attr("disabled","disabled").addClass("campoDesabilitado");
		
		document.forms[0].botaoIncluirPontoConsumoEquipamento.disabled = false;
		document.forms[0].botaoAlterarPontoConsumoEquipamento.disabled = true;	
	}

	function adicionarPontoConsumoEquipamento(form, actionAdicionarPontoConsumoEquipamento) {
		document.getElementById("indexListaPontoConsumoEquipamento").value = -1;
		submeter('imovelForm', actionAdicionarPontoConsumoEquipamento + '#pontosConsumo');
	}

	function alterarPontoConsumoEquipamento(form, actionAlterarPontoConsumoEquipamento) {
		document.getElementById("indexListaPontoConsumoEquipamento").value = document.getElementById("indexListaPontoConsumoEquipamento").value;
		submeter('imovelForm', actionAlterarPontoConsumoEquipamento + '?operacaoListaEquipamento=true#pontosConsumo');
	}	

	function exibirAlteracaoPontoConsumoEquipamento(idPontoConsumoEquipamento,indice, equipamento, descricao,
			idSegmento, segmentoDesc, potencia, horasPorDia, diasPorSemana, idPotenciaFixa, potenciaFixaAlta, potenciaFixaMedia, potenciaFixaBaixa, indPotenciaFixa) {
		if (indice != "") {
			document.forms[0].indexListaPontoConsumoEquipamento.value = indice;
			document.getElementById("indexListaPontoConsumoEquipamento").value = indice;
			document.forms[0].botaoIncluirPontoConsumoEquipamento.disabled = true;
			document.forms[0].botaoAlterarPontoConsumoEquipamento.disabled = false;	

			document.getElementById("idEquipamento").value = equipamento;
			document.getElementById("descricaoEquipamento").value = descricao;
			document.getElementById("descricaoSegmento").value = segmentoDesc;	
			document.getElementById("potencia").value = potencia;	
			document.getElementById("horasPorDia").value = horasPorDia;
			document.getElementById("diasPorSemana").value = diasPorSemana;
			document.getElementById("idPotenciaFixa").value = idPotenciaFixa;
			document.getElementById("potenciaFixaAlta").value = potenciaFixaAlta;
			document.getElementById("potenciaFixaMedia").value = potenciaFixaMedia;
			document.getElementById("potenciaFixaBaixa").value = potenciaFixaBaixa;
			alterarPotenciaFixa();

			if(indPotenciaFixa == "true" ) {
				
				$("#idPotenciaFixa").removeAttr("disabled").removeClass("campoDesabilitado");
	   			$("#potencia").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   			$("#horasPorDia").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   			$("#diasPorSemana").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   			
			} else {
				
	   			$("#potencia").removeAttr("disabled").removeClass("campoDesabilitado");
	   			$("#horasPorDia").removeAttr("disabled").removeClass("campoDesabilitado");
	   			$("#diasPorSemana").removeAttr("disabled").removeClass("campoDesabilitado");    
	   			$("#idPotenciaFixa").attr("disabled","disabled").addClass("campoDesabilitado").val("").attr("style","");
	   			$("#valorPotenciaFixa").val("");
	   			
			}
		}
	}

	function removerPontoConsumoEquipamento(actionRemoverPontoConsumoEquipamento, indice) {
		document.getElementById("indexListaPontoConsumoEquipamento").value = indice;
		document.forms[0].indexListaPontoConsumoEquipamento.value = indice;
		submeter('imovelForm',actionRemoverPontoConsumoEquipamento + '?operacaoListaEquipamento=true#pontosConsumo');
	}
	
	function alterarPotenciaFixa() {
		
		var idPotenciaFixa = document.getElementById("idPotenciaFixa").value;
		var idEquipamento = document.getElementById("idEquipamento").value;
		
		var campoValorPotenciaFixa = document.getElementById("valorPotenciaFixa");
		
		if(idPotenciaFixa != '') {				
			AjaxService.obterValorPotenciaFixaPorChave( idPotenciaFixa, idEquipamento, {
	           	callback: function(potenciaFixa) {	           		
	           		if(potenciaFixa != null){	           			           			
	           			campoValorPotenciaFixa.value = potenciaFixa["valor"];            	
	               	}
	        	}, async:false}
	        	
	        );	
		}
	}
	
	
	function selecionaMensagemAdicional(elem) {
		if (elem.value == "true") {
			$("#mensagemAdicional").fadeIn(500);
		} else {
			$("#mensagemAdicional").fadeOut(500);
		}
	}
	
	
	function checarIndicadorMensagemAdicional(){
		var indicadorMensagemAdicional = $("input[name=indicadorMensagemAdicional]:checked").val();
		if(indicadorMensagemAdicional == "false") {
			$("#descricaoMensagemAdicional").val("");
		}
	}

	function irParaGoogleMaps(){
		var latitude = $("#latitudeGrau").val().replace(",", ".");
		var longitude = $("#longitudeGrau").val().replace(",", ".");

		if(latitude != "" && longitude != "") {
			var url = "https://www.google.com/maps/search/?api=1&query="+latitude+"%2C"+longitude;
			window.open(url, '_blank').focus();
		} else {
			alert("Latitude e Longitude devem estar preenchidos para abrir o Google Maps.");
		}
	}

	function obterRota(){
		var chaveCep = $("#chaveCep").val().split(",")[0];
		var numero = $("#numeroImovelPontoConsumo").val();

	    AjaxService.obterRotaProximaCepNumero(chaveCep, numero,
      			{
  					callback: function(chaveRota) {
  						if(chaveRota != "") {
	  						$('#idRota option[value='+chaveRota+']').attr('selected','selected');           		      		         		
  						}
                	},async: true
    			}
        );	  
	}



	addLoadEvent(onloadPontoConsumo);

</script>

<input name="indexListaPontoConsumoTributo" type="hidden" id="indexListaPontoConsumoTributo" value="${imovelPontoConsumoVO.indexListaPontoConsumoTributo}">
<input name="indexListaPontoConsumoEquipamento" type="hidden" id="indexListaPontoConsumoEquipamento" value="${imovelPontoConsumoVO.indexListaPontoConsumoEquipamento}">
<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${imovelPontoConsumoVO.idPontoConsumo}">
<input id="numeroSequenciaLeitura" type="hidden" name="numeroSequenciaLeitura" value="${imovelPontoConsumoVO.numeroSequenciaLeitura}">

<a class="linkHelp" href="<help:help>/abapontosdeconsumocadastrodoimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
<fieldset id="imovelAbaPontosConsumo" class="conteinerBloco">
	<fieldset class="coluna">
		<legend class="conteinerBlocoTitulo">Dados do Ponto de Consumo</legend>
		
		<label class="rotulo rotulo2LinhasAlinhado" id="rotuloPontoConsumoLegado" for="pontoConsumoLegado">C&oacute;digo do Ponto de Consumo:</label>
        <input class="campoTexto campo2LinhasAlinhado" type="text" id="pontoConsumoLegado" name="pontoConsumoLegado" maxlength="9" size="9" value="${imovelPontoConsumoVO.pontoConsumoLegado}"
            onkeypress="return formatarCampoInteiro(event);"><br/><br/>
            
		<label class="rotulo campoObrigatorioAlinhado" id="rotuloDescricaoPontoConsumo" for="descricaoPontoConsumo"><span class="campoObrigatorioSimbolo2">* </span>Descri��o:</label>
		<input class="campoTexto campoAlinhado" type="text" id="descricaoPontoConsumo" name="descricaoPontoConsumo" maxlength="100" size="35" value="${imovelPontoConsumoVO.descricaoPontoConsumo}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		
		<label class="rotulo campoObrigatorio " id="rotuloSegmento idSegmentoPontoConsumoPosition" for="idSegmentoPontoConsumo"><span class="campoObrigatorioSimbolo2">* </span>Segmento:</label>
		<select name="idSegmentoPontoConsumo" id="idSegmentoPontoConsumo" class="campoSelect campo2Linhas" onchange="carregarRamosAtividadePontoConsumo2(this);">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaSegmento}" var="segmento">
				<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${imovelPontoConsumoVO.idSegmentoPontoConsumo == segmento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${segmento.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<br />
		<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloRamoAtividade" for="idRamoAtividadePontoConsumo"><span class="campoObrigatorioSimbolo2">* </span>Ramo de Atividade:</label>
		<select name="idRamoAtividadePontoConsumo" id="idRamoAtividadePontoConsumo" class="campoSelect campo2Linhas" <c:if test="${empty listaRamoAtividadePontoConsumo}">disabled="disabled""</c:if>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaRamoAtividadePontoConsumo}" var="ramoAtividade">
				<option value="<c:out value="${ramoAtividade.chavePrimaria}"/>" <c:if test="${imovelPontoConsumoVO.idRamoAtividadePontoConsumo == ramoAtividade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${ramoAtividade.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<br />
		<label class="rotulo rotulo2Linhas" id="rotuloModalidadeUso" for="idModalidadeUso">Modalidade de Uso:</label>
		<select name="idModalidadeUso" id="idModalidadeUso" class="campoSelect campo2Linhas">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaModalidadeUso}" var="modalidadeUso">
				<option value="<c:out value="${modalidadeUso.chavePrimaria}"/>" <c:if test="${imovelPontoConsumoVO.idModalidadeUso == modalidadeUso.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modalidadeUso.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<fieldset class="exibirCep">
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="cepObrigatorio" value="true"/>
				<jsp:param name="styleClassCepObrigatorio" value="campoObrigatorioSimbolo2"/>
				<jsp:param name="numeroCep" value="${imovelPontoConsumoVO.cepPontosConsumo}"/>
				<jsp:param name="idCampoCep" value="cepPontosConsumo"/>
				<jsp:param name="idCampoCidade" value="cidadePontosConsumo"/>
				<jsp:param name="idCampoUf" value="ufPontosConsumo"/>
				<jsp:param name="idCampoLogradouro" value="logradouroPontosConsumo"/>
			</jsp:include>
		</fieldset>
		<label class="rotulo" id="rotuloNumeroImovel" for="numeroEndereco">N�mero:</label>
		<input class="campoTexto" type="text" id="numeroImovelPontoConsumo" name="numeroImovelPontoConsumo" onkeyup="this.value = this.value.toUpperCase();" onkeyup="this.value = this.value.toUpperCase();" maxlength="5" size="5" value="${imovelPontoConsumoVO.numeroImovelPontoConsumo}"><br />
		<label class="rotulo" id="rotuloComplemento" for="complementoEndereco">Complemento:</label>
		<input class="campoTexto" type="text" id="descricaoComplementoPontoConsumo" name="descricaoComplementoPontoConsumo" maxlength="255" size="30" value="${imovelPontoConsumoVO.descricaoComplementoPontoConsumo}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"><br />
		<label class="rotulo" for="enderecoReferenciaPontoConsumo">Refer�ncia:</label>
		<input class="campoTexto" type="text" id="enderecoReferenciaPontoConsumo" name="enderecoReferenciaPontoConsumo" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="30" size="30" value="${imovelPontoConsumoVO.enderecoReferenciaPontoConsumo}"><br />
		<label class="rotulo campoObrigatorio" ><span class="campoObrigatorioSimbolo2">* </span>Quadra: </label>
		<select name="idQuadraPontoConsumo" id="idQuadraPontoConsumo" class="campoSelect" onchange="carregarQuadraFace2(this);carregarCampoRotaPorQuadra(this);obterRota();limparDiv(this);" <c:if test="${empty imovelPontoConsumoVO.cepPontosConsumo}">disabled="disabled"</c:if>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaQuadraPontoConsumo}" var="quadra">
				<option value="<c:out value="${quadra.chavePrimaria}"/>" <c:if test="${imovelPontoConsumoVO.idQuadraPontoConsumo == quadra.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${quadra.numeroQuadra}"/>
				</option>		
			</c:forEach>
		</select><br />
		
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo2">* </span>Face da Quadra: </label>
		<select name="idQuadraFacePontoConsumo" id="idQuadraFacePontoConsumo" class="campoSelect" onchange="carregarCityGate(this.form,'<c:out value="${param['actionCarregarCityGate']}"/>');" <c:if test="${imovelPontoConsumoVO.idQuadraPontoConsumo eq null}">disabled="disabled"</c:if>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaFacesQuadraPontoConsumo}" var="faceQuadra">
				<option value="<c:out value="${faceQuadra.chavePrimaria}"/>" <c:if test="${imovelPontoConsumoVO.idQuadraFacePontoConsumo == faceQuadra.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${faceQuadra.quadraFaceFormatada}"/>
				</option>		
			</c:forEach>
		</select><br />
		
		<label class="rotulo campoObrigatorio"><span id="spanRotaPontoConsumo" class="campoObrigatorioSimbolo2">* </span>Rota: </label>
		<select name="idRota" id="idRota" class="campoSelect" <c:if test="${imovelPontoConsumoVO.idRota eq null}">disabled="disabled"</c:if>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaRotaQuadraPontoConsumo}" var="itemRota">
				<option value="<c:out value="${itemRota.chavePrimaria}"/>" <c:if test="${imovelPontoConsumoVO.idRota == itemRota.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${itemRota.numeroRota}"/>
				</option>		
			</c:forEach>
		</select><br />
		
		<div>
		<label class="rotulo" for="latitudeGrau">Lat Grau: </label>
		<input class="campoTexto" type="text" id="latitudeGrau" name="latitudeGrau" 
				value="${imovelPontoConsumoVO.latitudeGrau}" maxlength="16"/>
		<a href="javascript:irParaGoogleMaps();"><img class="alinhamentoGoogleMaps" style="display:inline-block;padding: 0 20px;margin-right: 100px;height: 50px;float: right;" border="0" src="<c:url value="/imagens/google-maps.png"/>"/></a>				
		<br />
		
			<label class="rotulo" for="longitudeGrau">Long Grau: </label>
			<input class="campoTexto" type="text" id="longitudeGrau" name="longitudeGrau" 
					value="${imovelPontoConsumoVO.longitudeGrau}" maxlength="16" />
		<br />
		</div>
		
		<label class="rotulo" for="enderecoRemotoPontoConsumo">Endere�o Remoto:</label>
		<input class="campoTexto" type="text" id="enderecoRemotoPontoConsumo" name="enderecoRemotoPontoConsumo" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="50" size="30" value="${imovelPontoConsumoVO.enderecoRemotoPontoConsumo}"><br />
		<br />
		
		<label class="rotulo rotulo2Linhas" for="indicadorClassificacaoFiscal">N�o Utilizar Classifica��o Fiscal na Integra��o:</label>
		<input type="checkbox" class="campoCheckbox" id="indicadorClassificacaoFiscal" name="indicadorClassificacaoFiscal" value="true" <c:if test="${imovelPontoConsumoVO.indicadorClassificacaoFiscal eq 'true'}">checked="checked"</c:if>>
		<br />
		
		<label class="rotulo" for="indicadorUsoPontoConsumo">Indicador de Uso:</label>
		<input class="campoRadio" id="habilitadoPontoConsumo1" name="habilitadoPontoConsumo" value="true" type="radio" <c:if test="${imovelPontoConsumoVO.habilitadoPontoConsumo eq true}">checked="checked"</c:if>><label class="rotuloRadio" for="habilitadoPontoConsumo1">Ativo</label>
		<input class="campoRadio" id="habilitadoPontoConsumo2" name="habilitadoPontoConsumo" value="false" type="radio" <c:if test="${imovelPontoConsumoVO.habilitadoPontoConsumo eq false}">checked="checked"</c:if>><label class="rotuloRadio" for="habilitadoPontoConsumo2">Inativo</label>

		<label class="rotulo" for="observacaoPontoConsumo">Observa��o:</label>
		<textarea id="observacaoPontoConsumo" name="observacaoPontoConsumo" rows="5" cols="40" maxlength="200">${imovelPontoConsumoVO.observacaoPontoConsumo}</textarea><br/>
		<br />
		<br />

        <br />
		<c:if test="${not empty requestScope.listaCityGate}">
			<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
			<legend class="campoObrigatorio"><span class="campoObrigatorioSimbolo2">* </span>Percentual por City Gate:</legend>
		  	<label class="rotulo">In�cio de Vig�ncia:</label>
		  	<input class="campoData campoHorizontal" type="text" id="dataInicioVigenciaCityGate" name="dataInicioVigenciaCityGate" maxlength="10" value="${imovelVO.dataInicioVigenciaCityGate}" ><br />
			<table class="dataTableGGAS dataTableDialog">
				<thead>
					<tr>
						<th>City Gate</th>
						<th style="width: 100px">Participa��o</th>
					</tr>
				</thead>
				<tbody>
					<c:set var="classeLinha" value="odd"/>
					<c:forEach items="${listaCityGate}" var="cityGate" varStatus="i">
						<tr class="${classeLinha}"><td class="rotuloTabela"><label class="rotulo"><c:out value="${cityGate.descricao}"/></label></td>
							<c:choose>
								<c:when test="${sizeListaCityGate > 1}">
									<td class="campoTabela">
										<input class="campoTexto campoHorizontal" type="text" id="percentualCityGate" name="percentualCityGate" maxlength="7" size="7" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 4);" onblur="return formatarCampoDecimalPositivo(event, this, 3, 4);" value="${imovelVO.percentualCityGate[i.index]}">
								</c:when>
								<c:otherwise>
									<td class="campoTabela">
										<input class="campoTexto campoHorizontal" type="text" maxlength="5" size="6" value="${imovelVO.percentualCityGate[i.index]}" disabled="disabled">
										<input name="percentualCityGate" type="hidden" value="${imovelVO.percentualCityGate[i.index]}">
								</c:otherwise>
							</c:choose>
						</tr>
						<c:choose>
							<c:when test="${classeLinha eq 'odd'}">
								<c:set var="classeLinha" value="even"/>
							</c:when>
							<c:otherwise>
								<c:set var="classeLinha" value="odd"/>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tbody>
			</table>
		</c:if>		
				
		<hr class="linhaSeparadora" />
		
		<fieldset id="pesquisarEquipamentoPontoDeConsumo" class="pontoConsumoPesquisarEquipamento">
			<legend class="conteinerBlocoTitulo">Equipamentos do Ponto de Consumo</legend>
			<div class="pesquisarEquipamentoFundo">
				<input name="idEquipamento" type="hidden" id="idEquipamento" value="${imovelPontoConsumoVO.idEquipamento}">
				<input name="potenciaFixaAlta" type="hidden" id="potenciaFixaAlta" value="${imovelForm.map.potenciaFixaAlta}">
				<input name="potenciaFixaMedia" type="hidden" id="potenciaFixaMedia" value="${imovelForm.map.potenciaFixaMedia}">
				<input name="potenciaFixaBaixa" type="hidden" id="potenciaFixaBaixa" value="${imovelForm.map.potenciaFixaBaixa}">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Equipamento</span> para selecionar um equipamento.</p>
				<p class="orientacaoInterna">Apenas ser�o aceitos equipamentos do mesmo segmento do ponto de consumo.</p>
				<input type="button" id="botaoPesquisarEquipamento" class="bottonRightCol2" title="Pesquisar Equipamento"  value="Pesquisar Equipamento" onclick="exibirPopupPesquisaEquipamento();" type="button"/><br />
				<label class="rotulo" for="descricaoEquipamento">Equipamento:</label>
				<input class="campoDesabilitado" type="text" id="descricaoEquipamento" value="${imovelPontoConsumoVO.descricaoEquipamento}" name="descricaoEquipamento" maxlength="200" size="20" disabled="disabled"/>
				<label class="rotulo" for="descricaoSegmento">Segmento:</label>
				<input class="campoDesabilitado" type="text" id="descricaoSegmento" value="${imovelPontoConsumoVO.descricaoSegmento}" name="descricaoSegmento" maxlength="50" size="20" disabled="disabled"/>
			</div>
		</fieldset>
		
		<fieldset id="camposPontoConsumoEquipamento" class="pontoConsumoPesquisarEquipamento">
			<label class="rotulo rotulo2Linhas campoObrigatorio3" for="potencia"><span class="campoObrigatorioSimbolo3">* </span>Pot�ncia Nominal:</label> 
			<input class="campoTexto campoDesabilitado" type="text" id="potencia" name="potencia" maxlength="16" size="16" value="${imovelPontoConsumoVO.potencia}" onkeypress="return formatarCampoDecimalPositivo(event, this, 10, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" disabled="disabled"/>
			<label class="rotuloHorizontal rotuloInformativo" for="potencia">kCal/h</label>
			 <br>  <br>
			<label class="rotulo rotulo2Linhas campoObrigatorio3" for="horasPorDia"><span class="campoObrigatorioSimbolo3">* </span>Horas de Uso por Dia:</label> 
			<input class="campoTexto campoDesabilitado" type="text" id="horasPorDia" name="horasPorDia" maxlength="2" size="2" value="${imovelPontoConsumoVO.horasPorDia}" onkeypress="return formatarCampoInteiro(event)" disabled="disabled"/>
			<label class="rotuloHorizontal rotuloInformativo" for="horasPorDia">horas</label>
			<br>  <br>
			<label class="rotulo rotulo2Linhas campoObrigatorio3" for="diasPorSemana"><span class="campoObrigatorioSimbolo3">* </span>Dias de Uso por Semana:</label> 
			<input class="campoTexto campoDesabilitado" type="text" id="diasPorSemana" name="diasPorSemana" maxlength="1" size="1" value="${imovelPontoConsumoVO.diasPorSemana}" onkeypress="return formatarCampoInteiro(event)" disabled="disabled"/>
			<label class="rotuloHorizontal rotuloInformativo" for="diasPorSemana">dias</label>
			 <br>  <br>
			<label class="rotulo rotulo2Linhas" for="qtdeEquipamentos">N�mero de Unidades:</label> 
			<input class="campoTexto campoDesabilitado" type="text" id="qtdeEquipamentos" name="qtdeEquipamentos" maxlength="5" size="5" value="${imovelPontoConsumoVO.qtdeEquipamentos}" onkeypress="return formatarCampoInteiro(event)" disabled="disabled"/> <br>  <br>
			
			<label class="rotulo rotulo2Linhas campoObrigatorio3" id="labelPotenciaFixa" for="idPotenciaFixa"><span class="campoObrigatorioSimbolo3">* </span>Pot�ncia Fixa:</label> 
			<select name="idPotenciaFixa" id="idPotenciaFixa" class="campoSelect campoDesabilitado" onchange="alterarPotenciaFixa();" disabled="disabled">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaPotenciasFixas}" var="potenciaFixa">
					<option value="<c:out value="${potenciaFixa.codPotenciaFixa}"/>" <c:if test="${imovelPontoConsumoVO.idPotenciaFixa == potenciaFixa.codPotenciaFixa}">selected="selected"</c:if>>
						<c:out value="${potenciaFixa.descricao}"/>
					</option>		
			    </c:forEach>		
		    </select>
		    <br>  <br>
		    
		    <label class="rotulo rotulo2Linhas campoObrigatorio3"><span class="campoObrigatorioSimbolo3">* </span>Valor Pot�ncia Fixa:</label>
		    <input class="campoTexto campoDesabilitado" type="text" id="valorPotenciaFixa" onchange="aplicarMascaraNumeroDecimal(this, 2);" name="valorPotenciaFixa" maxlength="5" size="5" disabled="disabled"/>
		    <label class="rotuloHorizontal rotuloInformativo" for="valorPotenciaFixa">kCal/h</label>
		      <br> 
			
			<p class="legenda"><span class="campoObrigatorioSimbolo3">* </span>campos obrigat�rios para adicionar equipamentos ao <br />ponto de consumo quando n�o est�o desabilitados.</p>
		</fieldset>
		
		<fieldset class="conteinerBotoesInserir1">
			<input class="bottonRightCol2" name="botaoLimparPontoConsumoEquipamento" id="botaoLimparPontoConsumoEquipamento" value="Limpar" type="button" onclick="limparPontoConsumoEquipamento();">
			<input class="bottonRightCol2" name="botaoIncluirPontoConsumoEquipamento" id="botaoIncluirPontoConsumoEquipamento" value="Adicionar Equipamento" type="button" onclick="adicionarPontoConsumoEquipamento(this.form,'<c:out value="${param['actionAdicionarPontoConsumoEquipamento']}"/>');">	
			<input class="bottonRightCol2" disabled="disabled"  name="botaoAlterarPontoConsumoEquipamento" id="botaoAlterarPontoConsumoEquipamento" value="Alterar Equipamento" type="button" onclick="alterarPontoConsumoEquipamento(this.form,'<c:out value="${param['actionAlterarPontoConsumoEquipamento']}"/>');">
		</fieldset>
		
		<c:set var="i" value="0" />
		<c:set var="actionExibirPaginaTabelaEquipamento"><c:out value="${param['actionExibirPaginaTabelaEquipamento']}"/></c:set>
		
		<display:table class="dataTableGGAS" name="listaPontoConsumoEquipamento" sort="list" id="pontoConsumoEquipamento"
						 pagesize="15" 
						excludedParams="org.apache.struts.taglib.html.TOKEN acao actionAdicionarPontoConsumo actionRemoverPontoConsumo actionExibirAlteracaoPontoConsumo actionLimparPontoConsumoTributoEquipamento actionAdicionarPontoConsumoTributoAliquota actionRemoverPontoConsumoTributoAliquota actionAdicionarPontoConsumoEquipamento actionAlterarPontoConsumoEquipamento actionRemoverPontoConsumoEquipamento actionCarregarCityGate actionAlterarCityGate actionExibirPaginaTabelaEquipamento actionAlterarPontoConsumoTributoAliquota"  
						requestURI="${actionExibirPaginaTabelaEquipamento}" >
			
			<display:column sortable="false" title="Equipamento" style="width: 65px">
				    <c:out value="${pontoConsumoEquipamento.equipamento.descricao}"/>
			</display:column>
			
			<display:column sortable="false" title="Segmento" style="width: 65px">
				    <c:out value="${pontoConsumoEquipamento.equipamento.segmento.descricao}"/>
			</display:column>
							
			<display:column sortable="false" title="Pot�ncia" style="width: 65px">
				<c:choose>
					<c:when test="${pontoConsumoEquipamento.equipamento.segmento.indicadorEquipamentoPotenciaFixa eq 'true'}">
						  <c:forEach items="${listaPotenciasFixas}" var="potenciaFixa">
							<c:if test="${pontoConsumoEquipamento.potenciaFixa == potenciaFixa.codPotenciaFixa}">
								<c:choose>
									<c:when test="${pontoConsumoEquipamento.potenciaFixa == POTENCIA_ALTA}">
										<c:out value="${potenciaFixa.descricao}"/> - <c:out value="${pontoConsumoEquipamento.equipamento.potenciaFixaAltaFormatada}"/> kCal/h
									</c:when>
									<c:when test="${pontoConsumoEquipamento.potenciaFixa == POTENCIA_MEDIA}">
										<c:out value="${potenciaFixa.descricao}"/> - <c:out value="${pontoConsumoEquipamento.equipamento.potenciaFixaMediaFormatada}"/> kCal/h
									</c:when>
									<c:when test="${pontoConsumoEquipamento.potenciaFixa == POTENCIA_BAIXA}">
										<c:out value="${potenciaFixa.descricao}"/> - <c:out value="${pontoConsumoEquipamento.equipamento.potenciaFixaBaixaFormatada}"/> kCal/h
									</c:when>
									<c:otherwise>
										<c:out value="${potenciaFixa.descricao}"/>
									</c:otherwise>
								</c:choose>
							</c:if>
		   				 </c:forEach>	
					</c:when>			
					<c:otherwise>
				   		 <c:out value="${pontoConsumoEquipamento.potenciaFormatada}" /> kCal/h
					</c:otherwise>	
				</c:choose>	

			</display:column>
			
			<display:column sortable="false" title="Horas/Dia" style="width: 65px">
				    <c:out value="${pontoConsumoEquipamento.horasPorDia}"/>			
			</display:column>
			
			<display:column sortable="false" title="Dias/Semana" style="width: 65px">
				    <c:out value="${pontoConsumoEquipamento.diasPorSemana}"/>			
			</display:column>
					
			<display:column style="text-align: center; width: 20px"> 
				<a href="javascript:exibirAlteracaoPontoConsumoEquipamento('${pontoConsumoEquipamento.chavePrimaria}','${i}','${pontoConsumoEquipamento.equipamento.chavePrimaria}', '${ pontoConsumoEquipamento.equipamento.descricao}',
				 '${pontoConsumoEquipamento.equipamento.segmento.chavePrimaria}', '${pontoConsumoEquipamento.equipamento.segmento.descricao}', '${pontoConsumoEquipamento.potenciaFormatada}', '${pontoConsumoEquipamento.horasPorDia}','${pontoConsumoEquipamento.diasPorSemana}',
				 '${pontoConsumoEquipamento.potenciaFixa}', '${pontoConsumoEquipamento.equipamento.potenciaFixaAltaFormatada}', '${pontoConsumoEquipamento.equipamento.potenciaFixaMediaFormatada}', '${pontoConsumoEquipamento.equipamento.potenciaFixaBaixaFormatada}',
				 '${pontoConsumoEquipamento.equipamento.segmento.indicadorEquipamentoPotenciaFixa}'
				 );">
				 <img title="Alterar equipamento do ponto de consumo" alt="Alterar equipamento do ponto de consumo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a>
			</display:column>			
		
			<display:column style="text-align: center; width: 20px"> 
				<a onclick="return confirm('Deseja excluir o equipamento do ponto de consumo?');" href="javascript:removerPontoConsumoEquipamento('<c:out value="${param['actionRemoverPontoConsumoEquipamento']}"/>',<c:out value="${i}"/>);"><img id="imgExcluirEquipamentoPontoConsumo" title="Excluir Equipamento do ponto de consumo" alt="Excluir equipamento do ponto de consumo"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
			</display:column>
			
			<c:set var="i" value="${i+1}" />
		</display:table>
		
	</fieldset>
	

	<fieldset class="colunaFinal">			
		<fieldset id="conteinerAssociarTributosPontoConsumo" class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Exce��es Tribut�rias no Ponto de Consumo</legend>		
			<label class="rotulo campoObrigatorio3" id="rotuloPontoConsumoTributo" for="idPontoConsumoTributo"><span class="campoObrigatorioSimbolo3">* </span>Tributo:</label>
			<select name="idPontoConsumoTributo" id="idPontoConsumoTributo" class="campoSelect campoHorizontal">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTributo}" var="tributo">
					<option value="<c:out value="${tributo.chavePrimaria}"/>" <c:if test="${imovelPontoConsumoVO.idPontoConsumoTributo == tributo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tributo.descricao}"/>
					</option>		
				</c:forEach>
			</select>
			
			<label class="rotulo rotuloHorizontal rotuloDesabilitado" for="indicadorIsento">� Isento?</label>
			<input class="campoRadio" id="radioIsentoSim" name="indicadorIsento" value="true" <c:if test="${imovelPontoConsumoVO.indicadorIsento eq 'true'}">checked="checked"</c:if> type="radio"><label class="rotuloRadio" for="radioIsentoSim">Sim</label>
			<input class="campoRadio" id="radioIsentoNao" name="indicadorIsento" value="false" <c:if test="${imovelPontoConsumoVO.indicadorIsento eq 'false'}">checked="checked"</c:if> type="radio"><label class="rotuloRadio" for="radioIsentoNao">N�o</label><br />
					
			<label class="rotulo" id="rotuloIndicadorDestaqueNota" for="indicadorDestaqueNota">Destaque<br/> na nota?</label>
			<input class="campoRadio campoHorizontal radioDestaqueNotaSimPosicionamento" id="radioDestaqueNotaSim" name="indicadorDestaqueNota" value="true" <c:if test="${imovelPontoConsumoVO.indicadorDestaqueNota eq 'true'}">checked="checked"</c:if> type="radio" disabled="disabled">
				<label class="rotuloRadio labelDestaqueNotaSimPosicionamento" for="radioDestaqueNotaSim">Sim</label>
			<input class="campoRadio campoHorizontal radioDestaqueNotaSimPosicionamento" id="radioDestaqueNotaNao" name="indicadorDestaqueNota" value="false" <c:if test="${imovelPontoConsumoVO.indicadorDestaqueNota eq 'false'}">checked="checked"</c:if> type="radio" disabled="disabled">
				<label class="rotuloRadio labelDestaqueNotaSimPosicionamento" for="radioDestaqueNotaNao">N�o</label><br />
				
			<label class="rotulo"  id="rotuloCreditoIcms" for="indicadorCreditoIcms">Cred.ICMS<br/>(PRODESIN)</label>
			<input type="checkbox" class="campoCheckbox" id="creditoIcms" name="creditoIcms"  value="true" <c:if test="${imovelPontoConsumoVO.creditoIcms eq 'true'}">checked="checked"</c:if> onclick="desabilitarIcmsSubstituto(this);">
			
			<fieldset id="conteinerICMSSubstituido" class="conteinerBloco">
			<label class="rotulo rotulo2Linhas"  id="rotuloUtilizarIcmsSubstituto" for="indicadorIcmsSubstituto">Utilizar valor ICMS Substituto da tarifa</label>
			<input type="checkbox" class="campoCheckbox" id="indicadorIcmsSubstitutoTarifa" name="indicadorIcmsSubstitutoTarifa"  value="true" <c:if test="${imovelPontoConsumoVO.indicadorIcmsSubstitutoTarifa eq 'true'}">checked="checked"</c:if> onclick="habilitarAliquotaIcmsSubstituto(this);" >
			</fieldset>
		
			<label class="rotulo" id="rotuloIndicadorDrawback" for="indicadorDrawback">Drawback?</label>
			<input class="campoRadio campoHorizontal radioDrawbackSimPosicionamento" id="radioDrawbackSim" name="indicadorDrawback" value="true" <c:if test="${imovelPontoConsumoVO.indicadorDrawback eq 'true'}">checked="checked"</c:if> type="radio" disabled="disabled" >
				<label class="rotuloRadio labelDrawbackSimPosicionamento" for="radioDrawbackSim">Sim</label>
			<input class="campoRadio campoHorizontal radioDrawbackSimPosicionamento" id="radioDrawbackNao" name="indicadorDrawback" value="false" <c:if test="${imovelPontoConsumoVO.indicadorDrawback eq 'false'}">checked="checked"</c:if> type="radio" disabled="disabled" >
				<label class="rotuloRadio labelDrawbackSimPosicionamento" for="radioDrawbackNao">N�o</label><br />
					
			<label class="rotulo2 rotuloDesabilitado" for="valorDrawback">Valor Drawback:</label>
		  	<input class="campoTexto campoDesabilitado" type="text" name="valorDrawback" id="valorDrawback" maxlength="20" size="17" onkeypress="return formatarCampoDecimalPositivo(event, this, 17, 2)" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${imovelPontoConsumoVO.valorDrawback}" disabled="disabled" />
			<label class="rotuloInformativo" for="valorDrawback">m3</label>	
		
			<label class="rotulo campoObrigatorio3 rotuloDesabilitado" for="percentualAliquota"><span class="campoObrigatorioSimbolo3 rotuloDesabilitado">* </span>Al�quota:</label>
		  	<input class="campoTexto campoHorizontal campoDesabilitado" type="text" name="percentualAliquota" id="percentualAliquota" maxlength="6" size="3" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2)" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${imovelPontoConsumoVO.percentualAliquota}" disabled="disabled" />
			<label class="rotuloInformativo" for="percentualAliquota">%</label>
			
			<label class="rotulo campoObrigatorio3 rotuloHorizontal rotuloDesabilitado" for="dataInicioVigencia"><span class="campoObrigatorioSimbolo3 rotuloDesabilitado">* </span>Vig�ncia:</label>
			<input class="campoData campoHorizontal campoDesabilitado" type="text" id="dataInicioVigencia" name="dataInicioVigencia" maxlength="10" value="${imovelPontoConsumoVO.dataInicioVigencia}" disabled="disabled">
			<label class="rotuloEntreCampos rotuloDesabilitado">at�</label>
			<input class="campoData campoHorizontal campoDesabilitado" type="text" id="dataFimVigencia" name="dataFimVigencia" maxlength="10" value="${imovelPontoConsumoVO.dataFimVigencia}" disabled="disabled"><br />
			<p class="legenda"><span class="campoObrigatorioSimbolo3">* </span>campos obrigat�rios apenas para adicionar exce��es tribut�rias ao ponto de Consumo.</p>
		</fieldset>
		
		<fieldset class="conteinerBotoesInserir1">
			<input class="bottonRightCol2" name="botaoLimparPontoConsumoTributoAliquota" id="botaoLimparPontoConsumoTributoAliquota" value="Limpar" type="button" onclick="limparPontoConsumoTributoAliquota();">
			<input class="bottonRightCol2" name="botaoIncluirPontoConsumoTributoAliquota" id="botaoIncluirPontoConsumoTributoAliquota" value="Adicionar Tributo" type="button" onclick="adicionarPontoConsumoTributoAliquota(this.form,'<c:out value="${param['actionAdicionarPontoConsumoTributoAliquota']}"/>');">	
			<input class="bottonRightCol2" disabled="disabled"  name="botaoAlterarPontoConsumoTributoAliquota" id="botaoAlterarPontoConsumoTributoAliquota" value="Alterar Tributo" type="button" onclick="alterarPontoConsumoTributoAliquota(this.form,'<c:out value="${param['actionAlterarPontoConsumoTributoAliquota']}"/>');">
		</fieldset>
			
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS" name="listaPontoConsumoTributoAliquota" sort="list" id="pontoConsumoTributoAliquota" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			<display:column property="tributo.descricao" sortable="false" title="Tributo" />
			<display:column sortable="false" title="Isen��o" style="width: 55px">
				<c:if test="${pontoConsumoTributoAliquota.indicadorIsencao eq 'true'}">Sim</c:if>
				<c:if test="${pontoConsumoTributoAliquota.indicadorIsencao eq 'false'}">N�o</c:if>
			</display:column>
			
			<display:column sortable="false" title="Al�quota" style="width: 65px">
		       	<c:if test="${pontoConsumoTributoAliquota.porcentagemAliquota ne null}">
				    <fmt:formatNumber value="${pontoConsumoTributoAliquota.porcentagemAliquota * 100}"/>%
				</c:if>					
			</display:column>
			
			<display:column sortable="false" title="Inicio Vig�ncia" style="width: 85px">
		       	<c:if test="${pontoConsumoTributoAliquota.dataInicioVigencia ne null}">
					<fmt:formatDate value="${pontoConsumoTributoAliquota.dataInicioVigencia}" pattern="dd/MM/yyyy" />
				</c:if>				
			</display:column>
		
			<display:column sortable="false" title="Fim Vig�ncia" style="width: 85px">
		       	<c:if test="${pontoConsumoTributoAliquota.dataFimVigencia ne null}">
					<fmt:formatDate value="${pontoConsumoTributoAliquota.dataFimVigencia}" pattern="dd/MM/yyyy" />		        	
				</c:if>				
			</display:column>		
			
			<display:column style="text-align: center; width: 20px"> 
				<a href="javascript:exibirAlteracaoPontoConsumoTributoAliquota('${pontoConsumoTributoAliquota.chavePrimaria}','${i}','${pontoConsumoTributoAliquota.tributo.chavePrimaria}', '${pontoConsumoTributoAliquota.indicadorIsencao}','<fmt:formatNumber value="${pontoConsumoTributoAliquota.porcentagemAliquota}"/>','<fmt:formatDate value="${pontoConsumoTributoAliquota.dataInicioVigencia}" pattern="dd/MM/yyyy" />','<fmt:formatDate value="${pontoConsumoTributoAliquota.dataFimVigencia}" pattern="dd/MM/yyyy" />','${pontoConsumoTributoAliquota.destaqueIndicadorNota}','${pontoConsumoTributoAliquota.indicadorCreditoIcms}','${pontoConsumoTributoAliquota.indicadorIcmsSubstitutoTarifa}','${pontoConsumoTributoAliquota.indicadorDrawback}','<fmt:formatNumber value="${pontoConsumoTributoAliquota.valorDrawback}"/>');"><img title="Alterar tributo do ponto de consumo" alt="Alterar tributo do ponto de consumo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a>
			</display:column>			
		
			<display:column style="text-align: center; width: 20px"> 
				<a onclick="return confirm('Deseja excluir o tributo do ponto de consumo tributo aliquota ?');" href="javascript:removerPontoConsumoTributoAliquota('<c:out value="${param['actionRemoverPontoConsumoTributoAliquota']}"/>',<c:out value="${i}"/>);"><img title="Excluir tributo do ponto de consumo" alt="Excluir tributo do ponto de consumo"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
			</display:column>
			<c:set var="i" value="${i+1}" />
		</display:table>

	</fieldset>
	

	<hr class="linhaSeparadora" />
	<fieldset id="conteinerMensagemAdicional" class="conteinerBloco">
	<legend class="conteinerBlocoTitulo">Mensagem Adicional</legend>

	<label class="rotulo" for="indicadorMensagemAdicional">Possui Mensagem Adicional?</label>
	<input class="campoRadio" id="indicadorMensagemAdicional1" name="indicadorMensagemAdicional" onclick="selecionaMensagemAdicional(this)" value="true" type="radio" <c:if test="${imovelPontoConsumoVO.indicadorMensagemAdicional eq true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorMensagemAdicional1">Sim</label>
	<input class="campoRadio" id="indicadorMensagemAdicional2" name="indicadorMensagemAdicional" onclick="selecionaMensagemAdicional(this)" value="false" type="radio" <c:if test="${imovelPontoConsumoVO.indicadorMensagemAdicional eq false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorMensagemAdicional2">N�o</label>
	
	<div id="mensagemAdicional">
		<label class="rotulo" for="descricaoMensagemAdicional">Mensagem Adicional:</label>
		<textarea id="descricaoMensagemAdicional" name="descricaoMensagemAdicional" rows="5" cols="40" maxlength="1000">${imovelPontoConsumoVO.descricaoMensagemAdicional}</textarea><br/>
	</div>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar Pontos de Consumo.</p>
</fieldset>	

<fieldset class="conteinerBotoesAba">
	<input class="bottonRightCol2" name="botaoLimparPontoConsumo" id="botaoLimparPontoConsumo" value="Limpar" type="button" onclick="limparPontoConsumo(true);">
	<input class="bottonRightCol2" name="botaoIncluirPontoConsumo" id="botaoIncluirPontoConsumo" value="Adicionar Ponto de Consumo" type="button" onclick="incluirPontoConsumo(this.form,'<c:out value="${param['actionAdicionarPontoConsumo']}"/>');">
   	<input disabled class="bottonRightCol2" name="botaoAlterarPontoConsumo" id="botaoAlterarPontoConsumo" value="Alterar Ponto de Consumo" type="button" onclick="alterarPontoConsumo(this.form,'<c:out value="${param['actionAdicionarPontoConsumo']}"/>');">
</fieldset>

<input class="campoTexto" type="hidden" id="idSituacaoPontoConsumo" name="idSituacaoPontoConsumo" value="${imovelPontoConsumoVO.idSituacaoPontoConsumo}">
<c:set var="i" value="0" />
<!-- Display tag para inclus�o, n�o possui pagina��o -->
<c:if test="${imovelForm.map.isIncluir == true}">
<display:table class="dataTableGGAS dataTableAba" name="listaPontoConsumo" sort="list" id="pontoConsumo" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="ordenarPontoConsumo" >
	<display:column property="descricao" sortable="false" title="Descri��o" />
	<display:column property="segmento.descricao" sortable="false" title="Segmento" />
	<display:column property="ramoAtividade.descricao" sortable="false" title="Ramo de Atividade" />
	<display:column property="rota.numeroRota" sortable="false" title="Rota" />
	<display:column property="situacaoConsumo.descricao" sortable="false" title="Situa��o Consumo" />
	<display:column sortable="false" title="Indicador de uso">
				<c:if test="${pontoConsumo.habilitado eq 'true'}">ATIVO</c:if>
				<c:if test="${pontoConsumo.habilitado eq 'false'}">INATIVO</c:if>
	</display:column>>
	<display:column style="text-align: center;"> 
		<a href="javascript:exibirAlteracaoPontoConsumo('<c:out value="${param['actionExibirAlteracaoPontoConsumo']}"/>','${pontoConsumo.chavePrimaria}','${i}');"><img title="Alterar ponto de consumo" alt="Alterar ponto de consumo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a>
	</display:column>
	<display:column style="text-align: center;"> 
		<a onclick="return confirm('Deseja excluir o ponto de consumo ?');" href="javascript:removerPontoConsumo('<c:out value="${param['actionRemoverPontoConsumo']}"/>',<c:out value="${i}"/>);"><img title="Excluir ponto de consumo" alt="Excluir ponto de consumo"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
	</display:column>
	<c:set var="i" value="${i+1}" />
</display:table>
</c:if>
<!-- Display tag para altera��o, possui pagina��o -->
<c:if test="${imovelForm.map.isIncluir != true}">
<display:table class="dataTableGGAS dataTableAba" defaultsort="1" name="listaPontoConsumo" sort="list" id="pontoConsumo" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="ordenarPontoConsumo" >
	<display:column property="descricao" sortable="true" title="Descri��o" />
	<display:column property="segmento.descricao" sortable="true" title="Segmento" />
	<display:column property="ramoAtividade.descricao" sortable="true" title="Ramo de Atividade" />
	<display:column property="rota.numeroRota" sortable="true" title="Rota" />
	<display:column property="situacaoConsumo.descricao" sortable="false" title="Situa��o Consumo" />
	<display:column sortable="false" title="Indicador de uso">
				<c:if test="${pontoConsumo.habilitado eq 'true'}">ATIVO</c:if>
				<c:if test="${pontoConsumo.habilitado eq 'false'}">INATIVO</c:if>
	</display:column>
	
	<display:column style="text-align: center;"> 
		<a href="javascript:exibirAlteracaoPontoConsumo('<c:out value="${param['actionExibirAlteracaoPontoConsumo']}"/>','${pontoConsumo.chavePrimaria}','${i}');"><img id="imgAlterarPontoConsumo" title="Alterar ponto de consumo" alt="Alterar ponto de consumo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a>
	</display:column>
	<display:column style="text-align: center;"> 
		<a onclick="return confirm('Deseja excluir o ponto de consumo ?');" href="javascript:removerPontoConsumo('<c:out value="${param['actionRemoverPontoConsumo']}"/>',<c:out value="${i}"/>);"><img title="Excluir ponto de consumo" alt="Excluir ponto de consumo"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
	</display:column>
	<c:set var="i" value="${i+1}" />	 
</display:table>
</c:if>
