<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>

	$(document).ready(function(){
		addLoadEvent(init);

		// Datepicker
		$("#relacaoInicio").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#relacaoFim,#dateRelacaoFim,#dataEntrega,#dataPrevisaoEncerramentoObra").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
		<c:if test="${empty imovelVO.idQuadraImovel}">
			$("#idQuadraImovel").addClass("campoDesabilitado");
		</c:if>

		<c:if test="${empty imovelVO.idQuadraFace}">
			$("#idQuadraFace").addClass("campoDesabilitado");
		</c:if>

		<c:if test="${empty imovelPontoConsumoVO.cepPontosConsumo}">
			$("#idQuadraPontoConsumo").addClass("campoDesabilitado");
		</c:if>

		<c:if test="${empty imovelVO.idQuadraFacePontoConsumo}">
			$("#idQuadraFacePontoConsumo").addClass("campoDesabilitado");
		</c:if>

		<c:if test="${empty imovelPontoConsumoVO.idRamoAtividadePontoConsumo}">
			$("#idRamoAtividadePontoConsumo").addClass("campoDesabilitado");
		</c:if>

		<c:if test="${empty imovelUnidadeConsumidoraVO.idRamoAtividade}">
			$("#idRamoAtividade").addClass("campoDesabilitado");
		</c:if>		
		$('#cepImovel').on('blur', carregarQuadras);
		$('#cepPontosConsumo').on('blur', carregarQuadras2);
		$("#idQuadraPontoConsumo").removeClass("campoDesabilitado");
		$("#idQuadraPontoConsumo").removeAttr("readonly");
		$("#idQuadraFacePontoConsumo").removeClass("campoDesabilitado");
		$("#idQuadraFacePontoConsumo").removeAttr("readonly");
	});	
	
	function carregarQuadras(valorCep) {
		var selectQuadras = document.getElementById("idQuadraImovel");
		var load = false;
		var cep = this.value;
	  	if(cep == undefined){
			cep = valorCep.value;
			if(cep == undefined){
				cep = valorCep;
			}
		}
	  	//$("#idPontoConsumo").val('')
	  	$("#idQuadraImovel").val('')
		selectQuadras.disabled = false;
		$("#idQuadraImovel").removeClass("campoDesabilitado");

		selectQuadras.length=0;
		var novaOpcao = new Option("Selecione","-1");
		selectQuadras.options[selectQuadras.length] = novaOpcao;
		if((cep != undefined) && (trim(cep) != '') && (cep[8] != '_')){

			console.log(document.getElementById("idCepRetornoBusca").value)
			
			AjaxService.obterTamanhoListaQuadraPorCep(cep, {
			        callback:function(quadras) { 
			        	if(quadras == 1){
			        		selectQuadras.options[0].remove();
			        		load = true;
			        	}
			 	}, async:false}
			 ); 
			
			if (document.getElementById("idCepRetornoBusca").value && document.getElementById("idCepRetornoBusca").value != ''){
				AjaxService.consultarQuadrasPorIdCep( document.getElementById("idCepRetornoBusca").value, {
					callback:function(quadras) {
						for (key in quadras){
							var novaOpcao = new Option(quadras[key], key);
							selectQuadras.options[selectQuadras.length] = novaOpcao;
						}

					}, async:false}
				);
			}else{
				AjaxService.consultarQuadrasPorCep( cep, {
					callback:function(quadras) {
						for (key in quadras){
							var novaOpcao = new Option(quadras[key], key);
							selectQuadras.options[selectQuadras.length] = novaOpcao;
						}

					}, async:false}
				);
			}


		} else {
			$("#idQuadraImovel").addClass("campoDesabilitado");
			selectQuadras.disabled = true;
		}
		
		var selectQuadraFace = document.getElementById("idQuadraFace");
	  	selectQuadraFace.length=0;
	  	var novaOpcao = new Option("Selecione","-1");
	    selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;
	    selectQuadraFace.disabled = true;
		$("#idQuadraFace").addClass("campoDesabilitado");
		
		//Fun��o quando h� um carregamento �nico de quadra
		if(load){
			document.forms[0].idQuadraImovel.onchange();
		}
	}

	function carregarQuadraFace(elem) {
	    var codQuadra = elem.value;
     	var selectQuadraFace = document.getElementById("idQuadraFace");
     	var selectCEP = document.imovelForm.cepImovel.value;
     	var load = false;
     	selectQuadraFace.length=0;
     	var novaOpcao = new Option("Selecione","-1");
        selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;

     	if (codQuadra != "-1") {
     		selectQuadraFace.disabled = false;
   			$("#idQuadraFace").removeClass("campoDesabilitado");
   			
   			AjaxService.obterTamanhoListaFacesQuadraPorQuadra(codQuadra, selectCEP,
		            {callback: function(listaQuadraFace) {                                      
		                  if(listaQuadraFace == 1){
		                      selectQuadraFace.options[0].remove();
		                      load = true;
		                    }
		                }, async:false}
		            );
   			
	   		if ($("#idCepImovel").val() != ''){
	   			 AjaxService.consultarFacesQuadraPorQuadraIdCep(codQuadra, $("#idCepImovel").val(),
	   			            {callback: function(listaQuadraFace) {                                      
	   			                  for (key in listaQuadraFace){
	   			                      var novaOpcao = new Option(listaQuadraFace[key], key);
	   			                        selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;
	   			                    }
	   			                }, async:false}
	   			            );
	   		}else{
	   			 AjaxService.consultarFacesQuadraPorQuadra(codQuadra, selectCEP,
	   			            {callback: function(listaQuadraFace) {                                      
	   			                  for (key in listaQuadraFace){
	   			                      var novaOpcao = new Option(listaQuadraFace[key], key);
	   			                        selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;
	   			                    }
	   			                }, async:false}
	   			            );
	   		}
	   		document.forms[0].cepPontosConsumo.value = document.imovelForm.cepImovel.value;
	   		// Fun��es chamadas quando h� um carregamento �nico de face de quadra
	   		if(load){
		   		carregarQuadras2(selectCEP);
				carregarQuadraFace2(elem);
				document.forms[0].idQuadraFace.onchange();
	   		}
     	} else {
			selectQuadraFace.disabled = true;
			document.getElementById("idQuadraFacePontoConsumo").disabled = true;
			$("#idQuadraFace").addClass("campoDesabilitado");	
			$("#idQuadraFacePontoConsumo").addClass("campoDesabilitado");
     	} 	
	}		
	
	function carregarSituacaoImovel() {	
		var codQuadraFace = document.getElementById("idQuadraFace").value;
      	var selectSituacaoImovel = document.getElementById("idSituacao");
      	var selectChavePrimaria = document.imovelForm.chavePrimaria.value;

      	selectSituacaoImovel.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectSituacaoImovel.options[selectSituacaoImovel.length] = novaOpcao;

      	if (codQuadraFace != "-1") {
      		selectSituacaoImovel.disabled = false;
    		$("#idSituacao").removeClass("campoDesabilitado");
        	AjaxService.consultarSitucaoImovelPorFacesQuadra(codQuadraFace, selectChavePrimaria,
        		{callback: function(listaSitucaoImovel) {
                	for (key in listaSitucaoImovel){
                    	var novaOpcao = new Option(listaSitucaoImovel[key], key);
                    	selectSituacaoImovel.options[selectSituacaoImovel.length] = novaOpcao;
                    }
                }, async:false}
            );
      	} else {
      		selectSituacaoImovel.disabled = true;
    		$("#idSituacao").addClass("campoDesabilitado");	
      	}

	}
				
	function carregarQuadras2(valorCep) {	
		var selectQuadras = document.getElementById("idQuadraPontoConsumo");
		var cep = this.value;
		if(cep == undefined){
			cep = valorCep.value;
			if(cep == undefined){
				cep = valorCep;
			}
		}

      	selectQuadras.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectQuadras.options[selectQuadras.length] = novaOpcao;
           
		if((cep != undefined) && (trim(cep) != '') && (cep[8] != '_')){
			selectQuadras.disabled = false;
	      	$("#idQuadraPontoConsumo").removeClass("campoDesabilitado");    
	      	
			AjaxService.consultarQuadrasPorCep( cep, {
	           	callback:function(quadras) {	        		
		        	for (key in quadras){
	               		var novaOpcao = new Option(quadras[key], key);
	                   	selectQuadras.options[selectQuadras.length] = novaOpcao;
	               	}
	           	}, async:false}
	       	);
       	} else {
       		$("#idQuadraPontoConsumo").addClass("campoDesabilitado");
       		document.getElementById("idQuadraPontoConsumo").disabled = true;
       	}

      	var selectQuadraFacePontoConsumo = document.getElementById("idQuadraFacePontoConsumo");
      	selectQuadraFacePontoConsumo.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectQuadraFacePontoConsumo.options[selectQuadraFacePontoConsumo.length] = novaOpcao;
        selectQuadraFacePontoConsumo.disabled = true;
        $("#idQuadraFacePontoConsumo").addClass("campoDesabilitado");

        var cityGatePontoConsumo = document.getElementById("cityGatePontoConsumo");
        if (cityGatePontoConsumo != undefined) {
        	cityGatePontoConsumo.style.display = "none"; 
        } 
	}	

	function carregarQuadraFace2(elem) {
		var codQuadra = elem.value;
      	var selectQuadraFacePontoConsumo = document.getElementById("idQuadraFacePontoConsumo");
      	var selectCEP = document.forms[0].cepPontosConsumo.value;
      	
      	selectQuadraFacePontoConsumo.length=0;    	
      	var novaOpcao = new Option("Selecione","-1");
        selectQuadraFacePontoConsumo.options[selectQuadraFacePontoConsumo.length] = novaOpcao;

        selectQuadraFacePontoConsumo.disabled = false;
  		$("#idQuadraFacePontoConsumo").removeClass("campoDesabilitado");
  		
      	if (codQuadra != "-1") {
        	AjaxService.consultarFacesQuadraPorQuadra( codQuadra, selectCEP,
      			{callback: function(listaQuadraFace) {            		      		         		
                	for (key in listaQuadraFace){
                    	var novaOpcao = new Option(listaQuadraFace[key], key);
                        selectQuadraFacePontoConsumo.options[selectQuadraFacePontoConsumo.length] = novaOpcao;
                    }
                }, async: false}
            );
       	
      	} else {
      		$("#idQuadraFacePontoConsumo").addClass("campoDesabilitado"); 	
      		document.getElementById("idQuadraFacePontoConsumo").disabled = true;
      	}

        var cityGatePontoConsumo = document.getElementById("cityGatePontoConsumo");
        if (cityGatePontoConsumo != undefined) {
        	cityGatePontoConsumo.style.display = "none";  
        }
	}	
					
	function limparFormulario(){
	
		// Identifica��o e Localiza��o
		limparIdentificacaoLocalizacao();

		// Caracter�sticas	
		limparCaracteristicas();
		
		// Relacionamentos
		limparClienteImovel();		
		
		// Contatos
		limparContato();
		
		// Unidades Consumidoras
		limparUnidadeConsumidora();
		
		// Pontos de Consumo
		limparPontoConsumo(true);
	}
	
	function cancelar(){	
		location.href = '<c:url value="/exibirPesquisaImovel"/>';
	}
	
	function init(){

		document.getElementById('cepPontosConsumo').onchange = carregarQuadras2;
		document.getElementById('numeroImovel').onchange = carregarNumeroImovelPontoConsumo;
		document.getElementById('complementoImovel').onchange = carregarComplementoImovelPontoConsumo;
		document.getElementById('enderecoReferencia').onchange = carregarEnderecoReferenciaPontoConsumo;

		if(${imovelPontoConsumoVO.indicadorIsento eq 'true'}) {
			document.forms[0].percentualAliquota.disabled = true;
			document.forms[0].percentualAliquota.value = '';
		}
		else {
			document.forms[0].percentualAliquota.disabled = false;
		}
		
		// Replicando os valores de cep e quadra da aba Identifica��o e Localiza��o para a aba Ponto de Consumo
		if (document.imovelForm.cepImovel.value != "" && document.forms[0].cepPontosConsumo.value == "") {
			document.forms[0].cepPontosConsumo.value = document.imovelForm.cepImovel.value;
			document.forms[0].cepPontosConsumo.onchange();
		}		
		
		
		if (document.imovelForm.idQuadraImovel.value != "" && (document.forms[0].idQuadraPontoConsumo.value == ""  || document.forms[0].idQuadraPontoConsumo.value == "-1")) {
			document.forms[0].idQuadraPontoConsumo.value = document.imovelForm.idQuadraImovel.value;
			
			if(document.forms[0].idQuadraPontoConsumo.value != ""  ){
				document.forms[0].idQuadraPontoConsumo.onchange();
			}
		}else{
			//Mantendo o valor de face de quadra e rota ao recarregar a p�gina
			var valorQuadraFaceSessao = document.forms[0].idQuadraFacePontoConsumo.value;
			var valorRotaSessao = document.forms[0].idRota.value;
			document.forms[0].idQuadraPontoConsumo.onchange();
			document.forms[0].idQuadraFacePontoConsumo.value = valorQuadraFaceSessao;
			document.forms[0].idRota.value = valorRotaSessao;

		}
		
		// Replicando os valores de face de quadra da aba Identifica��o e Localiza��o para a aba Ponto de Consumo
		if (document.imovelForm.idQuadraFace.value != "" && (document.forms[0].idQuadraFacePontoConsumo.value == "" || document.forms[0].idQuadraFacePontoConsumo.value == "-1")) {
				document.getElementById("idQuadraFacePontoConsumo").value = document.imovelForm.idQuadraFace.value;
		}

		desabilitarSituacaoImovel();
	}

	function desabilitarSituacaoImovel(){
		var codQuadraFace = document.getElementById("idQuadraFace").value;
		
      	if (codQuadraFace == "-1") {
				$("#idSituacao").addClass("campoDesabilitado");
				document.getElementById("idSituacao").disabled = true;
			}
	}
	
	// Replicar dados da aba Identifica��o e Localiza��o para a aba Ponto de Consumo
	function carregarNumeroImovelPontoConsumo(){	
		if (document.getElementById('numeroImovel') != null) {
			document.getElementById('numeroImovelPontoConsumo').value = document.getElementById('numeroImovel').value; 
		} 
	}

	function carregarComplementoImovelPontoConsumo(){	
		if (document.getElementById('complementoImovel') != null) {
			document.getElementById('descricaoComplementoPontoConsumo').value = document.getElementById('complementoImovel').value; 
		}	
	}

	function carregarEnderecoReferenciaPontoConsumo(){	
		if (document.getElementById('enderecoReferencia') != null) {
			document.getElementById('enderecoReferenciaPontoConsumo').value = document.getElementById('enderecoReferencia').value; 
		}	
	}			
	
	function incluir(){
		selecionarRestricoes();
        $('#condominioSim').removeAttr('disabled')
        $('#condominioNao').removeAttr('disabled')
              $('#numeroImovel').removeAttr('disabled')
              $('#cepImovel').removeAttr('disabled')
		$("#formIncluirImovel").submit();
	}
	
</script>

<h1 class="tituloInterno">Incluir Im�vel<a href="<help:help>/imvelinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form method="post" action="incluirImovel" id="formIncluirImovel" name="imovelForm">
<token:token></token:token>
 <input name="idCepImovel" type="hidden" id="idCepImovel" value="${imovelVO.idCepImovel}"/>
<input name="acao" type="hidden" id="acao" value="incluirImovel"/>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${imovelVO.chavePrimaria}"/>
<input name="habilitado" type="hidden" id="habilitado" value="${true}"/>
<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="indexLista" type="hidden" id="indexLista" value="${imovelVO.indexLista}">
<input name="indexListaPontoConsumoTributo" type="hidden" id="indexListaPontoConsumoTributo" value="${imovelVO.indexListaPontoConsumoTributo}">
<input name="veioDoChamado" type="hidden" id="veioDoChamado" value="${imovelVO.veioDoChamado}" />

<fieldset id="tabs" style="display: none">
	<ul>
		<li><a href="#identificacaoLocalizacao"><span class="campoObrigatorioSimboloTabs">* </span>Identifica��o e Localiza��o</a></li>
		<li><a href="#caracteristicas"><span class="campoObrigatorioSimboloTabs">* </span>Caracter�sticas</a></li>
		<li><a href="#relacionamento">Relacionamentos</a></li>
		<li><a href="#contatos">Contatos</a></li>
		<li><a href="#unidadesConsumidoras">Unidades Consumidoras</a></li>
		<li><a href="#pontosConsumo">Pontos de Consumo</a></li>
	</ul>
	<fieldset class="conteinerAba" id="identificacaoLocalizacao">
		<jsp:include page="/jsp/cadastro/imovel/abaIdentificacaoLocalizacao.jsp">
			<jsp:param name="alteracao" value="false"/>
		</jsp:include>
	</fieldset>
	
	<fieldset class="conteinerAba" id="caracteristicas">
		<jsp:include page="/jsp/cadastro/imovel/abaCaracteristica.jsp">
			<jsp:param name="alteracao" value="false"/>
			<jsp:param name="qtdAndar" value="quantidadeAndar"/>
			<jsp:param name="qtdApartamentoAndar" value="quantidadeApartamentoAndar"/>
		</jsp:include>
	</fieldset>
	
	<fieldset class="conteinerAba" id="relacionamento">
		<jsp:include page="/jsp/cadastro/imovel/abaRelacionamento.jsp">
			<jsp:param name="actionAdicionarClienteImovel" value="adicionarClienteImovel" />			
			<jsp:param name="actionRemoverClienteImovel" value="removerClienteImovelFluxoInclusao" />			
		</jsp:include>			
	</fieldset>
	
	<fieldset class="conteinerAba" id="contatos">
		<jsp:include page="/jsp/cadastro/imovel/abaContatoImovel.jsp">
			<jsp:param name="actionAdicionarContato" value="adicionarContatoDoImovelFluxoInclusao" />			
			<jsp:param name="actionRemoverContato" value="removerContatoDoImovelFluxoInclusao" />
			<jsp:param name="actionAtualizarContatoPrincipal" value="atualizarContatoPrincipalDoImovelFluxoInclusao" />
		</jsp:include>
	</fieldset>
	
	<fieldset class="conteinerAba" id="unidadesConsumidoras">	
		<jsp:include page="/jsp/cadastro/imovel/abaUnidadesConsumidoras.jsp">
			<jsp:param name="actionAdicionarUnidadeConsumidora" value="adicionarUnidadeConsumidora" />			
			<jsp:param name="actionRemoverUnidadeConsumidora" value="removerUnidadeConsumidoraFluxoInclusao" />			
		</jsp:include>			
	</fieldset>
		
	<fieldset class="conteinerAba" id="pontosConsumo">	
		<jsp:include page="/jsp/cadastro/imovel/abaPontoConsumo.jsp">
			<jsp:param name="actionAdicionarPontoConsumo" value="adicionarPontoConsumo" />
			<jsp:param name="actionRemoverPontoConsumo" value="removerPontoConsumo" />
			<jsp:param name="actionExibirAlteracaoPontoConsumo" value="exibirAlteracaoPontoConsumo" />			

			<jsp:param name="actionLimparPontoConsumoTributoEquipamento" value="limparPontoConsumoTributoEquipamento" />
			
			<jsp:param name="actionAdicionarPontoConsumoTributoAliquota" value="adicionarPontoConsumoTributoAliquota" />
			<jsp:param name="actionAlterarPontoConsumoTributoAliquota" value="alterarPontoConsumoTributoAliquota" />			
			<jsp:param name="actionRemoverPontoConsumoTributoAliquota" value="removerPontoConsumoTributoAliquota" />
			
			<jsp:param name="actionAdicionarPontoConsumoEquipamento" value="adicionarPontoConsumoEquipamento" />
			<jsp:param name="actionAlterarPontoConsumoEquipamento" value="alterarPontoConsumoEquipamento" />			
			<jsp:param name="actionRemoverPontoConsumoEquipamento" value="removerPontoConsumoEquipamento" />
			
			<jsp:param name="actionCarregarCityGate" value="carregarCityGateFluxoInclusao" />
			<jsp:param name="actionAlterarCityGate" value="alterarCityGateFluxoInclusao" />	
						
			<jsp:param name="actionExibirPaginaTabelaEquipamento" value="exibirPaginaTabelaEquipamento" />	
					
		</jsp:include>			
	</fieldset>		
</fieldset>

<fieldset class="conteinerBotoes"> 
	<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="incluirImovel">
    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" value="Incluir" type="button" onclick="incluir()">
    </vacess:vacess>
 </fieldset>
</form>
