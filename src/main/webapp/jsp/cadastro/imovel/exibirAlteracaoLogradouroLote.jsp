<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Alterar Im�vel / Ponto Consumo em Lote<a href="<help:help>/consultadosimveis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar os Im�veis, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>


<script language="javascript">

$(document).ready(function(){
	$('#cepImovel').on('blur', carregarQuadras);
	
	$('#idQuadraFace').on('change', carregarImoveisFilhosPorSituacaoImovel);
	$('#idQuadraImovel,#numeroImovel,#nome,#nomeAntigoBloco,#nomeNovoBloco').on('change', desabilitarBotaoAlterar);
	
	desabilitarBotaoAlterar();
	
	$( "#spanCep" ).html('**');
});

function carregarImoveisFilhosPorSituacaoImovel(){
	var idQuadraFace = $("#idQuadraFace").val();
	if(idQuadraFace != "-1"){
		submeter('imovelForm', 'carregarImoveisFilhosPorSituacaoImovel');
		desabilitarBotaoAlterar();
	}
}

function desabilitarBotaoAlterar(){
	var cepImovel = $("#cepImovel").val();
	var idQuadraFace = $("#idQuadraFace").val();
	var idQuadraImovel = $("#idQuadraImovel").val();
	var numeroImovel = $("#numeroImovel").val();
	var nome = $("#nome").val();
	var nomeAntigoBloco = $("#nomeAntigoBloco").val();
	var nomeNovoBloco = $("#nomeNovoBloco").val();

	if((idQuadraFace != "-1" && idQuadraFace != "" && idQuadraImovel != "-1" && idQuadraImovel != "") || numeroImovel != "" || nome != "" || (nomeAntigoBloco != "" && nomeNovoBloco != "")){
		$("#buttonAlterar").prop("disabled", false);
		$("#existeImoveisFilhos").val(false);
	}else{
		$("#buttonAlterar").prop("disabled", true);
		$("#existeImoveisFilhos").val(null);
	}
	
	if(nomeAntigoBloco != ""){
		$("#nomeNovoBloco").prop("disabled", false);
	}else{
		$("#nomeNovoBloco").prop("disabled", true);
		$("#nomeNovoBloco").val(null);
	}
}

function cancelar(){	
	submeter("imovelForm", "pesquisarImovel");
}

function carregarQuadraFace(elem) {
	var codQuadra = elem.value;
  	var selectQuadraFace = document.getElementById("idQuadraFace");
  	var selectCEP = document.imovelForm.cepImovel.value;
  	
  	selectQuadraFace.length=0;
  	var novaOpcao = new Option("Selecione","-1");
    selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;

  	if (codQuadra != "-1") {
  		selectQuadraFace.disabled = false;
		$("#idQuadraFace").removeClass("campoDesabilitado");
		
		AjaxService.obterTamanhoListaFacesQuadraPorQuadra(codQuadra, selectCEP,
	            {callback: function(listaQuadraFace) {                                      
	                  if(listaQuadraFace == 1){
	                      selectQuadraFace.options[0].remove();
	                    }
	                }, async:false}
	            );
		
    	AjaxService.consultarFacesQuadraPorQuadra(codQuadra, selectCEP,
    		{callback: function(listaQuadraFace) {            		      		         		
            	for (key in listaQuadraFace){
                	var novaOpcao = new Option(listaQuadraFace[key], key);
                    selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;
                }
            }, async:false}
        );
  	} else {
		selectQuadraFace.disabled = true;
		
		$("#idQuadraFace").addClass("campoDesabilitado");	
  	}
  	
}	

function carregarQuadras(valorCep) {  			
	var cep = valorCep.value;
  	var selectQuadras = document.getElementById("idQuadraImovel");

  	if(cep == undefined){
  		cep = valorCep;
  	}	
  		
  	selectQuadras.disabled = false;
  	$("#idQuadraImovel").removeClass("campoDesabilitado");
  	
  	selectQuadras.length=0;
  	var novaOpcao = new Option("Selecione","-1");
    selectQuadras.options[selectQuadras.length] = novaOpcao;              		
	if((cep != undefined) && (trim(cep) != '') && (cep[8] != '_')){	
		
		AjaxService.obterTamanhoListaQuadraPorCep(cep, {
	        callback:function(quadras) { 
	        	if(quadras == 1){
	        		selectQuadras.options[0].remove();
	        	}
	 		}, async:false}
		 ); 
		
		AjaxService.consultarQuadrasPorCep( cep, 
			{callback: function(quadras) {	        		
	        	for (key in quadras){
               		var novaOpcao = new Option(quadras[key], key);
                   	selectQuadras.options[selectQuadras.length] = novaOpcao;
               	}

           	}, async:false}
       	);   
   	} else {
   		$("#idQuadraImovel").addClass("campoDesabilitado");
   		selectQuadras.disabled = true;   
   		$("#buttonAlterar").prop("disabled", true);
   	}

	var selectQuadraFace = document.getElementById("idQuadraFace");
  	selectQuadraFace.length=0;
  	var novaOpcao = new Option("Selecione","-1");
    selectQuadraFace.options[selectQuadraFace.length] = novaOpcao;
    selectQuadraFace.disabled = true;
	$("#idQuadraFace").addClass("campoDesabilitado");
	
	carregarQuadraFace(selectQuadras);
	
}

function alterarLogradouroLote(){
	var existeImoveisFilhos = $("#existeImoveisFilhos").val();
	
	if(existeImoveisFilhos == ""){
		var idQuadraFace = $("#idQuadraFace").val();
		popup = window.open('exibirPopupAlteracaoLogradouro?idQuadraFace=' + idQuadraFace +'','popup','height=650,width=1000,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes').focus();
	}else{
		submeter('imovelForm', 'alterarLogradouroLote');
	}
	
}

function setarCampoHidden(){
	$("#existeImoveisFilhos").val(true);	
}

function limparFormulario(){
	
	 var selectQuadras = document.getElementById("idQuadraImovel");
	 var novaOpcaoQuadra = new Option("Selecione","-1");
	 selectQuadras.options[selectQuadras.length] = novaOpcaoQuadra;
	 
	 var selectQuadraFace = document.getElementById("idQuadraFace");
	 var novaOpcaoQuadraFace = new Option("Selecione","-1");
	 selectQuadraFace.options[selectQuadraFace.length] = novaOpcaoQuadraFace;
	 
	 document.imovelForm.cepImovel.value = "";
	 document.imovelForm.cepImovel.value = "";
	 document.imovelForm.idQuadraImovel.value = "-1";
	 document.imovelForm.idQuadraImovel.disabled = true;
	 document.imovelForm.idQuadraFace.value = "-1";
	 document.imovelForm.idQuadraFace.disabled = true;
	 document.imovelForm.numeroImovel.value = "";
	 document.imovelForm.nome.value = "";
	 document.imovelForm.nomeAntigoBloco.value = "-1";
	 document.imovelForm.nomeNovoBloco.value = "";
	
	desabilitarBotaoAlterar();
}
				
</script>

<form method="post" action="alterarLogradouroLote" id="imovelForm" name="imovelForm" >
	<token:token></token:token>
	<input name="acao" type="hidden" id="acao" value="alterarLogradouroLote">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="">
	<input name="listaIndicadorRede" type="hidden" id="listaIndicadorRede">
	<input name="existeImoveisFilhos" type="hidden" id="existeImoveisFilhos" value="${imovelLoteVO.existeImoveisFilhos}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">		
		<fieldset class="colunaEsq" id="ImovelAbaIdLoc">
		
		<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
			<jsp:param name="cepObrigatorio" value="true"/>
			<jsp:param name="numeroCep" value="${imovelVO.cepImovel}"/>
			<jsp:param name="idCampoCep" value="cepImovel"/>
			<jsp:param name="idCampoCidade" value="cidadeImovel"/>
			<jsp:param name="idCampoUf" value="ufImovel"/>
			<jsp:param name="idCampoLogradouro" value="logradouroImovel"/>
			<jsp:param name="chaveCep" value="${imovelVO.chaveCep}"/>
		</jsp:include>
		
		<label class="rotulo campoObrigatorio" for="idQuadra"><span class="campoObrigatorioSimbolo2">** </span>Quadra: </label>
			<select name="idQuadraImovel" id="idQuadraImovel" class="campoSelect" onchange="carregarQuadraFace(this);" <c:if test="${empty imovelVO.cepImovel}">disabled="disabled"</c:if>>
				<option value="-1">Selecione</option>
				<c:forEach items="${listaQuadra}" var="quadra">
					<option value="<c:out value="${quadra.chavePrimaria}"/>" <c:if test="${imovelVO.idQuadraImovel == quadra.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${quadra.numeroQuadra}"/>
					</option>		
				</c:forEach>
			</select><br />
	
		<label class="rotulo rotulo2Linhas campoObrigatorio" for="idQuadraFace"><span class="campoObrigatorioSimbolo2">** </span>Face da Quadra: </label>
		<select name="idQuadraFace" id="idQuadraFace" class="campoSelect" onchange="desabilitarBotaoAlterar();" <c:if test="${empty imovelVO.idQuadraImovel}">disabled="disabled"</c:if>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaFacesQuadra}" var="faceQuadra">
				<option value="<c:out value="${faceQuadra.chavePrimaria}"/>" <c:if test="${imovelVO.idQuadraFace == faceQuadra.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${faceQuadra.quadraFaceFormatada}"/>
				</option>		
			</c:forEach>
		</select><br />
		
		</fieldset>	
		
		<fieldset id="funcionarioCol2" class="colunaFinal">				
			
			<label class="rotulo" id="rotuloMatricula"><span class="campoObrigatorioSimbolo">** </span>N�mero do im�vel:</label>
			<input class="campoTexto" type="text" id="numeroImovel" name="numeroImovel" maxlength="5" size="5" value="${imovelVO.numeroImovel}" onkeyup="this.value = this.value.toUpperCase();" onkeyup="this.value = this.value.toUpperCase();"><br />
			<label class="rotulo"><span class="campoObrigatorioSimbolo">** </span>Descri��o:</label>
			<input class="campoTexto" type="text" id="nome" name="nome" maxlength="50" size="40" value="${imovelVO.nome}" onchange="letraMaiuscula(this);">
			<label class="rotulo campoObrigatorio" for="idQuadra"><span class="campoObrigatorioSimbolo">** </span>Antigo Bloco:</label>
			<select name="nomeAntigoBloco" id="nomeAntigoBloco" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaNomeImoveis}" var="bloco">
					<option value="<c:out value="${bloco}"/>" <c:if test="${imovelLoteVO.nomeAntigoBloco == bloco}">selected="selected"</c:if>>
						<c:out value="${bloco}"/>
					</option>		
				</c:forEach>
			</select><br />
			<label class="rotulo"><span class="campoObrigatorioSimbolo">** </span>Novo Bloco:</label>
			<input class="campoTexto" type="text" id="nomeNovoBloco" disabled="disabled" name="nomeNovoBloco" maxlength="10" size="14" value="${imovelLoteVO.nomeNovoBloco}" onchange="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
		
		</fieldset>
		
	</fieldset>
	
	<c:if test="${sessionScope.listaImovel ne null}">
		<hr class="linhaSeparadoraPesquisa" />
	<legend class="conteinerBlocoTitulo">Im�veis Selecionados:</legend>
		<display:table class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="sessionScope.listaImovel" id="imovel" pagesize="25" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirAlteracaoLogradouroLote">
	     		     	
	     	<display:column style="width: 70px" title="Matr�cula" sortable="false" sortProperty="chavePrimaria">
					<c:out value='${imovel.chavePrimaria}'/>
		    </display:column>
		    <display:column sortable="false" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq">
					<c:out value='${imovel.nome}'/>
		    </display:column>		     
		    <display:column style="width: 80px" title="Situa��o" sortable="false" sortProperty="situacaoImovel">
		    		<c:out value='${imovel.situacaoImovel.descricao}'/>
		    </display:column>
		    <display:column sortable="false" title="Endere�o" sortProperty="enderecoFormatado" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq">
		    		<c:out value='${imovel.enderecoFormatado}'/>
		    </display:column>
		</display:table>
	</c:if>

	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		<vacess:vacess param="alterarImovel">
			<input id="buttonAlterar" name="buttonAlterar" value="Salvar" disabled="disabled" class="bottonRightCol2 botaoGrande1 botaoIncluir" type="button" onClick="alterarLogradouroLote();">
		</vacess:vacess>
	</fieldset>

</form>
