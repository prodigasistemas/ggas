<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Contas Cont�bil<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<p class="orientacaoInicial">
    Para pesquisar um registro espec�fico, informe o(s) dado(s) no(s) campo(s) abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>
    ou clique apenas em  <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. Para incluir um novo registro clique em
    <span class="destaqueOrientacaoInicial">Incluir</span>.<br />
</p>

<form:form method="post" action="pesquisaContaContabilSegmento" id="segmentoForm" name="segmentoForm">

<script type="text/javascript">

$().ready(function(){
    $("input#nomeConta").keyup(removeExtraManterEspaco).blur(removeExtraManterEspaco);
    var mascara = formatarMascarasJSP("${mascaraNumeroConta}");
	$("input#numeroConta").inputmask(mascara,{placeholder:"_"});
});

function init() {
	limparFormulario();
}
addLoadEvent(init);

function limparFormulario(){
	
	document.segmentoForm.nomeConta.value = "";	
	document.segmentoForm.numeroConta.value = "";	
	
}

function formatarMascarasJSP(mascara){
	
	var mask = mascara;
	var mascaraFormatada = '';
	for (i = 0; i < mask.length; i++) {
		if(mask.charAt(i) == '.') {
			mascaraFormatada += ".";
		} else if(mask.charAt(i) == '-') {
			mascaraFormatada += "-";
		} else if(mask.charAt(i) == '*') {
			mascaraFormatada += "*";
		} else {
			mascaraFormatada += "9";
		}
	}
	return mascaraFormatada;
}

function selecionarContaContabil(idContaContabil, numeroConta) {	
	
	window.opener.selecionarContaContabil(idContaContabil, numeroConta);

	window.close();
}

	

</script>

	<input type="hidden" name="acao" id="acao" value="pesquisaContaContabilSegmento"/>
	<input type="hidden" name="fluxoPesquisa" id="fluxoPesquisa" value="true"/>	
	<input type="hidden" name="chavePrimaria" id="chavePrimaria"/>
	<input type="hidden" name="chavePrimarias" id="chavePrimarias"/>
	<input type="hidden" name="idContaContabil" id="idContaContabil"/>

		<fieldset id="conteinerPesquisarIncluirAlterar">		
			<fieldset id="pesquisaMedicaoCol1">
			
			<label class="rotulo" id="rotuloNumero" for="numeroConta">Numero:</label>
			<input class="campoTexto" type="text" id="numeroConta" name="numeroConta" maxlength="30" size="30" 
				value="${contaContabil.numeroConta}"/>
			<br />

			<label class="rotulo" id="rotuloNome" for="nomeConta">Descri��o:</label>
			<input class="campoTexto" type="text" id="nomeConta" name="nomeConta" maxlength="30" size="30" onkeyup="toUpperCase(this);"
				value="${contaContabil.nomeConta}"/>
			<br/>									
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparFormulario();">
	    <input name="button" class="bottonRightCol2 botaoGrande1" id="botaoPesquisar" value="Pesquisar" type="submit">
	 </fieldset>
	<br/>	
	
		<c:if test="${listaContaContabil ne null}">
			<display:table class="dataTableGGAS dataTablePopup" name="listaContaContabil"
				sort="list" id="contaContabil" 
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="pesquisaContaContabilSegmento">
				<display:column title="Ativo" style="width: 30px">
						<c:choose>
							<c:when test="${contaContabil.habilitado == true}">
								<img alt="Ativo" title="Ativo"
									src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img alt="Inativo" title="Inativo"
									src="<c:url value="/imagens/cancel16.png"/>" border="0">
							</c:otherwise>
						</c:choose>
				</display:column>				
				<display:column style="text-align: center, width: 130px" title="N�mero Conta"
							sortable="true" sortProperty="numeroConta" >
					<a href='javascript:selecionarContaContabil(<c:out value='${contaContabil.chavePrimaria}'/>, "<c:out value='${contaContabil.numeroConta}'/>");'><span class="linkInvisivel"></span>
					<c:out value='${contaContabil.numeroConta}'/></a>
				</display:column>
				<display:column style="text-align: center, width: 130px" title="Descri��o Conta" 
						sortable="true" sortProperty="nomeConta" >
					<a href='javascript:selecionarContaContabil(<c:out value='${contaContabil.chavePrimaria}'/>, "<c:out value='${contaContabil.numeroConta}'/>");'><span class="linkInvisivel"></span>
					<c:out value='${contaContabil.nomeConta}'/></a>
				</display:column>
			</display:table>
		</c:if>	
	
</form:form> 