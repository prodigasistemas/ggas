<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">


function limparFormulario(){
	document.forms[0].tipoDocumento.value = -1;
	document.forms[0].descricao.value = "";
}
function detalharPrevisaoCaptacao(chave){
	document.forms["previsaoCaptacaoForm"].chavesPrimarias.value = chave;
	submeter('previsaoCaptacaoForm','exibirDetalhamentoSegmentoPrevisaoCaptacao?chavesPrimarias='+chave);
}

function pesquisar() {
	submeter('previsaoCaptacaoForm','pesquisarSegmentoPrevisaoCaptacao');
}

function incluir() {
	submeter('previsaoCaptacaoForm','exibirIncluirSegmentoPrevisaoCaptacao');
}

function removerAcao(){
	
	var selecao = verificarSelecao();	
	
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('acaoForm', 'removerAcao');
		}
    }
}

function alterarPrevisaoCaptacao() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms[0].chavesPrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('previsaoCaptacaoForm','exibirAlterarSegmentoPrevisaoCaptacao');
    }
	
}

</script>

<h1 class="tituloInterno">Pesquisar Previs�o de Capta��o</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>


<form:form method="post" modelAttribute="SegmentoPrevisaoCaptacao" action="incluirAcao" id="previsaoCaptacaoForm" name="previsaoCaptacaoForm">
	<fieldset id="pesquisaServicoTipoCol1" class="conteinerPesquisarIncluirAlterar">
	<input name="chavesPrimaria" type="hidden" id="chavesPrimaria" > 	
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" > 	
		<fieldset id="servicoTipoCol1" class="coluna">
			<label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Ano Refer�ncia:</label>
					<select id="anoReferencia" class="campoSelect" name="anoReferencia">
						<c:forEach items="${listaAnoReferencia}" var="anoReferencia">
						<option value="<c:out value="${anoReferencia}"/>"  <c:if test="${segmentoPrevisaoCaptacao.anoReferencia == anoReferencia}">selected="selected"</c:if> >
							<c:out value="${anoReferencia}"/>
						</option>
						</c:forEach>
					</select><br/>	
			<label class="rotulo" id="rotuloCanalAtendimento" for="canalAtendimento">Segmento:</label>
					<select id="segmentoPrevisao" class="campoSelect" name="segmento">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaSegmento}" var="segmento">
						<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${segmentoPrevisaoCaptacao.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${segmento.descricao}"/>
						</option>
						</c:forEach>
					</select><br/>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Pesquisar"  type="submit" onclick="pesquisar();">
	    	<input name="Button" class="bottonRightCol2 botaoGrande1" value="Limpar" type="button" onclick="limparFormulario();">
 	</fieldset>
	<c:if test="${listaPrevisaoCaptacao ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaPrevisaoCaptacao" sort="list" id="previsaoCaptacao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input id="checkboxChavesPrimarias" name="chavesPrimarias" type="checkbox" value="${previsaoCaptacao.chavePrimaria}">
	        </display:column>
			<display:column sortable="true" sortProperty="segmento.descricao" title="Segmento" style="text-align: center;" maxLength="50">
				<a href="javascript:detalharPrevisaoCaptacao(<c:out value='${previsaoCaptacao.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${previsaoCaptacao.segmento.descricao}"/>
	            </a>
			</display:column>				
			<display:column sortable="true" sortProperty="anoReferencia" title="Ano" style="text-align: center;" maxLength="50">
				<a href="javascript:detalharPrevisaoCaptacao(<c:out value='${previsaoCaptacao.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${previsaoCaptacao.anoReferencia}"/>
	            </a>
			</display:column>				
	    </display:table>	
	</c:if>
	
</form:form>
	
	
 	<fieldset class="conteinerBotoes"> 
		<input id="botaoAlterar" name="Button" class="bottonRightCol2" value="Alterar" type="button" onClick="alterarPrevisaoCaptacao();">
<!-- 	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Remover" type="button" onclick="removerAcao();"> -->
<%-- 	    <vacess:vacess param="incluirServicoTipo"> --%>
	    	<input id="botaoIncluir" name="button" class="bottonRightCol2 botaoGrande1" value="Incluir"  type="submit" onclick="incluir();">
<%-- 	    </vacess:vacess>	 --%>
 	</fieldset>
