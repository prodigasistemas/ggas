<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>

<style>
	#relacaoFim + .ui-datepicker-trigger {
	    position: absolute;
	    top:143px;
	    left:695px;
	}
</style>

<script>

$(document).ready(function(){

	// Datepicker
	$("#relacaoFim").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>',  minDate: '<c:out value="${dataMinima}"/>', maxDate: '<c:out value="${dataMaxima}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$("#dateRelacaoFim").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});


	// Dialog			
	$("#fimRelacionamentoPopup").dialog({
		autoOpen: false,
		width: 360,
		modal: true,
		minHeight: 90,
		resizable: false
	});

	$("#idMotivoFimRelacionamentoClienteImovel").change(function(){
		habilitaDataFimRelacionamento();
	});

	/*Permite a sele��o do CityGate a partir do teclado, 
	disparando o carregamento dos anos dispon�veis*/
	$("#idMotivoFimRelacionamentoClienteImovel").keyup(function(event) {
		if (event.keyCode == '38' || event.keyCode == '40'){
			habilitaDataFimRelacionamento();
		}
	});
	habilitaDataFimRelacionamento();
});

	function habilitaDataFimRelacionamentoDialog(){
		if($("#motivoFimRelacionamentoClienteImovel").val() != "-1"){
			$("label.rotulo[for=dateRelacaoFim], label.rotulo[for=dateRelacaoFim] .campoObrigatorioSimbolo2").removeClass("rotuloDesabilitado");
			$("#dateRelacaoFim").removeAttr("disabled").next().show();
		} else {
			$("label.rotulo[for=dateRelacaoFim], label.rotulo[for=dateRelacaoFim] .campoObrigatorioSimbolo2").addClass("rotuloDesabilitado");
			$("#dateRelacaoFim").attr("disabled","disabled").next().hide();
			$("#dateRelacaoFim").val("");
		}
	}

	function habilitaDataFimRelacionamento(actionAdicionarClienteImovel){

		if($("#idMotivoFimRelacionamentoClienteImovel").val() != "-1"){
		   $("label.rotulo[for=relacaoFim], label.rotulo[for=relacaoFim] .campoObrigatorioSimbolo2").removeClass("rotuloDesabilitado");
		   $("#relacaoFim").removeAttr("disabled").next().show();
	    }else {
		   $("label.rotulo[for=relacaoFim], label.rotulo[for=relacaoFim] .campoObrigatorioSimbolo2").addClass("rotuloDesabilitado");
		   $("#relacaoFim").attr("disabled","disabled").next().hide();
		}

		if (actionAdicionarClienteImovel == "adicionarClienteImovel"){
			var idMotivoFimRelacionamentoClienteImovel = document.getElementById("idMotivoFimRelacionamentoClienteImovel");	
			idMotivoFimRelacionamentoClienteImovel.style.visibility="hidden"

			var relacaoFim = document.getElementById("relacaoFim");	
			relacaoFim.style.visibility="hidden"

			var txtIdMotivoFimRelacionamentoClienteImovel = document.getElementById("txtIdMotivoFimRelacionamentoClienteImovel");	
			txtIdMotivoFimRelacionamentoClienteImovel.style.visibility="hidden"		
					
			var txtRelacaoFim = document.getElementById("txtRelacaoFim");	
			txtRelacaoFim.style.visibility="hidden"		
		}
	}

	
		
		
	function alterarClienteImovel(form, actionAdicionarClienteImovel) {
		submeter('imovelForm', actionAdicionarClienteImovel + '#relacionamento');
	}
	
	function incluirClienteImovel(form, actionAdicionarClienteImovel) {
		document.forms[0].indexLista.value = -1;
		submeter('imovelForm', actionAdicionarClienteImovel + '#relacionamento');
	}

	function validarRemoverClienteImovelAlteracao(actionRemoverClienteImovel,indice) {
		var dateRelacaoFim = $("#dateRelacaoFim").val();
		var motivoFimRelacionamentoClienteImovel = $("#motivoFimRelacionamentoClienteImovel").val();
				
		if (motivoFimRelacionamentoClienteImovel != null && 
				motivoFimRelacionamentoClienteImovel != "-1" &&
				dateRelacaoFim != null && dateRelacaoFim != "") {
			removerClienteImovel(actionRemoverClienteImovel,indice);
		}else{
			alert("Campos obrigat�rios n�o preenchidos!");
		}
	}
	   
	function removerClienteImovel(actionRemoverClienteImovel,indice) {
		var fluxoAlteracao = "${param.fluxoAlteracao}";
		
		if (fluxoAlteracao != "true") {
			document.forms[0].indexLista.value = indice;
		}
		
		var motivoFimRelacionamentoClienteImovel = document.getElementById('motivoFimRelacionamentoClienteImovel').value;
		if (motivoFimRelacionamentoClienteImovel != null && motivoFimRelacionamentoClienteImovel != "-1") {
			document.getElementById('idMotivoFimRelacionamentoClienteImovel').value = motivoFimRelacionamentoClienteImovel;
		}
		
		var dateRelacaoFim = document.getElementById('dateRelacaoFim').value;
		if (dateRelacaoFim != null){ 
			document.getElementById('relacaoFim').value = document.getElementById('dateRelacaoFim').value;
		} 
		
		submeter('imovelForm', actionRemoverClienteImovel + '?operacaoLista=true#relacionamento');
	}
	
	function exibirRemocaoClienteImovel(actionRemoverClienteImovel,indice,idCliente,nome,documentoFormatado,enderecoFormatado,email,idTipoRelacionamento,inicioRelacionamento, idMotivoRelacionamento, fimRelacionamento) {
		var fluxoAlteracao = "${param.fluxoAlteracao}";
		
		if (fluxoAlteracao == "true") {
			if (indice != "") {	
				$("#dateRelacaoFim").val("");
				$("#motivoFimRelacionamentoClienteImovel").val("-1");
							
				exibirJDialog("#fimRelacionamentoPopup");
				habilitaDataFimRelacionamentoDialog();
				
				exibirAlteracaoClienteImovel(indice,idCliente,nome,documentoFormatado,enderecoFormatado,email,idTipoRelacionamento,inicioRelacionamento, idMotivoRelacionamento, fimRelacionamento);							
				var botaoAlterarClienteImovel = document.getElementById("botaoAlterarClienteImovel");
				var botaoLimparClienteImovel = document.getElementById("botaoLimparClienteImovel");	
				var botaoIncluirClienteImovel = document.getElementById("botaoIncluirClienteImovel");
				botaoAlterarClienteImovel.disabled = true;
				botaoLimparClienteImovel.disabled = false;
				botaoIncluirClienteImovel.disabled = true;
			}
		} else {
			var retorno = confirm('Deseja excluir o relacionamento?');
			if (retorno == true) {
				removerClienteImovel(actionRemoverClienteImovel,indice);
			}
		}
	}
		
	function limparClienteImovel() {		
		document.forms[0].indexLista.value = -1;	
		limparFormularioDadosCliente();
		document.forms[0].idTipoRelacionamento.value = "-1";
		document.forms[0].idMotivoFimRelacionamentoClienteImovel.value = "-1";
        document.forms[0].relacaoInicio.value = "";
        document.forms[0].relacaoFim.value = "";
        
		var botaoAlterarClienteImovel = document.getElementById("botaoAlterarClienteImovel");
		var botaoLimparClienteImovel = document.getElementById("botaoLimparClienteImovel");	
		var botaoIncluirClienteImovel = document.getElementById("botaoIncluirClienteImovel");	
		
		botaoAlterarClienteImovel.disabled = true;
		botaoLimparClienteImovel.disabled = false;
		botaoIncluirClienteImovel.disabled = false;

		habilitaDataFimRelacionamento();
	}
	
	function exibirAlteracaoClienteImovel(indice,idCliente,nome,documentoFormatado,enderecoFormatado,email,idTipoRelacionamento,inicioRelacionamento, idMotivoRelacionamento, fimRelacionamento) {
		if (indice != "") {
			document.forms[0].indexLista.value = indice;

			if (idTipoRelacionamento != "") {
				var tamanho = document.forms[0].idTipoRelacionamento.length;
				var opcao = undefined;
				for(var i = 0; i < tamanho; i++) {
					opcao = document.forms[0].idTipoRelacionamento.options[i];
					if (idTipoRelacionamento == opcao.value) {
						opcao.selected = true;
					}
				}
			}

			selecionarCliente(idCliente);

			if (idMotivoRelacionamento != "") {
				var tamanho = document.forms[0].idMotivoFimRelacionamentoClienteImovel.length;
				var opcao = undefined;
				for(var i = 0; i < tamanho; i++) {
					opcao = document.forms[0].idMotivoFimRelacionamentoClienteImovel.options[i];
					if (idMotivoRelacionamento == opcao.value) {
						opcao.selected = true;
					}
				}
			}
		
			if (fimRelacionamento != "") {
				document.getElementById("relacaoFim").value = fimRelacionamento;
			} else {
				document.forms[0].relacaoFim.value = "";
			}	
			
			if (inicioRelacionamento != "") {
				document.forms[0].relacaoInicio.value = inicioRelacionamento;
			} else {
				document.forms[0].relacaoInicio.value = "";
			}	
			
			var botaoAlterarClienteImovel = document.getElementById("botaoAlterarClienteImovel");
			var botaoLimparClienteImovel = document.getElementById("botaoLimparClienteImovel");	
			var botaoIncluirClienteImovel = document.getElementById("botaoIncluirClienteImovel");	
			botaoAlterarClienteImovel.disabled = false;			
			botaoLimparClienteImovel.disabled = false;
			botaoIncluirClienteImovel.disabled = true;
			
			habilitaDataFimRelacionamento();
		}
		
	}
	
	function onloadClienteImovel() {
		<c:choose>
			<c:when test="${imovelVO.indexLista > -1 && empty param['operacaoLista']}">
				var botaoAlterarClienteImovel = document.getElementById("botaoAlterarClienteImovel");
				var botaoLimparClienteImovel = document.getElementById("botaoLimparClienteImovel");	
				var botaoIncluirClienteImovel = document.getElementById("botaoIncluirClienteImovel");
				botaoIncluirClienteImovel.disabled = true;
				botaoAlterarClienteImovel.disabled = false;	
				botaoLimparClienteImovel.disabled = false;
				
			</c:when>
			<c:when test="${sucessoManutencaoLista}">			
				limparClienteImovel();
			</c:when>					
		</c:choose>

		<c:if test="${imovelVO.indexLista > -1 && requestScope.paramAlteracao}">
			limparClienteImovel();
		</c:if>		
	}

<c:if test="${abaId == '2'}">
	addLoadEvent(onloadClienteImovel);
</c:if>

function init(){	
	
	habilitaDataFimRelacionamento('<c:out value="${param['actionAdicionarClienteImovel']}"/>');		
}	
addLoadEvent(init);

</script>
<a class="linkHelp" href="<help:help>/abarelacionamentoscadastrodoimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
<div id="fimRelacionamentoPopup" title="Excluir Relacionamento">
	<a class="linkHelp" href="<help:help>/abarelacionamentosexcluindoumrelacionamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	<label class="rotulo rotulo2Linhas" id="rotuloMotivoFimRelacionamento" for="MotivoFimRelacionamentoClienteImovel">Motivo do t�rmino<br />da rela��o:</label>
	<select name="motivoFimRelacionamentoClienteImovel" id="motivoFimRelacionamentoClienteImovel" class="campoSelect campo2Linhas" onchange="habilitaDataFimRelacionamentoDialog();">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMotivoFimRelacionamentoClienteImovel}" var="motivoFimRelacionamento">
			<option value="<c:out value="${motivoFimRelacionamento.chavePrimaria}"/>" <c:if test="${imovelRelacionamentoVO.idMotivoFimRelacionamentoClienteImovel == motivoFimRelacionamento.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${motivoFimRelacionamento.descricao}"/>
			</option>		
		</c:forEach>
	</select><br />
	<label class="rotulo rotulo2Linhas" id="rotuloDateRelacaoFim" for="dateRelacaoFim">Fim do<br />Relacionamento:</label>
	<input type="text" id="dateRelacaoFim" name="dateRelacaoFim" class="campoData campo2Linhas" value="${imovelRelacionamentoVO.relacaoFim}" maxlength="10" />
	<hr class="linhaSeparadora" />
	<input type="button" class="bottonRightCol2" style="float: right" value="Ok" onclick="validarRemoverClienteImovelAlteracao('<c:out value="${param['actionRemoverClienteImovel']}"/>', '');" />
</div>

<fieldset>	
	<fieldset id="pesquisarCliente" class="colunaEsq">
		<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
			<jsp:param name="idCampoIdCliente" value="idCliente"/>
			<jsp:param name="idCliente" value="${imovelRelacionamentoVO.idCliente}"/>
			<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
			<jsp:param name="nomeCliente" value="${imovelRelacionamentoVO.nomeCompletoCliente}"/>
			<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
			<jsp:param name="documentoFormatadoCliente" value="${imovelRelacionamentoVO.documentoFormatado}"/>
			<jsp:param name="idCampoEmail" value="emailCliente"/>
			<jsp:param name="emailCliente" value="${imovelRelacionamentoVO.emailCliente}"/>
			<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
			<jsp:param name="enderecoFormatadoCliente" value="${imovelRelacionamentoVO.enderecoFormatadoCliente}"/>		
			<jsp:param name="dadosClienteObrigatorios" value="true"/>									
		</jsp:include>		
	</fieldset>
	
	<fieldset id="ImovelAbaRelacionamento2">
		<label class="rotulo campoObrigatorio" for="idTipoRelacionamento"><span class="campoObrigatorioSimbolo2">* </span>Relacionamento:</label>
		<select name="idTipoRelacionamento" id="idTipoRelacionamento" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTiposRelacionamento}" var="tipoRelacionamento">
				<option value="<c:out value="${tipoRelacionamento.chavePrimaria}"/>" <c:if test="${imovelRelacionamentoVO.idTipoRelacionamento == tipoRelacionamento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoRelacionamento.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<br/><br/>
		
		<label class="rotulo campoObrigatorio"  for="relacaoInicio"><span class="campoObrigatorioSimbolo2">* </span>In�cio do Relacionamento:</label>
		<input type="text" id="relacaoInicio" name="relacaoInicio" class="campoData campo2Linhas relacaoInicio" value="${imovelRelacionamentoVO.relacaoInicio}" maxlength="10"  />
		
    	<label class="rotulo rotulo2Linhas" id="txtIdMotivoFimRelacionamentoClienteImovel" for="idMotivoFimRelacionamentoClienteImovel">Motivo do t�rmino<br />da rela��o:</label>
	    <select name="idMotivoFimRelacionamentoClienteImovel" id="idMotivoFimRelacionamentoClienteImovel" class="campoSelect campo2Linhas">
			<option value="-1">Selecione</option>
		    <c:forEach items="${listaMotivoFimRelacionamentoClienteImovel}" var="motivoFimRelacionamento">
			    <option value="<c:out value="${motivoFimRelacionamento.chavePrimaria}"/>" <c:if test="${imovelRelacionamentoVO.idMotivoFimRelacionamentoClienteImovel == motivoFimRelacionamento.chavePrimaria}">selected="selected"</c:if>>
				    <c:out value="${motivoFimRelacionamento.descricao}"/>
			    </option>		
		    </c:forEach>
	    </select>
	    <br />
	    
	    <label class="rotulo campoObrigatorio" id="txtRelacaoFim" for="relacaoFim"><span class="campoObrigatorioSimbolo2">* </span>Fim do Relacionamento:</label>
		<input type="text" id="relacaoFim" name="relacaoFim" class="campoData" value="${imovelRelacionamentoVO.relacaoFim}" maxlength="10"/>
		
	</fieldset>	
	<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar Relacionamentos.</p>
</fieldset>

<fieldset class="conteinerBotoesAba">
	<input class="bottonRightCol2" name="botaoLimparClienteImovel" id="botaoLimparClienteImovel" value="Limpar" type="button" onclick="limparClienteImovel();">
	<input class="bottonRightCol2" name="botaoIncluirClienteImovel" id="botaoIncluirClienteImovel" value="Adicionar Relacionamento" type="button" onclick="incluirClienteImovel(this.form,'<c:out value="${param['actionAdicionarClienteImovel']}"/>');">
   	<input class="bottonRightCol2" disabled="disabled" name="botaoAlterarClienteImovel" id="botaoAlterarClienteImovel" value="Alterar Relacionamento" type="button" onclick="alterarClienteImovel(this.form,'<c:out value="${param['actionAdicionarClienteImovel']}"/>');">
</fieldset>


<c:set var="i" value="0" />
<display:table class="dataTableGGAS dataTableAba" name="listaClienteImovel" sort="list" id="clienteImovel" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#"  >
	<display:column property="nome" sortable="false"  title="Nome" style="width: 150px" />
	<display:column property="documentoFormatado" sortable="false"  title="CPF/CNPJ" style="width: 100px" />
	<display:column sortable="false" title="Endere�o" style="width: 125px">
				<c:choose>
			      		<c:when test="${clienteImovel.enderecoFormatado ne null && clienteImovel.enderecoFormatado ne ''}">
			      			${clienteImovel.enderecoFormatado}
					</c:when>
					<c:otherwise>
			      			
					</c:otherwise>
				</c:choose>
		   		</display:column>
	<display:column property="descricaoTipoRelacionamento" sortable="false"  title="Relacionamento" style="width: 105px" />
	<display:column property="inicioRelacionamento" sortable="false"  title="Data Inicial" style="width: 75px" />
	<display:column property="fimRelacionamento" sortable="false"  title="Data Final" style="width: 75px" />
	<display:column property="descricaoMotivoFimRelacionamento" sortable="false"  title="Motivo do T�rmino da Rela��o" style="width: 40px" />
              
	<display:column style="width: 25px"> 
		<a href="javascript:exibirAlteracaoClienteImovel('${i}','${clienteImovel.idCliente}','${clienteImovel.nome}','${Cliente.documentoFormatado}','${clienteImovel.enderecoFormatado}','${clienteImovel.email}','${clienteImovel.idTipoRelacionamento}','${clienteImovel.inicioRelacionamento}', '${clienteImovel.idMotivoRelacionamento}','${clienteImovel.fimRelacionamento}');"><img title="Alterar Relacionamento" alt="Alterar Relacionamento"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a> 
	</display:column>     
	<c:set var="i" value="${i+1}" />
</display:table>