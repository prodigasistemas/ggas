<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna colunaMedia">
		<label class="rotulo rotulo2Linhas" for="numeroCiclos">N� de per�odos para c�lculos da m�dia de consumo:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${segmentoForm.numeroCiclos}"/></span><br />
		<label class="rotulo rotulo2Linhas" for="numeroDiasFaturamento">N� de dias para faturamento da liga��o nova:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${segmentoForm.numeroDiasFaturamento}"/></span><br />			
	</fieldset>
	<fieldset class="colunaFinal">
		<label class="rotulo" for="periodicidade">Periodicidade:</label>
		<span class="itemDetalhamento"><c:out value="${segmentoForm.periodicidade.descricao}"/></span><br />
		<label class="rotulo" for="tipoConsumoFaturamento">Tipo Consumo:</label>
		<span class="itemDetalhamento"><c:out value="${segmentoForm.tipoConsumoFaturamento.descricao}"/></span><br />
	    <label class="rotulo" for="contaContabil">Conta Cont�bil:</label>
	    <span class="itemDetalhamento"><c:out value="${segmentoForm.contaContabil.numeroConta}"/></span><br />					
		
	</fieldset>
</fieldset>