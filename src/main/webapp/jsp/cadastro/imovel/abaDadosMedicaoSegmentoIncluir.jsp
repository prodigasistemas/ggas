<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<label class="rotulo" for="estouroConsumo">Volume m�dio para estouro de consumo:</label>
<input class="campoTexto campoHorizontal" id="volumeMedioEstouro" name="volumeMedioEstouroRamo" type="text" size="8" maxlength="8" 
	value="${segmentoForm.volumeMedioEstouro}" onkeypress="return formatarCampoInteiro(event);">
<label class="rotuloInformativo" for="consumoAlto">m<span class="expoente">3</span></label><br class="quebraLinha2" />
	
<script type="text/javascript">

$(document).ready(function(){	
	habilitarDesabilitarTamanhoReducao();
});

function habilitarDesabilitarTamanhoReducao(){
	var lista = document.getElementById("listaSegmentoIntervaloPCSDisponiveis");
	var i;
	var valor;
	
	if(lista.length == 0){
		document.getElementById("tamanhoReducao").style.backgroundColor = "white";
		document.getElementById("tamanhoReducao").disabled = false;
	}
	for (i=0;i<lista.length;i++){
		valor = lista.options[i].text;
		if(valor == "Reduzido"){			
			document.getElementById("tamanhoReducao").style.backgroundColor = "#EBEBE4";
			document.getElementById("tamanhoReducao").disabled = true;
			document.getElementById("tamanhoReducao").value = "";
			break;
		}else{			
			document.getElementById("tamanhoReducao").style.backgroundColor = "white";
			document.getElementById("tamanhoReducao").disabled = false;
		}
	}
}

</script>	
		
<fieldset class="conteinerBloco">

   	<fieldset class="conteinerLista1">
	    <legend class="conteinerBlocoTitulo">Local de Amostragem do PCS</legend>
		<a name="listaSegmentoAmostragemPCSDisponiveis"></a>
		<label class="rotulo rotuloVertical" id="rotuloLocalAmostragemPCS" for="listaSegmentoAmostragemPCSDisponiveis">Dispon�veis:</label><br />
		<select id="listaSegmentoAmostragemPCSDisponiveis" class="campoList listaCurta1a campoVertical" name="listaSegmentoAmostragemPCSDisponiveis" multiple="multiple"  
		onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoAmostragemPCSDisponiveis,document.forms[0].listaSegmentoAmostragemPCSSelecionados,true)">
			<c:forEach items="${listaSegmentoAmostragemPCSDisponiveis}" var="localAmostragemPCS">
				<option value="<c:out value="${localAmostragemPCS.chavePrimaria}"/>" title="<c:out value="${localAmostragemPCS.descricao}"/>"> <c:out value="${localAmostragemPCS.descricao}"/></option>		
			</c:forEach>
		</select>
		
		<fieldset class="conteinerBotoesCampoList1a">
			<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol2" id="botaoDireita" 
				onClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoAmostragemPCSDisponiveis,document.forms[0].listaSegmentoAmostragemPCSSelecionados,true);">
			<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
				onClick="moveAllOptionsEspecial(document.forms[0].listaSegmentoAmostragemPCSDisponiveis,document.forms[0].listaSegmentoAmostragemPCSSelecionados,true);">
			<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
				onClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoAmostragemPCSSelecionados,document.forms[0].listaSegmentoAmostragemPCSDisponiveis,true);">
			<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
				onClick="moveAllOptionsEspecial(document.forms[0].listaSegmentoAmostragemPCSSelecionados,document.forms[0].listaSegmentoAmostragemPCSDisponiveis,true);">
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerLista2" id="camposConteinerLocalAmostragemPCSSelecionados">
		<label id="rotuloLocalAmostragemPCSSelecionados" class="rotulo rotuloListaLonga2" for="listaSegmentoAmostragemPCSSelecionados">Selecionados: <span class="itemContador" id="quantidadePontosAssociados"></span></label><br />
		<select id="listaSegmentoAmostragemPCSSelecionados" class="campoList listaCurta2a campoVertical" name="listaSegmentoAmostragemPCSSelecionados" multiple="multiple" 
			onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoAmostragemPCSSelecionados,document.forms[0].listaSegmentoAmostragemPCSDisponiveis,true);">
			<c:forEach items="${listaSegmentoAmostragemPCSSelecionados}" var="localAmostragemPCS">
				<option value="<c:out value="${localAmostragemPCS.chavePrimaria}"/>" title="<c:out value="${localAmostragemPCS.descricao}"/>"> <c:out value="${localAmostragemPCS.descricao}"/></option>
			</c:forEach>
		</select>
					
		<fieldset class="conteinerBotoesCampoList2">
			<input type="button" class="bottonRightCol botoesLargosIE7" value="Para cima" onclick="moveOptionUp(this.form['listaSegmentoAmostragemPCSSelecionados']);">
			<input type="button" class="bottonRightCol botoesLargosIE7" value="Para baixo" onclick="moveOptionDown(this.form['listaSegmentoAmostragemPCSSelecionados']);">
		</fieldset>
		
	</fieldset>
	
	<br/><br/>
	
	<fieldset class="conteinerLista1">
	    <legend class="conteinerBlocoTitulo">Intervalo de Recupera��o do PCS</legend>
		<a name="listaIntervaloPCS"></a>
		<label class="rotulo rotuloVertical" id="rotulolistaSegmentoIntervaloPCSDisponiveis" for="listaSegmentoIntervaloPCSDisponiveis">Dispon�veis:</label><br />
		<select id="listaSegmentoIntervaloPCSDisponiveis" class="campoList listaCurta1a campoVertical" name="listaSegmentoIntervaloPCSDisponiveis" multiple="multiple"  
		onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoIntervaloPCSDisponiveis,document.forms[0].listaSegmentoIntervaloPCSSelecionados,true);habilitarDesabilitarTamanhoReducao();">
			<c:forEach items="${listaSegmentoIntervaloPCSDisponiveis}" var="intervaloPCS">
				<option value="<c:out value="${intervaloPCS.chavePrimaria}"/>" title="<c:out value="${intervaloPCS.descricao}"/>"> <c:out value="${intervaloPCS.descricao}"/></option>		
			</c:forEach>
		</select>
		
		<fieldset class="conteinerBotoesCampoList1a">
			<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol2" id="botaoDireita" 
				onClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoIntervaloPCSDisponiveis,document.forms[0].listaSegmentoIntervaloPCSSelecionados,true);">
			<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
				onClick="moveAllOptionsEspecial(document.forms[0].listaSegmentoIntervaloPCSDisponiveis,document.forms[0].listaSegmentoIntervaloPCSSelecionados,true);">
			<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
				onClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoIntervaloPCSSelecionados,document.forms[0].listaSegmentoIntervaloPCSDisponiveis,true);">
			<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
				onClick="moveAllOptionsEspecial(document.forms[0].listaSegmentoIntervaloPCSSelecionados,document.forms[0].listaSegmentoIntervaloPCSDisponiveis,true);">
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerLista2" id="camposConteinerIntervaloPCSSelecionados">
		<label id="rotuloIntervaloPCSSelecionados" class="rotulo rotuloListaLonga2" for="listaSegmentoIntervaloPCSSelecionados">Selecionados: <span class="itemContador" id="quantidadeIntervaloPCSSelecionados"></span></label><br />
		<select id="listaSegmentoIntervaloPCSSelecionados" class="campoList listaCurta2a campoVertical" name="listaSegmentoIntervaloPCSSelecionados" multiple="multiple" 
			onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaSegmentoIntervaloPCSSelecionados,document.forms[0].listaSegmentoIntervaloPCSDisponiveis,true);habilitarDesabilitarTamanhoReducao();">
			<c:forEach items="${listaSegmentoIntervaloPCSSelecionados}" var="intervaloPCS">
				<option value="<c:out value="${intervaloPCS.chavePrimaria}"/>" title="<c:out value="${intervaloPCS.descricao}"/>"> <c:out value="${intervaloPCS.descricao}"/></option>
			</c:forEach>
		</select>
		
		<fieldset class="conteinerBotoesCampoList2">
			<input type="button" class="bottonRightCol botoesLargosIE7" value="Para cima" onclick="moveOptionUp(this.form['listaSegmentoIntervaloPCSSelecionados']);">
			<input type="button" class="bottonRightCol botoesLargosIE7" value="Para baixo" onclick="moveOptionDown(this.form['listaSegmentoIntervaloPCSSelecionados']);">
		</fieldset>
		
	</fieldset>

</fieldset>

<label class="rotulo" for="tamanhoReducao">Tamanho da redu��o:</label>
<input class="campoTexto campoHorizontal" id="tamanhoReducao" name="tamanhoReducaoRamo" type="text" size="4" maxlength="4" 
	value="${segmentoForm.tamanhoReducao}" onkeypress="return formatarCampoInteiro(event);" />
<label class="rotuloInformativo" for="tamanhoReducao">dias</label><br class="quebraLinha2" />
