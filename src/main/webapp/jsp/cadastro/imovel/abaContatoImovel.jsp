<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<script>

function alterarContato(form, actionAdicionarContato) {
	submeter('imovelForm', actionAdicionarContato + '#contatos');
}

function incluirContato(form, actionAdicionarContato) {
	document.forms[0].indexLista.value = -1;
	submeter('imovelForm', actionAdicionarContato + '#contatos');
}
   
function removerContato(actionRemoverContato, indice) {
	document.forms[0].indexLista.value = indice;
	submeter('imovelForm', actionRemoverContato + '?operacaoLista=true#contatos');
}

function atualizarContatoPrincipal(actionAtualizarContatoPrincipal, indice) {
	document.forms[0].indexLista.value = indice;
	submeter('imovelForm', actionAtualizarContatoPrincipal + '?operacaoLista=true#contatos');
}

function limparContato() {
	document.forms[0].indexLista.value = -1;
	document.forms[0].idTipoContato.selectedIndex = 0;
	document.forms[0].idProfissao.selectedIndex = 0;
	document.forms[0].idProfissao.disabled = false;
	document.forms[0].nomeContato.value = "";
	document.forms[0].dddContato.value = "";
	document.forms[0].telefoneContato.value = "";
	document.forms[0].ramalContato.value = "";
	document.forms[0].dddContato2.value = "";
	document.forms[0].telefoneContato2.value = "";
	document.forms[0].ramalContato2.value = "";
	document.forms[0].areaContato.value = "";
	document.forms[0].emailContato.value = "";
	document.forms[0].indexLista.disabled = false;
	document.forms[0].idTipoContato.disabled = false;
	document.forms[0].nomeContato.disabled = false;
	document.forms[0].dddContato.disabled = false;
	document.forms[0].telefoneContato.disabled = false;
	document.forms[0].ramalContato.disabled = false;
	document.forms[0].areaContato.disabled = false;
	document.forms[0].emailContato.disabled = false;
	
	var botaoAlterarContato = document.getElementById("botaoAlterarContato");
	var botaoLimparContato = document.getElementById("botaoLimparContato");	
	var botaoIncluirContato = document.getElementById("botaoIncluirContato");	
	
	botaoAlterarContato.disabled = true;
	botaoLimparContato.disabled = false;
	botaoIncluirContato.disabled = false;

}

function exibirAlteracaoDoContato(indice,idTipoContato,idProfissao,nomeContato,dddContato,telefoneContato,ramalContato,
		dddContato2,telefoneContato2,ramalContato2,areaContato,emailContato) {
	
	if (indice != "") {
		document.forms[0].indexLista.value = indice;
		if (idTipoContato != "") {
			var tamanho = document.forms[0].idTipoContato.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idTipoContato.options[i];
				if (idTipoContato == opcao.value) {
					opcao.selected = true;
				}
			}
		}
		if (idProfissao != "") {
			var tamanho = document.forms[0].idProfissao.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idProfissao.options[i];
				if (idProfissao == opcao.value) {
					opcao.selected = true;
				}
			}
		} else if(idProfissao == "") {
			document.forms[0].idProfissao.selectedIndex = 0;
			document.forms[0].idProfissao.disabled = false;
		}
		if (nomeContato != "") {
			document.forms[0].nomeContato.value = nomeContato;
		}
		if (dddContato != "") {
			document.forms[0].dddContato.value = dddContato;
		} else {
			document.forms[0].dddContato.value = "";
		}
		if (telefoneContato != "") {
			document.forms[0].telefoneContato.value = telefoneContato;
		} else {
			document.forms[0].telefoneContato.value = "";
		}
		if (ramalContato != "") {
			document.forms[0].ramalContato.value = ramalContato;
		} else {
			document.forms[0].ramalContato.value = "";
		}
		if (dddContato2 != "") {
			document.forms[0].dddContato2.value = dddContato2;
		} else {
			document.forms[0].dddContato2.value = "";
		}
		if (telefoneContato2 != "") {
			document.forms[0].telefoneContato2.value = telefoneContato2;
		} else {
			document.forms[0].telefoneContato2.value = "";
		}
		if (ramalContato2 != "") {
			document.forms[0].ramalContato2.value = ramalContato2;
		} else {
			document.forms[0].ramalContato2.value = "";
		}
		if (areaContato != "") {
			document.forms[0].areaContato.value = areaContato;
		} else {
			document.forms[0].areaContato.value = "";
		}
		if (emailContato != "") {
			document.forms[0].emailContato.value = emailContato;
		} else {
			document.forms[0].emailContato.value = "";
		}
		
		var botaoAlterarContato = document.getElementById("botaoAlterarContato");
		var botaoLimparContato = document.getElementById("botaoLimparContato");	
		var botaoIncluirContato = document.getElementById("botaoIncluirContato");	
		botaoAlterarContato.disabled = false;
		botaoLimparContato.disabled = false;
		botaoIncluirContato.disabled = true;
	}
}

function onloadContato() {

	<c:choose>
		<c:when test="${imovelVO.indexLista > -1 && empty param['operacaoLista'] }">
			var botaoAlterarContato = document.getElementById("botaoAlterarContato");
			var botaoLimparContato = document.getElementById("botaoLimparContato");	
			var botaoIncluirContato = document.getElementById("botaoIncluirContato");
			botaoIncluirContato.disabled = true;	
			botaoAlterarContato.disabled = false;
			botaoLimparContato.disabled = false;
			
		</c:when>
		<c:when test="${sucessoManutencaoLista}">
			limparContato();
		</c:when>
	</c:choose>

	<c:if test="${imovelVO.indexLista > -1 && requestScope.paramAlteracao}">
		limparContato();
	</c:if>			
}

<c:if test="${abaId == '3'}">
	addLoadEvent(onloadContato);
</c:if>

</script>
<a class="linkHelp" href="<help:help>/abacontatoscadastrodoimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
<fieldset>
	<fieldset class="colunaEsq" id="ImovelcolunaEsqAbaContato">
		<label class="rotulo campoObrigatorio" id="rotuloTipoContato" for="idTipoContato"><span class="campoObrigatorioSimbolo2">* </span>Tipo do contato:</label>
		<select name="idTipoContato" id="idTipoContato" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTiposContato}" var="tipoContato">
				<option value="<c:out value="${tipoContato.chavePrimaria}"/>" <c:if test="${imovelContatoVO.idTipoContato == tipoContato.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoContato.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<label class="rotulo campoObrigatorio" id="rotuloNomeContato" for="nomeContato"><span class="campoObrigatorioSimbolo2">* </span>Nome do contato:</label>
		<input class="campoTexto" type="text" id="nomeContato" name="nomeContato" maxlength="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${imovelContatoVO.nomeContato}"><br />
		<label class="rotulo labelDddContato" id="rotuloDDDContato" for="dddContato"><span class="campoObrigatorioSimbolo2">* </span>DDD:</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddContato" name="dddContato" onkeypress="return formatarCampoInteiro(event);" maxlength="2" size="1" value="${imovelContatoVO.dddContato}">
		<label class="rotulo rotuloHorizontal labelRotuloNumeroContato" id="rotuloNumeroContato" for="telefoneContato"><span class="campoObrigatorioSimbolo2">* </span>Telefone:</label>
		<input class="campoTexto campoHorizontal" type="text" id="telefoneContato" name="telefoneContato" maxlength="9" size="7" onkeypress="return formatarCampoInteiro(event)" value="${imovelContatoVO.telefoneContato}">
		<label class="rotulo rotuloHorizontal labelRotuloRamalContato" id="rotuloRamalContato" for="ramalContato">Ramal:</label>
		<input class="campoTexto campoHorizontal" type="text" id="ramalContato" name="ramalContato" maxlength="4" size="4" onkeypress="return formatarCampoInteiro(event)" value="${imovelContatoVO.ramalContato}">
		
		<label class="rotulo" id="rotuloDDDContato" for="dddContato">DDD: (opcional)</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddContato2" name="dddContato2" onkeypress="return formatarCampoInteiro(event);" maxlength="2" value="${imovelContatoVO.dddContato2}" style="width: 30px;" />
		<label class="rotulo rotuloHorizontal telefoneContatoOpcional" id="rotuloNumeroContato2" for="telefoneContato2">Telefone: (opcional)</label>
		<input class="campoTexto campoHorizontal" type="text" id="telefoneContato2" name="telefoneContato2" maxlength="9" onkeypress="return formatarCampoInteiro(event)" value="${imovelContatoVO.telefoneContato2}" style="width: 61px;" />
		<label class="rotulo rotuloHorizontal rotuloRamalContato rotuloRamalContatoCss2" id="rotuloRamalContato2" for="ramalContato2">Ramal: (opcional)</label>
		<input class="campoTexto campoHorizontal" type="text" id="ramalContato2" name="ramalContato2" maxlength="4" onkeypress="return formatarCampoInteiro(event)" value="${imovelContatoVO.ramalContato2}" style="width: 48px;" />
		
	</fieldset>
	<fieldset id="ImovelcolunaEsqAbaContato2">
		<label class="rotulo" id="rotuloProfissao" for="idProfissao">Profiss�o:</label>
		<select name="idProfissao" id="idProfissao" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaProfissao}" var="profissao">
				<option value="<c:out value="${profissao.chavePrimaria}"/>" <c:if test="${imovelContatoVO.idProfissao == profissao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${profissao.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<label class="rotulo" for="areaContato">�rea:</label>
		<input class="campoTexto" type="text" id="areaContato" name="areaContato" maxlength="50" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${imovelContatoVO.areaContato}"><br />
		<label class="rotulo" id="rotuloEmailContato" for="emailContato" >E-mail do contato:</label>
		<input class="campoTexto" type="text" id="emailContato" name="emailContato" maxlength="80" size="40" onkeypress="return formatarCampoEmail(event)" value="${imovelContatoVO.emailContato}">
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar Contatos.</p>
</fieldset>

<fieldset class="conteinerBotoesAba"> 
	<input class="bottonRightCol2" name="botaoLimparContato" id="botaoLimparContato" value="Limpar" type="button" onclick="limparContato();">
	<input class="bottonRightCol2" name="botaoIncluirContato" id="botaoIncluirContato" value="Adicionar contato" type="button" onclick="incluirContato(this.form,'<c:out value="${param['actionAdicionarContato']}"/>');">
   	<input class="bottonRightCol2" disabled="disabled" name="botaoAlterarContato" id="botaoAlterarContato" value="Alterar contato" type="button" onclick="alterarContato(this.form,'<c:out value="${param['actionAdicionarContato']}"/>');">
</fieldset>
		
<c:set var="i" value="0" />
<display:table class="dataTableGGAS dataTableAba" name="listaContato" sort="list" id="contato" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	<display:column property="nome" sortable="false"  title="Nome" />
	<display:column property="codigoDDD" sortable="false"  title="DDD" />
	<display:column property="fone" sortable="false"  title="Telefone" />
	<display:column property="ramal" sortable="false"  title="Ramal" />
	<display:column property="codigoDDD2" sortable="false"  title="DDD(Opcional)" />
	<display:column property="fone2" sortable="false"  title="Telefone(Opcional)" />
	<display:column property="ramal2" sortable="false"  title="Ramal(Opcional)" />
	<display:column property="email" sortable="false"  title="Email" />
       
       <display:column sortable="false"  title="Principal" >
  			<c:choose>
			<c:when test="${contato.principal}">Sim</c:when>
			<c:otherwise>N�o</c:otherwise>
      	</c:choose>
  		</display:column>
  		
  		<display:column style="text-align: center;"> 
  			<c:choose>
      		<c:when test="${contato.principal}">
      			<a href="javascript:atualizarContatoPrincipal('<c:out value="${param['actionAtualizarContatoPrincipal']}"/>',<c:out value="${i}"/>);"></a>
      		</c:when>
      		<c:otherwise>
      			<a href="javascript:atualizarContatoPrincipal('<c:out value="${param['actionAtualizarContatoPrincipal']}"/>',<c:out value="${i}"/>);"><c:out value="Tornar Principal"/></a>
      		</c:otherwise>
      	</c:choose>
		 
       </display:column>
       
       <display:column style="text-align: center;"> 
		<a href="javascript:exibirAlteracaoDoContato('${i}','${contato.tipoContato.chavePrimaria}','${contato.profissao.chavePrimaria}','${contato.nome}',
													'${contato.codigoDDD}','${contato.fone}','${contato.ramal}','${contato.codigoDDD2}','${contato.fone2}','${contato.ramal2}',
													'${contato.descricaoArea}','${contato.email}');">
			<img title="Alterar contato" alt="Alterar contato"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0">
		</a> 
       </display:column>
       <c:choose>
	       	<c:when test="${param['actionRemoverContato'] ne 'false'}">
		       <display:column style="text-align: center;"> 
		       	<a onclick="return confirm('Deseja excluir o contato?');" href="javascript:removerContato('<c:out value="${param['actionRemoverContato']}"/>',<c:out value="${i}"/>);"><img title="Excluir contato" alt="Excluir contato"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
		       </display:column>
	       	</c:when>
	       	<c:otherwise>
		       <display:column style="text-align: center;"> 
		       	<a onclick="selecionarContatoImovel(${contato.chavePrimaria});" href="#"><img title="Selecionar contato" alt="Selecionar contato"  src="<c:url value="/imagens/check2.gif"/>" border="0"></a>
		       </display:column>
	       	</c:otherwise>
       </c:choose>
       
	<c:set var="i" value="${i+1}" />
</display:table>