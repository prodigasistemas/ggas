<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Segmento<a class="linkHelp" href="<help:help>/incluiralterarsegmento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar um Segmento clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<form:form method="post" action="atualizarSegmento" id="segmentoForm" name="segmentoForm">
	<script>
		
	function cancelar() {
			location.href = '<c:url value="/pesquisarSegmentos"/>';
		}		
			    
	    function alterar(){
	    	document.forms[0].executado.value = true;
	    	document.forms[0].operacao.value = "alterar";
	    	submeter("segmentoForm", "exibirAtualizacaoSegmento");	    	
	    }
		
	</script>	
	
	<input name="acao" type="hidden" id="acao" value="atualizarSegmento">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${segmentoForm.chavePrimaria}">
	<input name="versao" type="hidden" id="versao" value="${segmentoForm.versao}">
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">	
	<input type="hidden" name="idContaContabil" id="idContaContabil"/>
	<input type="hidden" name="listaRamoAtividades" id="listaRamoAtividades" value="${segmentoForm.ramosAtividades}"/>
	<input type="hidden" name="operacao" id="operacao"/>
	<input type="hidden" name="executado" id="executado"/>
	
	
	<fieldset id="conteinerSegmento" class="conteinerPesquisarIncluirAlterar">
		<legend class="conteinerBlocoTitulo">Dados Gerais</legend>
		<fieldset class="coluna detalhamentoColunaLarga2">
			<label class="rotulo" for="descricao">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${segmentoForm.descricao}"/></span><br />
			<label class="rotulo" for="descricaoAbreviada">Descri��o Abreviada:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${segmentoForm.descricaoAbreviada}"/></span><br />
			<label class="rotulo" for="idTipoSegmento">Tipo do Segmento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${segmentoForm.tipoSegmento.descricao}"/></span><br />		
		</fieldset>
		<fieldset id="tabs" style="display: none">
			<ul>
				<li><a href="#dadosFaturamento">Dados de Faturamento</a></li>				
				<li><a href="#dadosMedicao">Dados de Medi��o</a></li>		
				<li><a href="#ramoAtividade">Ramo de Atividade</a></li>				
				<li><a href="#comercial">Comercial</a></li>				
			</ul>
			<fieldset class="conteinerAba" id="dadosFaturamento">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosFaturamentoSegmentoDetalhar.jsp">
					<jsp:param name="detalhamento" value="false"/>
				</jsp:include>
			</fieldset>				
			<fieldset class="conteinerAba" id="dadosMedicao">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosMedicaoSegmentoDetalhar.jsp">
					<jsp:param name="detalhamento" value="false"/>
				</jsp:include>
			</fieldset>
			<fieldset class="conteinerAba" id="ramoAtividade">
				<jsp:include page="/jsp/cadastro/imovel/abaRamoAtividadeSegmentoDetalhar.jsp">
					<jsp:param name="detalhamento" value="false"/>
				</jsp:include>			
			</fieldset>	
			<fieldset class="conteinerAba" id="comercial">
				<jsp:include page="/jsp/cadastro/imovel/abaComercialSegmentoDetalhar.jsp">
					<jsp:param name="detalhamento" value="false"/>
				</jsp:include>			
			</fieldset>	
		</fieldset>	
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol2" type="button" id="button" value="Cancelar" onClick="cancelar();">
  	  <vacess:vacess param="atualizarSegmento">
  	  	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" type="button" id="botaoAlterar" value="Alterar" onClick="alterar();">
 	   </vacess:vacess>
	</fieldset>
	</fieldset>
</form:form>