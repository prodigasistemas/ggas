<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<h1 class="tituloInterno">Pesquisar Im�vel<a href="<help:help>/consultadosimveis.htm</help:help>" target="right"
                                             onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span
        class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>
    para exibir todos.
    Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" name="imovelForm" id="imovelForm" action="pesquisarImovel">

    <script language="javascript">

        $(document).ready(function () {

            var idRamoAtividadePonto = '<c:out value="${imovel.idSegmentoPontoConsumo}" />';
            carregarRamosAtividadePontoConsumo2(idRamoAtividadePonto);
            var idRamoAtividade = '<c:out value="${imovel.idRamoAtividadePontoConsumo}" />';
            $("#idRamoAtividadePontoConsumo").val(idRamoAtividade);

            $("#dialogCondicaoMedicao").dialog({
                autoOpen: false,
                modal: true,
                width: 1000,
                resizeble: false,
                draggable: false
            });
        });

        function exibirTelaInclusaoEmLote(chave) {
            document.forms[0].chavePrimaria.value = chave;
            submeter("imovelForm", "exibirInclusaoEmLote");
        }

        function selecionarCliente(idSelecionado) {
            var idCliente = document.getElementById("idCliente");
            var nomeCompletoCliente = document.getElementById("nomeCompletoCliente");
            var documentoFormatado = document.getElementById("documentoFormatado");
            var emailCliente = document.getElementById("emailCliente");
            var enderecoFormatado = document.getElementById("enderecoFormatadoCliente");

            if (idSelecionado != '') {
                AjaxService.obterClientePorChave(idSelecionado, {
                        callback: function (cliente) {
                            if (cliente != null) {
                                idCliente.value = cliente["chavePrimaria"];
                                nomeCompletoCliente.value = cliente["nome"];
                                if (cliente["cnpj"] != undefined) {
                                    documentoFormatado.value = cliente["cnpj"];
                                } else {
                                    if (cliente["cpf"] != undefined) {
                                        documentoFormatado.value = cliente["cpf"];
                                    } else {
                                        documentoFormatado.value = "";
                                    }
                                }
                                emailCliente.value = cliente["email"];
                                enderecoFormatado.value = cliente["enderecoFormatado"];
                            }
                        }, async: false
                    }
                );
            } else {
                idCliente.value = "";
                nomeCompletoCliente.value = "";
                documentoFormatado.value = "";
                emailCliente.value = "";
                enderecoFormatado.value = "";

            }

            document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
            document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
            document.getElementById("emailClienteTexto").value = emailCliente.value;
            document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
        }

        function limparFormulario() {

            document.imovelForm.matriculaImovel.value = "";
            document.imovelForm.cepImovel.value = "";
            document.imovelForm.nome.value = "";
            document.imovelForm.numeroImovel.value = "";
            document.imovelForm.complementoImovel.value = "";
            document.imovelForm.condominioSim.checked = false;
            document.imovelForm.condominioNao.checked = false;
            document.imovelForm.condominioAmbos.checked = true;
            document.forms[0].habilitado[0].checked = true;
            document.forms[0].tipoMedicaoAmbos[2].checked = true;
            document.getElementById('idCliente').value = "";
            document.getElementById('nomeCompletoCliente').value = "";
            document.getElementById('documentoFormatado').value = "";
            document.getElementById('enderecoFormatadoCliente').value = "";
            document.getElementById('emailCliente').value = "";
            document.getElementById('nomeClienteTexto').value = "";
            document.getElementById('documentoFormatadoTexto').value = "";
            document.getElementById('emailClienteTexto').value = "";
            document.getElementById('emailClienteTexto').value = "";
            // document.getElementById('matriculaCondominio').value = "";
            document.getElementById('nome').value = "";
            document.getElementById('complementoImovel').value = "";
            document.getElementById('pontoConsumoLegado').value = "";
            document.imovelForm.descricaoPontoConsumo.value = "";
            document.imovelForm.idSegmentoPontoConsumo.value = "";
            document.imovelForm.idRamoAtividadePontoConsumo.value = "";
            document.imovelForm.enderecoFormatadoTexto.value = "";
            $("#idCepImovel").val("")
            
            var abaAberta = $('#uf').val() + $('#cidade').val() + $('#logradouro').val() +   $("#cepscepImovel").html()
            if (abaAberta.trim() != ''){
            	  $("#linkPesquisarCEP").click();
            }
            $('#uf').val('')
            $('#cidade').val('')
            $('#logradouro').val('')
            $("#cepscepImovel").html('');
            try{
            pesquisandoCep = false
            limparDialogcepImovel()
            }catch(e){}
        }

        function incluir() {
            location.href = '<c:url value="/exibirInclusaoImovel"/>';
        }

        function exibirPopupPesquisaCliente() {
            popup = window.open('exibirPesquisaClientePopup', 'popup', 'height=750,width=700,toolbar=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes ,modal=yes');
        }

        function removerImovel() {

            var selecao = verificarSelecao();
            if (selecao == true) {
                var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
                if (retorno == true) {
                    submeter('imovelForm', 'removerImovel');
                }
            }
        }

        function detalharImovel(chave) {
            document.forms[0].chavePrimaria.value = chave;
            submeter("imovelForm", "exibirDetalhamentoImovel");
        }

        function alterarImovel() {
            var selecao = verificarSelecaoApenasUm();
            if (selecao == true) {
                document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
                submeter("imovelForm", "exibirAlteracaoImovel");
            }
        }

        function pesquisarImovel() {
            $("#botaoPesquisar").attr('disabled', 'disabled');
            submeter('imovelForm', 'pesquisarImovel');
        }

        function habilitarMatriculaCondominio() {
            document.forms[0].matriculaCondominio.disabled = false;
        }

        function desabilitarMatriculaCondominio() {
            document.forms[0].matriculaCondominio.value = "";
            document.forms[0].matriculaCondominio.disabled = true;
        }

        function carregarRamosAtividadePontoConsumo2(idRamoAtividadePonto) {

            var selectRamoAtividade = document.getElementById("idRamoAtividadePontoConsumo");

            selectRamoAtividade.length = 0;
            var novaOpcao = new Option("Selecione", "");
            selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;

            if (idRamoAtividadePonto != "") {
                selectRamoAtividade.disabled = false;
                $("#idRamoAtividadePontoConsumo").removeClass("campoDesabilitado");
                AjaxService.listarRamoAtividadePorSegmento(idRamoAtividadePonto, {
                        callback: function (listaRamoAtividade) {
                            for (ramo in listaRamoAtividade) {
                                for (key in listaRamoAtividade[ramo]) {
                                    var novaOpcao = new Option(listaRamoAtividade[ramo][key], key);
                                    selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
                                }
                            }

                        }, async: false
                    }
                );
            } else {
            }
        }

        function carregarMunicipios(siglaUnidadeFederativa) {

            var selectMunicipio = document.getElementById("nomeMunicipioPontoConsumoChamado");

            selectMunicipio.length = 0;
            var novaOpcao = new Option("Selecione", "");
            selectMunicipio.options[selectMunicipio.length] = novaOpcao;

            if (siglaUnidadeFederativa != "") {
                selectMunicipio.disabled = false;
                $("#nomeMunicipioPontoConsumoChamado").removeClass("campoDesabilitado");
                AjaxService.consultarMunicipiosPorUnidadeFederativaPorSiglaChamado(siglaUnidadeFederativa,
                    {
                        callback: function (listaMunicipio) {
                            for (key in listaMunicipio) {
                                var novaOpcao = new Option(key, key);
                                selectMunicipio.options[selectMunicipio.length] = novaOpcao;
                            }
                        }, async: false
                    }
                );

            } else {
                //selectMunicipio.disabled = true;
                //$("#nomeMunicipioPontoConsumo").addClass("campoDesabilitado");
            }
        }

        function init() {
            <c:if test="${listaImoveis ne null}">
            $.scrollTo($('#imovel'), 800);
            </c:if>
        }

        addLoadEvent(init);

        function alterarLogradouroLote() {

            var selecao = verificarSelecao();
            if (selecao == true) {
                var retorno = confirm('Deseja alterar o logradouro dos im�veis selecionados?');
                if (retorno == true) {
                    $("#cepImovel").val(null);
                    $("#numeroImovel").val(null);
                    $("#nome").val(null);
                    submeter('imovelForm', 'exibirAlteracaoLogradouroLote');
                }
            }
        }

    </script>
 <input name="idCepImovel" type="hidden" id="idCepImovel" value=""/>
    <input name="acao" type="hidden" id="acao" value="pesquisarImovel"/>
    <input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
    <input name="chavePrimarias" type="hidden" id="chavePrimarias">
    <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="false">

    <fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarCliente" class="colunaEsq">
            <legend>Pesquisar Pessoa Relacionada</legend>
            <div class="pesquisarClienteFundo">
                <p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Pessoa Relacionada</span> para
                    selecionar uma Pessoa.</p>
                <input name="cliente" type="hidden" id="idCliente" value="${imovel.cliente.chavePrimaria}">
                <input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${imovel.cliente.nome}">
                <input name="documentoFormatado" type="hidden" id="documentoFormatado" value="${documentoFormatado}">
                <input name="enderecoFormatadoCliente" type="hidden" id="enderecoFormatadoCliente" value="${enderecoFormatadoCliente}">
                <input name="emailCliente" type="hidden" id="emailCliente" value="${imovel.cliente.emailPrincipal}">

                <input name="Button" id="botaoPesquisarCliente" class="bottonRightCol2" title="Pesquisar Pessoa Relacionada"
                       value="Pesquisar Pessoa Relacionada" onclick="exibirPopupPesquisaCliente();" type="button"><br>
                <label class="rotulo" id="rotuloCliente" for="nomeClienteTexto">Cliente:</label>
                <input class="campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto" maxlength="50" size="50"
                       disabled="disabled" value="${imovel.cliente.nome}"><br/>
                <label class="rotulo" id="rotuloCnpjTexto" for="documentoFormatadoTexto">CPF/CNPJ:</label>
                <input class="campoDesabilitado" type="text" id="documentoFormatadoTexto" name="documentoFormatadoTexto" maxlength="18"
                       size="18" disabled="disabled" value="${documentoFormatado}"><br/>
                <label class="rotulo" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto">Endere�o:</label>
                <textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoTexto" rows="2" cols="37"
                          disabled="disabled">${enderecoFormatadoCliente}</textarea><br/>
                <label class="rotulo" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
                <input class="campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto" maxlength="80" size="40"
                       disabled="disabled" value="${imovel.cliente.emailPrincipal}"><br/>
            </div>
        </fieldset>
        <fieldset style="margin: 0px;margin-left: 35%;" id="pesquisaImovelCamposEsq">

            <legend>Campos do Im�vel</legend>
            <div class="pesquisarClienteFundo">
                <fieldset id="pesquisaImovelExibirCep" class="exibirCep">
                    <jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
                        <jsp:param name="cepObrigatorio" value="false"/>
                        <jsp:param name="idCampoCep" value="cepImovel"/>
                        <jsp:param name="numeroCep" value="${imovel.cepImovel}"/>
                    </jsp:include>
                </fieldset>
                <label class="rotulo" id="rotuloMatricula" for="numeroImovel">N�mero do im�vel:</label>
                <input class="campoTexto" type="text" id="numeroImovel" name="numeroImovel" maxlength="9" size="9"
                       value="${imovel.numeroImovel}" onkeyup="this.value = this.value.toUpperCase();" onkeyup="this.value = this.value.toUpperCase();"><br/>
                <label class="rotulo" id="rotuloComplemento" for="complementoImovel">Complemento:</label>
                <input class="campoTexto" type="text" id="complementoImovel" name="complementoImovel" maxlength="255" size="30"
                       value="${imovel.complementoImovel}"
                       onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                               value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"><br/>
                <label class="rotulo" id="rotuloNomeFantasia" for="nome">Descri��o:</label>
                <input class="campoTexto" type="text" id="nome" name="nome" maxlength="50" size="40" value="${imovel.nome}"
                       onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                               value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
                <label class="rotulo" id="rotuloMatricula" for="matricula">Matr�cula:</label>
                <input class="campoTexto" type="text" id="matricula" name="matriculaImovel" maxlength="9" size="9"
                       value="${imovel.matriculaImovel}" onkeypress="return formatarCampoInteiro(event)">
                       
                <label class="rotulo" id="rotuloMatricula" for="idSegmento">Situa��o<br/>Ponto Consumo:</label>
                <select style="margin-top: 1.5%;" name="idSituacaoPontoConsumo" id="idSituacaoPontoConsumo" class="campoSelect campo2Linhas">
                	<option value="">Selecione</option>
                    <c:forEach items="${listaSituacaoPontoConsumo}" var="situacaoPontoConsumo">
                        <option value="<c:out value="${situacaoPontoConsumo.chavePrimaria}"/>"
                                <c:if test="${imovel.idSituacaoPontoConsumo eq situacaoPontoConsumo.chavePrimaria}">selected="selected"</c:if>>
                            <c:out value="${situacaoPontoConsumo.descricao}"/>
                        </option>
                    </c:forEach>
                </select>
                       
                <label class="rotulo" id="rotuloMatricula" for="idSegmento">Segmento<br/>Ponto Consumo:</label>
                <select style="margin-top: 1.5%;" name="idSegmentoPontoConsumo" id="idSegmentoPontoConsumo" class="campoSelect campo2Linhas"
                        onchange="carregarRamosAtividadePontoConsumo2(this.value);">
                    <option value="">Selecione</option>
                    <c:forEach items="${listaSegmento}" var="segmento">
                        <option value="<c:out value="${segmento.chavePrimaria}"/>"
                                <c:if test="${imovel.idSegmentoPontoConsumo eq segmento.chavePrimaria}">selected="selected"</c:if>>
                            <c:out value="${segmento.descricao}"/>
                        </option>
                    </c:forEach>
                </select>
                <label class="rotulo" id="rotuloRamoAtividade" for="idRamoAtividade">Ramo de atividade<br/>Ponto Consumo:</label>
                <select style="margin-top: 1.5%;" name="idRamoAtividadePontoConsumo" id="idRamoAtividadePontoConsumo"
                        class="campoSelect campo2Linhas">
                    <option value="">Selecione</option>
                    <c:forEach items="${listaRamoAtividade}" var="ramoAtividade">
                        <option value="<c:out value="${ramoAtividade.chavePrimaria}"/>"
                                <c:if test="${imovel.idRamoAtividadePontoConsumo eq ramoAtividade.chavePrimaria}">selected="selected"</c:if>>
                            <c:out value="${ramoAtividade.descricao}"/>
                        </option>
                    </c:forEach>
                </select>
                <label class="rotulo rotulo2Linhas" id="rotuloPontoConsumoLegado" for="pontoConsumoLegado">C�digo do Ponto <br/>de Consumo:</label>
                <input class="campoTexto" type="text" id="pontoConsumoLegado" name="pontoConsumoLegado" maxlength="9" size="9"
                       value="${imovel.pontoConsumoLegado}" onkeypress="return formatarCampoInteiro(event)"><br/>
                <label class="rotulo" id="rotuloDescricaoPontoConsumo" for="descricaoPontoConsumo">Descri��o do Ponto<br/> de
                    Consumo:</label>
                <input class="campoTexto" type="text" id="descricaoPontoConsumo" name="descricaoPontoConsumo" maxlength="50" size="40"
                       value="${imovel.descricaoPontoConsumo}"
                       onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                               value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">

                <label class="rotulo" id="rotuloImovelCondominio" for="condominioSim">O im�vel � condom�nio?</label>
                <input class="campoRadio" type="radio" value="true" name="indicadorCondominioAmbos" id="condominioSim"
                       <c:if test="${imovel.indicadorCondominioAmbos eq 'true'}">checked</c:if> onclick="">
                <label for="condominioSim" class="rotuloRadio">Sim</label>
                <input class="campoRadio" type="radio" value="false" name="indicadorCondominioAmbos" id="condominioNao"
                       <c:if test="${imovel.indicadorCondominioAmbos eq 'false'}">checked</c:if> onclick="">
                <label for="condominioNao" class="rotuloRadio">N�o</label>
                <input class="campoRadio" type="radio" value="ambos" name="indicadorCondominioAmbos" id="condominioAmbos"
                       <c:if test="${imovel.indicadorCondominioAmbos ne 'true' && imovel.indicadorCondominioAmbos ne 'false'}">checked</c:if>
                       onclick="">
                <label for="condominioAmbos" class="rotuloRadio">Todos</label><br class="quebraRadio">

                <label class="rotulo" id="rotuloImovelMedicao" for="tipoMedicaoSim">Tipo de Medicao</label>
                <input class="campoRadio" type="radio" value="1" name="tipoMedicaoAmbos" id="tipoMedicaoSim"
                       <c:if test="${imovel.tipoMedicaoAmbos eq '1'}">checked</c:if>>
                <label for="tipoMedicaoSim" class="rotuloRadio">Individual</label>
                <input class="campoRadio" type="radio" value="2" name="tipoMedicaoAmbos" id="tipoMedicaoNao"
                       <c:if test="${imovel.tipoMedicaoAmbos eq '2'}">checked</c:if>>
                <label for="tipoMedicaoNao" class="rotuloRadio">Coletiva</label>
                <input class="campoRadio" type="radio" value="ambos" name="tipoMedicaoAmbos" id="tipoMedicaoAmbos"
                       <c:if test="${imovel.tipoMedicaoAmbos ne '1' && imovel.tipoMedicaoAmbos ne '2'}">checked</c:if>>
                <label for="tipoMedicaoAmbos" class="rotuloRadio">Todos</label><br class="quebraRadio">

                <label class="rotulo" for="habilitado">Indicador de Uso:</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitadoAtivo" value="true"
                       <c:if test="${imovel.habilitado eq true}">checked</c:if>>
                <label class="rotuloRadio" for="habilitadoAtivo">Ativo</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitadoInativo" value="false"
                       <c:if test="${imovel.habilitado eq false}">checked</c:if>>
                <label class="rotuloRadio" for="habilitadoInativo">Inativo</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitadoTodos" value=""
                       <c:if test="${empty imovel.habilitado}">checked</c:if>>
                <label class="rotuloRadio" for="habilitadoTodos">Todos</label>
            </div>
        </fieldset>
        <fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDir">
            <vacess:vacess param="pesquisarImovel">
                <input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button"
                       onclick="pesquisarImovel()">
            </vacess:vacess>
            <input id="botaoLimpar" name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button"
                   onclick="limparFormulario();">
        </fieldset>

    </fieldset>

    <c:if test="${listaImoveis ne null}">
        <hr class="linhaSeparadoraPesquisa"/>
        <display:table class="dataTableGGAS dataTableCabecalho2Linhas"
                       decorator="br.com.ggas.web.cadastro.imovel.decorator.ImovelResultadoPesquisaDecorator" name="listaImoveis"
                       id="imovel" partialList="true" sort="external" pagesize="${tamanhoPagina}" size="${pageSize}"
                       excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarImovel">

            <display:column property="chavePrimariaComLock" style="width: 25px" media="html" sortable="false"
                            title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"/>

            <display:column style="width: 30px" title="Ativo">
                <c:choose>
                    <c:when test="${imovel.habilitado == true}">
                        <img alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
                    </c:when>
                    <c:otherwise>
                        <img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
                    </c:otherwise>
                </c:choose>
            </display:column>
            <display:column style="width: 70px" title="Matr�cula" sortable="true" sortProperty="chavePrimaria">
                <a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
                    <c:out value='${imovel.chavePrimaria}'/>
                </a>
            </display:column>
            <display:column sortable="true" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq"
                            class="quebraLinhaTexto conteudoTabelaEsq">
                <a style="width: 270px" href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span
                        class="linkInvisivel"></span>
                    <span id="${imovel.chavePrimaria}-nome-imovel"><c:out value='${imovel.nome}'/></span>
                </a>
            </display:column>
            <display:column style="width: 80px" title="Situa��o</br>Im�vel" sortable="true" sortProperty="situacaoImovel">
                <a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
                    <span id="${imovel.chavePrimaria}-situacao-imovel"><c:out value='${imovel.situacaoImovel.descricao}'/></span>
                </a>
            </display:column>
            <display:column sortable="true" title="Endere�o" sortProperty="enderecoFormatado" headerClass="tituloTabelaEsq"
                            class="quebraLinhaTexto conteudoTabelaEsq">
                <a style="width: 260px" href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span
                        class="linkInvisivel"></span>
                    <span id="${imovel.chavePrimaria}-endereco-imovel"><c:out value='${imovel.enderecoFormatado}'/></span>
                </a>
            </display:column>
            <display:column sortable="true" title="Tipo Medi��o" sortProperty="modalidadeMedicaoImovel" headerClass="tituloTabelaEsq"
                            class="quebraLinhaTexto conteudoTabelaEsq" style="width: 80px">
                <a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
                    <c:choose>
                        <c:when test="${imovel.imovelCondominio ne null}">
                        <c:if test="${imovel.imovelCondominio.modalidadeMedicaoImovel ne null}">
                            <c:out value='${imovel.imovelCondominio.modalidadeMedicaoImovel.descricao}'/>
                         </c:if>
                        </c:when>
                        <c:otherwise>
                          <c:if test="${imovel.modalidadeMedicaoImovel ne null}">
                            <c:out value='${imovel.modalidadeMedicaoImovel.descricao}'/>
                           </c:if>
                        </c:otherwise>
                    </c:choose>
                </a>
            </display:column>
            <display:column style="width: 50px" title="Incluir<br />em Lote">
                <c:if test="${imovel.condominio}">
                    <a href='javascript:exibirTelaInclusaoEmLote(<c:out value='${imovel.chavePrimaria}'/>);'><span
                            class="linkInvisivel"></span>
                        <img src="<c:url value="/imagens/add-16x16.png"  /> " border="0">
                    </a>
                </c:if>
            </display:column>
            <display:column style="width: 115px; text-align: center;" title="Situa��o</br>Ponto Consumo" sortable="true" sortProperty="situacaoC.descricao">
                <a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
                    <span id="${imovel.chavePrimaria}-situacao-ponto-consumo"><c:out value='${imovel.situacaoPontoConsumo}'/></span>
                </a>
            </display:column>
        </display:table>

        <jsp:include page="/jsp/comum/selecionadorTamanhoPaginaTabelaPesquisar.jsp">
            <jsp:param name="acao" value="pesquisarImovel?"/>
        </jsp:include>
    </c:if>

    <fieldset class="conteinerBotoes">
        <c:if test="${not empty listaImoveis}">
            <vacess:vacess param="exibirAlteracaoImovel">
                <input id="botaoAlterar" name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarImovel()"
                       type="button">
            </vacess:vacess>
            <vacess:vacess param="removerImovel">
                <input id="botaoRemover" name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo"
                       onclick="removerImovel()" type="button">
            </vacess:vacess>
            <vacess:vacess param="exibirAlteracaoImovel">
                <input name="button" value="Altera��o em Lote" class="bottonRightCol2" onclick="alterarLogradouroLote();" type="button">
            </vacess:vacess>
            <input name="button" value="Exibir condi��es para medi��o" class="bottonRightCol2" onclick="exibirCondicoesParaMedicao();"
                   type="button">
        </c:if>
        <vacess:vacess param="exibirInclusaoImovel">
            <input name="button" value="Incluir" id="botaoIncluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();"
                   type="button">
        </vacess:vacess>
    </fieldset>

</form:form>

<div id="dialogCondicaoMedicao" title="Condi��es para medi��o do im�vel selecionado" style="display: none">

    <div id="resultadoDialogCondicaoMedicao">
        <style>
            .table-log {
                border: 1px solid #c0c0c0;
            }

            .th-log {
                border: 1px solid #c0c0c0;
                padding: 10px;
                font-weight: 700;
            }

            .td-log {
                border: 1px solid #c0c0c0;
                padding: 10px;
            }

            .altura-icone {
                height: 15px;
            }
            b {
                font-weight: 700 !important;
            }

            .table-scroll {
                max-height: 300px;
            }

            ::-webkit-scrollbar {
                width: 7px
            }

            ::-webkit-scrollbar-thumb {
                background-color: #aaa;
                opacity: 0.5;
                border-radius: 6px;
                -webkit-transition: background-color .2s linear, width .2s ease-in-out;
            }
        </style>

        <p>A condi��o m�nima para que um im�vel seja apto para medi��o � ter todas as colunas da tabela preenchidas com
            <img alt="V�lido" title="Configurado" src="<c:url value="/imagens/success_icon.png"/>" border="0" class="altura-icone">. Se a coluna estiver preenchida
            com <img alt="Inv�lido" title="Configura��o faltante" src="<c:url value="/imagens/cancel16.png"/>" border="0" class="altura-icone">, ent�o a configura��o est�
            faltando.
        </p>

        <p id="dadosNecessarios" style="margin: 0 0 8px;">Dados necess�rios para faturamento do im�vel selecionado</p>

        <hr style="border: 1px solid #b9b9b9;margin: 2px 0 7px 0;"/>

        <div id="condicoes-medicao" style="display: none">

            <p style="margin-bottom: 2px;"><b>Im�vel</b>: <span id="imovel-log"></span></p>
            <p style="margin-bottom: 2px;"><b>Situa��o</b>: <span id="situacao-log"></span></p>
            
            <p><b>Endere�o</b>: <span id="endereco-log"></span></p>

            <div class="table-scroll">
                <table class="table-log">
                    <thead>
                    <tr>
                        <th class="th-log">Ponto de Consumo</th>
                        <th class="th-log">C�digo �nico</th>
                        <th class="th-log">City Gate</th>
                        <th class="th-log">Grupo de Faturamento</th>
                        <th class="th-log">Rota</th>
                        <th class="th-log">Periodicidade</th>
                        <th class="th-log">Meio de Leitura</th>
                        <th class="th-log">N�mero de S�rie do Medidor</th>
                        <th class="th-log">Tipo de Medidor</th>
                        <th class="th-log">Data de Instala��o</th>
                        <th class="th-log">Data de Ativa��o</th>
                        <th class="th-log">Press�o Contrato</th>
                        <th class="th-log">Vaz�o Instant�nea Contrato</th>
                        <th class="th-log">Google Maps</th>
                        
                    </tr>
                    </thead>

                    <tbody id="tbody-log">

                    </tbody>
                </table>
            </div>
        </div>
        <p id="erroDialogCondicaoMedicao" style="display: none; color: red; text-align: center; font-size: 18pt; margin-top: 20px;"></p>
        <p id="consultandoDialogCondicaoMedicao"
           style="display: none; color: #407da8; text-align: center; font-size: 18pt; margin-top: 20px;">Aguarde, consultando...</p>

    </div>
</div>

<script>

    var imagemInvalida = '<img alt="Configura��o faltante" title="Configura��o faltante" src="<c:url value="/imagens/cancel16.png"/>" border="0" class="altura-icone"> ';
    var imagemValida = '<img alt="Configurado" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0" class="altura-icone"> ';

    function novaCelula(objeto, propriedade, propridadeValido, exibirImagem) {
        var linha = '<td class="td-log" style="vertical-align: middle;">';
        if (exibirImagem) {


            if (!objeto[propridadeValido] || objeto[propriedade].length === 0) {
                linha += imagemInvalida + objeto[propriedade];
            } else {
                console.log('objeto[propriedade]', objeto[propriedade]);
                linha += imagemValida + objeto[propriedade];
            }
        } else {
            linha += objeto[propriedade];
        }
        linha += '</td>';
        return linha;
    }

    function exibirCondicoesParaMedicao() {
        console.warn('exibirCondicoesParaMedicao');

        $("#dialogCondicaoMedicao").dialog('open');
        var selecionados = $("input[name=chavesPrimarias]:checked").length;
        $('#erroDialogCondicaoMedicao').hide();
        $('#tbody-log').empty();
        $('#condicoes-medicao').hide();
        $('#dadosNecessarios').hide();
        if (selecionados === 1) {
            $('#consultandoDialogCondicaoMedicao').show();
            var idImovel = $($("input[name=chavesPrimarias]:checked")[0]).val();
            AjaxService.exibirCondicoesParaMedicao(idImovel,
                function (condicoes) {
                    $('#consultandoDialogCondicaoMedicao').hide();

                    if (condicoes[0]['erro'] == 'true') {
                        $('#erroDialogCondicaoMedicao').html('Ocorreu um erro ao verificar as medi��es, contate a equipe de TI e informe o ocorrido!');
                        $('#erroDialogCondicaoMedicao').show();
                    } else {
                        $('#dadosNecessarios').show();

                        $('#imovel-log').html($('#' + idImovel + '-nome-imovel').html());
                        $('#situacao-log').html($('#' + idImovel + '-situacao-imovel').html());
                        $('#endereco-log').html($('#' + idImovel + '-endereco-imovel').html());

                        for (var indice in condicoes) {
                        	
                            var linha = '<tr>';

                            linha += novaCelula(condicoes[indice], 'pontoConsumo.descricao', 'valido', false);
                            linha += novaCelula(condicoes[indice], 'codigoLegado', 'valido', true);
                            linha += novaCelula(condicoes[indice], 'cityGates.descricao', 'cityGates.valido', true);
                            linha += novaCelula(condicoes[indice], 'rota.grupoFaturamento.descricao', 'rota.grupoFaturamento.valido', true);
                            linha += novaCelula(condicoes[indice], 'rota.descricao', 'rota.valido', true);
                            linha += novaCelula(condicoes[indice], 'rota.periodicidade.descricao', 'rota.periodicidade.valido', true);
                            linha += novaCelula(condicoes[indice], 'rota.tipoLeitura.descricao', 'rota.tipoLeitura.valido', true);
                            linha += novaCelula(condicoes[indice], 'numeroSerieMedidor', 'valido', true);
                            linha += novaCelula(condicoes[indice], 'tipoMedidor', 'valido', true);
                            linha += novaCelula(condicoes[indice], 'dataInstalacao', 'valido', true);
                            linha += novaCelula(condicoes[indice], 'dataAtivacao', 'valido', true);
                            linha += novaCelula(condicoes[indice], 'pressaoContrato', 'valido', false);
                            linha += novaCelula(condicoes[indice], 'vazaoContrato', 'valido', false);
                            linha += '<td class="td-log" style="vertical-align: middle;"><a href="javascript:irParaGoogleMaps('+condicoes[indice]["latitude"]+','+condicoes[indice]["longitude"]+');"><img class="alinhamentoGoogleMaps" style="display:inline-block;height: 35px;" border="0" src="<c:url value="/imagens/google-maps.png"/>"/></a></td>';		


                            linha += '</tr>';

                            $('#tbody-log').append(linha);
                        }

                        $('#condicoes-medicao').show();
                    }
                }
            );
        } else if (selecionados > 1) {
            $('#erroDialogCondicaoMedicao').html('Selecione somente um im�vel');
            $('#erroDialogCondicaoMedicao').show();
        } else {
            $('#erroDialogCondicaoMedicao').html('Selecione pelo menos um im�vel');
            $('#erroDialogCondicaoMedicao').show();
        }


    }
    
    
    $( "#cepImovel" ).keydown(function() {
    	  	$( "#idCepImovel" ).val('')
    	  	 $("#cepscepImovel").html('');
    })
    
   function irParaGoogleMaps(latitude, longitude){

		if(latitude != null && longitude != null) {
			var url = "https://www.google.com/maps/search/?api=1&query="+latitude+"%2C"+longitude;
			window.open(url, '_blank').focus();
		} else {
			alert("N�o existe latitude ou longitude cadastrada para o ponto de consumo.");
		}
	}		
    

    
    
</script>
