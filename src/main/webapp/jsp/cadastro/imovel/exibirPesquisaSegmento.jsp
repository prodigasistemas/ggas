<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	
        
<h1 class="tituloInterno">Pesquisar Segmento<a class="linkHelp" href="<help:help>/consultadesegmento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarSegmentos" id="segmentoForm" name="segmentoForm">

	<script>

		function exibirDetalhamento(chave) {
			document.forms[0].chavePrimaria.value = chave;
			document.forms[0].operacao.value = "detalharSegmento";
			submeter("segmentoForm", "exibirDetalhamentoSegmento");
		}
		
		function alterarSegmento() {
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].operacao.value = "alterarSegmento";
				document.forms[0].executado.value = true;
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
	  		   submeter("segmentoForm", "exibirAtualizacaoSegmento");
			}
		}
	
		function incluir() {
			location.href = '<c:url value="/exibirInsercaoSegmento?operacao=incluir"/>';
		}
	
		function removerSegmento(){
			var selecao = verificarSelecao();
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('segmentoForm', 'removerSegmento');
				}
		    }
		}
		
		function limparFormulario() {		
			var form = document.segmentoForm;
			limparFormularios(form);
			document.forms[0].habilitado[0].checked = true;
		}
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarSegmentos">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input type="hidden" name="operacao" id="operacao"/>
	<input type="hidden" name="operacaoSegmento" id="operacaoSegmento"/>	
	<input type="hidden" name="executado" id="executado" />
	
	<fieldset id="pesquisarSegmento" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" for="descricao">Descri��o:</label>
			<input type="text" class="campoTexto" name="descricao" maxlength="15" size="15" id="descricao"
			onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" value="${segmentoForm.descricao}" /><br />
		
			<label class="rotulo" for="codigoTipoSegmento">Tipo do Segmento:</label>
			<select name="tipoSegmento" id="idTipoSegmento" class="campoSelect" n>
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoSegmento}" var="tipoSegmento">
				<option value="<c:out value="${tipoSegmento.chavePrimaria}"/>" <c:if test="${segmentoForm.tipoSegmento.chavePrimaria == tipoSegmento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoSegmento.descricao}"/>
				</option>
				</c:forEach>
			</select>
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicadorUsoAtivo" value="true" <c:if test="${habilitado eq true}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicadorUsoInativo" value="false" <c:if test="${habilitado eq false}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoInativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarSegmentos">
		    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
		    </vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaSegmento ne null}">     
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="listaSegmento" sort="list" id="segmento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarSegmentos">
        <display:column style="text-align: center; width: 40px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${segmento.chavePrimaria}">
        </display:column>
        <display:column title="Ativo" style="text-align: center; width: 30px">
	     	<c:choose>
				<c:when test="${segmento.habilitado == true}">
					<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
				</c:when>
				<c:otherwise>
					<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
				</c:otherwise>				
			</c:choose>
		</display:column>
                
        <display:column sortable="true" title="Descri��o">        
	        <a href="javascript:exibirDetalhamento(<c:out value='${segmento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value='${segmento.descricao}'/>
			</a>
		</display:column>
        
        <display:column sortable="true"  title="Descri��o Abreviada" style="width: 270px">
        	<a href="javascript:exibirDetalhamento(<c:out value='${segmento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
        		<c:out value='${segmento.descricaoAbreviada}'/>
			</a>
		</display:column>
        
    </display:table>
    </c:if>
    <fieldset class="conteinerBotoes">
   		<c:if test="${listaSegmento ne null}">
   			<vacess:vacess param="exibirAtualizacaoSegmento">
   				<input id="botaoaAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarSegmento()" type="button">
   			</vacess:vacess>
   			<vacess:vacess param="removerSegmento">
				<input id="botaoRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerSegmento()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInsercaoSegmento">
			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>

</form:form>