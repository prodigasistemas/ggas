<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<!-- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> -->

<h1 class="tituloInterno">Adicionar Ramo Atividade<a class="linkHelp" href="<help:help>/incluiralterarsegmento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Clique na Substitui��o Tribut�ria desejada para adicion�-la</p>

<form:form method="post" id="segmentoForm" name="segmentoForm">
	<script type="text/javascript">
	
		$(document).ready(function(){			
			$("#dataInicioVigencia,.dataInicioVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>',
				showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});  
			$("#dataFimVigencia,.dataFImVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>',
				showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
/* 			$("select#tipoSubsTributaria").attr('value',-1);
			$("input#dataInicioVigencia").val("");
			$("input#dataFimVigencia").val("");
			$("input#valorSubstituto").val("");
			$("input#percentualSubstituto").val(""); */

		});
		
		function adicionarRamoAtividade() {
			document.forms[0].executado.value = false;
			if (document.forms[0].descricaoRamoAtividade.value == "") {
				alert("O campo descri��o � obrig�t�rio.");
			} else {
				selecionarListasRamoAtividade();
				$.ajax({
					type : 'POST',
					url : "adicionarRamoAtividadeInserir",
					data : $("#segmentoForm").serialize(),
					async : false
				}).success(function(data) {
					window.opener.retornarTela();
					setInterval("window.close()", 30);
				});

				/*
				if (operacao=='incluir') {
				window.opener.submeter('segmentoForm', 'exibirInsercaoSegmento');
				} else if (operacao=='alterar') {
				
				window.opener.submeter('segmentoForm', 'exibirAtualizacaoSegmento');
				}			
				 */
				//setInterval("window.close()", 30);
			}
		}

		function cancelar() {
			submeter('segmentoForm', 'limparListaRamoAtividade');
			window.close();
		}

		function limparRamoAtividade() {
			limparFormularios(segmentoForm);
			moveAllOptionsEspecial(
					document.forms[0].listaLocalAmostragemPCSSelecionados,
					document.forms[0].listaLocalAmostragemPCSDisponiveis, true);
			moveAllOptionsEspecial(
					document.forms[0].listaIntervaloPCSSelecionados,
					document.forms[0].listaIntervaloPCSDisponiveis, true);
		}

		function carregarAbaMedicao() {

			document
					.getElementById('listaRamoAtividadeAmostragemPCSSelecionados').value = document.forms[0].listaRamoAtividadeAmostragemPCSSelecionados.value;
			document
					.getElementById('listaRamoAtividadeAmostragemPCSDisponiveis').value = document.forms[0].listaRamoAtividadeAmostragemPCSDisponiveis.value;
			document
					.getElementById('listaRamoAtividadeIntervaloPCSSelecionados').value = document.forms[0].listaRamoAtividadeIntervaloPCSSelecionados.value;
			document
					.getElementById('listaRamoAtividadeIntervaloPCSDisponiveis').value = document.forms[0].listaRamoAtividadeIntervaloPCSDisponiveis.value;
		}

		function selecionarListasRamoAtividade() {
			var listaRamoAtividadeAmostragemPCSDisponiveis = document
					.getElementById('listaRamoAtividadeAmostragemPCSDisponiveis');
			if (listaRamoAtividadeAmostragemPCSDisponiveis != undefined) {
				for (i = 0; i < listaRamoAtividadeAmostragemPCSDisponiveis.length; i++) {
					listaRamoAtividadeAmostragemPCSDisponiveis.options[i].selected = true;
				}
			}
			var listaRamoAtividadeAmostragemPCSSelecionados = document
					.getElementById('listaRamoAtividadeAmostragemPCSSelecionados');
			if (listaRamoAtividadeAmostragemPCSSelecionados != undefined) {
				for (i = 0; i < listaRamoAtividadeAmostragemPCSSelecionados.length; i++) {
					listaRamoAtividadeAmostragemPCSSelecionados.options[i].selected = true;
				}
			}
			var listaRamoAtividadeIntervaloPCSDisponiveis = document
					.getElementById('listaRamoAtividadeIntervaloPCSDisponiveis');
			if (listaRamoAtividadeIntervaloPCSDisponiveis != undefined) {
				for (i = 0; i < listaRamoAtividadeIntervaloPCSDisponiveis.length; i++) {
					listaRamoAtividadeIntervaloPCSDisponiveis.options[i].selected = true;
				}
			}
			var listaRamoAtividadeIntervaloPCSSelecionados = document
					.getElementById('listaRamoAtividadeIntervaloPCSSelecionados');
			if (listaRamoAtividadeIntervaloPCSSelecionados != undefined) {
				for (i = 0; i < listaRamoAtividadeIntervaloPCSSelecionados.length; i++) {
					listaRamoAtividadeIntervaloPCSSelecionados.options[i].selected = true;
				}
			}
		}
	</script>

	<input name="postBack" type="hidden" id="postBack" value="true"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${segmentoForm.chavePrimaria}"/>
	<input name="ramoAtividadeSubstituicaoTributaria" type="hidden" id="ramoAtividadeSubstituicaoTributaria" value="${ramoAtividade.ramoAtividadeSubstituicaoTributaria}"/>	
	<input name="versao" type="hidden" id="versao" value="${ramoAtividade.versao}"/>
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}"/>
	<input type="hidden" name="idContaContabil" id="idContaContabil"/>	
	<input type="hidden" name="idRamoAtividade" id="idRamoAtividade" value="${ramoAtividade.chavePrimaria}"/>
	<input type="hidden" name="operacao" id="operacao" value="${operacao}"/>
	<input type="hidden" name="operacaoSubstituicao" id="operacaoSubstituicao" />
	<input type="hidden" name="executado" id="executado" />	
	<input type="hidden" name="operacaoRamoAtividade" id="operacaoRamoAtividade" />
	<input type="hidden" name="operacaoExecutada" id="operacaoExecutada" value="${operacaoExecutada}"/>
	<input type="hidden" name="updateRamoAtividade" id="updateRamoAtividade" />
	
	<fieldset id="conteinerSegmento" class="conteinerPesquisarIncluirAlterar">
		<legend class="conteinerBlocoTitulo">Dados Gerais</legend>
		<fieldset class="coluna">
			<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
			<input class="campoTexto" id="descricaoRamoAtividade" name="descricao" type="text" size="90" maxlength="90"
			onkeypress="return letraMaiuscula(this)" onkeyup="return letraMaiuscula(this)" value="${ramoAtividade.descricao}"><br />
			<label class="rotulo" for="periodicidade">Periodicidade :</label>
			<select class="campoSelect" id="periodicidadeRamoAtividade" name="periodicidade" >
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaPeriodicidades}" var="periodicidade">
					<option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${ramoAtividade.periodicidade.chavePrimaria == periodicidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${periodicidade.descricao}"/>
					</option>
			    </c:forEach>
		    </select>
		</fieldset>
		<fieldset id="tabs" style="display: none">
			<ul>
				<li><a href="#dadosFaturamento">Dados de Faturamento</a></li>
				<li><a href="#dadosMedicao">Dados de Medi��o</a></li>
			</ul>
			<fieldset class="conteinerAba" id="dadosFaturamento">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosFaturamentoPopupRamoAtividadeIncluir.jsp">
					<jsp:param name="alteracao" value="false"/>
				</jsp:include>
			</fieldset>	
			<fieldset class="conteinerAba" id="dadosMedicao">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosMedicaoPopupRamoAtividadeIncluir.jsp">
					<jsp:param name="alteracao" value="false"/>
				</jsp:include>
			</fieldset>
		</fieldset>
		
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de um ramo de atividade.</p>
			
		<fieldset class="conteinerBotoesPopup"> 
			<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();"/>
		    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparRamoAtividade();"/>
		    <input name="botaoAdicionar" class="bottonRightCol2 botaoGrande1" id="botaoAdicionar" value="Adicionar" type="button" onclick="adicionarRamoAtividade();"/>
	 	</fieldset>
	</fieldset>
</form:form>