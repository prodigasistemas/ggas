<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<script>

	function carregarRamosAtividade(elem) {	
		var idSegmento = elem.value;
      	var selectRamoAtividade = document.getElementById("idRamoAtividade");
    
      	selectRamoAtividade.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
      	  
      	if(idSegmento != '-1'){
	       	AjaxService.listarRamoAtividadePorSegmento( idSegmento, {
	           	callback:function(ramosAtividade) {  
	               	for (ramo in ramosAtividade){
	               		for (key in ramosAtividade[ramo]){
		                	var novaOpcao = new Option(ramosAtividade[ramo][key], key);
		            		selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
	               		}
	            	}
	            	selectRamoAtividade.disabled = false;
	            	$("#idRamoAtividade").removeClass("campoDesabilitado");
	        	}, async:false}
	        );
	    } else {	    	
	    	selectRamoAtividade.disabled = true;
	    	$("#idRamoAtividade").addClass("campoDesabilitado");
	    }
			
	}

	function alterarUnidadeConsumidora(form, actionAdicionarUnidadeConsumidora) {
		submeter('imovelForm', actionAdicionarUnidadeConsumidora + '#unidadesConsumidoras');
	}
	
	function incluirUnidadeConsumidora(form, actionAdicionarUnidadeConsumidora) {
		document.forms[0].indexLista.value = -1;
		submeter('imovelForm', actionAdicionarUnidadeConsumidora + '#unidadesConsumidoras');
	}
	   
	function removerUnidadeConsumidora(actionRemoverUnidadeConsumidora, indice) {
		document.forms[0].indexLista.value = indice;
		submeter('imovelForm', actionRemoverUnidadeConsumidora + '?operacaoLista=true#unidadesConsumidoras');
	}
	
	function limparUnidadeConsumidora() {		
		document.forms[0].indexLista.value = -1;		
		document.forms[0].idRamoAtividade.value = "-1";
		document.forms[0].idSegmento.value = "-1";
        document.forms[0].quantidadeEconomia.value = "";
        document.forms[0].idRamoAtividade.disabled = true;
        
		var botaoAlterarUnidadeConsumidora = document.getElementById("botaoAlterarUnidadeConsumidora");
		var botaoLimparUnidadeConsumidora = document.getElementById("botaoLimparUnidadeConsumidora");	
		var botaoIncluirUnidadeConsumidora = document.getElementById("botaoIncluirUnidadeConsumidora");	
		
		botaoAlterarUnidadeConsumidora.disabled = true;
		botaoLimparUnidadeConsumidora.disabled = false;
		botaoIncluirUnidadeConsumidora.disabled = false;
	
	}
	
	function exibirAlteracaoUnidadeConsumidora(indice,idSegmento,idRamoAtividade,quantidadeEconomia) {
		if (indice != "") {
			
			document.forms[0].indexLista.value = indice;
			
			var selectRamoAtividade = document.getElementById("idRamoAtividade");
			selectRamoAtividade.length=0;
      		var novaOpcao = new Option("Selecione","-1");
        	selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
			
			if (idSegmento != "") {
			
				AjaxService.listarRamoAtividadePorSegmento( idSegmento, {
		           	callback: function(ramosAtividade) { 
		           		for (ramo in ramosAtividade){
		               		for (key in ramosAtividade[ramo]){
			                	var novaOpcao = new Option(ramosAtividade[ramo][key], key);
		            			selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
		               		}
	            		}
	            		selectRamoAtividade.disabled = false;
	            		$("#idRamoAtividade").removeClass("campoDesabilitado");
	        		}, async:false}
		        );
			
				var tamanho = document.forms[0].idSegmento.length;
				var opcao = undefined;
				for(var i = 0; i < tamanho; i++) {
					opcao = document.forms[0].idSegmento.options[i];
					if (idSegmento == opcao.value) {
						opcao.selected = true;
					}
				}
			}
			
			if (idRamoAtividade != "") {
				var tamanho = document.forms[0].idRamoAtividade.length;
				var opcao = undefined;
				for(var i = 0; i < tamanho; i++) {
					opcao = document.forms[0].idRamoAtividade.options[i];
					if (idRamoAtividade == opcao.value) {
						opcao.selected = true;
					}
				}
			}
			
			if (quantidadeEconomia != "") {
				document.forms[0].quantidadeEconomia.value = quantidadeEconomia;
			} else {
				document.forms[0].quantidadeEconomia.value = "";
			}			
						
			var botaoAlterarUnidadeConsumidora = document.getElementById("botaoAlterarUnidadeConsumidora");
			var botaoLimparUnidadeConsumidora = document.getElementById("botaoLimparUnidadeConsumidora");	
			var botaoIncluirUnidadeConsumidora = document.getElementById("botaoIncluirUnidadeConsumidora");	
			botaoAlterarUnidadeConsumidora.disabled = false;
			botaoLimparUnidadeConsumidora.disabled = false;
			botaoIncluirUnidadeConsumidora.disabled = true;
		}
	}
	
	function onloadUnidadeConsumidora() {
		<c:choose>
			<c:when test="${imovelVO.indexLista > -1 && empty param['operacaoLista']}">
				var botaoAlterarUnidadeConsumidora = document.getElementById("botaoAlterarUnidadeConsumidora");
				var botaoLimparUnidadeConsumidora = document.getElementById("botaoLimparUnidadeConsumidora");	
				var botaoIncluirUnidadeConsumidora = document.getElementById("botaoIncluirUnidadeConsumidora");
				botaoIncluirUnidadeConsumidora.disabled = true;
				botaoAlterarUnidadeConsumidora.disabled = false;	
				botaoLimparUnidadeConsumidora.disabled = false;				
			</c:when>
			<c:when test="${sucessoManutencaoLista}">	
				limparUnidadeConsumidora();
			</c:when>
		</c:choose>

		<c:if test="${imovelVO.indexLista > -1 && requestScope.paramAlteracao}">
			limparUnidadeConsumidora();
		</c:if>
		
	}

<c:if test="${abaId == '4'}">
	addLoadEvent(onloadUnidadeConsumidora);
</c:if>

</script>

<a class="linkHelp" href="<help:help>/abaunidadedeconsumidoracadastrodoimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
<fieldset>
	<label class="rotulo campoObrigatorio" id="rotuloSegmento labelIdSegmentoPosicionamento" for="idSegmento"><span class="campoObrigatorioSimbolo2">* </span>Segmento:</label>
	<select name="idSegmento" id="idSegmento" class="campoSelect campoHorizontal" onchange="carregarRamosAtividade(this);">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaSegmento}" var="segmento">
			<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${imovelUnidadeConsumidoraVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${segmento.descricao}"/>
			</option>		
		</c:forEach>
	</select>
	<label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloRamoAtividade" for="idRamoAtividade"><span class="campoObrigatorioSimbolo2">* </span>Ramo de atividade:</label>
	<select name="idRamoAtividade" id="idRamoAtividade" class="campoSelect campoHorizontal" <c:if test="${empty listaRamoAtividade}">disabled="disabled""</c:if>>
		<option value="-1">Selecione</option>
		<c:forEach items="${listaRamoAtividade}" var="ramoAtividade">
			<option value="<c:out value="${ramoAtividade.chavePrimaria}"/>" <c:if test="${imovelUnidadeConsumidoraVO.idRamoAtividade == ramoAtividade.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${ramoAtividade.descricao}"/>
			</option>		
		</c:forEach>
	</select>
	<label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloQuantidade" for="quantidadeEconomia"><span class="campoObrigatorioSimbolo2">* </span>Quantidade:</label>
	<input class="campoTexto" type="text" id="quantidadeEconomia" name="quantidadeEconomia" onkeypress="return formatarCampoInteiro(event);" maxlength="3" size="2" value="${imovelUnidadeConsumidoraVO.quantidadeEconomia}">
	<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar Unidades Consumidoras.</p>
</fieldset>

<fieldset class="conteinerBotoesAba"> 
	<input class="bottonRightCol2" name="botaoLimparUnidadeConsumidora" id="botaoLimparUnidadeConsumidora" value="Limpar" type="button" onclick="limparUnidadeConsumidora();">
	<input class="bottonRightCol2" name="botaoIncluirUnidadeConsumidora" id="botaoIncluirUnidadeConsumidora" value="Adicionar Unidade Consumidora" type="button" onclick="incluirUnidadeConsumidora(this.form,'<c:out value="${param['actionAdicionarUnidadeConsumidora']}"/>');">
   	<input class="bottonRightCol2" disabled="disabled" name="botaoAlterarUnidadeConsumidora" id="botaoAlterarUnidadeConsumidora" value="Alterar Unidade Consumidora" type="button" onclick="alterarUnidadeConsumidora(this.form,'<c:out value="${param['actionAdicionarUnidadeConsumidora']}"/>');">
</fieldset>


<c:set var="i" value="0" />
<display:table class="dataTableGGAS dataTableAba" name="listaUnidadeConsumidora" sort="list" id="unidadeConsumidora" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	<display:column property="descricaoSegmento" sortable="false"  title="Segmento" />
	<display:column property="descricaoRamoAtividade" sortable="false"  title="Ramo de Atividade" />
	<display:column property="quantidadeEconomia" sortable="false"  title="Quantidade" />
  		
	<display:column style="text-align: center;"> 
		<a href="javascript:exibirAlteracaoUnidadeConsumidora('${i}','${unidadeConsumidora.idSegmento}','${unidadeConsumidora.idRamoAtividade}','${unidadeConsumidora.quantidadeEconomia}');"><img title="Alterar unidade consumidora" alt="Alterar unidade consumidora"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a> 
	</display:column>
	<display:column style="text-align: center;"> 
		<a onclick="return confirm('Deseja excluir a unidade consumidora');" href="javascript:removerUnidadeConsumidora('<c:out value="${param['actionRemoverUnidadeConsumidora']}"/>',<c:out value="${i}"/>);"><img title="Excluir unidade consumidora" alt="Excluir unidade consumidora"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
	</display:column>
	<c:set var="i" value="${i+1}" />
</display:table>

