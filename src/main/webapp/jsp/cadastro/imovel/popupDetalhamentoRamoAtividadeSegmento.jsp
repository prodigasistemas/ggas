<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<!-- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> -->

<h1 class="tituloInterno">Detalhar Ramo Atividade<a class="linkHelp" href="<help:help>/incluiralterarsegmento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Clique na Substitui��o Tribut�ria desejada para detalh�-la</p>

<form:form method="post">
	<script>
		function cancelar() {
			window.close();
		}
	</script>
	
	<input name="acao" type="hidden" id="acao" value="atualizarSegmento">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${segmentoForm.chavePrimaria}">
	<input name="versao" type="hidden" id="versao" value="${segmentoForm.versao}">
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">	
	<input type="hidden" name="idContaContabil" id="idContaContabil"/>
	
	<fieldset id="conteinerSegmento" class="conteinerPesquisarIncluirAlterar">
		<legend class="conteinerBlocoTitulo">Dados Gerais</legend>
		<fieldset class="coluna detalhamentoColunaLarga2">			
			<label class="rotulo" for="descricao">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${segmentoForm.descricao}"/></span><br />
		</fieldset>
		<fieldset id="tabs" style="display: none">
			<ul>
				<li><a href="#dadosFaturamento">Dados de Faturamento</a></li>
				<li><a href="#dadosMedicao">Dados de Medi��o</a></li>		
			</ul>
			<fieldset class="conteinerAba" id="dadosFaturamento">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosFaturamentoPopupRamoAtividadeDetalhar.jsp">
					<jsp:param name="alteracao" value="false"/>
				</jsp:include>
			</fieldset>	
			<fieldset class="conteinerAba" id="dadosMedicao">
				<jsp:include page="/jsp/cadastro/imovel/abaDadosMedicaoPopupRamoAtividadeDetalhar.jsp">
					<jsp:param name="alteracao" value="false"/>
				</jsp:include>
			</fieldset>
		</fieldset>	
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol2" type="button" id="button" value="Cancelar" onClick="cancelar();">
	</fieldset>
	</fieldset>
</form:form>