<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<style>
	#dataPrevisaoEncerramentoObra + .ui-datepicker-trigger {
	    position: relative;
	    top: 31px;
		right: 126px;
	}
</style>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<script>

$(document).ready(function(){
	verificaCepPreenchidocepImovel();

	$("#numeroImovel").removeClass("campoDesabilitado");
	$("#numeroImovel").removeAttr("readonly");



});

function limparIdentificacaoLocalizacao(){	
	 var selectQuadras = document.getElementById("idQuadraImovel");
	 var novaOpcaoQuadra = new Option("Selecione","-1");
	 selectQuadras.options[selectQuadras.length] = novaOpcaoQuadra;
	 
	 var selectQuadraFace = document.getElementById("idQuadraFace");
	 var novaOpcaoQuadraFace = new Option("Selecione","-1");
	 selectQuadraFace.options[selectQuadraFace.length] = novaOpcaoQuadraFace;
	
	document.imovelForm.nome.value = "";
	document.imovelForm.cepImovel.value = "";
	document.imovelForm.chaveCep.value = "";
	document.imovelForm.numeroImovel.value = "";
	document.imovelForm.complementoImovel.value = "";
	document.imovelForm.enderecoReferencia.value = "";
	document.imovelForm.idQuadraImovel.value = "-1";
	document.imovelForm.idQuadraImovel.disabled = true;
	document.imovelForm.idQuadraFace.value = "-1";
	document.imovelForm.idQuadraFace.disabled = true;
	document.imovelForm.numeroLote.value = "";
	document.imovelForm.numeroSublote.value = "";
	document.imovelForm.testadaLote.value = "";
	document.imovelForm.idSituacao.value = "-1";

}

	function preencherDescricaoPontoConsumo(val){
		if(document.getElementById("descricaoPontoConsumo").value==null || document.getElementById("descricaoPontoConsumo").value==''){
			var limiteCampoDescricao = 50;
			var descricaoImovel = document.getElementById("nome").value;
			
			if(descricaoImovel.length > limiteCampoDescricao){
				document.getElementById("descricaoPontoConsumo").value = descricaoImovel.substring(0, limiteCampoDescricao);
			}else{
				document.getElementById("descricaoPontoConsumo").value = descricaoImovel;
			}
		}
	}
	
	function carregarCampoRotaPrevistaPorQuadra(elem){
		var codQuadra = elem.value;
		var rotaSelect = document.getElementById("idRotaPrevista");
		var total = rotaSelect.length;
		var itemsParaLimpar = new Array();
		
		if (codQuadra == -1){
			document.getElementById("idRotaPrevista").disabled = true;
		}else{
			document.getElementById("idRotaPrevista").disabled = false;
		}
		
		for(var x = 0; x < total; x++){
			if (x > 0){
				itemsParaLimpar.push(rotaSelect.options[x]);				
			}
		}
		
		for(var x = 0; x < itemsParaLimpar.length; x++){
			rotaSelect.removeChild(itemsParaLimpar[x]);
		}
		
		if (codQuadra != -1){
	    	AjaxService.listarRotasPorSetorComercialDaQuadra(codQuadra,
	      			{callback: function(listaRota) {            		      		         		
	                	for (key in listaRota){
	                    	var novaOpcao = new Option(listaRota[key], key);
	                    	rotaSelect.options[rotaSelect.length] = novaOpcao;
	                    }
							$("#${param.idCampoCep}").removeClass("campoDesabilitado");
							$("#${param.idCampoCep}").removeAttr("readonly");
							$("#numeroImovel").removeClass("campoDesabilitado");
							$("#numeroImovel").removeAttr("readonly");

	                }, async: false}
	        );			
		}	
	}



</script>

  
<a class="linkHelp" href="<help:help>/abaidentificaoelocalizaocadastrodoimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
<fieldset class="colunaEsq" id="ImovelAbaIdLoc">
	<label class="rotulo" id="rotuloNomeFantasia" for="nome"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
	<input class="campoTexto" type="text" id="nome" name="nome" maxlength="100" size="30" value="${imovelVO.nome}" 
		onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="-1"/>', 'formatarCampoNome(event)');">



	<fieldset class="exibirCep">
		<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
			<jsp:param name="cepObrigatorio" value="true"/>
			<jsp:param name="numeroCep" value="${imovelVO.cepImovel}"/>
			<jsp:param name="idCampoCep" value="cepImovel"/>
			<jsp:param name="idCampoCidade" value="cidadeImovel"/>
			<jsp:param name="idCampoUf" value="ufImovel"/>
			<jsp:param name="idCampoLogradouro" value="logradouroImovel"/>
			<jsp:param name="chaveCep" value="${imovelVO.chaveCep}"/>
		</jsp:include>
	</fieldset>
	<label class="rotulo campoObrigatorio" id="rotuloNumeroImovel" for="numeroImovel"><span class="campoObrigatorioSimbolo">* </span>N�mero:</label>
	<input class="campoTexto" type="text" id="numeroImovel" name="numeroImovel" onkeyup="this.value = this.value.toUpperCase();" onkeyup="this.value = this.value.toUpperCase();" maxlength="5" size="5" value="${imovelVO.numeroImovel}"><br />
	<label class="rotulo" id="rotuloComplemento" for="complementoImovel">Complemento:</label>
	<input class="campoTexto" type="text" id="complementoImovel" name="complementoImovel" maxlength="255" size="30" value="${imovelVO.complementoImovel}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"><br />
	<label class="rotulo" for="enderecoReferencia">Refer�ncia:</label>
	<input class="campoTexto" type="text" id="enderecoReferencia" name="enderecoReferencia" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="30" size="30" value="${imovelVO.enderecoReferencia}"><br />
	<label class="rotulo rotulo2Linhas">Previs�o de Encerramento:</label>
	<input type="text" id="dataPrevisaoEncerramentoObra" name="dataPrevisaoEncerramentoObra" class="campoData" value="${imovelVO.dataPrevisaoEncerramentoObra}"/><br />
	
	<c:if test="${param.alteracao}">
		<label class="rotulo rotulo2Linhas" id="rotuloHabilitado" for="indicadorAlterarImovelCondominioFilho">Alterar im�veis filhos:</label>
		<input class="campoRadio" type="radio" name="indicadorAlterarImovelCondominioFilho" id="indicadorAlterarImovelCondominioFilho1" value="true" <c:if test="${imovelVO.indicadorAlterarImovelCondominioFilho == 'true'}">checked</c:if>><label class="rotuloRadio">Sim</label>
		<input class="campoRadio" type="radio" name="indicadorAlterarImovelCondominioFilho" id="indicadorAlterarImovelCondominioFilho2" value="false" <c:if test="${imovelVO.indicadorAlterarImovelCondominioFilho == 'false'}">checked</c:if>><label class="rotuloRadio">N�o</label>
	</c:if>
	
	<c:if test="${param.alteracao}">
		<label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
	    <input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${imovelVO.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
	   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${imovelVO.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
	</c:if>
	
</fieldset>

<fieldset id="ImovelAbaIdLoc2">

	<label class="rotulo campoObrigatorio" for="idQuadra"><span class="campoObrigatorioSimbolo">* </span>Quadra: </label>
	<select name="idQuadraImovel" id="idQuadraImovel" class="campoSelect" onchange="carregarQuadraFace(this);carregarCampoRotaPrevistaPorQuadra(this);" <c:if test="${empty imovelVO.cepImovel}">disabled="disabled"</c:if>>
		<option value="-1">Selecione</option>
		<c:forEach items="${listaQuadra}" var="quadra">
			<option value="<c:out value="${quadra.chavePrimaria}"/>" <c:if test="${imovelVO.idQuadraImovel == quadra.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${quadra.numeroQuadra}"/>
			</option>		
		</c:forEach>
	</select><br />
	
	<label class="rotulo campoObrigatorio" for="idQuadraFace"><span class="campoObrigatorioSimbolo">* </span>Face da Quadra: </label>
	<select name="idQuadraFace" id="idQuadraFace" class="campoSelect" onchange="carregarSituacaoImovel();carregarQuadraFacePontoConsumo();" <c:if test="${empty imovelVO.idQuadraImovel}">disabled="disabled"</c:if>>
		<option value="-1">Selecione</option>
		<c:forEach items="${listaFacesQuadra}" var="faceQuadra">
			<option value="<c:out value="${faceQuadra.chavePrimaria}"/>" <c:if test="${imovelVO.idQuadraFace == faceQuadra.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${faceQuadra.quadraFaceFormatada}"/>
			</option>		
		</c:forEach>
	</select><br />
		
	<label class="rotulo" id="rotuloNumeroLote" for="numeroLote">N�mero do Lote:</label>
	<input class="campoTexto" type="text" id="numeroLote" name="numeroLote" onkeypress="return formatarCampoInteiro(event);" maxlength="4" size="5" value="${imovelVO.numeroLote}"><br />
	<label class="rotulo" id="rotuloNumeroSublote" for="numeroSublote">N�mero do Sublote:</label>
	<input class="campoTexto" type="text" id="numeroSublote" name="numeroSublote" onkeypress="return formatarCampoInteiro(event);" maxlength="3" size="5" value="${imovelVO.numeroSublote}"><br />
	<label class="rotulo" id="rotuloTestadaLote" for="testadaLote">Testada do Lote:</label>
	<input class="campoTexto" type="text" id="testadaLote" name="testadaLote" onkeypress="return formatarCampoInteiro(event);" maxlength="5" size="5" value="${imovelVO.testadaLote}"><br />
	
	<label class="rotulo campoObrigatorio" for="idSituacao"><span class="campoObrigatorioSimbolo">* </span>Situa��o do Im�vel:</label>
	<select name="idSituacao" id="idSituacao" class="campoSelect">
	 <option value="-1">Selecione</option>
		<c:forEach items="${listaSituacoesImovel}" var="situacao">
			<option value="<c:out value="${situacao.chavePrimaria}"/>" <c:if test="${imovelVO.idSituacao == situacao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${situacao.descricao}"/>
			</option>
		</c:forEach>
	</select><br />
	
	<label class="rotulo">Rota Prevista:</label>
	<select name="idRotaPrevista" id="idRotaPrevista" class="campoSelect">
	 <option value="-1">Selecione</option>
		<c:forEach items="${listaRotaPrevista}" var="rota">
			<option value="<c:out value="${rota.chavePrimaria}"/>" <c:if test="${imovelVO.idRotaPrevista == rota.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${rota.numeroRota}"/>
			</option>
		</c:forEach>
	</select><br />
	
	<label class="rotulo" id="rotuloAgente" for="agente" >Agente:</label>
	<select name="idAgente" id="idAgente" class="campoSelect">
	<option value="-1">Selecione</option>
		<c:forEach items="${listaAgentes}" var="agente">
			<option value="<c:out value="${agente.chavePrimaria}"/>" <c:if test="${imovelVO.idAgente == agente.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${agente.funcionario.nome}"/>
			</option>
		</c:forEach>
	</select><br />
	
</fieldset>
<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Im�veis.</p>

<style>
.disable{
pointer-events:none;
background:grey;
}
</style>
