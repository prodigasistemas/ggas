<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>

	function refresh(){
		window.location.reload(true);
		
	}

	function incluirRamoAtividade() {
		popup = window.open('popupAdicionarRamoAtividade?postBack=true&operacao=incluir','popup','height=500,width=1020,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');	 
	}
	
	function alterarRamoAtividadeInserir(idRamoAtividade) {
		popup = window.open('popupAlterarRamoAtividade?postBack=true&operacao=incluir&idRamoAtividade='+idRamoAtividade, 'popup','height=500,width=1020,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
		
		
	}
	
	function removerRamoAtividadeInserir(idRamoAtividade) {
		document.forms[0].operacao.value = 'incluir';
		$('#idRamoAtividade').val(idRamoAtividade);
		$.ajax({
			type : 'POST',
			url : "removerRamoAtividade",
			data : $("#segmentoForm").serialize(),
			async : false
		}).success(function(data) {
			retornarTela();
		});
		//submeter('segmentoForm', 'removerRamoAtividade?idRamoAtividade='+idRamoAtividade);
		//window.opener.retornarTela();
	}
	
	
	function limparRamoAtividade() {
		
		document.forms[0].descricaoRamoAtividade.value = "";
		document.getElementById('periodicidadeRamoAtividade').value = "-1";	
	}

</script>
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${segmentoForm.chavePrimaria}">
	<input name="descricaoRamoAtividade" type="hidden" id="descricaoRamoAtividade" value="${ramoAtividade.descricao}">
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">
	

	<fieldset class="conteinerBloco">
	
		<input class="bottonRightCol bottonLeftColUltimo" id="botaoIncluirRamoAtividade" name="botaoIncluirRamoAtividade" value="Adicionar" type="button" onclick="incluirRamoAtividade();">	   	
	
	<hr class="linhaSeparadora" />
		
		<fieldset class="conteinerBloco">
			
			<c:set var="i" value="0" />
			<display:table class="dataTableGGAS" name="${sessionScope.listaRamoAtividades}" sort="list" id="ramoAtividade" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			 pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" style="text-align: center; width: 886px" > 
				<display:column property="descricao" sortable="false" title="Descri��o" />			
				<display:column property="periodicidade.descricao" sortable="false" title="Periodicidade"/>	
				<display:column title="Alterar" style="text-align: center; width: 50px">
					<a href="javascript:alterarRamoAtividadeInserir('${ramoAtividade.chavePrimaria}');">
						<img title="Alterar ramo de atividade" alt="Alterar ramo de atividade"  src="<c:url value="/imagens/16x_editar.gif"/>">
					</a> 
				</display:column>			
				<display:column title="Remover" style="text-align: center; width: 60px"> 
					<a onclick="return confirm('Deseja excluir o ramo de atividade?');" href="javascript:removerRamoAtividadeInserir('${ramoAtividade.chavePrimaria}');">
						<img title="Excluir ramo de atividade" alt="Excluir ramo de atividade"  src="<c:url value="/imagens/deletar_x.png"/>">
					</a> 
				</display:column>
				<c:set var="i" value="${i+1}" />
			</display:table>	
		</fieldset>
		
	</fieldset>