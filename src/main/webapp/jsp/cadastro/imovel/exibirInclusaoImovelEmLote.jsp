<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Gerar Im�veis do Condom�nio<a href="<help:help>/inclusodeimveiscondomnios.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados do im�vel e clique em <span class="destaqueOrientacaoInicial">Preencher Dados</span> para gerar a tabela de preenchimento dos dados dos im�veis.
Ao finalizar o preenchimento, clique em <span class="destaqueOrientacaoInicial">Incluir</span> para criar os im�veis relacionados ao condom�nio</p>

<form method="post" action="incluirImoveisEmLote" name="imovelForm" id="imovelForm">
<token:token></token:token>
<script type="text/javascript">

	$(document).ready(function(){
		document.getElementById("quantidadeBloco").focus();
		//Popup informativo sobre o sistema
		$("#pontoConsumoPopup").dialog({
 			autoOpen: false,
 			modal: true,
 			width: 340,
			buttons: {
				"OK" : function() {
					
					
					document.getElementById('idSegmentoPC').value = document.getElementById('idSegmento').value;
					document.getElementById('idRamoAtividadePC').value = document.getElementById('idRamoAtividade').value;
					document.getElementById('idRotaPC').value = document.getElementById('idRota').value;
					
					submeter("imovelForm", "incluirImoveisEmLote");
				},
				"Cancelar" : function() {
					$(this).dialog("close");
				}
			},
			
			draggable: false,
 			resizable: false
 		});
	});

	function exibirPontoConsumoPopup() {
		$("#pontoConsumoPopup").parent().css({position:"absolute"}).end().dialog('open');
		
	    var indicadorRotaObrigatoria = "<c:out value="${indicadorRotaObrigatoria}"/>";
	    if(indicadorRotaObrigatoria == '0'){
	    	$("#spanRotaImovelLote").html('');
	    }

	}

	function cancelar(){
		location.href = '<c:url value="/pesquisarImovel"/>';
	}

	function carregarImoveisEmLote(){
		if(validarNomesTorres()){
			document.getElementById('indicadorBotaoAcionado').value = "true";		
			submeter("imovelForm", "carregarImoveisEmLote");
		}
	}

	function validarNomesTorres(){
		var torres = new Array();
		$("[name='nomeBlocos']").each(function (){torres.push(this.value)})

		var torresUnicas = torres.filter(function(elem, pos, self) {
		    return self.indexOf(elem) == pos;
		});

		if (torres.length > torresUnicas.length){
			alert("Os nomes das torres n�o podem se repetir");
			return false;
		}
		return true;
	}
	
	function alertInclusao(){
		var botaAcionado = document.getElementById('indicadorBotaoAcionado').value;
		
		if(botaAcionado == 'true'){
			exibirPontoConsumoPopup();
			
			var rotaSelect = document.getElementById("idRota");
			var idImovel = document.getElementById("chavePrimaria").value;
			
			AjaxService.listarRotasPorImovelPorSetorComercialDaQuadra(idImovel,
					{callback:function(rotas) {     
// 						$("#idRota > option").remove();
						for (key in rotas){       		
			            	var novaOpcao = new Option(rotas[key], key);
			        		rotaSelect.options[rotaSelect.length] = novaOpcao;
						}
				}, async:false}
			);
			
		}else{
			alert("Clique no bot�o Preencher Dados para informar os campos obrigat�rios.");			
		}
	}
	
	
	function limparFormulario(){
		document.getElementById('quantidadeBloco').value = "";
	    document.getElementById('quantidadeAndar').value = "";
	    document.getElementById('quantidadeApartamentoAndar').value = "";
	    document.getElementById('inicioApartamento').value = "";
	    document.getElementById('incrementoApartamento').value = "";
	    document.getElementById('tipoUnidade').value = "";
	}
	
	function removerApartamento(indice) {
		document.forms[0].indexListaLote.value = indice;
		submeter('imovelForm', 'removerApartamentoImoveisEmLote');
	}
	
	function preencherNomes(){
		var numeroBlocos = document.getElementById("quantidadeBloco");
		var qtdBlocos = numeroBlocos.value;
		
		$('div#conteinerNomeBloco').empty();
		
		for(var i = 0; i < qtdBlocos; i++) {
			var nome = 'nomeBloco' + i;
			var inputNome = '<input class="campoTexto" type="text" id="'+nome+'" name="nomeBlocos" maxlength="20" size="14" style="display: block;text-transform:uppercase" onkeyup="return removerEspacoInicioComLetraMaiuscula(this)">';
			$('div#conteinerNomeBloco').append(inputNome);
		}
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="incluirImoveisEmLote"/>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${imovelVO.chavePrimaria}"/>
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="indexListaLote" type="hidden" id="indexListaLote" value="${imovelLoteVO.indexListaLote}">
<input name="idSegmentoPC" type="hidden" id="idSegmentoPC" value="${imovelLoteVO.idSegmentoPC}">
<input name="idRamoAtividadePC" type="hidden" id="idRamoAtividadePC" value="${imovelLoteVO.idRamoAtividadePC}">
<input name="idRotaPC" type="hidden" id="idRotaPC" value="${imovelLoteVO.idRotaPC}">
<input name="indicadorBotaoAcionado" type="hidden" id="indicadorBotaoAcionado" value="${imovelLoteVO.indicadorBotaoAcionado}">

<fieldset class="conteinerPesquisarIncluirAlterar">
	
	<legend>Dados do Condom�nio</legend>
	<fieldset id="GerarImoveisCondominioDadosClienteFundo" class="conteinerDadosDetalhe">
		<fieldset id="gerarImoveisCondominioCol1" class="coluna">			
			<label class="rotulo" id="rotuloMatriculaImovel">Matr�cula do Im�vel:</label>
			<span class="itemDetalhamento"><c:out value='${imovel.chavePrimaria}'/></span><br />
			<label class="rotulo" id="rotuloNomeImovel">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value='${imovel.nome}'/></span><br />
			<label class="rotulo" id="rotuloNumeroImovel">Logradouro:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value='${imovel.quadraFace.endereco.cep.tipoLogradouro}'/>&nbsp;<c:out value='${imovel.quadraFace.endereco.cep.logradouro}'/></span><br />
			<label class="rotulo" id="rotuloNumeroImovel">N�mero:</label>
			<span class="itemDetalhamento"><c:out value='${imovel.numeroImovel}'/></span>
		</fieldset>
		<fieldset id="gerarImoveisCondominioCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloCidade">Cidade:</label>
			<span id="itemDetalhamentoCidade" class="itemDetalhamento"><c:out value='${imovel.quadraFace.endereco.cep.nomeMunicipio}'/></span><br />
			<label class="rotulo" id="rotuloEstado">Estado:</label>
			<span class="itemDetalhamento"><c:out value='${imovel.quadraFace.endereco.cep.uf}'/></span><br />
			<label class="rotulo" id="rotuloSituacao">Situa��o:</label>
			<span class="itemDetalhamento"><c:out value='${imovel.situacaoImovel.descricao}'/></span>
		</fieldset>
	</fieldset>

	<fieldset id="gerarImoveisCondominioCol3" class="colunaEsq">
		<label class="rotulo campoObrigatorio" id="rotuloNumeroBlocos" for="quantidadeBloco"><span class="campoObrigatorioSimbolo">* </span>N� de Blocos/Torres:</label>
		<input type="hidden" id="numeroBlocos" name="numeroBlocos" value="${imovelVO.quantidadeBloco}">
		<input class="campoTexto" style="width: 40px" type="text" id="quantidadeBloco" name="quantidadeBloco" onkeypress="return formatarCampoInteiro(event);" onblur="preencherNomes();" onchange="preencherNomes();" maxlength="3" value="${imovelVO.quantidadeBloco}">
		<label class="rotulo" id="rotuloNumeroAndares" for="quantidadeAndar">N� de Andares:</label>
		<input type="hidden" id="quantidadeAndar" name="quantidadeAndar" value="${imovelVO.quantidadeAndar}">
		<span class="itemDetalhamento"><c:out value="${imovelVO.quantidadeAndar}" /></span><br/>
		<label class="rotulo" id="rotuloNumeroAptoPorAndar" for="quantidadeApartamentoAndar">N� de Unidades por Andar:</label>
		<input type="hidden" id="quantidadeApartamentoAndar" name="quantidadeApartamentoAndar" value="${imovelVO.quantidadeApartamentoAndar}">
		<span class="itemDetalhamento"><c:out value="${imovelVO.quantidadeApartamentoAndar}" /></span><br/>
		<label class="rotulo" id="total" for="total">TOTAL:</label>
		<span class="itemDetalhamento"><c:out value="${qtdImoveis}" /></span><br/>
	</fieldset>
	
	<fieldset id="gerarImoveisCondominioCol3a" class="coluna">
			<input name="Button" id="botaoPreencherNomes" class="bottonRightCol2" title="Preencher Nomes" value="Preencher Nomes" 
				onclick="preencherNomes();" type="button" style="width: 150px; padding: 0px;"><br />
		<div id="conteinerNomeBloco">
			<c:if test="${imovelLoteVO.nomeBlocos ne null}">
				<c:forEach items="${imovelLoteVO.nomeBlocos}" var="bloco">
					<input class="campoTexto" type="text" name="nomeBlocos" maxlength="10" size="14"   style="display: block;" value="${bloco}" >
				</c:forEach>
			</c:if>
		</div>
	</fieldset>
	
	<fieldset id="gerarImoveisCondominioCol4" class="colunaFinal">
		<c:choose>
		<c:when test="${imovelVO.quantidadeAndar != 1}">
		<label class="rotulo campoObrigatorio" id="rotuloInicioApartamento" for="inicioApartamento"><span class="campoObrigatorioSimbolo">* </span>Iniciado por:</label>
		<input class="campoTexto" type="text" id="inicioApartamento" name="inicioApartamento" onkeypress="return formatarCampoInteiro(event);" maxlength="3" size="1" value="${imovelLoteVO.inicioApartamento}"><br />
		<label class="rotulo campoObrigatorio" id="rotuloIncrementoApartamento" for="incrementoApartamento"><span class="campoObrigatorioSimbolo">* </span>Incremento de:</label>
		<input class="campoTexto" type="text" id="incrementoApartamento" name="incrementoApartamento" onkeypress="return formatarCampoInteiro(event);" maxlength="4" size="1" value="${imovelLoteVO.incrementoApartamento}"><br />
		</c:when>
		<c:when test="${imovelVO.quantidadeApartamentoAndar != 1}">
		<label class="rotulo campoObrigatorio" id="rotuloInicioApartamento" for="inicioApartamento"><span class="campoObrigatorioSimbolo">* </span>Iniciado por:</label>
		<input class="campoTexto" type="text" id="inicioApartamento" name="inicioApartamento" onkeypress="return formatarCampoInteiro(event);" maxlength="3" size="1" value="${imovelLoteVO.inicioApartamento}"><br />
		<label class="rotulo campoObrigatorio" id="rotuloIncrementoApartamento" for="incrementoApartamento"><span class="campoObrigatorioSimbolo">* </span>Incremento de:</label>
		<input class="campoTexto" type="text" id="incrementoApartamento" name="incrementoApartamento" onkeypress="return formatarCampoInteiro(event);" maxlength="4" size="1" value="${imovelLoteVO.incrementoApartamento}"><br />
		</c:when>
		<c:otherwise>
		</c:otherwise>
		</c:choose>
		<label class="rotulo campoObrigatorio" id="rotuloTipoUnidade" for="tipoUnidade"><span class="campoObrigatorioSimbolo">* </span>Tipo da Unidade:</label>
		<input class="campoTexto" type="text" id="tipoUnidade" name="tipoUnidade" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="10" size="6" value="${imovelLoteVO.tipoUnidade}"><br />
		
		<label class="rotulo campoObrigatorio" id="rotuloCompImovel" for="complementoImovel">Complemento:</label>
		<input class="campoTexto" type="text" id="complementoImovel" name="complementoImovel" maxlength="11" size="7" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" value="${imovelVO.complementoImovel}">
		
		<label class="rotulo campoObrigatorio" id="rotuloQtdBanheiro" for="quantidadeBanheiro">Quantidade de banheiro:</label>
		<input class="campoTexto" type="text" id="quantidadeBanheiro" name="quantidadeBanheiro" maxlength="4" onkeypress="return formatarCampoInteiro(event);" size="6" value="${imovelVO.quantidadeBanheiro}"><br />
		
		<label class="rotulo campoObrigatorio" id="rotuloModalidadeUso" for="idModalidadeUso">Modalidade de uso:</label>
		<select name="idModalidadeUso" id="idModalidadeUso" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaModalidadeUso}" var="modalidadeUsoItem">
				<option value="<c:out value="${modalidadeUsoItem.chavePrimaria}"/>" <c:if test="${imovelVO.idModalidadeUso == modalidadeUsoItem.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modalidadeUsoItem.descricao}"/>
				</option>		
			</c:forEach>
		</select><br/>
		
		<label class="rotulo campoObrigatorio" id="rotuloModalidadeUso" for="idAreaConstruidaFaixa">Faixa �rea Construida:</label>
		<select name="idAreaConstruidaFaixa" id="idAreaConstruidaFaixa" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaAreaConstruidaFaixa}" var="areaConstruidaFaixa">
				<option value="<c:out value="${areaConstruidaFaixa.chavePrimaria}"/>" <c:if test="${imovelVO.idAreaConstruidaFaixa == areaConstruidaFaixa.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${areaConstruidaFaixa.menorFaixa}"/> - <c:out value="${areaConstruidaFaixa.maiorFaixa}"/>
				</option>		
			</c:forEach>
		</select>		
		
	</fieldset>
	<fieldset class="conteinerBotoesEsq"> 
	         <input name="Button" id="botaoPreencherDadosImoveis" class="bottonRightCol2 botaoVertical" title="Preencher Dados" value="Preencher Dados" onclick="carregarImoveisEmLote();" type="button">
	</fieldset>
	
    <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Gera��o de Im�veis do Condom�nio.</p>
</fieldset>

<fieldset>
	<c:if test="${listaImovel ne null && not empty listaImovel}">         
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaImovel" sort="list" id="imovel" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			<display:column sortable="false" title="">
		    	Dados do Im�vel:
		    </display:column>
		    <display:column sortable="false" title="N� Sublote">
		    	<input class="campoTexto" type="text" id="numeroSubLoteImovelLote" name="numeroSubLoteImovelLote" onkeypress="return formatarCampoInteiro(event);" maxlength="3" size="1" value="${imovelLoteVO.numeroSubLoteImovelLote[i]}">
		    </display:column>
	     	<display:column sortable="false" title="N� Sequencia de Leitura">
		    	<input class="campoTexto" type="text" id="numeroSequenciaLeituraImovelLote" name="numeroSequenciaLeituraImovelLote" onkeypress="return formatarCampoInteiro(event);" maxlength="5" size="5" value="${imovelLoteVO.numeroSequenciaLeituraImovelLote[i]}">
		    </display:column>
		    <display:column sortable="false" title="<span class='campoObrigatorioSimbolo'>* </span>Unidade">
		    	<input class="campoTexto" type="text" id="apartamentoImovelLote" name="apartamentoImovelLote" maxlength="4" size="2" onkeypress="return formatarCampoInteiro(event);" value="${imovelLoteVO.apartamentoImovelLote[i]}">
		    </display:column>
		    <display:column sortable="false" title="<span class='campoObrigatorioSimbolo'>* </span>Blocos/Torres">
		    	<input class="campoTexto" type="text" id="blocoImovelLote" name="blocoImovelLote" maxlength="20" size="8" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" value="${imovelLoteVO.blocoImovelLote[i]}">
		    </display:column>
		    
		    <display:column sortable="false" title="Qtd de banheiro">
		    	<input class="campoTexto" type="text" id="quantidadeBanheiroLote" name="quantidadeBanheiroLote" maxlength="20" size="6" value="${imovelLoteVO.quantidadeBanheiroLote[i]}">
		    </display:column>		    
		    
		    <display:column sortable="false" title="Complemento">
		    	<input class="campoTexto" type="text" id="complementoImovelLote" name="complementoImovelLote" maxlength="11" size="7" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" value="${imovelLoteVO.complementoImovelLote[i]}">
		    </display:column>
		    
		    <display:column sortable="false" title="Modalidade de uso">
		    	<select name="modalidadeUsoLote" id="modalidadeUsoLote" class="campoSelect" style="width: 130px">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaModalidadeUso}" var="modalidadeUsoItem">
				<option value="<c:out value="${modalidadeUsoItem.chavePrimaria}"/>" <c:if test="${imovelLoteVO.modalidadeUsoLote[i] == modalidadeUsoItem.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modalidadeUsoItem.descricao}"/>
				</option>		
			</c:forEach>
			</select>
		    </display:column>		    
		    
		    <display:column sortable="false" title="Faixa de �rea<br/>Constru�da">
		    	<select name="faixaAreaConstruidaImovelLote" id="faixaAreaConstruidaImovelLote" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaAreaConstruidaFaixa}" var="areaConstruidaFaixa">
						<option value="<c:out value="${areaConstruidaFaixa.chavePrimaria}"/>" <c:if test="${imovelLoteVO.faixaAreaConstruidaImovelLote[i] == areaConstruidaFaixa.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${areaConstruidaFaixa.menorFaixa}"/> - <c:out value="${areaConstruidaFaixa.maiorFaixa}"/>
						</option>		
					</c:forEach>
				</select>
		    </display:column>
		    <display:column style="text-align: center;" title="Remover"> 
	       		<a onclick="return confirm('Deseja remover o im�vel?');" href="javascript:removerApartamento(<c:out value="${i}"/>);"><img title="Excluir im�vel" alt="Excluir im�vel"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
	        </display:column>
		    <c:set var="i" value="${i+1}" />
		</display:table>
	</c:if>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparFormulario();">
    <c:choose>
		<c:when test="${qtdImoveis == 0}">
           <input name="button" class="bottonRightCol2 botaoGrande1" value="Incluir" disabled="disabled"  type="submit">
        </c:when>
		<c:otherwise>
			<vacess:vacess param="incluirImoveisEmLote">
	       		
	       		
	     <c:choose>
			<c:when test="${listaPontoConsumo ne null && not empty listaPontoConsumo}">
          		 <input name="button" class="bottonRightCol2 botaoGrande1" value="Incluir" type="submit">
        	</c:when>
			<c:otherwise>
	       		<input name="button" class="bottonRightCol2 botaoGrande1" value="Incluir" type="button" onclick="alertInclusao();" >
			</c:otherwise>
		</c:choose>
	       		
	       		
	       		
	       	</vacess:vacess>       	
		</c:otherwise>
	</c:choose>
</fieldset>

</form>
