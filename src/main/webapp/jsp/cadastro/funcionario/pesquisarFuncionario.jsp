<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Funcion�rio<a href="<help:help>/consultadosfuncionrios.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarFuncionarios" id="funcionarioForm" name="funcionarioForm" ModelAttribute="FuncionarioImpl"> 

	<script language="javascript">
	$(document).ready(function(){
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}		
	});
	
	function alterarFuncionario(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("funcionarioForm", "pesquisarFuncionarios");
	}
	
	
	function limparFormulario(){
		document.funcionarioForm.matricula.value = "";
		document.funcionarioForm.nome.value = "";
		document.funcionarioForm.descricaoCargo.value = "";
		document.funcionarioForm.idEmpresa.value = "-1";		
		document.funcionarioForm.idUnidadeOrganizacional.value = "-1";
	    document.forms[0].habilitado[0].checked = true;
	    
	}
	
	function incluir() {		
		location.href = '<c:url value="/exibirInserirFuncionario"/>';
	}
	
	function removerFuncionario(){
		
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('funcionarioForm', 'removerFuncionario');
			}
	    }
	}
	
	function alterarFuncionario(){
		
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('funcionarioForm', 'exibirAtualizarFuncionario');
	    }
	}
	
	
	
	function detalharFuncionario(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("funcionarioForm", 'exibirDetalhamentoFuncionario');
	}
	
	

	function consultarUnidadesOrganizacionais(obj) {
	     var idEmpresa = obj.options[obj.selectedIndex].value;
	     var selectUnidOrg = document.getElementById("idUnidadeOrganizacional");
	     
	
	     selectUnidOrg.length=0;
	     var novaOpcao = new Option("Selecione","-1");
	     selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
	     
	   	if (idEmpresa != "-1") {
		    AjaxService.consultarUnidadesOrganizacionais(idEmpresa, function(unidadeOrganizacional) {
		    	$('#idUnidadeOrganizacional').prop("disabled", false);
		    	for (key in unidadeOrganizacional){
		            var novaOpcao = new Option(unidadeOrganizacional[key], key);
		               selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
		            }
		        }
		 	);       
		}else{
			$('#idUnidadeOrganizacional').prop("disabled", true); 
		}
	} 
	
	
	function init() {
		if($("#indicador").val()!=""){
		$('#idUnidadeOrganizacional').prop("disabled", false);	
		}
	}

	addLoadEvent(init);
	
	
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarFuncionarios">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="funcionarioCol1" class="coluna">
			<label class="rotulo" id="rotuloMatricula" for="matricula" >Matr�cula:</label>
			<input class="campoTexto" type="text" id="matricula" name="matricula" maxlength="8" size="8" value="${funcionario.matricula}" onkeyup="letraMaiuscula(this);" onkeypress="return formatarCampoAlfaNumericoSemCaracteresEspeciais(event)"><br />
			<label class="rotulo" id="rotuloNome" for="nome" >Nome:</label>
			<input class="campoTexto" type="text" id="nome" name="nome" maxlength="50" size="40" value="${funcionario.nome}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
			<label class="rotulo" id="rotuloDescricaoCargo" for="descricaoCargo" >Descri��o do Cargo:</label>
			<input class="campoTexto" type="text" id="descricaoCargo" name="descricaoCargo" maxlength="50" size="40" value="${funcionario.descricaoCargo}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		</fieldset>
		
		<fieldset id="funcionarioCol2" class="colunaFinal">
			<label class="rotulo rotuloHorizontal" id="rotuloEmpresa" for="idEmpresa">Empresa:</label>
			<select name="empresa" id="idEmpresa" class="campoSelect" onchange="consultarUnidadesOrganizacionais(this);">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${empresas}" var="empresa">
					<option value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${funcionario.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${empresa.cliente.nome}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
		    <label class="rotulo rotulo2Linhas"  id="rotuloUnidadeOrganizacional" for="idUnidadeOrganizacional">Unidade Organizacional:</label>
			<select name="unidadeOrganizacional"  id="idUnidadeOrganizacional" class="campoSelect"  disabled="disabled">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadeOrganizacional}" var="unidadeOrganizacional">
					<option value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>" <c:if test="${funcionario.unidadeOrganizacional.chavePrimaria == unidadeOrganizacional.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidadeOrganizacional.descricao}"/>
					</option>		
			    </c:forEach>		
		    </select>
		    <label class="rotulo" for="habilitado">Indicador de Uso:</label>
		    <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDir">
	    		<input name="ButtonPesquisar" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" onclick="submeter('funcionarioForm', 'pesquisarFuncionarios');">
			<%--<vacess:vacess param="pesquisarFuncionarios">--%>
	    	<%--</vacess:vacess>--%>		
			<input name="ButtonLimpar" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
		
	<c:if test="${funcionarios ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="funcionarios" sort="list" id="funcionario" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarFuncionarios">
		      <display:column media="html" style="text-align: center;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		  <input type="checkbox" name="chavesPrimarias" value="${funcionario.chavePrimaria}">
	     	  </display:column>
	     	  <display:column title="Ativo">
		     	<c:choose>
					<c:when test="${funcionario.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
		      <display:column sortProperty="matricula" sortable="true" titleKey="FUNCIONARIO_MATRICULA" >
		      	<a href='javascript:detalharFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'>
						<c:out value='${funcionario.matricula}'/>
					</a>
		      </display:column> 
		      <display:column sortable="true" titleKey="FUNCIONARIO_NOME" sortProperty="nome" > 
		    		<a href='javascript:detalharFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'>
						<c:out value='${funcionario.nome}'/>
					</a>
		      </display:column>
		      <display:column sortProperty="descricaoCargo" style="width: 30px" sortable="true" titleKey="FUNCIONARIO_DESCRICAO_CARGO"> 
		    		<a href='javascript:detalharFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'>
						<c:out value='${funcionario.descricaoCargo}'/>
					</a>
		      </display:column>
		      <display:column sortProperty="empresa.cliente.nome" sortable="true" title="Empresa" > 
		    		<a href='javascript:detalharFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'>
						<c:out value='${funcionario.empresa.cliente.nome}'/>
					</a>
		      </display:column>
		      <display:column sortProperty="unidadeOrganizacional.descricao" sortable="true" title="Unidade" > 
		    		<a href='javascript:detalharFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'>
						<c:out value='${funcionario.unidadeOrganizacional.descricao}'/>
					</a>
		      </display:column>
		      <display:column sortProperty="codigoDDD" sortable="false" titleKey="FUNCIONARIO_CODIGODDD"> 
		    		<a href='javascript:detalharFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'>
						<c:if test="${not empty funcionario.codigoDDD}">
							<c:out value="${funcionario.codigoDDD}"/>
						</c:if>
					</a>
		      </display:column>
			  <display:column sortProperty="fone" sortable="false" titleKey="FUNCIONARIO_FONE"> 
		    		<a href='javascript:detalharFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'>
						<c:if test="${not empty funcionario.fone}">
							<c:out value="${funcionario.fone}"/>
						</c:if>
					</a>
		      </display:column>
		</display:table>		
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty funcionarios}">
				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarFuncionario()" type="button">
			<%--<vacess:vacess param="exibirAtualizarFuncionario">--%>
			<%--</vacess:vacess>--%>
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerFuncionario()" type="button">
			<%--<vacess:vacess param="removerFuncionario">--%>
			<%--</vacess:vacess>--%>
		</c:if>
			<input name="buttonIncluir" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir();" type="button">
		<%--<vacess:vacess param="exibirInserirFuncionario">--%>
		<%--</vacess:vacess>--%>
	</fieldset>
	
</form:form>