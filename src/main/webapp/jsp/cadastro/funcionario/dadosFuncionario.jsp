<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script language="javascript">	
	function selecionarFuncionario(idSelecionado){
		var idFuncionario = document.getElementById("<c:out value='${param.idCampoIdFuncionario}' default=""/>");
		var nomeCompletoFuncionario = document.getElementById("<c:out value='${param.idCampoNomeFuncionario}' default=""/>");
		var empresaFuncionario = document.getElementById("<c:out value='${param.idCampoNomeEmpresa}' default=""/>");
		var matriculaFuncionario = document.getElementById("<c:out value='${param.idCampoMatricula}' default=""/>");
		
		if(idSelecionado != '') {				
			AjaxService.obterFuncionarioPorChave( idSelecionado, {
	           	callback: function(funcionario) {	           		
	           		if(funcionario != null){  	           			        		      		         		
		               	idFuncionario.value = funcionario["chavePrimaria"];
		               	matriculaFuncionario.value = funcionario["matricula"];
		               	nomeCompletoFuncionario.value = funcionario["nome"];
		               	empresaFuncionario.value = funcionario["nomeEmpresa"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idFuncionario.value = "";
        	nomeCompletoFuncionario.value = "";
        	matriculaFuncionario.value = "";
        	empresaFuncionario.value = "";
       	}
        
        document.getElementById("nomeFuncionarioTexto").value = nomeCompletoFuncionario.value;
        document.getElementById("matriculaFuncionarioTexto").value = matriculaFuncionario.value;
        document.getElementById("empresaFuncionarioTexto").value = empresaFuncionario.value;
	}
	
	function exibirPopupPesquisaFuncionario() {			
			popup = window.open('exibirPesquisaFuncionarioPopup');
	}

	function limparFormularioDadosFuncionario(){
		document.getElementById('<c:out value='${param.idCampoIdFuncionario}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoNomeFuncionario}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoNomeEmpresa}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoMatricula}' default=""/>').value = "";
        document.getElementById('nomeFuncionarioTexto').value = "";
        document.getElementById('matriculaFuncionarioTexto').value = "";
        document.getElementById('empresaFuncionarioTexto').value = "";
	}

	function desabilitarPesquisaFuncionario(){
		document.getElementById('botaoPesquisarFuncionario').disabled = true;
	}
	
	function habilitarPesquisaFuncionario(){
		document.getElementById('botaoPesquisarFuncionario').disabled = false;
	}

</script>


<legend>Pesquisar Funcion�rio</legend>
<div class="pesquisarClienteFundo">
	<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Funcion�rio</span> para selecionar o Funcion�rio.</p>
	<input name="<c:out value='${param.idCampoIdFuncionario}' default=""/>" type="hidden" id="<c:out value='${param.idCampoIdFuncionario}' default=""/>" value="${param.idFuncionario}">
	<input name="<c:out value='${param.idCampoNomeFuncionario}' default=""/>" type="hidden" id="<c:out value='${param.idCampoNomeFuncionario}' default=""/>" value="${param.nomeFuncionario}">
	<input name="<c:out value='${param.idCampoNomeEmpresa}' default=""/>" type="hidden" id="<c:out value='${param.idCampoNomeEmpresa}' default=""/>" value="${param.idCampoNomeEmpresa}">
	<input name="<c:out value='${param.idCampoMatricula}' default=""/>" type="hidden" id="<c:out value='${param.idCampoMatricula}' default=""/>" value="${param.matricula}">
	
	<input name="Button" id="botaoPesquisarFuncionario" class="bottonRightCol2" title="Pesquisar Funcion�rio"  value="Pesquisar Funcion�rio" onclick="exibirPopupPesquisaFuncionario();" type="button"><br />
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloCliente" for="nomeClienteTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>?Funcion�rio:</label>
	
	<input class="campoDesabilitado" type="text" id="nomeFuncionarioTexto" name="nomeFuncionarioTexto"  maxlength="50" size="50" disabled="disabled" value="${param.nomeFuncionario}"><br />
	
	<input class="campoDesabilitado" type="text" id="matriculaFuncionarioTexto" name="matriculaFuncionarioTexto"  maxlength="18" size="18" disabled="disabled" value="${param.matricula}"><br />	
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>Endere�o:</label>
	<textarea class="campoDesabilitado" id="empresaFuncionarioTexto" name="enderecoFormatadoClienteTexto" rows="2" cols="37" disabled="disabled">${param.enderecoFormatadoCliente}</textarea><br />
	<label class="rotulo <c:if test="${param.dadosClienteObrigatorios}">campoObrigatorio</c:if>" id="rotuloEmailClienteTexto" for="emailClienteTexto"><c:if test="${param.dadosClienteObrigatorios}"><span class="campoObrigatorioSimbolo2">* </span></c:if>E-mail:</label>
	<input class="campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${param.emailCliente}"><br />
	
</div>