<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script language="javascript">

	$(document).ready(function(){
	
		//Coloca o cursor no primeiro campo de pesquisa
		$("#nomeFuncionario").focus();
	
		/*Executa a pesquisa quando a tecla ENTER � pressionada, 
		caso o cursor esteja em um dos campos do formul�rio*/
		$(':text').keypress(function(event) {
			if (event.keyCode == '13') {
				pesquisarFuncionario();
			}
	   	});
	
	});
	
	function selecionarFuncionario(idSelecionado) {
		window.opener.selecionarFuncionario(idSelecionado);
		window.close();
	}
	
	function limparDados(){
		document.getElementById('nomeFuncionario').value = '';
		document.getElementById('matricula').value = '';
		document.getElementById('idEmpresa').value = '-1';
	}
	
	function pesquisarFuncionario() {
		$("#botaoPesquisar").attr('disabled','disabled');
		submeter('pesquisaFuncionarioForm', 'pesquisarFuncionarioPopup');
	}
	
	function init() {
		<c:if test="${funcionariosEncontrados ne null}">
			$.scrollTo($('#funcionario'),800);
		</c:if>
	}
	addLoadEvent(init);

</script>

<h1 class="tituloInternoPopup">Pesquisar Funcion�rio<a href="<help:help>/consultandofuncionrio.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicialPopup">Para pesquisar um funcion�rio, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form:form method="post" id="pesquisaFuncionarioForm" name="pesquisaFuncionarioForm" action="pesquisarFuncionarioPopup"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarFuncionarioPopup">
	<input name="idSelecionado" type="hidden" id="idSelecionado" value="">
	
	<fieldset id="pesquisarFuncionarioPopup">
		<label class="rotulo" id="rotuloNomeFuncionario" for="nomeFuncionario">Nome do Funcion�rio:</label>
		<input class="campoTexto" type="text" id="nomeFuncionario" name="nome" maxlength="50" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${pesquisaFuncionarioForm.map.nomeFuncionario}"><br />
		
		<label class="rotulo" id="rotuloMatricula" for="matricula">Matricula:</label>
		<input class="campoTexto" type="text" id="matricula" name="matricula" onkeypress="return formatarCampoInteiro(event)" maxlength="50" size="50" value="${funcionario.matricula}"><br />
		
		<label class="rotulo" for="idEmpresa">Empresa:</label>
		<select name="empresa" id="idEmpresa" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaEmpresas}" var="empresa">
				<option value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${funcionario.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${empresa.cliente.nome}"/>
				</option>		
			</c:forEach>
		</select>
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input class="bottonRightCol2" value="Limpar" type="button" onclick="limparDados();">
	    
	    <input id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Pesquisar" type="button" onclick="pesquisarFuncionario()">
	    
	 </fieldset>
	
	<c:if test="${funcionariosEncontrados ne null}">
		<display:table class="dataTableGGAS dataTablePopup" name="funcionariosEncontrados" sort="list" id="funcionario" pagesize="15" >
			<display:column style="text-align: left; width: 40%" sortable="true" titleKey="FUNCIONARIO_NOME" sortProperty="nome">  
				<a href='javascript:selecionarFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'><c:out value='${funcionario.nome}'/></a>
			</display:column>
			<display:column style="width: 20%" sortable="true" titleKey="FUNCIONARIO_MATRICULA" sortProperty="nome">  
				<a href='javascript:selecionarFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'><c:out value='${funcionario.matricula}'/></a>
			</display:column>
			<display:column style="text-align: left; width: 40%" sortable="true" titleKey="FUNCIONARIO_EMPRESA" sortProperty="nome">  
				<a href='javascript:selecionarFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);'><c:out value='${funcionario.empresa.cliente.nome}'/></a>
			</display:column>
		</display:table>   
	</c:if>
	<a name="pesquisaFuncionarioResultados"></a>
</form:form> 
