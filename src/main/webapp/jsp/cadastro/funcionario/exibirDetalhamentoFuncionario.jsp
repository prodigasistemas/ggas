<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<h1 class="tituloInterno">Detalhar Funcion�rio<a href="<help:help>/cadastrodofuncionriodetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form method="post" name="funcionarioForm" id="funcionarioForm" action="exibirDetalhamentoFuncionario" enctype="multipart/form-data">

<script>
	
	$().ready(function(){
		$("#observacaoPopup").dialog({
			autoOpen: false,				
			width: 333,
			modal: true,
			minHeight: 90,
			resizable: false
		});
	});
	
	function voltar(){	
		submeter('funcionarioForm', 'pesquisarFuncionarios');
	}
	
	function alterar(){
		submeter('funcionarioForm', 'exibirAtualizarFuncionario');
	}
	
	function exibirObservacaoAfastamento(observacao) {
		$("#observacaoPopupDetalhamento").val(observacao);
		exibirJDialog("#observacaoPopup");
	}

	function closePopup(){
		$("#observacaoPopup").dialog('close');
	}	
	
</script>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${funcionario.chavePrimaria}">
<fieldset class="detalhamento" id="exibirDetalhamentoFuncionario">

	<div id="observacaoPopup" title="Observa��o">
		<textarea id="observacaoPopupDetalhamento" name="observacaoPopupDetalhamento" rows="5" cols="41" maxlength="200" style="margin-top: 0px; margin-left: 5px" disabled="disable"></textarea><br/>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Fechar" type="button" onclick="closePopup()">
	</div>
	
		<fieldset id="exibirFotoFuncionario" class="coluna">
			<h1 style="font-size: 16px; font-weight: bold; text-align:center; ">Foto do Funcion�rio</h1> <br/>
			<c:choose>
				<c:when test="${not empty funcionario.fotoFuncionario}">
					<img
						style="width: 216px; height: 216px; margin: auto; display: block;"
						src="<c:url 
					value='/exibirFotoFuncionario/${funcionario.chavePrimaria}'
				/>" />
					</br>
					</br>
				</c:when>

				<c:otherwise>
					<img
						style="width: 216px; height: 216px; margin: auto; display: block;"
						src="<c:url value="/imagens/no-image.jpg"/>" />
				</c:otherwise>
			</c:choose>

			</br></br></br>		
		</fieldset>	


	<fieldset id="funcionarioCol1" class="colunaFunc" style="padding-left:100px;">
		<label class="rotulo" id="rotuloMatricula">Matr�cula:</label>
		<span class="itemDetalhamento"><c:out value="${funcionario.matricula}"/></span><br />
		
		<label class="rotulo" id="rotuloNome">Nome:</label>
		<span class="itemDetalhamentoFunc"><c:out value="${funcionario.nome}"/></span><br />
	
		<label class="rotulo" id="rotuloDescricaoCargo">Descri��o do Cargo:</label>
		<span class="itemDetalhamentoFunc"><c:out value="${funcionario.descricaoCargo}"/></span><br />
		
		<label class="rotulo" id="rotuloCodigoDDD">DDD:</label>
		<span class="itemDetalhamento"><c:out value="${funcionario.codigoDDD}"/></span><br />

		<label class="rotulo" id="rotuloTelefone">Telefone:</label>
		<span class="itemDetalhamento"><c:out value="${funcionario.fone}"/></span><br />
		
		<label class="rotulo" id="rotuloEmail">Email:</label>
		<span class="itemDetalhamentoFuncCol2"><c:out value="${funcionario.email}"/></span><br />
	
		<label class="rotulo" id="rotuloEmpresa">Empresa:</label>		
		<span class="itemDetalhamentoFuncCol2"><c:out value="${funcionario.empresa.cliente.nome}"/></span><br />
	
		<label class="rotulo rotulo2Linhas" id="rotuloUnidadeOrganizacional">Unidade Organizacional:</label>
		<span class="itemDetalhamentoFuncCol2"><c:out value="${funcionario.unidadeOrganizacional.descricao}"/></span><br />
		
		<label class="rotulo">Fiscal:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:if test="${funcionario.indicadorFiscal eq 'true'}">Sim</c:if>
			<c:if test="${funcionario.indicadorFiscal eq 'false'}">N�o</c:if>
		</span>
			
		<label class="rotulo">Vendedor:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:if test="${funcionario.indicadorVendedor eq 'true'}">Sim</c:if>
			<c:if test="${funcionario.indicadorVendedor eq 'false'}">N�o</c:if>
		</span>
				
	</fieldset>
	
	<fieldset id="gridAfastamentos" class="conteinerBloco" style="width: 95.8%; padding-top:50px;">
		<jsp:include page="/jsp/cadastro/funcionario/gridAfastamentoFuncionario.jsp"></jsp:include>
	</fieldset>			
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="ButtonVoltar" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    <%--<vacess:vacess param="exibirAtualizarFuncionario">--%>    
    	<input name="buttonAlterar" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
    <%--</vacess:vacess>--%>
</fieldset>
</form:form>
