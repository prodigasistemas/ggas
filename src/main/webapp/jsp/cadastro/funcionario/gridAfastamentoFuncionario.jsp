<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 
<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:set var="indexAfastamento" value="0" />
 <display:table class="dataTableGGAS" sort="list" name="sessionScope.listaAfastamento" id="afastamento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

	          <display:column sortable="false"  title="Motivo Afastamento" style="text-align: center;">
	            	<c:out value="${afastamento.motivoAfastamento.descricao}"/>
	        </display:column>
	        <display:column  title="Data Inicio" sortable="false"  style="text-align: center; width: 100px ">
	        	<fmt:formatDate value="${afastamento.dataInicioAfastamento}" pattern="dd/MM/yyyy"/>
	        </display:column>
	        <display:column  title="Data Fim" sortable="false"  style="text-align: center; width: 100px ">
	        	    <fmt:formatDate value="${afastamento.dataFimAfastamento}" pattern="dd/MM/yyyy"/>
	        </display:column>
	        <display:column title="Observa��o" sortable="false" style="text-align: center;  width: 50px">
	            	<a href="javascript:exibirObservacaoAfastamento('<c:out value='${afastamento.observacao}'/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>					
	        </display:column>
	        <c:if test="${fluxoDetalhamento eq null}" >
	          <display:column sortable="false" style="text-align: center; ">
	           	<a href='javascript:carregarParaAlterarAfastamento("${indexAfastamento}",JSON.stringify(${afastamento.afastamentoJSON}));'><span class="linkInvisivel"></span>
	            	<img id="alterarAfastamento" title="Alterar Afastamento" alt="Alterar Afastamento"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0">
	            	</a>
	        </display:column>
	          <display:column sortable="false" style="text-align: center;  ">
	           	<a onclick="return confirm('Deseja excluir a pergunta?');" href="javascript:excluirAfastamento(<c:out value='${indexAfastamento}'/>);"><span class="linkInvisivel"></span>
	            	<img title="Excluir pergunta" alt="Excluir Afastamento"  src="<c:url value="/imagens/deletar_x.png"/>" border="0">
	            </a>
	        </display:column>
	        </c:if>
	        <c:set var="indexPergunta" value="${indexPergunta+1}" />
</display:table>

