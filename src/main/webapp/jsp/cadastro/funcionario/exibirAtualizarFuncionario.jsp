<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js"></script>

<script>

$(document).ready(function(){
	var botaoAlterarAfastamento = document.getElementById("botaoAlterarAfastamento");
	botaoAlterarAfastamento.disabled = true;
	
	var datepicker = $.fn.datepicker.noConflict();
	$.fn.bootstrapDP = datepicker;  
	$('.bootstrapDP').bootstrapDP({
	    autoclose: true,
		format: 'dd/mm/yyyy',
		language: 'pt-BR'
	});

	$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});
	
	
	$("#observacaoPopup").dialog({
		autoOpen: false,				
		width: 333,
		modal: true,
		minHeight: 90,
		resizable: false
	});
	
});

function limparFormulario(){
	document.funcionarioForm.matricula.value = "";
	document.funcionarioForm.nome.value = "";
	document.funcionarioForm.descricaoCargo.value = "";
	document.funcionarioForm.email.value = "";
	document.funcionarioForm.idEmpresa.value = "-1";	
	document.getElementById('idUnidadeOrganizacional').value = "-1";	
	document.funcionarioForm.codigoDDD.value = "";
	document.funcionarioForm.telefone.value = "";
}

function cancelar() {
	submeter('funcionarioForm', 'pesquisarFuncionarios');
}

function consultarUnidadesOrganizacionais(obj) {
     var idEmpresa = obj.options[obj.selectedIndex].value;
     var selectUnidOrg = document.getElementById("idUnidadeOrganizacional");
 
     selectUnidOrg.length=0;
     if (idEmpresa != "-1") {
             AjaxService.consultarUnidadesOrganizacionais(idEmpresa, 
                  function(unidadeOrganizacional) {
                         for (key in unidadeOrganizacional){
                              var novaOpcao = new Option(unidadeOrganizacional[key], key);
                              selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
                          }
                         });
     } else {
           var novaOpcao = new Option(" ","-1");
           selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
     }   
}

function consultarUnidadesOrganizacionais(obj) {
     var idEmpresa = obj.options[obj.selectedIndex].value;
     var selectUnidOrg = document.getElementById("idUnidadeOrganizacional");
 
     selectUnidOrg.length=0;
     var novaOpcao = new Option("Selecione","-1");
     selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
     
     if (idEmpresa != "-1") {
	     AjaxService.consultarUnidadesOrganizacionais(idEmpresa, 
	     	function(unidadeOrganizacional) {
	        	for (key in unidadeOrganizacional){
	            	var novaOpcao = new Option(unidadeOrganizacional[key], key);
	                selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
	            }
	        }
	     );
	 }            
}

window.onload = function manipularAsteristicoMatricula(){
	
	var exigeMatricula = "<c:out value='${ exigeMatriculaFuncionario }'/>";	
	
	if(exigeMatricula == 'false'){
		document.getElementById("rotuloMatricula").className = "rotulo";	 			
	}
	
	
}

function removerFotoFuncionario(chave) {
	document.forms[0].chavePrimaria.value = chave;
	document.forms[0].removaFotoFuncionario.value = true;
	submeter('funcionarioForm', 'exibirAtualizarFuncionario');
}

function adicionarAfastamento(){
	var motivoAfastamento = document.getElementById("motivoAfastamento").value;
	var dataInicio = document.getElementById("dataInicio").value;
	var dataFim = document.getElementById("dataFim").value;
	var observacao = document.getElementById("observacao").value;
	
	if(dataInicio == "") {
		alert("Necess�rio inserir pelo menos a data in�cio do afastamento!");
	} else {
		var indexList;
		if(document.forms[0].indexList.value==null){
			indexList = "0";
		}else{
			indexList = document.forms[0].indexList.value;
		}
		
		var url = "carregarAdicionarAfastamentoFuncionario?motivoAfastamento="+motivoAfastamento
				  +"&"+"dataInicio="+dataInicio
				  +"&"+"dataFim="+dataFim
				  +"&"+"observacao="+observacao
				  +"&"+"indexList="+indexList;
			
			
		
		 var isSucesso = carregarFragmento('gridAfastamentos',url);
		 
		 if(isSucesso){ 
			document.getElementById("dataInicio").value ='';
			document.getElementById("dataFim").value=''; 
			document.getElementById("observacao").value ='';
		 	document.getElementById("indexList").value = null;
		 	document.getElementById("botaoAlterarAfastamento").disabled=true;
	    	document.getElementById("botaoAdicionar").disabled=false;
	    	$('#motivoAfastamento option[value=-1]').attr('selected','selected');
		 }
		
	}

 
}


function carregarParaAlterarAfastamento(indexPergunta, afastamentoJSON){

	var afastamento = JSON.parse(afastamentoJSON);

	var botaoAlterarAfastamento = document.getElementById("botaoAlterarAfastamento");
	botaoAlterarAfastamento.disabled = false;
	
	var botaoAdicionar = document.getElementById("botaoAdicionar");
	botaoAdicionar.disabled = true;
	
	$('#motivoAfastamento option[value='+afastamento.motivoAfastamento+']').attr('selected','selected');
	$('#dataInicio').val(afastamento.dataInicio);
	$('#dataFim').val(afastamento.dataFim);
	$('#observacao').val(afastamento.observacao);

	
	document.forms[0].indexList.value = indexPergunta;
	
}

function excluirAfastamento(valorChave){
	carregarFragmento('gridAfastamentos', "removerAfastamento?indexAfastamento="+valorChave);
}

function exibirObservacaoAfastamento(observacao) {
	$("#observacaoPopupDetalhamento").val(observacao);
	exibirJDialog("#observacaoPopup");
}

function closePopup(){
	$("#observacaoPopup").dialog('close');
}

</script>

<h1 class="tituloInterno">Alterar Funcion�rio<a href="<help:help>/cadastrodefuncionrioinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar um Funcion�rio, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>
<form:form method="post" action="atualizarFuncionario" id="funcionarioForm" name="funcionarioForm" ModelAttribute="FuncionarioImpl" enctype="multipart/form-data"> 

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${funcionario.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${funcionario.versao}">
<input name="removaFotoFuncionario" type="hidden" id="removaFotoFuncionario" value="null">
<input type="hidden" name="indexList" id="indexList" >


	<div id="observacaoPopup" title="Observa��o">
		<textarea id="observacaoPopupDetalhamento" name="observacaoPopupDetalhamento" rows="5" cols="41" maxlength="200" style="margin-top: 0px; margin-left: 5px" disabled="disable"></textarea><br/>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Fechar" type="button" onclick="closePopup()">
	</div>

<fieldset class="conteinerPesquisarIncluirAlterar">

		<fieldset id="exibirFotoFuncionario" class="coluna">
			<h1 style="font-size: 16px; font-weight: bold; text-align:center; ">Foto do Funcion�rio</h1> <br/>
			<c:choose>
				<c:when test="${not empty funcionario.fotoFuncionario}">
					<img
						style="width: 216px; height: 216px; margin: auto; display: block;"
						src="<c:url 
					value='/exibirFotoFuncionario/${funcionario.chavePrimaria}'
				/>" />
					</br>
					</br>
					<input type="button" value="Remover Foto" class="bottonRightCol"
						id="removerLogotipoButton"
						onclick="removerFotoFuncionario(<c:out value='${funcionario.chavePrimaria}'/>);"
						style="margin: auto; display: block; float: none !important;">
				</c:when>

				<c:otherwise>
					<img
						style="width: 216px; height: 216px; margin: auto; display: block;"
						src="<c:url value="/imagens/no-image.jpg"/>" />
				</c:otherwise>
			</c:choose>

			</br></br></br></br>
			<p class="orientacaoInterna"
				style="padding-left: 4em; background-position: 2em 2px;">
				No campo <span class="destaqueOrientacaoInicial">Foto</span> insira
				arquivos no(s) formato(s) ${formatosAceitosFotoFuncionario} apenas.
			</p>
			<label class="rotulo" id="rotuloLogo" for="fotoFuncionario">Foto
				Funcionario:</label> <input class="campoFile" type="file"
				id="fotoFuncionario" name="fotoFuncionario" title="Procurar"
				style="font-size: 10pt;" /> <input type="button"
				value="Limpar Arquivo" class="bottonRightCol2" id="limparArquivo"
				style="width: 110px; padding: 4px; text-align: left; font-size: 10pt;">
				
			</br></br></br>		
		</fieldset>

		<fieldset id="funcionarioCol1" class="colunaFinal">
			<label class="rotulo campoObrigatorio" id="rotuloMatricula"
				for="matricula"><c:if
					test="${ exigeMatriculaFuncionario eq true }">
					<span class="campoObrigatorioSimbolo">* </span>
				</c:if>Matr�cula:</label> <input class="campoTexto" type="text" id="matricula"
				name="matricula" maxlength="8" size="8"
				value="${funcionario.matricula}" onkeyup="letraMaiuscula(this);"
				onkeypress="return formatarCampoAlfaNumericoSemCaracteresEspeciais(event)"><br />
			<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span
				class="campoObrigatorioSimbolo">* </span>Nome:</label> <input
				class="campoTexto" type="text" id="nome" name="nome" maxlength="50"
				size="50" value="${funcionario.nome}"
				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
			<label class="rotulo campoObrigatorio" id="rotuloDescricaoCargo"
				for="descricaoCargo"><span class="campoObrigatorioSimbolo">*
			</span>Descri��o do Cargo:</label> <input class="campoTexto" type="text"
				id="descricaoCargo" name="descricaoCargo" maxlength="50" size="50"
				value="${funcionario.descricaoCargo}"
				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
			<label class="rotulo" id="rotuloCodigoDDD" for="codigoDDD">DDD:</label>
			<input class="campoTexto" type="text" id="codigoDDD" name="codigoDDD"
				maxlength="2" size="2" value="${funcionario.codigoDDD}"
				onkeypress="return formatarCampoInteiro(event)"><br /> <label
				class="rotulo" id="rotuloTelefone" for="telefone">Telefone:</label>
			<input class="campoTexto" type="text" id="telefone" name="fone"
				maxlength="9" size="9" value="${funcionario.fone}"
				onkeypress="return formatarCampoInteiro(event)"> <label
				class="rotulo " id="rotuloEmail" for="email">Email:</label> <input
				class="campoTexto campoTextoGrande" type="text" id="email"
				name="email" maxlength="80" size="37"
				onkeypress="return formatarCampoEmail(event);"
				value="${funcionario.email}"><br /> <label
				class="rotulo campoObrigatorio" id="rotuloEmpresa" for="idEmpresa"><span
				class="campoObrigatorioSimbolo">* </span>Empresa:</label> <select
				name="empresa" id="idEmpresa" class="campoSelect"
				onchange="consultarUnidadesOrganizacionais(this);">
				<option value="-1">Selecione</option>
				<c:forEach items="${empresas}" var="empresa">
					<option value="<c:out value="${empresa.chavePrimaria}"/>"
						<c:if test="${funcionario.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${empresa.cliente.nome}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo rotulo2Linhas"
				id="rotuloUnidadeOrganizacional" for="idUnidadeOrganizacional"><span
				class="campoObrigatorioSimbolo">* </span>Unidade Organizacional:</label> <select
				name="unidadeOrganizacional" id="idUnidadeOrganizacional"
				class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadeOrganizacional}"
					var="unidadeOrganizacional">
					<option
						value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>"
						<c:if test="${funcionario.unidadeOrganizacional.chavePrimaria == unidadeOrganizacional.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidadeOrganizacional.descricao}" />
					</option>
				</c:forEach>
			</select><br /> <label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador
				de Uso:</label> <input class="campoRadio" type="radio" name="habilitado"
				id="habilitado1" value="true"
				<c:if test="${funcionario.habilitado == 'true'}">checked</c:if>><label
				class="rotuloRadio">Ativo</label> <input class="campoRadio"
				type="radio" name="habilitado" id="habilitado2" value="false"
				<c:if test="${funcionario.habilitado == 'false'}">checked</c:if>><label
				class="rotuloRadio">Inativo</label> <label class="rotulo">Fiscal:</label>
			<input class="campoRadio" type="radio" name="indicadorFiscal"
				id="indicadorFiscal1" value="true"
				<c:if test="${funcionario.indicadorFiscal == 'true'}">checked</c:if>><label
				class="rotuloRadio">Sim</label> <input class="campoRadio"
				type="radio" name="indicadorFiscal" id="indicadorFiscal2"
				value="false"
				<c:if test="${funcionario.indicadorFiscal == 'false'}">checked</c:if>><label
				class="rotuloRadio">N�o</label> <label class="rotulo">Vendedor:</label>
			<input class="campoRadio" type="radio" name="indicadorVendedor"
				id="indicadorVendedor1" value="true"
				<c:if test="${funcionario.indicadorVendedor == 'true'}">checked</c:if>><label
				class="rotuloRadio">Sim</label> <input class="campoRadio"
				type="radio" name="indicadorVendedor" id="indicadorVendedor2"
				value="false"
				<c:if test="${funcionario.indicadorVendedor == 'false'}">checked</c:if>><label
				class="rotuloRadio">N�o</label>

		</fieldset>



		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Altera��o dos dados do Funcion�rio.</p>
		
	     <fieldset class="pesquisarChamadoAssunto"  style="float: left; margin:100px 0 20px 0">
		       	<h1 style="font-size: 20px; font-weight: bold; ">Afastamentos</h1> <br/>
		       	
		       	<fieldset class="colunaFinal">
			       	<label class="rotulo" id="rotuloMotivoAfastamento" for="motivoAfastamento" ><span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Motivo Afastamento:</label>
			     	<select name="motivoAfastamento" class="campoSelect" id="motivoAfastamento" style="margin-top: 6px">
		                <c:forEach items="${motivosAfastamento}" var="motivoAfastamento">
		                    <option value="<c:out value="${motivoAfastamento.chavePrimaria}"/>">
		                        <c:out value="${motivoAfastamento.descricao}"/>
		                    </option>       
		                </c:forEach>    
		            </select><br />		       	
			       	<label class="rotulo2Linhas" id="rotuloDataInicio"> <span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Data Inicio</label>
			     	<input class="bootstrapDP" type="text"  name="dataInicio" id="dataInicio"  value="" style="margin-top: 5px" size="20" maxlength="200"/>
			     	
			     	<br />
			       	<label class="rotulo2Linhas" id="rotuloDataFim">Data Fim</label>
			     	<input class="bootstrapDP" type="text"  name="dataFim" id="dataFim"  value="" style="margin-top: 5px; margin-left: 58px"/>
			     	
			     	<br />
			     	<label class="rotulo2Linhas" id="rotuloObservacao">Observa��o</label>
			     	<textarea id="observacao" name="observacao" rows="5" cols="41" maxlength="200" style="margin-top: 0px; margin-left: 5px"></textarea><br/>
			     	

			       	<br />  	
					<hr class="linhaSeparadoraPesquisa" />
					
					<fieldset class="conteinerBotoesPesquisarDir">
			   			<input name="Button" class="bottonRightCol2" id="botaoAdicionar" value="Adicionar Afastamento" type="button" onclick="adicionarAfastamento()">
			   			<input name="Button" class="bottonRightCol2" id="botaoAlterarAfastamento" value="Alterar Afastamento" type="button" onclick="adicionarAfastamento()">
					</fieldset>
				</fieldset>	       
	      </fieldset>
	      
	       <fieldset id="gridAfastamentos" class="conteinerBloco" style="width: 95.8%">
	       		<jsp:include page="/jsp/cadastro/funcionario/gridAfastamentoFuncionario.jsp"></jsp:include>
	       </fieldset>		
</fieldset>

	

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <%--<vacess:vacess param="atualizarFuncionario">--%>
    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit">
    <%--</vacess:vacess>--%>
</fieldset>
</form:form> 