<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">
$(document).ready(function(){

	var datepicker = $.fn.datepicker.noConflict();
	$.fn.bootstrapDP = datepicker;  
	$('.bootstrapDP').bootstrapDP({
	    autoclose: true,
		format: 'yyyy',
		language: 'pt-BR',
	    viewMode: "years", 
	    minViewMode: "years"
	});

	$('.bootstrapDP').inputmask("9999",{placeholder:"_"});
	
    const monthOrder = {
        "janeiro": 1, "fevereiro": 2, "mar�o": 3, "abril": 4, "maio": 5, "junho": 6,
        "julho": 7, "agosto": 8, "setembro": 9, "outubro": 10, "novembro": 11, "dezembro": 12
    };

    $.fn.dataTable.ext.type.order['month-pre'] = function(d) {
        return monthOrder[d.toLowerCase()] || 0;
    };

    $('#redeComprimento').DataTable({
        "pageLength": 12,
        "lengthMenu": [[12, 25, 50, -1], [12, 25, 50, "Todos"]],
        "paging": false,        
        "info": false,          
        "searching": false,     
        "ordering": true,      
        "columnDefs": [
            { type: "month", targets: 0 }
        ],
        "order": [[0, "asc"]]   
    });

    $('.comprimentoEscala').each(function() {
        aplicarMascaraNumeroDecimal(this, 2);
    });
    
});

function gerarAno() {
	submeter('redeComprimentoForm','gerarRedeComprimento');
}

function salvarRedeComprimento() {
	submeter('redeComprimentoForm','salvarRedeComprimento');
}

</script>


<div class="bootstrap">
	<form:form action="exibirGerarRedeComprimento" id="redeComprimentoForm"
		name="redeComprimentoForm" method="post" modelAttribute="">
		<input type="hidden" name="anoGerar" value="${ano}" />

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Comprimento de Rede</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i>Selecionar um ano e clique em
					<b>Gerar</b> para exibir os m�ses daquele ano para inclus�o do
					comprimento, preencha e clique em <b>Salvar</b> para salvar o
					comprimento de rede.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">
						<div class="form-row align-items-center">
							<div class="col-mb-0">
								<label for="funcionario" class="mb-0">Ano:<span
									class="text-danger">*</span></label>
							</div>
							<div class="col-md-3">
								<div class="input-group input-group-sm col-sm-12 pl-1 pr-0"
									style="padding-left: 0px !important;">
									<input class="form-control form-control-sm bootstrapDP"
										type="text" id="ano" name="ano"
										maxlength="10" value="${ano}">
								</div>
							</div>
							<div class="col-md-3">
								<button class="btn btn-primary btn-sm" id="atualizar"
									type="button" onclick="gerarAno();">Gerar</button>
							</div>
						</div>
					</div>
					
					<c:if test="${listaComprimentoRede ne null}">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="redeComprimento" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center">M�s</th>
										<th scope="col" class="text-center">Comprimento</th>
									</tr>
								</thead>
								<tbody>
									<c:set var="i" value="0" />
									<c:forEach items="${listaComprimentoRede}" var="redeComprimento">
										<tr>
											<td class="text-center">
											    <fmt:parseDate value="${redeComprimento.referencia}01" pattern="yyyyMMd" var="dataFormatada" />
											    <fmt:formatDate value="${dataFormatada}" pattern="MMMM" />
											</td>
											<td class="text-center">
												<input
													onkeypress="return formatarCampoDecimal(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
													class="form-control comprimentoEscala" type="text"
													id="comprimento_${i}" name="comprimento"
													maxlength="13" size="13"
													value="<fmt:formatNumber value="${redeComprimento.comprimento}" type="number" maxFractionDigits="2" groupingUsed="false" />"/>											
											</td>
										</tr>
									<c:set var="i" value="${i + 1}" />
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoSalvar"
									type="button" onclick="salvarRedeComprimento();">Salvar</button>
								</button>
							</div>
						</div>							
					</c:if>					
					
				</div>
			</div>
		</div>
	</form:form>
</div>

