<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	
        
<h1 class="tituloInterno">Pesquisar Esta��o<a class="linkHelp" href="<help:help>/consultadasestaes.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarErp" id="erpForm" name="erpForm">

	<script>
	
		function removerErp(){
			
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('erpForm', 'removerErp');
				}
		    }
		}
	
		function limpar(){
			document.erpForm.nome.value = "";
			document.erpForm.idLocalidade.value = "-1";			
			document.forms[0].indicadorMedicao[2].checked = true;
			document.forms[0].indicadorPressao[2].checked = true;
			document.forms[0].habilitado[0].checked = true;
		}
		
		function alterarErp() {
		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('erpForm', 'exibirAtualizacaoErp');
		    }
		}	
	
		function incluir() {		
			location.href = '<c:url value="/exibirInserirErp"/>';
		}
		
		function detalharErp(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("erpForm", "exibirDetalhamentoErp");
		}
	
	</script>

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarERPCol1" class="coluna">
			<label class="rotulo" id="rotuloNome" for="nome" >Nome:</label>
			<input class="campoTexto" type="text" name="nome" id="nome" maxlength="40" size="40" value="${erp.nome}" onblur="validarCampoArrobaPonto(this)" onkeyup="validarCampoArrobaPonto(this)" onkeypress="return formatarCampoArrobaPonto(event);"> <br />
			<label class="rotulo" id="rotuloLocalidade" for="idLocalidade">Localidade:</label>
			<select name="localidade" id="idLocalidade" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaLocalidade}" var="localidade">
					<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${erp.localidade.chavePrimaria eq localidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${localidade.descricao}"/>
					</option>		
			    </c:forEach>	
			</select>
		</fieldset>
		
		<fieldset id="pesquisarERPCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloMedicao" for="indicadorMedicao">Medi��o:</label>
			<input class="campoRadio" type="radio" name="indicadorMedicao" id="indicadorMedicaoSim" value="true" <c:if test="${indicadorMedicao == true}">checked="checked"</c:if>><label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorMedicao" id="indicadorMedicaoNao" value="false" <c:if test="${indicadorMedicao == false}">checked="checked"</c:if>><label class="rotuloRadio">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorMedicao" id="indicadorMedicaoTodos" value="" <c:if test="${empty indicadorMedicao}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorMedicaoTodos">Todos</label><br />
			
			<label class="rotulo" id="rotuloPressao" for="indicadorPressao">Regulagem de Press�o:</label>
			<input class="campoRadio" type="radio" name="indicadorPressao" id="indicadorPressaoSim" value="true" <c:if test="${indicadorPressao eq true}">checked="checked"</c:if>><label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorPressao" id="indicadorPressaoNao" value="false" <c:if test="${indicadorPressao eq 'false'}">checked="checked"</c:if>><label class="rotuloRadio">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorPressao" id="indicadorPressaoTodos" value="" <c:if test="${empty indicadorPressao}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorPressaoTodos">Todos</label><br />
			
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq true}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq false}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDir">
			<vacess:vacess param="exibirPesquisaErp">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limpar();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaErp ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaErp" sort="list" id="erp" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarErp">
	        <display:column style="text-align: center;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${erp.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo">
		     	<c:choose>
					<c:when test="${erp.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	        <display:column sortable="true" title="Nome">        
	            <a href="javascript:detalharErp(<c:out value='${erp.chavePrimaria}'/>);">
	            	<c:out value="${erp.nome}"/>
	            </a>
	        </display:column>      
	        <display:column property="localidade.descricao" sortable="true" title="Localidade" />
	        <display:column sortable="true" title="Medi��o"> 
		        <c:choose>
		        	<c:when test="${erp.medicao}">Sim</c:when>
		        	<c:otherwise>N�o</c:otherwise>
		        </c:choose> 
		   </display:column>
		   <display:column sortable="true" title="Press�o"> 
		        <c:choose>
		        	<c:when test="${erp.pressao}">Sim</c:when>
		        	<c:otherwise>N�o</c:otherwise>
		        </c:choose> 
		   </display:column>
		</display:table>	       
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaErp}">
			<vacess:vacess param="exibirAtualizacaoErp">
				<input name="buttonRemover" value="Alterar" class="bottonRightCol2" onclick="alterarErp()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerErp">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerErp();" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInserirErp">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>
	<token:token></token:token>
</form:form>