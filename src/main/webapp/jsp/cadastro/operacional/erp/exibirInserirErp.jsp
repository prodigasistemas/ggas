<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<h1 class="tituloInterno">Inserir Esta��o<a class="linkHelp" href="<help:help>/estaoinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form:form method="post" action="inserirErp" id="erpForm" name="erpForm"> 


<script>

	function alterarValorCampoRegulagemPressao(){
		if(document.getElementById("indicadorMedicao1").checked == true){
			document.getElementById("indicadorPressao1").checked = true;
		}
	}

	function limparFormulario(){
		
		document.erpForm.medicao.checked = false;
		document.erpForm.pressao.checked = false;
	
		document.forms[0].nome.value = "";
		document.forms[0].idGerenciaRegional.value = "-1";
		document.forms[0].idLocalidade.value = "-1";
		document.forms[0].idLocalidade.disabled = true;
		document.forms[0].idTroncoAnterior.value = "-1";
		document.forms[0].idTroncoPosterior.value = "-1";
		
		
		document.forms[0].numero.value = "";
		document.forms[0].cep.value = "";
		document.forms[0].chaveCep.value = "";
		document.forms[0].cidade.value = "";
		document.forms[0].uf.value = "";
		var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
		removeAllOptions(selectCep);
		animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
		limparDialog();	
	}

	function carregarLocalidades(elem) {	
		var codGerenciaRegional = elem.value;
      	var selectLocalidades = document.getElementById("idLocalidade");
    	 
      	selectLocalidades.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
        
      	if (codGerenciaRegional != "-1") {
      		selectLocalidades.disabled = false;      		
        	AjaxService.consultarLocalidadesPorGerenciaRegional(codGerenciaRegional, 
            	function(localidades) {            		      		         		
                	for (key in localidades){
                    	var novaOpcao = new Option(localidades[key], key);
                        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
                    }
                }
            );
      	} else {
			selectLocalidades.disabled = true;		
      	}
      	
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}	

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasia");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("indicadorCondominioAmbos");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaErp"/>';
	}
	
</script>

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="ERPCol1" class="coluna">
		<label class="rotulo campoObrigatorio" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input class="campoTexto" type="text" id="nome" name="nome" maxlength="40" size="40" value="${erp.nome}" onblur="validarCampoArrobaPonto(this)" onkeyup="validarCampoArrobaPonto(this)" onkeypress="return formatarCampoArrobaPonto(event);"><br />							
		<label class="rotulo campoObrigatorio" for="idGerenciaRegional"><span class="campoObrigatorioSimbolo">* </span>Ger�ncia Regional:</label>
		<select name="gerenciaRegional" id="idGerenciaRegional" class="campoSelect" onchange="carregarLocalidades(this)" >
			<option value="-1">Selecione</option>
			<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
				<option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${localidade.gerenciaRegional.chavePrimaria eq gerenciaRegional.chavePrimaria or idGerenciaRegional eq gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${gerenciaRegional.nome}"/>
				</option>
			</c:forEach>
		</select><br />
		<label class="rotulo campoObrigatorio" for="idLocalidade"><span class="campoObrigatorioSimbolo">* </span>Localidade:</label>
	   	<select name="localidade" id="idLocalidade" class="campoSelect" <c:if test="${empty listaLocalidade}">disabled="disabled"</c:if>>
			<option value="-1">Selecione</option>
		  	<c:forEach items="${listaLocalidade}" var="localidade">
				<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${erp.localidade.chavePrimaria eq localidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localidade.descricao}"/>
				</option>
	  	   	</c:forEach>
		</select><br />
		<label class="rotulo" for="idTroncoAnterior">Tronco Anterior:</label>
		<select name="troncoAnterior" id="idTroncoAnterior" class="campoSelect">
			<option value="-1">Selecione</option>
		  	<c:forEach items="${listaTronco}" var="troncoAnterior">
				<option value="<c:out value="${troncoAnterior.chavePrimaria}"/>"<c:if test="${erp.troncoAnterior.chavePrimaria eq troncoAnterior.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${troncoAnterior.descricao}"/>
				</option>
	  	   	</c:forEach>
		</select><br />
		<label class="rotulo" for="idTroncoPosterior">Tronco Posterior:</label>
		<select name="troncoPosterior" id="idTroncoPosterior" class="campoSelect">
			<option value="-1">Selecione</option>
		  	<c:forEach items="${listaTronco}" var="troncoPosterior">
				<option value="<c:out value="${troncoPosterior.chavePrimaria}"/>" <c:if test="${erp.troncoPosterior.chavePrimaria eq troncoPosterior.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${troncoPosterior.descricao}"/>
				</option>
	  	   	</c:forEach>
		</select>
		
		<label class="rotulo" for="indicadorMedicao"><span class="campoObrigatorioSimbolo">* </span>Medi��o:</label>
		<input class="campoRadio" type="radio" name="medicao" id="indicadorMedicao1" value="true" <c:if test="${erp.medicao eq true}">checked</c:if> onchange="alterarValorCampoRegulagemPressao()" ><label class="rotuloRadio">Sim</label>
	   	<input class="campoRadio" type="radio" name="medicao" id="indicadorMedicao2" value="false" <c:if test="${erp.medicao eq false or erp.medicao eq ''}">checked</c:if> onchange="alterarValorCampoRegulagemPressao()" ><label class="rotuloRadio">N�o</label>
		<label class="rotulo" for="indicadorPressao"><span class="campoObrigatorioSimbolo">* </span>Regulagem <br/>de Press�o:</label>
		<input class="campoRadio" type="radio" name="pressao" id="indicadorPressao1" value="true" <c:if test="${erp.pressao eq true}">checked</c:if>><label class="rotuloRadio">Sim</label>
	   	<input class="campoRadio" type="radio" name="pressao" id="indicadorPressao2" value="false" <c:if test="${erp.pressao eq false or erp.pressao eq ''}">checked</c:if>><label class="rotuloRadio">N�o</label>
		<fieldset class="exibirCep">
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="idCampoCep" value="cep"/>
				<jsp:param name="cepObrigatorio" value="false"/>
				<jsp:param name="numeroCep" value="${endereco.cep.cep eq null ? numeroCep : endereco.cep.cep}"/>
				<jsp:param name="chaveCep" value="${endereco.cep.chavePrimaria}"/>
			</jsp:include>
		</fieldset>
		<label class="rotulo" for="numero">N�mero:</label>
		<input class="campoTexto" type="text" id="numero" name="numero" maxlength="5" size="5" value="${erp.endereco.numero}" onkeypress="return formatarCampoNumeroEndereco(event, this);"/>
	</fieldset>
	
	<fieldset id="pesquisarImovel" class="colunaFinal">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span> para selecionar um Im�vel.</p>
				<input name="imovel" type="hidden" id="idImovel" value="${erp.imovel.chavePrimaria}">
				<input name="imovel" type="hidden" id="nomeFantasia" value="${erp.imovel.nome}">
				<input name="imovel" type="hidden" id="matriculaImovel" value="${erp.imovel.chavePrimaria}">
				<input name="imovel" type="hidden" id="numeroImovel" value="${erp.imovel.numeroImovel}">
				<input name="imovel" type="hidden" id="cidadeImovel" value="${erp.endereco.cep.nomeMunicipio}">
				<input name="imovel" type="hidden" id="indicadorCondominioAmbos" value="${erp.imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Im�vel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${erp.imovel.nome}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${erp.imovel.chavePrimaria}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${erp.imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${erp.imovel.quadraFace.endereco.cep.nomeMunicipio}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${erp.imovel.condominio eq true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${erp.imovel.condominio eq false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Esta��o.</p>	
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="voltar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="exibirInserirErp">
    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Incluir"  type="submit">
    </vacess:vacess>
</fieldset>
	<token:token></token:token>
</form:form> 