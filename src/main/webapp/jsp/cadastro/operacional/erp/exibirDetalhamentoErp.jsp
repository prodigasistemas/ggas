<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>


<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	

<h1 class="tituloInterno">Detalhar Esta��o<a class="linkHelp" href="<help:help>/estaodetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form method="post" action="exibirDetalhamentoErp" id="erpForm" name="erpForm" enctype="multipart/form-data">

<script>
	
	function voltar(){
		submeter("erpForm", "pesquisarErp");
	}
	
	function alterar(){
		submeter('erpForm', 'exibirAtualizacaoErp');
	}
	
</script>

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${erp.chavePrimaria}">
<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">
<input name="indicadorMedicao" type="hidden" id="indicadorMedicao" value="${indicadorMedicao}">
<input name="indicadorPressao" type="hidden" id="indicadorPressao" value="${indicadorPressao}">
<input name="nome" type="hidden" id="nomePesq" value="${erpForm.nome}">
<input name="localidade" type="hidden" id="localidadePesq" value="${erpForm.localidade.chavePrimaria}">

<fieldset class="detalhamento">
	<fieldset id="detalhamentoERPCol1" class="coluna">
		<label class="rotulo" for="nome">Nome:</label>
		<span id="detalhamentoNome" class="itemDetalhamento"><c:out value="${erp.nome}"/></span><br />
		<label class="rotulo" for="localidade">Localidade:</label>
		<span class="itemDetalhamento"><c:out value="${erp.localidade.descricao}"/></span><br />
		<label class="rotulo" for="troncoAnterior">Tronco Anterior:</label>
		<span class="itemDetalhamento"><c:out value="${erp.troncoAnterior.descricao}"/></span><br />
		<label class="rotulo" for="troncoPosterior">Tronco Posterior:</label>
		<span class="itemDetalhamento"><c:out value="${erp.troncoPosterior.descricao}"/></span><br />
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<c:choose>
			<c:when test="${erp.habilitado == true}">
				<span class="itemDetalhamento"><c:out value="Ativo"/></span><br />
			</c:when>
			<c:otherwise>
				<span class="itemDetalhamento"><c:out value="Inativo"/></span><br />
			</c:otherwise>
		</c:choose>
	</fieldset>
	
	<fieldset id="detalhamentoERPCol2" class="colunaFinal">
		<label class="rotulo" for="indicadorMedicao">Medi��o:</label>
		<c:choose>
	   		<c:when test="${erp.medicao}">
	   			<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
	   		</c:when>
	   		<c:otherwise>
	   			<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
	   		</c:otherwise>
	   	</c:choose>
	   	
	   	<label class="rotulo" for="indicadorPressao">Regulagem de Press�o:</label>
		<c:choose>
	   		<c:when test="${erp.pressao}">
	   			<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
	   		</c:when>
	   		<c:otherwise>
	   			<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
	   		</c:otherwise>
	   	</c:choose> 
		
		<label class="rotulo" for="cep">CEP:</label>
		<span class="itemDetalhamento"><c:out value="${erp.endereco.cep.cep}"/></span><br />
		<label class="rotulo" for="numero">N�mero:</label>
		<span class="itemDetalhamento"><c:out value="${erp.endereco.numero}"/></span><br />
		
		<label class="rotulo" for="cep">Im�vel:</label>
		<span class="itemDetalhamento"><c:out value="${erp.imovel.nome}"/></span><br />
	
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAtualizacaoErp">    
    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>
<token:token></token:token>
</form:form>
