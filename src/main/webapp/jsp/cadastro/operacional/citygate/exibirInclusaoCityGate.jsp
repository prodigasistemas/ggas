<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Incluir City Gate<a href="<help:help>${paginaHelp}</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script language="javascript">

    $(document).ready(function(){

         var max = 0;

          $('.rotulo').each(function(){
                  if ($(this).width() > max)
                     max = $(this).width();   
              });
          $('.rotulo').width(max);
         
    });
    
   function limpar(){
	   limparFormularios(cityGateForm);
   }
    
    function salvar(){

        var tela = obterParametroURL("tela");
        var url = 'incluirCityGate';
        
        submeter('cityGateForm', url);

    }

    function cancelar(){    
    	location.href='exibirPesquisaCityGate';
    }
    
</script>

<form:form method="post" id="cityGateForm" name="cityGateForm" enctype="multipart/form-data" action="incluirCityGate">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="tela" type="hidden" id="tela" value="cityGate">
<input id="habilitado" name="habilitado" type="hidden" value="${true}">

<fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
        
        	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
       		<input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="250" size="30" value="${cityGate.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        
        	<label class="rotulo">Descri��o Abreviada:</label>
        
       		<input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${cityGate.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
             
                <label class="rotulo">Endere�o:</label>
                <input class="campoTexto campoHorizontal" type="text" id="endereco" name="enderecoReferencia" maxlength="30" size="30" value="${endereco.enderecoReferencia}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
                
               <br /><br /><br /><br />
               <label class="rotulo campoObrigatorio" id="rotuloCodigoCnae"><span class="campoObrigatorioSimbolo">* </span>Localidade:</label>
                <select name="localidade" class="campoSelect" id="idLocalidade">
                    <option value="-1">Selecione</option>
                    <c:forEach items="${listaLocalidades}" var="localidade">
                        <option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${cityGate.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
                            <c:out value="${localidade.descricao}"/>
                        </option>       
                    </c:forEach>    
                </select><br />
            
        </fieldset>
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
                <jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp" >
                       <jsp:param name="idCampoCep" value="cep"/>
                       <jsp:param name="cepObrigatorio" value="false"/>
                       <jsp:param name="numeroCep" value="${cep}"/>
                 </jsp:include>
                 
               <label class="rotulo">N�mero do Im�vel:</label>
               <input class="campoTexto campoHorizontal" type="text" id="numeroImovel" name="numero" maxlength="5" size="5" value="${endereco.numero}"  onkeypress="return formatarCampoInteiro(event)"> <br />
               
               <label class="rotulo">Complemento:</label>
               <input class="campoTexto campoHorizontal" type="text" id="complemento" name="complemento" maxlength="255" size="25" value="${endereco.complemento}" >  <br /> 
       </fieldset>
    </fieldset>
        
   	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>

	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limpar();">
	    <input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" id="botaoSalvar" onclick="javascript:salvar();">
	</fieldset>
</form:form>
