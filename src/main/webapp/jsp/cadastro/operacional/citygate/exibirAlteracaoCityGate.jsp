<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Alterar City Gate<a href="<help:help>${paginaHelp}</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>

    $(document).ready(function(){
        var max = 0;
  
        $('.rotulo').each(function(){
                if ($(this).width() > max)
                   max = $(this).width();   
            });
        $('.rotulo').width(max);
    });
    
    function salvar(){
        submeter('cityGateForm', 'alterarCityGate');
    }

    function cancelar(){    
    	location.href='exibirPesquisaCityGate';
    }

    function limparCampos(){
    	 limparFormularios(cityGateForm);
    	 document.cityGateForm.habilitado[0].checked = true;
    }
        
</script>

<form:form method="post" enctype="multipart/form-data" action="alterarCityGate" id="cityGateForm" name="cityGateForm">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="tela" type="hidden" id="tela" value="cityGate">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cityGate.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${cityGate.versao}"> 

<fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
	        <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��oo:</label>
	        <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${cityGate.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
	        <br />

        <label class="rotulo campoObrigatorio">Descri��o Abreviada:</label>
        <input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${cityGate.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        
        <br />
         <label class="rotulo">Endere�o:</label>
         <input class="campoTexto campoHorizontal" type="text" id="endereco" name="enderecoReferencia" maxlength="30" size="30" value="${endereco.enderecoReferencia}" onkeyup="letraMaiuscula(this);"> 
         <label class="rotulo campoObrigatorio" id="rotuloCodigoCnae"><span class="campoObrigatorioSimbolo">* </span>Localidade:</label>
         <select name="localidade" class="campoSelect" id="idLocalidade" >
             <option value="-1">Selecione</option>
             <c:forEach items="${listaLocalidades}" var="localidade">
                 <option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${cityGate.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
                     <c:out value="${localidade.descricao}"/>
                 </option>       
             </c:forEach>    
         </select><br />
    </fieldset>
    
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal colunaFinalAjuste">
           <br />
           <jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
                 <jsp:param name="idCampoCep" value="cep"/>
                 <jsp:param name="cepObrigatorio" value="false"/>
                 <jsp:param name="numeroCep" value="${cep}"/>
           </jsp:include>
            <label class="rotulo">N�mero do Im�vel:</label>
            <input class="campoTexto campoHorizontal" type="text" id="numeroImovel" name="numero" maxlength="5" size="5" value="${endereco.numero}" onkeypress="return formatarCampoInteiro(event)"> <br />
            
            <label class="rotulo">Complemento:</label>
            <input class="campoTexto campoHorizontal" type="text" id="complemento" name="complemento" maxlength="255" size="25" value="${endereco.complemento}" >  <br /> 
        
        <label class="rotulo" for="habilitado">Indicador de Uso:</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${cityGate.habilitado eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Ativo</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${cityGate.habilitado eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Inativo</label><br />
        </fieldset>
    
    <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
</fieldset>
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="javascript:limparCampos();">
    
    <vacess:vacess param="alterarCityGate">
        <input name="button" id="botaoSalvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="salvar();">
    </vacess:vacess>
</fieldset>
</form:form>
