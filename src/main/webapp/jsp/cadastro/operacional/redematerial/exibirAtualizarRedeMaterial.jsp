<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<fmt:setLocale value="pt-BR" />

<h1 class="tituloInterno">
	Alterar Material de Rede <a class="linkHelp"
		href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>

<p class="orientacaoInicial">
	Informe os dados abaixo e clique em <span
		class="destaqueOrientacaoInicial">Salvar</span> para finalizar.
</p>

<script>
	$(document).ready(function(){
		  var max = 0;
	
	  $('.rotulo').each(function(){
	          if ($(this).width() > max)
	             max = $(this).width();   
	      });
	  $('.rotulo').width(max);
		
	});
	
	function limpar() {
		$('#descricao').val("");
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisarRedeMaterial"/>';
	}
</script>

<form:form method="post" action="atualizarRedeMaterial"
	id="redeMaterialForm" name="redeMaterialForm">

	<input id="chavePrimaria" name="chavePrimaria" type="hidden"
		value="${redeMaterial.chavePrimaria}">

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">

			<label class="rotulo campoObrigatorio"><span
				class="campoObrigatorioSimbolo">* </span>Descri��o:</label> <input
				class="campoTexto campoHorizontal" type="text" name="descricao"
				id="descricao" maxlength="20" size="30"
				value="${redeMaterial.descricao}"
				onblur="this.value = removerEspacoInicialFinal(this.value);"
				onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');" />
			<br />

		</fieldset>

		<fieldset id="pesquisarTabelaAuxiliarCol2"
			class="colunaFinal colunaFinalAjuste">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value="true"
				<c:if test="${redeMaterial.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value="false"
				<c:if test="${redeMaterial.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Inativo</label>
		</fieldset>

		<p class="legenda">
			<span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios
		</p>
	</fieldset>

	<fieldset class="conteinerBotoes">
	 	<input id="botaoCancelar" name="Button" class="bottonRightCol2"
			value="Cancelar" type="button" onclick="cancelar();"> <input
			id="limparFormulario" name="Button"
			class="bottonRightCol2 bottonLeftColUltimo" value="Limpar"
			type="button" onclick="limpar();">
			
		<vacess:vacess param="atualizarRedeMaterial">	 
			<input id="botaoSalvar"
				name="button" 
				class="bottonRightCol2 botaoGrande1 botaoSalvar"
				value="Salvar" 
				type="submit" 
				onclick="salvar();">
		</vacess:vacess>
		
	</fieldset>
</form:form>
