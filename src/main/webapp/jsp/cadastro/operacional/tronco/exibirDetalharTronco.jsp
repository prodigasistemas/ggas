<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Detalhar Tronco<a href="<help:help>/consultadostroncos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informações deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>
		$(document).ready(function(){
			  var max = 0;
		
		    $('.rotulo').each(function(){
		            if ($(this).width() > max)
		               max = $(this).width();   
		        });
		    $('.rotulo').width(max);
			
		});

		function voltar(){	

			submeter('troncoForm', 'exibirPesquisaTronco');
		}
		
		function alterar(){
			
			submeter('troncoForm', 'exibirAlteracaoTronco');
		}

		addLoadEvent(init);
</script>

<form:form method="post" enctype="multipart/form-data" id="troncoForm" name="troncoForm">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tronco.chavePrimaria}">
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
		
		<label class="rotulo">Descrição:</label>
		<span class="itemDetalhamento"><c:out value="${tronco.descricao}"/></span>
		<br /><br />
	    
		 <label class="rotulo">City Gate:</label>
	   	 <span class="itemDetalhamento"><c:out value="${tronco.cityGate.descricao}"/></span><br />
   		
	    </fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    
    <vacess:vacess param="exibirAlteracaoTronco">    
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
   
</fieldset>
</form:form>
