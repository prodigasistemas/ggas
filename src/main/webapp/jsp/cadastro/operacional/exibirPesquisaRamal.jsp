<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Pesquisar Ramal<a class="linkHelp" href="<help:help>/consultadosramais.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" action="pesquisarRamal" id="ramalForm" name="ramalForm"> 

	<token:token></token:token>
	
	<script type="text/javascript">
	
	function removerRamais(){
		
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('ramalForm', 'excluirRamais');
			}
	    }
	}
	
	function alterarRamal(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('ramalForm', 'exibirAtualizacaoRamal');
	    }
	}
	
	
	function limparFormulario(){
		document.ramalForm.nome.value = "";
		document.forms[0].idRede.value = "-1";	
		document.ramalForm.habilitado[0].checked = true;
	}
	
	
	function incluir() {		
		location.href = '<c:url value="/exibirInserirRamal"/>';
	}
	
	function detalharRamal(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("ramalForm", "exibirDetalhamentoRamal");
	}
	
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarRamal">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="ramalCol1" class="coluna">
			<label class="rotulo" id="rotuloNome" for="nome">Nome:</label>
			<input class="campoTexto" type="text" name="nome" id="nome" maxlength="40" size="40" value="${ramal.nome}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/><br />
			<label class="rotulo" id="rotuloRede" for="idRede">Rede:</label>
			<select name="rede" id="idRede" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaRede}" var="rede">
					<option value="<c:out value="${rede.chavePrimaria}"/>" <c:if test="${ramal.rede.chavePrimaria == rede.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${rede.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select>
	    </fieldset>
	    
	    <fieldset id="ramalCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloHabilitado" for="habilitado1">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${habilitado == 'true'}">checked</c:if>><label class="rotuloRadio" for="habilitado1">Ativo&nbsp;</label>
		   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${habilitado == 'false'}">checked</c:if>><label class="rotuloRadio" for="habilitado2">Inativo&nbsp;</label>
		   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado3" value="" <c:if test="${empty habilitado}">checked</c:if>><label class="rotuloRadio" for="habilitado3">Todos</label>
		 </fieldset>
		 
		 <fieldset class="conteinerBotoesPesquisarDirFixo">
		 	<vacess:vacess param="pesquisarRamal">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
			
	<c:if test="${listaRamal ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaRamal" sort="list" id="ramal" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarRamal">
	        <display:column style="text-align: center;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${ramal.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo">
		     	<c:choose>
					<c:when test="${ramal.habilitado == true}">
						<img alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	        <display:column sortable="true" titleKey="RAMAL_NOME" sortProperty="nome">
	        	<a href='javascript:detalharRamal(<c:out value='${ramal.chavePrimaria}'/>);'>
	        		<c:out value='${ramal.nome}'/>
	        	</a>
	        </display:column>
	        <display:column property="rede.descricao" sortable="true" titleKey="RAMAL_REDE" />
	    </display:table>  	
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaRamal}">
			<vacess:vacess param="exibirAtualizacaoRamal">
				<input name="buttonRemover" value="Alterar" class="bottonRightCol2" onclick="alterarRamal()" type="button">
			</vacess:vacess>
			<vacess:vacess param="excluirRamais">
				<input name="buttonRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerRamais()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInserirRamal">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>

</form>