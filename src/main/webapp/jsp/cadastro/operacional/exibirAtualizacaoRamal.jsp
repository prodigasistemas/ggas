<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Alterar Ramal<a class="linkHelp" href="<help:help>/ramalinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar este Ramal, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form method="post" name="filtroPesquisaForm" action="pesquisarRamal">
	<input name="nome" type="hidden" value="${filtroPesquisa.nome }" />
	<input name="rede" type="hidden" value="${filtroPesquisa.rede.chavePrimaria }" />
	<input name="habilitado" type="hidden" value="${filtroHabilitado }" />
</form>

<form method="post" action="atualizarRamal"> 

	<input name="filtroNome" type="hidden" value="${filtroPesquisa.nome }" />
	<input name="filtroRede" type="hidden" value="${filtroPesquisa.rede.chavePrimaria }" />
	<input name="filtroHabilitado" type="hidden" value="${filtroHabilitado }" />
	
	<token:token></token:token>
	<script type="text/javascript">
		
		function voltar(){	
			submeter("filtroPesquisaForm", "pesquisarRamal");
		}
		
		function limparFormulario(){
			document.forms[0].nome.value = "";
			document.forms[0].idRede.value = "-1";
		}
		
	</script>

	<input type="hidden" name="acao" id="acao" value="atualizarRamal">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${ramal.chavePrimaria}">
	<input name="versao" type="hidden" id="versao" value="${ramal.versao}">
	<input name="postBack" type="hidden" id="postBack" value="true">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="ramalCol1" class="coluna">
			<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
			<input class="campoTexto" type="text" name="nome" id="nome" maxlength="40" size="40" value="${ramal.nome}"/><br />
			
			<label class="rotulo campoObrigatorio" id="rotuloRede" for="idRede"><span class="campoObrigatorioSimbolo">* </span>Rede:</label>
			<select name="rede" id="idRede" class="campoSelect" style="campoSelect">
	    		<option value="-1">Selecione</option>
				<c:forEach items="${listaRede}" var="rede">
					<option value="<c:out value="${rede.chavePrimaria}"/>" <c:if test="${ramal.rede.chavePrimaria == rede.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${rede.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select>
		</fieldset>
	
		<fieldset id="ramalCol2" class="colunaFinal">		
			<label class="rotulo" id="rotuloHabilitado" for="habilitado1">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${ramal.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio" for="habilitado1">Ativo&nbsp;</label>
		   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${ramal.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio" for="habilitado2">Inativo&nbsp;</label>
		</fieldset>
	</fieldset>

	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="voltar();">
		<input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
		<input name="button" value="Salvar" class="bottonRightCol2 botaoGrande1" type="submit">
	</fieldset>
</form> 