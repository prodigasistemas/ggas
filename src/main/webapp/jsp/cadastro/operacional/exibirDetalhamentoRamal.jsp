<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>


<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	

<h1 class="tituloInterno">Detalhar Ramal<a class="linkHelp" href="<help:help>/cadastrodoramaldetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<form method="post" action="exibirDetalhamentoRamal" id="ramalForm" name="ramalForm" enctype="multipart/form-data">

<input name="nome" type="hidden" value="${filtroPesquisa.nome }" />
<input name="rede" type="hidden" value="${filtroPesquisa.rede.chavePrimaria }" />
<input name="habilitado" type="hidden" value="${filtroHabilitado }" />
<token:token></token:token>

<script>
	
	function voltar(){	
		submeter("ramalForm", "pesquisarRamal");
	}
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter('ramalForm', 'exibirAtualizacaoRamal');
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoRamal">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${ramal.chavePrimaria}">

<fieldset id="detalharRamal" class="detalhamento">
	<fieldset id="ramalCol1" class="coluna">
		<label class="rotulo" for="nome">Nome:</label>
		<span class="itemDetalhamento"><c:out value="${ramal.nome}"/></span><br />
		<label class="rotulo" for="rede">Rede:</label>
		<span class="itemDetalhamento"><c:out value="${ramal.rede.descricao}"/></span><br />
	</fieldset>
	    
	<fieldset class="colunaFinal">
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<c:choose>
			<c:when test="${ramal.habilitado == 'true'}">
				<span class="itemDetalhamento"><c:out value="Ativo"/></span><br />
			</c:when>
			<c:otherwise>
				<span class="itemDetalhamento"><c:out value="Inativo"/></span><br />
			</c:otherwise>
		</c:choose>
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">    
    <input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
</fieldset>
</form>
