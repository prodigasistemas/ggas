<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<h1 class="tituloInterno">Detalhar Localidade<a href="<help:help>/cadastrodalocalidadedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form method="post" action="exibirDetalhamentoLocalidade" enctype="multipart/form-data" id="localidadeForm" name="localidadeForm">

<script>
	
	function voltar(){
		submeter("localidadeForm", "pesquisarLocalidade");
	}
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter('localidadeForm', 'exibirAtualizacaoLocalidade');
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoLocalidade">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${localidadeForm.chavePrimaria}">

<input name="descricao" type="hidden" id="descricao" value="${localidadeForm.descricao}">
<input name="gerenciaRegional" type="hidden" id="gerenciaRegional" value="${localidadeForm.gerenciaRegional.chavePrimaria}">
<input name="unidadeNegocio" type="hidden" id="unidadeNegocio" value="${localidadeForm.unidadeNegocio.chavePrimaria}">
<input name="habilitado" type="hidden" id="habilitado" value="${indicadorDeUSo}">

<input name="filtro" type="hidden" id="filtro" value="${filtro}">
<fieldset class="detalhamento">
	<fieldset id="localidadeCol1" class="coluna">
		<label class="rotulo" for="codigo">C�digo:</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.chavePrimaria}"/></span><br />	
		<label class="rotulo" for="descricao">Descri��o:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${localidadeForm.descricao}"/></span><br />	
		<label class="rotulo" for="numero">N�mero:</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.endereco.numero}"/></span><br />	
		<label class="rotulo" for="cep">Cep:</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.endereco.cep.cep}"/></span><br />	
		<label class="rotulo" for="complemento">Complemento:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${localidadeForm.endereco.complemento}"/></span><br />	
		<label class="rotulo" for="telefone">Telefone:</label>
		<span class="itemDetalhamento"><c:out value="("/><c:out value="${localidadeForm.codigoFoneDDD}"/><c:out value=")"/><c:out value=" "/><c:out value="${localidadeForm.fone}"/></span><br />	
		<label class="rotulo" for="ramal">Ramal:</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.ramalFone}"/></span><br />	
		<label class="rotulo" for="fax">Fax:</label>
		<span class="itemDetalhamento"><c:out value="("/><c:out value="${localidadeForm.codigoFaxDDD}"/><c:out value=")"/><c:out value=" "/><c:out value="${localidadeForm.fax}"/></span><br />	
		<label class="rotulo" for="email">Email:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${localidadeForm.email}"/></span>
	</fieldset>
	
	<fieldset id="localidadeCol2" class="colunaFinal">
		<label class="rotulo" for="idGerenciaRegional">Ger�ncia Regional:</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.gerenciaRegional.nome}"/></span><br />
		<label class="rotulo" for="idUnidadeNegocio">Unidade Neg�cio :</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.unidadeNegocio.descricao}"/></span><br />
		<label class="rotulo" for="idLocalidadeClasse">Classe :</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.classe.descricao}"/></span><br />
		<label class="rotulo" for="idLocalidadePorte">Porte :</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.porte.descricao}"/></span><br />
		<label class="rotulo" for="codigoCentroCusto">Centro de Custo :</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.codigoCentroCusto}"/></span><br />		
		<label class="rotulo" for="informatizada">Informatizada :</label>
		<c:choose>
			<c:when test="${localidadeForm.informatizada == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:otherwise>
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:otherwise>
		</c:choose>		
		<label class="rotulo" for="medidorLocalArmazenagem">Medidor Local Armazenagem :</label>
		<span class="itemDetalhamento"><c:out value="${localidadeForm.medidorLocalArmazenagem.descricaoAbreviada}"/></span><br />
	</fieldset>
	<hr class="linhaSeparadora" />		
	<label class="rotulo rotuloLocalidadeCliente" for="idCliente">Gestor Respons�vel:</label>
	<span class="itemDetalhamento"><c:out value="${localidadeForm.cliente.nome}"/></span><br />
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAtualizacaoLocalidade">    
    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>
</form:form>
