<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Detalhar Rede Externa<a class="linkHelp" href="<help:help>/cadastrodaredeexternadetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form method="post" action="exibirDetalhamentoRede" enctype="multipart/form-data" name="redeForm" id="redeForm" >

<script>
	
	function voltar() {
		submeter('redeForm', 'pesquisarRede');
	}
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter('redeForm', 'exibirAtualizacaoRede');
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoRede">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${rede.chavePrimaria}">
<input name="filtro" type="hidden" id="filtro" value="${filtro}">
<input name="habilitado" type="hidden" value="${habilitado}" />
<input name="descricao" type="hidden" value="${redeForm.descricao}" />
<input name="localidade" type="hidden" value="${redeForm.localidade.chavePrimaria}" />


<fieldset class="detalhamento">
	<fieldset id="detalharRedeCol1" class="coluna">
		<label class="rotulo" for="descricao">Descri��o:</label>
		<span class="itemDetalhamento"><c:out value="${rede.descricao}"/></span><br />
		<label class="rotulo" for="extensao">Extens�o:</label>
		<span class="itemDetalhamento"><c:out value="${rede.extensao}"/>&nbsp;</span>
		<span class="itemDetalhamento"><c:out value="${rede.unidadeExtensao.descricao}"/></span><br />
		<label class="rotulo" for="pressao">Press�o:</label>
		<span class="itemDetalhamento"><c:out value="${rede.valorPressaoFormatado}"/>&nbsp;</span>
		<span class="itemDetalhamento"><c:out value="${rede.unidadePressao.descricao}"/></span><br />
		<label class="rotulo" for="idLocalidade">Localidade:</label>
		<span class="itemDetalhamento"><c:out value="${rede.localidade.descricao}"/></span><br />
	</fieldset>
	
	<fieldset id="detalharRedeCol2" class="colunaFinal">
		<label class="rotulo" for="idRedeDiametro">Di�metro de Rede:</label>
		<span class="itemDetalhamento"><c:out value="${rede.redeDiametro.descricao}"/></span><br />
		<label class="rotulo" for="idRedeMaterial">Material de Rede:</label>
		<span class="itemDetalhamento"><c:out value="${rede.redeMaterial.descricao}"/></span><br />
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
	
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Troncos:</legend>
			<display:table class="dataTableGGAS" name="listaRedeTronco" sort="list" id="redeTronco" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		        <display:column property="tronco.cityGate.descricao" sortable="false"  title="City Gate" >
					<c:forEach items="${listaRedeTronco}" var="redeTronco" >	
						<span class="itemDetalhamento"><c:out value="${redeTronco.tronco.cityGate.descricao}"/></span><br />
					</c:forEach>	
		        </display:column>	
		   		<display:column property="tronco.descricao" sortable="false"  title="Tronco" >
		   		   	<c:forEach items="${listaRedeTronco}" var="redeTronco" >	
		   				<span class="itemDetalhamento"><c:out value="${redeTronco.tronco.descricao}"/></span><br />
		   			</c:forEach>	
		   		</display:column>	 
		    </display:table>
		</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAtualizacaoRede">
    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>
</form>
