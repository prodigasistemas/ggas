<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Detalhar Quadra<a class="linkHelp" href="<help:help>/cadastrodaquadradetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form method="post" action="exibirDetalhamentoQuadra" name="quadraForm"  id="quadraForm" enctype="multipart/form-data">
	
	<script>
		
		function voltar(){
			$('#chavePrimaria').val('');
			submeter("quadraForm", "pesquisarQuadra");
		}
		
		function alterar(){
			document.forms[0].postBack.value = false;
			submeter('quadraForm', 'exibirAtualizacaoQuadra');
		}
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoQuadra">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${quadra.chavePrimaria}" />
	
	<input name="habilitado" type="hidden" value="${habilitado}" />
	<input name="numeroQuadra" id="numeroQuadra" type="hidden" value="${quadraForm.numeroQuadra}" />
	<input name="zeis" id="zeis" type="hidden" value="${quadraFom.zeis.chavePrimaria}" />
	<input name="setorComercial" id="setorComercial" type="hidden" value="${quadraForm.setorComercial.chavePrimaria}" />
	<input name="perfilQuadra" id="perfilQuadra" type="hidden" value="${quadraForm.perfilQuadra.chavePrimaria}" />
	<input name="cep" id="cep" type="hidden" value="${cepTela}" />


	<fieldset class="detalhamento">
		<fieldset class="conteinerBloco">
			<fieldset id="exibirDetalhamentoQuadraCol1" class="coluna">
				<label class="rotulo" for="idGerenciaRegional">Ger�ncia Regional:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.setorComercial.localidade.gerenciaRegional.nome}"/></span><br />
				<label class="rotulo" for="idLocalidade">Localidade:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.setorComercial.localidade.descricao}"/></span><br />
				<label class="rotulo" for="idSetorComercial">Setor Comercial:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.setorComercial.descricao}"/></span><br />
				<label class="rotulo" for="numeroQuadra">Quadra:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.numeroQuadra}"/></span><br />
				<label class="rotulo" for="idPerfilQuadra">Perfil Quadra:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.perfilQuadra.descricao}"/></span><br />
			</fieldset>
		
			<fieldset id="exibirDetalhamentoQuadraCol2" class="colunaFinal">
				<label class="rotulo" for="idZeis">Zeis:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.zeis.descricao}"/></span><br />
				<label class="rotulo" for="idSetorCensitario">Setor Censit�rio:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.setorCensitario.descricao}"/></span><br />
				<label class="rotulo" for="idZonaBloqueio">Zona Bloqueio:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.zonaBloqueio.descricao}"/></span><br />
				<label class="rotulo" for="idTipoArea">Tipo �rea:</label>
				<span class="itemDetalhamento"><c:out value="${quadra.areaTipo.descricao}"/></span><br />
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadoraDetalhamento" />
	
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Faces:</legend>
			<display:table class="dataTableGGAS" name="listaQuadraFace" sort="list" id="quadraFace" pagesize="15" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoQuadra" >
		        <display:column property="endereco.numero" sortable="false"  title="N�mero" >
					<c:forEach items="${listaQuadraFace}" var="quadraFace" >	
						<span class="itemDetalhamento"><c:out value="${quadraFace.endereco.numero}"/></span><br />
					</c:forEach>	
		        </display:column>	
		   		<display:column property="endereco.cep.cep" sortable="false"  title="CEP" >
		   		   	<c:forEach items="${listaQuadraFace}" var="quadraFace" >	
		   				<span class="itemDetalhamento"><c:out value="${quadraFace.endereco.cep.cep}"/></span><br />
		   			</c:forEach>	
		   		</display:column>	 
		   		<display:column property="redeIndicador.descricao" sortable="false"  title="Indicador de rede" >
		   			<c:forEach items="${listaQuadraFace}" var="quadraFace" >	
		   				<span class="itemDetalhamento"><c:out value="${quadraFace.redeIndicador.descricao}"/></span><br />
		   			</c:forEach>	
		   		</display:column>    		
		   		<display:column property="rede.descricao" sortable="false"  title="Rede"> 
		   			<c:forEach items="${listaQuadraFace}" var="quadraFace" >	
		   				<span class="itemDetalhamento"><c:if test="${quadraFace.rede != null}"><c:out value="${quadraFace.rede.descricao}"/></c:if></span><br />
		   			</c:forEach>
		   		</display:column>
		    </display:table>
		</fieldset>
		
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
	        <vacess:vacess param="atualizarQuadra">
	    		<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
	    	</vacess:vacess>	    
	</fieldset>
</form>
