<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<script type="text/javascript">

function exibirOcultarRedes(obj) {
	var naoTemRedeIndicador = '${redeIndicadorNaoTem}';
	exibirOcultarRedesPorIndicador(obj.value, naoTemRedeIndicador);
}

function exibirOcultarRedesPorIndicador(valorIndicador, naoTemRedeIndicador) {	
	if (valorIndicador == naoTemRedeIndicador) {
		document.forms[0].idRede.selectedIndex = 0;
		ocultarElemento('trRedes');
	} else {
		exibirElemento('trRedes');
	}
}
   

function incluirFace(form) {

	document.forms[0].indexLista.value = -1;
	submeter('quadraForm', 'adicionarFaceNaQuadra');
}

 function confirmarAlteracaoImoveis(){
		var result = false; 
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_ALTERAR_IMOVEIS_VINCULADOS_FACE"/>');
		if(retorno == true) {
			result = true;
			
		}
		return result;
}

function submeterQuadraFace(nomeDoForm, acao, target){
	document.forms[nomeDoForm].action = acao;
	if (target != undefined) {
		document.forms[nomeDoForm].target = target;
	} else {
		document.forms[nomeDoForm].target = "";
	}
 	document.forms[nomeDoForm].submit();
 	
}


function alterarFace(form) {
	var radioObj =  document.forms[0].redeIndicador;
	var redeIndicadorNaoTem = '${redeIndicadorNaoTem}';
	var redeInficadorTem ='${redeIndicadorTem}'
	var redeIndicadorTemParcialmente = '${redeIndicadorTemParcialmente}';
	var indicadorAntigo = '${redeIndicador}';
	var indicadorNovo;
	var radioLength = radioObj.length;
	var timeoutID;
	
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked == true) {
			indicadorNovo = radioObj[i].value;
		}
	}
	if (indicadorAntigo == redeIndicadorNaoTem && indicadorNovo == redeIndicadorTemParcialmente ){
		if (confirmarAlteracaoImoveis()){
			submeterQuadraFace('quadraForm', 'adicionarFaceNaQuadra');
		}
		
	}else if (indicadorAntigo == redeInficadorTem && indicadorNovo == redeIndicadorTemParcialmente ){
		if (confirmarAlteracaoImoveis()){
			submeterQuadraFace('quadraForm', 'adicionarFaceNaQuadra');
		}
	}else{
		submeter('quadraForm', 'adicionarFaceNaQuadra');
	}
	
}

function removerFace(indice) {
	document.forms[0].indexLista.value = indice;
	submeter('quadraForm', 'removerFaceNaQuadra');
}

function sleep(milliseconds) {
	  var start = new Date().getTime();
	  for (var i = 0; i < 1e7; i++) {
	    if ((new Date().getTime() - start) > milliseconds){
	      break;
	    }
	  }
}

function limparFace() {
	document.forms[0].indexLista.value = -1;
	document.forms[0].idRede.selectedIndex = 0;
	document.forms[0].cep.value = "";
	document.forms[0].chaveCep.value = "";
	document.forms[0].numeroFace.value = "";
	document.forms[0].redeIndicador[0].checked = true;

	var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
	removeAllOptions(selectCep);
	animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
	limparDialog();	
	
	ocultarElemento('trRedes');
	
	var redeIndicador = '${redeIndicadorNaoTem}';
	var radioObj = document.forms[0].redeIndicador;
	var radioLength = radioObj.length;
	if(radioLength == undefined) {
		radioObj.checked = (radioObj.value == newValue.toString());
	}
	for(var i = 0; i < radioLength; i++) {
		radioObj[i].checked = false;
		if(radioObj[i].value == redeIndicador.toString()) {
			radioObj[i].checked = true;
		}
	}
	
	var botaoAlterarFace = document.getElementById("botaoAlterarFace");
	var botaoLimparFace = document.getElementById("botaoLimparFace");	
	var botaoIncluirFace = document.getElementById("botaoIncluirFace");	
	botaoAlterarFace.disabled = true;
	botaoLimparFace.disabled = false;
	botaoIncluirFace.disabled = false;
		
}

function exibirAlteracaoDaFace(indice,cep,numeroFace,redeIndicador,idRede,chaveCep) {
	if (indice != "") {
		document.forms[0].indexLista.value = indice;
		
		if (cep != "") {
			document.forms[0].cep.value = cep;
		}
		if (numeroFace != "") {
			document.forms[0].numeroFace.value = numeroFace;
		}
		if (chaveCep != "") {
			document.forms[0].chaveCep.value = chaveCep;
		}

		if (redeIndicador != "") {
			var radioObj = document.forms[0].redeIndicador;
			var radioLength = radioObj.length;
			if(radioLength == undefined) {
				radioObj.checked = (radioObj.value == newValue.toString());
			}
			for(var i = 0; i < radioLength; i++) {
				radioObj[i].checked = false;
				if(radioObj[i].value == redeIndicador.toString()) {
				radioObj[i].checked = true;
				}
			}
		}
		var naoTemRedeIndicador = '${redeIndicadorNaoTem}';
		if (idRede != "" && naoTemRedeIndicador != redeIndicador) {
			var tamanho = document.forms[0].idRede.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idRede.options[i];
				if (idRede == opcao.value) {
					opcao.selected = true;
				}
			}
			exibirElemento('trRedes');
		} else {
			ocultarElemento('trRedes');
		}
		
		var botaoAlterarFace = document.getElementById("botaoAlterarFace");
		var botaoLimparFace = document.getElementById("botaoLimparFace");	
		var botaoIncluirFace = document.getElementById("botaoIncluirFace");	
		botaoAlterarFace.disabled = false;
		botaoLimparFace.disabled = false;
		botaoIncluirFace.disabled = true;
	}

}

function onloadQuadraFace() {
	<c:choose>
		<c:when test="${indexLista > -1 && empty param['operacaoLista'] }">
			var botaoAlterarFace = document.getElementById("botaoAlterarFace");
			var botaoLimparFace = document.getElementById("botaoLimparFace");	
			var botaoIncluirFace = document.getElementById("botaoIncluirFace");	
			botaoIncluirFace.disabled = true;
			botaoAlterarFace.disabled = false;
			botaoLimparFace.disabled = false;
			
			var redeIndicador = '${redeIndicador}';
			if (redeIndicador != "") {
				var radioObj = document.forms[0].redeIndicador;
				var radioLength = radioObj.length;
				if(radioLength == undefined) {
					radioObj.checked = (radioObj.value == newValue.toString());
				}
				for(var i = 0; i < radioLength; i++) {
					radioObj[i].checked = false;
					if(radioObj[i].value == redeIndicador.toString()) {
					radioObj[i].checked = true;
					}
				}
			}
			
			var naoTemRedeIndicador = '${redeIndicadorNaoTem}';
			var idRede = '${idRede}';
			if (idRede != "" && naoTemRedeIndicador != redeIndicador) {
			var tamanho = document.forms[0].idRede.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idRede.options[i];
				if (idRede == opcao.value) {
					opcao.selected = true;
				}
			}
				exibirElemento('trRedes');
			}
			
		
		</c:when>
		<c:when test="${sucessoManutencaoLista}">
			limparFace();
		</c:when>
	</c:choose>
}
	function manterDadosTela() {
		var naoTemRedeIndicador = '${redeIndicadorNaoTem}';
		var redeIndicador = '${redeIndicador}';
		exibirOcultarRedesPorIndicador(redeIndicador, naoTemRedeIndicador);
	}

	function init(){
		onloadQuadraFace();
		<c:if test="${sucessoManutencaoLista eq false}">
			manterDadosTela();
		</c:if>
		<c:if test="${sucessoManutencaoLista eq false and erroAlterar}">
			var botaoAlterarFace = document.getElementById("botaoAlterarFace");
			var botaoLimparFace = document.getElementById("botaoLimparFace");
			var botaoIncluirFace = document.getElementById("botaoIncluirFace");
			botaoAlterarFace.disabled = false;
			botaoLimparFace.disabled = false;
			botaoIncluirFace.disabled = true;
		</c:if>


	}

	addLoadEvent(init);
</script>

<fieldset id="exibirAdicaoQuadraFace" class="coluna detalhamentoColunaMedia">
	<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
		<jsp:param name="idCampoCep" value="cep"/>
		<jsp:param name="cepObrigatorio" value="true"/>
		<jsp:param name="numeroCep" value="${cep}"/>
		<jsp:param name="chaveCep" value="${chaveCep}"/>
	</jsp:include>
</fieldset>

<fieldset id="exibirAdicaoQuadraFace2" class="colunaFinal">

	<label class="rotulo campoObrigatorio" for="numeroFace"><span class="campoObrigatorioSimbolo">* </span>N�mero:</label>
	<input class="campoTexto" type="text" name="numeroFace" id="numeroFace" size="7" maxlength="7" value="${numeroFace}" onkeypress="return formatarCampoInteiro(event)"><br />
	<label class="rotulo" for="redeIndicador1">Indicador de rede:</label>
	<input class="campoRadio" name="redeIndicador" id="redeIndicador" value="${redeIndicadorNaoTem}" type="radio" onclick="exibirOcultarRedes(this);" <c:if test="${redeIndicador eq 2 or redeIndicador eq 0 }">checked</c:if>><label class="rotuloRadio">N�o Tem</label>
	<input class="campoRadio" name="redeIndicador" id="redeIndicador" value="${redeIndicadorTem}" type="radio" onclick="exibirOcultarRedes(this);"  <c:if test="${redeIndicador eq 1}">checked</c:if>><label class="rotuloRadio">Tem</label>
	<input class="campoRadio" name="redeIndicador" id="redeIndicador" value="${redeIndicadorTemParcialmente}" type="radio" onclick="exibirOcultarRedes(this);"  <c:if test="${redeIndicador eq 3}">checked</c:if>><label class="rotuloRadio">Tem Parcialmente</label>
	<div id="trRedes" style="display: none;">
		<label class="rotulo" for="idRede">Rede:</label>
		<select id="idRede" name="idRede" class="campoSelect">
			<c:forEach items="${redes}" var="rede">
				<option value="<c:out value="${rede.chavePrimaria}"/>" <c:if test="${idRede == rede.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${rede.descricao}"/>
				</option>		
			</c:forEach>
		</select>
    </div>
</fieldset>	    
	    
<fieldset class="conteinerBotoesInserir2">
   	<input class="bottonRightCol2" name="botaoLimparFace" id="botaoLimparFace" value="Limpar" type="button" onclick="limparFace();">
   	<input class="bottonRightCol2" name="botaoIncluirFace" id="botaoIncluirFace" value="Adicionar face" type="button" onclick="incluirFace(this.form)">
	<input class="bottonRightCol2" disabled="disabled" name="botaoAlterarFace" id="botaoAlterarFace" value="Alterar face" type="button" onclick="alterarFace(this.form);">
</fieldset>

<c:set var="i" value="0" />	
<display:table class="dataTableGGAS" name="listaQuadraFace" sort="list" id="quadraFace" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	<display:column property="endereco.numero" sortable="false" title="N�mero" />
	<display:column property="endereco.cep.cep" sortable="false" title="CEP" /> 
	<display:column property="redeIndicador.descricao" sortable="false" title="Indicador de rede" />
	
	<display:column sortable="false" title="Rede">
		<c:if test="${quadraFace.rede != null}"><c:out value="${quadraFace.rede.descricao}"/></c:if>
	</display:column>
	
	<display:column style="text-align: center;"> 
		<a href="javascript:exibirAlteracaoDaFace('${i}','${quadraFace.endereco.cep.cep}','${quadraFace.endereco.numero}','${quadraFace.redeIndicador.codigo}','${quadraFace.rede.chavePrimaria}','${quadraFace.endereco.cep.chavePrimaria}');"><img title="Alterar face" alt="Alterar face"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a> 
	</display:column>
	
	<display:column style="text-align: center;"> 
		<a onclick="return confirm('Deseja excluir a face?');" href="javascript:removerFace(<c:out value="${i}"/>);"><img title="Exluir face" alt="Exluir face" src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
	</display:column>
	<c:set var="i" value="${i+1}" />
</display:table>