<!--
Copyright (C) <2011> GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

Este arquivo ? parte do GGAS, um sistema de gest?o comercial de Servi?os de Distribui??o de G?s

Este programa ? um software livre; voc? pode redistribu?-lo e/ou
modific?-lo sob os termos de Licen?a P?blica Geral GNU, conforme
publicada pela Free Software Foundation; vers?o 2 da Licen?a.

O GGAS ? distribu?do na expectativa de ser ?til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl?cita de
COMERCIALIZA??O ou de ADEQUA??O A QUALQUER PROP?SITO EM PARTICULAR.
Consulte a Licen?a P?blica Geral GNU para obter mais detalhes.

Voc? deve ter recebido uma c?pia da Licen?a P?blica Geral GNU
junto com este programa; se n?o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js"></script>
<script type="text/javascript"
        src="<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js"></script>

<script type="text/javascript">

    function cepCheckForEnter(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            if (this.onblur) {
                this.onblur();
            }
            return false;
        }
    }

    $(document).ready(function () {
        $("#${param.idCampoCep}").keydown(cepCheckForEnter);

        <c:if test="${not empty param.idCampoCep}">
        $("#${param.idCampoCep}").inputmask("99999-999", {placeholder: "_"});
        </c:if>

        // Dialog
        $("#conferirCEP<c:out value='${param.idCampoCep}' default=""/>").dialog({
            autoOpen: false,
            close: function () {
                $("#linkPesquisarCEP").focus()
            },
            width: 333,
            open: function (event, ui) {
                $("#conferirCEP<c:out value='${param.idCampoCep}' default=""/>").parent().css("z-index", 1042);
            },
            modal: true,
            resizable: false
        });

        // Dialog Link
        $("#linkConferirCEP<c:out value='${param.idCampoCep}' default=''/>").click(function () {
            <c:choose>
            <c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
            var cep = document.getElementById('${param.idCampoCep}');
            </c:when>
            </c:choose>

            if ((cep != undefined) && (cep.value != "")) {
                exibirJDialog("#conferirCEP<c:out value='${param.idCampoCep}' default=""/>");
            } else {
                limparDialog<c:out value="${param.idCampoCep}" default=""/>();
            }
            return false;
        });

        /*Executa a pesquisa quando a tecla ENTER ? pressionada, caso o cursor
        esteja apenas em um dos campos do formul?rio de pesquisa de CEP*/
        $("#pesquisarCepCampos>:text, #pesquisarCepCampos>select").keypress(function (event) {
            if (event.keyCode == '13') {
                consultarCep<c:out value='${param.idCampoCep}' default=""/>();
            }
        });

    });

    function clearOptionsFast(selectObj) {
        var selectParentNode = selectObj.parentNode;
        var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
        selectParentNode.replaceChild(newSelectObj, selectObj);
        return newSelectObj;

    }

    function exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(exibir) {

        <c:choose>
        <c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
        var cep = document.getElementById('${param.idCampoCep}');
		var nomeCampoCep = document.getElementById('${param.idCampoCep}').getAttribute('name');
        
        </c:when>
        </c:choose>

        var imagemConferirCEPAtivo = document.getElementById("imagemConferirCEPAtivo" + '<c:out value="${param.idCampoCep}" default=""/>');
        var imagemConferirCEPInativo = document.getElementById("imagemConferirCEPInativo" + '<c:out value="${param.idCampoCep}" default=""/>');
        var logradouroPopup = document.getElementById("logradouroPopup" + '<c:out value="${param.idCampoCep}" default=""/>');
        var bairroPopup = document.getElementById("bairroPopup" + '<c:out value="${param.idCampoCep}" default=""/>');
        var cidadePopup = document.getElementById("cidadePopup" + '<c:out value="${param.idCampoCep}" default=""/>');
        var ufCepPopup = document.getElementById("ufCepPopup" + '<c:out value="${param.idCampoCep}" default=""/>');
        var chaveCep = document.getElementById("chaveCep");
        var cepAuxiliar = document.getElementById("cepAuxiliar").value;

        var linkConferirCEP = document.getElementById("linkConferirCEP" + '<c:out value="${param.idCampoCep}" default=""/>');

        if (cep != undefined && cep.value[8] != '_' && trim(cep.value) != '') {
            imagemConferirCEPAtivo.src = "imagens/cep_ativo.gif";

    		if(cep.value != cepAuxiliar) {
    			chaveCep.value = "";
    		}
			
            if (chaveCep.value == "") {
                AjaxService.consultarEndereco(cep.value,
                    function (endereco) {
                        if (endereco != null) {
                            var tamanhoCeps = 0;
                            var cepEncontrado;
                            var cepKey;
                            for (key in endereco) {
                                tamanhoCeps++;
                                if (tamanhoCeps > 1) {
                                    break;
                                }
                                cepEncontrado = endereco[key];
                                cepKey = key;
                            }

                            if (tamanhoCeps > 1) {
                                if (exibir) {
                                    alert("O cep possui mais de um endere?o.");
                                }
                                limparDialog<c:out value="${param.idCampoCep}" default=""/>();

                                var ceps = document.getElementById("ceps" + '${param.idCampoCep}');
                                ceps = clearOptionsFast(ceps);

                                for (key in endereco) {

                                    var cepEndereco = endereco[key];
                                    var enderecoFormatado = cepEndereco[1] + ", " + cepEndereco[2] + ", " + cepEndereco[0];
                                    var novaOpcao = new Option(enderecoFormatado, key);
                                    novaOpcao.title = endereco[key];
                                    ceps.options[ceps.length] = novaOpcao;
                                }
                                animatedcollapse.show('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
                                exibirElemento('resultadoPesquisarCep' + '<c:out value='${param.idCampoCep}' default=""/>');
                            } else {
                                chaveCep.value = cepKey;

                                if (cepEncontrado[1].indexOf("null") == -1) {
                                    logradouroPopup.innerHTML = cepEncontrado[1];
                                } else {
                                    logradouroPopup.innerHTML = "";
                                }

                                if (cepEncontrado[2] != null) {
                                    bairroPopup.innerHTML = cepEncontrado[2];
                                } else {
                                    bairroPopup.innerHTML = "";
                                }

                                cidadePopup.innerHTML = cepEncontrado[3];
                                ufCepPopup.innerHTML = cepEncontrado[4];
                                linkConferirCEP.style.display = 'inline';
                                imagemConferirCEPAtivo.style.display = '';
                                imagemConferirCEPInativo.style.display = 'none';
                                cepAuxiliar.value = cepEncontrado[0];
                            }
                               verificarChamadaQuadra(nomeCampoCep, cep);
                        } else {
                            chaveCep.value = "";
                            cepAuxiliar.value = "";
                            limparDialog<c:out value="${param.idCampoCep}" default=""/>();
                            verificarChamadaQuadra(nomeCampoCep, cep);
                        }
                    });
            } else {
                AjaxService.consultarCepPorId(chaveCep.value,
                    function (cepEncontrado) {
                        if (cepEncontrado != null) {
                            if (cepEncontrado[0].indexOf("null") == -1) {
                                logradouroPopup.innerHTML = cepEncontrado[0];
                            } else {
                                logradouroPopup.innerHTML = "";
                            }

                            if (cepEncontrado[1] != null) {
                                bairroPopup.innerHTML = cepEncontrado[1];
                            } else {
                                bairroPopup.innerHTML = "";
                            }

                            cidadePopup.innerHTML = cepEncontrado[2];
                            ufCepPopup.innerHTML = cepEncontrado[3];
                            linkConferirCEP.style.display = 'inline';
                            imagemConferirCEPAtivo.style.display = '';
                            imagemConferirCEPInativo.style.display = 'none';

                        }
                    });
            }
        } else {
            limparDialog<c:out value="${param.idCampoCep}" default=""/>();
        	verificarChamadaQuadra(nomeCampoCep, cep);
        }
    }

    function limparDialog<c:out value="${param.idCampoCep}" default=""/>() {

        $("#logradouroPopup" + '<c:out value="${param.idCampoCep}" default=""/>').html("");
        $("#bairroPopup" + '<c:out value="${param.idCampoCep}" default=""/>').html("");
        $("#cidadePopup" + '<c:out value="${param.idCampoCep}" default=""/>').html("");
        $("#ufCepPopup" + '<c:out value="${param.idCampoCep}" default=""/>').html("");


        $("#linkConferirCEP" + '<c:out value="${param.idCampoCep}" default=""/>').css("display", "none");
        $("#imagemConferirCEPAtivo" + '<c:out value="${param.idCampoCep}" default=""/>').css("display", "none");
        $("#imagemConferirCEPInativo" + '<c:out value="${param.idCampoCep}" default=""/>').css("display", "inline");
        $("#imagemConferirCEPInativo" + '<c:out value="${param.idCampoCep}" default=""/>').css("display", "");

    }

    function limparFormularioComponenteCep<c:out value='${param.idCampoCep}' default=""/>() {

        <c:choose>
        <c:when test="${param.idCampoLogradouro ne '' && param.idCampoLogradouro ne undefined}">
        var logradouro = document.getElementById('${param.idCampoLogradouro}');
        </c:when>
        <c:otherwise>
        var logradouro = document.getElementById("logradouro");
        </c:otherwise>
        </c:choose>

        <c:choose>
        <c:when test="${param.idCampoCidade ne '' && param.idCampoCidade ne undefined}">
        var cidade = document.getElementById('${param.idCampoCidade}');
        </c:when>
        <c:otherwise>
        var cidade = document.getElementById("cidade");
        </c:otherwise>
        </c:choose>

        <c:choose>
        <c:when test="${param.idCampoUf ne '' && param.idCampoUf ne undefined}">
        var uf = document.getElementById('${param.idCampoUf}');
        </c:when>
        <c:otherwise>
        var uf = document.getElementById("uf");
        </c:otherwise>
        </c:choose>

        logradouro.value = "";
        cidade.value = "";
        uf.value = "";
    }

    function selecionarCep<c:out value='${param.idCampoCep}' default=""/>(obj) {

        <c:choose>
        <c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
        var cep = document.getElementById('${param.idCampoCep}');
        </c:when>
        </c:choose>

        document.getElementById("chaveCep").value = obj.options[obj.selectedIndex].value;
        var enderecoSelecionado = obj.options[obj.selectedIndex].text;

        var validadorTipoConsulta = <c:out value='${param.idCampoCep}' default=""/>.name;

        if (cep != undefined) {
            cep.focus();
            cep.value = enderecoSelecionado.split(",")[2].replace(" ", "");
            if(validadorTipoConsulta == 'cepPontosConsumo'){
                carregarQuadras2(cep.value);
            }else if(validadorTipoConsulta == 'cep') {
            	//exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(true);
            }else{
                carregarQuadras(cep.value);
            }

        }
        document.getElementById("cepAuxiliar").value = enderecoSelecionado.split(",")[2].replace(" ", "");

        // Mostrando asterisco para o campo n?mero na aba de faturamento de contrato.
        <c:choose>
        <c:when test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}">
        if (cep != undefined) {
            <c:out value="${param.mostrarAsterisco}"/>(cep.value, ['endFisEntFatNumero']);
        }
        </c:when>
        </c:choose>


    }

    function consultarCep<c:out value='${param.idCampoCep}' default=""/>() {

        <c:choose>
        <c:when test="${param.idCampoLogradouro ne '' && param.idCampoLogradouro ne undefined}">
        var logradouro = document.getElementById('${param.idCampoLogradouro}');
        </c:when>
        <c:otherwise>
        var logradouro = document.getElementById("logradouro");
        </c:otherwise>
        </c:choose>

        <c:choose>
        <c:when test="${param.idCampoCidade ne '' && param.idCampoCidade ne undefined}">
        var cidade = document.getElementById('${param.idCampoCidade}');
        </c:when>
        <c:otherwise>
        var cidade = document.getElementById("cidade");
        </c:otherwise>
        </c:choose>

        <c:choose>
        <c:when test="${param.idCampoUf ne '' && param.idCampoUf ne undefined}">
        var uf = document.getElementById('${param.idCampoUf}');
        </c:when>
        <c:otherwise>
        var uf = document.getElementById("uf");
        </c:otherwise>
        </c:choose>

        var botaoPesquisarCEP = document.getElementById("botaoPesquisarCEP<c:out value='${param.idCampoCep}' default=""/>");
        var ceps = document.getElementById("ceps" + '${param.idCampoCep}');

        logradouro.value = trim(logradouro.value);
        cidade.value = trim(cidade.value);

        if (logradouro.value != "" && cidade.value != ""
            && uf.options[uf.selectedIndex].value != "") {
            ceps = clearOptionsFast(ceps);
            botaoPesquisarCEP.disabled = true;
            exibirElemento("imgProc<c:out value='${param.idCampoCep}' default=""/>");
            var answer = false;
            AjaxService.consultarCep(logradouro.value, cidade.value, uf.options[uf.selectedIndex].value,
                function (cep) {
                    answer = cep;
                    if (answer) {
                        ocultarElemento("imgProc<c:out value='${param.idCampoCep}' default=""/>");
                        botaoPesquisarCEP.disabled = false;
                        var cepslength = 0;
                        ceps.selectedIndex = -1;

                        for (key in cep) {
                            var novaOpcao = new Option(cep[key], key);
                            novaOpcao.title = cep[key];
                            ceps.options[cepslength] = novaOpcao;
                            cepslength++;

                            if (cepslength % 1000 == 0) {
                                alert('A consulta retornou mais do que 1000 registros. Refine mais a consulta para um resultado mais preciso.')
                                break;
                            }
                        }

                        if (ceps.length == 0) {
                            var novaOpcao = new Option('Nenhum registro encontrado.', '');
                            ceps.options[ceps.length] = novaOpcao;
                            ceps.disabled = true;
                        } else {
                            ceps.disabled = false;
                        }

                        exibirElemento('resultadoPesquisarCep' + '<c:out value='${param.idCampoCep}' default=""/>');
                    }

                });
        } else {
            var camposNaoInformados = '';
            var separador = '';
            if (logradouro.value == "") {
                camposNaoInformados += 'Logradouro';
                separador = ', ';
            }
            if (cidade.value == "") {
                camposNaoInformados += separador + 'Cidade';
                separador = ', ';
            }
            if (uf.options[uf.selectedIndex].value == "") {
                camposNaoInformados += separador + 'UF';
            }

            //ocultarElemento('resultadoPesquisarCep'+'${param.idCampoCep}');
            alert('<fmt:message key="ERRO_NEGOCIO_CEP_PARAMETROS_NAO_INFORMADOS"><fmt:param>' + camposNaoInformados + '</fmt:param> </fmt:message>');
        }
    }

    animatedcollapse.addDiv('pesquisarCep' + '<c:out value='${param.idCampoCep}' default=""/>', 'fade=0,speed=400,persist=0,hide=1');

    function initCep() {
        var numeroCep = '${param.numeroCep}';
        if (numeroCep != "") {
            exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(false);
        }

        <c:choose>
        <c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
        var cep = document.getElementById('${param.idCampoCep}');
        </c:when>
        </c:choose>

        // Mostrando asterisco para o campo n?mero na aba de faturamento de contrato.
        <c:choose>
        <c:when test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}">
        if (cep != undefined && !$("#endFisEntFatNumero").attr("disabled")) {
            <c:out value="${param.mostrarAsterisco}"/>(cep.value, ['endFisEntFatNumero']);
        }
        </c:when>
        </c:choose>
    }


    function verificarChamadaQuadra(nomeCampoCep, cep){

    	if(nomeCampoCep == "cepComunicacao"){
    		carregarQuadras(cep.value);
    	}
    }

    addLoadEvent(initCep);

</script>

<c:set var="classeObrigatoriedadeCep" value="campoObrigatorioSimbolo"/>
<c:if test="${not empty param.styleClassCepObrigatorio}"><c:set var="classeObrigatoriedadeCep"
                                                                value="${param.styleClassCepObrigatorio}"/></c:if>

<input type="hidden" id="chaveCep" name="chaveCep" value="${param.chaveCep}">
<input type="hidden" id="cepAuxiliar" name="cepAuxiliar" value="${param.numeroCep}">

<div class="form-row mb-1">

    <c:set var="classeLabel" value="col-sm-2 text-md-right"/>
    <c:set var="classeInput" value="col-sm-10"/>

    <c:if test="${not empty param.inline and param.inline}">
        <%-- Seta a classe de inline --%>
        <c:set var="classeLabel" value="col-sm-12"/>
        <c:set var="classeInput" value="col-sm-12"/>
    </c:if>

    <c:if test="${not empty param.chamado and param.chamado}">
        <c:set var="classeLabel" value="col-sm-4 text-md-right"/>
        <c:set var="classeInput" value="col-sm-8"/>
    </c:if>

    <c:if test="${not empty param.servico and param.servico}">
        <c:set var="classeLabel" value="col-sm-3 text-md-right"/>
        <c:set var="classeInput" value="col-sm-9"/>
    </c:if>
    
    <c:if test="${not empty param.comunicacao and param.comunicacao}">
        <c:set var="classeLabel" value=""/>
        <c:set var="classeInput" value="col-md-12"/>
    </c:if>

    <c:choose>
        <c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
            <c:if test="${param.cepObrigatorio}">
                <label class="<c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if> ${classeLabel}"
                       id="rotuloCepTrue" for="${param.idCampoCep}">
                    CEP <span class="text-danger">*</span>
                </label>
            </c:if>
            <c:if test="${not param.cepObrigatorio}">
                <label class="<c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if> ${classeLabel}"
                       id="rotuloCepFalse" for="${param.idCampoCep}">
                    CEP
                </label>
            </c:if>
            <div class="${classeInput}">
                <div class="input-group input-group-sm">
                    <input type="text" name="${param.idCampoCep}" id="${param.idCampoCep}"
                           class="cep form-control form-control-sm" maxlength="9" value="${param.numeroCep}" onblur="
                    <c:if test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}"><c:out
                            value="${param.mostrarAsterisco}"/>(this.value,['endFisEntFatNumero']);
                    </c:if> exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(true);"
                           <c:if test="${param.cepDesabilitado eq true}">disabled="disabled"</c:if>>
                    <img id="imagemConferirCEPInativo<c:out value='${param.idCampoCep}' default=""/>"
                         src="imagens/cep_inativo.gif" border="0"/>
                    <a id="linkConferirCEP<c:out value='${param.idCampoCep}' default=""/>"
                       class="linkConferirCEPConteiner" href="#" style="display:none">
                        <img id="imagemConferirCEPAtivo<c:out value='${param.idCampoCep}' default=""/>"
                             src="imagens/cep_ativo.gif" border="0" style="height: 29px;"/>
                    </a>

                    <c:if test="${param.cepDesabilitado ne true}">

                        <div class="input-group-append">
                            <a class="input-group-text" id="linkPesquisarCEP"
                               rel="toggle[pesquisarCep<c:out value='${param.idCampoCep}' default=""/>]"
                               style="cursor: pointer">
                                Pesquisar CEP
                            </a>
                        </div>
                        <%--<a class="linkPesquisaAvancada linkPesquisarCEPDetalhe"  href="#"  data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pesquisar CEP <img src="imagens/setaBaixo.png" border="0" /></a>--%>
                    </c:if>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <c:if test="${param.cepObrigatorio}"><label
                    class="<c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if> ${classeLabel}"
                    id="rotuloCepTrue" for="cep"><span
                    class="${classeObrigatoriedadeCep}">* </span>CEP:</label></c:if><c:if
                test="${not param.cepObrigatorio}"><label
                class="rotulo<c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if>"
                id="rotuloCepFalse" for="cep">CEP:</label></c:if>
            <div class="${classeInput}">
                <div class="input-group input-group-sm">
                    <input type="text" name="cep" id="cep" class="cep form-control form-control-sm" maxlength="9"
                           value="${param.numeroCep}" onblur="
                    <c:if test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}"><c:out
                            value="${param.mostrarAsterisco}"/>(this.value,['endFisEntFatNumero']);
                    </c:if> exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(true);"
                           <c:if test="${param.cepDesabilitado eq true}">disabled="disabled"</c:if>>
                    <img id="imagemConferirCEPInativo<c:out value='${param.idCampoCep}' default=""/>"
                         src="imagens/cep_inativo.gif" border="0"/>
                    <a id="linkConferirCEP<c:out value='${param.idCampoCep}' default=""/>"
                       class="linkConferirCEPConteiner" href="#" style="display:none">
                        <img id="imagemConferirCEPAtivo<c:out value='${param.idCampoCep}' default=""/>"
                             src="imagens/cep_ativo.gif" border="0" style="height: 29px;"/>
                    </a>

                    <c:if test="${param.cepDesabilitado ne true}">
                        <div class="input-group-append">
                            <a class="input-group-text" id="linkPesquisarCEP" href="#"
                               rel="toggle[pesquisarCep<c:out value='${param.idCampoCep}' default=""/>]">
                                Pesquisar CEP
                            </a>
                        </div>
                    </c:if>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
</div>

<div id="conferirCEP<c:out value='${param.idCampoCep}' default=""/>" class="conferirCEPpopup" title="Endere?o do CEP" style="display: none">
    <label id="rotuloLogradouro" class="rotulo">Logradouro:</label>
    <span class="itemDetalhamento" id="logradouroPopup<c:out value='${param.idCampoCep}' default=""/>"></span><br/>
    <label id="rotuloBairro" class="rotulo">Bairro:</label>
    <span class="itemDetalhamento" id="bairroPopup<c:out value='${param.idCampoCep}' default=""/>"></span><br/>
    <label id="rotuloCidade" class="rotulo">Cidade:</label>
    <span class="itemDetalhamento" id="cidadePopup<c:out value='${param.idCampoCep}' default=""/>"></span><br/>
    <label id="rotuloUF" class="rotulo">UF:</label>
    <span class="itemDetalhamento" id="ufCepPopup<c:out value='${param.idCampoCep}' default=""/>"></span><br/><br/>
</div>

<div class="shadow-sm pb-3 pt-3 mb-5 pr-1 bg-light rounded"
     id="pesquisarCep<c:out value='${param.idCampoCep}' default=""/>"
     style="display: none">

    <c:choose>
        <c:when test="${param.idCampoLogradouro ne '' && param.idCampoLogradouro ne undefined}">
            <div class="form-row mb-1">
                <label for="${param.idCampoLogradouro}" class="col-sm-3 text-md-right">Logradouro <span
                        class="text-danger">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-control-sm"
                           id="${param.idCampoLogradouro}" name="${param.idCampoLogradouro}"
                           size="27" maxlength="50"
                           onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                                   value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="form-row mb-1">
                <label for="logradouro" class="col-sm-3 text-md-right">Logradouro <span
                        class="text-danger">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-control-sm"
                           id="logradouro" name="logradouro"
                           size="27" maxlength="50"
                           onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                                   value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
                </div>
            </div>
        </c:otherwise>
    </c:choose>

    <c:choose>
        <c:when test="${param.idCampoCidade ne '' && param.idCampoCidade ne undefined}">
            <div class="form-row mb-1">
                <label for="${param.idCampoCidade}" class="col-sm-3 text-md-right">Cidade <span
                        class="text-danger">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-control-sm"
                           name="${param.idCampoCidade}" id="${param.idCampoCidade}"
                           size="27" maxlength="30"
                           onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                                   value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="form-row mb-1">
                <label for="cidade" class="col-sm-3 text-md-right">Cidade <span
                        class="text-danger">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-control-sm"
                           name="cidade" id="cidade"
                           size="27" maxlength="30"
                           onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                                   value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
                </div>
            </div>
        </c:otherwise>
    </c:choose>

    <c:choose>
        <c:when test="${param.idCampoUf ne '' && param.idCampoUf ne undefined}">
            <c:set value="${param.idCampoUf}" var="campouf"></c:set>
        </c:when>
        <c:otherwise>
            <c:set value="uf" var="campouf"></c:set>
        </c:otherwise>
    </c:choose>

    <div class="form-row mb-1">
        <label for="<c:out value="${campouf}"></c:out>" class="col-sm-3 text-md-right">UF <span
                class="text-danger">*</span></label>
        <div class="col-sm-9">
            <select id="<c:out value="${campouf}"></c:out>" name="<c:out value="${campouf}"></c:out>"
                    class="form-control form-control-sm">
                <option value="">Selecione</option>
                <option value="AC">AC</option>
                <option value="AL">AL</option>
                <option value="AM">AM</option>
                <option value="AP">AP</option>
                <option value="BA">BA</option>
                <option value="CE">CE</option>
                <option value="DF">DF</option>
                <option value="ES">ES</option>
                <option value="GO">GO</option>
                <option value="MA">MA</option>
                <option value="MG">MG</option>
                <option value="MS">MS</option>
                <option value="MT">MT</option>
                <option value="PA">PA</option>
                <option value="PB">PB</option>
                <option value="PE">PE</option>
                <option value="PI">PI</option>
                <option value="PR">PR</option>
                <option value="RJ">RJ</option>
                <option value="RN">RN</option>
                <option value="RO">RO</option>
                <option value="RR">RR</option>
                <option value="RS">RS</option>
                <option value="SC">SC</option>
                <option value="SE">SE</option>
                <option value="SP">SP</option>
                <option value="TO">TO</option>
            </select>
        </div>
    </div>

    <div class="form-row m-1">
        <div class="form-group col-md-8 col-md-offset-4 pr-2">
            <small><span class="text-danger">*</span> campos obrigatórios apenas para pesquisar CEPs</small>
        </div>
    </div>

    <div class="row">
        <div class="col col d-flex justify-content-center">

            <img style="display:none" class="imgProc ml-1" id="imgProc<c:out value='${param.idCampoCep}' default=""/>"
                 alt="Pesquisando..." title="Pesquisando..." src="<c:url value="/imagens/indicator.gif"/>">

            <button class="btn btn-primary btn-sm" type="button"
                    id="botaoPesquisarCEP<c:out value='${param.idCampoCep}' default=""/>"
                    onclick="consultarCep<c:out value='${param.idCampoCep}' default=""/>();">
                <i class="fa fa-search"></i> Pesquisar CEP
            </button>

            <button class="btn btn-danger btn-sm ml-1" id="botaoLimparCEPNovo" type="button"
                    onclick="limparFormularioComponenteCep<c:out value='${param.idCampoCep}' default=""/>();">
                <i class="fa fa-times"></i> Limpar campos
            </button>

        </div>
    </div>

    <div class="form-row mb-1 mt-3" id="resultadoPesquisarCep<c:out value='${param.idCampoCep}' default=""/>"
         style="display: none">
        <label for="ceps<c:out value='${param.idCampoCep}' default=""/>" class="col-sm-3 text-md-right">
            Resultado (Selecione um dos registros):
        </label>
        <div class="col-sm-9">
            <div class="cepsLista">
                <select  id="ceps<c:out value='${param.idCampoCep}' default=""/>"
                         name="ceps<c:out value='${param.idCampoCep}' default=""/>"
                         onchange="selecionarCep<c:out value="${param.idCampoCep}" default=""/>(this); exibirEndereco<c:out
                                 value='${param.idCampoCep}' default=""/>(true);" size="10">
                </select>
            </div>
        </div>
    </div>
</div>
