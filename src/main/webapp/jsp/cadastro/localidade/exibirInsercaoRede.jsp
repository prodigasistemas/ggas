<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Incluir Rede Externa<a class="linkHelp" href="<help:help>/redeexternainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form method="post" action="inserirRede" name="redeForm" id="redeForm">

<script>

	function limparFormulario() {
		
		document.forms[0].descricao.value = "";
		document.forms[0].pressao.value = "";
		document.forms[0].extensao.value = "";
		document.forms[0].idLocalidade.value = "-1";
		document.forms[0].idRedeDiametro.value = "-1";
		document.forms[0].idRedeMaterial.value = "-1";
		document.forms[0].idTronco.value = "-1";
		document.forms[0].idCityGate.value = "-1";
		document.forms[0].idUnidadeExtensao.value = "-1";
		document.forms[0].idUnidadePressao.value = "-1";
	}
	
	function cancelar(){	
		location.href = '<c:url value="/exibirPesquisaRede"/>';		
	}
	
	
</script>

<input name="acao" type="hidden" id="acao" value="inserirRede">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="redeCol1" class="coluna">
		<label class="rotulo campoObrigatorio" id="rotuloRedeDescricao" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
		<input class="campoTexto" id="descricao" type="text" name="descricao" maxlength="50" size="45" tabindex="3" value="${rede.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
			
		<label class="rotulo campoObrigatorio" for="numeroExtensao"><span class="campoObrigatorioSimbolo">* </span>Extens�o:</label>
		<input class="campoTexto" type="text" id="extensao" name="extensao" maxlength="5" size="5" value="${rede.extensao}" onkeypress="return formatarCampoInteiro(event)">
		
		<select name="unidadeExtensao" id="idUnidadeExtensao" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaUnidadeMedicaoExtensao}" var="unidadeExtensao">
				<c:choose>
					<c:when test="${rede.unidadeExtensao.chavePrimaria != null}">
						<option value="<c:out value="${unidadeExtensao.chavePrimaria}"/>" <c:if test="${rede.unidadeExtensao.chavePrimaria == unidadeExtensao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${unidadeExtensao.descricao}"/>
						</option>
					</c:when>
					<c:otherwise>
						<option value="<c:out value="${unidadeExtensao.chavePrimaria}"/>" <c:if test="${unidadeExtensao.indicadorPadrao eq true}">selected="selected"</c:if>>
							<c:out value="${unidadeExtensao.descricao}"/>
						</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>	
		</select><br class="quebraLinha1" />
		
		<label class="rotulo campoObrigatorio" for="numeroPressao"><span class="campoObrigatorioSimbolo">* </span>Press�o:</label>
		<input class="campoTexto" type="text" id="pressao" name="pressao" maxlength="5" size="5" value="${pressao}" onkeypress="return formatarCampoDecimalPositivo(event, this, 10, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);">
		
		<select name="unidadePressao" id="idUnidadePressao" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaUnidadeMedicaoPressao}" var="unidadePressao">
				<c:choose>
					<c:when test="${rede.unidadePressao.chavePrimaria != null}">
						<option value="<c:out value="${unidadePressao.chavePrimaria}"/>" <c:if test="${rede.unidadePressao.chavePrimaria == unidadePressao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${unidadePressao.descricao}"/>
						</option>
					</c:when>
					<c:otherwise>
						<option value="<c:out value="${unidadePressao.chavePrimaria}"/>" <c:if test="${unidadePressao.indicadorPadrao eq true}">selected="selected"</c:if> >
							<c:out value="${unidadePressao.descricao}"/>
						</option>	
					</c:otherwise>
				</c:choose>
			</c:forEach>	
		</select><br class="quebraLinha1" />
		
		<label class="rotulo campoObrigatorio" for="idLocalidade"><span class="campoObrigatorioSimbolo">* </span>Localidade:</label>
		<select name="localidade" id="idLocalidade" class="campoSelect" >
			<option value="-1">Selecione</option>
			<c:forEach items="${listaLocalidade}" var="localidade">
				<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${rede.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localidade.descricao}"/>
				</option>		
			</c:forEach>	
		</select><br class="quebraLinha1" />
		
   	</fieldset>
   	
	<fieldset id="redeCol2" class="colunaFinal">
		<label class="rotulo campoObrigatorio" for="idRedeDiametro"><span class="campoObrigatorioSimbolo">* </span>Diametro de Rede:</label>
	   	<select name="redeDiametro" id="idRedeDiametro" class="campoSelect">
			<option value="-1">Selecione</option>
		  	<c:forEach items="${listaRedeDiametro}" var="redeDiametro">
				<option value="<c:out value="${redeDiametro.chavePrimaria}"/>" <c:if test="${rede.redeDiametro.chavePrimaria == redeDiametro.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${redeDiametro.descricao}"/>
				</option>		
	  	   	</c:forEach>	
		</select><br />	
		<label class="rotulo campoObrigatorio" for="idRedeMaterial"><span class="campoObrigatorioSimbolo">* </span>Material de Rede:</label>
	   	<select name="redeMaterial" id="idRedeMaterial" class="campoSelect">
		   	<option value="-1">Selecione</option>
			<c:forEach items="${listaRedeMaterial}" var="redeMaterial">
				<option value="<c:out value="${redeMaterial.chavePrimaria}"/>" <c:if test="${rede.redeMaterial.chavePrimaria == redeMaterial.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${redeMaterial.descricao}"/>
				</option>		
		    </c:forEach>	
		</select>
	</fieldset>
	
	<hr class="linhaSeparadora" />
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Troncos:</legend>
		<jsp:include page="/jsp/cadastro/localidade/exibirAdicaoTronco.jsp" >
			<jsp:param name="actionAdicionarTronco" value="adicionarTroncoNaRedeFluxoInclusao" />
			<jsp:param name="actionRemoverTronco" value="removerTroncoNaRedeFluxoInclusao" />
		</jsp:include>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Rede Externa.</p>
</fieldset>
	
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="inserirRede">
    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="submit">
    </vacess:vacess>
</fieldset>
<token:token></token:token>
</form>