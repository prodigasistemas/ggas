<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<h1 class="tituloInterno">Detalhar Munic�pio
	<a class="linkHelp"
		href="<help:help>/cadastrodemunicpiodetalhamento.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para modificar as informa��es deste registro clique em <span
		class="destaqueOrientacaoInicial">Alterar</span>
</p>

<script>

	$(document).ready(function(){
		  var max = 0;
	
	  $('.rotulo').each(function(){
	          if ($(this).width() > max)
	             max = $(this).width();   
	      });
	  $('.rotulo').width(max);
		
	});

	function voltar() {
		location.href = '<c:url value="/exibirPesquisarMunicipio"/>';
	}

	function alterar() {
		submeter('municipioForm', 'exibirAtualizarMunicipio');
	}
	
</script>

<form:form method="post" action="exibirDetalhamentoMunicipio" id="municipioForm" name="municipioForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria"value="${municipio.chavePrimaria}">
	<input name="descricao" type="hidden" id="descricao" value="${municipio.descricao}">
	<input name="habilitado" type="hidden" id="habilitado" value="${municipio.habilitado}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
			<label class="rotulo">Descri��o:</label>
			<span class="itemDetalhamento">${municipio.descricao}</span>
			<br /><br />
			
			<label class="rotulo">Cep In�cio:</label>
			<span class="itemDetalhamento"><c:out value="${municipio.cepInicio}"/></span><br />
			    
			<label class="rotulo">Cep Fim:</label>
			<span class="itemDetalhamento"><c:out value="${municipio.cepFim}"/></span><br />
				
			<label class="rotulo">DDD:</label>
			<span class="itemDetalhamento"><c:out value="${municipio.ddd}"/></span><br />
		</fieldset>
		
		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
			<label class="rotulo">Regi�o:</label>
			<span class="itemDetalhamento"><c:out value="${municipio.regiao.descricao}"/></span><br />
		    
		    <label class="rotulo">Microrregi�o:</label>
			<span class="itemDetalhamento"><c:out value="${municipio.microrregiao.descricao}"/></span><br />
		    
		    <label class="rotulo">Unidade Federa��o:</label>
			<span class="itemDetalhamento"><c:out value="${municipio.unidadeFederacao.descricao}"/></span><br />
				 
			<label class="rotulo">C�digo IBGE:</label>
		    <span class="itemDetalhamento"><c:out value="${municipio.unidadeFederativaIBGE}"/></span><br />
			
			<br />
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input id="botaoVoltar" name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
		<vacess:vacess param="atualizarMunicipio">
    		<input id="botaoAlterar" name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>
	</fieldset>
</form:form>