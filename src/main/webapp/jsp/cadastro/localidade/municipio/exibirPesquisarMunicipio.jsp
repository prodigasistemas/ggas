<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Munic�pio
	<a	class="linkHelp"
		href="<help:help>/consultasdosmunicpios.htm</help:help>"
		target="right" 
		onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>


<script>
	$(document).ready(function() {

		var max = 0;

		$('.rotulo').each(function() {
			if ($(this).width() > max)
				max = $(this).width();
		});
		$('.rotulo').width(max);
	});

	function limparFormulario() {
		document.forms[0].habilitado[0].checked = true;
		document.getElementById('descricao').value = "";
		document.getElementById('idMicrorregiao').value = "-1";
		document.getElementById('idUnidadeFederacao').value = "-1";
		document.getElementById('idRegiao').value = "-1";
		document.getElementById('codigoIBGE').value = "";
	}

	function pesquisar() {

		submeter("municipioForm", "pesquisarMunicipio");

	}

	function detalhar(chave) {
		document.forms['municipioForm'].chavePrimaria.value = chave;
		submeter("municipioForm", "exibirDetalhamentoMunicipio");
	}

	function incluir() {
		location.href = '<c:url value="/exibirInserirMunicipio"/>';
	}

	function alterar() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms['municipioForm'].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('municipioForm', 'exibirAtualizarMunicipio');
		}
	}

	function remover() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				document.forms[0].chavePrimaria.value = "";
				submeter('municipioForm', 'removerMunicipio');
			}
		}
	}

	function carregarRegiao(elem) {
		var codUnidadeFederacao = elem.value;
		var selectRegiao = document.getElementById("idRegiao");

		selectRegiao.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectRegiao.options[selectRegiao.length] = novaOpcao;

		if (codUnidadeFederacao != "-1") {
			selectRegiao.disabled = false;
			$("#idRegiao").removeClass("campoDesabilitado");
			AjaxService
					.consultarRegioesPorUnidadeFederacao(
							codUnidadeFederacao,
							{
								callback : function(listaRegioes) {
									for (key in listaRegioes) {
										var novaOpcao = new Option(
												listaRegioes[key], key);
										selectRegiao.options[selectRegiao.length] = novaOpcao;
									}
								},
								async : false
							});
		} else {
			AjaxService.listarRegioes({
				callback : function(listaRegioes) {
					for (key in listaRegioes) {
						var novaOpcao = new Option(listaRegioes[key], key);
						selectRegiao.options[selectRegiao.length] = novaOpcao;
					}
				},
				async : false
			});
		}

	}

	function carregarMicrorregiao(elem) {
		var codRegiao = elem.value;
		var selectMicrorregiao = document.getElementById("idMicrorregiao");

		selectMicrorregiao.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;

		if (codRegiao != "-1") {
			AjaxService
					.consultarMicrorregioesPorRegiao(
							codRegiao,
							{
								callback : function(listaMicrorregioes) {
									for (key in listaMicrorregioes) {
										var novaOpcao = new Option(
												listaMicrorregioes[key], key);
										selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;
									}
								},
								async : false
							});
		} else if (document.getElementById('idUnidadeFederacao').value != "-1") {
			carregarMicrorregiaoPorUnidadeFederacao(document
					.getElementById('idUnidadeFederacao'));
		} else {
			AjaxService
					.listarMicrorregioes({
						callback : function(listaMicrorregioes) {
							for (key in listaMicrorregioes) {
								var novaOpcao = new Option(
										listaMicrorregioes[key], key);
								selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;
							}
						},
						async : false
					});
		}

	}

	function carregarMicrorregiaoPorUnidadeFederacao(elem) {
		var codUnidadeFederacao = elem.value;
		var selectMicrorregiao = document.getElementById("idMicrorregiao");

		selectMicrorregiao.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;

		if (codUnidadeFederacao != "-1") {
			AjaxService
					.consultarMicrorregioesPorUnidadeFederacao(
							codUnidadeFederacao,
							{
								callback : function(listaMicrorregioes) {
									for (key in listaMicrorregioes) {
										var novaOpcao = new Option(
												listaMicrorregioes[key], key);
										selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;
									}
								},
								async : false
							});
		} else if (document.getElementById('idRegiao').value != "-1") {
			carregarMicrorregiao(document.getElementById('idRegiao'));
		} else {
			AjaxService
					.listarMicrorregioes({
						callback : function(listaMicrorregioes) {
							for (key in listaMicrorregioes) {
								var novaOpcao = new Option(
										listaMicrorregioes[key], key);
								selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;
							}
						},
						async : false
					});
		}

	}
</script>


<form:form method="post" action="pesquisarMunicipio" id="municipioForm" name="municipioForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	
	<fieldset class="conteinerPesquisarIncluirAlterar"> 
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
			<label class="rotulo" id="rotuloDescricao" for="descricao">Descri��o:</label>
			<input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${municipio.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/>
			
			<br /> 
			
			<label class="rotulo">Regi�o:</label> 
			<select name="regiao"
				class="campoSelect" id="idRegiao"
				onchange="carregarMicrorregiao(this);">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaRegiao}" var="regiao">
					<option value="<c:out value="${regiao.chavePrimaria}"/>"
						<c:if test="${municipio.regiao.chavePrimaria == regiao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${regiao.descricao}" />
					</option>
				</c:forEach>
			</select> 
			
			<br />
	            
	        <label class="rotulo">Microrregi�o:</label> 
	       	<select
				name="microrregiao" class="campoSelect" id="idMicrorregiao">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMicrorregioes}" var="microrregiao">
					<option value="<c:out value="${microrregiao.chavePrimaria}"/>"
						<c:if test="${municipio.microrregiao.chavePrimaria == microrregiao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${microrregiao.descricao}" />
					</option>
				</c:forEach>
			</select>
		</fieldset>
				
		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
			
			<label class="rotulo">Unidade Federa��o:</label>
            <select name="unidadeFederacao" class="campoSelect" id="idUnidadeFederacao" onchange="carregarRegiao(this);carregarMicrorregiaoPorUnidadeFederacao(this);">
                <option value="-1">Selecione</option>
                <c:forEach items="${listaUnidadeFederacao}" var="unidadeFederacao">
                    <option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${municipio.unidadeFederacao.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${unidadeFederacao.descricao}"/>
                    </option>       
                </c:forEach>    
            </select><br />
            
            <label class="rotulo">C�digo IBGE:</label>
            <input class="campoTexto campoHorizontal" type="text" id="codigoIBGE" name="unidadeFederativaIBGE" maxlength="10" value="${municipio.unidadeFederativaIBGE}" onkeypress="return formatarCampoInteiro(event)"> <br />
            
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq true}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq false}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label><br />
            <br /><br />
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="pesquisarMunicipio">
					<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
						value="Pesquisar" type="submit">
				</vacess:vacess>
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo"
					id="limpar" value="Limpar" type="button"
					onclick="limparFormulario();">
			</fieldset>
		</fieldset>
	</fieldset>
	
	<c:if test="${listaMunicipio ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaMunicipio" sort="list" id="municipio" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarMunicipio"> 
	        
	        <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${municipio.chavePrimaria}">
	     	</display:column>
	     	
	     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${municipio.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
			
			<display:column sortable="true" sortProperty="descricao" title="Descri��o">        
	            <a href="javascript:detalhar(<c:out value='${municipio.chavePrimaria}'/>);">
	            	<c:out value="${municipio.descricao}"/>
	            </a>
	        </display:column>
	        
			<display:column sortable="true" title="Unidade Federa��o">
				<a href="javascript:detalharTabelaAuxiliar(<c:out value='${municipio.chavePrimaria}'/>);"><span
					class="linkInvisivel"></span> <c:out
						value="${municipio.unidadeFederacao.descricao}" /> </a>
			</display:column>

		</display:table>	
	</c:if>

	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaMunicipio}">
			<vacess:vacess param="atualizarMunicipio">
				<input id="botaoAlterar" name="Alterar" value="Alterar"
					class="bottonRightCol2" onclick="alterar()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerMunicipio">
				<input id="botaoRemover" name="Remover" value="Remover"
					class="bottonRightCol2" onclick="remover();" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="inserirMunicipio">
			<input id="botaoIncluir" name="button" value="Incluir"
				class="bottonRightCol2 botaoGrande1 botaoIncluir"
				onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>

</form:form>