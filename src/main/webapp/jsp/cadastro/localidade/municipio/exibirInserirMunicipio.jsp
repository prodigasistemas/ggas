<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="pt-BR"/>

<h1 class="tituloInterno">Incluir Munic�pio
	<a class="linkHelp" href="<help:help>/cadastrodemunicpioinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>

<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>
	$(document).ready(function() {
		$("#cepInicio").inputmask("99999-999", {
			placeholder : "_"
		});
		$("#cepFim").inputmask("99999-999", {
			placeholder : "_"
		});

		var max = 0;

		$('.rotulo').each(function() {
			if ($(this).width() > max)
				max = $(this).width();
		});
		$('.rotulo').width(max);

	});

	function carregarRegiao(elem) {
		var codUnidadeFederacao = elem.value;
		var selectRegiao = document.getElementById("idRegiao");

		selectRegiao.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectRegiao.options[selectRegiao.length] = novaOpcao;

		if (codUnidadeFederacao != "-1") {
			selectRegiao.disabled = false;
			$("#idRegiao").removeClass("campoDesabilitado");
			AjaxService
					.consultarRegioesPorUnidadeFederacao(
							codUnidadeFederacao,
							{
								callback : function(listaRegioes) {
									for (key in listaRegioes) {
										var novaOpcao = new Option(
												listaRegioes[key], key);
										selectRegiao.options[selectRegiao.length] = novaOpcao;
									}
								},
								async : false
							});
		} else {
			selectRegiao.disabled = true;

			$("#idRegiao").addClass("campoDesabilitado");
		}

	}

	function carregarMicrorregiao(elem) {
		var codRegiao = elem.value;
		var selectMicrorregiao = document.getElementById("idMicrorregiao");

		selectMicrorregiao.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;

		if (codRegiao != "-1") {
			selectMicrorregiao.disabled = false;
			$("#idMicrorregiao").removeClass("campoDesabilitado");
			AjaxService
					.consultarMicrorregioesPorRegiao(
							codRegiao,
							{
								callback : function(listaMicrorregioes) {
									for (key in listaMicrorregioes) {
										var novaOpcao = new Option(
												listaMicrorregioes[key], key);
										selectMicrorregiao.options[selectMicrorregiao.length] = novaOpcao;
									}
								},
								async : false
							});
		} else {
			selectMicrorregiao.disabled = true;

			$("#idMicrorregiao").addClass("campoDesabilitado");
		}

	}

	function limpar() {
		document.getElementById('descricao').value = "";
		document.getElementById('idUnidadeFederacao').value = "-1";
		document.getElementById('idMicrorregiao').value = "-1";
		document.getElementById('idRegiao').value = "-1";
		document.getElementById('ddd').value = "";
		document.getElementById('cepInicio').value = "";
		document.getElementById('cepFim').value = "";
		document.getElementById('codigoIBGE').value = "";
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisarMunicipio"/>';
	}
</script>

<form:form method="post" action="inserirMunicipio" id="municipioForm" name="municipioForm">

	<input id="habilitado" name="habilitado" type="hidden" value="${true}">

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
       		<input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${municipio.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        	<br />
        	<br />
            <label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>Cep In�cio:</label>
            <input class="campoTexto campoHorizontal" type="text" id="cepInicio" name="cepInicio" size="9" maxlength="9" value="${municipio.cepInicio}" > <br />
            
            <label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>Cep Fim:</label>
            <input class="campoTexto campoHorizontal" type="text" id="cepFim" name="cepFim" maxlength="9" size="9" value="${municipio.cepFim}" > <br />
            
            <label class="rotulo">DDD:</label>
            <input class="campoTexto campoHorizontal" type="text" id="ddd" name="ddd" maxlength="3" size="3" value="${municipio.ddd}" onkeypress="return formatarCampoInteiro(event)"> <br />
            
   		</fieldset>

        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
           <br />
           
           <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Unidade Federa��o:</label>
            <select name="unidadeFederacao" class="campoSelect" id="idUnidadeFederacao" onchange="carregarRegiao(this);">
                <option value="-1">Selecione</option>
                <c:forEach items="${listaUnidadeFederacao}" var="unidadeFederacao">
                    <option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${municipio.unidadeFederacao.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${unidadeFederacao.descricao}"/>
                    </option>       
                </c:forEach>    
            </select>
            <br />
            
            <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Regi�o:</label>
            <select name="regiao" class="campoSelect" id="idRegiao" onchange="carregarMicrorregiao(this);" <c:if test="${empty municipio.unidadeFederacao.chavePrimaria || municipio.unidadeFederacao.chavePrimaria == '-1'}">disabled="disabled"</c:if>>
                <option value="-1">Selecione</option>
                <c:forEach items="${listaRegiao}" var="regiao">
                    <option value="<c:out value="${regiao.chavePrimaria}"/>" <c:if test="${municipio.regiao.chavePrimaria == regiao.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${regiao.descricao}"/>
                    </option>       
                </c:forEach>    
            </select>
            
            <br />
            
            <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Microrregi�o:</label>
            <select name="microrregiao" class="campoSelect" id="idMicrorregiao" <c:if test="${empty municipio.regiao.chavePrimaria || municipio.regiao.chavePrimaria == '-1'}">disabled="disabled"</c:if>>
                <option value="-1">Selecione</option>
                <c:forEach items="${listaMicrorregioes}" var="microrregiao">
                    <option value="<c:out value="${microrregiao.chavePrimaria}"/>" <c:if test="${municipio.microrregiao.chavePrimaria == microrregiao.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${microrregiao.descricao}"/>
                    </option>       
                </c:forEach>    
            </select>
            
            <br />
            
            <label class="rotulo">C�digo IBGE:</label>
            <input class="campoTexto campoHorizontal" maxlength="7" type="text" id="codigoIBGE" name="unidadeFederativaIBGE" value="${municipio.unidadeFederativaIBGE}" onkeypress="return formatarCampoInteiro(event)"> <br /><br />
                
        </fieldset>
        
        <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
   		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
		<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="javascript:limpar();">
		<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="submit" id="botaoSalvar" onclick="javascript:salvar();">
	</fieldset>

</form:form> 