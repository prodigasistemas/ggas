<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<h1 class="tituloInterno">Detalhar Setor Comercial<a href="<help:help>/cadastrodosetorcomercialdetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<form:form method="post" id="setorComercialForm" name="setorComercialForm" action="exibirDetalhamentoSetorComercial" enctype="multipart/form-data">
	
	<script>
		
		function voltar(){
			submeter('setorComercialForm', 'pesquisarSetoresComerciais');
		}
		
		function alterar(){
			document.forms[0].postBack.value = false;
			submeter('setorComercialForm', 'exibirAtualizacaoSetorComercial');
		}
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoSetorComercial">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${setorComercialForm.chavePrimaria}">
	<input name="filtro" type="hidden" id="filtro" value="${filtro}">
		
	<input name="gerenciaRegional" type="hidden" value="${setorComercialVO.gerenciaRegional.chavePrimaria}">
	<input name="localidade" type="hidden"  value="${setorComercialVO.localidade.chavePrimaria}">
	<input name="unidadeFederativa" type="hidden" value="${setorComercialVO.unidadeFederativa.chavePrimaria}">
	<input name="municipio" type="hidden"  value="${setorComercialVO.municipio.chavePrimaria}">
	<input name="habilitado" type="hidden"  value="${setorComercialVO.habilitado}">
	<input name="codigo" type="hidden"  value="${setorComercialVO.codigo}">
	<input name="descricao" type="hidden"  value="${setorComercialVO.descricao}">
	
	<fieldset id="setorComercial" class="detalhamento">
		<fieldset class="coluna">
			<label class="rotulo" for="descricaoLocalidade">Localidade:</label>
			<span class="itemDetalhamento itemDetalhamentoPequeno2"><c:out value="${setorComercialForm.localidade.descricao}"/></span><br />
			<label class="rotulo" for="codigoSetorComercial">C�digo do Setor Comercial:</label>
			<span class="itemDetalhamento"><c:out value="${setorComercialForm.codigo}"/></span><br />
			<label class="rotulo" for="nomeSetorComercial">Nome do Setor Comercial:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno2"><c:out value="${setorComercialForm.descricao}"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo">Unidade Federativa:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno2"><c:out value="${setorComercialForm.municipio.unidadeFederacao.descricao}"/></span><br />
			<label class="rotulo" for="idMunicipio">Municipio:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno2"><c:out value="${setorComercialForm.municipio.descricao}"/></span><br />
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<c:choose>
				<c:when test="${setorComercialForm.habilitado eq true}">
					<span class="itemDetalhamento"><c:out value="Ativo"/></span><br />
				</c:when>
				<c:otherwise>
					<span class="itemDetalhamento"><c:out value="Inativo"/></span><br />
				</c:otherwise>
			</c:choose>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
	    <vacess:vacess param="exibirAtualizacaoSetorComercial">    
	    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" value="Alterar" type="button" onclick="alterar();">
	    </vacess:vacess>
	</fieldset>
</form:form>
