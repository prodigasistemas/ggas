<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Pesquisar Ger�ncia Regional<a href="<help:help>/consultadagernciaregional.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script language="javascript">

	function removerGerenciaRegional(){
		
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('gerenciaRegionalForm', 'removerGerenciaRegional');
			}
	    }
	}
	
	function alterarGerenciaRegional(chave) {
			var valor = $('#codigo').val();
			$('#codigoForm').val(valor);
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("gerenciaRegionalForm", "exibirAtualizacaoGerenciaRegional");
		    }
		}	
	
	
	function limparFormulario(){
		document.forms[0].codigo.value = "";
		document.forms[0].nomeAbreviado.value = "";
		document.forms[0].nome.value = "";	
		document.forms[0].habilitado[0].checked = true;

	}	
	
	function incluir() {		
		location.href = '<c:url value="/exibirInserirGerenciaRegional"/>';
	}
	
	function detalharGerenciaRegional(chave){
		var valor = $('#codigo').val();
		$('#codigoForm').val(valor);
		submeter("gerenciaRegionalForm", "exibirDetalhamentoGerenciaRegional?chavePrimaria="+chave);
	}
		
	</script>

<form:form method="post" action="pesquisarGerenciaRegional" id="gerenciaRegionalForm" name="gerenciaRegionalForm"> 

	<input name="acao" type="hidden" id="acao" value="pesquisarGerenciaRegional">
	<input name="codigoForm" type="hidden" id="codigoForm">
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarGerenciaRegional" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" id="rotuloCodigo" for="codigo">C�digo:</label>
			<input class="campoTexto campoHorizontal" type="text" id="codigo" name="chavePrimaria" maxlength="3" size="3" value="${codigo}" onkeypress="return formatarCampoInteiro(event)">
			<label class="rotulo rotuloHorizontal" id="rotuloNomeAbreviado" for="nomeAbreviado">Nome Abreviado:</label>
			<input class="campoTexto campoHorizontal" type="text" id="nomeAbreviado" name="nomeAbreviado" maxlength="5" size="5" value="${gerenciaRegionalForm.nomeAbreviado}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br class="quebraLinha2" />
			<label class="rotulo" id="rotuloNome" for="nome">Nome:</label>
			<input class="campoTexto" type="text" id="nome" name="nome" maxlength="50" size="52" value="${gerenciaRegionalForm.nome}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicadorUsoAtivo" value="true" <c:if test="${habilitado eq true}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicadorUsoInativo" value="false" <c:if test="${habilitado eq false}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoInativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicadorUsoTodos" value="" <c:if test="${empty habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoTodos">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarGerenciaRegional">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaGerenciaRegional ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaGerenciaRegional" sort="list" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" id="gerenciaRegional" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarGerenciaRegional">
	     	<display:column media="html" style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
		      		<input type="checkbox" name="chavesPrimarias" value="${gerenciaRegional.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${gerenciaRegional.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	     	<display:column sortable="true" titleKey="GERE_NOME" headerClass="tituloTabelaEsq" style="text-align: left">
	     		<a href='javascript:detalharGerenciaRegional(<c:out value='${gerenciaRegional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${gerenciaRegional.nome}'/>
	        	</a>
		    </display:column>
		    <display:column sortable="true" titleKey="GERE_N0ME_ABREVIADO" style="width: 100px">
		    	<a href='javascript:detalharGerenciaRegional(<c:out value='${gerenciaRegional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${gerenciaRegional.nomeAbreviado}'/>
	        	</a>
		    </display:column>
		    <display:column sortable="true" titleKey="GERE_FONE" style="width: 75px">
		    	<a href='javascript:detalharGerenciaRegional(<c:out value='${gerenciaRegional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${gerenciaRegional.fone}'/>
	        	</a>
	        </display:column>
		    <display:column sortable="true" titleKey="GERE_RAMAL" style="width: 40px">
		    	<a href='javascript:detalharGerenciaRegional(<c:out value='${gerenciaRegional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${gerenciaRegional.ramalFone}'/>
	        	</a>
	        </display:column>
		    <display:column sortable="true" titleKey="GERE_FAX" style="width: 75px">
		    	<a href='javascript:detalharGerenciaRegional(<c:out value='${gerenciaRegional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${gerenciaRegional.fax}'/>
	        	</a>
	        </display:column>
		    <display:column sortable="true" titleKey="GERE_EMAIL" maxLength="30" style="width: 200px">
		    	<a href='javascript:detalharGerenciaRegional(<c:out value='${gerenciaRegional.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${gerenciaRegional.email}'/>
	        	</a>
	        </display:column>
		</display:table>		
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaGerenciaRegional}">
			<vacess:vacess param="exibirAtualizacaoGerenciaRegional">
				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarGerenciaRegional()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerGerenciaRegional">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerGerenciaRegional()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInserirGerenciaRegional">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>
<token:token></token:token>
</form:form> 