<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js"></script>

<script type="text/javascript">
	var controleOnBlur = false;
	function cepCheckForEnter(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			if (this.onblur){
				this.onblur();
			}
			return false;
		}
	}
	function recuperaIdCepCorrente(){
		var nomeCampoCorrente = '${param.idCampoCep}'
		try{
			return '#id' + nomeCampoCorrente.charAt(0).toUpperCase() + nomeCampoCorrente.substr(1,nomeCampoCorrente.length)
		}catch(e){
			return '#id' + nomeCampoCorrente
		}
	}
	$(document).ready(function(){
		$("#${param.idCampoCep}").keydown (cepCheckForEnter);

		<c:if test="${not empty param.idCampoCep}">
		$("#${param.idCampoCep}").inputmask("99999-999",{placeholder:"_"});
		</c:if>

		// Dialog
		$("#conferirCEP<c:out value='${param.idCampoCep}' default=""/>").dialog({
			autoOpen: false,
			close: function(){$("#linkPesquisarCEP").focus()},
			width: 333,
			open: function (event, ui){
				$("#conferirCEP<c:out value='${param.idCampoCep}' default=""/>").parent().css("z-index", 10);
			},
			modal: true,
			resizable: false
		});

		// Dialog Link
		$("#linkConferirCEP<c:out value='${param.idCampoCep}' default=''/>").click(function(){
			<c:choose>
			<c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
			var cep = document.getElementById('${param.idCampoCep}');
			</c:when>
			</c:choose>

			if ((cep != undefined) && (cep.value != "")) {
				exibirJDialog("#conferirCEP<c:out value='${param.idCampoCep}' default=""/>");
			} else {
				limparDialog<c:out value="${param.idCampoCep}" default=""/>();
			}
			return false;
		});

		/*Executa a pesquisa quando a tecla ENTER � pressionada, caso o cursor
        esteja apenas em um dos campos do formul�rio de pesquisa de CEP*/
		$("#pesquisarCepCampos>:text, #pesquisarCepCampos>select").keypress(function(event){
			if (event.keyCode == '13') {
				consultarCep<c:out value='${param.idCampoCep}' default=""/>();
			}
		});


		$("#cepImovel").removeClass("campoDesabilitado");
		$("#cepImovel").removeAttr("readonly");
		$("#numeroImovel").removeClass("campoDesabilitado");
		$("#numeroImovel").removeClass("campoDesabilitado");


		//initCep();
	});




	function clearOptionsFast(selectObj) {
		var selectParentNode = selectObj.parentNode;
		var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
		selectParentNode.replaceChild(newSelectObj, selectObj);
		return newSelectObj;

	}

	function exibirEnderecoAlteracao<c:out value="${param.idCampoCep}" default=""/>(exibir, selecionouCepNoGrid, chamadaInicial, acao){


		var imagemConferirCEPAtivo = document.getElementById("imagemConferirCEPAtivo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var imagemConferirCEPInativo = document.getElementById("imagemConferirCEPInativo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var logradouroPopup = document.getElementById("logradouroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var bairroPopup = document.getElementById("bairroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var cidadePopup = document.getElementById("cidadePopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var ufCepPopup = document.getElementById("ufCepPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var chaveCep = document.getElementById("chaveCep");
		var cepAuxiliar = document.getElementById("cepAuxiliar").value;
		var linkConferirCEP = document.getElementById("linkConferirCEP"+'<c:out value="${param.idCampoCep}" default=""/>');


		<c:choose>
		<c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
		var cep = document.getElementById('${param.idCampoCep}');
		</c:when>
		</c:choose>
		if (selecionouCepNoGrid){
			try{
				var chaveCep = document.getElementById("chaveCep");
				document.getElementById("idCepRetornoBusca").value = chaveCep.value;

				// $("#${param.idCampoCep}").val(chaveCep.value)
				$(recuperaIdCepCorrente()).val(chaveCep.value)

				AjaxService.consultarCepPorId(chaveCep.value,
						function(cepEncontrado) {
							if(cepEncontrado != null) {

								$("#campoEndereco").attr('value', (cepEncontrado[0]!=null?cepEncontrado[0]:'') + ", " + (cepEncontrado[1]!=null?cepEncontrado[1]:'') + ", " + (cepEncontrado[2]!=null?cepEncontrado[2]:'') + " - " + (cepEncontrado[3]!=null?cepEncontrado[3]:''));

							}
							pesquisandoCep = false
							var tipoMetodo = "<c:out value="${param.idCampoCep}" default=""/>";
							if(tipoMetodo != "cepPontosConsumo"){
								carregarQuadras(cepEncontrado[4]);
							}
						});
			}catch(e){

			}
		}
		if(chamadaInicial == undefined){
			chamadaInicial = true;
		}
		if (!chamadaInicial && !selecionouCepNoGrid ){

			if(cep.value != cepAuxiliar) {
				chaveCep.value = "";
			}



			if (!pesquisandoCep || pesquisandoCep){
				pesquisandoCep = true
				if(chaveCep.value == "" || chaveCep.value ) {

					var valorCep = cep.value.replace(/_/gi,'')
					if(valorCep.length == 9){

						AjaxService.consultarEndereco(cep.value,
								function(endereco) {
									//
									var funcaoOnblur = document.getElementById('<c:out value="${param.idCampoCep}" default=""/>').onblur
									document.getElementById('<c:out value="${param.idCampoCep}" default=""/>').onblur = null
									//debugger
									var chaveCep = document.getElementById("chaveCep");
									if(endereco != null) {
										var tamanhoCeps = 0;
										var cepEncontrado;
										var cepKey;
										for (key in endereco){
											tamanhoCeps++;
											if(tamanhoCeps > 1) {
												break;
											}
											cepEncontrado = endereco[key];
											cepKey = key;
										}

										if(tamanhoCeps > 1) {
											if(exibir) {
												alert("O cep possui mais de um endere�o.");
											}
											limparDialog<c:out value="${param.idCampoCep}" default=""/>();

											var ceps = document.getElementById("ceps"+'${param.idCampoCep}');
											ceps  = clearOptionsFast(ceps);

											for (key in endereco){

												var cepEndereco = endereco[key];
												var enderecoFormatado = cepEndereco[1] + ", " + cepEndereco[2] + ", " + cepEndereco[0];
												var novaOpcao = new Option(enderecoFormatado, key);
												novaOpcao.title = endereco[key];
												ceps.options[ceps.length] = novaOpcao;
											}
											animatedcollapse.show('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
											exibirElemento('resultadoPesquisarCep'+'<c:out value='${param.idCampoCep}' default=""/>');
										} else {
											chaveCep.value = cepKey;
											document.getElementById("chaveCep").value = cepKey;
											var ceps = document.getElementById("ceps"+'${param.idCampoCep}');
											ceps  = clearOptionsFast(ceps);
											animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
											if(cepEncontrado){
												if (cepEncontrado[1].indexOf("null") == -1){
													logradouroPopup.innerHTML = cepEncontrado[1];
												}else{
													logradouroPopup.innerHTML = "";
												}

												if (cepEncontrado[2] != null){
													bairroPopup.innerHTML = cepEncontrado[2];
												}else{
													bairroPopup.innerHTML = "";
												}

												cidadePopup.innerHTML = cepEncontrado[3];
												ufCepPopup.innerHTML = cepEncontrado[4];
												linkConferirCEP.style.display = 'inline';
												imagemConferirCEPAtivo.style.display = '';
												imagemConferirCEPInativo.style.display = 'none';
												// cepAuxiliar.value = cepEncontrado[0];

												var tipoMetodo = "<c:out value="${param.idCampoCep}" default=""/>";
												if(tipoMetodo == "cepPontosConsumo"){
													carregarQuadras2(cepEncontrado[0]);
												} else if(tipoMetodo == "endFisEntFatCEP"){
													carregarEnderecoCepAbaFaturamento();
												} else{
													carregarQuadras(cepEncontrado[0]);
												}

												document.getElementById("cepAuxiliar").value = cepEncontrado[0]
												try{
													//setTimeout("$('#campoEndereco').attr('value', '" + cepEncontrado[0] + ", " + cepEncontrado[1] + ", " + cepEncontrado[2] + " - " + cepEncontrado[3] + "')",1000)
													setTimeout("$('#campoEndereco').attr('value', '" +  ((cepEncontrado[1] != null)?cepEncontrado[1]:'')  + ", " + ((cepEncontrado[2] != null)?cepEncontrado[2]:'')  + " - " + ((cepEncontrado[3] != null)?cepEncontrado[3]:'')  + "')",1000)

												}catch(e){

												}
											}


										}
									} else {
										chaveCep.value = "";
										cepAuxiliar.value = "";
										limparDialog<c:out value="${param.idCampoCep}" default=""/>();
									}
									//
									document.getElementById('<c:out value="${param.idCampoCep}" default=""/>').onblur = funcaoOnblur
									pesquisandoCep = false
								});
					}
				} else {
					AjaxService.consultarCepPorId(chaveCep.value,
							function(cepEncontrado) {
								try{

									$(recuperaIdCepCorrente()).val(chaveCep.value)

								}catch(e){

								}
								if(cepEncontrado != null) {
									if (cepEncontrado[0].indexOf("null") == -1){
										logradouroPopup.innerHTML = cepEncontrado[0];
									}else{
										logradouroPopup.innerHTML = "";
									}

									if (cepEncontrado[1] != null){
										bairroPopup.innerHTML = cepEncontrado[1];
									}else{
										bairroPopup.innerHTML = "";
									}

									cidadePopup.innerHTML = cepEncontrado[2];
									ufCepPopup.innerHTML = cepEncontrado[3];
									linkConferirCEP.style.display = 'inline';
									imagemConferirCEPAtivo.style.display = '';
									imagemConferirCEPInativo.style.display = 'none';
									try{
										setTimeout("$('#campoEndereco').attr('value', '" + ((cepEncontrado[1] != null)?cepEncontrado[1]:cepEncontrado[0])  + ", " + ((cepEncontrado[2] != null)?cepEncontrado[2]:'')  + " - " + ((cepEncontrado[3] != null)?cepEncontrado[3]:'')  + "')",1000)
									}catch(e){

									}
								}
								pesquisandoCep = false
							});
				}
			}













		}


	}

	var pesquisandoCep = false

	function exibirEnderecoPersquisa<c:out value="${param.idCampoCep}" default=""/>(exibir, selecionouCepNoGrid, chamadaInicial){

		var imagemConferirCEPAtivo = document.getElementById("imagemConferirCEPAtivo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var imagemConferirCEPInativo = document.getElementById("imagemConferirCEPInativo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var logradouroPopup = document.getElementById("logradouroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var bairroPopup = document.getElementById("bairroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var cidadePopup = document.getElementById("cidadePopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var ufCepPopup = document.getElementById("ufCepPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var chaveCep = document.getElementById("chaveCep");
		var cepAuxiliar = document.getElementById("cepAuxiliar").value;

		var linkConferirCEP = document.getElementById("linkConferirCEP"+'<c:out value="${param.idCampoCep}" default=""/>');

		if (selecionouCepNoGrid){

			$("#linkPesquisarCEP").focus();
			$("#linkPesquisarCEP").focus();
			var chaveCep = document.getElementById("chaveCep");
			$(recuperaIdCepCorrente()).val(chaveCep.value)
			return
		}
		<c:choose>
		<c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
		var cep = document.getElementById('${param.idCampoCep}');
		</c:when>
		</c:choose>
		if (selecionouCepNoGrid){
			try{
				var chaveCep = document.getElementById("chaveCep");
				document.getElementById("idCepRetornoBusca").value = chaveCep.value;
				$(recuperaIdCepCorrente()).val(chaveCep.value)
				carregarQuadras();
			}catch(e){

			}
		}

		if (!chamadaInicial && !selecionouCepNoGrid){
			var valorCep = cep.value.replace(/_/gi,'')
			if(valorCep.length == 9){

				if (!pesquisandoCep){
					pesquisandoCep = true

					//
					var funcaoOnblur = document.getElementById('<c:out value="${param.idCampoCep}" default=""/>').onblur
					document.getElementById('<c:out value="${param.idCampoCep}" default=""/>').onblur = null

					AjaxService.consultarEndereco(valorCep,
							function(endereco) {
								//

								if(endereco != null) {
									var tamanhoCeps = 0;
									var cepEncontrado;
									var cepKey;
									for (key in endereco){
										tamanhoCeps++;
										if(tamanhoCeps > 1) {
											break;
										}
										cepEncontrado = endereco[key];
										cepKey = key;
									}

									if(tamanhoCeps > 1) {
										if(exibir) {
											alert("O cep possui mais de um endere�o.");
											$("#linkPesquisarCEP").focus();
											$("#linkPesquisarCEP").focus();
										}
										limparDialog<c:out value="${param.idCampoCep}" default=""/>();

										var ceps = document.getElementById("ceps"+'${param.idCampoCep}');
										ceps  = clearOptionsFast(ceps);

										for (key in endereco){

											var cepEndereco = endereco[key];
											var enderecoFormatado = cepEndereco[1] + ", " + cepEndereco[2] + ", " + cepEndereco[0];
											var novaOpcao = new Option(enderecoFormatado, key);
											novaOpcao.title = endereco[key];
											ceps.options[ceps.length] = novaOpcao;
										}
										animatedcollapse.show('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
										exibirElemento('resultadoPesquisarCep'+'<c:out value='${param.idCampoCep}' default=""/>');


									} else {
										$("#linkPesquisarCEP").focus();
										$("#linkPesquisarCEP").focus();
										var ceps = document.getElementById("ceps"+'${param.idCampoCep}');
										ceps  = clearOptionsFast(ceps);
										animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
										if (chaveCep){

											chaveCep.value = cepKey;
										}
										if (cepEncontrado[1].indexOf("null") == -1){
											if (logradouroPopup)
												logradouroPopup.innerHTML = cepEncontrado[1];
										}else{
											logradouroPopup.innerHTML = "";
										}

										if (cepEncontrado[2] != null){
											if(bairroPopup)
												bairroPopup.innerHTML = cepEncontrado[2];
										}else{
											bairroPopup.innerHTML = "";
										}

										cidadePopup.innerHTML = cepEncontrado[3];
										ufCepPopup.innerHTML = cepEncontrado[4];
										linkConferirCEP.style.display = 'inline';
										imagemConferirCEPAtivo.style.display = '';
										imagemConferirCEPInativo.style.display = 'none';
										cepAuxiliar.value = cepEncontrado[0];

									}
								} else {
									chaveCep.value = "";
									cepAuxiliar.value = "";
									limparDialog<c:out value="${param.idCampoCep}" default=""/>();
								}
								//
								document.getElementById('<c:out value="${param.idCampoCep}" default=""/>').onblur = funcaoOnblur
								pesquisandoCep = false
							});
				}else{
					pesquisandoCep = false
				}
			}else{
				console.log('nao faz nada')
			}
		}


	}

	function exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(exibir, selecionouCepNoGrid, chamadaInicial, acao) {

		if ($('#fluxoPesquisa').val() == 'true'){
			exibirEnderecoPersquisa<c:out value="${param.idCampoCep}" default=""/>(exibir, selecionouCepNoGrid, chamadaInicial)
			return
		}

		if ($("#chavePrimaria").val()!= ''){
			exibirEnderecoAlteracao<c:out value="${param.idCampoCep}" default=""/>(exibir, selecionouCepNoGrid, chamadaInicial, acao)
			return
		}
		<c:choose>
			<c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
				var cep = document.getElementById('${param.idCampoCep}');
				var nomeCampoCep = document.getElementById('${param.idCampoCep}').getAttribute('name');
			</c:when>
		</c:choose>

		var imagemConferirCEPAtivo = document.getElementById("imagemConferirCEPAtivo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var imagemConferirCEPInativo = document.getElementById("imagemConferirCEPInativo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var logradouroPopup = document.getElementById("logradouroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var bairroPopup = document.getElementById("bairroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var cidadePopup = document.getElementById("cidadePopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var ufCepPopup = document.getElementById("ufCepPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var chaveCep = document.getElementById("chaveCep");
		var cepAuxiliar = document.getElementById("cepAuxiliar").value;

		var linkConferirCEP = document.getElementById("linkConferirCEP"+'<c:out value="${param.idCampoCep}" default=""/>');

		if (cep != undefined && cep.value[8] != '_' && trim(cep.value) != '') {
			imagemConferirCEPAtivo.src="imagens/cep_ativo.gif";

		if(cep.value != cepAuxiliar) {
			chaveCep.value = "";
		}

			if (!pesquisandoCep){
				pesquisandoCep = true
				if(chaveCep.value == "") {

					var valorCep = cep.value.replace(/_/gi,'')
					if(valorCep.length == 9){

						AjaxService.consultarEndereco(cep.value,
								function(endereco) {
									$("#ceps${param.idCampoCep}").html('');
									if(endereco != null) {
										var tamanhoCeps = 0;
										var cepEncontrado;
										var cepKey;
										for (key in endereco){
											tamanhoCeps++;
											if(tamanhoCeps > 1) {
												break;
											}
											cepEncontrado = endereco[key];
											cepKey = key;
										}

										if(tamanhoCeps > 1) {
											if(exibir) {
												alert("O cep possui mais de um endere�o.");
											}
											limparDialog<c:out value="${param.idCampoCep}" default=""/>();

											var ceps = document.getElementById("ceps"+'${param.idCampoCep}');
											ceps  = clearOptionsFast(ceps);

											for (key in endereco){

												var cepEndereco = endereco[key];
												var enderecoFormatado = cepEndereco[1] + ", " + cepEndereco[2] + ", " + cepEndereco[0];
												var novaOpcao = new Option(enderecoFormatado, key);
												novaOpcao.title = endereco[key];
												ceps.options[ceps.length] = novaOpcao;
											}
											animatedcollapse.show('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
											exibirElemento('resultadoPesquisarCep'+'<c:out value='${param.idCampoCep}' default=""/>');
											$("#linkPesquisarCEP").focus();
										} else {
											chaveCep.value = cepKey;

											if (cepEncontrado[1].indexOf("null") == -1){
												logradouroPopup.innerHTML = cepEncontrado[1];
											}else{
												logradouroPopup.innerHTML = "";
											}

											if (cepEncontrado[2] != null){
												bairroPopup.innerHTML = cepEncontrado[2];
											}else{
												bairroPopup.innerHTML = "";
											}

											cidadePopup.innerHTML = cepEncontrado[3];
											ufCepPopup.innerHTML = cepEncontrado[4];
											linkConferirCEP.style.display = 'inline';
											imagemConferirCEPAtivo.style.display = '';
											imagemConferirCEPInativo.style.display = 'none';
											cepAuxiliar.value = cepEncontrado[0];
											$("#linkPesquisarCEP").focus();
										}
										verificarChamadaQuadra(nomeCampoCep, cep);
									} else {
										chaveCep.value = "";
										cepAuxiliar.value = "";
										limparDialog<c:out value="${param.idCampoCep}" default=""/>();
										verificarChamadaQuadra(nomeCampoCep, cep);

									}


								});
						pesquisandoCep = false
					}
				} else {
					AjaxService.consultarCepPorId(chaveCep.value,
							function(cepEncontrado) {
								try{

									$(recuperaIdCepCorrente()).val(chaveCep.value)

								}catch(e){

								}
								if(cepEncontrado != null) {
									if (cepEncontrado[0].indexOf("null") == -1){
										logradouroPopup.innerHTML = cepEncontrado[0];
									}else{
										logradouroPopup.innerHTML = "";
									}

									if (cepEncontrado[1] != null){
										bairroPopup.innerHTML = cepEncontrado[1];
									}else{
										bairroPopup.innerHTML = "";
									}

									cidadePopup.innerHTML = cepEncontrado[2];
									ufCepPopup.innerHTML = cepEncontrado[3];
									linkConferirCEP.style.display = 'inline';
									imagemConferirCEPAtivo.style.display = '';
									imagemConferirCEPInativo.style.display = 'none';
								}
								pesquisandoCep = false
							});
				}
				pesquisandoCep = false
			}
		} else {
			limparDialog<c:out value="${param.idCampoCep}" default=""/>();
		}
	}

	function limparDialog<c:out value="${param.idCampoCep}" default=""/>() {

		$("#logradouroPopup"+'<c:out value="${param.idCampoCep}" default=""/>').html("");
		$("#bairroPopup"+'<c:out value="${param.idCampoCep}" default=""/>').html("");
		$("#cidadePopup"+'<c:out value="${param.idCampoCep}" default=""/>').html("");
		$("#ufCepPopup"+'<c:out value="${param.idCampoCep}" default=""/>').html("");


		$("#linkConferirCEP"+'<c:out value="${param.idCampoCep}" default=""/>').css("display","none");
		$("#imagemConferirCEPAtivo"+'<c:out value="${param.idCampoCep}" default=""/>').css("display","none");
		$("#imagemConferirCEPInativo"+'<c:out value="${param.idCampoCep}" default=""/>').css("display","inline");
		$("#imagemConferirCEPInativo"+'<c:out value="${param.idCampoCep}" default=""/>').css("display","");

	}

	function limparFormularioComponenteCep<c:out value='${param.idCampoCep}' default=""/>() {

		<c:choose>
		<c:when test="${param.idCampoLogradouro ne '' && param.idCampoLogradouro ne undefined}">
		var logradouro = document.getElementById('${param.idCampoLogradouro}');
		</c:when>
		<c:otherwise>
		var logradouro = document.getElementById("logradouro");
		</c:otherwise>
		</c:choose>

		<c:choose>
		<c:when test="${param.idCampoCidade ne '' && param.idCampoCidade ne undefined}">
		var cidade = document.getElementById('${param.idCampoCidade}');
		</c:when>
		<c:otherwise>
		var cidade = document.getElementById("cidade");
		</c:otherwise>
		</c:choose>

		<c:choose>
		<c:when test="${param.idCampoUf ne '' && param.idCampoUf ne undefined}">
		var uf = document.getElementById('${param.idCampoUf}');
		</c:when>
		<c:otherwise>
		var uf = document.getElementById("uf");
		</c:otherwise>
		</c:choose>

		logradouro.value = "";
		cidade.value = "";
		uf.value = "";
		$("#ceps${param.idCampoCep}").html('');
		$("#${param.idCampoCep}").val('')
	}

	function selecionarCep<c:out value='${param.idCampoCep}' default=""/>(obj) {

		<c:choose>
		<c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
		var cep = document.getElementById('${param.idCampoCep}');
		</c:when>
		</c:choose>



		document.getElementById("idCepRetornoBusca").value = obj.options[obj.selectedIndex].id;

		document.getElementById("chaveCep").value = obj.options[obj.selectedIndex].value;

		var enderecoSelecionado = obj.options[obj.selectedIndex].text;

		if ($('#fluxoPesquisa').val() == 'true'){

			return
		}

		var validadorTipoConsulta = <c:out value='${param.idCampoCep}' default=""/>.name;

		if (cep != undefined) {
			// cep.focus();
			cep.value = enderecoSelecionado.split(",")[2].replace(" ","");
			verificarChamadaQuadra(validadorTipoConsulta, cep);
		}
		document.getElementById("cepAuxiliar").value = enderecoSelecionado.split(",")[2].replace(" ","");

		// Mostrando asterisco para o campo n�mero na aba de faturamento de contrato.
		<c:choose>
		<c:when test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}">
		if (cep != undefined) {
			<c:out value="${param.mostrarAsterisco}"/>(cep.value,['endFisEntFatNumero']);
		}
		</c:when>
		</c:choose>
	}

	function consultarCep<c:out value='${param.idCampoCep}' default=""/>() {

		<c:choose>
		<c:when test="${param.idCampoLogradouro ne '' && param.idCampoLogradouro ne undefined}">
		var logradouro = document.getElementById('${param.idCampoLogradouro}');
		</c:when>
		<c:otherwise>
		var logradouro = document.getElementById("logradouro");
		</c:otherwise>
		</c:choose>

		<c:choose>
		<c:when test="${param.idCampoCidade ne '' && param.idCampoCidade ne undefined}">
		var cidade = document.getElementById('${param.idCampoCidade}');
		</c:when>
		<c:otherwise>
		var cidade = document.getElementById("cidade");
		</c:otherwise>
		</c:choose>

		<c:choose>
		<c:when test="${param.idCampoUf ne '' && param.idCampoUf ne undefined}">
		var uf = document.getElementById('${param.idCampoUf}');
		</c:when>
		<c:otherwise>
		var uf = document.getElementById("uf");
		</c:otherwise>
		</c:choose>

		var botaoPesquisarCEP = document.getElementById("botaoPesquisarCEP<c:out value='${param.idCampoCep}' default=""/>");
		var ceps = document.getElementById("ceps"+'${param.idCampoCep}');

		logradouro.value = trim(logradouro.value);
		cidade.value = trim(cidade.value);

		if (logradouro.value != "" && cidade.value != ""
				&& uf.options[uf.selectedIndex].value != "") {
			ceps  = clearOptionsFast(ceps);
			botaoPesquisarCEP.disabled = true;
			exibirElemento("imgProc<c:out value='${param.idCampoCep}' default=""/>");
			var answer = false;
			AjaxService.consultarCep(logradouro.value,cidade.value,uf.options[uf.selectedIndex].value,
					function(cep) {
						answer = cep;
						if (answer) {
							ocultarElemento("imgProc<c:out value='${param.idCampoCep}' default=""/>");
							botaoPesquisarCEP.disabled = false;
							var cepslength = 0;
							ceps.selectedIndex = -1;

							for (key in cep){
								var novaOpcao = new Option(cep[key], key);
								novaOpcao.id = key;
								novaOpcao.title = cep[key];
								ceps.options[cepslength] = novaOpcao;
								cepslength++;

								if (cepslength % 1000 == 0 ) {
									alert('A consulta retornou mais do que 1000 registros. Refine mais a consulta para um resultado mais preciso.')
									break;
								}
							}

							if(ceps.length == 0){
								var novaOpcao = new Option('Nenhum registro encontrado.', '');
								ceps.options[ceps.length] = novaOpcao;
								ceps.disabled = true;
							} else {
								ceps.disabled = false;
							}

							exibirElemento('resultadoPesquisarCep'+'<c:out value='${param.idCampoCep}' default=""/>');
						}

						//carregarQuadras(cep);

					});
		} else {
			var camposNaoInformados = '';
			var separador = '';
			if(logradouro.value == ""){
				camposNaoInformados += 'Logradouro';
				separador = ', ';
			}
			if(cidade.value == ""){
				camposNaoInformados += separador + 'Cidade';
				separador = ', ';
			}
			if(uf.options[uf.selectedIndex].value == "" ){
				camposNaoInformados += separador + 'UF';
			}

			//ocultarElemento('resultadoPesquisarCep'+'${param.idCampoCep}');
			alert('<fmt:message key="ERRO_NEGOCIO_CEP_PARAMETROS_NAO_INFORMADOS"><fmt:param>' + camposNaoInformados + '</fmt:param> </fmt:message>');
		}
	}

	animatedcollapse.addDiv('pesquisarCep'+'<c:out value='${param.idCampoCep}' default=""/>', 'fade=0,speed=400,persist=0,hide=1');

	function initCep() {
		var numeroCep = '${param.numeroCep}';
		if (numeroCep != "") {
			exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(false, false, true, this);
		}

		<c:choose>
		<c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
		var cep = document.getElementById('${param.idCampoCep}');
		</c:when>
		</c:choose>

		// Mostrando asterisco para o campo n�mero na aba de faturamento de contrato.
		<c:choose>
		<c:when test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}">
		if (cep != undefined && !$("#endFisEntFatNumero").attr("disabled")) {
			<c:out value="${param.mostrarAsterisco}"/>(cep.value,['endFisEntFatNumero']);
		}
		</c:when>
		</c:choose>
	}



	function verificaCepPreenchido<c:out value="${param.idCampoCep}" default=""/>(){
		var imagemConferirCEPAtivo = document.getElementById("imagemConferirCEPAtivo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var imagemConferirCEPInativo = document.getElementById("imagemConferirCEPInativo"+'<c:out value="${param.idCampoCep}" default=""/>');
		var logradouroPopup = document.getElementById("logradouroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var bairroPopup = document.getElementById("bairroPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var cidadePopup = document.getElementById("cidadePopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var ufCepPopup = document.getElementById("ufCepPopup"+'<c:out value="${param.idCampoCep}" default=""/>');
		var chaveCep = document.getElementById("chaveCep");
		var cepAuxiliar = document.getElementById("cepAuxiliar").value;
		var linkConferirCEP = document.getElementById("linkConferirCEP"+'<c:out value="${param.idCampoCep}" default=""/>');

		if ($('#${param.idCampoCep}').val() != ''){
			AjaxService.consultarEndereco($('#${param.idCampoCep}').val(),
					function(endereco) {
						$("#ceps${param.idCampoCep}").html('');
						if(endereco != null) {
							var tamanhoCeps = 0;
							var cepEncontrado;
							var cepKey;
							for (key in endereco){
								tamanhoCeps++;
								if(tamanhoCeps > 1) {
									break;
								}
								cepEncontrado = endereco[key];
								cepKey = key;
							}

							if(tamanhoCeps > 1) {

								limparDialog<c:out value="${param.idCampoCep}" default=""/>();

								var ceps = document.getElementById("ceps"+'${param.idCampoCep}');
								ceps  = clearOptionsFast(ceps);

								for (key in endereco){

									var cepEndereco = endereco[key];
									var enderecoFormatado = cepEndereco[1] + ", " + cepEndereco[2] + ", " + cepEndereco[0];
									var novaOpcao = new Option(enderecoFormatado, key);
									novaOpcao.title = endereco[key];
									ceps.options[ceps.length] = novaOpcao;
								}
								animatedcollapse.show('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
								exibirElemento('resultadoPesquisarCep'+'<c:out value='${param.idCampoCep}' default=""/>');
							} else {

								chaveCep.value = cepKey;
								if (cepEncontrado){
									if (cepEncontrado[1].indexOf("null") == -1){
										logradouroPopup.innerHTML = cepEncontrado[1];
									}else{
										logradouroPopup.innerHTML = "";
									}

									if (cepEncontrado[2] != null){
										bairroPopup.innerHTML = cepEncontrado[2];
									}else{
										bairroPopup.innerHTML = "";
									}

									cidadePopup.innerHTML = cepEncontrado[3];
									ufCepPopup.innerHTML = cepEncontrado[4];
									linkConferirCEP.style.display = 'inline';
									imagemConferirCEPAtivo.style.display = '';
									imagemConferirCEPInativo.style.display = 'none';
									cepAuxiliar.value = cepEncontrado[0];
								}
							}
						} else {
							chaveCep.value = "";
							cepAuxiliar.value = "";
							limparDialog<c:out value="${param.idCampoCep}" default=""/>();
						}
					});
		}
	}


	$("#cepImovel").removeClass("campoDesabilitado");
	$("#cepImovel").removeAttr("readonly");
	$("#numeroImovel").removeClass("campoDesabilitado");
	$("#numeroImovel").removeClass("campoDesabilitado");

function verificarChamadaQuadra(nomeCampoCep, cep){

	if(nomeCampoCep == "cepImovel"){
		carregarQuadras(cep);
	} else if(nomeCampoCep == "endFisEntFatCEP"){
		carregarEnderecoCepAbaFaturamento();
	}else{
		carregarQuadras2(cep);
	}
}

</script>

<c:set var="classeObrigatoriedadeCep" value="campoObrigatorioSimbolo"/>
<c:if test="${not empty param.styleClassCepObrigatorio}"><c:set var="classeObrigatoriedadeCep" value="${param.styleClassCepObrigatorio}"/></c:if>

<input type="hidden" id="chaveCep" name="chaveCep" value="${param.chaveCep}">
<input type="hidden" id="cepAuxiliar" name="cepAuxiliar" value="${param.numeroCep}">
<input type="hidden" id="idCepRetornoBusca" name="idCepRetornoBusca" value="">


<div class = labelCampo>

	<c:choose>
		<c:when test="${param.idCampoCep ne '' && param.idCampoCep ne undefined}">
			<c:if test="${param.cepObrigatorio}">

				<label class="rotulo<c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if>" id="rotuloCepTrue" for="${param.idCampoCep}"><span id="spanCep" class="${classeObrigatoriedadeCep}">* </span>CEP:</label></c:if><c:if test="${not param.cepObrigatorio}"><label class="rotulo <c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if>" id="rotuloCepFalse" for="${param.idCampoCep}">CEP:</label></c:if>
			<input   type="text" name="${param.idCampoCep}" id="${param.idCampoCep}" class="campoTexto cep" maxlength="9" value="${param.numeroCep}" onblur="<c:if test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}"><c:out value="${param.mostrarAsterisco}"/>(this.value,['endFisEntFatNumero']);</c:if> exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(true, false, false, this);" <c:if test="${param.cepDesabilitado eq true}">disabled="disabled"</c:if>>
		</c:when>
		<c:otherwise>
			<c:if test="${param.cepObrigatorio}">
				<label class="rotulo<c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if>" id="rotuloCepTrue" for="cep"><span class="${classeObrigatoriedadeCep}">* </span>CEP:</label></c:if><c:if test="${not param.cepObrigatorio}"><label class="rotulo<c:if test="${param.cepDesabilitado eq true}"> rotuloDesabilitado</c:if>" id="rotuloCepFalse" for="cep">CEP:</label></c:if>
			<input    type="text" name="cep" id="cep" class="cep" maxlength="9" value="${param.numeroCep}" onblur="<c:if test="${param.mostrarAsterisco ne null && param.mostrarAsterisco ne ''}"><c:out value="${param.mostrarAsterisco}"/>(this.value,['endFisEntFatNumero']);</c:if> exibirEndereco<c:out value="${param.idCampoCep}" default=""/>(true,false, false, this);">
		</c:otherwise>
	</c:choose>
</div>

<img id="imagemConferirCEPInativo<c:out value='${param.idCampoCep}' default=""/>" class="imagemConferirCEP" src="imagens/cep_inativo.gif" border="0" />
<a id="linkConferirCEP<c:out value='${param.idCampoCep}' default=""/>" class="linkConferirCEPConteiner" href="#" style="display:none">
	<img id="imagemConferirCEPAtivo<c:out value='${param.idCampoCep}' default=""/>" class="imagemConferirCEP" src="imagens/cep_ativo.gif" border="0" />
</a>

<div id="conferirCEP<c:out value='${param.idCampoCep}' default=""/>" class="conferirCEPpopup" title="Endere�o do CEP">
	<label id="rotuloLogradouro" class="rotulo">Logradouro:</label>
	<span class="itemDetalhamento" id="logradouroPopup<c:out value='${param.idCampoCep}' default=""/>"></span><br />
	<label id="rotuloBairro" class="rotulo">Bairro:</label>
	<span class="itemDetalhamento" id="bairroPopup<c:out value='${param.idCampoCep}' default=""/>"></span><br />
	<label id="rotuloCidade" class="rotulo">Cidade:</label>
	<span class="itemDetalhamento" id="cidadePopup<c:out value='${param.idCampoCep}' default=""/>"></span><br />
	<label id="rotuloUF" class="rotulo">UF:</label>
	<span class="itemDetalhamento" id="ufCepPopup<c:out value='${param.idCampoCep}' default=""/>"></span><br /><br />
</div>
<c:if test="${param.cepDesabilitado ne true}">
<a class="linkPesquisaAvancada linkPesquisarCEPDetalhe" id="linkPesquisarCEP" href="#" rel="toggle[pesquisarCep<c:out value='${param.idCampoCep}' default=""/>]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pesquisar CEP <img src="imagens/setaBaixo.png" border="0" /></a>
</c:if>
<fieldset id="pesquisarCep<c:out value='${param.idCampoCep}' default=""/>" class="pesquisarCEP" style="display: none">
	<fieldset id="pesquisarCepCampos">
		<a class="linkHelp" href="<help:help>/consultandocep.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		<c:choose>	
			<c:when test="${param.idCampoLogradouro ne '' && param.idCampoLogradouro ne undefined}">
				<label class="rotulo campoObrigatorio" id="rotuloLogradouro" for="${param.idCampoLogradouro}"><span class="campoObrigatorioSimboloCEP">*</span>Logradouro:</label>
				<input class="campoTexto" type="text" name="${param.idCampoLogradouro}" id="${param.idCampoLogradouro}" size="27" maxlength="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
			</c:when>
			<c:otherwise>
				<label class="rotulo campoObrigatorio" id="rotuloLogradouro" for="logradouro"><span class="campoObrigatorioSimboloCEP">* </span>Logradouro:</label>
				<input class="campoTexto" type="text" name="logradouro" id="logradouro" size="27" maxlength="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when test="${param.idCampoCidade ne '' && param.idCampoCidade ne undefined}">
				<label class="rotulo campoObrigatorio" id="rotuloCidade" for="${param.idCampoCidade}"><span class="campoObrigatorioSimboloCEP">* </span>Cidade:</label>
				<input class="campoTexto" type="text" name="${param.idCampoCidade}" id="${param.idCampoCidade}" size="27" maxlength="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
			</c:when>
			<c:otherwise>
				<label class="rotulo campoObrigatorio" id="rotuloCidade" for="cidade"><span class="campoObrigatorioSimboloCEP">* </span>Cidade:</label>
				<input class="campoTexto" type="text" name="cidade" id="cidade" size="27" maxlength="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when test="${param.idCampoUf ne '' && param.idCampoUf ne undefined}">
				<c:set value="${param.idCampoUf}" var="campouf" ></c:set>
			</c:when>
			<c:otherwise>
				<c:set value="uf" var="campouf" ></c:set>
			</c:otherwise>
		</c:choose>

		<label class="rotulo campoObrigatorio" id="rotuloUF" for="<c:out value="${campouf}"></c:out>"><span class="campoObrigatorioSimboloCEP">* </span>UF:</label>
		<select id="<c:out value="${campouf}"></c:out>" name="<c:out value="${campouf}"></c:out>" class="campoSelect rotuloUFCss">
			<option value="">Selecione</option>
			<option value="AC">AC</option>
			<option value="AL">AL</option>
			<option value="AM">AM</option>
			<option value="AP">AP</option>
			<option value="BA">BA</option>
			<option value="CE">CE</option>
			<option value="DF">DF</option>
			<option value="ES">ES</option>
			<option value="GO">GO</option>
			<option value="MA">MA</option>
			<option value="MG">MG</option>
			<option value="MS">MS</option>
			<option value="MT">MT</option>
			<option value="PA">PA</option>
			<option value="PB">PB</option>
			<option value="PE">PE</option>
			<option value="PI">PI</option>
			<option value="PR">PR</option>
			<option value="RJ">RJ</option>
			<option value="RN">RN</option>
			<option value="RO">RO</option>
			<option value="RR">RR</option>
			<option value="RS">RS</option>
			<option value="SC">SC</option>
			<option value="SE">SE</option>
			<option value="SP">SP</option>
			<option value="TO">TO</option>
		</select>

		<hr class="linhaSeparadoraCEP" />
		<input id="botaoLimparCEP" class="bottonRightCol" type="button" value="Limpar Campos" onclick="limparFormularioComponenteCep<c:out value='${param.idCampoCep}' default=""/>();">
		<img style="display:none" class="imgProc" id="imgProc<c:out value='${param.idCampoCep}' default=""/>" alt="Pesquisando..." title="Pesquisando..." src="<c:url value="/imagens/indicator.gif"/>">
		<input id="botaoPesquisarCEP<c:out value='${param.idCampoCep}' default=""/>" class="bottonRightCol botaoPesquisarCEP" name="Pesquisar" value="Pesquisar CEP" type="button" onclick="consultarCep<c:out value='${param.idCampoCep}' default=""/>();">
		<p class="legendaCEP"><span class="campoObrigatorioSimboloCEP">* </span>campos obrigat�rios apenas para pesquisar CEPs.</p>
	</fieldset>

	<fieldset class="conteinerResultadoPesquisarCep" id="resultadoPesquisarCep<c:out value='${param.idCampoCep}' default=""/>" style="display: none">
		<label class="rotulo rotuloCepsResultados" for="ceps<c:out value='${param.idCampoCep}' default=""/>">Resultados:</label>
		<select class="cepsLista" id="ceps<c:out value='${param.idCampoCep}' default=""/>" name="ceps<c:out value='${param.idCampoCep}' default=""/>" onchange="selecionarCep<c:out value="${param.idCampoCep}" default=""/>(this); exibirEndereco<c:out value='${param.idCampoCep}' default=""/>(true, true, false);" size="10">
		</select>
	</fieldset>
</fieldset>

<script>
	$(document).ready(function(){
		setTimeout(function(){
			$("#cepImovel").removeClass("campoDesabilitado");
			$("#cepImovel").removeAttr("readonly");
			$("#numeroImovel").removeClass("campoDesabilitado");
			$("#numeroImovel").removeClass("campoDesabilitado");
		},1500)
	});
</script>

