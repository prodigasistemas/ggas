<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Incluir Zeis<a href="<help:help>/zeisinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script language="javascript">

    $(document).ready(function(){

         var max = 0;

          $('.rotulo').each(function(){
                  if ($(this).width() > max)
                     max = $(this).width();   
              });
          $('.rotulo').width(max);
         
    });
    
   function limpar(){
	   limparFormularios(zeisForm);
   }
    
    function salvar(){

        var url = 'incluirZeis';
        
        submeter('zeisForm', url);

    }

    function cancelar(){    
    	location.href='exibirPesquisaZeis';
    }
    
</script>

<form method="post" id="zeisForm" name="zeisForm" enctype="multipart/form-data" action="incluirZeis">
<input name="postBack" type="hidden" id="postBack" value="true">
<input id="habilitado" name="habilitado" type="hidden" value="${true}">

<fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
        
        	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descrição:</label>
       		<input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${zeis.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        
        	<label class="rotulo">Descrição Abreviada:</label>
        
       		<input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${zeis.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
             
              
              
         <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigatórios</p>	
            
        </fieldset>
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">


       </fieldset>
    </fieldset>
        
	
	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limpar();">
	    <input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" id="botaoSalvar" onclick="javascript:salvar();">
	</fieldset>
</form>
