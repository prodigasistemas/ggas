<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>

<h1 class="tituloInterno">Pesquisar Setor Comercial<a href="<help:help>/consultadossetorescomerciais.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" id="setorComercialForm" name="setorComercialForm" action="pesquisarSetoresComerciais"> 

<script language="javascript">

	
		$(document).ready(function(){

	   	$("#idUnidadeFederacao").keydown(function() {
		   	if(this.value!=-1){
		   		carregarMunicipios(this);
			}	   		
		});
		
	});	
	
	function removerSetorComercial(){
		
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('setorComercialForm', 'removerSetorComercial');
			}
	    }
	}

	function atualizarSetorComercial(chave){
		document.forms[0].chavePrimaria.value = chave;
		var habilitado = document.forms[0].habilitado.value;
		submeter("setorComercialForm", "exibirAtualizacaoSetorComercial?habilitado="+habilitado);
	}

	function carregarMunicipios(){
		var codUnidadeFederacao = document.forms[0].idUnidadeFederacao.value;
		var selectMunicipio = document.getElementById("idMunicipio");

      	selectMunicipio.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectMunicipio.options[selectMunicipio.length] = novaOpcao;
        
      	if (codUnidadeFederacao != "-1") {
      		selectMunicipio.disabled = false;

      		AjaxService.consultarMunicipiosPorUnidadeFederativa( codUnidadeFederacao, 
    				{callback: function(listaMunicipio) {            		      		         		
                    	for (key in listaMunicipio){
                        	var novaOpcao = new Option(key, listaMunicipio[key]);
                        	selectMunicipio.options[selectMunicipio.length] = novaOpcao;
                        }
                    } , async:false}
                );
      		
      	} else {
      		selectMunicipio.disabled = true;
      	}
	}
	
	function limparFormulario() {		
		document.forms[0].descricao.value = '';
		document.forms[0].codigo.value = '';
		document.forms[0].idGerenciaRegional.value = '-1';
		document.forms[0].idUnidadeFederacao.value = '-1';
		document.forms[0].idLocalidade.value = '-1';		
		document.forms[0].idMunicipio.value = '-1';
		document.forms[0].habilitado[0].checked = true;
	}
	
	function carregarLocalidades(elem) {	
		var codGerenciaRegional = elem.value;
      	var selectLocalidades = document.getElementById("idLocalidade");
    
      	selectLocalidades.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
              	
      	AjaxService.consultarLocalidadesPorGerenciaRegional( codGerenciaRegional, 
          	function(localidades) {            		      		         		
              	for (key in localidades){
                  	var novaOpcao = new Option(localidades[key], key);
                      selectLocalidades.options[selectLocalidades.length] = novaOpcao;
                }
            }
        );      				
	}
	
	function incluir() {		
		location.href = '<c:url value="/exibirInsercaoSetorComercial"/>';
	}
	
	function detalharSetorComercial(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("setorComercialForm", "exibirDetalhamentoSetorComercial");
	}

	function alterarSetorComercial(chave) {
	
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			document.forms[0].postBack.value = false;
			submeter("setorComercialForm", "exibirAtualizacaoSetorComercial");
	    }
	}

	addLoadEvent(init);
	
</script>

<input name="acao" type="hidden" id="acao" value="pesquisarSetoresComerciais">
<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
<input name="postBack" type="hidden" id="postBack" value="true">

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisaSetorComercialCol1" class="coluna2">
		<label class="rotulo" id="rotuloGerenciaRegional" for="idGerenciaRegional">Ger�ncia Regional:</label>
		<select name="gerenciaRegional" id="idGerenciaRegional" class="campoSelect" onchange="carregarLocalidades(this)">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
				<option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${setorComercialForm.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${gerenciaRegional.nome}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
		<label class="rotulo" id="rotuloLocalidade" for="idLocalidade">Localidade:</label>
		<select name="localidade" id="idLocalidade" class="campoSelect" >
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaLocalidade}" var="localidade">
				<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${setorComercialForm.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localidade.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
		<label class="rotulo" id="rotuloCodigoSetorComercial" for="codigo">C�digo do Setor Comercial:</label>
		<input class="campoTexto" type="text" name="codigo" id="codigo" maxlength="6" size="6" tabindex="2" value="${setorComercialForm.codigo}" onkeypress="return formatarCampoCodigoSetorComercial(event);"/>
	</fieldset>
	
	<fieldset id="pesquisaSetorComercialCol2" class="colunaFinal2">
		<label class="rotulo" id="rotuloNomeSetorComercial" for="descricao">Nome do Setor Comercial:</label>
		<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="45" tabindex="3" value="${setorComercialForm.descricao}" onblur="validarCampoNomeSetorComercial(this)" onkeyup="validarCampoNomeSetorComercial(this)" onkeypress="return formatarCampoNomeSetorComercial(event);"/><br />
		<label class="rotulo" id="rotuloMunicipio" for="idUnidadeFederacao">Unidade Federativa:</label>
		<select name="unidadeFederativa" id="idUnidadeFederacao" class="campoSelect" onchange="carregarMunicipios();" >
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaUnidadeFederacao}" var="unidadeFederacao">
				<option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${setorComercialForm.unidadeFederativa.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadeFederacao.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
		<label class="rotulo" id="rotuloMunicipio" for="idMunicipio">Municipio:</label>
		<select name="municipio" id="idMunicipio" class="campoSelect">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaMunicipio}" var="municipio">
				<option value="<c:out value="${municipio.chavePrimaria}"/>" <c:if test="${setorComercialForm.municipio.chavePrimaria == municipio.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${municipio.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select>
	    <label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${setorComercialForm.habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${setorComercialForm.habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty setorComercialForm.habilitado}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Todos</label>
	</fieldset>
	
	 <fieldset class="conteinerBotoesPesquisarDir">
	 	<vacess:vacess param="pesquisarSetoresComerciais">
    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
    	</vacess:vacess>		
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>
</fieldset>

	<c:if test="${listaSetorComercial ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaSetorComercial" sort="list" id="setorComercial" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarSetoresComerciais">
	        <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${setorComercial.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo" style="auto">
		     	<c:choose>
					<c:when test="${setorComercial.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	        <display:column sortable="true" title="C�digo do Setor<br />Comercial" sortProperty="codigo">
	        	<a href='javascript:detalharSetorComercial(<c:out value='${setorComercial.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	        		<c:out value='${setorComercial.codigo}'/>
	        	</a>
	        </display:column> 
	        <display:column sortable="true" titleKey="SETOR_COMERCIAL_DESCRICAO" sortProperty="descricao">
	        	<a href='javascript:detalharSetorComercial(<c:out value='${setorComercial.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	        		<c:out value='${setorComercial.descricao}'/>
	        	</a>
	        </display:column>
	        <display:column sortable="true" titleKey="SETOR_COMERCIAL_LOCALIDADE" sortProperty="localidade.descricao">
	        	<a href='javascript:detalharSetorComercial(<c:out value='${setorComercial.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	        		<c:out value='${setorComercial.localidade.descricao}'/>
	        	</a>
	        </display:column>
	        <display:column sortable="true" titleKey="SETOR_COMERCIAL_MUNICIPIO" sortProperty="municipio.descricao">
	        	<a href='javascript:detalharSetorComercial(<c:out value='${setorComercial.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	        		<c:out value='${setorComercial.municipio.descricao}'/>
	        	</a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaSetorComercial}">
  			<vacess:vacess param="exibirAtualizacaoSetorComercial">
				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarSetorComercial()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerSetorComercial">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerSetorComercial()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInsercaoSetorComercial">
   			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
   		</vacess:vacess>
	</fieldset>
</form:form>