<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Pesquisar Quadra<a class="linkHelp" href="<help:help>/consultadasquadras.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" action="pesquisarQuadra" name="quadraForm" id="quadraForm">
	
	<script language="javascript">

		$(document).ready(function(){
			$("#cep").inputmask("99999-999",{placeholder:"_"});
		});

		function removerQuadra(){		
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('quadraForm', 'removerQuadra');
				}
		    }
		}
			
		function limparFormulario(){
			document.getElementById("idSetorComercial").value = "-1";
			document.getElementById("numeroQuadra").value = "";
			document.getElementById("idPerfilQuadra").value = "-1";
			document.getElementById("idZeis").value = "-1";
			document.getElementById("cep").value = "";
			document.forms[0].habilitado[0].checked = true;
		}
	
		function pesquisarQuadras() {
			$("#botaoPesquisar").attr('disabled','disabled');
			submeter('quadraForm', 'pesquisarQuadra');
		}
		
		function incluir() {
			limparFormulario();
			location.href = '<c:url value="/exibirInsercaoQuadra"/>';
		}
		
		function alterarQuadra(chave) {		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("quadraForm", "exibirAtualizacaoQuadra");
		    }
		}	
		
		function detalharQuadra(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("quadraForm", "exibirDetalhamentoQuadra");
		}

	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarQuadra">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarQuadra" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaQuadraCol1" class="coluna">
			<label class="rotulo" id="rotuloSetorComercial" for="idSetorComercial">Setor Comercial:</label>
			<select name="setorComercial" id="idSetorComercial" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaSetorComercial}" var="setorComercial">
					<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${quadra.setorComercial.chavePrimaria == setorComercial.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${setorComercial.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
			<label class="rotulo" id="rotuloQuadra" for="numeroQuadra">Quadra:</label>
			<input class="campoTexto" type="text" name="numeroQuadra" id="numeroQuadra" maxlength="9" size="9" value="${quadra.numeroQuadra}" />
			
			<label class="rotulo" id="rotuloCep" for="cep">CEP:</label>
			<input class="campoTexto cep" type="text" name="cep" id="cep" maxlength="9" size="9" value="${cep}" />
		</fieldset>
		
		<fieldset id="pesquisaQuadraCol2" class="colunaFinal">
		<label class="rotulo" id="rotuloPerfilQuadra" for="idPerfilQuadra">Perfil Quadra:</label>
			<select name="perfilQuadra" id="idPerfilQuadra" class="campoSelect">
			   	<option value="-1">Selecione</option>
				<c:forEach items="${listaPerfilQuadra}" var="perfilQuadra">
					<option value="<c:out value="${perfilQuadra.chavePrimaria}"/>" <c:if test="${quadra.perfilQuadra.chavePrimaria == perfilQuadra.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${perfilQuadra.descricao}"/>
					</option>		
			    </c:forEach>	
		   </select><br />
			<label class="rotulo" id="rotuloZeis" for="idZeis">Zeis:</label>
			<select name="zeis" id="idZeis" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaZeis}" var="zeis">
					<option value="<c:out value="${zeis.chavePrimaria}"/>" <c:if test="${quadra.zeis.chavePrimaria == zeis.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${zeis.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    <label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
	    </fieldset>
	    
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<vacess:vacess param="pesquisarQuadra">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarQuadras()">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario()">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaQuadras ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<a name="pesquisaQuadraResultados"></a>
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaQuadras" sort="list" id="quadra" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarQuadra">
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${quadra.chavePrimaria}">
	     	</display:column>
			<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${quadra.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	        <display:column sortable="true" sortProperty="numeroQuadra"  title="Quadra" style="width: 60px">        
				<a href="javascript:detalharQuadra(<c:out value='${quadra.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value="${quadra.numeroQuadra}"/>
				</a>
	        </display:column>
	        <display:column sortable="true" sortProperty="perfilQuadra.descricao"  title="Perfil Quadra" style="width: 90px">
				<a href="javascript:detalharQuadra(<c:out value='${quadra.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${quadra.perfilQuadra.descricao}'/>
				</a>
	        </display:column>
	        <display:column sortable="true" sortProperty="setorComercial.codigo" title="C�digo do Setor" style="width: 60px">
				<a href="javascript:detalharQuadra(<c:out value='${quadra.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${quadra.setorComercial.codigo}'/>
				</a>
	        </display:column>
	        <display:column sortable="true" sortProperty="setorComercial.descricao" title="Setor Comercial">
	        	<a href="javascript:detalharQuadra(<c:out value='${quadra.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${quadra.setorComercial.descricao}'/>
				</a>
	        </display:column> 
	        <display:column sortable="true" sortProperty="zeis.descricao" title="Zeis">
	        	<a href="javascript:detalharQuadra(<c:out value='${quadra.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${quadra.zeis.descricao}'/>
				</a>
	        </display:column> 
	    </display:table>
	</c:if>
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaQuadras}">
			<vacess:vacess param="exibirAtualizacaoQuadra">
				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarQuadra()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerQuadra">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerQuadra()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInsercaoQuadra">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>
	<token:token></token:token>
</form>