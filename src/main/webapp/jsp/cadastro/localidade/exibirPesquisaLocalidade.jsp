<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Localidade<a href="<help:help>/consultadaslocalidades.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarLocalidade" id="localidadeForm"> 

	<script language="javascript">

	$(document).ready(function(){

	   	$("#idGerenciaRegional").keydown(function() {
		   	if(this.value!=-1){
		   		carregarUnidadesNegocio(this);
			}	   		
		});
		
		
	});
	
	function removerLocalidade(){
		
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('localidadeForm', 'removerLocalidade');
			}
	    }
	}
		function limparFormulario() {		
			document.forms[0].descricao.value = "";
			document.forms[0].idGerenciaRegional.value = '-1';
			document.forms[0].idUnidadeNegocio.value = '-1';
			document.forms[0].habilitado[0].checked = true;		
		}
	
		function carregarUnidadesNegocio(elem){
			var codGerenciaRegional = elem.value;
	      	var selectUnidades = document.getElementById("idUnidadeNegocio");
	    
	      	selectUnidades.length=0;
	      	var novaOpcao = new Option("Selecione","-1");
	        selectUnidades.options[selectUnidades.length] = novaOpcao;
	        
	      	if (codGerenciaRegional != "-1") {
	      		selectUnidades.disabled = false;      		
	        	AjaxService.consultarUnidadeNegocioPorGerenciaRegional( codGerenciaRegional, 
	            	function(unidades) {            		      		         		
	                	for (key in unidades){
	                    	var novaOpcao = new Option(unidades[key], key);
	                        selectUnidades.options[selectUnidades.length] = novaOpcao;
	                    }
	                }
	            );
	      	} else {
				selectUnidades.disabled = true;      	
	      	}
		}
		
		function incluir() {		
			location.href = '<c:url value="/exibirInsercaoLocalidade"/>';
		}
		
		function alterarLocalidade(chave) {
		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("localidadeForm", "exibirAtualizacaoLocalidade");
		    }
		}
		
		function detalharLocalidade(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("localidadeForm", "exibirDetalhamentoLocalidade");
		}
			
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarLocalidade">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarLocalidadeCol1" class="coluna">
			<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" maxlength="30" size="45" tabindex="3" value="${localidadeForm.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
			
			<label class="rotulo" id="rotuloGerenciaRegional" for="idGerenciaRegional">Ger�ncia Regional:</label>
			<select class="campoSelect" name="gerenciaRegional" id="idGerenciaRegional" onchange="carregarUnidadesNegocio(this);">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
					<option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${localidadeForm.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${gerenciaRegional.nome}"/>
					</option>
			    </c:forEach>	
		    </select>
	    </fieldset>
		
		<fieldset id="pesquisarLocalidadeCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloUnidadeNegocio" for="idUnidadeNegocio" >Unidade Neg�cio:</label>
			<select class="campoSelect" name="unidadeNegocio" id="idUnidadeNegocio" disabled="disabled">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadeNegocio}" var="unidadeNegocio">
					<option value="<c:out value="${unidadeNegocio.chavePrimaria}"/>" <c:if test="${localidadeForm.unidadeNegocio.chavePrimaria == unidadeNegocio.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidadeNegocio.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    
		    <label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
	    <fieldset class="conteinerBotoesPesquisarDir">
	    	<vacess:vacess param="pesquisarLocalidade">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>

<c:if test="${listaLocalidade ne null}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="listaLocalidade" sort="list" id="localidade" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarLocalidade">
		<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
      		<input type="checkbox" name="chavesPrimarias" value="${localidade.chavePrimaria}">
     	</display:column>
     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${localidade.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
        <display:column sortable="true" titleKey="LOCALIDADE_DESCRICAO">
            <a href="javascript:detalharLocalidade(<c:out value='${localidade.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
            	<c:out value="${localidade.descricao}"/>
            </a>
        </display:column>
		<display:column sortable="true" titleKey="LOCALIDADE_GERENCIA_REGIONAL" style="width: 250px">
			<a href="javascript:detalharLocalidade(<c:out value='${localidade.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${localidade.gerenciaRegional.nome}"/>
			</a>
		</display:column>
        <display:column sortable="true" titleKey="LOCALIDADE_UNIDADE_NEGOCIO" style="width: 150px">
        	<a href="javascript:detalharLocalidade(<c:out value='${localidade.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${localidade.unidadeNegocio.descricao}"/>
			</a>
        </display:column>
        <display:column sortable="true" titleKey="LOCALIDADE_INFORMATIZADA" style="width: 90px">
        	<a href="javascript:detalharLocalidade(<c:out value='${localidade.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	        	<c:choose>
	        		<c:when test="${localidade.informatizada}">Sim</c:when>
	        		<c:otherwise>N�o</c:otherwise>
	        	</c:choose>
	        </a>
        </display:column>
    </display:table>    
</c:if>

	<fieldset class="conteinerBotoes">
    	<c:if test="${not empty listaLocalidade}">
    		<vacess:vacess param="exibirAtualizacaoLocalidade">
   				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarLocalidade()" type="button">
   			</vacess:vacess>
   			<vacess:vacess param="removerLocalidade">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerLocalidade()" type="button">
			</vacess:vacess>
		</c:if>
			<vacess:vacess param="exibirInsercaoLocalidade">		
				<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
			</vacess:vacess>
	</fieldset>
</form:form>