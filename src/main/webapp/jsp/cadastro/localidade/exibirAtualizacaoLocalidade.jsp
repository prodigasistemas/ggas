<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>

	function limparFormulario(){
		document.forms[0].descricao.value = "";
		document.forms[0].idGerenciaRegional.value = "-1";
		document.forms[0].cep.value = "";
		document.forms[0].chaveCep.value = "";
		document.forms[0].numeroEndereco.value = "";
		document.forms[0].complementoEndereco.value = "";
		document.forms[0].fone.value = "";
		document.forms[0].ramalFone.value = "";
		document.forms[0].fax.value = "";
		document.forms[0].email.value = "";
		document.forms[0].idUnidadeNegocio.value = "-1";
		document.forms[0].idLocalidadeClasse.value = "-1";
		document.forms[0].idLocalidadePorte.value = "-1";
		document.forms[0].codigoCentroCusto.value = "";
		document.forms[0].informatizada.value = 'true';
		document.forms[0].idMedidorLocalArmazenagem.value = "-1";
		document.forms[0].dddTelefone.value = "";
		document.forms[0].dddFax.value = "";		
		
		document.forms[0].cep.value = "";
		document.forms[0].cidade.value = "";
		document.forms[0].uf.value = "";
		var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
		removeAllOptions(selectCep);
		animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
		limparDialog();	
	}
	
	function voltar() {
		submeter("localidadeForm", "pesquisarLocalidade");
	}
	
	function carregarUnidadesNegocio(elem){
		var codGerenciaRegional = elem.value;
      	var selectUnidades = document.getElementById("idUnidadeNegocio");
    
      	selectUnidades.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectUnidades.options[selectUnidades.length] = novaOpcao;
        
      	if (codGerenciaRegional != "-1") {
      		selectUnidades.disabled = false;      		
        	AjaxService.consultarUnidadeNegocioPorGerenciaRegional( codGerenciaRegional, 
            	function(unidades) {            		      		         		
                	for (key in unidades){
                    	var novaOpcao = new Option(unidades[key], key);
                        selectUnidades.options[selectUnidades.length] = novaOpcao;
                    }
                }
            );
      	} else {
			selectUnidades.disabled = true;      	
      	}
	}
	
	function exibirPopupPesquisaClienteSuperior() {
		popup = window.open('exibirPesquisaClientePopup?pessoaFisica=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function selecionarCliente(idSelecionado){
		AjaxService.consultarClientePorId(idSelecionado, 
          	function(cliente) {
              	var idCliente = document.getElementById("idCliente");
              	var nomeCliente = document.getElementById("nomeCliente");
              	var nomeClienteDes = document.getElementById("nomeClienteDes");
              	
              	idCliente.value = idSelecionado;
              	nomeCliente.value = cliente;
              	nomeClienteDes.value = cliente;
            }
        );
	}
	
</script>

<h1 class="tituloInterno">Alterar Localidade<a href="<help:help>/localidadeinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar uma Localidade, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>
<form:form method="post" action="atualizarLocalidade" id="localidadeForm" name="localidadeForm">

<input name="acao" type="hidden" id="acao" value="atualizarLocalidade">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="<c:out value='${localidadeForm.chavePrimaria}'/>">
<input name="versao" type="hidden" id="versao" value="<c:out value='${localidadeForm.versao}'/>">
	
<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="localidadeCol1" class="coluna">
		<label class="rotulo campoObrigatorio" id="rotuloCodigo" for="codigo"><span class="campoObrigatorioSimbolo">* </span>C�digo:</label>
		<span class="itemDetalhamento" id="atualizarLocalidadeCodigo"><c:out value="${localidadeForm.chavePrimaria}"/></span><br />
		<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
		<input class="campoTexto" type="text" name="descricao" maxlength="30" size="50" tabindex="3" value="${localidadeForm.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		<label class="rotulo" id="rotuloNumero" for="numeroEndereco">N�mero:</label>
		<input class="campoTexto" type="text" name="numeroEndereco" maxlength="5" size="10" tabindex="4" value="${localidadeForm.numeroEndereco}" onkeypress="return formatarCampoNumeroEndereco(event, this);">
		<fieldset class="exibirCep">
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="idCampoCep" value="cep"/>
				<jsp:param name="cepObrigatorio" value="false"/>
				<jsp:param name="numeroCep" value="${cep}"/>
				<jsp:param name="chaveCep" value="${localidade.endereco.cep.chavePrimaria}"/>
			</jsp:include>
		</fieldset>
		<label class="rotulo" id="rotuloComplemento" for="complementoEndereco" >Complemento:</label>
		<input class="campoTexto" type="text" name="complementoEndereco" maxlength="255" size="50" tabindex="5" value="${localidadeForm.complementoEndereco}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		<label class="rotulo" id="rotuloTelefone" for="fone" >Telefone:</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddTelefone" name="dddTelefone" tabindex="6" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event)"  value="${localidadeForm.dddTelefone}">
		<input class="campoTexto" type="text" name="fone" maxlength="8" size="10" tabindex="7" value="${localidadeForm.fone}" onkeypress="return formatarCampoInteiro(event)"><br />
		<label class="rotulo" class="rotulo" id="rotuloRamal" for="ramalFone" >Ramal:</label>
		<input class="campoTexto" type="text" name="ramalFone" maxlength="4" size="5" tabindex="8" value="${localidadeForm.ramalFone}" onkeypress="return formatarCampoInteiro(event)"><br />
		<label class="rotulo" id="rotuloFax" for="fax" >Fax:</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddFax" name="dddFax" maxlength="2" size="1" tabindex="9" onkeypress="return formatarCampoInteiro(event)"  value="${localidadeForm.dddFax}">
		<input class="campoTexto" type="text" name="fax" maxlength="9" size="10" tabindex="10" value="${localidadeForm.fax}" onkeypress="return formatarCampoInteiro(event)"><br />
		<label class="rotulo" id="rotuloEmail" for="email" >Email:</label>
		<input class="campoTexto" type="text" name="email" maxlength="80" size="50" tabindex="11" value="${localidadeForm.email}" onkeypress="return formatarCampoEmail(event);">
	</fieldset>
	
	<fieldset id="localidadeCol2" class="colunaFinal">
		<label class="rotulo campoObrigatorio" id="rotuloGerenciaRegional" for="idGerenciaRegional"><span class="campoObrigatorioSimbolo">* </span>Gerencia Regional:</label>
		<select name="gerenciaRegional" id="idGerenciaRegional" class="campoSelect" onchange="carregarUnidadesNegocio(this);" tabindex="12">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
				<option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${localidadeForm.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${gerenciaRegional.nome}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
		<label class="rotulo" id="rotuloUnidadeNegocio" for="idUnidadeNegocio">Unidade Neg�cio:</label>
		<select class="campoSelect" name="unidadeNegocio" id="idUnidadeNegocio" tabindex="13">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaUnidadeNegocio}" var="unidadeNegocio">
				<option value="<c:out value="${unidadeNegocio.chavePrimaria}"/>" <c:if test="${localidadeForm.unidadeNegocio.chavePrimaria == unidadeNegocio.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadeNegocio.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
	    <label class="rotulo campoObrigatorio" id="rotuloClasse" for="idLocalidadeClasse"><span class="campoObrigatorioSimbolo">* </span>Classe:</label>
		<select class="campoSelect" name="localidadeClasse" id="idLocalidadeClasse" tabindex="14">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaLocalidadeClasse}" var="localidadeClasse">
				<option value="<c:out value="${localidadeClasse.chavePrimaria}"/>" <c:if test="${localidadeForm.localidadeClasse.chavePrimaria == localidadeClasse.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localidadeClasse.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
	    <label class="rotulo campoObrigatorio" id="rotuloPorte" for="idLocalidadePorte"><span class="campoObrigatorioSimbolo">* </span>Porte:</label>
		<select class="campoSelect" name="localidadePorte" id="idLocalidadePorte" tabindex="15">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaLocalidadePorte}" var="localidadePorte">
				<option value="<c:out value="${localidadePorte.chavePrimaria}"/>" <c:if test="${localidadeForm.localidadePorte.chavePrimaria == localidadePorte.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localidadePorte.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
	    <label class="rotulo" id="rotuloCentroCusto" for="codigoCentroCusto">Centro de Custo:</label>
		<input class="campoTexto" type="text" name="codigoCentroCusto" maxlength="10" size="10" tabindex="16" value="${localidadeForm.codigoCentroCusto}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
	    <label class="rotulo campoObrigatorio" id="rotuloInformatizada" for="complementoEndereco"><span class="campoObrigatorioSimbolo">* </span>Informatizada?</label>
		<input class="campoRadio" type="radio" name="informatizada" value="true" <c:if test="${localidadeForm.informatizada == 'true'}">checked</c:if>/><label class="rotuloRadio" id="rotuloInformatizadaSim" >Sim</label>
		<input class="campoRadio" type="radio" name="informatizada" value="false" <c:if test="${localidadeForm.informatizada == 'false'}">checked</c:if>/><label class="rotuloRadio" id="rotuloInformatizadaNao" >N�o</label><br /><br />	
	   	<label class="rotulo" id="rotuloMedidorLocalArmazenagem" for="medidorLocalArmazenagem">Medidor Local Armazenagem:</label>
	   	<select class="campoSelect" name="medidorLocalArmazenagem" id="idMedidorLocalArmazenagem" tabindex="17">
    		<option value="-1">Selecione</option>
			<c:forEach items="${listaMedidorLocalArmazenagem}" var="medidorLocalArmazenagem">
				<option value="<c:out value="${medidorLocalArmazenagem.chavePrimaria}"/>" <c:if test="${localidadeForm.medidorLocalArmazenagem.chavePrimaria == medidorLocalArmazenagem.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${medidorLocalArmazenagem.descricaoAbreviada}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
	    <label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${localidadeForm.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
	   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${localidadeForm.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
    </fieldset>
    
   	<hr class="linhaSeparadora" />
	<fieldset class="localidadePesquisarCliente">
		<label class="rotulo" id="rotuloResponsavelSuperior" for="nomeResponsavelSuperiorDes">Gestor Respons�vel:</label>
		<input type="hidden" name="cliente" id="idCliente" value="${localidadeForm.cliente.chavePrimaria}">
		<input type="hidden" name="nomeCliente" id="nomeCliente" value="${localidadeForm.cliente.nome}">
		<input type="text" class="campoTexto" id="nomeClienteDes" name="nomeClienteDes"  maxlength="70" size="70" disabled="disabled" value="${localidadeForm.cliente.nome}">	
		<input name="Button" id="pesquisarClienteDocumentacao" class="bottonRightCol2" title="Pesquisar Gestor"  value="Pesquisar Gestor" onclick="exibirPopupPesquisaClienteSuperior();" type="button" style="margin:-5px 0 0 10px;">
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Localidade.</p>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="voltar();">
   	<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
   	<vacess:vacess param="atualizarLocalidade">
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="submit">
    </vacess:vacess>
</fieldset>
<token:token></token:token>
</form:form>   