<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<h1 class="tituloInterno">Alterar Setor Comercial<a href="<help:help>/setorcomercialinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar este Setor Comercial, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>


<form:form method="post" action="atualizarSetorComercial">

<script>
	
	function carregarLocalidades(elem) {	
		var codGerenciaRegional = elem.value;
      	var selectLocalidades = document.getElementById("idLocalidade");
    
      	selectLocalidades.length=0;
      	var novaOpcao = new Option("SELECIONE","-1");
        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
              		
		AjaxService.consultarLocalidadesPorGerenciaRegional( codGerenciaRegional, 
        	function(localidades) {            		      		         		
	        	for (key in localidades){
               		var novaOpcao = new Option(localidades[key], key);
                   	selectLocalidades.options[selectLocalidades.length] = novaOpcao;
               	}
           	}
       	);      	
	}

	function carregarMunicipios(elem){
		var codUnidadeFederacao = elem.value;
		var selectMunicipio = document.getElementById("idMunicipio");
	    
      	selectMunicipio.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectMunicipio.options[selectMunicipio.length] = novaOpcao;
        
      	if (codUnidadeFederacao != "-1") {
      		selectMunicipio.disabled = false;
      		      		
      		AjaxService.consultarMunicipiosPorUnidadeFederativa( codUnidadeFederacao, 
        		{callback: function(listaMunicipio) {            		      		         		
                    for (key in listaMunicipio){
                    	var novaOpcao = new Option(key, listaMunicipio[key]);
                    	selectMunicipio.options[selectMunicipio.length] = novaOpcao;
                	}
    			} , async:false}
            );
            
      	} else {
      		selectMunicipio.disabled = true;      	
      	}
	}
	
	function limpar(){
		document.forms[0].descricao.value = '';						
		document.forms[0].idUnidadeFederacao.value = '-1';
		document.forms[0].idMunicipio.value = '-1';
		document.forms[0].idMunicipio.disabled = true;
		
		document.forms[0].habilitado[0].checked = true;
		
	}
	
	function voltar(){
		submeter('setorComercialForm', 'pesquisarSetoresComerciais');
	}


</script>

<input name="acao" type="hidden" id="acao" value="atualizarSetorComercial">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${setorComercialForm.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${setorComercialForm.versao}">
<input name="localidade" type="hidden" value="${setorComercialForm.localidade.chavePrimaria}">
<input name="codigo" type="hidden" value="${setorComercialForm.codigo}">
<input name="descricaoLocalidade" type="hidden" value="${setorComercialForm.localidade.descricao}">  
<input name="gerenciaRegional" type="hidden" value="${setorComercialForm.localidade.gerenciaRegional.chavePrimaria}">
	                  
<fieldset id="setorComercial" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna">
		<label class="rotulo" id="rotuloLocalidade" for="descricaoLocalidade">Localidade:</label>
		<span class="itemDetalhamento" id="descricaoLocalidade"><c:out value="${setorComercialForm.localidade.descricao}"/></span><br />		
		<label class="rotulo" id="rotuloCodigoSetorComercial" for="codigo">C�digo do Setor Comercial :</label>
		<span class="itemDetalhamento" id="codigo"><c:out value="${setorComercialForm.codigo}"/></span><br class="quebraLinha2" />
		<label class="rotulo campoObrigatorio" id="rotuloNomeSetorComercial" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Nome do Setor Comercial:</label>
		<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="32" tabindex="3" value="${setorComercialForm.descricao}" onblur="validarCampoNomeSetorComercial(this);" onkeyup="validarCampoNomeSetorComercial(this);" onkeypress="return formatarCampoNomeSetorComercial(event);"/>
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo campoObrigatorio" id="rotuloMunicipio" for="idMunicipio"><span class="campoObrigatorioSimbolo">* </span>Unidade Federativa:</label>
		<select name="unidadeFederativa" id="idUnidadeFederacao" class="campoSelect" onchange="carregarMunicipios(this);" >
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaUnidadeFederacao}" var="unidadeFederacao">
				<option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${setorComercialForm.municipio.unidadeFederacao.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadeFederacao.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
		<label class="rotulo campoObrigatorio" id="rotuloMunicipio" for="idMunicipio"><span class="campoObrigatorioSimbolo2">* </span>Municipio:</label>
		<select name="municipio" id="idMunicipio" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaMunicipio}" var="municipio">
				<option value="<c:out value="${municipio.chavePrimaria}"/>" <c:if test="${setorComercialForm.municipio.chavePrimaria == municipio.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${municipio.descricao}"/>
				</option>		
			</c:forEach>	
		</select>
		<label class="rotulo" for="habilitado1">Indicador de Uso:</label>
		<input class="campoRadio" id="habilitado1" name="habilitado" value="true" type="radio" <c:if test="${setorComercialForm.habilitado == true}">checked="checked"</c:if>><label class="rotuloRadio" for="habilitado1">Ativo</label>
		<input class="campoRadio" id="habilitado2" name="habilitado" value="false" type="radio" <c:if test="${setorComercialForm.habilitado == false}">checked="checked"</c:if>><label class="rotuloRadio" for="habilitado2">Inativo</label>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para altera��o de Setor Comercial.</p>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="voltar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar();">
    <vacess:vacess param="atualizarSetorComercial">
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="submit">
    </vacess:vacess>
</fieldset>
</form:form>