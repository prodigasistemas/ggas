<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<h1 class="tituloInterno">Alterar Quadra<a class="linkHelp" href="<help:help>/quadrainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar uma Quadra, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form method="post" action="atualizarQuadra" name="quadraForm" id="quadraForm">


	<script>
	
		function carregarLocalidades(elem) {	
			var codGerenciaRegional = elem.value;
	      	var selectLocalidades = document.getElementById("idLocalidade");
	    
	      	selectLocalidades.length=0;
	      	var novaOpcao = new Option("Selecione","-1");
	        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
	        
	      	if (codGerenciaRegional != "-1") {
	      		selectLocalidades.disabled = false;       		
	        	AjaxService.consultarLocalidadesPorGerenciaRegional(codGerenciaRegional, 
	            	function(localidades) {            		      		         		
	                	for (key in localidades){
	                    	var novaOpcao = new Option(localidades[key], key);
	                        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
	                    }
	                    ordernarSelect(selectLocalidades);
	                }
	            );
	      	} else {
				selectLocalidades.disabled = true;	
	      	}      	
	      	carregarSetorComercial(selectLocalidades);
				
		} 
	
		function carregarSetorComercial(elem) {	
			var codLocalidade = elem.value;
	      	var selectSetores = document.getElementById("idSetorComercial");
	    
	      	selectSetores.length=0;
	      	var novaOpcao = new Option("Selecione","-1");
	        selectSetores.options[selectSetores.length] = novaOpcao;
	        
	      	if (codLocalidade != "-1") {      
	      		selectSetores.disabled = false;      				
	        	AjaxService.consultarSetorComercialPorLocalidade( codLocalidade, 
	            	function(setores) {            		      		         		
	                	for (key in setores){
	                    	var novaOpcao = new Option(setores[key], key);
	                        selectSetores.options[selectSetores.length] = novaOpcao;
	                    }
	                    ordernarSelect(selectSetores);
	                }
	            );
	      	} else {
				selectSetores.disabled = true;      	
      		}	
				
		}
		
	function limparFormulario(){
		document.forms[0].idGerenciaRegional.value = "-1";
		document.forms[0].idLocalidade.value = "-1";
		document.forms[0].idSetorComercial.value = "-1";
		document.forms[0].idPerfilQuadra.value = "-1";
		document.forms[0].idZeis.value = "-1";
		document.forms[0].numeroDeQuadra.value = "";
		document.forms[0].idZonaBloqueio.value = "-1";
		document.forms[0].idAreaTipo.value = "-1";
		document.forms[0].idSetorCensitario.value = "-1";
		document.forms[0].numeroFace.value = "";
		document.forms[0].cep.value = "";
		document.forms[0].chaveCep.value = "";
		var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
		removeAllOptions(selectCep);
		animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
		limparDialog();				
	
	}
	function verificarIds(){
		var idPerfilquadra = $('#perfilQuadraForm').val();
		var idSetorComercial = $('#setorComercialForm').val();
		var idZeis = $('#zeisForm').val();
			
		if(idPerfilquadra != ''){
			$('#perfilQuadra').val($('#perfilQuadraForm').val());
		} else {
			document.forms[0].idPerfilQuadra.value = "-1";
		}
		
		if(idSetorComercial != ''){
			$('#setorComercial').val($('#setorComercialForm').val());
		} else {
			document.forms[0].idSetorComercial.value = "-1";
		}
		
		if(idZeis != ''){
			$('#zeis').val($('#zeisForm').val());
		} else {
			document.forms[0].idZeis.value = "-1";
		}
	}
		
		function voltar() {
			verificarIds();
			$('#chavePrimaria').val('');
			$('#numeroDeQuadra').val($('#numeroQuadraForm').val());
			$('#habilitado').val($('#habilitadoForm').val());
			$('#cep').val($('#cepForm').val());
			
			submeter('quadraForm', 'pesquisarQuadra');
		}
	
	</script>
	
	<input name="acao" type="hidden" id="acao" value="atualizarQuadra">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${quadra.chavePrimaria}">
	<input name="versao" type="hidden" id="versao" value="${quadra.versao}"> 
	<input name="indexListas" type="hidden" id="indexLista" value="${indexLista}">  
	<input type="hidden" name="alteracaoTela" value="true" />
	
	<input name="habilitadoForm" type="hidden" value="${habilitado}" />
	<input name="numeroQuadraForm" id="numeroQuadraForm" type="hidden" value="${quadraForm.numeroQuadra}" />
	<input name="zeisForm" id="zeis" type="hidden" value="${quadraFom.zeis.chavePrimaria}" /> 
	<input name="setorComercialForm" id="setorComercialForm" type="hidden" value="${quadraForm.setorComercial.chavePrimaria}" />
	<input name="perfilQuadraForm" id="perfilQuadraForm" type="hidden" value="${quadraForm.perfilQuadra.chavePrimaria}" /> 
	<input name="cepForm" id="cepForm" type="hidden" value="${cepTela}" />
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="quadraCol1" class="coluna detalhamentoColunaMedia">
			<label class="rotulo campoObrigatorio" for="idGerenciaRegional"><span class="campoObrigatorioSimbolo">* </span>Ger�ncia Regional:</label>
			<select name="gerenciaRegional" id="idGerenciaRegional" class="campoSelect" onchange="carregarLocalidades(this)">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
					<option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${quadra.setorComercial.localidade.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${gerenciaRegional.nome}"/>
					</option>		
				</c:forEach>	
			</select><br />
			<label class="rotulo campoObrigatorio" for="idLocalidade"><span class="campoObrigatorioSimbolo">* </span>Localidade:</label>
		   	<select name="localidade" id="idLocalidade" class="campoSelect" onchange="carregarSetorComercial(this);" <c:if test="${empty listaLocalidade || empty listaGerenciaRegional}">disabled="disabled"</c:if>>
				<option value="-1">Selecione</option>
			  	<c:forEach items="${listaLocalidade}" var="localidade">
					<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${quadra.setorComercial.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${localidade.descricao}"/>
					</option>
		  	   	</c:forEach>
			</select><br />
			<label class="rotulo campoObrigatorio" for="idSetorComercial"><span class="campoObrigatorioSimbolo">* </span>Setor Comercial:</label>
		   	<select name="setorComercial" id="idSetorComercial" class="campoSelect" <c:if test="${empty listaSetorComercial}">disabled="disabled"</c:if>>
			   	<option value="-1">Selecione</option>
				<c:forEach items="${listaSetorComercial}" var="setorComercial">
					<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${quadra.setorComercial.chavePrimaria == setorComercial.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${setorComercial.descricao}"/>
					</option>
			    </c:forEach>
			</select><br />
			<label class="rotulo campoObrigatorio" for="numeroQuadra"><span class="campoObrigatorioSimbolo">* </span>Quadra:</label>
			<input class="campoTexto" type="text" name="numeroQuadra" id="numeroDeQuadra" maxlength="6" size="4" value="${quadra.numeroQuadra}" onkeypress="return formatarCampoInteiro(event)"><br />	
			
			<label class="rotulo campoObrigatorio" for="idPerfilQuadra"><span class="campoObrigatorioSimbolo">* </span>Perfil da Quadra:</label>
		   	<select name="perfilQuadra" id="idPerfilQuadra" class="campoSelect" >
		   		<option value="-1">Selecione</option>
				<c:forEach items="${listaPerfilQuadra}" var="perfilQuadra">
					<option value="<c:out value="${perfilQuadra.chavePrimaria}"/>" <c:if test="${quadra.perfilQuadra.chavePrimaria == perfilQuadra.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${perfilQuadra.descricao}"/>
					</option>		
		    	</c:forEach>	
		   	</select>
	   	</fieldset>
	   	<fieldset id="quadraCol2" class="colunaFinal">
			<label class="rotulo" for="idZeis">Zeis:</label>
		   	<select name="zeis" id="idZeis" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaZeis}" var="zeis">
					<option value="<c:out value="${zeis.chavePrimaria}"/>" <c:if test="${quadra.zeis.chavePrimaria == zeis.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${zeis.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
		    
		    <label class="rotulo" for="idSetorCensitario">Setor Censit�rio:</label>
		   	<select name="setorCensitario" id="idSetorCensitario" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaSetorCensitario}" var="setorCensitario">
					<option value="<c:out value="${setorCensitario.chavePrimaria}"/>" <c:if test="${quadra.setorCensitario.chavePrimaria == setorCensitario.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${setorCensitario.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
		    
		    <label class="rotulo" for="idZonaBloqueio">Zona Bloqueio:</label>
		   	<select name="zonaBloqueio" id="idZonaBloqueio" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaZonaBloqueio}" var="zonaBloqueio">
					<option value="<c:out value="${zonaBloqueio.chavePrimaria}"/>" <c:if test="${quadra.zonaBloqueio.chavePrimaria == zonaBloqueio.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${zonaBloqueio.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
		    
		    <label class="rotulo" for="idTipoArea">Tipo �rea:</label>
		   	<select name="areaTipo" id="idAreaTipo" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaAreaTipo}" var="areaTipo">
					<option value="<c:out value="${areaTipo.chavePrimaria}"/>" <c:if test="${quadra.areaTipo.chavePrimaria == areaTipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${areaTipo.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
		    <label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
		    <input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${quadra.habilitado eq true}">checked</c:if>><label class="rotuloRadio">Ativo</label>
		   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${quadra.habilitado == false}">checked</c:if>><label class="rotuloRadio">Inativo</label>
		</fieldset>
	   
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset id="quadraFaces" class="conteinerBloco">	
			<legend class="conteinerBlocoTitulo">Faces:</legend>
			<jsp:include page="/jsp/cadastro/localidade/exibirAdicaoQuadraFace.jsp" >
				<jsp:param name="actionAdicionarFace" value="adicionarFaceNaQuadraFluxoAlteracao" />
				<jsp:param name="actionRemoverFace" value="removerFaceNaQuadraFluxoAlteracao" />
			</jsp:include>
		</fieldset>
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para alterar a Quadra.</p>
	</fieldset>
	
	
	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="voltar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
	    <input name="button" id="buttonSalvar" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit">
	</fieldset>
	<token:token></token:token>

</form>
