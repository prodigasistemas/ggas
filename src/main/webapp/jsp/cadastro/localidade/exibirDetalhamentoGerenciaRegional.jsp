<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Ger�ncia Regional<a href="<help:help>/cadastrodagernciaregionaldetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>
	
	function voltar(){
		submeter("gerenciaRegionalForm", "pesquisarGerenciaRegional");
	}
	
	function alterar(){
		var valor = $('#chavesPrimarias').val();
		$('#chavePrimaria').val(valor);
		submeter('gerenciaRegionalForm', 'exibirAtualizacaoGerenciaRegional');
	}
	
</script>

<form:form method="post" action="exibirDetalhamentoGerenciaRegional" enctype="multipart/form-data" id="gerenciaRegionalForm" name="gerenciaRegionalForm">

<input name="codigoForm" type="hidden" id="codigoForm">
<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoGerenciaRegional">
<input name="postBack" type="hidden" id="postBack" value="false">
<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${gerenciaRegional.chavePrimaria}">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${codigo}">
<input name="nome" type="hidden" id="chavePrimaria" value="${gerenciaRegional.nome}">
<input name="nomeAbreviado" type="hidden" id="nomeAbreviado" value="${gerenciaRegional.nomeAbreviado}">
<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">

<fieldset id="gerenciaRegional" class="detalhamento">
	<fieldset class="coluna">
		<label class="rotulo" for="nome">C�digo:</label>
		<span class="itemDetalhamento"><c:out value="${gerenciaRegionalForm.chavePrimaria}"/></span><br />
		<label class="rotulo" for="nome">Nome:</label>
		<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${gerenciaRegionalForm.nome}"/></span><br />	
		<label class="rotulo" for="nomeAbreviado">Nome Abreviado:</label>
		<span class="itemDetalhamento"><c:out value="${gerenciaRegionalForm.nomeAbreviado}"/></span><br />	
		<label class="rotulo" for="telefone">Telefone:</label>
		<span class="itemDetalhamento"><c:out value="("/><c:out value="${gerenciaRegionalForm.codigoFoneDDD}"/><c:out value=")"/><c:out value=" "/><c:out value="${gerenciaRegionalForm.fone}"/></span><br />	
		<label class="rotulo" for="ramal">Ramal:</label>
		<span class="itemDetalhamento"><c:out value="${gerenciaRegionalForm.ramalFone}"/></span><br />	
		<label class="rotulo" for="fax">Fax:</label>
		<span class="itemDetalhamento"><c:out value="("/><c:out value="${gerenciaRegionalForm.codigoFaxDDD}"/><c:out value=")"/><c:out value=" "/><c:out value="${gerenciaRegionalForm.fax}"/></span><br />	
		<label class="rotulo" for="email">Email:</label>
		<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${gerenciaRegionalForm.email}"/></span>
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo" for="numero">N�mero:</label>
		<span class="itemDetalhamento"><c:out value="${gerenciaRegionalForm.endereco.numero}"/></span><br />	
		<label class="rotulo" for="cep">Cep:</label>
		<span class="itemDetalhamento"><c:out value="${gerenciaRegionalForm.endereco.cep.cep}"/></span><br />	
		<label class="rotulo" for="complemento">Complemento:</label>
		<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${gerenciaRegionalForm.endereco.complemento}"/></span><br />	
		<label class="rotulo" for="idEnderecoReferencia">Endere�o Refer�ncia :</label>
		<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${gerenciaRegionalForm.endereco.enderecoReferencia}"/></span><br />
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<c:choose>
			<c:when test="${gerenciaRegionalForm.habilitado == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:otherwise>
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:otherwise>
		</c:choose>
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAtualizacaoGerenciaRegional">    
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>
</form:form>
