<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<script type="text/javascript">

function carregarTroncos(elem){

		var codCityGate = elem.value;
      	var selectTroncos = document.getElementById("idTronco");
    
      	selectTroncos.length=0;
   	  	var novaOpcao = new Option("Selecione","-1");
  	    selectTroncos.options[selectTroncos.length] = novaOpcao;

   	    if (codCityGate != "-1") {
      		selectTroncos.disabled = false;      		
        	AjaxService.consultarTroncosPorCityGate( codCityGate, 
            	function(unidades) {            		      		         		
                	for (key in unidades){
                    	var novaOpcao = new Option(unidades[key], key);
                        selectTroncos.options[selectTroncos.length] = novaOpcao;
                    }
                }
            );
      	} else {
			selectTroncos.disabled = true;      	
      	}
	}
	
function incluirTronco(form) {
	document.forms[0].indexLista.value = -1;
	submeter('redeForm', 'adicionarTroncoNaRede');
}

function alterarTronco(form) {
	submeter('redeForm', 'adicionarTroncoNaRede');
}

function removerTronco(indice) {
	document.forms[0].indexLista.value = indice;
	submeter('redeForm', 'removerTroncoNaRede');
}

function limparTronco() {
	document.forms[0].indexLista.value = -1;
	document.forms[0].idCityGate.selectedIndex = 0;
	document.forms[0].idTronco.selectedIndex = 0;
	
	var botaoAlterarTronco = document.getElementById("botaoAlterarTronco");
	var botaoLimparTronco = document.getElementById("botaoLimparTronco");	
	var botaoIncluirTronco = document.getElementById("botaoIncluirTronco");	
	botaoAlterarTronco.disabled = true;
	botaoLimparTronco.disabled = false;
	botaoIncluirTronco.disabled = false;
		
}

function exibirAlteracaoDoTronco(indice,idCityGate,idTronco) {
		if (indice != "") {
			
			document.forms[0].indexLista.value = indice;
			
			var selectTronco = document.getElementById("idTronco");
			selectTronco.length=0;
      		var novaOpcao = new Option("Selecione","-1");
        	selectTronco.options[selectTronco.length] = novaOpcao;
			
			if (idCityGate != "") {
			
				AjaxService.consultarTroncosPorCityGate( idCityGate, {
		           	callback: function(troncos) {            		      		         		
	               		for (key in troncos){
		                	var novaOpcao = new Option(troncos[key], key);
		            		selectTronco.options[selectTronco.length] = novaOpcao;
	            		}
	            		selectTronco.disabled = false;
	        		}, async:false}
		        );
			
				var tamanho = document.forms[0].idCityGate.length;
				var opcao = undefined;
				for(var i = 0; i < tamanho; i++) {
					opcao = document.forms[0].idCityGate.options[i];
					if (idCityGate == opcao.value) {
						opcao.selected = true;
					}
				}
			}
			
			verificacaoIdTronco(idTronco);

		var botaoAlterarTronco = document.getElementById("botaoAlterarTronco");
		var botaoLimparTronco = document.getElementById("botaoLimparTronco");
		var botaoIncluirTronco = document.getElementById("botaoIncluirTronco");
		botaoAlterarTronco.disabled = false;
		botaoLimparTronco.disabled = false;
		botaoIncluirTronco.disabled = true;
	}
}
function verificacaoIdTronco(idTronco){
	if (idTronco != "") {
		var tamanho = document.forms[0].idTronco.length;
		var opcao = undefined;
		for(var i = 0; i < tamanho; i++) {
			opcao = document.forms[0].idTronco.options[i];
			if (idTronco == opcao.value) {
				opcao.selected = true;
			}
		}
	}
}

function onloadTronco() {
	<c:choose>
		<c:when test="${indexLista > -1 && empty param['operacaoLista'] }">
			var botaoAlterarTronco = document.getElementById("botaoAlterarTronco");
			var botaoLimparTronco = document.getElementById("botaoLimparTronco");	
			var botaoIncluirTronco = document.getElementById("botaoIncluirTronco");	
			botaoIncluirTronco.disabled = true;
			botaoAlterarTronco.disabled = false;
			botaoLimparTronco.disabled = false;
		</c:when>
		<c:when test="${sucessoManutencaoLista && limpaTronco}">
			limparTronco();
		</c:when>
	
	</c:choose>
}
	function init(){
		onloadTronco();
	}

	addLoadEvent(init);

</script>

<fieldset id="exibirAdicaTronco1" class="colunaEsq exibirAdicaTronco1Css">
	<label class="rotulo campoObrigatorio" for="idCityGate"><span class="campoObrigatorioSimbolo">* </span>City Gate:</label>
		<select name="idCityGate" id="idCityGate" class="campoSelect campoHorizontal" onchange="carregarTroncos(this);">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaCityGate}" var="cityGate">
				<option value="<c:out value="${cityGate.chavePrimaria}"/>" <c:if test="${idCityGate == cityGate.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${cityGate.descricao}"/>
				</option>		
			</c:forEach>
		</select>
    <label id="rotuloTronco" class="rotulo campoObrigatorio rotuloHorizontal" for="idTronco"><span class="campoObrigatorioSimbolo">* </span>Tronco:</label>
	   	<select name="idTronco" id="idTronco" class="campoSelect campoHorizontal">
	   		<option value="-1">Selecione</option>
			<c:forEach items="${listaTronco}" var="tronco">
				<option value="<c:out value="${tronco.chavePrimaria}"/>" <c:if test="${idTronco == tronco.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tronco.descricao}"/>
				</option>		
	    	</c:forEach>	
	   	</select>
</fieldset>	    



<fieldset class="conteinerBotoesInserir2">
   	<input class="bottonRightCol2" name="botaoLimparTronco" id="botaoLimparTronco" value="Limpar" type="button" onclick="limparTronco();">
   	<input class="bottonRightCol2" name="botaoIncluirTronco" id="botaoIncluirTronco" value="Adicionar Tronco" type="button" onclick="incluirTronco(this.form);">
	<input class="bottonRightCol2" disabled="disabled" name="botaoAlterarTronco" id="botaoAlterarTronco" value="Alterar Tronco" type="button" onclick="alterarTronco(this.form);">
</fieldset>

<c:set var="i" value="0" />
<display:table class="dataTableGGAS" name="listaRedeTronco" sort="list" id="troncos" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	<display:column property="tronco.cityGate.descricao" sortable="false" title="City Gate" />
	<display:column property="tronco.descricao" sortable="false" title="Tronco" /> 
	
	<display:column style="text-align: center; width: 25px"> 
		<a href="javascript:exibirAlteracaoDoTronco('${i}','${troncos.tronco.cityGate.chavePrimaria}','${troncos.tronco.chavePrimaria}');"><img title="Alterar Tronco" alt="Alterar Tronco"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a> 
	</display:column>
	
	<display:column style="text-align: center; width: 25px"> 
		<a onclick="return confirm('Deseja excluir o Tronco?');" href="javascript:removerTronco(<c:out value="${i}"/>);"><img title="Excluir Tronco" alt="Excluir Tronco" src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
	</display:column>
	<c:set var="i" value="${i+1}" />
</display:table>
<token:token></token:token>