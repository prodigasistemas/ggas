<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/dhtmlxcombo.css">

<h1 class="tituloInterno">Incluir Setor Comercial<a href="<help:help>/setorcomercialinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>


<form:form method="post" action="inserirSetorComercial">

<script>
 
	function carregarLocalidades(elem) {	
		var codGerenciaRegional = elem.value;
      	var selectLocalidades = document.getElementById("idLocalidade");
    
      	selectLocalidades.length=0;
      	var novaOpcao = new Option("Selecione","-1");
        selectLocalidades.options[selectLocalidades.length] = novaOpcao;
      	      		
       	AjaxService.consultarLocalidadesPorGerenciaRegional( codGerenciaRegional, 
           	function(localidades) {            		      		         		
               	for (key in localidades){
	                var novaOpcao = new Option(localidades[key], key);
	            	selectLocalidades.options[selectLocalidades.length] = novaOpcao;
            	}
        	}
        );
	}

	function carregarMunicipios(){
		var codUnidadeFederacao = document.forms[0].idUnidadeFederacao.value;
		var selectMunicipio = document.getElementById("idMunicipio");
	    var valorMunicipio = document.forms[0].idMunicipio.value;
	    
      	selectMunicipio.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectMunicipio.options[selectMunicipio.length] = novaOpcao;
        
      	if (codUnidadeFederacao != "-1") {
      		selectMunicipio.disabled = false;      		

      		AjaxService.consultarMunicipiosPorUnidadeFederativa( codUnidadeFederacao, 
    			{callback: function(listaMunicipio) {            		      		         		
                    for (key in listaMunicipio){
                        var novaOpcao = new Option(key, listaMunicipio[key]);
                    	selectMunicipio.options[selectMunicipio.length] = novaOpcao;
                	}
				} , async:false}
            );
            
            if (valorMunicipio != "-1"){
            	document.getElementById("idMunicipio").value = valorMunicipio;
            }
      	} else {
      		selectMunicipio.disabled = true;
      	}
	}
	
	function limpar(){
		document.forms[0].descricao.value = '';
		document.forms[0].codigo.value = '';
		document.forms[0].idGerenciaRegional.value = '-1';
		document.forms[0].idLocalidade.value = '-1';		
		document.forms[0].idUnidadeFederacao.value = '-1';
		document.forms[0].idMunicipio.value = '-1';
		document.forms[0].idMunicipio.disabled = true;
	}
	
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaSetorComercial"/>';
	}

	
	addLoadEvent(init);
</script>

<input name="acao" type="hidden" id="acao" value="inserirSetorComercial">
<input name="postBack" type="hidden" id="postBack" value="true">

<fieldset id="setorComercial" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna">
		<label class="rotulo" id="rotuloGerenciaRegional" for="idGerenciaRegional">Ger�ncia Regional:</label>
		<select name="gerenciaRegional" id="idGerenciaRegional" class="campoSelect" onchange="carregarLocalidades(this)">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaGerenciaRegional}" var="gerenciaRegional">
			
				<option value="<c:out value="${gerenciaRegional.chavePrimaria}"/>" <c:if test="${setorComercialForm.gerenciaRegional.chavePrimaria == gerenciaRegional.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${gerenciaRegional.nome}"/>
				</option>
						
		    </c:forEach>	
	    </select><br />
		<label class="rotulo campoObrigatorio" id="rotuloLocalidade" for="idLocalidade"><span class="campoObrigatorioSimbolo">* </span>Localidade:</label>
		<select name="localidade" id="idLocalidade" class="campoSelect" >
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaLocalidade}" var="localidade">
				<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${setorComercialForm.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localidade.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
		<label class="rotulo campoObrigatorio" id="rotuloCodigoSetorComercial" for="codigo"><span class="campoObrigatorioSimbolo">* </span>C�digo do Setor Comercial:</label>
		<input class="campoTexto" type="text" name="codigo" id="codigo" maxlength="6" size="6" tabindex="2" value="${setorComercialForm.codigo}" onkeypress="return formatarCampoCodigoSetorComercial(event);"/><br />
		<label class="rotulo campoObrigatorio" id="rotuloNomeSetorComercial" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Nome do Setor Comercial:</label>
		<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="32" tabindex="3" value="${setorComercialForm.descricao}" onblur="validarCampoNomeSetorComercial(this)" onkeyup="validarCampoNomeSetorComercial(this)" onkeypress="return formatarCampoNomeSetorComercial(event);"/>
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo campoObrigatorio" id="rotuloMunicipio" for="idMunicipio"><span class="campoObrigatorioSimbolo">* </span>Unidade Federativa:</label>
		<select name="unidadeFederativa" id="idUnidadeFederacao" class="campoSelect" onchange="carregarMunicipios();" >
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaUnidadeFederacao}" var="unidadeFederacao">
				<option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${setorComercialForm.unidadeFederativa.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadeFederacao.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select><br />
		<label class="rotulo campoObrigatorio" id="rotuloMunicipio" for="idMunicipio"><span class="campoObrigatorioSimbolo2">* </span>Munic�pio:</label>
		<select name="municipio" id="idMunicipio" class="campoSelect">
	    	<option value="-1">Selecione</option>
			<c:forEach items="${listaMunicipio}" var="municipio">
				<option value="<c:out value="${municipio.chavePrimaria}"/>" <c:if test="${setorComercialForm.municipio.chavePrimaria == municipio.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${municipio.descricao}"/>
				</option>		
		    </c:forEach>	
	    </select>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Setor Comercial.</p>
	<p class="legenda2"><span class="campoObrigatorioSimbolo2">* </span>campo obrigat�rio para sele��o de munic�pio.</p>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="voltar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar();">
    <vacess:vacess param="inserirSetorComercial">
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="submit">
    </vacess:vacess>
</fieldset>
</form:form>