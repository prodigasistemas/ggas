<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<h1 class="tituloInterno">Pesquisar Rede Externa<a class="linkHelp" href="<help:help>/consultadaredeexterna.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" action="pesquisarRede" name="redeForm" id="redeForm">

	<script language="javascript">
	
	
	function removerRede(){
		
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('redeForm', 'removerRede');
			}
	    }
	}
		function limparFormulario() {		
			document.forms[0].descricao.value = "";
			document.forms[0].idLocalidade.value = '-1';
			document.forms[0].habilitado[0].checked = true;		
		}
	
		function carregarTroncos(elem){
			var codLocalidade = elem.value;
	      	var selectTroncos = document.getElementById("idTronco");
	    
	      	selectTroncos.length=0;
	      	var novaOpcao = new Option("Selecione","-1");
	        selectTroncos.options[selectTroncos.length] = novaOpcao;
	        
	      	if (codLocalidade != "-1") {
	      		selectTroncos.disabled = false;      		
	        	AjaxService.consultarTroncosPorLocalidade( codLocalidade, 
	            	function(unidades) {            		      		         		
	                	for (key in unidades){
	                    	var novaOpcao = new Option(unidades[key], key);
	                        selectTroncos.options[selectTroncos.length] = novaOpcao;
	                    }
	                }
	            );
	      	} else {
				selectTroncos.disabled = true;      	
	      	}
		}
		
		function incluir() {		
			location.href = '<c:url value="/exibirInsercaoRede"/>';
		}
		
		function alterarRede(chave) {
		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				document.forms[0].idLocalidade.value = $('#idLocalidade').val();
				submeter("redeForm", "exibirAtualizacaoRede");
		    }
		}
		
		function detalharRede(chave){
			document.forms[0].chavePrimaria.value = chave;
			document.forms[0].descricao.value = $('#descricao').val();
			submeter("redeForm", "exibirDetalhamentoRede");
		}
			
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarRede">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="redeCol1" class="coluna">
			<label class="rotulo" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="50" size="45" tabindex="3" value="${rede.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		
			<label class="rotulo" for="idLocalidade" >Localidade:</label>
			<select class="campoSelect" name="localidade" id="idLocalidade" onchange="carregarTroncos(this);">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaLocalidade}" var="localidade">
					<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${rede.localidade.chavePrimaria == localidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${localidade.descricao}"/>
					</option>
			    </c:forEach>	
		    </select>
	    </fieldset>
	    
		<fieldset id="pesquisarRedeCol2" class="colunaFinal">
		    <label class="rotulo">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicador" value="true" <c:if test="${habilitado eq true}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicador" value="false" <c:if test="${habilitado eq false}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUsoInativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="indicador" value="" <c:if test="${empty habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
			
		    <fieldset class="conteinerBotoesPesquisarDir">
		    	<vacess:vacess param="pesquisarRede">
		    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
		    	</vacess:vacess>		
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
			</fieldset>
	    </fieldset>
	    
	     
	</fieldset>
	
	<c:if test="${listaRede ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaRede" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" sort="list" id="rede" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarRede">
	        <display:column style="text-align: center;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${rede.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo">
		     	<c:choose>
					<c:when test="${rede.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	        <display:column sortable="true" titleKey="REDE_DESCRICAO">
	            <a href="javascript:detalharRede(<c:out value='${rede.chavePrimaria}'/>);">
	            	<c:out value="${rede.descricao}"/>
	            </a>
	        </display:column>
	        <display:column property="localidade.descricao" sortable="true" titleKey="REDE_LOCALIDADE" />
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
    	<c:if test="${not empty listaRede}">
    		<vacess:vacess param="exibirAtualizacaoRede">
   				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarRede()" type="button">
   			</vacess:vacess>
   			<vacess:vacess param="removerRede">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerRede()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInsercaoRede">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>
	    
</form>



	