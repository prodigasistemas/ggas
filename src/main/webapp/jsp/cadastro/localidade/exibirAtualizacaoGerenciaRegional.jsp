<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>

function limparFormulario(){
	document.forms[0].nome.value = "";
	document.forms[0].nomeAbreviado.value = "";
	document.forms[0].fone.value = "";
	document.forms[0].ramalFone.value = "";
	document.forms[0].fax.value = "";
	document.forms[0].email.value = "";	
	document.forms[0].cep.value = "";
	document.forms[0].chaveCep.value = "";
	document.forms[0].numero.value = "";
	document.forms[0].complemento.value = "";		
	document.forms[0].enderecoReferencia.value = "";
	document.forms[0].dddTelefone.value = "";
	document.forms[0].dddFax.value = "";		
	var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
	removeAllOptions(selectCep);
	animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');	
	limparDialog();
}	
	function cancelar() {
		var codigo = $('#codigoForm').val();
		$('#chavePrimaria').val(codigo);
		
		var nome = $('#nomeForm').val();
		$('#nome').val(nome);
		
		var nomeAbrev = $('#nomeAbrForm').val();
		$('#nomeAbreviado').val(nomeAbrev);
		
		var habilitado = $('#habilitadoRadio').val();
		$('#habilitado').val(habilitado);
		
		submeter("gerenciaRegionalForm", "pesquisarGerenciaRegional");
	}
	
	function salvar() {
		submeter("gerenciaRegionalForm", "atualizarGerenciaRegional");
	}

</script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Alterar Ger�ncia Regional<a href="<help:help>/gernciaregionalinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar uma Ger�ncia Regional, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form:form method="post" id="gerenciaRegionalForm" name="gerenciaRegionalForm"> 

<input name="codigoForm" type="hidden" id="codigoForm" value="${codigo}">
<input name="nomeForm" type="hidden" id="nomeForm" value="${nome}">
<input name="nomeAbrForm" type="hidden" id="nomeAbrForm" value="${nomeAbreviado}">

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${gerenciaRegional.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${gerenciaRegional.versao}">
<input name="habilitadoRadio" type="hidden" id="habilitadoRadio" value="${habilitado}">
<input name="postBack" type="hidden" id="postBack" value="true">

<fieldset id="gerenciaRegional" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna">
		<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input class="campoTexto" type="text" id="nome" name="nome" maxlength="50" size="55" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${gerenciaRegional.nome}"><br />
		<label class="rotulo campoObrigatorio" id="rotuloNomeAbreviado" for="nomeAbreviado"><span class="campoObrigatorioSimbolo">* </span>Nome Abreviado:</label>
		<input class="campoTexto" type="text" id="nomeAbreviado" name="nomeAbreviado" maxlength="5" onblur="this.value = removerEspacoInicialFinal(this.value);" size="5" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${gerenciaRegional.nomeAbreviado}"><br />
		
		<label class="rotulo" id="rotuloFone" for="fone">Telefone:</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddTelefone" name="codigoFoneDDD" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event)"  value="${gerenciaRegional.codigoFoneDDD}">
		<input class="campoTexto campoHorizontal" type="text" id="fone" name="fone" maxlength="8" size="25" value="${gerenciaRegional.fone}" onkeypress="return formatarCampoInteiro(event)"><br class="quebraLinha2" />
		
		<label class="rotulo" id="rotuloRamalFone" for="ramalFone">Ramal:</label>
		<input class="campoTexto" type="text" id="ramalFone" name="ramalFone" maxlength="4" size="12" value="${gerenciaRegional.ramalFone}" onkeypress="return formatarCampoInteiro(event)"><br />
		
		<label class="rotulo" id="rotuloFax" for="fax">Fax:</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddFax" name="codigoFaxDDD" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event)"  value="${gerenciaRegional.codigoFaxDDD}">
		<input class="campoTexto campoHorizontal" type="text" id="fax" name="fax" maxlength="8" size="25" value="${gerenciaRegional.fax}" onkeypress="return formatarCampoInteiro(event)"><br class="quebraLinha2" />
		
		<label class="rotulo" id="rotuloEmail" for="email">Email:</label>
		<input class="campoTexto" type="text" id="email" name="email" maxlength="80" size="40" onkeypress="return formatarCampoEmail(event)" value="${gerenciaRegional.email}">
	</fieldset>
	
	<fieldset class="colunaFinal">
		<fieldset>
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="idCampoCep" value="cep"/>
				<jsp:param name="cepObrigatorio" value="false"/>
				<jsp:param name="numeroCep" value="${gerenciaRegional.endereco.cep.cep}"/>
				<jsp:param name="chaveCep" value="${gerenciaRegional.endereco}"/>
			</jsp:include>
		</fieldset>
		<label class="rotulo" id="rotuloNumero" for="numero">N�mero:</label>
		<input class="campoTexto" type="text" id="numero" name="numero" maxlength="5" size="5" value="${gerenciaRegional.endereco.numero}" onkeypress="return formatarCampoNumeroEndereco(event, this);" ><br />
		<label class="rotulo" id="rotuloComplemento" for="complemento">Complemento:</label>
		<input class="campoTexto" type="text" id="complemento" name="complemento" maxlength="255" size="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${gerenciaRegional.endereco.complemento}"><br />
	    <label class="rotulo" id="rotuloEnderecoReferencia" for="enderecoReferencia">Endere�o Refer�ncia:</label>
	    <input class="campoTexto" type="text" id="enderecoReferencia" name="enderecoReferencia" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="30" size="30" value="${gerenciaRegional.endereco.enderecoReferencia}"><br />
	    <label class="rotulo" id="rotuloNumero" for="numero">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="indicadorUso" value="true" <c:if test="${gerenciaRegional.habilitado == true}">checked</c:if>><label class="rotuloRadio">Ativo</label>
	   	<input class="campoRadio" type="radio" name="habilitado" id="indicadorUso" value="false" <c:if test="${gerenciaRegional.habilitado == false}">checked</c:if>><label class="rotuloRadio">Inativo</label>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para altera��o de Ger�ncia Regional.</p>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="atualizarGerenciaRegional">
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="button" onclick="salvar();">
    </vacess:vacess>
</fieldset>
<token:token></token:token>
</form:form> 