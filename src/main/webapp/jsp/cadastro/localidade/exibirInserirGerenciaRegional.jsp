<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<h1 class="tituloInterno">Incluir Ger�ncia Regional<a href="<help:help>/gernciaregionalinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form:form method="post" action="inserirGerenciaRegional" id="gerenciaRegionalForm" name="gerenciaRegionalForm"> 



<script>

function limparFormulario(){
	document.forms[0].nome.value = "";
	document.forms[0].nomeAbreviado.value = "";
	document.forms[0].fone.value = "";
	document.forms[0].ramalFone.value = "";
	document.forms[0].fax.value = "";
	document.forms[0].email.value = "";	
	document.forms[0].cep.value = "";
	document.forms[0].chaveCep.value = "";
	document.forms[0].numero.value = "";		
	document.forms[0].complemento.value = "";		
	document.forms[0].enderecoReferencia.value = "";
	document.forms[0].dddTelefone.value = "";
	document.forms[0].dddFax.value = "";
	var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
	removeAllOptions(selectCep);
	animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
	limparDialog();	
}	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaGerenciaRegional"/>';
	}

	//animatedcollapse.addDiv('pesquisarCep'+'<c:out value='${param.idCampoCep}' default=""/>', 'fade=0,speed=400,persist=0,hide=1');
</script>

<input type="hidden" name="acao" id="acao" value="inserirGerenciaRegional">
<input name="postBack" type="hidden" id="postBack" value="true">

<fieldset id="gerenciaRegional" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna">
		<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input class="campoTexto" type="text" id="nome" name="nome" maxlength="50" size="55" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${gerenciaRegionalForm.nome}"><br />
		<label class="rotulo campoObrigatorio" id="rotuloNomeAbreviado" for="nomeAbreviado"><span class="campoObrigatorioSimbolo">* </span>Nome Abreviado:</label>
		<input class="campoTexto" type="text" id="nomeAbreviado" name="nomeAbreviado" maxlength="3" size="5" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${gerenciaRegionalForm.nomeAbreviado}"><br />
		<label class="rotulo" id="rotuloFone" for="fone">Telefone:</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddTelefone" name="codigoFoneDDD" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event)"  value="${gerenciaRegionalForm.codigoFoneDDD}">
		<input class="campoTexto campoHorizontal" type="text" id="fone" name="fone" maxlength="8" size="25" value="${gerenciaRegionalForm.fone}" onkeypress="return formatarCampoInteiro(event);"><br class="quebraLinha2" />
		<label class="rotulo" id="rotuloRamalFone" for="ramalFone">Ramal:</label>
		<input class="campoTexto" type="text" id="ramalFone" name="ramalFone" maxlength="4" size="12" value="${gerenciaRegionalForm.ramalFone}" onkeypress="return formatarCampoInteiro(event);"><br />
		<label class="rotulo" id="rotuloFax" for="fax">Fax:</label>
		<input class="campoTexto campoHorizontal" type="text" id="dddFax" name="codigoFaxDDD" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event)"  value="${gerenciaRegionalForm.codigoFaxDDD}">
		<input class="campoTexto campoHorizontal" type="text" id="fax" name="fax" maxlength="8" size="25" value="${gerenciaRegionalForm.fax}" onkeypress="return formatarCampoInteiro(event);"><br class="quebraLinha2" />
		<label class="rotulo" id="rotuloEmail" for="email">Email:</label>
		<input class="campoTexto" type="text" id="email" name="email" maxlength="80" size="40" onkeypress="return formatarCampoEmail(event)" value="${gerenciaRegionalForm.email}">	
	</fieldset>
	
	<fieldset class="colunaFinal">
		<fieldset>
			<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
				<jsp:param name="idCampoCep" value="cep"/>
				<jsp:param name="cepObrigatorio" value="false"/>
				<jsp:param name="numeroCep" value="${cep}"/>
				<jsp:param name="chaveCep" value="${cep}"/>
			</jsp:include>
		</fieldset>
		<label class="rotulo" id="rotuloNumero" for="numero">N�mero:</label>
		<input class="campoTexto" type="text" id="numero" name="numero" maxlength="5" size="5" value="${enderecoForm.numero}" onkeypress="return formatarCampoNumeroEndereco(event, this);"><br />
	    <label class="rotulo" id="rotuloComplemento" for="complemento">Complemento:</label>
		<input class="campoTexto" type="text" id="complemento" name="complemento" maxlength="255" size="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${enderecoForm.complemento}"><br />
	    <label class="rotulo" id="rotuloEnderecoReferencia" for="enderecoReferencia">Endere�o Refer�ncia:</label>
	    <input class="campoTexto" type="text" id="enderecoReferencia" name="enderecoReferencia" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="30" size="30" value="${enderecoForm.enderecoReferencia}"><br />
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Ger�ncia Regional.</p>	
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="inserirGerenciaRegional">
    	<input name="Button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="submit">
    </vacess:vacess>
</fieldset>

	<token:token></token:token>

</form:form> 