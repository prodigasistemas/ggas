<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript">
	$(document).ready(function(){
		$("#resultadoApuracaoObservacoes").hide();
	});
	
</script>

<fieldset class="conteinerBloco">
	<legend class="conteinerBlocoTitulo">Dados da Penalidade:</legend>
	<fieldset class="coluna detalhamentoColunaLarga">
		<label class="rotulo">Consumo de refer�ncia superior:</label>
		<span class="itemDetalhamento"><c:out value="${consumoRefSuperiorInferior}"/></span><br />
		
		<label class="rotulo">Faixa penalidade por retirada a maior:</label>
		<span class="itemDetalhamento"><c:out value="${faixaPenalidadePorRetiradaMaiorMenor}"/></span>
	</fieldset>
	<fieldset class="colunaFinal">
		<label class="rotulo">Percentual por retirada a maior:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${percentualPorRetiradaMaiorMenor}" minFractionDigits="2"/> %</span>
	</fieldset>
</fieldset>

<c:set var="count" value="0"/>
<c:set var="totalValorCalculado" value="0"/>
<c:set var="totalValorFinal" value="0"/>

<display:table style="margin-top: 30px" class="dataTableGGAS dataTableMenor2 dataTableCabecalho2Linhas" name="listaApuracaoQtdaPenalidadePeriodicidade" id="apuracaoQtdaPenalidadePeriodicidade" decorator="br.com.ggas.web.faturamento.apuracaopenalidade.decorator.RetiradaMaiorMenorDecorator">

	<display:column title="Data" style="width: 70px">
		<input type="hidden" name="idsApuracaoQtdaPenalidadePeriodicidade" value="<c:out value="${apuracaoQtdaPenalidadePeriodicidade.chavePrimaria}"/>"/>	
		<fmt:formatDate value="${apuracaoQtdaPenalidadePeriodicidade.dataInicioApuracao}" pattern="dd/MM/yyyy"/>
	</display:column>
	
	<display:column title="QDR (m<span class='expoente'>3</span>/dia)" style="width: 130px">
		<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.qtdaDiariaRetirada}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	</display:column>
	
<%-- 	<display:column title="Consumo Refer�ncia (m<span class='expoente'>3</span>/dia)" style="width: 130px" property="consumoReferenciaRetiradaMaior" format="{0, number, #,##0.0000}"/> --%>
		
	<display:column title="Quantidade Retirada Maior (m<span class='expoente'>3</span>/dia)" style="width: 130px">
		<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.qtdaDiariaRetiradaMaior}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	</display:column>
	
	<display:column title="Valor<br />Tarifa m�dia (R$)" style="width: 130px">
		<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.valorTarifaMedia}" minFractionDigits="4" maxFractionDigits="4" type="number" />
		<input type="hidden" value="${apuracaoQtdaPenalidadePeriodicidade.valorTarifaMedia}"
				id="lblTarifaMedia<c:out value="${count}"/>"/>
	</display:column>
	
	<display:column title="Valor Calculado (R$)" style="width: 130px">
<%-- 		<c:set var="totalValorCalculado" value="${totalValorCalculado + apuracaoQtdaPenalidadePeriodicidade.valorCalculado}"/> --%>
<%-- 		<span id="valorCalculado<c:out value="${count}"/>"> --%>
			<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.valorCalculado}" minFractionDigits="2" />
<!-- 		</span>   			 -->
	</display:column>
	<display:column title="Valor Calculado<br /> (%) Cobran�a (R$)" style="width: 130px">
	<c:set var="totalValorCalculado" value="${totalValorCalculado + apuracaoQtdaPenalidadePeriodicidade.valorCalculadoPercenCobranca}"/>
	<span id="valorCalculado<c:out value="${count}"/>">
			<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.valorCalculadoPercenCobranca}" minFractionDigits="2" />
			</span> 
	</display:column>
	
	<display:column title="Desc.(%)" style="width: 130px">
		<c:choose>
			<c:when test="${bloquearCampos eq true}">
				<input type="text" class="campoTexto" readonly="readonly" name="desc" id="desc<c:out value="${count}"/>" size="6" maxlength="10" value="<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.percentualDescontoAplicado}" minFractionDigits="2" />" />			
			</c:when>
			<c:otherwise>
				<input type="text" class="campoTexto" <c:if test="${apuracaoQtdaPenalidadePeriodicidade.valorCalculado le 0}"> readonly="readonly"</c:if> name="desc" id="desc<c:out value="${count}"/>" size="6" maxlength="10" value="<c:out value="${penalidadeVO.desc[count]}"/>" onfocus="configurarCamposDesconto(this.name, <c:out value="${count}"/>)" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);configurarValoresCamposDesconto(this.name, <c:out value="${count}"/>);"/>			
			</c:otherwise>	
		</c:choose>
	</display:column>
	
	<display:column title="Desc.(m�)" style="width: 130px">
		<c:choose>
			<c:when test="${bloquearCampos eq true}">
				<input type="text" class="campoTexto" name="descMetro" id="descMetro<c:out value="${count}"/>" readonly="readonly" size="6" maxlength="10"  />			
			</c:when>
			<c:otherwise>
				<input type="text" class="campoTexto" name="descMetro" id="descMetro<c:out value="${count}"/>" 
						<c:if test="${apuracaoQtdaPenalidadePeriodicidade.valorCalculado le 0}"> readonly="readonly"</c:if>
						size="6" maxlength="10" 					
						value="<c:out value="${penalidadeVO.descMetro[count]}"/>"
						onfocus="configurarCamposDesconto(this.name, <c:out value="${count}"/>)" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);configurarValoresCamposDesconto(this.name, <c:out value="${count}"/>);"/>			
			</c:otherwise>	
		</c:choose>
	</display:column>		
	
	<display:column title="Desc.(R$)" style="width: 130px">
		<c:choose>
			<c:when test="${bloquearCampos eq true}">
				<input type="text" class="campoTexto" readonly="readonly" name="descValor" id="descValor<c:out value="${count}"/>" size="13" maxlength="10" value="<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.valorDescontoAplicado}" minFractionDigits="2"/>"/>
			</c:when>
			<c:otherwise>
				<input type="text" class="campoTexto" <c:if test="${apuracaoQtdaPenalidadePeriodicidade.valorCalculado le 0}"> readonly="readonly"</c:if> name="descValor" id="descValor<c:out value="${count}"/>" size="13" maxlength="10" value="<c:out value="${penalidadeVO.descValor[count]}"/>" onfocus="configurarCamposDesconto(this.name, <c:out value="${count}"/>)" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);configurarValoresCamposDesconto(this.name, <c:out value="${count}"/>);"/>
			</c:otherwise>
		</c:choose>
	</display:column>
	
	<display:column title="Valor Final (R$)" style="width: 130px">
		<input id="valorFinalHidden<c:out value="${count}"/>" type="hidden" name="vlFinal" value="<c:out value="${penalidadeVO.vlFinal[count]}"/>"/>
		<span id="valorFinal<c:out value="${count}"/>">
			<c:choose>
				<c:when test="${bloquearCampos eq true}">
					<c:set var="totalValorFinal" value="${totalValorFinal + apuracaoQtdaPenalidadePeriodicidade.valorCobrado}"/>
					<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.valorCobrado}" minFractionDigits="2" />
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${not empty penalidadeVO.vlFinal[count]}">
							<c:set var="total"><fmt:parseNumber value="${penalidadeVO.vlFinal[count]}" type="number"/></c:set>  
							<c:set var="totalValorFinal" value="${totalVariacaoValorFinal + total}"/>														
							<c:out value="${penalidadeVO.vlFinal[count]}"/>
						</c:when>
						<c:otherwise>
							<c:set var="totalValorFinal" value="${totalValorFinal + apuracaoQtdaPenalidadePeriodicidade.valorCalculadoPercenCobranca}"/>
							<fmt:formatNumber value="${apuracaoQtdaPenalidadePeriodicidade.valorCalculadoPercenCobranca}" minFractionDigits="2" />
						</c:otherwise>
					</c:choose>		
				</c:otherwise>
			</c:choose>			
		</span>
	</display:column>
	   		   		
	<display:column title="<span id='resultadoApuracaoObservacoes' class='campoObrigatorioSimbolo'>*</span> Observa��es">
		<c:choose>
			<c:when test="${bloquearCampos eq true}">
				<label class="campoObrigatorio" style="display: none"></label>
				<textarea rows="3" cols="25" readonly="readonly" name="obs"><c:out value="${apuracaoQtdaPenalidadePeriodicidade.observacoes}"/></textarea>
			</c:when>
			<c:otherwise>
				<label class="campoObrigatorio" style="display: none"></label>
				<textarea rows="3" cols="25" name="obs"><c:out value="${penalidadeVO.obs[count]}"/></textarea>	
			</c:otherwise>
		</c:choose>	   			
	</display:column>
	
	<display:footer>
	  	<tr class="total2">
	  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
	  		<td style="text-align: right; padding-right: 5px"></td>
	  		<td style="text-align: right; padding-right: 5px"></td>
	  		<td style="text-align: right; padding-right: 5px"></td>
	  		<td style="text-align: right; padding-right: 5px"></td>
	  		<td style="text-align: center; padding-right: 5px"><fmt:formatNumber value="${totalValorCalculado}" minFractionDigits="2" /></td>
	  		<td style="text-align: right; padding-right: 5px"></td>
	  		<td style="text-align: right; padding-right: 5px"></td>
	  		<td style="text-align: center; padding-right: 5px">
	  			<span id="totalValorFinal"><fmt:formatNumber value="${totalValorFinal}" minFractionDigits="2" /></span>
	  		</td>
	  		<td style="text-align: right; padding-right: 5px"></td>
	  	</tr>
	 </display:footer>
	
	<c:set var="count" value="${count + 1}"/>		
</display:table>