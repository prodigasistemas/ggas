<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">

	$(document).ready(function(){
		if($("#idModalidadeContrato").val() != -1){$.scrollTo($("#linhaSeparadoraPenalidades"),800);}

		$("#resultadoApuracaoObservacoes").hide();

		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#quantidadeNaoRetiradaVO").chromatable({
			width: "680px"
		});

		//exibirJDialog("#listaQuantidadeContratada");
		$("#listaQuantidadeContratada").dialog({autoOpen: false, width: 340, modal: true, minHeight: 100, maxHeight: 300, resizable: false});

		//exibirJDialog("#listaQuantidadeRetiradaPeriodo");
		$("#listaQuantidadeRetiradaPeriodo").dialog({autoOpen: false, width: 340, modal: true, minHeight: 100, maxHeight: 300, resizable: false});

		//exibirJDialog("#listaQtdaNaoRetiradaVOParadaProgramada");
		$("#listaQtdaNaoRetiradaVOParadaProgramada").dialog({autoOpen: false, width: 700, modal: true, minHeight: 100, maxHeight: 300, resizable: false});

		//exibirJDialog("#listaQtdaNaoRetiradaVOParadaProgramadaCdl");
		$("#listaQtdaNaoRetiradaVOParadaProgramadaCdl").dialog({autoOpen: false, width: 700, modal: true, minHeight: 100, maxHeight: 300, resizable: false});

		//exibirJDialog("#listaQtdaNaoRetiradaVOFalhaFornecimento");
		$("#listaQtdaNaoRetiradaVOFalhaFornecimento").dialog({autoOpen: false, width: 700, modal: true, minHeight: 100, maxHeight: 300, resizable: false});

		//exibirJDialog("#listaQtdaNaoRetiradaVOCasoFortuitoForcaMaior");
		$("#listaQtdaNaoRetiradaVOCasoFortuitoForcaMaior").dialog({autoOpen: false, width: 700, modal: true, minHeight: 100, maxHeight: 300, resizable: false});
	});

	function exibirListaQuantidadeContratada(){
		exibirJDialog("#listaQuantidadeContratada");
	}

	function exibirListaQuantidadeRetiradaPeriodo(){
		exibirJDialog("#listaQuantidadeRetiradaPeriodo");
	}

	function exibirListaParadaProgramada(){
		exibirJDialog("#listaQtdaNaoRetiradaVOParadaProgramada");
	}

	function exibirListaParadaProgramadaCdl(){
		exibirJDialog("#listaQtdaNaoRetiradaVOParadaProgramadaCdl");
	}

	function exibirListaFalhaFornecimento(){
		exibirJDialog("#listaQtdaNaoRetiradaVOFalhaFornecimento");
	}

	function exibirListaCasoFortuitoForcaMaior(){
		exibirJDialog("#listaQtdaNaoRetiradaVOCasoFortuitoForcaMaior");
	}

</script>

<div id="listaQtdaNaoRetiradaVOParadaProgramada" title="Parada Programada">
	<display:table class="dataTableGGAS dataTableDialog" name="listaQtdaNaoRetiradaVOParadaProgramada" id="quantidadeNaoRetiradaVO">
		<display:column title="Data" style="width: 70px">
   			<fmt:formatDate value="${quantidadeNaoRetiradaVO.data}" pattern="dd/MM/yyyy"/>
   		</display:column>
		
		<display:column title="QDR (m<span class='expoente'>3</span>/dia)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdr}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
   		
   		<display:column title="QDP (m<span class='expoente'>3</span>/dia)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdp}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
   		
   		<display:column title="Observa��es">
   			<c:out value="${quantidadeNaoRetiradaVO.observacoes}"/>	   			
   		</display:column>
	</display:table>
</div>

<div id="listaQtdaNaoRetiradaVOParadaProgramadaCdl" title="Parada Programada pela CDL">
	<display:table class="dataTableGGAS dataTableDialog" name="listaQtdaNaoRetiradaVOParadaProgramadaCdl" id="quantidadeNaoRetiradaVO">
		<display:column title="Data" style="width: 70px">
   			<fmt:formatDate value="${quantidadeNaoRetiradaVO.data}" pattern="dd/MM/yyyy"/>
   		</display:column>
		
		<display:column title="QDR (m<span class='expoente'>3</span>/dia)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdr}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
   		
   		<display:column title="QDP (m<span class='expoente'>3</span>/dia)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdp}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
   		
   		<display:column title="Observa��es">
   			<c:out value="${quantidadeNaoRetiradaVO.observacoes}"/>
   		</display:column>
	</display:table>
</div>

<div id="listaQtdaNaoRetiradaVOFalhaFornecimento" title="Falha de fornecimento">
	<display:table class="dataTableGGAS dataTableDialog" name="listaQtdaNaoRetiradaVOFalhaFornecimento" id="quantidadeNaoRetiradaVO">
		<display:column title="Data" style="width: 70px">
			<fmt:formatDate value="${quantidadeNaoRetiradaVO.data}" pattern="dd/MM/yyyy"/>
		</display:column>
		
		<display:column title="QDR (m<span class='expoente'>3</span>/dia)" style="width: 130px">
			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdr}" minFractionDigits="4" maxFractionDigits="4" type="number" />
		</display:column>
		
		<display:column title="QDP (m<span class='expoente'>3</span>/dia)" style="width: 130px">
			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdp}" minFractionDigits="4" maxFractionDigits="4" type="number" />
		</display:column>
		
		<display:column title="Observa��es">
			<c:out value="${quantidadeNaoRetiradaVO.observacoes}"/>
		</display:column>
	</display:table>
</div>

<div id="listaQtdaNaoRetiradaVOCasoFortuitoForcaMaior" title="Caso fortuito ou for�a maior">
	<display:table class="dataTableGGAS dataTableDialog" name="listaQtdaNaoRetiradaVOCasoFortuitoForcaMaior" id="quantidadeNaoRetiradaVO">
		<display:column title="Data" style="width: 70px">
			<fmt:formatDate value="${quantidadeNaoRetiradaVO.data}" pattern="dd/MM/yyyy"/>
		</display:column>
		
		<display:column title="QDR (m<span class='expoente'>3</span>/dia)" style="width: 130px">
			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdr}" minFractionDigits="4" maxFractionDigits="4" type="number" />
		</display:column>
		
		<display:column title="QDP (m<span class='expoente'>3</span>/dia)" style="width: 130px">
			<fmt:formatNumber value="${quantidadeNaoRetiradaVO.qdp}" minFractionDigits="4" maxFractionDigits="4" type="number" />
		</display:column>
		
		<display:column title="Observa��es">
			<c:out value="${quantidadeNaoRetiradaVO.observacoes}"/>
		</display:column>
	</display:table>
</div>

<div id="listaQuantidadeContratada" title="Quantidade Contratada">
	<display:table class="dataTableGGAS dataTableDialog" name="listaQtdaContratadaVO" id="quantidadeContratadaVO">
		<display:column title="Per�odo">
   			<c:out value="${quantidadeContratadaVO.periodoFormatado}"/>
   		</display:column>
		
		<display:column title="QDC (m<span class='expoente'>3</span>/dia)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeContratadaVO.qdc}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
   		
   		<display:column title="QMC (m<span class='expoente'>3</span>/m�s)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeContratadaVO.qmc}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
	</display:table>
</div>

<div id="listaQuantidadeRetiradaPeriodo" title="Quantidade Retirada no Per�odo">
	<display:table class="dataTableGGAS dataTableDialog" name="listaQtdaRetiradaPeriodoVO" id="quantidadeRetiradaPeriodoVO">
		<display:column title="Per�odo">
   			<c:out value="${quantidadeRetiradaPeriodoVO.periodoFormatado}"/>
   		</display:column>
		   		
   		<display:column title="QDR (m<span class='expoente'>3</span>/m�s)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeRetiradaPeriodoVO.qdr}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
   		
   		<display:column title="QDR M�dia (m<span class='expoente'>3</span>/m�s)" style="width: 130px">
   			<fmt:formatNumber value="${quantidadeRetiradaPeriodoVO.qdrMedia}" minFractionDigits="4" maxFractionDigits="4" type="number" />
   		</display:column>
	</display:table>
</div>

<fieldset class="conteinerBloco conteinerPenalidades">
	<legend class="conteinerBlocoTitulo">Dados da Penalidade:</legend>
	
	<fieldset class="coluna detalhamentoColunaLarga">
		<label class="rotulo">Per�odo:</label>
		<span class="itemDetalhamento"><c:out value="${periodicidadeContrato}"/></span><br />
						
		<label class="rotulo">Consumo de refer�ncia inferior:</label>
		<span class="itemDetalhamento"><c:out value="${consumoReferenciaInferiorContrato}"/></span> <br />

		<c:if test="${modalidadeContrato == null}">
			<label class="rotulo">Modalidade Contrato:</label>
			<span class="itemDetalhamento"><c:out value="${modalidadeContrato}"/></span>
			<c:if test="${empty modalidadeContrato}">
			<select class="campoSelect campoHorizontal" name="idModalidade" id="idModalidade">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaContratoModalidade}" var="modalidadeContrato">
						<option value="<c:out value="${modalidadeContrato.chavePrimaria}"/>" <c:if test="${penalidadeVO.idModalidade == modalidadeContrato.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${modalidadeContrato.descricao}"/>
						</option>		
					</c:forEach>
				</select>
			</c:if>
		</c:if>
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo">Compromisso de retirada ToP:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${compromissoRetiradaContrato}" minFractionDigits="2"/> %</span>
		
		<c:if test="${modalidadeContrato == null}">
			<label class="rotulo">Recupera��o Autom�tica:</label>
			<span class="itemDetalhamento"><c:out value="${recuperacaoAutomaticaContrato}"/></span>
		</c:if>
	</fieldset>
		
</fieldset>

<hr class="linhaSeparadora1" />

<fieldset class="conteinerBloco conteinerPenalidades">
	<legend class="conteinerBlocoTitulo">Quantidades:</legend>
	<fieldset class="coluna2">
		<label class="rotulo">Compromisso de retirada:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${compromissoRetirada}" minFractionDigits="4" maxFractionDigits="4" type="number" />(m<span class='expoente'>3</span>) <b>(-)</b> </span><br />
		
		<label class="rotulo">Quantidade retirada no per�odo:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${qtdaRetiradaPeriodo}" minFractionDigits="4" maxFractionDigits="4" type="number" /> (m<span class='expoente'>3</span>) <b>(+)</b> </span>
	</fieldset>
	<fieldset class="colunaEsq">
		<input name="button" class="bottonRightCol" style="width: 90px" <c:if test="${empty listaQtdaContratadaVO}">disabled="disabled"</c:if>  value="<< Detalhar" type="button" onclick="exibirListaQuantidadeContratada();">
		<input name="button" class="bottonRightCol" style="width: 90px" <c:if test="${empty listaQtdaRetiradaPeriodoVO}">disabled="disabled"</c:if>  value="<< Detalhar" type="button" onclick="exibirListaQuantidadeRetiradaPeriodo();">
	</fieldset>
	
</fieldset>

<hr class="linhaSeparadora1" />

<fieldset id="quantidadesNaoRetiradas" class="conteinerBloco conteinerPenalidades">
	<legend class="conteinerBlocoTitulo">Quantidades N�o Retiradas:</legend>
	<fieldset class="coluna2">
		<label class="rotulo">Parada programada:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${paradaProgramada}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>) <b>(-)</b> </span><br />
		
		<label class="rotulo">Parada n�o programada pela CDL:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${paradaNaoProgramadaCdl}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>) <b>(-)</b> </span><br />
		
		<label class="rotulo">Falha de fornecimento:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${falhaFornecimento}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>) <b>(-)</b> </span><br />
		
		<label class="rotulo">Caso fortuito ou for�a maior:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${casoFortuitoForcaMaior}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>) <b>(-)</b> </span>
	</fieldset>
	
	<fieldset class="coluna">
		<input name="button" class="bottonRightCol" <c:if test="${empty listaQtdaNaoRetiradaVOParadaProgramada}">disabled="disabled"</c:if> value="<< Detalhar" type="button" onclick="exibirListaParadaProgramada();">
		<input name="button" class="bottonRightCol" <c:if test="${empty listaQtdaNaoRetiradaVOParadaProgramadaCdl}">disabled="disabled"</c:if> value="<< Detalhar" type="button" onclick="exibirListaParadaProgramadaCdl();">
		<input name="button" class="bottonRightCol" <c:if test="${empty listaQtdaNaoRetiradaVOFalhaFornecimento}">disabled="disabled"</c:if> value="<< Detalhar" type="button" onclick="exibirListaFalhaFornecimento();">
		<input name="button" class="bottonRightCol" <c:if test="${empty listaQtdaNaoRetiradaVOCasoFortuitoForcaMaior}">disabled="disabled"</c:if> value="<< Detalhar" type="button" onclick="exibirListaCasoFortuitoForcaMaior();">
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo">Quantidade recuperada:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${qtdaRecuperada}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>) <b>(-)</b> </span><br />
		
		<label class="rotulo">QPNR:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${qpnr}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>) <b>(-)</b> </span><br />
		
		<label class="rotulo">Quantidade n�o retirada no per�odo:</label>
		<span class="itemDetalhamento"><fmt:formatNumber value="${qtdaNaoRetiradaPeriodo}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>)</span>
	</fieldset>
</fieldset>

<hr class="linhaSeparadora1" />

<fieldset id="resultadoApuracao" class="conteinerBloco">
	<legend class="conteinerBlocoTitulo">Resultado da Apura��o:</legend>
		
	<fieldset class="coluna" style="margin-bottom: 20px">			
		
		<label class="rotulo">QNR:</label>
		<span class="itemDetalhamento"><c:if test="${not empty (qnr)}"><fmt:formatNumber value="${qnr}" minFractionDigits="4" maxFractionDigits="4" type="number"/> (m<span class='expoente'>3</span>)</c:if></span><br />
		
		<label class="rotulo">Valor da Tarifa:</label>
		<span class="itemDetalhamento"><c:if test="${not empty (valorMedioTarifa)}">R$ <c:out value="${valorMedioTarifa}"></c:out></c:if></span><br />	
		
		<label class="rotulo">Valor calculado:</label>
		<c:if test="${not empty apuracao.valorCalculado}"><span class="itemDetalhamento">R$&nbsp</span></c:if>
		<span class="itemDetalhamento" id="valorCalculado"><fmt:formatNumber value="${apuracao.valorCalculado}" minFractionDigits="2"/></span><br />	
		
	</fieldset>
	
	<fieldset class="colunaMeio2">
		<label class="rotulo">Desconto:</label>
		
		<c:choose>
			<c:when test="${bloquearCampos eq true}">
				<input id="desc" class="campoTexto campoHorizontal" readonly="readonly" type="text" name="desconto" size="6" maxlength="10" value="<fmt:formatNumber value="${apuracao.percentualDescontoAplicado}" minFractionDigits="2"/>"></input><label class="rotuloInformativo">%</label>	
			</c:when>
			<c:otherwise>
				<input id="desc" class="campoTexto campoHorizontal" <c:if test="${habilitarCamposDesconto ne true}"> disabled="disabled"</c:if> type="text" name="desconto" size="6" maxlength="10" value="<fmt:formatNumber value="${apuracao.percentualDescontoAplicado}" minFractionDigits="2"/>" onfocus="configurarCamposDesconto(this.name, '')" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);configurarValoresCamposDesconto(this.name, '');"/><label class="rotuloInformativo">%</label>
			</c:otherwise>
		</c:choose>
				
		<br class="quebraLinha2"/>
		
		<label class="rotulo">Valor do desconto:</label>
		
		<c:choose>
			<c:when test="${bloquearCampos eq true}">
				<input type="text" id="descValor" class="campoTexto" readonly="readonly"  name="descontoValor" size="13" maxlength="10" value="<fmt:formatNumber value="${apuracao.valorDescontoAplicado}" minFractionDigits="2"/>"/><br />
			</c:when>
			<c:otherwise>
				<input type="text" id="descValor" class="campoTexto" <c:if test="${habilitarCamposDesconto ne true}"> disabled="disabled"</c:if>  name="descontoValor" size="13" maxlength="10" value="<fmt:formatNumber value="${apuracao.valorDescontoAplicado}" minFractionDigits="2"/>" onfocus="configurarCamposDesconto(this.name, '')" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);configurarValoresCamposDesconto(this.name, '');"/><br />			
			</c:otherwise>
		</c:choose>
				
		<label class="rotulo">Valor final:</label>
		<c:if test="${not empty valorFinal}"><span class='itemDetalhamento'>R$&nbsp</span></c:if>
		<span class="itemDetalhamento" id="valorFinal"><fmt:formatNumber value="${apuracao.valorCobrado}" minFractionDigits="2"/></span>
	</fieldset>
	
	<fieldset class="colunaFinal2">
		<label class="rotulo rotuloVertical">
		<span id="resultadoApuracaoObservacoes" class="campoObrigatorioSimbolo">* </span>Observa��es:</label>
		<textarea class="campoVertical" <c:if test="${bloquearCampos eq true}">readonly="readonly"</c:if> style="width: 287px; height: 105px" name="observacoes"><c:out value="${apuracao.observacoes}"/></textarea>
	</fieldset>
	
</fieldset>
