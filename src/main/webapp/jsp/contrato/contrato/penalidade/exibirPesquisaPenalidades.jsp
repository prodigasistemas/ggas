<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Penalidades<a class="linkHelp" href="<help:help>/consultadaspenalidades.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Selecione um Cliente, Im�vel ou quaisquer dos campos de pesquisa abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir as Penalidades. Se desejar especificar um Ponto de Consumo antes de pesquisar clique em <span class="destaqueOrientacaoInicial">Ponto de Consumo</span> para list�-los.</p>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/dhtmlxcombo.css">


<script type="text/javascript">

	$(document).ready(function(){

	 	$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#dataApuracaoInicio").change(function(){
			if($('#dataApuracaoInicio').val() != '__/__/____'){
				$('#dataApuracaoFim').datepicker('option', 'minDate', $("#dataApuracaoInicio").val());
				}else{
					$('#dataApuracaoFim').datepicker('option', 'minDate', '');
					}
		});
	
		
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);

		
				
	});

	function alterarPenalidade(idContrato, idPenalidade, idPontoConsumo, periodoApuracao, idModalidade){
		document.getElementById('idContrato').value = idContrato;
		document.getElementById('idPenalidade').value = idPenalidade;
		document.getElementById('idPontoConsumo').value = idPontoConsumo;
		document.getElementById('idModalidade').value = idModalidade;
		document.getElementById('periodoApuracao').value = periodoApuracao;
		
		submeter("penalidadeForm", "exibirAlterarPenalidade");
	}

	function exibirPenalidade(idPontoConsumo){
		var cobrada = document.forms[0].cobrada[0].checked;
		
		document.forms[0].cobrada[0].checked = cobrada;
		document.forms[0].idPontoConsumo.value = idPontoConsumo;
		pesquisar('penalidade');
	}	
	
	function pesquisar(tipoPesquisa){

		selectAllOptions(document.forms[0].idPenalidadesAssociadas);
		selectAllOptions(document.forms[0].idPenalidadesDisponiveis);
		document.forms[0].tipoPesquisa.value = tipoPesquisa;
		submeter("penalidadeForm", "pesquisarPenalidades");

	}

	function tratarPenalidades(){
		alert("remover botao");
        submeter("penalidadeForm", "tratarPenalidades");
    }

	function apurar(){
        alert("remover botao");
        submeter("penalidadeForm", "tratarPenalidades");
    }
    
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}

	function limparPesquisaPenalidades(){

		limparCamposPesquisa();		
		moveAllOptionsEspecial(document.forms[0].idPenalidadesAssociadas, document.forms[0].idPenalidadesDisponiveis, true);
		document.forms[0].numeroContrato.value = "";
		document.forms[0].dataApuracaoInicio.value = "";
		document.forms[0].dataApuracaoFim.value = "";
		document.forms[0].cobrada[0].checked = true;
			
	}	
	
	function limparCamposPesquisa(){

		limparFormularioDadosCliente();		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
	    } else {
	   		idImovel.value = "";
	    	matriculaImovel.value = "";
	    	nomeFantasia.value = "";
			numeroImovel.value = "";
	          	cidadeImovel.value = "";
	          	indicadorCondominio.value = "";
	   	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
		
	function init() {
		
		<c:choose>
			<c:when test="${penalidadeVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${penalidadeVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>

		var mensagensEstado = $(".mensagens").css("display");
		if(mensagensEstado == "none"){
			<c:if test="${listaPontoConsumoVO ne null && listaApuracaoQtdaPenalidadePeriodicidade eq null}">
				$.scrollTo($("#pontoConsumoVO"),800);
			</c:if>	
			<c:if test="${listaPontoConsumoVO eq null && listaApuracaoQtdaPenalidadePeriodicidade ne null}">
				$.scrollTo($("#linhaSeparadoraPenalidades"),1000);
			</c:if>
		}

		<c:if test="${exibirRelatorioNotasDebitosCreditos eq true}">
			this.efetuarDownloadRelatorioNotasCreditosDebitos();
		</c:if>
		
	}

	function efetuarDownloadRelatorioNotasCreditosDebitos() {
		var iframe = document.getElementById("download");
		if (iframe != undefined) {
			iframe.src = '<c:url value="/exibirRelatorioNotasDebitosCreditos"/>';
		}
	}

	//function teste(campoDataInicial){
		//$("#dataApuracaoFim").datepicker({minDate: new Date(2009, 10 - 1, 25)});
		//$('#dataApuracaoFim').datepicker('option', 'minDate', 3);
	//}

	addLoadEvent(init);	
	
</script>

<form:form method="post" action="exibirPesquisaPenalidades" id="penalidadeForm" name="penalidadeForm">

	<input id="tipoPesquisa" name="tipoPesquisa" type="hidden" value="${penalidadeVO.tipoPesquisa}"/>
	<input id="idContrato" name="idContrato" type="hidden" value=""/>
	<input id="idPontoConsumo" name="idPontoConsumo" type="hidden" value=""/>
	<input id="idPenalidade" name="idPenalidade" type="hidden" value=""/>
	<input id="idModalidade" name="idModalidade" type="hidden" value=""/>
	<input id="periodoApuracao" name="periodoApuracao" type="hidden" value="" />
	<input id="idApuracaoQtdaPenalidadePeriodicidade" name="idApuracaoQtdaPenalidadePeriodicidade" type="hidden" value="${penalidadeVO.idApuracaoQtdaPenalidadePeriodicidade}"/>
	
	<fieldset id="pesquisarPenalidades" class="conteinerPesquisarIncluirAlterar">
		
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${penalidadeVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.chavePrimaria}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nome}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${penalidadeVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nome}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.chavePrimaria}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.quadraFace.endereco.cep.municipio.descricao}">
				<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.municipio.descricao}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
				
		<fieldset class="conteinerBloco">
			<fieldset class="colunaEsq">
				<label class="rotulo rotuloVertical">Penalidades:</label><br />
				<select class="campoList campoVertical" style="width: 162px" name="idPenalidadesDisponiveis" multiple="multiple" onclick="moveSelectedOptionsEspecial(document.forms[0].idPenalidadesDisponiveis, document.forms[0].idPenalidadesAssociadas, true);">
		   			<c:forEach items="${listaPenalidades}" var="penalidade">
		   				<c:choose>
		   					<c:when test="${not empty penalidadeVO.idPenalidadesAssociadas}">
		   						<c:forEach items="${penalidadeVO.idPenalidadesDisponiveis}" var="idPenalidadesDisponivel">
		   							<c:if test="${penalidade.chavePrimaria eq idPenalidadesDisponivel}">
										<option value="<c:out value="${penalidade.chavePrimaria}"/>"><c:out value="${penalidade.descricao}"/></option>
									</c:if>			   						
		   						</c:forEach>			   					
		   					</c:when>
		   					<c:otherwise>
		   						<option title="<c:out value="${penalidade.descricao}"/>" value="<c:out value="${penalidade.chavePrimaria}"/>"> 
									<c:out value="${penalidade.descricao}"/>
								</option>			   					
		   					</c:otherwise>	
		   				</c:choose>
		   			</c:forEach>
		   		</select>														
				<fieldset class="conteinerBotoesCampoList1a">						
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" id="botaoDireita" onClick="moveSelectedOptionsEspecial(document.forms[0].idPenalidadesDisponiveis, document.forms[0].idPenalidadesAssociadas, true);">
					<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos" onClick="moveAllOptionsEspecial(document.forms[0].idPenalidadesDisponiveis, document.forms[0].idPenalidadesAssociadas, true);">
					
					<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop" onclick="moveSelectedOptionsEspecial(document.forms[0].idPenalidadesAssociadas, document.forms[0].idPenalidadesDisponiveis, true);">
					<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos" onClick="moveAllOptionsEspecial(document.forms[0].idPenalidadesAssociadas, document.forms[0].idPenalidadesDisponiveis, true);">
				</fieldset>				
				
				<select class="campoList" name="idPenalidadesAssociadas" multiple="multiple" onclick="moveSelectedOptionsEspecial(document.forms[0].idPenalidadesAssociadas, document.forms[0].idPenalidadesDisponiveis, true);">
					<c:forEach items="${listaPenalidades}" var="penalidade">
						<c:forEach items="${penalidadeVO.idPenalidadesAssociadas}" var="idPenalidadesAssociada">
							<c:if test="${penalidade.chavePrimaria eq idPenalidadesAssociada}">
								<option title="<c:out value="${penalidade.descricao}"/>" value="<c:out value="${penalidade.chavePrimaria}"/>">
									<c:out value="${penalidade.descricao}"/>
								</option>
							</c:if>
						</c:forEach>
					</c:forEach>
				</select>
			</fieldset>
			
			<fieldset class="colunaDir" style="margin-right: 80px">
				<label class="rotulo" for="numeroContrato">Contrato:</label>
				<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="9" size="8" value="${penalidadeVO.numeroContrato}" onkeyup="return validarCriteriosParaCampo(this, '1', '0');" onkeypress="return formatarCampoInteiro(event);"><br />
				
				<label class="rotulo">Data de apura��o:</label>
				<input class="campoData campoHorizontal" type="text" id="dataApuracaoInicio" name="dataApuracaoInicio" maxlength="10" value="${penalidadeVO.dataApuracaoInicio}" />
				<label class="rotuloEntreCampos" for="dataApuracaoFim"> a </label>
				<input class="campoData campoHorizontal" type="text" id="dataApuracaoFim" name="dataApuracaoFim" maxlength="10" value="${penalidadeVO.dataApuracaoFim}"><br /> 
				
				<label class="rotulo">Cobrada:</label>
				<input class="campoRadio" type="radio" name="cobrada" id="ativo" value="true" <c:if test="${penalidadeVO ne null ? penalidadeVO.cobrada eq 'true' : indicadorPenalidadeCobrada eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="ativo">Sim</label>
				<input class="campoRadio" type="radio" name="cobrada" id="inativo" value="false" <c:if test="${penalidadeVO ne null ? penalidadeVO.cobrada eq 'false' : indicadorPenalidadeCobrada eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="inativo">N�o</label>
				<input class="campoRadio" type="radio" name="cobrada" id="todos" value="" <c:if test="${penalidadeVO ne null ? empty  penalidadeVO.cobrada : empty indicadorPenalidadeCobrada}">checked</c:if>>
				<label class="rotuloRadio" for="todos">Todos</label>												
			</fieldset>
			
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="pesquisarPenalidades">
					<input class="bottonRightCol2 botaoPontosDeConsumo" type="button" id="botaoPontoConsumo" value="Pontos de Consumo" onclick="pesquisar('pontoConsumo');">
				</vacess:vacess>
				<vacess:vacess param="pesquisarPenalidades">
					<input class="bottonRightCol2" type="button" id="botaoPesquisar" value="Pesquisar" onclick="pesquisar('penalidade');">
				</vacess:vacess>
				<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparPesquisaPenalidades();">
				
				
			</fieldset>
		</fieldset>
	</fieldset>
		
	<c:if test="${listaPontoConsumoVO ne null}">
	
		<hr class="linhaSeparadoraPesquisa" />
		<p class="orientacaoInicial">Selecione um Ponto de Consumo clicando na lista abaixo para exibir as Penalidades especificas do Ponto de Consumo</p>
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumoVO" id="pontoConsumoVO" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPenalidades">
	    				    	
	    	<display:column sortable="true" title="Contrato" sortProperty="chavePrimaria" style="width: 80px">
	    		<a href="javascript:exibirPenalidade(<c:out value='${pontoConsumoVO.pontoConsumo.chavePrimaria}'/>)">
	    			<c:out value="${pontoConsumoVO.contrato.numeroFormatado}"/>			    		
	    		</a>			    		
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Im�vel - Ponto de Consumo" sortProperty="pontoConsumo.descricao" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
	    		<a href="javascript:exibirPenalidade(<c:out value='${pontoConsumoVO.pontoConsumo.chavePrimaria}'/>)">
	    			<c:out value="${pontoConsumoVO.pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumoVO.pontoConsumo.descricao}"/>
	    		</a>			    		
	    	</display:column>
	    	
	    	<display:column sortable="true" title="Segmento" sortProperty="segmento.descricao" style="width: 130px">
	    		<a href="javascript:exibirPenalidade(<c:out value='${pontoConsumoVO.pontoConsumo.chavePrimaria}'/>)">
	    			<c:out value="${pontoConsumoVO.pontoConsumo.segmento.descricao}"/>			    		
	    		</a>			    	
	    	</display:column>
	    				
			<display:column sortable="true" title="Situa��o" sortProperty="situacaoConsumo.descricao" style="width: 160px">
				<a href="javascript:exibirPenalidade(<c:out value='${pontoConsumoVO.pontoConsumo.chavePrimaria}'/>)">
	    			<c:out value="${pontoConsumoVO.pontoConsumo.situacaoConsumo.descricao}"/>			    		
	    		</a>										
			</display:column>
						    	
		</display:table>
					
	</c:if>
	
	<c:if test="${listaApuracaoQtdaPenalidadePeriodicidade ne null}">
		
		<hr id="linhaSeparadoraPenalidades" class="linhaSeparadoraPesquisa" />
		<p class="orientacaoInicial">Selecione uma penalidade listada abaixo para altera-la.</p>
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaApuracaoQtdaPenalidadePeriodicidade" id="apuracao" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPenalidades">
	    				    	
	    	<display:column title="Contrato" sortable="true" sortProperty="contrato.numero" style="width: 80px">
	    		<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	    			<c:out value="${apuracao.numeroContrato}"/>
	    		</a>
	    	</display:column>
	   		
	   		<display:column title="Im�vel - Ponto de Consumo" sortable="true" sortProperty="imovel.nome" style="width: 220px">
	   			<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	   				<c:out value="${apuracao.nomeImovel}"/> - <c:out value="${apuracao.descricaoPontoConsumo}"/>
	   			</a>
	   		</display:column>
	   		
	   		<display:column title="Cliente" sortable="true" sortProperty="clienteAssinatura.nome">
	   			<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	   				<c:out value="${apuracao.nomeClienteAssinatura}"/>	
	   			</a>
	   		</display:column>
	   		
	   		<display:column title="Modalidade" sortable="true" sortProperty="contratoModalidade.descricao" style="width: 80px">
	   			<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	   				<c:out value="${apuracao.descricaoModalidade}"/>	
	   			</a>
	   		</display:column>
	   		
	   		<display:column title="Penalidade" sortable="true" sortProperty="penalidade.descricao" style="width: 80px">
	   			<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	   				<c:out value="${apuracao.descricaoPenalidade}"/>	
	   			</a>			   		
	   		</display:column>
	   		
	   		<display:column title="Per�odo" sortable="true" sortProperty="periodoApuracao" style="width: 65px">
	   			<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	   				<c:out value="${apuracao.periodoApuracaoFormatado}"/>	
	   			</a>			   			
	   		</display:column>
	   		
	   		<display:column title="Valor (R$)" sortable="true" sortProperty="somaValorCalculado" style="width: 100px; text-align: right">
	   			<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	   				<fmt:formatNumber value="${apuracao.valorCalculadoPercentual}" minFractionDigits="2" />
	   			</a>			   		
	   		</display:column>
	   		
	   		<display:column title="Valor Final (R$)" sortable="true" sortProperty="somaValorCobrado" style="width: 100px; text-align: right">
	   			<a href="javascript:alterarPenalidade(<c:out value='${apuracao.chaveContrato}'/>,<c:out value='${apuracao.chavePenalidade}'/>,<c:choose><c:when test='${not empty apuracao.chavePontoConsumo}'><c:out value='${apuracao.chavePontoConsumo}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>,<c:out value='${apuracao.periodoApuracao}'/>, <c:choose><c:when test='${not empty apuracao.chaveModalidade}'><c:out value='${apuracao.chaveModalidade}'/></c:when><c:otherwise><c:out value='0'/></c:otherwise></c:choose>)">
	   				<fmt:formatNumber value="${apuracao.valorCobrado}" minFractionDigits="2" />
	   			</a>
	   		</display:column>				   					   								    	
		</display:table>
	</c:if>
</form:form>

<c:if test="${exibirRelatorioNotasDebitosCreditos}">
	<iframe id="download" src ="" width="0" height="0"></iframe> 
</c:if>