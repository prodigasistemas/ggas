<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type="text/javascript">

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosPontosConsumo', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosContrato', 'fade=0,speed=400,persist=1,hide=0');
	
	function configurarCamposDesconto(nomeCampo, posicao){

		nomeCampo = this.formartarNomeCampoDesconto(nomeCampo);

		var idCampoDesabilitado = '';
		var idCampoHabilitado = nomeCampo + posicao;
		
		if(nomeCampo == "desc"){
			idCampoDesabilitado = 'descValor' + posicao;
		}else if(nomeCampo == "descValor"){
			idCampoDesabilitado = 'desc' + posicao;
		}

		if (idCampoDesabilitado != ''){
			document.getElementById(idCampoDesabilitado).disabled = true;
		}
		
		document.getElementById(idCampoHabilitado).disabled = false;
		$("#"+idCampoDesabilitado).css({"background":"#EBEBE4","color":"#666","padding":"2px"});
	}
	
	function limparCamposComErro(){
		$.each($(".campoTexto"), function() {
			var textoCampo = $(this).val();
			
			if (textoCampo.indexOf("NaN") > -1){
				$(this).val("");
			}
		});
	}
	
	function isNumericoValido(valorNumerico){
		if (valorNumerico.indexOf("NaN") > -1 || valorNumerico == ""){
			return false;
		}
		
		return true;
	}
		
	function configurarValoresCamposDesconto(nomeCampo, posicao){

		nomeCampo = this.formartarNomeCampoDesconto(nomeCampo);

		var idCampoDesc = 'desc' + posicao;
		var idCampoDescValor = 'descValor' + posicao;
		var idCampoDescMetro = 'descMetro' + posicao;
		var idLabelTarifaMedia = "lblTarifaMedia" + posicao;
		
		var idCampoValorCalculado = 'valorCalculado' + posicao;
		var idCampoValorFinal = 'valorFinal' + posicao;
		var idCampoValorFinalHidden = 'valorFinalHidden' + posicao;	
		
		var campoValorCalculado = document.getElementById(idCampoValorCalculado);
		var campoDesc = document.getElementById(idCampoDesc);
		var campoDescValor = document.getElementById(idCampoDescValor);
		var campoValorFinal = document.getElementById(idCampoValorFinal);
		var campoValorFinalHidden = document.getElementById(idCampoValorFinalHidden);
		var campoTotalValorFinal = document.getElementById('totalValorFinal');

		//inicio campo metro c�bico
		if (document.getElementById(idCampoDescMetro)!= null) {
			var campoDescMetro = document.getElementById(idCampoDescMetro);
			var tarifaMedia = document.getElementById(idLabelTarifaMedia);
	
			var valorCampoDescMetro = campoDescMetro.value.replace(".","").replace(",","."); 	alert(valorCampoDescMetro);
			valorCampoDescMetro = parseFloat(valorCampoDescMetro);
			
			var tarifaMediaValor = tarifaMedia.value;
			tarifaMediaValor = parseFloat(tarifaMediaValor);
			
			var valorMetroCubico = tarifaMediaValor * valorCampoDescMetro;
		}	
		//fim campo metro c�bico

		if(campoDesc.value == undefined || campoDesc.value == "0,00" || campoDesc.value == ""){
			 $("#resultadoApuracaoObservacoes").hide()
			 $("#resultadoApuracaoObservacoes").parent("label").removeClass("campoObrigatorio");
		}else {
			$("#resultadoApuracaoObservacoes").show()
			 $("#resultadoApuracaoObservacoes").parent("label").addClass("campoObrigatorio");
		}
		
		if(nomeCampo == "desc"){
			this.aplicarPercentualDescontoSobreValor(campoValorCalculado, campoDesc , campoDescValor, campoValorFinal, campoValorFinalHidden, campoTotalValorFinal);
			
			var campoDescValorFloat = parseFloat(campoDescValor.value.replace(".","").replace(",","."));
			var valorMetroCubico = campoDescValorFloat / tarifaMediaValor;
			valorMetroCubico = Math.round(valorMetroCubico);
			
			campoDescMetro.value = valorMetroCubico;
			aplicarMascaraNumeroDecimalTruncadoSeparadorDecimalPonto(campoDescMetro, 2);
		}else if(nomeCampo == "descValor"){
			var campoDescValorFloat = parseFloat(campoDescValor.value.replace(".","").replace(",","."));
			var valorMetroCubico = campoDescValorFloat / tarifaMediaValor;
			valorMetroCubico = Math.round(valorMetroCubico);
			
			campoDescMetro.value = valorMetroCubico;
			aplicarMascaraNumeroDecimalTruncadoSeparadorDecimalPonto(campoDescMetro, 2);
			
			this.aplicarValorDescontoSobreValor(campoValorCalculado, campoDesc , campoDescValor, campoValorFinal, campoValorFinalHidden, campoTotalValorFinal, campoDescMetro);							
		}else if (nomeCampo == "descMetro"){
			campoDescValor.value = valorMetroCubico;
			aplicarMascaraNumeroDecimalTruncadoSeparadorDecimalPonto(campoDescValor, 2);
			
			this.aplicarValorDescontoSobreValor(campoValorCalculado, campoDesc , campoDescValor, campoValorFinal, campoValorFinalHidden, campoTotalValorFinal, campoDescMetro);
		}
			
		campoDesc.disabled = false;
		campoDescValor.disabled = false;

		$("#"+idCampoDesc).css({"background":"#FFF","color":"#000"});
		$("#"+idCampoDescValor).css({"background":"#FFF","color":"#000"});
		

		limparCamposComErro();
	}

	function aplicarPercentualDescontoSobreValor(campoValorCalculado, campoPercentualDesconto, campoValorDesconto, campoValorFinal, campoValorFinalHidden, campoTotalValorFinal){
		
		var valorCalculado = trim(campoValorCalculado.innerHTML);
		var percentualDesconto = trim(campoPercentualDesconto.value);
		var valorPercentual = obterValorDesconto(campoPercentualDesconto);
		var valorFinal = trim(campoValorFinal.innerHTML);
		var totalValorFinal = campoTotalValorFinal != null ? trim(campoTotalValorFinal.innerHTML) : null;
				
		if(valorPercentual != null && valorPercentual > 100){

			campoPercentualDesconto.value = "";
			percentualDesconto = null;
			alert('<fmt:message key="ERRO_NEGOCIO_PERCENTUAL_DESCONTO_MAIOR_QUE_VALOR_CALCULADO"/>');	

		}

		if (isNumericoValido(percentualDesconto) == false){
			percentualDesconto = "";
		}
		
		AjaxService.aplicarPercentualDescontoSobreValorTotal(valorCalculado, percentualDesconto, valorFinal, totalValorFinal,
			{callback: function(dados){
				campoValorDesconto.value = dados[0];
				campoValorFinal.innerHTML = dados[1];
				if(campoValorFinalHidden != null){
					campoValorFinalHidden.value = dados[1];
				}
				if(campoTotalValorFinal != null){
					campoTotalValorFinal.innerHTML = dados[2];
				}					
			}, async: false} 
		);

								
	}
	
	function aplicarValorDescontoSobreValor(campoValorCalculado, campoPercentualDesconto, campoValorDesconto, campoValorFinal, campoValorFinalHidden, campoTotalValorFinal, campoDescMetro){
		
		var valorCalculadoFormatado = trim(campoValorCalculado.innerHTML);
		var valorDescontoFormatado = campoValorDesconto.value;
		var valorCalculado = converterStringParaFloat(valorCalculadoFormatado);
		var valorDesconto = obterValorDesconto(campoValorDesconto);
		var valorFinal = trim(campoValorFinal.innerHTML);
		var totalValorFinal = campoTotalValorFinal != null ? trim(campoTotalValorFinal.innerHTML) : null;

		if(valorDesconto != null && (valorDesconto > valorCalculado)){

			campoValorDesconto.value = '';
			campoDescMetro.value = '';
			valorDescontoFormatado = null;
			alert('<fmt:message key="ERRO_NEGOCIO_VALOR_DESCONTO_MAIOR_QUE_VALOR_CALCULADO"/>');

		}	
		
		if (isNumericoValido(valorDescontoFormatado) == false){
			valorDescontoFormatado = "";
		}
		
		AjaxService.aplicarValorDescontoSobreValorTotal(valorCalculadoFormatado, valorDescontoFormatado, valorFinal, totalValorFinal, 
			function(dados){
				campoPercentualDesconto.value = dados[0];
				campoValorFinal.innerHTML = dados[1];
				if(campoValorFinalHidden != null){
					campoValorFinalHidden.value = dados[1];
				}
				if(campoTotalValorFinal != null){
					campoTotalValorFinal.innerHTML = dados[2];
				}	
			}
		);	
		
	}

	function formartarNomeCampoDesconto(nomeCampo){
		
		if(nomeCampo == 'descontoValor'){
			return 'descValor';
		}else if(nomeCampo == 'desconto'){
			return 'desc';
		}else{
			return nomeCampo;
		}

	}
	
	function obterValorDesconto(campo){
		
		var valor = trim(campo.value);	
		
		if(valor != ''){
			return this.converterStringParaFloat(valor);		
		}else{
			return null;	
		}
		
	}
	
	function cancelar(){
		submeter("penalidadeForm", "pesquisarPenalidades");
	}

	function aplicar(){

		if(confirm('Voc� tem certeza que deseja executar a opera��o ?')){
			$("#descValor").prop("disabled", false);
			submeter("penalidadeForm", "aplicarPenalidade");
		}
		
	}
	
	function salvar(){
		submeter("penalidadeForm", "salvarPenalidade");
	}
	

	function recalcular(){

		if(confirm('Voc� tem certeza que deseja executar a opera��o ?')){
			limparDadosRecalcular();
			submeter("penalidadeForm", "recalcularPenalidade");
		}

	}

	function limparDadosRecalcular(){

		var vlFinal = document.forms[0].vlFinal;
		var descValor = document.forms[0].descValor;
		var desc = document.forms[0].desc;
		var obs = document.forms[0].obs;

		var valorFinal = document.forms[0].valorFinal;
		var descontoValor = document.forms[0].descontoValor;
		var desconto = document.forms[0].desconto;
		var observacoes = document.forms[0].observacoes;
		
		if(vlFinal != null){
			limparCampoArray(vlFinal);
		}

		if(descValor != null){
			limparCampoArray(descValor);
		}

		if(desc != null){
			limparCampoArray(desc);
		}

		if(obs != null){
			limparCampoArray(obs);
		}

		if(valorFinal != null){
			valorFinal.value = "";			
		}
		
		if(descontoValor != null){
			descontoValor.value = "";
		}

		if(desconto != null){
			desconto.value = "";
		}

		if(observacoes != null){
			observacoes.value = "";
		}
		
	}

	function limparCampoArray(campo){

		for(i = 0; i < campo.length; i++){
			campo[i].value = "";
		}
			
	}
		
</script>

<form:form action="alterarPenalidade" id="penalidadeForm" name="penalidadeForm">

	<input id="tipoPesquisa" type="hidden" name="tipoPesquisa" value="${penalidadeVO.tipoPesquisa}"/>
	<input id="idContrato" name="idContrato" type="hidden" value="${penalidadeVO.idContrato}"/>
	<input id="idPontoConsumo" name="idPontoConsumo" type="hidden" value="${penalidadeVO.idPontoConsumo}"/>
	<input id="idPenalidade" name="idPenalidade" type="hidden" value="${penalidadeVO.idPenalidade}"/>
	<input id="idModalidade" name="idModalidade" type="hidden" value="${penalidadeVO.idModalidade}"/>
	<input id="periodoApuracao" name="periodoApuracao" type="hidden" value="${penalidadeVO.periodoApuracao}"/>
	<input id="idApuracaoQtdaPenalidadePeriodicidade" name="idApuracaoQtdaPenalidadePeriodicidade" type="hidden" value="${idApuracaoQtdaPenalidadePeriodicidade}"/>
	<input id="exibirDadosFiltro" type="hidden" name="exibirDadosFiltro" value="true"/>
	
	<h1 class="tituloInterno"> Alterar Penalidade - <c:out value="${descricaoPenalidade}"/><a class="linkHelp" href="<help:help>/penalidadesalteracao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
			
	<fieldset id="alterarPenalidade" class="conteinerPesquisarIncluirAlterar">			
		<fieldset class="conteinerBloco">
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosContrato" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Contrato <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosContrato" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">N�mero de contrato:</label>
					<span class="itemDetalhamento"><c:out value="${numeroContrato}"/></span><br />
													
					<label class="rotulo">Data de assinatura:</label>
					<span class="itemDetalhamento"><c:out value="${dataAssinaturaContrato}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo rotulo2Linhas" id="rotuloDataVencimentoObrigacoesContratuais">Data de vencimento das<br />obriga��es contratuais:</label>
					<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${dataVencimentoObrigacoesContratuaisContrato}"/></span>
				</fieldset>
			</fieldset>
			
			<hr class="linhaSeparadora2" />
			
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Cliente:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
									
					<label class="rotulo">CPF/CNPJ:</label>
					<span class="itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
				</fieldset>
				
				<fieldset class="colunaFinal">
					<label class="rotulo">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
					
					<label class="rotulo">E-mail:</label>
					<span class="itemDetalhamento"><c:out value="${cliente.emailPrincipal}"/></span><br />
				</fieldset>
			</fieldset>
			
			<hr class="linhaSeparadora2" />
			
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosPontosConsumo" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de consumo <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosPontosConsumo" class="conteinerDados conteinerBloco">
				<display:table class="dataTableGGAS dataTableAba" name="listaPontoConsumo" id="pontoConsumo">
					<display:column title="Descri��o">
						<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>
	   				</display:column>
	   				
	   				<display:column title="Endere�o" style="width: 500px">
	   					<c:out value="${pontoConsumo.enderecoFormatado}"/>
	   				</display:column>
	   				
	   				<display:column title="CEP" style="width: 70px">
	   					<c:out value="${pontoConsumo.cep.cep}"/>
	   				</display:column>				
				</display:table>
			</fieldset>		
		</fieldset>
		
		<hr id="linhaSeparadoraPenalidades" class="linhaSeparadora2" />
		<fieldset class="conteinerBloco">
			<c:if test="${penalidadeVO.idPenalidade eq codigoPenalidadeTakeOrPay}">
				<jsp:include page="/jsp/contrato/contrato/penalidade/dadosPenalidadeTakeOrPay.jsp"/>
			</c:if>
			
			<c:if test="${penalidadeVO.idPenalidade eq codigoPenalidadeShipOrPay}">
				<jsp:include page="/jsp/contrato/contrato/penalidade/dadosPenalidadeShipOrPay.jsp"/>			
			</c:if>
			
			<c:if test="${penalidadeVO.idPenalidade eq codigoPenalidadeDeliveryOrPay}">
				<jsp:include page="/jsp/contrato/contrato/penalidade/dadosPenalidadeDeliveryOrPay.jsp"/>			
			</c:if>
			
			<c:if test="${penalidadeVO.idPenalidade eq codigoPenalidadeRetiradaMaior}">
				<jsp:include page="/jsp/contrato/contrato/penalidade/dadosPenalidadeRetiradaMaior.jsp"/>			
			</c:if>
			
			<c:if test="${penalidadeVO.idPenalidade eq codigoPenalidadeRetiradaMenor}">
				<jsp:include page="/jsp/contrato/contrato/penalidade/dadosPenalidadeRetiradaMenor.jsp"/>			
			</c:if>
			
			<c:if test="${penalidadeVO.idPenalidade eq codigoPenalidadeGasForaDeEspecificacao}">
				<jsp:include page="/jsp/contrato/contrato/penalidade/dadosPenalidadeGasForaDeEspecificacao.jsp"/>			
			</c:if>
		</fieldset>
	</fieldset>
	<fieldset class="conteinerBotoes"> 
			<fieldset class="colunaEsq">
				<input name="button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
			</fieldset>
			<fieldset class="colunaDir">
				<input name="button" class="bottonRightCol2 botaoRecalcular" <c:if test="${bloquearCampos eq true}">disabled="disabled"</c:if> value="Recalcular" type="button" onclick="recalcular();">
			<c:if test="${empty modalidadeContrato}">
				<input name="button" class="bottonRightCol2 " value="Salvar" type="button" onclick="salvar();">
			</c:if>
			<input name="button" class="bottonRightCol2 bottonRightColUltimo botaoAplicar" <c:if test="${bloquearCampos}">disabled="disabled"</c:if> value="Aplicar" type="button" onclick="aplicar();">			
			</fieldset>
	 	</fieldset>
	 	
	<token:token></token:token>
	
</form:form>