<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/contrato/cadastrocontrato/contrato.js"></script>

<script>
	$(function(){
		
		//Verifica se existem campos obrigat�rios e exibe ou esconde a legenda.
		var campoObrigatorioConsumo = $("#consumo .campoObrigatorio").size();
		if (campoObrigatorioConsumo > 0){
			$("#consumo p.legenda").show();
		} else {
			$("#consumo p.legenda").hide();				
		};
	});
		
	function limparAbaConsumo(){
		document.getElementById('tamReducaoRecuperacaoPCS').value = '';	
		document.getElementById('horaInicialDia').value = '';	
		document.getElementById('regimeConsumo').value = '-1';
		document.getElementById('fornecimentoMaximoDiario').value = '';
		document.getElementById('unidadeFornecMaximoDiario').value = '-1';
		document.getElementById('fornecimentoMinimoDiario').value = '';
		document.getElementById('unidadeFornecMinimoDiario').value = '-1';
		document.getElementById('fornecimentoMinimoMensal').value = '';
		document.getElementById('unidadeFornecMinimoMensal').value = '-1';
		document.getElementById('fornecimentoMinimoAnual').value = '';	
		document.getElementById('unidadeFornecMinimoAnual').value = '-1';	
		document.getElementById('fatorUnicoCorrecao').value = '';	
		document.getElementById('consumoFatFalhaMedicao').value = '-1';
		moveAllOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true);	
		moveAllOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);
		ajustarAsteriscosConsumo();
	}

	function habilitarCampoReducaoIntervalo(){

		var tamReducaoSel = isCampoSelecionadoTamanhoReducao();
		var listaIntervalo = document.getElementById('idsIntervaloAmostragemAssociados');		

		if (listaIntervalo != undefined) {
			for (i=0; i<listaIntervalo.length; i++){
				if(listaIntervalo.options[i].value == 2 && tamReducaoSel == true){
					document.getElementById("tamReducaoRecuperacaoPCS").disabled = false;
					document.getElementById("tamReducaoRecuperacaoPCS").setAttribute("class", "campoTexto");
					mostrarAsteriscoConsumo(document.getElementById('idsIntervaloAmostragemAssociados'),['tamReducaoRecuperacaoPCS']);
				}
			}
	    }
	}

	function isCampoSelecionadoTamanhoReducao(){
		var campo = '<c:out value="${contratoVO.selTamReducaoRecuperacaoPCS}"/>';
		if( campo != undefined){
			return (campo == "" || campo == null) ? false:true;
		}
	}
	
	function desabilitarCampoReducaoIntervalo(){		
		var listaIntervalo = document.getElementById('intervalosAmostragemDisponiveis');		
		if (listaIntervalo != undefined) {
			for (i=0; i<listaIntervalo.length; i++){
				if(listaIntervalo.options[i].value == 2){
					document.getElementById("tamReducaoRecuperacaoPCS").value = "";
					document.getElementById("tamReducaoRecuperacaoPCS").disabled = true;
					document.getElementById("tamReducaoRecuperacaoPCS").setAttribute("class", "campoTexto campoDesabilitado");
					mostrarAsteriscoConsumo('',['tamReducaoRecuperacaoPCS']);
				}
			}
	    }
	}

	function carregarFatorUnicoCorrecao(){		
		var fatorUnico = '<c:out value="${contratoVO.fatorUnicoCorrecao}" default=''/>';
		fatorUnico = fatorUnico.replace(".",",");
		
		var chaveFaixaPressao = document.getElementById("faixaPressaoFornecimento").value;
		var selectFatorUnicoCorrecao = document.getElementById("fatorUnicoCorrecao");
	    
		selectFatorUnicoCorrecao.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectFatorUnicoCorrecao.options[selectFatorUnicoCorrecao.length] = novaOpcao;
				
		if(chaveFaixaPressao > 0){
			AjaxService.obterFatorCorrecaoPTZPCSporFaixaPressao( chaveFaixaPressao, {
				callback: function(fatorCorrecao) {
            		if(fatorCorrecao != undefined){            		      		         		
                		var novaOpcao = new Option(fatorCorrecao, fatorCorrecao);
                    	selectFatorUnicoCorrecao.options[selectFatorUnicoCorrecao.length] = novaOpcao;
                    }
                	if(fatorUnico != ''){
                		selectFatorUnicoCorrecao.value = fatorUnico;
                    }
                }, async:false}
            );
		}
	}

	function mostrarAsteriscoConsumo(elem,nomeCampo) {
		var mascaraData = "__/__/____";
		if (elem != undefined) {
			for (var i = 0; i < nomeCampo.length; i++) {
				var descricaoLabel = $("label[for="+nomeCampo[i]+"]").html();
				// Se o campo pai tem algum valor.
				if (elem.length > 0) {
					// Se o campo pai mudar de valor n�o adiciona outro asterisco, pois j� existe.
					if ($("#span"+nomeCampo[i]).length == 0) {
						var span = "<span id='span"+ nomeCampo[i] +"' class='campoObrigatorioSimbolo2'>* </span>";
						$("label[for="+nomeCampo[i]+"]").html(span + descricaoLabel);
						$("label[for="+nomeCampo[i]+"]").addClass("campoObrigatorio");
					}

				// Se o campo pai n�o tem valor.
				} else {
					var classe = $("#span"+nomeCampo[i]).attr("class");
					// S� remove caso o label tenha asterisco preto.
					if (classe == "campoObrigatorioSimbolo2") {
						$("#span"+nomeCampo[i]).remove();
						$("label[for="+nomeCampo[i]+"]").removeClass("campoObrigatorio");
					}
				}
			}
		}
	}

	function ajustarAsteriscosConsumo() {
		if (!$("#idsLocalAmostragemAssociados").attr("disabled")) {
			mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);
		}
	}

	function init() {
		ajustarAsteriscosConsumo();
	}
	
	addLoadEvent(init);
	
</script>

<fieldset id="consumo">		
	<fieldset>
		<c:choose>
			<c:when test="${contratoVO.fluxoDetalhamento}">
				<a class="linkHelp" href="<help:help>/detalhamentocontratoabaconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
			</c:when>
			<c:otherwise>
				<a class="linkHelp" href="<help:help>/abaconsumoinclusoalteraoadiodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
			</c:otherwise>
		</c:choose>
		<fieldset id="usoPCS" class="colunaEsq">
			<legend>Uso do PCS:</legend>
			<div class="conteinerDados">
				<fieldset id="conteinerLocalAmostragem">
					<legend><ggas:labelContrato styleClass="asteriscoLegend" forCampo="localAmostragemPCS"> </ggas:labelContrato>Local de amostragem:</legend>
					<fieldset class="conteinerDados2">
						<label class="rotulo rotuloVertical" for="ordemPrioridadePCS">Ordem de Prioridade:<span class="itemContador" id="quantidadePontosAssociados"></span></label><br />
						<select id="locaisAmostragemDisponiveis" class="campoList campoVertical" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].locaisAmostragemDisponiveis,document.forms[0].idsLocalAmostragemAssociados,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
							<c:forEach items="${listaLocalAmostragemPCS}" var="localAmostragem">
								<option value="<c:out value="${localAmostragem.chavePrimaria}"/>" title="<c:out value="${localAmostragem.descricao}"/>">
									<c:out value="${localAmostragem.descricao}"/>
								</option>
							</c:forEach>
					  	</select>
					  	<fieldset class="conteinerBotoesCampoList1a">
						<input id="adicionar-local-amostragem" type="button" name="right" value="&gt;&gt;" class="bottonRightCol" onClick="moveSelectedOptionsEspecial(document.forms[0].locaisAmostragemDisponiveis,document.forms[0].idsLocalAmostragemAssociados,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
						<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" onClick="moveAllOptionsEspecial(document.forms[0].locaisAmostragemDisponiveis,document.forms[0].idsLocalAmostragemAssociados,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
						<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop" onClick="moveSelectedOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
						<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" onClick="moveAllOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
						</fieldset>
					
					<select id="idsLocalAmostragemAssociados" class="campoList" name="idsLocalAmostragemAssociados" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
						<c:forEach items="${listaLocalAmostragemAssociados}" var="localAmostragemAssoc">
							<option value="<c:out value="${localAmostragemAssoc.chavePrimaria}"/>" title="<c:out value="${localAmostragemAssoc.descricao}"/>">
								<c:out value="${localAmostragemAssoc.descricao}"/>
							</option>
						</c:forEach>
					  	</select>
					  	<fieldset class="conteinerBotoesCampoList2">
						<input type="button" class="bottonRightCol botoesLargosIE7" value="Para cima" onclick="moveOptionUp(this.form['idsLocalAmostragemAssociados']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
						<input type="button" class="bottonRightCol botoesLargosIE7" value="Para baixo" onclick="moveOptionDown(this.form['idsLocalAmostragemAssociados']);" <ggas:campoSelecionado campo="localAmostragemPCS"/>>
						</fieldset>
					</fieldset>
				</fieldset>
				
				<fieldset id="conteinerIntervaloAmostragem">					
					<legend><ggas:labelContrato styleClass="asteriscoLegend" forCampo="intervaloRecuperacaoPCS"> </ggas:labelContrato>Intervalo de amostragem:</legend>
					<fieldset class="conteinerDados2">
						<label class="rotulo rotuloVertical" for="ordemPrioridadePCS">Ordem de Prioridade:<span class="itemContador" id="quantidadePontosAssociados"></span></label><br />
						<select id="intervalosAmostragemDisponiveis" class="campoList campoVertical" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].intervalosAmostragemDisponiveis,document.forms[0].idsIntervaloAmostragemAssociados,true);habilitarCampoReducaoIntervalo();" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
							<c:forEach items="${listaIntervaloPCS}" var="intervalo">
								<option value="<c:out value="${intervalo.chavePrimaria}"/>" title="<c:out value="${intervalo.descricao}"/>">
									<c:out value="${intervalo.descricao}"/>
								</option>
							</c:forEach>
					  	</select>
					  	<fieldset class="conteinerBotoesCampoList1a">
							<input id="adicionar-intervalo-amostragem" type="button" name="right" value="&gt;&gt;" class="bottonRightCol" onClick="moveSelectedOptionsEspecial(document.forms[0].intervalosAmostragemDisponiveis,document.forms[0].idsIntervaloAmostragemAssociados,true);habilitarCampoReducaoIntervalo();" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
							<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" onClick="moveAllOptionsEspecial(document.forms[0].intervalosAmostragemDisponiveis,document.forms[0].idsIntervaloAmostragemAssociados,true);habilitarCampoReducaoIntervalo();" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
							<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop" onClick="moveSelectedOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);desabilitarCampoReducaoIntervalo();" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
							<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" onClick="moveAllOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);desabilitarCampoReducaoIntervalo();" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
						</fieldset>					
						<select id="idsIntervaloAmostragemAssociados" class="campoList" name="idsIntervaloAmostragemAssociados" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);desabilitarCampoReducaoIntervalo();" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
							<c:forEach items="${listaIntervaloAmostragemAssociados}" var="intervaloAssoc">
								<option value="<c:out value="${intervaloAssoc.chavePrimaria}"/>" title="<c:out value="${intervaloAssoc.descricao}"/>"><c:out value="${intervaloAssoc.descricao}"/></option>
							</c:forEach>
					  	</select>
					  	<fieldset class="conteinerBotoesCampoList2">
							<input type="button" class="bottonRightCol botoesLargosIE7" value="Para cima" onclick="moveOptionUp(this.form['idsIntervaloAmostragemAssociados']);" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
							<input type="button" class="bottonRightCol botoesLargosIE7" value="Para baixo" onclick="moveOptionDown(this.form['idsIntervaloAmostragemAssociados']);" <ggas:campoSelecionado campo="intervaloRecuperacaoPCS"/>>
						</fieldset>
						<fieldset id="conteinerNumeroMaximoReducao">
							<ggas:labelContrato forCampo="tamReducaoRecuperacaoPCS">Tamanho da redu��o para recupera��o do PCS:</ggas:labelContrato>
							<input class="campoTexto" type="text" id="tamReducaoRecuperacaoPCS" name="tamReducaoRecuperacaoPCS" maxlength="2" size="1" <ggas:campoSelecionado campo="tamReducaoRecuperacaoPCS"/> value="${contratoVO.tamReducaoRecuperacaoPCS}" onkeypress="return formatarCampoInteiro(event);" disabled="disabled">
						</fieldset>
					</fieldset>
				</fieldset>				
			</div>
		</fieldset>
		
		<fieldset id="dadosConsumoCol2" class="colunaFinal">
		
			<div class=labelCampo>
				<ggas:labelContrato forCampo="horaInicialDia">Hora inicial do dia:</ggas:labelContrato>
				
				<c:choose>
					<c:when test="${contratoVO.valorFixoHoraInicialDia}">
					    <c:forEach begin="0" end="23" var="hora">
							<c:if test="${contratoVO.horaInicialDia == hora}">
								<input class="campoValorFixo" type="text" value="${hora}h" size="5" readonly="readonly" />
								<input type="hidden" name="horaInicialDia" value="${hora}" />
							</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<select id="horaInicialDia" class="campoSelect campoHorizontal" name="horaInicialDia" <ggas:campoSelecionado campo="horaInicialDia"/> >
							<option value="-1">Selecione</option>
						    <c:forEach begin="0" end="23" var="hora">
							  <option value="${hora}" <c:if test="${contratoVO.horaInicialDia == hora}">selected="selected"</c:if>>${hora}</option>
							</c:forEach>
						</select>
						<label class="rotuloHorizontal rotuloInformativo" for="horaInicialDia">h</label>
					</c:otherwise>
				</c:choose>
			</div>
			
			<br class="quebraLinha2" />
			
			<div class=labelCampo>
				<ggas:labelContrato forCampo="regimeConsumo">Regime de consumo:</ggas:labelContrato>
				<c:choose>
					<c:when test="${contratoVO.valorFixoRegimeConsumo}">
						<c:forEach items="${listaRegimeConsumo}" var="regimeConsumo">
							<c:if test="${contratoVO.regimeConsumo == regimeConsumo.chavePrimaria}">
								<input class="campoValorFixo" type="text" value="${regimeConsumo.descricao}" size="20" readonly="readonly" />
								<input type="hidden" name="regimeConsumo" value="${regimeConsumo.chavePrimaria}" />
							</c:if>
						</c:forEach>		
					</c:when>
					<c:otherwise>
						<select name="regimeConsumo" id="regimeConsumo" class="campoSelect campoHorizontal" <ggas:campoSelecionado campo="regimeConsumo"/>>		
							<option value="-1">Selecione</option>				
							<c:forEach items="${listaRegimeConsumo}" var="regimeConsumo">
								<option value="<c:out value="${regimeConsumo.chavePrimaria}"/>" <c:if test="${contratoVO.regimeConsumo == regimeConsumo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${regimeConsumo.descricao}"/>
								</option>		
							</c:forEach>		
						</select>
					</c:otherwise>
				</c:choose>
			</div>
			
			<br class="quebraLinha2" />

			<div class=labelCampo>
				<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="fornecimentoMaximoDiario">Fornecimento m�ximo di�rio:</ggas:labelContrato>
				<c:choose>
					<c:when test="${contratoVO.valorFixoFornecimentoMaximoDiario}">
						<input id="fornecimentoMaximoDiarioValorFixo" class="campoValorFixo" type="text" value="${contratoVO.fornecimentoMaximoDiario}" size="5" readonly="readonly" />
						<input type="hidden" name="fornecimentoMaximoDiario" value="${contratoVO.fornecimentoMaximoDiario}" />
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:if test="${contratoVO.unidadeFornecMaximoDiario eq unidade.chavePrimaria}">
								<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="4" readonly="readonly" />
								<input type="hidden" name="unidadeFornecMaximoDiario" value="${unidade.chavePrimaria}" />
							</c:if>				
						</c:forEach>
					</c:when>
					<c:otherwise>
						<input id="fornecimentoMaximoDiario" class="campoTexto campo2Linhas campoHorizontal" type="text" size="10" maxlength="10" name="fornecimentoMaximoDiario" <ggas:campoSelecionado campo="fornecimentoMaximoDiario"/> value="${contratoVO.fornecimentoMaximoDiario}" onkeypress="return formatarCampoDecimalPositivo(event, this, 7, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"/>
						<select name="unidadeFornecMaximoDiario" id="unidadeFornecMaximoDiario" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="fornecimentoMaximoDiario"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaUnidadeVolume}" var="unidade">
								<option value="<c:out value="${unidade.chavePrimaria}"/>" 
									<c:choose>
										<c:when test="${contratoVO.unidadeFornecMaximoDiario ne ''}">
											<c:if test="${contratoVO.unidadeFornecMaximoDiario eq unidade.chavePrimaria}">selected="selected"</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
										</c:otherwise>
									</c:choose>>
									<c:out value="${unidade.descricaoAbreviada}"/>
								</option>		
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>
			</div>
			
			<br class="quebraLinha2" />
			
			<div class=labelCampo>			
				<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="fornecimentoMinimoDiario">Fornecimento m�nimo di�rio:</ggas:labelContrato>
				
				<c:choose>
					<c:when test="${contratoVO.valorFixoFornecimentoMinimoDiario}">
						<input class="campoValorFixo" type="text" value="${contratoVO.fornecimentoMinimoDiario}" size="5" readonly="readonly" />
						<input type="hidden" name="fornecimentoMinimoDiario" value="${contratoVO.fornecimentoMinimoDiario}" />
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:if test="${contratoVO.unidadeFornecMinimoDiario eq unidade.chavePrimaria}">
								<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="4" readonly="readonly" />
								<input type="hidden" name="unidadeFornecMinimoDiario" value="${unidade.chavePrimaria}" />
							</c:if>				
						</c:forEach>
					</c:when>
					<c:otherwise>
						<input id="fornecimentoMinimoDiario" class="campoTexto campo2Linhas campoHorizontal" type="text" size="10" maxlength="10" name="fornecimentoMinimoDiario" <ggas:campoSelecionado campo="fornecimentoMinimoDiario"/> value="${contratoVO.fornecimentoMinimoDiario}" onkeypress="return formatarCampoDecimalPositivo(event, this, 7, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"/>
						<select name="unidadeFornecMinimoDiario" id="unidadeFornecMinimoDiario" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="fornecimentoMinimoDiario"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaUnidadeVolume}" var="unidade">
								<option value="<c:out value="${unidade.chavePrimaria}"/>" 
									<c:choose>
										<c:when test="${contratoVO.unidadeFornecMinimoDiario ne ''}">
											<c:if test="${contratoVO.unidadeFornecMinimoDiario eq unidade.chavePrimaria}">selected="selected"</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
										</c:otherwise>
									</c:choose>>
									<c:out value="${unidade.descricaoAbreviada}"/>
								</option>		
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>
			</div>
			
			<br class="quebraLinha2" />
			
			<div class=labelCampo>
			
				<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="fornecimentoMinimoMensal">Fornecimento m�nimo mensal:</ggas:labelContrato>
				<c:choose>
					<c:when test="${contratoVO.valorFixoFornecimentoMinimoMensal}">
						<input id="fornecimentoMinimoMensalValorFixo" class="campoValorFixo" type="text" value="${contratoVO.fornecimentoMinimoMensal}" size="5" readonly="readonly" />
						<input type="hidden" name="fornecimentoMinimoMensal" value="${contratoVO.fornecimentoMinimoMensal}" />
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:if test="${contratoVO.unidadeFornecMinimoMensal eq unidade.chavePrimaria}">
								<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="4" readonly="readonly" />
								<input type="hidden" name="unidadeFornecMinimoMensal" value="${unidade.chavePrimaria}" />
							</c:if>				
						</c:forEach>
					</c:when>
					<c:otherwise>
						<input id="fornecimentoMinimoMensal" class="campoTexto campo2Linhas campoHorizontal" type="text" size="10" maxlength="12" name="fornecimentoMinimoMensal" <ggas:campoSelecionado campo="fornecimentoMinimoMensal"/> value="${contratoVO.fornecimentoMinimoMensal}" onkeypress="return formatarCampoDecimalPositivo(event, this, 9, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"/>
						<select name="unidadeFornecMinimoMensal" id="unidadeFornecMinimoMensal" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="fornecimentoMinimoMensal"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaUnidadeVolume}" var="unidade">
								<option value="<c:out value="${unidade.chavePrimaria}"/>" 
									<c:choose>
										<c:when test="${contratoVO.unidadeFornecMinimoMensal ne ''}">
											<c:if test="${contratoVO.unidadeFornecMinimoMensal eq unidade.chavePrimaria}">selected="selected"</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
										</c:otherwise>
									</c:choose>>
									<c:out value="${unidade.descricaoAbreviada}"/>
								</option>		
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>
			</div>
			
			<br class="quebraLinha2" />
			
			<div class=labelCampo>
				<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="fornecimentoMinimoAnual">Fornecimento m�nimo anual:</ggas:labelContrato>
				
				<c:choose>
					<c:when test="${contratoVO.valorFixoFornecimentoMinimoAnual}">
						<input class="campoValorFixo" type="text" value="${contratoVO.fornecimentoMinimoAnual}" size="5" readonly="readonly" />
						<input type="hidden" name="fornecimentoMinimoAnual" value="${contratoVO.fornecimentoMinimoAnual}" />
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:if test="${contratoVO.unidadeFornecMinimoAnual eq unidade.chavePrimaria}">
								<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="4" readonly="readonly" />
								<input type="hidden" name="unidadeFornecMinimoAnual" value="${unidade.chavePrimaria}" />
							</c:if>				
						</c:forEach>
					</c:when>
					<c:otherwise>
						<input id="fornecimentoMinimoAnual" class="campoTexto campo2Linhas campoHorizontal" type="text" size="10" maxlength="15" name="fornecimentoMinimoAnual" <ggas:campoSelecionado campo="fornecimentoMinimoAnual"/> value="${contratoVO.fornecimentoMinimoAnual}" onkeypress="return formatarCampoDecimalPositivo(event, this, 12, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"/>
						<select name="unidadeFornecMinimoAnual" id="unidadeFornecMinimoAnual" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="fornecimentoMinimoAnual"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaUnidadeVolume}" var="unidade">
								<option value="<c:out value="${unidade.chavePrimaria}"/>" 
									<c:choose>
										<c:when test="${contratoVO.unidadeFornecMinimoAnual ne ''}">
											<c:if test="${contratoVO.unidadeFornecMinimoAnual eq unidade.chavePrimaria}">selected="selected"</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
										</c:otherwise>
									</c:choose>>
									<c:out value="${unidade.descricaoAbreviada}"/>
								</option>		
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>
			</div>
			
			<br class="quebraLinha2" />
			
			<div class=labelCampo>
				<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="fatorUnicoCorrecao">Fator �nico de corre��o de Volume:</ggas:labelContrato>
				<select name="fatorUnicoCorrecao" id="fatorUnicoCorrecao" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="fatorUnicoCorrecao"/>>
					<c:if test="${not empty fatorUnicoCorrecao}">
						<option value="<c:out value="${fatorUnicoCorrecao}"/>" selected="selected">
							<c:out value="${fatorUnicoCorrecao}"/>
						</option>		
					</c:if>
				</select><br class="quebraLinha2" />
			</div>
			
			<div class=labelCampo>
				<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="consumoFatFalhaMedicao">Consumo a ser faturado em falha de medi��o:</ggas:labelContrato>
				
				<c:choose>
					<c:when test="${contratoVO.valorFixoConsumoFatFalhaMedicao}">
						<c:forEach items="${listaAcaoAnormalidadeConsumo}" var="acao">
							<c:if test="${contratoVO.consumoFatFalhaMedicao eq acao.chavePrimaria}">
								<input class="campoValorFixo" type="text" value="${acao.descricao}" size="15" readonly="readonly" />
								<input type="hidden" name="consumoFatFalhaMedicao" value="${acao.chavePrimaria}" />
							</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<select name="consumoFatFalhaMedicao" id="consumoFatFalhaMedicao" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="consumoFatFalhaMedicao"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaAcaoAnormalidadeConsumo}" var="acao">
								<option value="<c:out value="${acao.chavePrimaria}"/>" <c:if test="${contratoVO.consumoFatFalhaMedicao eq acao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${acao.descricao}"/>
								</option>		
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>		
			</div>					
		</fieldset>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Contratos.</p>
</fieldset>