<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<script>
	$(function(){
		
		//habilita e desabilita o botao adicionar e exclui da aba faturamento		
		var selectedPontoConsumo = $("#pontoConsumo tr.clickedRow")[0];
		
		if (selectedPontoConsumo == undefined){
			document.getElementById('botaoAdicionarItemFaturamento').disabled = true;
			document.getElementById('botaoExcluirItemFaturamento').disabled = true;
		}
		
		//Seleciona e Desseleciona a coluna de checkbox.
		$("#checkAllItensVencimento").click(function(){
			$("input[type='checkbox'].checkItemVencimento").attr('checked', $('#checkAllItensVencimento').is(':checked'));
		});

		$("label.rotuloDesabilitado span").each(function(){
			$(this).addClass("rotuloDesabilitado");
		});
		
		//Verifica se existem campos obrigat�rios, exibindo ou escondendo a legenda.
		var campoObrigatorioFaturamento = $("#faturamento #dadosFaturamentoCol1 .conteinerDados .exibirCep > .campoObrigatorio, #faturamento #dadosFaturamentoCol1 .conteinerDados > .campoObrigatorio, #faturamento #dadosItensFaturamento .campoObrigatorio").size();
		var fluxoDetalhamento = "<c:out value='${fluxoDetalhamento}'/>"

		if (campoObrigatorioFaturamento > 0){
			//Situa��o em que os campos est�o selecionados no modelo
			$("#faturamento p.legenda").show();
			if ($("label[for='itemFatura'],label[for='tarifaConsumo'],label[for='diaVencimentoItemFatura'],label[for='dataReferenciaCambial'],label[for='diaCotacao']").hasClass("campoObrigatorio")){
				$(this).removeClass("campoObrigatorio2");
				$("#faturamento .conteinerDados p.legenda").hide();
				$("#faturamento span.campoObrigatorioSimbolo2").remove();
			} else {
				$("label[for='itemFatura'],label[for='tarifaConsumo'],label[for='diaVencimentoItemFatura'],label[for='dataReferenciaCambial'],label[for='diaCotacao']").prepend("<span class='campoObrigatorioSimbolo2'>* </span>");
			}
		} else {
			if(fluxoDetalhamento){
				$("#faturamento p.legenda, #faturamento .conteinerDados p.legenda").hide();
			} else {
				//Situa��o em que os campos est�o selecionados no modelo, mas n�o s�o obrigat�rios
				$("#faturamento p.legenda").hide();
				$("#faturamento .conteinerDados p.legenda").show();
				
				//Verifica se os r�tulos est�o habilitados e a classe modifica a apar�ncia dos aster�scos
				if($("label[for='itemFatura'],label[for='tarifaConsumo'],label[for='diaVencimentoItemFatura'],label[for='dataReferenciaCambial'],label[for='diaCotacao']").hasClass("rotuloDesabilitado")){
					$("label[for='itemFatura'],label[for='tarifaConsumo'],label[for='diaVencimentoItemFatura'],label[for='dataReferenciaCambial'],label[for='diaCotacao']").prepend("<span class='campoObrigatorioSimbolo2 rotuloDesabilitado'>* </span>");
				} else {
					$("label[for='itemFatura'],label[for='tarifaConsumo'],label[for='diaVencimentoItemFatura'],label[for='dataReferenciaCambial'],label[for='diaCotacao']").prepend("<span class='campoObrigatorioSimbolo2'>* </span>");
				}		
			}
		};

		configurarPercentualQdc();		
		manipularDivDiasPossiveisVencimento(null);
		habilitarCamposDataReferenciaDiaCotacao();
		validarPermissaoCampoFaseVencimento();
		
		var numeroCep = $("#endFisEntFatCEP").val();
		
		$("#endFisEntFatCEP").change(function(){
			carregarEnderecoCepAbaFaturamento()
		});

	});

	$( document ).ready(function(){
		carregarEnderecoCepAbaFaturamento();
	});
	
	function carregarEnderecoCepAbaFaturamento(){
		var cep = $("#endFisEntFatCEP").val().split("_").join("");
		if(cep.length == 9){
			AjaxService.consultarEndereco(cep, 
				function(endereco) {

				if(endereco != null && chaveCep != null && chaveCep != undefined){
					for(chaveCep in endereco){
						var enderecoArray = endereco[chaveCep];
						$("#campoEndereco").attr('value', enderecoArray[1] + ", " + enderecoArray[2] + ", " + enderecoArray[3] + " - " + enderecoArray[4]);
						$("#endFisEntFatCEP").attr('value', cep);
						break;
					}
				}else{
					$("#campoEndereco").attr('value','Cep n�o encontrado no sistema.');
				}
			});
		}
	}
	
	function manipularDivDiasPossiveisVencimento(itemFaturaAtual){
		var itemFatura = "<c:out value='${contratoVO.itemFatura}'/>";
		
		if(itemFaturaAtual != null){
			itemFatura = itemFaturaAtual;
		}		
		
		if(itemFatura ==  23){		
			$("#divDiasPossiveisVencimento").show();
		}else{
			$("#divDiasPossiveisVencimento").hide();
		}
	}
	

	function configurarPercentualQdc(){

		var habilitarCampo = true;
		var valorSelecionado = document.contratoForm.itemFatura.value;
		var itemFaturaMargemDistribuicao = <c:out value="${itemFaturaMargemDistribuicao}"/>;
		
		if(valorSelecionado == itemFaturaMargemDistribuicao){
			document.getElementById('spanpercminimoQDC').innerHTML='*';
			habilitarCampo = false;	
		}else{
			document.getElementById('spanpercminimoQDC').innerHTML='';
			document.getElementById('percminimoQDC').value='';
		}
		
		document.contratoForm.percminimoQDC.disabled = habilitarCampo;		
	}
	
	function selecionarOpcoesQDCContrato() {
		var selectQDC = document.getElementById("QDCContrato");
		if (selectQDC != undefined) {
			selectAllOptions(selectQDC);
		}	
	}

	function obterTarifas(){		
		var formulario = document.forms[0];
		var chavePontoConsumo = null;
		var chavesPrimariasPontoConsumo = formulario.chavesPrimariasPontoConsumo;
		var chaveModeloContrato = null;
		var chaveItemFatura = formulario.itemFatura[formulario.itemFatura.selectedIndex].value; 
		var tarifaConsumo = formulario.tarifaConsumo;

		if (formulario != undefined && formulario.idPontoConsumo != undefined) {
			chavePontoConsumo = formulario.idPontoConsumo.value;
		}
		
		if (formulario != undefined && formulario.idModeloContrato != undefined) {
			chaveModeloContrato = formulario.idModeloContrato.value;
		}
		
		if(chavePontoConsumo != null && chavePontoConsumo != ''){
			tarifaConsumo.length = 0;
			var novaOpcao = new Option("Selecione","-1");
			tarifaConsumo.options[tarifaConsumo.length] = novaOpcao;
						
			AjaxService.obterTarifas(chavePontoConsumo, chaveItemFatura, chaveModeloContrato, 
				function(tarifas){
					for(key in tarifas){
						novaOpcao = new Option(tarifas[key], key);
						tarifaConsumo.options[tarifaConsumo.length] = novaOpcao;
					}				
				}
			);			
		}else{
			alert('Selecione o ponto de consumo');
		}
	}

	function validarPermissaoCampoFaseVencimento() {	
	
		var opcaoVencimentoItemFatura = document.getElementById('opcaoVencimentoItemFatura');
		
		if(opcaoVencimentoItemFatura != null && opcaoVencimentoItemFatura.value != '-1'){
			AjaxService.permiteFaseReferencialVencimentoPorOpcaoVencimento( opcaoVencimentoItemFatura.value, {
	           	callback: function(permite) {            		      		         		
                	if(permite){                		
                		document.getElementById('faseVencimentoItemFatura').disabled = false;
                	} else {
                		document.getElementById('faseVencimentoItemFatura').value = '-1';
                		document.getElementById('faseVencimentoItemFatura').disabled = true;                		
                	}
                }	        	
	        	, async:false}
            );
		} else {
			if (document.getElementById('faseVencimentoItemFatura') != null){
				document.getElementById('faseVencimentoItemFatura').value = '-1';
            	document.getElementById('faseVencimentoItemFatura').disabled = true;
			}
		}
		
		document.getElementById('indicadorVencDiaNaoUtilNao').disabled = false;
		document.getElementById('indicadorVencDiaNaoUtilSim').disabled = false;
		if(opcaoVencimentoItemFatura.value == 30){
			document.getElementById('indicadorVencDiaNaoUtilNao').disabled = false;
			document.getElementById('indicadorVencDiaNaoUtilSim').disabled = true;
		}
		if(opcaoVencimentoItemFatura.value == 28 ^ opcaoVencimentoItemFatura.value == 496){
			$('#dataVencimentoFixo1').show();
			$('#dataVencimentoFixo2').show();
			$('#dataVencimentoFixo2').hide();
		}
		if(opcaoVencimentoItemFatura.value != 28 && opcaoVencimentoItemFatura.value != 496){
			$('#dataVencimentoFixo2').show();
			$('#dataVencimentoFixo1').hide();
		}
		<c:if test="${empty listaItemFaturamentoContratoVO}">
			var checkAllIntes = document.getElementById('checkAllItensVencimento');
			if(checkAllIntes != undefined && checkAllIntes != ""){
				document.getElementById('checkAllItensVencimento').disabled = true;
			}
		</c:if>		
	}
		
	function limparAbaFaturamento(){
		
		document.getElementById('tarifaConsumo').value = '-1';
		document.getElementById('fatPeriodicidade').value = '-1';
		document.getElementById('depositoIdentificadoSim').checked = false;
		document.getElementById('depositoIdentificadoNao').checked = false;
		document.getElementById('endFisEntFatCEP').value = '';
		document.getElementById('endFisEntFatNumero').value = '';
		document.getElementById('endFisEntFatComplemento').value = '';
		document.getElementById('endFisEntFatEmail').value = '';
		document.getElementById('itemFatura').value = '-1';
		document.getElementById('diaVencimentoItemFatura').value = '-1';
		document.getElementById('opcaoVencimentoItemFatura').value = '-1';
		document.getElementById('faseVencimentoItemFatura').value = '-1';
		document.getElementById('indicadorVencDiaNaoUtilSim').checked = false;
		document.getElementById('indicadorVencDiaNaoUtilNao').checked = false;
		document.getElementById("idItemFaturamento").value = '';
		document.getElementById('dataReferenciaCambial').value = '-1';
		document.getElementById('diaCotacao').value = '-1';
		habilitarBotaoAdicionarItemFaturamento(document.getElementById("itemFatura"));
		
		<c:choose>
		<c:when test="${listaContratoCompra ne null}">
			<c:forEach items="${listaContratoCompra}" var="contratoCompra">
				<c:if test="${contratoCompra.indicadorPadrao eq true}">
					document.getElementById('contratoCompra').value = ${contratoCompra.chavePrimaria};
				</c:if>
			</c:forEach>
		</c:when>
		<c:otherwise>
				document.getElementById('contratoCompra').value = -1;
			</c:otherwise>
		</c:choose>
	
	}

	function alterarItemFaturamentoCadastrado() {		
		adicionarNovoItemFaturamento();
	}

	function habilitarBotaoAdicionarItemFaturamento(campo){
		var selectedPontoConsumo = $("#pontoConsumo tr.clickedRow")[0];
		
		if(campo.value != '-1' && selectedPontoConsumo != undefined){
			document.getElementById('botaoAdicionarItemFaturamento').disabled = false;
		} else {
			document.getElementById('botaoAdicionarItemFaturamento').disabled = true;
		}	
	}

	function habilitarDataReferenciaCambialDiaCotacao(elem) {
		
		var codTarifa = elem.value
		var tarifaConsumo = document.getElementById('tarifaConsumo');

		var selDataReferenciaCambial = document.getElementById('dataReferenciaCambial');
		var selDiaCotacao = document.getElementById('diaCotacao');

		if(tarifaConsumo.value != -1){
			AjaxService.verificaUnidadeMonetariaTarifaVigencia(codTarifa, {
        	    callback: function(retorno) {            		      		         		
			       if (retorno) {
					 	selDataReferenciaCambial.disabled = false;
				   		selDiaCotacao.disabled = false;
				   		$("label[for=dataReferenciaCambial], label[for=dataReferenciaCambial] span, label[for=diaCotacao], label[for=diaCotacao] span").removeClass("rotuloDesabilitado");
				   		$("#dataReferenciaCambial, #diaCotacao").removeClass("campoDesabilitado");
					}else {
						selDataReferenciaCambial.disabled = true;
						selDataReferenciaCambial.value = -1;
						selDiaCotacao.disabled = true;
						selDiaCotacao.value = -1;
						$("label[for=dataReferenciaCambial], label[for=dataReferenciaCambial] span, label[for=diaCotacao], label[for=diaCotacao] span").addClass("rotuloDesabilitado");
						$("#dataReferenciaCambial,#diaCotacao").addClass("campoDesabilitado");
					}
            }, 
            async:false}
            );
		} else {
			selDataReferenciaCambial.disabled = true;
			selDataReferenciaCambial.value = -1;
			selDiaCotacao.disabled = true;
			selDiaCotacao.value = -1;
			$("label[for=dataReferenciaCambial], label[for=dataReferenciaCambial] span, label[for=diaCotacao], label[for=diaCotacao] span").addClass("rotuloDesabilitado");
			$("#dataReferenciaCambial,#diaCotacao").addClass("campoDesabilitado");
		}
	}

	function habilitarCamposDataReferenciaDiaCotacao(){
		var tarifaConsumo = document.getElementById('tarifaConsumo');
		if(tarifaConsumo != undefined){
			habilitarDataReferenciaCambialDiaCotacao(tarifaConsumo);
		}	
	}
	
// 	addLoadEvent(validarPermissaoCampoFaseVencimento);
	addLoadEvent(habilitarCamposDataReferenciaDiaCotacao);

	function mostrarAsteriscoCep(valor,nomeCampo) {
		var underline = "_";
		if (valor != undefined) {
			for (var i = 0; i < nomeCampo.length; i++) {
				var descricaoLabel = $("label[for="+nomeCampo[i]+"]").html();
				// Se o campo pai tem algum valor.
				if (valor != "" && valor.indexOf(underline) < 0) {
					// Se o campo pai mudar de valor n�o adiciona outro asterisco, pois j� existe.
					if ($("#span"+nomeCampo[i]).length == 0) {
						var span = "<span id='span"+ nomeCampo[i] +"' class='campoObrigatorioSimbolo2'>* </span>";
						$("label[for="+nomeCampo[i]+"]").html(span + descricaoLabel);
						$("label[for="+nomeCampo[i]+"]").addClass("campoObrigatorio");
					}

				// Se o campo pai n�o tem valor.
				} else {
					var classe = $("#span"+nomeCampo[i]).attr("class");
					// S� remove caso o label tenha asterisco preto.
					if (classe == "campoObrigatorioSimbolo2") {
						$("#span"+nomeCampo[i]).remove();
						$("label[for="+nomeCampo[i]+"]").removeClass("campoObrigatorio");
					}
				}
			}
		}
	}	
	
	function ordenarListaDiasVencimento(){
		var listaDiasDisponiveis = document.getElementById('listaDiasDisponiveis');	
	}

	function carregarLista(){
		var opcaoVencimentoItemFatura = document.getElementById("opcaoVencimentoItemFatura");
		var selectDiaVencimento = document.getElementById("diaVencimentoItemFatura");
		var qtdDias = null;
		var selectTexto = new Option("Selecione","-1");
		selectDiaVencimento.length=0;
		
		AjaxService.carregarListaDatasDisponiveisVencimento(opcaoVencimentoItemFatura.value,{
			
			callback: function(retorno) {
				qtdDias = retorno.length; 
				if(retorno){
					var i;
					selectDiaVencimento.options[selectDiaVencimento.length] = selectTexto; 
					for (i=0; i< qtdDias; i++){
                    	var novoDia = new Option(retorno[i]);
                    	selectDiaVencimento.options[selectDiaVencimento.length] = novoDia;
                    }
				}	
			}, 
		     async:false}
		);
	}

</script>

<fieldset id="faturamento">
	<c:choose>
		<c:when test="${contratoVO.fluxoDetalhamento}">
			<a class="linkHelp" href="<help:help>/detalhamentocontratoabafaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:when>
		<c:otherwise>
			<a class="linkHelp" href="<help:help>/abafaturamentoinclusoalteraoadiodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:otherwise>
	</c:choose>
	<div class="agrupamento">
		<fieldset id="dadosFaturamentoCol1" class="colunaEsq">
			<legend>Entrega da Fatura:</legend>
			<fieldset class="conteinerDados">
				<fieldset class="exibirCep">
					<c:set var="cepObrigatorio" value="false"/>
					<c:set var="cepDesabilitado" value="false"/>
					<c:if test="${contratoVO.obgEndFisEntFatCEP eq true}">
						<c:set var="cepObrigatorio" value="true"/>
					</c:if>
					<c:if test="${contratoVO.selEndFisEntFatCEP ne true}">
						<c:set var="cepDesabilitado" value="true"/>	
					</c:if>
					<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
						<jsp:param name="cepObrigatorio" value="${cepObrigatorio}"/>
						<jsp:param name="cepDesabilitado" value="${cepDesabilitado}"/>						
						<jsp:param name="numeroCep" value="${contratoVO.endFisEntFatCEP}"/>
						<jsp:param name="chaveCep" value="${contratoVO.chaveCep}"/>
						<jsp:param name="idCampoCep" value="endFisEntFatCEP"/>
						<jsp:param name="idCampoCidade" value="cidadeEndFisEntFat"/>
						<jsp:param name="idCampoUf" value="ufEndFisEntFat"/>
						<jsp:param name="idCampoLogradouro" value="logradouroEndFisEntFat"/>
						<jsp:param name="mostrarAsterisco" value="mostrarAsteriscoCep"/>
					</jsp:include>
				</fieldset>
				
				
				<c:if test="${contratoVO.obgEndFisEntFatCEP eq true}">
					<div class="labelCampo">
						<label class="rotulo" for="campoEndereco">Endere�o:</label>
						<input class="campoTexto" type="text" id="campoEndereco" name="campoEndereco" readonly='readonly' size="70" value="">
						<br class="quebraLinha" />
					</div>
				</c:if>
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="endFisEntFatNumero">N�mero:</ggas:labelContrato>
					<input class="campoTexto" type="text" id="endFisEntFatNumero" name="endFisEntFatNumero" onkeypress="return formatarCampoNumeroEndereco(event, this);" maxlength="5" size="5" <ggas:campoSelecionado campo="endFisEntFatNumero"/> value="${contratoVO.endFisEntFatNumero}"><br class="quebraLinha" />
				</div>
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="endFisEntFatComplemento">Complemento:</ggas:labelContrato>
					<input class="campoTexto" type="text" id="endFisEntFatComplemento" name="endFisEntFatComplemento" maxlength="255" size="30" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" <ggas:campoSelecionado campo="endFisEntFatComplemento"/> value="${contratoVO.endFisEntFatComplemento}"><br class="quebraLinha" />
				</div>
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="endFisEntFatEmail">Email:</ggas:labelContrato>
					<input class="campoTexto" type="text" id="endFisEntFatEmail" name="endFisEntFatEmail" maxlength="1000" size="45" <ggas:campoSelecionado campo="endFisEntFatEmail"/> value="${contratoVO.endFisEntFatEmail}"/>
				</div>
				
			</fieldset>
		</fieldset>	
	</div>
	
	<div class="agrupamento">	
		<fieldset id="dadosFaturamentoCol2" class="colunaEsq">
			<legend>Dados de Compra:</legend>
			<fieldset class="conteinerDados">
				<div class="labelCampo">
					<ggas:labelContrato forCampo="contratoCompra">Contrato de compra:</ggas:labelContrato>
					<c:choose>
						<c:when test="${contratoVO.valorFixoContratoCompra}">
							<c:forEach items="${listaContratoCompra}" var="contratoCompra">
								<c:if test="${contratoVO.contratoCompra eq contratoCompra.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="${contratoCompra.descricao}" size="30" readonly="readonly" />
									<input type="hidden" name="contratoCompra" value="${contratoCompra.chavePrimaria}" />
								</c:if>
							</c:forEach>					
						</c:when>
						<c:otherwise>
							<select class="campoSelect" id="contratoCompra" name="contratoCompra" <ggas:campoSelecionado campo="contratoCompra"/>>
								<option value="-1"/>Selecione</option>
								<c:forEach items="${listaContratoCompra}" var="contratoCompra">
									<option value="<c:out value="${contratoCompra.chavePrimaria}"/>"<c:if test="${contratoVO.contratoCompra eq contratoCompra.chavePrimaria}"> selected="selected"</c:if>>
										<c:out value="${contratoCompra.descricao}"/>
									</option>
								</c:forEach>					
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				
				<br />
			</fieldset>
		</fieldset>
	</div>	
	
	
	<fieldset id="dadosFaturamentoCol2" class="colunaEsq">
		<br />
		<div class="labelCampo">
			<ggas:labelContrato forCampo="fatPeriodicidade">Periodicidade:</ggas:labelContrato>
			<c:choose>
				<c:when test="${contratoVO.valorFixoFatPeriodicidade}">
					<c:forEach items="${listaPeriodicidade}" var="periodicidade">
						<c:if test="${contratoVO.fatPeriodicidade eq periodicidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${periodicidade.descricao}" size="15" readonly="readonly" />
							<input type="hidden" name="fatPeriodicidade" value="${periodicidade.chavePrimaria}" />
						</c:if>
					</c:forEach>						
				</c:when>
				<c:otherwise>
					<select class="campoSelect" id="fatPeriodicidade" name="fatPeriodicidade" <ggas:campoSelecionado campo="fatPeriodicidade"/>>
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPeriodicidade}" var="periodicidade">
							<option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${contratoVO.fatPeriodicidade eq periodicidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${periodicidade.descricao}"/>
							</option>		
						</c:forEach>						
					</select><br class="quebraLinha" />						
				</c:otherwise>
			</c:choose>
		</div>
	</fieldset>
	<br />
	<fieldset style="margin-right:10%" class="colunaEsq" id="dadosNotaFiscalEletronica">	
		<input name="valorEmissiaoRadioNfe" type="hidden" id="valorEmissiaoRadioNfe" value="${contratoVO.indicadorNFE }"/>
		
		<div class="labelCampo">
			<ggas:labelContrato forCampo="indicadorNFE">Emiss�o de nota fiscal<br/> eletr�nica:</ggas:labelContrato>
			<c:choose>
				<c:when test="${contratoVO.valorFixoIndicadorNFE}">
					<c:choose>
						<c:when test="${contratoVO.indicadorNFE}">
							<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
						</c:when>
						<c:otherwise>
							<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
						</c:otherwise>
					</c:choose>
					<input type="hidden" name="indicadorNFE" value="${contratoVO.indicadorNFE}" />
				</c:when>
				<c:otherwise>
					<input class="campoRadio" type="radio" name="indicadorNFE" id="indicadorNFESim" onClick = "habilitarRadioFatura()" value="true" <ggas:campoSelecionado campo="indicadorNFE"/> <c:if test="${contratoVO.indicadorNFE eq 'true'}">checked="checked"</c:if>>
					<label class="rotuloRadio <c:if test="${contratoVO.selIndicadorVencDiaNaoUtil ne true}"> rotuloDesabilitado</c:if>" for="indicadorNFESim">Sim</label>
					<input class="campoRadio" type="radio" name="indicadorNFE" id="indicadorNFENao" onClick = "desabilitarRadioFatura()" value="false"  <ggas:campoSelecionado campo="indicadorNFE"/> <c:if test="${contratoVO.indicadorNFE eq 'false'}">checked="checked"</c:if>>
					<label class="rotuloRadio <c:if test="${contratoVO.selIndicadorVencDiaNaoUtil ne true}"> rotuloDesabilitado</c:if>" for="indicadorNFENao">N�o</label>			
				</c:otherwise>
			</c:choose>			
		</div>		
		<br />
		<div class="labelCampo">
			<ggas:labelContrato forCampo="indicadorNFE">Emiss�o de fatura da<br/> nota fiscal eletr�nica:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoEmitirFaturaComNfe}">
					<c:choose>
						<c:when test="${contratoVO.emitirFaturaComNfe}">
							<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
						</c:when>
						<c:otherwise>
							<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
						</c:otherwise>
					</c:choose>
					<input type="hidden" name="emitirFaturaComNfe" value="${contratoVO.emitirFaturaComNfe}" />
				</c:when>
				<c:otherwise>
					<input class="campoRadio" type="radio" name="emitirFaturaComNfe" id="emitirFaturaComNfeSim" value="true" <ggas:campoSelecionado campo="indicadorNFE"/> <c:if test="${contratoVO.emitirFaturaComNfe eq 'true'}">checked="checked"</c:if>>
					<label id="labelFaturaSim" class="rotuloRadio <c:if test="${contratoVO.selIndicadorVencDiaNaoUtil ne true}"> rotuloDesabilitado</c:if>" for="emitirFaturaComNfeSim">Sim</label>
					<input class="campoRadio" type="radio" name="emitirFaturaComNfe" id="emitirFaturaComNfeNao" value="false" <ggas:campoSelecionado campo="indicadorNFE"/> <c:if test="${contratoVO.emitirFaturaComNfe eq 'false'}">checked="checked"</c:if>>
					<label id="labelFaturaNao" class="rotuloRadio <c:if test="${contratoVO.selIndicadorVencDiaNaoUtil ne true}"> rotuloDesabilitado</c:if>" for="emitirFaturaComNfeNao">N�o</label>			
				</c:otherwise>
			</c:choose>		
		</div>
					
	</fieldset>	
	<fieldset id="dadosItensFaturamento">
		<legend>Itens de Faturamento:</legend>
		<fieldset class="conteinerDados">
			<fieldset class="coluna2">
			
				<div class="labelCampo">
					<ggas:labelContrato styleClass="campoObrigatorio2" forCampo="itemFatura">Item da fatura:</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoItemFatura}">
							<c:forEach items="${listaItemFatura}" var="itemFatura">
								<c:if test="${contratoVO.itemFatura eq itemFatura.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="${itemFatura.descricao}" size="30" readonly="readonly" />
									<input type="hidden" name="itemFatura" value="${itemFatura.chavePrimaria}" />
								</c:if>
							</c:forEach>						
						</c:when>
						<c:otherwise>
							<select name="itemFatura" id="itemFatura" class="campoSelect" <ggas:campoSelecionado campo="itemFatura"/> onchange="habilitarBotaoAdicionarItemFaturamento(this);obterTarifas(this);configurarPercentualQdc();manipularDivDiasPossiveisVencimento(this.value);">
								<c:forEach items="${listaItemFatura}" var="itemFatura">
									<option value="<c:out value="${itemFatura.chavePrimaria}"/>" <c:if test="${contratoVO.itemFatura eq itemFatura.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${itemFatura.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				
				<br class="quebraLinha" />
				
				<div class="labelCampo">
					<ggas:labelContrato styleClass="campoObrigatorio2" forCampo="tarifaConsumo">Tarifa:</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoTarifaConsumo}">
							<c:forEach items="${listaTarifas}" var="tarifa">
								<c:if test="${contratoVO.tarifaConsumo eq tarifa.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="${tarifa.descricao}" size="15" readonly="readonly" />
									<input type="hidden" name="tarifaConsumo" value="${tarifa.chavePrimaria}" />
								</c:if>
							</c:forEach>						
						</c:when>
						<c:otherwise>
							<select class="campoSelect" id="tarifaConsumo" name="tarifaConsumo" <ggas:campoSelecionado campo="tarifaConsumo"/> onchange="habilitarDataReferenciaCambialDiaCotacao(this);" >
								<option value="-1">Selecione</option>
								<c:forEach items="${listaTarifas}" var="tarifa">
									<option value="<c:out value="${tarifa.chavePrimaria}"/>" <c:if test="${contratoVO.tarifaConsumo eq tarifa.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${tarifa.descricao}"/>
									</option>		
								</c:forEach>						
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				
				<br class="quebraLinha" />
				
				<div class="labelCampo">
					<ggas:labelContrato styleId="rotuloPercminimoQDC" styleClass="rotuloVertical rotulo2Linhas" forId="percminimoQDC" forCampo="percminimoQDC">Percentual Minimo do QDC:</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoPercminimoQDC}">
							<input class="campoValorFixo" type="text" value="${contratoVO.percminimoQDC}" size="4" readonly="readonly" />
							<input type="hidden" name="percminimoQDC" value="${contratoVO.percminimoQDC}" />
						</c:when>
						<c:otherwise>
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percminimoQDC" name="percminimoQDC" value="${contratoVO.percminimoQDC}" size="3" maxlength="6" <ggas:campoSelecionado campo="percminimoQDC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);">
							<label class="rotuloHorizontal rotuloInformativo">%</label>
						</c:otherwise>
					</c:choose>
				</div>
							
				<br class="quebraLinha" />	
				
				<div class="labelCampo">
					<ggas:labelContrato styleClass="rotulo2Linhas campoObrigatorio2" forCampo="dataReferenciaCambial">Data de<br />refer�ncia cambial:</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoDataReferenciaCambial}">
							<c:forEach items="${listaDataReferenciaCambial}" var="dataReferenciaCambial">
								<c:if test="${contratoVO.dataReferenciaCambial eq dataReferenciaCambial.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="${dataReferenciaCambial.descricao}" size="30" readonly="readonly" />
									<input type="hidden" name="dataReferenciaCambial" value="${dataReferenciaCambial.chavePrimaria}" />
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<select class="campoSelect campo2Linhas" id="dataReferenciaCambial" name="dataReferenciaCambial" <ggas:campoSelecionado campo="dataReferenciaCambial"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaDataReferenciaCambial}" var="dataReferenciaCambial">
									<option value="<c:out value="${dataReferenciaCambial.chavePrimaria}"/>" <c:if test="${contratoVO.dataReferenciaCambial eq dataReferenciaCambial.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${dataReferenciaCambial.descricao}"/>
									</option>		
								</c:forEach>						
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				
				<br class="quebraLinha" />
				
				<div class="labelCampo">
					<ggas:labelContrato styleClass="campoObrigatorio2" forCampo="diaCotacao">Dia da cota��o:</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoDiaCotacao}">
							<c:forEach items="${listaDiaCotacao}" var="diaCotacao">
								<c:if test="${contratoVO.diaCotacao eq diaCotacao.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="${diaCotacao.descricao}" size="30" readonly="readonly" />
									<input type="hidden" name="diaCotacao" value="${diaCotacao.chavePrimaria}" />
								</c:if>
							</c:forEach>						
						</c:when>
						<c:otherwise>
							<select class="campoSelect" id="diaCotacao" name="diaCotacao" <ggas:campoSelecionado campo="diaCotacao"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaDiaCotacao}" var="diaCotacao">
									<option value="<c:out value="${diaCotacao.chavePrimaria}"/>" <c:if test="${contratoVO.diaCotacao eq diaCotacao.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${diaCotacao.descricao}"/>
									</option>		
								</c:forEach>						
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				
				<br class="quebraLinha" />
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="depositoIdentificado">Dep�sito identificado?</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoDepositoIdentificado}">
							<c:choose>
								<c:when test="${contratoVO.depositoIdentificado}">
									<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
								</c:when>
								<c:otherwise>
									<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="emitirFaturaComNfe" value="${contratoVO.depositoIdentificado}" />
						</c:when>
						<c:otherwise>
							<input class="campoRadio" type="radio" name="depositoIdentificado" id="depositoIdentificadoSim" value="true" <ggas:campoSelecionado campo="depositoIdentificado"/> <c:if test="${contratoVO.depositoIdentificado eq 'true'}">checked="checked"</c:if>>
							<label class="rotuloRadio<c:if test="${contratoVO.selDepositoIdentificado ne true}"> rotuloDesabilitado</c:if>" for="depositoIdentificadoSim">Sim</label>
							<input class="campoRadio" type="radio" name="depositoIdentificado" id="depositoIdentificadoNao" value="false" <ggas:campoSelecionado campo="depositoIdentificado"/> <c:if test="${contratoVO.depositoIdentificado eq 'false'}">checked="checked"</c:if>>
							<label class="rotuloRadio<c:if test="${contratoVO.selDepositoIdentificado ne true}"> rotuloDesabilitado</c:if>" for="depositoIdentificadoNao">N�o</label>
						</c:otherwise>
					</c:choose>
				</div>
				
			</fieldset>
		
			<div class="conteinerDados2">
				
				<div class="">		
					<ggas:labelContrato styleId="rotuloItemFatura" styleClass="rotulo2Linhas rotuloHorizontal campoObrigatorio2" forCampo="diaVencimentoItemFatura">Dia do vencimento:</ggas:labelContrato>
					<c:choose>
						<c:when test="${contratoVO.valorFixoOpcaoVencimentoItemFatura}">
							<c:forEach items="${listaOpcaoFaseRefVencimento}" var="opcaoVencimentoItemFatura">
								<c:if test="${contratoVO.opcaoVencimentoItemFatura eq opcaoVencimentoItemFatura.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="${opcaoVencimentoItemFatura.descricao}" size="15" readonly="readonly" />
									<input type="hidden" id="opcaoVencimentoItemFatura" name="opcaoVencimentoItemFatura" value="${opcaoVencimentoItemFatura.chavePrimaria}" />
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<select name="opcaoVencimentoItemFatura" id="opcaoVencimentoItemFatura" class="campoSelect campo2Linhas campoHorizontal" onchange="validarPermissaoCampoFaseVencimento(); carregarLista();" <ggas:campoSelecionado campo="opcaoVencimentoItemFatura"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaOpcaoFaseRefVencimento}" var="opcaoVencimentoItemFatura">
									<option value="<c:out value="${opcaoVencimentoItemFatura.chavePrimaria}"/>" <c:if test="${contratoVO.opcaoVencimentoItemFatura eq opcaoVencimentoItemFatura.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${opcaoVencimentoItemFatura.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>							
					<div>
					<c:choose>
						<c:when test="${contratoVO.valorFixoDiaVencimentoItemFatura}">
							<c:forEach begin="1" end="31" var="dia"> 
								<c:if test="${empty contratoVO.diaVencimentoItemFaturaValorFixo or !fn:contains(contratoVO.diaVencimentoItemFaturaValorFixo, '0')}">
									<input id="diaVencimentoItemFaturaValorFixo" class="campoValorFixo" type="text" value="${dia}" size="4" readonly="readonly" />
							   		<input type="hidden" name="diaVencimentoItemFaturaValorFixo" value="${dia}" />
								</c:if>
							</c:forEach>
						</c:when>
					</c:choose>
					</div>
					<div id="dataVencimentoFixo1s">
						<select id="diaVencimentoItemFatura" class="campoSelect campo2Linhas campoHorizontal" name="diaVencimentoItemFatura" <ggas:campoSelecionado campo="diaVencimentoItemFatura"/> >
							<option value="-1">Selecione</option>
							<c:forEach begin="1" end="31" var="dia">
								<option value="${dia}" <c:if test="${contratoVO.diaVencimentoItemFatura == dia}">selected="selected"</c:if>>${dia}</option>
							</c:forEach>
						</select>
					</div>	
		
					<ggas:labelContrato styleId="rotuloFaseVencimentoItemFatura" styleClass="rotuloHorizontal" forCampo="faseVencimentoItemFatura">ap�s:</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoFaseVencimentoItemFatura}">
							<c:forEach items="${listaFaseReferencialVencimento}" var="faseVencimentoItemFatura">
									<c:if test="${contratoVO.faseVencimentoItemFatura eq faseVencimentoItemFatura.chavePrimaria}">
										<input class="campoValorFixo" type="text" value="${faseVencimentoItemFatura.descricao}" size="15" />
										<input type="hidden" name="faseVencimentoItemFatura" value="${faseVencimentoItemFatura.chavePrimaria}" />
									</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<select name="faseVencimentoItemFatura" id="faseVencimentoItemFatura" class="campoSelect campoHorizontal" <ggas:campoSelecionado campo="faseVencimentoItemFatura"/>>
								<option value="-1" selected="selected">Selecione</option>
								<c:forEach items="${listaFaseReferencialVencimento}" var="faseVencimentoItemFatura">
									<option value="<c:out value="${faseVencimentoItemFatura.chavePrimaria}"/>"<c:if test="${contratoVO.faseVencimentoItemFatura eq faseVencimentoItemFatura.chavePrimaria}"> selected="selected"</c:if>>
										<c:out value="${faseVencimentoItemFatura.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				
				<br />
							
				<div class="labelCampo">		
					<ggas:labelContrato forCampo="indicadorVencDiaNaoUtil">Vencimento em dia n�o �til?</ggas:labelContrato>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoIndicadorVencDiaNaoUtil}">
							<c:choose>
								<c:when test="${contratoVO.indicadorVencDiaNaoUtil}">
									<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
								</c:when>
								<c:otherwise>
									<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="indicadorVencDiaNaoUtil" value="${contratoVO.indicadorVencDiaNaoUtil}" />
						</c:when>
						<c:otherwise>
							<input class="campoRadio" type="radio" name="indicadorVencDiaNaoUtil" id="indicadorVencDiaNaoUtilSim" value="true" <ggas:campoSelecionado campo="indicadorVencDiaNaoUtil"/> <c:if test="${contratoVO.indicadorVencDiaNaoUtil eq 'true'}">checked="checked"</c:if>>
							<label class="rotuloRadio <c:if test="${contratoVO.selIndicadorVencDiaNaoUtil ne true}"> rotuloDesabilitado</c:if>" for="indicadorVencDiaNaoUtilSim">Sim</label>
							<input class="campoRadio" type="radio" name="indicadorVencDiaNaoUtil" id="indicadorVencDiaNaoUtilNao" value="false" <ggas:campoSelecionado campo="indicadorVencDiaNaoUtil"/> <c:if test="${contratoVO.indicadorVencDiaNaoUtil eq 'false'}">checked="checked"</c:if>>
							<label class="rotuloRadio <c:if test="${contratoVO.selIndicadorVencDiaNaoUtil ne true}"> rotuloDesabilitado</c:if>" for="indicadorVencDiaNaoUtilNao">N�o</label>
						</c:otherwise>
					</c:choose>		
				</div>
		<br/><br/><br/>
				<div id="divDiasPossiveisVencimento">	
					<fieldset id="dadosItensFaturamento">			
						<fieldset class="conteinerLista1">
						    <legend class="conteinerBlocoTitulo">Dias de Vencimentos Poss�veis</legend>
							<a name="listaSegmentoAmostragemPCSDisponiveis"></a>
							<label class="rotulo rotuloVertical" id="rotuloLocalAmostragemPCS" for="listaDiasDisponiveis">Dispon�veis:</label><br />
							<select id="listaDiasDisponiveis" class="campoList campoVertical" name="listaDiasDisponiveis" multiple="multiple"  
							onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaDiasDisponiveis,document.forms[0].listaDiasSelecionados,true);ordenarListaDiasVencimento();">
								<c:forEach items="${listaDiasDisponiveis}" var="dia">
									<option value="<c:out value="${dia}"/>" title="<c:out value="${dia}"/>"> <c:out value="${dia}"/></option>
								</c:forEach>
							</select>
							
							<fieldset class="conteinerBotoesCampoList1a">
								<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol2" id="botaoDireita" 
									onClick="moveSelectedOptionsEspecial(document.forms[0].listaDiasDisponiveis,document.forms[0].listaDiasSelecionados,true);">
								<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
									onClick="moveAllOptionsEspecial(document.forms[0].listaDiasDisponiveis,document.forms[0].listaDiasSelecionados,true);">
								<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
									onClick="moveSelectedOptionsEspecial(document.forms[0].listaDiasSelecionados,document.forms[0].listaDiasDisponiveis,true);">
								<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
									onClick="moveAllOptionsEspecial(document.forms[0].listaDiasSelecionados,document.forms[0].listaDiasDisponiveis,true);">
							</fieldset>
						</fieldset>
						
						<fieldset class="conteinerLista2" id="camposConteinerLocalAmostragemPCSSelecionados">
							<label id="rotuloLocalAmostragemPCSSelecionados" class="rotulo rotuloListaLonga2" for="listaDiasSelecionados">Selecionados: <span class="itemContador" id="quantidadePontosAssociados"></span></label><br />
							<select id="listaDiasSelecionados" class="campoList campoVertical" name="listaDiasSelecionados" multiple="multiple" 
								onDblClick="moveSelectedOptionsEspecial(document.forms[0].listaDiasSelecionados,document.forms[0].listaDiasDisponiveis,true);">
								<c:forEach items="${listaDiasSelecionados}" var="dia">
									<option value="<c:out value="${dia}"/>" title="<c:out value="${dia}"/>"> <c:out value="${dia}"/></option>
								</c:forEach>
							</select>	
						</fieldset>
					</fieldset>	
				</div>		
			</div>
			<input name="Button" id="botaoExcluirItemFaturamento" class="bottonRightCol" value="Excluir" type="button" onclick="excluirItensFaturamento();" <ggas:campoSelecionado campo="diaVencimentoItemFatura"/> />
			<input class="bottonRightCol" name="botaoAlterarContratoFaturamento" <ggas:campoSelecionado campo="diaVencimentoItemFatura"/> value="Alterar" <c:if test="${empty (contratoVO.idItemFaturamento) || contratoVO.idItemFaturamento <= 0}">disabled="disabled"</c:if>  onclick="alterarItemFaturamentoCadastrado();" type="button"/>
			<input id="botaoAdicionarItemFaturamento" name="Button" class="bottonRightCol bottonRightColUltimo" value="Adicionar" type="button" onclick="adicionarNovoItemFaturamento();" <ggas:campoSelecionado campo="diaVencimentoItemFatura"/> />
			<input name="idItemFaturamento" type="hidden" id="idItemFaturamento" value="${contratoVO.idItemFaturamento}"/>
			
			<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar Itens de Faturamento.</p>
			
			<display:table class="dataTableGGAS dataTableAba2 dataTableCabecalho2Linhas" name="listaItemFaturamentoContratoVO" sort="list" id="itemVencimento" requestURI="#" decorator="br.com.ggas.web.contrato.contrato.decorator.ItemFaturamentoCadastradoDecorator">
				<display:column style="width: 25px" media="html" sortable="false" title="<input type='checkbox' id='checkAllItensVencimento'/>">
		      		<input type="checkbox" class="checkItemVencimento" name="itensFaturamento" value="${itemVencimento.chavePrimariaItemFatura}">
		     	</display:column>
				<display:column title="Item da Fatura" style="width: 160px;">
					<a href="javascript:exibirAlteracaoItemFaturamentoCadastrado('${itemVencimento.chavePrimariaItemFatura}');">${itemVencimento.itemFatura.descricao}</a>
				</display:column>   		
				<display:column title="Tarifa">
					<a href="javascript:exibirAlteracaoItemFaturamentoCadastrado('${itemVencimento.chavePrimariaItemFatura}');">${itemVencimento.tarifa.descricao}</a>
				</display:column>
				<display:column title="Dia<br />Venc." style="width: 40px">
					<a href="javascript:exibirAlteracaoItemFaturamentoCadastrado('${itemVencimento.chavePrimariaItemFatura}');">${itemVencimento.numeroDiaVencimento}</a>
				</display:column>
				<display:column title="Op��o" style="width: 110px;">
					<a href="javascript:exibirAlteracaoItemFaturamentoCadastrado('${itemVencimento.chavePrimariaItemFatura}');">${itemVencimento.opcaoFaseReferencia.descricao}</a>
				</display:column>			
				<display:column title="Ap�s" style="width: 80px">
					<a href="javascript:exibirAlteracaoItemFaturamentoCadastrado('${itemVencimento.chavePrimariaItemFatura}');">${itemVencimento.faseReferencia.descricao}</a>
				</display:column>									
				<display:column title="Venc. em<br />dia n�o �til" style="width: 70px">
					<a href="javascript:exibirAlteracaoItemFaturamentoCadastrado('${itemVencimento.chavePrimariaItemFatura}');">
						<c:choose>
							<c:when test="${itemVencimento.vencimentoDiaUtil eq true}">Sim</c:when>
							<c:otherwise>N�o</c:otherwise>
						</c:choose>
					</a>
				</display:column>
				<c:set var="i" value="${i+1}" />
			</display:table>
										
		</fieldset>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Contratos.</p>
</fieldset>