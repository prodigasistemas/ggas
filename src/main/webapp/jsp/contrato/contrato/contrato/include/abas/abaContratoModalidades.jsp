<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<script>
	$(function(){
		
		var tipo = document.getElementById("qdsMaiorQDC").value
		
		var isFluxoDetalhamento = ${contratoVO.fluxoDetalhamento};
		var isFluxoAditamento = ${contratoVO.fluxoAditamento};
		
		//Verifica se existem campos obrigat�rios e exibe ou esconde a legenda.
		var campoObrigatorioModalidades = $("#modalidades .campoObrigatorio").size();
	
		if (campoObrigatorioModalidades > 0){
			$("#modalidades p.legenda").show();
			$("#tipoModalidade").removeAttr("style");
		} else {
			$("#modalidades p.legenda").hide();
			
			if (isFluxoDetalhamento) {
				$("#tipoModalidade").prop("style", "display:none");
			}
		}

		//Seleciona e Desseleciona as colunas de checkbox.
		$("#checkAllModalidades").click(
				function() {
					$("input[type='checkbox'].checkItemModalidade")
							.attr('checked',
									$('#checkAllModalidades').is(':checked'));
				});
		$('#checkAllCompromissos').click(
				function() {
					$("input[type='checkbox'].checkItemCompromissos").attr(
							'checked',
							$('#checkAllCompromissos').is(':checked'));
				});

		$('#checkAllCompromissosShipOrPay').click(
				function() {
					$("input[type='checkbox'].checkItemCompromissosShipOrPay")
							.attr(
									'checked',
									$('#checkAllCompromissosShipOrPay').is(
											':checked'));
				});

		manipularFormaApuracao($('#consideraParadaProgramadaCSim'),
				$('#consideraParadaProgramadaCNao'),
				$('#apuracaoParadaProgramada'),
				$('#spanApuracaoParadaProgramada'));

		manipularFormaApuracao($('#consideraFalhaFornecimentoCSim'),
				$('#consideraFalhaFornecimentoCNao'),
				$('#apuracaoFalhaFornecimento'),
				$('#spanApuracaoFalhaFornecimento'));

		manipularFormaApuracao($('#consideraCasoFortuitoCSim'),
				$('#consideraCasoFortuitoCNao'), $('#apuracaoCasoFortuito'),
				$('#spanApuracaoCasoFortuito'));

		ajustarAsteriscosFaturamento();
		//$("label[for='TOPrecuperacaoAutoTakeOrPayNao']").css({"padding-right":"0","margin-right":"0"})
		definirCamposQds();
	});

	function manipularFormaApuracao(elementRadioSim, elementRadioNao, element,
			spanElement) {
		$(elementRadioSim).click(function() {
			habilitaETornaCampoObrigatorio($(element), $(spanElement))
		});
		$(elementRadioNao).click(function() {
			desabilitarERetirarObrigatoriedade($(element), $(spanElement))
		});

		if ($(elementRadioSim).is(':checked')) {
			habilitaETornaCampoObrigatorio($(element), $(spanElement));
		}

		if ($(elementRadioNao).is(':checked')) {
			desabilitarERetirarObrigatoriedade($(element), $(spanElement))
		}
	}

	function habilitaETornaCampoObrigatorio(element, spanElement) {
		$(element).prop("disabled", false);
		$(spanElement).text('*');
	}
	function desabilitarERetirarObrigatoriedade(element, spanElement) {
		$(element).prop("disabled", true);
		$(element).val(-1);
		$(spanElement).text("");
	}

	function selecionarOpcoesQDCContrato() {
		var selectQDC = document.getElementById("QDCContrato");
		if (selectQDC != undefined) {
			selectAllOptions(selectQDC);
		}
	}

	function verificarSelecaoCompromissoTOPAbaModalidade() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.indicePeriodicidadeTakeOrPay != undefined) {
			var total = form.indicePeriodicidadeTakeOrPay.length;
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if (form.indicePeriodicidadeTakeOrPay[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.indicePeriodicidadeTakeOrPay.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				alert("Selecione um ou mais compromissos para realizar o opera��o!");
				return false;
			}

		} else {
			return false;
		}

		return true;
	}

	function verificarSelecaoPenalidadeRetiradaMaiorMenorAbaModalidade() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined
				&& form.indicePenalidadeRetiradaMaiorMenorModalidade != undefined) {
			var total = form.indicePenalidadeRetiradaMaiorMenorModalidade.length;
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if (form.indicePenalidadeRetiradaMaiorMenorModalidade[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.indicePenalidadeRetiradaMaiorMenorModalidade.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				alert("Selecione um ou mais compromissos para realizar o opera��o!");
				return false;
			}

		} else {
			return false;
		}

		return true;
	}

	function verificarSelecaoCompromissoSOPAbaModalidade() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.indicePeriodicidadeShipOrPay != undefined) {
			var total = form.indicePeriodicidadeShipOrPay.length;
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if (form.indicePeriodicidadeShipOrPay[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.indicePeriodicidadeShipOrPay.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				alert("Selecione um ou mais compromissos para realizar o opera��o!");
				return false;
			}

		} else {
			return false;
		}

		return true;
	}

	function alterarModalidadeCadastrada() {
		adicionarModalidadeContrato(false);
	}

	function verificarSelecaoModalidadeContrato() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.idsModalidadeContrato != undefined) {
			var total = form.idsModalidadeContrato.length;
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if (form.idsModalidadeContrato[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.idsModalidadeContrato.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				alert("Selecione um ou mais modalidades para realizar o opera��o!");
				return false;
			}

		} else {
			return false;
		}

		return true;
	}

	function limparCamposAbaModalidades() {

		document.getElementById("tipoModalidade").value = '-1';
		document.getElementById("QDCContrato").value = '';
		document.getElementById("prazoRevisaoQuantidadesContratadas").value = '';
		document.getElementById("indicadorQDSMaiorQDCNao").checked = false;
		document.getElementById("indicadorQDSMaiorQDCSim").checked = false;
		document.getElementById("antecedenciaEnvioMensal").value = '';
		document.getElementById("numMesesSolicConsumo").value = '';
		document.getElementById("QDPconfirmadoAutomaticamenteMudan�aDiariaNao").checked = false;
		document.getElementById("QDPconfirmadoAutomaticamenteMudan�aDiariaSim").checked = false;
		document.getElementById("tipoCompromisso").value = '-1';
		document.getElementById("mensalConsumoReferenciaInferior").value = '-1';
		document.getElementById("mensalVariacaoInferior").value = '';
		document.getElementById("TOPrecuperacaoAutoTakeOrPayNao").checked = true;
		document.getElementById("percentualMaximoRelacaoQDC").value = '';
		document.getElementById("percentualMaximoTerminoContrato").value = '';
		document.getElementById("dataInicioRecuperacao").value = '';
		document.getElementById("dataMaximaRecuperacao").value = '';
		document.getElementById("tempoValidadeRetiradaQPNR").value = '';
		document.getElementById("mensalConsumoReferenciaInferior2").value = '-1';
		document.getElementById("mensalVariacaoInferior2").value = '';
		document.getElementById("idModalidadeContrato").value = '';
		limparDadosQDCContrato();
		ajustarAsteriscosFaturamento();
		
	}

	function habilitarBotaoAdicionarModadelidade(campo) {
		if (campo.value != '-1') {
			document.getElementById('botaoAdicionar').disabled = false;
		} else {
			document.getElementById('botaoAdicionar').disabled = true;
		}
	}

	window.onload = function mostrarAsteriscoQDS() {

		var nomeCampo = [ "diasAntecSolicConsumo", "numMesesSolicConsumo" ];
		var campoDesabilitado = $("#indicadorQDSMaiorQDCSim").attr("disabled");

		for (var i = 0; i < nomeCampo.length; i++) {
			var classe = $("#span" + nomeCampo[i]).attr("class");

			if (classe != "campoObrigatorioSimbolo"
					&& campoDesabilitado != true) {

				var descricaoLabel = $("label[for=" + nomeCampo[i] + "]")
						.html();
				var span = "<span id='span"+ nomeCampo[i] +"' class='campoObrigatorioSimbolo'>* </span>";
				$("label[for=" + nomeCampo[i] + "]")
						.html(span + descricaoLabel);
				$("label[for=" + nomeCampo[i] + "]").addClass(
						"campoObrigatorio");

			}

		}
	}

	function init() {
		var periodicidadeTakeOrPaySelecionado = "<c:out value='${contratoVO.periodicidadeTakeOrPay}'/>";

		var selectQDC = document.getElementById('QDCContrato');
		if (selectQDC != null) {
			ordenarSelectQDC(selectQDC);
		}

		ajustarAsteriscosFaturamento();
	}

	addLoadEvent(init);

	function mostrarAsteriscoFaturamento(valor, nomeCampo) {
		if (valor != undefined) {
			for (var i = 0; i < nomeCampo.length; i++) {
				var descricaoLabel = $("label[for=" + nomeCampo[i] + "]")
						.html();
				// Se o campo pai tem algum valor.
				if (valor > 0 || valor == "true" || valor == "false") {
					// Se o campo pai mudar de valor n�o adiciona outro asterisco, pois j� existe.
					if ($("#span" + nomeCampo[i]).length == 0) {
						var span = "<span id='span"+ nomeCampo[i] +"' class='campoObrigatorioSimbolo2'>* </span>";
						$("label[for=" + nomeCampo[i] + "]").html(
								span + descricaoLabel);
						$("label[for=" + nomeCampo[i] + "]").addClass(
								"campoObrigatorio");
					}

					// Se o campo pai n�o tem valor.
				} else {
					var classe = $("#span" + nomeCampo[i]).attr("class");
					// S� remove caso o label tenha asterisco preto.
					if (classe == "campoObrigatorioSimbolo2") {
						$("#span" + nomeCampo[i]).remove();
						$("label[for=" + nomeCampo[i] + "]").removeClass(
								"campoObrigatorio");
					}
				}
			}
		}
	}

	function definirCamposQds() {
		if ($('#indicadorProgramacaoConsumo').val() == 'true') {
			habilitarCampos('qdsMaiorQDC', 'horaLimiteProgramacaoDiaria',
					'horaLimiteProgIntradiaria', 'horaLimiteAceitacaoDiaria',
					'horaLimiteAceitIntradiaria',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaSim',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaNao');
			exibirCampos('qdsMaiorQDC', 'horaLimiteProgramacaoDiaria',
					'horaLimiteProgIntradiaria', 'horaLimiteAceitacaoDiaria',
					'horaLimiteAceitIntradiaria',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaSim',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaNao');
		} else {
			desabilitarCampos('qdsMaiorQDC', 'horaLimiteProgramacaoDiaria',
					'horaLimiteProgIntradiaria', 'horaLimiteAceitacaoDiaria',
					'horaLimiteAceitIntradiaria',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaSim',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaNao');
			esconderCampos('qdsMaiorQDC', 'horaLimiteProgramacaoDiaria',
					'horaLimiteProgIntradiaria', 'horaLimiteAceitacaoDiaria',
					'horaLimiteAceitIntradiaria',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaSim',
					'QDPconfirmadoAutomaticamenteMudan�aDiariaNao');
		}
		definirCamposVariacaoSuperiorQDC();
	}

	function exibirCampos() {
		for (var i = 0; i < arguments.length; i++) {
			$('#' + arguments[i]).parent("div.labelCampo")
					.removeClass("hidden");
		}
	}

	function esconderCampos() {
		for (var i = 0; i < arguments.length; i++) {
			$('#' + arguments[i]).parent("div.labelCampo").addClass("hidden");
		}
	}

	function definirCamposVariacaoSuperiorQDC() {
		if ($('#indicadorProgramacaoConsumo').val() == 'true'
				&& $('#qdsMaiorQDC').val() == 'true') {
			habilitarCampos('variacaoSuperiorQDC');
			exibirCampos('variacaoSuperiorQDC');
		} else {
			desabilitarCampos('variacaoSuperiorQDC');
			esconderCampos('variacaoSuperiorQDC');
		}
	}

	function ajustarAsteriscosFaturamento() {
		if (!$("#indicadorQDSMaiorQDCSim").attr("disabled")) {
			if ($("#indicadorQDSMaiorQDCSim").attr("checked")) {
				mostrarAsterisco($("#indicadorQDSMaiorQDCSim").val(), [
						'diasAntecSolicConsumo', 'numMesesSolicConsumo' ]);
			} else if (!$("#indicadorQDSMaiorQDCNao").attr("disabled")) {
				if ($("#indicadorQDSMaiorQDCNao").attr("checked")) {
					mostrarAsterisco($("#indicadorQDSMaiorQDCNao").val(), [
							'diasAntecSolicConsumo', 'numMesesSolicConsumo' ]);
				} else {
					mostrarAsterisco(0, [ 'diasAntecSolicConsumo',
							'numMesesSolicConsumo' ]);
				}
			}
		}

		if (!$("#tipoCompromisso").attr("disabled")) {
			mostrarAsteriscoFaturamento($("#tipoCompromisso").val(),
					[ 'mensalConsumoReferenciaInferior',
							'mensalVariacaoInferior' ]);
		}

		if (!$("#dataInicioRecuperacao").attr("disabled")) {
			mostrarAsterisco($("#dataInicioRecuperacao").val(), [
					'dataMaximaRecuperacao', 'percentualMaximoTerminoContrato',
					'percentualMaximoRelacaoQDC' ]);
		}

		if (!$("#mensalConsumoReferenciaInferior2").attr("disabled")) {
			mostrarAsteriscoFaturamento($("#mensalConsumoReferenciaInferior2")
					.val(), [ 'mensalVariacaoInferior2' ]);
		}
	}

	function adicionarPenalidadeRetiradaMaiorMenorModalidade() {
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		submeter('contratoForm',
				'adicionarPenalidadeRetiradaMaiorMenorModalidade');
	}

	function adicionarPenalidadeRetiradaMaiorMenorModalidadeAditamento() {
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		submeter('contratoForm',
				'adicionarPenalidadeRetiradaMaiorMenorModalidadeAditamento');
	}

	function excluirPenalidadeRetiradaMaiorMenorModalidade() {
		var selecao = verificarSelecaoPenalidadeRetiradaMaiorMenorAbaModalidade();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				selecionarItensComponenteSelect();
				selecionarOpcoesQDCContrato();
				submeter('contratoForm',
						'excluirPenalidadeRetiradaMaiorMenorModalidade');
			}
		}
	}

	function excluirPenalidadeRetiradaMaiorMenorModalidadeAditamento() {
		var selecao = verificarSelecaoPenalidadeRetiradaMaiorMenorAbaModalidade();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				selecionarItensComponenteSelect();
				selecionarOpcoesQDCContrato();
				submeter('contratoForm',
						'excluirPenalidadeRetiradaMaiorMenorModalidadeAditamento');
			}
		}
	}

	function habilitarAvisoInterrupcao(valor) {
		if (valor == "3") {
			$("#percentualCobIntRetMaiorMenorM").removeClass(
					"rotuloDesabilitado");
			$("#percentualCobIntRetMaiorMenorM").prop("disabled", false);
		} else if (valor == "4") {
			$("#percentualCobIntRetMaiorMenorM").addClass("rotuloDesabilitado");
			$("#percentualCobIntRetMaiorMenorM").prop("disabled", true);
		}
	}
</script>
<!--  Aba Modalidades -->
<fieldset id="modalidades">
	<c:choose>
		<c:when test="${contratoVO.fluxoDetalhamento}">
			<a class="linkHelp" href="<help:help>/detalhamentodocontratoabamodalidadeconsumofaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:when>
		<c:otherwise>
			<a class="linkHelp" href="<help:help>/abamodalidadesconsumofaturamentoinclusoalteraoadiodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:otherwise>
	</c:choose>
	
	<div class="labelCampo">
		<ggas:labelContrato forCampo="modalidade">Modalidade:</ggas:labelContrato>
		<select class="campoSelect campoHorizontal" 
			name="modalidade" id="tipoModalidade" 
			<ggas:campoSelecionado campo="modalidade"/> 
			onchange="habilitarBotaoAdicionarModadelidade(this);">
			
			<option value="-1">Selecione</option>
			<c:forEach items="${listaContratoModalidade}" var="modalidadeContrato">
				<option value="<c:out value="${modalidadeContrato.chavePrimaria}"/>" 
					<c:if test="${contratoVO.modalidade == modalidadeContrato.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modalidadeContrato.descricao}"/>
				</option>		
			</c:forEach>
			
		</select>
		<hr class="linhaSeparadora"/>
	</div>
	
	<div class="agrupamento">
		<c:set var="dataQDC" value="qdc" />
		<c:set var="volumeQDC" value="qdc" />
		<c:set var="modalidade" value="true" />
		<c:set var="idConteinerQDC" value="conteinerQDCpontoConsumo" />
		<c:set var="nomeComponente" value="QDC do Ponto de Consumo" />
		<%@ include file="/jsp/contrato/contrato/contrato/include/dadosQDCContrato.jsp" %>
	</div>
	
	
	<div class="agrupamento">
		<fieldset id="conteinerConsumoQDS" class="colunaEsq2">
		
			<legend>QDS</legend>
			<div class="conteinerDados colunaEsquerda">
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="indicadorProgramacaoConsumo">Efetua programa��o de consumo?</ggas:labelContrato>
					<select  name="indicadorProgramacaoConsumo" class="campoSelect"  
						id="indicadorProgramacaoConsumo" style="min-width: 70px;"
						onchange="definirCamposQds()"
						<ggas:campoSelecionado campo="indicadorProgramacaoConsumo"/>  >
						
						<option value="true" id="indicadorProgramacaoConsumoSim"  <c:if test="${contratoVO.indicadorProgramacaoConsumo == 'true'}">selected="selected"</c:if>>Sim</option>
						<option value="false" id="indicadorProgramacaoConsumoNao" <c:if test="${contratoVO.indicadorProgramacaoConsumo == 'false'}">selected="selected"</c:if>>N�o</option>
					</select>
				</div>
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="qdsMaiorQDC">Pode ser maior que o QDC?</ggas:labelContrato>
					<select  name="qdsMaiorQDC" class="campoSelect" style="min-width: 70px;"
						id="qdsMaiorQDC" onchange="definirCamposVariacaoSuperiorQDC();" disabled="disabled" 
						<ggas:campoSelecionado campo="qdsMaiorQDC"/>  >
						
						<option value="true" id="qdsMaiorQDCSim"  <c:if test="${contratoVO.qdsMaiorQDC == 'true'}">selected="selected"</c:if>>Sim</option>
						<option value="false" id="qdsMaiorQDCNao" <c:if test="${contratoVO.qdsMaiorQDC == 'false'}">selected="selected"</c:if>>N�o</option>
					</select>
				</div>
				
				<div class="labelCampo">
					<ggas:labelContrato 
						styleClass="rotuloVertical" 
						forCampo="variacaoSuperiorQDC">
							Percentual de Toler�ncia Di�ria Superior a QDC:
					</ggas:labelContrato>
					
					<input class="campoTexto campoHorizontal" 
						type="text" id="variacaoSuperiorQDC" name="variacaoSuperiorQDC" 
						size="3" maxlength="6" disabled="disabled"
						<ggas:campoSelecionado campo="variacaoSuperiorQDC"/> 
						onblur="aplicarMascaraNumeroDecimal(this,2);" 
						onkeypress="return formatarCampoDecimal(event,this,3,2);" 
						name="variacaoSuperiorQDC" value="${contratoVO.variacaoSuperiorQDC}" >
						
					<label class="rotuloHorizontal rotuloInformativo">%</label>
				</div>
				
				<br />
				<br />	
				
				<div class="labelCampo">
					<ggas:labelContrato 
						styleClass="rotuloVertical" 
						forCampo="horaLimiteProgramacaoDiaria">
							Hor�rio Limite Programa��o Di�ria:
					</ggas:labelContrato>	
																
					<input id="horaLimiteProgramacaoDiaria" 
						class="campoTexto campoHorizontal" type="text" 
						size="1" maxlength="2" name="horaLimiteProgramacaoDiaria" 
						onkeypress="return formatarCampoInteiro(event);" disabled="disabled"
						<ggas:campoSelecionado campo="horaLimiteProgramacaoDiaria"/> 
						value="${contratoVO.horaLimiteProgramacaoDiaria}" />
						
					<label class="rotuloHorizontal rotuloInformativo" 
						for="horaLimiteProgramacaoDiaria">horas</label>	
				</div>
					
				<br />
				<br />
				
				<div class="labelCampo">
					<ggas:labelContrato styleClass="rotuloVertical" forCampo="horaLimiteProgIntradiaria">Hor�rio Limite Programa��o Intradi�ria:</ggas:labelContrato>	
					<input id="horaLimiteProgIntradiaria" class="campoTexto campoHorizontal" 
						type="text" size="1" maxlength="2" name="horaLimiteProgIntradiaria" 
						onkeypress="return formatarCampoInteiro(event);" disabled="disabled" 
						<ggas:campoSelecionado campo="horaLimiteProgIntradiaria"/> 
						value="${contratoVO.horaLimiteProgIntradiaria}" />
					<label class="rotuloHorizontal rotuloInformativo" for="horaLimiteProgIntradiaria">horas</label>		<br /><br />
				</div>	
				
				<div class="labelCampo">
					<ggas:labelContrato styleClass="rotuloVertical" forCampo="horaLimiteAceitacaoDiaria">Hor�rio Limite Aceita��o Di�ria:</ggas:labelContrato>												
					<input id="horaLimiteAceitacaoDiaria" 
						class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" disabled="disabled"
						name="horaLimiteAceitacaoDiaria" onkeypress="return formatarCampoInteiro(event);"
						<ggas:campoSelecionado campo="horaLimiteAceitacaoDiaria"/> 
						value="${contratoVO.horaLimiteAceitacaoDiaria}" />
					<label class="rotuloHorizontal rotuloInformativo" for="horaLimiteAceitacaoDiaria">horas</label>	<br /><br />
				</div>
				
				<div class="labelCampo">		
					<ggas:labelContrato styleClass="rotuloVertical" forCampo="horaLimiteAceitIntradiaria">Hor�rio Limite Aceita��o Intradi�ria:</ggas:labelContrato>	
					<input id="horaLimiteAceitIntradiaria" class="campoTexto campoHorizontal" 
						type="text" size="1" maxlength="2" name="horaLimiteAceitIntradiaria" 
						onkeypress="return formatarCampoInteiro(event);" disabled="disabled"
						<ggas:campoSelecionado campo="horaLimiteAceitIntradiaria"/> 
						value="${contratoVO.horaLimiteAceitIntradiaria}" />
					<label class="rotuloHorizontal rotuloInformativo" for="horaLimiteAceitIntradiaria">horas</label>		<br /><br />
				</div>		
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="confirmacaoAutomaticaQDS">Confirma��o autom�tica da QDS:</ggas:labelContrato>	
					
					<c:choose>
						<c:when test="${param.fluxoDetalhamento}">
							<c:choose>
								<c:when test="${contratoVO.confirmacaoAutomaticaQDS == 'true'}">
									<label>Sim</label>
								</c:when>
								<c:when test="${contratoVO.confirmacaoAutomaticaQDS == 'false'}">
									<label>N�o</label>
								</c:when>
							</c:choose>
						</c:when>
						<c:otherwise>
						
							<input class="campoRadio" type="radio" name="confirmacaoAutomaticaQDS" 
								id="QDPconfirmadoAutomaticamenteMudan�aDiariaSim" value="true"  
								<ggas:campoSelecionado campo="confirmacaoAutomaticaQDS"/> disabled="disabled" 
								<c:if test="${not empty contratoVO.confirmacaoAutomaticaQDS 
									&& contratoVO.confirmacaoAutomaticaQDS == true}">checked="checked"</c:if>>		
							<label for="QDPconfirmadoAutomaticamenteMudan�aDiariaSim">Sim</label>
							
							<input class="campoRadio" type="radio" name="confirmacaoAutomaticaQDS" 
								id="QDPconfirmadoAutomaticamenteMudan�aDiariaNao" value="false" 
								<ggas:campoSelecionado campo="confirmacaoAutomaticaQDS"/> disabled="disabled" 
								<c:if test="${not empty contratoVO.confirmacaoAutomaticaQDS 
									&& contratoVO.confirmacaoAutomaticaQDS == false}">checked="checked"</c:if>>
							<label for="QDPconfirmadoAutomaticamenteMudan�aDiariaNao">N�o</label>
							
						</c:otherwise>
					</c:choose>
				</div>
					
				<br class="quebra2Linhas" />
			</div>
		</fieldset>
	
		<hr class="linhaSeparadora"/>
	</div>
	
	<fieldset class="conteinerBloco">
		
		<div class="agrupamento">
			<fieldset id="conteinerGarantia" class="colunaEsq">
				<legend>Take or Pay</legend>
				<fieldset class="conteinerDados">
					<fieldset class="conteinerDados2">
						<div class="labelCampo">
							<ggas:labelContrato forId="tipoCompromisso" styleClass="rotuloVertical rotulo2Linhas" forCampo="periodicidadeTakeOrPay">Compromisso:</ggas:labelContrato>
								<select class="campoSelect campoHorizontal" name="periodicidadeTakeOrPay" id="tipoCompromisso" <ggas:campoSelecionado campo="periodicidadeTakeOrPay"/> onchange="mostrarAsteriscoFaturamento(this.value,['tipoReferenciaQFParadaProgramada','mensalConsumoReferenciaInferior','mensalVariacaoInferior','referenciaQFParadaProgramada','tipoAgrupamentoContrato','dataInicioVigencia','dataFimVigencia','precoCobranca','tipoApuracao','consideraParadaProgramada','consideraParadaNaoProgramada','consideraFalhaFornecimento','consideraCasoFortuito','recuperavel','percentualNaoRecuperavel','apuracaoParadaNaoProgramada']);">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeTakeOrPay">
									<option value="<c:out value="${periodicidadeTakeOrPay.chavePrimaria}"/>" <c:if test="${contratoVO.periodicidadeTakeOrPay == periodicidadeTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${periodicidadeTakeOrPay.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							<br class="quebraLinha2" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forId="tipoReferenciaQFParadaProgramada" styleClass="rotuloVertical rotulo2Linhas" forCampo="referenciaQFParadaProgramada">Refer�ncia Quantidade Faltante Parada Programada:</ggas:labelContrato>
								<select class="campoSelect campoHorizontal" name="referenciaQFParadaProgramada" id="tipoReferenciaQFParadaProgramada" <ggas:campoSelecionado campo="referenciaQFParadaProgramada"/> >
								<option value="-1">Selecione</option>
								<c:forEach items="${listaReferenciaQFParadaProgramada}" var="referenciaQFParadaProgramada">
									<option value="<c:out value="${referenciaQFParadaProgramada.chavePrimaria}"/>" <c:if test="${contratoVO.referenciaQFParadaProgramada == referenciaQFParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${referenciaQFParadaProgramada.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							<br class="quebraLinha2" />
						</div>
						
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="mensalConsumoReferenciaInferior" forCampo="consumoReferenciaTakeOrPay">Consumo refer�ncia inferior:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="mensalConsumoReferenciaInferior" name="consumoReferenciaTakeOrPay" <ggas:campoSelecionado campo="consumoReferenciaTakeOrPay"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaTakeOrPay">
								<option value="<c:out value="${consumoReferenciaTakeOrPay.chavePrimaria}"/>" <c:if test="${contratoVO.consumoReferenciaTakeOrPay == consumoReferenciaTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${consumoReferenciaTakeOrPay.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br class="quebraLinha2" />
						</div>
						
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloMensalVariacaoInferior" styleClass="rotuloVertical rotulo2Linhas" forId="mensalVariacaoInferior" forCampo="margemVariacaoTakeOrPay">Compromisso de retirada ToP:</ggas:labelContrato>
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="mensalVariacaoInferior" name="margemVariacaoTakeOrPay" size="3" maxlength="6" <ggas:campoSelecionado campo="margemVariacaoTakeOrPay"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" name="margemVariacaoTakeOrPay" value="${contratoVO.margemVariacaoTakeOrPay}">
							<label class="rotuloHorizontal rotuloInformativo">%</label>
							<br class="quebraLinha2" />
						</div>
						
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="tipoAgrupamentoContrato" forCampo="tipoAgrupamentoContrato">Tipo:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="tipoAgrupamentoContrato" name="tipoAgrupamentoContrato" <ggas:campoSelecionado campo="tipoAgrupamentoContrato"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoAgrupamentoContrato}" var="tipoAgrupamentoContrato">
								<option value="<c:out value="${tipoAgrupamentoContrato.chavePrimaria}"/>" <c:if test="${contratoVO.tipoAgrupamentoContrato == tipoAgrupamentoContrato.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoAgrupamentoContrato.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br class="quebraLinha2" />
						</div>
						
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="dataInicioVigencia">Data In�cio Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataInicioVigencia" name="dataInicioVigencia" maxlength="10" <ggas:campoSelecionado campo="dataInicioVigencia"/> value="${contratoVO.dataInicioVigenciaC}"><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="dataFimVigencia">Data Fim Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataFimVigencia" name="dataFimVigencia" maxlength="10" <ggas:campoSelecionado campo="dataFimVigencia"/> value="${contratoVO.dataFimVigenciaC}"><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">			
							<ggas:labelContrato styleId="tipoReferenciaQFParadaProgramada" styleClass="rotuloVertical rotulo2Linhas" forCampo="precoCobranca">Tabela de Pre�os para Cobran�a:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" name="precoCobranca" id="precoCobranca" <ggas:campoSelecionado campo="precoCobranca"/> >
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
									<option value="<c:out value="${precoCobranca.chavePrimaria}"/>" <c:if test="${contratoVO.precoCobrancaC == precoCobranca.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${precoCobranca.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							<br class="quebraLinha2" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="tipoApuracao" forCampo="tipoApuracao">Forma de C�lculo para Cobran�a e Recupera��o:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="tipoApuracao" name="tipoApuracao" <ggas:campoSelecionado campo="tipoApuracao"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
								<option value="<c:out value="${tipoApuracao.chavePrimaria}"/>" <c:if test="${contratoVO.tipoApuracaoC == tipoApuracao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoApuracao.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br class="quebraLinha2" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="consideraParadaProgramada" styleClass="rotuloVertical rotulo2Linhas">Considerar Paradas Programadas:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="consideraParadaProgramada" id="consideraParadaProgramadaCSim" value="true" <ggas:campoSelecionado campo="consideraParadaProgramada"/> <c:if test="${contratoVO.consideraParadaProgramadaC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraParadaProgramadaCSim">Sim</label>
							<input class="campoRadio" type="radio" name="consideraParadaProgramada" id="consideraParadaProgramadaCNao" value="false" <ggas:campoSelecionado campo="consideraParadaProgramada"/> <c:if test="${contratoVO.consideraParadaProgramadaC == false or empty contratoVO.consideraParadaProgramadaC}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraParadaProgramadaCNao">N�o</label>
							<br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">												
							<ggas:labelContrato forCampo="consideraFalhaFornecimento" styleClass="rotuloVertical rotulo2Linhas">Considerar Falha de Fornecimento:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="consideraFalhaFornecimento" id="consideraFalhaFornecimentoCSim" value="true" <ggas:campoSelecionado campo="consideraFalhaFornecimento"/> <c:if test="${contratoVO.consideraFalhaFornecimentoC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraFalhaFornecimentoCSim">Sim</label>
							<input class="campoRadio" type="radio" name="consideraFalhaFornecimento" id="consideraFalhaFornecimentoCNao" value="false" <ggas:campoSelecionado campo="consideraFalhaFornecimento"/> <c:if test="${contratoVO.consideraFalhaFornecimentoC == false or empty contratoVO.consideraFalhaFornecimentoC}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraFalhaFornecimentoCNao">N�o</label>
							<br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="consideraCasoFortuito" styleClass="rotuloVertical rotulo2Linhas">Considerar Casos Fortuitos:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="consideraCasoFortuito" id="consideraCasoFortuitoCSim" value="true" <ggas:campoSelecionado campo="consideraCasoFortuito"/> <c:if test="${contratoVO.consideraCasoFortuitoC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraCasoFortuitoCSim">Sim</label>
							<input class="campoRadio" type="radio" name="consideraCasoFortuito" id="consideraCasoFortuitoCNao" value="false" <ggas:campoSelecionado campo="consideraCasoFortuito"/> <c:if test="${contratoVO.consideraCasoFortuitoC == false or empty contratoVO.consideraCasoFortuitoC}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraCasoFortuitoCNao">N�o</label>
							<br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">												
							<ggas:labelContrato forCampo="recuperavel" styleClass="rotuloVertical rotulo2Linhas">Recuper�vel:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="recuperavel" id="TOPrecuperacaoAutoTakeOrPaySim" value="true" <ggas:campoSelecionado campo="recuperavel"/> <c:if test="${contratoVO.recuperavelC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="recuperavelCSim">Sim</label>
							<input class="campoRadio" type="radio" name="recuperavel" id="TOPrecuperacaoAutoTakeOrPayNao" value="false" <ggas:campoSelecionado campo="recuperavel"/> <c:if test="${contratoVO.recuperavelC == false or empty contratoVO.recuperavelC}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="recuperavelCNao">N�o</label>
							<br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloMensalVariacaoInferior" styleClass="rotuloVertical rotulo2Linhas" forId="percentualNaoRecuperavel" forCampo="percentualNaoRecuperavel">Percentual N�o Recuper�vel:</ggas:labelContrato>
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualNaoRecuperavel" name="percentualNaoRecuperavel" size="3" maxlength="6" <ggas:campoSelecionado campo="percentualNaoRecuperavel"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" name="percentualNaoRecuperavel" value="${contratoVO.percentualNaoRecuperavelC}">
							<label class="rotuloHorizontal rotuloInformativo">%</label>
							<br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoParadaProgramada" forCampo="apuracaoParadaProgramada">
								<span id='spanApuracaoParadaProgramada' class='campoObrigatorioSimbolo2'></span> Forma de Apura��o do Volume de Parada Programada:
							</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="apuracaoParadaProgramada" name="apuracaoParadaProgramada" <ggas:campoSelecionado campo="apuracaoParadaProgramada"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaApuracaoParadaProgramada}" var="apuracaoParadaProgramada">
								<option value="<c:out value="${apuracaoParadaProgramada.chavePrimaria}"/>" <c:if test="${contratoVO.apuracaoParadaProgramadaC == apuracaoParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${apuracaoParadaProgramada.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoFalhaFornecimento" forCampo="apuracaoFalhaFornecimento">
								<span id='spanApuracaoFalhaFornecimento' class='campoObrigatorioSimbolo2'></span> Forma de Apura��o do Volume de Falha de Fornecimento:
							</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="apuracaoFalhaFornecimento" name="apuracaoFalhaFornecimento" <ggas:campoSelecionado campo="apuracaoFalhaFornecimento"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaApuracaoFalhaFornecimento}" var="apuracaoFalhaFornecimento">
								<option value="<c:out value="${apuracaoFalhaFornecimento.chavePrimaria}"/>" <c:if test="${contratoVO.apuracaoFalhaFornecimentoC == apuracaoFalhaFornecimento.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${apuracaoFalhaFornecimento.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br />
						</div>	
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoCasoFortuito" forCampo="apuracaoCasoFortuito">
								<span id='spanApuracaoCasoFortuito' class='campoObrigatorioSimbolo2'></span>Forma de Apura��o do Volume de Caso Fortuito ou For�a Maior:
							</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="apuracaoCasoFortuito" name="apuracaoCasoFortuito" <ggas:campoSelecionado campo="apuracaoCasoFortuito"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaApuracaoCasoFortuito}" var="apuracaoCasoFortuito">
								<option value="<c:out value="${apuracaoCasoFortuito.chavePrimaria}"/>" <c:if test="${contratoVO.apuracaoCasoFortuitoC == apuracaoCasoFortuito.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${apuracaoCasoFortuito.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br />
						</div>
					</fieldset>
					
					<fieldset class="conteinerBloco">
						<input name="Button" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeTakeOrPay"/> value="Excluir" type="button" onclick="excluirCompromissoTOPAbaModalidade();">
						<input name="Button" id="botaoAdicionarTakeOrPay" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeTakeOrPay"/> value="Adicionar" type="button" onclick="adicionarCompromissoTOPAbaModalidade();">
					</fieldset>
					
					<c:set var="i" value="0" />
					<display:table id="compromissosTakeOrPayVO" class="dataTableGGAS dataTableAba" name="listaCompromissosTakeOrPayVO" style="width: 428px" sort="list" requestURI="#">
						<display:column style="width: 25px" title="<input type='checkbox' id='checkAllCompromissos'/>">
							<input type="checkbox" class="checkItemCompromissos" name="indicePeriodicidadeTakeOrPay" value="<c:out value="${i}"/>">
						</display:column>
						<display:column title="Compromissos">
							${compromissosTakeOrPayVO.periodicidadeTakeOrPay.descricao}
						</display:column>
						<display:column title="In�cio Vig�ncia">
							<fmt:formatDate value="${compromissosTakeOrPayVO.dataInicioVigencia}" pattern="dd/MM/yyyy"/>
						</display:column>
						<display:column title="Fim Vig�ncia">
							<fmt:formatDate value="${compromissosTakeOrPayVO.dataFimVigencia}" pattern="dd/MM/yyyy"/>
						</display:column>
						<display:column title="Compromisso de retirada ToP">
							<fmt:formatNumber type="percent" minFractionDigits="2" value="${compromissosTakeOrPayVO.margemVariacaoTakeOrPay}"></fmt:formatNumber>
						</display:column>							
						<c:set var="i" value="${i+1}" />
					</display:table>
					
					<div class="labelCampo">
						<ggas:labelContrato forCampo="recuperacaoAutoTakeOrPay" styleClass="rotuloVertical rotulo2Linhas">Recupera��o autom�tica?</ggas:labelContrato>	
						<input class="campoRadio" type="radio" name="recuperacaoAutoTakeOrPay" id="TOPrecuperacaoAutoTakeOrPaySim" value="true" <ggas:campoSelecionado campo="recuperacaoAutoTakeOrPay"/> <c:if test="${contratoVO.recuperacaoAutoTakeOrPay == true}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="TOPrecuperacaoAutoTakeOrPaySim">Sim</label>
						<input class="campoRadio" type="radio" name="recuperacaoAutoTakeOrPay" id="TOPrecuperacaoAutoTakeOrPayNao" value="false" <ggas:campoSelecionado campo="recuperacaoAutoTakeOrPay"/> <c:if test="${contratoVO.recuperacaoAutoTakeOrPay == false  or empty contratoVO.recuperacaoAutoTakeOrPay}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="TOPrecuperacaoAutoTakeOrPayNao">N�o</label>
					</div>
				</fieldset>		
			</fieldset>	
		</div>		
	
		<br/>
		
		<div class="agrupamento">
			<fieldset id="conteinerShipOrPay" class="colunaEsq2">
				<legend>Ship or Pay</legend>				
				<fieldset class="conteinerDados">
					<fieldset class="conteinerDados2">
						
						<div class="labelCampo">
							<ggas:labelContrato forId="tipoCompromissoShipOrPay" forCampo="periodicidadeShipOrPay">Compromisso:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" name="periodicidadeShipOrPay" id="tipoCompromissoShipOrPay"  <ggas:campoSelecionado campo="periodicidadeShipOrPay"/> onchange="mostrarAsteriscoFaturamento(this.value,['mensalConsumoReferenciaInferior2','mensalVariacaoInferior2']);">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeShipOrPay">
									<option value="<c:out value="${periodicidadeShipOrPay.chavePrimaria}"/>" <c:if test="${contratoVO.periodicidadeShipOrPay == periodicidadeShipOrPay.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${periodicidadeShipOrPay.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							<br />
						</div>
									
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloConsumoReferenciaShipOrPay" styleClass="rotuloVertical" forId="mensalConsumoReferenciaInferior2" forCampo="consumoReferenciaShipOrPay">Consumo refer�ncia inferior:</ggas:labelContrato>
							<select class="campoSelect campoVertical campo2Linhas" id="mensalConsumoReferenciaInferior2" name="consumoReferenciaShipOrPay" <ggas:campoSelecionado campo="consumoReferenciaShipOrPay"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaShipOrPay">
								<option value="<c:out value="${consumoReferenciaShipOrPay.chavePrimaria}"/>" <c:if test="${contratoVO.consumoReferenciaShipOrPay == consumoReferenciaShipOrPay.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${consumoReferenciaShipOrPay.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br />
						</div>
						
						
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloMargemVariacaoShipOrPay" styleClass="rotulo2Linhas" forId="mensalVariacaoInferior2" forCampo="margemVariacaoShipOrPay">Compromisso<br />de transporte SoP:</ggas:labelContrato>
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="mensalVariacaoInferior2" name="margemVariacaoShipOrPay" size="3" maxlength="6" <ggas:campoSelecionado campo="margemVariacaoShipOrPay"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"   value="${contratoVO.margemVariacaoShipOrPay}">
							<label class="rotuloHorizontal rotuloInformativo" >%</label>
						</div>
					</fieldset>
					
					<fieldset class="conteinerBloco">
						<input name="Button" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeShipOrPay"/> value="Excluir" type="button" onclick="excluirCompromissoSOPAbaModalidade();">
						<input name="Button" id="botaoAdicionarShipOrPay" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeShipOrPay"/> value="Adicionar" type="button" onclick="adicionarCompromissoSOPAbaModalidade();">
					</fieldset>
					
					<c:set var="i" value="0" />
					<display:table id="compromissosShipOrPayVO" class="dataTableGGAS dataTableAba" name="listaCompromissosShipOrPayVO" style="width: 291px" sort="list" requestURI="#">
						<display:column style="width: 25px" title="<input type='checkbox' id='checkAllCompromissosShipOrPay'/>">
							<input type="checkbox" class="checkItemCompromissosShipOrPay" name="indicePeriodicidadeShipOrPay" value="<c:out value="${i}"/>">
						</display:column>
						<display:column title="Compromissos">
							${compromissosShipOrPayVO.periodicidadeTakeOrPay.descricao}&nbsp;|&nbsp;${compromissosShipOrPayVO.consumoReferenciaTakeOrPay.descricao}&nbsp;|&nbsp;<fmt:formatNumber value="${compromissosShipOrPayVO.margemVariacaoTakeOrPay * 100}" minFractionDigits="2" maxFractionDigits="2"/>%
						</display:column>
						<c:set var="i" value="${i+1}" />
					</display:table>
	
					<!-- <label class="rotuloRadio" for="TOPrecuperacaoAutoTakeOrPayNao">N�o</label> -->
				</fieldset>
			</fieldset>
		</div>
		
		<hr class="linhaSeparadora"/>
	
	
		<div class="agrupamento">
			<fieldset id="conteinerPenalidadeRetMaiorMenor" class="colunaEsq">
			
				<legend>Penalidade por retirada a Maior/Menor:</legend>
				<fieldset class="conteinerDados">
					<fieldset class="conteinerDados2">
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="penalidadeRetMaiorMenorM">Penalidade:</ggas:labelContrato>
							<select class="campoSelect campo2Linhas" name="penalidadeRetMaiorMenorM" id="penalidadeRetMaiorMenorM" <ggas:campoSelecionado campo="penalidadeRetMaiorMenorM"/>onchange="habilitarAvisoInterrupcao(this.value)"; >
									<option value="-1">Selecione</option>
									<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="penalidadeRetMaiorMenor">
										<option value="<c:out value="${penalidadeRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.penalidadeRetMaiorMenorM == penalidadeRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${penalidadeRetMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
							</select><br class="quebraLinha" />
						</div>
							
						<div class="labelCampo">
							<ggas:labelContrato forCampo="periodicidadeRetMaiorMenorM">Periodicidade:</ggas:labelContrato>
							<select class="campoSelect campo2Linhas" name="periodicidadeRetMaiorMenorM" id="periodicidadeRetMaiorMenorM" <ggas:campoSelecionado campo="periodicidadeRetMaiorMenorM"/> >
									<option value="-1">Selecione</option>
									<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="periodicidadeRetMaiorMenor">
										<option value="<c:out value="${periodicidadeRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.periodicidadeRetMaiorMenorM == periodicidadeRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${periodicidadeRetMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
							</select><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="baseApuracaoRetMaiorMenorM">Base de Apura��o:</ggas:labelContrato>
								<select class="campoSelect" name="baseApuracaoRetMaiorMenorM" id="baseApuracaoRetMaiorMenorM" <ggas:campoSelecionado campo="baseApuracaoRetMaiorMenorM"/>>
									<option value="-1">Selecione</option>
									<c:forEach items="${listaBaseApuracao}" var="baseApuracaoRetMaiorMenor">
										<option value="<c:out value="${baseApuracaoRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.baseApuracaoRetMaiorMenorM == baseApuracaoRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${baseApuracaoRetMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
							</select><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="dataIniVigRetirMaiorMenorM">In�cio Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataIniVigRetirMaiorMenorM" name="dataIniVigRetirMaiorMenorM" <ggas:campoSelecionado campo="dataIniVigRetirMaiorMenorM"/> maxlength="10"  value="${contratoVO.dataIniVigRetirMaiorMenorM}"><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="dataFimVigRetirMaiorMenorM">Fim Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataFimVigRetirMaiorMenorM" name="dataFimVigRetirMaiorMenorM" <ggas:campoSelecionado campo="dataFimVigRetirMaiorMenorM"/>  maxlength="10" value="${contratoVO.dataFimVigRetirMaiorMenorM}"><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="precoCobrancaRetirMaiorMenorM">Tabela de Pre�o para Cobran�a:</ggas:labelContrato>
								<select class="campoSelect" name="precoCobrancaRetirMaiorMenorM" id="precoCobrancaRetirMaiorMenorM" <ggas:campoSelecionado campo="precoCobrancaRetirMaiorMenorM"/> >
									<option value="-1">Selecione</option>
									<c:forEach items="${listaPrecoCobranca}" var="precoCobrancaRetirMaiorMenor">
										<option value="<c:out value="${precoCobrancaRetirMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.precoCobrancaRetirMaiorMenorM == precoCobrancaRetirMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${precoCobrancaRetirMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="tipoApuracaoRetirMaiorMenorM">Forma de C�lculo para Cobran�a:</ggas:labelContrato>
								<select class="campoSelect" name="tipoApuracaoRetirMaiorMenorM" id="tipoApuracaoRetirMaiorMenorM" <ggas:campoSelecionado campo="tipoApuracaoRetirMaiorMenorM"/>>
									<option value="-1">Selecione</option>
									<c:forEach items="${listaTipoApuracao}" var="tipoApuracaoRetirMaiorMenor">
										<option value="<c:out value="${tipoApuracaoRetirMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.tipoApuracaoRetirMaiorMenorM == tipoApuracaoRetirMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${tipoApuracaoRetirMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />
						</div>	
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualCobRetMaiorMenorM">Percentual de Cobran�a:</ggas:labelContrato>
								<input class="campoTexto campo2Linhas campoHorizontal"
									type="text" id="percentualCobRetMaiorMenorM"
									name="percentualCobRetMaiorMenorM" maxlength="6" size="4"
									onkeypress="return formatarCampoDecimal(event, this, 3, 2);"
									onblur="aplicarMascaraNumeroDecimal(this, 2);"
									value="${contratoVO.percentualCobRetMaiorMenorM}"  <ggas:campoSelecionado campo="percentualCobRetMaiorMenorM"/>>
								<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>
						</div>
							
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualCobIntRetMaiorMenorM">Percentual de Cobran�a para Consumo Superior Durante Aviso de Interrup��o:</ggas:labelContrato>
								<input class="campoTexto campo2Linhas campoHorizontal"
									type="text" id="percentualCobIntRetMaiorMenorM"
									<ggas:campoSelecionado campo="percentualCobIntRetMaiorMenorM"/>
									name="percentualCobIntRetMaiorMenorM" maxlength="6" size="4"
									onkeypress="return formatarCampoDecimal(event, this, 3, 2);"
									onblur="aplicarMascaraNumeroDecimal(this, 2);"
									value="${contratoVO.percentualCobIntRetMaiorMenorM}">
								<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>	
						</div>	
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="consumoReferRetMaiorMenorM">Consumo Refer�ncia:</ggas:labelContrato>			
							<select class="campoSelect campo2Linhas" name="consumoReferRetMaiorMenorM" id="consumoReferRetMaiorMenorM" <ggas:campoSelecionado campo="consumoReferRetMaiorMenorM"/> >
								<option value="-1">Selecione</option>
								<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaRetMaiorMenor">
									<option value="<c:out value="${consumoReferenciaRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.consumoReferRetMaiorMenorM == consumoReferenciaRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${consumoReferenciaRetMaiorMenor.descricao}"/>
									</option>		
								</c:forEach>
							</select><br class="quebraLinha" />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualRetMaiorMenorM">Percentual Por Retirada:</ggas:labelContrato>
							<input class="campoTexto campo2Linhas campoHorizontal"
								type="text" id="percentualRetMaiorMenorM"
								<ggas:campoSelecionado campo="percentualRetMaiorMenorM"/>
								name="percentualRetMaiorMenorM" maxlength="6" size="4"
								onkeypress="return formatarCampoDecimal(event, this, 3, 2);"
								onblur="aplicarMascaraNumeroDecimal(this, 2);"
								value="${contratoVO.percentualRetMaiorMenorM}">
							<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>	
							<br class="quebraLinha" />
						</div>
							
						<div class="labelCampo">
							<ggas:labelContrato forCampo="indicadorImpostoM" styleClass="rotuloVertical rotulo2Linhas">Usar pre�os s/ impostos para cobran�a:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="indicadorImpostoM" id="indicadorImpostoMSim" value="true" <ggas:campoSelecionado campo="indicadorImpostoM"/> <c:if test="${contratoVO.indicadorImpostoM == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="indicadorImpostoMSim">Sim</label>
							<input class="campoRadio" type="radio" name="indicadorImpostoM" id="indicadorImpostoMNao" value="false" <ggas:campoSelecionado campo="indicadorImpostoM"/> <c:if test="${contratoVO.indicadorImpostoM == false or empty contratoVO.recuperacaoAutoTakeOrPay}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="indicadorImpostoMNao">N�o</label>
						</div>
						
					</fieldset>
						
						<br />
					
					<fieldset class="conteinerBloco">
						<c:choose>
							<c:when test="${contratoVO.fluxoInclusao}">
								<input name="Button" class="bottonRightCol"
									<ggas:campoSelecionado campo="periodicidadeRetMaiorMenorM"/>
									value="Excluir" type="button"
									onclick="excluirPenalidadeRetiradaMaiorMenorModalidade();">
								<input name="Button" id="botaoAdicionarRetirMaiorMenor"
									class="bottonRightCol"
									<ggas:campoSelecionado campo="periodicidadeRetMaiorMenorM"/>
									value="Adicionar" type="button"
									onclick="adicionarPenalidadeRetiradaMaiorMenorModalidade();">
							</c:when>
							<c:otherwise>
								<input name="Button" class="bottonRightCol"
									<ggas:campoSelecionado campo="periodicidadeRetMaiorMenorM"/>
									value="Excluir" type="button"
									onclick="excluirPenalidadeRetiradaMaiorMenorModalidadeAditamento();">
								<input name="Button" id="botaoAdicionarRetirMaiorMenor"
									class="bottonRightCol"
									<ggas:campoSelecionado campo="periodicidadeRetMaiorMenorM"/>
									value="Adicionar" type="button"
									onclick="adicionarPenalidadeRetiradaMaiorMenorModalidadeAditamento();">
							</c:otherwise>
						</c:choose>
					</fieldset>
					
					<br />
					
					<c:set var="i" value="0" />
					<display:table id="penalidadesRetMaiorMenorVO" class="dataTableGGAS dataTableAba" name="listaPenalidadesRetMaiorMenorModalidadeVO" style="width: 428px" sort="list" requestURI="#">
						<display:column style="width: 25px" title="<input type='checkbox' id='checkAllCompromissos'/>">
							<input type="checkbox" class="checkItemCompromissos" name="indicePenalidadeRetiradaMaiorMenorModalidade" value="<c:out value="${i}"/>">
						</display:column>
						<display:column title="Penalidade">
							${penalidadesRetMaiorMenorVO.penalidadeRetiradaMaiorMenor.descricao}
						</display:column>
						<display:column title="Consumo Refer�ncia">
							${penalidadesRetMaiorMenorVO.entidadeConsumoReferenciaRetMaiorMenor.descricao}
						</display:column>
						<display:column title="In�cio Vig�ncia">
							<fmt:formatDate value="${penalidadesRetMaiorMenorVO.dataInicioVigencia}" />
						</display:column>
						<display:column title="Fim Vig�ncia">
							<fmt:formatDate value="${penalidadesRetMaiorMenorVO.dataFimVigencia}" />
						</display:column>
						<c:set var="i" value="${i+1}" />
					</display:table>
					
				</fieldset>		
			</fieldset>
		</div>
		<br/> 
		
		<div class="agrupamento">		
			<fieldset id="conteinerConsumoQPNR" class="colunaEsq">
				<legend>Estimativa de Quantidade a Recuperar</legend>
				<fieldset class="conteinerDados">
					
					<div class="agrupamento">
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMinimoRelacaoQDC" forCampo="percMinDuranteRetiradaQPNR">Percentual m�nimo em rela��o � QDC para recupera��o:</ggas:labelContrato>				
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMinimoRelacaoQDC" name="percMinDuranteRetiradaQPNR" maxlength="6" size="3" <ggas:campoSelecionado campo="percMinDuranteRetiradaQPNR"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percMinDuranteRetiradaQPNR}">
							<label class="rotuloInformativo rotuloHorizontal">%</label><br />
						</div>
					
						<div class="labelCampo">
						 	<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoRelacaoQDC" forCampo="percDuranteRetiradaQPNR">Percentual m�ximo em rela��o � QDC para recupera��o:</ggas:labelContrato>				
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoRelacaoQDC" name="percDuranteRetiradaQPNR" maxlength="6" size="3" <ggas:campoSelecionado campo="percDuranteRetiradaQPNR"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percDuranteRetiradaQPNR}">
							<label class="rotuloInformativo rotuloHorizontal">%</label><br />
						</div>
						
						<div class="labelCampo">	
							<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoTerminoContrato" forCampo="percDuranteRetiradaQPNR">Percentual m�ximo da recupera��o com o t�rmino do contrato:</ggas:labelContrato>									
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoTerminoContrato" name="percFimRetiradaQPNR" maxlength="6" size="3"  <ggas:campoSelecionado campo="percFimRetiradaQPNR"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percFimRetiradaQPNR}">
							<label class="rotuloInformativo rotuloHorizontal">%</label><br /><br />
						</div>

					</div>
					
					<hr class="linhaSeparadora">
					
					<div class="agrupamento">					
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataInicioRecuperacao" forCampo="dataInicioRetiradaQPNR">Data de inicio da recupera��o:</ggas:labelContrato>
							<input class="campoData campo2Linhas" type="text" id="dataInicioRecuperacao" name="dataInicioRetiradaQPNR" maxlength="10" <ggas:campoSelecionado campo="dataInicioRetiradaQPNR"/> value="${contratoVO.dataInicioRetiradaQPNR}" onblur="mostrarAsterisco(this.value,['dataMaximaRecuperacao','percentualMaximoTerminoContrato','percentualMaximoRelacaoQDC']);"><br />
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataMaximaRecuperacao" forCampo="dataFimRetiradaQPNR">Data m�xima para recupera��o:</ggas:labelContrato>
							<input class="campoData campo2Linhas" type="text" id="dataMaximaRecuperacao" name="dataFimRetiradaQPNR" maxlength="10" <ggas:campoSelecionado campo="dataFimRetiradaQPNR"/> value="${contratoVO.dataFimRetiradaQPNR}"><br />
						</div>	

					</div>
					<hr class="linhaSeparadora">
					<div class="labelCampo">
						<ggas:labelContrato styleId="rotuloTempoValidadeRetiradaQPNR" styleClass="rotulo2Linhas" forCampo="tempoValidadeRetiradaQPNR">Tempo de validade para recupera��o da QPNR do ano anterior:</ggas:labelContrato>
						<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="tempoValidadeRetiradaQPNR" name="tempoValidadeRetiradaQPNR" maxlength="2" size="1" <ggas:campoSelecionado campo="tempoValidadeRetiradaQPNR"/> value="${contratoVO.tempoValidadeRetiradaQPNR}" onkeypress="return formatarCampoInteiro(event);">
						<label class="rotuloInformativo3Linhas rotuloHorizontal" for="tempoValidadeRetiradaQPNR">anos</label>
					</div>
				</fieldset>
			</fieldset>
		</div>
		
		<br />
		
		<div class="agrupamento">
			<fieldset id="conteinerConsumoQNR" class="colunaEsq2">
				<legend>Quantidade N�o Retirada</legend>
				<fieldset class="conteinerDados">
					<div class="labelCampo">
					 	<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualQNR" forCampo="percentualQNR">Percentual a considerar para QNR:</ggas:labelContrato>				
						<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualQNR" name="percentualQNR" maxlength="6" size="3" <ggas:campoSelecionado campo="percentualQNR"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percentualQNR}">
						<label class="rotuloInformativo rotuloHorizontal">%</label><br />
					</div>
				</fieldset>
			</fieldset>		
		</div>		
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input name="Button" id="botaoAdicionar" class="bottonRightCol2" value="Adicionar" type="button" <ggas:campoSelecionado campo="modalidade"/> onclick="adicionarModalidadeContrato(true);" <c:if test="${empty contratoVO.modalidade || contratoVO.modalidade eq '-1'}">disabled="disabled"</c:if>>
	    	<input name="botaoLimparContratoResponsabilidade" class="bottonRightCol bottonRightColUltimo" <ggas:campoSelecionado campo="modalidade"/> value="Limpar" type="button" onclick="limparCamposAbaModalidades();">
		</fieldset>
	</fieldset>			
	
	<fieldset class="conteinerBotoesAba">
	   	<input class="bottonRightCol" name="botaoAlterarContratoResponsabilidade" <ggas:campoSelecionado campo="modalidade"/> value="Alterar" <c:if test="${empty (contratoVO.idModalidadeContrato) || contratoVO.idModalidadeContrato <= 0}">disabled="disabled"</c:if>  onclick="alterarModalidadeCadastrada();" type="button">
	   	<input class="bottonRightCol bottonRightColUltimo" name="botaoRemoverContratoResponsabilidade" <ggas:campoSelecionado campo="modalidade"/> value="Excluir" onclick="excluirModalidadeContrato();" type="button">
	</fieldset>
	
	<input name="idModalidadeContrato" type="hidden" id="idModalidadeContrato" value="${contratoVO.idModalidadeContrato}"/>
	<display:table class="dataTableGGAS dataTableAba" name="listaModalidadeConsumoFaturamentoVO" id="modalidadeConsumoFaturamentoVO" sort="list" requestURI="#" decorator="br.com.ggas.web.contrato.contrato.decorator.ModalidadeCadastradaDecorator">
		<display:column style="width: 25px" title="<input type='checkbox' id='checkAllModalidades'/>">
			<input type="checkbox" class="checkItemModalidade" name="idsModalidadeContrato" value="${modalidadeConsumoFaturamentoVO.modalidade.chavePrimaria}" <c:if test="${modalidadeConsumoFaturamentoVO.modalidade.chavePrimaria == contratoVO.idModalidadeContrato}">checked="checked"</c:if>>
		</display:column>
		<display:column title="Modalidades Cadastradas">
			<a href="javascript:exibirAlteracaoModalidadeCadastrada('${modalidadeConsumoFaturamentoVO.modalidade.chavePrimaria}');">${modalidadeConsumoFaturamentoVO.modalidade.descricao}</a>
		</display:column> 			
	</display:table>

	
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Contratos.</p>
</fieldset>