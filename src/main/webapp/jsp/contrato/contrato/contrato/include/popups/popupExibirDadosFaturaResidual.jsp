<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css" integrity="sha512-TQQ3J4WkE/rwojNFo6OJdyu6G8Xe9z8rMrlF9y7xpFbQfW5g8aSWcygCQ4vqRiJqFsDsE1T6MoAOMJkFXlrI9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.pt-BR.min.js" integrity="sha512-mVkLPLQVfOWLRlC2ZJuyX5+0XrTlbW2cyAwyqgPkLGxhoaHNSWesYMlcUjX8X+k45YB8q90s88O7sos86636NQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script language="javascript">
	
	$(document).ready(function(){

		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});
		
		
		// Datepicker
		$('.dataLeituras').inputmask("99/99/9999",{placeholder:"_"});
        $("input.botaoAdicionar").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/check2.gif) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
		
		
	});

	function salvarLeituraData(idPontoConsumo) {
		payload = {
				idPontoConsumo: parseInt(idPontoConsumo),
				dataLeitura: $('#dataLeitura_'+idPontoConsumo).val(),
				valorLeitura:  $('#valorLeitura_'+idPontoConsumo).val(),
				situacaoContrato: "C",
			}

		if(payload.dataLeitura == "" || payload.valorLeitura == "") {
			alert("Data ou Valor Invalido");
			return false;
		}
		
		$.ajax({
			type: 'POST',
			url: '/ggas/salvarDataLeitura',
			data: JSON.stringify(payload),
			success: function(data) {
				try {
					if (data.erro) {
				        $("input#botaoAdicionar_"+idPontoConsumo).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/circle_red.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
						return false;
					}
			        $("input#botaoAdicionar_"+idPontoConsumo).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/circle_green.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			        $("input#botaoAdicionar_"+idPontoConsumo).attr("title", "Salvo");

				} catch (e) {
			        $("input#botaoAdicionar_"+idPontoConsumo).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/circle_red.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
				}
			},
			contentType: 'application/json'
		});
		return true;
		
	}

	function salvar() {

		if(verificaDadosPreenchidoFaturaResidual()) {
			if(confirm('H� pontos de consumo sem data e leitura salvas para a Fatura Residual, deseja continuar?')){
				window.opener.adicionarFuncionarioInput($("#funcionarioPopup").val());
				window.opener.submeter('contratoForm', 'incluirContrato');
				window.close();
			}
		} else {
			window.opener.adicionarFuncionarioInput($("#funcionarioPopup").val());
			window.opener.submeter('contratoForm', 'incluirContrato');
			window.close();
		}
	}


	function verificaDadosPreenchidoFaturaResidual(){
		var retorno = false;
		$('#pontoConsumo tbody tr').each(function() {
			if($(this).find(".botaoAdicionar").attr('title') == "Salvar"){
				retorno = true;
			}
		});	
		return retorno;
	}
	
</script>


<h1 class="tituloInternoPopup">Incluir Dados Reativa��o Medidor</h1>
<p class="orientacaoInicialPopup">Para incluir os dados da reativa��o do medidor, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para incluir os dados e salvar o contrato</p>


	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Funcion�rio:</label>
	<select class="campoSelect" id="funcionarioPopup" name="funcionarioPopup" style="width: 200px">
		<c:forEach items="${listaFuncionario}" var="funcionarioPopup">
			<option value="<c:out value="${funcionarioPopup.chavePrimaria}"/>" <c:if test="${funcionarioSelecionado.chavePrimaria == funcionarioPopup.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${funcionarioPopup.nome}"/>
			</option>		
		</c:forEach>
	</select><br/><br/>
			
	<c:if test="${listaPontoConsumoResidual ne null}">
		<fieldset class="conteinerPesquisarIncluirAlterar">
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumoResidual" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="" requestURI="#">
		        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: center">
		         	<c:out value="${pontoConsumo.descricao}"/>
		        </display:column>
		        <display:column title="Segmento" style="width: 130px">
		       		<c:out value="${pontoConsumo.segmento.descricao}"/>
		        </display:column>
		        <display:column title="Situa��o" style="width: 160px">
		        	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
		        </display:column>
<%-- 		        <display:column  title="Fatura" style="width: 50px" >
			    	<c:if test="${pontoConsumo.idFatura ne null}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if>
			    </display:column>
			    <display:column title="2� Via" style="width: 50px">
		        	<c:if test="${pontoConsumo.idFatura ne null}">
						<a href='javascript:imprimirFaturaEncerramento(<c:out value='${pontoConsumo.idFatura}'/>);'>
				    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir" title="Imprimir" border="0">
				    	</a>
			    	</c:if>					
				</display:column> --%>
			    <display:column title="Data Leitura" style="width: 150px;">
					<input type="text" style="margin-left:20px;" class="campoTexto campoHorizontal bootstrapDP dataLeituras" id="dataLeitura_${pontoConsumo.chavePrimaria}" name="dataLeitura_${pontoConsumo.chavePrimaria}" maxlength="10" size="8" value="${dataLeitura}">			
				</display:column>
			    <display:column title="Leitura" style="width: 100px;">
					<input type="text" class="campoTexto campoHorizontal valorLeituras" id="valorLeitura_${pontoConsumo.chavePrimaria}" name="valorLeitura_${pontoConsumo.chavePrimaria}" maxlength="10" size="8" value="${leitura}" onkeypress="return formatarCampoInteiro(event)">				
				</display:column>
				<display:column title="Salvar Data e Leitura" style="width: 50px;">
					<input type="button" class="botaoAdicionar" id="botaoAdicionar_${pontoConsumo.chavePrimaria}" onClick="return salvarLeituraData(${pontoConsumo.chavePrimaria});" title="Salvar" />
				</display:column>								
		    </display:table>
		</fieldset>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol2" type="button" id="button" value="Salvar" onClick="salvar();">
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.opener.esconderIndicador();window.close();">
	</fieldset>
	
	
	