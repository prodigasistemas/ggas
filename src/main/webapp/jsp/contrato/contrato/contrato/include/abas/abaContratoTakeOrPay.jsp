<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<script type='text/javascript' src='
<c:out value='${pageContext.request.contextPath}'/>
/dwr/engine.js'> </script>
<script type='text/javascript' src='
<c:out value='${pageContext.request.contextPath}'/>
/dwr/util.js'> </script>
<script type='text/javascript' src='
<c:out value='${pageContext.request.contextPath}'/>
/dwr/interface/AjaxService.js'></script>


<script type="text/javascript">
$( document ).ready(function() {

	manipularFormaApuracao($('#consideraParadaProgramadaCSim'), 
			   $('#consideraParadaProgramadaCNao'), 
			   $('#apuracaoParadaProgramadaC'), 
			   $('#spanApuracaoParadaProgramada'));

	manipularFormaApuracao($('#consideraFalhaFornecimentoCSim'), 
			   $('#consideraFalhaFornecimentoCNao'), 
			   $('#apuracaoFalhaFornecimentoC'), 
			   $('#spanApuracaoFalhaFornecimento'));

	manipularFormaApuracao($('#consideraCasoFortuitoCSim'), 
			   $('#consideraCasoFortuitoCNao'), 
			   $('#apuracaoCasoFortuitoC'), 
			   $('#spanApuracaoCasoFortuito'));
});
function manipularFormaApuracao(elementRadioSim, elementRadioNao, element, spanElement){
	$(elementRadioSim).click(function(){
		habilitaETornaCampoObrigatorio($(element), $(spanElement))
	});
	$(elementRadioNao).click(function(){
		desabilitarERetirarObrigatoriedade($(element), $(spanElement))
	});
	
	if($(elementRadioSim).is(':checked')){
		habilitaETornaCampoObrigatorio($(element),$(spanElement));
	}
	
	if($(elementRadioNao).is(':checked')){
		desabilitarERetirarObrigatoriedade($(element), $(spanElement))
	}
}

function habilitaETornaCampoObrigatorio(element, spanElement){
	$(element).prop("disabled", false);
	$(spanElement).text('*');
}
function desabilitarERetirarObrigatoriedade(element, spanElement){
	$(element).prop("disabled", true);
	$(element).val(-1);
	$(spanElement).text("");
}


	function editarCompromissoTOP() {
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var selecao = verificarSelecaoCompromissoTOPAbaGeral(true, false);
		if (selecao == true) {
			submeter('contratoForm', 'exibirEditarCompromissoTOP');
		}	
	}
</script>

<div id="abaTakeOrPay">

	<div id="menuAbasInternas">
		<fieldset id="abasInternas">
			<ul>
				<li class="abaInterna"><a href="#qdc">QDC</a></li>
				<li class="abaInterna"><a href="#take_or_pay">Take or Pay</a></li>
				<li class="abaInterna"><a href="#recuperacao">Recupera��o</a></li>
			</ul>
		
			<div id="take_or_pay">
			   <fieldset id="conteinerTakeOrPay" >
			       <legend>Take or Pay</legend>
			       <div class="conteinerDados agrupamento">
			                <div class="labelCampo">
			                   <ggas:labelContrato forId="tipoCompromisso" forCampo="periodicidadeTakeOrPayC">Periodicidade ToP:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoPeriodicidadeTakeOrPayC }">
			                   			<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeTakeOrPay">
		                      				<c:if test='${contratoVO.periodicidadeTakeOrPayC == periodicidadeTakeOrPay.chavePrimaria}'>
		                      					<input class="campoValorFixo" type="text" value="${periodicidadeTakeOrPay.descricao}" size="15" readonly="readonly" />
												<input type="hidden" name="periodicidadeTakeOrPayC" value="${periodicidadeTakeOrPay.chavePrimaria}" />
		                      				</c:if>
			                   			</c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" name="periodicidadeTakeOrPayC" id="periodicidadeTakeOrPayC" <ggas:campoSelecionado campo="periodicidadeTakeOrPayC"/>
					                   		onchange="mostrarAsterisco(this.value,['referenciaQFParadaProgramadaC','mensalConsumoReferenciaInferior','margemVariacaoTakeOrPayC','dataInicioVigenciaC','dataFimVigenciaC','precoCobrancaC','tipoApuracaoC','consideraParadaProgramadaC','consideraParadaNaoProgramadaC','consideraFalhaFornecimentoC','consideraCasoFortuitoC','consideraCasoFortuitoC','recuperavelC','percentualNaoRecuperavelC','apuracaoParadaNaoProgramadaC']);">
					                   		<option value="-1">Selecione</option>
					                   			<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeTakeOrPay">
					                      			<option value="<c:out value="${periodicidadeTakeOrPay.chavePrimaria}"/>" 
					                      				<c:if test='${contratoVO.periodicidadeTakeOrPayC == periodicidadeTakeOrPay.chavePrimaria}'>selected="selected"</c:if>>
					                      				<c:out value="${periodicidadeTakeOrPay.descricao}"/>
					                      			</option>       
					                   			</c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleClass="rotuloVertical rotulo2Linhas" forCampo="referenciaQFParadaProgramadaC">Refer�ncia Quantidade Faltante Parada Programada:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoReferenciaQFParadaProgramadaC}">
				                   		<c:forEach items="${listaReferenciaQFParadaProgramada}" var="referenciaQFParadaProgramada">
			                      			<c:if test='${contratoVO.referenciaQFParadaProgramadaC == referenciaQFParadaProgramada.chavePrimaria}'>
		                      					<input class="campoValorFixo" type="text" value="${referenciaQFParadaProgramada.descricao}" size="30" readonly="readonly" />
												<input type="hidden" name="referenciaQFParadaProgramadaC" value="${referenciaQFParadaProgramada.chavePrimaria}" />
			                      			</c:if>
				                   		</c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" name="referenciaQFParadaProgramadaC" id="tipoReferenciaQFParadaProgramada" <ggas:campoSelecionado campo="referenciaQFParadaProgramadaC"/>>
					                   		<option value="-1">Selecione</option>
					                   		<c:forEach items="${listaReferenciaQFParadaProgramada}" var="referenciaQFParadaProgramada">
					                      		<option value="<c:out value="${referenciaQFParadaProgramada.chavePrimaria}"/>" 
					                      			<c:if test='${contratoVO.referenciaQFParadaProgramadaC == referenciaQFParadaProgramada.chavePrimaria}'>selected="selected"</c:if>>
					                      			<c:out value="${referenciaQFParadaProgramada.descricao}"/>
				                      			</option>       
					                   		</c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="mensalConsumoReferenciaInferior" forCampo="consumoReferenciaTakeOrPayC">Consumo Referencial para ToP:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoConsumoReferenciaTakeOrPayC}">
				                   		<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaTakeOrPay">
			                      			<c:if test='${contratoVO.consumoReferenciaTakeOrPayC == consumoReferenciaTakeOrPay.chavePrimaria}'>
		                      					<input class="campoValorFixo" type="text" value="${consumoReferenciaTakeOrPay.descricao}" size="30" readonly="readonly" />
												<input type="hidden" name="consumoReferenciaTakeOrPayC" value="${consumoReferenciaTakeOrPay.chavePrimaria}" />			                      				
			                      			</c:if>
				                   		</c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" id="mensalConsumoReferenciaInferior" name="consumoReferenciaTakeOrPayC" 
					                   		<ggas:campoSelecionado campo="consumoReferenciaTakeOrPayC"/>>
					                   		<option value="-1">Selecione</option>
					                   		<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaTakeOrPay">
					                      		<option value="<c:out value="${consumoReferenciaTakeOrPay.chavePrimaria}"/>" 
					                      			<c:if test='${contratoVO.consumoReferenciaTakeOrPayC == consumoReferenciaTakeOrPay.chavePrimaria}'>selected="selected"</c:if>>
				                      				<c:out value="${consumoReferenciaTakeOrPay.descricao}"/>
					                      		</option>       
					                   		</c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleId="rotuloMensalVariacaoInferior" styleClass="rotuloVertical rotulo2Linhas" forId="margemVariacaoTakeOrPayC" forCampo="margemVariacaoTakeOrPayC">Compromisso de retirada ToP:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoMargemVariacaoTakeOrPayC}">
                     					<input class="campoValorFixo" type="text" value="${contratoVO.margemVariacaoTakeOrPayC}%" size="7" readonly="readonly" />
										<input type="hidden" name="margemVariacaoTakeOrPayC" value="${contratoVO.margemVariacaoTakeOrPayC}" />			                      				
									</c:when>
									<c:otherwise>
					                   <input class="campoTexto campoHorizontal campo2Linhas" type="text" id="margemVariacaoTakeOrPayC" name="margemVariacaoTakeOrPayC" size="3" maxlength="6" <ggas:campoSelecionado campo="margemVariacaoTakeOrPayC"/>
					                   		onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" name="margemVariacaoTakeOrPayC" value="${contratoVO.margemVariacaoTakeOrPayC}">
					                   <label class="rotuloHorizontal rotuloInformativo">%</label><br class="quebraLinha2" />
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato forCampo="dataInicioVigenciaC">Data In�cio Vig�ncia:</ggas:labelContrato>
			                   <input class="campoData" type="text" id="dataInicioVigenciaC" name="dataInicioVigenciaC" maxlength="10" 
			                   <ggas:campoSelecionado campo="dataInicioVigenciaC"/>
			                   value="${contratoVO.dataInicioVigenciaC}"><br class="quebraLinha2" />
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato forCampo="dataFimVigenciaC">Data Fim Vig�ncia:</ggas:labelContrato>
			                   <input class="campoData" type="text" id="dataFimVigenciaC" name="dataFimVigenciaC" maxlength="10" 
			                   <ggas:campoSelecionado campo="dataFimVigenciaC"/>
			                   value="${contratoVO.dataFimVigenciaC}"><br class="quebraLinha2" />
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleId="tipoReferenciaQFParadaProgramada" styleClass="rotuloVertical rotulo2Linhas" forCampo="precoCobrancaC">Tabela de Pre�os para Cobran�a: </ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoPrecoCobrancaC}">
				                       <c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
				                          <c:if test='${contratoVO.precoCobrancaC == precoCobranca.chavePrimaria}'>
				                          		<input class="campoValorFixo" type="text" value="${precoCobranca.descricao}" size="60" readonly="readonly" />
												<input type="hidden" name="precoCobrancaC" value="${precoCobranca.chavePrimaria}" />	
				                          </c:if>
				                       </c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" name="precoCobrancaC" id="precoCobrancaC"  <ggas:campoSelecionado campo="precoCobrancaC"/>  >
					                   	<option value="-1">Selecione</option>
					                       <c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
					                          <option value="<c:out value="${precoCobranca.chavePrimaria}"/>" 
					                          		<c:if test='${contratoVO.precoCobrancaC == precoCobranca.chavePrimaria}'>selected="selected"</c:if>>
					                                <c:out value="${precoCobranca.descricao}"/>
					                          </option>       
					                       </c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="tipoApuracaoC" forCampo="tipoApuracaoC">Forma de C�lculo para Cobran�a e Recupera��o: </ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoTipoApuracaoC }">
				                   		<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
			                      			<c:if test='${contratoVO.tipoApuracaoC == tipoApuracao.chavePrimaria}'>
				                          		<input class="campoValorFixo" type="text" value="${tipoApuracao.descricao}" size="60" readonly="readonly" />
												<input type="hidden" name="tipoApuracaoC" value="${tipoApuracao.chavePrimaria}" />	
			                      			</c:if>
				                   		</c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" id="tipoApuracaoC" name="tipoApuracaoC" <ggas:campoSelecionado campo="tipoApuracaoC"/>>
					                   	<option value="-1">Selecione</option>
					                   		<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
					                      		<option value="<c:out value="${tipoApuracao.chavePrimaria}"/>" 
					                      			<c:if test='${contratoVO.tipoApuracaoC == tipoApuracao.chavePrimaria}'>selected="selected"</c:if>>
					                      		<c:out value="${tipoApuracao.descricao}"/>
					                      		</option>       
					                   		</c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato forCampo="consideraParadaProgramadaC" styleClass="rotuloVertical rotulo2Linhas">Considerar Paradas Programadas:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoConsideraParadaProgramadaC}">
										<c:choose>
											<c:when test="${contratoVO.consideraParadaProgramadaC}">
												<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
											</c:when>
											<c:otherwise>
												<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
											</c:otherwise>
										</c:choose>
										<input type="hidden" name="consideraParadaProgramadaC" value="${contratoVO.consideraParadaProgramadaC}" />
									</c:when>
									<c:otherwise>
					                   <input class="campoRadio" type="radio" name="consideraParadaProgramadaC" id="consideraParadaProgramadaCSim" value="true" <ggas:campoSelecionado campo="consideraParadaProgramadaC"/>
					                   		<c:if test="${contratoVO.consideraParadaProgramadaC == true}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="consideraParadaProgramadaCSim">Sim</label>
					                   <input class="campoRadio" type="radio" name="consideraParadaProgramadaC" id="consideraParadaProgramadaCNao" value="false" <ggas:campoSelecionado campo="consideraParadaProgramadaC"/>
					                   		<c:if test="${contratoVO.consideraParadaProgramadaC == false}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="consideraParadaProgramadaCNao">N�o</label>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato forCampo="consideraFalhaFornecimentoC" styleClass="rotuloVertical rotulo2Linhas">Considerar Falha de Fornecimento:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoConsideraFalhaFornecimentoC}">
										<c:choose>
											<c:when test="${contratoVO.consideraFalhaFornecimentoC}">
												<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
											</c:when>
											<c:otherwise>
												<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
											</c:otherwise>
										</c:choose>
										<input type="hidden" name="consideraFalhaFornecimentoC" value="${contratoVO.consideraFalhaFornecimentoC}" />
									</c:when>
									<c:otherwise>
					                   <input class="campoRadio" type="radio" name="consideraFalhaFornecimentoC" id="consideraFalhaFornecimentoCSim" value="true" <ggas:campoSelecionado campo="consideraFalhaFornecimentoC"/>
					                   		<c:if test="${contratoVO.consideraFalhaFornecimentoC == true}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="consideraFalhaFornecimentoCSim">Sim</label>
					                   <input class="campoRadio" type="radio" name="consideraFalhaFornecimentoC" id="consideraFalhaFornecimentoCNao" value="false" <ggas:campoSelecionado campo="consideraFalhaFornecimentoC"/>
					                   		<c:if test="${contratoVO.consideraFalhaFornecimentoC == false}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="consideraFalhaFornecimentoCNao">N�o</label>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato forCampo="consideraCasoFortuitoC" styleClass="rotuloVertical rotulo2Linhas">Considerar Casos Fortuitos:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoConsideraCasoFortuitoC}">
										<c:choose>
											<c:when test="${contratoVO.consideraCasoFortuitoC}">
												<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
											</c:when>
											<c:otherwise>
												<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
											</c:otherwise>
										</c:choose>
										<input type="hidden" name="consideraCasoFortuitoC" value="${contratoVO.consideraCasoFortuitoC}" />
									</c:when>
									<c:otherwise>
					                   <input class="campoRadio" type="radio" name="consideraCasoFortuitoC" id="consideraCasoFortuitoCSim" value="true" <ggas:campoSelecionado campo="consideraCasoFortuitoC"/>
					                   		<c:if test="${contratoVO.consideraCasoFortuitoC == true}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="consideraCasoFortuitoCSim">Sim</label>
					                   <input class="campoRadio" type="radio" name="consideraCasoFortuitoC" id="consideraCasoFortuitoCNao" value="false" <ggas:campoSelecionado campo="consideraCasoFortuitoC"/>
					                   		<c:if test="${contratoVO.consideraCasoFortuitoC == false}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="consideraCasoFortuitoCNao">N�o</label>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato forCampo="recuperavelC" styleClass="rotuloVertical rotulo2Linhas">Recuper�vel:</ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoRecuperavelC}">
										<c:choose>
											<c:when test="${contratoVO.recuperavelC}">
												<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
											</c:when>
											<c:otherwise>
												<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
											</c:otherwise>
										</c:choose>
										<input type="hidden" name="recuperavelC" value="${contratoVO.recuperavelC}" />
									</c:when>
									<c:otherwise>
					                   <input class="campoRadio" type="radio" name="recuperavelC" id="TOPrecuperacaoAutoTakeOrPaySim" value="true" <ggas:campoSelecionado campo="recuperavelC"/>
					                   		<c:if test="${contratoVO.recuperavelC == true}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="recuperavelCSim">Sim</label>
					                   <input class="campoRadio" type="radio" name="recuperavelC" id="TOPrecuperacaoAutoTakeOrPayNao" value="false" <ggas:campoSelecionado campo="recuperavelC"/>
					                   		<c:if test="${contratoVO.recuperavelC == false}">checked="checked"</c:if>>
					                   <label class="rotuloRadio" for="recuperavelCNao">N�o</label>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                    <ggas:labelContrato styleId="rotuloMensalVariacaoInferior" styleClass="rotuloVertical rotulo2Linhas" forId="percentualNaoRecuperavelC" forCampo="percentualNaoRecuperavelC">Percentual N�o Recuper�vel:</ggas:labelContrato>
			                    <c:choose>
									<c:when test="${contratoVO.valorFixoPercentualNaoRecuperavelC}">
										<input class="campoValorFixo" type="text" value="${contratoVO.percentualNaoRecuperavelC}%" size="7" readonly="readonly" />
										<input type="hidden" name="percentualNaoRecuperavelC" value="${contratoVO.percentualNaoRecuperavelC}" />
									</c:when>
									<c:otherwise>
					                   <input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualNaoRecuperavelC" name="percentualNaoRecuperavelC" size="3" maxlength="6" <ggas:campoSelecionado campo="percentualNaoRecuperavelC"/>
					                   		onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" name="percentualNaoRecuperavelC" value="${contratoVO.percentualNaoRecuperavelC}">
					                   <label class="rotuloHorizontal rotuloInformativo">%</label>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoParadaProgramadaC" forCampo="apuracaoParadaProgramadaC">
			                  		<span id='spanApuracaoParadaProgramada' class='campoObrigatorioSimbolo2'></span>Forma de Apura��o do Volume de Parada Programada:
			                   </ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoApuracaoParadaProgramadaC}">
					                   	<c:forEach items="${listaApuracaoParadaProgramada}" var="apuracaoParadaProgramada">
				                      		<c:if test='${contratoVO.apuracaoParadaProgramadaC == apuracaoParadaProgramada.chavePrimaria}'>
				                      			<input class="campoValorFixo" type="text" value="${apuracaoParadaProgramada.descricao}" size="30" readonly="readonly" />
												<input type="hidden" name="apuracaoParadaProgramadaC" value="${apuracaoParadaProgramada.chavePrimaria}" />
				                      		</c:if>
					                   	</c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" id="apuracaoParadaProgramadaC" name="apuracaoParadaProgramadaC" <ggas:campoSelecionado campo="apuracaoParadaProgramadaC"/>>
					                   <option value="-1">Selecione</option>
					                   	<c:forEach items="${listaApuracaoParadaProgramada}" var="apuracaoParadaProgramada">
					                      	<option value="<c:out value="${apuracaoParadaProgramada.chavePrimaria}"/>" 
					                      		<c:if test='${contratoVO.apuracaoParadaProgramadaC == apuracaoParadaProgramada.chavePrimaria}'>selected="selected"</c:if>>
					                      		<c:out value="${apuracaoParadaProgramada.descricao}"/>
					                    	  </option>       
					                   	</c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
			                </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoParadaNaoProgramadaC" forCampo="apuracaoParadaNaoProgramadaC">Forma de Apura��o do Volume de Parada N�o Programada:</ggas:labelContrato>
			                   <select class="campoSelect campo2Linhas campoHorizontal" id="apuracaoParadaNaoProgramadaC" name="apuracaoParadaNaoProgramadaC" 
			                   <ggas:campoSelecionado campo="apuracaoParadaNaoProgramadaC"/>
			                   >
			                   <option value="-1">Selecione</option>
			                   <c:forEach items="${listaApuracaoParadaNaoProgramada}" var="apuracaoParadaNaoProgramada">
			                      <option value="<c:out value="${apuracaoParadaNaoProgramada.chavePrimaria}"/>" 
			                      <c:if test='${contratoVO.apuracaoParadaNaoProgramada == apuracaoParadaNaoProgramada.chavePrimaria}'>selected="selected"</c:if>
			                      >
			                      <c:out value="${apuracaoParadaNaoProgramada.descricao}"/>
			                      </option>       
			                   </c:forEach>
			                   </select>
			                </div>
			                <div class="labelCampo">
  			                    <ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoFalhaFornecimentoC" forCampo="apuracaoFalhaFornecimentoC">
  			                    	<span id='spanApuracaoFalhaFornecimento' class='campoObrigatorioSimbolo2'></span>Forma de Apura��o do Volume de Falha de Fornecimento:
  			                    </ggas:labelContrato>
				                <c:choose>
									<c:when test="${contratoVO.valorFixoApuracaoFalhaFornecimentoC}">
				                   		<c:forEach items="${listaApuracaoFalhaFornecimento}" var="apuracaoFalhaFornecimento">
			                      			<c:if test='${contratoVO.apuracaoFalhaFornecimentoC == apuracaoFalhaFornecimento.chavePrimaria}'>
			                      				<input class="campoValorFixo" type="text" value="${apuracaoFalhaFornecimento.descricao}" size="30" readonly="readonly" />
												<input type="hidden" name="apuracaoFalhaFornecimentoC" value="${apuracaoFalhaFornecimento.chavePrimaria}" />
			                      			</c:if>
				                   		</c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" id="apuracaoFalhaFornecimentoC" name="apuracaoFalhaFornecimentoC" <ggas:campoSelecionado campo="apuracaoFalhaFornecimentoC"/>>
					                   	<option value="-1">Selecione</option>
					                   		<c:forEach items="${listaApuracaoFalhaFornecimento}" var="apuracaoFalhaFornecimento">
					                      		<option value="<c:out value="${apuracaoFalhaFornecimento.chavePrimaria}"/>" 
					                      			<c:if test='${contratoVO.apuracaoFalhaFornecimentoC == apuracaoFalhaFornecimento.chavePrimaria}'>selected="selected"</c:if>>
					                      			<c:out value="${apuracaoFalhaFornecimento.descricao}"/>
					                      		</option>       
					                   		</c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
		                   </div>
			                <div class="labelCampo">
			                   <ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoCasoFortuitoC" forCampo="apuracaoCasoFortuitoC">
									<span id='spanApuracaoCasoFortuito' class='campoObrigatorioSimbolo2'></span>Forma de Apura��o do Volume de Caso Fortuito ou For�a Maior:
							   </ggas:labelContrato>
			                   <c:choose>
									<c:when test="${contratoVO.valorFixoApuracaoCasoFortuitoC}">
					                   	<c:forEach items="${listaApuracaoCasoFortuito}" var="apuracaoCasoFortuito">
					                      <c:if test='${contratoVO.apuracaoCasoFortuitoC == apuracaoCasoFortuito.chavePrimaria}'>
					                      		<input class="campoValorFixo" type="text" value="${apuracaoCasoFortuito.descricao}" size="30" readonly="readonly" />
												<input type="hidden" name="apuracaoCasoFortuitoC" value="${apuracaoCasoFortuito.chavePrimaria}" />
					                      </c:if>
					                   	</c:forEach>
									</c:when>
									<c:otherwise>
					                   <select class="campoSelect campo2Linhas campoHorizontal" id="apuracaoCasoFortuitoC" name="apuracaoCasoFortuitoC" <ggas:campoSelecionado campo="apuracaoCasoFortuitoC"/>>
					                   <option value="-1">Selecione</option>
					                   	<c:forEach items="${listaApuracaoCasoFortuito}" var="apuracaoCasoFortuito">
					                      <option value="<c:out value="${apuracaoCasoFortuito.chavePrimaria}"/>" 
						                      <c:if test='${contratoVO.apuracaoCasoFortuitoC == apuracaoCasoFortuito.chavePrimaria}'>selected="selected"</c:if>>
						                      <c:out value="${apuracaoCasoFortuito.descricao}"/>
					                      </option>       
					                   	</c:forEach>
					                   </select>
									</c:otherwise>
								</c:choose>
			                </div>
			        </div>

					<br class="quebraLinha2" />
			
			        <fieldset class="conteinerBloco">
			           <input name="Button" class="bottonRightCol" 
			           <ggas:campoSelecionado campo="periodicidadeTakeOrPayC"/>
			           value="Excluir" type="button" onclick="excluirCompromissoTOPAbaGeral();">
			           <input name="Button" id="botaoAdicionarTakeOrPay" class="bottonRightCol" 
			           <ggas:campoSelecionado campo="periodicidadeTakeOrPayC"/>
			           value="Adicionar / Salvar" type="button" onclick="adicionarCompromissoTOPAbaGeral();">
			           <input name="Button" id="botaoEditarTakeOrPay" class="bottonRightCol" 
			           <ggas:campoSelecionado campo="periodicidadeTakeOrPayC"/>
			           value="Editar" type="button" onclick="editarCompromissoTOP();">
			        </fieldset>
			
			      <fieldset class="conteinerBloco" style="width: 100%">
			           <c:set var="i" value="0" />
			           <display:table id="compromissosTakeOrPayVO" class="dataTableGGAS dataTableAba" name="sessionScope.listaCompromissosTakeOrPayCVO" style="width: 100%" sort="list" requestURI="#">
			              <display:column style="width: 25px" title="<input type='checkbox' id='checkCompromissosTakeOrPay' class='checkAllCompromissos'/>">
			                 <input type="checkbox" class="checkCompromissosTakeOrPay" name="indicePeriodicidadeTakeOrPay" value="<c:out value="${i}"/>"
			                 	<c:if test='${indicePeriodicidadeTakeOrPay == i}'> checked='true'</c:if>
			                 >
			              </display:column>
			              <display:column title="Periodicidade ToP">
			                 ${compromissosTakeOrPayVO.periodicidadeTakeOrPay.descricao}
			              </display:column>
			              <display:column title="Compromisso de Retirada">
			                 <fmt:formatNumber type="percent" minFractionDigits="2" value="${compromissosTakeOrPayVO.margemVariacaoTakeOrPay}"></fmt:formatNumber>
			              </display:column>
			              <display:column title="In�cio Vig�ncia">
			                 <fmt:formatDate value="${compromissosTakeOrPayVO.dataInicioVigencia}" pattern="dd/MM/yyyy"/>
			              </display:column>
			              <display:column title="Fim Vig�ncia">
			                 <fmt:formatDate value="${compromissosTakeOrPayVO.dataFimVigencia}" pattern="dd/MM/yyyy"/>
			              </display:column>
			              <display:column title="Tabela de Pre�o">
			              	${compromissosTakeOrPayVO.precoCobranca.descricao}
			              </display:column>
			              <display:column title="Forma de C�lculo">
			              	${compromissosTakeOrPayVO.tipoApuracao.descricao}
			              </display:column>
			              <display:column title="Recuper�vel">
			              	<c:choose>
			              		<c:when test="${compromissosTakeOrPayVO.recuperavel }">Sim</c:when>
			              		<c:otherwise>N�o</c:otherwise>
		              		</c:choose>
			              </display:column>
			              <display:column title="% N�o Recuper�vel">
			              	<fmt:formatNumber
                                 value="${(compromissosTakeOrPayVO.percentualNaoRecuperavel * 100)}" /> %
			              </display:column>
			              <c:set var="i" value="${i+1}" />
			           </display:table>
			      </fieldset>
			
			        <div class="agrupamento">
			            <div class="labelCampo">
			               <ggas:labelContrato forCampo="recuperacaoAutoTakeOrPayC" styleClass="rotuloVertical rotulo2Linhas">Recupera��o autom�tica?</ggas:labelContrato>
			               <input class="campoRadio" type="radio" name="recuperacaoAutoTakeOrPayC" id="recuperacaoAutoTakeOrPayCSim" value="true" 
			               <ggas:campoSelecionado campo="recuperacaoAutoTakeOrPayC"/>
			               <c:if test="${contratoVO.recuperacaoAutoTakeOrPayC == true}">checked="checked"</c:if>
			               >
			               <label class="rotuloRadio" for="recuperacaoAutoTakeOrPayCSim">Sim</label>
			               <input class="campoRadio" type="radio" name=recuperacaoAutoTakeOrPayC id="recuperacaoAutoTakeOrPayCNao" value="false" 
			               <ggas:campoSelecionado campo="recuperacaoAutoTakeOrPayC"/>
			               <c:if test="${contratoVO.recuperacaoAutoTakeOrPayC == false}">checked="checked"</c:if>
			               >
			               <label class="rotuloRadio" for="recuperacaoAutoTakeOrPayCNao">N�o</label>
			            </div>
			        </div>
			   </fieldset>
		   </div>
		   
				<c:set var="dataQDC" value="QDCContrato" />
				<c:set var="volumeQDC" value="QDCContrato" />
				<c:set var="modalidade" value="false" />
				<c:set var="idConteinerQDC" value="conteinerQDCContrato" />
				<c:set var="nomeComponente" value="QDC do Contrato" />
		
		   	<div id="qdc">
				<%@ include file="/jsp/contrato/contrato/contrato/include/dadosQDCContrato.jsp" %>
		   	</div>
			
			
			<div id="recuperacao">
				<fieldset id="estimativaQtdeRecuperar">
					<legend>Estimativa de Quantidade a Recuperar</legend>
					<div class="conteinerDados agrupamento">
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMinimoRelacaoQDCC" forCampo="percMinDuranteRetiradaQPNRC">Percentual m�nimo em rela��o � QDC para recupera��o:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoPercMinDuranteRetiradaQPNRC}">
									<input class="campoValorFixo" type="text" value="${contratoVO.percMinDuranteRetiradaQPNRC}%" size="15" readonly="readonly" />
									<input type="hidden" name="percMinDuranteRetiradaQPNRC" value="${contratoVO.percMinDuranteRetiradaQPNRC}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMinimoRelacaoQDCC" name="percMinDuranteRetiradaQPNRC" maxlength="6" size="3" 
										<ggas:campoSelecionado campo="percMinDuranteRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  
										value="${contratoVO.percMinDuranteRetiradaQPNRC}">
									<label class="rotuloInformativo rotuloHorizontal">%</label>
								</c:otherwise>
							</c:choose>				
						</div>

						<br />

						<div class="labelCampo">
						 	<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoRelacaoQDCC" forCampo="percDuranteRetiradaQPNRC">Percentual m�ximo em rela��o � QDC para recupera��o:</ggas:labelContrato>
						 	<c:choose>
						 		<c:when test="${contratoVO.valorFixoPercDuranteRetiradaQPNRC}">
						 			<input class="campoValorFixo" type="text" value="${contratoVO.percDuranteRetiradaQPNRC}%" size="15" readonly="readonly" />
									<input type="hidden" name="percDuranteRetiradaQPNRC" value="${contratoVO.percDuranteRetiradaQPNRC}" />
						 		</c:when>
						 		<c:otherwise>
									<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoRelacaoQDCC" name="percDuranteRetiradaQPNRC" maxlength="6" size="3" 
										<ggas:campoSelecionado campo="percDuranteRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  
										value="${contratoVO.percDuranteRetiradaQPNRC}">
									<label class="rotuloInformativo rotuloHorizontal">%</label>
						 		</c:otherwise>
						 	</c:choose>				
						</div>

						<br />

						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoTerminoContratoC" forCampo="percDuranteRetiradaQPNRC">Percentual m�ximo da recupera��o com o t�rmino do contrato:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoPercFimRetiradaQPNRC}">
									<input class="campoValorFixo" type="text" value="${contratoVO.percFimRetiradaQPNRC}%" size="15" readonly="readonly" />
									<input type="hidden" name="percFimRetiradaQPNRC" value="${contratoVO.percFimRetiradaQPNRC}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoTerminoContratoC" name="percFimRetiradaQPNRC" maxlength="6" size="3"  
										<ggas:campoSelecionado campo="percFimRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" 
										value="${contratoVO.percFimRetiradaQPNRC}">
									<label class="rotuloInformativo rotuloHorizontal">%</label>
								</c:otherwise>
							</c:choose>									
						</div>

						<br /><br />

						<hr class="linhaSeparadora">

						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataInicioRecuperacaoC" forCampo="dataInicioRetiradaQPNRC">Data de inicio da recupera��o:</ggas:labelContrato>
							<input class="campoData campo2Linhas" type="text" id="dataInicioRecuperacaoC" name="dataInicioRetiradaQPNRC" maxlength="10" <ggas:campoSelecionado campo="dataInicioRetiradaQPNRC"/> value="${contratoVO.dataInicioRetiradaQPNRC}" onblur="mostrarAsterisco(this.value,['dataMaximaRecuperacaoC','percentualMaximoTerminoContratoC','percentualMaximoRelacaoQDCC']);"><br />
						</div>
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataMaximaRecuperacaoC" forCampo="dataFimRetiradaQPNRC">Data m�xima para recupera��o:</ggas:labelContrato>
							<input class="campoData campo2Linhas" type="text" id="dataMaximaRecuperacaoC" name="dataFimRetiradaQPNRC" maxlength="10" <ggas:campoSelecionado campo="dataFimRetiradaQPNRC"/> value="${contratoVO.dataFimRetiradaQPNRC}"><br />
						</div>
						<hr class="linhaSeparadora">
						<div class="labelCampo">
							<ggas:labelContrato styleId="rotuloTempoValidadeRetiradaQPNR" styleClass="rotulo2Linhas" forCampo="tempoValidadeRetiradaQPNRC">Tempo de validade para recupera��o da QPNR do ano anterior:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoTempoValidadeRetiradaQPNRC}">
									<input class="campoValorFixo" type="text" value="${contratoVO.tempoValidadeRetiradaQPNRC} anos" size="15" readonly="readonly" />
									<input type="hidden" name="tempoValidadeRetiradaQPNRC" value="${contratoVO.tempoValidadeRetiradaQPNRC}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="tempoValidadeRetiradaQPNRC" name="tempoValidadeRetiradaQPNRC" maxlength="2" size="1" 
										<ggas:campoSelecionado campo="tempoValidadeRetiradaQPNRC"/> value="${contratoVO.tempoValidadeRetiradaQPNRC}" onkeypress="return formatarCampoInteiro(event);">
									<label class="rotuloInformativo3Linhas rotuloHorizontal" for="tempoValidadeRetiradaQPNRC">anos</label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</fieldset>
			</div>
		</fieldset>
   </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var $tabs = $('#abasInternas').tabs({active : 1});
		$("#abasInternas").attr({style: "display: block" });
	});

</script>