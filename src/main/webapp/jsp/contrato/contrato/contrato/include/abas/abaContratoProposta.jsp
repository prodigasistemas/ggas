<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<script type="text/javascript">
	function exibirPopupPesquisaProposta() {	
		popup = window.open('exibirPesquisaPropostaPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarProposta(idSelecionado){

		var idProposta = document.getElementById("idProposta");
		var numeroProposta = document.getElementById("numeroProposta");
		var exibirNumeroProposta = document.getElementById("exibirNumeroProposta");

		var gastoEstimadoGNMes = document.getElementById("gastoEstimadoGNMes");
		var economiaEstimadaGNMes = document.getElementById("economiaEstimadaGNMes");
		var economiaEstimadaGNAno = document.getElementById("economiaEstimadaGNAno");
		var descontoEfetivoEconomia = document.getElementById("descontoEfetivoEconomia");	
		var valorParticipacaoCliente = document.getElementById("valorParticipacaoCliente");
		var qtdParcelasFinanciamento = document.getElementById("qtdParcelasFinanciamento");
		var percentualJurosFinanciamento = document.getElementById("percentualJurosFinanciamento");
		var valorInvestimento = document.getElementById("valorInvestimento");
		var dataInvestimento = document.getElementById("dataInvestimento");
		
		var idCliente = null;
		var idImovel = null;
		
		limparCamposProposta();
		
		if(idSelecionado != '') {
			
			idProposta.value = idSelecionado;
					
			AjaxService.obterPropostaPorChave( idSelecionado, {
				callback: function(proposta) {	           		
	           		if(proposta != null){
		           		if(proposta["chavePrimaria"] != undefined){
							idProposta.value = proposta['chavePrimaria'];
			           	}  	     
		           		if(proposta["numeroCompletoProposta"] != undefined){
							numeroProposta.value = proposta['numeroCompletoProposta'];
							exibirNumeroProposta.value = proposta['numeroCompletoProposta'];
			           	}  	     
		               	if(proposta["valorGastoMensal"] != undefined){
		               		gastoEstimadoGNMes.value = proposta["valorGastoMensal"];
		               	}
		               	if(proposta["economiaMensalGN"] != undefined){
		               		economiaEstimadaGNMes.value = proposta["economiaMensalGN"];
		               	}
		                if(proposta["economiaAnualGN"] != undefined){
		               		economiaEstimadaGNAno.value = proposta["economiaAnualGN"];
		               	}
		                if(proposta["percentualEconomia"] != undefined){
		                	descontoEfetivoEconomia.value = proposta["percentualEconomia"];
		               	}
		               	if(proposta["valorCliente"] != undefined){
		               		valorParticipacaoCliente.value = proposta["valorCliente"];
		               	}
		               	if(proposta["quantidadeParcelas"] != undefined){
		               		qtdParcelasFinanciamento.value = proposta["quantidadeParcelas"];
		               	}
		               	if(proposta["percentualJuros"] != undefined){
		               		percentualJurosFinanciamento.value = proposta["percentualJuros"];
		               	}		        
		               	if(proposta["valorInvestimento"] != undefined){
		               		valorInvestimento.value = proposta["valorInvestimento"];
		               	}
		               	if(proposta["dataInvestimento"] != undefined){
		               		dataInvestimento.value = proposta["dataInvestimento"];
		               	}
		               	
		               	if(proposta["idImovel"] != undefined){
		               		idImovel = proposta["idImovel"];
		               	}
		               	
		               	if(proposta["idCliente"] != undefined){
		               		idCliente = proposta["idCliente"];
		               	}
		               		               	
	               	}
	           		habilitarCamposParticipacao();
	        	}, async:false}
	        );	        
        }
		

		if (confirm("Deseja associar o im�vel e o cliente da proposta ao contrato?")) {
			if(idCliente != null) {
			    selecionarCliente(idCliente);
			}
			else {
				alert("Proposta sem cliente!");
			}
			if(idImovel != null) {
				selecionarImovelAbaProposta(idImovel);		
			}
			else {
				alert("Proposta sem im�vel!");
			}
		}


		
	}

	function limparCamposProposta() {
		document.getElementById("idProposta").value = '';
		document.getElementById("gastoEstimadoGNMes").value = '';
		document.getElementById("economiaEstimadaGNMes").value = '';
		document.getElementById("economiaEstimadaGNAno").value = '';
		document.getElementById("descontoEfetivoEconomia").value = '';
		document.getElementById('valorParticipacaoCliente').value = '';
		document.getElementById("qtdParcelasFinanciamento").value = '';
		document.getElementById("percentualJurosFinanciamento").value = '';
		document.getElementById("valorInvestimento").value = '';
		document.getElementById("dataInvestimento").value = '';
	}
	
	
</script>

<div id="abaProposta" class="agrupamento">

<fieldset id="contratoCol1" class="colunaEsq">
	<legend>N�mero da Proposta</legend>
	<fieldset id="pesquisarProposta">
		<div class="pesquisarClienteFundo" style="height:150px;">
			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Proposta</span> para selecionar a Proposta.</p>
			<input name="Button" id="botaoPesquisarCliente" class="bottonRightCol2" title="Pesquisar Proposta"  value="Pesquisar Proposta" onclick="exibirPopupPesquisaProposta(); exibirIndicador();" type="button">
			<br />
			<label class="rotulo campoObrigatorio" id="rotuloNumeroProposta" for="numeroProposta"><span class="campoObrigatorioSimbolo2">* </span>N�mero da Proposta:</label>
			<input class="campoDesabilitado" type="text" id="exibirNumeroProposta" name="exbirNumeroProposta"  maxlength="50" disabled="disabled" value="${contratoVO.numeroProposta}"><br />
			<input type="hidden" id="numeroProposta" name="numeroProposta"  maxlength="50" value="${contratoVO.numeroProposta}"><br />
			<input type="hidden" name="idProposta" id="idProposta" value="${contratoVO.idProposta}"/>
		</div>
	</fieldset>
</fieldset>

	<fieldset id="contratoCol2" class="colunaFinal">
		<legend>Proposta</legend>
		<div class="conteinerDados">

			<div class="labelCampo">
				<ggas:labelContrato forCampo="gastoEstimadoGNMes">Gasto Mensal com GN:</ggas:labelContrato>
				<input
					class="campoTexto campo2Linhas campoHorizontal campoValorReal"
					type="text" id="gastoEstimadoGNMes" name="gastoEstimadoGNMes"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,11,2);"
					maxlength="10" size="13"
					<ggas:campoSelecionado campo="gastoEstimadoGNMes"/>
					value="<fmt:formatNumber value='${contratoVO.gastoEstimadoGNMes}' maxFractionDigits='2' minFractionDigits='2'/>">
				<label class="rotuloInformativo rotuloHorizontal gastoEstimadoGNMes"
					for="gastoEstimadoGNMes">R$/m�s</label><br class="quebraLinha2">
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="economiaEstimadaGNMes">Economia Mensal<br />com GN:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoValorReal" type="text"
					id="economiaEstimadaGNMes" name="economiaEstimadaGNMes"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,11,2);"
					maxlength="10" size="13"
					<ggas:campoSelecionado campo="economiaEstimadaGNMes"/>
					value="<fmt:formatNumber value='${contratoVO.economiaEstimadaGNMes}' maxFractionDigits='2' minFractionDigits='2'/>"><br
					class="quebraLinha2">
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="economiaEstimadaGNAno">Economia Anual<br />com GN:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoValorReal" type="text"
					id="economiaEstimadaGNAno" name="economiaEstimadaGNAno"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,11,2);"
					maxlength="10" size="13"
					<ggas:campoSelecionado campo="economiaEstimadaGNAno"/>
					value="<fmt:formatNumber value='${contratoVO.economiaEstimadaGNAno}' maxFractionDigits='2' minFractionDigits='2'/>"><br
					class="quebraLinha2">
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="descontoEfetivoEconomia">Desconto Efetivo na Economia:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoHorizontal" type="text"
					id="descontoEfetivoEconomia" name="descontoEfetivoEconomia"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,3,2);"
					maxlength="10" size="3"
					<ggas:campoSelecionado campo="descontoEfetivoEconomia"/>
					value="${contratoVO.descontoEfetivoEconomia}"> <label
					class="rotuloInformativo rotuloInformativo2Linhas rotuloHorizontal"
					for="descontoEfetivoEconomia">%</label><br class="quebraLinha" />
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="diaVencFinanciamento">Dia para Vencimento do Financiamento:</ggas:labelContrato>
				<c:choose>
					<c:when test="${contratoVO.valorFixoDiaVencFinanciamento}">
						<input class="campoValorFixo" type="text" value="${contratoVO.diaVencFinanciamento}" size="2" readonly="readonly" />
						<input type="hidden" name="diaVencFinanciamento" value="${contratoVO.diaVencFinanciamento}" />
					</c:when>
					<c:otherwise>
						<select id="diaVencFinanciamento" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="diaVencFinanciamento"/> name="diaVencFinanciamento">
							<option value="-1">Selecione</option>
							<c:forEach begin="1" end="31" var="dia">
								<option value="${dia}"
									<c:if test="${contratoVO.diaVencFinanciamento eq dia}">selected="selected"</c:if>>${dia}</option>
							</c:forEach>
						</select> 
						<label class="rotuloInformativo rotuloHorizontal" for="diaVencFinanciamento">(01-31)</label>
					</c:otherwise>
				</c:choose>
				<br class="quebraLinha2">
			</div>
			
				<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="valorParticipacaoCliente">Valor da Participa��o do Cliente:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoValorReal" type="text"
					id="valorParticipacaoCliente" name="valorParticipacaoCliente"
					onblur="aplicarMascaraNumeroDecimal(this,2);habilitarCamposParticipacao();"
					onkeypress="return formatarCampoDecimal(event,this,11,2);"
					onkeyup="habilitarCamposParticipacao()" maxlength="10" size="13"
					<ggas:campoSelecionado campo="valorParticipacaoCliente"/>
					value="<fmt:formatNumber value='${contratoVO.valorParticipacaoCliente}' maxFractionDigits='2' minFractionDigits='2'/>"><br
					class="quebraLinha" />
			</div>
			
				<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="qtdParcelasFinanciamento">Quantidade de Parcelas do Financiamento:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas" type="text"
					id="qtdParcelasFinanciamento" name="qtdParcelasFinanciamento"
					maxlength="3" size="2"
					<ggas:campoSelecionado campo="qtdParcelasFinanciamento"/>
					value="${contratoVO.qtdParcelasFinanciamento}"
					onkeypress="return formatarCampoInteiro(event);"<c:if test="${empty contratoVO.valorParticipacaoCliente}">disabled="disabled"</c:if> "><br
					class="quebraLinha" />
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="percentualJurosFinanciamento">Percentual de Juros do Financiamento:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoHorizontal" type="text"
					id="percentualJurosFinanciamento"
					name="percentualJurosFinanciamento"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,3,2);"
					maxlength="10" size="3"
					<ggas:campoSelecionado campo="percentualJurosFinanciamento"/>
					value="${contratoVO.percentualJurosFinanciamento}"
					<c:if test="${empty contratoVO.valorParticipacaoCliente}">disabled="disabled"</c:if>>
				<label class="rotuloInformativo rotuloHorizontal"
					for="percentualJurosFinanciamento">%</label><br
					class="quebraLinha2" />
			</div>
			
			<div class="labelCampo">
					<ggas:labelContrato styleClass="rotulo2Linhas"
						forCampo="sistemaAmortizacao">Sistema de<br />Amortiza��o:</ggas:labelContrato>
					<select id="sistemaAmortizacao" class="campoSelect campo2Linhas"
						name="sistemaAmortizacao"
						<c:if test="${empty contratoVO.valorParticipacaoCliente}">disabled="disabled"</c:if>>
						<option value="-1">Selecione</option>
						<c:forEach items="${listaAmortizacoes}" var="amortizacao">
							<option value="${amortizacao.chavePrimaria}"
								<c:if test="${contratoVO.sistemaAmortizacao eq amortizacao.chavePrimaria}">selected="selected"</c:if>>${amortizacao.descricao}</option>
						</c:forEach>
					</select><br class="quebraLinha" />
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo" forCampo="valorInvestimento">Valor do Investimento:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoValorReal" type="text"
					id="valorInvestimento" name="valorInvestimento"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,11,2);"
					maxlength="10" size="13"
					<ggas:campoSelecionado campo="valorInvestimento"/>
					value="<fmt:formatNumber value='${contratoVO.valorInvestimento}' maxFractionDigits='2' minFractionDigits='2'/>"><br
					class="quebraLinha" />
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo" forCampo="valorInvestimento">Data do Investimento:</ggas:labelContrato>
				<input class="campoData campo2Linhas" type="text"
					id="dataInvestimento" name="dataInvestimento" maxlength="10"
					<ggas:campoSelecionado campo="valorInvestimento"/>
					value="${contratoVO.dataInvestimento}"><br
					class="quebraLinha" />
			</div>

		</div>
	</fieldset>
</div>