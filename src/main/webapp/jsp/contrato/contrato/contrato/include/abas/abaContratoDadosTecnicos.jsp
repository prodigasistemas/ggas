<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script>
	$(function(){
		
		//Verifica se existem campos obrigat�rios e exibe ou esconde a legenda.
		var campoObrigatorioTecnicos = $("#tecnicos .campoObrigatorio").size();
		if (campoObrigatorioTecnicos > 0){
			$("#tecnicos p.legenda").show();
		} else {
			$("#tecnicos p.legenda").hide();				
		};
	});

	function limparAbaDadosTecnicos(){
		document.getElementById('vazaoInstantanea').value = '';
		document.getElementById('unidadeVazaoInstan').value = '-1';
		document.getElementById('vazaoInstantaneaMaxima').value = '';
		document.getElementById('unidadeVazaoInstanMaxima').value = '-1';
		document.getElementById('vazaoInstantaneaMinima').value = '';
		document.getElementById('unidadeVazaoInstanMinima').value = '-1';
		document.getElementById('faixaPressaoFornecimento').value = '-1';
		document.getElementById('pressaoMinimaFornecimento').value = '';
		document.getElementById('unidadePressaoMinimaFornec').value = '-1';
		document.getElementById('pressaoMaximaFornecimento').value = '';
		document.getElementById('unidadePressaoMaximaFornec').value = '-1';
		document.getElementById('pressaoLimiteFornecimento').value = '';
		document.getElementById('unidadePressaoLimiteFornec').value = '-1';		
		document.getElementById('numAnosCtrlParadaCliente').value = '';
		document.getElementById('maxTotalParadasCliente').value = '';
		document.getElementById('maxAnualParadasCliente').value = '';
		document.getElementById('numDiasProgrParadaCliente').value = '';
		document.getElementById('numAnosCtrlParadaCDL').value = '';
		document.getElementById('maxTotalParadasCDL').value = '';
		document.getElementById('maxAnualParadasCDL').value = '';
		document.getElementById('numDiasProgrParadaCDL').value = '';
		document.getElementById('numDiasConsecParadaCliente').value = '';		
		document.getElementById('numDiasConsecParadaCDL').value = '';	
		ajustarAsteriscosDadosTecnicos();	
	}

	function eventoOnChavePressaoFornecimento() {
		if(funcaoExiste('carregarFatorUnicoCorrecao')) {
			carregarFatorUnicoCorrecao();
		}
	}

	function ajustarAsteriscosDadosTecnicos() {
		if (!$("#numAnosCtrlParadaCliente").attr("disabled")) {
			mostrarAsterisco($("#numAnosCtrlParadaCliente").val(),['maxTotalParadasCliente']);
		}
		if (!$("#numAnosCtrlParadaCDL").attr("disabled")) {
			mostrarAsterisco($("#numAnosCtrlParadaCDL").val(),['maxTotalParadasCDL']);
		}
	}

	$( document ).ready(function() {
		eventoOnChavePressaoFornecimento();
	});

	function calcularPressaoMaxMin(){
		if($("#faixaPressaoFornecimento :selected").text().split(" ").length > 1){
			var pressao = $("#faixaPressaoFornecimento :selected").text().split(" ")[0];
			pressao = pressao.replace(',','.');
			pressao = Number.parseFloat(pressao);
			if (pressao > 0){
				var pressaoMin = pressao * 0.9;
				var pressaoMax = pressao * 1.05;
				$("#pressaoMinimaFornecimento").val(pressaoMin.toFixed(4).replace('.',','));
				$("#pressaoMaximaFornecimento").val(pressaoMax.toFixed(4).replace('.',','));
			}
		}
	}
	
</script>

<fieldset id="tecnicos">
	<c:choose>
		<c:when test="${contratoVO.fluxoDetalhamento}">
			<a class="linkHelp" href="<help:help>/detalhamentodocontratoabadadostcnicos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:when>
		<c:otherwise>
			<a class="linkHelp" href="<help:help>/abadadostcnicosinclusoalteraoadiodecontratos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:otherwise>
	</c:choose>
	<fieldset id="dadosTecnicosCol1" class="colunaEsq">
	
		<div class="labelCampo">
			<ggas:labelContrato forCampo="vazaoInstantanea">Vaz�o instant�nea:</ggas:labelContrato>
			<c:choose>
				<c:when test="${contratoVO.valorFixoVazaoInstantanea}">
					<input class="campoValorFixo" type="text" value="${contratoVO.vazaoInstantanea}" size="10" readonly="readonly" />
					<input type="hidden" name="vazaoInstantanea" value="${contratoVO.vazaoInstantanea}" />
					<c:forEach items="${listaUnidadeVazao}" var="unidade">
						<c:if test="${contratoVO.unidadeVazaoInstan eq unidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="10" readonly="readonly" />
							<input type="hidden" name="unidadeVazaoInstan" value="${unidade.chavePrimaria}" />
						</c:if>
					</c:forEach>						
				</c:when>
				<c:otherwise>
					<input class="campoTexto campoHorizontal" type="text" id="vazaoInstantanea" name="vazaoInstantanea" maxlength="14" size="11" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);" <ggas:campoSelecionado campo="vazaoInstantanea"/> value="${contratoVO.vazaoInstantanea}">
					<select class="campoSelect campoHorizontal" id="unidadeVazaoInstan" name="unidadeVazaoInstan" <ggas:campoSelecionado campo="vazaoInstantanea"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${contratoVO.unidadeVazaoInstan eq unidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>						
					</select>
				</c:otherwise>
			</c:choose>
		</div>

		<br />
		
		<div class="labelCampo">
			<ggas:labelContrato forCampo="vazaoInstantaneaMinima">Vaz�o instant�nea m�nima:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoVazaoInstantaneaMinima}">
					<input class="campoValorFixo" type="text" value="${contratoVO.vazaoInstantaneaMinima}" size="10" readonly="readonly" />
					<input type="hidden" name="vazaoInstantaneaMinima" value="${contratoVO.vazaoInstantaneaMinima}" />
					&nbsp;&nbsp;
					<c:forEach items="${listaUnidadeVazao}" var="unidade">
						<c:if test="${contratoVO.unidadeVazaoInstanMinima eq unidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="10" readonly="readonly" />
							<input type="hidden" name="unidadeVazaoInstanMinima" value="${unidade.chavePrimaria}" />
						</c:if>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<input class="campoTexto campoHorizontal" type="text" id="vazaoInstantaneaMinima" name="vazaoInstantaneaMinima" maxlength="14" size="11" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);" <ggas:campoSelecionado campo="vazaoInstantaneaMinima"/> value="${contratoVO.vazaoInstantaneaMinima}">
					<select class="campoSelect campoHorizontal" id="unidadeVazaoInstanMinima" name="unidadeVazaoInstanMinima" <ggas:campoSelecionado campo="vazaoInstantaneaMinima"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${contratoVO.unidadeVazaoInstanMinima eq unidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>						
					</select>
				</c:otherwise>
			</c:choose>
		</div>
		
		<br />
			
		<div class="labelCampo">
			<ggas:labelContrato forCampo="vazaoInstantaneaMaxima">Vaz�o instant�nea m�xima:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoVazaoInstantaneaMaxima}">
					<input id="vazaoInstantaneaMaximaValorFixo" class="campoValorFixo" type="text" value="${contratoVO.vazaoInstantaneaMaxima}" size="10" readonly="readonly" />
					<input type="hidden" name="vazaoInstantaneaMaxima" value="${contratoVO.vazaoInstantaneaMaxima}" />
					<c:forEach items="${listaUnidadeVazao}" var="unidade">
						<c:if test="${contratoVO.unidadeVazaoInstanMaxima eq unidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="10" readonly="readonly" />
							<input type="hidden" name="unidadeVazaoInstanMaxima" value="${unidade.chavePrimaria}" />
						</c:if>
					</c:forEach>						
				</c:when>
				<c:otherwise>
					<input class="campoTexto campoHorizontal" type="text" id="vazaoInstantaneaMaxima" name="vazaoInstantaneaMaxima" maxlength="14" size="11" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);" <ggas:campoSelecionado campo="vazaoInstantaneaMaxima"/> value="${contratoVO.vazaoInstantaneaMaxima}">
					<select class="campoSelect campoHorizontal" id="unidadeVazaoInstanMaxima" name="unidadeVazaoInstanMaxima" <ggas:campoSelecionado campo="vazaoInstantaneaMaxima"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${contratoVO.unidadeVazaoInstanMaxima eq unidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>						
					</select>
				</c:otherwise>
			</c:choose>
		</div>
		
		<br class="quebraLinha2" />
		
		<div class="labelCampo">
			<ggas:labelContrato forCampo="faixaPressaoFornecimento">Press�o de fornecimento:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoFaixaPressaoFornecimento}">
					<c:forEach items="${listaFaixasPressaoFornecimento}" var="faixa">
						<c:if test="${contratoVO.faixaPressaoFornecimento eq faixa.chavePrimaria}">
						 	<input class="campoValorFixo" type="text" value="${faixa.descricaoFormatada}" size="15" readonly="readonly" />
							<input type="hidden" id="faixaPressaoFornecimento" name="faixaPressaoFornecimento" value="${faixa.chavePrimaria}" />
						</c:if>
					</c:forEach>					
				</c:when>
				<c:otherwise>
					<select class="campoSelect" id="faixaPressaoFornecimento" name="faixaPressaoFornecimento" onchange="eventoOnChavePressaoFornecimento();calcularPressaoMaxMin();" <ggas:campoSelecionado campo="faixaPressaoFornecimento"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaFaixasPressaoFornecimento}" var="faixa">
							<option value="<c:out value="${faixa.chavePrimaria}"/>"<c:if test="${contratoVO.faixaPressaoFornecimento eq faixa.chavePrimaria}"> selected="selected"</c:if>><c:out value="${faixa.descricaoFormatada}"/></option>
						</c:forEach>					
					</select>
				</c:otherwise>
			</c:choose>
		</div>
		
		<br />
		
		<div class="labelCampo">
			<ggas:labelContrato forCampo="pressaoColetada">Press�o coletada:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoPressaoColetada}">
					<input class="campoValorFixo" type="text" value="${contratoVO.pressaoColetada}" size="10" readonly="readonly" />
					<input type="hidden" name="pressaoColetada" value="${contratoVO.pressaoColetada}" />
					<c:forEach items="${listaUnidadePressao}" var="unidade">
						<c:if test="${contratoVO.unidadePressaoColetada eq unidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="10" readonly="readonly" />
							<input type="hidden" name="unidadePressaoColetada" value="${unidade.chavePrimaria}" />
						</c:if>
					</c:forEach>						
				</c:when>
				<c:otherwise>
					<input class="campoTexto campoHorizontal" type="text" id="pressaoColetada" name="pressaoColetada" maxlength="11" size="9" 
						onkeypress="return formatarCampoDecimalPositivo(event,this,5,4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" <ggas:campoSelecionado campo="pressaoColetada"/> value="${contratoVO.pressaoColetada}">
					<select class="campoSelect campoHorizontal" id="unidadePressaoColetada" name="unidadePressaoColetada" <ggas:campoSelecionado campo="pressaoColetada"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${contratoVO.unidadePressaoColetada eq unidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>						
					</select>
				</c:otherwise>
			</c:choose>
		</div>
		
		<br />
				
		<div class="labelCampo">
			<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="pressaoMinimaFornecimento">Press�o m�nima de fornecimento:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoPressaoMinimaFornecimento}">
					<input class="campoValorFixo" type="text" value="${contratoVO.pressaoMinimaFornecimento}" size="10" readonly="readonly" />
					<input type="hidden" name="pressaoMinimaFornecimento" value="${contratoVO.pressaoMinimaFornecimento}" />
					<c:forEach items="${listaUnidadePressao}" var="unidade">
						<c:if test="${contratoVO.unidadePressaoMinimaFornec eq unidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="10" readonly="readonly" />
							<input type="hidden" name="unidadePressaoMinimaFornec" value="${unidade.chavePrimaria}" />
						</c:if>
					</c:forEach>						
				</c:when>
				<c:otherwise>
					<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="pressaoMinimaFornecimento" name="pressaoMinimaFornecimento" maxlength="11" size="9" onkeypress="return formatarCampoDecimalPositivo(event,this,5,4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" <ggas:campoSelecionado campo="pressaoMinimaFornecimento"/> value="${contratoVO.pressaoMinimaFornecimento}">
					<select class="campoSelect campoHorizontal campo2Linhas" id="unidadePressaoMinimaFornec" name="unidadePressaoMinimaFornec" <ggas:campoSelecionado campo="pressaoMinimaFornecimento"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${contratoVO.unidadePressaoMinimaFornec eq unidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>						
					</select>
				</c:otherwise>
			</c:choose>
		</div>
		<br />
		
		<div class="labelCampo">
			<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="pressaoMaximaFornecimento">Press�o m�xima de fornecimento:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoPressaoMaximaFornecimento}">
					<input class="campoValorFixo" type="text" value="${contratoVO.pressaoMaximaFornecimento}" size="10" readonly="readonly" />
					<input type="hidden" name="pressaoMaximaFornecimento" value="${contratoVO.pressaoMaximaFornecimento}" />
					<c:forEach items="${listaUnidadePressao}" var="unidade">
						<c:if test="${contratoVO.unidadePressaoMaximaFornec eq unidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="10" readonly="readonly" />
							<input type="hidden" name="unidadePressaoMaximaFornec" value="${unidade.chavePrimaria}" />
						</c:if>
					</c:forEach>						
				</c:when>
				<c:otherwise>
					<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="pressaoMaximaFornecimento" name="pressaoMaximaFornecimento" maxlength="11" size="9" onkeypress="return formatarCampoDecimalPositivo(event,this,5,4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" <ggas:campoSelecionado campo="pressaoMaximaFornecimento"/> value="${contratoVO.pressaoMaximaFornecimento}">
					<select class="campoSelect campoHorizontal campo2Linhas" id="unidadePressaoMaximaFornec" name="unidadePressaoMaximaFornec" <ggas:campoSelecionado campo="pressaoMaximaFornecimento"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${contratoVO.unidadePressaoMaximaFornec eq unidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>						
					</select>
				</c:otherwise>
			</c:choose>
		</div>
		
		<br />
				
		<div class="labelCampo">
			<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="pressaoLimiteFornecimento">Press�o limite de fornecimento:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoPressaoLimiteFornecimento}">
					<input class="campoValorFixo" type="text" value="${contratoVO.pressaoLimiteFornecimento}" size="10" readonly="readonly" />
					<input type="hidden" name="pressaoLimiteFornecimento" value="${contratoVO.pressaoLimiteFornecimento}" />
					<c:forEach items="${listaUnidadePressao}" var="unidade">
						<c:if test="${contratoVO.unidadePressaoLimiteFornec eq unidade.chavePrimaria}">
							<input class="campoValorFixo" type="text" value="${unidade.descricaoAbreviada}" size="10" readonly="readonly" />
							<input type="hidden" name="unidadePressaoLimiteFornec" value="${unidade.chavePrimaria}" />
						</c:if>
					</c:forEach>						
				</c:when>
				<c:otherwise>
					<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="pressaoLimiteFornecimento" name="pressaoLimiteFornecimento" maxlength="11" size="9" onkeypress="return formatarCampoDecimalPositivo(event,this,5,4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" <ggas:campoSelecionado campo="pressaoLimiteFornecimento"/> value="${contratoVO.pressaoLimiteFornecimento}">
					<select class="campoSelect campoHorizontal campo2Linhas" id="unidadePressaoLimiteFornec" name="unidadePressaoLimiteFornec" <ggas:campoSelecionado campo="pressaoLimiteFornecimento"/>>
						<option value="-1"/>Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${contratoVO.unidadePressaoLimiteFornec eq unidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>						
					</select>		
				</c:otherwise>
			</c:choose>
		</div>
		
	</fieldset>
	
	<c:if test="${contratoVO.fluxoInclusao}">
		<fieldset id="dadosTecnicosCol3" class="colunaDir">
			<label class="rotulo" style="margin-left: 10px; font-size:15px;">Informa��es do �ltimo contrato</label>
			</br></br>
			<label class="rotulo">Vaz�o instant�nea m�xima Anterior:</label>
			<span class="itemDetalhamento" style="margin-right: 5px;"><c:out value="${vazaoInstantaneaMaximaAnterior}"/></span>
			</br></br>
			<label class="rotulo">Press�o de fornecimento Anterior:</label>
			<span class="itemDetalhamento" style="margin-right: 5px;"><c:out value="${pressaoFornecimentoAnterior}"/></span>
			</br></br>
			<label class="rotulo">Fornecimento M�ximo Di�rio Anterior:</label>
			<span class="itemDetalhamento" style="margin-right: 5px;"><c:out value="${fornecimentoMaximoDiarioAnterior}"/></span>
			</br></br>
		</fieldset>
	</c:if>
	
	
	<fieldset id="dadosTecnicosCol2">
		<div class="agrupamento">
			<fieldset id="paradasProgramadasUsuario">
				<legend>Paradas Programadas Cliente</legend>
				<div id="dadosParadasProgramadasUsuarioFundo" class="conteinerDados">
					<fieldset class="coluna">
						<div class="labelCampo">
							<ggas:labelContrato forCampo="numAnosCtrlParadaCliente">Per�odo:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoNumAnosCtrlParadaCliente}">
									<input class="campoValorFixo" type="text" value="${contratoVO.numAnosCtrlParadaCliente} anos" size="10" readonly="readonly" />
									<input type="hidden" name="numAnosCtrlParadaCliente" value="${contratoVO.numAnosCtrlParadaCliente}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="numAnosCtrlParadaCliente" name="numAnosCtrlParadaCliente" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="numAnosCtrlParadaCliente"/> value="${contratoVO.numAnosCtrlParadaCliente}" onblur="mostrarAsterisco(this.value,['maxTotalParadasCliente']);">
									<label class="rotuloHorizontal rotuloInformativo" for="numAnosCtrlParadaCliente">anos</label>
								</c:otherwise>
							</c:choose>				
						</div>
						
						<br />
						
						<div class="labelCampo">					
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="maxTotalParadasCliente">Quantidade total do per�odo:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoMaxTotalParadasCliente}">
									<input class="campoValorFixo" type="text" value="${contratoVO.maxTotalParadasCliente} dias" size="10" readonly="readonly" />
									<input type="hidden" name="maxTotalParadasCliente" value="${contratoVO.maxTotalParadasCliente}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="maxTotalParadasCliente" name="maxTotalParadasCliente" maxlength="3" size="2" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="maxTotalParadasCliente"/> value="${contratoVO.maxTotalParadasCliente}">
									<label class="rotuloHorizontal rotuloInformativo" >dias</label>					
								</c:otherwise>
							</c:choose>		
						</div>
								
					</fieldset>
					<fieldset class="colunaFinal">
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="maxAnualParadasCliente">Quantidade m�xima por ano:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoMaxAnualParadasCliente}">
									<input class="campoValorFixo" type="text" value="${contratoVO.maxAnualParadasCliente} dias" size="10" readonly="readonly" />
									<input type="hidden" name="maxTotalParadasCliente" value="${contratoVO.maxAnualParadasCliente}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="maxAnualParadasCliente" name="maxAnualParadasCliente" maxlength="3" size="2" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="maxAnualParadasCliente"/> value="${contratoVO.maxAnualParadasCliente}">
									<label class="rotuloHorizontal rotuloInformativo" for="maxAnualParadasCliente">dias</label>
								</c:otherwise>
							</c:choose>				
						</div>
						
						<br />
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="numDiasProgrParadaCliente">Anteced�ncia do aviso:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoNumDiasProgrParadaCliente}">
									<input class="campoValorFixo" type="text" value="${contratoVO.numDiasProgrParadaCliente} dias" size="10" readonly="readonly" />
									<input type="hidden" name="numDiasProgrParadaCliente" value="${contratoVO.numDiasProgrParadaCliente}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="numDiasProgrParadaCliente" name="numDiasProgrParadaCliente" maxlength="3" size="1" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="numDiasProgrParadaCliente"/> value="${contratoVO.numDiasProgrParadaCliente}">
									<label class="rotuloHorizontal rotuloInformativo" for="numDiasProgrParadaCliente">dias</label>
								</c:otherwise>
							</c:choose>				
						</div>
						
						<br />	
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="numDiasConsecParadaCliente">N�mero de dias consecutivos de paradas do cliente:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoNumDiasConsecParadaCliente}">
									<input class="campoValorFixo" type="text" value="${contratoVO.numDiasConsecParadaCliente} dias" size="10" readonly="readonly" />
									<input type="hidden" name="numDiasConsecParadaCliente" value="${contratoVO.numDiasConsecParadaCliente}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="numDiasConsecParadaCliente" name="numDiasConsecParadaCliente" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="numDiasConsecParadaCliente"/> value="${contratoVO.numDiasConsecParadaCliente}">
									<label class="rotuloHorizontal rotuloInformativo" for="numDiasConsecParadaCliente">dias</label>
								</c:otherwise>
							</c:choose>		
						</div>		
					</fieldset>
				</div>
			</fieldset>
		</div>		
		<div class="agrupamento">
			<fieldset id="paradasProgramadasCDL">
				<legend>Paradas Programadas CDL</legend>
				<div id="dadosParadasProgramadasCDLFundo" class="conteinerDados">
					<fieldset class="coluna">
						<div class="labelCampo">
							<ggas:labelContrato forCampo="numAnosCtrlParadaCDL">Per�odo:</ggas:labelContrato>
											
							<c:choose>
								<c:when test="${contratoVO.valorFixoNumAnosCtrlParadaCDL}">
									<input class="campoValorFixo" type="text" value="${contratoVO.numAnosCtrlParadaCDL} anos" size="10" readonly="readonly" />
									<input type="hidden" name="numAnosCtrlParadaCDL" value="${contratoVO.numAnosCtrlParadaCDL}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="numAnosCtrlParadaCDL" name="numAnosCtrlParadaCDL" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="numAnosCtrlParadaCDL"/> value="${contratoVO.numAnosCtrlParadaCDL}" onblur="mostrarAsterisco(this.value,['maxTotalParadasCDL']);">
									<label class="rotuloHorizontal rotuloInformativo" for="numAnosCtrlParadaCDL">anos</label>
								</c:otherwise>
							</c:choose>
						</div>
						
						<br />
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="maxTotalParadasCDL">Quantidade total do periodo:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoMaxTotalParadasCDL}">
									<input class="campoValorFixo" type="text" value="${contratoVO.maxTotalParadasCDL} dias" size="10" readonly="readonly" />
									<input type="hidden" name="maxTotalParadasCDL" value="${contratoVO.maxTotalParadasCDL}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="maxTotalParadasCDL" name="maxTotalParadasCDL" maxlength="3" size="2" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="maxTotalParadasCDL"/> value="${contratoVO.maxTotalParadasCDL}">
									<label class="rotuloHorizontal rotuloInformativo">dias</label>
								</c:otherwise>
							</c:choose>			
						</div>	
					</fieldset>
					<fieldset class="colunaFinal">
						<div class="labelCampo">
							<ggas:labelContrato forCampo="maxAnualParadasCDL">Quantidade m�xima por ano:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoMaxAnualParadasCDL}">
									<input class="campoValorFixo" type="text" value="${contratoVO.maxAnualParadasCDL} dias" size="10" readonly="readonly" />
									<input type="hidden" name="maxAnualParadasCDL" value="${contratoVO.maxAnualParadasCDL}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="maxAnualParadasCDL" name="maxAnualParadasCDL" maxlength="3" size="2" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="maxAnualParadasCDL"/> value="${contratoVO.maxAnualParadasCDL}">
									<label class="rotuloHorizontal rotuloInformativo" for="numAnosCtrlParadaCDL">dias</label>
								</c:otherwise>
							</c:choose>
						</div>
									
						<br />
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="numDiasProgrParadaCDL">Anteced�ncia do aviso:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoNumDiasProgrParadaCDL}">
									<input class="campoValorFixo" type="text" value="${contratoVO.numDiasProgrParadaCDL} dias" size="10" readonly="readonly" />
									<input type="hidden" name="numDiasProgrParadaCDL" value="${contratoVO.numDiasProgrParadaCDL}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="numDiasProgrParadaCDL" name="numDiasProgrParadaCDL" maxlength="3" size="1" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="numDiasProgrParadaCDL"/> value="${contratoVO.numDiasProgrParadaCDL}">
									<label class="rotuloHorizontal rotuloInformativo" for="numDiasProgrParadaCDL">dias</label>
								</c:otherwise>
							</c:choose>				
						</div>
						
						<br />
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="numDiasConsecParadaCDL">N�mero de dias consecutivos<br />de paradas da CDL:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoNumDiasConsecParadaCDL}">
									<input class="campoValorFixo" type="text" value="${contratoVO.numDiasConsecParadaCDL} dias" size="10" readonly="readonly" />
									<input type="hidden" name="numDiasConsecParadaCDL" value="${contratoVO.numDiasConsecParadaCDL}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="numDiasConsecParadaCDL" name="numDiasConsecParadaCDL" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="numDiasConsecParadaCDL"/> value="${contratoVO.numDiasConsecParadaCDL}">
									<label class="rotuloHorizontal rotuloInformativo" for="numDiasConsecParadaCDL">dias</label>				
								</c:otherwise>
							</c:choose>
						</div>
										
					</fieldset>				
				</div>
			</fieldset>			
		</div>
	</fieldset>
	
	
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Contratos.</p>
</fieldset>