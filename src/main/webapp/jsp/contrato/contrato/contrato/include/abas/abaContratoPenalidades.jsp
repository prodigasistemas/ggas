<%@ page contentType="text/html; charset=iso-8859-1"%>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<script type="text/javascript">
	function editarPenalidade(){
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var selecao = verificarSelecaoPenalidadeRetiradaMaiorMenorGeral(true);
		if (selecao == true) {
			submeter('contratoForm', 'exibirEditarPenalidadeRetiradaMaiorMenorGeral?#penalidades');
		}	
	}

	$(document).ready(function() {
		habilitarAvisoInterrupcao( <c:out value='${contratoVO.penalidadeRetMaiorMenor}' /> )
	});
</script>

<div id="abaContratoPenalidades">
    <legend>Penalidades</legend>
    <fieldset id="conteinerSobreDemanda">
        <div class="conteinerDados">
            <fieldset id="conteinerRetiradaMaiorMenor">
                <legend>Penalidade por retirada a Maior/Menor:</legend>
                <div class="conteinerDados2 agrupamento">
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="penalidadeRetMaiorMenor">Penalidade:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPenalidadeRetMaiorMenor}">
                      			<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="penalidadeRetMaiorMenor">
                       				<c:if test='${contratoVO.penalidadeRetMaiorMenor == (penalidadeRetMaiorMenor.chavePrimaria + "")}'>
                       					<input class="campoValorFixo" type="text" value="${penalidadeRetMaiorMenor.descricao}" size="30" readonly="readonly" />
										<input type="hidden" name="penalidadeRetMaiorMenor" value="${penalidadeRetMaiorMenor.chavePrimaria}" />
                       				</c:if>
                      			</c:forEach>
							</c:when>
							<c:otherwise>
		                        <select class="campoSelect campo2Linhas" name="penalidadeRetMaiorMenor" id="penalidadeRetMaiorMenor"
		                        	<ggas:campoSelecionado campo="penalidadeRetMaiorMenor"/> onchange="habilitarAvisoInterrupcao(this.value)"; >
		                        		<option value="-1">Selecione</option>
		                       			<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="penalidadeRetMaiorMenor">
		                           			<option value="<c:out value="${penalidadeRetMaiorMenor.chavePrimaria}"/>"
		                           				<c:if test='${contratoVO.penalidadeRetMaiorMenor == (penalidadeRetMaiorMenor.chavePrimaria + "")}'> selected="selected"</c:if>>
					                            <c:out value="${penalidadeRetMaiorMenor.descricao}" />
		                           			</option>
		                       			</c:forEach>
		                       	</select>
							</c:otherwise>
						</c:choose>
                    </div>
                    <br class="quebraLinha" />
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="periodicidadeRetMaiorMenor">Periodicidade:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPeriodicidadeRetMaiorMenor}">
                        		<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="periodicidadeRetMaiorMenor">
                           			<c:if test='${contratoVO.periodicidadeRetMaiorMenor == (periodicidadeRetMaiorMenor.chavePrimaria + "")}'>
                           				<input class="campoValorFixo" type="text" value="${periodicidadeRetMaiorMenor.descricao}" size="30" readonly="readonly" />
										<input type="hidden" name="periodicidadeRetMaiorMenor" value="${periodicidadeRetMaiorMenor.chavePrimaria}" />
                           			</c:if>
                        		</c:forEach>
							</c:when>
							<c:otherwise>
		                        <select class="campoSelect campo2Linhas" name="periodicidadeRetMaiorMenor" id="periodicidadeRetMaiorMenor" <ggas:campoSelecionado campo="periodicidadeRetMaiorMenor"/>>
		                        	<option value="-1">Selecione</option>
		                        		<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="periodicidadeRetMaiorMenor">
		                            		<option value="<c:out value="${periodicidadeRetMaiorMenor.chavePrimaria}"/>"
		                            			<c:if test='${contratoVO.periodicidadeRetMaiorMenor == (periodicidadeRetMaiorMenor.chavePrimaria + "")}'>selected="selected"</c:if>>
		                            			<c:out value="${periodicidadeRetMaiorMenor.descricao}" />
		                            		</option>
		                        		</c:forEach>
		                        </select>
							</c:otherwise>
						</c:choose>
                    </div>
                    <br class="quebraLinha" />
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="baseApuracaoRetMaiorMenor">Base de Apura��o:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoBaseApuracaoRetMaiorMenor}">
	                        	<c:forEach items="${listaBaseApuracao}" var="baseApuracaoRetMaiorMenor">
                            		<c:if test='${contratoVO.baseApuracaoRetMaiorMenor == (baseApuracaoRetMaiorMenor.chavePrimaria + "")}'>
                            			<input class="campoValorFixo" type="text" value="${baseApuracaoRetMaiorMenor.descricao}" size="30" readonly="readonly" />
										<input type="hidden" name="baseApuracaoRetMaiorMenor" value="${baseApuracaoRetMaiorMenor.chavePrimaria}" />
                            		</c:if>
	                        	</c:forEach>
							</c:when>
							<c:otherwise>
		                        <select class="campoSelect" name="baseApuracaoRetMaiorMenor" id="baseApuracaoRetMaiorMenor" <ggas:campoSelecionado campo="baseApuracaoRetMaiorMenor"/>>
		                        	<option value="-1">Selecione</option>
		                        	<c:forEach items="${listaBaseApuracao}" var="baseApuracaoRetMaiorMenor">
		                            	<option value="<c:out value="${baseApuracaoRetMaiorMenor.chavePrimaria}"/>"
		                            		<c:if test='${contratoVO.baseApuracaoRetMaiorMenor == (baseApuracaoRetMaiorMenor.chavePrimaria + "")}'>selected="selected"</c:if>>
		                            		<c:out value="${baseApuracaoRetMaiorMenor.descricao}" />
		                            	</option>
		                        	</c:forEach>
		                        </select>
							</c:otherwise>
						</c:choose>
                    </div>
                    <br class="quebraLinha" />
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="dataIniVigRetirMaiorMenor">In�cio Vig�ncia:</ggas:labelContrato>
                        <input class="campoData" type="text" id="dataIniVigRetirMaiorMenor"
                        name="dataIniVigRetirMaiorMenor" maxlength="10"
                        <ggas:campoSelecionado campo="dataIniVigRetirMaiorMenor"/>
                        value="${contratoVO.dataIniVigRetirMaiorMenor}"><br
                            class="quebraLinha" />
                    </div>
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="dataFimVigRetirMaiorMenor">Fim Vig�ncia:</ggas:labelContrato>
                        <input class="campoData" type="text" id="dataFimVigRetirMaiorMenor"
                        name="dataFimVigRetirMaiorMenor" maxlength="10"
                        <ggas:campoSelecionado campo="dataFimVigRetirMaiorMenor"/>
                        value="${contratoVO.dataFimVigRetirMaiorMenor}"><br
                            class="quebraLinha" />
                    </div>
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="precoCobrancaRetirMaiorMenor">Tabela de Pre�o para Cobran�a:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPrecoCobrancaRetirMaiorMenor}">
	                        	<c:forEach items="${listaPrecoCobranca}" var="precoCobrancaRetirMaiorMenor">
                            		<c:if test='${contratoVO.precoCobrancaRetirMaiorMenor == (precoCobrancaRetirMaiorMenor.chavePrimaria + "")}'>
                            			<input class="campoValorFixo" type="text" value="${precoCobrancaRetirMaiorMenor.descricao}" size="60" readonly="readonly" />
										<input type="hidden" name="precoCobrancaRetirMaiorMenor" value="${precoCobrancaRetirMaiorMenor.chavePrimaria}" />
                            		</c:if>
	                        	</c:forEach>
							</c:when>
							<c:otherwise>
		                        <select class="campoSelect" name="precoCobrancaRetirMaiorMenor" id="precoCobrancaRetirMaiorMenor" <ggas:campoSelecionado campo="precoCobrancaRetirMaiorMenor"/>>
		                        	<option value="-1">Selecione</option>
		                        	<c:forEach items="${listaPrecoCobranca}" var="precoCobrancaRetirMaiorMenor">
		                            	<option value="<c:out value="${precoCobrancaRetirMaiorMenor.chavePrimaria}"/>"
		                            		<c:if test='${contratoVO.precoCobrancaRetirMaiorMenor == (precoCobrancaRetirMaiorMenor.chavePrimaria + "")}'>selected="selected"</c:if>>
		                            		<c:out value="${precoCobrancaRetirMaiorMenor.descricao}" />
		                            	</option>
		                        	</c:forEach>
		                        </select>
							</c:otherwise>
						</c:choose>
                    </div>
                    <br class="quebraLinha" />
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="tipoApuracaoRetirMaiorMenor">Forma de C�lculo para Cobran�a:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoTipoApuracaoRetirMaiorMenor}">
	                        	<c:forEach items="${listaTipoApuracao}" var="tipoApuracaoRetirMaiorMenor">
                           			<c:if test='${contratoVO.tipoApuracaoRetirMaiorMenor == (tipoApuracaoRetirMaiorMenor.chavePrimaria + "")}'>
                           				<input class="campoValorFixo" type="text" value="${tipoApuracaoRetirMaiorMenor.descricao}" size="60" readonly="readonly" />
										<input type="hidden" name="tipoApuracaoRetirMaiorMenor" value="${tipoApuracaoRetirMaiorMenor.chavePrimaria}" />
                           			</c:if>
	                        	</c:forEach>
							</c:when>
							<c:otherwise>
		                        <select class="campoSelect" name="tipoApuracaoRetirMaiorMenor" id="tipoApuracaoRetirMaiorMenor" <ggas:campoSelecionado campo="tipoApuracaoRetirMaiorMenor"/>>
		                        	<option value="-1">Selecione</option>
		                        	<c:forEach items="${listaTipoApuracao}" var="tipoApuracaoRetirMaiorMenor">
		                            	<option value="<c:out value="${tipoApuracaoRetirMaiorMenor.chavePrimaria}"/>"
		                           			<c:if test='${contratoVO.tipoApuracaoRetirMaiorMenor == (tipoApuracaoRetirMaiorMenor.chavePrimaria + "")}'>selected="selected"</c:if>>
		                            		<c:out value="${tipoApuracaoRetirMaiorMenor.descricao}" />
		                            	</option>
		                        	</c:forEach>
		                        </select>
							</c:otherwise>
						</c:choose>
                    </div>
                    <br class="quebraLinha" />
                    <div class="labelCampo">
                        <ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualCobRetMaiorMenor">Percentual de Cobran�a:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPercentualCobRetMaiorMenor}">
								<input class="campoValorFixo" type="text" value="${contratoVO.percentualCobRetMaiorMenor}%" size="5" readonly="readonly" />
								<input type="hidden" name="percentualCobRetMaiorMenor" value="${contratoVO.percentualCobRetMaiorMenor}" />
							</c:when>
							<c:otherwise>
		                        <input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualCobRetMaiorMenor" <ggas:campoSelecionado campo="percentualCobRetMaiorMenor"/>
		                        	name="percentualCobRetMaiorMenor" maxlength="6" size="4" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
		                        	value="${contratoVO.percentualCobRetMaiorMenor}"> 
		                       	<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>
							</c:otherwise>
						</c:choose>
                    </div>
                    <div class="labelCampo">
                        <ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualCobIntRetMaiorMenor">Percentual de Cobran�a para Consumo Superior Durante Aviso de Interrup��o:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPercentualCobIntRetMaiorMenor}">
								<input class="campoValorFixo" type="text" value="${contratoVO.percentualCobIntRetMaiorMenor}%" size="5" readonly="readonly" />
								<input type="hidden" name="percentualCobIntRetMaiorMenor" value="${contratoVO.percentualCobIntRetMaiorMenor}" />
							</c:when>
							<c:otherwise>
		                        <input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualCobIntRetMaiorMenor" <ggas:campoSelecionado campo="percentualCobIntRetMaiorMenor"/>
		                        	name="percentualCobIntRetMaiorMenor" maxlength="6" size="4" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
		                        	value="${contratoVO.percentualCobIntRetMaiorMenor}">
		                        <label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>
							</c:otherwise>
						</c:choose>
                    </div>
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="consumoReferenciaRetMaiorMenor">Consumo Refer�ncia:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoConsumoReferenciaRetMaiorMenor}">
	                        	<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaRetMaiorMenor">
                            		<c:if test='${contratoVO.consumoReferenciaRetMaiorMenor == (consumoReferenciaRetMaiorMenor.chavePrimaria + "")}'>
                            			<input class="campoValorFixo" type="text" value="${consumoReferenciaRetMaiorMenor.descricao}" size="30" readonly="readonly" />
										<input type="hidden" name="consumoReferenciaRetMaiorMenor" value="${consumoReferenciaRetMaiorMenor.chavePrimaria}" />
                            		</c:if>
	                        	</c:forEach>
							</c:when>
							<c:otherwise>
		                        <select class="campoSelect campo2Linhas" name="consumoReferenciaRetMaiorMenor" id="consumoReferenciaRetMaiorMenor" <ggas:campoSelecionado campo="consumoReferenciaRetMaiorMenor"/>>
		                        	<option value="-1">Selecione</option>
		                        	<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaRetMaiorMenor">
		                            	<option value="<c:out value="${consumoReferenciaRetMaiorMenor.chavePrimaria}"/>"
		                            		<c:if test='${contratoVO.consumoReferenciaRetMaiorMenor == (consumoReferenciaRetMaiorMenor.chavePrimaria + "")}'>selected="selected"</c:if>>
		                            		<c:out value="${consumoReferenciaRetMaiorMenor.descricao}" />
		                            	</option>
		                        	</c:forEach>
		                        </select>
							</c:otherwise>
						</c:choose>
                    </div>
                    <br class="quebraLinha" />
                    <div class="labelCampo">
                        <ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualRetMaiorMenor">Percentual Por Retirada:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPercentualRetMaiorMenor}">
								<input class="campoValorFixo" type="text" value="${contratoVO.percentualRetMaiorMenor}%" size="5" readonly="readonly" />
								<input type="hidden" name="percentualRetMaiorMenor" value="${contratoVO.percentualRetMaiorMenor}" />
							</c:when>
							<c:otherwise>
		                        <input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualRetMaiorMenor" <ggas:campoSelecionado campo="percentualRetMaiorMenor"/>
		                        	name="percentualRetMaiorMenor" maxlength="6" size="4" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
		                        	value="${contratoVO.percentualRetMaiorMenor}"> 
		                       	<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label> 
							</c:otherwise>
						</c:choose>
                    </div>
                   	<br class="quebraLinha" />
                    <div class="labelCampo">
                        <ggas:labelContrato forCampo="indicadorImposto" styleClass="rotuloVertical rotulo2Linhas">Usar pre�os s/ impostos para cobran�a:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoIndicadorImposto}">
								<c:choose>
									<c:when test="${contratoVO.indicadorImposto}">
										<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
									</c:when>
									<c:otherwise>
										<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
									</c:otherwise>
								</c:choose>
								<input type="hidden" name="indicadorImposto" value="${contratoVO.indicadorImposto}" />
							</c:when>
							<c:otherwise>
		                        <input class="campoRadio" type="radio" name="indicadorImposto" id="indicadorImpostoSim" value="true" <ggas:campoSelecionado campo="indicadorImposto"/>
		                       		<c:if test="${contratoVO.indicadorImposto == true}">checked="checked"</c:if>>
		                        <label class="rotuloRadio" for="indicadorImpostoSim">Sim</label> 
		                        
		                        <input class="campoRadio" type="radio" name="indicadorImposto" id="indicadorImpostoNao" value="false" <ggas:campoSelecionado campo="indicadorImposto"/>
		                        	<c:if test="${contratoVO.indicadorImposto == false}">checked="checked"</c:if>>
		                        <label class="rotuloRadio" for="indicadorImpostoNao">N�o</label>
							</c:otherwise>
						</c:choose>
                    </div>

                </div>
                <br class="quebraLinha" />
                <fieldset class="conteinerBloco">
                    <input name="Button" class="bottonRightCol"
                    <ggas:campoSelecionado campo="periodicidadeRetMaiorMenor"/>
                    value="Excluir" type="button" onclick="excluirPenalidadeRetiradaMaiorMenorGeral();"> 
                    
                    <input name="Button" id="botaoAdicionarRetirMaiorMenor" class="bottonRightCol"
                    <ggas:campoSelecionado campo="periodicidadeRetMaiorMenor"/>
                    value="Adicionar / Salvar" type="button" onclick="adicionarPenalidadeRetiradaMaiorMenorGeral();">

                    <input name="Button" class="bottonRightCol"
                    <ggas:campoSelecionado campo="periodicidadeRetMaiorMenor"/>
                    value="Editar" type="button" onclick="editarPenalidade();">
                </fieldset>
                <div>
                    <c:set var="i" value="0" />
                    <display:table id="penalidadesRetMaiorMenorVO"
                        class="dataTableGGAS dataTableAba"
                        name="sessionScope.listaPenalidadesRetMaiorMenorVO"
                        style="width: 100%" sort="list" requestURI="#">
                        <display:column style="width: 25px"
                            title="<input type='checkbox' id='checkCompromissosPenalidades' class='checkAllCompromissos'/>">
                            <input type="checkbox" class="checkCompromissosPenalidades" name="indicePenalidadeRetiradaMaiorMenor" value=" <c:out value="${i}"/>"
                            	<c:if test='${indicePenalidadeRetiradaMaiorMenor == i}'> checked='true'</c:if>
                            >
                        </display:column>
                        <display:column title="Penalidade">
                            ${penalidadesRetMaiorMenorVO.penalidadeRetiradaMaiorMenor.descricao}
                        </display:column>
                        <display:column title="Periodicidade">
                            ${penalidadesRetMaiorMenorVO.entidadePeriodicidadeRetiradaMaiorMenor.descricao}
                        </display:column>
                        <display:column title="Base Apura��o">
                            ${penalidadesRetMaiorMenorVO.entidadeBaseApuracaoRetiradaMaiorMenor.descricao}
                        </display:column>
                        <display:column title="In�cio Vig�ncia">
                            <fmt:formatDate
                                 value="${penalidadesRetMaiorMenorVO.dataInicioVigencia}" />
                         </display:column>
                         <display:column title="Fim Vig�ncia">
                             <fmt:formatDate
                                 value="${penalidadesRetMaiorMenorVO.dataFimVigencia}" />
                         </display:column>
                        <display:column title="Tabela Pre�o">
                            ${penalidadesRetMaiorMenorVO.entidadePrecoCobrancaRetiradaMaiorMenor.descricao}
                        </display:column>
                        <display:column title="Percentual Cobran�a">
                            <fmt:formatNumber
                                 value="${(penalidadesRetMaiorMenorVO.valorPercentualCobRetMaiorMenor * 100)}" /> %
                         </display:column>
                         <display:column title="Percentual Retirada">
                            <fmt:formatNumber
                                 value="${(penalidadesRetMaiorMenorVO.valorPercentualRetMaiorMenor * 100)}" /> %
                        </display:column>
                        <display:column title="Consumo Refer�ncia">
                            ${penalidadesRetMaiorMenorVO.entidadeConsumoReferenciaRetMaiorMenor.descricao}
                        </display:column>
                        <c:set var="i" value="${i+1}" />
                    </display:table>
                 </div>
            </fieldset>
        </div>
    </fieldset>

    <fieldset id="conteinerDeliveryPay">
        <div class="conteinerDados agrupamento">
            <legend>Penalidade por Delivery Or Pay:</legend>
            <fieldset id="conteinerDeliveryPay">

                <div class="conteinerDados2">
                    <div class="labelCampo">
                        <ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualTarifaDoP">Percentual Sobre a Tarifa do G�s:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPercentualTarifaDoP}">
								<input class="campoValorFixo" type="text" value="${contratoVO.percentualTarifaDoP}%" size="5" readonly="readonly" />
								<input type="hidden" name="percentualTarifaDoP" value="${contratoVO.percentualTarifaDoP}" />
							</c:when>
							<c:otherwise>
		                        <input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualTarifaDoP" <ggas:campoSelecionado campo="percentualTarifaDoP"/>
		                        	name="percentualTarifaDoP" maxlength="6" size="4" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
		                        	value="${contratoVO.percentualTarifaDoP}"> 
	                        	<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>
							</c:otherwise>
						</c:choose>
                    </div>
                </div>
            </fieldset>
        </div>
    </fieldset>
    <fieldset id="conteinerDeliveryPay">
        <div class="conteinerDados agrupamento">
            <legend>Penalidade por G�s Fora de Especifica��o:</legend>
            <fieldset id="conteinerDeliveryPay">
                <div class="conteinerDados2">
                    <div class="labelCampo">
                        <ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualSobreTariGas">Percentual Sobre a Tarifa do G�s:</ggas:labelContrato>
                        <c:choose>
							<c:when test="${contratoVO.valorFixoPercentualSobreTariGas}">
								<input class="campoValorFixo" type="text" value="${contratoVO.percentualSobreTariGas}%" size="5" readonly="readonly" />
								<input type="hidden" name="percentualSobreTariGas" value="${contratoVO.percentualSobreTariGas}" />
							</c:when>
							<c:otherwise>
		                        <input class="campoTexto campoHorizontal" type="text" id="percentualSobreTariGas" <ggas:campoSelecionado campo="percentualSobreTariGas"/>
		                        	name="percentualSobreTariGas" maxlength="6" size="4" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
		                        	value="${contratoVO.percentualSobreTariGas}"> 
		                      	<label class="rotuloInformativo rotuloHorizontal" for="percentualSobreTariGas">%</label>
							</c:otherwise>
						</c:choose>
                    </div>
                </div>
            </fieldset>
        </div>
    </fieldset>
</div>