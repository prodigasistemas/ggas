<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>
	$(function(){
		
		//Verifica se existem campos obrigat�rios e exibe ou esconde a legenda.
		var campoObrigatorioResponsabilidades = $("#responsabilidades .campoObrigatorio").size();
		if (campoObrigatorioResponsabilidades > 0){
			$("#responsabilidades p.legenda").show();
		} else {
			$("#responsabilidades p.legenda").hide();
		};
		
	});
	
	
	function adicionarResponsabilidade(form, adicionarContratoCliente) {
		selecionarItensComponenteSelect();
		if(funcaoExiste('selecionarOpcoesQDCContrato')) {
			selecionarOpcoesQDCContrato();
		}
		document.forms[0].indexLista.value = -1;
		submeter('contratoForm', adicionarContratoCliente + '?#responsabilidades');
	}
	
	function alterarResponsabilidade(form, adicionarContratoCliente) {
		selecionarItensComponenteSelect();
		if(funcaoExiste('selecionarOpcoesQDCContrato')) {
			selecionarOpcoesQDCContrato();
		}
		submeter('contratoForm', adicionarContratoCliente + '?#responsabilidades');
	}
	
	function removerResponsabilidade(removerContratoCliente,indice) {
		var fluxoAlteracao = "${param.fluxoAlteracao}";
		
		if (fluxoAlteracao != "true") {
			document.forms[0].indexLista.value = indice;
		}
		selecionarItensComponenteSelect();
		if(funcaoExiste('selecionarOpcoesQDCContrato')) {
			selecionarOpcoesQDCContrato();
		}
		submeter('contratoForm', removerContratoCliente + '?operacaoLista=true&#responsabilidades');
	}

	
	function exibirAlteracaoContratoCliente(indice, idCliente, idResponsabilidade, relacaoInicio, fimRelacionamento, descricaoTipoResponsabilidade) {
		var fluxoDetalhamento = "${param.fluxoDetalhamento}";

		if (indice != "") {
			document.forms[0].indexLista.value = indice;
			selecionarCliente(idCliente);

			if (fluxoDetalhamento == "true") {
				exibirDadosDetalhamento(descricaoTipoResponsabilidade, relacaoInicio);
			} else {
				if (idResponsabilidade != "") {
					document.forms[0].tipoResponsabilidade.value = idResponsabilidade;
				}
				if (relacaoInicio != "") {
					document.forms[0].dataInicioRelacao.value = relacaoInicio;
				} else {
					document.forms[0].dataInicioRelacao.value = "";
				}
			}
			
			if (fimRelacionamento != "") {
				document.forms[0].dataFimRelacao.value = fimRelacionamento;
			} else {
				document.forms[0].dataFimRelacao.value = "";
			}

			var botaoAlterarContratoResponsabilidade = document.getElementById("botaoAlterarContratoResponsabilidade");
			var botaoRemoverContratoResponsabilidade = document.getElementById("botaoRemoverContratoResponsabilidade");
			var botaoLimparContratoResponsabilidade = document.getElementById("botaoLimparContratoResponsabilidade");	
			var botaoIncluirContratoResponsabilidade = document.getElementById("botaoIncluirContratoResponsabilidade");	
			botaoAlterarContratoResponsabilidade.disabled = false;
			botaoRemoverContratoResponsabilidade.disabled = true;
			botaoLimparContratoResponsabilidade.disabled = false;
			botaoIncluirContratoResponsabilidade.disabled = true;
		
			<c:if test="${param.fluxoAlteracao eq true}">					
				document.getElementById('fimRelacionamento').style.display = '';				
			</c:if>			
						
		}
	}

	function exibirDadosDetalhamento(descricaoTipoResponsabilidade, relacaoInicio) {
		
		var nome = document.getElementById("nomeClienteTexto").value;
		var documento = document.getElementById("documentoFormatadoTexto").value;
		var email = document.getElementById("emailClienteTexto").value;
		var endereco = document.getElementById("enderecoFormatadoTexto").value;
		var inputNomeCliente = document.getElementById("nomeClienteTexto");
		
		$(function(){
			if (inputNomeCliente.style.display == "") {
					$("#nomeClienteTexto").css("display","none").after("<span class='itemDetalhamento itemDetalhamentoMedio'>" + nome + "</span>");
					$("#documentoFormatadoTexto").css("display","none").after("<span class='itemDetalhamento'>" + documento + "</span>");		
					$("#emailClienteTexto").css("display","none").after("<span class='itemDetalhamento itemDetalhamentoMedio'>" + email + "</span>");		
					$("#enderecoFormatadoTexto").css("display","none").after("<span class='itemDetalhamento itemDetalhamentoMedio'>" + endereco + "</span>");							
			} else {
					$("#nomeClienteTexto + span").text(nome);
					$("#documentoFormatadoTexto + span").text(documento);
					$("#emailClienteTexto + span").text(email);
					$("#enderecoFormatadoTexto + span").text(endereco);
			}
			$("#descricaoTipoResponsabilidade").text(descricaoTipoResponsabilidade);
			$("#dataInicioRelacao").text(relacaoInicio);
		});
	}
	
	function exibirRemocaoContratoCliente(removerClienteImovel,indice,idCliente,idResponsabilidade,relacaoInicio,fimRelacionamento) {
		
		var fluxoAlteracao = "${param.fluxoAlteracao}";
		
		if (fluxoAlteracao == "true") {
			
			if (indice != "") {
				document.forms[0].indexLista.value = indice;
				selecionarCliente(idCliente);
				if (relacaoInicio != "") {
					document.forms[0].dataInicioRelacao.value = relacaoInicio;
				} else {
					document.forms[0].dataInicioRelacao.value = "";
				}			
				
				if (idResponsabilidade != "") {
					var tamanho = document.forms[0].tipoResponsabilidade.length;
					var opcao = undefined;
					for(var i = 0; i < tamanho; i++) {
						opcao = document.forms[0].tipoResponsabilidade.options[i];
						if (idResponsabilidade == opcao.value) {
							opcao.selected = true;
						}
					}
				}
				
				if (fimRelacionamento != "") {
					document.forms[0].dataFimRelacao.value = fimRelacionamento;
				} else {
					document.forms[0].dataFimRelacao.value = "";
				}
				
				var botaoAlterarContratoResponsabilidade = document.getElementById("botaoAlterarContratoResponsabilidade");
				var botaoRemoverContratoResponsabilidade = document.getElementById("botaoRemoverContratoResponsabilidade");
				var botaoLimparContratoResponsabilidade = document.getElementById("botaoLimparContratoResponsabilidade");	
				var botaoIncluirContratoResponsabilidade = document.getElementById("botaoIncluirContratoResponsabilidade");	
				botaoAlterarContratoResponsabilidade.disabled = true;
				botaoRemoverContratoResponsabilidade.disabled = false;
				botaoLimparContratoResponsabilidade.disabled = false;
				botaoIncluirContratoResponsabilidade.disabled = true;
			
				<c:if test="${param.fluxoAlteracao eq true}">					
					document.getElementById('fimRelacionamento').style.display = '';				
				</c:if>			
							
			}

		} else {
			var retorno = confirm('Deseja excluir a responsabilidade?');
			if (retorno == true) {
				removerResponsabilidade(removerClienteImovel,indice);
			}
		}
	}
	
	function limparAbaResponsabilidade() {
		
		document.forms[0].indexLista.value = -1;	
		limparFormularioDadosCliente();
		document.forms[0].tipoResponsabilidade.value = "-1";
		document.forms[0].dataInicioRelacao.value = "";

		var botaoAlterarContratoResponsabilidade = document.getElementById("botaoAlterarContratoResponsabilidade");
		var botaoRemoverContratoResponsabilidade = document.getElementById("botaoRemoverContratoResponsabilidade");
		var botaoLimparContratoResponsabilidade = document.getElementById("botaoLimparContratoResponsabilidade");	
		var botaoIncluirContratoResponsabilidade = document.getElementById("botaoIncluirContratoResponsabilidade");	
		
		botaoAlterarContratoResponsabilidade.disabled = true;
		botaoRemoverContratoResponsabilidade.disabled = true;
		botaoLimparContratoResponsabilidade.disabled = false;
		botaoIncluirContratoResponsabilidade.disabled = false;
		
		<c:if test="${param.fluxoAlteracao eq true}">					
			document.getElementById('fimRelacionamento').style.display = 'none';
		</c:if>
	}
	
	function onloadResponsabilidade() {
		<c:choose>
			<c:when test="${contratoVO.indexLista > -1 && empty param['operacaoLista']}">
				var botaoAlterarContratoResponsabilidade = document.getElementById("botaoAlterarContratoResponsabilidade");
				var botaoLimparContratoResponsabilidade = document.getElementById("botaoLimparContratoResponsabilidade");	
				var botaoIncluirContratoResponsabilidade = document.getElementById("botaoIncluirContratoResponsabilidade");
				botaoIncluirContratoResponsabilidade.disabled = true;
				botaoAlterarContratoResponsabilidade.disabled = false;	
				botaoLimparContratoResponsabilidade.disabled = false;
				
				<c:if test="${param.fluxoAlteracao eq true}">					
					document.getElementById('fimRelacionamento').style.display = '';				
				</c:if>
				
			</c:when>
			<c:when test="${sucessoManutencaoLista}">			
				limparAbaResponsabilidade();
			</c:when>					
		</c:choose>
	}

	<c:if test="${abaId == '7'}">
		addLoadEvent(onloadResponsabilidade);
	</c:if>
	
</script>

<fieldset id="responsabilidades">	
	<fieldset>
		<c:choose>
			<c:when test="${param.fluxoDetalhamento eq true}">
				<a class="linkHelp" href="<help:help>/detalhamentocontratoabaresponsabilidade.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
			</c:when>
			<c:otherwise>
				<a class="linkHelp" href="<help:help>/abaresponsabilidadeinclusoalteraoadiodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
			</c:otherwise>
		</c:choose>
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${contratoVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${contratoVO.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${contratoVO.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${contratoVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${contratoVO.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="true"/>									
			</jsp:include>		
		</fieldset>
		<fieldset class="colunaFinal">
			<label class="rotulo campoObrigatorio" for="idTipoResponsabilidade"><span class="campoObrigatorioSimbolo2">* </span>Tipo de responsabilidade:</label>
			<c:choose>
				<c:when test="${param.fluxoDetalhamento eq true}">
					<span id="descricaoTipoResponsabilidade" class="itemDetalhamento"></span><br class="quebraLinha2" />
				</c:when>
				<c:otherwise>
					
					<c:choose>
						<c:when test="${contratoVO.valorFixoTipoResponsabilidade}">
							<c:forEach items="${listaResponsabilidades}" var="responsabilidade">
								<c:if test="${contratoVO.tipoResponsabilidade == responsabilidade.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="${responsabilidade.descricao}" size="15" readonly="readonly" />
									<input type="hidden" name="tipoResponsabilidade" value="${responsabilidade.chavePrimaria}" />
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<select name="tipoResponsabilidade" id="tipoResponsabilidade" class="campoSelect campoHorizontal">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaResponsabilidades}" var="responsabilidade">
									<option value="<c:out value="${responsabilidade.chavePrimaria}"/>" <c:if test="${contratoVO.tipoResponsabilidade == responsabilidade.chavePrimaria}">selected=selected</c:if>>
										<c:out value="${responsabilidade.descricao}"/>
									</option>
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>
					
					
					<br class="quebraLinha2" />
				</c:otherwise>
			</c:choose>
			
			<div class="labelCampo">
				<label class="rotulo campoObrigatorio" id="rotuloDataInicioRelacao" for="dataInicioRelacao" ><span class="campoObrigatorioSimbolo2">* </span>Data de in�cio do relacionamento:</label>
				<c:choose>
					<c:when test="${param.fluxoDetalhamento eq true}">
						<span id="dataInicioRelacao" class="itemDetalhamento"></span><br class="quebraLinha2" />
					</c:when>
					<c:otherwise>
						<input class="campoData" type="text" id="dataInicioRelacao" name="dataInicioRelacao" maxlength="10" value="${contratoVO.dataInicioRelacao}"><br />
					</c:otherwise>
				</c:choose>
			</div>
			<fieldset id="fimRelacionamento" style="display: none">	
				<label class="rotulo campoObrigatorio" id="rotuloDataFimRelacao" for="dataFimRelacao"><span class="campoObrigatorioSimbolo2">* </span>Data de fim do relacionamento:</label>
				<input type="text" id="dataFimRelacao" name="dataFimRelacao" class="campoData" value="${contratoVO.dataFimRelacao}" maxlength="10"/>
			</fieldset>	
		</fieldset>	
		<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar Responsabilidades.</p>
	</fieldset>
	
	<fieldset class="conteinerBotoesAba">
		<input class="bottonRightCol" name="botaoLimparContratoResponsabilidade" id="botaoLimparContratoResponsabilidade" value="Limpar" type="button" onclick="limparAbaResponsabilidade();">
		<input class="bottonRightCol" name="botaoIncluirContratoResponsabilidade" id="botaoIncluirContratoResponsabilidade" value="Adicionar Responsabilidade" type="button" onclick="adicionarResponsabilidade(this.form,'<c:out value="${param['adicionarContratoCliente']}"/>');">
	   	<input class="bottonRightCol" name="botaoAlterarContratoResponsabilidade" id="botaoAlterarContratoResponsabilidade" value="Alterar Responsabilidade" disabled="disabled" type="button" onclick="alterarResponsabilidade(this.form,'<c:out value="${param['adicionarContratoCliente']}"/>');">
	   	<input class="bottonRightCol" name="botaoRemoverContratoResponsabilidade" id="botaoRemoverContratoResponsabilidade" value="Excluir Responsabilidade" disabled="disabled" type="button" onclick="removerResponsabilidade('<c:out value="${param['removerContratoCliente']}"/>', '');">
	</fieldset>
	
	<c:if test="${listaContratoCliente ne null}">
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS dataTableAba" name="listaContratoCliente" sort="list" id="contratoCliente" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#"  >
			<display:column sortable="false" title="Nome">
				<a href="javascript:exibirAlteracaoContratoCliente('${i}','${contratoCliente.idCliente}','${contratoCliente.idResponsabilidade}','${contratoCliente.relacaoInicio}','${contratoCliente.fimRelacionamento}', '${contratoCliente.responsabilidade}');">
					<c:out value="${contratoCliente.nome}"/>
				</a>
			</display:column>
			<display:column sortable="false" title="Tipo de Responsabilidade" style="width: 190px">
				<a href="javascript:exibirAlteracaoContratoCliente('${i}','${contratoCliente.idCliente}','${contratoCliente.idResponsabilidade}','${contratoCliente.relacaoInicio}','${contratoCliente.fimRelacionamento}', '${contratoCliente.responsabilidade}');">
					<c:out value="${contratoCliente.responsabilidade}"/>
				</a>
			</display:column>
			<display:column sortable="false" title="Data Inicial" style="width: 100px">
			<a href="javascript:exibirAlteracaoContratoCliente('${i}','${contratoCliente.idCliente}','${contratoCliente.idResponsabilidade}','${contratoCliente.relacaoInicio}','${contratoCliente.fimRelacionamento}', '${contratoCliente.responsabilidade}');">
					<c:out value="${contratoCliente.relacaoInicio}"/>
				</a>
			</display:column>
		    <display:column style="text-align: center;" class="abaContratoResponsabilidadeExcluir" headerClass="abaContratoResponsabilidadeExcluir">
				<a href="javascript:exibirRemocaoContratoCliente('removerContratoCliente','${i}','${contratoCliente.idCliente}','${contratoCliente.idResponsabilidade}','${contratoCliente.relacaoInicio}','${contratoCliente.fimRelacionamento}');"><img title="Excluir Responsabilidade" alt="Excluir Responsabilidade"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
			</display:column>        
			<c:set var="i" value="${i+1}" />
		</display:table>
	</c:if>
</fieldset>		
		