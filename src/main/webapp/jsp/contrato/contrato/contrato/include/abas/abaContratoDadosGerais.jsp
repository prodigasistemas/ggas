<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<fieldset id="contratoCol1" class="colunaEsq">
	<fieldset id="pesquisarCliente">
		<ggas:labelContrato styleClass="asteriscoLegend"
			forCampo="clienteAssinatura">
		</ggas:labelContrato>
		<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
			<jsp:param name="idCampoIdCliente" value="idCliente" />
			<jsp:param name="idCliente" value="${contratoVO.idCliente}" />
			<jsp:param name="idCampoNomeCliente" value="nomeCliente" />
			<jsp:param name="nomeCliente" value="${contratoVO.nomeCliente}" />
			<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente" />
			<jsp:param name="documentoFormatadoCliente"
				value="${contratoVO.documentoCliente}" />
			<jsp:param name="idCampoEmail" value="emailCliente" />
			<jsp:param name="emailCliente"
				value="${contratoVO.emailCliente}" />
			<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente" />
			<jsp:param name="enderecoFormatadoCliente"
				value="${contratoVO.enderecoCliente}" />
			<jsp:param name="dadosClienteObrigatorios" value="false" />
			<jsp:param name="nomeComponente" value="Cliente Assinatura" />
		</jsp:include>
	</fieldset>
</fieldset>

<fieldset id="contratoCol2" class="colunaFinal">
	<div id="contrato" class="agrupamento">
		<fieldset id="conteinerContrato">
			<legend>Contrato</legend>
			<div class="conteinerDados">
				<div class="labelCampo">
					<label class="rotulo" for="numeroContratoDisabled">N�mero do
						Contrato:</label> <input class="campoTexto" type="text"
						id="numeroContratoDisabled" name="numeroContratoDisabled" size="10"
						value="" disabled="disabled"> <input class="campoTexto"
						type="hidden" id="numeroContrato" name="numeroContrato"
						value="${contratoVO.numeroContrato}">
				</div>
				<div class="labelCampo">
					<ggas:labelContrato styleId="labelNumeroAnteriorContrato"
						forCampo="numeroAnteriorContrato">
									N�mero Anterior do Contrato:
								</ggas:labelContrato>
					<input class="campoTexto" type="text" id="numeroAnteriorContrato"
						name="numeroAnteriorContrato" maxlength="19" size="10"
						<ggas:campoSelecionado campo="numeroAnteriorContrato"/>
						value="${contratoVO.numeroAnteriorContrato}">
				</div>
				<div class="labelCampo">
					<ggas:labelContrato styleId="labelDescricaoContrato"
						forCampo="descricaoContrato">
									Descri��o:
								</ggas:labelContrato>
					<input class="campoTexto" type="text" id="descricaoContrato"
						name="descricaoContrato" maxlength="200" size="40"
						<ggas:campoSelecionado campo="descricaoContrato"/>
						onkeyup="return validarCriteriosParaCampo(this, '
										<c:out value="${isCaixaAlta}"/>', 
										'<c:out value="${isPermiteCaracteresEspeciais}"/>', 
										'formatarCampoNome(event)');"
						value="${contratoVO.descricaoContrato}" style="width: 216px;">
					<br class="quebraLinha" />
				</div>
				
				<c:if test='${contratoVO.fluxoAditamento}'>
				
					<div>
						<label class="rotulo" for="numeroAditivoDisabled">N�mero do Aditivo:</label>
						<input class="campoTexto" type="text" id="numeroAditivoDisabled" name="numeroAditivoDisabled" size="10" value="${contratoVO.numeroAditivo}" disabled="disabled">
						<input class="campoTexto" type="hidden" id="numeroAditivo" name="numeroAditivo" value="${contratoVO.numeroAditivo}" >
						<br class="quebraLinha" />
					</div>
					<div class="labelCampo">	
						<ggas:labelContrato forCampo="dataAditivo">Data do Aditivo:</ggas:labelContrato>
						<input class="campoData" type="text" id="dataAditivo" name="dataAditivo" maxlength="10" <ggas:campoSelecionado campo="dataAditivo"/> value="${contratoVO.dataAditivo}" >
						<br class="quebraLinha" />
					</div>
					<div class="labelCampo">	
						<ggas:labelContrato forCampo="descricaoAditivo">Descri��o do Aditivo:</ggas:labelContrato>
						<input class="campoTexto" type="text" size="30" maxlength="30"  id="descricaoAditivo" name="descricaoAditivo" <ggas:campoSelecionado campo="descricaoAditivo"/> value="${contratoVO.descricaoAditivo}" >
						<br class="quebraLinha" />
					</div>
					
				</c:if>
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="situacaoContrato">Situa��o do Contrato:</ggas:labelContrato>
					<c:choose>
						<c:when test="${contratoVO.valorFixoSituacaoContrato}">
							<c:forEach items="${listaSituacaoContrato}" var="situacaoContrato">
								<c:if test="${contratoVO.situacaoContrato == situacaoContrato.chavePrimaria || sessionScope.situacaoEscolhida == situacaoContrato.chavePrimaria}">
									<input class="campoValorFixo" type="text" value="<c:out value='${situacaoContrato.descricao}' />" size="15" readonly="readonly" />
									<input type="hidden" name="situacaoContrato" value="${situacaoContrato.chavePrimaria}" />
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<select name="situacaoContrato" id="situacaoContrato"
								class="campoSelect"
								<ggas:campoSelecionado campo="situacaoContrato"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaSituacaoContrato}" var="situacaoContrato">
									<option value="<c:out value="${situacaoContrato.chavePrimaria}"/>"
										<c:if test="${contratoVO.situacaoContrato == situacaoContrato.chavePrimaria || sessionScope.situacaoEscolhida == situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${situacaoContrato.descricao}" />
									</option>
								</c:forEach>
							</select>						
						</c:otherwise>
					</c:choose>
					<br class="quebraLinha2" />
				</div>
				<div class="labelCampo">
					<ggas:labelContrato forCampo="dataAssinatura">Data da assinatura:</ggas:labelContrato>
					<input class="campoData" type="text" id="dataAssinatura"
						name="dataAssinatura" maxlength="10"
						<ggas:campoSelecionado campo="dataAssinatura"/>
						value="${contratoVO.dataAssinatura}">
					<br class="quebraLinha2" />
				</div>
				<div class="labelCampo">
					<ggas:labelContrato forCampo="numeroEmpenho">N�mero do Empenho:</ggas:labelContrato>
					<input class="campoTexto campoHorizontal" type="text"
						id="numeroEmpenho" name="numeroEmpenho" maxlength="12" size="20"
						<ggas:campoSelecionado campo="numeroEmpenho"/>
						value="${contratoVO.numeroEmpenho}"
						onkeypress="return formatarCampoInteiro(event);">
					<br class="quebraLinha2" />
				</div>
				<div class="labelCampo">
					<ggas:labelContrato styleClass="rotulo2Linhas"
						forCampo="dataVencObrigacoesContratuais">Data de Vencimento das Obriga��es Contratuais:</ggas:labelContrato>
					<input class="campoData campo2Linhas" type="text"
						id="dataVencObrigacoesContratuais"
						name="dataVencObrigacoesContratuais" maxlength="10"
						<ggas:campoSelecionado campo="dataVencObrigacoesContratuais"/>
						onchange="validaObrigatoriedade();"
						value="${contratoVO.dataVencObrigacoesContratuais}"
						style="margin: 7px 0px 0px 0px;">
						<br class="quebraLinha2" />
				</div>
				<div class="labelCampo">
					<ggas:labelContrato forCampo="indicadorAnoContratual">Defini��o de Ano Contratual:</ggas:labelContrato>
					<c:choose>
						<c:when test="${contratoVO.valorFixoIndicadorAnoContratual}">
							<c:choose>
								<c:when test="${contratoVO.indicadorAnoContratual == true }">
									<input class="campoValorFixo" type="text" value="ANO(Fiscal)" size="15" readonly="readonly" />							
								</c:when>
								<c:otherwise>
									<input class="campoValorFixo" type="text" value="ano(Calend�rio)" size="15" readonly="readonly" />
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="indicadorAnoContratual" value="${contratoVO.indicadorAnoContratual}" />
						</c:when>
						<c:otherwise>
							<input class="campoRadio" type="radio" name="indicadorAnoContratual"
								id="anoContratualMaiusculo"
								<ggas:campoSelecionado campo="indicadorAnoContratual"/> value="true"
								<c:if test="${contratoVO.indicadorAnoContratual == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" id="labelAnoContratualMaiusculo"
								for="anoContratualMaiusculo">ANO(Fiscal)</label> <input
								class="campoRadio" type="radio" name="indicadorAnoContratual"
								id="anoContratualMinusculo"
								<ggas:campoSelecionado campo="indicadorAnoContratual"/>
								value="false"
								<c:if test="${contratoVO.indicadorAnoContratual == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" id="labelAnoContratualMinusculo"
								for="anoContratualMinusculo">ano(Calend�rio)</label>
						</c:otherwise>
					</c:choose>
					
					<br class="quebraLinha" />
				</div>
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="renovacaoAutomatica">Renova��o Autom�tica:</ggas:labelContrato>
					<c:choose>
						<c:when test="${contratoVO.valorFixoRenovacaoAutomatica}">
							<c:choose>
								<c:when test="${contratoVO.renovacaoAutomatica == true }">
									<input class="campoValorFixo" type="text" value="Sim" size="15" readonly="readonly" />							
								</c:when>
								<c:otherwise>
									<input class="campoValorFixo" type="text" value="N�o" size="15" readonly="readonly" />
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="renovacaoAutomatica" value="${contratoVO.renovacaoAutomatica}" />
						</c:when>
						<c:otherwise>
							<input class="campoRadio" type="radio" name="renovacaoAutomatica"
								id="indicadorRenovacaoAutomaticaSim"
								<ggas:campoSelecionado campo="renovacaoAutomatica"/> value="true"
								<c:if test="${contratoVO.renovacaoAutomatica == true}">checked="checked"</c:if>
								onclick="mostrarAsterisco(this.value,['numDiasRenoAutoContrato']);">
							<label class="rotuloRadio" for="indicadorRenovacaoAutomaticaSim">Sim</label>
							<input class="campoRadio" type="radio" name="renovacaoAutomatica"
								id="indicadorRenovacaoAutomaticaNao"
								<ggas:campoSelecionado campo="renovacaoAutomatica"/> value="false"
								<c:if test="${contratoVO.renovacaoAutomatica == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="indicadorRenovacaoAutomaticaNao">N�o</label>
							<br class="quebraRadio" />						
						</c:otherwise>
					</c:choose>
					
					<fieldset id="ConteinerQuantDiasRenovacao">
						<ggas:labelContrato forCampo="numDiasRenoAutoContrato">Per�odo de Renova��o:</ggas:labelContrato>
						<c:choose>
							<c:when test="${contratoVO.valorFixoNumDiasRenoAutoContrato}">
								<input class="campoValorFixo" type="text" value="${contratoVO.numDiasRenoAutoContrato}" size="4" readonly="readonly" />
								<input type="hidden" name="numDiasRenoAutoContrato" value="${contratoVO.numDiasRenoAutoContrato}" />
							</c:when>
							<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="numDiasRenoAutoContrato" name="numDiasRenoAutoContrato" maxlength="4" size="4"
										<ggas:campoSelecionado campo="numDiasRenoAutoContrato"/> value="${contratoVO.numDiasRenoAutoContrato}" onkeypress="return formatarCampoInteiro(event);"> 
										<label class="rotuloInformativo" for="numDiasRenoAutoContrat">dias</label>
							</c:otherwise>
						</c:choose>	
					</fieldset>							
		
					<fieldset id="ConteinerTempoAntecedenciaRenovacao" style="height: 13px;">
						<ggas:labelContrato styleClass="rotulo2Linhas" styleId="labelTempoAntecedenciaRenovacao" forCampo="tempoAntecedenciaRenovacao">
							Tempo de Anteced�ncia para Negocia��o da Renova��o:
						</ggas:labelContrato>
						<c:choose>
							<c:when test="${contratoVO.valorFixoTempoAntecedenciaRenovacao}">
								<input class="campoValorFixo" type="text" value="${contratoVO.tempoAntecedenciaRenovacao}" size="2" readonly="readonly" />
								<input type="hidden" name="tempoAntecedenciaRenovacao" value="${contratoVO.tempoAntecedenciaRenovacao}" />
							</c:when>
							<c:otherwise>
								<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="tempoAntecedenciaRenovacao" name="tempoAntecedenciaRenovacao" maxlength="3" size="2" style="margin-top: 7px;"
									<ggas:campoSelecionado campo="tempoAntecedenciaRenovacao"/> value="${contratoVO.tempoAntecedenciaRenovacao}" onkeypress="return formatarCampoInteiro(event);"> 
									<label class="rotuloInformativo2Linhas" style="margin-top: 6px;" for="tempoAntecedenciaRenovacao">dias</label>
							</c:otherwise>
						</c:choose>
							
					</fieldset>
					<br class="quebraLinha"  />
				</div>
				<div class="labelCampo">
					<ggas:labelContrato forCampo="valorContrato">Valor do contrato:</ggas:labelContrato>
					<input class="campoTexto campoValorReal valorContratoCss"
						type="text" id="valorContrato" name="valorContrato"
						onblur="aplicarMascaraNumeroDecimal(this,2);"
						onkeypress="return formatarCampoDecimal(event,this,11,2);"
						maxlength="10" size="13"
						<ggas:campoSelecionado campo="valorContrato"/>
						value="<fmt:formatNumber value='${contratoVO.valorContrato}' maxFractionDigits='2' minFractionDigits='2'/>">
					<br class="quebraLinha" />
				</div>
				<div class="labelCampo">
					<ggas:labelContrato styleClass="rotulo"
						styleId="labelVolumeReferencia" forCampo="volumeReferencia">Volume de Refer�ncia:</ggas:labelContrato>
					<input class="campoTexto" type="text" id="volumeReferencia"
						name="volumeReferencia"
						onblur="aplicarMascaraNumeroDecimal(this,2);"
						onkeypress="return formatarCampoDecimal(event,this,7,2);"
						maxlength="10" size="10"
						<ggas:campoSelecionado campo="volumeReferencia"/>
						value="${contratoVO.volumeReferencia}">
						
					<br class="quebraLinha" />
				</div>
				<div class="labelCampo">
					<ggas:labelContrato forCampo="tipoPeriodicidadePenalidade"> Tipo Periodicidade Penalidade: </ggas:labelContrato>
					<c:choose>
						<c:when test="${contratoVO.valorFixoTipoPeriodicidadePenalidade}">
							<c:choose>
								<c:when test="${contratoVO.tipoPeriodicidadePenalidade == 'true'}">
									<input class="campoValorFixo" type="text" value="Civil" size="15" readonly="readonly" />
								</c:when>
								<c:otherwise>
									<input class="campoValorFixo" type="text" value="Cont�nuo" size="15" readonly="readonly" />
								</c:otherwise>
							</c:choose>
							
							<input type="hidden" id="periodicidadeContinua" name="tipoPeriodicidadePenalidade" value="${contratoVO.tipoPeriodicidadePenalidade}" />
						</c:when>
						<c:otherwise>
							<input class="campoRadio" type="radio" value="true" name="tipoPeriodicidadePenalidade" id="periodicidadeContinua" <ggas:campoSelecionado campo="tipoPeriodicidadePenalidade"/>
								<c:if test="${contratoVO.tipoPeriodicidadePenalidade == 'true'}"> checked="checked" </c:if> />
							<label class="rotuloRadio" for="tipoPeriodicidadePenalidade">Civil</label>
							
							<input class="campoRadio" type="radio" value="false" name="tipoPeriodicidadePenalidade" <ggas:campoSelecionado campo="tipoPeriodicidadePenalidade"/>
								<c:if test="${contratoVO.tipoPeriodicidadePenalidade == 'false'}"> checked="checked" </c:if> />
							<label class="rotuloRadio" for="tipoPeriodicidadePenalidade">Cont�nuo</label>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="labelCampo">
					<label class="rotulo" id="rotuloCliente">Modelo:</label>
					<span id="detalhamentoDescricao" class="itemDetalhamento"><c:out value="${modeloContrato.descricao}"/></span>
				</div>
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="prazoVigencia">Prazo de Vig�ncia:</ggas:labelContrato>
					<input class="campoTexto campoHorizontal" type="text"
						id="prazoVigencia" name="prazoVigencia" maxlength="3" size="20"
						<ggas:campoSelecionado campo="prazoVigencia"/>
						value="${contratoVO.prazoVigencia}"
						onkeypress="return formatarCampoInteiro(event);">
					<label class="rotuloInformativo rotuloHorizontal prazoVigencia"
						for="prazoVigencia">Quantidade m�ses</label><br class="quebraLinha2">						
				</div>
				
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="incentivosComerciais">Incentivos Comerciais:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoValorReal" type="text"
					id="incentivosComerciais" name="incentivosComerciais"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,11,2);"
					maxlength="10" size="13"
					<ggas:campoSelecionado campo="incentivosComerciais"/>
					value="<fmt:formatNumber value='${contratoVO.incentivosComerciais}' maxFractionDigits='2' minFractionDigits='2'/>"><br
					class="quebraLinha2">
			</div>
			
			<div class="labelCampo">
				<ggas:labelContrato styleClass="rotulo2Linhas"
					forCampo="incentivosInfraestrutura">Incentivos Infraestrutura:</ggas:labelContrato>
				<input class="campoTexto campo2Linhas campoValorReal" type="text"
					id="incentivosInfraestrutura" name="incentivosInfraestrutura"
					onblur="aplicarMascaraNumeroDecimal(this,2);"
					onkeypress="return formatarCampoDecimal(event,this,11,2);"
					maxlength="10" size="13"
					<ggas:campoSelecionado campo="incentivosInfraestrutura"/>
					value="<fmt:formatNumber value='${contratoVO.incentivosInfraestrutura}' maxFractionDigits='2' minFractionDigits='2'/>"><br
					class="quebraLinha2">
			</div>	
			</div>
			
		</fieldset>
	</div>
	
	<!-- 
	<div id="pagamento" class="agrupamento">
		<fieldset id="conteinerMulta">
			<legend>Pagamento Efetuado em Atraso</legend>
			<div class="conteinerDados">
				<div class="labelCampo">
					<ggas:labelContrato styleClass="rotulo2Linhas"
						forCampo="indicadorMultaAtraso">O contrato � pass�vel de Multa?</ggas:labelContrato>
					<input class="campoRadio campoRadio2Linhas indicadorMulta"
						type="radio" name="indicadorMultaAtraso" id="indicadorMultaSim"
						<ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="true"
						onclick="manipularCamposJurosMulta(this.value, 'percentualMulta');"
						<c:if test="${contratoVO.indicadorMultaAtraso == true}">checked="checked"</c:if>>
					<label class="rotuloRadio rotuloRadio2Linhas indicadorMulta2"
						for="indicadorMultaSim">Sim</label> <input
						class="campoRadio campoRadio2Linhas indicadorMulta" type="radio"
						name="indicadorMultaAtraso" id="indicadorMultaNao"
						<ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="false"
						onclick="manipularCamposJurosMulta(this.value, 'percentualMulta');"
						<c:if test="${contratoVO.indicadorMultaAtraso == false}">checked="checked"</c:if>>
					<label class="rotuloRadio rotuloRadio2Linhas indicadorMulta2"
						for="indicadorMultaNao">N�o</label>
						<br class="quebraLinha" /> 
				
					<label
						class="rotulo rotulo2Linhas campoObrigatorio"
						id="labelpercentualMulta"><span id="spanpercentualMulta"
						class="campoObrigatorioSimbolo2"> </span>Percentual de Multa:</label>
					<%-- 						<ggas:labelContrato forCampo="percentualMulta">Percentual de Multa:</ggas:labelContrato> --%>
					<input class="campoTexto campoHorizontal" type="text"
						id="percentualMulta" name="percentualMulta"
						onblur="aplicarMascaraNumeroDecimal(this,2);"
						onkeypress="return formatarCampoDecimal(event,this,3,2);"
						maxlength="10" size="5"
						<ggas:campoSelecionado campo="indicadorMultaAtraso"/>
						value="${contratoVO.percentualMulta}"> <label
						class="rotuloInformativo rotuloHorizontal"
						for="descontoEfetivoEconomia">%</label>
					<br class="quebraLinha">
				</div>
				
				<div class="labelCampo">
					<ggas:labelContrato styleClass="rotulo2Linhas"
						forCampo="indicadorJurosMora">O contrato � pass�vel de Juros de Mora?</ggas:labelContrato>
					<input class="campoRadio" type="radio" name="indicadorJurosMora"
						id="incluirContrato-indicadorJurosMora"
						<ggas:campoSelecionado campo="indicadorJurosMora"/> value="true"
						onclick="manipularCamposJurosMulta(this.value, 'percentualJurosMora');"
						<c:if test="${contratoVO.indicadorJurosMora == true}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorJurosMoraSim"
						id="incluirContrato-indicadorJurosMoraSimNao">Sim</label> <input
						class="campoRadio" type="radio" name="indicadorJurosMora"
						id="incluirContrato-indicadorJurosMora"
						<ggas:campoSelecionado campo="indicadorJurosMora"/> value="false"
						onclick="manipularCamposJurosMulta(this.value, 'percentualJurosMora');"
						<c:if test="${contratoVO.indicadorJurosMora == false}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorJurosMoraNao"
						id="incluirContrato-indicadorJurosMoraSimNao">N�o</label><br
						class="quebraLinha2" /> <label
						class="rotulo rotulo2Linhas campoObrigatorio"
						id="labelpercentualJurosMora"><span
						id="spanpercentualJurosMora" class="campoObrigatorioSimbolo2">
					</span>Percentual de Juros de Mora:</label>
					<%-- 						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualJurosMora">Percentual de Juros de Mora:</ggas:labelContrato> --%>
					<input class="campoTexto campoHorizontal" type="text"
						id="percentualJurosMora" name="percentualJurosMora"
						onblur="aplicarMascaraNumeroDecimal(this,2);"
						onkeypress="return formatarCampoDecimal(event,this,3,2);"
						maxlength="10" size="5"
						<ggas:campoSelecionado campo="indicadorJurosMora"/>
						value="${contratoVO.percentualJurosMora}"> <label
						class="rotuloInformativo rotuloHorizontal"
						for="descontoEfetivoEconomia">%</label>
					<br class="quebraLinha">
				</div>
				<div class="labelCampo">
					<ggas:labelContrato forCampo="indiceCorrecaoMonetaria">Corre��o Monet�ria:</ggas:labelContrato>
					<select name="indiceCorrecaoMonetaria" id="indiceCorrecaoMonetaria"
						class="campoSelect"
						<ggas:campoSelecionado campo="indiceCorrecaoMonetaria"/>>
						<option value="-1">Selecione</option>
						<c:forEach items="${listaIndiceCorrecaoMonetaria}"
							var="indiceCorrecaoMonetaria">
							<option
								value="<c:out value="${indiceCorrecaoMonetaria.chavePrimaria}"/>"
								<c:if test="${contratoVO.indiceCorrecaoMonetaria == indiceCorrecaoMonetaria.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${indiceCorrecaoMonetaria.descricaoAbreviada}" />
							</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</fieldset>
	</div>
	 -->
</fieldset>
