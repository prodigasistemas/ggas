<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<div id="abaContratoDocsAnexos"  class="agrupamento">
		
	<fieldset class="conteinerPesquisarIncluirAlterar">
	<legend>Documentos Anexos</legend>
		<div class="labelCampo">
		<jsp:include page="/jsp/contrato/contrato/contrato/include/selecionarAnexos.jsp">			
			<jsp:param name="actionAdicionarAnexo" value="adicionarAnexoContrato" />
			<jsp:param name="actionRemoverAnexo" value="removerAnexoContrato" />
			<jsp:param name="actionVisualizarAnexo" value="visualizarAnexoContrato" />
			<jsp:param name="actionLimparCampoArquivo" value="limparCampoArquivoContrato" />
			<jsp:param name="moduloSistema" value="Contrato" />
			<jsp:param name="nomeForm" value="contratoForm" />
		</jsp:include>

		</div>			
	</fieldset>
	
</div>