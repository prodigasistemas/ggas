<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<div id="abaContratoPontoConsumo"  class="agrupamento">
	<legend>Pontos de Consumo</legend>
	<div id="conteinerDados">

		<fieldset class="conteinerPesquisarIncluirAlterar">
			<div class="labelCampo">
				<legend id="legendPesquisarImoveis">
					<span id="asteriscoPesquisarImoveis" class="campoObrigatorioSimbolo">*
					</span>Sele��o dos Pontos de Consumo
				</legend>
				<a name="ancoraPesquisarImovelInclusaoContrato"></a> <input
					name="idImovel" type="hidden" id="idImovel" value=""> <input
					name="Button" class="bottonRightCol2" id="pesquisarImoveisContrato"
					value="Pesquisar Im�veis" type="button"
					onclick="exibirPopupPesquisaImovel();"
					<c:if test="${contratoVO.chavePrimariaPrincipal > 0}">disabled</c:if>>
			</div>	
			<fieldset id="pesquisaImovelContrato" class="conteinerBloco">			
				
				<br/>

				<table class="dataTableGGAS dataTableDialog">
					<thead>
						<tr>
							<th style="width: 20%;" > Im�vel
								<table style="width: 100%;"><thead><tr><th>&nbsp;</th></tr></thead></table>
							</th>
							<th style="width: 75%">
								Pontos de Consumo
								<table style="width: 100%">
									<thead>
										<tr>
											 <th style="width: 3%"><input type='checkbox' name='todos' id='todosImoveis' onclick='controlarCheckBox2();'/></td>
											 <th style="width: 33%" > Descri��o </td>
											 <th style="width: 31%"> Segmento </td>
											 <th style="width: 33%">Ramo de Atividade</td>
										</tr>
									</thead>
								</table>
							</th>
							<th style="width: 10%">	&nbsp;
								<table style="width: 100%;"><thead><tr><th>&nbsp;</th></tr></thead></table>
							</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="classeLinha" value="odd"/>
						<c:forEach items="${listaImovelPontoConsumoVO}" var="imovelPontoConsumoVO" varStatus="i">
							<tr class="${classeLinha}">
								<td class="rotuloTabela" style="text-align: left; width: 20%">
										<c:out value="${imovelPontoConsumoVO.nome}"/>
								</td>
								<td class="colunaSubTable">
									<display:table class="subDataTable"  list="${imovelPontoConsumoVO.listaPontoConsumo}" sort="list" id="pontoConsumo" decorator="br.com.ggas.web.contrato.contrato.decorator.PontoConsumoContratoDecorator" excludedParams="" requestURI="#">
									    <display:column media="html" style="width: 3%" sortable="false">
											<input type="checkbox" name="chavesPrimariasPontoConsumo"
												value="${pontoConsumo.chavePrimaria}"
												id="chavePrimariaPontoConsumo${pontoConsumo.chavePrimaria}">
											<!-- campo hidden para guardar a chave prim�ria do im�vel --> 
											<input type="hidden" name="imovelChavesPonto"
												value="${pontoConsumo.imovel.chavePrimaria}">
										</display:column>
										<display:column property="descricao" sortable="false"  headerClass="tituloTabelaEsq" style="width: 33%" />
										<display:column property="segmentoDescricao" style="width: 31%" sortable="false" />
										<display:column property="ramoAtividadeDescricao" style="width: 33%" sortable="false" />
									    	 
									</display:table>
			
								</td>
								<td style="width: 10%">
										<a class="apagarItemPesquisa"
										onclick="return confirm('Deseja retirar o im�vel?');"
										href="javascript:retirarImovel('${imovelPontoConsumoVO.chavePrimaria}');"
										title="Retirar Im�vel"><img
										src="<c:url value="/imagens/deletar_x.png"/>" /></a>
								</td>
							</tr>
							<c:choose>
								<c:when test="${classeLinha eq 'odd'}">
									<c:set var="classeLinha" value="even"/>
								</c:when>
								<c:otherwise>
									<c:set var="classeLinha" value="odd"/>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</tbody>
				</table>
				
			</fieldset>
		</fieldset>
	</div>
</div>