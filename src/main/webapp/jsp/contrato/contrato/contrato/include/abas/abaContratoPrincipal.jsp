<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script>
	$(function(){
		
		var fluxoDetalhamento = "<c:out value='${fluxoDetalhamento}'/>"
			
		//Verifica se existem campos obrigat�rios e exibe ou esconde a legenda.
		var campoObrigatorioPrincipal = $("#principal .campoObrigatorio").size();
		if (campoObrigatorioPrincipal > 0){
			$("#principal p.legenda").show();
		} else {
			$("#principal p.legenda").hide();				
		};
	});
	
	function calcularDataGarantiaConversao(){
		var inicioGarantiaConversao = document.getElementById('inicioGarantiaConversao');
		var numeroDiasGarantia = document.getElementById('numeroDiasGarantia');
		var fimGarantiaConversao = document.getElementById('fimGarantiaConversao');
	
		if(inicioGarantiaConversao.value != ''&& numeroDiasGarantia.value != ''){

			AjaxService.isDataValida( inicioGarantiaConversao.value, {
	           	callback: function(retorno) {
	           		if (retorno != null && retorno == true) {
	           			AjaxService.adicionarDiasData( inicioGarantiaConversao.value, numeroDiasGarantia.value, {
	        	           	callback: function(novaData) {
	        	           		
	        	           		$(function(){
	        						$("#fimGarantiaConversao").val(fimGarantiaConversao);
	        					});
	        	           			
	                        	fimGarantiaConversao.value = novaData;
	                        }	        	
	        	        	, async:false}
	                    );
		           	} else {
						alert("Data de Inicio de Garantia da Convers�o inv�lida.");
			        }
                }	        	
	        	, async:false}
            );
		} else {
			fimGarantiaConversao.value = '';
		}
	}
	
	function limparAbaPrincipal(){
		document.getElementById('periodoTesteDataInicio').value = '';
		document.getElementById('prazoTeste').value = '';
		document.getElementById('periodoTesteDateFim').value = '';
		document.getElementById('volumeTeste').value = '';
		document.getElementById('faxDDD').value = '';
		document.getElementById('faxNumero').value = '';
		document.getElementById('email').value = '';
		document.getElementById('inicioGarantiaConversao').value = '';
		document.getElementById('numeroDiasGarantia').value = '';
		document.getElementById('fimGarantiaConversao').value = '';	
		ajustarAsteriscosPrincipal();				
	}

	function init(){
		calcularDataGarantiaConversao();	
		ajustarAsteriscosPrincipal();	
	}	
	
	addLoadEvent(init);

	function ajustarAsteriscosPrincipal() {
		if (!$("#periodoTesteDataInicio").attr("disabled")) {
			mostrarAsterisco($("#periodoTesteDataInicio").val(),['periodoTesteDateFim']);
		}
		if (!$("#faxDDD").attr("disabled")) {
			mostrarAsterisco($("#faxDDD").val(),['faxNumero']);
		}
	}
	
</script>

<fieldset id="principal">
	<c:choose>
		<c:when test="${contratoVO.fluxoDetalhamento}">
			<a class="linkHelp" href="<help:help>/detalhamentocontratoabaprincipal.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:when>
		<c:otherwise>
			<a class="linkHelp" href="<help:help>/abaprincipalinclusoalteraoadiodecontratos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		</c:otherwise>
	</c:choose>
	<fieldset id="dadosPrincipalCol1" class="coluna">
		<div class="agrupamento">
			<fieldset id="conteinerPeriodoTestes">
				<legend>Periodo de Testes</legend>
				<fieldset class="conteinerDados">
					<fieldset class="coluna">
						<div class=labelCampo>
							<ggas:labelContrato forCampo="periodoTesteDataInicio">In�cio:</ggas:labelContrato>
							<input class="campoData" type="text" id="periodoTesteDataInicio" name="periodoTesteDataInicio" maxlength="10" <ggas:campoSelecionado campo="periodoTesteDataInicio"/> value="${contratoVO.periodoTesteDataInicio}" onblur="mostrarAsterisco(this.value,['periodoTesteDateFim']);"><br />
						</div>
						<div class=labelCampo>
							<ggas:labelContrato forCampo="prazoTeste">Prazo:</ggas:labelContrato>
							<input class="campoHorizontal" type="text" id="prazoTeste" name="prazoTeste" <ggas:campoSelecionado campo="prazoTeste"/> maxlength="3" size="1" onkeypress="return formatarCampoInteiro(event);" value="${contratoVO.prazoTeste}">
							<label class="rotuloHorizontal rotuloInformativo" for="prazoRevisaoQuantidadesContratadas">dias</label>
						</div>
					</fieldset>
					<fieldset class="colunaEsq">
						<div class=labelCampo>
							<ggas:labelContrato styleId="rotuloDataFimPeriodoTestes" forCampo="periodoTesteDateFim">Fim:</ggas:labelContrato>
							<input class="campoData" type="text" id="periodoTesteDateFim" name="periodoTesteDateFim" maxlength="10" <ggas:campoSelecionado campo="periodoTesteDateFim"/> value="${contratoVO.periodoTesteDateFim}"><br />					
						</div>
						<div class=labelCampo>
							<ggas:labelContrato styleId="rotuloVolumeTeste" forCampo="volumeTeste">Volume:</ggas:labelContrato>
							<input class="campoTexto campoHorizontal" type="text" id="volumeTeste" name="volumeTeste" maxlength="14" size="10" onkeypress="return formatarCampoDecimalPositivo(event,this,7,4);" <ggas:campoSelecionado campo="volumeTeste"/> value="${contratoVO.volumeTeste}">
							<label class="rotuloInformativo" for="vazaoInstantanea">m<span class="expoente">3</span></label>
						</div>
					</fieldset>
				</fieldset>
			</fieldset>
		</div>
		
		<div class="agrupamento">
			<fieldset id="conteinerFaxCaracterOperacional">
				<legend>Contato Operacional</legend>
				<fieldset class="conteinerDados">
					<div class=labelCampo>
						<ggas:labelContrato styleId="rotuloCodigoDDD" forCampo="faxDDD">DDD:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal" type="text" id="faxDDD" name="faxDDD" maxlength="2" size="1" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="faxDDD"/> value="${contratoVO.faxDDD}" onblur="mostrarAsterisco(this.value,['faxNumero']);">
					</div>
					<div class=labelCampo>
						<ggas:labelContrato styleId="rotuloNumeroTelefone" styleClass="rotuloHorizontal" forCampo="faxNumero">N�mero:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal" type="text" id="faxNumero" name="faxNumero" maxlength="8" size="7" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="faxNumero"/> value="${contratoVO.faxNumero}">
					</div>
				</fieldset>
			</fieldset><br class="quebraLinha2" />
		</div>
		
		<div class=labelCampo>
			<ggas:labelContrato styleId="RotuloEmailCaracterOperacional" styleClass="rotuloVertical" forCampo="email">E-mail contato operacional:</ggas:labelContrato>
			<input class="campoTexto campoVertical" type="text" id="email" name="email" maxlength="50" size="50" <ggas:campoSelecionado campo="email"/> value="${contratoVO.email}" onkeypress="return formatarCampoEmail(event)">
		</div>
	</fieldset>
	
	<fieldset id="dadosPrincipalCol2" class="colunaFinal">
	
		<div class=labelCampo>
			<ggas:labelContrato styleId="rotuloComplemento" styleClass="rotulo2Linhas" forCampo="inicioGarantiaConversao">Data de inicio de garantia da convers�o:</ggas:labelContrato>
			<input class="campoData campo2Linhas" type="text" id="inicioGarantiaConversao" name="inicioGarantiaConversao" maxlength="10" <ggas:campoSelecionado campo="inicioGarantiaConversao"/> value="${contratoVO.inicioGarantiaConversao}" onchange="calcularDataGarantiaConversao();"><br />
		</div>
		
		<div class=labelCampo>
			<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="numeroDiasGarantia">Tempo de garantia da convers�o:</ggas:labelContrato>
			
			<c:choose>
				<c:when test="${contratoVO.valorFixoNumeroDiasGarantia}">
					<input class="campoValorFixo" type="text" value="${contratoVO.numeroDiasGarantia}" size="4" readonly="readonly" />
					<input type="hidden" name="numeroDiasGarantia" value="${contratoVO.numeroDiasGarantia}" />
				</c:when>
				<c:otherwise>
					<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="numeroDiasGarantia" name="numeroDiasGarantia" maxlength="3" size="2" onkeypress="return formatarCampoInteiro(event);" <ggas:campoSelecionado campo="numeroDiasGarantia"/> value="${contratoVO.numeroDiasGarantia}" onchange="calcularDataGarantiaConversao();">
					<label class="rotuloInformativo" for="numeroDiasGarantia">dias</label><br class="quebraLinha2" />
				</c:otherwise>
			</c:choose>
		</div>
		
		<div class=labelCampo>
			<ggas:labelContrato styleId="rotuloComplemento" styleClass="rotulo2Linhas" forCampo="numeroDiasGarantia">Data final da garantia da convers�o:</ggas:labelContrato>
			<input class="campoData campo2Linhas" type="text" id="fimGarantiaConversao" name="fimGarantiaConversao" maxlength="10" value="${contratoVO.fimGarantiaConversao}" disabled="disabled">		
		</div>
		
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Contratos.</p>
</fieldset>