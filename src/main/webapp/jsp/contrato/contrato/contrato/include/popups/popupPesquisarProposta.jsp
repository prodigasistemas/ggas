<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>


<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<script language="javascript">

$(document).ready(function() {

	window.opener.esconderIndicador();
	
	$("#dataInicio,#dataFim").datepicker({
		changeYear : true,
		showOn : 'button',
		buttonImage : '<c:url value="/imagens/calendario.gif"/>',
		buttonImageOnly : true,
		buttonText : 'Exibir Calend�rio',
		dateFormat : 'dd/mm/yy'
	});
});

function limparFormulario(){
	$("input[type=text]").attr('value','');
}

function selecionarProposta(idSelecionado) {
	window.opener.selecionarProposta(idSelecionado);
	window.close();
}

function validarFormPesquisaProposta(){
	var form = $("form#formPesquisaPropostaPopup");

	if ($("#dataInicio").attr('value') != undefined && $("#dataInicio").attr('value') != '' 
			&& $("#dataFim").attr('value') != undefined && $("#dataFim").attr('value') != ''){
		if(validarDatas("dataInicio", "dataFim","")){
			form.submit();
		}else{
			alert("Datas Inv�lidas");
		}
	}else{
		form.submit();
	}
}


</script>

	<h1 class="tituloInternoPopup">Pesquisar Proposta</h1>
	<p class="orientacaoInicialPopup">Para pesquisar uma Proposta, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

	<form:form styleId="formPesquisaPropostaPopup" method="post" action="pesquisarPropostaPopup" id="formPesquisaPropostaPopup" name="formPesquisaPropostaPopup"> 

	
	<fieldset id="pesquisarPropostaPopup">		
		<label class="rotulo" for="numeroProposta">N�mero da Proposta:</label>
		<input class="campoTexto" type="text" id="numeroProposta" name="numeroProposta" maxlength="50" size="40" onkeypress="return formatarCampoInteiro(event);" value="" onkeyup="return validarCriteriosParaCampo(this, '1', '1', 'formatarCampoAlfaNumerico(event)');" tabindex="5">

		<br class="quebraLinha2" />
		
		<label class="rotulo" forCampo="dataInicio">Data In�cio:</label>
		<input class="campoData" type="text" id="dataInicio" name="dataInicio" maxlength="10" />

		<br class="quebraLinha2" />

		<label class="rotulo" forCampo="dataFim">Data Fim:</label>
		<input class="campoData" type="text" id="dataFim" name="dataFim" maxlength="10" />
		
		<br class="quebraLinha2" />
		
		<label class="rotulo" for="descricaoImovel">Descri��o Im�vel:</label>
		<input class="campoTexto" type="text" id="descricaoImovel" name="descricaoImovel" maxlength="50" size="40">
		
		<br class="quebraLinha2" />


		<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
			<jsp:param name="cepObrigatorio" value="false"/>
			<jsp:param name="idCampoCep" value="cepImovel"/>
		</jsp:include>
<!-- 		<label class="rotulo" for="cepImovel">Cep Im�vel:</label> -->
<!-- 		<input class="campoTexto" type="text" id="cepImovel" name="cepImovel" maxlength="50" size="40"> -->
		
		<br class="quebraLinha2" />

		<label class="rotulo" for="nomeCliente">Nome Cliente:</label>
		<input class="campoTexto" type="text" id="nomeCliente" name="nomeCliente" maxlength="50" size="40">

		<label class="rotulo" for="cpfCnpj">CPF/CNPJ Cliente:</label>
		<input class="campoTexto" type="text" id="cpfCnpj" name="cpfCnpj" maxlength="50" size="40">
		
		<br class="quebraLinha2" />
		
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		
		<input class="campoRadio" type="radio" name="habilitado" id="ICAtivo" value="true" >
		<label class="rotuloRadio" for="ICAtivo">Ativo</label>
		
		<input class="campoRadio" type="radio" name="habilitado" id="ICInativo" value="false" >
		<label class="rotuloRadio" for="ICInativo">Inativo</label>
		
		<input class="campoRadio" type="radio" name="habilitado" id="ICTodos" value="" checked>
		<label class="rotuloRadio" for="ICTodos">Todos</label>
		
		<br class="quebraLinha2" />
		
		
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol" value="Limpar" type="button" onclick="limparFormulario();">
	    <input name="button" class="bottonRightCol2 botaoGrande1" type="button" value="Pesquisar" onclick="validarFormPesquisaProposta()"  >
 	</fieldset>
 	
 	<c:if test="${listaPropostas ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="listaPropostas" sort="list" id="proposta" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPropostaPopup">
	     	 <display:column style="width: 50px" title="Ativo">
		     	<c:choose>
					<c:when test="${proposta.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
			  <display:column sortable="true" title="N�mero da Proposta" style="width: 100px">
		     	<a href='javascript:selecionarProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.numeroCompletoProposta}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" titleKey="PROPOSTA_IMOVEL"  style="width: 200px" >
		     	<input type="hidden" id="idsQuadraFaces" name="idsQuadraFaces" value="${proposta.imovel.quadraFace.chavePrimaria}" />
		     	<a href='javascript:selecionarProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.imovel.nome}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" title="Proposta /<br />Vers�o" style="width: 240px">
				<a href='javascript:selecionarProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.numeroProposta}'/> / <c:out value='${proposta.versaoProposta}'/>
	        	</a>
	    	 </display:column>
		 	<display:column sortable="true" title="Situa��o<br />da Proposta" style="width: 85px">
		 		<a href='javascript:selecionarProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.situacaoProposta.descricao}'/>
				</a>
	    	</display:column>
	    	<display:column sortable="true" title="Data de<br />Emiss�o" style="width: 75px">
	    		<a href='javascript:selecionarProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${proposta.dataEmissao}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>
		</display:table>
	</c:if>
 
</form:form> 
