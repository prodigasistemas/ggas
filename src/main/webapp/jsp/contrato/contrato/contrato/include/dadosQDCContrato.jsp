<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script>

	function adicionarQDC() {

		var dataQDC = document.getElementById("QDCData");
		var valorQDC = document.getElementById("QDCValor");
		var selectQDC = document.getElementById("QDCContrato");
		var dataValorQDC = "";
		
		if(dataQDC == null || dataQDC.value == "") {
			alert("Digite a data.");
		} else {
			var retorno = true;
			AjaxService.isDataValida( dataQDC.value, {
	           	callback: function(valido) {	          		
	           		if(!valido){  
	           			retorno = false;	     
	           			alert('<fmt:message key="ERRO_DADOS_INVALIDOS"><fmt:param>' + 'Data' + '</fmt:param> </fmt:message>');
	               	}
	        	}	        		        	
	        	, async:false}
	        );
			if(!retorno){
				return;
			}
			
			
			dataValorQDC = dataValorQDC + dataQDC.value + " | ";
			if(valorQDC == null || valorQDC.value == "") {
				alert("Digite o valor do QDC.");
				dataValorQDC = "";
			} else {

				var dateQDC = converterStringBrData(dataQDC.value);
				dataValorQDC = validarDatasQDC(dataValorQDC, dateQDC);

			}
		}
		
		if(dataValorQDC != "") {
			var volumeQDC = valorQDC.value;
			dataValorQDC = dataValorQDC + volumeQDC;

			var contemData = verificarSelectContemData(selectQDC,dataQDC.value);
			
			if(contemData) {
				alert('<fmt:message key="ERRO_DATA_QDC_CONTRATO_EXISTENTE"/>');
			} else {
				addOption(selectQDC,dataValorQDC,dataValorQDC,false);
				ordenarSelectQDC(selectQDC);
				dataQDC.value = "";
				valorQDC.value = "";
			}
		}

	}

    function getDataVencimentoObrigacoes(){
    	var dataVencimentoCont = "${sessionScope.contrato.dataVencimentoObrigacoes}";
		mesString = dataVencimentoCont.substr(4,3);
		diaString = dataVencimentoCont.substr(8,2);
		anoString = dataVencimentoCont.substr(-4,4);
		dataVencimento = new Date(mesString+" "+diaString+", "+anoString);

    	return dataVencimento;
    }
	
	// Valida as datas de assinatura e de vencimento.
	function validarDatasQDC(dataValorQDC,dateQDC) {
		var dataAssinatura = document.getElementById("dataAssinatura");
		var dataVencimento = document.forms[0].dataVencObrigacoesContratuais.value;
		
		if(dataAssinatura != null && dataAssinatura.value != "") {
			if(dataVencimento != null && dataVencimento.value != "") {
				if(dateQDC < converterStringBrData(dataAssinatura.value)) {
					alert("Data do QDC � menor que a data de assinatura.");
					dataValorQDC = "";
					return dataValorQDC;
				}

                //////////////
                var objDataObrigacoesContratuais = document.getElementById("dataVencObrigacoesContratuais");
                var dtVencimento = $('#dataVencObrigacoesContratuais').datepicker('getDate');

                if (dtVencimento == null){
                	dtVencimento = getDataVencimentoObrigacoes();
                }
                
                var dtQDC = $('#QDCData').datepicker('getDate'); 

                //Checa se existe o checkbox "indicadorRenovacaoAutomaticaSim"
                var renovacaoAutomatica = document.forms[0].renovacaoAutomatica;
												
             	if((renovacaoAutomatica != undefined) && (getCheckedValue(renovacaoAutomatica) == 'true') ){
             		var diasRenovacao = document.getElementById("numDiasRenoAutoContrato").value;
   	        	    if ((diasRenovacao != null) && (diasRenovacao > 0)){
   	        		    diasRenovacao = parseInt(diasRenovacao);
   	        		    dtVencimento.setDate(dtVencimento.getDate() +  diasRenovacao);
   	                }
             	}
                
                /////////////
				if(dtQDC > dtVencimento) {					
					alert("Data do QDC � maior que a data de vencimento.");
					dataValorQDC = "";
					return dataValorQDC;
				}
			} else {
				alert('O preenchimento da Data de Vencimento das Obriga��es Contratuais � obrigat�rio para inclus�o do QDC');
				dataValorQDC = "";
			}
		} else {
			alert("Data de assinatura inv�lida.");
			dataValorQDC = "";
		}
		return dataValorQDC;
	}

	// Ordena o select de QDCs pela data.
	function ordenarSelectQDC(selectQDC) {
		var datasQDC = new Array();
		for(var i = 0; i < selectQDC.length-1; i++) {
			for(var j = 0; j < selectQDC.length-1; j++) {
				var dataString = selectQDC[j].value.split(" ")[0];
				var dateQDC = converterStringBrData(dataString);
				var dataComparada = selectQDC[j+1].value.split(" ")[0];
				var dateQDCComparada = converterStringBrData(dataComparada);
				if(dateQDC > dateQDCComparada) {
					swapOptions(selectQDC,j,j+1);
				}
			}
		}
	}

	function converterStringBrData(dataString) {
		var dataFormatada = dataString.substr(3,2) + "/" + dataString.substr(0,2) + "/" + dataString.substr(6,4);		
		var data = new Date(dataFormatada);
		return data;
	}

	function replicarQDC() {
		var dataAssinatura = document.getElementById("dataAssinatura");
		var dataVencimento = document.getElementById("dataVencObrigacoesContratuais");
		var selectQDC = document.getElementById("QDCContrato");
		var QDCReplicados = new Array();

		if(dataAssinatura != null && dataAssinatura.value != "") {
			var dateAss = converterStringBrData(dataAssinatura.value);
			var dateVenc;
			if(dataVencimento != null && dataVencimento.value != "") {
				dateVenc = converterStringBrData(dataVencimento.value);
			} else {
				// se o usu�rio n�o escolheu a Data de Vencimento das Obriga��es
				// Contratuais � poss�vel replicar as QDCs para o ano seguinte
				dateVenc = new Date(dateAss);
				var proximoAno = dateAss.getFullYear() + 1;
				dateVenc.setFullYear(proximoAno);
			}

				var QDCSelecionados = new Array();				
				for(var i = 0; i < selectQDC.length; i++) {
					if(selectQDC[i].selected) {
						QDCSelecionados[QDCSelecionados.length] = selectQDC[i].value;
					}
				}

				if(dateVenc > dateAss && verificarSelecaoQDC(QDCSelecionados)) {
					var anosVigencia = dateVenc.getFullYear() - dateAss.getFullYear();
					
					if(anosVigencia > 0) {
						for(var j = 0; j < QDCSelecionados.length; j++) {
							var dataQDC = QDCSelecionados[j];
							var dataQDCString = dataQDC.split(" ")[0];
							var QDCDate = converterStringBrData(dataQDCString);

						// se o usu�rio n�o escolheu a Data de Vencimento das Obriga��es
						// Contratuais � poss�vel replicar as QDCs para o ano seguinte
						if(dataVencimento == null || dataVencimento.value == "") {
							dateVenc = new Date(QDCDate);
							var proximoAno = QDCDate.getFullYear() + 1;
							dateVenc.setFullYear(proximoAno);
						}

							// Criando as datas para o per�odo de vig�ncia do contrato.							
							for(var k = 0; k < anosVigencia; k++) {
								var anoData = QDCDate.getFullYear();
								QDCDate.setFullYear(anoData+ 1);
								var mesData = parseInt(QDCDate.getMonth())+1;
								if(mesData < 10) {
									mesData = "0" + mesData;
								}
								var dia = parseInt(QDCDate.getDate());
								if(dia < 10) {
									dia = "0" + dia;
								}
								dataQDCString = dia + "/" + mesData + "/" + QDCDate.getFullYear() + " | " + dataQDC.split(" ")[2];
							if(!verificarSelectContemData(selectQDC,dataQDCString) && QDCDate <= dateVenc) {
									QDCReplicados[QDCReplicados.length] = dataQDCString;
								}
							}
						}
					}
				}
		} else {
			alert("Data de assinatura inv�lida.");
		}
		
		for(var i = 0; i < QDCReplicados.length; i++) {
			addOption(selectQDC,QDCReplicados[i],QDCReplicados[i],false);
		}
		ordenarSelectQDC(selectQDC);
	}

	function verificarSelectContemData(select,valor) {
		if(select != undefined && select != null) {
			for(var i = 0; i < select.length; i++) {
				var dataValor = valor.split(" ")[0];
				var dataSelect = select[i].value.split(" ")[0];
				if(dataValor == dataSelect) {
					return true;
				}
			}
		}
		return false;
	}

	function verificarSelecaoQDC(QDCSelecionados) {
		if(QDCSelecionados.length == 0) {
			alert("Selecione um QDC para replicar.");
			return false;
		} else {
			for(var i = 0; i < QDCSelecionados.length; i++) {
				var dataMes = QDCSelecionados[i].split(" ")[0].substr(0,5);
				for(var j = 0; j < QDCSelecionados.length; j++) {
					var dataMesRepetido = QDCSelecionados[j].split(" ")[0].substr(0,5);
					if(j != i && dataMesRepetido == dataMes) {
						alert("Data do QDC repetida.");
						return false;
					}
				}
			}
		}
		return true;
	}

	function limparDadosQDCContrato(){
		document.getElementById('QDCData').value = '';
		document.getElementById('QDCValor').value = '';
		document.getElementById('QDCContrato').length = 0;
	}
	
</script>
<div id="qdc_Contrato">
	<fieldset id="${idConteinerQDC}">
		<legend><c:out value="${nomeComponente}"/></legend>
		<div class="conteinerDados agrupamento">
			<div class="labelCampo">
				<ggas:labelContrato styleId="rotuloQDCData" forCampo="${dataQDC}">In�cio Vig�ncia QDC:</ggas:labelContrato>
				<input class="campoData campoHorizontal" type="text" id="QDCData" name="dataVigenciaQDC" maxlength="10" <ggas:campoSelecionado campo="${dataQDC}"/>>
			</div>
			<div class="labelCampo">
				<ggas:labelContrato styleId="rotuloQDC" styleClass="rotuloHorizontal" forCampo="${volumeQDC}">QDC:</ggas:labelContrato>
				<input class="campoTexto campoHorizontal" type="text" id="QDCValor" name="qdc" maxlength="15" size="10" <ggas:campoSelecionado campo="${volumeQDC}"/> onkeypress="return formatarCampoDecimalPositivo(event, this, 12, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);">
			</div>
			<div class="labelCampo">
				<label class="rotuloInformativo rotuloHorizontal" for="QDCValor">m<span class="expoente">3</span></label>
				<input id="botaoOKQDC" class="bottonRightCol" name="Button" value="OK" type="button" <ggas:campoSelecionado campo="${volumeQDC}"/> onclick="adicionarQDC();" style="height: 27px;"><br class="quebraLinha" />
				<select id="QDCContrato" name="QDCContrato" size="5" multiple="multiple" <ggas:campoSelecionado campo="${volumeQDC}"/>>
					<c:forEach items="${contratoVO.QDCContrato}"  varStatus="loop">
						<option value="<c:out value="${contratoVO.QDCContrato[loop.index]}"/>">
							<c:out value="${contratoVO.QDCContrato[loop.index]}"/>
						</option>		
					</c:forEach>
				</select>
				</div>
			<input id="botaoReplicarQDC" class="bottonRightCol" name="Button" value="Replicar" type="button" <ggas:campoSelecionado campo="${volumeQDC}"/> onclick="replicarQDC();"><br class="quebra2Linhas" />
			<input id="botaoExcluirQDC" class="bottonRightCol" name="Button" value="Excluir" type="button" <ggas:campoSelecionado campo="${volumeQDC}"/> onclick="removeSelectedOptions(document.forms[0].QDCContrato);">
			
			<c:if test="${modalidade eq 'true'}">
				<hr class="linhaSeparadora">
				
				<div class="labelCampo">
					<ggas:labelContrato forCampo="prazoRevizaoQDC">Prazo para revis�o das quantidades contratadas:</ggas:labelContrato>
					<input class="campoData campoHorizontal" type="text" id="prazoRevisaoQuantidadesContratadas" name="prazoRevizaoQDC" <ggas:campoSelecionado campo="prazoRevizaoQDC"/> value="${contratoVO.prazoRevizaoQDC}">
				</div>
			</c:if>
		</div>
	</fieldset>
</div>