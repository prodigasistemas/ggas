<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>

<div id="abaDadosFinanceiros">
	<fieldset id="conteinerDadosFinanceiros">
		<legend>Dados Financeiros</legend>
		<div class="conteinerDados">
			<div class="agrupamento">
				<fieldset id="conteinerCobranca">
					<legend>Cobran�a</legend>
					<div class="conteinerDados">
						<div class="labelCampo">
							<ggas:labelContrato forCampo="participaECartas">Participa e-cartas?</ggas:labelContrato>

							<c:choose>
								<c:when test="${contratoVO.valorFixoParticipaECartas}">
									<c:choose>
										<c:when test="${contratoVO.participaECartas == true }">
											<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />
										</c:when>
										<c:otherwise>
											<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
										</c:otherwise>
									</c:choose>
									<input type="hidden" name="participaECartas" value="${contratoVO.participaECartas}" />
									<br class="quebraLinha" />
								</c:when>
								<c:otherwise>
									<input class="campoRadio" type="radio" name="participaECartas" value="true"
										<ggas:campoSelecionado campo="participaECartas"/>
										   <c:if test="${contratoVO.participaECartas == true}" >checked="checked"</c:if>>
									<label class="rotuloRadio">Sim</label>
									<input class="campoRadio" type="radio" name="participaECartas" value="false"
										<ggas:campoSelecionado campo="participaECartas"/>
										   <c:if test="${contratoVO.participaECartas == false}">checked="checked"</c:if>>
									<label class="rotuloRadio">N�o</label>
									<br class="quebraRadio" />
								</c:otherwise>
							</c:choose>
						</div>

						<div class="labelCampo">
							<ggas:labelContrato forCampo="faturamentoAgrupamento">Faturamento Agrupado?</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoFaturamentoAgrupamento}">
									<c:choose>
										<c:when test="${contratoVO.faturamentoAgrupamento == true }">
											<input class="campoValorFixo" type="text" value="Sim" size="4" reasalvarContratoParcial();donly="readonly" />
										</c:when>
										<c:otherwise>
											<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
										</c:otherwise>
									</c:choose>
									<input type="hidden" name="faturamentoAgrupamento" value="${contratoVO.faturamentoAgrupamento}" />
									<br class="quebraLinha" />
								</c:when>
								<c:otherwise>
									<input class="campoRadio" type="radio" name="faturamentoAgrupamento" id="incluirContrato-faturamentoAgrupamento" value="true" 
										<ggas:campoSelecionado campo="faturamentoAgrupamento"/> onclick="showContainerTipoAgrupamento()" 
										<c:if test="${contratoVO.faturamentoAgrupamento == true}" >checked="checked"</c:if>>
									<label class="rotuloRadio" for="faturamentoAgrupamentoSim" id="incluirContrato-FaturamentoAgrupamentoSimNao">Sim</label> 
									<input class="campoRadio" type="radio" name="faturamentoAgrupamento" id="incluirContrato-faturamentoAgrupamento" value="false"
										onclick="hideContainerTipoAgrupamento()" <ggas:campoSelecionado campo="faturamentoAgrupamento"/>
										<c:if test="${contratoVO.faturamentoAgrupamento == false}">checked="checked"</c:if>>
									<label class="rotuloRadio" for="faturamentoAgrupamentoNao" id="incluirContrato-FaturamentoAgrupamentoSimNao">N�o</label>								
									<br class="quebraRadio" />
								</c:otherwise>
							</c:choose>
							
							<fieldset id="ContainerTipoAgrupamento">
								<ggas:labelContrato forCampo="tipoAgrupamento">Tipo de Agrupamento:</ggas:labelContrato>
								<c:choose>
									<c:when test="${contratoVO.valorFixoTipoAgrupamento}">
										<c:forEach items="${listaTipoAgrupamento}" var="tipoAgrupamento">
											<c:if test="${contratoVO.tipoAgrupamento == tipoAgrupamento.chavePrimaria}">
												<input class="campoValorFixo" type="text" value="${tipoAgrupamento.descricao}" size="15" readonly="readonly" />
												<input type="hidden" name="tipoAgrupamento" value="${tipoAgrupamento.chavePrimaria}" />
											</c:if>
										</c:forEach>
										<br class="quebraLinha" />
									</c:when>
									<c:otherwise>
										<select name="tipoAgrupamento" id="tipoAgrupamento"
											class="campoSelect"
											<ggas:campoSelecionado campo="tipoAgrupamento"/>>
											<option value="-1">Selecione</option>
											<c:forEach items="${listaTipoAgrupamento}" var="tipoAgrupamento">
												<option value="<c:out value="${tipoAgrupamento.chavePrimaria}"/>"
													<c:if test="${contratoVO.tipoAgrupamento == tipoAgrupamento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${tipoAgrupamento.descricao}" />
												</option>
											</c:forEach>
										</select>
										<br class="quebraLinha" />
									</c:otherwise>
								</c:choose>								
							</fieldset>
						</div>
		
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="emissaoFaturaAgrupada">Emiss�o de Fatura<br />Agrupada?</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoEmissaoFaturaAgrupada}">
									<c:choose>
										<c:when test="${contratoVO.emissaoFaturaAgrupada == true }">
											<input class="campoValorFixo" type="text" value="Sim" size="15" readonly="readonly" />							
										</c:when>
										<c:otherwise>
											<input class="campoValorFixo" type="text" value="N�o" size="15" readonly="readonly" />
										</c:otherwise>
									</c:choose>
									<input type="hidden" name="emissaoFaturaAgrupada" value="${contratoVO.emissaoFaturaAgrupada}" />
									<br class="quebraLinha" />
								</c:when>
								<c:otherwise>
									<input class="campoRadio" type="radio" name="emissaoFaturaAgrupada" id="incluirContrato-emissaoFaturaAgrupada"
										<ggas:campoSelecionado campo="emissaoFaturaAgrupada"/> value="true" <c:if test="${contratoVO.emissaoFaturaAgrupada == true}">checked="checked"</c:if>>
									<label class="rotuloRadio" for="emissaoFaturaAgrupadaSim" id="incluirContrato-emissaoFaturaAgrupadaSimNao">Sim</label> 
									
									<input class="campoRadio" type="radio" name="emissaoFaturaAgrupada" id="incluirContrato-emissaoFaturaAgrupada" 
										<ggas:campoSelecionado campo="emissaoFaturaAgrupada"/> value="false" <c:if test="${contratoVO.emissaoFaturaAgrupada == false}">checked="checked"</c:if>>
									<label class="rotuloRadio" for="emissaoFaturaAgrupadaNao" id="incluirContrato-emissaoFaturaAgrupadaSimNao">N�o</label>
									<br class="quebraLinha" /> 
								</c:otherwise>
							</c:choose>							
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="formaCobranca">Tipo de Conv�nio:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoFormaCobranca}">
									<c:forEach items="${listaFormaCobranca}" var="formaCobranca">
										<c:if test="${contratoVO.formaCobranca == formaCobranca.chavePrimaria}">
											<input id="formaCobrancaValorFixo" class="campoValorFixo" type="text" value="<c:out value='${formaCobranca.descricao}' />" size="30" readonly="readonly" />
											<input type="hidden" name="formaCobranca" value="${formaCobranca.chavePrimaria}" />
										</c:if>
									</c:forEach>									
								</c:when>
								<c:otherwise>
									<select class="campoSelect" id="formaCobranca" name="formaCobranca" <ggas:campoSelecionado campo="formaCobranca"/>>
										<c:forEach items="${listaFormaCobranca}" var="formaCobranca">
											<option value="<c:out value="${formaCobranca.chavePrimaria}"/>"
												<c:if test="${contratoVO.formaCobranca == formaCobranca.chavePrimaria}">selected="selected"</c:if>>
												<c:out value="${formaCobranca.descricao}" />
											</option>
										</c:forEach>
									</select>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="labelCampo">
							<%--<ggas:labelContrato forCampo="arrecadadorConvenio">Arrecadador Conv�nio:</ggas:labelContrato>>--%>
							<label class="rotulo" forCampo="arrecadadorConvenio"> Arrecadador Conv�nio: </label>
							<c:choose>
								<c:when test="${contratoVO.valorFixoArrecadadorConvenio}">
									<c:forEach items="${listaArrecadadorConvenio}" var="arrecadadorConvenio">
										<c:if test="${contratoVO.arrecadadorConvenio == arrecadadorConvenio.chavePrimaria}">
											<input class="campoValorFixo" type="text" value="<c:out value='${arrecadadorConvenio.codigoConvenio}' />" size="30" readonly="readonly" />
											<input type="hidden" name="arrecadadorConvenio" value="${arrecadadorConvenio.chavePrimaria}" />
										</c:if>
									</c:forEach>									
								</c:when>
								<c:otherwise>
									<select class="campoSelect" id="arrecadadorConvenio" name="arrecadadorConvenio" <ggas:campoSelecionado campo="arrecadadorConvenio"/>>
										<option value="-1">Selecione</option>
										<c:forEach items="${listaArrecadadorConvenio}" var="arrecadadorConvenio">
											<option value="<c:out value="${arrecadadorConvenio.chavePrimaria}"/>"
												<c:if test="${contratoVO.arrecadadorConvenio == arrecadadorConvenio.chavePrimaria}">selected="selected"</c:if>>
												<c:out value="${arrecadadorConvenio.codigoConvenio} - ${fn:toUpperCase(arrecadadorConvenio.arrecadadorCarteiraCobranca.tipoCarteira.descricao) == 'SEM REGISTRO' ? 'CN' : 'CR'} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
											</option>
										</c:forEach>
									</select>
								</c:otherwise>
							</c:choose>
							<br class="quebraLinha" />
						</div>
						<div class="labelCampo">
							<%--<ggas:labelContrato forCampo="debitoAutomatico">D�bito Autom�tico?</ggas:labelContrato>>--%>
							<label class="rotulo" forCampo="debitoAutomatico"> D�bito Autom�tico? </label>
							<c:choose>
								<c:when test="${contratoVO.valorFixoDebitoAutomatico}">
									<c:choose>
										<c:when test="${contratoVO.debitoAutomatico eq true }">
											<input class="campoValorFixo" type="text" value="Sim" size="4" readonly="readonly" />							
										</c:when>
										<c:otherwise>
											<input class="campoValorFixo" type="text" value="N�o" size="4" readonly="readonly" />
										</c:otherwise>
									</c:choose>
									<input type="hidden" name="debitoAutomatico" value="${contratoVO.debitoAutomatico}" />
									<br class="quebraLinha" />
								</c:when>
								<c:otherwise>
									<input class="campoRadio" type="radio" name="debitoAutomatico" id="incluirContrato-debitoAutomatico" value="true" 
										 onclick="showContainerDebitoAutomatico()" 
										<c:if test="${contratoVO.debitoAutomatico eq true}" >checked="checked"</c:if>>
									<label class="rotuloRadio" for="debitoAutomaticoSim" id="incluirContrato-DebitoAutomaticoSimNao">Sim</label> 
									<input class="campoRadio" type="radio" name="debitoAutomatico" id="incluirContrato-debitoAutomatico" value="false"
										onclick="hideContainerDebitoAutomatico()" 
										<c:if test="${contratoVO.debitoAutomatico eq false or empty contratoVO.debitoAutomatico}" >checked="checked"</c:if> />
									<label class="rotuloRadio" for="debitoAutomaticoNao" id="incluirContrato-DebitoAutomaticoSimNao">N�o</label>								
									<br class="quebraRadio" />
								</c:otherwise>
							</c:choose>
							<br class="quebraLinha" />
						</div>
						<br class="quebraLinha" />
						<fieldset id="ContainerDebitoAutomatico">
							<%--<ggas:labelContrato forCampo="arrecadadorConvenioDebitoAutomatico">Arrecadador Conv�nio para D�bito Autom�tico:</ggas:labelContrato>--%>
								<label class="rotulo" forCampo="arrecadadorConvenioDebitoAutomatico"> Arrecadador Conv�nio para D�bito Autom�tico: </label>
							<c:choose>
								<c:when test="${contratoVO.valorFixoArrecadadorConvenioDebitoAutomatico}">
									<c:forEach items="${listaArrecadadorConvenioDebitoAutomatico}" var="arrecadadorConvenio">
										<c:if test="${contratoVO.arrecadadorConvenioDebitoAutomatico == arrecadadorConvenio.chavePrimaria}">
											<input class="campoValorFixo" type="text" value="<c:out value='${arrecadadorConvenio.codigoConvenio}' />" size="30" readonly="readonly" />
											<input type="hidden" name="arrecadadorConvenioDebitoAutomatico" value="${arrecadadorConvenio.chavePrimaria}" />
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<select class="campoSelect" id="arrecadadorConvenioDebitoAutomatico" name="arrecadadorConvenioDebitoAutomatico" >
										<option value="-1">Selecione</option>
										<c:forEach items="${listaArrecadadorConvenioDebitoAutomatico}" var="arrecadadorConvenio">
											<option value="<c:out value="${arrecadadorConvenio.chavePrimaria}"/>"
												<c:if test="${arrecadadorConvenio.chavePrimaria == arrecadadorConvenioDebitoAutomatico}">selected="selected"</c:if>>
												<c:out value="${arrecadadorConvenio.codigoConvenio} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
											</option>
										</c:forEach>
									</select>
								</c:otherwise>
							</c:choose>


							<div class="labelCampo">
								<%--<ggas:labelContrato forCampo="banco">Banco:</ggas:labelContrato>--%>
								<label class="rotulo" forCampo="banco"> Banco: </label>
								<c:choose>
									<c:when test="${contratoVO.valorFixoBanco}">
										<c:forEach items="${listaBanco}" var="banco">
											<c:if test="${contratoVO.banco == banco.chavePrimaria}">
												<input class="campoValorFixo" type="text" value="<c:out value='${banco.nome}' />" size="30" readonly="readonly" />
												<input type="hidden" name="banco" value="${banco.chavePrimaria}" />
											</c:if>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<select class="campoSelect" id="banco" name="banco" >
											<option value="-1">Selecione</option>
											<c:forEach items="${listaBanco}" var="bancoSistema">
												<option value="<c:out value="${bancoSistema.chavePrimaria}"/>"
														<c:if test="${banco == bancoSistema.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${bancoSistema.nome} - ${bancoSistema.nomeAbreviado}" />
												</option>
											</c:forEach>
										</select>
									</c:otherwise>
								</c:choose>

							</div>
							<br class="quebraLinha" />
							<div class="labelCampo">
								<%--<ggas:labelContrato forCampo="agencia" >Ag�ncia:</ggas:labelContrato>--%>
								<label class="rotulo" forCampo="agencia"> Ag�ncia: </label>
								<c:choose>
									<c:when test="${contratoVO.valorFixoAgencia}">
										<input class="campoValorFixo" type="text" value="${agencia}" size="15" readonly="readonly" />
										<input type="hidden" name="agencia" value="${agencia}" />
									</c:when>
									<%--onkeypress="return formatarCampoInteiro(event);"--%>
									<c:otherwise>
										<input class="campoTexto campoHorizontal" id="agencia" name="agencia" type="text"
											   onkeypress="return formatarCampoInteiro(event);" onkeyup="validarCampoInteiro(this)"
											    maxlength="10"
											   value="<c:out value='${agencia}' />" size="10" />
									</c:otherwise>
								</c:choose>

							</div>
							<br class="quebraLinha" />
							<div class="labelCampo">
								<%--<ggas:labelContrato forCampo="contaCorrente">Conta Corrente:</ggas:labelContrato>--%>
									<label class="rotulo" forCampo="agencia"> Conta Corrente: </label>
								<c:choose>
									<c:when test="${contratoVO.valorFixoContaCorrente}">
										<input class="campoValorFixo" type="text" value="${contaCorrente}" size="15" readonly="readonly" />
										<input type="hidden" name="contaCorrente" value="${contaCorrente}" />
									</c:when>
									<%--onkeypress="return formatarCampoInteiro(event);"--%>
									<c:otherwise>
										<input class="campoTexto campoHorizontal" id="contaCorrente" name="contaCorrente" type="text"
											   onkeypress="return formatarCampoInteiro(event);" onkeyup="validarCampoInteiro(this)"
											    maxlength="10"
											   value="<c:out value='${contaCorrente}' />" size="10" />
									</c:otherwise>
								</c:choose>

							</div>
						</fieldset>
					</div>
				</fieldset>
			</div>
			<div class="agrupamento">			
				<fieldset id="conteinerMulta">
					<legend>Pagamento Efetuado em Atraso</legend>
					<div class="conteinerDados">
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="indicadorMultaAtraso">O contrato � pass�vel de Multa?</ggas:labelContrato>
						
							<c:choose>
								<c:when test="${contratoVO.valorFixoIndicadorMultaAtraso}">
									<c:choose>
										<c:when test="${contratoVO.indicadorMultaAtraso == true }">
											<input class="campoValorFixo" type="text" value="Sim" size="15" readonly="readonly" />							
										</c:when>
										<c:otherwise>
											<input class="campoValorFixo" type="text" value="N�o" size="15" readonly="readonly" />
										</c:otherwise>
									</c:choose>
									<input type="hidden" name="indicadorMultaAtraso" value="${contratoVO.indicadorMultaAtraso}" />
								</c:when>
								<c:otherwise>
									<input class="campoRadio campoRadio2Linhas indicadorMulta" type="radio" name="indicadorMultaAtraso" id="indicadorMultaSim"
										<ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="true" onclick="manipularCamposJurosMulta(this.value, 'percentualMulta');"
										<c:if test="${contratoVO.indicadorMultaAtraso == true}">checked="checked"</c:if>>					
									<label class="rotuloRadio rotuloRadio2Linhas indicadorMulta2" for="indicadorMultaSim">Sim</label> 
									
									<input class="campoRadio campoRadio2Linhas indicadorMulta" type="radio" name="indicadorMultaAtraso" id="indicadorMultaNao"
										<ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="false" onclick="manipularCamposJurosMulta(this.value, 'percentualMulta');"
										<c:if test="${contratoVO.indicadorMultaAtraso == false}">checked="checked"</c:if>>
									<label class="rotuloRadio rotuloRadio2Linhas indicadorMulta2" for="indicadorMultaNao">N�o</label>
								</c:otherwise>
							</c:choose>
							
							<br class="quebraLinha" /> 
							
							<%-- 						<ggas:labelContrato forCampo="percentualMulta">Percentual de Multa:</ggas:labelContrato> --%>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoPercentualMulta}">
									<label class="rotulo">Percentual de Multa:</label>
									<input class="campoValorFixo" type="text" value="${contratoVO.percentualMulta}%" size="15" readonly="readonly" />
									<input type="hidden" name="percentualMulta" value="${contratoVO.percentualMulta}" />
								</c:when>
								<c:otherwise>
									<label class="rotulo rotulo2Linhas campoObrigatorio" id="labelpercentualMulta"><span id="spanpercentualMulta" class="campoObrigatorioSimbolo2"> </span>Percentual de Multa:</label>
									<input class="campoTexto campoHorizontal" type="text" id="percentualMulta" name="percentualMulta" onblur="aplicarMascaraNumeroDecimal(this,2);"
										onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5"
										<ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="${contratoVO.percentualMulta}"> 
									<label class="rotuloInformativo rotuloHorizontal" for="descontoEfetivoEconomia">%</label><br class="quebraLinha">
								</c:otherwise>
							</c:choose>
						</div>
						
							<br class="quebraLinha" />
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="indicadorJurosMora">O contrato � pass�vel de Juros de Mora?</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoIndicadorJurosMora}">
									<c:choose>
										<c:when test="${contratoVO.indicadorJurosMora == true }">
											<input class="campoValorFixo" type="text" value="Sim" size="15" readonly="readonly" />							
										</c:when>
										<c:otherwise>
											<input class="campoValorFixo" type="text" value="N�o" size="15" readonly="readonly" />
										</c:otherwise>
									</c:choose>
									<input type="hidden" name="indicadorJurosMora" value="${contratoVO.indicadorJurosMora}" />
								</c:when>
								<c:otherwise>
									<input class="campoRadio" type="radio" name="indicadorJurosMora" id="incluirContrato-indicadorJurosMora" <ggas:campoSelecionado campo="indicadorJurosMora"/> value="true"
										onclick="manipularCamposJurosMulta(this.value, 'percentualJurosMora');" <c:if test="${contratoVO.indicadorJurosMora == true}">checked="checked"</c:if>>
									<label class="rotuloRadio" for="indicadorJurosMoraSim" id="incluirContrato-indicadorJurosMoraSimNao">Sim</label> 
									
									<input class="campoRadio" type="radio" name="indicadorJurosMora" id="incluirContrato-indicadorJurosMora" <ggas:campoSelecionado campo="indicadorJurosMora"/> value="false"
										onclick="manipularCamposJurosMulta(this.value, 'percentualJurosMora');" <c:if test="${contratoVO.indicadorJurosMora == false}">checked="checked"</c:if>>
									<label class="rotuloRadio" for="indicadorJurosMoraNao" id="incluirContrato-indicadorJurosMoraSimNao">N�o</label>								
								</c:otherwise>
							</c:choose>
							
							<br class="quebraLinha2" /> 
							
							<label class="rotulo rotulo2Linhas campoObrigatorio" id="labelpercentualJurosMora">
								<span id="spanpercentualJurosMora" class="campoObrigatorioSimbolo2"> </span>Percentual de Juros de Mora:
							</label>
						<%-- 						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualJurosMora">Percentual de Juros de Mora:</ggas:labelContrato> --%>
							<c:choose>
								<c:when test="${contratoVO.valorFixoPercentualJurosMora}">
									<input class="campoValorFixo" type="text" value="${contratoVO.percentualJurosMora}%" size="15" readonly="readonly" />
									<input type="hidden" name="percentualJurosMora" value="${contratoVO.percentualJurosMora}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campoHorizontal" type="text" id="percentualJurosMora" name="percentualJurosMora" onblur="aplicarMascaraNumeroDecimal(this,2);"
										onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5" <ggas:campoSelecionado campo="indicadorJurosMora"/> value="${contratoVO.percentualJurosMora}"> 
									<label class="rotuloInformativo rotuloHorizontal" for="descontoEfetivoEconomia">%</label>
								</c:otherwise>
							</c:choose>							
							<br class="quebraLinha">
						</div>
						
						<div class="labelCampo">
							<ggas:labelContrato forCampo="indiceCorrecaoMonetaria">Corre��o Monet�ria:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoIndiceCorrecaoMonetaria}">
									<c:forEach items="${listaIndiceCorrecaoMonetaria}" var="indiceCorrecaoMonetaria">
										<c:if test="${contratoVO.indiceCorrecaoMonetaria == indiceCorrecaoMonetaria.chavePrimaria}">
											<input class="campoValorFixo" type="text" value="${indiceCorrecaoMonetaria.descricaoAbreviada}" size="15" readonly="readonly" />
											<input type="hidden" name="indiceCorrecaoMonetaria" value="${indiceCorrecaoMonetaria.chavePrimaria}" />
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<select name="indiceCorrecaoMonetaria" id="indiceCorrecaoMonetaria" class="campoSelect" <ggas:campoSelecionado campo="indiceCorrecaoMonetaria"/>>
										<option value="-1">Selecione</option>
										<c:forEach items="${listaIndiceCorrecaoMonetaria}"
											var="indiceCorrecaoMonetaria">
											<option value="<c:out value="${indiceCorrecaoMonetaria.chavePrimaria}"/>"
												<c:if test="${contratoVO.indiceCorrecaoMonetaria == indiceCorrecaoMonetaria.chavePrimaria}">selected="selected"</c:if>>
												<c:out value="${indiceCorrecaoMonetaria.descricaoAbreviada}" />
											</option>
										</c:forEach>
									</select>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</fieldset>
			</div>
	
			<div class="agrupamento">
				<fieldset id="conteinerGarantia">
					<legend>Garantia</legend>
					<div class="conteinerDados">
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="tipoGarantiaFinanceira">Tipo da Garantia Financeira:</ggas:labelContrato>
							
							<c:choose>
								<c:when test="${contratoVO.valorFixoTipoGarantiaFinanceira}">							
									<c:forEach items="${listaTipoGarantiaFinanceira}" var="tipoGarantiaFinanceira">
										<c:if test="${contratoVO.tipoGarantiaFinanceira == tipoGarantiaFinanceira.chavePrimaria}">
											<input class="campoValorFixo" type="text" value="${tipoGarantiaFinanceira.descricao}" size="50" readonly="readonly" />
											<input type="hidden" name="tipoGarantiaFinanceira" value="${tipoGarantiaFinanceira.chavePrimaria}" />
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<select name="tipoGarantiaFinanceira" id="tipoGarantiaFinanceira" class="campoSelect campo2Linhas" <ggas:campoSelecionado campo="tipoGarantiaFinanceira"/>>
										<option value="-1">Selecione</option>
										<c:forEach items="${listaTipoGarantiaFinanceira}" var="tipoGarantiaFinanceira">
											<option value="<c:out value="${tipoGarantiaFinanceira.chavePrimaria}"/>"
												<c:if test="${contratoVO.tipoGarantiaFinanceira == tipoGarantiaFinanceira.chavePrimaria}">selected="selected"</c:if>>
												<c:out value="${tipoGarantiaFinanceira.descricao}" />
											</option>
										</c:forEach>
									</select>
								</c:otherwise>
							</c:choose>	
						</div>
						<br class="quebraLinha" />
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="descGarantiaFinanc">Descri��o da Garantia Financeira:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoDescGarantiaFinanc}">
									<input class="campoValorFixo" type="text" value="${contratoVO.descGarantiaFinanc}" size="40" readonly="readonly" />
									<input type="hidden" name="descGarantiaFinanc" value="${contratoVO.descGarantiaFinanc}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campo2Linhas" type="text" id="descGarantiaFinanc" name="descGarantiaFinanc" maxlength="40"
										size="40" <ggas:campoSelecionado campo="descGarantiaFinanc"/> value="${contratoVO.descGarantiaFinanc}">
								</c:otherwise>
							</c:choose>
						</div>
						<br class="quebraLinha" />
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="valorGarantiaFinanceira">Valor da Garantia Financeira:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoValorGarantiaFinanceira}">
									<input class="campoValorFixo" type="text" value="${contratoVO.valorGarantiaFinanceira}" size="15" readonly="readonly" />
									<input type="hidden" name="valorGarantiaFinanceira" value="${contratoVO.valorGarantiaFinanceira}" />
								</c:when>
								<c:otherwise>
									<input class="campoTexto campo2Linhas campoValorReal" type="text" id="valorGarantiaFinanceira" name="valorGarantiaFinanceira" onblur="aplicarMascaraNumeroDecimal(this,2);"
										onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="valorGarantiaFinanceira"/>
										value="<fmt:formatNumber value='${contratoVO.valorGarantiaFinanceira}' maxFractionDigits='2' minFractionDigits='2'/>">
								</c:otherwise>
							</c:choose>
						</div>

						<br class="quebraLinha" />
						
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo2Linhas labelContratoGarantia" forCampo="garantiaFinanceiraRenovada">A garantia �?</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoGarantiaFinanceiraRenovada }">
									<c:forEach items="${listaGarantiaFinanceiraRenovada}" var="garantiaFinanceiraRenovada">
										<c:if test="${contratoVO.garantiaFinanceiraRenovada == garantiaFinanceiraRenovada}">
											<input class="campoValorFixo" type="text" value="${garantiaFinanceiraRenovada}" size="15" readonly="readonly" />
											<input type="hidden" id="garantiaFinanceiraRenovada" name="garantiaFinanceiraRenovada" value="${garantiaFinanceiraRenovada}" />
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<select name="garantiaFinanceiraRenovada" id="garantiaFinanceiraRenovada" class="campoSelect campo2Linhas" onchange="mostrarAsteriscoGarantiaFinanceira(this.value);"
										<ggas:campoSelecionado campo="garantiaFinanceiraRenovada"/>>
										
										<option value="-1">Selecione</option>
										<c:forEach items="${listaGarantiaFinanceiraRenovada}" var="garantiaFinanceiraRenovada">
											<option value="<c:out value="${garantiaFinanceiraRenovada}"/>"
												<c:if test="${contratoVO.garantiaFinanceiraRenovada == garantiaFinanceiraRenovada}">selected="selected"</c:if>>
												<c:out value="${garantiaFinanceiraRenovada}" />
											</option>
										</c:forEach>
									</select>
								</c:otherwise>
							</c:choose>	
						</div>
						
						<fieldset id="ConteinerTempoAntecedenciaRevisaoGarantias">
							<div class="labelCampo">
								<ggas:labelContrato styleClass="rotulo2Linhas"
									forCampo="dataInicioGarantiaFinanceira">Data de In�cio da Garantia Financeira:</ggas:labelContrato>
								<input class="campoData campo2Linhas" type="text"
									id="dataInicioGarantiaFinanceira"
									name="dataInicioGarantiaFinanceira" maxlength="10"
									<ggas:campoSelecionado campo="dataInicioGarantiaFinanceira"/>
									value="${contratoVO.dataInicioGarantiaFinanceira}">
									<br class="quebraLinha" />
							</div>
							<div class="labelCampo">
								<ggas:labelContrato styleClass="rotulo2Linhas"
									forCampo="dataFinalGarantiaFinanceira">Data Final da Garantia Financeira:</ggas:labelContrato>
								<input class="campoData campo2Linhas" type="text"
									id="dataFinalGarantiaFinanceira"
									name="dataFinalGarantiaFinanceira" maxlength="10"
									<ggas:campoSelecionado campo="dataFinalGarantiaFinanceira"/>
									value="${contratoVO.dataFinalGarantiaFinanceira}">
									<br class="quebraDataRadio" />
							</div>
							
							<br class="quebraLinha" />
							
							<div class="labelCampo">
								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="tempoAntecRevisaoGarantias">Tempo de Anteced�ncia para Revis�o das Garantias:</ggas:labelContrato>
								<c:choose>
									<c:when test="${contratoVO.valorFixoTempoAntecRevisaoGarantias}">
										<input class="campoValorFixo" type="text" value="${contratoVO.tempoAntecRevisaoGarantias} dias" size="15" readonly="readonly" />
										<input type="hidden" name="tempoAntecRevisaoGarantias" value="${contratoVO.tempoAntecRevisaoGarantias}" />
									</c:when>
									<c:otherwise>
										<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="tempoAntecRevisaoGarantias" name="tempoAntecRevisaoGarantias" maxlength="3" size="2"
											<ggas:campoSelecionado campo="tempoAntecRevisaoGarantias"/> value="${contratoVO.tempoAntecRevisaoGarantias}" onkeypress="return formatarCampoInteiro(event);"> 
										<label class="rotuloInformativo3Linhas rotuloHorizontal">dias</label>
									</c:otherwise>
								</c:choose>
							</div>
						</fieldset>
						<fieldset id="ConteinerPeriodicidadeReavalicacaoGarantias">
							<div class="labelCampo">
								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="periodicidadeReavGarantias">Periodicidade para reavalia��o das Garantias:</ggas:labelContrato>
								<c:choose>
									<c:when test="${contratoVO.valorFixoPeriodicidadeReavGarantias}">
										<input class="campoValorFixo" type="text" value="${contratoVO.periodicidadeReavGarantias} dias" size="15" readonly="readonly" />
										<input type="hidden" name="periodicidadeReavGarantias" value="${contratoVO.periodicidadeReavGarantias}" />
									</c:when>
									<c:otherwise>
										<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="periodicidadeReavGarantias" name="periodicidadeReavGarantias" maxlength="3" size="2"
											<ggas:campoSelecionado campo="periodicidadeReavGarantias"/> value="${contratoVO.periodicidadeReavGarantias}" onkeypress="return formatarCampoInteiro(event);"> 
										<label class="rotuloInformativo3Linhas rotuloHorizontal">dias</label>
									</c:otherwise>
								</c:choose>
							</div>
						</fieldset>
					</div>
				</fieldset>
			</div>
		
			<div class="agrupamento">
				<fieldset id="conteinerMultaRecisoria">
					<legend>Multa Rescis�ria</legend>
					<div class="conteinerDados">
						<div class="labelCampo">
							<ggas:labelContrato styleClass="rotulo" forCampo="multaRecisoria">Multa Rescis�ria:</ggas:labelContrato>
							<c:choose>
								<c:when test="${contratoVO.valorFixoMultaRecisoria}">
									<c:forEach items="${listaTipoMultaRecisoria}" var="multaRecisoria">
										<c:if test="${contratoVO.multaRecisoria == multaRecisoria.chavePrimaria}">
											<input class="campoValorFixo" type="text" value="${multaRecisoria.descricao}" size="50" readonly="readonly" />
											<input type="hidden" name="multaRecisoria" value="${multaRecisoria.chavePrimaria}" />
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<select name="multaRecisoria" id="multaRecisoria" class="campoSelect campo2Linhas" <ggas:campoSelecionado campo="multaRecisoria"/>>
										<option value="-1">Selecione</option>
										<c:forEach items="${listaTipoMultaRecisoria}" var="multaRecisoria">
											<option value="<c:out value="${multaRecisoria.chavePrimaria}"/>"
												<c:if test="${contratoVO.multaRecisoria == multaRecisoria.chavePrimaria}">selected="selected"</c:if>>
												<c:out value="${multaRecisoria.descricao}" />
											</option>
										</c:forEach>
									</select>								
								</c:otherwise>
							</c:choose>
							<br class="quebraLinha2" />
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</fieldset>
</div>
