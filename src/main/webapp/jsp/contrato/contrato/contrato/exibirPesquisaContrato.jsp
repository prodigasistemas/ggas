<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Contrato<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script language="javascript">

	$(document).ready(function(){

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		//s� executa se a funcionalidade de migra��o do modelo do contrato for realizada
		var logErroMigracaoContrato = "<c:out value="${sessionScope.logErroMigracao}"/>";
		if(logErroMigracaoContrato != ""){
			submeter('contratoForm', 'exibirLogErroMigracaoContrato');
		}
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};			
		
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	
	function limparFormulario(){
		
		$("#fluxoAlteracao").val(false);
		$("#fluxoAditamento").val(false);
		$("#fluxoDetalhamento").val(false);
		$("#fluxoInclusao").val(false);
		
		document.contratoForm.numeroContrato.value = "";
		document.contratoForm.habilitado[0].checked = true;	
		
    	limparCamposPesquisa();
    	
    	document.contratoForm.indicadorPesquisa[0].checked = false;
    	document.contratoForm.indicadorPesquisa[1].checked = false;
    	
    	$("#faturavel_todos").prop('checked', true);
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	
	function incluir() {
		$("#fluxoInclusao").val(true);
		$("#numeroContrato").val('');
		$("#idModeloContrato").val('-1');
		
		submeter("contratoForm", "exibirInclusaoContrato");
	}
	
	function incluirComplementar() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimariaPrincipal.value = obterValorUnicoCheckboxSelecionado();
			$("#fluxoInclusao").val(true);
			$("#numeroContrato").val('');
			$("#idModeloContrato").val('-1');
			submeter("contratoForm", "exibirInclusaoContrato");
		}
	}
	
	function ordenarContratos() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("contratoForm", "exibirOrdemFaturamentoContrato");
		}
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function removerContrato(){
		var selecao = verificarSelecao();
		if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('contratoForm', 'excluirContrato');
				}
	    }
	} 	
	
	function exibirDadosFaturamentoContrato(){
		var selecao = verificarSelecao();
		if (selecao == true) {
			window.open('about:blank','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
			submeter('contratoForm', 'exibirDadosFaturamentoContratoPopup', "popup");
	    }
	}

	function migrarModeloContrato(){
		var selecao = verificarSelecao();
		if (selecao == true) {
			$("#idModeloContrato").val("-1");
			submeter('contratoForm', 'exibirMigracaoModeloContratoFluxoContrato');
	    }
	}
	function exibirMigrarSaldo(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('contratoForm', 'exibirMigrarSaldo');
		}
	}	

	function detalharContrato(chave) {
		document.forms[0].chavePrimaria.value = chave;
		$("#fluxoDetalhamento").val(true);
		submeter("contratoForm", "exibirDetalhamentoContrato");
	}

	function aditarContrato(){
		
		var selecao = verificarSelecaoApenasUm();
		
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			//document.forms[0].fluxoAlteracao.value = "false";
			document.forms[0].fluxoAditamento.value = true;
			submeter("contratoForm", "exibirAditamentoContrato");
		}
		
	}

	function alterarContrato(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			document.forms[0].fluxoAlteracao.value = "true";
			submeter("contratoForm", "exibirAditamentoContrato");
		}
	}

	function encerrarRescindirContrato(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("contratoForm", "exibirPontoConsumoContrato");
		}
	}
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}
	
	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		document.getElementById('situacaoContrato').value = "-1";
		document.getElementById('idModeloContrato').value = "-1";
		document.getElementById('idTipoContrato').value = "-1";
		
	}
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function carregarModeloContrato(elem){
		
		var idTipoContrato = elem.value;
	  	var selectModelo = document.getElementById("idModeloContrato");
	  	var idModeloContrato = "${contratoVO.idModeloContrato}";		  	

	  	selectModelo.length=0;
	  	var novaOpcao = new Option("Selecione","-1");
	  	selectModelo.options[selectModelo.length] = novaOpcao;
	   	AjaxService.listarModeloContratoPorTipoContrato(idTipoContrato,
	   			function(funcoes) {            		      		         		
	           	for (key in funcoes){
	                var novaOpcao = new Option(funcoes[key], key);
	                if (key == idModeloContrato){
	                	novaOpcao.selected = true;
	                }
	                selectModelo.options[selectModelo.length] = novaOpcao;		            			            	
	        	}
	        	ordernarSelect(selectModelo)
	    	}
	    );	  	
	}

	function init() {
		
		var aditamentoContratoSalvoSucesso = '${aditamentoContratoSalvoSucesso}';
		var alteracaoContratoSalvoSucesso = '${alteracaoContratoSalvoSucesso}';
		
		if(aditamentoContratoSalvoSucesso || alteracaoContratoSalvoSucesso){
			limparCamposPesquisa();
		}
		
		<c:choose>
			<c:when test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
	}	
	
	addLoadEvent(init);		
	
	function pesquisar(){
		
		submeter("contratoForm", "pesquisarContrato");
	}

	function imprimirContrato() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			window.open('exibirImprimirContrato?postBack=true&chavePrimaria='+obterValorUnicoCheckboxSelecionado(),'popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
		}
	}

	function aprovaContrato(){
		var chavesPrimarias = obterValoresCheckboxesSelecionados();
		if (chavesPrimarias.length > 0){
			$("#chavePrimarias").val(chavesPrimarias);
			submeter("contratoForm", "aprovarContrato");
		}else{
			alert("Selecione um ou mais contratos para aprovar.");
		}
	}
					
	</script>

<form:form method="post" action="pesquisarContrato" id="contratoForm" name="contratoForm"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >

	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="chavePrimariaPrincipal" type="hidden" id="chavePrimariaPrincipal">
	
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${contratoVO.fluxoAlteracao}"/>
	<input name="fluxoAditamento" type="hidden" id="fluxoAditamento" value="${contratoVO.fluxoAditamento}"/>
	<input name="fluxoDetalhamento" type="hidden" id="fluxoDetalhamento" value="${contratoVO.fluxoDetalhamento}">
	<input name="fluxoInclusao" type="hidden" id="fluxoInclusao" value="${contratoVO.fluxoInclusao}">
	
	<fieldset id="pesquisarContrato" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${contratoVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${contratoVO.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${contratoVO.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${contratoVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${contratoVO.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${contratoVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${contratoVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${contratoVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${contratoVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${contratoVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${contratoVO.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${contratoVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${contratoVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${contratoVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${contratoVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${contratoVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${contratoVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<fieldset class="coluna" style="margin-top:30px ;margin-right:40px; padding-right: 70px">
				<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:<!-- fix bug --></label>
				<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="9" size="8" value="${contratoVO.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"><br />
				<label class="rotulo" for="habilitado">Situa��o do Contrato:</label>
				<select class="campoSelect" id="situacaoContrato" name="situacaoContrato">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSituacaoContrato}" var="situacaoContrato">
						<option value="<c:out value="${situacaoContrato.chavePrimaria}"/>" <c:if test="${contratoVO.situacaoContrato == situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${situacaoContrato.descricao}"/>
						</option>		
			    	</c:forEach>
				</select>
			</fieldset>
			
			<fieldset class="colunaDir" style="margin-top:30px ;margin-right:30px;">
				<label class="rotulo" for="habilitado">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="ativo" value="true" <c:if test="${contratoVO.habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="ativo">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="inativo" value="false" <c:if test="${contratoVO.habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="inativo">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="todos" value="" <c:if test="${empty contratoVO.habilitado}">checked</c:if>>
				<label class="rotuloRadio" for="todos">Todos</label>
				<br />
				<label class="rotulo" for="habilitado">Tipo de Contrato:</label>
				<select class="campoSelect" id="idTipoContrato" name="idTipoContrato">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoContrato}" var="tipoContrato">
						<option value="<c:out value="${tipoContrato.chavePrimaria}"/>" <c:if test="${contratoVO.idTipoContrato == tipoContrato.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoContrato.descricao}"/>
						</option>		
			    	</c:forEach>
				</select>
				<br />
				<label class="rotulo" for="habilitado">Modelo de Contrato:</label>
				<select class="campoSelect" id="idModeloContrato" name="idModeloContrato">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaModelosContrato}" var="modeloContrato">
						<option value="<c:out value="${modeloContrato.chavePrimaria}"/>" <c:if test="${contratoVO.idModeloContrato == modeloContrato.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${modeloContrato.descricao}"/>
						</option>		
			    	</c:forEach>
				</select>
				
				<label class="rotulo" for="habilitado">Fatur�vel:</label>
				<input class="campoRadio" type="radio" name="faturavel" id="faturavel_sim" value="true" <c:if test="${contratoVO.faturavel eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="faturavel_sim">Sim</label>
				<input class="campoRadio" type="radio" name="faturavel" id="faturavel_nao" value="false" <c:if test="${contratoVO.faturavel eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="faturavel_nao">N�o</label>
				<input class="campoRadio" type="radio" name="faturavel" id="faturavel_todos" value="" <c:if test="${contratoVO.faturavel eq ''}">checked</c:if>>
				<label class="rotuloRadio" for="faturavel_todos">Todos</label>

				
			</fieldset>
		
		<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarContrato">
    			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisar();">
    		</vacess:vacess>							
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
			</fieldset>
		</fieldset>
	</fieldset>
			
	<c:if test="${listaContratos ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" decorator="br.com.ggas.web.contrato.contrato.decorator.ContratoResultadoPesquisaDecorator" name="listaContratos" id="contrato" partialList="true" sort="external" pagesize="${tamanhoPagina}" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarContrato">
	     	 <display:column  property="chavePrimariaComLock" style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"/>

	     	 <display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${contrato.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
		     <display:column sortable="true" sortProperty="clienteAssinatura.nome" title="Nome do Cliente" headerClass="tituloTabelaEsq" style="text-align: left">
		     	<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.clienteAssinatura.nome}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" sortProperty="pontosConsumo.descricao" title="Ponto Consumo" >
		     
		    	<c:set var="descricaoPontoConsumo" value=""/>
		     	<c:set var="titlePontoConsumo" value=""/>
		     	
		     	<c:forEach items="${contrato.listaContratoPontoConsumo}" var="contratoPontoConsumo" varStatus="stat">
		     		<c:choose>
		     			<c:when test="${stat.first}">
				     		<c:set var="descricaoPontoConsumo" value="${contratoPontoConsumo.pontoConsumo.descricao}" />		     		
		     			</c:when>
		     			<c:otherwise>
				     		<c:set var="titlePontoConsumo" value='${titlePontoConsumo}${contratoPontoConsumo.pontoConsumo.descricao}&#013;' />
		     			</c:otherwise>
		     		</c:choose>		     		
		     	</c:forEach>
		     	
	     		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);' title="${titlePontoConsumo}"><span class="linkInvisivel"></span>
	     			<c:out value="${descricaoPontoConsumo}" />
     			</a>
	     			
		     </display:column>
		     <display:column sortable="true" title="CPF/CNPJ Cliente" style="width: 135px"  sortProperty="clienteAssinatura.numeroDocumentoFormatado">
		     	<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.clienteAssinatura.numeroDocumentoFormatado}'/>
				</a>
		     </display:column>		     
		     <display:column sortable="true" title="N�mero do<br />Contrato" sortProperty="numeroFormatado" style="width: 120px">
				<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.numeroFormatado}'/>
	        	</a>
	    	 </display:column>
		 	<display:column sortable="true" title="Situa��o" style="width: 110px" sortProperty="situacao.descricao">
		 		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.situacao.descricao}'/>
				</a>
	    	</display:column>
		 	<display:column sortable="true" title="Tipo" style="width: 110px" sortProperty="situacao.descricao">
		 		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		 		<c:choose>
					<c:when test="${contrato.chavePrimariaPrincipal > '0'}">Complementar</c:when>
					<c:otherwise>Principal</c:otherwise>
				</c:choose>
				</a>
	    	</display:column>	    	
	    	<display:column title="Data da<br />Assinatura" style="width: 80px" sortable="true" sortProperty="dataAssinatura">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataAssinatura}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>			     
	    	<display:column title="Data de<br />Vencimento" style="width: 80px" sortable="true" sortProperty="dataAssinatura">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataVencimentoObrigacoes}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>
		</display:table>
		
		<jsp:include page="/jsp/comum/selecionadorTamanhoPaginaTabelaPesquisaPOST.jsp">
			<jsp:param name="form" value="contratoForm"/>
			<jsp:param name="action" value="pesquisarContrato"/>
		</jsp:include>
		
	</c:if>
	
	 
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaContratos}">
			<vacess:vacess param="exibirSalvarAditamentoPontoConsumo">
				<input name="buttonRemover" id="aditar-contrato" value="Aditar" class="bottonRightCol2 botaoAditar" onclick="aditarContrato();" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirSalvarAlteracaoPontoConsumo">
				<input name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarContrato();" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirPontoConsumoContrato">
				<input name="botaoEncerrarRescindir" id="botaoEncerrarRescindir" value="Encerrar/Rescindir" class="bottonRightCol2 botaoEncerrarRescindir bottonLeftColUltimo" 
				onclick="encerrarRescindirContrato();" type="button">
			</vacess:vacess>
			<vacess:vacess param="excluirContrato">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerContrato()" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirMigracaoModeloContratoFluxoContrato">
				<input name="buttonRemover" value="Migrar Contrato" class="bottonRightCol2 bottonLeftColUltimo" onclick="migrarModeloContrato()" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirMigrarSaldo">
				<input name="buttonMigrarSaldo" value="Migrar Saldo" class="bottonRightCol2 bottonLeftColUltimo" onclick="exibirMigrarSaldo()" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirInclusaoContrato">
				<input name="buttonRemover" value="Incluir Complementar" class="bottonRightCol2 bottonLeftColUltimo" onclick="incluirComplementar()" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirInclusaoContrato">
				<input name="buttonRemover" value="Ordenar Faturamento" class="bottonRightCol2 bottonLeftColUltimo" onclick="ordenarContratos()" type="button">
			</vacess:vacess>			
<%-- 			<vacess:vacess param="imprimirContrato"> --%>
				<input name="buttonImprimir" id="botaoImprimir" value="Imprimir Contrato" class="bottonRightCol2 bottonLeftColUltimo" onclick="imprimirContrato()" type="button">
<%-- 			</vacess:vacess>			 --%>

			<%--<vacess:vacess param="exibirDadosFaturamento"> --%>
				<input name="buttonExibirDadosFaturamento" id="buttonExibirDadosFaturamento" value="Exibir Condi��es para Faturamento" class="bottonRightCol2 bottonLeftColUltimo" onclick="exibirDadosFaturamentoContrato()" type="button">
<%-- 		</vacess:vacess>			 --%>
			<vacess:vacessoperacao operacao="APROVAR" modulo="Contrato">
				<input name="buttonAprovarContrato" id="buttonAprovarContrato" value="Aprovar" class="bottonRightCol2" onclick="aprovaContrato()" type="button">
			</vacess:vacessoperacao>			
		</c:if>
		<vacess:vacess param="exibirInclusaoContrato">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>

</form:form> 
