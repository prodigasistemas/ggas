<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

	var qdcContrato = "";

	$(document)
			.ready(
					function() {

						// Datepicker
						$(
								"#dataAssinatura,#dataInvestimento,#dataVencObrigacoesContratuais,#dataInicioGarantiaFinanceira,#dataFinalGarantiaFinanceira,#QDCData,#dataIniVigRetirMaiorMenor,#dataFimVigRetirMaiorMenor,#dataInicioVigenciaC,#dataFimVigenciaC,#dataInicioRecuperacaoC,#dataMaximaRecuperacaoC")
								.datepicker(
										{
											changeYear : true,
											showOn : 'button',
											buttonImage : '<c:url value="/imagens/calendario.gif"/>',
											buttonImageOnly : true,
											buttonText : 'Exibir Calend�rio',
											dateFormat : 'dd/mm/yy'
										});
						$("#dataAssinatura").datepicker('option', 'maxDate',
								'+0d');

						$("input#indicadorRenovacaoAutomaticaSim")
								.click(
										function() {

											animatedcollapse
													.show('ConteinerQuantDiasRenovacao');
											animatedcollapse
													.hide('ConteinerTempoAntecedenciaRenovacao');
										});

						$("input#indicadorRenovacaoAutomaticaNao")
								.click(
										function() {
											animatedcollapse
													.show('ConteinerTempoAntecedenciaRenovacao');
											animatedcollapse
													.hide('ConteinerQuantDiasRenovacao');
										})

						//Adiciona a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
						$("input[type='text']:disabled,select:disabled")
								.addClass("campoDesabilitado");

						$(".campoValorReal[disabled='disabled']").addClass(
								"campoValorRealDesabilitado");

						//Identifica todos os campos de data e esconde a imagem do calend�rio
						$(".campoData:disabled + img").each(function() {
							$(this).css("display", "none");
						});

						//Identifica todos os campos radio desabilitados e esmaece a cor dos labels
						$("input:disabled:radio + label.rotuloRadio").each(
								function() {
									$(this).addClass("rotuloDesabilitado");
								});

						/*-- IN�CIO: Comportamento do campo Modelo de Contrato e do bot�o Exibir --*/
						//Habilita ou desabilita o bot�o Exibir (Modelo de Contrato).
						$("#botaoExibirModeloContrato").attr("disabled","disabled");
						$("#listaModeloContrato").change(function() {
							verificarModelo();
						});

						//Exibe o Modelo de Contrato selecionado ao pressionar a tecla ENTER
						$("#listaModeloContrato")
								.keypress(
										function(event) {
											if (event.keyCode == '13'
													&& $("#listaModeloContrato")
															.val() != -1) {
												exibirModelo();
											}
										});

						//Habilita ou desabilita o bot�o Exibir (Modelo de Contrato) pelo teclado
						$("#listaModeloContrato").keyup(function(event) {
							verificarModelo();
						});
						/*-- FIM: Comnportamento do campo Modelo de Contrato e do bot�o Exibir --*/

						
						var indicadorJurosMora = $('input[name="indicadorJurosMora"]:checked').val();
						if (indicadorJurosMora == undefined){
							indicadorJurosMora = $('input[name="indicadorJurosMora"]').val();
						}
						var indicadorMultaAtraso = $('input[name="indicadorMultaAtraso"]:checked').val();
						if (indicadorMultaAtraso == undefined){
							indicadorMultaAtraso = $('input[name="indicadorMultaAtraso"]').val();
						}
						
						manipularCamposJurosMulta(indicadorJurosMora, "percentualJurosMora");
						manipularCamposJurosMulta(indicadorMultaAtraso, "percentualMulta");

						mudarAba(<c:out value="${abaAtual}"/>);

						$('#formaCobranca').change(function(){
							$('#arrecadadorConvenio').children('option:not(:first)').remove();
							AjaxService.obterConvenios( $('#formaCobranca :selected').val(),
								{callback: function(convenios) { 
									$.each(convenios, function(key, value) {
										 $('#arrecadadorConvenio')
											 .append($("<option></option>")
											 .attr("value",key)
											 .text(value));
									});
								}, async:false}
							);
						});
						
						$('#formaCobranca').trigger('change');
						$("#arrecadadorConvenio").val(${contratoVO.arrecadadorConvenio});
						
						if($('#indicadorClienteComDebito').val() == 'true'){
						    $( "#clienteComDebitoConfirmacao" ).dialog({
						        resizable: false,
						        height: "auto",
						        width: 400,
						        modal: true,
						        buttons: {
						          "Continuar": function() {
						        	  $('#indicadorClienteComDebito').val('ignorar');
						        	  submeter('contratoForm',$('#urlExecutadaClienteDebito').val());
						          },
						          Cancelar: function() {
						            $( this ).dialog( "close" );
						            $('#indicadorClienteComDebito').val('');
						            $('#urlExecutadaClienteDebito').val('');
						          }
						        }
						      });
						}

						
						
						init();
						
						
					});

	animatedcollapse.addDiv('ConteinerQuantDiasRenovacao',
			'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerTempoAntecedenciaRenovacao',
			'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerTempoAntecedenciaRevisaoGarantias',
			'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerPeriodicidadeReavalicacaoGarantias',
			'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ContainerTipoAgrupamento',
			'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ContainerDebitoAutomatico',
			'fade=0,speed=400,hide=1');

	function showContainerTipoAgrupamento() {
		animatedcollapse.show('ContainerTipoAgrupamento');
	}

	function hideContainerTipoAgrupamento() {
		animatedcollapse.hide('ContainerTipoAgrupamento');
	}

	function showContainerDebitoAutomatico() {
		animatedcollapse.show('ContainerDebitoAutomatico');
	}

	function hideContainerDebitoAutomatico() {
		animatedcollapse.hide('ContainerDebitoAutomatico');
	}

	//Verifica se algum Modelo de Contrato est� selecionado e habilita ou n�o o bot�o exibir 
	function verificarModelo() {
		if ($("#listaModeloContrato option:selected").val() != -1) {
			$("#botaoExibirModeloContrato").removeAttr("disabled");
		} else {
			$("#botaoExibirModeloContrato").attr("disabled", "disabled");
		}
	}

	function exibirModelo() {
		if ($("#indicadorContratoExibido").val() == "true") {
			var confirmacao = window.confirm('A altera��o do Modelo de Contrato causar� a perda dos dados. Tem certeza que deseja alterar o Modelo? ')
			if (confirmacao == true) {
				$("#indicadorContratoExibido").val("true");
				$("#indicadorImovelSelecionado").val("false");
				selecionarModeloContrato();
			} else {
				return false;
			}
		} else {
			$("#indicadorContratoExibido").val("true");
			$("#indicadorImovelSelecionado").val("false");
			selecionarModeloContrato();
		}
	}

	function selecionarModeloContrato() {
		$("#botaoExibirModeloContrato").attr('disabled', 'disabled');
		
		<c:choose>
			<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
			submeter('contratoForm','reexibirAditamentoContrato?mudancaModelo=true');
			</c:when>
			<c:otherwise>
				submeter('contratoForm','reexibirInclusaoContrato?mudancaModelo=true');
			</c:otherwise>
		</c:choose>

	}

	function listarVersoesProposta(elem) {
		var numeroProposta = elem.value;
		var selectVersoesProposta = document.getElementById("idProposta");
		var qtdVersoes = null;

		selectVersoesProposta.length = 0;
		selectVersoesProposta.disabled = true;

		if (numeroProposta != "") {
			limparCamposProposta();
			AjaxService
					.listarVersoesAprovadasPropostaPorNumero(
							numeroProposta,
							{
								callback : function(versoes) {
									qtdVersoes = versoes.length;
									for (key in versoes) {
										var novaOpcao = new Option(
												versoes[key], key);
										selectVersoesProposta.options[selectVersoesProposta.length] = novaOpcao;
										document.getElementById("idProposta").value = key;
									}
									if (selectVersoesProposta.length > 0) {
										selectVersoesProposta.disabled = false;
									}
									preencherProposta();
								},
								async : false
							});

			//Obter quantidade de propostas pelo n�mero.
			AjaxService
					.obterQtdPropostasAprovadas(
							numeroProposta,
							{
								callback : function(qtdPropostas) {
									if (selectVersoesProposta.length == 0) {
										if (qtdPropostas == null
												|| qtdPropostas == 0) {
											alert('<fmt:message key="ERRO_NEGOCIO_PROPOSTA_NAO_EXISTENTE"/>');
										} else {
											alert('<fmt:message key="ERRO_NEGOCIO_PROPOSTA_NAO_APROVADA"/>');
										}
									} else {
										$("#idProposta").removeClass(
												"campoDesabilitado");
									}
								},
								async : false
							});

		}
	}

	function preencherProposta() {
		var idProposta = document.getElementById("idProposta");
		var gastoEstimadoGNMes = document.getElementById("gastoEstimadoGNMes");
		var economiaEstimadaGNMes = document
				.getElementById("economiaEstimadaGNMes");
		var economiaEstimadaGNAno = document
				.getElementById("economiaEstimadaGNAno");
		var descontoEfetivoEconomia = document
				.getElementById("descontoEfetivoEconomia");
		var valorParticipacaoCliente = document
				.getElementById("valorParticipacaoCliente");
		var qtdParcelasFinanciamento = document
				.getElementById("qtdParcelasFinanciamento");
		var percentualJurosFinanciamento = document
				.getElementById("percentualJurosFinanciamento");
		var valorInvestimento = document.getElementById("valorInvestimento");
		var dataInvestimento = document.getElementById("dataInvestimento");

		limparCamposProposta();

		if (idProposta.value != '') {
			AjaxService
					.obterPropostaPorChave(
							idProposta.value,
							{
								callback : function(proposta) {
									if (proposta != null) {
										if (proposta["valorGastoMensal"] != undefined) {
											gastoEstimadoGNMes.value = proposta["valorGastoMensal"];
										}
										if (proposta["economiaMensalGN"] != undefined) {
											economiaEstimadaGNMes.value = proposta["economiaMensalGN"];
										}
										if (proposta["economiaAnualGN"] != undefined) {
											economiaEstimadaGNAno.value = proposta["economiaAnualGN"];
										}
										if (proposta["percentualEconomia"] != undefined) {
											descontoEfetivoEconomia.value = proposta["percentualEconomia"];
										}
										if (proposta["valorCliente"] != undefined) {
											valorParticipacaoCliente.value = proposta["valorCliente"];
										}
										if (proposta["quantidadeParcelas"] != undefined) {
											qtdParcelasFinanciamento.value = proposta["quantidadeParcelas"];
										}
										if (proposta["percentualJuros"] != undefined) {
											percentualJurosFinanciamento.value = proposta["percentualJuros"];
										}
										if (proposta["valorInvestimento"] != undefined) {
											valorInvestimento.value = proposta["valorInvestimento"];
										}
										if (proposta["dataInvestimento"] != undefined) {
											dataInvestimento.value = proposta["dataInvestimento"];
										}
										if (proposta["indicadorParticipacaoCliente"] == 'false') {
											$("#valorParticipacaoCliente").val(
													null);
											$("#valorParticipacaoCliente")
													.prop("disabled", true);
										} else {
											$("#valorParticipacaoCliente")
													.prop("disabled", false);
										}
										habilitarCamposParticipacao();
									}
								},
								async : false
							}

					);
		}
	}

	function limparCamposProposta() {
		document.getElementById("gastoEstimadoGNMes").value = '';
		document.getElementById("economiaEstimadaGNMes").value = '';
		document.getElementById("economiaEstimadaGNAno").value = '';
		document.getElementById("descontoEfetivoEconomia").value = '';
		document.getElementById('valorParticipacaoCliente').value = '';
		habilitarCamposParticipacao();
	}

	function limparCampos() {
		debugger;
		limparFormularioDadosCliente();
		document.getElementById("numeroProposta").value = '';
		document.getElementById("idProposta").length = 0;
		document.getElementById("idProposta").disabled = true;
		limparCamposProposta();
		document.getElementById("diaVencFinanciamento").value = '-1';
		document.getElementById("numeroAnteriorContrato").value = '';
		document.getElementById("situacaoContrato").value = '-1';
		document.getElementById("dataAssinatura").value = '';
		document.getElementById("numeroEmpenho").value = '';
		document.getElementById("dataVencObrigacoesContratuais").value = '';
		document.getElementById("numDiasRenoAutoContrato").value = '';
		document.getElementById("tipoGarantiaFinanceira").value = '-1';
		document.getElementById("descGarantiaFinanc").value = '';
		document.getElementById("valorGarantiaFinanceira").value = '';
		document.getElementById("dataInicioGarantiaFinanceira").value = '';
		document.getElementById("dataFinalGarantiaFinanceira").value = '';
		document.getElementById('valorParticipacaoCliente').value = '';
		document.getElementById('percentualTarifaDoP').value = '';
		if(document.getElementById('consumoReferenciaSobreDem') != null){
			document.getElementById('consumoReferenciaSobreDem').value = '-1';
		}
		if(document.getElementById('faixaPenalidadeSobreDem') != null){
			document.getElementById('faixaPenalidadeSobreDem').value = '-1';
		}
		if(document.getElementById('consumoReferenciaSobDem') != null){
			document.getElementById('consumoReferenciaSobDem').value = '-1';
		}
		if(document.getElementById('faixaPenalidadeSobDem') != null){
			document.getElementById('faixaPenalidadeSobDem').value = '-1';
		}
		if(document.getElementById('indiceCorrecaoMonetaria') != null){
			document.getElementById('indiceCorrecaoMonetaria').value = '-1';
		}
		if(document.getElementById('formaCobranca') != null){
			document.getElementById('formaCobranca').value = '-1';
		}
		if(document.getElementById('arrecadadorConvenio') != null){
			document.getElementById('arrecadadorConvenio').value = '-1';
		}
		if(document.getElementById('percentualMulta') != null){
			document.getElementById('percentualMulta').value = '';
		}
		if(document.getElementById('percentualJurosMora') != null){
			document.getElementById('percentualJurosMora').value = '';
		}
		
		document.getElementById('valorContrato').value = '';
		document.getElementById('volumeReferencia').value = '';
		document.getElementById('tempoAntecedenciaRenovacao').value = '';
		document.getElementById('tempoAntecRevisaoGarantias').value = '';
		document.getElementById('periodicidadeReavGarantias').value = '';
		limparDadosQDCContrato();
		habilitarCamposParticipacao();
		ajustarAsteriscos();
	}

	function avancar() {
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var abaSelecionada = verificarAbaSelecionada();
		
		<c:choose>
			<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
				submeter('contratoForm', 'exibirAditamentoContratoPontoConsumo?limparPreenchimentoPontoConsumo=true');
			</c:when>
			<c:otherwise>
				submeter('contratoForm','exibirInclusaoContratoPontoConsumo?#' + abaSelecionada);
				$('#urlExecutadaClienteDebito').val('exibirInclusaoContratoPontoConsumo?#' + abaSelecionada);
			</c:otherwise>
		</c:choose>

	}

	function salvarContratoParcial() {
		
		var qdcSalvarContratoParcial = document.getElementById("QDCContrato");
		selectAllOptions(qdcSalvarContratoParcial);
		var abaSelecionada = verificarAbaSelecionada();
		
		$("#indicadorImovelSelecionado").val("false");
		submeter('contratoForm', 'salvarContratoParcialPasso1?#' + abaSelecionada);
		$('#urlExecutadaClienteDebito').val('salvarContratoParcialPasso1?#' + abaSelecionada);
	}

	function cancelar() {
		<c:choose>	
			<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
				location.href = '<c:url value="/exibirPesquisaContrato"/>';
			</c:when>
			<c:otherwise>
				location.href = '<c:url value="/exibirPesquisaContrato"/>';
			</c:otherwise>
		</c:choose>
	}

	function selecionarImovel(idSelecionado) {
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var idImovel = document.getElementById("idImovel");
		if (idSelecionado != '' && idImovel != undefined) {
			idImovel.value = idSelecionado;
			var selectQDC = document.getElementById("QDCContrato");
			selectAllOptions(selectQDC);
			$("#indicadorImovelSelecionado").val("true");
			
			<c:choose>
				<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
					submeter('contratoForm', 'selecionarImovelContratoFluxoAditarAlterar?#ancoraPesquisarImovelInclusaoContrato');	
				</c:when>
				<c:otherwise>
					submeter('contratoForm', 'selecionarImovelContratoFluxoIncluir?#ancoraPesquisarImovelInclusaoContrato');
				</c:otherwise>
			</c:choose>

		}
	}
	
	function selecionarImovelAbaProposta(idSelecionado) {
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var idImovel = document.getElementById("idImovel");
		if (idSelecionado != '' && idImovel != undefined) {
			idImovel.value = idSelecionado;
			var selectQDC = document.getElementById("QDCContrato");
			selectAllOptions(selectQDC);
			$("#indicadorImovelSelecionado").val("true");
			
			<c:choose>
				<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
					submeter('contratoForm', 'selecionarImovelContratoFluxoAditarAlterar#ancoraPesquisarImovelInclusaoContrato');	
				</c:when>
				<c:otherwise>
					submeter('contratoForm', 'selecionarImovelContratoFluxoIncluirAbaProposta?#ancoraPesquisarImovelInclusaoContrato');
				</c:otherwise>
			</c:choose>
		}
	}

	function exibirPopupPesquisaImovel() {

		popup = window
				.open(
						'exibirPesquisaImovelCompletoPopup?postBack=true',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function retirarImovel(idSelecionado) {

		var idImovel = document.getElementById("idImovel");
		
		if (idSelecionado != '' && idImovel != undefined) {
			idImovel.value = idSelecionado;
			var selectQDC = document.getElementById("QDCContrato");
			selectAllOptions(selectQDC);
			
			<c:choose>
				<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
				
				submeter('contratoForm', 'retirarImovelAditamentoContrato?#ancoraPesquisarImovelInclusaoContrato&isAcaoLimparCampo=true');			
				</c:when>
				<c:otherwise>
					submeter('contratoForm', 'retirarImovelInclusaoContrato?#ancoraPesquisarImovelInclusaoContrato&isAcaoLimparCampo=true');
				</c:otherwise>
			</c:choose>

		}
	}

	function init() {
		
		var renovacaoAutomatica = "<c:out value='${contratoVO.renovacaoAutomatica}'/>";
		
		if (renovacaoAutomatica == "true") {
			animatedcollapse.show('ConteinerQuantDiasRenovacao');
		} else {
			animatedcollapse.show('ConteinerTempoAntecedenciaRenovacao');
		}

		var garantiaFinanceiraRenovada = "<c:out value='${contratoVO.garantiaFinanceiraRenovada}'/>";

		if (garantiaFinanceiraRenovada == "Renov�vel") {
			animatedcollapse.show('ConteinerTempoAntecedenciaRevisaoGarantias');
			animatedcollapse
					.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
		} else if (garantiaFinanceiraRenovada == "Renovada") {
			animatedcollapse
					.show('ConteinerPeriodicidadeReavalicacaoGarantias');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
		} else {
			animatedcollapse
					.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
		}

		mostrarAsteriscoGarantiaFinanceira(garantiaFinanceiraRenovada.value);

		var faturamentoAgrupamento = "<c:out value='${contratoVO.faturamentoAgrupamento}'/>";
		if (faturamentoAgrupamento == "true") {
			animatedcollapse.show('ContainerTipoAgrupamento');
		}

		var debitoAutomatico = '${contratoVO.debitoAutomatico}';
		if (debitoAutomatico == "true") {
			animatedcollapse.show('ContainerDebitoAutomatico');
		}
		
		<c:forEach var = "id" items="${contratoVO.chavesPrimariasPontoConsumo}">
			var elementValue = $("#chavePrimariaPontoConsumo" + ${id}).val();
			
			if (elementValue != undefined) {
				$("#chavePrimariaPontoConsumo" + ${id}).attr("checked", true);
			}
		</c:forEach>
		
		<c:forEach var = "id" items="${contratoVO.chavesPrimarias}">
			var elementValue = $("#chavePrimariaPontoConsumo" + ${id}).val();
			
			if (elementValue != undefined) {
				$("#chavePrimariaPontoConsumo" + ${id}).attr("checked", true);
			}
		</c:forEach>

		var selectQDC = document.getElementById('QDCContrato');
		if (selectQDC != null) {
			ordenarSelectQDC(selectQDC);
		}

		var mensagensEstado = $(".mensagens").css("display");
		if (mensagensEstado == "none") {
			if ($("#indicadorImovelSelecionado").val() == "true") {
				$.scrollTo($("#legendPesquisarImoveis"), 1000);
			}
		}

		ajustarAsteriscos();

		verificarModelo();

		corrigirPosicaoDatepicker();

		esconderCamposDesabilitados();

		$('.checkAllCompromissos').click(function(){
			var indicador = $(this);
			$("." + indicador.attr("id")).each(function(){
				$(this).attr('checked', indicador.is(':checked'));
			})
		});
		
		var idCliente = '${contratoVO.idCliente}';
		var isAcaoLimparCampo = '${isAcaoLimparCampo}';
		if(idCliente != null && isAcaoLimparCampo == 'false' || isAcaoLimparCampo == ""){
			selecionarCliente(idCliente);
		}
		
		//selecionarProposta(1);
		
	}
	
	function adicionarCompromissoTOPAbaGeral() {
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);

		if (verificarSelecaoCompromissoTOPAbaGeral(false,true)) {
			if (confirm("Essa opera��o ir� substituir o compromisso selecionado, deseja continuar?")) {
				
				<c:choose>
					<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
						submeter('contratoForm', 'adicionarCompromissoTOPAbaGeral');
					</c:when>
					<c:otherwise>
						submeter('contratoForm', 'adicionarCompromissoTOPAbaGeral');
					</c:otherwise>
				</c:choose>
				

			}
		}
	}

	function excluirCompromissoTOPAbaGeral() {
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var selecao = verificarSelecaoCompromissoTOPAbaGeral();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				
				<c:choose>
					<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
						submeter('contratoForm', 'excluirCompromissoTOPAbaGeral');
					</c:when>
					<c:otherwise>
						submeter('contratoForm', 'excluirCompromissoTOPAbaGeral');
					</c:otherwise>
				</c:choose>				

			}
		}
	}

	function verificarSelecaoCompromissoTOPAbaGeral(editar = false, adicionar = false) {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.indicePeriodicidadeTakeOrPay != undefined) {
			var total = form.indicePeriodicidadeTakeOrPay.length;
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if (form.indicePeriodicidadeTakeOrPay[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.indicePeriodicidadeTakeOrPay.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				if (adicionar){
					<c:choose>
						<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
							submeter('contratoForm', 'adicionarCompromissoTOPAbaGeral');
						</c:when>
						<c:otherwise>
							submeter('contratoForm', 'adicionarCompromissoTOPAbaGeral');
						</c:otherwise>
					</c:choose>
					
					return false;
				} else if(editar){
					alert("Selecione um compromisso para realizar essa opera��o!");
					return false;
				} else {
					alert("Selecione um ou mais compromissos para realizar o opera��o!");
					return false;
				}
			} else if (editar && flag != 1){
				alert("Selecione apenas um compromisso para realizar essa opera��o!");
				return false;
			} else if (adicionar && flag != 1){
				alert("Selecione apenas um compromisso para realizar essa opera��o!");
				return false;
			}

		} else if (adicionar) {
			<c:choose>
				<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
					submeter('contratoForm', 'adicionarCompromissoTOPAbaGeral');
				</c:when>
				<c:otherwise>
					submeter(
							'contratoForm', 'adicionarCompromissoTOPAbaGeral');
				</c:otherwise>
			</c:choose>
			return false;
		} else {
			return false;
		}

		return true;
	}

	function adicionarPenalidadeRetiradaMaiorMenorGeral() {
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		if (verificarSelecaoPenalidadeRetiradaMaiorMenorGeral(false,true)) {
			if (confirm("Essa opera��o ir� substituir o compromisso selecionado, deseja continuar?")) {
			
				<c:choose>
					<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
						submeter('contratoForm', 'adicionarPenalidadeRetiradaMaiorMenorGeral');
					</c:when>
					<c:otherwise>
						submeter('contratoForm', 'adicionarPenalidadeRetiradaMaiorMenorGeral');
					</c:otherwise>
				</c:choose>

			}
		} 
	}

	function excluirPenalidadeRetiradaMaiorMenorGeral() {
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var selecao = verificarSelecaoPenalidadeRetiradaMaiorMenorGeral();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				<c:choose>
					<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
						submeter('contratoForm', 'excluirPenalidadeRetiradaMaiorMenorGeralAditamento?#penalidades');
					</c:when>
					<c:otherwise>
						submeter('contratoForm', 'excluirPenalidadeRetiradaMaiorMenorGeral?#penalidades');
					</c:otherwise>
				</c:choose>

			}
		}
	}

	function verificarSelecaoPenalidadeRetiradaMaiorMenorGeral(editar = false, adicionar = false) {
		var flag = 0;
		var form = document.forms[0];
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);

		if (form != undefined
				&& form.indicePenalidadeRetiradaMaiorMenor != undefined) {
			var total = form.indicePenalidadeRetiradaMaiorMenor.length;
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if (form.indicePenalidadeRetiradaMaiorMenor[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.indicePenalidadeRetiradaMaiorMenor.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				if (adicionar){
					
					<c:choose>
						<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
							submeter('contratoForm', 'adicionarPenalidadeRetiradaMaiorMenorGeral');
						</c:when>
						<c:otherwise>
							submeter('contratoForm', 'adicionarPenalidadeRetiradaMaiorMenorGeral');
						</c:otherwise>
					</c:choose>
					return false;
				} else if(editar){
					alert("Selecione um compromisso para realizar essa opera��o!");
					return false;
				} else {
					alert("Selecione um ou mais compromissos para realizar essa opera��o!");
					return false;
				}
			} else if (editar && flag != 1){
				alert("Selecione apenas um compromisso para realizar essa opera��o!");
				return false;
			} else if (adicionar && flag != 1){
				alert("Selecione apenas um compromisso para realizar essa opera��o!");
				return false;
			}

		} else if (adicionar) {
			<c:choose>
				<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
					submeter('contratoForm', 'adicionarPenalidadeRetiradaMaiorMenorGeral');
				</c:when>
				<c:otherwise>
					submeter('contratoForm', 'adicionarPenalidadeRetiradaMaiorMenorGeral');
				</c:otherwise>
			</c:choose>
			return false;
		} else {
			return false;
		}

		return true;
	}

	function controlarCheckBox(chaveImovel) {
		var chavesPonto = document
				.getElementsByName('chavesPrimariasPontoConsumo');
		var chavesImovel = document.getElementsByName('imovelChavesPonto');
		var indicadorTodos = document.getElementById('todos' + chaveImovel);

		if (chavesPonto != undefined && chavesPonto.length > 0) {
			for (var i = 0; i < chavesPonto.length; i++) {
				if (chavesImovel[i].value == chaveImovel) {
					if (indicadorTodos.checked == true) {
						chavesPonto[i].checked = true;
					} else {
						chavesPonto[i].checked = false;
					}
				}
			}
		}
	}
	
	function controlarCheckBox2() {
		var chavesPonto = document
				.getElementsByName('chavesPrimariasPontoConsumo');
		var chavesImovel = document.getElementsByName('imovelChavesPonto');
		var indicadorTodos = document.getElementById('todosImoveis');

		if (chavesPonto != undefined && chavesPonto.length > 0) {
			for (var i = 0; i < chavesPonto.length; i++) {

					if (indicadorTodos.checked == true) {
						chavesPonto[i].checked = true;
					} else {
						chavesPonto[i].checked = false;
					}

			}
		}
	}

	function habilitarCamposParticipacao() {
		if (document.getElementById('valorParticipacaoCliente').value == '') {
			limparCamposDependentesParticipacao();

			$("#qtdParcelasFinanciamento").parent("div").addClass("hidden");
			document.getElementById('qtdParcelasFinanciamento').disabled = true;
			$("#qtdParcelasFinanciamento").addClass("campoDesabilitado");

			$("#percentualJurosFinanciamento").parent("div").addClass("hidden");
			document.getElementById('percentualJurosFinanciamento').disabled = true;
			$("#percentualJurosFinanciamento").addClass("campoDesabilitado");

			$("#sistemaAmortizacao").parent("div").addClass("hidden");
			document.getElementById('sistemaAmortizacao').disabled = true;
			$("#sistemaAmortizacao").addClass("campoDesabilitado");

		} else {
			$("#qtdParcelasFinanciamento").parent("div").removeClass("hidden");
			document.getElementById('qtdParcelasFinanciamento').disabled = false;
			$("#qtdParcelasFinanciamento").removeClass("campoDesabilitado");

			$("#percentualJurosFinanciamento").parent("div").removeClass("hidden");
			document.getElementById('percentualJurosFinanciamento').disabled = false;
			$("#percentualJurosFinanciamento").removeClass("campoDesabilitado");

			$("#sistemaAmortizacao").parent("div").removeClass("hidden");
			document.getElementById('sistemaAmortizacao').disabled = false;
			$("#sistemaAmortizacao").removeClass("campoDesabilitado");
		}
	}

	function limparCamposDependentesParticipacao() {
		document.getElementById('qtdParcelasFinanciamento').value = '';
		document.getElementById('percentualJurosFinanciamento').value = '';
		document.getElementById('sistemaAmortizacao').value = '-1';
	}

	function mostrarAsterisco(valor, nomeCampo) {

		var underline = "_";
		if (valor != undefined) {
			for (var i = 0; i < nomeCampo.length; i++) {
				var campo = nomeCampo[i];
				var descricaoLabel = $("label[for=" + campo + "]").html();
				// Se o campo pai tem algum valor.
				if (valor > 0
						|| valor == "true"
						|| valor == "false"
						|| ($("#" + campo).hasClass('campoData') && valor != "" && valor
								.indexOf(underline) < 0)) {
					// Se o campo pai mudar de valor n�o adiciona outro asterisco, pois j� existe.
					if ($("#span" + campo).length == 0) {
						var span = "<span id='span"+ campo +"' class='campoObrigatorioSimbolo2'>* </span>";
						$("label[for=" + campo + "]").html(
								span + descricaoLabel);
						$("label[for=" + campo + "]").addClass(
								"campoObrigatorio");
					}

					// Se o campo pai n�o tem valor.
				} else {
					var classe = $("#span" + campo).attr("class");
					// S� remove caso o label tenha asterisco preto.
					if (classe == "campoObrigatorioSimbolo2") {
						$("#span" + campo).remove();
						$("label[for=" + campo + "]").removeClass(
								"campoObrigatorio");
					}
				}
			}
		}
	}

	function mostrarAsteriscoGarantiaFinanceira(valor) {

		var underline = "_";
		if (valor != undefined && valor != "Selecione") {
			var nomeCampo;
			if (valor == "Renov�vel") {
				valor = "true";
				nomeCampo = [ 'dataInicioGarantiaFinanceira',
						'dataFinalGarantiaFinanceira',
						'tempoAntecRevisaoGarantias' ];
				animatedcollapse
						.show('ConteinerTempoAntecedenciaRevisaoGarantias');
				animatedcollapse
						.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
			} else if (valor == "Renovada") {
				valor = "false";
				nomeCampo = [ 'periodicidadeReavGarantias' ];
				animatedcollapse
						.show('ConteinerPeriodicidadeReavalicacaoGarantias');
				animatedcollapse
						.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
			} else {
				animatedcollapse
						.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
				animatedcollapse
						.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
			}
			mostrarAsterisco(valor, nomeCampo);
		}
	}

	function ajustarAsteriscos() {

		if (!$("#indicadorRenovacaoAutomaticaSim").attr("disabled")) {
			mostrarAsterisco($("#indicadorRenovacaoAutomaticaSim").val(),
					[ 'numDiasRenoAutoContrato' ]);
		}

		if (document.getElementById('garantiaFinanceiraRenovada').value == "Renov�vel") {
			mostrarAsteriscoGarantiaFinanceira($("#garantiaFinanceiraRenovada")
					.val());
		}
		if (document.getElementById('garantiaFinanceiraRenovada').value == "Renovada") {
			mostrarAsteriscoGarantiaFinanceira($("#garantiaFinanceiraRenovada")
					.val());
		}

		if (!$("#consumoReferenciaSobreDem").attr("disabled")) {
			mostrarAsterisco($("#consumoReferenciaSobreDem").val(), [
					'faixaPenalidadeSobreDem', 'percentualReferenciaSobreDem',
					'baseApuracaoRetMaior' ]);
		}
		if (!$("#consumoReferenciaSobDem").attr("disabled")) {
			mostrarAsterisco($("#consumoReferenciaSobDem").val(), [
					'faixaPenalidadeSobDem', 'percentualReferenciaSobDem',
					'baseApuracaoRetMenor' ]);
		}
		if (!$("#faturamentoAgrupamentoSim").attr("disabled")) {
			mostrarAsterisco($("#faturamentoAgrupamentoSim").val(),
					[ 'tipoAgrupamento' ]);
		}

		if (!$("#periodicidadeTakeOrPayC").attr("disabled")) {
			mostrarAsterisco($("#periodicidadeTakeOrPayC").val(), [
					'referenciaQFParadaProgramadaC',
					'mensalConsumoReferenciaInferior',
					'margemVariacaoTakeOrPayC', 'dataInicioVigenciaC',
					'dataFimVigenciaC', 'precoCobrancaC', 'tipoApuracaoC',
					'consideraParadaProgramadaC',
					'consideraParadaNaoProgramadaC',
					'consideraFalhaFornecimentoC', 'consideraCasoFortuitoC',
					'consideraCasoFortuitoC', 'recuperavelC',
					'percentualNaoRecuperavelC',
					'apuracaoParadaNaoProgramadaC' ]);
		}

	}
	function validaObrigatoriedade() {

		var dataVencObrigacoesContratuais = document
				.getElementById('dataVencObrigacoesContratuais').value;
		var selectQDC = document.getElementById("QDCContrato");

		if (dataVencObrigacoesContratuais != "") {
			qdcContrato = dataVencObrigacoesContratuais;
		}

		if (selectQDC.options.length > 0 & dataVencObrigacoesContratuais == "") {
			alert('O preenchimento da Data de Vencimento das Obriga��es Contratuais � obrigat�rio quando existe um QDC do contrato.');
			document.getElementById('dataVencObrigacoesContratuais').value = qdcContrato;
		}

	}

	function carregarModeloContrato(elem) {

		var idTipoContrato = elem.value;
		var selectModelo = document.getElementById("listaModeloContrato");
		var idModeloContrato = "${contratoVO.idModeloContrato}";

		selectModelo.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectModelo.options[selectModelo.length] = novaOpcao;
		AjaxService.listarModeloContratoPorTipoContrato(idTipoContrato,
				function(funcoes) {
					for (key in funcoes) {
						var novaOpcao = new Option(funcoes[key], key);
						if (key == idModeloContrato) {
							novaOpcao.selected = true;
						}
						selectModelo.options[selectModelo.length] = novaOpcao;
					}
					ordernarSelect(selectModelo)
				});
	}

	function manipularCamposJurosMulta(valor, nomeCampo) {

		if (valor == "true") {
			$("#" + nomeCampo).prop("disabled", false);
			$("#span" + nomeCampo).html('*&nbsp;');
			$("#label" + nomeCampo).removeClass("rotuloDesabilitado");
		} else {
			$("#span" + nomeCampo).html('');
			$("#" + nomeCampo).val("");
			$("#" + nomeCampo).prop("disabled", true);
			$("#label" + nomeCampo).addClass("rotuloDesabilitado");
		}
	}

	function habilitarAvisoInterrupcao(valor) {
		if (valor == "3") {
			$("#percentualCobIntRetMaiorMenor").removeClass(
					"rotuloDesabilitado");
			$("#percentualCobIntRetMaiorMenor").prop("disabled", false);
		} else if (valor == "4") {
			$("#percentualCobIntRetMaiorMenor").addClass("rotuloDesabilitado");
			$("#percentualCobIntRetMaiorMenor").prop("disabled", true);
		}
	}

	function esconderCamposDesabilitados(){

		$(".campoDesabilitado").parent("div.labelCampo").addClass("hidden");

		$(".campoRadio").each(function(){
			if($("label[for='"+$(this).attr('name')+"']").hasClass("rotuloDesabilitado")){
				$(this).parent("div.labelCampo").addClass("hidden");
			}
		});
		esconderAgrupamentosDesabilitados();
	}

	function esconderAgrupamentosDesabilitados(){
		$("div.agrupamento").each(function(){
			var agrupamento = $(this);
			var esconder = true;

			agrupamento.find("div.labelCampo").each(function(){
				if(!$(this).hasClass("hidden")){
					esconder = false;
					return;
				}
			});
			
			if(esconder){
				agrupamento.addClass("hidden");
			}
		});
		esconderAbasSemInformacoesEditaveis()
	}

	function esconderAbasSemInformacoesEditaveis(){
		$("div.agrupamentoAba").each(function(){
			var esconder = true;
			var aba = $(this);
			aba.find("div.agrupamento").each(function(){
				if(!$(this).hasClass("hidden")){
					esconder = false;
					return;
				}
			});

			if(esconder){
				var idAba = aba.find("fieldset:first").attr("id");
				$("li."+idAba).addClass("hidden");
			}
		});
	}

	function mudarAba(aba){
		$('#tabs').tabs({ active : aba });
	}

	function aditar(){
		
		var abaSelecionada = verificarAbaSelecionada();
		
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		submeter('contratoForm', 'exibirSalvarAditamentoPontoConsumo?#' + abaSelecionada);
	}
	
	function verificarAbaSelecionada(){
		
		var abaSelecionada;
		
		$('.aba').each(function(index, element) {

			var eleValue = element.attributes.getNamedItem("aria-selected").value;
			
			if(eleValue == 'true'){
				abaSelecionada = element.attributes.getNamedItem("aria-controls").textContent;
			}

		});
		
		return abaSelecionada;
		
	}

	function alterar(){
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		var abaSelecionada = verificarAbaSelecionada();
		
		submeter('contratoForm', 'exibirSalvarAlteracaoPontoConsumo?#' + abaSelecionada);
	}

	function limpar(){
		<c:choose>
			<c:when test='${contratoVO.fluxoAditamento || contratoVO.fluxoAlteracao}'>
				submeter('contratoForm', 'exibirAditamentoContrato?isAcaoLimparCampo=false');
			</c:when>
			<c:otherwise>
				limparCampos();
				submeter('contratoForm', 'reexibirInclusaoContrato?mudancaModelo=true');
			</c:otherwise>
		</c:choose>
	}

</script>

<h1 class="tituloInterno">
	<c:choose>
		<c:when test='${contratoVO.fluxoAditamento}'> Aditar Contrato </c:when>
		<c:when test='${contratoVO.fluxoAlteracao}'> Alterar Contrato </c:when>
		<c:otherwise>
			Incluir Contrato<a class="linkHelp" href="<help:help>/contratoinclusoalteraoaditamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
			<p class="orientacaoInicial">
				Selecione na lista abaixo o Modelo de Contrato a ser usado para este
				contrato e clique em <span class="destaqueOrientacaoInicial">Exibir</span>.
			</p>
		</c:otherwise>
	</c:choose>
</h1>

<form:form method="post" styleId="formIncluirContrato" enctype="multipart/form-data" id="contratoForm" name="contratoForm">
	<input name="indicadorContratoExibido" type="hidden" id="indicadorContratoExibido" value="${indicadorContratoExibido}" />
	<input name="indicadorImovelSelecionado" type="hidden" id="indicadorImovelSelecionado" value="${contratoVO.indicadorImovelSelecionado}" />
	<input name="acao" type="hidden" id="acao" value="incluirContrato" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoVO.chavePrimaria}" />
	<input name="chavePrimariaPrincipal" type="hidden" id="chavePrimariaPrincipal" value="${contratoVO.chavePrimariaPrincipal}" />
	<input name="abaId" type="hidden" id="abaId" value="${abaId}" />
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="indexLista" type="hidden" id="indexLista" value="${contratoVO.indexLista}">
	<input name="enderecoPadrao" type="hidden" id="enderecoPadrao" value="none">
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${contratoVO.fluxoAlteracao}"/>
	<input name="fluxoAditamento" type="hidden" id="fluxoAditamento" value="${contratoVO.fluxoAditamento}"/>
	<input name="fluxoInclusao" type="hidden" id="fluxoInclusao" value="${contratoVO.fluxoInclusao}">
	
	<input name="indicadorClienteComDebito" type="hidden" id="indicadorClienteComDebito" value="${indicadorClienteComDebito}">
	<input name="urlExecutadaClienteDebito" type="hidden" id="urlExecutadaClienteDebito" value="${urlExecutadaClienteDebito}">
	
	
	
	<c:choose>
		<c:when test='${!contratoVO.fluxoAditamento && !contratoVO.fluxoAlteracao}'> 
			<fieldset class="conteinerPesquisarIncluirAlterar">
				<fieldset class="colunaDir" style="margin-right: 30px;">
					<legend class="conteinerBlocoTitulo">Modelos de Contrato:</legend>
					<select name="idModeloContrato" class="campoSelect campoHorizontal"
						id="listaModeloContrato">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaModelosContrato}" var="modeloContrato">
							<option value="<c:out value="${modeloContrato.chavePrimaria}"/>"
								<c:if test="${contratoVO.idModeloContrato == modeloContrato.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${modeloContrato.descricao}" />
							</option>
						</c:forEach>
					</select> 
					
					<input id="botaoExibirModeloContrato" class="bottonRightCol2 botaoExibir" name="button" value="Exibir" type="button" onclick="exibirModelo()">
				</fieldset>
			</fieldset>
		</c:when>
		<c:otherwise>
			<input name="indicadorFaturamentoAgrupado" type="hidden" id="indicadorFaturamentoAgrupado" value="<c:choose> <c:when test="${contratoVO.faturamentoAgrupamento == true}">true</c:when> <c:otherwise>false</c:otherwise> </c:choose>	"/>
			<input name="indicadorEmissaoAgrupada" type="hidden" id="indicadorEmissaoAgrupada" value="<c:choose> <c:when test="${contratoVO.emissaoFaturaAgrupada == true}">true</c:when> <c:otherwise>false</c:otherwise> </c:choose>"/>		
			<input name="idModeloContrato" type="hidden" id="idModeloContrato" value="${contratoVO.idModeloContrato}">
			<input name="anoContrato" type="hidden" value="${contratoVO.anoContrato}"/>
			<input name="versao" type="hidden" id="versao" value="<c:out value='${contratoVO.versao}'/>" />
			<input name="anoContrato" type="hidden" value="${contratoVO.anoContrato}"/>
			<input name="chavePrimariaPai" type="hidden" id="chavePrimariaPai" value="${contratoVO.chavePrimariaPai}"/>
		</c:otherwise>
	</c:choose>

	<c:if test="${modeloContrato ne null}">
	
		<br />

		<fieldset id="tabs" style="display: none">
			<ul>
				<li class="aba dadosGerais"><a href="#dadosGerais"><span
						class="campoObrigatorioSimboloTabs">* </span>Dados Gerais</a></li>
				<li class="aba proposta"><a href="#proposta"><span
						class="campoObrigatorioSimboloTabs">* </span>Proposta</a></li>
				<li class="aba dadosFinanceiros"><a href="#dadosFinanceiros"><span
						class="campoObrigatorioSimboloTabs">* </span>Dados Financeiros</a></li>
				<li class="aba takeOrPay"><a href="#takeOrPay"><span
						class="campoObrigatorioSimboloTabs">* </span>Take or Pay</a></li>
				<li class="aba penalidades"><a href="#penalidades"><span
						class="campoObrigatorioSimboloTabs">* </span>Penalidades</a></li>
				<li class="aba pontosConsumo"><a href="#pontosConsumo"><span
						class="campoObrigatorioSimboloTabs">* </span>Pontos de Consumo</a></li>
				<c:if test="${contratoVO.selDocumentosAnexos eq true}" >
					<li class="aba docsAnexos"><a href="#docsAnexos">
						<c:if test="${contratoVO.obgDocumentosAnexos eq true}" >
							<span class="campoObrigatorioSimboloTabs">* </span>
						</c:if>						
						Documentos Anexos</a></li>
				</c:if>
			</ul>

			<br />

			<div class="agrupamentoAba">
				<fieldset class="conteinerAba" id="dadosGerais">
					<jsp:include
						page="/jsp/contrato/contrato/contrato/include/abas/abaContratoDadosGerais.jsp" />
				</fieldset>
			</div>

			<div class="agrupamentoAba">
				<fieldset class="conteinerAba" id="proposta">
					<jsp:include
						page="/jsp/contrato/contrato/contrato/include/abas/abaContratoProposta.jsp" />
				</fieldset>
			</div>

			<div class="agrupamentoAba">
				<fieldset class="conteinerAba" id="dadosFinanceiros">
					<jsp:include
						page="/jsp/contrato/contrato/contrato/include/abas/abaContratoDadosFinanceiros.jsp" />
				</fieldset>
			</div>

			<div class="agrupamentoAba">
				<fieldset class="conteinerAba" id="takeOrPay">
					<jsp:include
						page="/jsp/contrato/contrato/contrato/include/abas/abaContratoTakeOrPay.jsp" />
				</fieldset>
			</div>

			<div class="agrupamentoAba">
				<fieldset class="conteinerAba" id="penalidades">
					<jsp:include
						page="/jsp/contrato/contrato/contrato/include/abas/abaContratoPenalidades.jsp" />
				</fieldset>
			</div>

			<div class="agrupamentoAba">
				<fieldset class="conteinerAba" id="pontosConsumo">
					<jsp:include
						page="/jsp/contrato/contrato/contrato/include/abas/abaContratoPontosConsumo.jsp" />
				</fieldset>
			</div>
			<c:if test="${contratoVO.selDocumentosAnexos eq true}" >
				<div class="agrupamentoAba">
					<fieldset class="conteinerAba" id="docsAnexos">
						<jsp:include
							page="/jsp/contrato/contrato/contrato/include/abas/abaContratoDocumentoAnexo.jsp" />
					</fieldset>
				</div>
			</c:if>
		</fieldset>
		<fieldset class="conteinerBotoes">
			<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
			<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar();">
			
			<fieldset class="conteinerBotoesDirFixo">
				<c:choose>
					<c:when test="${listaImovelPontoConsumoVO ne null && not empty listaImovelPontoConsumoVO}">
						<c:if test='${!contratoVO.fluxoAditamento && !contratoVO.fluxoAlteracao}'>
							<input name="button" class="bottonRightCol2 botaoSalvarParcial" value="Salvar Parcial" type="button" onclick="salvarContratoParcial();">	
						</c:if>
						<c:choose>
							<c:when test='${contratoVO.fluxoAditamento}'>
								<input name="button" class="bottonRightCol2 botaoAditar" id="aditar-contrato" value="Salvar" type="button" onclick="aditar();">	
							</c:when>
							<c:when test='${contratoVO.fluxoAlteracao}'>
								<input name="button" class="bottonRightCol2 botaoAditar" id="alterar-contrato" value="Salvar" type="button" onclick="alterar();">	
							</c:when>
						</c:choose>
						<input name="button" class="bottonRightCol2 botaoAvancar bottonRightColUltimo" value="Avan�ar >>" id="avancar-contrato" type="button" onclick="avancar();">
					</c:when>
					<c:otherwise>	
						<c:if test='${!contratoVO.fluxoAditamento && !contratoVO.fluxoAlteracao}'>
							<vacess:vacess param="salvarContratoParcialPasso1">
								<input name="button" class="bottonRightCol2 botaoSalvarParcial" value="Salvar Parcial" type="button" onclick="salvarContratoParcial();">
							</vacess:vacess>
						</c:if>
					</c:otherwise>
				</c:choose>
			</fieldset>
		</fieldset>
	</c:if>
	

	<c:if test="${indicadorClienteComDebito == 'true'}">
		<fieldset style="display: none">
			<fieldset id="clienteComDebitoConfirmacao">  
				o Cliente ${contratoForm.map.nomeCliente} possui debitos pendentes deseja continuar?
			</fieldset>
		</fieldset>
	</c:if>
	
</form:form>
