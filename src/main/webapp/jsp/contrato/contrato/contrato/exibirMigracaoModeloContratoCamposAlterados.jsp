<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Migrar Modelo Contrato - Campos Alterados<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para migrar o modelo de contrato preencha as informa��es necess�rias e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<script language="javascript">

$(document).ready(function(){

});

function salvar(){
	$("#botaoSalvar").prop("disabled", true);
	submeter('contratoForm', 'salvarMigracaoModeloContrato');
}

function voltar(){
	$("#chavesPrimarias").val("0");
	submeter('contratoForm', 'exibirMigracaoModeloContratoFluxoContrato');
}
					
</script>

<form:form method="post" action="/exibirMigracaoModeloContratoCamposAlterados" id="contratoForm" name="contratoForm"> 
	
	<input name="acao" type="hidden" id="acao" value="exibirAtributosMigracaoModeloContrato"/>
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${contratoVO.chavesPrimarias}"/>
	<input name="idModeloContrato" type="hidden" id="idModeloContrato" value="${modeloNovo.chavePrimaria}"/>
	
	<fieldset id="pesquisarContrato" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<label class="rotulo">Novo Modelo:</label>
			<span class="itemDetalhamento"><c:out value="${modeloNovo.descricao}"/></span><br />
		</fieldset>
		
	</fieldset>
		
	<jsp:include page="/jsp/contrato/cadastrocontrato/gridAtributosContratoModeloAgrupado.jsp"></jsp:include>
	
	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onClick="voltar();">
<%-- 		<vacess:vacess param="alterarConrtato"> --%>
			<input name="botaoSalvar" value="Salvar" class="bottonRightCol2 botaoGrande1" id="botaoSalvar" onclick="salvar();" type="button">
<%-- 		</vacess:vacess> --%>
	</fieldset>

</form:form> 
