<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<!--[if lt IE 10]>
    <style type="text/css">
        #alterarContrato-indicadorJurosMoraSimNao{margin: 0px 3px 0px;}
    </style>
<![endif]-->

<script>

	$(document).ready(function(){
		
		$("#dadosMedidor").dialog({
			autoOpen: false,
			width: 250,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
		// Datepicker
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#dataAditivo").datepicker('option', 'maxDate', '+0d');
		
		$("input#indicadorRenovacaoAutomaticaSim").click(function () { 
			animatedcollapse.show('ConteinerQuantDiasRenovacao');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRenovacao');
			$("#tempoAntecedenciaRenovacao").val('');			
		});
		
		$("input#indicadorRenovacaoAutomaticaNao").click(function () { 
			animatedcollapse.show('ConteinerTempoAntecedenciaRenovacao');
			animatedcollapse.hide('ConteinerQuantDiasRenovacao');
			$("#numDiasRenoAutoContrato").val('');			
		})
		
		if($("#indicadorRenovacaoAutomaticaSim").is(":checked")){
			animatedcollapse.show('ConteinerQuantDiasRenovacao');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRenovacao');
			$("#tempoAntecedenciaRenovacao").val('');
	 	} else if($("#indicadorRenovacaoAutomaticaNao").is(":checked")) {
			animatedcollapse.show('ConteinerTempoAntecedenciaRenovacao');
			animatedcollapse.hide('ConteinerQuantDiasRenovacao');
			$("#numDiasRenoAutoContrato").val('');
	 	}
		
		//Adiciona a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
		$("input[type='text']:disabled,select:disabled").addClass("campoDesabilitado");
		
		$(".campoValorReal[disabled='disabled']").addClass("campoValorRealDesabilitado");
		
		//Identifica todos os campos de data e esconde a imagem do calend�rio
		$(".campoData:disabled + img").each(function(){
			$(this).css("display","none");
		});
		
		//Identifica todos os campos radio desabilitados e esmaece a cor dos labels
		$("input:disabled:radio + label.rotuloRadio").each(function(){
			$(this).addClass("rotuloDesabilitado");
		});
		
		/*-- IN�CIO: Comportamento do campo Modelo de Contrato e do bot�o Exibir --*/
		//Habilita ou desabilita o bot�o Exibir (Modelo de Contrato).
		$("#botaoExibirModeloContrato").attr("disabled","disabled");
		$("#listaModeloContrato").change(function(){
			verificarModelo();
		});		
		
		
		//Exibe o Modelo de Contrato selecionado ao pressionar a tecla ENTER
		$("#listaModeloContrato").keypress(function(event){
			if (event.keyCode == '13' && $("#listaModeloContrato").val() != -1){
				exibirModelo();
			}			
		});

		//Habilita ou desabilita o bot�o Exibir (Modelo de Contrato) pelo teclado
		$("#listaModeloContrato").keyup(function(event){
			verificarModelo();
		});
		/*-- FIM: Comnportamento do campo Modelo de Contrato e do bot�o Exibir --*/

		var indicadorAtualizacaoCadastral = "${sessionScope.indicadorAtualizacaoCadastral}";	
		if(indicadorAtualizacaoCadastral != ''){
			$('#botaoCancelar').attr("disabled","disabled");		
		}
		
		var indicadorJurosMora = $('input[name="indicadorJurosMora"]:checked').val();
		var indicadorMultaAtraso = $('input[name="indicadorMultaAtraso"]:checked').val();
		manipularCamposJurosMulta(indicadorJurosMora, "percentualJurosMora");
		manipularCamposJurosMulta(indicadorMultaAtraso, "percentualMulta");
		
		$('#formaCobranca').change(function(){
			$('#arrecadadorConvenio').children('option:not(:first)').remove();
			AjaxService.obterConvenios( $('#formaCobranca :selected').val(),
				{callback: function(convenios) { 
					$.each(convenios, function(key, value) {
						 $('#arrecadadorConvenio')
							 .append($("<option></option>")
							 .attr("value",key)
							 .text(value));
					});
				}, async:false}
			);
		});
		$('#formaCobranca').trigger('change');
	});
	
	animatedcollapse.addDiv('ConteinerQuantDiasRenovacao', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerTempoAntecedenciaRenovacao', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerTempoAntecedenciaRevisaoGarantias', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerPeriodicidadeReavalicacaoGarantias', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ContainerTipoAgrupamento', 'fade=0,speed=400,hide=1');
	
	function showContainerTipoAgrupamento(){		
		animatedcollapse.show('ContainerTipoAgrupamento');
	}
	
	function hideContainerTipoAgrupamento(){		
		animatedcollapse.hide('ContainerTipoAgrupamento');
	}

	//Verifica se algum Modelo de Contrato est� selecionado e habilita ou n�o o bot�o exibir 
	function verificarModelo(){
		if($("#listaModeloContrato option:selected").val() != -1){
			$("#botaoExibirModeloContrato").removeAttr("disabled");
		} else {
			$("#botaoExibirModeloContrato").attr("disabled","disabled");
		}
	}
	
	function exibirModelo(){
		if($("#indicadorContratoExibido").val()== "true"){
			var confirmacao = window.confirm('A altera��o do Modelo de Contrato causar� a perda dos dados. Tem certeza que deseja alterar o Modelo? ')
			if (confirmacao == true) {
				$("#indicadorContratoExibido").val("true");
				$("#indicadorImovelSelecionado").val("false");
				selecionarModeloContrato();				
			} else {
				return false;
			}
		} else {
			$("#indicadorContratoExibido").val("true");
			$("#indicadorImovelSelecionado").val("false");
			selecionarModeloContrato();
		}
	}

	function selecionarModeloContrato(){
		$("#botaoExibirModeloContrato").attr('disabled','disabled');
		$("#fluxoAditamento").val(true);
		submeter('contratoForm', 'reexibirInclusaoContrato?mudancaModelo=true');
	}
	
	function listarVersoesProposta(elem) {
		var numeroProposta = elem.value;
		listarVersoesPropostaContrato(numeroProposta);	
	}
	
	function listarVersoesPropostaContrato(numeroProposta){
      	var selectVersoesProposta = document.getElementById("idProposta");
      	var qtdVersoes = null;
      	selectVersoesProposta.length=0;
      	selectVersoesProposta.disabled = true;      	
      	      	
      	if (numeroProposta != "") {      		
      		limparCamposProposta();
        	AjaxService.listarVersoesAprovadasPropostaPorNumero( numeroProposta, {
	           	callback: function(versoes) { 
        			qtdVersoes = versoes.length;           		      		         		
                	for (key in versoes){
                    	var novaOpcao = new Option(versoes[key], key);
                        selectVersoesProposta.options[selectVersoesProposta.length] = novaOpcao;
                        document.getElementById("idProposta").value = key;
                    }
                    if(selectVersoesProposta.length > 0){
                    	selectVersoesProposta.disabled = false;
					}
					
                    preencherProposta();
                }, async:false}
            );

            //Obter quantidade de propostas pelo n�mero.
            AjaxService.obterQtdPropostasAprovadas( numeroProposta, {
            	callback: function(qtdPropostas) {            		
            		if (selectVersoesProposta.length == 0) {
            			if(qtdPropostas == null || qtdPropostas == 0) {
	            			alert('<fmt:message key="ERRO_NEGOCIO_PROPOSTA_NAO_EXISTENTE"/>');
	            		} else {
	            			alert('<fmt:message key="ERRO_NEGOCIO_PROPOSTA_NAO_APROVADA"/>');
	            		}
            		}
            	}, async:false}
            );
      	}      
	}
	
	function preencherProposta() {
	
		var idProposta = document.getElementById("idProposta");
		var gastoEstimadoGNMes = document.getElementById("gastoEstimadoGNMes");
		var economiaEstimadaGNMes = document.getElementById("economiaEstimadaGNMes");
		var economiaEstimadaGNAno = document.getElementById("economiaEstimadaGNAno");
		var descontoEfetivoEconomia = document.getElementById("descontoEfetivoEconomia");	
		var valorParticipacaoCliente = document.getElementById("valorParticipacaoCliente");
		var qtdParcelasFinanciamento = document.getElementById("qtdParcelasFinanciamento");
		var percentualJurosFinanciamento = document.getElementById("percentualJurosFinanciamento");
		var valorInvestimento = document.getElementById("valorInvestimento");
		var dataInvestimento = document.getElementById("dataInvestimento");
		
		limparCamposProposta();
		
		if(idProposta.value != '') {				
			AjaxService.obterPropostaPorChave( idProposta.value, {
	           	callback: function(proposta) {	           		
	           		if(proposta != null){  	     
		               	if(proposta["valorGastoMensal"] != undefined){
		               		gastoEstimadoGNMes.value = proposta["valorGastoMensal"];
		               	}
		               	if(proposta["economiaMensalGN"] != undefined){
		               		economiaEstimadaGNMes.value = proposta["economiaMensalGN"];
		               	}
		                if(proposta["economiaAnualGN"] != undefined){
		               		economiaEstimadaGNAno.value = proposta["economiaAnualGN"];
		               	}
		                if(proposta["percentualEconomia"] != undefined){
		                	descontoEfetivoEconomia.value = proposta["percentualEconomia"];
		               	}
		               	if(proposta["valorCliente"] != undefined){
		               		valorParticipacaoCliente.value = proposta["valorCliente"];
		               	}
		               	if(proposta["quantidadeParcelas"] != undefined){
		               		qtdParcelasFinanciamento.value = proposta["quantidadeParcelas"];
		               	}
		               	if(proposta["percentualJuros"] != undefined){
		               		percentualJurosFinanciamento.value = proposta["percentualJuros"];
		               	}		        
		               	if(proposta["valorInvestimento"] != undefined){
		               		valorInvestimento.value = proposta["valorInvestimento"];
		               	}
		               	if(proposta["dataInvestimento"] != undefined){
		               		dataInvestimento.value = proposta["dataInvestimento"];
		               	}
		               	habilitarCamposParticipacao();       	
	               	}
	        	}	        		        	
	        	, async:false}
	        	
	        );	        
        }
	}
	
	function limparCamposProposta() {
		document.getElementById("gastoEstimadoGNMes").value = '';
		document.getElementById("economiaEstimadaGNMes").value = '';
		document.getElementById("economiaEstimadaGNAno").value = '';
		document.getElementById("descontoEfetivoEconomia").value = '';
		document.getElementById('valorParticipacaoCliente').value = '';
		habilitarCamposParticipacao();
	}

	function limparCampos() {
		limparFormularioDadosCliente();
		document.getElementById("numeroProposta").value = '';
		document.getElementById("idProposta").length = 0;
		document.getElementById("idProposta").disabled = true;
		limparCamposProposta();
		document.getElementById("diaVencFinanciamento").value = '-1';
		document.getElementById("numeroAnteriorContrato").value = '';
		document.getElementById("situacaoContrato").value = '-1';
		document.getElementById("numeroEmpenho").value = '';
		document.getElementById("dataVencObrigacoesContratuais").value = '';
		document.getElementById("numDiasRenoAutoContrato").value = '';
		document.getElementById("tipoGarantiaFinanceira").value = '-1';
		document.getElementById("descGarantiaFinanc").value = '';
		document.getElementById("valorGarantiaFinanceira").value = '';
		document.getElementById("dataInicioGarantiaFinanceira").value = '';
		document.getElementById("dataFinalGarantiaFinanceira").value = '';
		document.getElementById('valorParticipacaoCliente').value = '';
		document.getElementById('tempoAntecedenciaRenovacao').value = '';
		document.getElementById('periodicidadeReavGarantias').value = '';
		habilitarCamposParticipacao();
		document.getElementById('valorContrato').value = '';
		document.getElementById('formaCobranca').value = '-1';
		document.getElementById('percentualTarifaDoP').value = '';
		document.getElementById('dataAditivo').value = '';
		document.getElementById('descricaoAditivo').value = '';
		document.getElementById('tempoAntecRevisaoGarantias').value = '';
		document.getElementById('faixaPenalidadeSobreDem').value = '-1';
		document.getElementById('consumoReferenciaSobreDem').value = '-1';
		document.getElementById('faixaPenalidadeSobDem').value = '-1';
		document.getElementById('consumoReferenciaSobDem').value = '-1';
		document.getElementById('indiceCorrecaoMonetaria').value = '-1';
		document.getElementById('percentualMulta').value = '';	
		document.getElementById('percentualJurosMora').value = '';
		limparDadosQDCContrato();
		ajustarAsteriscos();
	}
	
	function avancar() {
	
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
	
		submeter('contratoForm', 'exibirAditamentoContratoPontoConsumo');
	}
	
	function cancelar() {		
		//location.href = '<c:url value="/pesquisarContrato?unlock=true"/>';
		submeter('0','pesquisarContrato');
	}

	function selecionarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		if (idSelecionado != '' && idImovel != undefined) {
			idImovel.value = idSelecionado;
			var selectQDC = document.getElementById("QDCContrato");
			selectAllOptions(selectQDC);
			$("#indicadorImovelSelecionado").val("true");
			submeter('contratoForm', 'selecionarImovelContratoFluxoAditarAlterar?#ancoraPesquisarImovelAditamentoContrato');	
		}	
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function validarRetirarPontoConsumo(isFaturamentoAgrupado) {
		if($("#faturamentoAgrupamentoSim").attr("checked")){
			alert('Para retirar um ponto de consumo � preciso que o faturamento do contrato n�o seja agrupado.');
			return false;
		}else{
			return confirm('Deseja retirar o ponto de consumo?');
		}
	}
	
	function retirarPontoConsumo(chavePontoConsumo,chaveImovel) {
		var idImovel = document.getElementById("idImovel");
		var idPontoConsumo = document.getElementById("idPontoConsumoLista");
		if (chaveImovel != '' && chavePontoConsumo != '' && idImovel != undefined && idPontoConsumo != undefined) {
			idImovel.value = chaveImovel;
			idPontoConsumo.value = chavePontoConsumo;
			var selectQDC = document.getElementById("QDCContrato");
			selectAllOptions(selectQDC);
			submeter('contratoForm', 'retirarImovelAditamentoContrato?#ancoraPesquisarImovelAditamentoContrato');	
		}	
	}

	function retirarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		if (idSelecionado != '' && idImovel != undefined) {
			idImovel.value = idSelecionado;
			var selectQDC = document.getElementById("QDCContrato");
			selectAllOptions(selectQDC);
			submeter('contratoForm', 'retirarImovelAditamentoContrato?#ancoraPesquisarImovelAditamentoContrato');	
		}	
	}
	
	function init () {
		var condicaoFluxoAlteracao = "<c:out value='${contratoVO.fluxoAlteracao}'/>";
				
		if(condicaoFluxoAlteracao == 'false'){
			if(document.getElementById('spandataAditivo')!=null){
				document.getElementById('spandataAditivo').innerHTML='*';
			}
			if(document.getElementById('spandescricaoAditivo')!=null){
				document.getElementById('spandescricaoAditivo').innerHTML='*';
			}
		}else{
			if(document.getElementById('spandataAditivo')!=null){
				document.getElementById('spandataAditivo').innerHTML='';
			}
			if(document.getElementById('spandescricaoAditivo')!=null){
				document.getElementById('spandescricaoAditivo').innerHTML='';
			}
		}

		var renovacaoAutomatica = "<c:out value='${contratoVO.renovacaoAutomatica}'/>";
		if(renovacaoAutomatica == "true"){
			animatedcollapse.show('ConteinerQuantDiasRenovacao');
		} else {
			animatedcollapse.show('ConteinerTempoAntecedenciaRenovacao');
		}

		var garantiaFinanceiraRenovada = "<c:out value='${contratoVO.garantiaFinanceiraRenovada}'/>";
		if(garantiaFinanceiraRenovada == "Renov�vel"){
			animatedcollapse.show('ConteinerTempoAntecedenciaRevisaoGarantias');	
			animatedcollapse.hide('ConteinerPeriodicidadeReavalicacaoGarantias');	
							
		} else if(garantiaFinanceiraRenovada == "Renovada"){
			animatedcollapse.show('ConteinerPeriodicidadeReavalicacaoGarantias');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');	
			
		}else{
			animatedcollapse.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
			}

		mostrarAsteriscoGarantiaFinanceira(garantiaFinanceiraRenovada.value);
		
		var faturamentoAgrupamento = "<c:out value='${contratoVO.faturamentoAgrupamento}'/>";
		if(faturamentoAgrupamento == "true"){
			animatedcollapse.show('ContainerTipoAgrupamento');
		}
			
		<c:forEach items="${contratoVO.chavesPrimariasPontoConsumo}" var="chavePrimariaPontoConsumo">		
			if(document.getElementById('chavePrimariaPontoConsumo'+'<c:out value="${chavePrimariaPontoConsumo}"/>') != undefined){
				document.getElementById('chavePrimariaPontoConsumo'+'<c:out value="${chavePrimariaPontoConsumo}"/>').checked = true;	
			}	
		</c:forEach>
		<c:forEach items="${contratoVO.chavesPrimarias}" var="chavePonto">
			if(document.getElementById('chavePrimariaPontoConsumo'+'<c:out value="${chavePonto}"/>') != undefined){
				document.getElementById('chavePrimariaPontoConsumo'+'<c:out value="${chavePonto}"/>').checked = true;	
			}	
		</c:forEach>
		
		var mensagensEstado = $(".mensagens").css("display");
		if(mensagensEstado == "none"){
			if ($("#indicadorImovelSelecionado").val() == "true") {
				$.scrollTo($("#legendPesquisarImoveis"),1000);
			}			
		}

		ajustarAsteriscos();

		habilitarDesabilitarDataRecisao();
		
		verificarModelo();
		
		corrigirPosicaoDatepicker();
		
	}
	addLoadEvent(init);

	function controlarCheckBox(chaveImovel) {
		var chavesPonto = document.getElementsByName('chavesPrimariasPontoConsumo');
		var chavesImovel = document.getElementsByName('imovelChavesPonto');
		var indicadorTodos = document.getElementById('todos'+chaveImovel);
		
		if(chavesPonto != undefined && chavesPonto.length > 0) {
			for(var i = 0; i < chavesPonto.length; i++) {
				if(chavesImovel[i].value == chaveImovel) {
					if(indicadorTodos.checked == true) {
						chavesPonto[i].checked = true;
					} else {
						chavesPonto[i].checked = false;
					}
				}
			}
		}
	}
	
	function habilitarCamposParticipacao(){
		if(document.getElementById('valorParticipacaoCliente').value == ''){
			limparCamposDependentesParticipacao();			
			document.getElementById('qtdParcelasFinanciamento').disabled = true;
			document.getElementById('percentualJurosFinanciamento').disabled = true;
			document.getElementById('sistemaAmortizacao').disabled = true;
		} else {			
			document.getElementById('qtdParcelasFinanciamento').disabled = false;
			document.getElementById('percentualJurosFinanciamento').disabled = false;
			document.getElementById('sistemaAmortizacao').disabled = false;
		}
	}


	function mostrarAsteriscoGarantiaFinanceira(valor) {
		
		var underline = "_";
		if (valor != undefined && valor != "Selecione") {
			var nomeCampo;
			if(valor == "Renov�vel"){
				valor = "true";
				nomeCampo = ['dataInicioGarantiaFinanceira','dataFinalGarantiaFinanceira','tempoAntecRevisaoGarantias'];
				animatedcollapse.show('ConteinerTempoAntecedenciaRevisaoGarantias');	
				animatedcollapse.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
			}else if(valor == "Renovada"){
				valor = "false";
				nomeCampo = ['periodicidadeReavGarantias'];
				animatedcollapse.show('ConteinerPeriodicidadeReavalicacaoGarantias');	
				animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
			}else{
				animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
				animatedcollapse.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
			}
			for (var i = 0; i < nomeCampo.length; i++) {
				var campo = nomeCampo[i];
				var descricaoLabel = $("label[for="+campo+"]").html();
				// Se o campo pai tem algum valor.
				if (valor > 0 || valor == "true" || valor == "false" || ($("#"+campo).hasClass('campoData') && valor != "" && valor.indexOf(underline) < 0)) {
					// Se o campo pai mudar de valor n�o adiciona outro asterisco, pois j� existe.
					if ($("#span"+campo).length == 0) {
						var span = "<span id='span"+ campo +"' class='campoObrigatorioSimbolo2'>* </span>";
						$("label[for="+campo+"]").html(span + descricaoLabel);
						$("label[for="+campo+"]").addClass("campoObrigatorio");
					}

				// Se o campo pai n�o tem valor.
				} else {
					var classe = $("#span"+campo).attr("class");
					// S� remove caso o label tenha asterisco preto.
					if (classe == "campoObrigatorioSimbolo2") {
						$("#span"+campo).remove();
						$("label[for="+campo+"]").removeClass("campoObrigatorio");
					}
				}
			}
		}
	}
	
	function limparCamposDependentesParticipacao(){
		document.getElementById('qtdParcelasFinanciamento').value = '';
		document.getElementById('percentualJurosFinanciamento').value = '';
		document.getElementById('sistemaAmortizacao').value = '-1';
	}

	function aditar(){
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		submeter('contratoForm', 'exibirSalvarAditamentoPontoConsumo');
	}

	function alterar(){
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		submeter('contratoForm', 'exibirSalvarAlteracaoPontoConsumo');
	}
	
	function mostrarAsterisco(valor,nomeCampo) {
		
		var mascaraData = "__/__/____";
		if (valor != undefined) {
			for (var i = 0; i < nomeCampo.length; i++) {
				var descricaoLabel = $("label[for="+nomeCampo[i]+"]").html();

				// Se o campo pai tem algum valor.
				if (valor > 0 || valor == "true" || valor == "false") {
					// Se o campo pai mudar de valor n�o adiciona outro asterisco, pois j� existe.
					if ($("#span"+nomeCampo[i]).length == 0) {
						var span = "<span id='span"+ nomeCampo[i] +"' class='campoObrigatorioSimbolo2'>* </span>";
						$("label[for="+nomeCampo[i]+"]").html(span + descricaoLabel);
						$("label[for="+nomeCampo[i]+"]").addClass("campoObrigatorio");
					}

				// Se o campo pai n�o tem valor.
				} else {
					var classe = $("#span"+nomeCampo[i]).attr("class");
					// S� remove caso o label tenha asterisco preto.
					if (classe == "campoObrigatorioSimbolo2") {
						$("#span"+nomeCampo[i]).remove();
						$("label[for="+nomeCampo[i]+"]").removeClass("campoObrigatorio");
					}
				}
			}
		}
	}

	

	function ajustarAsteriscos() {

		if(document.getElementById('garantiaFinanceiraRenovada').value == "Renov�vel"){
			mostrarAsteriscoGarantiaFinanceira($("#garantiaFinanceiraRenovada").val());
		}
		if (document.getElementById('garantiaFinanceiraRenovada').value == "Renovada") {
			mostrarAsteriscoGarantiaFinanceira($("#garantiaFinanceiraRenovada").val());
		}
		if (!$("#consumoReferenciaSobreDem").attr("disabled")) {
			mostrarAsterisco($("#consumoReferenciaSobreDem").val(),['faixaPenalidadeSobreDem','percentualReferenciaSobreDem','baseApuracaoRetMaior']);
		}
		if (!$("#consumoReferenciaSobDem").attr("disabled")) {
			mostrarAsterisco($("#consumoReferenciaSobDem").val(),['faixaPenalidadeSobDem','percentualReferenciaSobDem','baseApuracaoRetMenor']);
		}
		if (!$("#faturamentoAgrupamentoSim").attr("disabled")) {
			mostrarAsterisco($("#faturamentoAgrupamentoSim").val(),['tipoAgrupamento']);
		}
	}

	function habilitarDesabilitarDataRecisao() {
		var situacaoContrato = document.getElementById('situacaoContrato').value;
		
		// se situacao contrato igual a encerrado ou rescindido habilitar dataRecisao
		if(situacaoContrato == 2 || situacaoContrato == 7){
			$("#dataRecisao").removeAttr("disabled").css({"background-color":"#FFF","color":"#000","padding":"2px"});
			$("#dataRecisao").next().css("display","inline");
			$("#spanDataRecisao").attr("class","campoObrigatorioSimbolo");
			$("label[for=dataRecisao]").removeClass("rotuloDesabilitado").addClass("rotulo");
		}else {								
			$("#dataRecisao").attr("disabled","disabled").css({"background-color":"#EBEBE4","color":"#666","padding":"2px"});
			$("#dataRecisao").next().css("display","none");
			$("#spanDataRecisao").attr("class","campoObrigatorioSimboloDesabilitado");			
			$("label[for=dataRecisao]").addClass("rotuloDesabilitado");
			document.getElementById('dataRecisao').value = '';			
		}
		
	}

	function detalharMedidorPontoConsumo(id){
		
		AjaxService.obterMedidorPontoConsumo(id, {
			callback: function(dadosMedidor) {
									
				if(dadosMedidor != null){
					$("#modeloMedidorPopup").html(dadosMedidor["modeloMedidor"]);
					$("#numeroSeriePopup").html(dadosMedidor["numeroSerie"]);
					$("#dataInstalacaoPopup").html(dadosMedidor["dataInstalacao"]);
					$("#dataAtivacaoPopup").html(dadosMedidor["dataAtivacao"]);
					exibirJDialog("#dadosMedidor");
				}else{
					alert('<fmt:message key="CONTRATO_PONTO_CONSUMO_NAO_POSSUI_MEDIDOR"/>');
				}
			}
			, async:false}
		);
	}
	
	function manipularCamposJurosMulta(valor, nomeCampo){
		
		if(valor == "true"){
			$("#" + nomeCampo).prop("disabled", false);
			$( "#span" + nomeCampo ).html('*&nbsp;');
			$( "#label" + nomeCampo ).removeClass( "rotuloDesabilitado" );
		}else{
			$( "#span" + nomeCampo ).html('');
			$("#" + nomeCampo).val("");
			$("#" + nomeCampo).prop("disabled", true);
			$( "#label" + nomeCampo ).addClass( "rotuloDesabilitado" );
		}
	}
	
	function adicionarPenalidadeRetiradaMaiorMenorGeralAditamento(){
		submeter('contratoForm', 'adicionarPenalidadeRetiradaMaiorMenorGeralAditamento');
	}
	
	function excluirPenalidadeRetiradaMaiorMenorGeralAditamento(){
		var selecao = verificarSelecaoPenalidadeRetiradaMaiorMenorGeral();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('contratoForm', 'excluirPenalidadeRetiradaMaiorMenorGeralAditamento');
			}
	    }		
	}

	function verificarSelecaoPenalidadeRetiradaMaiorMenorGeral() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.indicePenalidadeRetiradaMaiorMenor != undefined) {
			var total = form.indicePenalidadeRetiradaMaiorMenor.length;
			if (total != undefined) {
				for ( var i = 0; i < total; i++) {
					if (form.indicePenalidadeRetiradaMaiorMenor[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.indicePenalidadeRetiradaMaiorMenor.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				alert("Selecione um ou mais compromissos para realizar o opera��o!");
				return false;
			}

		} else {
			return false;
		}

		return true;
	}

	
	function adicionarCompromissoTOPAbaGeral() {
		submeter('contratoForm', 'adicionarCompromissoTOPAbaGeralAditamento');
	}
	
	function excluirCompromissoTOPAbaGeral() {
		var selecao = verificarSelecaoTakeOrPay();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('contratoForm', 'excluirCompromissoTOPAbaGeralAditamento');
			}
	    }	
	}
	
	function verificarSelecaoTakeOrPay() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.indicePeriodicidadeTakeOrPay != undefined) {
			var total = form.indicePeriodicidadeTakeOrPay.length;
			if (total != undefined) {
				for ( var i = 0; i < total; i++) {
					if (form.indicePeriodicidadeTakeOrPay[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.indicePeriodicidadeTakeOrPay.checked == true) {
					flag++;
				}
			}

			if (flag <= 0) {
				alert("Selecione um ou mais compromissos para realizar o opera��o!");
				return false;
			}

		} else {
			return false;
		}

		return true;
	}

	
</script>

<div id="dadosMedidor" title="Dados do medidor">
	<label class="rotulo">Modelo:</label>
	<span class="itemDetalhamento" id="modeloMedidorPopup"></span><br />
	<label class="rotulo">Numero de s�rie:</label>
	<span class="itemDetalhamento" id="numeroSeriePopup"></span><br />
	<label class="rotulo">Data de instala��o:</label>
	<span class="itemDetalhamento" id="dataInstalacaoPopup"></span><br /><br />	
	<label class="rotulo">Data de ativa��o:</label>
	<span class="itemDetalhamento" id="dataAtivacaoPopup"></span><br /><br />
</div>

<h1 class="tituloInterno">   
	<c:choose>
		<c:when test="${contratoVO.fluxoAditamento eq true}">
			Aditar Contrato		
		</c:when>
		<c:otherwise>
			Alterar Contrato	
		</c:otherwise>
	</c:choose>
	<a class="linkHelp" href="<help:help>/contratoinclusoalteraoaditamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>

<form:form method="post" styleId="formAditarContrato" id="contratoForm" name="contratoForm">
	<input name="acao" type="hidden" id="acao" value="aditarContrato"/>
	<input name="indicadorContratoExibido" type="hidden" id="indicadorContratoExibido" value="${indicadorContratoExibido}"/>
	<input name="indicadorImovelSelecionado" type="hidden" id="indicadorImovelSelecionado" value="${contratoVO.indicadorImovelSelecionado}"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoVO.chavePrimaria}"/>
	<input name="chavePrimariaPrincipal" type="hidden" id="chavePrimariaPrincipal" value="${contratoVO.chavePrimariaPrincipal}"/>
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="indexLista" type="hidden" id="indexLista" value="${contratoVO.indexLista}">
	<input name="idModeloContrato" type="hidden" id="idModeloContrato" value="${contratoVO.idModeloContrato}">
	<input name="versao" type="hidden" id="versao" value="<c:out value='${contratoVO.versao}'/>" />
	<input name="chavePrimariaPai" type="hidden" id="chavePrimariaPai" value="${contratoVO.chavePrimariaPai}"/>
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${contratoVO.fluxoAlteracao}"/>
	<input name="fluxoAditamento" type="hidden" id="fluxoAditamento" value="${contratoVO.fluxoAditamento}"/>
	<input name="indicadorFaturamentoAgrupado" type="hidden" id="indicadorFaturamentoAgrupado" value="<c:choose> <c:when test="${contratoVO.faturamentoAgrupamento == true}">true</c:when> <c:otherwise>false</c:otherwise> </c:choose>	"/>
	<input name="indicadorEmissaoAgrupada" type="hidden" id="indicadorEmissaoAgrupada" value="<c:choose> <c:when test="${contratoVO.emissaoFaturaAgrupada == true}">true</c:when> <c:otherwise>false</c:otherwise> </c:choose>"/>
	<input name="anoContrato" type="hidden" value="${contratoVO.anoContrato}"/>
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Avan�ar</span> para continuar 
			<c:choose>
				<c:when test="${contratoVO.fluxoAditamento eq true}">
					o aditamento		
				</c:when>
				<c:otherwise>
					a altera��o	
				</c:otherwise>
			</c:choose> do Contrato.
		</p>
		
		<fieldset id="contratoCol1" class="colunaEsq">
			
			<fieldset id="pesquisarCliente">
				<ggas:labelContrato styleClass="asteriscoLegend" forCampo="clienteAssinatura"> </ggas:labelContrato>
				<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
					<jsp:param name="idCampoIdCliente" value="idCliente"/>
					<jsp:param name="idCliente" value="${cliente.idCliente}"/>
					<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
					<jsp:param name="nomeCliente" value="${cliente.nomeCliente}"/>
					<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
					<jsp:param name="documentoFormatadoCliente" value="${cliente.documentoCliente}"/>
					<jsp:param name="idCampoEmail" value="emailCliente"/>
					<jsp:param name="emailCliente" value="${cliente.emailCliente}"/>
					<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
					<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoCliente}"/>		
					<jsp:param name="dadosClienteObrigatorios" value="false"/>
					<jsp:param name="nomeComponente" value="Cliente Assinatura"/>													
				</jsp:include>
			</fieldset>
			
			<fieldset id="conteinerProposta">
				<legend>Proposta</legend>
				<div class="conteinerDados">
					<ggas:labelContrato forCampo="numeroProposta">Proposta:</ggas:labelContrato>
					<input class="campoTexto campoHorizontal" type="text" id="numeroProposta" name="numeroProposta" maxlength="30" size="32" <ggas:campoSelecionado campo="numeroProposta"/> value="${contratoVO.numeroProposta}" onchange="listarVersoesProposta(this);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumerico(event)');">
					<select class="campoSelect campoHorizontal" name="idProposta" id="idProposta" <ggas:campoSelecionado campo="numeroProposta"/> onclick="preencherProposta();" <c:if test="${empty contratoVO.idProposta}">disabled="disabled"</c:if>>
						<c:forEach items="${listaVersoesProposta}" var="proposta">
							<option value="<c:out value="${proposta.chavePrimaria}"/>" <c:if test="${contratoVO.idProposta == proposta.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${proposta.versaoProposta}"/>
							</option>		
						</c:forEach>
					</select>
					<br class="quebraLinha" />
					
					<ggas:labelContrato forCampo="gastoEstimadoGNMes">Gasto Mensal com GN:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas campoHorizontal campoValorReal" type="text" id="gastoEstimadoGNMes" name="gastoEstimadoGNMes" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="gastoEstimadoGNMes"/> value="<fmt:formatNumber value='${contratoVO.gastoEstimadoGNMes}' maxFractionDigits='2' minFractionDigits='2'/>">
					<label class="rotuloInformativo rotuloHorizontal" for="gastoEstimadoGNMes">R$/m�s</label><br class="quebraLinha2">

					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="economiaEstimadaGNMes">Economia Mensal<br />com GN:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas campoValorReal" type="text" id="economiaEstimadaGNMes" name="economiaEstimadaGNMes" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="economiaEstimadaGNMes"/> value="<fmt:formatNumber value='${contratoVO.economiaEstimadaGNMes}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha">

					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="economiaEstimadaGNAno">Economia Anual<br />com GN:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas campoValorReal" type="text" id="economiaEstimadaGNAno" name="economiaEstimadaGNAno" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="economiaEstimadaGNAno"/> value="<fmt:formatNumber value='${contratoVO.economiaEstimadaGNAno}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha">
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="descontoEfetivoEconomia">Desconto Efetivo na Economia:</ggas:labelContrato>
					<input class="campoTexto campoHorizontal" type="text" id="descontoEfetivoEconomia" name="descontoEfetivoEconomia" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  maxlength="10" size="3" <ggas:campoSelecionado campo="descontoEfetivoEconomia"/> value="${contratoVO.descontoEfetivoEconomia}">
					<label class="rotuloInformativo rotuloHorizontal" for="descontoEfetivoEconomia">%</label><br class="quebraLinha">
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="diaVencFinanciamento">Dia para Vencimento Financiamento:</ggas:labelContrato>
					<select id="diaVencFinanciamento" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="diaVencFinanciamento"/> name="diaVencFinanciamento">
						<option value="-1">Selecione</option>
						<c:forEach begin="1" end="31" var="dia">
							<option value="${dia}" <c:if test="${contratoVO.diaVencFinanciamento == dia}">selected="selected"</c:if>>${dia}</option>
					  	</c:forEach>
					</select>
					<label class="rotuloInformativo rotuloHorizontal" for="diaVencFinanciamento">(01-31)</label><br class="quebraLinha2">
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="valorParticipacaoCliente">Valor da Participa��o do Cliente:</ggas:labelContrato>					
					<input class="campoTexto campoValorReal" type="text" id="valorParticipacaoCliente" name="valorParticipacaoCliente" onblur="aplicarMascaraNumeroDecimal(this,2);habilitarCamposParticipacao();" onkeypress="return formatarCampoDecimal(event,this,11,2);" onkeyup="habilitarCamposParticipacao()" maxlength="10" size="13" <ggas:campoSelecionado campo="valorParticipacaoCliente"/> value="<fmt:formatNumber value='${contratoVO.valorParticipacaoCliente}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha"/>
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="qtdParcelasFinanciamento">Quantidade de Parcelas do Financiamento:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas" type="text" id="qtdParcelasFinanciamento" name="qtdParcelasFinanciamento" maxlength="3" size="2" <ggas:campoSelecionado campo="qtdParcelasFinanciamento"/> value="${contratoVO.qtdParcelasFinanciamento}" onkeypress="return formatarCampoInteiro(event);" <c:if test="${empty contratoVO.valorParticipacaoCliente}">disabled="disabled"</c:if>><br class="quebraLinha"/>
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualJurosFinanciamento">Percentual de Juros do Financiamento:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualJurosFinanciamento" name="percentualJurosFinanciamento" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="3" <ggas:campoSelecionado campo="percentualJurosFinanciamento"/> value="${contratoVO.percentualJurosFinanciamento}" <c:if test="${empty contratoVO.valorParticipacaoCliente}">disabled="disabled"</c:if>>
					<label class="rotuloInformativo rotuloHorizontal" for="percentualJurosFinanciamento">%</label><br class="quebraLinha2" />
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="sistemaAmortizacao">Sistema de<br />Amortiza��o:</ggas:labelContrato>
					<select id="sistemaAmortizacao" class="campoSelect" name="sistemaAmortizacao" <c:if test="${empty contratoVO.valorParticipacaoCliente}">disabled="disabled"</c:if>>
						<option value="-1">Selecione</option>
						<c:forEach items="${listaAmortizacoes}" var="amortizacao">
							<option value="${amortizacao.chavePrimaria}" <c:if test="${contratoVO.sistemaAmortizacao eq amortizacao.chavePrimaria}">selected="selected"</c:if>>${amortizacao.descricao}</option>
					  	</c:forEach>
					</select><br class="quebraLinha"/>
					
					<ggas:labelContrato styleClass="rotulo" forCampo="valorInvestimento">Valor do Investimento:</ggas:labelContrato>					
					<input class="campoTexto campo2Linhas campoValorReal" type="text" id="valorInvestimento" name="valorInvestimento" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="valorInvestimento"/> value="<fmt:formatNumber value='${contratoVO.valorInvestimento}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha"/>
					
					<ggas:labelContrato styleClass="rotulo" forCampo="dataInvestimento">Data do Investimento:</ggas:labelContrato>
					<input class="campoData campo2Linhas" type="text" id="dataInvestimento" name="dataInvestimento" maxlength="10" <ggas:campoSelecionado campo="dataInvestimento"/>  value="${contratoVO.dataInvestimento}"><br class="quebraLinha"/>
						
				</div>
			</fieldset>
			
			<fieldset id="conteinerGarantia">
				<legend>Garantia</legend>
				<div class="conteinerDados">
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="tipoGarantiaFinanceira">Tipo Garantia Financeira:</ggas:labelContrato>
					<select name="tipoGarantiaFinanceira" id="tipoGarantiaFinanceira" class="campoSelect campo2Linhas" <ggas:campoSelecionado campo="tipoGarantiaFinanceira"/>>
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoGarantiaFinanceira}" var="tipoGarantiaFinanceira">
							<option value="<c:out value="${tipoGarantiaFinanceira.chavePrimaria}"/>" <c:if test="${contratoVO.tipoGarantiaFinanceira == tipoGarantiaFinanceira.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoGarantiaFinanceira.descricao}"/>
							</option>		
						</c:forEach>
					</select><br class="quebraLinha">
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="descGarantiaFinanc">Descri��o da Garantia Financeira:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas" type="text" id="descGarantiaFinanc" name="descGarantiaFinanc" maxlength="40" size="40" <ggas:campoSelecionado campo="descGarantiaFinanc"/> value="${contratoVO.descGarantiaFinanc}"><br class="quebraLinha" />
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="valorGarantiaFinanceira">Valor da Garantia Financeira:</ggas:labelContrato>					
					<input class="campoTexto campo2Linhas campoValorReal" type="text" id="valorGarantiaFinanceira" name="valorGarantiaFinanceira" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="valorGarantiaFinanceira"/> value="<fmt:formatNumber value='${contratoVO.valorGarantiaFinanceira}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha" />
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="garantiaFinanceiraRenovada">A garantia �?</ggas:labelContrato>
						<select name="garantiaFinanceiraRenovada" id="garantiaFinanceiraRenovada" class="campoSelect" onchange="mostrarAsteriscoGarantiaFinanceira(this.value);">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaGarantiaFinanceiraRenovada}" var="garantiaFinanceiraRenovada">
							<option value="<c:out value="${garantiaFinanceiraRenovada}"/>" <c:if test="${contratoVO.garantiaFinanceiraRenovada == garantiaFinanceiraRenovada}">selected="selected"</c:if>>
								<c:out value="${garantiaFinanceiraRenovada}"/>
							</option>		
						</c:forEach>
						</select><br class="quebraLinha">
					<fieldset id="ConteinerTempoAntecedenciaRevisaoGarantias">
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="dataInicioGarantiaFinanceira">Data inicio da Garantia da Financeira:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataInicioGarantiaFinanceira" name="dataInicioGarantiaFinanceira" maxlength="10" <ggas:campoSelecionado campo="dataInicioGarantiaFinanceira"/> value="${contratoVO.dataInicioGarantiaFinanceira}"><br class="quebraLinha" />
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="dataFinalGarantiaFinanceira">Data Final da Garantia Financeira:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataFinalGarantiaFinanceira" name="dataFinalGarantiaFinanceira" maxlength="10" <ggas:campoSelecionado campo="dataFinalGarantiaFinanceira"/> value="${contratoVO.dataFinalGarantiaFinanceira}"><br class="quebraDataRadio" />
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="tempoAntecRevisaoGarantias">Tempo de Anteced�ncia para Revis�o das Garantias:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal campo3Linhas" type="text" id="tempoAntecRevisaoGarantias" name="tempoAntecRevisaoGarantias" maxlength="3" size="2" <ggas:campoSelecionado campo="tempoAntecRevisaoGarantias"/> value="${contratoVO.tempoAntecRevisaoGarantias}" onkeypress="return formatarCampoInteiro(event);">
						<label class="rotuloInformativo3Linhas rotuloHorizontal">dias</label>
					</fieldset>
					<fieldset id="ConteinerPeriodicidadeReavalicacaoGarantias">
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="periodicidadeReavGarantias">Periodicidade para reavalia��o das Garantias:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal campo3Linhas" type="text" id="periodicidadeReavGarantias" name="periodicidadeReavGarantias" maxlength="3" size="2" <ggas:campoSelecionado campo="periodicidadeReavGarantias"/> value="${contratoVO.periodicidadeReavGarantias}" onkeypress="return formatarCampoInteiro(event);">
						<label class="rotuloInformativo3Linhas rotuloHorizontal">dias</label>
					</fieldset>	
				</div>
			</fieldset>
			
			<fieldset id="conteinerGarantia">
					<legend>Multa Rescis�ria</legend>
					<div class="conteinerDados">
						<ggas:labelContrato styleClass="rotulo" forCampo="multaRecisoria">Multa Rescis�ria:</ggas:labelContrato>
						<select name="multaRecisoria" id="multaRecisoria" class="campoSelect campo2Linhas" <ggas:campoSelecionado campo="multaRecisoria"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoMultaRecisoria}" var="multaRecisoria">
								<option value="<c:out value="${multaRecisoria.chavePrimaria}"/>" <c:if test="${contratoVO.multaRecisoria == multaRecisoria.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${multaRecisoria.descricao}"/>
								</option>		
							</c:forEach>
						</select><br class="quebraLinha" />								
																																			
						<ggas:labelContrato styleClass="rotulo" forCampo="dataRecisao"><span class="campoObrigatorioSimbolo" id="spanDataRecisao" >* </span>Data da Recis�o:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataRecisao" name="dataRecisao" maxlength="10" value="${contratoVO.dataRecisao}" > <br class="quebraLinha"/>
																	
											
					</div>
				</fieldset>
				
				<fieldset id="conteinerGarantia">
					<legend>Take or Pay</legend>

					<div class="conteinerDados">
						<div class="conteinerDados2">
							<ggas:labelContrato forId="tipoCompromisso" forCampo="periodicidadeTakeOrPayC">Compromisso:</ggas:labelContrato>
								<select class="campoSelect campoHorizontal" name="periodicidadeTakeOrPayC" id="tipoCompromisso" <ggas:campoSelecionado campo="periodicidadeTakeOrPayC"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeTakeOrPay">
									<option value="<c:out value="${periodicidadeTakeOrPay.chavePrimaria}"/>" <c:if test="${contratoVO.periodicidadeTakeOrPayC == periodicidadeTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${periodicidadeTakeOrPay.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							<br class="quebraLinha2" />
							
							<ggas:labelContrato styleId="tipoReferenciaQFParadaProgramada" styleClass="rotuloVertical rotulo2Linhas" forCampo="referenciaQFParadaProgramadaC">Refer�ncia Quantidade Faltante Parada Programada:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" name="referenciaQFParadaProgramadaC" id="tipoReferenciaQFParadaProgramada" <ggas:campoSelecionado campo="referenciaQFParadaProgramadaC"/> >
								<option value="-1">Selecione</option>
								<c:forEach items="${listaReferenciaQFParadaProgramada}" var="referenciaQFParadaProgramada">
									<option value="<c:out value="${referenciaQFParadaProgramada.chavePrimaria}"/>" <c:if test="${contratoVO.referenciaQFParadaProgramadaC == referenciaQFParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${referenciaQFParadaProgramada.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							<br class="quebraLinha2" />
							
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="mensalConsumoReferenciaInferior" forCampo="consumoReferenciaTakeOrPayC">Consumo refer�ncia inferior:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="mensalConsumoReferenciaInferior" name="consumoReferenciaTakeOrPayC" <ggas:campoSelecionado campo="consumoReferenciaTakeOrPayC"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaTakeOrPay">
								<option value="<c:out value="${consumoReferenciaTakeOrPay.chavePrimaria}"/>" <c:if test="${contratoVO.consumoReferenciaTakeOrPayC == consumoReferenciaTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${consumoReferenciaTakeOrPay.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br class="quebraLinha2" />
							
							<ggas:labelContrato styleId="rotuloMensalVariacaoInferior" styleClass="rotuloVertical rotulo2Linhas" forId="margemVariacaoTakeOrPayC" forCampo="margemVariacaoTakeOrPayC">Compromisso de retirada ToP:</ggas:labelContrato>
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="margemVariacaoTakeOrPayC" name="margemVariacaoTakeOrPayC" size="3" maxlength="6" <ggas:campoSelecionado campo="margemVariacaoTakeOrPayC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" name="margemVariacaoTakeOrPayC" value="${contratoVO.margemVariacaoTakeOrPayC}">
							<label class="rotuloHorizontal rotuloInformativo">%</label><br class="quebraLinha" />

							<ggas:labelContrato forCampo="dataInicioVigenciaC">Data In�cio Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataInicioVigenciaC" name="dataInicioVigenciaC" maxlength="10" <ggas:campoSelecionado campo="dataInicioVigenciaC"/> value="${contratoVO.dataInicioVigenciaC}"><br class="quebraLinha" />
						
							<ggas:labelContrato forCampo="dataFimVigenciaC">Data Fim Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataFimVigenciaC" name="dataFimVigenciaC" maxlength="10" <ggas:campoSelecionado campo="dataFimVigenciaC"/> value="${contratoVO.dataFimVigenciaC}"><br class="quebraLinha" />
													
							<ggas:labelContrato styleId="tipoReferenciaQFParadaProgramada" styleClass="rotuloVertical rotulo2Linhas" forCampo="precoCobrancaC">Tabela de Pre�os para Cobran�a:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" name="precoCobrancaC" id="precoCobrancaC" <ggas:campoSelecionado campo="precoCobrancaC"/> >
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
									<option value="<c:out value="${precoCobranca.chavePrimaria}"/>" <c:if test="${contratoVO.precoCobrancaC == precoCobranca.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${precoCobranca.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							<br class="quebraLinha2" />
							
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="tipoApuracaoC" forCampo="tipoApuracaoC">Forma de C�lculo para Cobran�a e Recupera��o:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="tipoApuracaoC" name="tipoApuracaoC" <ggas:campoSelecionado campo="tipoApuracaoC"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
								<option value="<c:out value="${tipoApuracao.chavePrimaria}"/>" <c:if test="${contratoVO.tipoApuracaoC == tipoApuracao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoApuracao.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br class="quebraLinha2" />
							
							<ggas:labelContrato forCampo="consideraParadaProgramadaC" styleClass="rotuloVertical rotulo2Linhas">Considerar Paradas Programadas:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="consideraParadaProgramadaC" id="consideraParadaProgramadaCSim" value="true" <ggas:campoSelecionado campo="consideraParadaProgramadaC"/> <c:if test="${contratoVO.consideraParadaProgramadaC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraParadaProgramadaCSim">Sim</label>
							<input class="campoRadio" type="radio" name="consideraParadaProgramadaC" id="consideraParadaProgramadaCNao" value="false" <ggas:campoSelecionado campo="consideraParadaProgramadaC"/> <c:if test="${contratoVO.consideraParadaProgramadaC == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraParadaProgramadaCNao">N�o</label>
							<br class="quebraLinha" />
													
													
							<ggas:labelContrato forCampo="consideraFalhaFornecimentoC" styleClass="rotuloVertical rotulo2Linhas">Considerar Falha de Fornecimento:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="consideraFalhaFornecimentoC" id="consideraFalhaFornecimentoCSim" value="true" <ggas:campoSelecionado campo="consideraFalhaFornecimentoC"/> <c:if test="${contratoVO.consideraFalhaFornecimentoC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraFalhaFornecimentoCSim">Sim</label>
							<input class="campoRadio" type="radio" name="consideraFalhaFornecimentoC" id="consideraFalhaFornecimentoCNao" value="false" <ggas:campoSelecionado campo="consideraFalhaFornecimentoC"/> <c:if test="${contratoVO.consideraFalhaFornecimentoC == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraFalhaFornecimentoCNao">N�o</label>
							<br class="quebraLinha" />
													
							<ggas:labelContrato forCampo="consideraCasoFortuitoC" styleClass="rotuloVertical rotulo2Linhas">Considerar Casos Fortuitos:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" name="consideraCasoFortuitoC" id="consideraCasoFortuitoCSim" value="true" <ggas:campoSelecionado campo="consideraCasoFortuitoC"/> <c:if test="${contratoVO.consideraCasoFortuitoC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraCasoFortuitoCSim">Sim</label>
							<input class="campoRadio" type="radio" name="consideraCasoFortuitoC" id="consideraCasoFortuitoCNao" value="false" <ggas:campoSelecionado campo="consideraCasoFortuitoC"/> <c:if test="${contratoVO.consideraCasoFortuitoC == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="consideraCasoFortuitoCNao">N�o</label>
							<br class="quebraLinha" />
													
							<ggas:labelContrato forCampo="recuperavelC" styleClass="rotuloVertical rotulo2Linhas">Recuper�vel:</ggas:labelContrato>	
							<input class="campoRadio" type="radio" id="recuperavelCSim" name="recuperavelC" id="TOPrecuperacaoAutoTakeOrPaySim" value="true" <ggas:campoSelecionado campo="recuperavelC"/> <c:if test="${contratoVO.recuperavelC == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="recuperavelCSim">Sim</label>
							<input class="campoRadio" type="radio" id="recuperavelCNao" name="recuperavelC" id="TOPrecuperacaoAutoTakeOrPayNao" value="false" <ggas:campoSelecionado campo="recuperavelC"/> <c:if test="${contratoVO.recuperavelC == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="recuperavelCNao">N�o</label>
							<br class="quebraLinha" />

							<ggas:labelContrato styleId="rotuloMensalVariacaoInferior" styleClass="rotuloVertical rotulo2Linhas" forId="percentualNaoRecuperavelC" forCampo="percentualNaoRecuperavelC">Percentual N�o Recuper�vel:</ggas:labelContrato>
							<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualNaoRecuperavelC" name="percentualNaoRecuperavelC" size="3" maxlength="6" <ggas:campoSelecionado campo="percentualNaoRecuperavelC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" name="percentualNaoRecuperavelC" value="${contratoVO.percentualNaoRecuperavelC}">
							<label class="rotuloHorizontal rotuloInformativo">%</label>
							<br class="quebraLinha" />

							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoParadaProgramadaC" forCampo="apuracaoParadaProgramadaC">Forma de Apura��o do Volume de Parada Programada:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="apuracaoParadaProgramadaC" name="apuracaoParadaProgramadaC" <ggas:campoSelecionado campo="apuracaoParadaProgramadaC"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaApuracaoParadaProgramada}" var="apuracaoParadaProgramada">
								<option value="<c:out value="${apuracaoParadaProgramada.chavePrimaria}"/>" <c:if test="${contratoVO.apuracaoParadaProgramadaC == apuracaoParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${apuracaoParadaProgramada.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br />
							
							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoCasoFortuitoC" forCampo="apuracaoCasoFortuitoC">Forma de Apura��o do Volume de Caso Fortuito ou For�a Maior:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="apuracaoCasoFortuitoC" name="apuracaoCasoFortuitoC" <ggas:campoSelecionado campo="apuracaoCasoFortuitoC"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaApuracaoCasoFortuito}" var="apuracaoCasoFortuito">
								<option value="<c:out value="${apuracaoCasoFortuito.chavePrimaria}"/>" <c:if test="${contratoVO.apuracaoCasoFortuitoC == apuracaoCasoFortuito.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${apuracaoCasoFortuito.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br />

							<ggas:labelContrato styleId="rotuloConsumoReferenciaTakeOrPay" styleClass="rotuloVertical rotulo2Linhas" forId="apuracaoFalhaFornecimentoC" forCampo="apuracaoFalhaFornecimentoC">Forma de Apura��o do Volume de Falha de Fornecimento:</ggas:labelContrato>
							<select class="campoSelect campoHorizontal" id="apuracaoFalhaFornecimentoC" name="apuracaoFalhaFornecimentoC" <ggas:campoSelecionado campo="apuracaoFalhaFornecimentoC"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaApuracaoFalhaFornecimento}" var="apuracaoFalhaFornecimento">
								<option value="<c:out value="${apuracaoFalhaFornecimento.chavePrimaria}"/>" <c:if test="${contratoVO.apuracaoFalhaFornecimentoC == apuracaoFalhaFornecimento.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${apuracaoFalhaFornecimento.descricao}"/>
								</option>		
							</c:forEach>
							</select>
							<br />
									
						</div>
						
						<br />
						
						<fieldset class="conteinerBloco">
							<input name="Button" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeTakeOrPayC"/> value="Excluir" type="button" onclick="excluirCompromissoTOPAbaGeral();">
							<input name="Button" id="botaoAdicionarTakeOrPay" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeTakeOrPayC"/> value="Adicionar" type="button" onclick="adicionarCompromissoTOPAbaGeral();">
						</fieldset>
						
						<br />
						
						<c:set var="i" value="0" />
						<display:table id="compromissosTakeOrPayVO" class="dataTableGGAS dataTableAba" name="sessionScope.listaCompromissosTakeOrPayCVO" style="width: 428px" sort="list" requestURI="#">
							<display:column style="width: 25px" title="<input type='checkbox' id='checkAllCompromissos'/>">
								<input type="checkbox" class="checkItemCompromissos" name="indicePeriodicidadeTakeOrPay" value="<c:out value="${i}"/>">
							</display:column>
							<display:column title="Compromissos">
								${compromissosTakeOrPayVO.periodicidadeTakeOrPay.descricao}
							</display:column>
							<display:column title="In�cio Vig�ncia">
								<fmt:formatDate value="${compromissosTakeOrPayVO.dataInicioVigencia}" pattern="dd/MM/yyyy"/>
							</display:column>
							<display:column title="Fim Vig�ncia">
								<fmt:formatDate value="${compromissosTakeOrPayVO.dataFimVigencia}" pattern="dd/MM/yyyy"/>
							</display:column>
							<display:column title="Compromisso de Retirada ToP">
								<fmt:formatNumber type="percent" minFractionDigits="2" value="${compromissosTakeOrPayVO.margemVariacaoTakeOrPay}"></fmt:formatNumber>
							</display:column>
							<c:set var="i" value="${i+1}" />
						</display:table>
	
						<ggas:labelContrato forCampo="recuperacaoAutoTakeOrPayC" styleClass="rotuloVertical rotulo2Linhas">Recupera��o autom�tica?</ggas:labelContrato>	
						<input class="campoRadio" type="radio" name="recuperacaoAutoTakeOrPayC" id="recuperacaoAutoTakeOrPayCSim" value="true" <ggas:campoSelecionado campo="recuperacaoAutoTakeOrPayC"/> <c:if test="${contratoVO.recuperacaoAutoTakeOrPayC == true}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="recuperacaoAutoTakeOrPayCSim">Sim</label>
						<input class="campoRadio" type="radio" name=recuperacaoAutoTakeOrPayC id="recuperacaoAutoTakeOrPayCNao" value="false" <ggas:campoSelecionado campo="recuperacaoAutoTakeOrPayC"/> <c:if test="${contratoVO.recuperacaoAutoTakeOrPayC == false}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="recuperacaoAutoTakeOrPayCNao">N�o</label>
					</div>
				</fieldset>
							
		</fieldset>
		
		<fieldset id="contratoCol2" class="colunaFinal">
			<fieldset id="conteinerContrato">
				<legend>Contrato</legend>
				<div class="conteinerDados">
					<label class="rotulo" for="numeroContratoDisabled">N�mero do Contrato:</label>
					<input class="campoTexto campo2Linhas" type="text" id="numeroContratoDisabled" name="numeroContratoDisabled" size="10" value="${contratoVO.numeroFormatado}" disabled="disabled">
					<input class="campoTexto" type="hidden" id="numeroContrato" name="numeroContrato" value="${contratoVO.numeroContrato}" ><br class="quebraLinha" />
					
					<ggas:labelContrato styleClass="rotulo2Linhas" styleId="labelNumeroAnteriorContrato" forCampo="numeroAnteriorContrato">N�mero Anterior do Contrato:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas" type="text" id="numeroAnteriorContrato" name="numeroAnteriorContrato" maxlength="19" size="10" <ggas:campoSelecionado campo="numeroAnteriorContrato"/> value="${contratoVO.numeroAnteriorContrato}"><br class="quebraLinha" />
					
					<ggas:labelContrato styleClass="rotulo2Linhas" styleId="labelDescricaoContrato" forCampo="descricaoContrato">Descri��o:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas" type="text" id="descricaoContrato" name="descricaoContrato" maxlength="200" size="40" <ggas:campoSelecionado campo="descricaoContrato"/> onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${contratoVO.descricaoContrato}"><br class="quebraLinha" />
					
					<label class="rotulo" for="numeroAditivoDisabled">N�mero do Aditivo:</label>
					<input class="campoTexto" type="text" id="numeroAditivoDisabled" name="numeroAditivoDisabled" size="10" value="" disabled="disabled">
					<input class="campoTexto" type="hidden" id="numeroAditivo" name="numeroAditivo" value="${contratoVO.numeroAditivo}" ><br class="quebraLinha" />
					
					<ggas:labelContrato forCampo="dataAditivo">Data do Aditivo:</ggas:labelContrato>
					<input class="campoData" type="text" id="dataAditivo" name="dataAditivo" maxlength="10" <ggas:campoSelecionado campo="dataAditivo"/> value="${contratoVO.dataAditivo}"  <c:if test="${contratoVO.fluxoAlteracao eq 'true'}"> disabled="disabled" </c:if> ><br class="quebraLinha" />
					
					<ggas:labelContrato forCampo="descricaoAditivo">Descri��o do Aditivo:</ggas:labelContrato>
					<input class="campoTexto" type="text" size="30" maxlength="30"  id="descricaoAditivo" name="descricaoAditivo" <ggas:campoSelecionado campo="descricaoAditivo"/> value="${contratoVO.descricaoAditivo}"  <c:if test="${contratoVO.fluxoAlteracao eq 'true'}"> disabled="disabled" </c:if>  ><br class="quebraLinha" />	
						
										
					<ggas:labelContrato forCampo="situacaoContrato">Situa��o do Contrato:</ggas:labelContrato>
					<select name="situacaoContrato" id="situacaoContrato" class="campoSelect" <ggas:campoSelecionado campo="situacaoContrato"/> onchange="habilitarDesabilitarDataRecisao();" >
						<option value="-1">Selecione</option>
						<c:forEach items="${listaSituacaoContrato}" var="situacaoContrato">
							<option value="<c:out value="${situacaoContrato.chavePrimaria}"/>" <c:if test="${contratoVO.situacaoContrato == situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${situacaoContrato.descricao}"/>
							</option>		
						</c:forEach>
					</select><br class="quebraLinha" />
					
					<c:choose>
						<c:when test="${contratoVO.fluxoAlteracao eq 'false'}">
							<c:if test="${contratoVO.situacaoContrato eq 8}">
								<label class="rotulo" for="dataAssinaturaDisabled">Data da assinatura:</label>
								<input class="campoTexto campo2Linhas" type="text" id="numeroContratoDisabled" name="numeroContratoDisabled" size="10" value="${contratoVO.dataAssinatura}" disabled="disabled">
								<input class="campoTexto" type="hidden" id="dataAssinatura" name="dataAssinatura" value="${contratoVO.dataAssinatura}" ><br class="quebraLinha" />
							</c:if>
							<c:if test="${contratoVO.situacaoContrato ne 8}">
								<ggas:labelContrato forCampo="dataAssinatura">Data da assinatura:</ggas:labelContrato>							
								<input class="campoData" type="text" id="dataAssinatura" name="dataAssinatura" maxlength="10" <ggas:campoSelecionado campo="dataAssinatura"/> value="${contratoVO.dataAssinatura}"><br class="quebraLinha" />
							</c:if>				
						</c:when>
						<c:otherwise>
							<c:if test="${contratoVO.situacaoContrato ne 8}">
								<label class="rotulo" for="dataAssinaturaDisabled">Data da assinatura:</label>
								<input class="campoTexto campo2Linhas" type="text" id="numeroContratoDisabled" name="numeroContratoDisabled" size="10" value="${contratoVO.dataAssinatura}" disabled="disabled">
								<input class="campoTexto" type="hidden" id="dataAssinatura" name="dataAssinatura" value="${contratoVO.dataAssinatura}" ><br class="quebraLinha" />
							</c:if>
							<c:if test="${contratoVO.situacaoContrato eq 8}">
								<ggas:labelContrato forCampo="dataAssinatura">Data da assinatura:</ggas:labelContrato>							
								<input class="campoData" type="text" id="dataAssinatura" name="dataAssinatura" maxlength="10" <ggas:campoSelecionado campo="dataAssinatura"/> value="${contratoVO.dataAssinatura}"><br class="quebraLinha" />
							</c:if>								
						</c:otherwise>
					</c:choose>
									
					<ggas:labelContrato forCampo="numeroEmpenho">N�mero do Empenho:</ggas:labelContrato>
					<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="numeroEmpenho" name="numeroEmpenho" maxlength="12" size="20" <ggas:campoSelecionado campo="numeroEmpenho"/> value="${contratoVO.numeroEmpenho}" onkeypress="return formatarCampoInteiro(event);"><br class="quebraLinha" />
					
					<ggas:labelContrato styleClass="rotulo2Linhas dataVencObrigacoesContratuais" forCampo="dataVencObrigacoesContratuais">Vencimento das Obriga��es Contratuais:</ggas:labelContrato>
					<input class="campoData campo2Linhas" type="text" id="alterarContrato-dataVencObrigacoesContratuais" name="dataVencObrigacoesContratuais" maxlength="10" <ggas:campoSelecionado campo="dataVencObrigacoesContratuais"/> value="${contratoVO.dataVencObrigacoesContratuais}"><br class="quebraDataRadio" />
					
					<ggas:labelContrato forCampo="indicadorAnoContratual">Defini��o de Ano Contratual:</ggas:labelContrato>
					<input class="campoRadio" type="radio" name="indicadorAnoContratual" id="anoContratualMaiusculo" <ggas:campoSelecionado campo="indicadorAnoContratual"/> value="true" <c:if test="${contratoVO.indicadorAnoContratual == true}">checked="checked"</c:if>>
					<label class="rotuloRadio" id="labelAnoContratualMaiusculo" for="anoContratualMaiusculo">ANO(Fiscal)</label>
					<input class="campoRadio" type="radio" name="indicadorAnoContratual" id="anoContratualMinusculo" <ggas:campoSelecionado campo="indicadorAnoContratual"/> value="false" <c:if test="${contratoVO.indicadorAnoContratual == false}">checked="checked"</c:if>>
					<label class="rotuloRadio" id="labelAnoContratualMinusculo" for="anoContratualMinusculo">ano(Calend�rio)</label><br class="quebraLinha" />
					
					<ggas:labelContrato forCampo="renovacaoAutomatica">Renova��o Autom�tica:</ggas:labelContrato>
					<input class="campoRadio" type="radio" name="renovacaoAutomatica" id="indicadorRenovacaoAutomaticaSim" <ggas:campoSelecionado campo="renovacaoAutomatica"/> value="true" <c:if test="${contratoVO.renovacaoAutomatica == true}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorRenovacaoAutomaticaSim">Sim</label>
					<input class="campoRadio" type="radio" name="renovacaoAutomatica" id="indicadorRenovacaoAutomaticaNao" <ggas:campoSelecionado campo="renovacaoAutomatica"/> value="false" <c:if test="${contratoVO.renovacaoAutomatica == false}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorRenovacaoAutomaticaNao">N�o</label><br class="quebraRadio" />
					<fieldset id="ConteinerQuantDiasRenovacao">
						<ggas:labelContrato forCampo="numDiasRenoAutoContrato">Per�odo de Renova��o:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal" type="text" id="numDiasRenoAutoContrato" name="numDiasRenoAutoContrato" maxlength="4" size="4" <ggas:campoSelecionado campo="numDiasRenoAutoContrato"/> value="${contratoVO.numDiasRenoAutoContrato}" onkeypress="return formatarCampoInteiro(event);">
						<label class="rotuloInformativo campoHorizontal" for="numDiasRenoAutoContrato">dias</label><br />
					</fieldset>
					<fieldset id="ConteinerTempoAntecedenciaRenovacao">
						<ggas:labelContrato styleClass="rotulo2Linhas" styleId="labelTempoAntecedenciaRenovacao" forCampo="tempoAntecedenciaRenovacao">Tempo de Anteced�ncia para Negocia��o da Renova��o:</ggas:labelContrato>
						<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="tempoAntecedenciaRenovacao" name="tempoAntecedenciaRenovacao" maxlength="3" size="2" <ggas:campoSelecionado campo="tempoAntecedenciaRenovacao"/> value="${contratoVO.tempoAntecedenciaRenovacao}" onkeypress="return formatarCampoInteiro(event);">
						<label class="rotuloInformativo3Linhas" for="tempoAntecedenciaRenovacao">dias</label>
					</fieldset>
					
					<fieldset>
						<ggas:labelContrato forCampo="valorContrato">Valor do contrato:</ggas:labelContrato>
						<input class="campoTexto campoValorReal" type="text" id="valorContrato" name="valorContrato" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="valorContrato"/> value="<fmt:formatNumber value='${contratoVO.valorContrato}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha" />
						<fieldset Class="faturamentoAgrupamento">
							<ggas:labelContrato forCampo="faturamentoAgrupamento">Faturamento Agrupado?</ggas:labelContrato>
							<input class="campoRadio" type="radio" name="faturamentoAgrupamento" 
								id="alterarContrato-FaturamentoAgrupamento" value="true"
								onclick="showContainerTipoAgrupamento()"
								<ggas:campoSelecionado campo="faturamentoAgrupamento"/> 
								<c:if test="${contratoVO.faturamentoAgrupamento == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="faturamentoAgrupamentoSim" 
								id="alterarContrato-FaturamentoAgrupamentoSimNao">Sim</label>
							<input class="campoRadio" type="radio" name="faturamentoAgrupamento" 
								id="alterarContrato-FaturamentoAgrupamento" value="false" 
								onclick="hideContainerTipoAgrupamento()"
								<ggas:campoSelecionado campo="faturamentoAgrupamento"/> 
								<c:if test="${contratoVO.faturamentoAgrupamento == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="faturamentoAgrupamentoNao" 
								id="alterarContrato-FaturamentoAgrupamentoSimNao">N�o</label>
							<br class="quebraRadio" />
						</fieldset>
						<fieldset id="ContainerTipoAgrupamento">
							<ggas:labelContrato forCampo="tipoAgrupamento">Tipo de Agrupamento:</ggas:labelContrato>
							<select name="tipoAgrupamento" id="tipoAgrupamento" class="campoSelect" <ggas:campoSelecionado campo="tipoAgrupamento"/>>
								<option value="-1">Selecione</option>
									<c:forEach items="${listaTipoAgrupamento}" var="tipoAgrupamento">
								<option value="<c:out value="${tipoAgrupamento.chavePrimaria}"/>" <c:if test="${contratoVO.tipoAgrupamento == tipoAgrupamento.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoAgrupamento.descricao}"/>
							</option>		
						</c:forEach>
						</select><br class="quebraLinha" />
						</fieldset>
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="emissaoFaturaAgrupada">Emiss�o de Fatura<br />Agrupada?</ggas:labelContrato>
						<input class="campoRadio" type="radio" name="emissaoFaturaAgrupada" id="alterarContrato-emissaoFaturaAgrupada" <ggas:campoSelecionado campo="emissaoFaturaAgrupada"/> value="true" <c:if test="${contratoVO.emissaoFaturaAgrupada == true}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="emissaoFaturaAgrupadaSim" id="alterarContrato-emissaoFaturaAgrupadaSimNao">Sim</label>
						<input class="campoRadio" type="radio" name="emissaoFaturaAgrupada" id="alterarContrato-emissaoFaturaAgrupada" <ggas:campoSelecionado campo="emissaoFaturaAgrupada"/> value="false" <c:if test="${contratoVO.emissaoFaturaAgrupada == false}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="emissaoFaturaAgrupadaNao" id="alterarContrato-emissaoFaturaAgrupadaSimNao">N�o</label>
						<br class="quebraLinha2" />
						
						<ggas:labelContrato forCampo="formaCobranca">Tipo de Conv�nio:</ggas:labelContrato>
						<select class="campoSelect" id="formaCobranca" name="formaCobranca" <ggas:campoSelecionado campo="formaCobranca"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaFormaCobranca}" var="formaCobranca">
								<option value="<c:out value="${formaCobranca.chavePrimaria}"/>" <c:if test="${contratoVO.formaCobranca eq formaCobranca.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${formaCobranca.descricao}"/>
								</option>		
							</c:forEach>						
						</select>
						
						<ggas:labelContrato forCampo="arrecadadorConvenio">Arrecadador Conv�nio:</ggas:labelContrato>
						<%-- <label class="rotulo" id="arrecadadorConvenio" for="arrecadadorConvenio">Arrecadador Conv�nio:</label> --%>
						<select class="campoSelect" id="arrecadadorConvenio" name="arrecadadorConvenio" >
								<option value="-1">Selecione</option>
							<c:forEach items="${listaArrecadadorConvenio}" var="arrecadadorConvenio">
								<option tipo="<c:out value="${arrecadadorConvenio.tipoConvenio.chavePrimaria}"/>" value="<c:out value="${arrecadadorConvenio.chavePrimaria}"/>"
								 <c:if test="${contratoVO.arrecadadorConvenio == arrecadadorConvenio.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${arrecadadorConvenio.codigoConvenio} - ${fn:toUpperCase(arrecadadorConvenio.arrecadadorCarteiraCobranca.tipoCarteira.descricao) == 'SEM REGISTRO' ? 'CN' : 'CR'} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
								</option>		
							</c:forEach>						
						</select>
						
						<ggas:labelContrato forCampo="arrecadadorConvenioDebitoAutomatico">Arrecadador Conv�nio para D�bito Autom�tico:</ggas:labelContrato>
						<select class="campoSelect" id="arrecadadorConvenioDebitoAutomatico" name="arrecadadorConvenioDebitoAutomatico" <ggas:campoSelecionado campo="arrecadadorConvenioDebitoAutomatico"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaArrecadadorConvenioDebitoAutomatico}" var="arrecadadorConvenioDebitoAutomatico">
								<option tipo="<c:out value="${arrecadadorConvenioDebitoAutomatico.tipoConvenio.chavePrimaria}"/>" value="<c:out value="${arrecadadorConvenioDebitoAutomatico.chavePrimaria}"/>"
									<c:if test="${contratoVO.arrecadadorConvenioDebitoAutomatico == arrecadadorConvenioDebitoAutomatico.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${arrecadadorConvenioDebitoAutomatico.codigoConvenio} - ${arrecadadorConvenioDebitoAutomatico.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
								</option>
							</c:forEach>
						</select>
						
						<ggas:labelContrato styleClass="rotulo2Linhas" styleId="labelVolumeReferencia" forCampo="volumeReferencia">Volume de Refer�ncia:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas" type="text" id="volumeReferencia" name="volumeReferencia" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,7,2);" maxlength="10" size="10" <ggas:campoSelecionado campo="volumeReferencia"/> value="${contratoVO.volumeReferencia}"><br class="quebraLinha" />
					
					</fieldset>
					
					<fieldset>
						<label class="rotulo" for="tipoPeriodicidadePenalidade">Tipo
							Periodicidade Penalidade:</label> <input
							class="campoRadio campoRadio3Linhas" type="radio" value="true"
							name="tipoPeriodicidadePenalidade"
							<c:if test="${contratoVO.tipoPeriodicidadePenalidade == 'true'}">checked="checked"</c:if> />
						<label class="rotuloRadio"
							for="tipoPeriodicidade">Civil</label> <input
							class="campoRadio campoRadio3Linhas" type="radio" value="false"
							name="tipoPeriodicidadePenalidade"
							<c:if test="${contratoVO.tipoPeriodicidadePenalidade == 'false'}">checked="checked"</c:if> />
						<label class="rotuloRadio"
							for="tipoPeriodicidade">Cont�nuo</label>
	
					</fieldset>
				</div>
			</fieldset>
			
			<fieldset id="conteinerMulta">
				<legend>Pagamento Efetuado em Atraso</legend>
				<div class="conteinerDados">
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="indicadorMultaAtraso">O contrato � pass�vel de Multa?</ggas:labelContrato>
					<input class="campoRadio" type="radio" name="indicadorMultaAtraso" id="alterarContrato-indicadorMultaAtraso" <ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="true" onclick="manipularCamposJurosMulta(this.value, 'percentualMulta');" <c:if test="${contratoVO.indicadorMultaAtraso == true}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorMultaSim" id="alterarContrato-indicadorMultaAtrasoSimNao">Sim</label>
					<input class="campoRadio" type="radio" name="indicadorMultaAtraso" id="alterarContrato-indicadorMultaAtraso" <ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="false" onclick="manipularCamposJurosMulta(this.value, 'percentualMulta');" <c:if test="${contratoVO.indicadorMultaAtraso == false}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorMultaNao" id="alterarContrato-indicadorMultaAtrasoSimNao">N�o</label><br class="quebraLinha" />

					<label class="rotulo rotulo2Linhas campoObrigatorio" id="labelpercentualMulta"><span id="spanpercentualMulta" class="campoObrigatorioSimbolo2"> </span>Percentual de Multa:</label>
					<input class="campoTexto campoHorizontal" type="text" id="percentualMulta" name="percentualMulta" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5" value="${contratoVO.percentualMulta}">
					<label class="rotuloInformativo rotuloHorizontal" for="descontoEfetivoEconomia">%</label><br class="quebraLinha">
					
					<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="indicadorJurosMora">O contrato � pass�vel de Juros de Mora?</ggas:labelContrato>
					<input class="campoRadio" type="radio" name="indicadorJurosMora" id="alterarContrato-indicadorJurosMora" <ggas:campoSelecionado campo="indicadorJurosMora"/> value="true" onclick="manipularCamposJurosMulta(this.value, 'percentualJurosMora');" <c:if test="${contratoVO.indicadorJurosMora == true}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorJurosMoraSim" id="alterarContrato-indicadorJurosMoraSimNao">Sim</label>
					<input class="campoRadio" type="radio" name="indicadorJurosMora" id="alterarContrato-indicadorJurosMora" <ggas:campoSelecionado campo="indicadorJurosMora"/> value="false" onclick="manipularCamposJurosMulta(this.value, 'percentualJurosMora');" <c:if test="${contratoVO.indicadorJurosMora == false}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorJurosMoraNao" id="alterarContrato-indicadorJurosMoraSimNao">N�o</label><br class="quebraLinha2" />

					<label class="rotulo rotulo2Linhas campoObrigatorio" id="labelpercentualJurosMora"><span id="spanpercentualJurosMora" class="campoObrigatorioSimbolo2"> </span>Percentual de Juros de Mora:</label>
					<input class="campoTexto campoHorizontal" type="text" id="percentualJurosMora" name="percentualJurosMora" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5" value="${contratoVO.percentualJurosMora}">
					<label class="rotuloInformativo rotuloHorizontal" for="descontoEfetivoEconomia">%</label><br class="quebraLinha">
					
					<ggas:labelContrato forCampo="indiceCorrecaoMonetaria">Corre��o Monet�ria:</ggas:labelContrato>
					<select name="indiceCorrecaoMonetaria" id="indiceCorrecaoMonetaria" class="campoSelect" <ggas:campoSelecionado campo="indiceCorrecaoMonetaria"/>>
						<option value="-1">Selecione</option>
						<c:forEach items="${listaIndiceCorrecaoMonetaria}" var="indiceCorrecaoMonetaria">
							<option value="<c:out value="${indiceCorrecaoMonetaria.chavePrimaria}"/>" <c:if test="${contratoVO.indiceCorrecaoMonetaria == indiceCorrecaoMonetaria.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${indiceCorrecaoMonetaria.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</div>
			</fieldset>
			
			<fieldset id="conteinerPenalidades">
					<legend>Penalidades</legend>
					<div class="conteinerDados">
						<fieldset id="conteinerSobreDemanda">
							<legend>Penalidade por retirada a Maior/Menor:</legend>
							<div class="conteinerDados2">
							
							<ggas:labelContrato forCampo="penalidadeRetMaiorMenor">Penalidade:</ggas:labelContrato>
							<select class="campoSelect campo2Linhas" name="penalidadeRetMaiorMenor" id="penalidadeRetMaiorMenor" <ggas:campoSelecionado campo="penalidadeRetMaiorMenor"/> >
									<option value="-1">Selecione</option>
									<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="penalidadeRetMaiorMenor">
										<option value="<c:out value="${penalidadeRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.penalidadeRetMaiorMenor == penalidadeRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${penalidadeRetMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />
								
								<ggas:labelContrato forCampo="periodicidadeRetMaiorMenor">Periodicidade:</ggas:labelContrato>
							<select class="campoSelect campo2Linhas" name="periodicidadeRetMaiorMenor" id="periodicidadeRetMaiorMenor" <ggas:campoSelecionado campo="periodicidadeRetMaiorMenor"/> >
									<option value="-1">Selecione</option>
									<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="periodicidadeRetMaiorMenor">
										<option value="<c:out value="${periodicidadeRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.periodicidadeRetMaiorMenor == periodicidadeRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${periodicidadeRetMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />
							
							<ggas:labelContrato forCampo="baseApuracaoRetMaiorMenor">Base de Apura��o:</ggas:labelContrato>
								<select class="campoSelect" name="baseApuracaoRetMaiorMenor" id="baseApuracaoRetMaiorMenor" <ggas:campoSelecionado campo="baseApuracaoRetMaiorMenor"/>>
									<option value="-1">Selecione</option>
									<c:forEach items="${listaBaseApuracao}" var="baseApuracaoRetMaiorMenor">
										<option value="<c:out value="${baseApuracaoRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.baseApuracaoRetMaiorMenor == baseApuracaoRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${baseApuracaoRetMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />
								
								<ggas:labelContrato forCampo="dataIniVigRetirMaiorMenor">In�cio Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataIniVigRetirMaiorMenor" name="dataIniVigRetirMaiorMenor" maxlength="10" <ggas:campoSelecionado campo="dataIniVigRetirMaiorMenor"/> value="${contratoVO.dataIniVigRetirMaiorMenor}"><br class="quebraLinha" />
						
							<ggas:labelContrato forCampo="dataFimVigRetirMaiorMenor">Fim Vig�ncia:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataFimVigRetirMaiorMenor" name="dataFimVigRetirMaiorMenor" maxlength="10" <ggas:campoSelecionado campo="dataFimVigRetirMaiorMenor"/> value="${contratoVO.dataFimVigRetirMaiorMenor}"><br class="quebraLinha" />
						
							
							<ggas:labelContrato forCampo="precoCobrancaRetirMaiorMenor">Tabela de Pre�o para Cobran�a:</ggas:labelContrato>
								<select class="campoSelect" name="precoCobrancaRetirMaiorMenor" id="precoCobrancaRetirMaiorMenor" <ggas:campoSelecionado campo="precoCobrancaRetirMaiorMenor"/>>
									<option value="-1">Selecione</option>
									<c:forEach items="${listaPrecoCobranca}" var="precoCobrancaRetirMaiorMenor">
										<option value="<c:out value="${precoCobrancaRetirMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.precoCobrancaRetirMaiorMenor == precoCobrancaRetirMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${precoCobrancaRetirMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />
							
							<ggas:labelContrato forCampo="tipoApuracaoRetirMaiorMenor">Forma de C�lculo para Cobran�a:</ggas:labelContrato>
								<select class="campoSelect" name="tipoApuracaoRetirMaiorMenor" id="tipoApuracaoRetirMaiorMenor" <ggas:campoSelecionado campo="tipoApuracaoRetirMaiorMenor"/>>
									<option value="-1">Selecione</option>
									<c:forEach items="${listaTipoApuracao}" var="tipoApuracaoRetirMaiorMenor">
										<option value="<c:out value="${tipoApuracaoRetirMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.tipoApuracaoRetirMaiorMenor == tipoApuracaoRetirMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${tipoApuracaoRetirMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />
							
							
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualCobRetMaiorMenor">Percentual de Cobran�a:</ggas:labelContrato>
								<input class="campoTexto campo2Linhas campoHorizontal"
									type="text" id="percentualCobRetMaiorMenor"
									<ggas:campoSelecionado campo="percentualCobRetMaiorMenor"/>
									name="percentualCobRetMaiorMenor" maxlength="6" size="4"
									onkeypress="return formatarCampoDecimal(event, this, 3, 2);"
									onblur="aplicarMascaraNumeroDecimal(this, 2);"
									value="${contratoVO.percentualCobRetMaiorMenor}">
								<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>
								
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualCobIntRetMaiorMenor">Percentual de Cobran�a para Consumo Superior Durante Aviso de Interrup��o:</ggas:labelContrato>
								<input class="campoTexto campo2Linhas campoHorizontal"
									type="text" id="percentualCobIntRetMaiorMenor"
									<ggas:campoSelecionado campo="percentualCobIntRetMaiorMenor"/>
									name="percentualCobIntRetMaiorMenor" maxlength="6" size="4"
									onkeypress="return formatarCampoDecimal(event, this, 3, 2);"
									onblur="aplicarMascaraNumeroDecimal(this, 2);"
									value="${contratoVO.percentualCobIntRetMaiorMenor}">
								<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>	
							
							
								<ggas:labelContrato forCampo="consumoReferenciaRetMaiorMenor">Consumo Refer�ncia:</ggas:labelContrato>			
								<select class="campoSelect campo2Linhas" name="consumoReferenciaRetMaiorMenor" id="consumoReferenciaRetMaiorMenor" <ggas:campoSelecionado campo="consumoReferenciaRetMaiorMenor"/> >
									<option value="-1">Selecione</option>
									<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaRetMaiorMenor">
										<option value="<c:out value="${consumoReferenciaRetMaiorMenor.chavePrimaria}"/>" <c:if test="${contratoVO.consumoReferenciaRetMaiorMenor == consumoReferenciaRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${consumoReferenciaRetMaiorMenor.descricao}"/>
										</option>		
									</c:forEach>
								</select><br class="quebraLinha" />

								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualRetMaiorMenor">Percentual Por Retirada:</ggas:labelContrato>
								<input class="campoTexto campo2Linhas campoHorizontal"
									type="text" id="percentualRetMaiorMenor"
									<ggas:campoSelecionado campo="percentualRetMaiorMenor"/>
									name="percentualRetMaiorMenor" maxlength="6" size="4"
									onkeypress="return formatarCampoDecimal(event, this, 3, 2);"
									onblur="aplicarMascaraNumeroDecimal(this, 2);"
									value="${contratoVO.percentualRetMaiorMenor}">
								<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>
								<br class="quebraLinha" />	
								
								<ggas:labelContrato forCampo="indicadorImposto" styleClass="rotuloVertical rotulo2Linhas">Usar pre�os s/ impostos para cobran�a:</ggas:labelContrato>	
								<input class="campoRadio" type="radio" name="indicadorImposto" id="indicadorImpostoSim" value="true" <ggas:campoSelecionado campo="indicadorImposto"/> <c:if test="${contratoVO.indicadorImposto == true}">checked="checked"</c:if>>
								<label class="rotuloRadio" for="indicadorImpostoSim">Sim</label>
								<input class="campoRadio" type="radio" name="indicadorImposto" id="indicadorImpostoNao" value="false" <ggas:campoSelecionado campo="indicadorImposto"/> <c:if test="${contratoVO.indicadorImposto == false}">checked="checked"</c:if>>
								<label class="rotuloRadio" for="indicadorImpostoNao">N�o</label>
							</div>
							
							<br />
						
						<fieldset class="conteinerBloco">
							<input name="Button" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeRetMaiorMenor"/> value="Excluir" type="button" onclick="excluirPenalidadeRetiradaMaiorMenorGeralAditamento();">
							<input name="Button" id="botaoAdicionarRetirMaiorMenor" class="bottonRightCol" <ggas:campoSelecionado campo="periodicidadeRetMaiorMenor"/> value="Adicionar" type="button" onclick="adicionarPenalidadeRetiradaMaiorMenorGeralAditamento();">
						</fieldset>
						
						<br />
						
						<c:set var="i" value="0" />
						<display:table id="penalidadesRetMaiorMenorVO" class="dataTableGGAS dataTableAba" name="sessionScope.listaPenalidadesRetMaiorMenorVO" style="width: 428px" sort="list" requestURI="#">
							<display:column style="width: 25px" title="<input type='checkbox' id='checkAllCompromissos'/>">
								<input type="checkbox" class="checkItemCompromissos" name="indicePenalidadeRetiradaMaiorMenor" value="<c:out value="${i}"/>">
							</display:column>
							<display:column title="Penalidade">
								${penalidadesRetMaiorMenorVO.penalidadeRetiradaMaiorMenor.descricao}
							</display:column>
							<display:column title="Consumo Refer�ncia">
								${penalidadesRetMaiorMenorVO.entidadeConsumoReferenciaRetMaiorMenor.descricao}
							</display:column>
							<display:column title="In�cio Vig�ncia">
								<fmt:formatDate value="${penalidadesRetMaiorMenorVO.dataInicioVigencia}" />
							</display:column>
							<display:column title="Fim Vig�ncia">
								<fmt:formatDate value="${penalidadesRetMaiorMenorVO.dataFimVigencia}" />
							</display:column>
							<c:set var="i" value="${i+1}" />
						</display:table>
						</fieldset>
						
				
						
						<fieldset id="conteinerDeliveryPay">
							<legend>Penalidade por Delivery Or Pay:</legend>
							<div class="conteinerDados2">
								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualTarifaDoP">Percentual Sobre a Tarifa do G�s:</ggas:labelContrato>								
								<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualTarifaDoP" <ggas:campoSelecionado campo="percentualTarifaDoP"/> name="percentualTarifaDoP" maxlength="6" size="4" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${contratoVO.percentualTarifaDoP}">
								<label class="rotuloInformativo2Linhas rotuloHorizontal">%</label>
							</div>
						</fieldset>
						
						<fieldset id="conteinerDeliveryPay">
							<legend>Penalidade por G�s Fora de Especifica��o:</legend>
							<div class="conteinerDados2">
								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualSobreTariGas">Percentual Sobre a Tarifa do G�s:</ggas:labelContrato>								
								<input class="campoTexto campoHorizontal" type="text" id="percentualSobreTariGas" <ggas:campoSelecionado campo="percentualSobreTariGas"/> name="percentualSobreTariGas" maxlength="6" size="4" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${contratoVO.percentualSobreTariGas}">
								<label class="rotuloInformativo rotuloHorizontal" for="percentualSobreTariGas">%</label>
							</div>
						</fieldset>
					</div>
				</fieldset>			
			
			<c:set var="dataQDC" value="QDCContrato" />
			<c:set var="volumeQDC" value="QDCContrato" />
			<c:set var="modalidade" value="false" />
			<c:set var="idConteinerQDC" value="conteinerQDCContrato" />
			<c:set var="nomeComponente" value="QDC do Contrato" />
			<%@ include file="/jsp/contrato/contrato/contrato/include/dadosQDCContrato.jsp" %>
			
			<fieldset id="estimativaQtdeRecuperar">
					<legend>Estimativa de Quantidade a Recuperar</legend>
					<div class="conteinerDados">
						<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMinimoRelacaoQDCC" forCampo="percMinDuranteRetiradaQPNRC">Percentual m�nimo em rela��o � QDC para recupera��o:</ggas:labelContrato>				
						<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMinimoRelacaoQDCC" name="percMinDuranteRetiradaQPNRC" maxlength="6" size="3" <ggas:campoSelecionado campo="percMinDuranteRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percMinDuranteRetiradaQPNRC}">
						<label class="rotuloInformativo rotuloHorizontal">%</label><br />
					
					 	<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoRelacaoQDCC" forCampo="percDuranteRetiradaQPNRC">Percentual m�ximo em rela��o � QDC para recupera��o:</ggas:labelContrato>				
						<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoRelacaoQDCC" name="percDuranteRetiradaQPNRC" maxlength="6" size="3" <ggas:campoSelecionado campo="percDuranteRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percDuranteRetiradaQPNRC}">
						<label class="rotuloInformativo rotuloHorizontal">%</label><br />
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoTerminoContratoC" forCampo="percDuranteRetiradaQPNRC">Percentual m�ximo da recupera��o com o t�rmino do contrato:</ggas:labelContrato>									
						<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoTerminoContratoC" name="percFimRetiradaQPNRC" maxlength="6" size="3"  <ggas:campoSelecionado campo="percFimRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percFimRetiradaQPNRC}">
						<label class="rotuloInformativo rotuloHorizontal">%</label><br /><br />
						
						<hr class="linhaSeparadora">
						
						<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataInicioRecuperacaoC" forCampo="dataInicioRetiradaQPNRC">Data de inicio da recupera��o:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataInicioRecuperacaoC" name="dataInicioRetiradaQPNRC" maxlength="10" <ggas:campoSelecionado campo="dataInicioRetiradaQPNRC"/> value="${contratoVO.dataInicioRetiradaQPNRC}" onblur="mostrarAsterisco(this.value,['dataMaximaRecuperacaoC','percentualMaximoTerminoContratoC','percentualMaximoRelacaoQDCC']);"><br />
				
						<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataMaximaRecuperacaoC" forCampo="dataFimRetiradaQPNRC">Data m�xima para recupera��o:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataMaximaRecuperacaoC" name="dataFimRetiradaQPNRC" maxlength="10" <ggas:campoSelecionado campo="dataFimRetiradaQPNRC"/> value="${contratoVO.dataFimRetiradaQPNRC}"><br />
						
						<hr class="linhaSeparadora">
						
						<ggas:labelContrato styleId="rotuloTempoValidadeRetiradaQPNR" styleClass="rotulo2Linhas" forCampo="tempoValidadeRetiradaQPNRC">Tempo de validade para recupera��o da QPNR do ano anterior:</ggas:labelContrato>
						<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="tempoValidadeRetiradaQPNRC" name="tempoValidadeRetiradaQPNRC" maxlength="2" size="1" <ggas:campoSelecionado campo="tempoValidadeRetiradaQPNRC"/> value="${contratoVO.tempoValidadeRetiradaQPNRC}" onkeypress="return formatarCampoInteiro(event);">
						<label class="rotuloInformativo3Linhas rotuloHorizontal" for="tempoValidadeRetiradaQPNRC">anos</label>
					</div>
				</fieldset>			
			
		</fieldset>
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Contratos.</p>
		<hr class="linhaSeparadora">
	</fieldset>	
	
	<fieldset class="conteinerPesquisarIncluirAlterar">		
		<legend id="legendPesquisarImoveis"><span id="asteriscoPesquisarImoveis" class="campoObrigatorioSimbolo">* </span>Sele��o dos Pontos de Consumo</legend>
		<fieldset class="conteinerBotoesInserir1">
			<a name="ancoraPesquisarImovelAditamentoContrato"></a>
			<input name="idImovel" type="hidden" id="idImovel" value="">
			<input name="idPontoConsumoLista" type="hidden" id="idPontoConsumoLista" value="">
			<input name="Button" class="bottonRightCol2" id="pesquisarImoveisContrato" value="Pesquisar Im�veis " type="button" onclick="exibirPopupPesquisaImovel();">		
		</fieldset>
		
		<fieldset id="pesquisaImovelContrato" class="conteinerBloco">	
		<c:forEach items="${listaImovelPontoConsumoVO}" var="imovelPontoConsumoVO" >
			<hr class="linhaSeparadora2" />
			<label class="rotulo">Im�vel:</label>
			<span class="itemDetalhamento">${imovelPontoConsumoVO.nome}</span>
			<a class="apagarItemPesquisa" onclick="return confirm('Deseja retirar o im�vel?');" href="javascript:retirarImovel('${imovelPontoConsumoVO.chavePrimaria}');" title="Retirar Im�vel"><img src="<c:url value="/imagens/deletar_x.png"/>" /></a>
			
			<display:table class="dataTableGGAS dataTablePesquisa" name="${imovelPontoConsumoVO.listaPontoConsumo}" sort="list" id="pontoConsumo" pagesize="15" excludedParams="" requestURI="#">
				<display:column style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='todos' id='todos${pontoConsumo.imovel.chavePrimaria}' onclick='controlarCheckBox(${pontoConsumo.imovel.chavePrimaria});'/>">
					<input type="checkbox" name="chavesPrimariasPontoConsumo" value="${pontoConsumo.chavePrimaria}" id="chavePrimariaPontoConsumo${pontoConsumo.chavePrimaria}">
					<!-- campo hidden para guardar a chave prim�ria do im�vel -->
			      	<input type="hidden" name="imovelChavesPonto" value="${pontoConsumo.imovel.chavePrimaria}">
				</display:column>	     	 
				
				<display:column sortable="false" title="Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
					<a href="javascript:detalharMedidorPontoConsumo(${pontoConsumo.chavePrimaria});" title="Clique aqui para detalhar os dados do medidor instalado">
						<c:out value='${pontoConsumo.descricao}'/>
					</a>
				</display:column>
								
				<display:column sortable="false" title="Segmento" style="width: 100px">
					<a href="javascript:detalharMedidorPontoConsumo(${pontoConsumo.chavePrimaria});" title="Clique aqui para detalhar os dados do medidor instalado">
						<c:out value='${pontoConsumo.segmento.descricao}'/>						
					</a>				
				</display:column>
								
				<display:column sortable="false" title="Ramo de Atividade" style="width: 250px">
					<a href="javascript:detalharMedidorPontoConsumo(${pontoConsumo.chavePrimaria});" title="Clique aqui para detalhar os dados do medidor instalado">
						<c:out value='${pontoConsumo.ramoAtividade.descricao}'/>						
					</a>				
				</display:column>
								
				<display:column style="width: 25px">
					<a class="alterarContrato-apagarItemPesquisa" onclick="return validarRetirarPontoConsumo('<c:out value="${contratoVO.faturamentoAgrupamento}"/>');" href="javascript:retirarPontoConsumo('${pontoConsumo.chavePrimaria}','${imovelPontoConsumoVO.chavePrimaria}');" title="Retirar Ponto de Consumo"><img src="<c:url value="/imagens/deletar_x.png"/>" /></a>
				</display:column>
			</display:table>
		</c:forEach>	
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="botaoCancelar" id="botaoCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
		<input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparCampos();">
		
		<fieldset class="conteinerBotoesDirFixo4">
			<c:if test="${listaImovelPontoConsumoVO ne null}">
				<c:choose>
					<c:when test="${contratoVO.fluxoAlteracao eq 'false'}">
						<input name="button" id="aditar-contrato" class="bottonRightCol2 botaoAditar" value="Salvar" type="button" onclick="aditar();">	
					</c:when>
					<c:otherwise>
						<input name="button" class="bottonRightCol2 botaoAditar" value="Salvar" type="button" onclick="alterar();">	
					</c:otherwise>
				</c:choose>
				<input name="button" class="bottonRightCol2 bottonRightColUltimo botaoAvancar" value="Avan�ar >>" type="button" onclick="avancar();">
			</c:if>
		</fieldset>
	</fieldset>	
</form:form>
