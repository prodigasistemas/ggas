<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Encerrar/Rescindir Contrato</h1>
	
	
<script type="text/javascript">

	$(document).ready(function(){

		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});
		
		
		// Datepicker
		$("#periodoRecisaoCampo").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$('.dataLeituras').inputmask("99/99/9999",{placeholder:"_"});
        $("input.botaoAdicionar").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/check2.gif) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
        $("input.botaoLeituraSalva").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/circle_green.png) no-repeat','border':'0','height':'16px','width':'16px'});
		
		
	});
	
	function cancelar() {
		//location.href = '<c:url value="/pesquisarContrato"/>';
		$("#chavePrimaria").val("");
		submeter('0','pesquisarContrato');
	}
	
	function selecionarPontoConsumo(id) {
		document.forms[0].idPontoConsumo.value = id;
	}
	
	function pesquisar(idPontoConsumo) {
		document.getElementById('idPontoConsumo').value = idPontoConsumo;
	}

	function imprimirFaturaEncerramento(idFatura) {
		document.getElementById('chavePrimariaFatura').value = idFatura;
		submeter('0', 'imprimirFatura');
	}

	function  finalizar(){
		if(verificaDadosPreenchidoFaturaResidual()) {
			if(confirm('H� pontos de consumo sem data e leitura salvas para a Fatura Residual, deseja continuar?')){
				submeter('0', 'exibirEncerrarRescindirContrato');
			}
		} else {
			submeter('0', 'exibirEncerrarRescindirContrato');
		}


	}	
	
	function incluirFatura() {
		var isFaturamentoAgrupado = <c:out value="${contratoVO.indicadorFaturamentoAgrupado}"/>;//document.getElementById('indicadorFaturamentoAgrupado').value;
		var listaRemovidosAgrupados = document.getElementById('listaIdsPontoConsumoRemovidosAgrupados').value;
		
		if(isFaturamentoAgrupado){	
			if((listaRemovidosAgrupados == undefined || listaRemovidosAgrupados == '')){
				var listaPontos = '';
				var delimitador = '';
				var chavesPontoConsumo = document.getElementsByName('chavesPontoConsumo');
				
				for (var i = 0; i < chavesPontoConsumo.length; i++) {
					listaPontos += delimitador + chavesPontoConsumo[i].value;
					delimitador = ",";
				}
					
				document.getElementById('paramListaIdsPontoConsumoRemovidosAgrupados').value = listaPontos;
			}else{
				document.getElementById('paramListaIdsPontoConsumoRemovidosAgrupados').value = listaRemovidosAgrupados;
			}	
		}else{
			document.getElementById('paramListaIdsPontoConsumoRemovidosAgrupados').value = '';
		}
		
		submeter('0','exibirInclusaoFatura');
	}

	
	function habilitarInclusao(valor, faturaGerada) {
		document.getElementById('idPontoConsumo').value = valor;
		var itemClicado = document.getElementById('chavePontoConsumo' + valor);
		
		if (itemClicado != undefined) {
			if (itemClicado.checked && faturaGerada == 'false') {
				$("#botaoIncluir").attr("disabled", false);
			} else {
				$("#botaoIncluir").attr("disabled", true);
			}
		} 
	}
	
	function habitiltar(){
		if(${contratoVO.finalizacaoEncerramento} == true){
			$("#botaoFinalizar").removeAttr("disabled");
		}
	}
	
	function init() {
		document.getElementById('idContratoEncerrarRescindir').value = document.getElementById('chavePrimaria').value;
		habitiltar();
	}

	function salvarLeituraData(idPontoConsumo) {
		payload = {
				idPontoConsumo: parseInt(idPontoConsumo),
				dataLeitura: $('#dataLeitura_'+idPontoConsumo).val(),
				valorLeitura:  $('#valorLeitura_'+idPontoConsumo).val(),
				situacaoContrato: "E",
			}

		if(payload.dataLeitura == "" || payload.valorLeitura == "") {
			alert("Data ou Valor Invalido");
			return false;
		}
		
		$.ajax({
			type: 'POST',
			url: '/ggas/salvarDataLeitura',
			data: JSON.stringify(payload),
			success: function(data) {
				try {
					if (data.erro) {
				        $("input#botaoAdicionar_"+idPontoConsumo).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/circle_red.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
						return false;
					}
			        $("input#botaoAdicionar_"+idPontoConsumo).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/circle_green.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			        $("input#botaoAdicionar_"+idPontoConsumo).attr("title", "Salvo");

				} catch (e) {
			        $("input#botaoAdicionar_"+idPontoConsumo).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/circle_red.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
				}
			},
			contentType: 'application/json'
		});
		return true;
		
	}

	function verificaDadosPreenchidoFaturaResidual(){
		var retorno = false;
		$('#pontoConsumo tbody tr').each(function() {
			if($(this).find(".botaoAdicionar").attr('title') == "Salvar"){
				retorno = true;
			}
		});	
		return retorno;
	}

	addLoadEvent(init);	
	
</script>

<form:form method="post" action="exibirEncerrarRescindirContrato" name="faturaForm">
	<input name="acao" type="hidden" id="acao" value="exibirEncerrarRescindirContrato">
	<input name="indexLista" type="hidden" id="indexLista" value="-1">
	<input name="postBack" type="hidden" id="postBack" value="false">
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoVO.chavePrimaria}" />	
	<input name="chavePrimariaFatura" type="hidden" id="chavePrimariaFatura" value="" />
	<input name="listaIdsPontoConsumoRemovidos" type="hidden" id="listaIdsPontoConsumoRemovidos" value="${contratoVO.listaIdsPontoConsumoRemovidos}">
	
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${contratoVO.idPontoConsumo}" />
	<input name="idContratoEncerrarRescindir" type="hidden" id="idContratoEncerrarRescindir" value="${idContratoEncerrarRescindir}">	
	<!-- faturamentoAgrupamento 
	<input name="indicadorFaturamentoAgrupado" type="hidden" id="indicadorFaturamentoAgrupado" value="<c:choose> <c:when test="${contratoVO.indicadorFaturamentoAgrupado == true}">true</c:when> <c:otherwise>false</c:otherwise> </c:choose>"/>
	-->
	<input name="listaIdsPontoConsumoRemovidosAgrupados" type="hidden" id="listaIdsPontoConsumoRemovidosAgrupados" value="${contratoVO.listaIdsPontoConsumoRemovidosAgrupados}" />
	<input name="paramListaIdsPontoConsumoRemovidosAgrupados" type="hidden" id="paramListaIdsPontoConsumoRemovidosAgrupados" value="" />
		
	<c:if test="${listaPontoConsumo ne null}">
		<fieldset class="conteinerPesquisarIncluirAlterar">
			<p class="orientacaoInicial">Selecione um Ponto de Consumo na lista abaixo para exibir as Faturas. Para incluir uma fatura associada ao Ponto de Consumo marque a caixa de sele��o � esquerda do Ponto de Consumo e clique em <span class="destaqueOrientacaoInicial">Incluir</span>.</p>
			
			<label class="rotulo campoObrigatorio" for="periodoRecisao"><span class="campoObrigatorioSimbolo">* </span>Data de Rescis�o:</label>
			<input type="text" class="campoData campoHorizontal" id="periodoRecisaoCampo" name="periodoRecisaoCampo" maxlength="10" size="8" value="${periodoRecisaoCampo}"
				style="margin-bottom: 20px;">
				
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Funcion�rio:</label>
			<select class="campoSelect" id="funcionario" name="funcionario" style="width: 200px">
				<c:forEach items="${listaFuncionario}" var="funcionario">
					<option value="<c:out value="${funcionario.chavePrimaria}"/>" <c:if test="${funcionarioSelecionado.chavePrimaria == funcionario.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${funcionario.nome}"/>
					</option>		
		    	</c:forEach>
			</select><br/><br/><br/><br/><br/>
				
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoFatura">
		        <display:column style="text-align: center; width: 25px">
		        	<input type="radio" name="chavesPontoConsumo" id="chavePontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}" onclick="javascript:habilitarInclusao(this.value, '<c:out value="${pontoConsumo.idFatura ne null}"/>')">
		        </display:column>
		        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq"  style="text-align: left">
		            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${pontoConsumo.descricao}"/>
		            </a>
		        </display:column>
		        <display:column title="Segmento" style="width: 130px">
		            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${pontoConsumo.descricaoSegmento}"/>
		            </a>
		        </display:column>
		        <display:column title="Situa��o" style="width: 160px">
		            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${pontoConsumo.descricaoSituacao}"/>
		            </a>
		        </display:column>
<%-- 		        <display:column  title="Fatura" style="width: 50px" >
			    	<c:if test="${pontoConsumo.idFatura ne null}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if>
			    </display:column>
			    <display:column title="2� Via" style="width: 50px">
		        	<c:if test="${pontoConsumo.idFatura ne null}">
						<a href='javascript:imprimirFaturaEncerramento(<c:out value='${pontoConsumo.idFatura}'/>);'>
				    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir" title="Imprimir" border="0">
				    	</a>
			    	</c:if>					
				</display:column> --%>
			    <display:column title="Data Leitura" style="width: 150px;">
			    	<c:choose>
				    	<c:when test="${pontoConsumo.dataLeitura ne null}">
				    		<a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				    			<c:out value="${pontoConsumo.dataLeitura}"/>
				    		</a>
				    	</c:when>
				    	<c:otherwise>
							<input type="text" style="margin-left:20px;" class="campoTexto campoHorizontal bootstrapDP dataLeituras" id="dataLeitura_${pontoConsumo.chavePrimaria}" name="dataLeitura_${pontoConsumo.chavePrimaria}" maxlength="10" size="8" value="${dataLeitura}">			
				    	</c:otherwise>
			    	</c:choose>
				</display:column>
			    <display:column title="Leitura" style="width: 100px;">
					<c:choose>
				    	<c:when test="${pontoConsumo.valorLeitura ne null}">
				    		<a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				    			<c:out value="${pontoConsumo.valorLeitura}"/>
				    		</a>
				    	</c:when>
				    	<c:otherwise>
							<input type="text" class="campoTexto campoHorizontal valorLeituras" id="valorLeitura_${pontoConsumo.chavePrimaria}" name="valorLeitura_${pontoConsumo.chavePrimaria}" maxlength="10" size="8" value="${leitura}" onkeypress="return formatarCampoInteiro(event)">				
				    	</c:otherwise>
					</c:choose>
				</display:column>
				<display:column title="Salvar Data e Leitura" style="width: 50px;">
					<c:choose>
						<c:when test="${pontoConsumo.dataLeitura ne null}">
							<input  class="botaoLeituraSalva" id="botaoLeituraSalva_${pontoConsumo.chavePrimaria}" title="Salvo" /
						</c:when>
						<c:otherwise>
							<input type="button" class="botaoAdicionar" id="botaoAdicionar_${pontoConsumo.chavePrimaria}" onClick="return salvarLeituraData(${pontoConsumo.chavePrimaria});" title="Salvar" />
						</c:otherwise>
					</c:choose>
				</display:column>								
		    </display:table>
		</fieldset>
	</c:if>
	
	<fieldset class="conteinerBotoes"> 
	    <input class="bottonRightCol2 bottonLeftColUltimo" type="button" value="Cancelar" onclick="cancelar();"> 
	    <!-- No periodo de migracao teve a necessidade de finalizar contratos sem incerir fatura de encerramente, por isso a decis�o incluir a fatura esta a cargo do operador -->
	    <!-- <input class="bottonRightCol2 botaoGrande1" id="botaoFinalizar" type="button" value="Finalizar" disabled="disabled"; style="margin-left: 10px" onclick="finalizar();"> -->
	    <input class="bottonRightCol2 botaoGrande1" id="botaoFinalizar" type="button" value="Finalizar" style="margin-left: 10px" onclick="finalizar();">
		<!--  
		<input class="bottonRightCol2 botaoGrande1" name="botaoIncluir" id="botaoIncluir" type="button" value="Incluir Fatura" disabled="disabled"; onclick="incluirFatura();">
		-->
	</fieldset>
	
</form:form>
