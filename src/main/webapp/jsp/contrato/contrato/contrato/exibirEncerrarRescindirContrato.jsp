<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<jsp:useBean id="controladorContrato" class="br.com.ggas.contrato.contrato.impl.ControladorContratoImpl" />

<h1 class="tituloInterno">Encerrar/Rescindir Contrato</h1>

<script type="text/javascript">
	var atualizarBotaoIncluirNota;
	
	$(document).ready(function(){

		
		// Datepicker
		$("#periodoRecisaoCampo").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#dataVencimento").datepicker({ minDate: '+0d', changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

		atualizarBotaoIncluirNota = function(){
			if($("#nao").attr("checked")){
				// Bot�es e campos
				$("#botaoIncluirNota").removeAttr("disabled");
				
				$("#botaoCalcular, #valorAdicional, #valorAdicionalMensal").attr("disabled", "disabled");
				$("input[name=valorInvestimento], input[name=periodoInvestimento], input[name=igpmInicial], input[name=periodoRecisaoCampo], input[name=igpmRecisao], input[name=periodoConsumo]").attr("disabled", "disabled");
				$("input[name=qtdDiariaContratada], input[name=qtdConsumida], input[name=qtdMinimaContratada], input[name=diasRestantes], input[name=tarifaGas], input[name=valorIndenizacao], input[name=valorCobrado]").attr("disabled", "disabled");

				// Labels
				$("label[for=investimentoCorrigido], label[for=valorIndenizacao], label[for=valorCobrado]").addClass("rotuloDesabilitado");
				$("label[for=valorAdicional]").addClass("rotuloDesabilitado");//.removeClass("campoObrigatorio")
				$("label[for=valorInvestimento], label[for=periodoInvestimento], label[for=igpmInicial], label[for=periodoRecisao], label[for=igpmRecisao], label[for=periodoConsumo], #aa, #am,").addClass("rotuloDesabilitado"); 
				$("label[for=qtdDiariaContratada], label[for=qtdConsumida], label[for=qtdMinimaContratada]").addClass("rotuloDesabilitado");	
				$("label[for=diasRestantes], label[for=tarifaGas]").addClass("rotuloDesabilitado");
			}
			else{
				// Bot�es e campos
				if($("#valorIndenizacao").val() != ""){
					$("#botaoIncluirNota").removeAttr("disabled");
				}else{
					$("#botaoIncluirNota").attr("disabled", "disabled");
				}
				
				$("#botaoCalcular, #valorAdicional, #valorAdicionalMensal").removeAttr("disabled");
				$("input[name=valorInvestimento], input[name=periodoInvestimento], input[name=igpmInicial], input[name=periodoRecisaoCampo], input[name=igpmRecisao], input[name=periodoConsumo]").removeAttr("disabled");
				$("input[name=qtdDiariaContratada], input[name=qtdConsumida], input[name=qtdMinimaContratada], input[name=diasRestantes], input[name=tarifaGas], input[name=valorIndenizacao], input[name=valorCobrado]").removeAttr("disabled");

				// Labels
				$("label[for=investimentoCorrigido], label[for=valorIndenizacao], label[for=valorCobrado]").removeClass("rotuloDesabilitado");
				$("label[for=valorAdicional]").removeClass("rotuloDesabilitado");//.addClass("campoObrigatorio")
				$("label[for=valorInvestimento], label[for=periodoInvestimento], label[for=igpmInicial], label[for=periodoRecisao], label[for=igpmRecisao], label[for=periodoConsumo], #aa, #am,").removeClass("rotuloDesabilitado"); 
				$("label[for=qtdDiariaContratada], label[for=qtdConsumida], label[for=qtdMinimaContratada]").removeClass("rotuloDesabilitado");
				$("label[for=diasRestantes], label[for=tarifaGas]").removeClass("rotuloDesabilitado");
			}
		};

		$('[name="cobrarIndenizacao"]').click(
				function(){
					atualizarBotaoIncluirNota();
				}
		);

		animatedcollapse.show(['dadosContrato','dadosPontoConsumo','dadosCliente']);
		
		//alert(${contratoVO.finalizacaoContrato});
		if(${contratoVO.finalizacaoContrato} == true){
			$("#botaoFinalizar").removeAttr("disabled");
		}
	});
	
	function imprimirMultaRecisoria(idFatura) {
		document.getElementById('chavePrimariaFatura').value = idFatura;
		submeter('0', 'imprimirFatura');
	}

	function preencherMemoriaDeCalculo(chave) {
		document.forms[0].idPontoConsumoEncerrar.value = chave;
		submeter("0", "exibirEncerrarRescindirContrato");
	}

	function incluirNota(){
		submeter("0", "incluirNotaMultaRecisoria");
	}
	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaContrato"/>';
	}	

	function finalizar(){
		submeter("0", "finalizarContrato");
	}
	
	function calcular() {
		
		var idContrato = $("#chavePrimaria").val();
		var tipoMulta = '<c:out value="${contratoVO.multaRecisoria}"/>';
		
		var valorInvestimento = isNaN($("input[name=valorInvestimento]").val()) ? 0 : $("input[name=valorInvestimento]").val();		
		
		var periodoInvestimento = $("input[name=periodoInvestimento]").val();
		var igpmInicial = isNaN($("input[name=igpmInicial]").val()) ? 0 : $("input[name=igpmInicial]").val();
		
		var periodoRecisao = $("input[name=periodoRecisaoCampo]").val();
		var igpmRecisao = isNaN($("input[name=igpmRecisao]").val()) ? 0 : $("input[name=igpmRecisao]").val();

		var periodoConsumo = isNaN($("input[name=periodoConsumo]").val()) ? 0 : $("input[name=periodoConsumo]").val();
		var valorAdicionalMensal = isNaN($("#valorAdicionalMensal").val()) ? 0 : $("#valorAdicionalMensal").val()/100;
		var valorAdicional = $("#valorAdicional").val();
				
		var qtdDiariaContratada = isNaN($("input[name=qtdDiariaContratada]").val()) ? 0 : $("input[name=qtdDiariaContratada]").val();
		var qtdConsumida = isNaN($("input[name=qtdConsumida]").val()) ? 0 : $("input[name=qtdConsumida]").val();
		var qtdMinimaContratada = isNaN($("input[name=qtdMinimaContratada]").val()) ? 0 : $("input[name=qtdMinimaContratada]").val();

		var diasRestantes = isNaN($("input[name=diasRestantes]").val()) ? 0 : $("input[name=diasRestantes]").val();
		var tarifaGas = isNaN($("input[name=tarifaGas]").val()) ? 0 : $("input[name=tarifaGas]").val();
		
		if(idContrato == "" || parseInt(idContrato) <= 0){
			alert("Erro ao calcular indeniza��o: identificador do contrato inv�lido.");
			return;
		}else if(tipoMulta == "" || parseInt(tipoMulta) <= 0){
			alert("Erro ao calcular indeniza��o: tipo de c�lculo indefinido.");
			return;
		}else if(tipoMulta != ${controladorContrato.contratoMultaPorQtdMedia} && 
				(valorAdicionalMensal == undefined || valorAdicionalMensal == "" ||
						periodoRecisao == undefined || periodoRecisao == "" ||
						 valorAdicional == undefined || valorAdicional == "")){
			alert("Campos obrigat�rios n�o preenchidos!");
			return;
		}

		
		
		AjaxService.calcularValorIndenizacaoEncerrarRescindir( idContrato, tipoMulta, 
       		valorInvestimento, periodoInvestimento, igpmInicial, periodoRecisao, igpmRecisao, periodoConsumo, valorAdicionalMensal, 
       		qtdDiariaContratada, qtdConsumida, qtdMinimaContratada, diasRestantes, tarifaGas, 
			{ 
           		callback: function(calculo) {
       				if(calculo['erro'] != undefined && calculo['erro'] != ""){
           				alert(calculo['erro']);
       				}else if(calculo['erroCalculoIndenizacao'] != undefined && calculo['erroCalculoIndenizacao'] != ""){
           				alert(calculo['erroCalculoIndenizacao']);
       				}else {
       					$("#investimentoCorrigido").val(calculo['investimentoCorrigido']);
       					
			           	$("#valorIndenizacao").val(calculo['valorIndenizacao']);
			           	$("#valorIndenizacaoDisabled").val(calculo['valorIndenizacaoFormatado']);
			           	
			           	$("#valorCobrado").val(calculo['valorIndenizacaoFormatado']);
       				}
       	  		}, 
       	 		async: false
       		}
       	);
		
		atualizarBotaoIncluirNota();
	}

	function validarPeriodoRecisao() {
		
		var periodoInvestimento = $("input[name=periodoInvestimento]").val();
		var periodoRecisao = $("input[name=periodoRecisaoCampo]").val();
		
		if(periodoInvestimento == undefined || periodoInvestimento == "" || 
			periodoRecisao == undefined || periodoRecisao == ""){
			return;
		}
		
		AjaxService.validarPeriodoRecisao( periodoInvestimento, periodoRecisao, 
				{ 
       		callback: function(validarPeriodoRecisao) {
			if(validarPeriodoRecisao['erro'] != undefined && validarPeriodoRecisao['erro'] != ""){
		          	alert(validarPeriodoRecisao['erro']);
		      }else{

					var valorIgpm = validarPeriodoRecisao['igpmRecisao'];
					var campoIgpm = document.forms[0].igpmRecisaoDisabled;

					if(valorIgpm != undefined){
						campoIgpm.value = valorIgpm.replace(".", ",");
					}else{
						campoIgpm.value = valorIgpm;
					}
					//formataValorMonetario(campoIgpm, 6);	

					var valorPeriodoConsumo = validarPeriodoRecisao['periodoConsumo'];
					var campoPeriodoConsumo = document.forms[0].periodoConsumoDisabled;

					campoPeriodoConsumo.value = valorPeriodoConsumo;
					formataValorMonetario(campoPeriodoConsumo, 6);
		           	
		           	$("#valorIndenizacao").val('');
		           	$("#investimentoCorrigido").val('');
		      }
   	  		}, 
   	 		async: false
   			}
       	);
	}

	function calcularAdicionalMensal(){
		var valorAdicional = $("#valorAdicional").val();
		if(!isNaN(valorAdicional)){
			valorAdicional = (valorAdicional/12).toFixed(2);
			$("#valorAdicionalMensal").val(valorAdicional);
			$("#valorAdicionalMensalDisabled").val($("#valorAdicionalMensal").val().replace('.',','));
		}
	}
	
	animatedcollapse.addDiv('dadosContrato', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=0');
	
</script>

<form:form method="post" action="finalizarContrato">
	<input name="acao" type="hidden" id="acao" value="finalizarContrato" >
	<input name="postBack" type="hidden" id="postBack" value="true" >
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoVO.chavePrimaria}" >
	<input name="chavePrimariaFatura" type="hidden" id="chavePrimariaFatura" value="" >
	<input name="impressao" type="hidden" id="impressao" value="notaDebito" >
	
	<input name="idPontoConsumoEncerrar" type="hidden" id="idPontoConsumoEncerrar" value="${contratoVO.idPontoConsumoEncerrar}" >
	<input name="pontosSemRecisao" type="hidden" id="pontosSemRecisao" value="${contratoVO.pontosSemRecisao}" >
	<input name="listaIdsPontoConsumoRemovidos" type="hidden" id="listaIdsPontoConsumoRemovidos" value="${contratoVO.listaIdsPontoConsumoRemovidos}" >
	
	<fieldset id="encerrarRescindirContrato" class="conteinerPesquisarIncluirAlterar">
		
		<a id="linkDadosContrato" class="linkExibirDetalhes" href="#" rel="toggle[dadosContrato]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Contrato <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosContrato" class="conteinerDadosDetalhe" >
			<fieldset class="coluna">
				<label class="rotulo">N�mero do Contrato:</label>
				<span class="itemDetalhamento"><c:out value="${contratoVO.numeroFormatado}"/></span><br />
				
				<label class="rotulo">Situa��o do Contrato:</label>
				<span class="itemDetalhamento"><c:out value="${situacaoContrato.descricao}"/></span><br />
			</fieldset>
			
			<fieldset class="colunaFinal">					
				<label class="rotulo">Data da assinatura:</label>
				<span class="itemDetalhamento"><c:out value="${contratoVO.dataAssinatura}"/></span><br />
				
				<label class="rotulo">Vencimento das Obriga��es Contratuais:</label>
				<span class="itemDetalhamento"><c:out value="${contratoVO.dataVencObrigacoesContratuais}"/></span>
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosPontoConsumo" class="conteinerBloco">
			<display:table class="dataTableGGAS dataTablePesquisa" name="${listaPontoConsumo}" sort="list" id="pontoConsumo" pagesize="15" excludedParams="" requestURI="#">
				<display:column sortable="false" title="Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
				<a href="#" onclick="preencherMemoriaDeCalculo(<c:out value='${pontoConsumo.chavePrimaria}'/>);">
					<span class="linkInvisivel"></span>
					<input name="chavesPrimariasPontoConsumoVO" type="hidden" id="chavesPrimariasPontoConsumoVO" value="${pontoConsumo.chavePrimaria}"/>
					<c:out value='${pontoConsumo.nomeImovel}'/> - <c:out value='${pontoConsumo.descricaoPontoConsumo}'/>
				</a>
				</display:column>
				<display:column sortable="false" title="Segmento" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
				<a href='javascript:preencherMemoriaDeCalculo(<c:out value='${pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				    <c:out value='${pontoConsumo.descricaoSegmento}'/>
				</a>
				</display:column>
				<display:column  sortable="false" title="Ramo de Atividade" style="width: 250px" >
				<a href='javascript:preencherMemoriaDeCalculo(<c:out value='${pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				   <c:out value='${pontoConsumo.descricaoRamoAtividade}'/>
				</a>
				</display:column>
				<display:column  title="" style="width: 50px" >
					<c:if test="${pontoConsumo.rubrica eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if>
				</display:column>
				<display:column title="" style="width: 50px">
		        	<c:if test="${pontoConsumo.idNotaDebito ne null}">
						<a href='javascript:imprimirMultaRecisoria(<c:out value='${pontoConsumo.idNotaDebito}'/>);'>
				    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir" title="Imprimir" border="0">
				    	</a>
			    	</c:if>					
				</display:column>
			</display:table>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<c:choose>
			<c:when test="${contratoVO.idCliente ne null && contratoVO.idCliente > 0}">
				<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosCliente" class="conteinerDadosDetalhe">
					<fieldset class="coluna">
						<label class="rotulo">Nome:</label>
						<span class="itemDetalhamento"><c:out value="${contratoVO.nomeCliente}"/></span><br />
						
						<label class="rotulo">CPF/CNPJ:</label>
						<span class="itemDetalhamento"><c:out value="${contratoVO.documentoCliente}"/></span><br />
					</fieldset>
					
					<fieldset class="colunaFinal">
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento"><c:out value="${contratoVO.enderecoCliente}"/></span><br />
						
						<label class="rotulo">E-mail:</label>
						<span class="itemDetalhamento"><c:out value="${contratoVO.emailCliente}"/></span><br />
					</fieldset>
				</fieldset>
			</c:when>
			
			<c:otherwise>
				Cliente n�o encontrado
			</c:otherwise>
		</c:choose>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset id="dadosCalculoIndenizacao" style="clear: both;">
			<legend>Mem�ria de C�lculo</legend>
			
			<fieldset class="conteinerDados" style="width: 420px">
				<input type="hidden" id="periodoRecisao" name="periodoRecisao" value="${dadosCalculoIndenizacao.dataRecisaoFormatada}">
								
				<c:if test="${contratoVO.multaRecisoria ne controladorContrato.contratoMultaPorQtdMedia}">				
					<label class="rotulo" for="valorInvestimento">Valor do Investimento:</label>					
					<input type="text" class="campoDesabilitado" name="valorInvestimentoDisabled" maxlength="10" size="13" readonly="readonly" value="<fmt:formatNumber value="${dadosCalculoIndenizacao.valorInvestimento}" type="currency"/>">
					<input type="hidden" name="valorInvestimento" value="${dadosCalculoIndenizacao.valorInvestimento}">
					<br class="quebraLinha" />
					
					<label class="rotulo" for="periodoInvestimento">Per�odo do Investimento:</label>
					<input type="text" class="campoTexto campoHorizontal campoDesabilitado" name="periodoInvestimento" maxlength="10" size="8" readonly="readonly" value="${dadosCalculoIndenizacao.dataInvestimentoFormatada}">																					
					
					<label class="rotuloEntreCampos" for="igpmInicial">IGPM - Inicial:</label>
					<input type="text" class="campoTexto campoHorizontal campoDesabilitado" name="igpmInicialDisabled" maxlength="10" size="10" readonly="readonly" value="<fmt:formatNumber value="${dadosCalculoIndenizacao.igpmInvestimento}" />" >
					<input type="hidden" name="igpmInicial" value="${dadosCalculoIndenizacao.igpmInvestimento}">
					<br class="quebraLinha" />
					
					<label class="rotulo campoObrigatorio" for="periodoRecisao"><span class="campoObrigatorioSimbolo">* </span>Per�odo da Rescis�o:</label>
					<input type="text" class="campoData campoHorizontal" id="periodoRecisaoCampo" name="periodoRecisaoCampo" maxlength="10" size="8" value="${dadosCalculoIndenizacao.dataRecisaoFormatada}" onchange="validarPeriodoRecisao();">
					
					<label class="rotuloEntreCampos" for="igpmRecisao">IGPM - Rescis�o:</label>
					<input type="text" class="campoTexto campoHorizontal campoDesabilitado" name="igpmRecisaoDisabled" maxlength="10" size="10" readonly="readonly" value="<fmt:formatNumber value="${dadosCalculoIndenizacao.igpmRecisao}" />" >
					<input type="hidden" name="igpmRecisao" value="${dadosCalculoIndenizacao.igpmRecisao}">
					<br class="quebraLinha" />
					
					<label class="rotulo" for="periodoConsumo">Per�odo de Consumo:</label>
					<input type="text" class="campoTexto campoDesabilitado" name="periodoConsumoDisabled" maxlength="10" readonly="readonly" value="<fmt:formatNumber value="${dadosCalculoIndenizacao.mesesPeriodoConsumo}" />">
					<input type="hidden" name="periodoConsumo" value="${dadosCalculoIndenizacao.mesesPeriodoConsumo}">
					<br class="quebraLinha" />
					
					<label class="rotulo campoObrigatorio" for="valorAdicional"><span class="campoObrigatorioSimbolo">* </span>Adicional:</label>
					<input type="text" class="campoTexto campoHorizontal" id="valorAdicional" name="valorAdicional" maxlength="6" size="6" 
						 onblur="calcularAdicionalMensal();" onkeypress="return formatarCampoDecimal(event,this,3,2);">
					<label id="aa" class="rotuloInformativo">% a.a.</label>
					
					<input type="text" class="campoTexto campoHorizontal campoDesabilitado" id="valorAdicionalMensalDisabled" name="valorAdicionalMensalDisabled" maxlength="6" size="6" readonly="readonly" >
					<input type="hidden" id="valorAdicionalMensal" name="valorAdicionalMensal" value="${dadosCalculoIndenizacao.mesesPeriodoConsumo}">
					<label id="am" class="rotuloInformativo">% a.m.</label>
					<br class="quebraLinha" />
					
					<label class="rotulo" for="investimentoCorrigido">Investimento Corrigido:</label>
					<input type="text" class="campoValorRealDesabilitado" id="investimentoCorrigido" name="investimentoCorrigido" maxlength="15" readonly="readonly" >
					<br class="quebraLinha" />				
				</c:if>
				
				<c:if test="${contratoVO.multaRecisoria ne controladorContrato.contratoMultaPorTempoRelacionamento}">
					<label class="rotulo" for="qtdDiariaContratada">Quantidade Di�ria Contratada:</label>
					<input type="text" class="campoTexto campoDesabilitado" id="qtdDiariaContratada" name="qtdDiariaContratada" maxlength="15" readonly="readonly" value="${dadosCalculoIndenizacao.qtdDiariaContratada}">
				</c:if>
				
				<c:if test="${contratoVO.multaRecisoria eq controladorContrato.contratoMultaPorQtdMinimaContratada}">
					<label class="rotulo" for="qtdConsumida">Quantidade Consumida:</label>
					<input type="text" class="campoTexto campoDesabilitado" id="qtdConsumidaDisabled" name="qtdConsumidaDisabled" maxlength="15" readonly="readonly" value="<fmt:formatNumber value="${dadosCalculoIndenizacao.qtdConsumida}" />">
					<input type="hidden" id="qtdConsumida" name="qtdConsumida" value="${dadosCalculoIndenizacao.qtdConsumida}">
					
					<label class="rotulo" for="qtdMinimaContratada">Quantidade M�nima Contratada:</label>
					<input type="text" class="campoTexto campoDesabilitado" id="qtdMinimaContratada" name="qtdMinimaContratada" maxlength="15" readonly="readonly" value="${dadosCalculoIndenizacao.qtdMinimaContratada}">
				</c:if>
				
				<c:if test="${contratoVO.multaRecisoria eq controladorContrato.contratoMultaPorQtdMedia}">
					<label class="rotulo" for="diasRestantes">Dias Restantes:</label>
					<input type="text" class="campoTexto campoDesabilitado" id="diasRestantes" name="diasRestantes" maxlength="15" readonly="readonly" value="${dadosCalculoIndenizacao.diasRestantes}">
					<br class="quebraLinha" />
					
					<label class="rotulo" for="tarifaGas">Tarifa do G�s:</label>
					<input type="text" class="campoDesabilitado" id="tarifaGasDisabled" name="tarifaGasDisabled" maxlength="15" readonly="readonly" value="<fmt:formatNumber value="${dadosCalculoIndenizacao.tarifaGas}" type="currency"/>">
					<input type="hidden" id="tarifaGas" name="tarifaGas"value="${dadosCalculoIndenizacao.tarifaGas}">
					<br class="quebraLinha" />
				</c:if>
				
				<label id="rotuloValorIndenizacao" class="rotulo" for="valorIndenizacao">Valor da Indeniza��o:</label>
				<input type="text" class="campoValorRealDesabilitado" id="valorIndenizacaoDisabled" name="valorIndenizacaoDisabled" maxlength="15" readonly="readonly">
				<input class="campoTexto" type="hidden" id="valorIndenizacao" name="valorIndenizacao" >
				
				<label id="rotuloValorCobrado" class="rotulo" for="valorCobrado">Valor Cobrado:</label>
				<input type="text" class="campoValorReal" id="valorCobrado" name="valorCobrado" maxlength="15" >
				
				<label class="rotulo campoObrigatorio" for="dataVencimento"><span class="campoObrigatorioSimbolo">* </span>Data de Vencimento:</label>
				<input type="text" class="campoData campoHorizontal" id="dataVencimento" name="dataVencimento" maxlength="10" size="8" value="${contratoVO.dataVencimento}"
						onchange="">
				<input type="button" class="botaoInline" id="botaoCalcular" value="Calcular" onclick="calcular();">
				
				<p class="legenda" style="margin-top: 0"><span class="campoObrigatorioSimbolo">* </span>campo obrigat�rio para o c�lculo da indeniza��o.</p>
			</fieldset>
			
			<br class="quebraLinha" />
			
			<fieldset id="conteinerCobrarIndenizacao">			
				<label class="rotulo" style="margin-right: 20px" for="cobrarIndenizacao">Cobrar indeniza��o: </label>
				
				<input class="campoRadio" type="radio" name="cobrarIndenizacao" id="sim" value="true" 
					<c:if test="${contratoVO.cobrarIndenizacao eq 'true'}">checked="checked"</c:if>>
				<label class="rotuloRadio" for="ativo">Sim</label>
				
				<input class="campoRadio" type="radio" name="cobrarIndenizacao" id="nao" value="false" 
					<c:if test="${contratoVO.cobrarIndenizacao eq 'false'}">checked="checked"</c:if>>
				<label class="rotuloRadio" for="inativo">N�o</label>
			</fieldset>
			
			<br class="quebraLinha" />
			
		</fieldset>
		
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input class="bottonRightCol2 botaoGrande1" id="botaoFinalizar" type="button" value="Finalizar" onclick="finalizar();" disabled="disabled" style="margin-left: 10px" >
		<input class="bottonRightCol2 botaoGrande1" id="botaoIncluirNota" type="button" value="Incluir Nota" onclick="incluirNota();" disabled="disabled" >
	</fieldset>
	
</form:form>
	