<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script>

	$(function(){				
		var periodoTesteDataInicio = $("#periodoTesteDataInicio").val();
				
		if(periodoTesteDataInicio == ""){
			$("#periodoTesteDataInicio").prop("style", "display: none");
		}
		
		var isDisabledBotaoAditar = $("input[name='buttonAditar']").prop("disabled");
		var isDisabledBotaoAlterar = $("input[name='buttonAlterar']").prop("disabled");
		
		
		
		//Desabilita todos os campos input (todos), select e textarea
		$(":input").attr("disabled","disabled");
		$(":input#searchInput").removeAttr("disabled");

		//Remove o atributo disabled="disabled" para elementos HTML espec�ficos desta tela
		$("input[type='hidden'],.conteinerBotoes,.conteinerBotoes .bottonRightCol,.ui-dialog button").removeAttr("disabled");
		$("input[name='buttonVoltar']").removeAttr("disabled");
		$("input[name='buttonCancelar']").removeAttr("disabled");
		
		//Esconde elementos espec�ficos da tela.
		$("img.ui-datepicker-trigger,span.campoObrigatorioSimbolo,th:has(input:checkbox),td:has(input:checkbox),input[type='radio'],label.rotuloRadio,select:has(option[value='-1']:selected) + label.rotuloInformativo,select:has(option[value='-1']:selected),span.campoObrigatorioSimbolo2,#botaoLimparContratoResponsabilidade,#botaoAlterarContratoResponsabilidade,#botaoRemoverContratoResponsabilidade,#botaoIncluirContratoResponsabilidade,.legendIndicadorPesquisa,.orientacaoInterna,#botaoPesquisarCliente,#linkPesquisarCEP,.imagemConferirCEP").hide();
		
		//Altera a apar�ncia dos elementos <label> cujos bot�es radio estejam selecionados.
		$("input[type='radio']:checked + label.rotuloRadio").each(function(){
			$(this).css({"display":"block","font-size":"14px","padding-top":"5px"});
		});
		
		//Esconde os r�tulos de unidades de medida e tempo e <selects> cujos campos <input> estejam vazios.
		$(":input[value=''] + label.rotuloInformativo,:input[value=''] + select.campoHorizontal").each(function(){
			$(this).hide();
		});
		
		
		//Adiciona a classe para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
		$("input[type='text'],select,textarea,#endFisEntFatCEP").addClass("campoDesabilitadoDetalhar");
		$("input.searchInput,select").removeClass("campoDesabilitadoDetalhar").addClass("searchInput");
		$("input.campo2Linhas[type='text'],select.campo2Linhas").removeClass("campoDesabilitadoDetalhar").addClass("campoDesabilitado2Linhas");
		$(".rotuloInformativo2Linhas").css("margin","6px 0 0");
		$(".campoValorReal.campoDesabilitadoDetalhar,.campoValorReal.campoDesabilitado2Linhas").addClass("campoValorRealDesabilitadoDetalhar");
		
		//Remove a classe campoObrigatorio de todos os r�tulos para evitar desalinhementos.
		$("*").removeClass("campoObrigatorio campoObrigatorio2");

		<c:if test="${contratoVO.idPontoConsumo == null || contratoVO.idPontoConsumo == 0 || empty contratoVO.idPontoConsumo}">
			limparFormulario();
		</c:if>	
		
		//Retira o texto "Selecione" da primeira op��o do <select>.
		$("option[value='-1']").text('');
		
		//ABA CONSUMO: corre��o de exibi��o dos elementos.
		$("#conteinerNumeroMaximoReducao label").addClass("rotulo2Linhas").css("width","155px");
		$("#conteinerNumeroMaximoReducao input").removeClass("campoDesabilitadoDetalhar").addClass("campo2Linhas campoDesabilitado2Linhas")
		
		/* Substitui os <select> que possuem valor por <span> */
		$("select:has(option[value!='-1']:selected)").each(function(){
			$("select").hide();
			var selectId = $(this).attr("id");
			var textOption = $("#" + selectId + " option:selected").text();
			$("#" + selectId + " + label.rotuloInformativo").css({"display":"block","margin-left":"0"});
            $("#" + selectId).after("<span class='itemDetalhamento'>" + textOption + "</span>");            
		});

		/* Substitui os <inputs> que possuem valor por <span class="itemDetalhamento"...> */
		$("input.campoDesabilitadoDetalhar[value!=''],input.campoDesabilitado2Linhas[value!='']").each(function(){
			$(this).hide();
			var valueInput = $(this).val();
			var inputId = $(this).attr("id");
			
			$("#" + inputId + " + label.rotuloInformativo").css({"display":"block","margin-left":"0"});
			if("endFisEntFatEmail"== inputId){
				if(valueInput.indexOf(";")!=-1){
					valueInput = valueInput.split(";").join(" ");
					$("#" + inputId).after("<span class='itemDetalhamento' style='width:600px'>" + valueInput + "</span>");
				}
			}else{
				$("#" + inputId).after("<span class='itemDetalhamento'>" + valueInput + "</span>");
			}
			$("input.campoValorRealDesabilitadoDetalhar + span").addClass("itemDetalhamento");			
		});
		
		//
		$.fn.separaValorMedida = function (){
			var faixaPressaoFornecimento = $(this).text();
			var faixaPressaoFornecimentoValor = (faixaPressaoFornecimento.split(" ")[0]);
			var faixaPressaoFornecimentoMedida = (faixaPressaoFornecimento.split(" ")[1]);
			$(this).text(faixaPressaoFornecimentoValor).after("<span class='rotuloInformativoDetalhar'  style='float: left; margin-top: 6px'>" + faixaPressaoFornecimentoMedida + "</span>");		
		};
		$("select#faixaPressaoFornecimento + span.itemDetalhamento").separaValorMedida();
		
		/* Esconde os <span class="itemDetalhamento"...> dos <inputs> sem valor */
		$("input[value='']").each(function(){
			var inputSemValorId = $(this).attr("id");
			$("#" + inputSemValorId + " + .rotuloInformativo2Linhas,#" + inputSemValorId + " + .rotuloInformativo,#" + inputSemValorId + " + select + span").hide();
		});
		
		// Corrige a classe dos <span class="itemDetalhamento"...> cujos r�tulos t�m de 2 linhas
		$(".campoDesabilitado2Linhas + span").removeClass("itemDetalhamento").addClass("itemDetalhamento2Linhas");
		
		//Corrige os r�tulos de unidades de medida e tempo com 2 linhas em <label>
		$(".itemDetalhamento2Linhas + label.rotuloInformativo").removeClass("rotuloInformativo").addClass("rotuloInformativo2LinhasDetalhar");

		// Corrige os <span> e <label> cujos r�tulos t�m de 3 linhas
		$("input#tempoValidadeRetiradaQPNR + span").removeClass("itemDetalhamento2Linhas").addClass("itemDetalhamento3Linhas")		
		$(".rotuloInformativo3Linhas").css("margin-top","18px");
		
		//Corrige os r�tulos de unidades de medida e tempo exibidos em <span>
		$("input.campoDesabilitadoDetalhar + span + select + span.itemDetalhamento").removeClass("itemDetalhamento").addClass("rotuloInformativoDetalhar").css({"float":"left","margin-top":"6px"});
		$("input.campoDesabilitado2Linhas + span + select + span.itemDetalhamento2Linhas").removeClass("itemDetalhamento2Linhas").addClass("rotuloInformativo2LinhasDetalhar");
		
		
		//ABA CONSUMO
		$("#usoPCS select#locaisAmostragemDisponiveis,#usoPCS select#intervalosAmostragemDisponiveis,#usoPCS .conteinerBotoesCampoList1a,#usoPCS .conteinerBotoesCampoList2").remove();
		$("select#idsIntervaloAmostragemAssociados,select#idsLocalAmostragemAssociados").addClass("campoVertical").css({'height':'70px'});
		$("select#idsIntervaloAmostragemAssociados").css("display","block");
		$("select#idsLocalAmostragemAssociados").css({'margin-right':'10px','display':'block'});

		/* Corrige a exibi��o das abas quando n�o existem Pontos de Consumo Selecionados */
		if ($("#idPontoConsumo").val() == ''){
			$(".itemDetalhamento,.rotuloInformativo,.rotuloRadio").text("");
			$("#rotuloItemFatura").css("margin-right","130px");
			$("#rotuloFaseVencimentoItemFatura").css("margin-right","100px");
			$("input.campoDesabilitado2Linhas + select + span.itemDetalhamento2Linhas").hide();
		}
				
		
		//ABA FATURAMENTO
		//Adi��o de r�tulo e detalhamento para o endere�o
		$("label#rotuloCepTrue + input + span.itemDetalhamento").css({"padding-top":"0","margin-top":"-1px"})
		$("#dadosFaturamentoCol2 .exibirCep").before("<label class='rotulo' id='rotuloNome'>Endere�o:</label><span class='itemDetalhamento itemDetalhamentoLargo'><c:out value='${contratoVO.enderecoFormatado}'/></span><br />");
		$("input[type='radio']:checked + label.rotuloRadio").each(function(){
			$(this).css("display","block");
		});
		$("#dadosFaturamentoCol2 .exibirCep + label,#dadosFaturamentoCol2 .exibirCep + label + input,#dadosFaturamentoCol2 .exibirCep + label + input + br").hide();
		$("#dadosItensFaturamento input[type='button']").hide();
		$("select#faseVencimentoItemFatura:has(option[value='-1']:selected)").prev().hide();
		$(".itemDetalhamento").css("margin-right","5px");
		$(".itemDetalhamento3Linhas").css("margin-right","0");
		$("select#listaDiasDisponiveis").css("display","block");
		$("select#listaDiasSelecionados").css("display","block");
		$("select#listaDiasSelecionados").css("display","block");
		
		
		
		//ABA MODALIDADE
		$("#rotuloQDCData,#QDCData,#rotuloQDC,#QDCValor,#botaoOKQDC,#botaoReplicarQDC,#botaoExcluirQDC,#conteinerQDCpontoConsumo br.quebraLinha,#conteinerQDCpontoConsumo br.quebra2Linhas").remove()
		$("#QDCContrato").css({"margin-left":"0","display":"block"}).removeAttr("disabled");
		$("#conteinerQDCpontoConsumo").css("width","auto")
		$("#conteinerQDCpontoConsumo hr.linhaSeparadora").css("width","251px");
		$("label[for='prazoRevizaoQDC']").addClass("rotulo2Linhas").css("width","150px");
		$("#prazoRevisaoQuantidadesContratadas").removeClass("campoDesabilitadoDetalhar").addClass("campoDesabilitado2Linhas");
		$("#prazoRevisaoQuantidadesContratadas + span").removeClass("itemDetalhamento").addClass("itemDetalhamento2Linhas");
		$("#prazoRevisaoQuantidadesContratadas + span + label").removeClass("rotuloInformativo").addClass("rotuloInformativo2LinhasDetalhar");
		$("input#dataRevis�oQuantidadesContratadas").addClass("campo2Linhas");
		$("#modalidades input[type='button'],#conteinerTakeOrPay .conteinerDados fieldset.colunaEsq").remove();
		$("#conteinerShipOrPay .conteinerDados2 fieldset.colunaEsq2").remove();
		
		$("#conteinerTakeOrPay").css("width","auto")
		$("#conteinerTakeOrPay fieldset.colunaEsq2").css("padding-left","0");
		$("#conteinerTakeOrPay label.rotuloRadio").css({'padding-right':'0','margin-right':'0'});
		$("#conteinerTakeOrPay .conteinerDados2").hide(); 
		$("#conteinerShipOrPay .conteinerDados2").hide();
		$("#rotuloMensalVariacaoInferior,#rotuloMargemVariacaoShipOrPay,#rotuloMargemVariacaoShipOrPay + input + span").css("margin-top","10px");
		$("#rotuloMargemVariacaoShipOrPay + input + span + label").css("margin-top","16px");		
		$("#mensalConsumoReferenciaInferior2 + span").addClass("itemDetalhamentoVertical").css("white-space","nowrap");
		
		//ABA RESPONSABILIDADE
		$(".pesquisarClienteFundo input[type='text'],.pesquisarClienteFundo textarea").css("margin-top","10px")
		$("table#contratoCliente th.abaContratoResponsabilidadeExcluir,table#contratoCliente td:has(a img)").hide();
		
		
		// habilitar e desabilitar os bot�es Aditar e Alterar utilizados no controle de acesso.
		if(isDisabledBotaoAditar){
			$("input[name='buttonAditar']").attr("disabled","disabled");
		}else{
			$("input[name='buttonAditar']").removeAttr("disabled");
		}
		
		if(isDisabledBotaoAlterar){
			$("input[name='buttonAlterar']").attr("disabled","disabled");
		}else{
			$("input[name='buttonAlterar']").removeAttr("disabled");
		}
		
	});
		

	function limparFormulario() {
		//TODO: Colocar o limpar de cada aba
		if(funcaoExiste('limparAbaPrincipal')) {
			document.getElementById('periodoTesteDataInicio').value = '';
			document.getElementById('prazoTeste').value = '';
			document.getElementById('periodoTesteDateFim').value = '';
			document.getElementById('volumeTeste').value = '';
			document.getElementById('faxDDD').value = '';
			document.getElementById('faxNumero').value = '';
			document.getElementById('email').value = '';
			document.getElementById('inicioGarantiaConversao').value = '';
			document.getElementById('numeroDiasGarantia').value = '';
			document.getElementById('fimGarantiaConversao').value = '';	
		}

		if(funcaoExiste('limparAbaDadosTecnicos')) {
			document.getElementById('vazaoInstantanea').value = '';
			document.getElementById('unidadeVazaoInstan').value = '-1';
			document.getElementById('vazaoInstantaneaMaxima').value = '';
			document.getElementById('unidadeVazaoInstanMaxima').value = '-1';
			document.getElementById('vazaoInstantaneaMinima').value = '';
			document.getElementById('unidadeVazaoInstanMinima').value = '-1';
			document.getElementById('faixaPressaoFornecimento').value = '-1';
			document.getElementById('pressaoMinimaFornecimento').value = '';
			document.getElementById('unidadePressaoMinimaFornec').value = '-1';
			document.getElementById('pressaoMaximaFornecimento').value = '';
			document.getElementById('unidadePressaoMaximaFornec').value = '-1';
			document.getElementById('pressaoLimiteFornecimento').value = '';
			document.getElementById('unidadePressaoLimiteFornec').value = '-1';		
			document.getElementById('numAnosCtrlParadaCliente').value = '';
			document.getElementById('maxTotalParadasCliente').value = '';
			document.getElementById('maxAnualParadasCliente').value = '';
			document.getElementById('numDiasProgrParadaCliente').value = '';
			document.getElementById('numAnosCtrlParadaCDL').value = '';
			document.getElementById('maxTotalParadasCDL').value = '';
			document.getElementById('maxAnualParadasCDL').value = '';
			document.getElementById('numDiasProgrParadaCDL').value = '';
			document.getElementById('numDiasConsecParadaCliente').value = '';		
			document.getElementById('numDiasConsecParadaCDL').value = '';		
		}	
		
		
		if(funcaoExiste('limparAbaConsumo')) {
			document.getElementById('locaisAmostragemDisponiveis').value = '';
			document.getElementById('idsLocalAmostragemAssociados').value = '';
			document.getElementById('intervalosAmostragemDisponiveis').value = '';
			document.getElementById('idsIntervaloAmostragemAssociados').value = '';
			document.getElementById('tamReducaoRecuperacaoPCS').value = '';	
			document.getElementById('horaInicialDia').value = '';	
			document.getElementById('regimeConsumo').value = '';
			document.getElementById('fornecimentoMaximoDiario').value = '';
			document.getElementById('unidadeFornecMaximoDiario').value = '';
			document.getElementById('fornecimentoMinimoDiario').value = '';
			document.getElementById('unidadeFornecMinimoDiario').value = '';
			document.getElementById('fornecimentoMinimoMensal').value = '';
			document.getElementById('unidadeFornecMinimoMensal').value = '';
			document.getElementById('fornecimentoMinimoAnual').value = '';	
			document.getElementById('unidadeFornecMinimoAnual').value = '';	
			document.getElementById('fatorUnicoCorrecao').value = '';	
			document.getElementById('consumoFatFalhaMedicao').value = '';
		}
		
		if(funcaoExiste('limparAbaFaturamento')) {
			document.getElementById('tarifaConsumo').value = '';
			document.getElementById('fatPeriodicidade').value = '';
			document.getElementById('depositoIdentificadoSim').value = '';
			document.getElementById('depositoIdentificadoNao').value = '';
			document.getElementById('endFisEntFatCEP').value = '';
			document.getElementById('endFisEntFatNumero').value = '';
			document.getElementById('endFisEntFatComplemento').value = '';
			document.getElementById('endFisEntFatEmail').value = '';
			document.getElementById('itemFatura').value = '';
			document.getElementById('diaVencimentoItemFatura').value = '';
			document.getElementById('opcaoVencimentoItemFatura').value = '';
			document.getElementById('faseVencimentoItemFatura').value = '';
			document.getElementById('indicadorVencDiaNaoUtilSim').value = '';
			document.getElementById('indicadorVencDiaNaoUtilNao').value = '';
			document.getElementById("idItemFaturamento").value = '';
			document.getElementById('dataReferenciaCambial').value = '';
			document.getElementById('diaCotacao').value = '';
			document.getElementById('opcaoVencimentoItemFatura').value = '';
			document.getElementById('faseVencimentoItemFatura').value = '';
			document.getElementById('contratoCompra').value = '';
		}
		
		if(funcaoExiste('limparCamposAbaModalidades')) {
			document.getElementById("tipoModalidade").value = '';
			document.getElementById("QDCContrato").value = '';
			document.getElementById("prazoRevisaoQuantidadesContratadas").value = '';
			document.getElementById("indicadorQDSMaiorQDCNao").value = '';
			document.getElementById("indicadorQDSMaiorQDCSim").value = '';
			document.getElementById("antecedenciaEnvioMensal").value = '';
			document.getElementById("numMesesSolicConsumo").value = '';
			document.getElementById("QDPconfirmadoAutomaticamenteMudan�aDiariaNao").value = '';
			document.getElementById("QDPconfirmadoAutomaticamenteMudan�aDiariaSim").value = '';
			document.getElementById("tipoCompromisso").value = '';
			document.getElementById("mensalConsumoReferenciaInferior").value = '';
			document.getElementById("mensalVariacaoInferior").value = '';
			document.getElementById("TOPrecuperacaoAutoTakeOrPayNao").value = '';
			document.getElementById("percentualMaximoRelacaoQDC").value = '';
			document.getElementById("percentualMaximoTerminoContrato").value = '';
			document.getElementById("dataInicioRecuperacao").value = '';
			document.getElementById("dataMaximaRecuperacao").value = '';
			document.getElementById("tempoValidadeRetiradaQPNR").value = '';
			document.getElementById("mensalConsumoReferenciaInferior2").value = '';
			document.getElementById("mensalVariacaoInferior2").value = '';
			document.getElementById("idModalidadeContrato").value = '';
			document.getElementById("labelAntecedenciaEnvioMensal").innerText = '';	
		}

		if(funcaoExiste('limparAbaResponsabilidade')) {
			limparFormularioDadosCliente();
			document.getElementById("descricaoTipoResponsabilidade").value = "";
			document.getElementById("dataInicioRelacao").value = "";
		}
	}
	
	var popup;
	
	function cancelar() {
		$("#chavePrimaria").val("");
		
		if($("#isTelaChamado").val() == 'true') {
			submeter('contratoForm','visualizarInformacoesPontoConsumo');
		} else {
			submeter('contratoForm', 'pesquisarContrato');
		}
		
	}
	function voltar() {		
		submeter('contratoForm', 'exibirDetalhamentoContrato?postBack=false');
	}
	
	function aditar(){
		document.forms[0].postBack.value = false;
		document.forms[0].fluxoAditamento.value = true;
		document.forms[0].fluxoAlteracao.value = false;
		submeter('contratoForm', 'exibirAditamentoContrato');
	}

	function alterar(){
		document.forms[0].postBack.value = false;
		document.forms[0].fluxoAlteracao.value = true;
		document.forms[0].fluxoAditamento.value = false;
		submeter('contratoForm', 'exibirAditamentoContrato');
	}   
	
	function exibirAlteracaoModalidadeCadastrada(id) {
		document.forms[0].idModalidadeContrato.value = id;
		submeter('contratoForm', 'exibirAlteracaoModalidadeCadastrada');
	}
	
	function popularCamposContratoPontoConsumo(id) {
		document.forms[0].idPontoConsumo.value = id;
		submeter('contratoForm', 'popularCamposContratoPontoConsumoDetalhamento');		
	}

	function exibirAlteracaoItemFaturamentoCadastrado(id) {
		document.forms[0].idItemFaturamento.value = id;
		submeter('contratoForm', 'exibirAlteracaoItemFaturamentoCadastrado');
	}

	function init () {		
		if(funcaoExiste('carregarFatorUnicoCorrecao')) {	
			carregarFatorUnicoCorrecao();			
			$("select#fatorUnicoCorrecao").hide();
			var textOption = $("#fatorUnicoCorrecao option:selected").text();
			if(textOption != "Selecione") {
            	$("#fatorUnicoCorrecao").after("<span class='itemDetalhamento2Linhas'>" + textOption + "</span>");
			}            

		}
		
		
	}
	
	function selecionarListas() {		
		var listaDiasDisponiveis = document.getElementById('listaDiasDisponiveis');		
		
		if (listaDiasDisponiveis != undefined) {
			for (i=0; i<listaDiasDisponiveis.length; i++){
				listaDiasDisponiveis.options[i].selected = true;
				
			}
		}
		var listaDiasSelecionados = document.getElementById('listaDiasSelecionados');		
		if (listaDiasSelecionados != undefined) {
			for (i=0; i<listaDiasSelecionados.length; i++){
				listaDiasSelecionados.options[i].selected = true;
			}
		}	
	}
	
	addLoadEvent(init);
</script>

<h1 class="tituloInterno">Detalhamento de Contrato</h1>

<form:form method="post" styleId="formContratoPontoConsumo" id="contratoForm" name="contratoForm">
	<input name="acao" type="hidden" id="acao" value="incluirContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoVO.chavePrimaria}"/>
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="indexLista" type="hidden" id="indexLista" value="${contratoVO.indexLista}">
	<input name="idModeloContrato" type="hidden" id="idModeloContrato" value="${contratoVO.idModeloContrato}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${param.idPontoConsumo}">
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${contratoVO.fluxoAlteracao}"/>
	<input name="fluxoAditamento" type="hidden" id="fluxoAditamento" value="${contratoVO.fluxoAditamento}"/>
	<input name="fluxoDetalhamento" type="hidden" id="fluxoDetalhamento" value="${contratoVO.fluxoDetalhamento}"/>
	<input type="hidden" id="isTelaChamado" name="isTelaChamado" value="${contratoVO.isTelaChamado}"/>
	
	
		
	<fieldset id="conteinerContratoPontoConsumo" class="detalhamento">	
		<c:forEach items="${listaImovelPontoConsumoSelecionadoVO}" var="imovelPontoConsumoVO" >
			<label class="rotulo">Im�vel:</label>
			<span class="itemDetalhamento">${imovelPontoConsumoVO.nome}</span>			
			
			<display:table class="dataTableGGAS"  list="${imovelPontoConsumoVO.listaPontoConsumo}" sort="list" id="pontoConsumo" decorator="br.com.ggas.web.contrato.contrato.decorator.PontoConsumoContratoDecorator" pagesize="15" excludedParams="" requestURI="#">
				<display:column property="selecionado" style="width: 25px" sortable="false" title="&nbsp;"/>
				<display:column sortable="false" title="Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 5px">
					<a href="javascript:popularCamposContratoPontoConsumo(${ pontoConsumo.chavePrimaria });">					
						${ pontoConsumo.descricao }
					</a>
				</display:column>
				<display:column style="width: 140px" sortable="false" title="Segmento">
					<a href="javascript:popularCamposContratoPontoConsumo(${ pontoConsumo.chavePrimaria });">					
						${ pontoConsumo.segmento.descricao }
					</a>					
				</display:column>
				<display:column style="width: 250px" sortable="false" title="Ramo de Atividade">
					<a href="javascript:popularCamposContratoPontoConsumo(${ pontoConsumo.chavePrimaria });">					
						${ pontoConsumo.ramoAtividade.descricao }
					</a>					
				</display:column>
		   	</display:table>
		   	<hr class="linhaSeparadora2" />
		</c:forEach>	  	
	</fieldset>

	<fieldset id="tabs" style="display: none">
		<ul>
			<c:if test="${contratoVO.selAbaPrincipais eq true}">
			<li><a href="#principal">Principal</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaTecnicos eq true}">
			<li><a href="#tecnicos">Dados T�cnicos</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaConsumo eq true}">
			<li><a href="#consumo">Consumo</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaFaturamento eq true}">
			<li><a href="#faturamento">Faturamento</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaRegrasFaturamento eq true}">
			<li><a href="#modalidades">Modalidades Consumo Faturamento</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaResponsabilidade eq true}">
			<li><a href="#responsabilidades">Responsabilidades</a></li>
			</c:if>
		</ul>
		
		<c:if test="${contratoVO.selAbaPrincipais eq true}">
			<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoPrincipal.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaTecnicos eq true}">
			<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoDadosTecnicos.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaConsumo eq true}">
			<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoConsumo.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaFaturamento eq true}">
			<c:set var="fluxoDetalhamento" value="true"/>
			<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoFaturamento.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaRegrasFaturamento eq true}">
			<jsp:include page="/jsp/contrato/contrato/contrato/include/abas/abaContratoModalidades.jsp">
				<jsp:param name="fluxoDetalhamento" value="true" />
			</jsp:include>
		</c:if>
		<c:if test="${contratoVO.selAbaResponsabilidade eq true}">
			<jsp:include page="/jsp/contrato/contrato/contrato/include/abas/abaContratoResponsabilidade.jsp">
				<jsp:param name="fluxoDetalhamento" value="true" />
				<jsp:param name="adicionarContratoCliente" value="adicionarContratoCliente" />			
				<jsp:param name="removerContratoCliente" value="removerContratoCliente" />
			</jsp:include>
		</c:if>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonVoltar" class="bottonRightCol2" value="<< Voltar" type="button" onclick="voltar();">
		<input name="buttonCancelar" class="bottonRightCol2 bottonLeftColUltimo" value="Cancelar" type="button" onclick="cancelar();">
		
		<c:if test = "${contratoVO.isTelaChamado eq null or contratoVO.isTelaChamado eq 'false'}">
			<fieldset class="conteinerBotoesDirFixo">
				<c:if test="${contratoVO.habilitado == true}">	    	
					<vacess:vacess param="alterarContrato">
						<input name="buttonAlterar" id="botaoAplicarIncluirContratoPontoConsumo" class="bottonRightCol2" value="Alterar" type="button" onclick="alterar();">
					</vacess:vacess>
						
					<vacess:vacess param="aditarContrato">
						<input name="buttonAditar" id="botaoAplicarIncluirContratoPontoConsumo" class="bottonRightCol2 bottonRightColUltimo" value="Aditar" type="button" onclick="aditar();">
					</vacess:vacess>
				</c:if>
			</fieldset>
		</c:if>
	</fieldset>
</form:form>