<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<script>

	$(document).ready(function(){
		
		var isDisabledBotaoAditar = $("input[name='buttonAditar']").prop("disabled");
		var isDisabledBotaoAlterar = $("input[name='buttonAlterar']").prop("disabled");
						
		//Desabilita todos os campos input (todos), select e textarea.
		$(":input").attr("disabled","disabled");	
		$(":input#searchInput").removeAttr("disabled");
		
		//Remove o atributo disabled="disabled" para elementos HTML espec�ficos desta tela.
		$("input[type='hidden'],.conteinerBotoes .bottonRightCol,#chavesPrimariasPontoConsumoVO,#idModeloContrato,#QDCContrato,.ui-dialog button").removeAttr("disabled");
		$("input[name='buttonCancelar']").removeAttr("disabled");
		$("input[name='buttonAvancar']").removeAttr("disabled");
		
		//Esconde elementos espec�ficos da tela.
		$("img.ui-datepicker-trigger,a.apagarItemPesquisa,span.campoObrigatorioSimbolo,input[type='radio'],label.rotuloRadio,#rotuloQDCData,#QDCData,#rotuloQDC,#QDCValor,label.rotuloInformativo,#botaoOKQDC,#botaoReplicarQDC,#botaoExcluirQDC").css("display","none");
		
		//Esconde os bot�es radio e exibe o label correspodente.
		$("input[type='radio']:checked + label.rotuloRadio").each(function(){
			$(this).css({"display":"block","font-size":"14px","padding-top":"5px"});
		});
		
		//Esconde os r�tulos de unidades de medida e tempo e <selects> cujos campos <input> estejam vazios.
		$(":input[value=''] + label.rotuloInformativo,:input[value=''] + select.campoHorizontal").each(function(){
			$(this).css("display","none");
		});

		$(".itemDetalhamento").css("margin-right","5px");
		$(".itemDetalhamento3Linhas").css("margin-right","0");
		
		//Adiciona a classe "campoDesabilitadoDetalhar" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
		$("input[type='text'],select").addClass("campoDesabilitadoDetalhar");
		$("input.searchInput,select").removeClass("campoDesabilitadoDetalhar").addClass("searchInput");
		$("input.campo2Linhas[type='text'],select.campo2Linhas").removeClass("campoDesabilitadoDetalhar").addClass("campoDesabilitado2Linhas");
		$(".campoValorReal.campoDesabilitadoDetalhar,.campoValorReal.campoDesabilitado2Linhas").addClass("campoValorRealDesabilitadoDetalhar");
		
		//Remove a classe campoObrigatorio de todos os r�tulos para evitar desalinhementos.
		$("*").removeClass("campoObrigatorio");
		
		//Retira o texto "Selecione" da primeira op��o do <select>.
		$("option[value='-1']").text('');
				
		/* Substitui os <select> que possuem valor por <span> */
		$("select:has(option[value!='-1']:selected)").each(function(){
			$("select").css("display","none");
			var selectId = $(this).attr("id");
			var textOption = $("#" + selectId + " option:selected").text();
            $("#" + selectId).after("<span class='itemDetalhamento'>" + textOption + "</span>");
		});
		
		/* Substitui os <inputs> que possuem valor por <span class="itemDetalhamento"...> */
		$("input.campoDesabilitadoDetalhar[value!=''],input.campoDesabilitado2Linhas[value!='']").each(function(){
			$(this).css("display","none")
			var valueInput = $(this).val();
			var inputId = $(this).attr("id");
			$("#" + inputId + " + label.rotuloInformativo").css({"display":"block","margin-left":"0"});
			$("#" + inputId).after("<span class='itemDetalhamento'>" + valueInput + "</span>");
			$("input.campoValorRealDesabilitadoDetalhar + span").addClass("itemDetalhamento");			
		});
		
		/* Esconde os <inputs> sem valor e seus respectivos <span class="itemDetalhamento"...> */
		var emptyTextBoxes = $('input:text').filter(function() { return this.value == ""; });
	    emptyTextBoxes.each(function() {
	    	$(this).css("display","none");
			var inputSemValorId = $(this).attr("id");
			$("#" + inputSemValorId + " + .rotuloInformativo2Linhas,#" + inputSemValorId + " + .rotuloInformativo").css("display","none");

	    });
			
		//$("input[value='']").each(function(){
			//alert("executou o script!");
		//	$(this).css("display","none");
		//	var inputSemValorId = $(this).attr("id");
		//	$("#" + inputSemValorId + " + .rotuloInformativo2Linhas,#" + inputSemValorId + " + .rotuloInformativo").css("display","none");
		//});
		
		// Corrige a classe dos <span class="itemDetalhamento"...> cujos r�tulos tem de 2 linhas
		$(".campoDesabilitado2Linhas  + span").removeClass("itemDetalhamento").addClass("itemDetalhamento2Linhas");
		
		// Corrige os <span> e <label> cujos r�tulos tem de 3 linhas
		$("input#tempoAntecRevisaoGarantias + span").removeClass("itemDetalhamento2Linhas").addClass("itemDetalhamento3Linhas")
		$("input#periodicidadeReavGarantias + span").removeClass("itemDetalhamento2Linhas").addClass("itemDetalhamento3Linhas")		
		$(".rotuloInformativo3Linhas").css("margin-top","18px");
		
		/* Corrige o QDC do Contrato */
		$("#QDCContrato").css({"display":"block","margin":"0"}).removeClass("campoDesabilitadoDetalhar");
		$("#conteinerQDCContrato label,#conteinerQDCContrato input,#conteinerQDCContrato br").remove()
		
		// habilitar e desabilitar os bot�es Aditar e Alterar utilizados no controle de acesso.
		if(isDisabledBotaoAditar){
			$("input[name='buttonAditar']").attr("disabled","disabled");
		}else{
			$("input[name='buttonAditar']").removeAttr("disabled");
		}
		
		if(isDisabledBotaoAlterar){
			$("input[name='buttonAlterar']").attr("disabled","disabled");
		}else{
			$("input[name='buttonAlterar']").removeAttr("disabled");
		}

			
	});
	
	animatedcollapse.addDiv('ConteinerQuantDiasRenovacao', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerTempoAntecedenciaRenovacao', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerTempoAntecedenciaRevisaoGarantias', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerPeriodicidadeReavalicacaoGarantias', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ContainerTipoAgrupamento', 'fade=0,speed=400,hide=1');
	
	function avancar() {
	
		var selectQDC = document.getElementById("QDCContrato");
		selectAllOptions(selectQDC);
		
		$("#fluxoDetalhamento").val(true);
		$("#fluxoAditamento").val(false);
		$("#fluxoAlteracao").val(false);
	
		submeter('contratoForm', 'exibirDetalhamentoContratoPontoConsumo');
	}
	
	function cancelar() {
		
		if($("#isTelaChamado").val() == 'true') {
			submeter('contratoForm','visualizarInformacoesPontoConsumo');
		} else {
			location.href = '<c:url value="/pesquisarContrato"/>';
		}
	}
		
	function aditar(){
		document.forms[0].postBack.value = false;
		document.forms[0].fluxoAlteracao.value = false;
		document.forms[0].fluxoAditamento.value = true;
		submeter('contratoForm', 'exibirAditamentoContrato');
	}

	function alterar(){
		document.forms[0].postBack.value = false;
		document.forms[0].fluxoAlteracao.value = true;
		document.forms[0].fluxoAditamento.value = false;
		submeter('contratoForm', 'exibirAditamentoContrato');
	}  

	function init () {
				
		configurarDadosMedidor();
		
		var renovacaoAutomatica = "<c:out value='${contratoVO.renovacaoAutomatica}'/>";
		if(renovacaoAutomatica == "true"){
			animatedcollapse.show('ConteinerQuantDiasRenovacao');
		} else {
			animatedcollapse.show('ConteinerTempoAntecedenciaRenovacao');
		}

		var faturamentoAgrupamento = "<c:out value='${contratoVO.faturamentoAgrupamento}'/>";

		if(faturamentoAgrupamento == "true"){
			animatedcollapse.show('ContainerTipoAgrupamento');
		}
		
		var garantiaFinanceiraRenovada = "<c:out value='${contratoVO.garantiaFinanceiraRenovada}'/>";
		if(garantiaFinanceiraRenovada == "Renov�vel"){
			animatedcollapse.show('ConteinerTempoAntecedenciaRevisaoGarantias');	
			animatedcollapse.hide('ConteinerPeriodicidadeReavalicacaoGarantias');					
		} else if(garantiaFinanceiraRenovada == "Renovada"){
			animatedcollapse.show('ConteinerPeriodicidadeReavalicacaoGarantias');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');	
		}else{
			animatedcollapse.hide('ConteinerPeriodicidadeReavalicacaoGarantias');
			animatedcollapse.hide('ConteinerTempoAntecedenciaRevisaoGarantias');
			}
	}

	function configurarDadosMedidor(){
		
		$("#dadosMedidor").dialog({
			autoOpen: false,
			width: 250,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
	}
	
	function detalharMedidorPontoConsumo(id){
		AjaxService.obterMedidorPontoConsumo(id, {
			callback: function(dadosMedidor) {
									
				if(dadosMedidor != null){

					$("#modeloMedidorPopup").html(dadosMedidor["modeloMedidor"]);
					$("#numeroSeriePopup").html(dadosMedidor["numeroSerie"]);
					$("#dataInstalacaoPopup").html(dadosMedidor["dataInstalacao"]);
					$("#dataAtivacaoPopup").html(dadosMedidor["dataAtivacao"]);
										
					exibirJDialog("#dadosMedidor");
																					
				}else{
					
					alert('<fmt:message key="CONTRATO_PONTO_CONSUMO_NAO_POSSUI_MEDIDOR"/>');
					
				}
				
			}
			
			, async:false}
			
		);
				
	}
	var windows = {};
	function imprimirArquivo(chaveAnexo) {
		
		if(windows['popup'] != undefined && windows['popup'] != null) {
			windows['popup'].close();	
		}	
		var url = 'imprimirArquivoChamadoContrato';
		windows['popup'] = window.open(url+"&chaveAnexo="+chaveAnexo ,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	addLoadEvent(init);
	
</script>

<h1 class="tituloInterno">Detalhamento de Contrato<a class="linkHelp" href="<help:help>/detalhamentodoscontratos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" styleId="formDetalharContrato" id="contratoForm" name="contratoForm">
	<input name="acao" type="hidden" id="acao" value="aditarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoVO.chavePrimaria}"/>
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="indexLista" type="hidden" id="indexLista" value="${contratoVO.indexLista}">
	<input name="idModeloContrato" type="hidden" id="idModeloContrato" value="${contratoVO.idModeloContrato}">
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${contratoVO.fluxoAlteracao}"/>
	<input name="fluxoAditamento" type="hidden" id="fluxoAditamento" value="${contratoVO.fluxoAditamento}"/>
	<input name="fluxoDetalhamento" type="hidden" id="fluxoDetalhamento" value="${contratoVO.fluxoDetalhamento}"/>
	<input name="idCliente" type="hidden" id="idCliente" value="${contratoVO.idCliente}"/>
	<input type="hidden" id="isTelaChamado" name="isTelaChamado" value="${contratoVO.isTelaChamado}"/>
	<input type="hidden" id="idPontoConsumo" name="idPontoConsumo" value="${contratoVO.idPontoConsumo}"/>
	
	


<!-- <div id="dadosMedidor" title="Dados do medidor"> -->
<!-- 	<label class="rotulo">Modelo:</label> -->
<!-- 	<span class="itemDetalhamento" id="modeloMedidorPopup"></span><br /> -->
	
<!-- </div> -->

	<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Avan�ar</span> para continuar o detalhamento do Contrato.</p>
	
	<fieldset class="detalhamento">
		<fieldset class="conteinerBloco">
			<label class="rotulo" id="rotuloCliente">Modelo:</label>
			<span id="detalhamentoDescricao" class="itemDetalhamento"><c:out value="${modeloContrato.descricao}"/></span>
		</fieldset>
		<br/>
		<fieldset id="conteinerDetalharContrato" class="conteinerBloco">
			<fieldset id="contratoCol1" class="colunaEsq">
				<fieldset id="conteinerClienteDetalharContrato">
					<legend>Dados do Cliente</legend>
					<fieldset class="conteinerDados">
						<div>											
							<label class="rotulo" id="rotuloCliente">Cliente:</label>
							<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${contratoVO.nomeCliente}"/></span><br />
							<label class="rotulo" id="rotuloCnpjTexto">CPF/CNPJ:</label>
							<span class="itemDetalhamento"><c:out value="${contratoVO.documentoCliente}"/></span><br />
							<label class="rotulo" id="rotuloEnderecoTexto">Endere�o:</label>
							<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${contratoVO.enderecoCliente}"/></span><br />
							<label class="rotulo" id="rotuloEnderecoTexto">E-mail:</label>
							<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${contratoVO.emailCliente}"/></span><br />
						</div>
					</fieldset>
				</fieldset>
						
				<fieldset id="conteinerProposta">
					<legend>Proposta</legend>
					<fieldset class="conteinerDados">					
						<ggas:labelContrato forCampo="numeroProposta">N�mero da Proposta:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal" type="text" id="numeroProposta" name="numeroProposta" maxlength="30" size="20" <ggas:campoSelecionado campo="numeroProposta"/> value="${contratoVO.numeroProposta}" onchange="listarVersoesProposta(this);">
						<select class="campoSelect campoHorizontal" name="idProposta" id="idProposta" <ggas:campoSelecionado campo="numeroProposta"/> onclick="preencherProposta();" <c:if test="${empty contratoVO.idProposta}">disabled="disabled"</c:if>>
							<c:forEach items="${listaVersoesProposta}" var="proposta">
								<option value="<c:out value="${proposta.chavePrimaria}"/>" <c:if test="${contratoVO.idProposta == proposta.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${proposta.versaoProposta}"/>
								</option>		
							</c:forEach>
						</select>
						<br class="quebraLinha" />
						
						<ggas:labelContrato forCampo="gastoEstimadoGNMes">Gasto Mensal com GN:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal campoValorReal" type="text" id="gastoEstimadoGNMes" name="gastoEstimadoGNMes" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="gastoEstimadoGNMes"/> value="<fmt:formatNumber value="${contratoVO.gastoEstimadoGNMes}" type="currency"/>">
						<label class="rotuloHorizontal rotuloInformativo" for="gastoEstimadoGNMes">R$/m�s</label><br class="quebraLinha">
	
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="economiaEstimadaGNMes">Economia Mensal<br />com GN:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas campoValorReal" type="text" id="economiaEstimadaGNMes" name="economiaEstimadaGNMes" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="economiaEstimadaGNMes"/> value="<fmt:formatNumber value="${contratoVO.economiaEstimadaGNMes}" type="currency"/>"><br class="quebraLinha">
	
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="economiaEstimadaGNAno">Economia Anual<br />com GN:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas campoValorReal" type="text" id="economiaEstimadaGNAno" name="economiaEstimadaGNAno" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="economiaEstimadaGNAno"/> value="<fmt:formatNumber value="${contratoVO.economiaEstimadaGNAno}" type="currency"/>"><br class="quebraLinha">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="descontoEfetivoEconomia">Desconto Efetivo na Economia:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="descontoEfetivoEconomia" name="descontoEfetivoEconomia" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="3" <ggas:campoSelecionado campo="descontoEfetivoEconomia"/> value="${contratoVO.descontoEfetivoEconomia}">
						<label class="rotuloInformativo2Linhas rotuloHorizontal" for="descontoEfetivoEconomia">%</label><br class="quebraLinha">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="diaVencFinanciamento">Dia para Vencimento Financiamento:</ggas:labelContrato>
						<select id="diaVencFinanciamento" class="campoSelect campo2Linhas campoHorizontal" <ggas:campoSelecionado campo="diaVencFinanciamento"/> name="diaVencFinanciamento">
							<option value="-1">Selecione</option>
							<c:forEach begin="1" end="31" var="dia">
								<option value="${dia}" <c:if test="${contratoVO.diaVencFinanciamento eq dia}">selected="selected"</c:if>>${dia}</option>
						  	</c:forEach>
						</select>
						<label class="rotuloInformativo2Linhas rotuloHorizontal" for="diaVencFinanciamento">(01-31)</label><br class="quebra2Linhas">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="economiaEstimadaGNMes">Valor da Participa��o do Cliente:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas campoValorReal" type="text" id="valorParticipacaoCliente" name="valorParticipacaoCliente" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="valorParticipacaoCliente"/> value="<fmt:formatNumber value="${contratoVO.valorParticipacaoCliente}" type="currency"/>">
						<br class="quebraLinha">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="qtdParcelasFinanciamento">Quantidade de Parcelas do Financiamento:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas" type="text" id="qtdParcelasFinanciamento" name="qtdParcelasFinanciamento" size="2" <ggas:campoSelecionado campo="qtdParcelasFinanciamento"/> value="${contratoVO.qtdParcelasFinanciamento}">
						<br class="quebraLinha">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualJurosFinanciamento">Percentual de Juros do Financiamento:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualJurosFinanciamento" name="percentualJurosFinanciamento" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="3" <ggas:campoSelecionado campo="percentualJurosFinanciamento"/> value="${contratoVO.percentualJurosFinanciamento}">
						<label class="rotuloInformativo2Linhas rotuloHorizontal" for="percentualJurosFinanciamento">%</label><br class="quebraLinha2">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="sistemaAmortizacao">Sistema de<br />Amortiza��o:</ggas:labelContrato>
						<select id="sistemaAmortizacao" class="campoSelect campo2Linhas" name="sistemaAmortizacao" <c:if test="${empty contratoVO.valorParticipacaoCliente}">disabled="disabled"</c:if>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaAmortizacoes}" var="amortizacao">
								<option value="${amortizacao.chavePrimaria}" <c:if test="${contratoVO.sistemaAmortizacao eq amortizacao.chavePrimaria}">selected="selected"</c:if>>${amortizacao.descricao}</option>
						  	</c:forEach>
						</select>
						
						<ggas:labelContrato styleClass="rotulo" forCampo="valorInvestimento">Valor do Investimento:</ggas:labelContrato>					
						<input class="campoTexto campoHorizontal campoValorReal" type="text" id="valorInvestimento" name="valorInvestimento" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,11,2);" maxlength="10" size="13" <ggas:campoSelecionado campo="valorInvestimento"/> value="<fmt:formatNumber value='${contratoVO.valorInvestimento}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha"/>
						
						<ggas:labelContrato styleClass="rotulo" forCampo="dataInvestimento">Data do Investimento:</ggas:labelContrato>
						<input class="campoTexto campoHorizontal" type="text" id="dataInvestimento" name="dataInvestimento" maxlength="10" <ggas:campoSelecionado campo="dataInvestimento"/>  value="${contratoVO.dataInvestimento}"><br class="quebraLinha"/>
																	
					</fieldset>
				</fieldset>
				
				<fieldset id="conteinerGarantia">
					<legend>Garantia</legend>
					<fieldset class="conteinerDados">
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="tipoGarantiaFinanceira">Tipo da Garantia Financeira:</ggas:labelContrato>
						<select name="tipoGarantiaFinanceira" id="tipoGarantiaFinanceira" class="campoSelect campo2Linhas" <ggas:campoSelecionado campo="tipoGarantiaFinanceira"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoGarantiaFinanceira}" var="tipoGarantiaFinanceira">
								<option value="<c:out value="${tipoGarantiaFinanceira.chavePrimaria}"/>" <c:if test="${contratoVO.tipoGarantiaFinanceira == tipoGarantiaFinanceira.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoGarantiaFinanceira.descricao}"/>
								</option>		
							</c:forEach>
						</select><br class="quebraLinha">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="descGarantiaFinanc">Descri��o da Garantia Financeira:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas" type="text" id="descGarantiaFinanc" name="descGarantiaFinanc" maxlength="40" size="40" <ggas:campoSelecionado campo="descGarantiaFinanc"/> value="${contratoVO.descGarantiaFinanc}"><br class="quebraLinha" />
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="valorGarantiaFinanceira">Valor da Garantia Financeira:</ggas:labelContrato>					
						<input class="campoTexto campoValorReal campo2Linhas" type="text" id="valorGarantiaFinanceira" name="valorGarantiaFinanceira" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="valorGarantiaFinanceira"/> value="<fmt:formatNumber value='${contratoVO.valorGarantiaFinanceira}' maxFractionDigits='2' minFractionDigits='2'/>"><br class="quebraLinha" />
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="garantiaFinanceiraRenovada">A garantia �?</ggas:labelContrato>
						<select name="garantiaFinanceiraRenovada" id="garantiaFinanceiraRenovada" class="campoSelect" >
						<option value="-1">Selecione</option>
						<c:forEach items="${listaGarantiaFinanceiraRenovada}" var="garantiaFinanceiraRenovada">
							<option value="<c:out value="${garantiaFinanceiraRenovada}"/>" <c:if test="${contratoVO.garantiaFinanceiraRenovada == garantiaFinanceiraRenovada}">selected="selected"</c:if>>
								<c:out value="${garantiaFinanceiraRenovada}"/>
							</option>		
						</c:forEach>
						</select><br class="quebraLinha">
						
						<fieldset id="ConteinerTempoAntecedenciaRevisaoGarantias">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="dataInicioGarantiaFinanceira">Data de In�cio da Garantia Financeira:</ggas:labelContrato>
							<input class="campoData campo2Linhas" type="text" id="dataInicioGarantiaFinanceira" name="dataInicioGarantiaFinanceira" maxlength="10" <ggas:campoSelecionado campo="dataInicioGarantiaFinanceira"/> value="${contratoVO.dataInicioGarantiaFinanceira}"><br class="quebraLinha" />
							
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="dataFinalGarantiaFinanceira">Data Final da Garantia Financeira:</ggas:labelContrato>
							<input class="campoData campo2Linhas" type="text" id="dataFinalGarantiaFinanceira" name="dataFinalGarantiaFinanceira" maxlength="10" <ggas:campoSelecionado campo="dataFinalGarantiaFinanceira"/> value="${contratoVO.dataFinalGarantiaFinanceira}"><br class="quebraDataRadio" />
							
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="tempoAntecRevisaoGarantias">Tempo de Anteced�ncia para Revis�o das Garantias:</ggas:labelContrato>
							<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="tempoAntecRevisaoGarantias" name="tempoAntecRevisaoGarantias" maxlength="3" size="2" <ggas:campoSelecionado campo="tempoAntecRevisaoGarantias"/> value="${contratoVO.tempoAntecRevisaoGarantias}" onkeypress="return formatarCampoInteiro(event);">
							<label class="rotuloInformativo rotuloInformativo3Linhas rotuloHorizontal" for="tempoAntecRevisaoGarantias">dias</label>
						</fieldset>
						
						<fieldset id="ConteinerPeriodicidadeReavalicacaoGarantias">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="periodicidadeReavGarantias">Periodicidade para reavalia��o das Garantias:</ggas:labelContrato>
							<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="periodicidadeReavGarantias" name="periodicidadeReavGarantias" maxlength="3" size="2" <ggas:campoSelecionado campo="periodicidadeReavGarantias"/> value="${contratoVO.periodicidadeReavGarantias}" onkeypress="return formatarCampoInteiro(event);">
							<label class="rotuloInformativo rotuloInformativo3Linhas rotuloHorizontal" for="periodicidadeReavGarantias">dias</label>
						</fieldset>																		
					</fieldset>
				</fieldset>			
				
				<fieldset id="conteinerGarantia">
					<legend>Multa Rescis�ria</legend>
					<fieldset class="conteinerDados">
						<ggas:labelContrato styleClass="campoHorizontal" forCampo="multaRecisoria">Multa Rescis�ria:</ggas:labelContrato>
						<select name="multaRecisoria" id="multaRecisoria" class="campoSelect campoHorizontal" <ggas:campoSelecionado campo="multaRecisoria"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaTipoMultaRecisoria}" var="multaRecisoria">
								<option value="<c:out value="${multaRecisoria.chavePrimaria}"/>" <c:if test="${contratoVO.multaRecisoria == multaRecisoria.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${multaRecisoria.descricao}"/>
								</option>		
							</c:forEach>
						</select><br class="quebraLinha" />		
						
						<c:if test="${contratoVO.situacaoContrato == 2 || contratoVO.situacaoContrato == 7}">
							<ggas:labelContrato styleClass="campoHorizontal" forCampo="dataRecisao">Data da Rescis�o:</ggas:labelContrato>
							<input class="campoData" type="text" id="dataRecisao" name="dataRecisao" maxlength="10" <ggas:campoSelecionado campo="dataRecisao"/>  value="${contratoVO.dataRecisao}"><br class="quebraLinha"/>
						</c:if>										
					</fieldset>
				</fieldset>	
				
				<fieldset id="conteinerGarantia">
					<legend>Take or Pay</legend>
					<fieldset class="conteinerDados">

						<c:set var="i" value="0" />
						<display:table id="compromissosTakeOrPayVO" class="dataTableGGAS dataTableAba" name="sessionScope.listaCompromissosTakeOrPayCVO" style="width: 428px" sort="list" requestURI="#">
							<display:column title="Compromissos">
								${compromissosTakeOrPayVO.periodicidadeTakeOrPay.descricao}
							</display:column>
							<display:column title="In�cio Vig�ncia">
								<fmt:formatDate value="${compromissosTakeOrPayVO.dataInicioVigencia}" pattern="dd/MM/yyyy"/>
							</display:column>
							<display:column title="Fim Vig�ncia">
								<fmt:formatDate value="${compromissosTakeOrPayVO.dataFimVigencia}" pattern="dd/MM/yyyy"/>
							</display:column>
							<display:column title="Compromisso de retirada ToP">
								<fmt:formatNumber type="percent" minFractionDigits="2" value="${compromissosTakeOrPayVO.margemVariacaoTakeOrPay}"></fmt:formatNumber>
							</display:column>
							<c:set var="i" value="${i+1}" />
						</display:table>
					</fieldset>
				</fieldset>	
						
			</fieldset>

			<fieldset id="contratoCol2" class="colunaFinal">
				<fieldset id="conteinerContrato">
					<legend>Contrato</legend>
					<fieldset class="conteinerDados">
						<ggas:labelContrato forCampo="numeroContrato">N�mero do Contrato:</ggas:labelContrato>
						<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="10" size="10" <ggas:campoSelecionado campo="numeroContrato"/> value="${contratoVO.numeroFormatado}" onkeypress="return formatarCampoInteiro(event);" ><br class="quebraLinha" />
						
						<ggas:labelContrato forCampo="numeroAnteriorContrato">N�mero Anterior do Contrato:</ggas:labelContrato>
						<input class="campoTexto" type="text" id="numeroAnteriorContrato" name="numeroAnteriorContrato" maxlength="19" size="10" <ggas:campoSelecionado campo="numeroAnteriorContrato"/> value="${contratoVO.numeroAnteriorContrato}"><br class="quebraLinha" />
						
						<ggas:labelContrato forCampo="descricaoContrato">Descri��o:</ggas:labelContrato>
						<input class="campoTexto" type="text" id="descricaoContrato" name="descricaoContrato" maxlength="200" size="40" <ggas:campoSelecionado campo="descricaoContrato"/> value="${contratoVO.descricaoContrato}"><br class="quebraLinha" />
					
						<ggas:labelContrato forCampo="numeroAditivo">N�mero do Aditivo:</ggas:labelContrato>
						<input class="campoTexto" type="text" size="10" maxlength="10" name="numeroAditivo" id="numeroAditivo" <ggas:campoSelecionado campo="numeroAditivo"/> value="<c:if test="${empty contratoVO.numeroAditivo == false && contratoVO.numeroAditivo > 0}">${contratoVO.numeroAditivo}</c:if>"><br class="quebraLinha" />
	
						<ggas:labelContrato forCampo="dataAditivo">Data do Aditivo:</ggas:labelContrato>
						<input class="campoData" type="text" id="dataAditivo" name="dataAditivo" maxlength="10" <ggas:campoSelecionado campo="dataAditivo"/> value="${dataAditivo}"><br class="quebraLinha" />
	
						<ggas:labelContrato forCampo="descricaoAditivo">Descri��o do Aditivo:</ggas:labelContrato>
						<input class="campoTexto" type="text" size="30" maxlength="30"  id="descricaoAditivo" name="descricaoAditivo" <ggas:campoSelecionado campo="descricaoAditivo"/> value="${contratoVO.descricaoAditivo}"><br class="quebraLinha" />
						
						<ggas:labelContrato forCampo="situacaoContrato">Situa��o do Contrato:</ggas:labelContrato>
						<select name="situacaoContrato" id="situacaoContrato" class="campoSelect campoHorizontal" <ggas:campoSelecionado campo="situacaoContrato"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaSituacaoContrato}" var="situacaoContrato">
								<option value="<c:out value="${situacaoContrato.chavePrimaria}"/>" <c:if test="${contratoVO.situacaoContrato == situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${situacaoContrato.descricao}"/>
								</option>		
							</c:forEach>
						</select><br class="quebraLinha" />
						
						<ggas:labelContrato forCampo="dataAssinatura">Data da assinatura:</ggas:labelContrato>
						<input class="campoData" type="text" id="dataAssinatura" name="dataAssinatura" maxlength="10" <ggas:campoSelecionado campo="dataAssinatura"/> value="${contratoVO.dataAssinatura}"><br class="quebraLinha" />
						
						<ggas:labelContrato forCampo="numeroEmpenho">N�mero do Empenho:</ggas:labelContrato>
						<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="numeroEmpenho" name="numeroEmpenho" size="20" <ggas:campoSelecionado campo="numeroEmpenho"/> value="${contratoVO.numeroEmpenho}"><br class="quebraLinha" />
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="dataVencObrigacoesContratuais">Data de Vencimento das Obriga��es Contratuais:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataVencObrigacoesContratuais" name="dataVencObrigacoesContratuais" maxlength="10" <ggas:campoSelecionado campo="dataVencObrigacoesContratuais"/> value="${contratoVO.dataVencObrigacoesContratuais}"><br class="quebraDataRadio" />
						
						<ggas:labelContrato forCampo="indicadorAnoContratual">Defini��o de Ano Contratual:</ggas:labelContrato>
						<input class="campoRadio" type="radio" name="indicadorAnoContratual" id="anoContratualMaiusculo" <ggas:campoSelecionado campo="indicadorAnoContratual"/> value="true" <c:if test="${contratoVO.indicadorAnoContratual == true}">checked="checked"</c:if>>
						<label class="rotuloRadio" id="labelAnoContratualMaiusculo" for="anoContratualMaiusculo">ANO(Fiscal)</label>
						<input class="campoRadio" type="radio" name="indicadorAnoContratual" id="anoContratualMinusculo" <ggas:campoSelecionado campo="indicadorAnoContratual"/> value="false" <c:if test="${contratoVO.indicadorAnoContratual == false}">checked="checked"</c:if>>
						<label class="rotuloRadio" id="labelAnoContratualMinusculo" for="anoContratualMinusculo">ano(Calend�rio)</label><br class="quebraRadio2Linhas" />
						
						
						<ggas:labelContrato forCampo="renovacaoAutomatica">Renova��o Autom�tica:</ggas:labelContrato>
						<input class="campoRadio" type="radio" name="renovacaoAutomatica" id="indicadorRenovacaoAutomaticaSim" <ggas:campoSelecionado campo="renovacaoAutomatica"/> value="true" <c:if test="${contratoVO.renovacaoAutomatica == true}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="indicadorRenovacaoAutomaticaSim">Sim</label>
						<input class="campoRadio" type="radio" name="renovacaoAutomatica" id="indicadorRenovacaoAutomaticaNao" <ggas:campoSelecionado campo="renovacaoAutomatica"/> value="false" <c:if test="${contratoVO.renovacaoAutomatica == false}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="indicadorRenovacaoAutomaticaNao">N�o</label><br class="quebraRadio2Linhas" />
						
						<fieldset id="ConteinerQuantDiasRenovacao">
							<ggas:labelContrato forCampo="numDiasRenoAutoContrato">Per�odo de Renova��o:</ggas:labelContrato>
							<input class="campoTexto campoHorizontal" type="text" id="numDiasRenoAutoContrato" name="numDiasRenoAutoContrato" maxlength="3" size="2" <ggas:campoSelecionado campo="numDiasRenoAutoContrato"/> value="${contratoVO.numDiasRenoAutoContrato}">
							<label class="rotuloInformativo" for="numDiasRenoAutoContrato">dias</label>
						</fieldset>
						<fieldset id="ConteinerTempoAntecedenciaRenovacao">
							<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="tempoAntecedenciaRenovacao">Tempo de Anteced�ncia para Negocia��o da Renova��o:</ggas:labelContrato>
							<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="tempoAntecedenciaRenovacao" name="tempoAntecedenciaRenovacao" maxlength="3" size="2" <ggas:campoSelecionado campo="tempoAntecedenciaRenovacao"/> value="${contratoVO.tempoAntecedenciaRenovacao}" onkeypress="return formatarCampoInteiro(event);">
							<label class="rotuloInformativo2Linhas rotuloHorizontal" for="tempoAntecedenciaRenovacao">dias</label>
						</fieldset>
						
						<fieldset>
							<ggas:labelContrato forCampo="valorContrato">Valor do contrato:</ggas:labelContrato>
							<input class="campoTexto campoValorReal" type="text" id="valorContrato" name="valorContrato" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="valorContrato"/> value="<fmt:formatNumber value="${contratoVO.valorContrato}" type="currency"/>"><br class="quebraLinha" />
							
							<ggas:labelContrato forCampo="faturamentoAgrupamento">Faturamento Agrupado?</ggas:labelContrato>
							<input class="campoRadio" type="radio" name="faturamentoAgrupamento" id="faturamentoAgrupamentoSim" <ggas:campoSelecionado campo="faturamentoAgrupamento"/> value="true" <c:if test="${contratoVO.faturamentoAgrupamento == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="faturamentoAgrupamentoSim">Sim</label>
							<input class="campoRadio" type="radio" name="faturamentoAgrupamento" id="faturamentoAgrupamentoNao" <ggas:campoSelecionado campo="faturamentoAgrupamento"/> value="false" <c:if test="${contratoVO.faturamentoAgrupamento == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="faturamentoAgrupamentoNao">N�o</label>
							<br class="quebraRadio" />
							
							<fieldset id="ContainerTipoAgrupamento">
								<ggas:labelContrato forCampo="tipoAgrupamento">Tipo de Agrupamento:</ggas:labelContrato>
								<select name="tipoAgrupamento" id="tipoAgrupamento" class="campoSelect campoHorizontal" <ggas:campoSelecionado campo="tipoAgrupamento"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaTipoAgrupamento}" var="tipoAgrupamento">
									<option value="<c:out value="${tipoAgrupamento.chavePrimaria}"/>" <c:if test="${contratoVO.tipoAgrupamento == tipoAgrupamento.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${tipoAgrupamento.descricao}"/>
									</option>		
								</c:forEach>
								</select><br class="quebraLinha" />
							</fieldset>
							
							
							<ggas:labelContrato forCampo="emissaoFaturaAgrupada">Emiss�o de Fatura Agrupada?</ggas:labelContrato>
							<input class="campoRadio" type="radio" name="emissaoFaturaAgrupada" id="emissaoFaturaAgrupadaSim" <ggas:campoSelecionado campo="emissaoFaturaAgrupada"/> value="true" <c:if test="${contratoVO.emissaoFaturaAgrupada == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="emissaoFaturaAgrupadaSim">Sim</label>
							<input class="campoRadio" type="radio" name="emissaoFaturaAgrupada" id="emissaoFaturaAgrupadaNao" <ggas:campoSelecionado campo="emissaoFaturaAgrupada"/> value="false" <c:if test="${contratoVO.emissaoFaturaAgrupada == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="emissaoFaturaAgrupadaNao">N�o</label>
							<br class="quebraRadio" />
							
							<ggas:labelContrato forCampo="formaCobranca">Tipo de Conv�nio:</ggas:labelContrato>
							<select class="campoSelect" id="formaCobranca" name="formaCobranca" <ggas:campoSelecionado campo="formaCobranca"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaFormaCobranca}" var="formaCobranca">
									<option value="<c:out value="${formaCobranca.chavePrimaria}"/>" 
									<c:if test="${contratoVO.formaCobranca eq formaCobranca.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${formaCobranca.descricao}"/>
									</option>		
								</c:forEach>						
							</select>
							
							<ggas:labelContrato forCampo="arrecadadorConvenio">Arrecadador Conv�nio:</ggas:labelContrato>
							<%-- <label class="rotulo" id="arrecadadorConvenio" for="arrecadadorConvenio">Arrecadador Conv�nio:</label> --%>
							<select class="campoSelect" id="arrecadadorConvenio" name="arrecadadorConvenio" <ggas:campoSelecionado campo="arrecadadorConvenio"/>>
								<option value="-1">Selecione</option>
								<c:forEach items="${listaArrecadadorConvenio}" var="arrecadadorConvenio">
									<option value="<c:out value="${arrecadadorConvenio.chavePrimaria}"/>" 
									<c:if test="${contratoVO.arrecadadorConvenio eq arrecadadorConvenio.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${arrecadadorConvenio.codigoConvenio} - ${fn:toUpperCase(arrecadadorConvenio.arrecadadorCarteiraCobranca.tipoCarteira.descricao) == 'SEM REGISTRO' ? 'CN' : 'CR'} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
									</option>		
								</c:forEach>						
							</select>
							
							<ggas:labelContrato forCampo="debitoAutomatico">D�bito Autom�tico?</ggas:labelContrato>
							<input class="campoRadio" type="radio" name="debitoAutomatico" id="debitoAutomaticoSim" <ggas:campoSelecionado campo="debitoAutomatico"/> value="true" <c:if test="${contratoVO.debitoAutomatico == true}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="debitoAutomaticoSim">Sim</label>
							<input class="campoRadio" type="radio" name="debitoAutomatico" id="debitoAutomaticoNao" <ggas:campoSelecionado campo="debitoAutomatico"/> value="false" <c:if test="${contratoVO.debitoAutomatico == false}">checked="checked"</c:if>>
							<label class="rotuloRadio" for="debitoAutomaticoNao">N�o</label>
							<br class="quebraRadio" />

							<c:if test="${contratoVO.debitoAutomatico == true}">
								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="arrecadadorConvenioDebitoAutomatico">Arrecadador Conv�nio para D�bito Autom�tico:</ggas:labelContrato>
								<select class="campoSelect" id="arrecadadorConvenioDebitoAutomatico" name="arrecadadorConvenioDebitoAutomatico" <ggas:campoSelecionado campo="arrecadadorConvenioDebitoAutomatico"/>>
									<option value="-1">Selecione</option>
									<c:forEach items="${listaArrecadadorConvenioDebitoAutomatico}" var="arrecadadorConvenioDebitoAutomatico">
										<option tipo="<c:out value="${arrecadadorConvenioDebitoAutomatico.tipoConvenio.chavePrimaria}"/>" value="<c:out value="${arrecadadorConvenioDebitoAutomatico.chavePrimaria}"/>"
											<c:if test="${contratoVO.arrecadadorConvenioDebitoAutomatico == arrecadadorConvenioDebitoAutomatico.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${arrecadadorConvenioDebitoAutomatico.codigoConvenio} - ${arrecadadorConvenioDebitoAutomatico.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
										</option>
									</c:forEach>
								</select>

								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="banco">Banco:</ggas:labelContrato>
								<select class="campoSelect" id="banco" name="banco" <ggas:campoSelecionado campo="banco"/>>
									<option value="-1">Selecione</option>
									<c:forEach items="${listaBanco}" var="banco">
										<option tipo="<c:out value="${banco.chavePrimaria}"/>" value="<c:out value="${banco.chavePrimaria}"/>"
												<c:if test="${contratoVO.banco == banco.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${banco.codigoBanco} - ${banco.nome}" />
										</option>
									</c:forEach>
								</select>

								<ggas:labelContrato forCampo="agencia">Ag�ncia:</ggas:labelContrato>
								<input class="campoTexto" type="text" id="agencia" name="agencia" maxlength="10" size="10" <ggas:campoSelecionado campo="agencia"/> value="${contratoVO.agencia}">
								<br class="quebraLinha" />

								<ggas:labelContrato forCampo="contaCorrente">Conta Corrente:</ggas:labelContrato>
								<input class="campoTexto" type="text" id="contaCorrente" name="contaCorrente" maxlength="10" size="10" <ggas:campoSelecionado campo="contaCorrente"/> value="${contratoVO.contaCorrente}">
								<br class="quebraLinha" />
							</c:if>
							
							<ggas:labelContrato forCampo="volumeReferencia">Volume de Refer�ncia:</ggas:labelContrato>
							<input class="campoTexto" type="text" id="volumeReferencia" name="volumeReferencia" maxlength="10" size="10" <ggas:campoSelecionado campo="volumeReferencia"/> value="${contratoVO.volumeReferencia}"><br class="quebraLinha" />
					
						</fieldset>
						<fieldset>
								<label class="rotulo" for="tipoPeriodicidadePenalidade">Tipo Periodicidade Penalidade:</label>
								<input class="campoRadio campoRadio3Linhas" type="radio" value="true" 
									name="tipoPeriodicidadePenalidade"
									<c:if test="${contratoVO.tipoPeriodicidadePenalidade == 'true'}">checked="checked"</c:if> />
								<label class="rotuloRadio"
									for="tipoPeriodicidade">Civil</label> <input
									class="campoRadio campoRadio3Linhas" type="radio" value="false"
									name="tipoPeriodicidadePenalidade"
									<c:if test="${contratoVO.tipoPeriodicidadePenalidade == 'false'}">checked="checked"</c:if> />
								<label class="rotuloRadio"
									for="tipoPeriodicidade">Cont�nuo</label>

							</fieldset>
							<ggas:labelContrato forCampo="numeroAditivo">Prazo Vig�ncia:</ggas:labelContrato>
							<input class="campoTexto" type="text" size="10" maxlength="10" name="prazoVigencia" id="prazoVigencia" <ggas:campoSelecionado campo="prazoVigencia"/> value="<c:if test="${empty contratoVO.prazoVigencia == false && contratoVO.prazoVigencia > 0}">${contratoVO.prazoVigencia}</c:if>"><br class="quebraLinha" />
							<ggas:labelContrato forCampo="valorContrato">Incentivos Comerciais:</ggas:labelContrato>
							<input class="campoTexto campoValorReal" type="text" id="incentivosComerciais" name="incentivosComerciais" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="incentivosComerciais"/> value="<fmt:formatNumber value="${contratoVO.incentivosComerciais}" type="currency" currencySymbol="R$"/>"><br class="quebraLinha" />
							<ggas:labelContrato forCampo="valorContrato">Incentivos Infraestrutura:</ggas:labelContrato>
							<input class="campoTexto campoValorReal" type="text" id="incentivosInfraestrutura" name="incentivosInfraestrutura" onkeyup="formataValorMonetario(this,13)" maxlength="10" size="13" <ggas:campoSelecionado campo="incentivosInfraestrutura"/> value="<fmt:formatNumber value="${contratoVO.incentivosInfraestrutura}" type="currency" currencySymbol="R$"/>"><br class="quebraLinha" />
														
					</fieldset>
				</fieldset>
				
				<fieldset id="conteinerMulta">
					<legend>Pagamento Efetuado em Atraso</legend>
					<fieldset class="conteinerDados">
						<ggas:labelContrato forCampo="indicadorMultaAtraso">O contrato � pass�vel de Multa?</ggas:labelContrato>
						<input class="campoRadio" type="radio" name="indicadorMultaAtraso" id="indicadorMultaSim" <ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="true" <c:if test="${contratoVO.indicadorMultaAtraso == true}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="indicadorMultaSim">Sim</label>
						<input class="campoRadio" type="radio" name="indicadorMultaAtraso" id="indicadorMultaNao" <ggas:campoSelecionado campo="indicadorMultaAtraso"/> value="false" <c:if test="${contratoVO.indicadorMultaAtraso == false}">checked="checked"</c:if>>
						<label class="rotuloRadio" for="indicadorMultaNao">N�o</label><br class="quebraLinha" />
						<label class="rotulo rotulo2Linhas campoObrigatorio" id="labelpercentualMulta"><span id="spanpercentualMulta" class="campoObrigatorioSimbolo2"> </span>Percentual de Multa: </label>
					    <input class="campoTexto campoHorizontal" type="text" id="percentualMulta" name="percentualMulta" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5" value="${contratoVO.percentualMulta}">
					    <label class="rotuloInformativo rotuloHorizontal" for="descontoEfetivoEconomia">%</label><br class="quebraLinha">
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="indicadorJurosMora">O contrato � pass�vel de Juros de Mora?</ggas:labelContrato>
						<input class="campoRadio campoRadio2Linhas" type="radio" name="indicadorJurosMora" id="indicadorJurosMoraSim" <ggas:campoSelecionado campo="indicadorJurosMora"/> value="true" <c:if test="${contratoVO.indicadorJurosMora == true}">checked="checked"</c:if>>
						<label class="rotuloRadio rotuloRadio2Linhas" for="indicadorJurosMoraSim">Sim</label>
						<input class="campoRadio campoRadio2Linhas" type="radio" name="indicadorJurosMora" id="indicadorJurosMoraNao" <ggas:campoSelecionado campo="indicadorJurosMora"/> value="false" <c:if test="${contratoVO.indicadorJurosMora == false}">checked="checked"</c:if>>
						<label class="rotuloRadio rotuloRadio2Linhas" for="indicadorJurosMoraNao">N�o</label><br class="quebraRadio2Linhas" />
						<label class="rotulo rotulo2Linhas campoObrigatorio" id="labelpercentualJurosMora"><span id="spanpercentualJurosMora" class="campoObrigatorioSimbolo2"> </span>Percentual de Juros de Mora:</label>
					    <input class="campoTexto campoHorizontal" type="text" id="percentualJurosMora" name="percentualJurosMora" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5" value="${contratoVO.percentualJurosMora}">
					    <label class="rotuloInformativo rotuloHorizontal" for="descontoEfetivoEconomia">%</label><br class="quebraLinha">
											
						<ggas:labelContrato forCampo="indiceCorrecaoMonetaria">Corre��o Monet�ria:</ggas:labelContrato>
						<select name="indiceCorrecaoMonetaria" id="indiceCorrecaoMonetaria" class="campoSelect campoHorizontal" <ggas:campoSelecionado campo="indiceCorrecaoMonetaria"/>>
							<option value="-1">Selecione</option>
							<c:forEach items="${listaIndiceCorrecaoMonetaria}" var="indiceCorrecaoMonetaria">
								<option value="<c:out value="${indiceCorrecaoMonetaria.chavePrimaria}"/>" <c:if test="${contratoVO.indiceCorrecaoMonetaria == indiceCorrecaoMonetaria.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${indiceCorrecaoMonetaria.descricaoAbreviada}"/>
								</option>		
							</c:forEach>
						</select>
					</fieldset>
				</fieldset>
				
				<fieldset id="conteinerPenalidades">
					<legend>Penalidades</legend>
					<fieldset class="conteinerDados">
						<fieldset id="conteinerSobreDemanda">
							<legend>Penalidade por retirada a Maior/Menor:</legend>
							<fieldset class="conteinerDados2">
								<display:table id="penalidadesRetMaiorMenorVO" class="dataTableGGAS dataTableAba" name="listaPenalidadeRetMaiorMenor" style="width: 428px" sort="list" requestURI="#">
									<display:column style="width: 25px" title="<input type='checkbox' id='checkAllCompromissos'/>">
										<input type="checkbox" class="checkItemCompromissos" name="indicePenalidadeRetiradaMaiorMenor" value="<c:out value="${i}"/>">
									</display:column>
									<display:column title="Penalidade">
										${penalidadesRetMaiorMenorVO.penalidade.descricao}
									</display:column>
									<display:column title="Consumo Refer�ncia">
										${penalidadesRetMaiorMenorVO.consumoReferencia.descricao}
									</display:column>
									<display:column title="In�cio Vig�ncia">
										<fmt:formatDate value="${penalidadesRetMaiorMenorVO.dataInicioVigencia}" />
									</display:column>
									<display:column title="Fim Vig�ncia">
										<fmt:formatDate value="${penalidadesRetMaiorMenorVO.dataFimVigencia}" />
									</display:column>
									<c:set var="i" value="${i+1}" />
								</display:table>
								
								
								
																
							</fieldset>
						</fieldset>
				
						
						<fieldset id="conteinerDeliveryOrPay">
							<legend>Penalidade por Delivery Or Pay:</legend>
							<fieldset class="conteinerDados2">
								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="consumoReferenciaSobDem">Percentual Sobre a Tarifa do G�s:</ggas:labelContrato>															
								<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualTarifaDoP" <ggas:campoSelecionado campo="percentualTarifaDoP"/> name="percentualTarifaDoP" maxlength="6" size="4" value="${contratoVO.percentualTarifaDoP}">
								<c:if test="${contratoVO.percentualTarifaDoP ne null && contratoVO.percentualTarifaDoP ne ''}">
									<label class="rotuloHorizontal rotuloInformativo" for="percentualTarifaDoP">&nbsp;%</label>
								</c:if>
							</fieldset>
						</fieldset>

						<fieldset>
							<legend>Penalidade por G�s Fora de Especifica��o:</legend>
							<fieldset class="conteinerDados2">
								<ggas:labelContrato styleClass="rotulo2Linhas" forCampo="percentualSobreTariGas">Percentual Sobre a Tarifa do G�s:</ggas:labelContrato>															
								<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="percentualSobreTariGas" <ggas:campoSelecionado campo="percentualSobreTariGas"/> name="percentualSobreTariGas" maxlength="6" size="4" value="${contratoVO.percentualSobreTariGas}">
								<c:if test="${contratoVO.percentualSobreTariGas ne null && contratoVO.percentualSobreTariGas ne ''}">
									<label class="rotuloInformativo2Linhas rotuloHorizontal" for="percentualSobreTariGas">&nbsp;%</label>
								</c:if>
							</fieldset>
						</fieldset>
					</fieldset>
				</fieldset>			
				
				<c:set var="dataQDC" value="QDCContrato" />
				<c:set var="volumeQDC" value="QDCContrato" />
				<c:set var="modalidade" value="false" />
				<c:set var="idConteinerQDC" value="conteinerQDCContrato" />
				<c:set var="nomeComponente" value="QDC do Contrato" />
				<%@ include file="/jsp/contrato/contrato/contrato/include/dadosQDCContrato.jsp" %>
				
				<fieldset id="estimativaQtdeRecuperar">
					<legend>Estimativa de Quantidade a Recuperar</legend>
					<fieldset class="conteinerDados">
						<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMinimoRelacaoQDCC" forCampo="percMinDuranteRetiradaQPNRC">Percentual m�nimo em rela��o � QDC para recupera��o:</ggas:labelContrato>				
						<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMinimoRelacaoQDCC" name="percMinDuranteRetiradaQPNRC" maxlength="6" size="3" <ggas:campoSelecionado campo="percMinDuranteRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percMinDuranteRetiradaQPNRC}">
						<label class="rotuloInformativo rotuloHorizontal">%</label><br />
					
					 	<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoRelacaoQDCC" forCampo="percDuranteRetiradaQPNRC">Percentual m�ximo em rela��o � QDC para recupera��o:</ggas:labelContrato>				
						<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoRelacaoQDCC" name="percDuranteRetiradaQPNRC" maxlength="6" size="3" <ggas:campoSelecionado campo="percDuranteRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percDuranteRetiradaQPNRC}">
						<label class="rotuloInformativo rotuloHorizontal">%</label><br />
						
						<ggas:labelContrato styleClass="rotulo2Linhas" forId="percentualMaximoTerminoContratoC" forCampo="percDuranteRetiradaQPNRC">Percentual m�ximo da recupera��o com o t�rmino do contrato:</ggas:labelContrato>									
						<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="percentualMaximoTerminoContratoC" name="percFimRetiradaQPNRC" maxlength="6" size="3"  <ggas:campoSelecionado campo="percFimRetiradaQPNRC"/> onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);"  value="${contratoVO.percFimRetiradaQPNRC}">
						<label class="rotuloInformativo rotuloHorizontal">%</label><br /><br />
						
						<hr class="linhaSeparadora">
						
						<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataInicioRecuperacaoC" forCampo="dataInicioRetiradaQPNRC">Data de inicio da recupera��o:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataInicioRecuperacaoC" name="dataInicioRetiradaQPNRC" maxlength="10" <ggas:campoSelecionado campo="dataInicioRetiradaQPNRC"/> value="${contratoVO.dataInicioRetiradaQPNRC}" onblur="mostrarAsterisco(this.value,['dataMaximaRecuperacaoC','percentualMaximoTerminoContratoC','percentualMaximoRelacaoQDCC']);"><br />
				
						<ggas:labelContrato styleClass="rotulo2Linhas rotuloConsumoQPNR" forId="dataMaximaRecuperacaoC" forCampo="dataFimRetiradaQPNRC">Data m�xima para recupera��o:</ggas:labelContrato>
						<input class="campoData campo2Linhas" type="text" id="dataMaximaRecuperacaoC" name="dataFimRetiradaQPNRC" maxlength="10" <ggas:campoSelecionado campo="dataFimRetiradaQPNRC"/> value="${contratoVO.dataFimRetiradaQPNRC}"><br />
						
						<hr class="linhaSeparadora">
						
						<ggas:labelContrato styleId="rotuloTempoValidadeRetiradaQPNR" styleClass="rotulo2Linhas" forCampo="tempoValidadeRetiradaQPNRC">Tempo de validade para recupera��o da QPNR do ano anterior:</ggas:labelContrato>
						<input class="campoTexto campo3Linhas campoHorizontal" type="text" id="tempoValidadeRetiradaQPNRC" name="tempoValidadeRetiradaQPNRC" maxlength="2" size="1" <ggas:campoSelecionado campo="tempoValidadeRetiradaQPNRC"/> value="${contratoVO.tempoValidadeRetiradaQPNRC}" onkeypress="return formatarCampoInteiro(event);">
						<label class="rotuloInformativo3Linhas rotuloHorizontal" for="tempoValidadeRetiradaQPNRC">anos</label>
					</fieldset>
				</fieldset>

				<fieldset id="documentosAnexos" style="margin-top: 10px">
					<legend>Documentos Anexos</legend>
					<fieldset class="conteinerDados">
						<jsp:include page="/jsp/contrato/contrato/contrato/include/grids/gridContratoAnexo.jsp"></jsp:include>
					</fieldset>
				</fieldset>
				
			</fieldset>	
		</fieldset>
		
		<fieldset id="pesquisaImovelContrato" class="conteinerBloco">
			<legend id="legendPesquisarImoveis">Pontos de Consumo</legend>
			<c:forEach items="${listaImovelPontoConsumoVO}" var="imovelPontoConsumoVO" >
				<hr class="linhaSeparadora2" />
				<label class="rotulo">Im�vel:</label>
				<span class="itemDetalhamento">${imovelPontoConsumoVO.nome}</span>
				<a class="apagarItemPesquisa" onclick="return confirm('Deseja retirar o im�vel?');" href="javascript:retirarImovel('${imovelPontoConsumoVO.chavePrimaria}');" title="Retirar Im�vel"><img src="<c:url value="/imagens/deletar_x.png"/>" /></a>
				
				<display:table class="dataTableGGAS dataTablePesquisa" name="${imovelPontoConsumoVO.listaPontoConsumo}" sort="list" id="pontoConsumo" pagesize="15" excludedParams="" requestURI="#">
					<display:column sortable="false" title="Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
						<input name="chavesPrimariasPontoConsumoVO" type="hidden" id="chavesPrimariasPontoConsumoVO" value="${pontoConsumo.chavePrimaria}"/>
						<a href="javascript:detalharMedidorPontoConsumo(${pontoConsumo.chavePrimaria});" title="Clique aqui para detalhar os dados do medidor instalado">
							<c:out value='${pontoConsumo.descricao}'/>
						</a>
					</display:column>
					<display:column sortable="false" title="Segmento" style="width: 140px">
						<a href="javascript:detalharMedidorPontoConsumo(${pontoConsumo.chavePrimaria});" title="Clique aqui para detalhar os dados do medidor instalado">
							<c:out value='${pontoConsumo.segmento.descricao}'/>						
						</a>
					</display:column>
					<display:column sortable="false" title="Ramo de Atividade" style="width: 250px">
						<a href="javascript:detalharMedidorPontoConsumo(${pontoConsumo.chavePrimaria});" title="Clique aqui para detalhar os dados do medidor instalado">
							<c:out value='${pontoConsumo.ramoAtividade.descricao}'/>						
						</a>
					</display:column>
				</display:table>
				
			</c:forEach>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
		
		<fieldset class="conteinerBotoesDirFixo">
		
			<c:if test="${contratoVO.habilitado == true and (contratoVO.isTelaChamado eq null or contratoVO.isTelaChamado eq 'false')}">
					<vacess:vacess param="aditarContrato">
						<input name="buttonAditar" class="bottonRightCol2 botaoAditar" value="Aditar" type="button" onclick="aditar();">
					</vacess:vacess>
					
					<vacess:vacess param="alterarContrato">
						<input name="buttonAlterar" class="bottonRightCol2 botaoAditar" value="Alterar" type="button" onclick="alterar();">
					</vacess:vacess>
			</c:if>
			 
			<c:if test="${listaImovelPontoConsumoVO ne null}">
				<input name="buttonAvancar" class="bottonRightCol2 botaoAvancar bottonRightColUltimo" value="Avan�ar >>" type="button" onclick="avancar();">
			</c:if>		 	
		 </fieldset>
	</fieldset>
</form:form> 
