<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/contrato/cadastrocontrato/contrato.js"></script>

<script>

	$(document).ready(function(){
		verificaRadioNfeAbaFaturamento();
	});
	
	$(function(){
		var camposData = "#fimGarantiaConversao,#QDCData,#prazoRevisaoQuantidadesContratadas,#dataMaximaRecuperacao,#dataInicioRelacao,#dataFimRelacao";
		var separador = ",";

		$("#dataInicioRecuperacao").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy',
			onSelect: function() {
				mostrarAsterisco($("#dataInicioRecuperacao").val(),['dataMaximaRecuperacao','percentualMaximoTerminoContrato','percentualMaximoRelacaoQDC']);
			}
		});
		
		<c:if test="${contratoVO.selAbaPrincipais eq true}">
			<c:if test="${contratoVO.selPeriodoTesteDataInicio}">
			$("#periodoTesteDataInicio").datepicker({
				changeYear: true, 
				showOn: 'button', 
				buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
				buttonImageOnly: true, 
				buttonText: 'Exibir Calend�rio', 
				dateFormat: 'dd/mm/yy',
				onSelect: function() {
					mostrarAsterisco($("#periodoTesteDataInicio").val(),['periodoTesteDateFim']);
				}
			});
			</c:if>
			<c:if test="${contratoVO.selPeriodoTesteDateFim}">
				camposData = camposData + separador + "#periodoTesteDateFim";
				separador = ',';			
			</c:if>
			<c:if test="${contratoVO.selInicioGarantiaConversao}">
				camposData = camposData + separador + "#inicioGarantiaConversao";			
			</c:if>
		</c:if>
		
		// Datepicker
		$(camposData).datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
		//Identifica todos os campo de data e esconde a imagem do calend�rio
		$(".campoData:disabled + img").each(function(){
			$(this).css("display","none");
		});
		
		//Identifica todos os campos radio desabilitados e clareia a cor do <label>
		$("input:disabled:radio + label.rotuloRadio").each(function(){
			$(this).addClass("rotuloDesabilitado");
		});
		
		//Adiciona a classe para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
		$("input[type='text']:disabled,select:disabled").addClass("campoDesabilitado");
		
	});
		
	
	var popup;

	function exibirPopupPesquisaProposta() {
		popup = window.open('exibirPesquisaPropostaPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function exibirPopupCopiarPontoConsumo(idPontoConsumo) {
		popup = window.open('exibirCopiaDadosPontoConsumoPopup?pIdPontoConsumo='+ idPontoConsumo +'&postBack=true','popup','height=750,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function voltar() {
		document.getElementById("postBack").value = "false";		
		submeter('contratoForm', 'reexibirAditamentoContrato');
	}
	
	function aplicar(){

		var idPontoConsumoSelecionado = document.getElementById('idPontoConsumo').value;

		if((idPontoConsumoSelecionado != undefined) && (idPontoConsumoSelecionado != '')){

			selecionarItensComponenteSelect();
			if(funcaoExiste('selecionarOpcoesQDCContrato')) {
				selecionarOpcoesQDCContrato();
			}

			submeter('contratoForm', 'aplicarDadosContratoPontoConsumoAditamento');
			
		}else{

			alert('Selecione um ponto de consumo.');

		}

	} 
	
	function selecionarItensComponenteSelect() {
		var listaLocal = document.getElementById('idsLocalAmostragemAssociados');		
		if (listaLocal != undefined) {
			for (i=0; i<listaLocal.length; i++){
				listaLocal.options[i].selected = true;
			}
		}	

		var listaIntervalo = document.getElementById('idsIntervaloAmostragemAssociados');		
		if (listaIntervalo != undefined) {
			for (i=0; i<listaIntervalo.length; i++){
				listaIntervalo.options[i].selected = true;
			}
	    }	
	} 
	
	function limparFormulario() {
		if(funcaoExiste('limparAbaPrincipal')) {
			limparAbaPrincipal();
		}
		limparAbaDadosTecnicos()
		if(funcaoExiste('limparAbaConsumo')) {
			limparAbaConsumo();
		}
		if(funcaoExiste('limparAbaFaturamento')) {
			limparAbaFaturamento();
		}
		if(funcaoExiste('limparAbaResponsabilidade')) {
			limparAbaResponsabilidade();
		}
		if(funcaoExiste('limparCamposAbaModalidades')) {
			limparCamposAbaModalidades();
		}
	}
	
	function popularCamposContratoPontoConsumo(id) {
		document.forms[0].idPontoConsumo.value = id;
		submeter('contratoForm', 'popularCamposContratoPontoConsumoAditamento?acao=popularCamposContratoPontoConsumo');		
	}
	
	function aditar(){
		submeter('contratoForm', 'aditarContrato');
	}

	function alterar(){
		submeter('contratoForm', 'alterarContrato');
	}
	
	//############# Inicio javascript Aba modalidades
	
	function adicionarCompromissoTOPAbaModalidade() {
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		submeter('contratoForm', 'adicionarCompromissoTOPAbaModalidade');
	}
	
	
	function excluirCompromissoTOPAbaModalidade() {
		var selecao = verificarSelecaoCompromissoTOPAbaModalidade();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				selecionarItensComponenteSelect();
				selecionarOpcoesQDCContrato();
				submeter('contratoForm', 'excluirCompromissoTOPAbaModalidade');
			}
	    }		
	}

	function adicionarCompromissoSOPAbaModalidade() {
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		submeter('contratoForm', 'adicionarCompromissoSOPAbaModalidade');
	}
	
	
	function excluirCompromissoSOPAbaModalidade() {
		var selecao = verificarSelecaoCompromissoSOPAbaModalidade();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				selecionarItensComponenteSelect();
				selecionarOpcoesQDCContrato();
				submeter('contratoForm', 'excluirCompromissoSOPAbaModalidade');
			}
	    }		
	}

	
	
	function adicionarModalidadeContrato(adicao) {
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		if (adicao) {
			document.forms[0].idModalidadeContrato.value = '';
		}
		submeter('contratoForm', 'adicionarModalidadeContratoAditamento?acao=adicionarModalidadeContrato');
	}
	
	function exibirAlteracaoModalidadeCadastrada(id) {
		selecionarItensComponenteSelect();
		document.forms[0].idModalidadeContrato.value = id;
		submeter('contratoForm', 'exibirAlteracaoModalidadeCadastradaAditamento?acao=exibirAlteracaoModalidadeCadastrada#modalidades');
	}
	
		
	function excluirModalidadeContrato() {
		var selecao = verificarSelecaoModalidadeContrato();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					selecionarItensComponenteSelect();
					document.forms[0].idModalidadeContrato.value = '';
					selecionarOpcoesQDCContrato();
					submeter('contratoForm', 'excluirModalidadeContratoAditamento?acao=excluirModalidadeContrato');
				}
		    }				
	}
	
	//############# Fim javascript Aba modalidades
	
	//############# Inicio javascript Aba faturamento
	function adicionarNovoItemFaturamento(){
		selecionarListas();
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		submeter('contratoForm', 'adicionarItemFaturamentoAditamento?acao=adicionarItemFaturamento');
	}
	
	function excluirItensFaturamento(){
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		submeter('contratoForm', 'excluirItemFaturamentoAditamento?acao=excluirItemFaturamento');
	}
	
	function exibirAlteracaoItemFaturamentoCadastrado(id) {
		selecionarItensComponenteSelect();
		selecionarOpcoesQDCContrato();
		document.forms[0].idItemFaturamento.value = id;
		submeter('contratoForm', 'exibirAlteracaoItemFaturamentoCadastradoAditamento?acao=exibirAlteracaoItemFaturamentoCadastrado#faturamento');
	}
	//############# Fim javascript Aba faturamento
	
	function cancelar() {
		var retorno = confirm('Ao cancelar os dados ser�o perdidos, confirma o cancelamento?');
			if(retorno == true) {
				//location.href = '<c:url value="/exibirPesquisaContrato?limparSessao=true&unlock=true"/>';
				submeter('0','pesquisarContrato?limparSessao=true');
			}
	}
	
	function setarDatasContrato() {
		var dataAssinaturaCont = "${sessionScope.contrato.dataAssinatura}";
		var dataVencimentoCont = "${sessionScope.contrato.dataVencimentoObrigacoes}";

		// Formata��o da data porque o IE n�o formata pelo formato que vem do Java.
		var mesString = dataAssinaturaCont.substr(4,3);
		var diaString = dataAssinaturaCont.substr(8,2);
		var anoString = dataAssinaturaCont.substr(30,4);
		var dataAssinatura = new Date(mesString+" "+diaString+", "+anoString);
		mesString = dataVencimentoCont.substr(4,3);
		diaString = dataVencimentoCont.substr(8,2);
		anoString = dataVencimentoCont.substr(30,4);
		dataVencimento = new Date(mesString+" "+diaString+", "+anoString);
        
		var diaData = parseInt(dataAssinatura.getDate());
		if(diaData < 10) {
			diaData = "0" + diaData;
		}
		var mesData = parseInt(dataAssinatura.getMonth())+1;
		if(mesData < 10) {
			mesData = "0" + mesData;
		}		
		document.getElementById("dataAssinatura").value = diaData + "/" + mesData + "/" + dataAssinatura.getFullYear();
		mesData = parseInt(dataVencimento.getMonth());
		diaData = parseInt(dataVencimento.getDate());
		var anoData = parseInt(dataVencimento.getYear());
		if(diaData < 10) {
			diaData = "0" + diaData;
		}
		if(mesData < 10) {
			mesData = "0" + mesData;
		}
		document.getElementById("dataVencObrigacoesContratuais").value = diaData + "/" + mesData + "/" + dataVencimento.getFullYear();
	}	

	function onclickCheckPontoConsumo(chavePontoConsumo){

		obterChaveSegmentoPontoConsumo(chavePontoConsumo);
		
		document.getElementById("chaveSegmentoSelecionado").value = obterChaveSegmentoPontoConsumo(chavePontoConsumo);
		document.getElementById("chaveRamoAtividadeSelecionado").value = obterChaveRamoAtividadePontoConsumo(chavePontoConsumo);

		<c:if test="${contratoVO.selAbaConsumo eq true}">
			carregarSugestaoAmostragemPCSPorRamoAtividade();
			carregarSugestaoIntervalosPCSPorRamoAtividade();		
		</c:if>
		
		if(funcaoExiste('habilitarCampoReducaoIntervalo')) {		
			habilitarCampoReducaoIntervalo();	
		}	
		if(funcaoExiste('desabilitarCampoReducaoIntervalo')) {			
			desabilitarCampoReducaoIntervalo();	
		}
		
	}

	function init () {		

		if(funcaoExiste('habilitarCampoReducaoIntervalo')) {		
			habilitarCampoReducaoIntervalo();	
		}	

		if(funcaoExiste('desabilitarCampoReducaoIntervalo')) {			
			desabilitarCampoReducaoIntervalo();	
		}

		if(funcaoExiste('carregarFatorUnicoCorrecao')) {
			carregarFatorUnicoCorrecao();
		}	
		
		
		var itemFatura = document.getElementById("itemFatura");
		if(itemFatura != undefined && itemFatura != ""){
			habilitarBotaoAdicionarItemFaturamento(itemFatura);
		}		
		
		ajustarAsteriscosDadosTecnicos();
		
		corrigirPosicaoDatepicker();

	}

	function refreshPagina(){

		submeter('contratoForm', 'reexibirAditamentoContratoPontoConsumo');

	}
	
	function selecionarListas() {		
		var listaDiasDisponiveis = document.getElementById('listaDiasDisponiveis');		
		if (listaDiasDisponiveis != undefined) {
			for (i=0; i<listaDiasDisponiveis.length; i++){
				listaDiasDisponiveis.options[i].selected = true;
			}
		}
		var listaDiasSelecionados = document.getElementById('listaDiasSelecionados');		
		if (listaDiasSelecionados != undefined) {
			for (i=0; i<listaDiasSelecionados.length; i++){
				listaDiasSelecionados.options[i].selected = true;
			}
		}	
	}
	
	function habilitarRadioFatura(){
		document.getElementById('emitirFaturaComNfeNao').disabled = false;
		document.getElementById('emitirFaturaComNfeSim').disabled = false;
		document.getElementById('labelFaturaSim').disabled = false;
		document.getElementById('labelFaturaNao').disabled = false;
	}
	
	function desabilitarRadioFatura(){
		document.getElementById('emitirFaturaComNfeNao').disabled = true;
		document.getElementById('emitirFaturaComNfeSim').disabled = true;
		document.getElementById('emitirFaturaComNfeNao').checked = "checked";
	}
	
	function verificaRadioNfeAbaFaturamento(){
		var valor = document.getElementById('valorEmissiaoRadioNfe').value;
		if(valor == "false"){			
			desabilitarRadioFatura();
		}
	}
	
	addLoadEvent(init);

</script>

<h1 class="tituloInterno">
	<c:choose>
		<c:when test="${contratoVO.fluxoAditamento}">
			Aditar Contrato		
		</c:when>
		<c:otherwise>
			Alterar Contrato	
		</c:otherwise>
	</c:choose>
</h1>

<p class="orientacaoInicial">Selecione um ou mais Pontos de Consumo na lista abaixo, preencha os dados nas abas e clique em <span class="destaqueOrientacaoInicial">Aplicar</span>. Repita este procedimento para todos os Pontos de Consumo listados e ao final clique em <span class="destaqueOrientacaoInicial">Salvar</span> para concluir 
	<c:choose>
		<c:when test="${contratoVO.fluxoAditamento}">
			o aditamento		
		</c:when>
		<c:otherwise>
			a altera��o	
		</c:otherwise>
	</c:choose> do contrato.
</p>

<form:form method="post" styleId="formAditarContratoPontoConsumo" id="contratoForm" name="contratoForm">
	<input name="acao" type="hidden" id="acao" value="incluirContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoVO.chavePrimaria}"/>
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="indexLista" type="hidden" id="indexLista" value="${contratoVO.indexLista}">
	<input name="idModeloContrato" type="hidden" id="idModeloContrato" value="${contratoVO.idModeloContrato}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${contratoVO.idPontoConsumo}">
	<input id="dataAssinatura" name="dataAssinatura" type="hidden" value="${contratoVO.dataAssinatura}">
	<input id="chaveSegmentoSelecionado" type="hidden" value=""/>
	<input id="chaveRamoAtividadeSelecionado" type="hidden" value=""/>
	<input name="versao" type="hidden" id="versao" value="<c:out value='${contratoVO.versao}'/>" />
	<input id="idFaixaPressaoFornecimento" type="hidden" value="${contratoVO.faixaPressaoFornecimento}">
	<input name="versao" type="hidden" id="versao" value="<c:out value='${contratoVO.versao}'/>" />
	<input name="chavePrimariaPai" type="hidden" id="chavePrimariaPai" value="${contratoVO.chavePrimariaPai}"/>
	<input name="chavePrimariaPrincipal" type="hidden" id="chavePrimariaPrincipal" value="${contratoVO.chavePrimariaPrincipal}"/>

	<input class="campoTexto" type="hidden" id="numeroContrato" name="numeroContrato" value="${contratoVO.numeroContrato}" >
	<input class="campoTexto" type="hidden" id="situacaoContrato" name="situacaoContrato" value="${contratoVO.situacaoContrato}" >
	
	
	<input type="hidden" id="dataVencObrigacoesContratuais" name="dataVencObrigacoesContratuais" value="${sessionScope.contrato.dataVencimentoObrigacoes}"/>
    <input type="hidden" id="renovacaoAutomatica" name="renovacaoAutomatica" value="${sessionScope.contrato.renovacaoAutomatica}" />
    <input type="hidden" id="numDiasRenoAutoContrato" name="numDiasRenoAutoContrato" value="${sessionScope.contrato.numeroDiasRenovacaoAutomatica}" />	

	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${contratoVO.fluxoAlteracao}"/>
	<input name="fluxoAditamento" type="hidden" id="fluxoAditamento" value="${contratoVO.fluxoAditamento}"/>
	<fieldset id="conteinerContratoPontoConsumo" class="conteinerPesquisarIncluirAlterar">
	
	
	<table class="dataTableGGAS dataTableDialog">
		<thead>
			<tr>
				<th style="width: 20%;" > Im�vel
								<table style="width: 100%;"><thead><tr><th>&nbsp;</th></tr></thead></table>
				</th>
				<th style="width: 80%">
					Pontos de Consumo
					<table style="width: 100%">
						<thead style="width: 100%">
							<tr>
								 <th style="width: 3%"></th>
								 <th style="width: 35%" > Descri��o </th>
								 <th style="width: 30%"> Segmento </th>
								 <th style="width: 32%">Ramo de Atividade</th>
							</tr>
						</thead>
					</table>
				</th>
			</tr>
		</thead>
		<tbody>
			<c:set var="classeLinha" value="odd"/>
			<c:forEach items="${listaImovelPontoConsumoSelecionadoVO}" var="imovelPontoConsumoVO" varStatus="i">
				<tr class="${classeLinha}">
					<td class="rotuloTabela" style="text-align: left; width: 20%">
							<c:out value="${imovelPontoConsumoVO.nome}"/>
					</td>
					<td class="colunaSubTable">
						<display:table class="subDataTable"  list="${imovelPontoConsumoVO.listaPontoConsumo}" sort="list" id="pontoConsumo" decorator="br.com.ggas.web.contrato.contrato.decorator.PontoConsumoContratoDecorator" excludedParams="" requestURI="#">
						    <display:column property="selecionado" style="width: 3%" sortable="false" />
							<display:column property="descricao" sortable="false"  headerClass="tituloTabelaEsq" style="width: 32%" />
							<display:column property="segmentoDescricao" style="width: 30%" sortable="false" />
							<display:column property="ramoAtividadeDescricao" style="width: 32%" sortable="false" />
							<display:column property="copiaLiberada" style="width: 3%" sortable="false" />
							
							<display:column class="hidden" headerClass="hidden" title="">
								<input type="hidden" name="chavesPrimariasPontoConsumo" value="${pontoConsumo.chavePrimaria}" id="chavePrimariaPontoConsumo${pontoConsumo.chavePrimaria}">
							</display:column>	     	 
						</display:table>

					</td>
				</tr>
				<c:choose>
					<c:when test="${classeLinha eq 'odd'}">
						<c:set var="classeLinha" value="even"/>
					</c:when>
					<c:otherwise>
						<c:set var="classeLinha" value="odd"/>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</tbody>
	</table>
	  	
	</fieldset>

	<fieldset id="tabs" style="display: none">
		<ul>
			<c:if test="${contratoVO.selAbaPrincipais eq true}">
			<li><a href="#principal">Principal</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaTecnicos eq true}">
			<li><a href="#tecnicos">Dados T�cnicos</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaConsumo eq true}">
			<li><a href="#consumo">Consumo</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaFaturamento eq true}">
			<li><a href="#faturamento">Faturamento</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaRegrasFaturamento eq true}">
			<li><a href="#modalidades">Modalidades Consumo Faturamento</a></li>
			</c:if>
			<c:if test="${contratoVO.selAbaResponsabilidade eq true}">
			<li><a href="#responsabilidades">Responsabilidades</a></li>
			</c:if>
		</ul>
		
		<c:if test="${contratoVO.selAbaPrincipais eq true}">
		<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoPrincipal.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaTecnicos eq true}">
		<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoDadosTecnicos.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaConsumo eq true}">
		<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoConsumo.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaFaturamento eq true}">
		<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoFaturamento.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaRegrasFaturamento eq true}">
		<%@ include file="/jsp/contrato/contrato/contrato/include/abas/abaContratoModalidades.jsp" %>
		</c:if>
		<c:if test="${contratoVO.selAbaResponsabilidade eq true}">
			<jsp:include page="/jsp/contrato/contrato/contrato/include/abas/abaContratoResponsabilidade.jsp">
				<jsp:param name="fluxoAlteracao" value="true" />
				<jsp:param name="adicionarContratoCliente" value="adicionarContratoCliente" />			
				<jsp:param name="removerContratoCliente" value="removerContratoCliente" />
			</jsp:include>
		</c:if>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol2 botaoVoltar" value="<< Voltar" type="button" onclick="voltar();">
		<input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Cancelar" type="button" onclick="cancelar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
	    
		<fieldset class="conteinerBotoesDirFixo">
			<input name="button" class="bottonRightCol2" value="Aplicar" type="button" onclick="aplicar();">
			<c:if test="${(todosPontosConsumoDadosInformados eq true) && (!alterandoPontoConsumo)}">
					<c:choose>
					<c:when test="${contratoVO.fluxoAditamento}">
						<input name="button" class="bottonRightCol2 botaoAditar" value="Salvar" type="button" onclick="aditar();">	
					</c:when>
					<c:otherwise>
						<input name="button" class="bottonRightCol2 botaoAditar" value="Salvar" type="button" onclick="alterar();">	
					</c:otherwise>
				</c:choose>
			</c:if>
	    </fieldset>
	</fieldset>
</form:form> 