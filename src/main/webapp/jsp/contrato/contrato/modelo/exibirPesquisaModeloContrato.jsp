<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Modelo de Contrato<a class="linkHelp" href="<help:help>/consultadosmodelosdecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe o nome do modelo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir um novo modelo clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarModeloContrato" id="modeloContratoForm" name="modeloContratoForm">

	<script language="javascript">
	
$(document).ready(function(){
		
		//s� executa se a funcionalidade de migra��o do modelo do contrato for realizada
		var logErroMigracaoContrato = "<c:out value="${sessionScope.logErroMigracao}"/>";
		if(logErroMigracaoContrato != ""){
			submeter('modeloContratoForm', 'exibirLogErroMigracaoModeloContrato');
		}
		
	});
	
		function limparFormulario() {		
			document.forms[0].descricao.value = "";
			document.forms[0].habilitado[0].checked = true;		
			document.getElementById("tipoModelo").value = '-1';
		}	
			
		function incluir() {
			location.href = '<c:url value="/exibirInclusaoModeloContrato"/>';
		}
		
		function alterarModeloContrato(chave) {
		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				$("#isAlterar").val(true);
				submeter("modeloContratoForm", "exibirAlteracaoModeloContrato");
		    }
		}
		
		function detalharModeloContrato(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("modeloContratoForm", "exibirDetalhamentoModeloContrato");
		}	
			
		function removerListaModeloContrato(){		
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('modeloContratoForm', 'removerModeloContrato');
				}
	    	}
		}	
		
		function migrarModeloContrato(){
			var selecao = verificarSelecao();
			console.log(selecao)
			if (selecao == true) {
				submeter('modeloContratoForm', 'exibirMigracaoModelo');
		    }
		}
		
		function verificarModeloAtivo(){
			var indicesSelecionados = obterValoresCheckboxesSelecionados();
			
			for (var i = 0; i< indicesSelecionados.length; i++) {
				var objetuu = "#id_icon_habilitado_" + indicesSelecionados[i];
				var valorr = $(objetuu).val();
				var test = valorr != 'ativo';
				if (test) {
					alert('S� modelos de contrato ativos podem ser migrados.');
					return false;
				}
			}
			
			return true;
		}
		
	</script>
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input type="hidden" name="isAlterar" id="isAlterar" value="false"/>
	
	<fieldset id="conteinerPesquisarModeloContrato" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo" id="rotuloNome" for="descricao" >Nome:</label>
		<input class="campoTexto campoHorizontal" id="descricaoModelo" type="text" name="descricao" maxlength="30" size="45" tabindex="3" value="${modeloContratoVO.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
		
		<label class="rotulo rotuloHorizontal" id="rotuloIndicadorUso" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" 
			<c:if test="${modeloContratoVO.habilitado eq 'true'}">checked</c:if>>
			
		<label class="rotuloRadio" for="habilitado">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" 
			<c:if test="${modeloContratoVO.habilitado eq 'false'}">checked</c:if>>
			
		<label class="rotuloRadio" for="habilitado">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" 
			<c:if test="${empty modeloContratoVO.habilitado}">checked</c:if>>
			
		<label class="rotuloRadio" for="habilitado">Todos</label><br /><br />
		
		<label class="rotulo campoObrigatorio rotuloHorizontal" for="descricaoAbreviada">Tipo do Modelo:</label>
		<select name="tipoModelo" id="tipoModelo" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoModelo}" var="tipoModelo">
				<option value="<c:out value="${tipoModelo.chavePrimaria}"/>" 
					<c:if test="${modeloContratoVO.tipoModelo eq tipoModelo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoModelo.descricao}"/>
				</option>	
			</c:forEach>
		</select>		
		
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    
	    	<vacess:vacess param="pesquisarModeloContrato">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" 
	    			value="Pesquisar" type="submit">
	    	</vacess:vacess>
	    			
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" 
				value="Limpar" type="button" onclick="limparFormulario();">
						
		</fieldset>
		
	</fieldset>
	
	<c:if test="${listaModeloContrato ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaModeloContrato" id="modeloContrato"
			decorator="br.com.ggas.web.contrato.modelocontrato.decorator.ModeloContratoResultadoPesquisaDecorator" 
			sort="list" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" 
			requestURI="pesquisarModeloContrato"> 
	        
	        <display:column property="chavePrimariaComLock" style="width: 25px" media="html" sortable="false" 
	        	title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"/>
	     	 
	     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${modeloContrato.habilitado == true}">
						<img  alt="Ativo" title="Ativo" border="0" 
							src="<c:url value="/imagens/success_icon.png"/>">
						<input type="hidden" id="id_icon_habilitado_${modeloContrato.chavePrimaria}" 
							value="ativo"/>
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" border="0"
							src="<c:url value="/imagens/cancel16.png"/>">
						<input type="hidden" id="id_icon_habilitado_${modeloContrato.chavePrimaria}" 
							value="inativo"/>
					</c:otherwise>
				</c:choose>
			</display:column>
			
	        <display:column sortable="true" sortProperty="descricao" title="Nome" 
	        	headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href="javascript:detalharModeloContrato(<c:out value='${modeloContrato.chavePrimaria}'/>);">
	            	<span class="linkInvisivel"></span>
	            	<c:out value="${modeloContrato.descricao}"/>
	            </a>
	        </display:column>
	        
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
    	<c:if test="${not empty listaModeloContrato}">
    		<vacess:vacess param="exibirAlteracaoModeloContrato">
   				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" 
   					onclick="alterarModeloContrato();" type="button">
   			</vacess:vacess>
   			<vacess:vacess param="removerModeloContrato">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" 
					onclick="removerListaModeloContrato()" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirMigracaoModelo">
				<input name="botaoMigrar" value="Migrar Contrato" class="bottonRightCol2 bottonLeftColUltimo" onclick="migrarModeloContrato()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInclusaoModeloContrato">
			<input id="botaoIncluirModeloContrato" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" 
				onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>
	    
</form:form>
