<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<c:if test="${mapaContratoMigracao ne null}">
		
		<c:forEach items="${mapaContratoMigracao}" var="mapaContrato">
			<hr class="linhaSeparadoraPesquisa" />
			
			<label class="rotulo">Modelo:</label>			
			<c:if test="${not empty mapaContrato.key}">
				<span class="itemDetalhamento">${mapaContrato.key.second}</span>
			</c:if>
			
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" list="${mapaContrato.value}" sort="list" id="contrato" pagesize="15" excludedParams="" requestURI="#" >
				 
				 <display:column sortable="true" title="N�mero do<br />Contrato" sortProperty="numeroFormatado" style="width: 120px">
					<c:out value='${contrato.numeroFormatado}'/>
		    	 </display:column>
			     <display:column sortable="true" sortProperty="clienteAssinatura.nome" title="Nome do Cliente" headerClass="tituloTabelaEsq" style="text-align: left">
					<c:out value='${contrato.clienteAssinatura.nome}'/>
			     </display:column>
			     <display:column sortable="true" title="CPF/CNPJ Cliente" style="width: 135px"  sortProperty="clienteAssinatura.numeroDocumentoFormatado">
					<c:out value='${contrato.clienteAssinatura.numeroDocumentoFormatado}'/>
			     </display:column>		     
			 	<display:column sortable="true" title="Situa��o" style="width: 110px" sortProperty="situacao.descricao">
					<c:out value='${contrato.situacao.descricao}'/>
		    	</display:column>
		    	<display:column title="Data da<br />Assinatura" style="width: 80px" sortable="true" sortProperty="dataAssinatura">
					<fmt:formatDate value="${contrato.dataAssinatura}" pattern="dd/MM/yyyy"/>
		    	</display:column>			     
		    	<display:column title="Data de<br />Vencimento" style="width: 80px" sortable="true" sortProperty="dataAssinatura">
					<fmt:formatDate value="${contrato.dataVencimentoObrigacoes}" pattern="dd/MM/yyyy"/>
		    	
		    	</display:column>
			</display:table>
			
		</c:forEach>
	</c:if>
