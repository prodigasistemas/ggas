<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fieldset id="Tecnicos">
	<a class="linkHelp" href="<help:help>/abatcnicosinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>		
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado</th>
				<th width="73px">Obrigat�rio</th>
				<th width="73px">Valor Fixo</th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">						
				<td><c:if test="${selVazaoInstantanea eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgVazaoInstantanea eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoVazaoInstantanea eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Vaz�o Instant�nea:</td>
				<td class="campoTabela">
					<span id="detalhamentoVazaoInstantanea" class="itemDetalhamento"><c:out value="${vazaoInstantanea}"/></span>
					<c:if test="${vazaoInstantanea ne null && vazaoInstantanea ne ''}">
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<c:choose>
								<c:when test="${unidadeVazaoInstan ne ''}">
									<c:if test="${unidadeVazaoInstan eq unidade.chavePrimaria}">
										<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selVazaoInstantanea eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selVazaoInstantaneaMaxima eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgVazaoInstantaneaMaxima eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoVazaoInstantaneaMaxima eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Vaz�o M�xima Instant�nea:</td>
				<td class="campoTabela">
					<span id="detalhamentoVazaoInstantaneaMaxima" class="itemDetalhamento"><c:out value="${vazaoInstantaneaMaxima}"/></span>
					<c:if test="${vazaoInstantaneaMaxima ne null && vazaoInstantaneaMaxima ne ''}">
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<c:choose>
								<c:when test="${unidadeVazaoInstanMaxima ne ''}">
									<c:if test="${unidadeVazaoInstanMaxima eq unidade.chavePrimaria}">
										<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selVazaoInstantaneaMaxima eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selVazaoInstantaneaMinima eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgVazaoInstantaneaMinima eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoVazaoInstantaneaMinima eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Vaz�o M�nima Instant�nea:</td>
				<td class="campoTabela">
					<span id="detalhamentoVazaoInstantaneaMinima" class="itemDetalhamento"><c:out value="${vazaoInstantaneaMinima}"/></span>
					<c:if test="${vazaoInstantaneaMinima ne null && vazaoInstantaneaMinima ne ''}">
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<c:choose>
								<c:when test="${unidadeVazaoInstanMinima ne ''}">
									<c:if test="${unidadeVazaoInstanMinima eq unidade.chavePrimaria}">
										<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selVazaoInstantaneaMinima eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><c:if test="${valorFixoFaixaPressaoFornecimento eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Press�o de Fornecimento:</td>
				<td class="campoTabela">
					<c:if test="${faixaPressaoFornecimento ne null && faixaPressaoFornecimento ne ''}">
						<c:forEach items="${listaFaixasPressaoFornecimento}" var="faixa">
							<c:if test="${faixaPressaoFornecimento eq faixa.chavePrimaria}">
								<span id="detalhamentoFaixaPressaoFornecimento" class="itemDetalhamento"><c:if test="${not empty faixa.segmento}"><c:out value="${faixa.segmento.descricao}"/> - </c:if><c:out value="${faixa.descricaoFormatada}"/></span>
							</c:if>										
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPressaoMinimaFornecimento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPressaoMinimaFornecimento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPressaoMinimaFornecimento eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Press�o M�nima de Fornecimento:</td>
				<td class="campoTabela">
					<span id="detalhamentoPressaoMinimaForncecimento" class="itemDetalhamento"><c:out value="${pressaoMinimaFornecimento}"/></span>
					<c:if test="${pressaoMinimaFornecimento ne null && pressaoMinimaFornecimento ne ''}">
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<c:choose>
								<c:when test="${unidadePressaoMinimaFornec ne ''}">
									<c:if test="${unidadePressaoMinimaFornec eq unidade.chavePrimaria}">
										<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selPressaoMinimaForncecimento eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPressaoMaximaFornecimento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPressaoMaximaFornecimento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPressaoMaximaFornecimento eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Press�o M�xima de Fornecimento:</td>
				<td class="campoTabela">
					<span id="detalhamentoPressaoMaximaForncecimento" class="itemDetalhamento"><c:out value="${pressaoMaximaFornecimento}"/></span>
					<c:if test="${pressaoMinimaFornecimento ne null && pressaoMinimaFornecimento ne ''}">
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<c:choose>
								<c:when test="${unidadePressaoMaximaFornec ne ''}">
									<c:if test="${unidadePressaoMaximaFornec eq unidade.chavePrimaria}">
										<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selPressaoMaximaForncecimento eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPressaoLimiteFornecimento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPressaoLimiteFornecimento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPressaoLimiteFornecimento eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Press�o Limite de Fornecimento:</td>
				<td class="campoTabela">
					<span id="detalhamentoPressaoLimiteFornecimento" class="itemDetalhamento"><c:out value="${pressaoLimiteFornecimento}"/></span>
					<c:if test="${pressaoLimiteFornecimento ne null && pressaoLimiteFornecimento ne ''}">
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<c:choose>
								<c:when test="${unidadePressaoLimiteFornec ne ''}">
									<c:if test="${unidadePressaoLimiteFornec eq unidade.chavePrimaria}">
										<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selPressaoLimiteFornecimento eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span id="detalhamentoUnidadeVazaoInstan" class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Paradas Programadas Cliente:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selNumAnosCtrlParadaCliente eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumAnosCtrlParadaCliente eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoNumAnosCtrlParadaCliente eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">N�mero de Anos para Controle de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${numAnosCtrlParadaCliente ne null && numAnosCtrlParadaCliente ne ''}">
						<span id="detalhamentoParProgNumAnosCtrlParadaCliente" class="itemDetalhamento"><c:out value="${numAnosCtrlParadaCliente}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="parProgNumAnosCtrlParadaCliente">anos</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selMaxTotalParadasCliente eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMaxTotalParadasCliente eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoMaxTotalParadasCliente eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Limite M�ximo Total de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${maxTotalParadasCliente ne null && maxTotalParadasCliente ne ''}">
						<span id="detalhamentoParMaxTotalParadasCliente" class="itemDetalhamento"><c:out value="${maxTotalParadasCliente}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="parProgMaxTotalParadasCliente">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selMaxAnualParadasCliente eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMaxAnualParadasCliente eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoMaxAnualParadasCliente eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Limite M�ximo Anual de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${maxAnualParadasCliente ne null && maxAnualParadasCliente ne ''}">
						<span id="detalhamentoMaxAnualParadasCliente" class="itemDetalhamento"><c:out value="${maxAnualParadasCliente}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="maxAnualParadasCliente">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selNumDiasProgrParadaCliente eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumDiasProgrParadaCliente eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoNumDiasProgrParadaCliente eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Anteced�ncia para Programa��o de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${numDiasProgrParadaCliente ne null && numDiasProgrParadaCliente ne ''}">
						<span id="detalhamentoNumDiasProgrParadaCliente" class="itemDetalhamento"><c:out value="${numDiasProgrParadaCliente}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="numDiasProgrParadaCliente">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selNumDiasConsecParadaCliente eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumDiasConsecParadaCliente eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoNumDiasConsecParadaCliente eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">N�mero Dias Consecutivos de Paradas do Cliente:</td>
				<td class="campoTabela">
					<c:if test="${numDiasConsecParadaCliente ne null && numDiasConsecParadaCliente ne ''}">
						<span id="detalhamentoNumDiasConsecParadaCliente" class="itemDetalhamento"><c:out value="${numDiasConsecParadaCliente}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="detalhamentoNumDiasConsecParadaCliente">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Paradas Programadas CDL:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selNumAnosCtrlParadaCDL eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumAnosCtrlParadaCDL eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoNumAnosCtrlParadaCDL eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">N�mero de Anos para Controle de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${numAnosCtrlParadaCDL ne null && numAnosCtrlParadaCDL ne ''}">
						<span id="detalhamentoNumAnosCDLCtrlParadaCDL" class="itemDetalhamento"><c:out value="${numAnosCtrlParadaCDL}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="numAnosCtrlParadaCDL">anos</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selMaxTotalParadasCDL eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMaxTotalParadasCDL eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoMaxTotalParadasCDL eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Limite M�ximo Total de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${maxTotalParadasCDL ne null && maxTotalParadasCDL ne ''}">
						<span id="detalhamentoobgMaxTotalParadasCDL" class="itemDetalhamento"><c:out value="${maxTotalParadasCDL}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="maxTotalParadasCDL">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selMaxAnualParadasCDL eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMaxAnualParadasCDL eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoMaxAnualParadasCDL eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Limite M�ximo Anual de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${maxAnualParadasCDL ne null && maxAnualParadasCDL ne ''}">
						<span id="detalhamentoMaxAnualParadasCDL" class="itemDetalhamento"><c:out value="${maxAnualParadasCDL}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="maxAnualParadasCDL">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selNumDiasProgrParadaCDL eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumDiasProgrParadaCDL eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoNumDiasProgrParadaCDL eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Anteced�ncia para Programa��o de Paradas:</td>
				<td class="campoTabela">
					<c:if test="${numDiasProgrParadaCDL ne null && numDiasProgrParadaCDL ne ''}">
						<span id="detalhamentoNumDiasProgrParadaCDL" class="itemDetalhamento"><c:out value="${numDiasProgrParadaCDL}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="numDiasProgrParadaCDL">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selNumDiasConsecParadaCDL eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumDiasConsecParadaCDL eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoNumDiasConsecParadaCDL eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">N�mero de Dias Consecutivos de Paradas da CDL:</td>
				<td class="campoTabela">
					<c:if test="${numDiasConsecParadaCDL ne null && numDiasConsecParadaCDL ne ''}">
						<span id="detalhamentoNumDiasConsecParadaCDL" class="itemDetalhamento"><c:out value="${numDiasConsecParadaCDL}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="detalhamentoNumDiasConsecParadaCDL">dias</label>
					</c:if>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>