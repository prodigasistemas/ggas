<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<script>
	
	function controlarCheckBoxObrigatorios4(checkObrigatorios) {
		var valorCheck = checkObrigatorios.checked;

		var camposSelecionados = new Array('selHoraInicialDia','selRegimeConsumo','selFornecimentoMaximoDiario','selFornecimentoMinimoDiario','selFornecimentoMinimoMensal','selFornecimentoMinimoAnual','selFatorUnicoCorrecao','selConsumoFatFalhaMedicao','selLocalAmostragemPCS','obgLocalAmostragemPCS');
		var camposObrigatorios = new Array('obgHoraInicialDia','obgRegimeConsumo','obgFornecimentoMaximoDiario','obgFornecimentoMinimoDiario','obgFornecimentoMinimoMensal','obgFornecimentoMinimoAnual','obgFatorUnicoCorrecao','obgConsumoFatFalhaMedicao','obgLocalAmostragemPCS','obgTamReducaoRecuperacaoPCS');

		marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, valorCheck);	
	}
	
	function limparAbaConsumo(){
		
		document.getElementById('horaInicialDia').selectedIndex = "7";
		document.getElementById('unidadeFornecMaximoDiario').selectedIndex = "2";
		document.getElementById('unidadeFornecMinimoDiario').selectedIndex = "2";
		document.getElementById('unidadeFornecMinimoMensal').selectedIndex = "2";
		document.getElementById('unidadeFornecMinimoAnual').selectedIndex = "2";
	}
	
</script>

<fieldset id="Consumo">
	<a class="linkHelp" href="<help:help>/abaconsumoinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado<input id="checkAllSelecionado4" type="checkbox" name="todos"/></th>
				<th width="73px">Obrigat�rio<input id="checkAllObrigatorio4" type="checkbox" name="todos" onclick="controlarCheckBoxObrigatorios4(this);"/></th>
				<th width="73px">Valor Fixo<input id="checkAllValorFixo4" type="checkbox" name="todos" onclick="controlarCheckBoxValorFixo(this);" /></th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selHoraInicialDia" name="selHoraInicialDia" onclick="permitirSelecaoObrigatorio('selHoraInicialDia', 'obgHoraInicialDia');" <c:if test="${modeloContratoVO.selHoraInicialDia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgHoraInicialDia" name="obgHoraInicialDia" onclick="verificarSelecaoObrigatorio('obgHoraInicialDia', 'selHoraInicialDia');" <c:if test="${modeloContratoVO.obgHoraInicialDia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoHoraInicialDia" name="valorFixoHoraInicialDia" onclick="verificarSelecaoObrigatorio('valorFixoHoraInicialDia', 'selHoraInicialDia');" <c:if test="${modeloContratoVO.valorFixoHoraInicialDia == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Hora Inicial do Dia:</td>
				<td class="campoTabela">																			
					<select id="horaInicialDia" class="campoTexto campoHorizontal" name="horaInicialDia">
					  <option value="-1">Selecione</option>
					  <c:forEach begin="0" end="23" var="hora">
						<option value="${hora}" <c:if test="${modeloContratoVO.horaInicialDia == hora}">selected="selected"</c:if>>${hora}</option>
					  </c:forEach>
					</select>
					<label class="rotuloHorizontal rotuloInformativo" for="horaInicialDia">h</label>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selRegimeConsumo" name="selRegimeConsumo" onclick="permitirSelecaoObrigatorio('selRegimeConsumo', 'obgRegimeConsumo');" <c:if test="${modeloContratoVO.selRegimeConsumo == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgRegimeConsumo" name="obgRegimeConsumo" onclick="verificarSelecaoObrigatorio('obgRegimeConsumo', 'selRegimeConsumo');" <c:if test="${modeloContratoVO.obgRegimeConsumo == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoRegimeConsumo" name="valorFixoRegimeConsumo" onclick="verificarSelecaoObrigatorio('valorFixoRegimeConsumo', 'selRegimeConsumo');" <c:if test="${modeloContratoVO.valorFixoRegimeConsumo == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Regime de Consumo:</td>
				<td class="campoTabela">
					<select id="regimeConsumo" class="campoSelect" name="regimeConsumo">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaRegimeConsumo}" var="regimeConsumo">
							<option value="<c:out value="${regimeConsumo.chavePrimaria}"/>" <c:if test="${modeloContratoVO.regimeConsumo == regimeConsumo.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${regimeConsumo.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
								
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selFornecimentoMaximoDiario" name="selFornecimentoMaximoDiario" onclick="permitirSelecaoObrigatorio('selFornecimentoMaximoDiario', 'obgFornecimentoMaximoDiario');" <c:if test="${modeloContratoVO.selFornecimentoMaximoDiario == true}">checked="checked"</c:if>/><input name="selUnidadeFornecMaximoDiario" type="hidden" id="selUnidadeFornecMaximoDiario" value="${modeloContratoVO.selFornecimentoMaximoDiario}"></td>
                <td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgFornecimentoMaximoDiario" name="obgFornecimentoMaximoDiario" onclick="verificarSelecaoObrigatorio('obgFornecimentoMaximoDiario', 'selFornecimentoMaximoDiario');" <c:if test="${modeloContratoVO.obgFornecimentoMaximoDiario == true}">checked="checked"</c:if>/><input name="obgUnidadeFornecMaximoDiario" type="hidden" id="obgUnidadeFornecMaximoDiario" value="${modeloContratoVO.obgFornecimentoMaximoDiario}"></td>
                <td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFornecimentoMaximoDiario" name="valorFixoFornecimentoMaximoDiario" onclick="verificarSelecaoObrigatorio('valorFixoFornecimentoMaximoDiario', 'selFornecimentoMaximoDiario');" <c:if test="${modeloContratoVO.valorFixoFornecimentoMaximoDiario == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Fornecimento M�ximo Di�rio:</td>
				<td class="campoTabela">
					<input id="fornecimentoMaximoDiario" class="campoTexto campoHorizontal" type="text" size="9" maxlength="10" name="fornecimentoMaximoDiario" onkeypress="return formatarCampoDecimalPositivo(event, this, 7, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.fornecimentoMaximoDiario}"/>
					<select name="unidadeFornecMaximoDiario" id="unidadeFornecMaximoDiario" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadeFornecMaximoDiario}">
										<c:if test="${modeloContratoVO.unidadeFornecMaximoDiario eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selFornecimentoMinimoDiario" name="selFornecimentoMinimoDiario" onclick="permitirSelecaoObrigatorio('selFornecimentoMinimoDiario', 'obgFornecimentoMinimoDiario');" <c:if test="${modeloContratoVO.selFornecimentoMinimoDiario == true}">checked="checked"</c:if>/><input name="selUnidadeFornecMinimoDiario" type="hidden" id="selUnidadeFornecMinimoDiario" value="${modeloContratoVO.selFornecimentoMinimoDiario}"></td>
                <td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgFornecimentoMinimoDiario" name="obgFornecimentoMinimoDiario" onclick="verificarSelecaoObrigatorio('obgFornecimentoMinimoDiario', 'selFornecimentoMinimoDiario');" <c:if test="${modeloContratoVO.obgFornecimentoMinimoDiario == true}">checked="checked"</c:if>/><input name="obgUnidadeFornecMinimoDiario" type="hidden" id="obgUnidadeFornecMinimoDiario" value="${modeloContratoVO.obgFornecimentoMinimoDiario}"></td>
                <td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFornecimentoMinimoDiario" name="valorFixoFornecimentoMinimoDiario" onclick="verificarSelecaoObrigatorio('valorFixoFornecimentoMinimoDiario', 'selFornecimentoMinimoDiario');" <c:if test="${modeloContratoVO.valorFixoFornecimentoMinimoDiario == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Fornecimento M�nimo Di�rio:</td>
				<td class="campoTabela">
					<input id="fornecimentoMinimoDiario" class="campoTexto campoHorizontal" type="text" size="9" maxlength="10" name="fornecimentoMinimoDiario" onkeypress="return formatarCampoDecimalPositivo(event, this, 7, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.fornecimentoMinimoDiario}"/>
					<select name="unidadeFornecMinimoDiario" id="unidadeFornecMinimoDiario" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadeFornecMinimoDiario}">
										<c:if test="${modeloContratoVO.unidadeFornecMinimoDiario eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selFornecimentoMinimoMensal" name="selFornecimentoMinimoMensal" onclick="permitirSelecaoObrigatorio('selFornecimentoMinimoMensal', 'obgFornecimentoMinimoMensal');" <c:if test="${modeloContratoVO.selFornecimentoMinimoMensal == true}">checked="checked"</c:if>/><input name="selUnidadeFornecMinimoMensal" type="hidden" id="selUnidadeFornecMinimoMensal" value="${modeloContratoVO.selFornecimentoMinimoMensal}"></td>
                <td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgFornecimentoMinimoMensal" name="obgFornecimentoMinimoMensal" onclick="verificarSelecaoObrigatorio('obgFornecimentoMinimoMensal', 'selFornecimentoMinimoMensal');" <c:if test="${modeloContratoVO.obgFornecimentoMinimoMensal == true}">checked="checked"</c:if>/><input name="obgUnidadeFornecMinimoMensal" type="hidden" id="obgUnidadeFornecMinimoMensal" value="${modeloContratoVO.obgFornecimentoMinimoMensal}"></td>
                <td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFornecimentoMinimoMensal" name="valorFixoFornecimentoMinimoMensal" onclick="verificarSelecaoObrigatorio('valorFixoFornecimentoMinimoMensal', 'selFornecimentoMinimoMensal');" <c:if test="${modeloContratoVO.valorFixoFornecimentoMinimoMensal == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Fornecimento M�nimo Mensal:</td>
				<td class="campoTabela">
					<input id="fornecimentoMinimoMensal" class="campoTexto campoHorizontal" type="text" size="11" maxlength="12" name="fornecimentoMinimoMensal" onkeypress="return formatarCampoDecimalPositivo(event, this, 9, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.fornecimentoMinimoMensal}"/>
					<select name="unidadeFornecMinimoMensal" id="unidadeFornecMinimoMensal" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadeFornecMinimoMensal}">
										<c:if test="${modeloContratoVO.unidadeFornecMinimoMensal eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
						
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selFornecimentoMinimoAnual" name="selFornecimentoMinimoAnual" onclick="permitirSelecaoObrigatorio('selFornecimentoMinimoAnual', 'obgFornecimentoMinimoAnual');" <c:if test="${modeloContratoVO.selFornecimentoMinimoAnual == true}">checked="checked"</c:if>/><input name="selUnidadeFornecMinimoAnual" type="hidden" id="selUnidadeFornecMinimoAnual" value="${modeloContratoVO.selFornecimentoMinimoAnual}"></td>
                <td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgFornecimentoMinimoAnual" name="obgFornecimentoMinimoAnual" onclick="verificarSelecaoObrigatorio('obgFornecimentoMinimoAnual', 'selFornecimentoMinimoAnual');" <c:if test="${modeloContratoVO.obgFornecimentoMinimoAnual == true}">checked="checked"</c:if>/><input name="obgUnidadeFornecMinimoAnual" type="hidden" id="obgUnidadeFornecMinimoAnual" value="${modeloContratoVO.obgFornecimentoMinimoAnual}"></td>
                <td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFornecimentoMinimoAnual" name="valorFixoFornecimentoMinimoAnual" onclick="verificarSelecaoObrigatorio('valorFixoFornecimentoMinimoAnual', 'selFornecimentoMinimoAnual');" <c:if test="${modeloContratoVO.valorFixoFornecimentoMinimoAnual == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Fornecimento M�nimo Anual:</td>
				<td class="campoTabela">
					<input id="fornecimentoMinimoAnual" class="campoTexto campoHorizontal" type="text" size="15" maxlength="15" name="fornecimentoMinimoAnual" onkeypress="return formatarCampoDecimalPositivo(event, this, 12, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.fornecimentoMinimoAnual}"/>
					<select name="unidadeFornecMinimoAnual" id="unidadeFornecMinimoAnual" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadeFornecMinimoAnual}">
										<c:if test="${modeloContratoVO.unidadeFornecMinimoAnual eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selFatorUnicoCorrecao" name="selFatorUnicoCorrecao" onclick="permitirSelecaoObrigatorio('selFatorUnicoCorrecao', 'obgFatorUnicoCorrecao');" <c:if test="${modeloContratoVO.selFatorUnicoCorrecao == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgFatorUnicoCorrecao" name="obgFatorUnicoCorrecao" onclick="verificarSelecaoObrigatorio('obgFatorUnicoCorrecao', 'selFatorUnicoCorrecao');" <c:if test="${modeloContratoVO.obgFatorUnicoCorrecao == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Fator �nico de Corre��o de Volume:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selConsumoFatFalhaMedicao" name="selConsumoFatFalhaMedicao" onclick="permitirSelecaoObrigatorio('selConsumoFatFalhaMedicao', 'obgConsumoFatFalhaMedicao');" <c:if test="${modeloContratoVO.selConsumoFatFalhaMedicao == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgConsumoFatFalhaMedicao" name="obgConsumoFatFalhaMedicao" onclick="verificarSelecaoObrigatorio('obgConsumoFatFalhaMedicao', 'selConsumoFatFalhaMedicao');" <c:if test="${modeloContratoVO.obgConsumoFatFalhaMedicao == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoConsumoFatFalhaMedicao" name="valorFixoConsumoFatFalhaMedicao" onclick="verificarSelecaoObrigatorio('valorFixoConsumoFatFalhaMedicao', 'selConsumoFatFalhaMedicao');" <c:if test="${modeloContratoVO.valorFixoConsumoFatFalhaMedicao == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Consumo a ser Faturado em Falha de Medi��o:</td>
				<td class="campoTabela">							
					<select name="consumoFatFalhaMedicao" id="consumoFatFalhaMedicao" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaAcaoAnormalidadeConsumo}" var="acao">
							<option value="<c:out value="${acao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoFatFalhaMedicao eq acao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${acao.descricao}"/>
							</option>		
						</c:forEach>
					</select>							
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">PCS:</td>				
			</tr>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado4" type="checkbox" value="true" id="selLocalAmostragemPCS" name="selLocalAmostragemPCS" onclick="permitirSelecaoObrigatorio('selLocalAmostragemPCS', 'obgLocalAmostragemPCS');" <c:if test="${modeloContratoVO.selLocalAmostragemPCS == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio4" type="checkbox" value="true" id="obgLocalAmostragemPCS" name="obgLocalAmostragemPCS" onclick="verificarSelecaoObrigatorio('obgLocalAmostragemPCS', 'selLocalAmostragemPCS');" <c:if test="${modeloContratoVO.obgLocalAmostragemPCS == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Local de Amostragem do PCS:</td>
				<td class="campoTabela">

					<label class="rotulo rotuloVertical" for="ordemPrioridadePCS">Ordem de Prioridade:<span class="itemContador" id="quantidadePontosAssociados"></span></label><br />
					<select id="locaisAmostragemDisponiveis" class="campoList campoVertical" style="width: 111px" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].locaisAmostragemDisponiveis,document.forms[0].idsLocalAmostragemAssociados,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);">
						<c:forEach items="${listaLocalAmostragemPCS}" var="localAmostragem">
							<option value="<c:out value="${localAmostragem.chavePrimaria}"/>" title="<c:out value="${localAmostragem.descricao}"/>">
								<c:out value="${localAmostragem.descricao}"/>
							</option>
						</c:forEach>
				  	</select>
				  	<fieldset class="conteinerBotoesCampoList3">
						<input type="button" name="right" value="&gt;" class="bottonRightCol" onClick="moveSelectedOptionsEspecial(document.forms[0].locaisAmostragemDisponiveis,document.forms[0].idsLocalAmostragemAssociados,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);">
						<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" onClick="moveAllOptionsEspecial(document.forms[0].locaisAmostragemDisponiveis,document.forms[0].idsLocalAmostragemAssociados,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);">
						<input type="button" name="left" value="&lt;" class="bottonRightCol" id="botaoDirTop" onClick="moveSelectedOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);">
						<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol" onClick="moveAllOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);">
					</fieldset>
					<select id="idsLocalAmostragemAssociados" class="campoList" style="width: 111px" name="idsLocalAmostragemAssociados" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true); mostrarAsteriscoConsumo(document.getElementById('idsLocalAmostragemAssociados'),['intervaloRecuperacaoPCS']);">
						<c:forEach items="${listaLocalAmostragemAssociados}" var="localAmostragemAssoc">
							<option value="<c:out value="${localAmostragemAssoc.chavePrimaria}"/>" title="<c:out value="${localAmostragemAssoc.descricao}"/>">
								<c:out value="${localAmostragemAssoc.descricao}"/>
							</option>
						</c:forEach>
				  	</select>
				  	<fieldset class="conteinerBotoesCampoList2">
						<input type="button" class="bottonRightCol" value="&#8892;" onclick="moveOptionUp(this.form['idsLocalAmostragemAssociados']);">
						<input type="button" class="bottonRightCol" value="&#8891;" onclick="moveOptionDown(this.form['idsLocalAmostragemAssociados']);">
					</fieldset>
				
				</td>					
			</tr>
			
			<tr class="odd semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio4" name="obgIntervaloRecuperacaoPCS" value="true" campoPai="localAmostragemPCS"  /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Intervalo para Recupera��o do PCS:</td>
				<td class="campoTabela">

					<label class="rotulo rotuloVertical" for="ordemPrioridadePCS">Ordem de Prioridade:<span class="itemContador" id="quantidadePontosAssociados"></span></label><br />
					<select id="intervalosAmostragemDisponiveis" class="campoList campoVertical" style="width: 111px" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].intervalosAmostragemDisponiveis,document.forms[0].idsIntervaloAmostragemAssociados,true);habilitarCampoReducaoIntervalo();">
						<c:forEach items="${listaIntervaloPCS}" var="intervalo">
							<option value="<c:out value="${intervalo.chavePrimaria}"/>" title="<c:out value="${intervalo.descricao}"/>">
								<c:out value="${intervalo.descricao}"/>
							</option>
						</c:forEach>
				  	</select>
				  	<fieldset class="conteinerBotoesCampoList3">
						<input type="button" name="right" value="&gt;" class="bottonRightCol" onClick="moveSelectedOptionsEspecial(document.forms[0].intervalosAmostragemDisponiveis,document.forms[0].idsIntervaloAmostragemAssociados,true);habilitarCampoReducaoIntervalo();">
						<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" onClick="moveAllOptionsEspecial(document.forms[0].intervalosAmostragemDisponiveis,document.forms[0].idsIntervaloAmostragemAssociados,true);habilitarCampoReducaoIntervalo();">
						<input type="button" name="left" value="&lt;" class="bottonRightCol" id="botaoDirTop" onClick="moveSelectedOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);desabilitarCampoReducaoIntervalo();">
						<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol" onClick="moveAllOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);desabilitarCampoReducaoIntervalo();">
					</fieldset>					
					<select id="idsIntervaloAmostragemAssociados" class="campoList" style="width: 111px" name="idsIntervaloAmostragemAssociados" multiple="multiple" onDblClick="moveSelectedOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);desabilitarCampoReducaoIntervalo();">
						<c:forEach items="${listaIntervaloAmostragemAssociados}" var="intervaloAssoc">
							<option value="<c:out value="${intervaloAssoc.chavePrimaria}"/>" title="<c:out value="${intervaloAssoc.descricao}"/>"><c:out value="${intervaloAssoc.descricao}"/></option>
						</c:forEach>
				  	</select>
				  	<fieldset class="conteinerBotoesCampoList2">
						<input type="button" class="bottonRightCol" value="&#8892;" onclick="moveOptionUp(this.form['idsIntervaloAmostragemAssociados']);">
						<input type="button" class="bottonRightCol" value="&#8891;" onclick="moveOptionDown(this.form['idsIntervaloAmostragemAssociados']);">
					</fieldset>				
				
				</td>					
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio4" styleId="obgTamReducaoRecuperacaoPCS" name="obgTamReducaoRecuperacaoPCS" value="true" campoPai="localAmostragemPCS" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Tamanho da Redu��o para Recupera��o do PCS:</td>
				<td class="campoTabela"><input class="campoTexto" type="text" id="tamReducaoRecuperacaoPCS" name="tamReducaoRecuperacaoPCS" maxlength="2" size="1" value="${modeloContratoVO.tamReducaoRecuperacaoPCS}" onkeypress="return formatarCampoInteiro(event);"></td>					
			</tr>
			
		</tbody>
	</table>
</fieldset>