<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script>


	function controlarCheckBoxObrigatorios3(elemCheck) {
		var valorCheck = elemCheck.checked;

		var camposSelecionados = new Array('selPressaoColetada','selVazaoInstantanea','selVazaoInstantaneaMaxima','selVazaoInstantaneaMinima','selPressaoMinimaFornecimento','selPressaoMaximaFornecimento','selPressaoLimiteFornecimento','selNumAnosCtrlParadaCliente','selMaxAnualParadasCliente','selNumDiasProgrParadaCliente','selNumDiasConsecParadaCliente','selNumAnosCtrlParadaCDL','selMaxAnualParadasCDL','selNumDiasProgrParadaCDL','selNumDiasConsecParadaCDL');
		var camposObrigatorios = new Array('obgPressaoColetada','obgVazaoInstantanea','obgVazaoInstantaneaMaxima','obgVazaoInstantaneaMinima','obgPressaoMinimaFornecimento','obgPressaoMaximaFornecimento','obgPressaoLimiteFornecimento','obgNumAnosCtrlParadaCliente','obgMaxAnualParadasCliente','obgNumDiasProgrParadaCliente','obgNumDiasConsecParadaCliente','obgNumAnosCtrlParadaCDL','obgMaxAnualParadasCDL','obgNumDiasProgrParadaCDL','obgNumDiasConsecParadaCDL');

		marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, valorCheck);
		
	}
	
	function limparAbaTecnicos(){
	}
	
</script>

<fieldset id="Tecnicos">
	<a class="linkHelp" href="<help:help>/abatcnicosinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>		
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado<input id="checkAllSelecionado3" type="checkbox" name="todos"/></th>
				<th width="73px">Obrigat�rio<input id="checkAllObrigatorio3" type="checkbox" name="todos" onclick="controlarCheckBoxObrigatorios3(this);"/></th>
				<th width="73px">Valor Fixo <input id="checkAllValorFixo3" type="checkbox" name="todos" onclick="controlarCheckBoxValorFixo(this);"/></th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>					
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selVazaoInstantanea" name="selVazaoInstantanea" onclick="permitirSelecaoObrigatorio('selVazaoInstantanea', 'obgVazaoInstantanea');" <c:if test="${modeloContratoVO.selVazaoInstantanea == true}">checked="checked"</c:if>/> <input name="selUnidadeVazaoInstan" type="hidden" id="selUnidadeVazaoInstan" value="${modeloContratoVO.selVazaoInstantanea}"></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgVazaoInstantanea" name="obgVazaoInstantanea" onclick="verificarSelecaoObrigatorio('obgVazaoInstantanea', 'selVazaoInstantanea');" <c:if test="${modeloContratoVO.obgVazaoInstantanea == true}">checked="checked"</c:if>/> <input name="obgUnidadeVazaoInstan" type="hidden" id="obgUnidadeVazaoInstan" value="${modeloContratoVO.obgVazaoInstantanea}"></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoVazaoInstantanea" name="valorFixoVazaoInstantanea" onclick="verificarSelecaoObrigatorio('valorFixoVazaoInstantanea', 'selVazaoInstantanea');" <c:if test="${modeloContratoVO.valorFixoVazaoInstantanea == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Vaz�o Instant�nea:</td>
				<td class="campoTabela">
					<input id="vazaoInstantanea" class="campoTexto campoHorizontal" type="text" size="14" maxlength="14" name="vazaoInstantanea" onkeypress="return formatarCampoDecimalPositivo(event, this, 9, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${modeloContratoVO.vazaoInstantanea}"/>
					<select name="unidadeVazaoInstan" id="unidadeVazaoInstan" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>"
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadeVazaoInstan}">
										<c:if test="${modeloContratoVO.unidadeVazaoInstan eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selVazaoInstantaneaMaxima" name="selVazaoInstantaneaMaxima" onclick="permitirSelecaoObrigatorio('selVazaoInstantaneaMaxima', 'obgVazaoInstantaneaMaxima');" <c:if test="${modeloContratoVO.selVazaoInstantaneaMaxima == true}">checked="checked"</c:if>/> <input name="selUnidadeVazaoInstanMaxima" type="hidden" id="selUnidadeVazaoInstanMaxima" value="${modeloContratoVO.selVazaoInstantaneaMaxima}"></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgVazaoInstantaneaMaxima" name="obgVazaoInstantaneaMaxima" onclick="verificarSelecaoObrigatorio('obgVazaoInstantaneaMaxima', 'selVazaoInstantaneaMaxima');" <c:if test="${modeloContratoVO.obgVazaoInstantaneaMaxima == true}">checked="checked"</c:if>/> <input name="obgUnidadeVazaoInstanMaxima" type="hidden" id="obgUnidadeVazaoInstanMaxima" value="${modeloContratoVO.obgVazaoInstantaneaMaxima}"></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoVazaoInstantaneaMaxima" name="valorFixoVazaoInstantaneaMaxima" onclick="verificarSelecaoObrigatorio('valorFixoVazaoInstantaneaMaxima', 'selVazaoInstantaneaMaxima');" <c:if test="${modeloContratoVO.valorFixoVazaoInstantaneaMaxima == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Vaz�o M�xima Instant�nea:</td>
				<td class="campoTabela">
					<input id="vazaoInstantaneaMaxima" class="campoTexto campoHorizontal" type="text" size="14" maxlength="14" name="vazaoInstantaneaMaxima" onkeypress="return formatarCampoDecimalPositivo(event, this, 9, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${modeloContratoVO.vazaoInstantaneaMaxima}"/>
					<select name="unidadeVazaoInstanMaxima" id="unidadeVazaoInstanMaxima" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>"
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadeVazaoInstanMaxima}">
										<c:if test="${modeloContratoVO.unidadeVazaoInstanMaxima eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selVazaoInstantaneaMinima" name="selVazaoInstantaneaMinima" onclick="permitirSelecaoObrigatorio('selVazaoInstantaneaMinima', 'obgVazaoInstantaneaMinima');" <c:if test="${modeloContratoVO.selVazaoInstantaneaMinima == true}">checked="checked"</c:if>/> <input name="selUnidadeVazaoInstanMinima" type="hidden" id="selUnidadeVazaoInstantaneaMinima" value="${modeloContratoVO.selVazaoInstantaneaMinima}"></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgVazaoInstantaneaMinima" name="obgVazaoInstantaneaMinima" onclick="verificarSelecaoObrigatorio('obgVazaoInstantaneaMinima', 'selVazaoInstantaneaMinima');" <c:if test="${modeloContratoVO.obgVazaoInstantaneaMinima == true}">checked="checked"</c:if>/> <input name="obgUnidadeVazaoInstanMinima" type="hidden" id="obgUnidadeVazaoInstantaneaMinima" value="${modeloContratoVO.obgVazaoInstantaneaMinima}"></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoVazaoInstantaneaMinima" name="valorFixoVazaoInstantaneaMinima" onclick="verificarSelecaoObrigatorio('valorFixoVazaoInstantaneaMinima', 'selVazaoInstantaneaMinima');" <c:if test="${modeloContratoVO.valorFixoVazaoInstantaneaMinima == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Vaz�o M�nima Instant�nea:</td>
				<td class="campoTabela">
					<input id="vazaoInstantaneaMinima" class="campoTexto campoHorizontal" type="text" size="14" maxlength="14" name="vazaoInstantaneaMinima" onkeypress="return formatarCampoDecimalPositivo(event, this, 9, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${modeloContratoVO.vazaoInstantaneaMinima}"/>
					<select name="unidadeVazaoInstanMinima" id="unidadeVazaoInstanMinima" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadeVazao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>"
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadeVazaoInstanMinima}">
										<c:if test="${modeloContratoVO.unidadeVazaoInstanMinima eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado3" type="hidden" value="true" name="selFaixaPressaoFornecimento" id="selFaixaPressaoFornecimento" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio3" type="hidden" value="true" name="obgFaixaPressaoFornecimento" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFaixaPressaoFornecimento" name="valorFixoFaixaPressaoFornecimento" <c:if test="${modeloContratoVO.valorFixoFaixaPressaoFornecimento == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Press�o de Fornecimento:</td>
				<td class="campoTabela">
					<select name="faixaPressaoFornecimento" id="faixaPressaoFornecimento" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaFaixasPressaoFornecimento}" var="faixa">
							<option value="<c:out value="${faixa.chavePrimaria}"/>" <c:if test="${modeloContratoVO.faixaPressaoFornecimento eq faixa.chavePrimaria}">selected="selected"</c:if>>											
								<c:if test="${not empty faixa.segmento}"><c:out value="${faixa.segmento.descricao}"/> - </c:if><c:out value="${faixa.descricaoFormatada}"/>
							</option>											
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selPressaoColetada" name="selPressaoColetada" onclick="permitirSelecaoObrigatorio('selPressaoColetada', 'obgPressaoColetada');" <c:if test="${modeloContratoVO.selPressaoColetada == true}">checked="checked"</c:if>/> <input name="selUnidadePressaoColetada" type="hidden" id="selUnidadePressaoColetada" value="${modeloContratoVO.selPressaoColetada}"></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgPressaoColetada" name="obgPressaoColetada" onclick="verificarSelecaoObrigatorio('obgPressaoColetada', 'selPressaoColetada');" <c:if test="${modeloContratoVO.obgPressaoColetada == true}">checked="checked"</c:if>/> <input name="obgUnidadePressaoColetada" type="hidden" id="obgUnidadePressaoColetada" value="${modeloContratoVO.obgPressaoColetada}"></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPressaoColetada" name="valorFixoPressaoColetada" onclick="verificarSelecaoObrigatorio('valorFixoPressaoColetada', 'selPressaoColetada');" <c:if test="${modeloContratoVO.valorFixoPressaoColetada == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Press�o Coletada:</td>
				<td class="campoTabela">
					<input id="pressaoColetada" class="campoTexto campoHorizontal" type="text" size="9" maxlength="11" name="pressaoColetada" onkeypress="return formatarCampoDecimalPositivo(event, this, 5, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${modeloContratoVO.pressaoColetada}"/>
					<select name="unidadePressaoColetada" id="unidadePressaoColetada" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadePressaoColetada}">
										<c:if test="${modeloContratoVO.unidadePressaoColetada eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selPressaoMinimaFornecimento" name="selPressaoMinimaFornecimento" onclick="permitirSelecaoObrigatorio('selPressaoMinimaFornecimento', 'obgPressaoMinimaFornecimento');" <c:if test="${modeloContratoVO.selPressaoMinimaFornecimento == true}">checked="checked"</c:if>/> <input name="selUnidadePressaoMinimaFornec" type="hidden" id="selUnidadePressaoMinimaFornec" value="${modeloContratoVO.selPressaoMinimaFornecimento}"></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgPressaoMinimaFornecimento" name="obgPressaoMinimaFornecimento" onclick="verificarSelecaoObrigatorio('obgPressaoMinimaFornecimento', 'selPressaoMinimaFornecimento');" <c:if test="${modeloContratoVO.obgPressaoMinimaFornecimento == true}">checked="checked"</c:if>/> <input name="obgUnidadePressaoMinimaFornec" type="hidden" id="obgUnidadePressaoMinimaFornec" value="${modeloContratoVO.obgPressaoMinimaFornecimento}"></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPressaoMinimaFornecimento" name="valorFixoPressaoMinimaFornecimento" onclick="verificarSelecaoObrigatorio('valorFixoPressaoMinimaFornecimento', 'selPressaoMinimaFornecimento');" <c:if test="${modeloContratoVO.valorFixoPressaoMinimaFornecimento == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Press�o M�nima de Fornecimento:</td>
				<td class="campoTabela">
					<input id="pressaoMinimaFornecimento" class="campoTexto campoHorizontal" type="text" size="9" maxlength="11" name="pressaoMinimaFornecimento" onkeypress="return formatarCampoDecimalPositivo(event, this, 5, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${modeloContratoVO.pressaoMinimaFornecimento}"/>							
					<select name="unidadePressaoMinimaFornec" id="unidadePressaoMinimaFornec" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadePressaoMinimaFornec}">
										<c:if test="${modeloContratoVO.unidadePressaoMinimaFornec eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selPressaoMaximaFornecimento" name="selPressaoMaximaFornecimento" onclick="permitirSelecaoObrigatorio('selPressaoMaximaFornecimento', 'obgPressaoMaximaFornecimento');" <c:if test="${modeloContratoVO.selPressaoMaximaFornecimento == true}">checked="checked"</c:if>/> <input name="selUnidadePressaoMaximaFornec" type="hidden" id="selUnidadePressaoMaximaFornec" value="${modeloContratoVO.selPressaoMaximaFornecimento}"></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgPressaoMaximaFornecimento" name="obgPressaoMaximaFornecimento" onclick="verificarSelecaoObrigatorio('obgPressaoMaximaFornecimento', 'selPressaoMaximaFornecimento');" <c:if test="${modeloContratoVO.obgPressaoMaximaFornecimento == true}">checked="checked"</c:if>/> <input name="obgUnidadePressaoMaximaFornec" type="hidden" id="obgUnidadePressaoMaximaFornec" value="${modeloContratoVO.obgPressaoMaximaFornecimento}"></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPressaoMaximaFornecimento" name="valorFixoPressaoMaximaFornecimento" onclick="verificarSelecaoObrigatorio('valorFixoPressaoMaximaFornecimento', 'selPressaoMaximaFornecimento');" <c:if test="${modeloContratoVO.valorFixoPressaoMaximaFornecimento == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Press�o M�xima de Fornecimento:</td>
				<td class="campoTabela">
					<input id="pressaoMaximaFornecimento" class="campoTexto campoHorizontal" type="text" size="9" maxlength="11" name="pressaoMaximaFornecimento" onkeypress="return formatarCampoDecimalPositivo(event, this, 5, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${modeloContratoVO.pressaoMaximaFornecimento}"/>
					<select name="unidadePressaoMaximaFornec" id="unidadePressaoMaximaFornec" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadePressaoMaximaFornec}">
										<c:if test="${modeloContratoVO.unidadePressaoMaximaFornec eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
						
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selPressaoLimiteFornecimento" name="selPressaoLimiteFornecimento" onclick="permitirSelecaoObrigatorio('selPressaoLimiteFornecimento', 'obgPressaoLimiteFornecimento');" <c:if test="${modeloContratoVO.selPressaoLimiteFornecimento == true}">checked="checked"</c:if>/> <input name="selUnidadePressaoLimiteFornec" type="hidden" id="selUnidadePressaoLimiteFornec" value="${modeloContratoVO.selPressaoLimiteFornecimento}"></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgPressaoLimiteFornecimento" name="obgPressaoLimiteFornecimento" onclick="verificarSelecaoObrigatorio('obgPressaoLimiteFornecimento', 'selPressaoLimiteFornecimento');" <c:if test="${modeloContratoVO.obgPressaoLimiteFornecimento == true}">checked="checked"</c:if>/> <input name="obgUnidadePressaoLimiteFornec" type="hidden" id="obgUnidadePressaoLimiteFornec" value="${modeloContratoVO.obgPressaoLimiteFornecimento}"></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPressaoLimiteFornecimento" name="valorFixoPressaoLimiteFornecimento" onclick="verificarSelecaoObrigatorio('valorFixoPressaoLimiteFornecimento', 'selPressaoLimiteFornecimento');" <c:if test="${modeloContratoVO.valorFixoPressaoLimiteFornecimento == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Press�o Limite de Fornecimento:</td>
				<td class="campoTabela">
					<input id="pressaoLimiteFornecimento" class="campoTexto campoHorizontal" type="text" size="9" maxlength="11" name="pressaoLimiteFornecimento" onkeypress="return formatarCampoDecimalPositivo(event, this, 5, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${modeloContratoVO.pressaoLimiteFornecimento}"/>
					<select name="unidadePressaoLimiteFornec" id="unidadePressaoLimiteFornec" class="campoSelect campoHorizontal">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
							<option value="<c:out value="${unidade.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.unidadePressaoMaximaFornec}">
										<c:if test="${modeloContratoVO.unidadePressaoLimiteFornec eq unidade.chavePrimaria}">selected="selected"</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${unidade.indicadorPadrao eq true}">selected="selected"</c:if>	
									</c:otherwise>
								</c:choose>>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Paradas Programadas Cliente:</td>				
			</tr>
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selNumAnosCtrlParadaCliente" name="selNumAnosCtrlParadaCliente" onclick="permitirSelecaoObrigatorio('selNumAnosCtrlParadaCliente', 'obgNumAnosCtrlParadaCliente');" <c:if test="${modeloContratoVO.selNumAnosCtrlParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgNumAnosCtrlParadaCliente" name="obgNumAnosCtrlParadaCliente" onclick="verificarSelecaoObrigatorio('obgNumAnosCtrlParadaCliente', 'selNumAnosCtrlParadaCliente');" <c:if test="${modeloContratoVO.obgNumAnosCtrlParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumAnosCtrlParadaCliente" name="valorFixoNumAnosCtrlParadaCliente" onclick="verificarSelecaoObrigatorio('valorFixoNumAnosCtrlParadaCliente', 'selNumAnosCtrlParadaCliente');" <c:if test="${modeloContratoVO.valorFixoNumAnosCtrlParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">N�mero de Anos para Controle de Paradas:</td>
				<td class="campoTabela">
					<input id="numAnosCtrlParadaCliente" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="numAnosCtrlParadaCliente" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numAnosCtrlParadaCliente}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="numAnosCtrlParadaCliente">anos</label>
				</td>
			</tr>
			
			<tr class="even semLinhaFilhoUltimo">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio3" name="obgMaxTotalParadasCliente" value="true" campoPai="numAnosCtrlParadaCliente" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoMaxTotalParadasCliente" campoPai="selNumAnosCtrlParadaCliente" name="valorFixoMaxTotalParadasCliente" <c:if test="${modeloContratoVO.valorFixoMaxTotalParadasCliente == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Limite M�ximo Total de Paradas:</td>
				<td class="campoTabela">
					<input id="maxTotalParadasCliente" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="maxTotalParadasCliente" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.maxTotalParadasCliente}" />
					<label class="rotuloHorizontal rotuloInformativo" for="maxTotalParadasCliente">dias</label>
				</td>
			</tr>		
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selMaxAnualParadasCliente" name="selMaxAnualParadasCliente" onclick="permitirSelecaoObrigatorio('selMaxAnualParadasCliente', 'obgMaxAnualParadasCliente');" <c:if test="${modeloContratoVO.selMaxAnualParadasCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgMaxAnualParadasCliente" name="obgMaxAnualParadasCliente" onclick="verificarSelecaoObrigatorio('obgMaxAnualParadasCliente', 'selMaxAnualParadasCliente');" <c:if test="${modeloContratoVO.obgMaxAnualParadasCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoMaxAnualParadasCliente" name="valorFixoMaxAnualParadasCliente" onclick="verificarSelecaoObrigatorio('valorFixoMaxAnualParadasCliente', 'selMaxAnualParadasCliente');" <c:if test="${modeloContratoVO.valorFixoMaxAnualParadasCliente == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Limite M�ximo Anual de Paradas:</td>
				<td class="campoTabela">
					<input id="maxAnualParadasCliente" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="maxAnualParadasCliente" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.maxAnualParadasCliente}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="maxAnualParadasCliente">dias</label>
				</td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selNumDiasProgrParadaCliente" name="selNumDiasProgrParadaCliente" onclick="permitirSelecaoObrigatorio('selNumDiasProgrParadaCliente', 'obgNumDiasProgrParadaCliente');" <c:if test="${modeloContratoVO.selNumDiasProgrParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgNumDiasProgrParadaCliente" name="obgNumDiasProgrParadaCliente" onclick="verificarSelecaoObrigatorio('obgNumDiasProgrParadaCliente', 'selNumDiasProgrParadaCliente');" <c:if test="${modeloContratoVO.obgNumDiasProgrParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumDiasProgrParadaCliente" name="valorFixoNumDiasProgrParadaCliente" onclick="verificarSelecaoObrigatorio('valorFixoNumDiasProgrParadaCliente', 'selNumDiasProgrParadaCliente');" <c:if test="${modeloContratoVO.valorFixoNumDiasProgrParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Anteced�ncia para Programa��o de Paradas:</td>
				<td class="campoTabela">
					<input id="numDiasProgrParadaCliente" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="numDiasProgrParadaCliente" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numDiasProgrParadaCliente}" />
					<label class="rotuloHorizontal rotuloInformativo" for="numDiasProgrParadaCliente">dias</label>
				</td>
			</tr>
			
			<tr class="odd">
				<td><input class="checkSelecionado3" type="checkbox" value="true" id="selNumDiasConsecParadaCliente" name="selNumDiasConsecParadaCliente" onclick="permitirSelecaoObrigatorio('selNumDiasConsecParadaCliente', 'obgNumDiasConsecParadaCliente');" <c:if test="${modeloContratoVO.selNumDiasConsecParadaCliente == true}">checked="checked"</c:if>/></td>
				<td><input class="checkObrigatorio3" type="checkbox" value="true" id="obgNumDiasConsecParadaCliente" name="obgNumDiasConsecParadaCliente" onclick="verificarSelecaoObrigatorio('obgNumDiasConsecParadaCliente', 'selNumDiasConsecParadaCliente');" <c:if test="${modeloContratoVO.obgNumDiasConsecParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumDiasConsecParadaCliente" name="valorFixoNumDiasConsecParadaCliente" onclick="verificarSelecaoObrigatorio('valorFixoNumDiasConsecParadaCliente', 'selNumDiasConsecParadaCliente');" <c:if test="${modeloContratoVO.valorFixoNumDiasConsecParadaCliente == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">N�mero Dias Consecutivos de Paradas do Cliente:</td>
				<td class="campoTabela">
					<input id="numDiasConsecParadaCliente" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="numDiasConsecParadaCliente" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numDiasConsecParadaCliente}" />
					<label class="rotuloHorizontal rotuloInformativo" for="numDiasConsecParadaCliente">dias</label>
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Paradas Programadas CDL:</td>				
			</tr>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selNumAnosCtrlParadaCDL" name="selNumAnosCtrlParadaCDL" onclick="permitirSelecaoObrigatorio('selNumAnosCtrlParadaCDL', 'obgNumAnosCtrlParadaCDL');" <c:if test="${modeloContratoVO.selNumAnosCtrlParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgNumAnosCtrlParadaCDL" name="obgNumAnosCtrlParadaCDL" onclick="verificarSelecaoObrigatorio('obgNumAnosCtrlParadaCDL', 'selNumAnosCtrlParadaCDL');" <c:if test="${modeloContratoVO.obgNumAnosCtrlParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumAnosCtrlParadaCDL" name="valorFixoNumAnosCtrlParadaCDL" onclick="verificarSelecaoObrigatorio('valorFixoNumAnosCtrlParadaCDL', 'selNumAnosCtrlParadaCDL');" <c:if test="${modeloContratoVO.valorFixoNumAnosCtrlParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">N�mero de Anos para Controle de Paradas:</td>
				<td class="campoTabela">
					<input id="numAnosCtrlParadaCDL" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="numAnosCtrlParadaCDL" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numAnosCtrlParadaCDL}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="numAnosCtrlParadaCDL">anos</label>
				</td>
			</tr>
			<tr class="even semLinhaFilhoUltimo">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio3" name="obgMaxTotalParadasCDL" value="true" campoPai="numAnosCtrlParadaCDL" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoMaxTotalParadasCDL" name="valorFixoMaxTotalParadasCDL" campoPai="selNumAnosCtrlParadaCDL" <c:if test="${modeloContratoVO.valorFixoMaxTotalParadasCDL == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Limite M�ximo Total de Paradas:</td>
				<td class="campoTabela">
					<input id="maxTotalParadasCDL" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="maxTotalParadasCDL" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.maxTotalParadasCDL}" />
					<label class="rotuloHorizontal rotuloInformativo" for="maxTotalParadasCDL">dias</label>
				</td>
			</tr>						
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selMaxAnualParadasCDL" name="selMaxAnualParadasCDL" onclick="permitirSelecaoObrigatorio('selMaxAnualParadasCDL', 'obgMaxAnualParadasCDL');" <c:if test="${modeloContratoVO.selMaxAnualParadasCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgMaxAnualParadasCDL" name="obgMaxAnualParadasCDL" onclick="verificarSelecaoObrigatorio('obgMaxAnualParadasCDL', 'selMaxAnualParadasCDL');" <c:if test="${modeloContratoVO.obgMaxAnualParadasCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoMaxAnualParadasCDL" name="valorFixoMaxAnualParadasCDL" onclick="verificarSelecaoObrigatorio('valorFixoMaxAnualParadasCDL', 'selMaxAnualParadasCDL');" <c:if test="${modeloContratoVO.valorFixoMaxAnualParadasCDL == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Limite M�ximo Anual de Paradas:</td>
				<td class="campoTabela">
					<input id="maxAnualParadasCDL" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="maxAnualParadasCDL" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.maxAnualParadasCDL}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="maxAnualParadasCDL">dias</label>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selNumDiasProgrParadaCDL" name="selNumDiasProgrParadaCDL" onclick="permitirSelecaoObrigatorio('selNumDiasProgrParadaCDL', 'obgNumDiasProgrParadaCDL');" <c:if test="${modeloContratoVO.selNumDiasProgrParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgNumDiasProgrParadaCDL" name="obgNumDiasProgrParadaCDL" onclick="verificarSelecaoObrigatorio('obgNumDiasProgrParadaCDL', 'selNumDiasProgrParadaCDL');" <c:if test="${modeloContratoVO.obgNumDiasProgrParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumDiasProgrParadaCDL" name="valorFixoNumDiasProgrParadaCDL" onclick="verificarSelecaoObrigatorio('valorFixoNumDiasProgrParadaCDL', 'selNumDiasProgrParadaCDL');" <c:if test="${modeloContratoVO.valorFixoNumDiasProgrParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Anteced�ncia para Programa��o de Paradas:</td>
				<td class="campoTabela">
					<input id="numDiasProgrParadaCDL" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="numDiasProgrParadaCDL" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numDiasProgrParadaCDL}" />
					<label class="rotuloHorizontal rotuloInformativo" for="numDiasProgrParadaCDL">dias</label>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado3" type="checkbox" value="true" id="selNumDiasConsecParadaCDL" name="selNumDiasConsecParadaCDL" onclick="permitirSelecaoObrigatorio('selNumDiasConsecParadaCDL', 'obgNumDiasConsecParadaCDL');" <c:if test="${modeloContratoVO.selNumDiasConsecParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio3" type="checkbox" value="true" id="obgNumDiasConsecParadaCDL" name="obgNumDiasConsecParadaCDL" onclick="verificarSelecaoObrigatorio('obgNumDiasConsecParadaCDL', 'selNumDiasConsecParadaCDL');" <c:if test="${modeloContratoVO.obgNumDiasConsecParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumDiasConsecParadaCDL" name="valorFixoNumDiasConsecParadaCDL" onclick="verificarSelecaoObrigatorio('valorFixoNumDiasConsecParadaCDL', 'selNumDiasConsecParadaCDL');" <c:if test="${modeloContratoVO.valorFixoNumDiasConsecParadaCDL == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">N�mero de Dias Consecutivos de Paradas da CDL:</td>
				<td class="campoTabela">
					<input id="numDiasConsecParadaCDL" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="numDiasConsecParadaCDL" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numDiasConsecParadaCDL}" />
					<label class="rotuloHorizontal rotuloInformativo" for="numDiasConsecParadaCDL">dias</label>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>