<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fieldset id="Principais">
	<input name="selAbaPrincipais" type="hidden" id="selAbaPrincipais" value="true"/>
	<a class="linkHelp" href="<help:help>/abaprincipaisinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado</th>
				<th width="73px">Obrigat�rio</th>
				<th width="73px">Valor Fixo</th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Garantia Convers�o:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selNumeroDiasGarantia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumeroDiasGarantia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoNumeroDiasGarantia eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">N�mero de dias da Garantia:</td>
				<td class="campoTabela">
					<c:if test="${numeroDiasGarantia ne null && numeroDiasGarantia ne ''}">
						<span class="itemDetalhamento"><c:out value="${numeroDiasGarantia}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="numeroDiasGarantia">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selInicioGarantiaConversao eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgInicioGarantiaConversao eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">In�cio da Garantia de Convers�o:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:out value="${inicioGarantiaConversao}"/></span>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Dados para Testes:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selPeriodoTesteDataInicio eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodoTesteDataInicio eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">In�cio do per�odo:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${periodoTesteDataInicio}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPeriodoTesteDateFim eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodoTesteDateFim eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Fim do per�odo:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${periodoTesteDateFim}"/></span></td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selVolumeTeste eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgVolumeTeste eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Volume:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${volumeTeste}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPrazoTeste eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPrazoTeste eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Prazo:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${prazoTeste}"/></span></td>	
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Contato Operacional:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selFaxDDD eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFaxDDD eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">DDD:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${faxDDD}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selFaxNumero eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFaxNumero eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">N�mero:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${faxNumero}"/></span></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selEmail eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEmail eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">E-mail:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${email}"/></span></td>
			</tr>
		</tbody>
	</table>
</fieldset>