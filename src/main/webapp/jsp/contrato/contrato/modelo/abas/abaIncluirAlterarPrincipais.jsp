<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script>

	function controlarCheckBoxObrigatorios2(checkObrigatorios) {
		var valorCheck = checkObrigatorios.checked;

		var camposSelecionados = new Array('selNumeroDiasGarantia','selInicioGarantiaConversao','selPeriodoTesteDataInicio','obgPeriodoTesteDataInicio','obgPeriodoTesteDataInicio','selFaxDDD','selEmail');
		var camposObrigatorios = new Array('obgNumeroDiasGarantia','obgInicioGarantiaConversao','obgPeriodoTesteDataInicio','obgVolumeTeste','obgPrazoTeste','obgFaxDDD','obgEmail');

		marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, valorCheck);
	}
	
	function limparAbaPrincipais(){
		
	}
	
</script>

<fieldset id="Principais">
	<a class="linkHelp" href="<help:help>/abaprincipaisinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<input name="selAbaPrincipais" type="hidden" id="selAbaPrincipais" value="true"/>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado<input id="checkAllSelecionado2" type="checkbox" name="todos"/></th>
				<th width="73px">Obrigat�rio<input id="checkAllObrigatorio2" type="checkbox" name="todos" onclick="controlarCheckBoxObrigatorios2(this);"/></th>
				<th width="73px">Valor Fixo<input id="checkAllValorFixo2" type="checkbox" name="todos" onclick="controlarCheckBoxValorFixo(this)" /></th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Garantia Convers�o:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado2" type="checkbox" value="true" id="selNumeroDiasGarantia" name="selNumeroDiasGarantia" onclick="permitirSelecaoObrigatorio('selNumeroDiasGarantia', 'obgNumeroDiasGarantia');" <c:if test="${modeloContratoVO.selNumeroDiasGarantia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio2" type="checkbox" value="true" id="obgNumeroDiasGarantia" name="obgNumeroDiasGarantia" onclick="verificarSelecaoObrigatorio('obgNumeroDiasGarantia', 'selNumeroDiasGarantia');" <c:if test="${modeloContratoVO.obgNumeroDiasGarantia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumeroDiasGarantia" name="valorFixoNumeroDiasGarantia" onclick="verificarSelecaoObrigatorio('valorFixoNumeroDiasGarantia', 'selNumeroDiasGarantia');" <c:if test="${modeloContratoVO.valorFixoNumeroDiasGarantia == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">N�mero de dias da Garantia:</td>
				<td class="campoTabela">
					<input id="numeroDiasGarantia" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="numeroDiasGarantia" value="${modeloContratoVO.numeroDiasGarantia}" onkeypress="return formatarCampoInteiro(event)"/>
					<label class="rotuloHorizontal rotuloInformativo" for="numeroDiasGarantia">dias</label>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado2" type="checkbox" value="true" id="selInicioGarantiaConversao" name="selInicioGarantiaConversao" onclick="permitirSelecaoObrigatorio('selInicioGarantiaConversao', 'obgInicioGarantiaConversao');" <c:if test="${modeloContratoVO.selInicioGarantiaConversao == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio2" type="checkbox" value="true" id="obgInicioGarantiaConversao" name="obgInicioGarantiaConversao" onclick="verificarSelecaoObrigatorio('obgInicioGarantiaConversao', 'selInicioGarantiaConversao');" <c:if test="${modeloContratoVO.obgInicioGarantiaConversao == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">In�cio da Garantia de Convers�o:</td>
				<td class="campoTabela"></td>				
			</tr>	
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Dados para Testes:</td>				
			</tr>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado2" type="checkbox" value="true" id="selPeriodoTesteDataInicio" name="selPeriodoTesteDataInicio" onclick="permitirSelecaoObrigatorio('selPeriodoTesteDataInicio', 'obgPeriodoTesteDataInicio');" <c:if test="${modeloContratoVO.selPeriodoTesteDataInicio == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio2" type="checkbox" value="true" id="obgPeriodoTesteDataInicio" name="obgPeriodoTesteDataInicio" onclick="verificarSelecaoObrigatorio('obgPeriodoTesteDataInicio', 'selPeriodoTesteDataInicio');" <c:if test="${modeloContratoVO.obgPeriodoTesteDataInicio == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Inicio do per�odo:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio2" name="obgPeriodoTesteDateFim" value="true" campoPai="periodoTesteDataInicio" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Fim do per�odo:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio2" styleId="obgVolumeTeste" name="obgVolumeTeste" value="true" campoPai="periodoTesteDataInicio" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Volume:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio2" styleId="obgPrazoTeste" name="obgPrazoTeste" value="true" campoPai="periodoTesteDataInicio" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Prazo:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Contato Operacional:</td>				
			</tr>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado2" type="checkbox" value="true" id="selFaxDDD" name="selFaxDDD" onclick="permitirSelecaoObrigatorio('selFaxDDD', 'obgFaxDDD');" <c:if test="${modeloContratoVO.selFaxDDD == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio2" type="checkbox" value="true" id="obgFaxDDD" name="obgFaxDDD" onclick="verificarSelecaoObrigatorio('obgFaxDDD', 'selFaxDDD');" <c:if test="${modeloContratoVO.obgFaxDDD == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">DDD:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even semLinhaFilhoUltimo">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio2" name="obgFaxNumero" value="true" campoPai="faxDDD" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">N�mero:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado2" type="checkbox" value="true" id="selEmail" name="selEmail" onclick="permitirSelecaoObrigatorio('selEmail', 'obgEmail');" <c:if test="${modeloContratoVO.selEmail == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio2" type="checkbox" value="true" id="obgEmail" name="obgEmail" onclick="verificarSelecaoObrigatorio('obgEmail', 'selEmail');" <c:if test="${modeloContratoVO.obgEmail == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">E-mail:</td>
				<td class="campoTabela"></td>
			</tr>		
		</tbody>
	</table>
</fieldset>