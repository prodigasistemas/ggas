<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!--[if lt IE 10]>
    <style type="text/css">
    	.anoContratualMaiusculoCss{padding:7px 5px 0px 0px;}
    	.renovacaoAutomaticaCss{padding:7px 5px 0px 0px;}
    	.faturamentoAgrupamentoCss{padding:7px 5px 0px 0px;}
    	.faturamentoAgrupamentoCss{padding:5px 5px 0px 0px;}
    	.emissaoFaturaAgrupadaCss{padding:5px 5px 0px 0px;}
    	.indicadorMultaAtrasoCss{padding:5px 5px 0px 0px;}
    	.indicadorJurosMoraCss{padding:5px 5px 0px 0px;}
    </style>
<![endif]-->

<script>
	
	$(document).ready(function(){
		$("#diaVencFinanciamento").css("margin-left","0");
		
		var indicadorJurosMora = $('input[name="indicadorJurosMora"]:checked').val();
		var indicadorMultaAtraso = $('input[name="indicadorMultaAtraso"]:checked').val();
		habilitarDesabilitarCamposJurosMulta(indicadorJurosMora, "percentualJurosMora");
		habilitarDesabilitarCamposJurosMulta(indicadorMultaAtraso, "percentualMulta");
		
		$('#formaCobranca').change(function(){
			$('#arrecadadorConvenio').children('option:not(:first)').remove();
			AjaxService.obterConvenios( $('#formaCobranca :selected').val(),
				{callback: function(convenios) {
					var arrecadadorSelecionado = <c:out value="${modeloContratoVO.arrecadadorConvenio}"/> 
					$.each(convenios, function(key, value) {
						var selecionado = "";
						if (key == arrecadadorSelecionado){
							selecionado = "selected='selected'"
						}
						 $('#arrecadadorConvenio')
							 .append($("<option " + selecionado + "></option>")
							 .attr("value",key)
							 .text(value));
					});
				}, async:false}
			);
		});
		$('#formaCobranca').trigger('change');
	});
		
	function limparAbaGeral(){
		 
		document.getElementsByName('indicadorAnoContratual')[0].checked = true;		
		document.getElementsByName('renovacaoAutomatica')[1].checked = true;
		document.getElementsByName('faturamentoAgrupamento')[1].checked = true;
		document.getElementsByName('emissaoFaturaAgrupada')[1].checked = true;
		document.getElementsByName('indicadorMultaAtraso')[1].checked = true;
		document.getElementsByName('indicadorJurosMora')[1].checked = true;
		document.getElementById('numDiasRenoAutoContrato').value = '60';

	}

	function controlarCheckBoxObrigatorios1(checkObrigatorios) {
		
		var valorCheck = checkObrigatorios.checked;

		var camposSelecionados = new Array('selDataInicioRetiradaQPNRC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeTakeOrPayC','selPeriodicidadeRetMaiorMenor','selPenalidadeRetMaiorMenor','selArrecadadorConvenio','selValorInvestimento','selVolumeReferencia','selDataAssinatura','selDescricaoContrato','selNumeroAnteriorContrato','selNumeroEmpenho','selDataVencObrigacoesContratuais','selRenovacaoAutomatica','selEmissaoFaturaAgrupada','selQDCContrato','selValorContrato','selNumeroProposta','selGastoEstimadoGNMes','selEconomiaEstimadaGNMes','selEconomiaEstimadaGNAno','selDescontoEfetivoEconomia','selDiaVencFinanciamento','selValorParticipacaoCliente','selValorGarantiaFinanceira','selDescGarantiaFinanc','selTipoGarantiaFinanceira','selGarantiaFinanceiraRenovada','selIndicadorMultaAtraso','selIndicadorJurosMora','selIndiceCorrecaoMonetaria','selPercentualTarifaDoP','selValorInvestimento','selMultaRecisoria','selPercentualJurosMora','selPercentualMulta', 'selDataInicioRetiradaQPNRC');
		var camposObrigatorios = new Array('obgTempoValidadeRetiradaQPNRC','obgApuracaoFalhaFornecimentoC','obgApuracaoCasoFortuitoC','obgApuracaoParadaProgramadaC','obgPercentualNaoRecuperavelC','obgRecuperavelC','obgConsideraCasoFortuitoC','obgConsideraFalhaFornecimentoC','obgConsideraParadaProgramadaC','obgTipoApuracaoC','obgPrecoCobrancaC','obgDataFimVigenciaC','obgDataInicioVigenciaC','obgMargemVariacaoTakeOrPayC','obgConsumoReferenciaTakeOrPayC','obgReferenciaQFParadaProgramadaC','obgPeriodicidadeTakeOrPayC','obgPeriodicidadeRetMaiorMenor','obgPenalidadeRetMaiorMenor','obgArrecadadorConvenio','obgValorInvestimento','obgVolumeReferencia','obgDataAssinatura','obgDescricaoContrato','obgNumeroAnteriorContrato','obgNumeroEmpenho','obgDataVencObrigacoesContratuais','obgRenovacaoAutomatica','obgEmissaoFaturaAgrupada','obgQDCContrato','obgValorContrato','obgNumeroProposta','obgGastoEstimadoGNMes','obgEconomiaEstimadaGNMes','obgEconomiaEstimadaGNAno','obgDescontoEfetivoEconomia','obgDiaVencFinanciamento','obgValorParticipacaoCliente','obgValorGarantiaFinanceira','obgDescGarantiaFinanc','obgTipoGarantiaFinanceira','obgGarantiaFinanceiraRenovada','obgIndicadorMultaAtraso','obgIndicadorJurosMora','obgIndiceCorrecaoMonetaria','obgPercentualTarifaDoP','obgValorInvestimento','obgMultaRecisoria','obgPercentualJurosMora','obgPercentualMulta', 'obgDataInicioRetiradaQPNRC');
		
		if (valorCheck == true) {
			document.getElementById('obgDescricaoAditivo').checked = true;
		} else {
			document.getElementById('obgDescricaoAditivo').checked = false;
		}

		var camposObrigatoriosSemSelecionados = new Array('obgNumDiasRenoAutoContrato','obgTempoAntecedenciaRenovacao','obgQtdParcelasFinanciamento','obgPercentualJurosFinanciamento','obgSistemaAmortizacao','obgPercentualSobreTariGas');
		
		for (var i = 0; i < camposObrigatoriosSemSelecionados.length; i++) {
			var campoSelecionado = document.getElementById(camposObrigatoriosSemSelecionados[i]).checked;
			if(document.getElementById(camposObrigatoriosSemSelecionados[i]) != null){
				if (campoSelecionado != null && valorCheck == true) {
					document.getElementById(camposObrigatoriosSemSelecionados[i]).checked = true;
				} else {
					document.getElementById(camposObrigatoriosSemSelecionados[i]).checked = false;
				}			
			}
		}
		
		marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, valorCheck);
		
	}

	function habilitarDesabilitarCamposJurosMulta(valor, nomeCampo){
		if(valor == "true"){
			$("#" + nomeCampo).prop("disabled", false);
		}else{
			$("#" + nomeCampo).val("");
			$("#" + nomeCampo).prop("disabled", true);
		}
	}
	
</script>

<fieldset id="Geral">
	<a class="linkHelp" href="<help:help>/abageralinclusoalteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<input name="selAbaGeral" type="hidden" id="selAbaGeral" value="true"/>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado<input id="checkAllSelecionado1" type="checkbox" name="todos"/></th>
				<th width="73px">Obrigat�rio<input id="checkAllObrigatorio1" type="checkbox" name="todos" onclick="controlarCheckBoxObrigatorios1(this)"/></th>
				<th width="73px">Valor Fixo<input id="checkAllValorFixo1" type="checkbox" name="todos" onclick="controlarCheckBoxValorFixo(this)"/></th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even" style="vertical-align:middle; height:40px;">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selClienteAssinatura" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgClienteAssinatura" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Cliente Assinatura:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selPontoConsumo"/></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgPontoConsumo"/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Ponto de consumo:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Contrato:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selNumeroContrato" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgNumeroContrato" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">N�mero do Contrato:</td>
				<td class="campoTabela"></td>
			</tr>			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selNumeroAnteriorContrato" name="selNumeroAnteriorContrato" onclick="permitirSelecaoObrigatorio('selNumeroAnteriorContrato', 'obgNumeroAnteriorContrato');" <c:if test="${modeloContratoVO.selNumeroAnteriorContrato == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgNumeroAnteriorContrato" name="obgNumeroAnteriorContrato" onclick="verificarSelecaoObrigatorio('obgNumeroAnteriorContrato', 'selNumeroAnteriorContrato');" <c:if test="${modeloContratoVO.obgNumeroAnteriorContrato == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">N�mero Anterior do Contrato:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selDescricaoContrato" name="selDescricaoContrato" onclick="permitirSelecaoObrigatorio('selDescricaoContrato', 'obgDescricaoContrato');" <c:if test="${modeloContratoVO.selDescricaoContrato == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDescricaoContrato" name="obgDescricaoContrato" onclick="verificarSelecaoObrigatorio('obgDescricaoContrato', 'selDescricaoContrato');" <c:if test="${modeloContratoVO.obgDescricaoContrato == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Descri��o:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selSituacaoContrato" id="selSituacaoContrato" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgSituacaoContrato" id="obgSituacaoContrato" checked="true" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoSituacaoContrato" name="valorFixoSituacaoContrato" <c:if test="${modeloContratoVO.valorFixoSituacaoContrato == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Situa��o do Contrato:</td>
				<td class="campoTabela">
					<select name="situacaoContrato" id="situacaoContrato" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaSituacaoContrato}" var="situacaoContrato">
							<option value="<c:out value="${situacaoContrato.chavePrimaria}"/>" <c:if test="${modeloContratoVO.situacaoContrato eq situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${situacaoContrato.descricao}"/>
							</option>	
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input type="hidden" value="true" name="selExigeAprovacao"/></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input type="hidden" value="true" name="obgExigeAprovacao"/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Exige aprova��o?</td>
				<td class="campoTabela">
					<input id="exigeAprovacaoSim" class="campoRadio faturamentoAgrupamentoCss" type="radio" value="true" name="exigeAprovacao" <c:if test="${modeloContratoVO.exigeAprovacao == true}">checked</c:if>/>
					<label class="rotuloRadio" for="exigeAprovacaoSim">Sim</label>
					<input id="exigeAprovacaoNao" class="campoRadio faturamentoAgrupamentoCss" type="radio" value="false" name="exigeAprovacao" <c:if test="${modeloContratoVO.exigeAprovacao == false or empty modeloContratoVO.exigeAprovacao}">checked</c:if>/>
					<label class="rotuloRadio" for="exigeAprovacaoNao">N�o</label>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selNumeroAditivo"/></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgNumeroAditivo"/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">N�mero do Aditivo:</td>
				<td class="campoTabela"></td>
			</tr>		
			<tr class="even semLinhaPaiMeio">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selDataAditivo"/></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgDataAditivo"/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data do Aditivo:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd semLinhaFilhoUltimo">
				<td class="item-selecionado"><img alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgDescricaoAditivo" styleClass="checkObrigatorio1" name="obgDescricaoAditivo" value="true" campoPai="dataAditivo" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Descri��o do Aditivo:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" id="selDataAssinatura" name="selDataAssinatura" checked="true"/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDataAssinatura" name="obgDataAssinatura"  <c:if test="${modeloContratoVO.obgDataAssinatura == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data da Assinatura:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selNumeroEmpenho" name="selNumeroEmpenho" onclick="permitirSelecaoObrigatorio('selNumeroEmpenho', 'obgNumeroEmpenho');" <c:if test="${modeloContratoVO.selNumeroEmpenho == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgNumeroEmpenho" name="obgNumeroEmpenho" onclick="verificarSelecaoObrigatorio('obgNumeroEmpenho', 'selNumeroEmpenho');" <c:if test="${modeloContratoVO.obgNumeroEmpenho == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">N�mero do Empenho:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="even" style="vertical-align:moddle; height:40px;">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selDataVencObrigacoesContratuais" name="selDataVencObrigacoesContratuais" onclick="permitirSelecaoObrigatorio('selDataVencObrigacoesContratuais', 'obgDataVencObrigacoesContratuais');"<c:if test="${modeloContratoVO.selDataVencObrigacoesContratuais == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDataVencObrigacoesContratuais" name="obgDataVencObrigacoesContratuais" onclick="verificarSelecaoObrigatorio('obgDataVencObrigacoesContratuais', 'selDataVencObrigacoesContratuais');" <c:if test="${modeloContratoVO.obgDataVencObrigacoesContratuais == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data de Vencimento das Obriga��es Contratuais:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selIndicadorAnoContratual" id="selIndicadorAnoContratual" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgIndicadorAnoContratual" id="obgIndicadorAnoContratual" checked="true" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoIndicadorAnoContratual" name="valorFixoIndicadorAnoContratual" <c:if test="${modeloContratoVO.valorFixoIndicadorAnoContratual == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Defini��o de Ano Contratual:</td>
				<td class="campoTabela">
					<input id="anoContratualMaiusculo" class="campoRadio anoContratualMaiusculoCss" type="radio" value="true" name="indicadorAnoContratual" <c:if test="${modeloContratoVO.indicadorAnoContratual == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="anoContratualMaiusculo">ANO(Fiscal)</label>
					<input id="anoContratualMinusculo" class="campoRadio anoContratualMaiusculoCss" type="radio" value="false" name="indicadorAnoContratual" <c:if test="${modeloContratoVO.indicadorAnoContratual == false or empty modeloContratoVO.indicadorAnoContratual}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="anoContratualMinusculo">ano(Calend�rio)</label>
				</td>
			</tr>
			<tr class="even semLinhaPaiMeio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selRenovacaoAutomatica" name="selRenovacaoAutomatica" onclick="permitirSelecaoObrigatorio('selRenovacaoAutomatica', 'obgRenovacaoAutomatica');" <c:if test="${modeloContratoVO.selRenovacaoAutomatica == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgRenovacaoAutomatica" name="obgRenovacaoAutomatica" onclick="verificarSelecaoObrigatorio('obgRenovacaoAutomatica', 'selRenovacaoAutomatica');" <c:if test="${modeloContratoVO.obgRenovacaoAutomatica == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoRenovacaoAutomatica" name="valorFixoRenovacaoAutomatica" onclick="verificarSelecaoObrigatorio('valorFixoRenovacaoAutomatica', 'selRenovacaoAutomatica');" <c:if test="${modeloContratoVO.valorFixoRenovacaoAutomatica == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Renova��o Autom�tica?</td>
				<td class="campoTabela">
					<input id="renovacaoAutomaticaSim" class="campoRadio renovacaoAutomaticaCss" type="radio" value="true" name="renovacaoAutomatica" <c:if test="${modeloContratoVO.renovacaoAutomatica == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="renovacaoAutomaticaSim">Sim</label>
					<input id="renovacaoAutomaticaNao" class="campoRadio renovacaoAutomaticaCss" type="radio" value="false" name="renovacaoAutomatica" <c:if test="${modeloContratoVO.renovacaoAutomatica == false or empty modeloContratoVO.renovacaoAutomatica}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="renovacaoAutomaticaNao">N�o</label>
				</td>
			</tr>
			<tr class="odd semLinhaFilho">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgNumDiasRenoAutoContrato" styleClass="checkObrigatorio1" name="obgNumDiasRenoAutoContrato" value="true" campoPai="renovacaoAutomatica" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoNumDiasRenoAutoContrato" name="valorFixoNumDiasRenoAutoContrato" campoPai="selRenovacaoAutomatica" <c:if test="${modeloContratoVO.valorFixoNumDiasRenoAutoContrato == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Per�odo de Renova��o:</td>
				<td class="campoTabela">
					<input id="numDiasRenoAutoContrato" class="campoTexto campoHorizontal" type="text" size="4" maxlength="4" name="numDiasRenoAutoContrato" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numDiasRenoAutoContrato}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="pressaoManometrica">dias</label>
				</td>
			</tr>
			<tr class="even semLinhaFilhoUltimo">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgTempoAntecedenciaRenovacao" styleClass="checkObrigatorio1" name="obgTempoAntecedenciaRenovacao" value="true" campoPai="renovacaoAutomatica" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" id="valorFixoTempoAntecedenciaRenovacao" name="valorFixoTempoAntecedenciaRenovacao" campoPai="selRenovacaoAutomatica" <c:if test="${modeloContratoVO.valorFixoTempoAntecedenciaRenovacao == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tempo de Anteced�ncia para Negocia��o da Renova��o:</td>
				<td class="campoTabela">
					<input id="tempoAntecedenciaRenovacao" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="tempoAntecedenciaRenovacao" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.tempoAntecedenciaRenovacao}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="tempoAntecedenciaRenovacao">dias</label>
				</td>
			</tr>		
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selQDCContrato" name="selQDCContrato" onclick="permitirSelecaoObrigatorio('selQDCContrato', 'obgQDCContrato');" <c:if test="${modeloContratoVO.selQDCContrato == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgQDCContrato" name="obgQDCContrato" onclick="verificarSelecaoObrigatorio('obgQDCContrato', 'selQDCContrato');" <c:if test="${modeloContratoVO.obgQDCContrato == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">QDC do Contrato (sazonalidade):</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selValorContrato" name="selValorContrato" onclick="permitirSelecaoObrigatorio('selValorContrato', 'obgValorContrato');" <c:if test="${modeloContratoVO.selValorContrato == true}">checked="checked"</c:if> /></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgValorContrato" name="obgValorContrato" onclick="verificarSelecaoObrigatorio('obgValorContrato', 'selValorContrato');" <c:if test="${modeloContratoVO.obgValorContrato == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Valor do Contrato:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td>
					<img  alt="Selecionado" title="Selecionado" 
						src="<c:url value="/imagens/check2.gif"/>" border="0">
					<input class="checkSelecionado1" type="hidden" value="true" 
						name="selTipoPeriodicidadePenalidade" id="selTipoPeriodicidadePenalidade" checked="true" />
				</td>
				<td>
					<img  alt="Obrigat�rio" title="Obrigat�rio" 
						src="<c:url value="/imagens/check2.gif"/>" border="0">
					<input class="checkObrigatorio1" type="hidden" value="true" 
						name="obgTipoPeriodicidadePenalidade" id="obgTipoPeriodicidadePenalidade" checked="true" />
				</td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTipoPeriodicidadePenalidade" name="valorFixoTipoPeriodicidadePenalidade" <c:if test="${modeloContratoVO.valorFixoTipoPeriodicidadePenalidade == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tipo Periodicidade Penalidade:</td>
				<td class="campoTabela">
					<input id="tipoPeriodicidadePenalidadeCivil" class="campoRadio campoRadio3Linhas" 
						type="radio" value="true" name="tipoPeriodicidadePenalidade" 
						<c:if test="${modeloContratoVO.tipoPeriodicidadePenalidade == true}">
							checked="checked"
						</c:if>/>
					<label class="rotuloRadio" for="anoContratualMaiusculo">Civil</label>
					
					<input id="tipoPeriodicidadePenalidadeContinuo" class="campoRadio campoRadio3Linhas" 
						type="radio" value="false" name="tipoPeriodicidadePenalidade" 
						<c:if test="${modeloContratoVO.tipoPeriodicidadePenalidade == false or empty modeloContratoVO.tipoPeriodicidadePenalidade}">
							checked="checked"
						</c:if>/>
					<label class="rotuloRadio" for="anoContratualMinusculo">Cont�nuo</label>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selVolumeReferencia" name="selVolumeReferencia" onclick="permitirSelecaoObrigatorio('selVolumeReferencia', 'obgVolumeReferencia');" <c:if test="${modeloContratoVO.selVolumeReferencia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgVolumeReferencia" name="obgVolumeReferencia" onclick="verificarSelecaoObrigatorio('obgVolumeReferencia', 'selVolumeReferencia');" <c:if test="${modeloContratoVO.obgVolumeReferencia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Volume de Refer�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPrazoVigencia" name="selPrazoVigencia" onclick="permitirSelecaoObrigatorio('selPrazoVigencia', 'obgPrazoVigencia');" <c:if test="${modeloContratoVO.selPrazoVigencia == true}">checked="checked"</c:if> /></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPrazoVigencia" name="obgPrazoVigencia" onclick="verificarSelecaoObrigatorio('obgPrazoVigencia', 'selPrazoVigencia');" <c:if test="${modeloContratoVO.obgPrazoVigencia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Prazo Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selIncentivosComerciais" name="selIncentivosComerciais" onclick="permitirSelecaoObrigatorio('selIncentivosComerciais', 'obgIncentivosComerciais');" <c:if test="${modeloContratoVO.selIncentivosComerciais == true}">checked="checked"</c:if> /></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgIncentivosComerciais" name="obgIncentivosComerciais" onclick="verificarSelecaoObrigatorio('obgIncentivosComerciais', 'selIncentivosComerciais');" <c:if test="${modeloContratoVO.obgIncentivosComerciais == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Incentivos Comerciais:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selIncentivosInfraestrutura" name="selIncentivosInfraestrutura" onclick="permitirSelecaoObrigatorio('selIncentivosInfraestrutura', 'obgIncentivosInfraestrutura');" <c:if test="${modeloContratoVO.selIncentivosInfraestrutura == true}">checked="checked"</c:if> /></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgIncentivosInfraestrutura" name="obgIncentivosInfraestrutura" onclick="verificarSelecaoObrigatorio('obgIncentivosInfraestrutura', 'selIncentivosInfraestrutura');" <c:if test="${modeloContratoVO.obgIncentivosInfraestrutura == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Incentivos Infraestrutura:</td>
				<td class="campoTabela"></td>
			</tr>												
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Documentos Anexos:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selDocumentosAnexos" name="selDocumentosAnexos" <c:if test="${modeloContratoVO.selDocumentosAnexos == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDocumentosAnexos" name="obgDocumentosAnexos" <c:if test="${modeloContratoVO.obgDocumentosAnexos== true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Adicionar anexos</td>
				<td class="campoTabela"></td>
			</tr>			
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Proposta:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selNumeroProposta" name="selNumeroProposta" onclick="permitirSelecaoObrigatorio('selNumeroProposta', 'obgNumeroProposta');" <c:if test="${modeloContratoVO.selNumeroProposta == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgNumeroProposta" name="obgNumeroProposta" onclick="verificarSelecaoObrigatorio('obgNumeroProposta', 'selNumeroProposta');" <c:if test="${modeloContratoVO.obgNumeroProposta == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">N�mero da Proposta:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"></td>
				<td class="item-selecionado"></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Exige aprova��o pr�via da proposta?</td>
				<td class="campoTabela">
					<input id="propostaAprovadaSim" class="campoRadio renovacaoAutomaticaCss" type="radio" value="true" name="propostaAprovada" <c:if test="${modeloContratoVO.propostaAprovada == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="renovacaoAutomaticaSim">Sim</label>
					<input id="propostaAprovadaNao" class="campoRadio renovacaoAutomaticaCss" type="radio" value="false" name="propostaAprovada" <c:if test="${modeloContratoVO.propostaAprovada == false or empty modeloContratoVO.propostaAprovada}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="renovacaoAutomaticaNao">N�o</label>
				</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selGastoEstimadoGNMes" name="selGastoEstimadoGNMes" onclick="permitirSelecaoObrigatorio('selGastoEstimadoGNMes', 'obgGastoEstimadoGNMes');" <c:if test="${modeloContratoVO.selGastoEstimadoGNMes == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgGastoEstimadoGNMes" name="obgGastoEstimadoGNMes" onclick="verificarSelecaoObrigatorio('obgGastoEstimadoGNMes', 'selGastoEstimadoGNMes');" <c:if test="${modeloContratoVO.obgGastoEstimadoGNMes == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Gasto Mensal com GN:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selEconomiaEstimadaGNMes" name="selEconomiaEstimadaGNMes" onclick="permitirSelecaoObrigatorio('selEconomiaEstimadaGNMes', 'obgEconomiaEstimadaGNMes');" <c:if test="${modeloContratoVO.selEconomiaEstimadaGNMes == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgEconomiaEstimadaGNMes" name="obgEconomiaEstimadaGNMes" onclick="verificarSelecaoObrigatorio('obgEconomiaEstimadaGNMes', 'selEconomiaEstimadaGNMes');" <c:if test="${modeloContratoVO.obgEconomiaEstimadaGNMes == true}">checked="checked"</c:if>/></td>
				<td class="campoTabela"></td>
				<td class="rotuloTabela">Economia Mensal com GN:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selEconomiaEstimadaGNAno" name="selEconomiaEstimadaGNAno" onclick="permitirSelecaoObrigatorio('selEconomiaEstimadaGNAno', 'obgEconomiaEstimadaGNAno');" <c:if test="${modeloContratoVO.selEconomiaEstimadaGNAno == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgEconomiaEstimadaGNAno" name="obgEconomiaEstimadaGNAno" onclick="verificarSelecaoObrigatorio('obgEconomiaEstimadaGNAno', 'selEconomiaEstimadaGNAno');" <c:if test="${modeloContratoVO.obgEconomiaEstimadaGNAno == true}">checked="checked"</c:if>/></td>
				<td class="campoTabela"></td>
				<td class="rotuloTabela">Economia Anual com GN:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selDescontoEfetivoEconomia" name="selDescontoEfetivoEconomia" onclick="permitirSelecaoObrigatorio('selDescontoEfetivoEconomia', 'obgDescontoEfetivoEconomia');" <c:if test="${modeloContratoVO.selDescontoEfetivoEconomia == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDescontoEfetivoEconomia" name="obgDescontoEfetivoEconomia" onclick="verificarSelecaoObrigatorio('obgDescontoEfetivoEconomia', 'selDescontoEfetivoEconomia');" <c:if test="${modeloContratoVO.obgDescontoEfetivoEconomia == true}">checked="checked"</c:if>/></td>
				<td class="campoTabela"></td>
				<td class="rotuloTabela">Desconto Efetivo na Economia:</td>
				<td class="campoTabela"></td>
			</tr>			
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selDiaVencFinanciamento" name="selDiaVencFinanciamento" onclick="permitirSelecaoObrigatorio('selDiaVencFinanciamento', 'obgDiaVencFinanciamento');" <c:if test="${modeloContratoVO.selDiaVencFinanciamento == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDiaVencFinanciamento" name="obgDiaVencFinanciamento" onclick="verificarSelecaoObrigatorio('obgDiaVencFinanciamento', 'selDiaVencFinanciamento');" <c:if test="${modeloContratoVO.obgDiaVencFinanciamento == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDiaVencFinanciamento" name="valorFixoDiaVencFinanciamento" onclick="verificarSelecaoObrigatorio('valorFixoDiaVencFinanciamento', 'selDiaVencFinanciamento');" <c:if test="${modeloContratoVO.valorFixoDiaVencFinanciamento == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Dia de vencimento do financiamento:</td>
				<td class="campoTabela">					
					<select id="diaVencFinanciamento" class="campoTexto campoHorizontal" name="diaVencFinanciamento">
						<option value="-1">Selecione</option>
					  	<c:forEach begin="1" end="31" var="dia">
							<option value="${dia}" <c:if test="${modeloContratoVO.diaVencFinanciamento == dia}">selected="selected"</c:if>>${dia}</option>
					  	</c:forEach>
					</select>
					<label class="rotuloInformativo" for="diaVencFinanciamento">(01-31)</label>
				</td>
			</tr>
			
			<tr class="odd semLinhaPaiMeio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selValorInvestimento" name="selValorInvestimento" onclick="permitirSelecaoObrigatorio('selValorInvestimento', 'obgValorInvestimento');" <c:if test="${modeloContratoVO.selValorInvestimento == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgValorInvestimento" name="obgValorInvestimento" onclick="verificarSelecaoObrigatorio('obgValorInvestimento', 'selValorInvestimento');" <c:if test="${modeloContratoVO.obgValorInvestimento == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Valor do Investimento:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="odd semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente"  title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgDataInvestimento" styleClass="checkObrigatorio1" name="obgDataInvestimento" value="true" campoPai="valorInvestimento" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data do Investimento:</td>
				<td class="campoTabela"></td>
			</tr>
					 	
			<tr class="even semLinhaPaiMeio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selValorParticipacaoCliente" name="selValorParticipacaoCliente" onclick="permitirSelecaoObrigatorio('selValorParticipacaoCliente', 'obgValorParticipacaoCliente');" <c:if test="${modeloContratoVO.selValorParticipacaoCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgValorParticipacaoCliente" name="obgValorParticipacaoCliente" onclick="verificarSelecaoObrigatorio('obgValorParticipacaoCliente', 'selValorParticipacaoCliente');" <c:if test="${modeloContratoVO.obgValorParticipacaoCliente == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Valor da Participa��o do Cliente:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente"  title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgQtdParcelasFinanciamento" styleClass="checkObrigatorio1" name="obgQtdParcelasFinanciamento" value="true" campoPai="valorParticipacaoCliente" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Quantidade de Parcelas do Financiamento:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente"  title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgPercentualJurosFinanciamento" styleClass="checkObrigatorio1" name="obgPercentualJurosFinanciamento" value="true" campoPai="valorParticipacaoCliente" /></td>
				<td class="campoTabela"></td>
				<td class="rotuloTabela">Percentual de Juros do Financiamento:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente"  title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgSistemaAmortizacao" styleClass="checkObrigatorio1" name="obgSistemaAmortizacao" value="true" campoPai="valorParticipacaoCliente" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Sistema de Amortiza��o:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Cobran�a:</td>				
			</tr>
			<tr class="even semLinhaPaiMeio">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selFaturamentoAgrupamento" id="selFaturamentoAgrupamento" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgFaturamentoAgrupamento" id="obgFaturamentoAgrupamento" checked="true" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFaturamentoAgrupamento" name="valorFixoFaturamentoAgrupamento" <c:if test="${modeloContratoVO.valorFixoFaturamentoAgrupamento== true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Faturamento Agrupado?</td>
				<td class="campoTabela">
					<input id="faturamentoAgrupamentoSim" class="campoRadio faturamentoAgrupamentoCss" type="radio" value="true" name="faturamentoAgrupamento" <c:if test="${modeloContratoVO.faturamentoAgrupamento == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="faturamentoAgrupamentoSim">Sim</label>
					<input id="faturamentoAgrupamentoNao" class="campoRadio faturamentoAgrupamentoCss" type="radio" value="false" name="faturamentoAgrupamento" <c:if test="${modeloContratoVO.faturamentoAgrupamento == false or empty modeloContratoVO.faturamentoAgrupamento}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="faturamentoAgrupamentoNao">N�o</label>
				</td>
			</tr>
			
			<tr class="even semLinhaFilhoUltimo">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgTipoAgrupamento" styleClass="checkObrigatorio1" name="obgTipoAgrupamento" value="true" campoPai="faturamentoAgrupamento" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTipoAgrupamento" name="valorFixoTipoAgrupamento" campoPai="selFaturamentoAgrupamento" <c:if test="${modeloContratoVO.valorFixoTipoAgrupamento == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tipo de Agrupamento:</td>
				<td class="campoTabela">
					<select name="tipoAgrupamento" id="tipoAgrupamento" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoAgrupamento}" var="tipoAgrupamento">
							<option value="<c:out value="${tipoAgrupamento.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoAgrupamento eq tipoAgrupamento.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoAgrupamento.descricao}"/>
							</option>	
						</c:forEach>
					</select>
				</td>
			</tr>		
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selEmissaoFaturaAgrupada" name="selEmissaoFaturaAgrupada" onclick="permitirSelecaoObrigatorio('selEmissaoFaturaAgrupada', 'obgEmissaoFaturaAgrupada');" <c:if test="${modeloContratoVO.selEmissaoFaturaAgrupada == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgEmissaoFaturaAgrupada" name="obgEmissaoFaturaAgrupada" onclick="verificarSelecaoObrigatorio('obgEmissaoFaturaAgrupada', 'selEmissaoFaturaAgrupada');" <c:if test="${modeloContratoVO.obgEmissaoFaturaAgrupada == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoEmissaoFaturaAgrupada" name="valorFixoEmissaoFaturaAgrupada" onclick="verificarSelecaoObrigatorio('valorFixoEmissaoFaturaAgrupada', 'selEmissaoFaturaAgrupada');" <c:if test="${modeloContratoVO.valorFixoEmissaoFaturaAgrupada == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Emiss�o de Fatura Agrupada?</td>
				<td class="campoTabela">
					<input id="emissaoFaturaAgrupadaSim" class="campoRadio emissaoFaturaAgrupadaCss" type="radio" value="true" name="emissaoFaturaAgrupada" <c:if test="${modeloContratoVO.emissaoFaturaAgrupada == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="emissaoFaturaAgrupadaSim">Sim</label>
					<input id="emissaoFaturaAgrupadaNao" class="campoRadio emissaoFaturaAgrupadaCss" type="radio" value="false" name="emissaoFaturaAgrupada" <c:if test="${modeloContratoVO.emissaoFaturaAgrupada == false or empty modeloContratoVO.emissaoFaturaAgrupada}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="emissaoFaturaAgrupadaNao">N�o</label>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selFormaCobranca" id="selFormaCobranca" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgFormaCobranca" id="obgFormaCobranca" checked="true" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFormaCobranca" name="valorFixoFormaCobranca" <c:if test="${modeloContratoVO.valorFixoFormaCobranca == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tipo de Conv�nio:</td>
				<td class="campoTabela">
					<select id="formaCobranca" class="campoSelect" name="formaCobranca">
						<c:forEach items="${listaFormaCobranca}" var="formaCobranca">
							<option value="<c:out value="${formaCobranca.chavePrimaria}"/>" <c:if test="${modeloContratoVO.formaCobranca == formaCobranca.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${formaCobranca.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selArrecadadorConvenio" checked="true" id="selArrecadadorConvenio" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgArrecadadorConvenio" id="obgArrecadadorConvenio" checked="true" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoArrecadadorConvenio" name="valorFixoArrecadadorConvenio" <c:if test="${modeloContratoVO.valorFixoArrecadadorConvenio == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Arrecadador Conv�nio:</td>
				<td class="campoTabela">
					<select id="arrecadadorConvenio" class="campoSelect" name="arrecadadorConvenio">
					<option value="-1">Selecione</option>
						<c:forEach items="${listaArrecadadorConvenio}" var="arrecadadorConvenio">
							<option tipo="<c:out value="${arrecadadorConvenio.tipoConvenio.chavePrimaria}"/>" value="<c:out value="${arrecadadorConvenio.chavePrimaria}"/>" <c:if test="${modeloContratoVO.arrecadadorConvenio == arrecadadorConvenio.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${arrecadadorConvenio.codigoConvenio} - ${fn:toUpperCase(arrecadadorConvenio.arrecadadorCarteiraCobranca.tipoCarteira.descricao) == 'SEM REGISTRO' ? 'CN' : 'CR'} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selDebitoAutomatico" id="selDebitoAutomatico" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgDebitoAutomatico" id="obgDebitoAutomatico" checked="true" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDebitoAutomatico" name="valorFixoDebitoAutomatico" onclick="verificarSelecaoObrigatorio('valorFixoDebitoAutomatico', 'selDebitoAutomatico');" <c:if test="${modeloContratoVO.valorFixoDebitoAutomatico == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">D�bito Autom�tico?</td>
				<td class="campoTabela">
					<input id="debitoAutomaticoSim" class="campoRadio faturamentoAgrupamentoCss" type="radio" value="true" name="debitoAutomatico" <c:if test="${modeloContratoVO.debitoAutomatico == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="debitoAutomaticoSim">Sim</label>
					<input id="debitoAutomaticoNao" class="campoRadio faturamentoAgrupamentoCss" type="radio" value="false" name="debitoAutomatico" <c:if test="${modeloContratoVO.debitoAutomatico == false or empty modeloContratoVO.debitoAutomatico}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="debitoAutomaticoNao">N�o</label>
				</td>
			</tr>		
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>

				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgArrecadadorConvenioDebitoAutomatico" styleClass="checkObrigatorio1" name="obgArrecadadorConvenioDebitoAutomatico" value="true" campoPai="debitoAutomatico" /></td>

				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoArrecadadorConvenioDebitoAutomatico" name="valorFixoArrecadadorConvenioDebitoAutomatico" campoPai="selDebitoAutomatico" <c:if test="${modeloContratoVO.valorFixoArrecadadorConvenioDebitoAutomatico == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Arrecadador Conv�nio para D�bito Autom�tico:</td>
				<td class="campoTabela">
					<select id="arrecadadorConvenioDebitoAutomatico" class="campoSelect" name="arrecadadorConvenioDebitoAutomatico">
					<option value="-1">Selecione</option>
						<c:forEach items="${listaArrecadadorConvenioDebitoAutomatico}" var="arrecadadorConvenioDebitoAutomatico">
							<option value="<c:out value="${arrecadadorConvenioDebitoAutomatico.chavePrimaria}"/>" <c:if test="${modeloContratoVO.arrecadadorConvenioDebitoAutomatico == arrecadadorConvenioDebitoAutomatico.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${arrecadadorConvenioDebitoAutomatico.codigoConvenio} - ${arrecadadorConvenioDebitoAutomatico.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>

			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente"  title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgBanco" styleClass="checkObrigatorio1" name="obgBanco" value="true" campoPai="debitoAutomatico" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Banco:</td>
				<td class="campoTabela">
					<select id="banco" class="campoSelect" name="banco">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaBancos}" var="banco">
							<option value="<c:out value="${banco.chavePrimaria}"/>" <c:if test="${modeloContratoVO.banco == banco.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${banco.codigoBanco} - ${banco.nome}" />
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>

			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente"  title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgAgencia" styleClass="checkObrigatorio1" name="obgAgencia" value="true" campoPai="obgBanco" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Ag�ncia:</td>
				<td class="campoTabela"><input id="agencia" class="campoTexto campoHorizontal" type="text" size="10" maxlength="10"  name="agencia" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${modeloContratoVO.agencia}"/></td>
			</tr>

			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente"  title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgContaCorrente" styleClass="checkObrigatorio1" name="obgContaCorrente" value="true" campoPai="obgAgencia" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Conta corrente:</td>
				<td class="campoTabela"><input id="contaCorrente" class="campoTexto campoHorizontal" type="text" size="35" maxlength="10"  name="contaCorrente" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${modeloContratoVO.contaCorrente}"/></td>
			</tr>

			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selParticipaECartas" id="selParticipaECartas" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgParticipaECartas" id="obgParticipaECartas" checked="true" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoParticipaECartas" name="valorFixoParticipaECartas" <c:if test="${modeloContratoVO.valorFixoParticipaECartas == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Participa do conv�nio e-cartas?</td>
				<td class="campoTabela">
					<input id="participaECartasSim" class="campoRadio emissaoFaturaAgrupadaCss" type="radio" value="true" name="participaECartas" <c:if test="${modeloContratoVO.participaECartas == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="participaECartasSim">Sim</label>
					<input id="participaECartasNao" class="campoRadio emissaoFaturaAgrupadaCss" type="radio" value="false" name="participaECartas" <c:if test="${modeloContratoVO.participaECartas != true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="participaECartasNao">N�o</label>
				</td>
			</tr>

			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Garantia Financeira:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selValorGarantiaFinanceira" name="selValorGarantiaFinanceira" onclick="permitirSelecaoObrigatorio('selValorGarantiaFinanceira', 'obgValorGarantiaFinanceira');" <c:if test="${modeloContratoVO.selValorGarantiaFinanceira == true}">checked="checked"</c:if> /></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgValorGarantiaFinanceira" name="obgValorGarantiaFinanceira" onclick="verificarSelecaoObrigatorio('obgValorGarantiaFinanceira', 'selValorGarantiaFinanceira');" <c:if test="${modeloContratoVO.obgValorGarantiaFinanceira == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoValorGarantiaFinanceira" name="valorFixoValorGarantiaFinanceira" onclick="verificarSelecaoObrigatorio('valorFixoValorGarantiaFinanceira', 'selValorGarantiaFinanceira');" <c:if test="${modeloContratoVO.valorFixoValorGarantiaFinanceira == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Valor da Garantia Financeira:</td>
				<td class="campoTabela"><input id="valorGarantiaFinanceira" class="campoTexto campoValorReal" type="text" size="15" maxlength="14" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"  name="valorGarantiaFinanceira" value="${modeloContratoVO.valorGarantiaFinanceira}"/></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selDescGarantiaFinanc" name="selDescGarantiaFinanc" onclick="permitirSelecaoObrigatorio('selDescGarantiaFinanc', 'obgDescGarantiaFinanc');" <c:if test="${modeloContratoVO.selDescGarantiaFinanc == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDescGarantiaFinanc" name="obgDescGarantiaFinanc" onclick="verificarSelecaoObrigatorio('obgDescGarantiaFinanc', 'selDescGarantiaFinanc');" <c:if test="${modeloContratoVO.obgDescGarantiaFinanc == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDescGarantiaFinanc" name="valorFixoDescGarantiaFinanc" onclick="verificarSelecaoObrigatorio('valorFixoDescGarantiaFinanc', 'selDescGarantiaFinanc');" <c:if test="${modeloContratoVO.valorFixoDescGarantiaFinanc == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Descri��o da Garantia Financeira:</td>
				<td class="campoTabela"><input id="descGarantiaFinanc" class="campoTexto campoHorizontal" type="text" size="35" maxlength="40"  name="descGarantiaFinanc" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${modeloContratoVO.descGarantiaFinanc}"/></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selTipoGarantiaFinanceira" name="selTipoGarantiaFinanceira" onclick="permitirSelecaoObrigatorio('selTipoGarantiaFinanceira', 'obgTipoGarantiaFinanceira');" <c:if test="${modeloContratoVO.selTipoGarantiaFinanceira == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgTipoGarantiaFinanceira" name="obgTipoGarantiaFinanceira" onclick="verificarSelecaoObrigatorio('obgTipoGarantiaFinanceira', 'selTipoGarantiaFinanceira');" <c:if test="${modeloContratoVO.obgTipoGarantiaFinanceira == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTipoGarantiaFinanceira" name="valorFixoTipoGarantiaFinanceira" onclick="verificarSelecaoObrigatorio('valorFixoTipoGarantiaFinanceira', 'selTipoGarantiaFinanceira');" <c:if test="${modeloContratoVO.valorFixoTipoGarantiaFinanceira == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tipo da Garantia Financeira:</td>
				<td class="campoTabela">
					<select name="tipoGarantiaFinanceira" id="tipoGarantiaFinanceira" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoGarantiaFinanceira}" var="tipoGarantiaFinanceira">
							<option value="<c:out value="${tipoGarantiaFinanceira.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoGarantiaFinanceira == tipoGarantiaFinanceira.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoGarantiaFinanceira.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="odd semLinhaPaiMeio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selGarantiaFinanceiraRenovada" name="selGarantiaFinanceiraRenovada" onclick="permitirSelecaoObrigatorio('selGarantiaFinanceiraRenovada', 'obgGarantiaFinanceiraRenovada');" <c:if test="${modeloContratoVO.selGarantiaFinanceiraRenovada == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgGarantiaFinanceiraRenovada" name="obgGarantiaFinanceiraRenovada" onclick="verificarSelecaoObrigatorio('obgGarantiaFinanceiraRenovada', 'selGarantiaFinanceiraRenovada');" <c:if test="${modeloContratoVO.obgGarantiaFinanceiraRenovada == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoGarantiaFinanceiraRenovada" name="valorFixoGarantiaFinanceiraRenovada" onclick="verificarSelecaoObrigatorio('valorFixoGarantiaFinanceiraRenovada', 'selGarantiaFinanceiraRenovada');" <c:if test="${modeloContratoVO.valorFixoGarantiaFinanceiraRenovada == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">A garantia �?</td>
				<td class="campoTabela">
				
					<select name="garantiaFinanceiraRenovada" id="garantiaFinanceiraRenovada" class="campoSelect" >
						<option value="-1">Selecione</option>
						<c:forEach items="${listaGarantiaFinanceiraRenovada}" var="garantiaFinanceiraRenovada" >
							<option value="<c:out value="${garantiaFinanceiraRenovada}"/>" <c:if test="${modeloContratoVO.garantiaFinanceiraRenovada == garantiaFinanceiraRenovada}">selected="selected"</c:if>>
								<c:out value="${garantiaFinanceiraRenovada}"/>
							</option>		
						</c:forEach>
					</select>

				</td>				
			</tr>
			<tr class="odd semLinhaFilho">
				<td class="item-selecionado"><img alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgDataInicioGarantiaFinanceira" styleClass="checkObrigatorio1" name="obgDataInicioGarantiaFinanceira" value="true" campoPai="garantiaFinanceiraRenovada" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data de In�cio da Garantia Financeira:</td>
				<td class="campoTabela"></td>				
			</tr>
			<tr class="odd semLinhaFilho">
				<td class="item-selecionado"><img alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgDataFinalGarantiaFinanceira" styleClass="checkObrigatorio1" name="obgDataFinalGarantiaFinanceira" value="true" campoPai="garantiaFinanceiraRenovada" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data Final da Garantia Financeira:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgTempoAntecRevisaoGarantias" styleClass="checkObrigatorio1" name="obgTempoAntecRevisaoGarantias" value="true" campoPai="garantiaFinanceiraRenovada" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTempoAntecRevisaoGarantias" name="valorFixoTempoAntecRevisaoGarantias" campoPai="selGarantiaFinanceiraRenovada" <c:if test="${modeloContratoVO.valorFixoTempoAntecRevisaoGarantias == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tempo de Anteced�ncia para Revis�o das Garantias:</td>
				<td class="campoTabela">
					<input id="tempoAntecRevisaoGarantias" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="tempoAntecRevisaoGarantias" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.tempoAntecRevisaoGarantias}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="tempoAntecRevisaoGarantias">dias</label>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgPeriodicidadeReavGarantias" styleClass="checkObrigatorio1" name="obgPeriodicidadeReavGarantias" value="true" campoPai="garantiaFinanceiraRenovada" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPeriodicidadeReavGarantias" name="valorFixoPeriodicidadeReavGarantias" campoPai="selGarantiaFinanceiraRenovada" <c:if test="${modeloContratoVO.valorFixoPeriodicidadeReavGarantias == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Periodicidade para reavalia��o das Garantias:</td>
				<td class="campoTabela">
					<input id="periodicidadeReavGarantias" class="campoTexto campoHorizontal" type="text" size="2" maxlength="3" name="periodicidadeReavGarantias" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.periodicidadeReavGarantias}"/>
					<label class="rotuloHorizontal rotuloInformativo" for="periodicidadeReavGarantias">dias</label>
				</td>
			</tr>				
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Pagamento Efetuado em Atraso:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selIndicadorMultaAtraso" name="selIndicadorMultaAtraso" onclick="permitirSelecaoObrigatorio('selIndicadorMultaAtraso', 'obgIndicadorMultaAtraso');" <c:if test="${modeloContratoVO.selIndicadorMultaAtraso == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgIndicadorMultaAtraso" name="obgIndicadorMultaAtraso" onclick="verificarSelecaoObrigatorio('obgIndicadorMultaAtraso', 'selIndicadorMultaAtraso');" <c:if test="${modeloContratoVO.obgIndicadorMultaAtraso == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoIndicadorMultaAtraso" name="valorFixoIndicadorMultaAtraso" onclick="verificarSelecaoObrigatorio('valorFixoIndicadorMultaAtraso', 'selIndicadorMultaAtraso');" <c:if test="${modeloContratoVO.valorFixoIndicadorMultaAtraso == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Multa por Atraso?</td>
				<td class="campoTabela">
					<input id="indicadorMultaAtrasoSim" class="campoRadio indicadorMultaAtrasoCss" type="radio" value="true" name="indicadorMultaAtraso" onclick="habilitarDesabilitarCamposJurosMulta(this.value, 'percentualMulta');" <c:if test="${modeloContratoVO.indicadorMultaAtraso == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="indicadorMultaAtrasoSim">Sim</label>
					<input id="indicadorMultaAtrasoNao" class="campoRadio indicadorMultaAtrasoCss" type="radio" value="false" name="indicadorMultaAtraso" onclick="habilitarDesabilitarCamposJurosMulta(this.value, 'percentualMulta');" <c:if test="${modeloContratoVO.indicadorMultaAtraso == false or empty modeloContratoVO.indicadorMultaAtraso}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="indicadorMultaAtrasoNao">N�o</label>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPercentualMulta" name="selPercentualMulta" onclick="permitirSelecaoObrigatorio('selPercentualMulta', 'obgPercentualMulta');" <c:if test="${modeloContratoVO.selPercentualMulta == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPercentualMulta" name="obgPercentualMulta" onclick="verificarSelecaoObrigatorio('obgPercentualMulta', 'selPercentualMulta');" <c:if test="${modeloContratoVO.obgPercentualMulta == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualMulta" name="valorFixoPercentualMulta" onclick="verificarSelecaoObrigatorio('valorFixoPercentualMulta', 'selPercentualMulta');" <c:if test="${modeloContratoVO.valorFixoPercentualMulta == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual Multa por Atraso:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text" id="percentualMulta" name="percentualMulta" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.percentualMulta}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualMulta">%</label>				
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selIndicadorJurosMora" name="selIndicadorJurosMora" onclick="permitirSelecaoObrigatorio('selIndicadorJurosMora', 'obgIndicadorJurosMora');" <c:if test="${modeloContratoVO.selIndicadorJurosMora == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgIndicadorJurosMora" name="obgIndicadorJurosMora" onclick="verificarSelecaoObrigatorio('obgIndicadorJurosMora', 'selIndicadorJurosMora');" <c:if test="${modeloContratoVO.obgIndicadorJurosMora == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoIndicadorJurosMora" name="valorFixoIndicadorJurosMora" onclick="verificarSelecaoObrigatorio('valorFixoIndicadorJurosMora', 'selIndicadorJurosMora');" <c:if test="${modeloContratoVO.valorFixoIndicadorJurosMora == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Juros de Mora?</td>
				<td class="campoTabela">
					<input id="indicadorJurosMoraSim" class="campoRadio indicadorJurosMoraCss" type="radio" value="true" name="indicadorJurosMora" onclick="habilitarDesabilitarCamposJurosMulta(this.value, 'percentualJurosMora');" <c:if test="${modeloContratoVO.indicadorJurosMora == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="indicadorJurosMoraSim">Sim</label>
					<input id="indicadorJurosMoraNao" class="campoRadio indicadorJurosMoraCss" type="radio" value="false" name="indicadorJurosMora" onclick="habilitarDesabilitarCamposJurosMulta(this.value, 'percentualJurosMora');" <c:if test="${modeloContratoVO.indicadorJurosMora == false or empty modeloContratoVO.indicadorJurosMora}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="indicadorJurosMoraNao">N�o</label>
				</td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPercentualJurosMora" name="selPercentualJurosMora" onclick="permitirSelecaoObrigatorio('selPercentualJurosMora', 'obgPercentualJurosMora');" <c:if test="${modeloContratoVO.selPercentualJurosMora == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPercentualJurosMora" name="obgPercentualJurosMora" onclick="verificarSelecaoObrigatorio('obgPercentualJurosMora', 'selPercentualJurosMora');" <c:if test="${modeloContratoVO.obgPercentualJurosMora == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualJurosMora" name="valorFixoPercentualJurosMora" onclick="verificarSelecaoObrigatorio('valorFixoPercentualJurosMora', 'selPercentualJurosMora');" <c:if test="${modeloContratoVO.valorFixoPercentualJurosMora == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual Juros de Mora:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text" id="percentualJurosMora" name="percentualJurosMora" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.percentualJurosMora}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualJurosMora">%</label>				
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selIndiceCorrecaoMonetaria" name="selIndiceCorrecaoMonetaria" onclick="permitirSelecaoObrigatorio('selIndiceCorrecaoMonetaria', 'obgIndiceCorrecaoMonetaria');" <c:if test="${modeloContratoVO.selIndiceCorrecaoMonetaria == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgIndiceCorrecaoMonetaria" name="obgIndiceCorrecaoMonetaria" onclick="verificarSelecaoObrigatorio('obgIndiceCorrecaoMonetaria', 'selIndiceCorrecaoMonetaria');" <c:if test="${modeloContratoVO.obgIndiceCorrecaoMonetaria == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoIndiceCorrecaoMonetaria" name="valorFixoIndiceCorrecaoMonetaria" onclick="verificarSelecaoObrigatorio('valorFixoIndiceCorrecaoMonetaria', 'selIndiceCorrecaoMonetaria');" <c:if test="${modeloContratoVO.valorFixoIndiceCorrecaoMonetaria == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">�ndice de Corre��o Monet�ria:</td>
				<td class="campoTabela">
					<select name="indiceCorrecaoMonetaria" id="indiceCorrecaoMonetaria" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaIndiceMonetario}" var="indiceCorrecaoMonetaria">
							<option value="<c:out value="${indiceCorrecaoMonetaria.chavePrimaria}"/>" 
								<c:choose>
									<c:when test="${not empty modeloContratoVO.indiceCorrecaoMonetaria}">
										<c:if test="${modeloContratoVO.indiceCorrecaoMonetaria eq indiceCorrecaoMonetaria.chavePrimaria}">selected="selected"</c:if>
									</c:when>

								</c:choose>>
								<c:out value="${indiceCorrecaoMonetaria.descricaoAbreviada}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="5">Estimativa de Quantidade a Recuperar:</td>				
					</tr>
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selDataInicioRetiradaQPNRC" name="selDataInicioRetiradaQPNRC" onclick="permitirSelecaoObrigatorio('selDataInicioRetiradaQPNRC', 'obgDataInicioRetiradaQPNRC');" <c:if test="${modeloContratoVO.selDataInicioRetiradaQPNRC == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgDataInicioRetiradaQPNRC" name="obgDataInicioRetiradaQPNRC" onclick="verificarSelecaoObrigatorio('obgDataInicioRetiradaQPNRC', 'selDataInicioRetiradaQPNRC');" <c:if test="${modeloContratoVO.obgDataInicioRetiradaQPNRC == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"></td>
						<td class="rotuloTabela">Data para In�cio da Recupera��o:</td>
						<td class="campoTabela"></td>					
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgDataFimRetiradaQPNRC" value="true" campoPai="dataInicioRetiradaQPNRC" /></td>
						<td class="item-selecionado"></td>
						<td class="rotuloTabela">Data para Fim da Recupera��o:</td>
						<td class="campoTabela"></td>					
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercMinDuranteRetiradaQPNRC" value="true" campoPai="dataInicioRetiradaQPNRC" /></td>
						<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercMinDuranteRetiradaQPNRC" name="valorFixoPercMinDuranteRetiradaQPNRC" campoPai="selDataInicioRetiradaQPNRC" <c:if test="${modeloContratoVO.valorFixoPercMinDuranteRetiradaQPNRC == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Percentual M�nimo em rela��o � QDC para Recupera��o:</td>
						<td class="campoTabela">
							<input id="percMinDuranteRetiradaQPNRC" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" name="percMinDuranteRetiradaQPNRC" value="${modeloContratoVO.percMinDuranteRetiradaQPNRC }" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);"/>
							<label class="rotuloHorizontal rotuloInformativo" for="percMinDuranteRetiradaQPNRC">%</label>
						</td>					
					</tr>
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercDuranteRetiradaQPNRC" value="true" campoPai="dataInicioRetiradaQPNRC" /></td>
						<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercDuranteRetiradaQPNRC" name="valorFixoPercDuranteRetiradaQPNRC" campoPai="selDataInicioRetiradaQPNRC" <c:if test="${modeloContratoVO.valorFixoPercDuranteRetiradaQPNRC == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Percentual M�ximo em rela��o � QDC para Recupera��o:</td>
						<td class="campoTabela">
							<input id="percDuranteRetiradaQPNRC" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" name="percDuranteRetiradaQPNRC" value="${modeloContratoVO.percDuranteRetiradaQPNRC }" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);"/>
							<label class="rotuloHorizontal rotuloInformativo" for="percDuranteRetiradaQPNRC">%</label>
						</td>					
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercFimRetiradaQPNRC" value="true" campoPai="dataInicioRetiradaQPNRC" /></td>
						<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercFimRetiradaQPNRC" name="valorFixoPercFimRetiradaQPNRC" campoPai="selDataInicioRetiradaQPNRC" <c:if test="${modeloContratoVO.valorFixoPercFimRetiradaQPNRC == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Percentual M�ximo para Recupera��o com o t�rmino do Contrato:</td>
						<td class="campoTabela">
							<input id="percFimRetiradaQPNRC" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" name="percFimRetiradaQPNRC" value="${modeloContratoVO.percFimRetiradaQPNRC}" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" />
							<label class="rotuloHorizontal rotuloInformativo" for="percFimRetiradaQPNRC">%</label>
						</td>					
					</tr>
					
					<tr class="even">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" styleId="obgTempoValidadeRetiradaQPNRC" name="obgTempoValidadeRetiradaQPNRC" value="true" campoPai="dataInicioRetiradaQPNRC" /></td>
						<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTempoValidadeRetiradaQPNRC" name="valorFixoTempoValidadeRetiradaQPNRC" campoPai="selDataInicioRetiradaQPNRC" <c:if test="${modeloContratoVO.valorFixoTempoValidadeRetiradaQPNRC == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Tempo de Validade para Recupera��o da QPNR do Ano Anterior:</td>
						<td class="campoTabela">
							<input id="tempoValidadeRetiradaQPNRC" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="tempoValidadeRetiradaQPNRC" value="${modeloContratoVO.tempoValidadeRetiradaQPNRC}" onkeypress="return formatarCampoInteiro(event);"/>
							<label class="rotuloHorizontal rotuloInformativo" for="tempoValidadeRetiradaQPNRC">anos</label>
						</td>					
					</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Penalidades por Retirada a Maior/Menor:</td>				
			</tr>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPenalidadeRetMaiorMenor" name="selPenalidadeRetMaiorMenor" onclick="permitirSelecaoObrigatorio('selPenalidadeRetMaiorMenor', 'obgPenalidadeRetMaiorMenor');" <c:if test="${modeloContratoVO.selPenalidadeRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPenalidadeRetMaiorMenor" name="obgPenalidadeRetMaiorMenor" onclick="verificarSelecaoObrigatorio('obgPenalidadeRetMaiorMenor', 'selPenalidadeRetMaiorMenor');" <c:if test="${modeloContratoVO.obgPenalidadeRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPenalidadeRetMaiorMenor" name="valorFixoPenalidadeRetMaiorMenor" onclick="verificarSelecaoObrigatorio('valorFixoPenalidadeRetMaiorMenor', 'selPenalidadeRetMaiorMenor');" <c:if test="${modeloContratoVO.valorFixoPenalidadeRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Penalidade:</td>
				<td class="campoTabela">
					<select name="penalidadeRetMaiorMenor" id="penalidadeRetMaiorMenor" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="penalidadeRetMaiorMenor">
							<option value="<c:out value="${penalidadeRetMaiorMenor.chavePrimaria}"/>" <c:if test="${modeloContratoVO.penalidadeRetMaiorMenor == penalidadeRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${penalidadeRetMaiorMenor.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPeriodicidadeRetMaiorMenor" name="selPeriodicidadeRetMaiorMenor" onclick="permitirSelecaoObrigatorio('selPeriodicidadeRetMaiorMenor', 'obgPeriodicidadeRetMaiorMenor');" <c:if test="${modeloContratoVO.selPeriodicidadeRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPeriodicidadeRetMaiorMenor" name="obgPeriodicidadeRetMaiorMenor" onclick="verificarSelecaoObrigatorio('obgPeriodicidadeRetMaiorMenor', 'selPeriodicidadeRetMaiorMenor');" <c:if test="${modeloContratoVO.obgPeriodicidadeRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPeriodicidadeRetMaiorMenor" name="valorFixoPeriodicidadeRetMaiorMenor" onclick="verificarSelecaoObrigatorio('valorFixoPeriodicidadeRetMaiorMenor', 'selPeriodicidadeRetMaiorMenor');" <c:if test="${modeloContratoVO.valorFixoPeriodicidadeRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Periodicidade:</td>
				<td class="campoTabela">
					<select name="periodicidadeRetMaiorMenor" id="periodicidadeRetMaiorMenor" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="periodicidadeRetMaiorMenor">
							<option value="<c:out value="${periodicidadeRetMaiorMenor.chavePrimaria}"/>" <c:if test="${modeloContratoVO.periodicidadeRetMaiorMenor == periodicidadeRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${periodicidadeRetMaiorMenor.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>	
			
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgBaseApuracaoRetMaiorMenor" styleClass="checkObrigatorio1" name="obgBaseApuracaoRetMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoBaseApuracaoRetMaiorMenor" name="valorFixoBaseApuracaoRetMaiorMenor" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoBaseApuracaoRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Base de Apura��o:</td>
				<td class="campoTabela">
					<select class="campoSelect" name="baseApuracaoRetMaiorMenor" id="baseApuracaoRetMaiorMenor">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaBaseApuracao}" var="baseApuracaoRetMaiorMenor">
							<option value="<c:out value="${baseApuracaoRetMaiorMenor.chavePrimaria}"/>" <c:if test="${modeloContratoVO.baseApuracaoRetMaiorMenor == baseApuracaoRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${baseApuracaoRetMaiorMenor.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgDataIniVigRetirMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">In�cio de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgDataFimVigRetirMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="campoTabela"></td>
				<td class="rotuloTabela">Fim de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPrecoCobrancaRetirMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPrecoCobrancaRetirMaiorMenor" name="valorFixoPrecoCobrancaRetirMaiorMenor" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoPrecoCobrancaRetirMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
				<td class="campoTabela">
					<select name="precoCobrancaRetirMaiorMenor" id="precoCobrancaRetirMaiorMenor" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
							<option value="<c:out value="${precoCobranca.chavePrimaria}"/>" <c:if test="${modeloContratoVO.precoCobrancaRetirMaiorMenor == precoCobranca.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${precoCobranca.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgTipoApuracaoRetirMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTipoApuracaoRetirMaiorMenor" name="valorFixoTipoApuracaoRetirMaiorMenor" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoTipoApuracaoRetirMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Forma de C�lculo para Cobran�a:</td>
				<td class="campoTabela">
					<select name="tipoApuracaoRetirMaiorMenor" id="tipoApuracaoRetirMaiorMenor" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
							<option value="<c:out value="${tipoApuracao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoApuracaoRetirMaiorMenor == tipoApuracao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoApuracao.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualCobRetMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualCobRetMaiorMenor" name="valorFixoPercentualCobRetMaiorMenor" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoPercentualCobRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual de Cobran�a:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text"
					id="percentualCobRetMaiorMenor"
					name="percentualCobRetMaiorMenor" maxlength="6" size="4"
					onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);"
					onblur="aplicarMascaraNumeroDecimal(this, 2);"
					value="${modeloContratoVO.percentualCobRetMaiorMenor}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobreDem">%</label>				
				</td>
			</tr>
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualCobIntRetMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualCobIntRetMaiorMenor" name="valorFixoPercentualCobIntRetMaiorMenor" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoPercentualCobIntRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual de Cobran�a para Consumo Superior Durante Aviso de Interrup��o:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text"
					id="percentualCobIntRetMaiorMenor"
					name="percentualCobIntRetMaiorMenor"
					maxlength="6" size="4"
					onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);"
					onblur="aplicarMascaraNumeroDecimal(this, 2);"
					value="${modeloContratoVO.percentualCobIntRetMaiorMenor}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobreDem">%</label>				
				</td>
			</tr>
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgConsumoReferenciaRetMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoConsumoReferenciaRetMaiorMenor" name="valorFixoConsumoReferenciaRetMaiorMenor" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoConsumoReferenciaRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Consumo Refer�ncia:</td>
				<td class="campoTabela">
					<select name="consumoReferenciaRetMaiorMenor" id="consumoReferenciaRetMaiorMenor" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaRetMaiorMenor">
							<option value="<c:out value="${consumoReferenciaRetMaiorMenor.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoReferenciaRetMaiorMenor == consumoReferenciaRetMaiorMenor.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${consumoReferenciaRetMaiorMenor.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualRetMaiorMenor" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualRetMaiorMenor" name="valorFixoPercentualRetMaiorMenor" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoPercentualRetMaiorMenor == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual Por Retirada:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text"
					id="percentualRetMaiorMenor" name="percentualRetMaiorMenor"
					maxlength="6" size="4"
					onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);"
					onblur="aplicarMascaraNumeroDecimal(this, 2);"
					value="${modeloContratoVO.percentualRetMaiorMenor}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobreDem">%</label>				
				</td>
			</tr>

			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgIndicadorImposto" value="true" campoPai="periodicidadeRetMaiorMenor" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoIndicadorImposto" name="valorFixoIndicadorImposto" campoPai="selPeriodicidadeRetMaiorMenor" <c:if test="${modeloContratoVO.valorFixoIndicadorImposto == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Usar pre�os s/ impostos para cobran�a:</td>
				<td class="campoTabela">
					<input id="indicadorImpostoSim" class="campoRadio indicadorImpostoCSS" type="radio" value="true" name="indicadorImposto" <c:if test="${modeloContratoVO.indicadorImposto == true}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorImpostoSim">Sim</label>
					<input id="indicadorImpostoNao" class="campoRadio indicadorImpostoCSS" type="radio" value="false" name="indicadorImposto" <c:if test="${modeloContratoVO.indicadorImposto == false or empty modeloContratoVO.indicadorImposto}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="indicadorImpostoNao">N�o</label>
				</td>				
			</tr>			
			
			
			<!-------------FIM-------------------------->
			
<!-- 			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr> -->
<!-- 			<tr> -->
<!-- 				<td class="tituloGrupoTabela" colspan="4">Penalidades por Retirada a maior:</td>				 -->
<!-- 			</tr> -->
<!-- 			<tr class="even semLinhaPaiInicio"> -->
<%-- 				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selConsumoReferenciaSobreDem" name="selConsumoReferenciaSobreDem" onclick="permitirSelecaoObrigatorio('selConsumoReferenciaSobreDem', 'obgConsumoReferenciaSobreDem');" <c:if test="${modeloContratoVO.selConsumoReferenciaSobreDem == true}">checked="checked"</c:if>/></td> --%>
<%-- 				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgConsumoReferenciaSobreDem" name="obgConsumoReferenciaSobreDem" onclick="verificarSelecaoObrigatorio('obgConsumoReferenciaSobreDem', 'selConsumoReferenciaSobreDem');" <c:if test="${modeloContratoVO.obgConsumoReferenciaSobreDem == true}">checked="checked"</c:if>/></td> --%>
<!-- 				<td class="rotuloTabela">Consumo Referencial para Penalidade por Retirada a maior que:</td> -->
<!-- 				<td class="campoTabela"> -->
<!-- 					<select name="consumoReferenciaSobreDem" id="consumoReferenciaSobreDem" class="campoSelect"> -->
<!-- 						<option value="-1">Selecione</option> -->
<%-- 						<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaSobreDem"> --%>
<%-- 							<option value="<c:out value="${consumoReferenciaSobreDem.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoReferenciaSobreDem == consumoReferenciaSobreDem.chavePrimaria}">selected="selected"</c:if>> --%>
<%-- 								<c:out value="${consumoReferenciaSobreDem.descricao}"/> --%>
<!-- 							</option>		 -->
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</td> -->
<!-- 			</tr> -->
<!-- 			<tr class="even semLinhaPaiInicio"> -->
<%-- 				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgFaixaPenalidadeSobreDem" value="true" campoPai="consumoReferenciaSobreDem" /></td> --%>
<!-- 				<td class="rotuloTabela">Valor ou Faixa Penalidade por Retirada a Maior:</td> -->
<!-- 				<td class="campoTabela"> -->
<!-- 					<select name="faixaPenalidadeSobreDem" id="faixaPenalidadeSobreDem" class="campoSelect"> -->
<!-- 						<option value="-1">Selecione</option> -->
<%-- 						<c:forEach items="${listaFaixaPenalidadeSobreDem}" var="faixaPenalidadeSobreDem"> --%>
<%-- 							<option value="<c:out value="${faixaPenalidadeSobreDem.chavePrimaria}"/>" <c:if test="${modeloContratoVO.faixaPenalidadeSobreDem == faixaPenalidadeSobreDem.chavePrimaria}">selected="selected"</c:if>> --%>
<%-- 								<c:out value="${faixaPenalidadeSobreDem.descricao}"/> --%>
<!-- 							</option>		 -->
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</td> -->
<!-- 			</tr> -->
<!-- 			<tr class="even semLinhaPaiInicio"> -->
<%-- 				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualReferenciaSobreDem" value="true" campoPai="consumoReferenciaSobreDem" /></td> --%>
<!-- 				<td class="rotuloTabela">Percentual por Retirada a Maior:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<input class="campoTexto campoHorizontal" type="text" id="percentualReferenciaSobreDem" name="percentualReferenciaSobreDem" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.percentualReferenciaSobreDem}"> --%>
<!-- 					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobreDem">%</label>				 -->
<!-- 				</td> -->
<!-- 			</tr> -->
			
<!-- 			<tr class="even"> -->
<%-- 				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgBaseApuracaoRetMaior" styleClass="checkObrigatorio1" name="obgBaseApuracaoRetMaior" value="true" campoPai="consumoReferenciaSobreDem" /></td> --%>
<!-- 				<td class="rotuloTabela">Base de Apura��o:</td> -->
<!-- 				<td class="campoTabela"> -->
<!-- 					<select class="campoSelect" name="baseApuracaoRetMaior" id="baseApuracaoRetMaior"> -->
<!-- 						<option value="-1">Selecione</option> -->
<%-- 						<c:forEach items="${listaBaseApuracao}" var="baseApuracao"> --%>
<%-- 							<option value="<c:out value="${baseApuracao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.baseApuracaoRetMaior == baseApuracao.chavePrimaria}">selected="selected"</c:if>> --%>
<%-- 								<c:out value="${baseApuracao.descricao}"/> --%>
<!-- 							</option>		 -->
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</td> -->
<!-- 			</tr> -->
			
<!-- 			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr> -->
<!-- 			<tr> -->
<!-- 				<td class="tituloGrupoTabela" colspan="4">Penalidades por Retirada a menor:</td>				 -->
<!-- 			</tr> -->
<!-- 			<tr class="even semLinhaPaiInicio"> -->
<%-- 				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selConsumoReferenciaSobDem" name="selConsumoReferenciaSobDem" onclick="permitirSelecaoObrigatorio('selConsumoReferenciaSobDem', 'obgConsumoReferenciaSobDem');" <c:if test="${modeloContratoVO.selConsumoReferenciaSobDem == true}">checked="checked"</c:if>/></td> --%>
<%-- 				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgConsumoReferenciaSobDem" name="obgConsumoReferenciaSobDem" onclick="verificarSelecaoObrigatorio('obgConsumoReferenciaSobDem', 'selConsumoReferenciaSobDem');" <c:if test="${modeloContratoVO.obgConsumoReferenciaSobDem == true}">checked="checked"</c:if>/></td> --%>
<!-- 				<td class="rotuloTabela">Consumo Referencial para Penalidade por Retirada a menor que:</td> -->
<!-- 				<td class="campoTabela"> -->
<!-- 					<select name="consumoReferenciaSobDem" id="consumoReferenciaSobDem" class="campoSelect"> -->
<!-- 						<option value="-1">Selecione</option> -->
<%-- 						<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaSobDem"> --%>
<%-- 							<option value="<c:out value="${consumoReferenciaSobDem.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoReferenciaSobDem == consumoReferenciaSobDem.chavePrimaria}">selected="selected"</c:if>> --%>
<%-- 								<c:out value="${consumoReferenciaSobDem.descricao}"/> --%>
<!-- 							</option>		 -->
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</td> -->
<!-- 			</tr> -->
			
<!-- 			<tr class="even semLinhaPaiInicio"> -->
<%-- 				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgFaixaPenalidadeSobDem" value="true" campoPai="consumoReferenciaSobDem" /></td> --%>
<!-- 				<td class="rotuloTabela">Valor ou Faixa Penalidade por Retirada a Menor:</td> -->
<!-- 				<td class="campoTabela"> -->
<!-- 					<select name="faixaPenalidadeSobDem" id="faixaPenalidadeSobDem" class="campoSelect"> -->
<!-- 						<option value="-1">Selecione</option> -->
<%-- 						<c:forEach items="${listaFaixaPenalidadeSobDem}" var="faixaPenalidadeSobDem"> --%>
<%-- 							<option value="<c:out value="${faixaPenalidadeSobDem.chavePrimaria}"/>" <c:if test="${modeloContratoVO.faixaPenalidadeSobDem == faixaPenalidadeSobDem.chavePrimaria}">selected="selected"</c:if>> --%>
<%-- 								<c:out value="${faixaPenalidadeSobDem.descricao}"/> --%>
<!-- 							</option>		 -->
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</td> -->
<!-- 			</tr> -->
			
<!-- 			<tr class="even semLinhaPaiInicio"> -->
<%-- 				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgPercentualReferenciaSobDem" styleClass="checkObrigatorio1" name="obgPercentualReferenciaSobDem" value="true" campoPai="consumoReferenciaSobDem" /></td> --%>
<!-- 				<td class="rotuloTabela">Percentual por Retirada a Menor:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<input class="campoTexto campoHorizontal" type="text" id="percentualReferenciaSobDem" name="percentualReferenciaSobDem" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.percentualReferenciaSobDem}"> --%>
<!-- 					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobDem">%</label>				 -->
<!-- 				</td> -->
<!-- 			</tr> -->
			
<!-- 			<tr class="even"> -->
<%-- 				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgBaseApuracaoRetMenor" styleClass="checkObrigatorio1" name="obgBaseApuracaoRetMenor" value="true" campoPai="consumoReferenciaSobDem" /></td> --%>
<!-- 				<td class="rotuloTabela">Base de Apura��o:</td> -->
<!-- 				<td class="campoTabela"> -->
<!-- 					<select class="campoSelect" name="baseApuracaoRetMenor" id="baseApuracaoRetMenor"> -->
<!-- 						<option value="-1">Selecione</option> -->
<%-- 						<c:forEach items="${listaBaseApuracao}" var="baseApuracao"> --%>
<%-- 							<option value="<c:out value="${baseApuracao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.baseApuracaoRetMenor == baseApuracao.chavePrimaria}">selected="selected"</c:if>> --%>
<%-- 								<c:out value="${baseApuracao.descricao}"/> --%>
<!-- 							</option>		 -->
<%-- 						</c:forEach> --%>
<!-- 					</select>									 -->
<!-- 				</td> -->
<!-- 			</tr> -->
		
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Penalidades por Delivery Or Pay:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPercentualTarifaDoP" name="selPercentualTarifaDoP" onclick="permitirSelecaoObrigatorio('selPercentualTarifaDoP', 'obgPercentualTarifaDoP');" <c:if test="${modeloContratoVO.selPercentualTarifaDoP == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPercentualTarifaDoP" name="obgPercentualTarifaDoP" onclick="verificarSelecaoObrigatorio('obgPercentualTarifaDoP', 'selPercentualTarifaDoP');" <c:if test="${modeloContratoVO.obgPercentualTarifaDoP == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualTarifaDoP" name="valorFixoPercentualTarifaDoP" onclick="verificarSelecaoObrigatorio('valorFixoPercentualTarifaDoP', 'selPercentualTarifaDoP');" <c:if test="${modeloContratoVO.valorFixoPercentualTarifaDoP == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual Sobre a Tarifa do G�s:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text" id="percentualTarifaDoP" name="percentualTarifaDoP" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.percentualTarifaDoP}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualTarifaDoP">%</label>				
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Penalidades por G�s Fora de Especifica��o:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPercentualSobreTariGas" name="selPercentualSobreTariGas" onclick="permitirSelecaoObrigatorio('selPercentualSobreTariGas', 'obgPercentualSobreTariGas');" <c:if test="${modeloContratoVO.selPercentualSobreTariGas == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPercentualSobreTariGas" name="obgPercentualSobreTariGas" onclick="verificarSelecaoObrigatorio('obgPercentualSobreTariGas', 'selPercentualSobreTariGas');" <c:if test="${modeloContratoVO.obgPercentualSobreTariGas == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualSobreTariGas" name="valorFixoPercentualSobreTariGas" onclick="verificarSelecaoObrigatorio('valorFixoPercentualSobreTariGas', 'selPercentualSobreTariGas');" <c:if test="${modeloContratoVO.valorFixoPercentualSobreTariGas == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual Sobre a Tarifa do G�s:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text" id="percentualSobreTariGas" name="percentualSobreTariGas" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.percentualSobreTariGas}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualSobreTariGas">%</label>				
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Multa Rescis�ria:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selMultaRecisoria" 
					name="selMultaRecisoria" onclick="permitirSelecaoObrigatorio('selMultaRecisoria', 'obgMultaRecisoria');" 
					<c:if test="${modeloContratoVO.selMultaRecisoria == true}">checked="checked"</c:if>/>
				</td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgMultaRecisoria" 
					name="obgMultaRecisoria" onclick="verificarSelecaoObrigatorio('obgMultaRecisoria', 'selMultaRecisoria');" 
					<c:if test="${modeloContratoVO.obgMultaRecisoria == true}">checked="checked"</c:if>/>
				</td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoMultaRecisoria" 
					name="valorFixoMultaRecisoria" onclick="verificarSelecaoObrigatorio('valorFixoMultaRecisoria', 'selMultaRecisoria');" 
					<c:if test="${modeloContratoVO.valorFixoMultaRecisoria == true}">checked="checked"</c:if>/>
				</td>
				<td class="rotuloTabela">Multa Rescis�ria:</td>
				<td class="campoTabela">
					<select name="multaRecisoria" id="multaRecisoria" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoMultaRecisoria}" var="multaRecisoria">
							<option value="<c:out value="${multaRecisoria.chavePrimaria}"/>" 
								<c:if test="${modeloContratoVO.multaRecisoria == multaRecisoria.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${multaRecisoria.descricao}"/>
							</option>		
						</c:forEach>
					</select>				
				</td>
			</tr>
			<tr class="odd">										
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selDataRecisao"/></td>				
				<td class="item-selecionado"></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data da Recis�o:</td>
				<td class="campoTabela"></td>				
			</tr>
			
		
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Take or Pay:</td>				
			</tr>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPeriodicidadeTakeOrPayC" name="selPeriodicidadeTakeOrPayC" onclick="permitirSelecaoObrigatorio('selPeriodicidadeTakeOrPayC', 'obgPeriodicidadeTakeOrPayC');" <c:if test="${modeloContratoVO.selPeriodicidadeTakeOrPayC == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPeriodicidadeTakeOrPayC" name="obgPeriodicidadeTakeOrPayC" onclick="verificarSelecaoObrigatorio('obgPeriodicidadeTakeOrPayC', 'selPeriodicidadeTakeOrPayC');" <c:if test="${modeloContratoVO.obgPeriodicidadeTakeOrPayC == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPeriodicidadeTakeOrPayC" name="valorFixoPeriodicidadeTakeOrPayC" onclick="verificarSelecaoObrigatorio('valorFixoPeriodicidadeTakeOrPayC', 'selPeriodicidadeTakeOrPayC');" <c:if test="${modeloContratoVO.valorFixoPeriodicidadeTakeOrPayC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Periodicidade ToP:</td>
				<td class="campoTabela">
					<select name="periodicidadeTakeOrPayC" id="periodicidadeTakeOrPayC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeTakeOrPay">
							<option value="<c:out value="${periodicidadeTakeOrPay.chavePrimaria}"/>" <c:if test="${modeloContratoVO.periodicidadeTakeOrPayC == periodicidadeTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${periodicidadeTakeOrPay.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>	
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgReferenciaQFParadaProgramadaC" styleId="obgReferenciaQFParadaProgramadaC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoReferenciaQFParadaProgramadaC" name="valorFixoReferenciaQFParadaProgramadaC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoReferenciaQFParadaProgramadaC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Refer�ncia Quantidade Faltante Parada Programada:</td>
				<td class="campoTabela">
					<select name="referenciaQFParadaProgramadaC" id="referenciaQFParadaProgramadaC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaReferenciaQFParadaProgramada}" var="referenciaQFParadaProgramada">
							<option value="<c:out value="${referenciaQFParadaProgramada.chavePrimaria}"/>" <c:if test="${modeloContratoVO.referenciaQFParadaProgramadaC == referenciaQFParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${referenciaQFParadaProgramada.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>	
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgConsumoReferenciaTakeOrPayC" styleId="obgConsumoReferenciaTakeOrPayC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoConsumoReferenciaTakeOrPayC" name="valorFixoConsumoReferenciaTakeOrPayC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoConsumoReferenciaTakeOrPayC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Consumo Referencial para ToP:</td>
				<td class="campoTabela">
					<select name="consumoReferenciaTakeOrPayC" id="consumoReferenciaTakeOrPayC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaTakeOrPay">
							<option value="<c:out value="${consumoReferenciaTakeOrPay.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoReferenciaTakeOrPayC == consumoReferenciaTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${consumoReferenciaTakeOrPay.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgMargemVariacaoTakeOrPayC" styleId="obgMargemVariacaoTakeOrPayC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoMargemVariacaoTakeOrPayC" name="valorFixoMargemVariacaoTakeOrPayC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoMargemVariacaoTakeOrPayC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Compromisso de Retirada ToP:</td>
				<td class="campoTabela">
					<input id="margemVariacaoTakeOrPayC" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" name="margemVariacaoTakeOrPayC" value="${modeloContratoVO.margemVariacaoTakeOrPayC }"/>
					<label class="rotuloHorizontal rotuloInformativo" for="margemVariacaoTakeOrPayC">%</label>
				</td>
			</tr>
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgDataInicioVigenciaC" styleId="obgDataInicioVigenciaC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data In�cio de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgDataFimVigenciaC" styleId="obgDataFimVigenciaC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Data Fim de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPrecoCobrancaC" styleId="obgPrecoCobrancaC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPrecoCobrancaC" name="valorFixoPrecoCobrancaC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoPrecoCobrancaC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
				<td class="campoTabela">
					<select name="precoCobrancaC" id="precoCobrancaC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
							<option value="<c:out value="${precoCobranca.chavePrimaria}"/>" <c:if test="${modeloContratoVO.precoCobrancaC == precoCobranca.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${precoCobranca.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgTipoApuracaoC" styleId="obgTipoApuracaoC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTipoApuracaoC" name="valorFixoTipoApuracaoC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoTipoApuracaoC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Forma de C�lculo para Recupera��o:</td>
				<td class="campoTabela">
					<select name="tipoApuracaoC" id="tipoApuracaoC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
							<option value="<c:out value="${tipoApuracao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoApuracaoC == tipoApuracao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoApuracao.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgConsideraParadaProgramadaC" styleId="obgConsideraParadaProgramadaC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoConsideraParadaProgramadaC" name="valorFixoConsideraParadaProgramadaC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoConsideraParadaProgramadaC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Considerar Paradas Programadas:</td>
				<td class="campoTabela">
					<input id="consideraParadaProgramadaSimC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="consideraParadaProgramadaC" <c:if test="${modeloContratoVO.consideraParadaProgramadaC == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="consideraParadaProgramadaSimC">Sim</label>
					<input id="consideraParadaProgramadaNaoC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="consideraParadaProgramadaC" <c:if test="${modeloContratoVO.consideraParadaProgramadaC == false or empty modeloContratoVO.consideraParadaProgramadaC}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="consideraParadaProgramadaNaoC">N�o</label>
				</td>
			</tr>


			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgConsideraFalhaFornecimentoC" styleId="obgConsideraFalhaFornecimentoC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoConsideraFalhaFornecimentoC" name="valorFixoConsideraFalhaFornecimentoC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoConsideraFalhaFornecimentoC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Considerar Falhas de Fornecimento:</td>
				<td class="campoTabela">
					<input id="consideraFalhaFornecimentoSimC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="consideraFalhaFornecimentoC" <c:if test="${modeloContratoVO.consideraFalhaFornecimentoC == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="consideraFalhaFornecimentoSimC">Sim</label>
					<input id="consideraFalhaFornecimentoNaoC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="consideraFalhaFornecimentoC" <c:if test="${modeloContratoVO.consideraFalhaFornecimentoC == false or empty modeloContratoVO.consideraFalhaFornecimentoC}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="consideraFalhaFornecimentoNaoC">N�o</label>
				</td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgConsideraCasoFortuitoC" styleId="obgConsideraCasoFortuitoC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoConsideraCasoFortuitoC" name="valorFixoConsideraCasoFortuitoC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoConsideraCasoFortuitoC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Considerar Casos Fortuitos:</td>
				<td class="campoTabela">
					<input id="consideraCasoFortuitoSimC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="consideraCasoFortuitoC" <c:if test="${modeloContratoVO.consideraCasoFortuitoC == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="consideraCasoFortuitoSimC">Sim</label>
					<input id="consideraCasoFortuitoNaoC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="consideraCasoFortuitoC" <c:if test="${modeloContratoVO.consideraCasoFortuitoC == false or empty modeloContratoVO.consideraCasoFortuitoC}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="consideraCasoFortuitoNaoC">N�o</label>
				</td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgRecuperavelC" styleId="obgRecuperavelC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoRecuperavelC" name="valorFixoRecuperavelC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoRecuperavelC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Recuper�vel:</td>
				<td class="campoTabela">
					<input id="recuperavelSimC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="recuperavelC" <c:if test="${modeloContratoVO.recuperavelC == true}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="recuperavelSimC">Sim</label>
					<input id="recuperavelNaoC" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="recuperavelC" <c:if test="${modeloContratoVO.recuperavelC == false or empty modeloContratoVO.recuperavelC}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="recuperavelNaoC">N�o</label>
				</td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualNaoRecuperavelC" styleId="obgPercentualNaoRecuperavelC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercentualNaoRecuperavelC" name="valorFixoPercentualNaoRecuperavelC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoPercentualNaoRecuperavelC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual N�o Recuper�vel:</td>
				<td class="campoTabela">
					<input id="percentualNaoRecuperavelC" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" name="percentualNaoRecuperavelC" value="${modeloContratoVO.percentualNaoRecuperavelC }"/>
					<label class="rotuloHorizontal rotuloInformativo" for="percentualNaoRecuperavelC">%</label>
				</td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgApuracaoParadaProgramadaC" styleId="obgApuracaoParadaProgramadaC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoApuracaoParadaProgramadaC" name="valorFixoApuracaoParadaProgramadaC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoApuracaoParadaProgramadaC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume de Parada Programada:</td>
				<td class="campoTabela">
					<select name="apuracaoParadaProgramadaC" id="apuracaoParadaProgramadaC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaApuracaoParadaProgramada}" var="apuracaoParadaProgramada">
							<option value="<c:out value="${apuracaoParadaProgramada.chavePrimaria}"/>" <c:if test="${modeloContratoVO.apuracaoParadaProgramadaC == apuracaoParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${apuracaoParadaProgramada.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
								
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgApuracaoCasoFortuitoC" styleId="obgApuracaoCasoFortuitoC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoApuracaoCasoFortuitoC" name="valorFixoApuracaoCasoFortuitoC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoApuracaoCasoFortuitoC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume do Caso Fortuito ou For�a Maior:</td>
				<td class="campoTabela">
					<select name="apuracaoCasoFortuitoC" id="apuracaoCasoFortuitoC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaApuracaoCasoFortuito}" var="apuracaoCasoFortuito">
							<option value="<c:out value="${apuracaoCasoFortuito.chavePrimaria}"/>" <c:if test="${modeloContratoVO.apuracaoCasoFortuitoC == apuracaoCasoFortuito.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${apuracaoCasoFortuito.descricao}"/>
							</option>		
						</c:forEach>
					</select>						
				</td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgApuracaoFalhaFornecimentoC" styleId="obgApuracaoFalhaFornecimentoC" value="true" campoPai="periodicidadeTakeOrPayC" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoApuracaoFalhaFornecimentoC" name="valorFixoApuracaoFalhaFornecimentoC" campoPai="selPeriodicidadeTakeOrPayC" <c:if test="${modeloContratoVO.valorFixoApuracaoFalhaFornecimentoC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume de Falha de Fornecimento:</td>
				<td class="campoTabela">
					<select name="apuracaoFalhaFornecimentoC" id="apuracaoFalhaFornecimentoC" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaApuracaoFalhaFornecimento}" var="apuracaoFalhaFornecimento">
							<option value="<c:out value="${apuracaoFalhaFornecimento.chavePrimaria}"/>" <c:if test="${modeloContratoVO.apuracaoFalhaFornecimentoC == apuracaoFalhaFornecimento.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${apuracaoFalhaFornecimento.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
																							
		</tbody>
	</table>
</fieldset>
