<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<!--[if lt IE 10]>
    <style type="text/css">
    	.indicadorRadioCss{padding:5px 5px 0px 0px;}
    </style>
<![endif]-->

<script>
	$(document).ready(function(){
		$("#diaVencimentoItemFatura").css("margin-left","0");
		configurarPercentualQdc();
	});

	function configurarPercentualQdc(){

		var habilitarCampo = true;
		var valorSelecionado = $("#itemFatura").val();
		
		var itemFaturaMargemDistribuicao = <c:out value="${itemFaturaMargemDistribuicao}"/>;

		if(valorSelecionado == itemFaturaMargemDistribuicao){
			habilitarCampo = false;	
		}

		$("#percminimoQDC").prop("disabled", habilitarCampo);
	}
	 
	function controlarCheckBoxObrigatorios5(checkObrigatorios) {
			
		var valorCheck = checkObrigatorios.checked;
		var camposObrigatorios = new Array('obgDepositoIdentificado','obgEndFisEntFatCEP','obgEndFisEntFatEmail');
		var camposSelecionados = new Array('selItemFatura','selEndFisEntFatCEP','selEndFisEntFatEmail');

		var camposObrigatoriosSemSelecionados = new Array('obgFaseVencimentoItemFatura','obgIndicadorVencDiaNaoUtil','obgDepositoIdentificado','obgEndFisEntFatComplemento');
		
		for (var i = 0; i < camposObrigatoriosSemSelecionados.length; i++) {
			var campoSelecionado = document.getElementById(camposObrigatoriosSemSelecionados[i]).checked;
			if(document.getElementById(camposObrigatoriosSemSelecionados[i]) != null){
				if (campoSelecionado != null && valorCheck == true) {
					document.getElementById(camposObrigatoriosSemSelecionados[i]).checked = true;
				} else {
					document.getElementById(camposObrigatoriosSemSelecionados[i]).checked = false;
				}			
			}
		}
		
		marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, valorCheck);
	}
	
	function limparAbaFaturamento(){
		document.getElementById('itemFatura').selectedIndex = "0";
		document.getElementsByName('indicadorVencDiaNaoUtil')[1].checked = true;		
		document.getElementsByName('debitoAutomatico')[1].checked = true;
		document.getElementsByName('depositoIdentificado')[1].checked = true;
		document.getElementById('contratoCompra').selectedIndex = "3";
		document.getElementsByName('enderecoPadrao')[2].checked = true;
	}
	
</script>

<fieldset id="Faturamento">
	<a class="linkHelp" href="<help:help>/abafaturamentoinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado<input id="checkAllSelecionado5" type="checkbox" name="todos"/></th>
				<th width="73px">Obrigat�rio<input id="checkAllObrigatorio5" type="checkbox" name="todos" onclick="controlarCheckBoxObrigatorios5(this);"/></th>
				<th width="73px">Valor Fixo<input id="checkAllValorFixo5" type="checkbox" name="todos" onclick="controlarCheckBoxValorFixo(this);" /></th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
			<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selFatPeriodicidade" id="selFatPeriodicidade" checked="true" /></td>
			<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgFatPeriodicidade" /></td>
			<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFatPeriodicidade" name="valorFixoFatPeriodicidade" <c:if test="${modeloContratoVO.valorFixoFatPeriodicidade == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Periodicidade:</td>
				<td class="campoTabela">
					<select id="fatPeriodicidade" class="campoSelect" name="fatPeriodicidade">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPeriodicidade}" var="fatPeriodicidade">
							<option value="<c:out value="${fatPeriodicidade.chavePrimaria}"/>" <c:if test="${modeloContratoVO.fatPeriodicidade == fatPeriodicidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${fatPeriodicidade.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selIndicadorNFE" id="selIndicadorNFE" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgIndicadorNFE" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoIndicadorNFE" name="valorFixoIndicadorNFE" <c:if test="${modeloContratoVO.valorFixoIndicadorNFE == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Emiss�o de nota fiscal eletr�nica:</td>				
				<td class="campoTabela">
					<input id="indicadorNFESim" class="campoRadio indicadorRadioCss" type="radio" value="true" name="indicadorNFE" <c:if test="${modeloContratoVO.indicadorNFE == true}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="indicadorNFESim">Sim</label>
					<input id="indicadorNFENao" class="campoRadio indicadorRadioCss" type="radio" value="false" name="indicadorNFE" <c:if test="${modeloContratoVO.indicadorNFE == false or empty modeloContratoVO.indicadorNFE}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="indicadorNFENao">N�o</label>				
				</td>
			</tr>				
			
			
				<tr class="odd">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selEmitirFaturaComNfe" id="selEmitirFaturaComNfe" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgEmitirFaturaComNfe" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoEmitirFaturaComNfe" name="valorFixoEmitirFaturaComNfe" <c:if test="${modeloContratoVO.valorFixoEmitirFaturaComNfe == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Emiss�o de fatura da  nota fiscal eletr�nica:</td>				
				<td class="campoTabela">
					<input id="emitirFaturaComNfeSim" class="campoRadio indicadorRadioCss" type="radio" value="true" name="emitirFaturaComNfe" <c:if test="${modeloContratoVO.emitirFaturaComNfe == true}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="emitirFaturaComNfeSim">Sim</label>
					<input id="emitirFaturaComNfeNao" class="campoRadio indicadorRadioCss" type="radio" value="false" name="emitirFaturaComNfe" <c:if test="${modeloContratoVO.emitirFaturaComNfe == false or empty modeloContratoVO.emitirFaturaComNfe}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="emitirFaturaComNfeNao">N�o</label>				
				</td>
			</tr>	
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Item de Faturamento:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selItemFatura" id="selItemFatura" checked="true"/></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgItemFatura"/></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoItemFatura" name="valorFixoItemFatura" <c:if test="${modeloContratoVO.valorFixoItemFatura == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Item da Fatura:</td>
				<td class="campoTabela">
					<select id="itemFatura" class="campoSelect" name=itemFatura onchange="configurarPercentualQdc();">
						<c:forEach items="${listaItemFatura}" var="itemFatura">
							<option value="<c:out value="${itemFatura.chavePrimaria}"/>" <c:if test="${modeloContratoVO.itemFatura == itemFatura.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${itemFatura.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
									
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" name="obgTarifaConsumo" value="true" campoPai="itemFatura"  /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTarifaConsumo" name="valorFixoTarifaConsumo" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoTarifaConsumo == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Tarifa de Consumo:</td>
				<td class="campoTabela">
					<select id="tarifaConsumo" class="campoSelect" name=tarifaConsumo>
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTarifas}" var="tarifaConsumo">
							<option value="<c:out value="${tarifaConsumo.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tarifaConsumo == tarifaConsumo.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tarifaConsumo.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" name="obgPercminimoQDC" value="true" campoPai="itemFatura"  /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoPercminimoQDC" name="valorFixoPercminimoQDC" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoPercminimoQDC == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Percentual Minimo do QDC:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text" id="percminimoQDC" name="percminimoQDC" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${modeloContratoVO.percminimoQDC}">
					<label class="rotuloHorizontal rotuloInformativo" for="percminimoQDC">%</label>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" name="obgDataReferenciaCambial" value="true" campoPai="itemFatura"  /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDataReferenciaCambial" name="valorFixoDataReferenciaCambial" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoDataReferenciaCambial == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Data de Refer�ncia Cambial:</td>
				<td class="campoTabela">
					<select id="dataReferenciaCambial" class="campoSelect" name="dataReferenciaCambial">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaDataReferenciaCambial}" var="dataReferenciaCambial">
							<option value="<c:out value="${dataReferenciaCambial.chavePrimaria}"/>" <c:if test="${modeloContratoVO.dataReferenciaCambial eq dataReferenciaCambial.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${dataReferenciaCambial.descricao}"/>
							</option>		
						</c:forEach>						
					</select>
				</td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" name="obgDiaCotacao" value="true" campoPai="itemFatura"  /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDiaCotacao" name="valorFixoDiaCotacao" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoDiaCotacao == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Dia da Cota��o:</td>
				<td class="campoTabela">
					<select id="diaCotacao" class="campoSelect" name="diaCotacao">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaDiaCotacao}" var="diaCotacao">
							<option value="<c:out value="${diaCotacao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.diaCotacao eq diaCotacao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${diaCotacao.descricao}"/>
							</option>		
						</c:forEach>						
					</select>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" name="obgDiaVencimentoItemFatura" value="true" campoPai="itemFatura" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDiaVencimentoItemFatura" name="valorFixoDiaVencimentoItemFatura" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoDiaVencimentoItemFatura == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Dia para Vencimento:</td>
				<td class="campoTabela">
					<select id="diaVencimentoItemFatura" class="campoTexto campoHorizontal" name="diaVencimentoItemFatura">
					  <option value="-1">Selecione</option>
					  <c:forEach begin="1" end="31" var="dia">
						<option value="${dia}" <c:if test="${modeloContratoVO.diaVencimentoItemFatura == dia}">selected="selected"</c:if>>${dia}</option>
					  </c:forEach>
					</select>
					<label class="rotuloHorizontal rotuloInformativo" for="diaVencimentoItemFatura">(01-31)</label>
				</td>
			</tr>
			
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" name="obgOpcaoVencimentoItemFatura" value="true" campoPai="itemFatura"  /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoOpcaoVencimentoItemFatura" name="valorFixoOpcaoVencimentoItemFatura" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoOpcaoVencimentoItemFatura == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Op��o para Vencimento:</td>
				<td class="campoTabela">
					<select id="opcaoVencimentoItemFatura" class="campoSelect" name="opcaoVencimentoItemFatura">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaOpcaoFaseRefVencimento}" var="opcaoVencimentoItemFatura">
							<option value="<c:out value="${opcaoVencimentoItemFatura.chavePrimaria}"/>" <c:if test="${modeloContratoVO.opcaoVencimentoItemFatura == opcaoVencimentoItemFatura.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${opcaoVencimentoItemFatura.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" styleId="obgFaseVencimentoItemFatura" name="obgFaseVencimentoItemFatura" value="true" campoPai="itemFatura"  /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoFaseVencimentoItemFatura" name="valorFixoFaseVencimentoItemFatura" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoFaseVencimentoItemFatura == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Fase Refer�ncia para Vencimento:</td>
				<td class="campoTabela">
					<select id="faseVencimentoItemFatura" class="campoSelect" name="faseVencimentoItemFatura">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaFaseReferencialVencimento}" var="faseVencimentoItemFatura">
							<option value="<c:out value="${faseVencimentoItemFatura.chavePrimaria}"/>" <c:if test="${modeloContratoVO.faseVencimentoItemFatura == faseVencimentoItemFatura.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${faseVencimentoItemFatura.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" styleId="obgIndicadorVencDiaNaoUtil" name="obgIndicadorVencDiaNaoUtil" value="true" campoPai="itemFatura" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoIndicadorVencDiaNaoUtil" name="valorFixoIndicadorVencDiaNaoUtil" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoIndicadorVencDiaNaoUtil == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Vencimento em dia n�o �til?</td>
				<td class="campoTabela">
					<input id="indicadorVencDiaNaoUtilSim" class="campoRadio indicadorRadioCss" type="radio" value="true" name="indicadorVencDiaNaoUtil" <c:if test="${modeloContratoVO.indicadorVencDiaNaoUtil == true}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="indicadorVencDiaNaoSim">Sim</label>
					<input id="indicadorVencDiaNaoUtilNao" class="campoRadio indicadorRadioCss" type="radio" value="false" name="indicadorVencDiaNaoUtil" <c:if test="${modeloContratoVO.indicadorVencDiaNaoUtil == false or empty modeloContratoVO.indicadorVencDiaNaoUtil}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="indicadorVencDiaNaoUtilNao">N�o</label>
				</td>
			</tr>
			
<!-- 			<tr class="odd"> -->
<%-- 				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" styleId="obgDebitoAutomatico" name="obgDepositoIdentificado" value="true" campoPai="itemFatura" /></td> --%>
<%-- 				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDebitoAutomatico" name="valorFixoDebitoAutomatico" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoDebitoAutomatico == true}">checked="checked"</c:if>/></td> --%>
<!-- 				<td class="rotuloTabela">D�bito Autom�tico?</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<input id="debitoAutomaticoSim" class="campoRadio indicadorRadioCss" type="radio" value="true" name="debitoAutomatico" <c:if test="${modeloContratoVO.debitoAutomatico == true}">checked="checked"</c:if> /> --%>
<!-- 					<label class="rotuloRadio" for="debitoAutomaticoSim">Sim</label> -->
<%-- 					<input id="debitoAutomaticoNao" class="campoRadio indicadorRadioCss" type="radio" value="false" name="debitoAutomatico" <c:if test="${modeloContratoVO.debitoAutomatico == false or empty modeloContratoVO.debitoAutomatico}">checked="checked"</c:if> /> --%>
<!-- 					<label class="rotuloRadio" for="debitoAutomaticoNao">N�o</label> -->
<!-- 				</td> -->
<!-- 			</tr> -->
			
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" styleId="obgDepositoIdentificado" name="obgDepositoIdentificado" value="true" campoPai="itemFatura" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoDepositoIdentificado" name="valorFixoDepositoIdentificado" campoPai="selItemFatura" <c:if test="${modeloContratoVO.valorFixoDepositoIdentificado == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Dep�sito Identificado?</td>
				<td class="campoTabela">
					<input id="depositoIdentificadoSim" class="campoRadio indicadorRadioCss" type="radio" value="true" name="depositoIdentificado" <c:if test="${modeloContratoVO.depositoIdentificado == true}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="depositoIdentificadoSim">Sim</label>
					<input id="depositoIdentificadoNao" class="campoRadio indicadorRadioCss" type="radio" value="false" name="depositoIdentificado" <c:if test="${modeloContratoVO.depositoIdentificado == false or empty modeloContratoVO.depositoIdentificado}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="depositoIdentificadoNao">N�o</label>
				</td>
			</tr>					
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Entrega da Fatura:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><input class="checkSelecionado5" type="checkbox" value="true" id="selEndFisEntFatCEP" name="selEndFisEntFatCEP" onclick="permitirSelecaoObrigatorio('selEndFisEntFatCEP', 'obgEndFisEntFatCEP');" <c:if test="${modeloContratoVO.selEndFisEntFatCEP == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio5" type="checkbox" value="true" id="obgEndFisEntFatCEP" name="obgEndFisEntFatCEP" onclick="verificarSelecaoObrigatorio('obgEndFisEntFatCEP', 'selEndFisEntFatCEP');" <c:if test="${modeloContratoVO.obgEndFisEntFatCEP == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">CEP:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" name="obgEndFisEntFatNumero" value="true" campoPai="endFisEntFatCEP" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">N�mero:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio5" styleId="obgEndFisEntFatComplemento" name="obgEndFisEntFatComplemento" value="true" campoPai="endFisEntFatCEP" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Complemento:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td class="item-selecionado"><input class="checkSelecionado5" type="checkbox" value="true" id="selEndFisEntFatEmail" name="selEndFisEntFatEmail" onclick="permitirSelecaoObrigatorio('selEndFisEntFatEmail', 'obgEndFisEntFatEmail');" <c:if test="${modeloContratoVO.selEndFisEntFatEmail == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio5" type="checkbox" value="true" id="obgEndFisEntFatEmail" name="obgEndFisEntFatEmail" onclick="verificarSelecaoObrigatorio('obgEndFisEntFatEmail', 'selEndFisEntFatEmail');" <c:if test="${modeloContratoVO.obgEndFisEntFatEmail == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">E-mail:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selEnderecoPadrao" id="selEnderecoPadrao" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgEnderecoPadrao" /></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Endere�o padr�o: </td>
					<td class="campoTabela">
					<input id="enderecoPadraoSim" class="campoRadio indicadorRadioCss" type="radio" value="True" name="enderecoPadrao" <c:if test="${modeloContratoVO.enderecoPadrao eq true}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="enderecoPadraoSim">Cliente</label>
					<input id="enderecoPadraoNao" class="campoRadio indicadorRadioCss" type="radio" value="false" name="enderecoPadrao" <c:if test="${modeloContratoVO.enderecoPadrao eq false}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="enderecoPadraoNao">Ponto de Consumo</label>
					<input id="enderecoPadraoNenhum" class="campoRadio indicadorRadioCss" type="radio" value="" name="enderecoPadrao" <c:if test="${empty modeloContratoVO.enderecoPadrao}">checked="checked"</c:if> />
					<label class="rotuloRadio" for="enderecoPadraoNenhum">Nenhum</label>
					</td>
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Dados de Compra:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selContratoCompra" id="selContratoCompra" checked="true" /></td>
				<td class="item-selecionado"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgContratoCompra" /></td>
				<td class="item-selecionado"><input class="checkValorFixo" type="checkbox" value="true" id="valorFixoContratoCompra" name="valorFixoContratoCompra" <c:if test="${modeloContratoVO.valorFixoContratoCompra == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Contrato de Compra:</td>
				<td class="campoTabela">
			<select name="contratoCompra" id="contratoCompra" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaContratoCompra}" var="contratoCompra">
					<option value="<c:out value="${contratoCompra.chavePrimaria}"/>" 
						<c:choose>
							<c:when test="${not empty modeloContratoVO.contratoCompra}">
								<c:if test="${modeloContratoVO.contratoCompra eq contratoCompra.chavePrimaria}">selected="selected"</c:if>
							</c:when>
							<c:otherwise>
								<c:if test="${contratoCompra.indicadorPadrao eq true}">selected="selected"</c:if>	
							</c:otherwise>
						</c:choose>>
						<c:out value="${contratoCompra.descricao}"/>
					</option>		
				</c:forEach>
			</select>
		</td>
			</tr>
			
		</tbody>
	</table>
</fieldset>