<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fieldset id="Consumo">		
	<a class="linkHelp" href="<help:help>/abaconsumoinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado</th>
				<th width="73px">Obrigat�rio</th>
				<th width="73px">Valor Fixo</th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td><c:if test="${selHoraInicialDia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgHoraInicialDia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoHoraInicialDia eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Hora Inicial do Dia:</td>
				<td class="campoTabela">
					<c:if test="${horaInicialDia ne null && horaInicialDia ne ''}">
						<span class="itemDetalhamento"><c:out value="${horaInicialDia}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="horaInicialDia">h</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selRegimeConsumo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgRegimeConsumo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoRegimeConsumo eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Regime de Consumo:</td>
				<td class="campoTabela">
					<c:forEach items="${listaRegimeConsumo}" var="objRegimeConsumo">
						<c:if test="${regimeConsumo eq objRegimeConsumo.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objRegimeConsumo.descricao}"/></span>
						</c:if>
					</c:forEach>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selFornecimentoMaximoDiario eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFornecimentoMaximoDiario eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFornecimentoMaximoDiario eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Fornecimento M�ximo Di�rio:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:out value="${fornecimentoMaximoDiario}"/></span>
					<c:if test="${fornecimentoMaximoDiario ne null && fornecimentoMaximoDiario ne ''}">
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:choose>
								<c:when test="${unidadeFornecMaximoDiario ne ''}">
									<c:if test="${unidadeFornecMaximoDiario eq unidade.chavePrimaria}">
										<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selFornecimentoMaximoDiario eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selFornecimentoMinimoDiario eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFornecimentoMinimoDiario eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFornecimentoMinimoDiario eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Fornecimento M�nimo Di�rio:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:out value="${fornecimentoMinimoDiario}"/></span>
					<c:if test="${fornecimentoMinimoDiario ne null && fornecimentoMinimoDiario ne ''}">
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:choose>
								<c:when test="${unidadeFornecMinimoDiario ne ''}">
									<c:if test="${unidadeFornecMinimoDiario eq unidade.chavePrimaria}">
										<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selFornecimentoMinimoDiario eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selFornecimentoMinimoMensal eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFornecimentoMinimoMensal eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFornecimentoMinimoMensal eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Fornecimento M�nimo Mensal:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:out value="${fornecimentoMinimoMensal}"/></span>
					<c:if test="${fornecimentoMinimoDiario ne null && fornecimentoMinimoDiario ne ''}">
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:choose>
								<c:when test="${unidadeFornecMinimoMensal ne ''}">
									<c:if test="${unidadeFornecMinimoMensal eq unidade.chavePrimaria}">
										<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selFornecimentoMinimoMensal eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selFornecimentoMinimoAnual eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFornecimentoMinimoAnual eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFornecimentoMinimoAnual eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Fornecimento M�nimo Anual:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:out value="${fornecimentoMinimoAnual}"/></span>
					<c:if test="${fornecimentoMinimoDiario ne null && fornecimentoMinimoDiario ne ''}">
						<c:forEach items="${listaUnidadeVolume}" var="unidade">
							<c:choose>
								<c:when test="${unidadeFornecMinimoAnual ne ''}">
									<c:if test="${unidadeFornecMinimoAnual eq unidade.chavePrimaria}">
										<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${selFornecimentoMinimoAnual eq true}">
										<c:if test="${unidade.indicadorPadrao eq true}">
											<span class="itemDetalhamento"><c:out value="${unidade.descricaoAbreviada}"/></span>
										</c:if>
									</c:if>	
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selFatorUnicoCorrecao eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFatorUnicoCorrecao eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Fator �nico de Corre��o de Volume:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:out value="${fatorUnicoCorrecao}"/></span>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selConsumoFatFalhaMedicao eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsumoFatFalhaMedicao eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoConsumoFatFalhaMedicao eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Consumo a ser Faturado em Falha de Medi��o:</td>
				<td class="campoTabela">
					<c:forEach items="${listaAcaoAnormalidadeConsumo}" var="acao">
						<c:if test="${consumoFatFalhaMedicao eq acao.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${acao.descricao}"/></span>
						</c:if>
					</c:forEach>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">PCS:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selLocalAmostragemPCS eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgLocalAmostragemPCS eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Local de Amostragem do PCS:</td>
				<td class="campoTabela">
				<c:if test="${listaLocalAmostragemAssociados.size() > 0}">
					<select id="idsLocalAmostragemAssociados" class="campoList" name="idsLocalAmostragemAssociados" multiple="multiple" disabled="disabled">
						<c:forEach items="${listaLocalAmostragemAssociados}" var="objLocalAmostragemAssoc">
							<option value="<c:out value="${objLocalAmostragemAssoc.chavePrimaria}"/>" title="<c:out value="${objLocalAmostragemAssoc.descricao}"/>">
								<c:out value="${objLocalAmostragemAssoc.descricao}"/>
							</option>
						</c:forEach>
				  	</select>
				</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selIntervaloRecuperacaoPCS eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIntervaloRecuperacaoPCS eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Intervalo para Recupera��o do PCS:</td>
				<td class="campoTabela">
				<c:if test="${listaIntervaloAmostragemAssociados.size() > 0}">
					<select id="idsIntervaloAmostragemAssociados" class="campoList" name="idsIntervaloAmostragemAssociados" multiple="multiple" disabled="disabled">
						<c:forEach items="${listaIntervaloAmostragemAssociados}" var="objIntervaloAssoc">
							<option value="<c:out value="${objIntervaloAssoc.chavePrimaria}"/>" title="<c:out value="${objIntervaloAssoc.descricao}"/>"><c:out value="${objIntervaloAssoc.descricao}"/></option>
						</c:forEach>
				  	</select>
				</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selTamReducaoRecuperacaoPCS eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTamReducaoRecuperacaoPCS eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Tamanho da Redu��o para Recupera��o do PCS:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:out value="${tamReducaoRecuperacaoPCS}"/></span>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>