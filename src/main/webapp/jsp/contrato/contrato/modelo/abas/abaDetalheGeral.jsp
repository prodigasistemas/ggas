<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<fieldset id="Geral">
	<a class="linkHelp" href="<help:help>/abageralinclusoalteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<input name="selAbaGeral" type="hidden" id="selAbaGeral" value="true"/>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado</th>
				<th width="73px">Obrigat�rio</th>
				<th width="73px">Valor Fixo</th>
				<th>Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td></td>
				<td class="rotuloTabela">Cliente Assinatura:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPontoConsumo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPontoConsumo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Ponto Consumo:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPontoConsumo}" var="objPontoConsumo">
						<c:if test="${pontoConsumo eq objPontoConsumo.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPontoConsumo.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Contrato:</td>				
			</tr>
			<tr class="even">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td></td>
				<td class="rotuloTabela">N�mero do Contrato:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${numeroContrato}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selNumeroAnteriorContrato eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumeroAnteriorContrato eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">N�mero Anterior do Contrato:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${numeroAnteriorContrato}"/></span></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selDescricaoContrato eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDescricaoContrato eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Descri��o:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><c:if test="${valorFixoSituacaoContrato eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Situa��o do Contrato:</td>
				<td class="campoTabela">
					<c:forEach items="${listaSituacaoContrato}" var="objSituacaoContrato">
						<c:if test="${situacaoContrato eq objSituacaoContrato.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objSituacaoContrato.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>	
			<tr class="even">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td></td>
				<td class="rotuloTabela">Exige aprova��o?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${exigeAprovacao == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>	
			<tr class="odd">
				<td><c:if test="${selNumeroAditivo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumeroAditivo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">N�mero do Aditivo:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${numeroAditivo}"/></span></td>
			</tr>		
			<tr class="even">
				<td><c:if test="${selDataAditivo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataAditivo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Data do Aditivo:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${dataAditivo}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDescricaoAditivo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDescricaoAditivo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Descri��o do Aditivo:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${descricaoAditivo}"/></span></td>
			</tr>
			<tr class="even">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td></td>
				<td class="rotuloTabela">Data da Assinatura:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${dataAssinatura}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selNumeroEmpenho eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumeroEmpenho eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">N�mero do Empenho:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${numeroEmpenho}</span></td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selDataVencObrigacoesContratuais  eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataVencObrigacoesContratuais eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Data de Vencimento das obriga��es Contratuais:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${dataVencObrigacoesContratuais}"/></span></td>				
			</tr>
			<tr class="odd">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><c:if test="${valorFixoIndicadorAnoContratual eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Defini��o de Ano Contratual:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${indicadorAnoContratual eq true}"><span class="itemDetalhamento">ANO(Fiscal)</span></c:when>
						<c:otherwise><span class="itemDetalhamento">ano(Calend�rio)</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selRenovacaoAutomatica eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgRenovacaoAutomatica eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoRenovacaoAutomatica eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Renova��o Autom�tica do Contrato?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${renovacaoAutomatica == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selNumDiasRenoAutoContrato eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumDiasRenoAutoContrato eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${numDiasRenoAutoContrato eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Per�odo de Renova��o:</td>
				<td class="campoTabela">
					<c:if test="${numDiasRenoAutoContrato ne null &&  numDiasRenoAutoContrato ne ''}">
						<span class="itemDetalhamento">${numDiasRenoAutoContrato}</span>
						<label class="rotuloHorizontal rotuloInformativo">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selTempoAntecedenciaRenovacao eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTempoAntecedenciaRenovacao eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTempoAntecedenciaRenovacao eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tempo de Anteced�ncia para Negocia��o da Renova��o:</td>
				<td class="campoTabela">
					<c:if test="${tempoAntecedenciaRenovacao ne null &&  tempoAntecedenciaRenovacao ne ''}">
						<span class="itemDetalhamento">${tempoAntecedenciaRenovacao}</span>
						<label class="rotuloHorizontal rotuloInformativo">dias</label>
					</c:if>
				</td>
			</tr>		
			<tr class="odd">
				<td><c:if test="${selQDCContrato eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgQDCContrato eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">QDC do Contrato (sazonalidade):</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${QDCContrato}"/></span></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selValorContrato eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgValorContrato eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Valor do Contrato:</td>
				<td class="campoTabela">
					<c:if test="${valorContrato ne null &&  valorContrato ne ''}">
						<span class="itemDetalhamento">R$ <c:out value="${valorContrato}"/></span>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selVolumeReferencia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgVolumeReferencia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Volume de Refer�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="even">
				<td><c:if test="${selPrazoVigencia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPrazoVigencia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Prazo Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>	
			
			<tr class="odd">
				<td><c:if test="${selIncentivosComerciais eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIncentivosComerciais eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Incentivos Comerciais:</td>
				<td class="campoTabela"></td>
			</tr>	
			
			<tr class="even">
				<td><c:if test="${selIncentivosInfraestrutura eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIncentivosInfraestrutura eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Incentivos Infraestrutura:</td>
				<td class="campoTabela"></td>
			</tr>	
													
			<tr class="odd">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkSelecionado1" type="hidden" value="true" name="selDataAssinatura"/></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"><input class="checkObrigatorio1" type="hidden" value="true" name="obgDataAssinatura"/></td>
				<td><c:if test="${valorFixoTipoPeriodicidadePenalidade eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tipo Periodicidade Penalidade:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${tipoPeriodicidadePenalidade eq true}">
							<span class="itemDetalhamento">Civil</span>
						</c:when>
						<c:otherwise>
							<span class="itemDetalhamento">Cont�nuo</span>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Documentos Anexos:</td>				
			</tr>
			<tr class="even">
				<td class="item-selecionado"><c:if test="${selDocumentosAnexos == true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="item-selecionado"><c:if test="${obgDocumentosAnexos == true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="item-selecionado"></td>
				<td class="rotuloTabela">Adicionar anexos</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Proposta:</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selNumeroProposta eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumeroProposta eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">N�mero da Proposta:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${numeroProposta}</span></td>
			</tr>
			<tr class="even">
				<td></td>
				<td></td>
				<td></td>
				<td class="rotuloTabela">Exige aprova��o pr�via da proposta?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${propostaAprovada == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selGastoEstimadoGNMes eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgGastoEstimadoGNMes eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Gasto Mensal com GN:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento">${gastoEstimadoGNMes}</span>
				</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selEconomiaEstimadaGNMes eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEconomiaEstimadaGNMes eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Economia Mensal com GN:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento">${economiaEstimadaGNMes}</span>
				</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selEconomiaEstimadaGNAno eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEconomiaEstimadaGNAno eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Economia Anual com GN:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${economiaEstimadaGNAno}</span></td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selDescontoEfetivoEconomia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDescontoEfetivoEconomia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Desconto Efetivo na Economia:</td>
				<td class="campoTabela">
					<c:if test="${descontoEfetivoEconomia ne null &&  descontoEfetivoEconomia ne ''}">
						<span class="itemDetalhamento">${descontoEfetivoEconomia}</span>
						<label class="rotuloHorizontal rotuloInformativo">%</label>
					</c:if>
				</td>
			</tr>					
			<tr class="odd">
				<td><c:if test="${selDiaVencFinanciamento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDiaVencFinanciamento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td><c:if test="${valorFixoDiaVencFinanciamento eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Dia de vencimento do financiamento:</td>
				<td class="campoTabela">
					<c:if test="${diaVencFinanciamento ne null &&  diaVencFinanciamento ne ''}">
						<span class="itemDetalhamento">${diaVencFinanciamento}</span>
						<label class="rotuloHorizontal rotuloInformativo" for="pressaoManometrica">(01-31)</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selValorInvestimento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgValorInvestimento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Valor do Investimento:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento">${valorInvestimento}</span>
				</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataInvestimento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataInvestimento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Data do Investimento:</td>
				<td class="campoTabela">
					<c:if test="${dataInvestimento ne null &&  dataInvestimento ne ''}">
					<span class="itemDetalhamento">${dataInvestimento}</span>
						<label class="rotuloHorizontal rotuloInformativo"></label>
					</c:if>
				</td>				
			</tr>
			
			<tr class="even">
				<td><c:if test="${selValorParticipacaoCliente eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgValorParticipacaoCliente eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Valor da Participa��o do Cliente:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento">${valorParticipacaoCliente}</span>
				</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selQtdParcelasFinanciamento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgQtdParcelasFinanciamento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Quantidade de Parcelas do Financiamento:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento">${qtdParcelasFinanciamento}</span>
				</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selPercentualJurosFinanciamento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualJurosFinanciamento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Percentual de Juros do Financiamento:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento">${percentualJurosFinanciamento}</span>
				</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selSistemaAmortizacao eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgSistemaAmortizacao eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Sistema de Amortiza��o:</td>
				<td class="campoTabela">
					<c:forEach items="${listaAmortizacoes}" var="objAmortizacao">
						<c:if test="${sistemaAmortizacao == objAmortizacao.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objAmortizacao.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>				
			</tr>
											
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Cobran�a:</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selFaturamentoAgrupamento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFaturamentoAgrupamento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFaturamentoAgrupamento eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Faturamento Agrupamento?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${faturamentoAgrupamento == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
				<tr class="even">
				<td><c:if test="${selTipoAgrupamento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTipoAgrupamento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTipoAgrupamento eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tipo de Agrupamento:</td>
				<td class="campoTabela">
					<c:forEach items="${listaTipoAgrupamento}" var="objTipoAgrupamento">
						<c:if test="${tipoAgrupamento eq objTipoAgrupamento.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objTipoAgrupamento.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>			
			<tr class="odd">
				<td><c:if test="${selEmissaoFaturaAgrupada eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEmissaoFaturaAgrupada eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoEmissaoFaturaAgrupada eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Emiss�o de Fatura Agrupada?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${emissaoFaturaAgrupada == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>	
			<tr class="even">
				<td><c:if test="${selFormaCobranca eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFormaCobranca eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFormaCobranca eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tipo de Conv�nio:</td>
				<td class="campoTabela">
					<c:forEach items="${listaFormaCobranca}" var="objFormaCobranca">
						<c:if test="${formaCobranca eq objFormaCobranca.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objFormaCobranca.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selArrecadadorConvenio eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgArrecadadorConvenio eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoArrecadadorConvenio eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Arrecadador Conv�nio:</td>
				<td class="campoTabela">
					<c:forEach items="${listaArrecadadorConvenio}" var="objArrecadadorConvenio">
						<c:if test="${arrecadadorConvenio eq objArrecadadorConvenio.chavePrimaria}">
							<span class="itemDetalhamento">
								<c:out value="${objArrecadadorConvenio.codigoConvenio} - ${fn:toUpperCase(objArrecadadorConvenio.arrecadadorCarteiraCobranca.tipoCarteira.descricao) == 'SEM REGISTRO' ? 'CN' : 'CR'} - ${objArrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
							</span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selDebitoAutomatico eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDebitoAutomatico eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoDebitoAutomatico eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">D�bito Autom�tico?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${debitoAutomatico == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>				
			</tr>				
			<tr class="odd">
				<td><c:if test="${selArrecadadorConvenioDebitoAutomatico eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgArrecadadorConvenioDebitoAutomatico eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoArrecadadorConvenioDebitoAutomatico eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Arrecadador Conv�nio para D�bito Autom�tico:</td>
				<td class="campoTabela">
					<c:forEach items="${listaArrecadadorConvenioDebitoAutomatico}" var="objArrecadadorConvenioDebitoAutomatico">
						<c:if test="${arrecadadorConvenioDebitoAutomatico eq objArrecadadorConvenioDebitoAutomatico.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objArrecadadorConvenioDebitoAutomatico.codigoConvenio} - ${objArrecadadorConvenioDebitoAutomatico.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" /></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>

			<tr class="even">
				<td><c:if test="${selBanco eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgBanco eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoBanco eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Banco:</td>
				<td class="campoTabela">
					<c:forEach items="${listaBancos}" var="objBanco">
						<c:if test="${banco eq objBanco.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objBanco.nome}"/></span>
						</c:if>
					</c:forEach>
				</td>
			</tr>

			<tr class="odd">
				<td><c:if test="${selAgencia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgAgencia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoAgencia eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Ag�ncia:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${agencia}</span></td>
			</tr>

			<tr class="even">
				<td><c:if test="${selContaCorrente eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgContaCorrente eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoContaCorrente eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Conta Corrente:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${contaCorrente}</span></td>
			</tr>

			<tr class="odd">
				<td><c:if test="${selParticipaECartas eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgParticipaECartas eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoParticipaECartas eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Participa do conv�nio e-cartas?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${participaECartas == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>

			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Garantia Financeira:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selValorGarantiaFinanceira eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgValorGarantiaFinanceira eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoValorGarantiaFinanceira eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td class="rotuloTabela">Valor da Garantia Financeira:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${valorGarantiaFinanceira}</span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDescGarantiaFinanc eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDescGarantiaFinanc eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoDescGarantiaFinanc eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td class="rotuloTabela">Descri��o da Garantia Financeira:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${descGarantiaFinanc}</span></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selTipoGarantiaFinanceira eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTipoGarantiaFinanceira eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTipoGarantiaFinanceira eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td class="rotuloTabela">Tipo da Garantia Financeira:</td>
				<td class="campoTabela">
						<c:forEach items="${listaTipoGarantiaFinanceira}" var="objTipoGarantiaFinanceira">
							<c:if test="${tipoGarantiaFinanceira == objTipoGarantiaFinanceira.chavePrimaria}">
								<span class="itemDetalhamento"><c:out value="${objTipoGarantiaFinanceira.descricao}"/></span>
							</c:if>
						</c:forEach>
				</td>						
			</tr>
			<tr class="odd">
				<td><c:if test="${selGarantiaFinanceiraRenovada eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgGarantiaFinanceiraRenovada eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoGarantiaFinanceiraRenovada eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td class="rotuloTabela">A garantia �?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${garantiaFinanceiraRenovada == true}"><span class="itemDetalhamento">Renov�vel</span></c:when>
						<c:otherwise><span class="itemDetalhamento">Renovada</span></c:otherwise>
					</c:choose>
				</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selDataInicioGarantiaFinanceira eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataInicioGarantiaFinanceira eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>				
				<td class="rotuloTabela">Data de In�cio da Garantia Financeira:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${dataInicioGarantiaFinanceira}</span></td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataFinalGarantiaFinanceira eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataFinalGarantiaFinanceira eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>				
				<td class="rotuloTabela">Data Final da Garantia Financeira:</td>
				<td class="campoTabela"><span class="itemDetalhamento">${dataFinalGarantiaFinanceira}</span></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selTempoAntecRevisaoGarantias eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTempoAntecRevisaoGarantias eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTempoAntecRevisaoGarantias eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tempo de Anteced�ncia para Revis�o das Garantias:</td>
				<td class="campoTabela">
					<c:if test="${tempoAntecRevisaoGarantias ne null &&  tempoAntecRevisaoGarantias ne ''}">
						<span class="itemDetalhamento">${tempoAntecRevisaoGarantias}</span>
						<label class="rotuloHorizontal rotuloInformativo">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPeriodicidadeReavGarantias eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodicidadeReavGarantias eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPeriodicidadeReavGarantias eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Periodicidade para reavalia��o das Garantias:</td>
				<td class="campoTabela">
					<c:if test="${periodicidadeReavGarantias ne null &&  periodicidadeReavGarantias ne ''}">
						<span class="itemDetalhamento">${periodicidadeReavGarantias}</span>
						<label class="rotuloHorizontal rotuloInformativo">dias</label>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Pagamento Efetuado em Atraso:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selIndicadorMultaAtraso eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIndicadorMultaAtraso eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoIndicadorMultaAtraso eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Multa por Atraso?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${indicadorMultaAtraso == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPercentualMulta eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualMulta eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td><c:if test="${valorFixoPercentualMulta eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual Multa por Atraso:</td>
				<td class="campoTabela">
					<c:if test="${percentualMulta ne null &&  percentualMulta ne ''}">
						<span class="itemDetalhamento">${percentualMulta} %</span>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selIndicadorJurosMora eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIndicadorJurosMora eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoIndicadorJurosMora eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Juros de Mora?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${indicadorJurosMora == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPercentualJurosMora eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualJurosMora eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercentualJurosMora eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td class="rotuloTabela">Percentual Multa por Atraso:</td>
				<td class="campoTabela">
					<c:if test="${percentualJurosMora ne null &&  percentualJurosMora ne ''}">
						<span class="itemDetalhamento">${percentualJurosMora} %</span>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selIndiceCorrecaoMonetaria eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIndiceCorrecaoMonetaria eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoIndiceCorrecaoMonetaria eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				
				<td class="rotuloTabela">�ndice de Corre��o Monet�ria:</td>
				<td class="campoTabela">
					<c:forEach items="${listaIndiceMonetario}" var="objIndiceCorrecaoMonetaria">
						<c:if test="${indiceCorrecaoMonetaria eq objIndiceCorrecaoMonetaria.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objIndiceCorrecaoMonetaria.descricaoAbreviada}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Estimativa de Quantidade a Recuperar:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selDataInicioRetiradaQPNRC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataInicioRetiradaQPNRC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Data para In�cio da Recupera��o:</td>
				<td class="campoTabela">
					<span id="detalhamentoDataIncioRetiradaQPNRC" class="itemDetalhamento"><c:out value="${dataInicioRetiradaQPNRC}"/></span>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataFimRetiradaQPNRC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataFimRetiradaQPNRC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Data para Fim da Recupera��o:</td>
				<td class="campoTabela">
					<span id="detalhamentoDataFimRetiradaQPNRC" class="itemDetalhamento"><c:out value="${dataFimRetiradaQPNRC}"/></span>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPercMinDuranteRetiradaQPNRC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercMinDuranteRetiradaQPNRC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercMinDuranteRetiradaQPNRC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual M�nimo em rela��o � QDC para Recupera��o:</td>
				<td class="campoTabela">
					<c:if test="${percMinDuranteRetiradaQPNRC ne null &&  percMinDuranteRetiradaQPNRC ne ''}">
						<span id="detalhamentoPercentualRetiradaQPNRduranteContratoC" class="itemDetalhamento"><c:out value="${percMinDuranteRetiradaQPNRC}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRduranteContratoC">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPercDuranteRetiradaQPNRC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercDuranteRetiradaQPNRC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercDuranteRetiradaQPNRC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual M�ximo em rela��o � QDC para Recupera��o:</td>
				<td class="campoTabela">
					<c:if test="${percDuranteRetiradaQPNRC ne null &&  percDuranteRetiradaQPNRC ne ''}">
						<span id="detalhamentoPercentualRetiradaQPNRduranteContratoC" class="itemDetalhamento"><c:out value="${percDuranteRetiradaQPNRC}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRduranteContratoC">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPercFimRetiradaQPNRC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercFimRetiradaQPNRC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercFimRetiradaQPNRC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual M�ximo para Recupera��o com o t�rmino do Contrato:</td>
				<td class="campoTabela">
					<c:if test="${percFimRetiradaQPNRC ne null &&  percFimRetiradaQPNRC ne ''}">
						<span id="detalhamentoPercentualRetiradaQPNRfimContratoC" class="itemDetalhamento"><c:out value="${percFimRetiradaQPNRC}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRfimContratoC">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selTempoValidadeRetiradaQPNRC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTempoValidadeRetiradaQPNRC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTempoValidadeRetiradaQPNRC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tempo de Validade para Recupera��o da QPNR do Ano Anterior:</td>
				<td class="campoTabela">
					<c:if test="${tempoValidadeRetiradaQPNRC ne null &&  tempoValidadeRetiradaQPNRC ne ''}">
						<span id="detalhamentoTempoValidadeRetiradaQPNRC" class="itemDetalhamento"><c:out value="${tempoValidadeRetiradaQPNRC}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRfimContratoC">anos</label>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Penalidades por Retirada a Maior/Menor:</td>				
			</tr>
			
			<tr class="even">
				<td><c:if test="${selPenalidadeRetMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPenalidadeRetMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPenalidadeRetMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Penalidade:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="objPenalidadeRetMaiorMenor">
						<c:if test="${penalidadeRetMaiorMenor eq objPenalidadeRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPenalidadeRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selPeriodicidadeRetMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodicidadeRetMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPeriodicidadeRetMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Periodicidade:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="objPeriodicidadeRetMaiorMenor">
						<c:if test="${periodicidadeRetMaiorMenor eq objPeriodicidadeRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPeriodicidadeRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="even">
				<td><c:if test="${selBaseApuracaoRetMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgBaseApuracaoRetMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoBaseApuracaoRetMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Base de Apura��o:</td>
				<td class="campoTabela">
					<c:forEach items="${listaBaseApuracao}" var="objBaseApuracaoRetMaiorMenor">
						<c:if test="${baseApuracaoRetMaiorMenor eq objBaseApuracaoRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objBaseApuracaoRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataIniVigRetirMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataIniVigRetirMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">In�cio de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selDataFimVigRetirMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataFimVigRetirMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Fim de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPrecoCobrancaRetirMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPrecoCobrancaRetirMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPrecoCobrancaRetirMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPrecoCobranca}" var="objPrecoCobranca">
						<c:if test="${precoCobrancaRetirMaiorMenor eq objPrecoCobranca.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPrecoCobranca.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			<tr class="even">
				<td><c:if test="${selTipoApuracaoRetirMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTipoApuracaoRetirMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTipoApuracaoRetirMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de C�lculo para Cobran�a:</td>
				<td class="campoTabela">
					<c:forEach items="${listaTipoApuracao}" var="objTipoApuracao">
						<c:if test="${tipoApuracaoRetirMaiorMenor eq objTipoApuracao.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objTipoApuracao.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selPercentualCobRetMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualCobRetMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercentualCobRetMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual de Cobran�a:</td>
				<td class="campoTabela">
					<c:if test="${percentualCobRetMaiorMenor ne null &&  percentualCobRetMaiorMenor ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualCobRetMaiorMenor}"/>%</span>
					</c:if>				
				</td>
			</tr>	
			
			<tr class="even">
				<td><c:if test="${selPercentualCobIntRetMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualCobIntRetMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercentualCobIntRetMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual de Cobran�a para Consumo Superior Durante Aviso de Interrup��o:</td>
				<td class="campoTabela">
					<c:if test="${percentualCobIntRetMaiorMenor ne null &&  percentualCobIntRetMaiorMenor ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualCobIntRetMaiorMenor}"/>%</span>
					</c:if>				
				</td>
			</tr>	
			<tr class="odd">
				<td><c:if test="${selConsumoReferenciaRetMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsumoReferenciaRetMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoConsumoReferenciaRetMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Consumo Refer�ncia:</td>
				<td class="campoTabela">
					<c:forEach items="${listaConsumoReferencial}" var="objConsumoReferenciaRetMaiorMenor">
						<c:if test="${consumoReferenciaRetMaiorMenor eq objConsumoReferenciaRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objConsumoReferenciaRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="even">
				<td><c:if test="${selPercentualRetMaiorMenor eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualRetMaiorMenor eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercentualRetMaiorMenor eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual Por Retirada:</td>
				<td class="campoTabela">
					<c:if test="${percentualRetMaiorMenor ne null &&  percentualRetMaiorMenor ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualRetMaiorMenor}"/>%</span>
					</c:if>				
				</td>
			</tr>

			<tr class="even">
				<td><c:if test="${selIndicadorImposto eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIndicadorImposto eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoIndicadorImposto eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Usar pre�os s/ impostos para cobran�a:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${indicadorImposto == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>				
			
	<!-- 	FIM   	-->
<!-- 			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr> -->
<!-- 			<tr> -->
<!-- 				<td class="tituloGrupoTabela" colspan="4">Penalidades por Retirada a maior:</td>				 -->
<!-- 			</tr> -->
<!-- 			<tr class="even"> -->
<%-- 				<td><c:if test="${selConsumoReferenciaSobreDem eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<%-- 				<td><c:if test="${obgConsumoReferenciaSobreDem eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<!-- 				<td class="rotuloTabela">Consumo Referencial para Penalidade por Retirada a maior que:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<c:forEach items="${listaConsumoReferencial}" var="objConsumoReferenciaSobreDem"> --%>
<%-- 						<c:if test="${consumoReferenciaSobreDem eq objConsumoReferenciaSobreDem.chavePrimaria}"> --%>
<%-- 							<span class="itemDetalhamento"><c:out value="${objConsumoReferenciaSobreDem.descricao}"/></span> --%>
<%-- 						</c:if>	 --%>
<%-- 					</c:forEach> --%>
<!-- 				</td>		 -->
<!-- 			</tr> -->
<!-- 			<tr class="odd"> -->
<%-- 				<td><c:if test="${selFaixaPenalidadeSobreDem eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<%-- 				<td><c:if test="${obgFaixaPenalidadeSobreDem eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<!-- 				<td class="rotuloTabela">Valor ou Faixa Penalidade por Retirada a Maior:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<c:forEach items="${listaFaixaPenalidadeSobreDem}" var="objFaixaPenalidadeSobreDem"> --%>
<%-- 						<c:if test="${faixaPenalidadeSobreDem eq objFaixaPenalidadeSobreDem.chavePrimaria}"> --%>
<%-- 							<span class="itemDetalhamento"><c:out value="${objFaixaPenalidadeSobreDem.descricao}"/></span> --%>
<%-- 						</c:if>	 --%>
<%-- 					</c:forEach> --%>
<!-- 				</td>		 -->
<!-- 			</tr> -->
<!-- 			<tr class="even"> -->
<%-- 				<td><c:if test="${selPercentualReferenciaSobreDem eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<%-- 				<td><c:if test="${obgPercentualReferenciaSobreDem eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				 --%>
<!-- 				<td class="rotuloTabela">Percentual por Retirada a Maior:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<c:if test="${percentualReferenciaSobreDem ne null &&  percentualReferenciaSobreDem ne ''}"> --%>
<%-- 						<span class="itemDetalhamento">${percentualReferenciaSobreDem}</span> --%>
<!-- 						<label class="rotuloHorizontal rotuloInformativo" for="percentualSobreTariGas">%</label> -->
<%-- 					</c:if> --%>
<!-- 				</td> -->
<!-- 			</tr> -->
<!-- 			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr> -->
<!-- 			<tr> -->
<!-- 				<td class="tituloGrupoTabela" colspan="4">Penalidades por Retirada a menor:</td>				 -->
<!-- 			</tr> -->
<!-- 			<tr class="even"> -->
<%-- 				<td><c:if test="${selConsumoReferenciaSobDem eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<%-- 				<td><c:if test="${obgConsumoReferenciaSobDem eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<!-- 				<td class="rotuloTabela">Consumo Referencial para Penalidade por Retirada a menor que:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<c:forEach items="${listaConsumoReferencial}" var="objConsumoReferenciaSobDem"> --%>
<%-- 						<c:if test="${consumoReferenciaSobDem eq objConsumoReferenciaSobDem.chavePrimaria}"> --%>
<%-- 							<span class="itemDetalhamento"><c:out value="${objConsumoReferenciaSobDem.descricao}"/></span> --%>
<%-- 						</c:if>	 --%>
<%-- 					</c:forEach> --%>
<!-- 				</td>		 -->
<!-- 			</tr> -->
<!-- 			<tr class="odd"> -->
<%-- 				<td><c:if test="${selFaixaPenalidadeSobDem eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<%-- 				<td><c:if test="${obgFaixaPenalidadeSobDem eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<!-- 				<td class="rotuloTabela">Valor ou Faixa Penalidade por Retirada a Menor:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<c:forEach items="${listaFaixaPenalidadeSobDem}" var="objFaixaPenalidadeSobDem"> --%>
<%-- 						<c:if test="${faixaPenalidadeSobDem eq objFaixaPenalidadeSobDem.chavePrimaria}"> --%>
<%-- 							<span class="itemDetalhamento"><c:out value="${objFaixaPenalidadeSobDem.descricao}"/></span> --%>
<%-- 						</c:if>	 --%>
<%-- 					</c:forEach> --%>
<!-- 				</td>		 -->
<!-- 			</tr>	 -->
<!-- 			<tr class="even"> -->
<%-- 				<td><c:if test="${selPercentualReferenciaSobDem eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td> --%>
<%-- 				<td><c:if test="${obgPercentualReferenciaSobDem eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>				 --%>
<!-- 				<td class="rotuloTabela">Percentual por Retirada a Menor:</td> -->
<!-- 				<td class="campoTabela"> -->
<%-- 					<c:if test="${percentualReferenciaSobDem ne null &&  percentualReferenciaSobDem ne ''}"> --%>
<%-- 						<span class="itemDetalhamento">${percentualReferenciaSobDem}</span> --%>
<!-- 						<label class="rotuloHorizontal rotuloInformativo" for="percentualSobreTariGas">%</label> -->
<%-- 					</c:if> --%>
<!-- 				</td> -->
<!-- 			</tr> -->
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Penalidades por Delivery Or Pay:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selPercentualTarifaDoP eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualTarifaDoP eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercentualTarifaDoP eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual Sobre a Tarifa do G�s:</td>
				<td class="campoTabela">
					<c:if test="${percentualTarifaDoP ne null &&  percentualTarifaDoP ne ''}">
						<span id="detalhamentoMargemVariacaoToP" class="itemDetalhamento"><c:out value="${percentualTarifaDoP}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualTarifaDoP">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Penalidades por G�s Fora de Especifica��o:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selPercentualSobreTariGas eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualSobreTariGas eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercentualSobreTariGas eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual por G�s Fora de Especifica��o:</td>
				<td class="campoTabela">
					<c:if test="${percentualSobreTariGas ne null &&  percentualSobreTariGas ne ''}">
						<span id="detalhamentoPercentualSobreTariGas" class="itemDetalhamento"><c:out value="${percentualSobreTariGas}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualSobreTariGas">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Multa Rescis�ria:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selMultaRecisoria eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMultaRecisoria eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoMultaRecisoria eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Multa Rescis�ria:</td>
				<td class="campoTabela">
					<select name="multaRecisoria" id="multaRecisoria" class="campoSelect" disabled="disabled">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoMultaRecisoria}" var="objMultaRecisoria">
							<option value="<c:out value="${objMultaRecisoria.chavePrimaria}"/>" 
								<c:if test="${multaRecisoria == objMultaRecisoria.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${objMultaRecisoria.descricao}"/>
							</option>		
						</c:forEach>
					</select>				
				</td>
			</tr>
			<tr class="odd">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td></td>
				<td></td>
				<td class="rotuloTabela">Data da Recis�o:</td>
				<td class="campoTabela">
					<c:if test="${dataRecisao ne null &&  dataRecisao ne ''}">
						<span id="detalhamentoDataRecisao" class="itemDetalhamento"><c:out value="${dataRecisao}"/></span>						
					</c:if>
				</td>
			</tr>
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Take or Pay:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selPeriodicidadeTakeOrPayC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodicidadeTakeOrPayC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPeriodicidadeTakeOrPayC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Periodicidade ToP:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeTakeOrPay">
						<c:if test="${periodicidadeTakeOrPayC eq periodicidadeTakeOrPay.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${periodicidadeTakeOrPay.descricao}"/></span>
						</c:if>	
					</c:forEach>		
				</td>	
			</tr>
			<tr class="odd">
				<td><c:if test="${selReferenciaQFParadaProgramadaC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgReferenciaQFParadaProgramadaC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoReferenciaQFParadaProgramadaC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Refer�ncia Quantidade Faltante Parada Programada:</td>
				<td class="campoTabela">
					<c:forEach items="${listaReferenciaQFParadaProgramada}" var="objReferenciaQFParadaProgramada">
						<c:if test="${referenciaQFParadaProgramadaC eq objReferenciaQFParadaProgramada.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objReferenciaQFParadaProgramada.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>			
			<tr class="even">
				<td><c:if test="${selConsumoReferenciaTakeOrPayC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsumoReferenciaTakeOrPayC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoConsumoReferenciaTakeOrPayC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Consumo Referencial para ToP:</td>
				<td class="campoTabela">
					<c:forEach items="${listaConsumoReferencial}" var="objConsumoReferenciaTakeOrPay">
						<c:if test="${consumoReferenciaTakeOrPayC eq objConsumoReferenciaTakeOrPay.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objConsumoReferenciaTakeOrPay.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			<tr class="odd">
				<td><c:if test="${selMargemVariacaoTakeOrPayC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMargemVariacaoTakeOrPayC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoMargemVariacaoTakeOrPayC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Compromisso de Retirada ToP:</td>
				<td class="campoTabela">
					<c:if test="${margemVariacaoTakeOrPayC ne null &&  margemVariacaoTakeOrPayC ne ''}">
						<span id="detalhamentoMargemVariacaoToP" class="itemDetalhamento"><c:out value="${margemVariacaoTakeOrPayC}"/>%</span>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selDataInicioVigenciaC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataInicioVigenciaC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Data In�cio de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataFimVigenciaC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataFimVigenciaC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Data Fim de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPrecoCobrancaC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPrecoCobrancaC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPrecoCobrancaC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPrecoCobranca}" var="objPrecoCobranca">
						<c:if test="${precoCobrancaC eq objPrecoCobranca.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPrecoCobranca.descricao}"/></span>
						</c:if>	
					</c:forEach>		
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selTipoApuracaoC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTipoApuracaoC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTipoApuracaoC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de C�lculo para Recupera��o:</td>
				<td class="campoTabela">
					<c:forEach items="${listaTipoApuracao}" var="objTipoApuracao">
						<c:if test="${tipoApuracaoC eq objTipoApuracao.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objTipoApuracao.descricao}"/></span>
						</c:if>	
					</c:forEach>	
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selConsideraParadaProgramadaC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsideraParadaProgramadaC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoConsideraParadaProgramadaC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Considerar Paradas Programadas:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${consideraParadaProgramadaC == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selConsideraFalhaFornecimentoC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsideraFalhaFornecimentoC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoConsideraFalhaFornecimentoC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Considerar Falhas de Fornecimento:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${consideraFalhaFornecimentoC == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selConsideraCasoFortuitoC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsideraCasoFortuitoC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoConsideraCasoFortuitoC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Considerar Casos Fortuitos:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${consideraCasoFortuitoC == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selRecuperavelC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgRecuperavelC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoRecuperavelC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Recuper�vel:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${recuperavelC == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPercentualNaoRecuperavelC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualNaoRecuperavelC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoPercentualNaoRecuperavelC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual N�o Recuper�vel:</td>
				<td class="campoTabela">
					<c:if test="${percentualNaoRecuperavelC ne null &&  percentualNaoRecuperavelC ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualNaoRecuperavelC}"/>%</span>
					</c:if>				
				</td>
			</tr>	
			<tr class="odd">
				<td><c:if test="${selApuracaoParadaNaoProgramada eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgApuracaoParadaNaoProgramada eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoApuracaoParadaProgramadaC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume de Parada N�o Programada:</td>
				<td class="campoTabela">
					<c:forEach items="${listaApuracaoParadaNaoProgramada}" var="objApuracaoParadaNaoProgramada">
						<c:if test="${apuracaoParadaNaoProgramada eq objApuracaoParadaNaoProgramada.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objApuracaoParadaNaoProgramada.descricao}"/></span>
						</c:if>	
					</c:forEach>					
				</td>
			</tr>							
			<tr class="even">
				<td><c:if test="${selApuracaoCasoFortuitoC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgApuracaoCasoFortuitoC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoApuracaoCasoFortuitoC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume do Caso Fortuito ou For�a Maior:</td>
				<td class="campoTabela">
					<c:forEach items="${listaApuracaoCasoFortuito}" var="objApuracaoCasoFortuito">
						<c:if test="${apuracaoCasoFortuitoC eq objApuracaoCasoFortuito.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objApuracaoCasoFortuito.descricao}"/></span>
						</c:if>	
					</c:forEach>				
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selApuracaoFalhaFornecimentoC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgApuracaoFalhaFornecimentoC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoApuracaoFalhaFornecimentoC eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume de Falha de Fornecimento:</td>
				<td class="campoTabela">
					<c:forEach items="${listaApuracaoFalhaFornecimento}" var="objApuracaoFalhaFornecimento">
						<c:if test="${apuracaoFalhaFornecimentoC eq objApuracaoFalhaFornecimento.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objApuracaoFalhaFornecimento.descricao}"/></span>
						</c:if>	
					</c:forEach>					
				</td>
			</tr>
			
		</tbody>
	</table>
</fieldset>
