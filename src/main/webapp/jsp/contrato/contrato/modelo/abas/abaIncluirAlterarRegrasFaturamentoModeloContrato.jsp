<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<!--[if lt IE 10]>
    <style type="text/css">
    	.confirmacaoAutomaticaQDSCss{padding:7px 5px 0px 0px;}
		.indicadorImpostoCSS{padding:7px 5px 0px 0px;}
		.recuperacaoAutoTakeOrCss{padding:7px 5px 0px 0px;}
        .recuperacaoAutoTakeOrPayCss{padding:7px 5px 0px 0px;}
    </style>
<![endif]-->

<script>
function configurarCampoVariacaoSuperiorQDC(sel){
	var campo = sel.options[sel.selectedIndex].value;
	var campoVariacaoSuperiorQDC = document.forms[0].variacaoSuperiorQDC;
	if (campo == 'true'){
		campoVariacaoSuperiorQDC.disabled = false;
	}else {
		campoVariacaoSuperiorQDC.disabled = true;
		campoVariacaoSuperiorQDC.value = '';
	}
}

	function controlarCheckBoxObrigatorios6(checkObrigatorios) {
			
		var valorCheck = checkObrigatorios.checked;

		

		var camposSelecionados = new Array('selModalidade','selQdc','selDataVigenciaQDC','selPrazoRevizaoQDC','selQdsMaiorQDC','selDataInicioRetiradaQPNR','selPeriodicidadeTakeOrPay','selPeriodicidadeShipOrPay');
		var camposObrigatorios = new Array('obgModalidade','obgQdc','obgDataVigenciaQDC','obgPrazoRevizaoQDC','obgQdsMaiorQDC','obgDataInicioRetiradaQPNR','obgPeriodicidadeTakeOrPay','obgPeriodicidadeShipOrPay');

		var camposObrigatoriosSemSelecionados = new Array('obgVariacaoSuperiorQDC','obgRecuperacaoAutoTakeOrPay','obgPercentualNaoRecuperavel','obgConsideraParadaProgramada','obgConsideraParadaNaoProgramada','obgConsideraFalhaFornecimento','obgConsideraCasoFortuito','obgRecuperavel');
		
		for (var i = 0; i < camposObrigatoriosSemSelecionados.length; i++) {
			var campoSelecionado = document.getElementById(camposObrigatoriosSemSelecionados[i]).checked;
			if(document.getElementById(camposObrigatoriosSemSelecionados[i]) != null){
				if (campoSelecionado != null && valorCheck == true) {
					document.getElementById(camposObrigatoriosSemSelecionados[i]).checked = true;
				} else {
					document.getElementById(camposObrigatoriosSemSelecionados[i]).checked = false;
				}			
			}
		}

		
		marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, valorCheck);
		
	}
	
	function limparAbaRegrasFaturamentoModeloContrato(){
		document.getElementsByName('confirmacaoAutomaticaQDS')[1].checked = true;
		document.getElementsByName('consideraParadaProgramada')[1].checked = true;
		//document.getElementsByName('consideraParadaNaoProgramada')[1].checked = true;
		document.getElementsByName('consideraFalhaFornecimento')[1].checked = true;
		document.getElementsByName('consideraCasoFortuito')[1].checked = true;
		document.getElementsByName('recuperavel')[1].checked = true;
	}
	
</script>

		<fieldset id="regrasFaturamento">
			<a class="linkHelp" href="<help:help>/abamodalidadeconsumofaturamentoinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
			<table class="dataTableGGAS dataTableAba">
				<thead>
					<tr>
						<th width="110px">Selecionado<input id="checkAllSelecionado6" type="checkbox" name="todos" /></th>
						<th width="110px">Obrigat�rio<input id="checkAllObrigatorio6" type="checkbox" name="todos" onclick="controlarCheckBoxObrigatorios6(this);" /></th>
						<th width="460px">Nome do Campo</th>
						<th>Valor Padr�o</th>
					</tr>
				</thead>
				<tbody>
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
					<tr class="even">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selModalidade" name="selModalidade" onclick="permitirSelecaoObrigatorio('selModalidade', 'obgModalidade');" <c:if test="${modeloContratoVO.selModalidade == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgModalidade" name="obgModalidade" onclick="verificarSelecaoObrigatorio('obgModalidade', 'selModalidade');" <c:if test="${modeloContratoVO.obgModalidade == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Modalidade:</td>
						<td class="campoTabela">							
							<select name="modalidade" id="modalidade" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaContratoModalidade}" var="modalidadeContrato">
									<option value="<c:out value="${modalidadeContrato.chavePrimaria}"/>" <c:if test="${modeloContratoVO.modalidade == modalidadeContrato.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${modalidadeContrato.descricao}"/>
									</option>		
								</c:forEach>
							</select>							
						</td>
					</tr>
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="4">QDC:</td>				
					</tr>
					<tr class="even">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selQdc" name="selQdc" onclick="permitirSelecaoObrigatorio('selQdc', 'obgQdc');" <c:if test="${modeloContratoVO.selQdc == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgQdc" name="obgQdc" onclick="verificarSelecaoObrigatorio('obgQdc', 'selQdc');" <c:if test="${modeloContratoVO.obgQdc == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">QDC:</td>
						<td class="campoTabela"></td>
					</tr>
					
					<tr class="odd">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selDataVigenciaQDC" name="selDataVigenciaQDC" onclick="permitirSelecaoObrigatorio('selDataVigenciaQDC', 'obgDataVigenciaQDC');" <c:if test="${modeloContratoVO.selDataVigenciaQDC == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgDataVigenciaQDC" name="obgDataVigenciaQDC" onclick="verificarSelecaoObrigatorio('obgDataVigenciaQDC', 'selDataVigenciaQDC');" <c:if test="${modeloContratoVO.obgDataVigenciaQDC == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Data de Vig�ncia da QDC:</td>
						<td class="campoTabela"></td>						
					</tr>
					
					<tr class="even">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selPrazoRevizaoQDC" name="selPrazoRevizaoQDC" onclick="permitirSelecaoObrigatorio('selPrazoRevizaoQDC', 'obgPrazoRevizaoQDC');" <c:if test="${modeloContratoVO.selPrazoRevizaoQDC == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgPrazoRevizaoQDC" name="obgPrazoRevizaoQDC" onclick="verificarSelecaoObrigatorio('obgPrazoRevizaoQDC', 'selPrazoRevizaoQDC');" <c:if test="${modeloContratoVO.obgPrazoRevizaoQDC == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Prazo para revis�o das quantidades contratadas:</td>
						<td class="campoTabela"></td>						
					</tr>
					
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="4">QDS:</td>				
					</tr>
					
					
					
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selIndicadorProgramacaoConsumo" name="selIndicadorProgramacaoConsumo" onclick="permitirSelecaoObrigatorio('selIndicadorProgramacaoConsumo', 'obgIndicadorProgramacaoConsumo');" <c:if test="${modeloContratoVO.selIndicadorProgramacaoConsumo == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgIndicadorProgramacaoConsumo" name="obgIndicadorProgramacaoConsumo" onclick="verificarSelecaoObrigatorio('obgIndicadorProgramacaoConsumo', 'selIndicadorProgramacaoConsumo');" <c:if test="${modeloContratoVO.obgIndicadorProgramacaoConsumo == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Efetua programa��o de consumo?</td>
						<td class="campoTabela">
							<select  name="indicadorProgramacaoConsumo" class="campoSelect"  id="indicadorProgramacaoConsumo" >
								<option value="true" id="indicadorProgramacaoConsumoSim"  <c:if test="${modeloContratoVO.indicadorProgramacaoConsumo == 'true'}">selected="selected"</c:if>>Sim</option>
								<option value="false" id="indicadorProgramacaoConsumoNao" <c:if test="${modeloContratoVO.indicadorProgramacaoConsumo == 'false'}">selected="selected"</c:if>>N�o</option>
						</select>
						</td>						
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td><td class="rotuloTabela">QDS poder ser maior que QDC?</td>
						<td class="campoTabela">
							<select  name="qdsMaiorQDC" class="campoSelect"  id="qdsMaiorQDC" onchange="configurarCampoVariacaoSuperiorQDC(this);">
								<option value="true" id="qdsMaiorQDCSim"  <c:if test="${modeloContratoVO.qdsMaiorQDC == 'true'}">selected="selected"</c:if>>Sim</option>
								<option value="false" id="qdsMaiorQDCNao" <c:if test="${modeloContratoVO.qdsMaiorQDC == 'false'}">selected="selected"</c:if>>N�o</option>
							</select>
						</td>						
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="rotuloTabela">Percentual de Toler�ncia Di�ria Superior a QDC:</td>
						<td class="campoTabela">
							<input id="variacaoSuperiorQDC" class="campoTexto campoHorizontal" 
								type="text" size="4" maxlength="6" name="variacaoSuperiorQDC" 
								value="${modeloContratoVO.variacaoSuperiorQDC}" 
								<c:if test="${modeloContratoVO.qdsMaiorQDC == false 
										or empty modeloContratoVO.qdsMaiorQDC}">disabled="disabled"</c:if> 
									onblur="aplicarMascaraNumeroDecimal(this,2);" 
									onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" />
							<label class="rotuloHorizontal rotuloInformativo" for="variacaoSuperiorQDC">%</label>
						</td>
					</tr>
										
<!-- 					<tr class="even semLinhaFilho"> -->
<%-- 						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" styleId="obgDiasAntecSolicConsumo" name="obgDiasAntecSolicConsumo" value="true" campoPai="indicadorProgramacaoConsumo" /></td> --%>
<!-- 						<td class="rotuloTabela">Dias de Anteced�ncia para Solicita��o de Consumo:</td> -->
<!-- 						<td class="campoTabela"> -->
<%-- 							<input id="diasAntecSolicConsumo" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="diasAntecSolicConsumo" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.diasAntecSolicConsumo}" /> --%>
<!-- 							<label class="rotuloHorizontal rotuloInformativo" for="diasAntecSolicConsumo">dias</label>						 -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr class="even semLinhaFilho"> -->
<%-- 						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td> --%>
<%-- 						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgNumMesesSolicConsumo" value="true" campoPai="qdsMaiorQDC" /></td> --%>
<!-- 						<td class="rotuloTabela">N�mero de Meses para Solicita��o de Consumo:</td> -->
<!-- 						<td class="campoTabela"> -->
<%-- 							<input id="numMesesSolicConsumo" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="numMesesSolicConsumo" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.numMesesSolicConsumo}" /> --%>
<!-- 							<label class="rotuloHorizontal rotuloInformativo" for="numMesesSolicConsumo">meses</label>		 -->
<!-- 						</td>						 -->
<!-- 					</tr>	 -->
					
					
					
						<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgHoraLimiteProgramacaoDiaria" value="true" campoPai="indicadorProgramacaoConsumo" /></td>
						<td class="rotuloTabela">Hor�rio Limite Programa��o Di�ria:</td>
						<td class="campoTabela">
							<input id="horaLimiteProgramacaoDiaria" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="horaLimiteProgramacaoDiaria" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.horaLimiteProgramacaoDiaria}" />
							<label class="rotuloHorizontal rotuloInformativo" for="horaLimiteProgramacaoDiaria">horas</label>		
						</td>						
					</tr>
					
						<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgHoraLimiteProgIntradiaria" value="true" campoPai="indicadorProgramacaoConsumo" /></td>
						<td class="rotuloTabela">Hor�rio Limite  Programa��o Intradi�ria:</td>
						<td class="campoTabela">
							<input id="horaLimiteProgIntradiaria" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="horaLimiteProgIntradiaria" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.horaLimiteProgIntradiaria}" />
							<label class="rotuloHorizontal rotuloInformativo" for="horaLimiteProgIntradiaria">horas</label>		
						</td>						
					</tr>
					
						<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgHoraLimiteAceitacaoDiaria" value="true" campoPai="indicadorProgramacaoConsumo" /></td>
						<td class="rotuloTabela">Hor�rio Limite Aceita��o Di�ria:</td>
						<td class="campoTabela">
							<input id="horaLimiteAceitacaoDiaria" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="horaLimiteAceitacaoDiaria" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.horaLimiteAceitacaoDiaria}" />
							<label class="rotuloHorizontal rotuloInformativo" for="horaLimiteAceitacaoDiaria">horas</label>		
						</td>						
					</tr>
					
						<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgHoraLimiteAceitIntradiaria" value="true" campoPai="indicadorProgramacaoConsumo" /></td>
						<td class="rotuloTabela">Hor�rio Limite  Aceita��o Intradi�ria:</td>
						<td class="campoTabela">
							<input id="horaLimiteAceitIntradiaria" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="horaLimiteAceitIntradiaria" onkeypress="return formatarCampoInteiro(event);" value="${modeloContratoVO.horaLimiteAceitIntradiaria}" />
							<label class="rotuloHorizontal rotuloInformativo" for="horaLimiteAceitIntradiaria">horas</label>		
						</td>						
					</tr>
					
					
					
					
					
					
					
					
									
					<tr class="even">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgConfirmacaoAutomaticaQDS" value="true" campoPai="qdsMaiorQDC" /></td>
						<td class="rotuloTabela">Confirma��o Autom�tica da QDS?</td>
						<td class="campoTabela">
							<input id="confirmacaoAutomaticaQDSSim" class="campoRadio confirmacaoAutomaticaQDSCss" type="radio" value="true" name="confirmacaoAutomaticaQDS" <c:if test="${modeloContratoVO.confirmacaoAutomaticaQDS == true}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="confirmacaoAutomaticaQDSSim">Sim</label>
							<input id="confirmacaoAutomaticaQDSNao" class="campoRadio confirmacaoAutomaticaQDSCss" type="radio" value="false" name="confirmacaoAutomaticaQDS" <c:if test="${modeloContratoVO.confirmacaoAutomaticaQDS == false or empty modeloContratoVO.confirmacaoAutomaticaQDS}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="confirmacaoAutomaticaQDSNao">N�o</label>
						</td>
					</tr>
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="4">Estimativa de Quantidade a Recuperar:</td>				
					</tr>
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selDataInicioRetiradaQPNR" name="selDataInicioRetiradaQPNR" onclick="permitirSelecaoObrigatorio('selDataInicioRetiradaQPNR', 'obgDataInicioRetiradaQPNR');" <c:if test="${modeloContratoVO.selDataInicioRetiradaQPNR == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgDataInicioRetiradaQPNR" name="obgDataInicioRetiradaQPNR" onclick="verificarSelecaoObrigatorio('obgDataInicioRetiradaQPNR', 'selDataInicioRetiradaQPNR');" <c:if test="${modeloContratoVO.obgDataInicioRetiradaQPNR == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Data para In�cio da Recupera��o:</td>
						<td class="campoTabela"></td>					
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgDataFimRetiradaQPNR" value="true" campoPai="dataInicioRetiradaQPNR" /></td>
						<td class="rotuloTabela">Data para Fim da Recupera��o:</td>
						<td class="campoTabela"></td>					
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercMinDuranteRetiradaQPNR" value="true" campoPai="dataInicioRetiradaQPNR" /></td>
						<td class="rotuloTabela">Percentual M�nimo em rela��o � QDC para Recupera��o:</td>
						<td class="campoTabela">
							<input id="percMinDuranteRetiradaQPNR" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" name="percMinDuranteRetiradaQPNR" value="${modeloContratoVO.percMinDuranteRetiradaQPNR }" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);"/>
							<label class="rotuloHorizontal rotuloInformativo" for="percMinDuranteRetiradaQPNR">%</label>
						</td>					
					</tr>
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercDuranteRetiradaQPNR" value="true" campoPai="dataInicioRetiradaQPNR" /></td>
						<td class="rotuloTabela">Percentual M�ximo em rela��o � QDC para Recupera��o:</td>
						<td class="campoTabela">
							<input id="percDuranteRetiradaQPNR" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" name="percDuranteRetiradaQPNR" value="${modeloContratoVO.percDuranteRetiradaQPNR }" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);"/>
							<label class="rotuloHorizontal rotuloInformativo" for="percDuranteRetiradaQPNR">%</label>
						</td>					
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercFimRetiradaQPNR" value="true" campoPai="dataInicioRetiradaQPNR" /></td>
						<td class="rotuloTabela">Percentual M�ximo para Recupera��o com o t�rmino do Contrato:</td>
						<td class="campoTabela">
							<input id="percFimRetiradaQPNR" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" name="percFimRetiradaQPNR" value="${modeloContratoVO.percFimRetiradaQPNR}" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" />
							<label class="rotuloHorizontal rotuloInformativo" for="percFimRetiradaQPNR">%</label>
						</td>					
					</tr>
					
					<tr class="even">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercFimRetiradaQPNR" value="true" campoPai="dataInicioRetiradaQPNR" /></td>
						<td class="rotuloTabela">Tempo de Validade para Recupera��o da QPNR do Ano Anterior:</td>
						<td class="campoTabela">
							<input id="tempoValidadeRetiradaQPNR" class="campoTexto campoHorizontal" type="text" size="1" maxlength="2" name="tempoValidadeRetiradaQPNR" value="${modeloContratoVO.tempoValidadeRetiradaQPNR}" onkeypress="return formatarCampoInteiro(event);"/>
							<label class="rotuloHorizontal rotuloInformativo" for="tempoValidadeRetiradaQPNR">anos</label>
						</td>					
					</tr>
					
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">Penalidades por Retirada a Maior/Menor:</td>				
			</tr>
				
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado1" type="checkbox" value="true" id="selPenalidadeRetMaiorMenorM" name="selPenalidadeRetMaiorMenorM" onclick="permitirSelecaoObrigatorio('selPenalidadeRetMaiorMenorM', 'obgPenalidadeRetMaiorMenorM');" <c:if test="${modeloContratoVO.selPenalidadeRetMaiorMenorM == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio1" type="checkbox" value="true" id="obgPenalidadeRetMaiorMenorM" name="obgPenalidadeRetMaiorMenorM" onclick="verificarSelecaoObrigatorio('obgPenalidadeRetMaiorMenorM', 'selPenalidadeRetMaiorMenorM');" <c:if test="${modeloContratoVO.obgPenalidadeRetMaiorMenorM == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Penalidade:</td>
				<td class="campoTabela">
					<select name="penalidadeRetMaiorMenorM" id="penalidadeRetMaiorMenorM" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="penalidadeRetMaiorMenorM">
							<option value="<c:out value="${penalidadeRetMaiorMenorM.chavePrimaria}"/>" <c:if test="${modeloContratoVO.penalidadeRetMaiorMenorM == penalidadeRetMaiorMenorM.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${penalidadeRetMaiorMenorM.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selPeriodicidadeRetMaiorMenorM" name="selPeriodicidadeRetMaiorMenorM" onclick="permitirSelecaoObrigatorio('selPeriodicidadeRetMaiorMenorM', 'obgPeriodicidadeRetMaiorMenorM');" <c:if test="${modeloContratoVO.selPeriodicidadeRetMaiorMenorM == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgPeriodicidadeRetMaiorMenorM" name="obgPeriodicidadeRetMaiorMenorM" onclick="verificarSelecaoObrigatorio('obgPeriodicidadeRetMaiorMenorM', 'selPeriodicidadeRetMaiorMenorM');" <c:if test="${modeloContratoVO.obgPeriodicidadeRetMaiorMenorM == true}">checked="checked"</c:if>/></td>
				<td class="rotuloTabela">Periodicidade:</td>
				<td class="campoTabela">
					<select name="periodicidadeRetMaiorMenorM" id="periodicidadeRetMaiorMenorM" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="periodicidadeRetMaiorMenorM">
							<option value="<c:out value="${periodicidadeRetMaiorMenorM.chavePrimaria}"/>" <c:if test="${modeloContratoVO.periodicidadeRetMaiorMenorM == periodicidadeRetMaiorMenorM.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${periodicidadeRetMaiorMenorM.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>			
					
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleId="obgBaseApuracaoRetMaiorMenorM" styleClass="checkObrigatorio1" name="obgBaseApuracaoRetMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Base de Apura��o:</td>
				<td class="campoTabela">
					<select class="campoSelect" name="baseApuracaoRetMaiorMenorM" id="baseApuracaoRetMaiorMenorM">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaBaseApuracao}" var="baseApuracaoRetMaiorMenorM">
							<option value="<c:out value="${baseApuracaoRetMaiorMenorM.chavePrimaria}"/>" <c:if test="${modeloContratoVO.baseApuracaoRetMaiorMenorM == baseApuracaoRetMaiorMenorM.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${baseApuracaoRetMaiorMenorM.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>		
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgDataIniVigRetirMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">In�cio de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>

			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgDataFimVigRetirMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Fim de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPrecoCobrancaRetirMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
				<td class="campoTabela">
					<select name="precoCobrancaRetirMaiorMenorM" id="precoCobrancaRetirMaiorMenorM" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
							<option value="<c:out value="${precoCobranca.chavePrimaria}"/>" <c:if test="${modeloContratoVO.precoCobrancaRetirMaiorMenorM == precoCobranca.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${precoCobranca.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgTipoApuracaoRetirMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Forma de C�lculo para Cobran�a:</td>
				<td class="campoTabela">
					<select name="tipoApuracaoRetirMaiorMenorM" id="tipoApuracaoRetirMaiorMenorM" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
							<option value="<c:out value="${tipoApuracao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoApuracaoRetirMaiorMenorM == tipoApuracao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoApuracao.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>		
					
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualCobRetMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Percentual de Cobran�a:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text"
					id="percentualCobRetMaiorMenorM"
					name="percentualCobRetMaiorMenorM" maxlength="6" size="4"
					onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);"
					onblur="aplicarMascaraNumeroDecimal(this, 2);"
					value="${modeloContratoVO.percentualCobRetMaiorMenorM}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobreDem">%</label>				
				</td>
			</tr>	
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualCobIntRetMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Percentual de Cobran�a para Consumo Superior Durante Aviso de Interrup��o:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text"
					id="percentualCobIntRetMaiorMenorM"
					name="percentualCobIntRetMaiorMenorM"
					maxlength="6" size="4"
					onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);"
					onblur="aplicarMascaraNumeroDecimal(this, 2);"
					value="${modeloContratoVO.percentualCobIntRetMaiorMenorM}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobreDem">%</label>				
				</td>
			</tr>	
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgConsumoReferRetMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Consumo Refer�ncia:</td>
				<td class="campoTabela">
					<select name="consumoReferRetMaiorMenorM" id="consumoReferRetMaiorMenorM" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaRetMaiorMenorM">
							<option value="<c:out value="${consumoReferenciaRetMaiorMenorM.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoReferRetMaiorMenorM == consumoReferenciaRetMaiorMenorM.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${consumoReferenciaRetMaiorMenorM.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgPercentualRetMaiorMenorM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Percentual Por Retirada:</td>
				<td class="campoTabela">
					<input class="campoTexto campoHorizontal" type="text"
					id="percentualRetMaiorMenorM" name="percentualRetMaiorMenorM"
					maxlength="6" size="4"
					onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2);"
					onblur="aplicarMascaraNumeroDecimal(this, 2);"
					value="${modeloContratoVO.percentualRetMaiorMenorM}">
					<label class="rotuloHorizontal rotuloInformativo" for="percentualReferenciaSobreDem">%</label>				
				</td>
			</tr>

			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio1" name="obgIndicadorImpostoM" value="true" campoPai="periodicidadeRetMaiorMenorM" /></td>
				<td class="rotuloTabela">Usar pre�os s/ impostos para cobran�a:</td>
				<td class="campoTabela">
					<input id="indicadorImpostoSim" class="campoRadio indicadorImpostoCSS" type="radio" value="true" name="indicadorImpostoM" <c:if test="${modeloContratoVO.indicadorImpostoM == true}">checked="checked"</c:if>>
					<label class="rotuloRadio" for="indicadorImpostoSim">Sim</label>
					<input id="indicadorImpostoNaoM" class="campoRadio indicadorImpostoCSS" type="radio" value="false" name="indicadorImpostoM" <c:if test="${modeloContratoVO.indicadorImpostoM == false or empty modeloContratoVO.indicadorImpostoM}">checked="checked"</c:if>/>
					<label class="rotuloRadio" for="indicadorImpostoNaoM">N�o</label>
				</td>				
			</tr>					
					
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="4">Take or Pay:</td>				
					</tr>
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selPeriodicidadeTakeOrPay" name="selPeriodicidadeTakeOrPay" onclick="permitirSelecaoObrigatorio('selPeriodicidadeTakeOrPay', 'obgPeriodicidadeTakeOrPay');" <c:if test="${modeloContratoVO.selPeriodicidadeTakeOrPay == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgPeriodicidadeTakeOrPay" name="obgPeriodicidadeTakeOrPay" onclick="verificarSelecaoObrigatorio('obgPeriodicidadeTakeOrPay', 'selPeriodicidadeTakeOrPay');" <c:if test="${modeloContratoVO.obgPeriodicidadeTakeOrPay == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Periodicidade ToP:</td>
						<td class="campoTabela">
							<select name="periodicidadeTakeOrPay" id="periodicidadeTakeOrPay" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeTakeOrPay">
									<option value="<c:out value="${periodicidadeTakeOrPay.chavePrimaria}"/>" <c:if test="${modeloContratoVO.periodicidadeTakeOrPay == periodicidadeTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${periodicidadeTakeOrPay.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgTipoAgrupamentoContrato" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Tipo:</td>
						<td class="campoTabela">
							<select name="tipoAgrupamentoContrato" id="tipoAgrupamentoContrato" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaTipoAgrupamentoContrato}" var="tipoAgrupamentoContrato">
									<option value="<c:out value="${tipoAgrupamentoContrato.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoAgrupamentoContrato == tipoAgrupamentoContrato.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${tipoAgrupamentoContrato.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>						
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgReferenciaQFParadaProgramada" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Refer�ncia Quantidade Faltante Parada Programada:</td>
						<td class="campoTabela">
							<select name="referenciaQFParadaProgramada" id="referenciaQFParadaProgramada" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaReferenciaQFParadaProgramada}" var="referenciaQFParadaProgramada">
									<option value="<c:out value="${referenciaQFParadaProgramada.chavePrimaria}"/>" <c:if test="${modeloContratoVO.referenciaQFParadaProgramada == referenciaQFParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${referenciaQFParadaProgramada.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>	
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgConsumoReferenciaTakeOrPay" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Consumo Referencial para ToP:</td>
						<td class="campoTabela">
							<select name="consumoReferenciaTakeOrPay" id="consumoReferenciaTakeOrPay" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaTakeOrPay">
									<option value="<c:out value="${consumoReferenciaTakeOrPay.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoReferenciaTakeOrPay == consumoReferenciaTakeOrPay.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${consumoReferenciaTakeOrPay.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgMargemVariacaoTakeOrPay" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Compromisso de Retirada ToP:</td>
						<td class="campoTabela">
							<input id="margemVariacaoTakeOrPay" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" name="margemVariacaoTakeOrPay" value="${modeloContratoVO.margemVariacaoTakeOrPay }"/>
							<label class="rotuloHorizontal rotuloInformativo" for="margemVariacaoTakeOrPay">%</label>
						</td>
					</tr>
					
					<tr class="even">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgRecuperacaoAutoTakeOrPay" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Indicador de Recupera��o Autom�tica ToP:</td>
						<td class="campoTabela">
							<input id="recuperacaoAutoTakeOrPaySim" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="recuperacaoAutoTakeOrPay" <c:if test="${modeloContratoVO.recuperacaoAutoTakeOrPay == true}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="recuperacaoAutoTakeOrPaySim">Sim</label>
							<input id="recuperacaoAutoTakeOrPayNao" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="recuperacaoAutoTakeOrPay" <c:if test="${modeloContratoVO.recuperacaoAutoTakeOrPay == false or empty modeloContratoVO.recuperacaoAutoTakeOrPay}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="recuperacaoAutoTakeOrPayNao">N�o</label>
						</td>
					</tr>
					
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgDataInicioVigencia" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Data In�cio de Vig�ncia:</td>
						<td class="campoTabela"></td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgDataFimVigencia" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Data Fim de Vig�ncia:</td>
						<td class="campoTabela"></td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPrecoCobranca" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
						<td class="campoTabela">
							<select name="precoCobranca" id="precoCobranca" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
									<option value="<c:out value="${precoCobranca.chavePrimaria}"/>" <c:if test="${modeloContratoVO.precoCobranca == precoCobranca.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${precoCobranca.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgTipoApuracao" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Forma de C�lculo para Recupera��o:</td>
						<td class="campoTabela">
							<select name="tipoApuracao" id="tipoApuracao" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
									<option value="<c:out value="${tipoApuracao.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoApuracao == tipoApuracao.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${tipoApuracao.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgConsideraParadaProgramada" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Considerar Paradas Programadas:</td>
						<td class="campoTabela">
							<input id="consideraParadaProgramadaSim" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="consideraParadaProgramada" <c:if test="${modeloContratoVO.consideraParadaProgramada == true}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="consideraParadaProgramadaSim">Sim</label>
							<input id="consideraParadaProgramadaNao" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="consideraParadaProgramada" <c:if test="${modeloContratoVO.consideraParadaProgramada == false or empty modeloContratoVO.consideraParadaProgramada}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="consideraParadaProgramadaNao">N�o</label>
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgConsideraFalhaFornecimento" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Considerar Falhas de Fornecimento:</td>
						<td class="campoTabela">
							<input id="consideraFalhaFornecimentoSim" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="consideraFalhaFornecimento" <c:if test="${modeloContratoVO.consideraFalhaFornecimento == true}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="consideraFalhaFornecimentoSim">Sim</label>
							<input id="consideraFalhaFornecimentoNao" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="consideraFalhaFornecimento" <c:if test="${modeloContratoVO.consideraFalhaFornecimento == false or empty modeloContratoVO.consideraFalhaFornecimento}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="consideraFalhaFornecimentoNao">N�o</label>
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgConsideraCasoFortuito" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Considerar Casos Fortuitos:</td>
						<td class="campoTabela">
							<input id="consideraCasoFortuitoSim" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="consideraCasoFortuito" <c:if test="${modeloContratoVO.consideraCasoFortuito == true}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="consideraCasoFortuitoSim">Sim</label>
							<input id="consideraCasoFortuitoNao" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="consideraCasoFortuito" <c:if test="${modeloContratoVO.consideraCasoFortuito == false or empty modeloContratoVO.consideraCasoFortuito}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="consideraCasoFortuitoNao">N�o</label>
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgRecuperavel" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Recuper�vel:</td>
						<td class="campoTabela">
							<input id="recuperavelSim" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="true" name="recuperavel" <c:if test="${modeloContratoVO.recuperavel == true}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="recuperavelSim">Sim</label>
							<input id="recuperavelNao" class="campoRadio recuperacaoAutoTakeOrCss" type="radio" value="false" name="recuperavel" <c:if test="${modeloContratoVO.recuperavel == false or empty modeloContratoVO.recuperavel}">checked="checked"</c:if>/>
							<label class="rotuloRadio" for="recuperavelNao">N�o</label>
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgPercentualNaoRecuperavel" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Percentual N�o Recuper�vel:</td>
						<td class="campoTabela">
							<input id="percentualNaoRecuperavel" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" name="percentualNaoRecuperavel" value="${modeloContratoVO.percentualNaoRecuperavel }"/>
							<label class="rotuloHorizontal rotuloInformativo" for="percentualNaoRecuperavel">%</label>
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgApuracaoParadaProgramada" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Forma de Apura��o do Volume de Parada Programada:</td>
						<td class="campoTabela">
							<select name="apuracaoParadaProgramada" id="apuracaoParadaProgramada" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaApuracaoParadaProgramada}" var="apuracaoParadaProgramada">
									<option value="<c:out value="${apuracaoParadaProgramada.chavePrimaria}"/>" <c:if test="${modeloContratoVO.apuracaoParadaProgramada == apuracaoParadaProgramada.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${apuracaoParadaProgramada.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>
										
					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgApuracaoCasoFortuito" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Forma de Apura��o do Volume do Caso Fortuito ou For�a Maior:</td>
						<td class="campoTabela">
							<select name="apuracaoCasoFortuito" id="apuracaoCasoFortuito" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaApuracaoCasoFortuito}" var="apuracaoCasoFortuito">
									<option value="<c:out value="${apuracaoCasoFortuito.chavePrimaria}"/>" <c:if test="${modeloContratoVO.apuracaoCasoFortuito == apuracaoCasoFortuito.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${apuracaoCasoFortuito.descricao}"/>
									</option>		
								</c:forEach>
							</select>						
						</td>
					</tr>

					<tr class="even semLinhaFilho">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgApuracaoFalhaFornecimento" value="true" campoPai="periodicidadeTakeOrPay" /></td>
						<td class="rotuloTabela">Forma de Apura��o do Volume de Falha de Fornecimento:</td>
						<td class="campoTabela">
							<select name="apuracaoFalhaFornecimento" id="apuracaoFalhaFornecimento" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaApuracaoFalhaFornecimento}" var="apuracaoFalhaFornecimento">
									<option value="<c:out value="${apuracaoFalhaFornecimento.chavePrimaria}"/>" <c:if test="${modeloContratoVO.apuracaoFalhaFornecimento == apuracaoFalhaFornecimento.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${apuracaoFalhaFornecimento.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>
					
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="4">Ship or Pay:</td>				
					</tr>
					
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><input class="checkSelecionado6" type="checkbox" value="true" id="selPeriodicidadeShipOrPay" name="selPeriodicidadeShipOrPay" onclick="permitirSelecaoObrigatorio('selPeriodicidadeShipOrPay', 'obgPeriodicidadeShipOrPay');" <c:if test="${modeloContratoVO.selPeriodicidadeShipOrPay == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio6" type="checkbox" value="true" id="obgPeriodicidadeShipOrPay" name="obgPeriodicidadeShipOrPay" onclick="verificarSelecaoObrigatorio('obgPeriodicidadeShipOrPay', 'selPeriodicidadeShipOrPay');" <c:if test="${modeloContratoVO.obgPeriodicidadeShipOrPay == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Periodicidade SoP:</td>
						<td class="campoTabela">
							<select name="periodicidadeShipOrPay" id="periodicidadeShipOrPay" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="periodicidadeShipOrPay">
									<option value="<c:out value="${periodicidadeShipOrPay.chavePrimaria}"/>" <c:if test="${modeloContratoVO.periodicidadeShipOrPay == periodicidadeShipOrPay.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${periodicidadeShipOrPay.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>
					
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgConsumoReferenciaShipOrPay" value="true" campoPai="periodicidadeShipOrPay" /></td>
						<td class="rotuloTabela">Consumo Refer�ncia Inferior:</td>
						<td class="campoTabela">
							<select name="consumoReferenciaShipOrPay" id="consumoReferenciaShipOrPay" class="campoSelect">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaShipOrPay">
									<option value="<c:out value="${consumoReferenciaShipOrPay.chavePrimaria}"/>" <c:if test="${modeloContratoVO.consumoReferenciaShipOrPay == consumoReferenciaShipOrPay.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${consumoReferenciaShipOrPay.descricao}"/>
									</option>		
								</c:forEach>
							</select>
						</td>
					</tr>
					
					<tr class="even">
						<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
						<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio6" name="obgMargemVariacaoShipOrPay" value="true" campoPai="consumoReferenciaShipOrPay" /></td>
						<td class="rotuloTabela">Compromisso de Transporte SoP:</td>
						<td class="campoTabela">
							<input id="margemVariacaoShipOrPay" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" name="margemVariacaoShipOrPay" value="${modeloContratoVO.margemVariacaoShipOrPay }"/>
							<label class="rotuloHorizontal rotuloInformativo" for="margemVariacaoShipOrPay">%</label>
						</td>
					</tr>
					
					<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="4">Quantidade N�o Retirada:</td>				
					</tr>
					
					<tr class="even semLinhaPaiInicio">
						<td class="item-selecionado"><input class="checkSelecionado7" type="checkbox" value="true" id="selPercentualQNR" name="selPercentualQNR" onclick="permitirSelecaoObrigatorio('selPercentualQNR', 'obgPercentualQNR');" <c:if test="${modeloContratoVO.selPercentualQNR == true}">checked="checked"</c:if>/></td>
						<td class="item-selecionado"><input class="checkObrigatorio7" type="checkbox" value="true" id="obgPercentualQNR" name="obgPercentualQNR" onclick="verificarSelecaoObrigatorio('obgPercentualQNR', 'selPercentualQNR');" <c:if test="${modeloContratoVO.obgPercentualQNR == true}">checked="checked"</c:if>/></td>
						<td class="rotuloTabela">Percentual a considerar para QNR:</td>
						<td class="campoTabela">
							<input id="percentualQNR" class="campoTexto campoHorizontal" type="text" size="4" maxlength="6" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" name="percentualQNR" value="${modeloContratoVO.percentualQNR }"/>
							<label class="rotuloHorizontal rotuloInformativo" for="percentualQNR">%</label>
						</td>
					</tr>					
				</tbody>
			</table>
		</fieldset>