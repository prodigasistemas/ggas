<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fieldset id="regrasFaturamento">
	<a class="linkHelp" href="<help:help>/abamodalidadeconsumofaturamentoinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="110px">Selecionado</th>
				<th width="110px">Obrigat�rio</th>
				<th width="460px">Nome do Campo</th>
				<th>Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td><c:if test="${selModalidade eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgModalidade eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Modalidade:</td>
				<td class="campoTabela">
					<c:forEach items="${listaContratoModalidade}" var="modalidadeContrato">
						<c:if test="${modalidade eq modalidadeContrato.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${modalidadeContrato.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">QDC do Ponto de Consumo:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selQdc eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgQdc eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">QDC:</td>
				<td class="campoTabela"><span id="detalhamentoQDC" class="itemDetalhamento"><c:out value="${qdc}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataVigenciaQDC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataVigenciaQDC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Data de Vig�ncia da QDC:</td>
				<td class="campoTabela"><span id="detalhamentoQDC" class="itemDetalhamento"><c:out value="${dataVigenciaQDC}"/></span></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPrazoRevizaoQDC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPrazoRevizaoQDC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Prazo para revis�o das quantidades contratadas:</td>
				<td class="campoTabela"><span id="detalhamentoQDC" class="itemDetalhamento"><c:out value="${prazoRevizaoQDC}"/></span></td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">QDS:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selQdsMaiorQDC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgQdsMaiorQDC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">QDS poder ser maior que QDC?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${qdsMaiorQDC == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>		
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selDiasAntecSolicConsumo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDiasAntecSolicConsumo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Dias de Anteced�ncia para Solicita��o de Consumo:</td>
				<td class="campoTabela">
					<c:if test="${diasAntecSolicConsumo ne null && diasAntecSolicConsumo ne ''}">
						<span id="detalhamentoDiasAntecSolicConsumo" class="itemDetalhamento"><c:out value="${diasAntecSolicConsumo}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="diasAntecedenciaSolicitacaoConsumo">dias</label>
					</c:if>
				</td>
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selVariacaoSuperiorQDC eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgVariacaoSuperiorQDC eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual de Varia��o Superior ao QDC:</td>
				<td class="campoTabela">
					<c:if test="${variacaoSuperiorQDC ne null && variacaoSuperiorQDC ne ''}">
						<span id="detalhamentoVariacaoSuperiorQDC" class="itemDetalhamento"><c:out value="${variacaoSuperiorQDC}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="variacaoSuperiorQDC">%</label>
					</c:if>
				</td>
			</tr>
			
			<tr class="even">
				<td><c:if test="${selNumMesesSolicConsumo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgNumMesesSolicConsumo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">N�mero de Meses para Solicita��o de Consumo:</td>
				<td class="campoTabela">
					<c:if test="${numMesesSolicConsumo ne null && numMesesSolicConsumo ne ''}">
						<span id="detalhamentoNumeroMesesSolicitacaoConsumo" class="itemDetalhamento"><c:out value="${numMesesSolicConsumo}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="numeroMesesSolicitacaoConsumo">meses</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selConfirmacaoAutomaticaQDS eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConfirmacaoAutomaticaQDS eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Confirma��o Autom�tica da QDS?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${confirmacaoAutomaticaQDS == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>		
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">Estimativa de Quantidade a Recuperar:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selDataInicioRetiradaQPNR eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataInicioRetiradaQPNR eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Data para In�cio da Recupera��o:</td>
				<td class="campoTabela">
					<span id="detalhamentoDataIncioRetiradaQPNR" class="itemDetalhamento"><c:out value="${dataInicioRetiradaQPNR}"/></span>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataFimRetiradaQPNR eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataFimRetiradaQPNR eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Data para Fim da Recupera��o:</td>
				<td class="campoTabela">
					<span id="detalhamentoDataFimRetiradaQPNR" class="itemDetalhamento"><c:out value="${dataFimRetiradaQPNR}"/></span>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPercMinDuranteRetiradaQPNR eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercMinDuranteRetiradaQPNR eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual M�nimo em rela��o � QDC para Recupera��o:</td>
				<td class="campoTabela">
					<c:if test="${percMinDuranteRetiradaQPNR ne null && percMinDuranteRetiradaQPNR ne ''}">
						<span id="detalhamentoPercentualRetiradaQPNRduranteContrato" class="itemDetalhamento"><c:out value="${percMinDuranteRetiradaQPNR}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRduranteContrato">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPercDuranteRetiradaQPNR eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercDuranteRetiradaQPNR eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual M�ximo em rela��o � QDC para Recupera��o:</td>
				<td class="campoTabela">
					<c:if test="${percDuranteRetiradaQPNR ne null && percDuranteRetiradaQPNR ne ''}">
						<span id="detalhamentoPercentualRetiradaQPNRduranteContrato" class="itemDetalhamento"><c:out value="${percDuranteRetiradaQPNR}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRduranteContrato">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPercFimRetiradaQPNR eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercFimRetiradaQPNR eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual M�ximo para Recupera��o com o t�rmino do Contrato:</td>
				<td class="campoTabela">
					<c:if test="${percFimRetiradaQPNR ne null && percFimRetiradaQPNR ne ''}">
						<span id="detalhamentoPercentualRetiradaQPNRfimContrato" class="itemDetalhamento"><c:out value="${percFimRetiradaQPNR}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRfimContrato">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selTempoValidadeRetiradaQPNR eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTempoValidadeRetiradaQPNR eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tempo de Validade para Recupera��o da QPNR do Ano Anterior:</td>
				<td class="campoTabela">
					<c:if test="${tempoValidadeRetiradaQPNR ne null && tempoValidadeRetiradaQPNR ne ''}">
						<span id="detalhamentoTempoValidadeRetiradaQPNR" class="itemDetalhamento"><c:out value="${tempoValidadeRetiradaQPNR}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="percentualRetiradaQPNRfimContrato">anos</label>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">Penalidades por Retirada a Maior/Menor:</td>				
			</tr>
			
			<tr class="even">
				<td><c:if test="${selPenalidadeRetMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPenalidadeRetMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Penalidade:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPenalidadeRetiradaMaiorMenor}" var="penalidadeRetMaiorMenor">
						<c:if test="${penalidadeRetMaiorMenorM eq penalidadeRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${penalidadeRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selPeriodicidadeRetMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodicidadeRetMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Periodicidade:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPeriodicidadeRetMaiorMenor}" var="periodicidadeRetMaiorMenor">
						<c:if test="${periodicidadeRetMaiorMenorM eq periodicidadeRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${periodicidadeRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="even">
				<td><c:if test="${selBaseApuracaoRetMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgBaseApuracaoRetMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Base de Apura��o:</td>
				<td class="campoTabela">
					<c:forEach items="${listaBaseApuracao}" var="baseApuracaoRetMaiorMenor">
						<c:if test="${baseApuracaoRetMaiorMenorM eq baseApuracaoRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${baseApuracaoRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataIniVigRetirMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataIniVigRetirMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">In�cio de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selDataFimVigRetirMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataFimVigRetirMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Fim de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPrecoCobrancaRetirMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPrecoCobrancaRetirMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPrecoCobranca}" var="precoCobranca">
						<c:if test="${precoCobrancaRetirMaiorMenorM eq precoCobranca.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${precoCobranca.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			<tr class="even">
				<td><c:if test="${selTipoApuracaoRetirMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTipoApuracaoRetirMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de C�lculo para Cobran�a:</td>
				<td class="campoTabela">
					<c:forEach items="${listaTipoApuracao}" var="tipoApuracao">
						<c:if test="${tipoApuracaoRetirMaiorMenorM eq tipoApuracao.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${tipoApuracao.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selPercentualCobRetMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualCobRetMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual de Cobran�a:</td>
				<td class="campoTabela">
					<c:if test="${percentualCobRetMaiorMenorM ne null && percentualCobRetMaiorMenorM ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualCobRetMaiorMenorM}"/>%</span>
					</c:if>				
				</td>
			</tr>	
			
			<tr class="even">
				<td><c:if test="${selPercentualCobIntRetMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualCobIntRetMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual de Cobran�a para Consumo Superior Durante Aviso de Interrup��o:</td>
				<td class="campoTabela">
					<c:if test="${percentualCobIntRetMaiorMenorM ne null && percentualCobIntRetMaiorMenorM ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualCobIntRetMaiorMenorM}"/>%</span>
					</c:if>				
				</td>
			</tr>	
			<tr class="odd">
				<td><c:if test="${selConsumoReferRetMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsumoReferRetMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Consumo Refer�ncia:</td>
				<td class="campoTabela">
					<c:forEach items="${listaConsumoReferencial}" var="consumoReferenciaRetMaiorMenor">
						<c:if test="${consumoReferRetMaiorMenorM eq consumoReferenciaRetMaiorMenor.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${consumoReferenciaRetMaiorMenor.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			
			<tr class="even">
				<td><c:if test="${selPercentualRetMaiorMenorM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualRetMaiorMenorM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual Por Retirada:</td>
				<td class="campoTabela">
					<c:if test="${percentualRetMaiorMenorM ne null && percentualRetMaiorMenorM ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualRetMaiorMenorM}"/>%</span>
					</c:if>				
				</td>
			</tr>
			
			<tr class="even">
				<td><c:if test="${selIndicadorImpostoM eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIndicadorImpostoM eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Usar pre�os s/ impostos para cobran�a:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${indicadorImpostoM == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>			
			
			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">Take or Pay:</td>				
			</tr>
			<tr class="odd">
				<td><c:if test="${selPeriodicidadeTakeOrPay eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodicidadeTakeOrPay eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Periodicidade ToP:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="objPeriodicidadeTakeOrPay">
						<c:if test="${periodicidadeTakeOrPay eq objPeriodicidadeTakeOrPay.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPeriodicidadeTakeOrPay.descricao}"/></span>
						</c:if>	
					</c:forEach>		
				</td>	
			</tr>
			<tr class="even">
				<td><c:if test="${selReferenciaQFParadaProgramada eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgReferenciaQFParadaProgramada eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Refer�ncia Quantidade Faltante Parada Programada:</td>
				<td class="campoTabela">
					<c:forEach items="${listaReferenciaQFParadaProgramada}" var="objReferenciaQFParadaProgramada">
						<c:if test="${referenciaQFParadaProgramada eq objReferenciaQFParadaProgramada.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objReferenciaQFParadaProgramada.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>				
			<tr class="odd">
				<td><c:if test="${selConsumoReferenciaTakeOrPay eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsumoReferenciaTakeOrPay eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Consumo Referencial para ToP:</td>
				<td class="campoTabela">
					<c:forEach items="${listaConsumoReferencial}" var="objConsumoReferenciaTakeOrPay">
						<c:if test="${consumoReferenciaTakeOrPay eq objConsumoReferenciaTakeOrPay.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objConsumoReferenciaTakeOrPay.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			<tr class="even">
				<td><c:if test="${selMargemVariacaoTakeOrPay eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMargemVariacaoTakeOrPay eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Compromisso de Retirada ToP:</td>
				<td class="campoTabela">
					<c:if test="${margemVariacaoTakeOrPay ne null && margemVariacaoTakeOrPay ne ''}">
						<span id="detalhamentoMargemVariacaoToP" class="itemDetalhamento"><c:out value="${margemVariacaoTakeOrPay}"/>%</span>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selRecuperacaoAutoTakeOrPay eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgRecuperacaoAutoTakeOrPay eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Indicador de Recupera��o Autom�tica ToP:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${recuperacaoAutoTakeOrPay == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>		
			</tr>
			<tr class="even">
				<td><c:if test="${selDataInicioVigencia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataInicioVigencia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Data In�cio de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDataFimVigencia eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataFimVigencia eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Data Fim de Vig�ncia:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selPrecoCobranca eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPrecoCobranca eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tabela de Pre�o para Cobran�a:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPrecoCobranca}" var="objPrecoCobranca">
						<c:if test="${precoCobranca eq objPrecoCobranca.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPrecoCobranca.descricao}"/></span>
						</c:if>	
					</c:forEach>		
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selTipoApuracao eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTipoApuracao eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de C�lculo para Recupera��o:</td>
				<td class="campoTabela">
					<c:forEach items="${listaTipoApuracao}" var="objTipoApuracao">
						<c:if test="${tipoApuracao eq objTipoApuracao.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objTipoApuracao.descricao}"/></span>
						</c:if>	
					</c:forEach>	
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selConsideraParadaProgramada eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsideraParadaProgramada eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Considerar Paradas Programadas:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${consideraParadaProgramada == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selConsideraFalhaFornecimento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsideraFalhaFornecimento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Considerar Falhas de Fornecimento:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${consideraFalhaFornecimento == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selConsideraCasoFortuito eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsideraCasoFortuito eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Considerar Casos Fortuitos:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${consideraCasoFortuito == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selRecuperavel eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgRecuperavel eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Recuper�vel:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${recuperavel == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selPercentualNaoRecuperavel eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualNaoRecuperavel eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual N�o Recuper�vel:</td>
				<td class="campoTabela">
					<c:if test="${percentualNaoRecuperavel ne null && percentualNaoRecuperavel ne ''}">
						<span id="detalhamentoPercentualNaoRecuperavel" class="itemDetalhamento"><c:out value="${percentualNaoRecuperavel}"/>%</span>
					</c:if>				
				</td>
			</tr>	
			<tr class="even">
				<td><c:if test="${selApuracaoParadaProgramada eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgApuracaoParadaProgramada eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume de Parada Programada:</td>
				<td class="campoTabela">
					<c:forEach items="${listaApuracaoParadaProgramada}" var="objApuracaoParadaProgramada">
						<c:if test="${apuracaoParadaProgramada eq objApuracaoParadaProgramada.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objApuracaoParadaProgramada.descricao}"/></span>
						</c:if>	
					</c:forEach>					
				</td>
			</tr>		
			<tr class="even">
				<td><c:if test="${selApuracaoCasoFortuito eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgApuracaoCasoFortuito eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume do Caso Fortuito ou For�a Maior:</td>
				<td class="campoTabela">
					<c:forEach items="${listaApuracaoCasoFortuito}" var="objApuracaoCasoFortuito">
						<c:if test="${apuracaoCasoFortuito eq objApuracaoCasoFortuito.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objApuracaoCasoFortuito.descricao}"/></span>
						</c:if>	
					</c:forEach>				
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selApuracaoFalhaFornecimento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgApuracaoFalhaFornecimento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Forma de Apura��o do Volume de Falha de Fornecimento:</td>
				<td class="campoTabela">
					<c:forEach items="${listaApuracaoFalhaFornecimento}" var="objApuracaoFalhaFornecimento">
						<c:if test="${apuracaoFalhaFornecimento eq objApuracaoFalhaFornecimento.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objApuracaoFalhaFornecimento.descricao}"/></span>
						</c:if>	
					</c:forEach>					
				</td>
			</tr>

			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">Ship or Pay:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selPeriodicidadeShipOrPay eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPeriodicidadeShipOrPay eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Periodicidade ToP:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPeriodicidadeTakeOrPay}" var="objPeriodicidadeShipOrPay">
						<c:if test="${periodicidadeShipOrPay eq objPeriodicidadeShipOrPay.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objPeriodicidadeShipOrPay.descricao}"/></span>
						</c:if>	
					</c:forEach>		
				</td>	
			</tr>
			<tr class="even">
				<td><c:if test="${selConsumoReferenciaShipOrPay eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgConsumoReferenciaShipOrPay eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Consumo Referencial para SoP:</td>
				<td class="campoTabela">
					<c:forEach items="${listaConsumoReferencial}" var="objConsumoReferenciaShipOrPay">
						<c:if test="${consumoReferenciaShipOrPay eq objConsumoReferenciaShipOrPay.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objConsumoReferenciaShipOrPay.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>		
			</tr>
			<tr class="odd">
				<td><c:if test="${selMargemVariacaoShipOrPay eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgMargemVariacaoShipOrPay eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Compromisso de Transporte SoP:</td>
				<td class="campoTabela">
					<c:if test="${margemVariacaoShipOrPay ne null && margemVariacaoShipOrPay ne ''}">
						<span id="detalhamentoMargemVariacaoSoP" class="itemDetalhamento"><c:out value="${margemVariacaoShipOrPay}"/></span>
						<label class="rotuloHorizontal rotuloInformativo" for="MargemVariacaoSoP">%</label>
					</c:if>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="4"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="4">Quantidade N�o Retirada:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selPercentualQNR eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgPercentualQNR eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Percentual a considerar para QNR:</td>
				<td class="campoTabela"></td>	
			</tr>
		</tbody>
	</table>
</fieldset>