<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fieldset id="Faturamento">
	<a class="linkHelp" href="<help:help>/abafaturamentoinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado</th>
				<th width="73px">Obrigat�rio</th>
				<th width="73px">Valor Fixo</th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td><c:if test="${selFatPeriodicidade eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFatPeriodicidade eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFatPeriodicidade eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Periodicidade:</td>
				<td class="campoTabela">
					<c:forEach items="${listaPeriodicidade}" var="objFatPeriodicidade">
						<c:if test="${fatPeriodicidade eq objFatPeriodicidade.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objFatPeriodicidade.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selIndicadorNFE eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIndicadorNFE eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoIndicadorNFE eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Emiss�o de nota fiscal eletr�nica:</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${indicadorNFE == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>			
			
		<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Item de Faturamento:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selItemFatura eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgItemFatura eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoItemFatura eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Item da Fatura:</td>
				<td class="campoTabela">
					<c:forEach items="${listaItemFatura}" var="objItemFatura">
						<c:if test="${itemFatura eq objItemFatura.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objItemFatura.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>

			<tr class="even">
				<td><c:if test="${selTarifaConsumo eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgTarifaConsumo eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoTarifaConsumo eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Tarifa de Consumo:</td>
				<td class="campoTabela">
					<c:forEach items="${listaTarifas}" var="objTarifaConsumo">
						<c:if test="${tarifaConsumo eq objTarifaConsumo.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objTarifaConsumo.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selDataReferenciaCambial eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDataReferenciaCambial eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoDataReferenciaCambial eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Data de Refer�ncia Cambial:</td>
				<td class="campoTabela">
					<c:forEach items="${listaDataReferenciaCambial}" var="objDataReferenciaCambial">
						<c:if test="${dataReferenciaCambial eq objDataReferenciaCambial.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objDataReferenciaCambial.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			
			<tr class="even">
				<td><c:if test="${selDiaCotacao eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDiaCotacao eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoDiaCotacao eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Dia da Cota��o:</td>
				<td class="campoTabela">
					<c:forEach items="${listaDiaCotacao}" var="objDiaCotacao">
						<c:if test="${diaCotacao eq objDiaCotacao.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objDiaCotacao.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			
			<tr class="odd">
				<td><c:if test="${selDiaVencimentoItemFatura eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDiaVencimentoItemFatura eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoDiaVencimentoItemFatura eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Dia para Vencimento:</td>
				<td class="campoTabela">
					<span class="itemDetalhamento"><c:if test="${diaVencimentoItemFatura ne '-1'}"><c:out value="${diaVencimentoItemFatura}"/></c:if></span>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selOpcaoVencimentoItemFatura eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgOpcaoVencimentoItemFatura eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoOpcaoVencimentoItemFatura eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Op��o para Vencimento:</td>
				<td class="campoTabela">
					<c:forEach items="${listaOpcaoFaseRefVencimento}" var="objOpcaoVencimentoItemFatura">
						<c:if test="${opcaoVencimentoItemFatura eq objOpcaoVencimentoItemFatura.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objOpcaoVencimentoItemFatura.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selFaseVencimentoItemFatura eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgFaseVencimentoItemFatura eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoFaseVencimentoItemFatura eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Fase Refer�ncia para Vencimento:</td>
				<td class="campoTabela">
					<c:forEach items="${listaFaseReferencialVencimento}" var="objFaseVencimentoItemFatura">
						<c:if test="${faseVencimentoItemFatura eq objFaseVencimentoItemFatura.chavePrimaria}">
							<span class="itemDetalhamento"><c:out value="${objFaseVencimentoItemFatura.descricao}"/></span>
						</c:if>	
					</c:forEach>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selIndicadorVencDiaNaoUtil eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgIndicadorVencDiaNaoUtil eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoIndicadorVencDiaNaoUtil eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Vencimento em dia n�o �til?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${indicadorVencDiaNaoUtil == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selDebitoAutomatico eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDebitoAutomatico eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoDebitoAutomatico eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">D�bito Autom�tico?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${debitoAutomatico == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="even">
				<td><c:if test="${selDepositoIdentificado eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgDepositoIdentificado eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${valorFixoDepositoIdentificado eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td class="rotuloTabela">Dep�sito Identificado?</td>
				<td class="campoTabela">
					<c:choose>
						<c:when test="${depositoIdentificado == true}"><span class="itemDetalhamento">Sim</span></c:when>
						<c:otherwise><span class="itemDetalhamento">N�o</span></c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
			<tr>
				<td class="tituloGrupoTabela" colspan="5">Entrega da Fatura:</td>				
			</tr>
			<tr class="even">
				<td><c:if test="${selEndFisEntFatCEP eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEndFisEntFatCEP eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">CEP:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${endFisEntFatCEP}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selEndFisEntFatNumero eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEndFisEntFatNumero eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">N�mero:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${endFisEntFatNumero}"/></span></td>
			</tr>
			<tr class="even">
				<td><c:if test="${selEndFisEntFatComplemento eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEndFisEntFatComplemento eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">Complemento:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${endFisEntFatComplemento}"/></span></td>
			</tr>
			<tr class="odd">
				<td><c:if test="${selEndFisEntFatEmail eq true}"><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td><c:if test="${obgEndFisEntFatEmail eq true}"><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
				<td></td>
				<td class="rotuloTabela">E-mail:</td>
				<td class="campoTabela"><span class="itemDetalhamento"><c:out value="${endFisEntFatEmail}"/></span></td>
			</tr>
			<tr class="even">
				<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>" border="0"></td>
				<td></td>
				<td class="rotuloTabela">Endere�o padr�o: </td>
				<td class="campoTabela">
						<c:if test="${enderecoPadrao eq true}"><span class="itemDetalhamento">Cliente</span></c:if>
						<c:if test="${enderecoPadrao eq false}"><span class="itemDetalhamento">Ponto de Consumo</span></c:if>
						<c:if test="${enderecoPadrao eq undefined}"><span class="itemDetalhamento">Nenhum</span></c:if>
				</td>
			</tr>
			
			
			
			<tr class="linhaSeparadoraTabela"><td colspan="5"></td></tr>
					<tr>
						<td class="tituloGrupoTabela" colspan="5">Dados de Compra:</td>				
					</tr>
					<tr class="even">
						<td><img  alt="Selecionado" title="Selecionado" src="<c:url value="/imagens/check2.gif"/>"><input class="checkSelecionado1" type="hidden" value="true" name="selContratoCompra" /></td>
						<td><img  alt="Obrigat�rio" title="Obrigat�rio" src="<c:url value="/imagens/check2.gif"/>"><input class="checkObrigatorio1" type="hidden" value="true" name="obgContratoCompra" /></td>
						<td><c:if test="${valorFixoContratoCompra eq true}"><img  alt="Valor Fixo" title="Valor Fixo" src="<c:url value="/imagens/check2.gif"/>" border="0"></c:if></td>
						<td class="rotuloTabela">Contrato de Compra:</td>
						<td class="campoTabela">
							<c:forEach items="${listaContratoCompra}" var="objContratoCompra">
								<c:if test="${contratoCompra eq objContratoCompra.chavePrimaria}">
									<span class="itemDetalhamento"><c:out value="${objContratoCompra.descricao}"/></span>
								</c:if>	
							</c:forEach>
						</td>
					</tr>
		</tbody>
	</table>
</fieldset>