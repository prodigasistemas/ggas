<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script>
	

	function controlarCheckBoxObrigatorios7(elemCheck) {
			
		var valorCheck = elemCheck.checked;

		var camposSelecionados = new Array('selClienteResponsavel');
		var camposObrigatorios = new Array('obgClienteResponsavel');

		marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, valorCheck);

	}
	
	function limparabaResponsabilidade(){
		
	}

</script>

<fieldset id="responsabilidade">
	<a class="linkHelp" href="<help:help>/abaresponsabilidadeinclusoealteraodemodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>	
	<table class="dataTableGGAS dataTableAba">
		<thead>
			<tr>
				<th width="73px">Selecionado<input id="checkAllSelecionado7" type="checkbox" name="todos"/></th>
				<th width="73px">Obrigat�rio<input id="checkAllObrigatorio7" type="checkbox" name="todos" onclick="controlarCheckBoxObrigatorios7(this);"/></th>
				<th width="73px">Valor Fixo<input id="checkAllValorFixo7" type="checkbox" name="todos" onclick="controlarCheckBoxValorFixo(this);" /></th>
				<th width="340px">Nome do Campo</th>
				<th width="247px">Valor Padr�o</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even semLinhaPaiInicio">
				<td class="item-selecionado"><input class="checkSelecionado7" type="checkbox" value="true" id="selClienteResponsavel" name="selClienteResponsavel" onclick="permitirSelecaoObrigatorio('selClienteResponsavel', 'obgClienteResponsavel');" 	<c:if test="${modeloContratoVO.selClienteResponsavel == true}">checked="checked"</c:if>/></td>
				<td class="item-selecionado"><input class="checkObrigatorio7" type="checkbox" value="true" id="obgClienteResponsavel" name="obgClienteResponsavel" onclick="verificarSelecaoObrigatorio('obgClienteResponsavel', 'selClienteResponsavel');" <c:if test="${modeloContratoVO.obgClienteResponsavel == true}">checked="checked"</c:if>/></td>
				<td></td>
				<td class="rotuloTabela">Cliente Respons�vel:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio7" name="obgTipoResponsabilidade" value="true" campoPai="clienteResponsavel" /></td>
				<td class="item-selecionado">
					<input class="checkValorFixo" type="checkbox" value="true" id="valorFixoTipoResponsabilidade" name="valorFixoTipoResponsabilidade" 
						onclick="verificarSelecaoObrigatorio('valorFixoTipoResponsabilidade', 'selClienteResponsavel')" campoPai="selClienteResponsavel" <c:if test="${modeloContratoVO.valorFixoTipoResponsabilidade == true}">checked="checked"</c:if>
						/>
				</td>
				<td class="rotuloTabela">Tipo de Responsabilidade: </td>
				<td class="campoTabela">
					<select name="tipoResponsabilidade" id="tipoResponsabilidade" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoResponsabilidade}" var="tipoResponsabilidade">
							<option value="<c:out value="${tipoResponsabilidade.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoResponsabilidade == tipoResponsabilidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoResponsabilidade.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr class="even semLinhaFilho">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio7" name="obgDataInicioRelacao" value="true" campoPai="clienteResponsavel" /></td>
				<td></td>
				<td class="rotuloTabela">Data de In�cio do Relacionamento:</td>
				<td class="campoTabela"></td>
			</tr>
			<tr class="even">
				<td class="item-selecionado"><img  alt="Dependente" title="Dependente" src="<c:url value="/imagens/dependente.gif"/>" border="0"></td>
				<td class="item-selecionado"><ggas:checkBoxDependente styleClass="checkObrigatorio7" name="obgDataFimRelacao" value="true" campoPai="clienteResponsavel" /></td>
				<td></td>
				<td class="rotuloTabela">Data de Fim do Relacionamento:</td>
				<td class="campoTabela"></td>
			</tr>
		</tbody>
	</table>
</fieldset>		