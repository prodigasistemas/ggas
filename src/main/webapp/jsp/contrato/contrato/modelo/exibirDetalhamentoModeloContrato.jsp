<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

/* 	$(function(){
		
		//Selecionar/Desselecionar todos os checkboxes da coluna Selecionado
		if ( $("#checkAllSelecionado1,#checkAllSelecionado2,#checkAllSelecionado3,#checkAllSelecionado4,#checkAllSelecionado5,#checkAllSelecionado6,#checkAllSelecionado7").length > 0 ) { 
			
			$('#checkAllSelecionado1').click(function(){
				$("input[type='checkbox'].checkSelecionado1").attr('checked', $('#checkAllSelecionado1').is(':checked'));
			});
			$('#checkAllSelecionado2').click(function(){
				$("input[type='checkbox'].checkSelecionado2").attr('checked', $('#checkAllSelecionado2').is(':checked'));
			});
			$('#checkAllSelecionado3').click(function(){
				$("input[type='checkbox'].checkSelecionado3").attr('checked', $('#checkAllSelecionado3').is(':checked'));
			});
			$('#checkAllSelecionado4').click(function(){
				$("input[type='checkbox'].checkSelecionado4").attr('checked', $('#checkAllSelecionado4').is(':checked'));
			});
			$('#checkAllSelecionado5').click(function(){
				$("input[type='checkbox'].checkSelecionado5").attr('checked', $('#checkAllSelecionado5').is(':checked'));
			});
			$('#checkAllSelecionado6').click(function(){
				$("input[type='checkbox'].checkSelecionado6").attr('checked', $('#checkAllSelecionado6').is(':checked'));
			});
			$('#checkAllSelecionado7').click(function(){
				$("input[type='checkbox'].checkSelecionado7").attr('checked', $('#checkAllSelecionado7').is(':checked'));
			})
		}
		
		//Selecionar/Desselecionar todos os checkboxes da coluna Obrigat�rio
		if ( $("#checkAllObrigatorio1,#checkAllObrigatorio2,#checkAllObrigatorio3,#checkAllObrigatorio4,#checkAllObrigatorio5,#checkAllObrigatorio6,#checkAllObrigatorio7").length > 0 ) { 
			$('#checkAllObrigatorio1').click(function(){
				$("input[type='checkbox'].checkObrigatorio1").attr('checked', $('#checkAllObrigatorio1').is(':checked'));
			});
			$('#checkAllObrigatorio2').click(function(){
				$("input[type='checkbox'].checkObrigatorio2").attr('checked', $('#checkAllObrigatorio2').is(':checked'));
			});
			$('#checkAllObrigatorio3').click(function(){
				$("input[type='checkbox'].checkObrigatorio3").attr('checked', $('#checkAllObrigatorio3').is(':checked'));
			});
			$('#checkAllObrigatorio4').click(function(){
				$("input[type='checkbox'].checkObrigatorio4").attr('checked', $('#checkAllObrigatorio4').is(':checked'));
			});
			$('#checkAllObrigatorio5').click(function(){
				$("input[type='checkbox'].checkObrigatorio5").attr('checked', $('#checkAllObrigatorio5').is(':checked'));
			});
			$('#checkAllObrigatorio6').click(function(){
				$("input[type='checkbox'].checkObrigatorio6").attr('checked', $('#checkAllObrigatorio6').is(':checked'));
			});
			$('#checkAllObrigatorio7').click(function(){
				$("input[type='checkbox'].checkObrigatorio7").attr('checked', $('#checkAllObrigatorio7').is(':checked'));
			})
		}
	
	}); */
	
	function voltar(){	
		submeter("modeloContratoForm", "pesquisarModeloContrato");
	}
		
	function alterar(){
		//document.forms[0].postBack.value = false;
		$("#isAlterar").val(true);
		submeter('modeloContratoForm', 'exibirAlteracaoModeloContrato');
	}
	
	//Seta a aba atual no input
	function aba(aba){	
		$("#aba").val(aba);
	}	
	
	//Manter a aba atual ap�s refresh
	window.setTimeout(function() {
		var aba = $("#aba").val();
		document.getElementById(aba).click();
		
	}, 0);
	

</script>

<h1 class="tituloInterno">Detalhar Modelo de Contrato<a class="linkHelp" href="<help:help>/detalharcadastromodelodecontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form action="exibirDetalhamentoModeloContrato" method="post" styleId="modeloContratoDetalharForm" id="modeloContratoForm" name="modeloContratoForm">
	
	<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoModeloContrato"/>
	<input type="hidden" name="isAlterar" id="isAlterar" value="false"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${modeloContratoVO.chavePrimaria}"/>
	<input name="aba" type="hidden" id="aba" value=""/>
	
	<fieldset id="conteinerDetalharModeloContrato" class="detalhamento">
		<label class="rotulo">Nome do Modelo:</label>
		<span id="detalhamentoDescricao" class="itemDetalhamento campoHorizontal"><c:out value="${modeloContratoVO.descricao}"/></span>
		<label class="rotulo rotuloHorizontal" id="rotuloDescricaoAbreviada">Descri��o Abreviada:</label>
		<span id="detalhamentoDescricaoAbreviada" class="itemDetalhamento campoHorizontal"><c:out value="${modeloContratoVO.descricaoAbreviada}"/></span><br class="quebraLinha" />
		<label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
		<span id="detalhamentoDescricaoAbreviada" class="itemDetalhamento"><c:out value="${modeloContratoVO.indicadorUsoTelaDetalhamento}"/></span>
	</fieldset>
	
	<fieldset id="tabs" style="display: none">
		<ul>
			<li><a id="aba1" onclick="aba('aba1');" href="#Geral">Geral</a></li>
			<li><a id="aba2" onclick="aba('aba2');" href="#Principais">Principais</a></li>
			<li><a id="aba3" onclick="aba('aba3');" href="#Tecnicos">T�cnicos</a></li>
			<li><a id="aba4" onclick="aba('aba4');" href="#Consumo">Consumo</a></li>
			<li><a id="aba5" onclick="aba('aba5');" href="#Faturamento">Faturamento</a></li>
			<li><a id="aba6" onclick="aba('aba6');" href="#regrasFaturamento">Modalidades Consumo Faturamento</a></li>
			<li><a id="aba7" onclick="aba('aba7');" href="#responsabilidade">Responsabilidade</a></li>
		</ul>
		
		<jsp:include page="/jsp/contrato/contrato/modelo/abas/abaDetalheGeral.jsp"/>
		<jsp:include page="/jsp/contrato/contrato/modelo/abas/abaDetalhePrincipais.jsp"/>
		<jsp:include page="/jsp/contrato/contrato/modelo/abas/abaDetalheTecnicos.jsp"/>
		<jsp:include page="/jsp/contrato/contrato/modelo/abas/abaDetalheConsumo.jsp"/>
		<jsp:include page="/jsp/contrato/contrato/modelo/abas/abaDetalheFaturamento.jsp"/>
		<jsp:include page="/jsp/contrato/contrato/modelo/abas/abaDetalheResponsabilidade.jsp"/>
		<jsp:include page="/jsp/contrato/contrato/modelo/abas/abaDetalheRegrasFaturamentoModeloContrato.jsp"/>	
		
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" id="botaoVoltar" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">	    
	    
	    <vacess:vacess param="exibirAlteracaoModeloContrato">
	    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
	    </vacess:vacess>
	 </fieldset>
	 
</form:form>
 