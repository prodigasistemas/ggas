<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/contrato/modelo/modelo.js"></script>
<script>


	$().ready(function(){
		//dadosDefault();
	});

	$(function(){
		
		// Datepicker
		$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
		//Selecionar/Desselecionar todos os checkboxes da coluna Selecionado
		if ( $("#checkAllSelecionado1,#checkAllSelecionado2,#checkAllSelecionado3,#checkAllSelecionado4,#checkAllSelecionado5,#checkAllSelecionado6,#checkAllSelecionado7").length > 0 ) { 
			$('#checkAllSelecionado1').click(function(){
				var checked = $('#checkAllSelecionado1').is(':checked');
				$("input[type='checkbox'].checkSelecionado1").attr('checked', checked);
				if(checked == false){
					$("#checkAllObrigatorio1").attr('checked', checked);
					$("#checkAllValorFixo1").attr('checked', checked);
					$("input[type='checkbox'].checkObrigatorio1").attr('checked', checked);
					$(this).parents("table").find(".checkValorFixo").attr('checked', checked);				
				}
			});
			$('#checkAllSelecionado2').click(function(){
				var checked = $('#checkAllSelecionado2').is(':checked');
				$("input[type='checkbox'].checkSelecionado2").attr('checked', checked);
				if(checked == false){
					$("#checkAllObrigatorio2").attr('checked', checked);
					$("#checkAllValorFixo2").attr('checked', checked);
					$("input[type='checkbox'].checkObrigatorio2").attr('checked', checked);
					$(this).parents("table").find(".checkValorFixo").attr('checked', checked);				
				}
			});
			$('#checkAllSelecionado3').click(function(){
				var checked = $('#checkAllSelecionado3').is(':checked');
				$("input[type='checkbox'].checkSelecionado3").attr('checked', checked);
				if(checked == false){
					$("#checkAllObrigatorio3").attr('checked', checked);
					$("#checkAllValorFixo3").attr('checked', checked);
					$("input[type='checkbox'].checkObrigatorio3").attr('checked', checked);
					$(this).parents("table").find(".checkValorFixo").attr('checked', checked);
				}
			});
			$('#checkAllSelecionado4').click(function(){
				var checked = $('#checkAllSelecionado4').is(':checked');
				$("input[type='checkbox'].checkSelecionado4").attr('checked', checked);
				if(checked == false){
					$("#checkAllObrigatorio4").attr('checked', checked);
					$("#checkAllValorFixo4").attr('checked', checked);
					$("input[type='checkbox'].checkObrigatorio4").attr('checked', checked);
					$(this).parents("table").find(".checkValorFixo").attr('checked', checked);
				}
			});
			$('#checkAllSelecionado5').click(function(){
				var checked = $('#checkAllSelecionado5').is(':checked');
				$("input[type='checkbox'].checkSelecionado5").attr('checked', checked);
				if(checked == false){
					$("#checkAllObrigatorio5").attr('checked', checked);
					$("#checkAllValorFixo5").attr('checked', checked);
					$("input[type='checkbox'].checkObrigatorio5").attr('checked', checked);
					$(this).parents("table").find(".checkValorFixo").attr('checked', checked);
				}
			});
			$('#checkAllSelecionado6').click(function(){
				var checked = $('#checkAllSelecionado6').is(':checked');
				$("input[type='checkbox'].checkSelecionado6").attr('checked', checked);
				if(checked == false){
					$("#checkAllObrigatorio6").attr('checked', checked);
					$("input[type='checkbox'].checkObrigatorio6").attr('checked', checked);
				}
			});
			$('#checkAllSelecionado7').click(function(){
				var checked = $('#checkAllSelecionado7').is(':checked');
				$("input[type='checkbox'].checkSelecionado7").attr('checked', checked);
				if(checked == false){
					$("#checkAllObrigatorio7").attr('checked', checked);
					$("#checkAllValorFixo7").attr('checked', checked);
					$("input[type='checkbox'].checkObrigatorio7").attr('checked', checked);
					$(this).parents("table").find(".checkValorFixo").attr('checked', checked);
				}
			})
		}
		
		$("select.campoHorizontal").css("margin-left","5px")
		
		
	});
	
	var popup;

	function exibirPopupPesquisaProposta() {
		popup = window.open('exibirPesquisaPropostaPopup?acao=exibirPesquisaPropostaPopup&postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function cancelar(){
		location.href = '<c:url value="exibirPesquisaModeloContrato"/>';
	}
	
	function limpar(form){		
		
		limparFormularios(form);
		
		limparAbaGeral();		
		limparAbaPrincipais();
		limparAbaTecnicos();
		limparAbaConsumo();
		limparAbaFaturamento();
		limparAbaRegrasFaturamentoModeloContrato();
// 		limparabaResponsabilidade();
		dadosDefault();
		
	}

	function controlarCheckBoxValorFixo(elemento){
		var tabela = $(elemento).parents("table");
		var valor = $(elemento).prop("checked")

		tabela.find(".checkValorFixo").each(function(){
			var checkValorFixo = $(this);
			if (valor) {
				// so marca os checkbox cujo a coluna de selecionado esteja marcado 
				var idValorFixo = checkValorFixo.attr("id");
				var idSel = idValorFixo.replace("valorFixo","sel");
				var sel = $("#"+idSel);

				if (sel.prop("checked")) {
					checkValorFixo.prop("checked", true);
				} else if (checkValorFixo.attr("campoPai") != undefined) {
					var idCampoPai = checkValorFixo.attr("campoPai");
					if($("#"+idCampoPai).prop("checked")){
						checkValorFixo.prop("checked", true);
					}
				} 
            } else {
            	checkValorFixo.prop("checked", false);
            }
		});
	}

	function selecionarItensComponenteSelect() {
		var listaLocal = document.getElementById('idsLocalAmostragemAssociados');		
		if (listaLocal != undefined) {
			for (i=0; i<listaLocal.length; i++){
				listaLocal.options[i].selected = true;
			}
		}	

		var listaIntervalo = document.getElementById('idsIntervaloAmostragemAssociados');		
		if (listaIntervalo != undefined) {
			for (i=0; i<listaIntervalo.length; i++){
				listaIntervalo.options[i].selected = true;
			}
	    }	
	}
	
	function dadosDefault(){
	
		//abaGeral
		$("#exigeAprovacaoNao").attr("checked", true);
		$("#anoContratualMaiusculo").attr("checked", true);
		$("#renovacaoAutomaticaNao").attr("checked", true);
		$("#numDiasRenoAutoContrato").val("60");
		$("#tempoAntecedenciaRenovacao").val("60");
		$("#tipoPeriodicidadePenalidadeCivil").attr("checked", true);
		$("#propostaAprovadaNao").attr("checked", true);
		$("#faturamentoAgrupamentoNao").attr("checked", true);
		$("#emissaoFaturaAgrupadaNao").attr("checked", true);
		$("#debitoAutomaticoNao").attr("checked", true);
		$("#participaECartasNao").attr("checked", true);
		$("#indicadorMultaAtrasoNao").attr("checked", true);
		$("#indicadorJurosMoraNao").attr("checked", true);
		$("#indicadorImpostoNao").attr("checked", true);
		$("#consideraParadaProgramadaNaoC").attr("checked", true);
		$("#consideraFalhaFornecimentoNaoC").attr("checked", true);
		$("#consideraCasoFortuitoNaoC").attr("checked", true);
		$("#recuperavelNaoC").attr("checked", true);
		
		//abaConsumo
		$("#horaInicialDia").val("-1");
		$("#regimeConsumo").val("-1");
		$("#unidadeFornecMaximoDiario").val("-1");
		$("#unidadeFornecMinimoDiario").val("-1");
		$("#unidadeFornecMinimoMensal").val("-1");
		$("#unidadeFornecMinimoAnual").val("-1");
		$("#consumoFatFalhaMedicao").val("-1");
		moveAllOptionsEspecial(document.forms[0].idsLocalAmostragemAssociados,document.forms[0].locaisAmostragemDisponiveis,true);
		moveAllOptionsEspecial(document.forms[0].idsIntervaloAmostragemAssociados,document.forms[0].intervalosAmostragemDisponiveis,true);
		
		//abaFaturamento
		$("#indicadorNFENao").attr("checked", true);
		$("#emitirFaturaComNfeNao").attr("checked", true);
		$("#indicadorVencDiaNaoUtilNao").attr("checked", true);
		$("#depositoIdentificadoNao").attr("checked", true);
		$("#enderecoPadraoNao").attr("checked", true);
		$("#enderecoPadraoNenhum").attr("checked", true);
		$("#contratoCompra").val("-1");
		
		//abaModalidadesConsumoFaturamento
		$("#confirmacaoAutomaticaQDSNao").attr("checked", true);
		$("#indicadorImpostoNao").attr("checked", true);
		$("#recuperacaoAutoTakeOrPayNao").attr("checked", true);
		$("#consideraParadaProgramadaNao").attr("checked", true);
		$("#consideraCasoFortuitoNao").attr("checked", true);
		$("#recuperavelNao").val(false);
		
	} 
	
</script>

<h1 class="tituloInterno">Incluir Modelo de Contrato<a class="linkHelp" href="<help:help>/modelosdecontratoinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form:form action="incluirModeloContrato" method="post" styleId="formModeloContratoIncluir" id="modeloContratoForm" name="modeloContratoForm">

	<input name="acao" type="hidden" id="acao" value="incluirModeloContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${modeloContratoVO.chavePrimaria}"/>
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	
	<fieldset id="conteinerModeloContrato" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input class="campoTexto campoHorizontal" type="text" id="descricao" name="descricao" maxlength="50" size="40" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${modeloContratoVO.descricao}">
		<label class="rotulo campoObrigatorio rotuloHorizontal" for="descricaoAbreviada" style="clear: left;"><span class="campoObrigatorioSimbolo">* </span>Descri��o Abreviada:</label>
		<input class="campoTexto campoHorizontal" type="text" id="descricaoAbreviada" name="descricaoAbreviada" maxlength="20" size="15" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${modeloContratoVO.descricaoAbreviada}">
		<label class="rotulo campoObrigatorio rotuloHorizontal" for="tipoModelo" style="clear: left;"><span class="campoObrigatorioSimbolo">* </span>Tipo do Modelo:</label>
		<select name="tipoModelo" id="tipoModelo" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoModelo}" var="tipoModelo">
				<option value="<c:out value="${tipoModelo.chavePrimaria}"/>" <c:if test="${modeloContratoVO.tipoModelo eq tipoModelo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoModelo.descricao}"/>
				</option>
			</c:forEach>
		</select>
	</fieldset>
	
	<fieldset id="tabs" style="display: none">
		<ul>
			<li><a href="#Geral">Geral</a></li>
			<li><a href="#Principais">Principais</a></li>
			<li><a href="#Tecnicos">T�cnicos</a></li>
			<li><a href="#Consumo">Consumo</a></li>
			<li><a href="#Faturamento">Faturamento</a></li>
			<li><a href="#regrasFaturamento">Modalidades Consumo Faturamento</a></li>
			<li><a href="#responsabilidade">Responsabilidade</a></li>
			<li><a href="#impressao">Impress�o</a></li>
		</ul>

		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarGeral.jsp" %>
		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarPrincipais.jsp" %>
		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarTecnicos.jsp" %>
		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarConsumo.jsp" %>
		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarFaturamento.jsp" %>		
		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarRegrasFaturamentoModeloContrato.jsp" %>
		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarResponsabilidade.jsp" %>
		<%@ include file="/jsp/contrato/contrato/modelo/abas/abaIncluirAlterarImpressao.jsp" %>
		
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar(this.form);">
	    <vacess:vacess param="incluirModeloContrato">
	    	<input id="botaoIncluirModeloContrato" name="button" class="bottonRightCol2 botaoGrande1" value="Incluir" type="submit" onclick="selecionarItensComponenteSelect();">
	    </vacess:vacess>
	 </fieldset>

	<token:token></token:token>
	 
</form:form>
