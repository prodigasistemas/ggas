<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>

	$(document).ready(function() {
		var fluxoLevantamentoMercado = document.getElementById("fluxoLevantamentoMercado").value;
		
		if(fluxoLevantamentoMercado == "true"){
			document.forms[0].idModalidadeUso.disabled = true;
			$("#valorPrecoPago").attr('readonly', 'readonly');
			$("#valorGastoMensal").attr('readonly', 'readonly');
			$("#idSegmento").prop("disabled", true);
		}else{
			document.forms[0].idModalidadeUso.disabled = false;
			$("#valorPrecoPago").removeAttr('readonly');
			$("#valorGastoMensal").removeAttr('readonly');
		}
	});

	$(function(){
		
		$("#anoMesReferenciaPreco").inputmask("99/9999",{placeholder:"_"});
		
		//Datepicker
		$(".campoData").datepicker({showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	
	});	

	function limparFormulario() {
		
		limparFormularios(document.propostaForm);
		
		document.forms[0].indicadorParticipacao.value = false;
		
		document.getElementById('indicadorParticipacao2').checked = true;		
		animatedcollapse.hide('camposIndicadorParticipacao');				
        document.getElementById('indicadorCondominioImovelTexto1').checked = false;        
        document.getElementById('indicadorCondominioImovelTexto2').checked = false;        
        document.getElementById('valorParticipacaoCDLTexto').value = "";
        $("input#valorInvestimentoTexto + span.itemDetalhamento").html("");
        $("input#valorParticipacaoCDLTexto + span.itemDetalhamento").html("");
        esconderDadosParticipacao();
		
	}
	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaProposta"/>';
	}

</script>

<h1 class="tituloInterno">Incluir Proposta<a class="linkHelp" href="<help:help>/propostainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form:form method="post" action="incluirProposta" enctype="multipart/form-data" name="propostaForm" id="propostaForm">
<input name="acao" type="hidden" id="acao" value="incluirProposta"/>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${propostaVO.chavePrimaria}"/>
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="tela" type="hidden" id="tela" value="inclusao">
<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
<input name="segundaVia" type="hidden" id="segundaVia" value="false">
<fieldset id="tabs" style="display: none">
		<ul>
			<li><a href="#propostaAbaIdentificacao">Identifica��o</a></li>
			<li><a href="#propostaAbaConsumo">Comparativo</a></li>
			<li><a href="#propostaAbaImportacao">Importar Planilha</a></li>
		</ul>
		<fieldset id="propostaAbaIdentificacao">
			<jsp:include page="/jsp/contrato/proposta/abaIdentificacaoProposta.jsp">
				<jsp:param name="alteracao" value="false"/>
			</jsp:include>
		</fieldset>
		<fieldset id="propostaAbaConsumo">
			<jsp:include page="/jsp/contrato/proposta/abaConsumoProposta.jsp"></jsp:include>
		</fieldset>
		<fieldset id="propostaAbaImportacao">
			<jsp:include page="/jsp/contrato/proposta/abaImportacaoProposta.jsp">
				<jsp:param name="actionLimparImportacao" value="exibirInclusaoProposta" />
				<jsp:param name="actionImportar" value="importarDadosInclusaoProposta" />
			</jsp:include>
		</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
	<input id="botaoCancelar" name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="incluirProposta">
    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="submit">
    </vacess:vacess>
 </fieldset>
<token:token></token:token>
</form:form> 