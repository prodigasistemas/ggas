<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>
$(function(){
		
		$("#anoMesReferenciaPreco").inputmask("99/9999",{placeholder:"_"});
		
		// Datepicker
		$(".campoData").datepicker({showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		 
	});	

	function limparFormulario() {
		document.forms[0].numeroProposta.value = "";
		document.forms[0].versaoProposta.value = "";
		document.forms[0].dataEmissao.value = "";
		document.forms[0].dataVigencia.value = "";
		document.forms[0].dataEntrega.value = "";
		document.forms[0].percentualTIR.value = "";
		document.forms[0].valorMaterial.value = "";
		document.forms[0].valorMaoDeObra.value = "";
		document.forms[0].valorInvestimento.value = "";
		document.forms[0].indicadorParticipacao[1].checked = true;
		document.forms[0].percentualCliente.value = "";
		document.forms[0].valorCliente.value = "";
		document.forms[0].quantidadeParcela.value = "";
		document.forms[0].valorParcela.value = "";
		document.forms[0].percentualJuros.value = "";
		document.forms[0].anoMesReferenciaPreco.value = "";
		//document.forms[0].valorPrecoPago.value = "";
		document.forms[0].valorGastoMensal.value = "";
		document.forms[0].idSituacaoProposta.value = "-1";
		document.forms[0].idTarifa.value = "-1";
		document.forms[0].consumoMedioAnual.value = "";
		document.forms[0].consumoMedioMensal.value = "";
		//document.forms[0].consumoNormalizadoEquivalente.value = "";
		document.getElementById('consumoUnidadeConsumidora').value = "";
		document.forms[0].idUnidadeConsumoAnual.value = "-1";
		document.forms[0].idUnidadeConsumoMensal.value = "-1";
		document.getElementById('idSegmento').value = "";
		document.getElementById('idModalidadeUso').value = "";
		document.getElementById('consumoMedioAnualGN').value = "";
		document.getElementById('consumoMedioMensalGN').value = "";
		document.getElementById('valorMedioGN').value = "";
		document.getElementById('valorGastoMensalGN').value = "";
		document.getElementById('economiaMensalGN').value = "";
       	document.getElementById('economiaAnualGN').value = "";
       	document.getElementById('percentualEconomia').value = "";
		animatedcollapse.hide('camposIndicadorParticipacao');
		document.getElementById('volumeDiarioEstimadoGN').value = "";
		document.getElementById('idImovel').value = "";
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
    	document.getElementById('nomeFantasia').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	
       	document.getElementById('nomeImovelTexto').value = "";
       	document.getElementById('matriculaImovelTexto').value = "";
       	document.getElementById('numeroImovelTexto').value = "";
       	document.getElementById('cidadeImovelTexto').value = "";
       	document.getElementById('indicadorCondominioImovelTexto1').checked = false;
        document.getElementById('indicadorCondominioImovelTexto2').checked = false;
        
        document.forms[0].dataEleicaoSindico.value = "";
        document.forms[0].dataAssembleiaCondominio.value = "";
        document.forms[0].comentario.value = "";
        document.forms[0].idPeriodicidadeJuros.value = "-1";
        
        somaValorInvestimento();
        
	}
	
	function cancelar() {
		submeter('propostaForm',  'cancelarAcaoProposta');
	}

	function salvar() {
		document.forms[0].versaoProposta.disabled = false;		
		submeter('propostaForm',  'alterarProposta');
	}
	
</script>


<h1 class="tituloInterno">Alterar Proposta<a class="linkHelp" href="<help:help>/propostainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar uma Proposta, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form:form method="post" action="alterarProposta" id="propostaForm" name="propostaForm" enctype="multipart/form-data"> 
	<input name="acao" type="hidden" id="acao" value="alterarProposta"/>
	<input name="tela" type="hidden" id="tela" value="alteracao">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${propostaVO.chavePrimaria}"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>

<fieldset id="tabs" style="display: none">
	<ul>
		<li><a href="#propostaAbaIdentificacao">Identifica��o</a></li>
		<li><a href="#propostaAbaConsumo">Comparativo</a></li>
		<li><a href="#propostaAbaImportacao">Importar Planilha</a></li>
	</ul>
	<fieldset id="propostaAbaIdentificacao">
		<jsp:include page="/jsp/contrato/proposta/abaIdentificacaoProposta.jsp">
			<jsp:param name="alteracao" value="true"/>
		</jsp:include>
	</fieldset>
	<fieldset id="propostaAbaConsumo">
		<jsp:include page="/jsp/contrato/proposta/abaConsumoProposta.jsp"></jsp:include>
	</fieldset>
	<fieldset id="propostaAbaImportacao">
		<jsp:include page="/jsp/contrato/proposta/abaImportacaoProposta.jsp">
			<jsp:param name="actionLimparImportacao" value="exibirAlteracaoProposta" />
			<jsp:param name="actionImportar" value="importarDadosAlteracaoProposta" />
		</jsp:include>
	</fieldset>
</fieldset>
<fieldset class="conteinerBotoes"> 
	<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  onclick="salvar();" type="submit">
 </fieldset>
 <token:token></token:token>
</form:form> 