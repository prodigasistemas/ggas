<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Pesquisar Proposta<a class="linkHelp" href="<help:help>/consultadaspropostas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script language="javascript">
	
	$(document).ready(function(){	

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		//Exibe a janela de Op��es de exporta��o 			
		$("#situacaoImovel").dialog({autoOpen: false, width: 255, modal: true, minHeight: 170, maxHeight: 170, resizable: false});		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};
	
	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document.getElementById("nomeCompletoCliente");
		var documentoFormatado = document.getElementById("documentoFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document.getElementById("enderecoFormatadoCliente");		
		
		if(idSelecionado != '') {				
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];		               	
		               	if(cliente["cnpj"] != undefined ){
		               		documentoFormatado.value = cliente["cnpj"];
		               	} else {
			               	if(cliente["cpf"] != undefined ){
			               		documentoFormatado.value = cliente["cpf"];
			               	} else {
			               		documentoFormatado.value = "";
			               	}
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	documentoFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
	
		document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
		document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
		document.getElementById("emailClienteTexto").value = emailCliente.value;
		document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
	}
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasia");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("indicadorCondominioAmbos");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function limparFormulario(){
		document.propostaForm.numeroProposta.value = "";	
		document.forms[0].habilitado[0].checked = true;	
		document.getElementById('idCliente').value = "";
    	document.getElementById('nomeCompletoCliente').value = "";
    	document.getElementById('documentoFormatado').value = "";
    	document.getElementById('enderecoFormatadoCliente').value = "";
    	document.getElementById('emailCliente').value = "";
    	document.getElementById('nomeClienteTexto').value = "";
    	document.getElementById('documentoFormatadoTexto').value = "";
    	document.getElementById('emailClienteTexto').value = "";
    	document.getElementById('enderecoFormatadoTexto').value = "";
    	
    	document.getElementById('nomeFantasia').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	
       	document.getElementById('idImovel').value = "";
       	document.getElementById('nomeImovelTexto').value = "";
       	document.getElementById('matriculaImovelTexto').value = "";
       	document.getElementById('numeroImovelTexto').value = "";
       	document.getElementById('cidadeImovelTexto').value = "";
       	document.getElementById('indicadorCondominioImovelTexto1').checked = false;
       	document.getElementById('indicadorCondominioImovelTexto2').checked = false;
       
	}

	function incluir() {		
		location.href = '<c:url value="/exibirInclusaoProposta"/>';
	}

	function exibirPopupPesquisaCliente() {
		popup = window.open('exibirPesquisaClientePopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function removerProposta(){

		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('propostaForm', 'removerProposta');
			}
    	}
   } 	

	function detalharProposta(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("propostaForm", "exibirDetalhamentoProposta");
	}

	function alterarProposta(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("propostaForm", "exibirAlteracaoProposta");
		}
	}
		
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
			
		}	
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasia').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('indicadorCondominioAmbos').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		
	}
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = valor;
	}
	
	function manterDadosTela(){
		<c:choose>
			<c:when test="${propostaVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				document.getElementById('indicadorPesquisaCliente').checked = true;
				pesquisarCliente(true);
				habilitaCliente();
			</c:when>
			<c:otherwise>
				document.getElementById('indicadorPesquisaImovel').checked = true;
				pesquisarImovel(true);
				habilitaImovel();
			</c:otherwise>
		</c:choose>
	}
	
	function init () {
		manterDadosTela();
		exibirRelatorio();
	}
	
	 function exibirRelatorio(){
		<c:if test="${exibirRelatorio eq true}">
			var iframe = document.getElementById("download");
			iframe.src = 'exibirRelatorioProposta';
		</c:if>	
	}
	
	function imprimirSegundaVia(idNota) {
		document.forms['propostaForm'].chavePrimaria.value = idNota;
		submeter('propostaForm', 'gerarPropostaPDF');
	}
	
	function enviarPropostaPorEmail(idNota) {
		document.getElementById('chavePrimaria').value = idNota;
		submeter('propostaForm', 'enviarPropostaPorEmail');
	}
	
	function rejeitarProposta(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_REJEITAR"/>');
			if(retorno == true) {
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('propostaForm', 'rejeitarProposta');
			}
    	}
   }
	
	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirSituacaoImovel(){
		carregarSituacaoImovel();
		count = 0;
		listaOptionsSelect = document.getElementById("idSituacao").options;
		i = 0;
		
		for(i; i < listaOptionsSelect.length; i++){
			if(listaOptionsSelect[i].text.toUpperCase() == "FACT�VEL"){
				count = count+1;
			}
			if(listaOptionsSelect[i].text.toUpperCase() == "POTENCIAL"){
				count = count+1;
			}
		}
		if(count == 2){
			exibirJDialog("#situacaoImovel");
		}else{
			aprovar();
		}
	}
	
	function obterValorUnicoIdQuadraFace() {
		var form = document.forms[0];

		if (form != undefined && form.idsQuadraFaces != undefined) {
			var total = form.idsQuadraFaces.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form.chavesPrimarias[i].checked == true){		
						return form.idsQuadraFaces[i].value;
					}
				}
			} else {
				if(form.chavesPrimarias.checked == true){
					return form.idsQuadraFaces.value;
				}
			}	
		}
	}
	
	function aprovar(){
		var e = document.getElementById("idSituacao");
		var itemSelecionado = e.options[e.selectedIndex].value;
		document.getElementById("idSituacaoImovel").value = itemSelecionado;
		
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('propostaForm', 'aprovarProposta');
    	}
	}
	
	function carregarSituacaoImovel() {			
		var codQuadraFace = obterValorUnicoIdQuadraFace();
      	var selectSituacaoImovel = document.getElementById("idSituacao");
      	var selectChavePrimaria = obterValorUnicoCheckboxSelecionado();
      	
      	selectSituacaoImovel.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectSituacaoImovel.options[selectSituacaoImovel.length] = novaOpcao;

      	if (codQuadraFace != "-1") {
      		selectSituacaoImovel.disabled = false;
    		$("#idSituacao").removeClass("campoDesabilitado");
        	AjaxService.consultarSitucaoImovelPorFacesQuadra(codQuadraFace, selectChavePrimaria+";",
        		{callback: function(listaSitucaoImovel) {
                	for (key in listaSitucaoImovel){
                    	var novaOpcao = new Option(listaSitucaoImovel[key], key);
                    	selectSituacaoImovel.options[selectSituacaoImovel.length] = novaOpcao;
                    }
                }, async:false}
            );
      	} else {
      		selectSituacaoImovel.disabled = true;
    		$("#idSituacao").addClass("campoDesabilitado");	
      	}
	}

	
   addLoadEvent(init);
   
   </script>

<form:form method="post" action="pesquisarProposta" id="propostaForm" name="propostaForm"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarProposta"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="segundaVia" type="hidden" id="segundaVia" value="true">
	<input name="idSituacaoImovel" type="hidden" id="idSituacaoImovel">
	<jsp:include page="/jsp/contrato/proposta/popupSelecionaSituacaoImovel.jsp"/>
	<fieldset id="pesquisarProposta" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${propostaVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Cliente</legend>
			<div class="pesquisarClienteFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span> para selecionar um Cliente.</p>
				<input name="idCliente" type="hidden" id="idCliente" value="${cliente.chavePrimaria}">
				<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${cliente.nome}">
				<input name="documentoFormatado" type="hidden" id="documentoFormatado" value="${cliente.numeroDocumentoFormatado}">
				<input name="enderecoFormatadoCliente" type="hidden" id="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal}">
				<input name="emailCliente" type="hidden" id="emailCliente" value="${cliente.emailPrincipal}">
				
				<input name="Button" id="botaoPesquisarCliente" class="bottonRightCol" title="Pesquisar cliente"  value="Pesquisar cliente" onclick="exibirPopupPesquisaCliente();" type="button"><br >
				<label class="rotulo" id="rotuloCliente" for="nomeClienteTexto">Cliente:</label>
				<input class="campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" size="50" disabled="disabled" value="${cliente.nome}"><br />
				<label class="rotulo" id="rotuloCnpjTexto" for="documentoFormatadoTexto">CPF/CNPJ:</label>
				<input class="campoDesabilitado" type="text" id="documentoFormatadoTexto" name="documentoFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${cliente.numeroDocumentoFormatado}"><br />	
				<label class="rotulo" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto">Endere�o:</label>
				<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoTexto" rows="2" cols="37" disabled="disabled">${cliente.enderecoPrincipal.enderecoFormatado}</textarea><br />
				<label class="rotulo" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
				<input class="campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${cliente.emailPrincipal}"><br />
			</div>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${propostaVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}">
				<input name="nomeFantasia" type="hidden" id="nomeFantasia" value="${imovel.nome}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.chavePrimaria}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.quadraFace.endereco.cep.municipio.descricao}">
				<input name="indicadorCondominioAmbos" type="hidden" id="indicadorCondominioAmbos" value="${imovel.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Im�vel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.municipio.descricao}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio eq 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio eq 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		<fieldset class="conteinerBloco">
			<label class="rotulo" id="rotuloNumeroProposta" for="numeroProposta">N�mero da Proposta:<!-- N�o remover este coment�rio: corrige um bug de caracteres duplecados no IE6 e 7 --></label>
			<input class="campoTexto" type="text" id="numeroProposta" name="numeroProposta" maxlength="50" size="40" onkeypress="return formatarCampoInteiro(event);" value="${propostaVO.numeroProposta}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumerico(event)');">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${propostaVO.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${propostaVO.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty propostaVO.habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
			
			<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="pesquisarProposta">
	    			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    		</vacess:vacess>		
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
			</fieldset>
		</fieldset>
	</fieldset>
			
	<c:if test="${listaPropostas ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="listaPropostas" sort="list" id="proposta" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarProposta">
	     	 <display:column style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${proposta.chavePrimaria}">
	     	 </display:column>
	     	 <display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${proposta.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
			  <display:column sortable="true" title="N�mero da Proposta" style="width: 100px">
		     	<a href='javascript:detalharProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.numeroCompletoProposta}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" titleKey="PROPOSTA_IMOVEL" headerClass="tituloTabelaEsq" style="text-align: left">
		     	<input type="hidden" id="idsQuadraFaces" name="idsQuadraFaces" value="${proposta.imovel.quadraFace.chavePrimaria}" />
		     	<a href='javascript:detalharProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.imovel.nome}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" title="Proposta /<br />Vers�o" style="width: 240px">
				<a href='javascript:detalharProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.numeroProposta}'/> / <c:out value='${proposta.versaoProposta}'/>
	        	</a>
	    	 </display:column>
		 	<display:column sortable="true" title="Situa��o<br />da Proposta" style="width: 85px">
		 		<a href='javascript:detalharProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${proposta.situacaoProposta.descricao}'/>
				</a>
	    	</display:column>
	    	<display:column sortable="true" title="Data de<br />Emiss�o" style="width: 75px">
	    		<a href='javascript:detalharProposta(<c:out value='${proposta.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${proposta.dataEmissao}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>
	    	<display:column title="2� Via" style="width: 40px" sortable="true">
				<a href='javascript:imprimirSegundaVia(<c:out value='${proposta.chavePrimaria}'/>);'>
		    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via" title="Imprimir 2� Via" border="0">
		    	</a>
			</display:column>
			<display:column title="Email" style="width: 40px" sortable="true">
				<c:if test="${!proposta.situacaoProposta.propostaRejeitada}">
					<a href='javascript:enviarPropostaPorEmail(<c:out value='${proposta.chavePrimaria}'/>);'>
			    		<img src="<c:url value="/imagens/icon_email.gif" />" alt="Enviar Email"  border="0">
			    	</a>
		    	</c:if>
			</display:column>
		</display:table>
	</c:if>

	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaPropostas}">
			<vacess:vacess param="exibirAlteracaoProposta">
				<input id="botaoAlterar" name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarProposta()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerProposta">
				<input id="botaoRemover" name="buttonRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerProposta()" type="button">
			</vacess:vacess>
			
			<input name="buttonRejeitar" id="buttonRejeitar" value="Rejeitar" class="bottonRightCol bottonLeftColUltimo" onclick="rejeitarProposta()" type="button">
			
			<input name="buttonAprovar" id="buttonRejeitar" value="Aprovar" class="bottonRightCol bottonLeftColUltimo" onclick="exibirSituacaoImovel()" type="button">
			
		</c:if>
		<vacess:vacess param="exibirInclusaoProposta">
			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>
	<iframe id="download" src ="" width="0" height="0"></iframe>
</form:form> 
