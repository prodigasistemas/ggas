	<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<script>

	$(document).ready(function(){
		/* Substitui os <inputs> que possuem valor por <span class="itemDetalhamento"...> */
		$("input#valorInvestimentoTexto").each(function(){
			$(this).css("display","none")
			var valueInput = $("input#valorInvestimento").val();
			var inputId = $(this).attr("id");
			if (valueInput != undefined && valueInput != "") {
				valueInput = "R$ " + valueInput;
			}
			$("#" + inputId).after("<span class='itemDetalhamento'>" + valueInput + "</span>");
			
			
		});

		$("input#valorParticipacaoCDLTexto").each(function(){
			$(this).css("display","none")
			var valueInput = $("input#valorParticipacaoCDL").val();
			var inputId = $(this).attr("id");
			if (valueInput != undefined && valueInput != "") {
				valueInput = "R$ " + valueInput;
			}
			$("#" + inputId).after("<span class='itemDetalhamento'>" + valueInput + "</span>");
		});
		$(".ui-datepicker-trigger").css("float", "left");
		$(".ui-datepicker-trigger").css("margin-top", "5px");
	});

	<c:choose>	
		<c:when test="${propostaVO.indicadorParticipacao ne true}">
			animatedcollapse.addDiv('camposIndicadorParticipacao', 'fade=0,speed=400,hide=1');
		</c:when>
		<c:otherwise>			
			animatedcollapse.addDiv('camposIndicadorParticipacao', 'fade=0,speed=400,hide=0');
		</c:otherwise>
	</c:choose>

	function exibirDadosParticipacao() {
		animatedcollapse.show('camposIndicadorParticipacao');
	}
	
	function esconderDadosParticipacao() {
		animatedcollapse.hide('camposIndicadorParticipacao');
		/*
		document.forms[0].percentualCliente.value = "";
		document.forms[0].valorCliente.value = "";
		document.forms[0].quantidadeParcela.value = "";
		document.forms[0].valorParcela.value = "";
		document.forms[0].percentualJuros.value = "";
		*/
	}
	
	function exibirPopupPesquisaCliente() {
		popup = window.open('exibirPesquisaClientePopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document.getElementById("nomeCompletoCliente");
		var documentoFormatado = document.getElementById("documentoFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document.getElementById("enderecoFormatadoCliente");		
		
		if(idSelecionado != '') {				
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];		               	
		               	if(cliente["cnpj"] != undefined ){
		               		documentoFormatado.value = cliente["cnpj"];
		               	} else {
			               	if(cliente["cpf"] != undefined ){
			               		documentoFormatado.value = cliente["cpf"];
			               	} else {
			               		documentoFormatado.value = "";
			               	}
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	documentoFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
	
		document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
		document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
		document.getElementById("emailClienteTexto").value = emailCliente.value;
		document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
	}
	
	function exibirPopupPesquisaImovel() {
			popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function selecionarImovel(idSelecionado){
			var idImovel = document.getElementById("idImovel");
			var matriculaImovel = document.getElementById("matriculaImovel");
			var nomeFantasia = document.getElementById("nomeFantasia");
			var numeroImovel = document.getElementById("numeroImovel");
			var cidadeImovel = document.getElementById("cidadeImovel");
			var indicadorCondominio = document.getElementById("indicadorCondominioAmbos");
			var nuoLojas = '';
			var codigoModalidadeMedicao = '';
						
			if(idSelecionado != '') {				
				AjaxService.obterImovelPorChave( idSelecionado, {
		           	callback: function(imovel) {	           		
		           		if(imovel != null){  	           			        		      		         		
			               	idImovel.value = imovel["chavePrimaria"];
			               	matriculaImovel.value = imovel["chavePrimaria"];		               	
			               	nomeFantasia.value = imovel["nomeFantasia"];
			               	numeroImovel.value = imovel["numeroImovel"];
			               	cidadeImovel.value = imovel["cidadeImovel"];
			               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
			               	if(imovel["numeroAptoLojas"]) {
								nuoLojas = imovel["numeroAptoLojas"];
							}
			               	codigoModalidadeMedicao = imovel["codigoModalidadeMedicao"];
		               	}
		        	}, async:false}
		        	
		        );	        
	        } else {
	       		idImovel.value = "";
	        	matriculaImovel.value = "";
	        	nomeFantasia.value = "";
	        	numeroImovel.value = "";
	        	cidadeImovel.value = "";
	        	indicadorCondominio.value = "";
	       	}
		
			document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
			document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
			document.getElementById("numeroImovelTexto").value = numeroImovel.value;
			document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
			if(indicadorCondominio.value == 'true') {
				document.getElementById("indicadorCondominioImovelTexto1").checked = true;
				habilitarElementosImovel(true,idSelecionado,nuoLojas,codigoModalidadeMedicao);
			} else {
				document.getElementById("indicadorCondominioImovelTexto2").checked = true;
				habilitarElementosImovel(false,idSelecionado,nuoLojas,codigoModalidadeMedicao);
			}
			
		}
		
		function somaValorInvestimento(){
			var valorMaterial = document.getElementById("valorMaterial").value;
			var valorMaoDeObra = document.getElementById("valorMaoDeObra").value;
			var valorInvestimentoFloat;
			if((valorMaterial == "") && (valorMaoDeObra == "" )){
				document.getElementById("valorInvestimentoTexto").value = "";
				document.forms[0].valorInvestimento.value = "";
				$("input#valorInvestimentoTexto + span.itemDetalhamento").html("");
			}
			
			if(valorMaterial != ""){
				while(valorMaterial.indexOf(".") != -1) {
					valorMaterial = valorMaterial.replace(".","");
				}
				
			}
			
			if(valorMaoDeObra != ""){	
				while(valorMaoDeObra.indexOf(".") != -1) {
					valorMaoDeObra = valorMaoDeObra.replace(".","");
				}
				
			}
				
			if(valorMaterial != "" && valorMaoDeObra != ""){
				valorInvestimentoFloat = (parseFloat(valorMaterial.replace(",",".")) + parseFloat(valorMaoDeObra.replace(",","."))).toFixed(2);				
			} else if(valorMaterial != ""  && valorMaoDeObra == "") {
				valorInvestimentoFloat = (parseFloat(valorMaterial.replace(",","."))).toFixed(2);				
			} else if(valorMaterial == ""  && valorMaoDeObra != ""){
				valorInvestimentoFloat = (parseFloat(valorMaoDeObra.replace(",","."))).toFixed(2);
			}

			if(valorInvestimentoFloat > 0){
				var valorInvestimentoString = valorInvestimentoFloat.toString().replace(",",".");
				var valorInvestimento = document.getElementById("valorInvestimento");
				var valorInvestimentoTexto = document.getElementById("valorInvestimentoTexto");
				
				valorInvestimento.value = valorInvestimentoString;
				valorInvestimentoTexto.value = valorInvestimentoString;

				formataValorMonetario(valorInvestimento, 15);
				formataValorMonetario(valorInvestimentoTexto, 15);

				var numeroComMascara = '';				
				var inteiros = obterParteInteira(valorInvestimentoString);
				numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
				var decimais = obterParteDecimal(valorInvestimentoString);
	
				valorInvestimentoFloat = valorInvestimentoFloat.replace(".",",");
				
				$("input#valorInvestimentoTexto + span.itemDetalhamento").html("R$ " + document.getElementById("valorInvestimento").value);
				
			}

			var participacaoCliente = document.getElementsByName('indicadorParticipacao');
			if(participacaoCliente != null && participacaoCliente[0].checked) {
				var percentualCliente = document.getElementById('percentualCliente');
				if(percentualCliente != null) {
					preencherValorCliente(percentualCliente);
				}
			}
			
		}
			
		function habilitarVersao() {			
			var valorVersao = document.getElementById('versaoPropostaAnterior').value;
			document.forms[0].versaoProposta.value = valorVersao;
		}
		
		function desabilitarVersao(incrementar) {			
			var valorVersao = 0;
			var versaoAnterior = document.getElementById('versaoPropostaAnterior').value;
			if(versaoAnterior != ''){
				valorVersao = versaoAnterior;
			}			
			if (incrementar) {
				document.forms[0].versaoProposta.value = parseInt(valorVersao) + 1;				
			}
			document.forms[0].versaoProposta.disabled = true;
		}
		
		function preencherValorCliente(percentualCliente) {
			var valorInvestimento = document.getElementById("valorInvestimento").value;
			var percentualClienteString = percentualCliente.value;
			
			if(valorInvestimento != "" && percentualClienteString != "") { 
				var percentualClienteFloat = parseFloat(percentualClienteString.replace(",",".")).toFixed(2);
				if(percentualClienteFloat > 100.00) {
					alert("Percentual do Cliente maior que 100%");
				} else {
					while(valorInvestimento.indexOf(".") != -1) {
						valorInvestimento = valorInvestimento.replace(".","");
					}
					
					var valorInvestimentoFloat = parseFloat(valorInvestimento.replace(",",".")).toFixed(2);
					
					var valorCliente = document.getElementById("valorCliente");
					var resultadoValorCliente = (((valorInvestimentoFloat*percentualClienteFloat)/100).toFixed(2)).toString().replace(".", ",");
					valorCliente.value = resultadoValorCliente;
					formataValorMonetario(valorCliente, 14);
					preencherPercentualCliente(valorCliente);
				}
			}
		}
		
		
		function preencherPercentualCliente(valorCliente) {
			var valorInvestimento = document.getElementById("valorInvestimento").value;
			var valorClienteString = valorCliente.value;
			
			if(valorInvestimento != "" && valorClienteString != "") {
				while(valorClienteString.indexOf(".") != -1) {
					valorClienteString = valorClienteString.replace(".","");
				}				
				var valorClienteFloat = Number(valorClienteString.replace(",","."));
				while(valorInvestimento.indexOf(".") != -1) {
					valorInvestimento = valorInvestimento.replace(".","");
				}				
				var valorInvestimentoFloat = Number(valorInvestimento.replace(",","."));
				
				if(valorClienteFloat > valorInvestimentoFloat) {
					alert("Valor do Cliente maior que Valor do Investimento.");
				} else {
					var resultadoPercentual = (((valorClienteFloat*100)/valorInvestimentoFloat).toFixed(2));
					var percentualCliente = document.getElementById("percentualCliente");
					percentualCliente.value = resultadoPercentual.toString().replace(".",",");

					var participacaoCDLFloat = ((valorInvestimentoFloat - valorClienteFloat).toFixed(2));
					var valorParticipacaoCDL = document.getElementById("valorParticipacaoCDL");
					var valorParticipacaoCDLTexto = document.getElementById("valorParticipacaoCDLTexto");

					valorParticipacaoCDLString = participacaoCDLFloat.toString().replace(".",",");
					valorParticipacaoCDL.value = valorParticipacaoCDLString;
					valorParticipacaoCDLTexto = valorParticipacaoCDLString;
					
					aplicarMascaraNumeroDecimal(valorParticipacaoCDL,2);
					if(valorParticipacaoCDLString != ""){
						$("input#valorParticipacaoCDLTexto + span.itemDetalhamento").html("R$ " + document.getElementById("valorParticipacaoCDL").value);
					} else {
						$("input#valorParticipacaoCDLTexto + span.itemDetalhamento").html("");
					}	
					
				}
			}
		}

		function habilitarElementosImovel(field,idSelecionado,nuoLojas,codigoModalidadeMedicao){
			
			if(field == true){
				document.getElementById('numeroAptoLojas').value = nuoLojas;
				document.getElementById('idModalidadeMedicao').value = codigoModalidadeMedicao;
				var idSegmento = '-1'; 

				AjaxService.obterSegmentoPontoConsumoImovel( idSelecionado, {
		           	callback: function(segmento) {	           		
		           		if(segmento != null){  	           			        		      		         		
				        idSegmento = segmento["idSegmento"];
		               	} 
		        	}, async:false}
		        	
		        );

		        document.getElementById('idSegmento').value = idSegmento;
				document.getElementById('elementosImovel').style.display = "block";
			}else{
				document.getElementById('idSegmento').value = '-1';
				document.getElementById('elementosImovel').style.display = "none";
			}			
		}
		
		function init(){
			var arquivo = '${sessionScope.planilhaImportacao.name}';
			// S� recalcular o valor de investimento caso a p�gina tenha voltado da importa��o da planilha.
			if(arquivo != null & arquivo != '') {
				somaValorInvestimento();
			}
			
			if("${indicadorNovaProposta}" == "true"){				
				desabilitarVersao(true);
			}
			
			var condominio = "${requestScope.condominio}";
			if(condominio != null && condominio != ""){
				document.getElementById('elementosImovel').style.display = "block";
			}
		}
		
addLoadEvent(init);

</script>

<fieldset id="dadosAbaIdentificacaoProposta" style="float: left">
	<input type="hidden" id="numeroProposta" name="numeroProposta" value="${propostaVO.numeroProposta}">
	<input type="hidden" id="propostaLevantamentoMercado" name="propostaLevantamentoMercado" value="${propostaVO.propostaLevantamentoMercado}">
	<input type="hidden" id="chavePrimariaLM" name="chavePrimariaLM" value="${propostaVO.chavePrimariaLM}">
	<a class="linkHelp" href="<help:help>/inclusoalteraodapropostaabaidentificao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<c:if test="${param.alteracao}">
		<label class="rotulo campoObrigatorio" id="rotuloNumeroProposta" for="numeroProposta"><span class="campoObrigatorioSimbolo">* </span>N�mero da Proposta:</label>
		<input class="campoTexto" type="text" id="numeroCompletoProposta" name="numeroCompletoProposta" disabled="disabled " maxlength="30" size="11" onkeypress="return formatarCampoInteiro(event);" value="${propostaVO.numeroProposta}"><br />
		<label class="rotulo" id="rotuloIndicadorNovaProposta" for="indicadorNovaProposta">Gerar Nova Vers�o?</label>
		<input class="campoRadio" id="indicadorNovaProposta1" name="indicadorNovaProposta" value="true" type="radio" onclick="desabilitarVersao(true);"<c:if test="${indicadorNovaProposta eq true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorNovaProposta">Sim</label>
		<input class="campoRadio" id="indicadorNovaProposta2" name="indicadorNovaProposta" value="false" type="radio" onclick="habilitarVersao();" <c:if test="${indicadorNovaProposta ne true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorNovaProposta">N�o</label><br />
		<input type="hidden" id="versaoPropostaAnterior" name="versaoPropostaAnterior" value="${propostaVO.versaoPropostaAnterior}"><br />
	</c:if>
	<label class="rotulo campoObrigatorio" id="rotuloIdSituacao" for="idSituacaoProposta"><span class="campoObrigatorioSimbolo">* </span>Situacao da Proposta:</label>
	<select name="idSituacaoProposta" id="idSituacaoProposta" class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaSituacaoProposta}" var="situacao">
			<option value="<c:out value="${situacao.chavePrimaria}"/>" <c:if test="${propostaVO.idSituacaoProposta == situacao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${situacao.descricao}"/>
			</option>		
		</c:forEach>
	</select>
	<label class="rotulo" id="rotuloVersaoProposta" for="versaoProposta">Vers�o da Proposta:</label>
	<input class="campoTexto" type="text" id="versaoProposta" name="versaoProposta" maxlength="3" size="1" onkeypress="return formatarCampoInteiro(event);" value="${propostaVO.versaoProposta}"><br />
	<label class="rotulo campoObrigatorio" style="margin-bottom: 10px" id="rotuloDataEmissao" for="dataEmissao" ><span class="campoObrigatorioSimbolo">* </span>Data de Emiss�o:</label>
	<input class="campoData" type="text" style="margin-bottom: 4px" id="dataEmissao" name="dataEmissao" maxlength="10" value="${propostaVO.dataEmissao}"><br />
	<label class="rotulo" id="rotuloDataVigencia" for="dataVigencia" >Data Final de Vig�ncia:</label>
	<input class="campoData" type="text" id="dataVigencia" name="dataVigencia" maxlength="10" value="${propostaVO.dataVigencia}"><br />
	<label class="rotulo" id="rotuloDataEmissao" for="dataEntrega" >Data de Entrega:</label>
	<input class="campoData" type="text" id="dataEntrega" name="dataEntrega" maxlength="10" value="${propostaVO.dataEntrega}"><br />
	<label class="rotulo rotulo2Linhas" style="margin-bottom: 2px" id="rotuloDataApresentacaoAssembleia" for="dataAssembleiaCondominio" >Data de Apresenta��o em Assembl�ia:</label>
	<input class="campoData campo2Linhas" style="margin-bottom: 2px"type="text" id="dataAssembleiaCondominio" name="dataAssembleiaCondominio" maxlength="10" value="${propostaVO.dataAssembleiaCondominio}"><br />
	<label class="rotulo" id="rotuloDataEleicaoSindico" for="dataEleicaoSindico">Data Elei��o S�ndico:</label>
	<input class="campoData" type="text" id="dataEleicaoSindico" name="dataEleicaoSindico" maxlength="10" value="${propostaVO.dataEleicaoSindico}"><br />
	
	<label class="rotulo" id="rotuloIdSegmento" for="idSegmento">Segmento:</label>
	<select name="idSegmento" id="idSegmento" class="campoSelect">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaSegmento}" var="segmento">
			<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${propostaVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${segmento.descricao}"/>
			</option>		
		</c:forEach>
	</select><br class="quebraLinha2" />
		<fieldset id="elementosImovel" style="display: none;">
			<label class="rotulo" id="rotuloIdModalidadeMedicao" for="idModalidadeMedicao">Modalidade de Medi��o:</label>
			<select name="codigoModalidadeMedicao" id="idModalidadeMedicao" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaModalidadeMedicao}" var="modalidade">
					<option value="<c:out value="${modalidade.codigo}"/>" <c:if test="${propostaVO.codigoModalidadeMedicao == modalidade.codigo}">selected="selected"</c:if>>
						<c:out value="${modalidade.descricao}"/>
					</option>		
				</c:forEach>
			</select><br />
			<label class="rotulo" for="numeroAptoLojas">N� de Apto/Lojas:</label>
			<input class="campoTexto" type="text" id="numeroAptoLojas" name="numeroAptoLojas" maxlength="4" size="4" onkeypress="return formatarCampoInteiro(event);" value="${propostaVO.numeroAptoLojas}"><br />
		</fieldset>
	<label class="rotulo" id="rotuloPercentualTIR" for="percentualTIR" >Percentual TIR:</label>
	<input class="campoTexto campoHorizontal" type="text" id="percentualTIR" name="percentualTIR" maxlength="7" size="4" onkeypress="return formatarCampoDecimalNegativo(event,this,3,2);" value="${propostaVO.percentualTIR}">
	<label class="rotuloHorizontal rotuloInformativo" for="percentualTIR">%</label><br class="quebraLinha4">
	<label class="rotulo" id="rotuloValorMaterial" for="valorMaterial">Valor Material:</label>
	<input class="campoTexto campoValorRealCss" type="text" id="valorMaterial" name="valorMaterial" maxlength="17" size="15" onkeypress="return formatarCampoDecimalPositivo(event,this,11,2);" onblur="somaValorInvestimento();aplicarMascaraNumeroDecimal(this,2);"  value="${propostaVO.valorMaterial}"><br />
	<label class="rotulo" id="rotuloValorMaoDeObra" for="valorMaoDeObra" >Valor da M�o de Obra:</label>
	<input class="campoTexto campoValorRealNovo" type="text" id="valorMaoDeObra" name="valorMaoDeObra" maxlength="17" size="15" onkeypress="return formatarCampoDecimalPositivo(event,this,11,2);" onblur="somaValorInvestimento();aplicarMascaraNumeroDecimal(this,2);" value="${propostaVO.valorMaoDeObra}"><br />
	<label class="rotulo" id="rotuloValorInvestimento" for="valorInvestimento" >Valor do Investimento:</label>
	<input type="hidden" id="valorInvestimento" name="valorInvestimento" value="${propostaVO.valorInvestimento}">
	<input class="campoTexto campoValorReal" type="text" id="valorInvestimentoTexto" name="valorInvestimentoTexto" maxlength="15" size="15" value="${propostaVO.valorInvestimento}" disabled="disabled"><br />
	<label class="rotulo" id="rotuloIndicadorParticipacao" for="indicadorParticipacao">Participa��o do Cliente:</label>
	<input class="campoRadio" id="indicadorParticipacao1" name="indicadorParticipacao" value="true" type="radio" onclick="exibirDadosParticipacao();" <c:if test="${propostaVO.indicadorParticipacao == true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorParticipacao1">Sim</label>
	<input class="campoRadio" id="indicadorParticipacao2" name="indicadorParticipacao" value="false" type="radio" onclick="esconderDadosParticipacao();" <c:if test="${propostaVO.indicadorParticipacao == false or empty propostaVO.indicadorParticipacao}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorParticipacao2">N�o</label><br />
	<label class="rotulo" id="rotuloUnidadeOrganizacional" for="unidadeOrganizacional">Vendedor:</label>
	<select id="idVendedor" class="campoSelect" name="idVendedor" >
		<option value="-1">Selecione</option>
		<c:forEach items="${listaVendedor}" var="vendedor">
			<option value="<c:out value="${vendedor.chavePrimaria}"/>" <c:if test="${propostaVO.idVendedor == vendedor.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${vendedor.nome}"/>
			</option>
		</c:forEach>
	</select><br/>
	<label class="rotulo" id="rotuloUnidadeOrganizacional" for="unidadeOrganizacional">Fiscal:</label>
	<select id="idFiscal" class="campoSelect" name="idFiscal" >
		<option value="-1">Selecione</option>
		<c:forEach items="${listaFiscal}" var="fiscal">
			<option value="<c:out value="${fiscal.chavePrimaria}"/>" <c:if test="${propostaVO.idFiscal== fiscal.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${fiscal.nome}"/>
			</option>
		</c:forEach>
	</select><br/>
	
	<fieldset id="camposIndicadorParticipacao">
		<fieldset class="coluna2">
			<label class="rotulo campoObrigatorio" id="rotuloPercentualCliente" for="percentualCliente" ><span class="campoObrigatorioSimbolo">* </span>Percentual Cliente:</label>
			<input class="campoTexto campoHorizontal" type="text" id="percentualCliente" name="percentualCliente" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" onblur="preencherValorCliente(this);aplicarMascaraNumeroDecimal(this,2);" value="${propostaVO.percentualCliente}">
			<label class="rotuloHorizontal rotuloInformativo" for="percentualCliente">%</label><br class="quebra2Linhas">
			<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloValorCliente" for="valorCliente" ><span class="campoObrigatorioSimbolo">* </span>Valor do Investimento pelo Cliente:</label>
			<input class="campoTexto campo2Linhas campoValorReal" type="text" id="valorCliente" name="valorCliente" maxlength="15" size="15" onkeypress="return formatarCampoDecimalPositivo(event,this,12,2);" onblur="preencherPercentualCliente(this);aplicarMascaraNumeroDecimal(this,2);" value="${propostaVO.valorCliente}"><br />
			<label class="rotulo" id="rotuloValorParticipacaoCDL" for="valorParticipacaoCDL" >Participa��o CDL:</label>
			<input type="hidden" id="valorParticipacaoCDL" name="valorParticipacaoCDL" value="${propostaVO.valorParticipacaoCDL}">
			<input class="campoTexto campoValorReal" type="text" id="valorParticipacaoCDLTexto" name="valorParticipacaoCDLTexto" maxlength="15" size="15" onblur="aplicarMascaraNumeroDecimal(this,2);" value="${propostaVO.valorParticipacaoCDL}" disabled="disabled"><br />
		</fieldset>
		<fieldset class="colunaFinal2">
			<label class="rotulo" id="rotuloQuantidadeParcela" for="quantidadeParcela" >Quantidade de Parcelas:</label>
			<input class="campoTexto" type="text" id="quantidadeParcela" name="quantidadeParcela" maxlength="5" size="3" onkeypress="return formatarCampoInteiro(event);" value="${propostaVO.quantidadeParcela}"><br />
			<label class="rotulo" id="rotuloValorParcela" for="valorParcela" >Valor das Parcelas:</label>
			<input class="campoTexto campoValorReal" type="text" id="valorParcela" name="valorParcela" maxlength="14" size="15" onkeypress="return formatarCampoDecimalPositivo(event,this,11,2);" onblur="aplicarMascaraNumeroDecimal(this,2);" value="${propostaVO.valorParcela}">
			<label class="rotulo" id="rotuloPercentualJuros" for="percentualJuros" >Percentual dos Juros:</label>
			<input class="campoTexto campoHorizontal" type="text" id="percentualJuros" name="percentualJuros" maxlength="6" size="4" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" onblur="aplicarMascaraNumeroDecimal(this,2);" value="${propostaVO.percentualJuros}">
			<label class="rotuloInformativo" for="percentualJuros">%</label><br class="quebraLinha4">
			<label class="rotulo campoObrigatorio" id="rotuloPeriodicidadeJuros" for="periodicidadeJuros" ><span class="campoObrigatorioSimbolo">* </span>Periodicidade dos Juros:</label>
			<select name="idPeriodicidadeJuros" id="idPeriodicidadeJuros" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaPeriodicidadeJuros}" var="periodicidadeJuros">
					<option value="<c:out value="${periodicidadeJuros.chavePrimaria}"/>"<c:if test="${propostaVO.idPeriodicidadeJuros == periodicidadeJuros.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${periodicidadeJuros.descricao}"/>
					</option>		
				</c:forEach>
			</select><br />
			
		</fieldset>
	</fieldset>
</fieldset>

<fieldset id="pesquisarCliente" class="colunaMeio">
	<legend class="legendIndicadorPesquisa">Pesquisar Cliente</legend>
	<div class="pesquisarClienteFundo">
		<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span> para selecionar um Cliente.</p>
		<input name="idCliente" type="hidden" id="idCliente" value="${cliente.chavePrimaria}">
		<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${cliente.nome}">
		<input name="documentoFormatado" type="hidden" id="documentoFormatado" value="${cliente.numeroDocumentoFormatado}">
		<input name="enderecoFormatadoCliente" type="hidden" id="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal}">
		<input name="emailCliente" type="hidden" id="emailCliente" value="${cliente.emailPrincipal}">
		
		<input name="Button" id="botaoPesquisarCliente" class="bottonRightCol" title="Pesquisar cliente"  value="Pesquisar cliente" onclick="exibirPopupPesquisaCliente();" type="button"><br >
		<label class="rotulo" id="rotuloCliente" for="nomeClienteTexto">Cliente:</label>
		<input class="campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" size="50" disabled="disabled" value="${cliente.nome}"><br />
		<label class="rotulo" id="rotuloCnpjTexto" for="documentoFormatadoTexto">CPF/CNPJ:</label>
		<input class="campoDesabilitado" type="text" id="documentoFormatadoTexto" name="documentoFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${cliente.numeroDocumentoFormatado}"><br />	
		<label class="rotulo" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto">Endere�o:</label>
		<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoTexto" rows="2" cols="37" disabled="disabled">${cliente.enderecoPrincipal.enderecoFormatado}</textarea><br />
		<label class="rotulo" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
		<input class="campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${cliente.emailPrincipal}"><br />
	</div>
</fieldset>

<fieldset id="pesquisarImovel" style="float: right" class="colunaDir">
		<legend><span class="campoObrigatorioSimboloTabs">* </span>Pesquisar Im�vel</legend>
		<div class="pesquisarImovelFundo">
			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span> para selecionar um Im�vel.</p>
			<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}">
			<input name="nomeFantasia" type="hidden" id="nomeFantasia" value="${imovel.nome}">
			<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.chavePrimaria}">
			<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
			<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.quadraFace.endereco.cep.municipio.descricao}">
			<input name="indicadorCondominioAmbos" type="hidden" id="indicadorCondominioAmbos" value="${imovel.condominio}">
			
			<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Im�vel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
			<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
			<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto" maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
			<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
			<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto" onchange="habilitarElementosImovel(this);" maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
			<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
			<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
			<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
			<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.municipio.descricao}"><br />
			<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio?:</label>
			<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
			<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
		</div>
		
		<label class="rotulo rotuloVertical rotuloVerticalCss" for="observacao">Solicita��es Adicionais:</label>
		<textarea id="comentario" name="comentarioAdicional" class="campoTexto campoVertical" rows="3" onkeypress="return formatarCampoTextoComLimite(event,this,200);">${propostaVO.comentarioAdicional}</textarea>
		<c:if test="${param.alteracao}">
		<label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Usa:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${propostaVO.habilitado eq 'true'}">checked</c:if>><label id="rotuloHabilitado1" class="rotuloRadio">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${propostaVO.habilitado eq 'false'}">checked</c:if>><label id="rotuloHabilitado2" class="rotuloRadio">Inativo</label>
		</c:if>	
</fieldset>




<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Proposta.</p>