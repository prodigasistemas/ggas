<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="relatorioUtil" class="br.com.ggas.util.RelatorioUtil" />

<script>
		
	$(document).ready(function(){		

		$("#botaoOK").click(function(){
			realizarAcoesPreSubmit();
			$("#situacaoImovel").dialog("close");
		});
	});
	

	function realizarAcoesPreSubmit() {
		if($("#exibirFiltrosCheck").is(":checked")){
			$("#exibirFiltros").val("true");
		} else {
			$("#exibirFiltros").val("false");
		}

		<c:if test="${param.apenasXLS == null || !param.apenasXLS}">
			$(":radio[name=formatoImpressaoRadio]").each(function(){
				var radioFormatoImpressao = $(this).val();
				if($(this).is(":checked")){$("#formatoImpressao").val(radioFormatoImpressao);}
			});
		</c:if>

		<c:if test="${param.exibeAnaliticoSintetico}">
			$(":radio[name=tipoExibicaoRadio]").each(function(){
				var radioTipoExibicao = $(this).val();
				if($(this).is(":checked")){$("#tipoExibicao").val(radioTipoExibicao);}
			});
		</c:if>

		<c:if test="${param.exibeGrafico}">
			var checkExibirGraficos = $("#exibirGraficoCheck").val();
			if($("#exibirGraficoCheck").is(":checked")){$("#exibirGrafico").val(checkExibirGraficos);}

			var selectIdTipoGrafico = $("idTipoGraficoSelect").val();
			$("idTipoGrafico").val(selectIdTipoGrafico);
		</c:if>
		
		gerar();
	}
	
</script>


<input name="idSituacaoImovel" type="hidden" id="idSituacaoImovel" />

<div id="situacaoImovel" title="Situa��o Im�vel" style="display: none">
	
		<label class="rotulo" id="rotuloTipoCombustivel" for="tipoCombustivel" ><span class="campoObrigatorioSimbolo">* </span>Situa��o Im�vel:</label>
		<select name="idSituacao" id="idSituacao" class="campoSelect">
		<option value="-1">Selecione</option>
			<c:forEach items="${ listaSituacaoImovel }" var="situacao">
				<option value="<c:out value="${situacao.chavePrimaria}"/>" >
					<c:out value="${situacao.descricao}"/>
				</option>
			</c:forEach>
		</select><br />
	<br class="quebraLinha2" />	
	<hr class="linhaSeparadoraPopup" />
	
	<input type="button" id="botaoOK" class="bottonRightCol" value="OK" onClick="aprovar();">
</div>