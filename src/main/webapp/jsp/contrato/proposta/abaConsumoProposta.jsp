<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">


	function confirmaMudanca(flag) {

		if (flag == true) {

			mudarAnual = confirm("A mudan�a da 'Unidade de Consumo M�dio Anual' acarretar� na troca da 'Unidade de Consumo M�dio Mensal'\nDeseja Continuar?");

			if (mudarAnual == true) {
				valorUnidadeConsumoAnual();
			} else if (mudarAnual == false) {
				valorUnidadeConsumoMensal();
			}
		} else if (flag == false) {

			mudarMensal = confirm("A mudan�a da 'Unidade de Consumo M�dio Mensal' acarretar� na troca da 'Unidade de Consumo M�dio Anual'\nDeseja Continuar?");

			if (mudarMensal == true) {
				valorUnidadeConsumoMensal();
			} else if (mudarMensal == false) {
				valorUnidadeConsumoAnual();
			}
		}
	}

	function verificadorSelectMensal() {

		if (document.getElementById("idUnidadeConsumoAnual").value != '-1') {
			confirmaMudanca(false);
		} else {
			valorUnidadeConsumoMensal();
		}
	}

	function verificadorSelectAnual() {

		if (document.getElementById("idUnidadeConsumoMensal").value != '-1') {
			confirmaMudanca(true);
		} else {
			valorUnidadeConsumoAnual();
		}

	}

	function valorUnidadeConsumoAnual() {

		var valorSelecionado = document.getElementById("idUnidadeConsumoAnual").value;

		document.getElementById("idUnidadeConsumoMensal").value = valorSelecionado;
		inner(valorSelecionado);
	}

	function valorUnidadeConsumoMensal() {

		var valorSelecionado = document
				.getElementById("idUnidadeConsumoMensal").value;

		document.getElementById("idUnidadeConsumoAnual").value = valorSelecionado;
		inner(valorSelecionado);
	}

	function inner(valorSelecionado) {

		var label = document.getElementById("labelMudanca");

		if (valorSelecionado == '-1') {
			label.innerHTML = 'R$/m�s';
		}
		if (valorSelecionado == '35') {
			label.innerHTML = 'R$/Kg';
		}
		if (valorSelecionado == '25') {
			label.innerHTML = 'R$/m3';
		}
	}
	
	function calcularValorMensalGastoGN(){
		var selectedTarifa = document.getElementById("idTarifa");
		var valorMedioGN = document.getElementById("valorMedioGN");
		var idSelecionado = selectedTarifa.options[selectedTarifa.selectedIndex].value;
		if(idSelecionado > 0){
			var valorGastoMensalGN = document.getElementById("valorGastoMensalGN");
			var consumo = document.getElementById("consumoMedioMensalGN").value;
			var consumoFloat = converterStringParaFloat(consumo);
			consumoFloat = consumoFloat.toFixed(0);
			consumoFloat = consumoFloat.replace(",", "");
			
			if(idSelecionado != '') {				
				AjaxService.calcularValorMensalGasto( idSelecionado, consumo, {
		           	callback: function(dados) {	           		
		           		if(dados != null){  	           			        		      		         		
		           			valorGastoMensalGN.value = dados["valorMensalGasto"];
		           			valorMedioGN.value = dados["valorMedioTarifa"].replace(".",",");
		               	}
		        	}, async:false}
		        	
		        );	        
	        } else {
	        	valorGastoMensalGN.value = "";
	       	}
		}
	}
	
	function calcularValores(){
		var consumoMedioAnualGN = document.getElementById('consumoMedioAnualGN').value;
		var consumoMedioMensalGN = document.getElementById('consumoMedioMensalGN').value;
		var volumeDiarioEstimadoGN = document.getElementById('volumeDiarioEstimadoGN').value;
		
		
		if(consumoMedioAnualGN.length > 0){
			var valorFloat = converterStringParaFloat(consumoMedioAnualGN);
			var valorMensal = valorFloat/12;
			var valorDiario = valorFloat/360;
			document.getElementById('consumoMedioMensalGN').value = valorMensal.toFixed(4).replace(".",",");
			document.getElementById('volumeDiarioEstimadoGN').value = valorDiario.toFixed(4).replace(".",",");
		
		}else if(consumoMedioMensalGN.length > 0 && consumoMedioAnualGN.length <= 0){
			var valorFloat = converterStringParaFloat(consumoMedioMensalGN);
			var valorAnual = valorFloat*12;
			var valorDiario = valorFloat/30;
			
			document.getElementById('consumoMedioAnualGN').value = valorAnual.toFixed(4).replace(".",",");
			document.getElementById('volumeDiarioEstimadoGN').value = valorDiario.toFixed(4).replace(".",",");
		
		}else if(volumeDiarioEstimadoGN.length > 0 && consumoMedioMensalGN.length <= 0){
			var valorFloat = converterStringParaFloat(volumeDiarioEstimadoGN);
			var valorMensal = valorFloat*30;
			var valorAnual = valorFloat*360;
			
			document.getElementById('consumoMedioMensalGN').value = valorMensal.toFixed(4).replace(".",",");
			document.getElementById('consumoMedioAnualGN').value = valorAnual.toFixed(4).replace(".",",");
		}
	}
	
	function calcularDiferancaDeValorEntreCombustiveis(){
		var valorGastoMensal = document.forms[0].valorGastoMensal.value;
		var valorGastoMensalGN = document.forms[0].valorGastoMensalGN.value;
		var economiaMensalGN = document.forms[0].economiaMensalGN;
		var economiaAnualGN = document.forms[0].economiaAnualGN;
		var percentualEconomia = document.forms[0].percentualEconomia;
		
		valorGastoMensal = valorGastoMensal.replace(" ", "");
		valorGastoMensalGN = valorGastoMensalGN.replace(" ", "");
		if(valorGastoMensal.replace(",", "") > 0 && valorGastoMensalGN.replace(",", "") > 0){
			var valorGastoMensalFloat = converterStringParaFloat(valorGastoMensal);
			var valorGastoMensalGNFloat = converterStringParaFloat(valorGastoMensalGN);
			
			var diferencaMensal = valorGastoMensalFloat - valorGastoMensalGNFloat;
			var diferencaAnual = diferencaMensal*12;
			economiaMensalGN.value = diferencaMensal.toFixed(4).replace(".",",");
			economiaAnualGN.value = diferencaAnual.toFixed(4).replace(".",",");
			
			//Calcular diferen�a em percentual
			var diferenca = (valorGastoMensalFloat - valorGastoMensalGNFloat)*100;
			var percentualEconomia = diferenca/valorGastoMensalFloat;
			
			document.getElementById('percentualEconomia').value = percentualEconomia.toFixed(4).replace(".",",");
		}
	}
	
</script>

<fieldset id="conteinerDadosGLP" class="coluna2">
	<input name="fluxoLevantamentoMercado" type="hidden" id="fluxoLevantamentoMercado" value="${fluxoLevantamentoMercado}">
	<a class="linkHelp" href="<help:help>/abacomparativoglpegncadastroalteraodaproposta.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<legend class="conteinerBlocoTitulo"></legend>
	<label class="rotulo" id="rotuloModalidadeUso" for="modalidadeUso" >Modalidade de Uso:</label>
	<select name="idModalidadeUso" id="idModalidadeUso" class="campoSelect">
	<option value="-1">Selecione</option>
		<c:forEach items="${listaModalidadeUso}" var="modalidade">
			<option value="<c:out value="${modalidade.chavePrimaria}"/>" <c:if test="${modalidade.chavePrimaria eq propostaVO.idModalidadeUso}"> selected </c:if>>
				<c:out value="${modalidade.descricao}"/>
			</option>
		</c:forEach>
	</select><br />
	<label class="rotulo" id="rotuloConsumoMedioAnual" for="consumoMedioAnual" >Consumo M�dio Anual:</label>
	<input class="campoTexto campoHorizontal" type="text" id="consumoMedioAnual" name="consumoMedioAnual" maxlength="16" size="12" onkeypress="return formatarCampoDecimal(event, this, 7, 4)" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${propostaVO.consumoMedioAnual}">
	<select name="idUnidadeConsumoAnual" id="idUnidadeConsumoAnual" class="campoSelectPequeno" onchange="verificadorSelectAnual();" >
		<option value="-1">Selecione</option>
		<c:forEach items="${listaUnidadeConsumo}" var="unidadeConsumoAnual">
			<option value="<c:out value="${unidadeConsumoAnual.chavePrimaria}"/>" <c:if test="${propostaVO.idUnidadeConsumoAnual == unidadeConsumoAnual.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${unidadeConsumoAnual.descricaoAbreviada}"/>
			</option>		
		</c:forEach>
	</select><br />
	<label class="rotulo" id="rotuloConsumoMedioMensal" for="consumoMedioMensal" >Consumo M�dio Mensal:</label>
	<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="consumoMedioMensal" name="consumoMedioMensal" maxlength="16" size="12" onkeypress="return formatarCampoDecimal(event, this, 7, 4)" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${propostaVO.consumoMedioMensal}">
	<select name="idUnidadeConsumoMensal" id="idUnidadeConsumoMensal" class="campoSelectPequeno" onchange="verificadorSelectMensal();">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaUnidadeConsumo}" var="unidadeConsumoMensal">
			<option value="<c:out value="${unidadeConsumoMensal.chavePrimaria}"/>" <c:if test="${propostaVO.idUnidadeConsumoMensal == unidadeConsumoMensal.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${unidadeConsumoMensal.descricaoAbreviada}"/>
			</option>		
		</c:forEach>
	</select><br />
	<label class="rotulo rotulo2Linhas" id="rotuloAnoMesReferenciaPreco" for="anoMesReferenciaPreco" >M�s / Ano de<br />Refer�ncia do Pre�o:</label>
	<input class="campoTexto campoHorizontal campo2Linhas" type="text" id="anoMesReferenciaPreco" name="anoMesReferenciaPreco" maxlength="7" size="5" onkeypress="return formatarCampoInteiro(event);" value="${propostaVO.anoMesReferenciaPreco}"><br />
	<label class="rotulo" id="rotuloValorGastoMensal" for="valorGastoMensal" >Valor Mensal:</label>
	<input class="campoTexto campo2Linhas campoValorReal campoHorizontal valorGastoMensal" type="text" id="valorGastoMensal" name="valorGastoMensal" maxlength="14" size="15" onkeypress="return formatarCampoDecimal(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${propostaVO.valorGastoMensal}">
	<label class="rotuloHorizontal rotuloInformativo"  id="labelMudanca" for="percentualTIR">R$/m�s</label><br class="quebraLinha0">
	<label class="rotulo rotulo2Linhas" id="rotuloConsumoUnidadeConsumidora" for="consumoUnidadeConsumidora">Consumo da Unidade<br />Consumidora em GN:</label>
	<input class="campoTexto campo2Linhas campoHorizontal campo2Linhas" type="text" id="consumoUnidadeConsumidora" name="consumoUnidadeConsumidora" maxlength="16" size="12" onkeypress="return formatarCampoDecimal(event, this, 7, 4);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${propostaVO.consumoUnidadeConsumidora}">
	<label class="rotuloInformativo" for="vazaoInstantanea">m<span class="expoente">3</span>/dia</label>
</fieldset>			

<fieldset id="conteinerDadosGN" class="colunaFinal2">
	<legend class="conteinerBlocoTitulo">GN</legend>	
	<label class="rotulo" id="rotuloConsumoMedioAnualGN" for="consumoMedioAnualGN" >Consumo M�dio Anual:</label>
	<input class="campoTexto campoHorizontal" type="text" id="consumoMedioAnualGN" name="consumoMedioAnualGN" maxlength="16" size="12" onkeypress="return formatarCampoDecimal(event, this, 7, 4)" onblur="aplicarMascaraNumeroDecimal(this, 4); calcularValores();" value="${propostaVO.consumoMedioAnualGN}">
	<label class="rotuloHorizontal rotuloInformativo" for="consumoMedioAnualGN">m<span class="expoente">3</span></label><br />
	
	<label class="rotulo" id="rotuloConsumoMedioMensalGN" for="consumoMedioMensalGN" >Consumo M�dio Mensal:</label>
	<input class="campoTexto campoHorizontal" type="text" id="consumoMedioMensalGN" name="consumoMedioMensalGN" maxlength="16" size="12" onkeypress="return formatarCampoDecimal(event, this, 7, 4)" onblur="aplicarMascaraNumeroDecimal(this, 4); calcularValores();" value="${propostaVO.consumoMedioMensalGN}">
	<label class="rotuloHorizontal rotuloInformativo" for="consumoMedioMensalGN">m<span class="expoente">3</span></label><br />
	
	<label class="rotulo" id="rotuloVolumeDiarioEstimado" for="volumeDiarioEstimadoGN" >Volume Di�rio Estimado:</label>
	<input class="campoTexto campoHorizontal" type="text" id="volumeDiarioEstimadoGN" name="volumeDiarioEstimadoGN" maxlength="16" size="12" onkeypress="return formatarCampoDecimal(event, this, 7, 4)" onblur="aplicarMascaraNumeroDecimal(this, 4); calcularValores();" value="${propostaVO.volumeDiarioEstimadoGN}">
	<label class="rotuloHorizontal rotuloInformativo" for="volumeDiarioEstimado">m<span class="expoente">3</span></label><br />
	
	<label class="rotulo" for="tarifa" >Tarifa:</label>
	<select name="idTarifa" id="idTarifa" style="max-width: 158px; min-width: 158px;"class="campoSelect" 
	onChange="calcularValorMensalGastoGN()">
		<option value="-1">Selecione</option>
		<c:forEach items="${ listaTarifas }" var="tarifa">
			<option value="<c:out value="${tarifa.chavePrimaria}"/>" <c:if test="${propostaVO.idTarifa == tarifa.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${tarifa.descricao}"/>
			</option>		
		</c:forEach>
	</select>
	
	<label class="rotulo" id="rotuloValorGastoMensalGN" for="valorGastoMensalGN" >Valor:</label>
	<input class="campoTexto campoValorReal campoHorizontal valorGastoMensal" type="text" id="valorMedioGN" name="valorMedioGN" maxlength="14" size="15" onkeypress="return formatarCampoDecimal(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${propostaVO.valorMedioGN}">
	<label class="rotuloHorizontal rotuloInformativo" for="percentualEconomia">R$/m<span class="expoente">3</span></label><br />
	
	<label class="rotulo" id="rotuloValorGastoMensalGN" for="valorGastoMensalGN" >Valor Mensal:</label>
	<input class="campoTexto campoValorReal campoHorizontal valorGastoMensal" onBlur="calcularDiferancaDeValorEntreCombustiveis();" type="text" id="valorGastoMensalGN" name="valorGastoMensalGN" maxlength="14" size="15" onkeypress="return formatarCampoDecimal(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 4);" value="${propostaVO.valorGastoMensalGN}"><br />
	
	<label class="rotulo" id="rotuloEconomiaMensalGN" for="economiaMensalGN" >Economia Mensal GN:</label>
	<input class="campoTexto campoValorReal campoHorizontal valorGastoMensal" type="text" id="economiaMensalGN" name="economiaMensalGN" maxlength="16" size="15" onkeypress="return formatarCampoDecimal(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${propostaVO.economiaMensalGN}"><br />
	
	<label class="rotulo" id="rotuloEconomiaAnualGN" for="economiaAnualGN" >Economia Anual GN:</label>
	<input class="campoTexto campoValorReal campoHorizontal valorGastoMensal" type="text" id="economiaAnualGN" name="economiaAnualGN" maxlength="16" size="15" onkeypress="return formatarCampoDecimal(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${propostaVO.economiaAnualGN}"><br />
	
	<label class="rotulo" id="rotuloPercentualEconomia" for="percentualEconomia" >Percentual de Economia:</label>
	<input class="campoTexto" type="text" style="width: 154px" id="percentualEconomia" name="percentualEconomia" onkeypress="return formatarCampoDecimal(event, this, 3, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);" value="${propostaVO.percentualEconomia}">
	<label class="rotuloHorizontal rotuloInformativo" for="percentualEconomia">%</label>
	
</fieldset>

<fieldset style="margin-top: 50px;" id="gridLM">

	<display:table class="dataTableGGAS" name="listaTipoCombustivelLM" sort="list" id="levantamentoMercadoTipoCombustivel" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.descricao" title="Combust�vel">
			<c:out value="${ levantamentoMercadoTipoCombustivel.tipoCombustivel.descricao }"/>
		</display:column>
		
		<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.consumo" title="Consumo (Kg)" >
			<fmt:formatNumber
						value="${levantamentoMercadoTipoCombustivel.consumo}" maxFractionDigits="2"
						minFractionDigits="2"/>
		</display:column>
		
		<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.periodicidade.descricao" title="Periodicidade" >
			<c:out value="${levantamentoMercadoTipoCombustivel.periodicidade.descricao}"></c:out>
		</display:column>
		
		<display:column  sortable="false" sortProperty="levantamentoMercadoTipoCombustivel.preco" title="Pre�o (R$/Kg)" >
			<fmt:formatNumber
						value="${levantamentoMercadoTipoCombustivel.preco}" maxFractionDigits="2"
						minFractionDigits="2"/>
		</display:column>
	</display:table>	

</fieldset>

		