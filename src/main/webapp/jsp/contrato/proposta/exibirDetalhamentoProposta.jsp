<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<h1 class="tituloInterno">Detalhar Proposta<a class="linkHelp" href="<help:help>/cadastrodapropostadetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form method="post" id="propostaForm" name="propostaForm" action="exibirDetalhamentoProposta" enctype="multipart/form-data"  >

<script>
	
	function voltar(){	
		submeter('propostaForm',  'cancelarAcaoProposta');
	}
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter('propostaForm', 'exibirAlteracaoProposta');
	}

</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoProposta">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${propostaVO.chavePrimaria}">
<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${propostaVO.chavesPrimarias}">

<fieldset id="detalhamentoProposta" class="detalhamento">
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Identifica��o</legend>
		<fieldset class="colunaEsq detalhamentoColunaLarga">
			<label class="rotulo">Im�vel:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${imovel.nome}"/></span><br />
			<label class="rotulo">Cliente:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${cliente.nome}"/></span><br />
			<label class="rotulo">Modalidade de Medi��o:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${imovel.modalidadeMedicaoImovel.descricao}"/></span><br />
			<label class="rotulo">N� de Apto/Lojas:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${propostaVO.numeroAptoLojas}"/></span><br />
			<label class="rotulo">N�mero Proposta:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.numeroProposta}"/></span><br />
			<label class="rotulo rotulo2Linhas">N�mero da Vers�o da Proposta:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${propostaVO.versaoProposta}"/></span><br />
			<label class="rotulo">Data Emiss�o:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.dataEmissao}"/></span><br />
			<label class="rotulo">Data Final de Vig�ncia:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.dataVigencia}"/></span><br />
			<label class="rotulo">Data da Entrega:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.dataEntrega}"/></span><br />
			<label class="rotulo">Situa��o da proposta:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.descricaoSituacaoProposta}"/></span><br />
			<label class="rotulo rotulo2Linhas">Data Apresenta��o em Assembl�ia:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${propostaVO.dataAssembleiaCondominio}"/></span><br />
			<label class="rotulo">Data Elei��o de S�ndico:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.dataEleicaoSindico}"/></span><br />
			<label class="rotulo">Segmento:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.descricaoSegmento}"/></span><br />
			<c:if test="${propostaVO.indicadorMedicao == true}">
				<label class="rotulo">Modalidade de Medi��o:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.modalidadeMedicaoImovel.descricao}"/></span><br />
				<label class="rotulo">N� de Apto/Lojas:</label>
				<span class="itemDetalhamento"><c:out value="${propostaVO.numeroAptoLojas}"/></span><br />				
			</c:if>
			<label class="rotulo">Solicita��es Adicionais:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno" style="word-wrap: break-word"><c:out value="${propostaVO.comentarioAdicional}"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal2">
			<label class="rotulo">Percentual TIR:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.percentualTIR}"/></span>
			<c:if test="${not empty (propostaVO.percentualTIR)}"><label class="rotuloInformativo">%</label></c:if><br />
			<label class="rotulo">Valor do Material:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (propostaVO.valorMaterial)}">R$ </c:if><c:out value="${propostaVO.valorMaterial}"/></span><br />
			<label class="rotulo">Valor M�o de Obra:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (propostaVO.valorMaoDeObra)}">R$ </c:if><c:out value="${propostaVO.valorMaoDeObra}"/></span><br />
			<label class="rotulo">Valor Total do Investimento:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (propostaVO.valorInvestimento)}">R$ </c:if><c:out value="${propostaVO.valorInvestimento}"/></span><br />
			<c:if test="${propostaVO.indicadorParticipacao eq false}">
				<label class="rotulo">Participa��o do Cliente:</label>
				<span class="itemDetalhamento"><c:out value="N�o"/></span>
				<label class="rotulo">Vendedor:</label>
				<span class="itemDetalhamento"><c:out value="${propostaVO.nomeVendedor}"/></span>
				<label class="rotulo">Fiscal:</label>
				<span class="itemDetalhamento"><c:out value="${propostaVO.nomeFiscal}"/></span>
			</c:if>
			<c:if test="${propostaVO.indicadorParticipacao eq true}">
			<hr class="linhaSeparadora" />
			<label class="rotulo">Participa��o do Cliente:</label>
			<span class="itemDetalhamento"><c:out value="Sim"/></span>
			<fieldset class="conteinerDados">
				<label class="rotulo">Percentual Cliente:</label>
				<span class="itemDetalhamento"><c:out value="${propostaVO.percentualCliente}"/></span>
				<label class="rotuloInformativo">%</label><br />
				<label class="rotulo rotulo2Linhas">Valor do Investimento pelo Cliente:</label>
				<span class="itemDetalhamento itemDetalhamento2Linhas">R$ <c:out value="${propostaVO.valorCliente}"/></span><br />
				<label class="rotulo">Participa��o CDL:</label>
				<span class="itemDetalhamento">R$ <c:out value="${propostaVO.valorParticipacaoCDL}"/></span><br />
				<label class="rotulo">Quantidade de Parcelas:</label>
				<span class="itemDetalhamento"><c:out value="${propostaVO.quantidadeParcela}"/></span><br />
				<label class="rotulo">Valor das Parcelas:</label>
				<span class="itemDetalhamento">R$ <c:out value="${propostaVO.valorParcela}"/></span><br />
				<label class="rotulo">Percentual de Juros:</label>
				<span class="itemDetalhamento"><c:out value="${propostaVO.percentualJuros}"/></span>
				<label class="rotuloInformativo">%</label><br />
				<label class="rotulo">Periodicidade de Juros:</label>
				<span class="itemDetalhamento"><c:out value="${propostaVO.periodicidadeJuros}"/></span>
			</fieldset>
			</c:if>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
					
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Comparativo GLP e GN</legend>	
       	<fieldset class="colunaEsq detalhamentoColunaLarga">
       		<legend>GLP:</legend>
			<label class="rotulo">Consumo M�dio Anual:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.consumoMedioAnual}"/></span>
			<c:if test="${not empty propostaVO.consumoMedioAnual}"><label class="rotuloInformativo"><c:out value="${propostaVO.descricaoUnidadeConsumoAnual}"/></label></c:if><br />
							
			<label class="rotulo">Consumo M�dio Mensal:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.consumoMedioMensal}"/></span>
			<c:if test="${not empty propostaVO.consumoMedioMensal}"><label class="rotuloInformativo"><c:out value="${propostaVO.descricaoUnidadeConsumoMensal}"/></label></c:if><br />
			
			<label class="rotulo rotulo2Linhas">M�s / Ano de<br />Refer�ncia do Pre�o:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${propostaVO.anoMesReferenciaPreco}"/></span><br />
			
			<label class="rotulo">Valor Mensal:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (propostaVO.valorGastoMensal)}">R$ </c:if><c:out value="${propostaVO.valorGastoMensal}"/></span><c:if test="${not empty (propostaVO.valorGastoMensal)}"><label class="rotuloInformativo">/m�s</label></c:if><br />
			
			<label class="rotulo rotulo2Linhas">Consumo da Unidade<br />Consumidora em GN:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${propostaVO.consumoUnidadeConsumidora}"/></span>
			<c:if test="${not empty (propostaVO.consumoUnidadeConsumidora)}"><label class="rotuloInformativo2LinhasDetalhar">m<span class='expoente'>3</span>/dia</label></c:if>
		</fieldset>
		
		<fieldset class="colunaFinal2">
			<legend>GN:</legend>
			<label class="rotulo">Consumo M�dio Anual:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.consumoMedioAnualGN}"/></span>
			<c:if test="${not empty propostaVO.consumoMedioAnualGN}"><label class="rotuloInformativo">m<span class="expoente">3</span></label></c:if><br />

			<label class="rotulo">Consumo M�dio Mensal:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.consumoMedioMensalGN}"/></span>
			<c:if test="${not empty propostaVO.consumoMedioMensalGN}"><label class="rotuloInformativo">m<span class="expoente">3</span></label></c:if><br />
			
			<label class="rotulo">Volume Di�rio Estimado:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.volumeDiarioEstimadoGN}"/></span>
			<c:if test="${not empty (propostaVO.volumeDiarioEstimadoGN)}"><label class="rotuloInformativo">m<span class="expoente">3</span></label></c:if><br />
			
			<label class="rotulo">Tarifa:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.descricaoTarifa}"/></span>
			
			<label class="rotulo">Valor:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.valorMedioGN}"/></span>
			
			<label class="rotulo">Valor Mensal:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (propostaVO.valorGastoMensalGN)}">R$ </c:if><c:out value="${propostaVO.valorGastoMensalGN}"/></span>
			<c:if test="${not empty (propostaVO.valorGastoMensalGN)}"><label class="rotuloInformativo">/m�s</label></c:if><br />
			
			<label class="rotulo">Economia Mensal GN:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (propostaVO.economiaMensalGN)}">R$ </c:if><c:out value="${propostaVO.economiaMensalGN}"/></span><br />
			
			<label class="rotulo">Economia Anual GN:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (propostaVO.economiaAnualGN)}">R$ </c:if><c:out value="${propostaVO.economiaAnualGN}"/></span><br />
							
			<label class="rotulo">Percentual de Economia:</label>
			<span class="itemDetalhamento"><c:out value="${propostaVO.percentualEconomia}"/></span>
			<c:if test="${not empty (propostaVO.percentualEconomia)}"><label class="rotuloInformativo">%</label></c:if>
		</fieldset>
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">    
    <input id="botaoAlterar" name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
</fieldset>

</form:form>
