<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Contrato Adequa��o Parceria<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script language="javascript">

	$(document).ready(function(){

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		//s� executa se a funcionalidade de migra��o do modelo do contrato for realizada
		var logErroMigracaoContrato = "<c:out value="${sessionScope.logErroMigracao}"/>";
		if(logErroMigracaoContrato != ""){
			submeter('contratoAdequacaoParceriaForm', 'exibirLogErroMigracaoContrato');
		}
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};			
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	
	function limparFormulario(){
		document.Form.numeroContrato.value = "";
		document.Form.habilitado[0].checked = true;	
		
    	limparCamposPesquisa();
    	
    	document.Form.indicadorPesquisa[0].checked = false;
    	document.Form.indicadorPesquisa[1].checked = false;
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	
	function incluir() {
// 		document.forms[0].fluxoPesqusia.value = true;
		
		submeter("contratoAdequacaoParceriaForm", "exibirInclusaoContratoAdequacao");
	}
	
	
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function removerContrato(){
		var selecao = verificarSelecao();
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
 				submeter('contratoAdequacaoParceriaForm', 'removerContratoAdequacaoParceria');
			}
	    }
	} 	
	
	function detalharContratoAdequacaoParceria(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("contratoAdequacaoParceriaForm", "exibirDetalhamentoContratoAdequacaoParceria");
		
	}

	function alterarContrato(){
		var selecao = verificarSelecaoApenasUm();
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		
		if (selecao == true) {	
			submeter("contratoAdequacaoParceriaForm", "exibirAtualizacaoContratoAdequacaoParceria");
			
		}
	}
	
	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('nomeImovelTexto').value = "";
       	document.getElementById('numeroImovelTexto').value = "";
       	document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroContratoPrincipal').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		document.Form.habilitado[0].checked = true;	
    }
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}

	function init() {
		
		<c:choose>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>
		var idCliente = '${idCliente}';
		if(idCliente != ''){
			selecionarCliente(idCliente);
		}
		
		var idImovel = '${idImovel}'
		if(idImovel != ''){
			selecionarImovel(idImovel);
		}
	}	
	
	addLoadEvent(init);		
	
	function pesquisarContratoAdequacaoParceria(){
		submeter("contratoAdequacaoParceriaForm", "pesquisarContratoAdequacaoParceria");
	}
	
	function pesquisarpontoConsumo(){
		submeter("contratoAdequacaoParceriaForm", "pesquisarAssociacaoPontoConsumo");
	}
	
	
	
	</script>

<form:form method="post" action="pesquisarContratoAdequacaoParceria" name="contratoAdequacaoParceriaForm"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarContratoAdequacaoParceria"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoParceria.chavePrimaria}"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idCliente" type="hidden" id="idCliente" value="${idCliente}"/>
	<input name="idImovel" type="hidden" id="idImovel" value="${idImovel}"/>
	
	<fieldset id="pesquisarContrato" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${Form.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${Form.map.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${Form.map.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${Form.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${Form.map.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${Form.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${Form.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${Form.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${Form.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${Form.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${Form.map.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${Form.map.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${Form.map.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${Form.map.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${Form.map.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${Form.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${Form.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
	
		<fieldset class="conteinerBloco">
			<fieldset class="colunaEsq" style="margin-top:30px ;margin-right:40px; padding-right: 70px">
				<label class="rotulo" id="rotuloNumeroContratoPincipal" for="numeroContratoPrincipal">N�mero do Contrato Principal:<!-- fix bug --></label>
				<input class="campoTexto3" type="text" id="numeroContratoPrincipal" name="numeroContratoPrincipal" maxlength="9" size="8" value="${contratoParceria.numeroContratoPrincipal}" onkeypress="return formatarCampoInteiro(event);"><br />
			</fieldset>
			
			<fieldset class="colunaDir" style="margin-top:30px ;margin-right:30px;">
				<label class="rotulo" for="habilitado">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="ativo" value="true" <c:if test="${habilitado eq 'true' or empty habilitado}">checked</c:if>>
				<label class="rotuloRadio" for="ativo">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="inativo" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="inativo">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="todos" value="" <c:if test="${empty habilitado}">checked</c:if>>
				<label class="rotuloRadio" for="todos">Todos</label>
				<br />
			</fieldset>
		
			<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarContrato">
    			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarContratoAdequacaoParceria();">
    		</vacess:vacess>							
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">		
			</fieldset>
		</fieldset>
	</fieldset>
			
			
	<c:if test="${listaContratosAdequacaoParceria ne null}">
  			<hr class="linhaSeparadoraPesquisa" />
			<display:table class="dataTableGGAS" name="listaContratosAdequacaoParceria"
			sort="list" id="contrato"
			decorator="br.com.ggas.web.contratoadequacaoparceria.decorator.ContratoAdequacaoParceriaResultadoPesquisaDecorator"
			pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="pesquisarContratoAdequacaoParceria">
			
			
			
			<display:column property="chavePrimariaComLock" style="width: 25px"
 				media="html" sortable="false" 
 				title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>" /> 

			<display:column style="width: 30px" title="Ativo">
				<c:choose>
					<c:when test="${contrato.habilitado == true}">
						<img alt="Ativo" title="Ativo"
							src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo"
							src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>

			<display:column sortable="true" title="Contrato Principal"
				style="width: 110px" sortProperty="contrato.numeroContratoPrincipal">
				<a
					href='javascript:detalharContratoAdequacaoParceria(<c:out value='${contrato.chavePrimaria}'/>);'><span
					class="linkInvisivel"></span> <c:out
						value='${contrato.contrato.numeroCompletoContrato}' /> </a>
			</display:column>

			<display:column sortable="true" title="Ponto Consumo"
				style="width: 110px" sortProperty="contrato.pontoConsumo.descricao">
				<a
					href='javascript:detalharContratoAdequacaoParceria(<c:out value='${contrato.chavePrimaria}'/>);'><span
					class="linkInvisivel"></span> <c:out
						value='${contrato.pontoConsumo.descricao}' /> </a>
			</display:column>

			<display:column sortable="true" title="Valor Pago Cliente"
				style="width: 110px" sortProperty="valorCliente">
				<a
					href='javascript:detalharContratoAdequacaoParceria(<c:out value='${contrato.chavePrimaria}'/>);'><span
					class="linkInvisivel"></span> <fmt:formatNumber
						value="${contrato.valorCliente}" maxFractionDigits="2"
						minFractionDigits="2" /> </a>
			</display:column>

			<display:column sortable="true" title="Valor Pago CDL"
				style="width: 110px" sortProperty="valorEmpresa">
				<a
					href='javascript:detalharContratoAdequacaoParceria(<c:out value='${contrato.chavePrimaria}'/>);'><span
					class="linkInvisivel"></span> <fmt:formatNumber
						value="${contrato.valorEmpresa}" maxFractionDigits="2"
						minFractionDigits="2" /> </a>
			</display:column>

			<display:column sortable="true" title="Prazo" style="width: 110px"
				sortProperty="prazo">
				<a
					href='javascript:detalharContratoAdequacaoParceria(<c:out value='${contrato.chavePrimaria}'/>);'><span
					class="linkInvisivel"></span> <c:out value='${contrato.prazo}' /> </a>
			</display:column>

		</display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaContratosAdequacaoParceria}">
			
			<vacess:vacess param="exibirSalvarAlteracaoPontoConsumo">
				<input name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarContrato();" type="button">
			</vacess:vacess>
			
			<vacess:vacess param="excluirContrato">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerContrato();" type="button">
			</vacess:vacess>
			
		</c:if>
		<vacess:vacess param="exibirInclusaoContrato">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>

</form:form> 
