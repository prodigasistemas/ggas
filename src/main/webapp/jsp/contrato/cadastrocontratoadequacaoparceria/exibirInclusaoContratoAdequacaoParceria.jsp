<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Incluir Contrato Adequa��o Parceria<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em  <span class="destaqueOrientacaoInicial"> Salvar </span>para finalizar.</p>
<script language="javascript">

	$(document).ready(function(){
		
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		//s� executa se a funcionalidade de migra��o do modelo do contrato for realizada
		var logErroMigracaoContrato = "<c:out value="${sessionScope.logErroMigracao}"/>";
		if(logErroMigracaoContrato != ""){
			submeter('contratoAdequacaoParceriaForm', 'exibirLogErroMigracaoContrato');
		}
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};			
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}
	
	function formatarCampos(){
		var per = $("#percentualMulta").val();
		$("#percentualMulta").val(per.replace(".", ","));
		var per = $("#valorPagoPelaCdl").val();
		$("#valorPagoPelaCdl").val(per.replace(".", ","));
		var per = $("#valorPagoPeloCliente").val();
		$("#valorPagoPeloCliente").val(per.replace(".", ","));
	}
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	
	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaContratoAdequacao"/>';
	}
	
	function limparFormulario(){
		document.Form.numeroContrato.value = "";
		document.Form.habilitado[0].checked = true;	
		
    	limparCamposPesquisa();
    	
    	document.Form.indicadorPesquisa[0].checked = false;
    	document.Form.indicadorPesquisa[1].checked = false;
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	
	function limparCampos(){

		limparFormularioDadosCliente();		
		
		document.getElementById('nomeImovelTexto').value = "";
  	 	document.getElementById('numeroImovelTexto').value = "";
    	document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroContratoPrincipal').value = "";
		document.getElementById('dataInicioVigencia').value = "";
		document.getElementById('numeroContratoPrincipal').value = "";
		document.getElementById('valorPagoPelaCdl').value = "";
		document.getElementById('indiceFinanceiro').value = "-1";
		document.getElementById('percentualMulta').value = "";
		document.getElementById('valorPagoPeloCliente').value = "";
		document.getElementById('prazoVencimento').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		
		
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		
		
		var checkBox = document.getElementsByName("chavesPrimariasFaturas");
		for( var i = 0 ; i < checkBox.length ; i++){
			checkBox[i].checked = false;
		} 		
	}
	
	function manterDadosCheckBox(valor){
		<c:forEach items="${Form.map.chavesPrimariasFaturas}" var="fatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>').checked = valor;
			}	
		</c:forEach>

	}
	
	function init() {
		
		manterDadosCheckBox(true);
		
		<c:choose>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
	}
	
	
	
	function pesquisarpontoConsumo(){
// 		document.forms[0].fluxoPesquisa.value = false;
		submeter("contratoAdequacaoParceriaForm", "pesquisarAssociacaoPontoConsumo");
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}


	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('nomeImovelTexto').value = "";
       	document.getElementById('numeroImovelTexto').value = "";
       	document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
    }
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}

	function init() {
		formatarCampos();
		<c:choose>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
		var idCliente = '${idCliente}';
		if(idCliente != ''){
			selecionarCliente(idCliente);
		}
		
		var idImovel = '${idImovel}'
		if(idImovel != ''){
			selecionarImovel(idImovel);
		}
	}	
	
	addLoadEvent(init);		
	
	function formatarParaReal(){
		var per = $("#percentualMulta").val();
		$("#percentualMulta").val(per.replace(",00", ""));
		var per = $("#valorPagoPelaCdl").val();
		$("#valorPagoPelaCdl").val(per.replace(",00", ""));
		var per = $("#valorPagoPeloCliente").val();
		$("#valorPagoPeloCliente").val(per.replace(",00", ""));
	}
	
	function pesquisarpontoConsumo(){
		submeter("contratoAdequacaoParceriaForm", "pesquisarAssociacaoPontoConsumo");
	}
	
	function incluir() {
		formatarParaReal();
		submeter('contratoAdequacaoParceriaForm', 'incluirContratoAdequacaoParceria');
	}
	
	function carregarNumeroContrato( valor ) {
		document.getElementById("ultimoPontoConsumoSelecionado").value = valor;
// 		alert(document.getElementById("ultimoPontoConsumoSelecionado").value);
		submeter('contratoAdequacaoParceriaForm', 'carregarNumeroContrato');
	}
	
	

	
	</script>

<form:form method="post" action="pesquisarContratoAdequacaoParceria" name="contratoAdequacaoParceriaForm"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarContratoAdequacaoParceria"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoParceria.chavePrimaria}"/>
	<input name="isInclusao" type="hidden" id="isInclusao" value="true">
	<input name="idCliente" type="hidden" id="idCliente" value="${idCliente}"/>
	<input name="idImovel" type="hidden" id="idImovel" value="${idImovel}"/>
	<input name="habilitado" type="hidden" id="habilitado" value="true"/>
	<input name="ultimoPontoConsumoSelecionado" type="hidden" id="ultimoPontoConsumoSelecionado"/>
	
	<fieldset id="pesquisarContrato" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${Form.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${Form.map.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${Form.map.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${Form.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${Form.map.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${Form.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${Form.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${Form.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${Form.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${Form.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${Form.map.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${Form.map.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${Form.map.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${Form.map.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${Form.map.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${Form.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${Form.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
	
			<br/><br/>
		
	<fieldset class="conteinerBloco" style="margin-top: -20px; "> 
		<input name="Button" style="margin-left: 690px;"class="bottonRightCol2" id="botaoPesquisar" value="Pontos de Consumo" type="button" onClick="pesquisarpontoConsumo();"> 
	</fieldset>
		

	<c:if test="${listaPontoConsumoAssociacao ne null}">
		<hr class="linhaSeparadoraPesquisa" style="width: 885px;" />
		<fieldset class="conteinerBloco" style="width: 885px;">
			<p class="orientacaoInicial">Para associar um Ponto de Consumo, selecione o registro desejado.</p>
			<display:table class="dataTableGGAS"  name="listaPontoConsumoAssociacao"
			sort="list" id="pontoConsumo"
			decorator="br.com.ggas.web.contratoadequacaoparceria.decorator.ContratoAdequacaoParceriaResultadoPesquisaDecorator"
			pagesize="10"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="pesquisarContratoAdequacaoParceria">
		    	<display:column  style="width: 25px">
		    		<input type="radio" name="checkPontoConsumo" id="checkPontoConsumo" value="<c:out value="${pontoConsumo.chavePrimaria}"/>" <c:if test="${pontoConsumo.chavePrimaria eq checkPontoConsumo}">checked="checked"</c:if> onclick="carregarNumeroContrato(${pontoConsumo.chavePrimaria});", '<c:out value="${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}"/>', '<c:out value="${pontoConsumo.instalacaoMedidor.vazaoCorretor.numeroSerie}"/>')" />	
		    	</display:column>
		    	
		    	<display:column sortable="true" title="Im�vel" sortProperty="imovel.nome" property="imovel.nome" headerClass="tituloTabelaEsq" style="text-align: center; padding-left: 10px; width: 328px; " />
		    	
		    	<display:column sortable="true" title="Ponto de Consumo" sortProperty="descricao" property="descricao" headerClass="tituloTabelaEsq" style="width: 230px; text-align: center; padding-left: 10px; width: 328px;" />
		    				
				<display:column sortable="true" title="Status" sortProperty="situacaoConsumo.descricao" property="situacaoConsumo.descricao" style="width: 328px;" />
			</display:table>
		</fieldset>
	</c:if>
	
	<fieldset id="conteinerCampos" class="colunaDir" style="margin-top:10px;">
		<div style="margin-top: 10px;">
			<label class="rotulo campoObrigatorios" style="margin-left:-13px;"><span class="campoObrigatorioSimbolo">* </span>N�mero Contrato Principal:</label>
			<input class="campoTexto"  onkeypress="return formatarCampoInteiro(event);" type="text" id="numeroContratoPrincipal" name="numeroContratoPrincipal" style="margin-left:9px; width:177px;" maxlength="9" size="8" value="${contratoParceria.numeroContratoPrincipal}"/><br />
			
			<label class="rotulo" style="margin-left: 18px; " >Valor Pago Pelo Cliente:  </label>
			<input class="campoTexto campoValorReal" type="text" id="valorPagoPeloCliente"  name="valorCliente" style="margin-left: 10px; width: 152px;"  maxlength="18" size="18" value="${valorPagoCliente}"
				   onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
					style=""/><br />
			<label class="rotulo campoObrigatorios" style="margin-left: 47px;"><span class="campoObrigatorioSimbolo">* </span>Valor Pago CDL:</label>
			<input class="campoTexto campoValorReal" type="text" id="valorPagoPelaCdl" name="valorPagoPelaCdl" style="margin-left: 14px; width: 152px;"  maxlength="18" size="18" value="${contratoParceria.valorPagoPelaCdl}"
				   onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
					style="margin: 0px"/><br />
					
			<label class="rotulo campoObrigatorios" style="margin-left: 58px;" forCampo="numeroAnteriorContrato"><span class="campoObrigatorioSimbolo">* </span>In�cio Vig�ncia:</label>
			<input tabindex="5" style="margin-left: 10px;" class="campoData campoHorizontal" type="text" id="dataInicioVigencia" name="dataInicioVigencia" value="${dataInicioVigencia} " />
		</div>
	</fieldset>
		
	<fieldset id="conteinerDadosAdequacao" class="colunaDir" style="margin-top: 10px;"  >
			<label class="rotulo campoObrigatorios" ><span class="campoObrigatorioSimbolo">* </span>Percentual de Multa: </label>
			<input class="campoTexto2"  type="text" id="percentualMulta" name="percentualMulta"  style=" margin-left:10px; width:197px; margin-left: 0px;" 
			onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5" value="${percentualMulta}"><label class="rotuloInformativo2Linhas rotuloHorizontal">%</label><br />
			
			<label class="rotulo campoObrigatorios" ><span class="campoObrigatorioSimbolo">* </span>Prazo(Meses): </label>
			<input class="campoTexto2" onkeypress="return formatarCampoInteiro(event);" type="text" id="prazoVencimento" name="prazo"  style="margin-left: 8px; width: 197px; margin-left: 0px;" maxlength="18" size="18" value="${prazoEstabelecido}"><br />
			
			<label class="rotulo" for="indiceFinanceiro"><span class="campoObrigatorioSimbolo">* </span>Corre��o Monet�ria:</label>
			<select id="indiceFinanceiro" class="campoSelectTamanhoFixo" name="indiceFinanceiro">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaIndiceFinanceiro}" var="indiceCorrecaoMonetaria">
					<option value="<c:out value="${indiceCorrecaoMonetaria.chavePrimaria}"/>" >
						<c:out value="${indiceCorrecaoMonetaria.descricaoAbreviada}"/>
					</option>
				</c:forEach>
			</select><br/>
	</fieldset>
	
	<fieldset class="conteinerBotoes" style="width: 880px;"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
		<input class="bottonRightCol2" value="Limpar" type="button" onclick="limparCampos();">
		<input name="button" class="bottonRightCol10" id="botaoIncluir" onClick="incluir();" value="Salvar" />
	</fieldset>
</form:form> 
