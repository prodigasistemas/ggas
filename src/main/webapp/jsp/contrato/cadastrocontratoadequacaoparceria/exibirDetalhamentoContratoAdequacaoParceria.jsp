<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<script>

	$(document).ready(function(){
		
		var valorPagoPelaCdl = document.getElementById('valorPagoPelaCdl');
		aplicarMascaraNumeroDecimal(valorPagoPelaCdl, 2);
		var valorPagoPeloCliente = document.getElementById('valorPagoPeloCliente');
		aplicarMascaraNumeroDecimal(valorPagoPeloCliente, 2);
		var isDisabledBotaoAditar = $("input[name='buttonAditar']").prop("disabled");
		var isDisabledBotaoAlterar = $("input[name='buttonAlterar']").prop("enabled");
						
// 		//Desabilita todos os campos input (todos), select e textarea.
// 		$(":input").attr("disabled","disabled");	
// 		$(":input#searchInput").removeAttr("disabled");
		
		//Remove o atributo disabled="disabled" para elementos HTML espec�ficos desta tela.
		$("input[type='hidden'],.conteinerBotoes .bottonRightCol,#chavesPrimariasPontoConsumoVO,#idModeloContrato,#QDCContrato,.ui-dialog button").removeAttr("disabled");
		$("input[name='buttonCancelar']").removeAttr("disabled");
		$("input[name='buttonAvancar']").removeAttr("enabled");
		$("input[name='buttonAlterar']").removeAttr("enabled");
		
		//Esconde elementos espec�ficos da tela.
		$("img.ui-datepicker-trigger,a.apagarItemPesquisa,span.campoObrigatorioSimbolo,input[type='radio'],label.rotuloRadio,#rotuloQDCData,#QDCData,#rotuloQDC,#QDCValor,label.rotuloInformativo,#botaoOKQDC,#botaoReplicarQDC,#botaoExcluirQDC").css("display","none");
		
		//Esconde os bot�es radio e exibe o label correspodente.
		$("input[type='radio']:checked + label.rotuloRadio").each(function(){
			$(this).css({"display":"block","font-size":"14px","padding-top":"5px"});
		});
		
		//Esconde os r�tulos de unidades de medida e tempo e <selects> cujos campos <input> estejam vazios.
		$(":input[value=''] + label.rotuloInformativo,:input[value=''] + select.campoHorizontal").each(function(){
			$(this).css("display","none");
		});

		$(".itemDetalhamento").css("margin-right","5px");
		$(".itemDetalhamento3Linhas").css("margin-right","0");
		
		//Adiciona a classe "campoDesabilitadoDetalhar" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
		$("input[type='text'],select").addClass("campoDesabilitadoDetalhar");
		$("input.searchInput,select").removeClass("campoDesabilitadoDetalhar").addClass("searchInput");
		$("input.campo2Linhas[type='text'],select.campo2Linhas").removeClass("campoDesabilitadoDetalhar").addClass("campoDesabilitado2Linhas");
		$(".campoValorReal.campoDesabilitadoDetalhar,.campoValorReal.campoDesabilitado2Linhas").addClass("campoValorRealDesabilitadoDetalhar");
		
		//Remove a classe campoObrigatorio de todos os r�tulos para evitar desalinhementos.
		$("*").removeClass("campoObrigatorio");
		
		//Retira o texto "Selecione" da primeira op��o do <select>.
		$("option[value='-1']").text('');
				
		/* Substitui os <select> que possuem valor por <span> */
		$("select:has(option[value!='-1']:selected)").each(function(){
			$("select").css("display","none");
			var selectId = $(this).attr("id");
			var textOption = $("#" + selectId + " option:selected").text();
            $("#" + selectId).after("<span class='itemDetalhamento'>" + textOption + "</span>");
		});
		
		/* Substitui os <inputs> que possuem valor por <span class="itemDetalhamento"...> */
		$("input.campoDesabilitadoDetalhar[value!=''],input.campoDesabilitado2Linhas[value!='']").each(function(){
			$(this).css("display","none")
			var valueInput = $(this).val();
			var inputId = $(this).attr("id");
			$("#" + inputId + " + label.rotuloInformativo").css({"display":"block","margin-left":"0"});
			$("#" + inputId).after("<span class='itemDetalhamento'>" + valueInput + "</span>");
			$("input.campoValorRealDesabilitadoDetalhar + span").addClass("itemDetalhamento");			
		});
			
		// Corrige a classe dos <span class="itemDetalhamento"...> cujos r�tulos t�m de 2 linhas
		$(".campoDesabilitado2Linhas  + span").removeClass("itemDetalhamento").addClass("itemDetalhamento2Linhas");
		
		// habilitar e desabilitar os bot�es Aditar e Alterar utilizados no controle de acesso.
		if(isDisabledBotaoAditar){
			$("input[name='buttonAditar']").attr("disabled","disabled");
		}else{
			$("input[name='buttonAditar']").removeAttr("disabled");
		}
	});

	function alterarContrato(){
		submeter("contratoAdequacaoParceriaForm", "exibirAtualizacaoContratoAdequacaoParceria");
	}
	
	function cancelar() {		
		location.href = '<c:url value="/exibirPesquisaContratoAdequacao"/>';
	}
		
	
	
</script>

<h1 class="tituloInterno">Detalhamento Contrato Adequa��o Parceria<a class="linkHelp" href="<help:help>/detalhamentodoscontratos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" id="formDetalharContrato" name="contratoAdequacaoParceriaForm">
	<input name="acao" type="hidden" id="acao" value="aditarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoParceria.chavePrimaria}"/>
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">
	<input name="idModeloContrato" type="hidden" id="idModeloContrato" value="${idModeloContrato}">
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${fluxoAlteracao}"/>
	<input name="idCliente" type="hidden" id="idCliente" value="${idCliente}"/>
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${idPontoConsumo}"/>
	<input name="idImovel" type="hidden" id="idImovel" value="${idImovel}"/>
	
		
	<fieldset class="detalhamento">
		
		<fieldset id="conteinerDetalharCliente" class="conteinerBloco"  >
			<c:if test="${listaContratoAdequacao ne null}">
				<p class="orientacaoInicial"><span class="destaqueOrientacaoInicial">Para modificar as informa��es deste registro clique em Alterar.</span></p>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaContratoAdequacao" id="contratoAdequacao"   pagesize="15"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" >
		    	
		    		<display:column sortable="false" title="Im�vel" sortProperty="pontoConsumo.imovel.nome" property="pontoConsumo.imovel.nome" />
			    	
			    	<display:column sortable="false" title="Ponto de Consumo" sortProperty="pontoConsumo.descricao" property="pontoConsumo.descricao"  />
						    	
				</display:table>
			</c:if>
		</fieldset>	
		<fieldset class="coluna detalhamentoColunaLarga2"  >
			<label class="rotulo" forCampo="numeroAnteriorContrato">N�mero Contrato Principal:</label>
			<input class="campoTexto" type="text" id="numeroContratoPrincipal" name="numeroContratoPrincipal" maxlength="10" size="10" <ggas:campoSelecionado campo="numeroAnteriorContrato"/> value="${contratoParceria.contrato.numeroCompletoContrato}"><br class="quebraLinha" />
					
			<label  class="rotulo" forCampo="valorPagoPelaCdl" style="margin-left:60px;">Valor Pago CDL:</label>
			<input class="campoTexto" type="text" size="10" maxlength="10" name="valorPagoPelaCdl" id="valorPagoPelaCdl" <ggas:campoSelecionado campo="numeroAditivo"/> value="${contratoParceria.valorEmpresa}"><br class="quebraLinha" />

			<label class="rotulo" forCampo="valorPagoPelaCdl" style="margin-left:45px;">Valor Pago Cliente:</label>

			<input class="campoTexto" type="text" id="valorPagoPeloCliente" name="valorPagoPeloCliente" maxlength="10" <ggas:campoSelecionado campo="dataAditivo"/> value="${contratoParceria.valorCliente}"><br />
		
			<label class="rotulo" forCampo="valorPagoPelaCdl" style="margin-left:68px;">In�cio Vig�ncia:</label>
			
			<span class="itemDetalhamento" ><fmt:formatDate value="${contratoParceria.dataInicioVigencia}" pattern="dd/MM/yyyy"/>
			</span>		</fieldset>
			
		<fieldset id="blocoDetalhamento2" class="colunaDir">
			<label class="rotulo" >Percentual de Multa: </label>
			<input class="campoTexto" type="text" id="percentualMulta" name="percentualMulta" value="${contratoParceria.percentualMulta}"><label>%</label></br>
			
			<label style="margin-left: 30px;"  class="rotulo" > Prazo (Meses):</label>
			</br>
			<span class="itemDetalhamento" ><c:out value="${contratoParceria.prazo}"/></span>
			
			<label class="rotulo" for="indiceFinanceiro">Corre��o Monet�ria:</label>
			<span class="itemDetalhamento" ><c:out value="${contratoParceria.indiceFinanceiro.descricao}"/></span>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonRemover" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterarContrato();"/>	
		<input name="buttonCancelar" class="bottonRightCol2 botaoAlterar" value="Cancelar" type="button" onclick="cancelar();"/>
	</fieldset>
				
</form:form> 
			
		