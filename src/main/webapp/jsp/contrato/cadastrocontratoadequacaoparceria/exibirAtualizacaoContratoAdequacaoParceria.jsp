<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Contrato Adequa��o Parceria<a class="linkHelp" href="<help:help>/inclusoalteraodosrecebimentos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<script type="text/javascript">
	
	$(document).ready(function() {

	   	$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosAteDataAtual}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

	   	$(".campoValorReal[disabled='disabled']").addClass("campoValorRealDesabilitado");

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto,#rotuloPontoConsumoLegado").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#faturaVO").chromatable({
			width: "891px"
		});
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
// 	function habilitaCliente(){
// 		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
// 		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
// 	};
// 	function habilitaImovel(){
// 		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
// 		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
// 	};
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCampos(){

		document.getElementById('numeroContratoPrincipal').value = "";
		document.getElementById('valorPagoPelaCdl').value = "";
		document.getElementById('indiceFinanceiro').value = "-1";
		document.getElementById('percentualMulta').value = "";
		document.getElementById('valorPagoPeloCliente').value = "";
		document.getElementById('prazoVencimento').value = "";
		document.getElementById('dataInicioVigencia').value = "";
		
	}
	
	function formatarParaReal(){
		var per = $("#percentualMulta").val();
		$("#percentualMulta").val(per.replace(",00", ""));
		var per = $("#valorPagoPelaCdl").val();
		$("#valorPagoPelaCdl").val(per.replace(",00", ""));
		var per = $("#valorPagoPeloCliente").val();
		$("#valorPagoPeloCliente").val(per.replace(",00", ""));
	}

	function formatarCampos(){
		var valorCliente = document.getElementById('valorPagoPeloCliente');
		aplicarMascaraNumeroDecimal(valorCliente, 2);
		var valorPagoPelaCdl = document.getElementById('valorPagoPelaCdl');
		aplicarMascaraNumeroDecimal(valorPagoPelaCdl, 2);
		var percentualMulta = document.getElementById('percentualMulta');
		aplicarMascaraNumeroDecimal(percentualMulta, 2);
	}

	
	function salvar(){
		formatarParaReal();
		$("#numeroContratoPrincipal").prop('disabled', false);
		submeter('contratoAdequacaoParceriaForm', 'atualizarContratoAdequacaoParceria');
	}

	
	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaContratoAdequacao"/>';
	}


	function manterDadosCheckBox(valor){

		<c:forEach items="${Form.map.chavesPrimariasFaturas}" var="fatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${fatura}"/>').checked = valor;
			}	
		</c:forEach>

	}

	function init() {
		formatarCampos();
		
		var elem = document.getElementById('idArrecadador');
		
		if(elem != null){
		  carregarTiposConvenio(elem);
		}
		
		manterDadosCheckBox(true);
		
		<c:choose>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				document.getElementById('botaoPesquisar').disabled = false;
				habilitaCliente();
			</c:when>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				document.getElementById('botaoPesquisar').disabled = false;
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
				document.getElementById('botaoPesquisar').disabled = true;
			</c:otherwise>
		</c:choose>	

		var idCliente = '${idCliente}';
		if(idCliente != ''){
			selecionarCliente(idCliente);
		}
		
		var idImovel = '${idImovel}'
		if(idImovel != ''){
			selecionarImovel(idImovel);
		}
		
	}
	addLoadEvent(init);
	addLoadEvent(gerarRelatorioReciboQuitacao);
	
</script>	
<input name="isInclusao" type="hidden" id="isInclusao" value="false">
<input name="habilitado" type="hidden" id="habilitado" value="${contratoParceria.habilitado}"/>
<form:form method="post" action="atualizarContratoAdequacaoParceria" name="contratoAdequacaoParceriaForm">
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${contratoParceria.chavePrimaria}">
		
	<fieldset id="incluirContratoAdequacaoParceria" class="conteinerPesquisarIncluirAlterar">
			<fieldset class="conteiner2" style="border-bottom: 1px solid #b9d0e2; width: 1000px; margin-left:5px;" >
				<c:if test="${listaContratoAdequacao ne null}">
<!-- 				<hr class="linhaSeparadoraPesquisa" /> -->
					<fieldset class="conteinerBloco">
					<p class="orientacaoInicial"><span class="destaqueOrientacaoInicial">Para alterar um Contrato, informe os dados abaixo e clique em Salvar.</span></p>
					<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaContratoAdequacao" id="contratoAdequacao"   pagesize="15"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAssociacaoPontoConsumo">
			    	
			    		<display:column sortable="true" title="Im�vel" sortProperty="pontoConsumo.imovel.nome" property="pontoConsumo.imovel.nome" style="text-center: left; padding-left: 10px; width: 288px;" />
				    	
				    	<display:column sortable="true" title="Ponto de Consumo" sortProperty="pontoConsumo.descricao" property="pontoConsumo.descricao" style="width: 288px;  text-center: left; padding-left: 10px" />
				    				
<%-- 						<display:column sortable="true" title="Cliente" sortProperty="cliente.nome" property="cliente.nome" style="width: 288px;" /> --%>
							    	
					</display:table>
				</fieldset>
			</c:if>
		</fieldset >
		<fieldset class="conteiner2"  style="width: 1055px"    >
		<div style="width: 400px; float: left;">
			<label class="rotulo"><span class="campoObrigatorioSimbolo">* </span>N�mero Contrato Principal:</label>
			<input class="campoTexto2" type="text"  disabled="disabled" onkeypress="return formatarCampoInteiro(event);" style="margin-left: 9px; width:175px;" id="numeroContratoPrincipal" name="numeroContratoPrincipal"  maxlength="18" size="18" value="${contratoParceria.numeroContratoPrincipal}"><br />
			
			<label class="rotulo" style="margin-left: 32px;">Valor pago pelo Cliente:</label>
				<input class="campoTexto campoValorReal" type="text" id="valorPagoPeloCliente"  name="valorCliente" style="margin-left: 10px; "  maxlength="18" size="18" value="${valorPagoCliente}"
				   onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);" onblur="aplicarMascaraNumeroDecimal(this, 2);"
					style="margin: 0px"/><br />
			
			<label class="rotulo" style="margin-left: 60px;"><span class="campoObrigatorioSimbolo">* </span>Valor Pago CDL:</label>
			<input class="campoTexto campoValorReal" type="text" id="valorPagoPelaCdl"  name="valorPagoPelaCdl" style="margin-left: 11px;"  maxlength="18" size="18" 
				  value="${valorPagoPelaCdl}" onkeypress="return formatarCampoDecimal(event,this,11,2);" onblur="aplicarMascaraNumeroDecimal(this,2);" /><br />
		
			<label class="rotulo campoObrigatorios" style="margin-left: 68px;" forCampo="numeroAnteriorContrato"><span class="campoObrigatorioSimbolo">* </span>In�cio Vig�ncia:</label>
			<input tabindex="5" style="margin-left: 10px;" class="campoData campoHorizontal" type="text" id="dataInicioVigencia" name="dataInicioVigencia" value="${dataInicioVigencia} " />
		</div>
	
		<fieldset id="conteinerDadosAdequacao" class="colunaDir" style="margin-top: 10px;"  >
			<label class="rotulo campoObrigatorios" ><span class="campoObrigatorioSimbolo">* </span>Percentual de Multa: </label>
			<input class="campoTexto2"  type="text" id="percentualMulta" name="percentualMulta"  style=" margin-left:10px; width:197px; margin-left: 0px;" 
			onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" size="5" value="${percentualMulta}"><label class="rotuloInformativo2Linhas rotuloHorizontal">%</label><br />
			
			<label class="rotulo campoObrigatorios" ><span class="campoObrigatorioSimbolo">* </span>Prazo(Meses): </label>
			<input class="campoTexto2" onkeypress="return formatarCampoInteiro(event);" type="text" id="prazoVencimento" name="prazo"  style="margin-left: 8px; width: 197px; margin-left: 0px;" maxlength="18" size="18" value="${prazoEstabelecido}"><br />
			
			<label class="rotulo" for="indiceFinanceiro"><span class="campoObrigatorioSimbolo">* </span>Corre��o Monet�ria:</label>
			<select id="indiceFinanceiro" class="campoSelectTamanhoFixo" name="indiceFinanceiro">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaIndiceFinanceiro}" var="indiceCorrecaoMonetaria">
				<option value="<c:out value="${indiceCorrecaoMonetaria.chavePrimaria}"/>" <c:if test="${contratoParceria.indiceFinanceiro.chavePrimaria == indiceCorrecaoMonetaria.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${indiceCorrecaoMonetaria.descricaoAbreviada}"/>
				</option>
				</c:forEach>
			</select><br/>
	</fieldset>
	</fieldset>
	</fieldset>
	
	
	
		<fieldset class="conteinerBotoes">
			<input name="button" class="bottonRightCol2" type="button" id="button" value="Cancelar" onClick="cancelar();">
	 	    <input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCampos();">
	  	 	<vacess:vacess param="atualizarSegmento">
		   		<input type="submit" value="Salvar" class="bottonRightCol2 botaoGrande1 botaoAlterar" name="Button" onClick="salvar();">
	 	   	</vacess:vacess>
	 	  
		</fieldset>
</form:form> 