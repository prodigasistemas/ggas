<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Migrar Saldo QNR e/ou QPNR<a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>


<script language="javascript">

	$(document).ready(function(){
		
		$('#checkAllAuto').click(function(){
			
			var checked = $('#checkAllAuto').is(':checked');
			var checkPontos = document.getElementsByName('idsPontosConsumo');
			
			
			var arrayValorQNR = document.getElementsByName('valorQNR');
			var arrayvalorQPNR = document.getElementsByName('valorQPNR');
			
			for (var i = 0; i < checkPontos.length; i++) {
				
				checkPontos[i].checked = checked;
				
				
				
				atualizarValorTotal(checkPontos[i], arrayValorQNR[i].value, arrayvalorQPNR[i].value);
				
				
			}
		});
		limparCamposPesquisa();
		

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		
		
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};			
		
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	
	function limparFormulario(){
		document.contratoForm.numeroContrato.value = "";
		document.contratoForm.habilitado[0].checked = true;	
		
    	limparCamposPesquisa();
    	
    	document.contratoForm.indicadorPesquisa[0].checked = false;
    	document.contratoForm.indicadorPesquisa[1].checked = false;
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	
	function transferir() {		
		submeter('contratoForm', 'transferirSaldo');
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function removerContrato(){
		var selecao = verificarSelecao();
		if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('contratoForm', 'excluirContrato');
				}
	    }
	} 	
	
	function exibirMigrarSaldo(){
		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('contratoForm', 'exibirMigrarSaldo');
		}
	}
	
	function pesquisarContratoMigrar(){
			submeter('contratoForm', 'pesquisarContratoMigrar');
	}
	
	

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}
	
	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		
		document.getElementById('numeroContrato').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		document.getElementById('situacaoContrato').value = "-1";
		
	}
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}

	function init() {
		<c:choose>
			<c:when test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
	}	
	
	function identificarImoveis(component){
		
		document.forms[0].chavePrimariaContratoMigrar = component; 
		submeter('contratoForm', 'pesquisarImovelContratoMigrar');
 		
	}
	
	function cancelar() {		
		submeter('contratoForm','pesquisarContrato');
	}
	
	function atualizarValorTotal(obj, paramValorQNR, paramValorQPNR) {
		
		var valorFloatQNR = converterStringParaFloat(paramValorQNR);
		var valorFloatQPNR = converterStringParaFloat(paramValorQPNR);
		
		var inputValorCreditosQNR = document.getElementById("saldoQNR");
		var inputValorCreditosQPNR = document.getElementById("saldoQPNR");
		
		if (inputValorCreditosQNR.value == "") {
			inputValorCreditosQNR.value = 0;
		}
		
		if (inputValorCreditosQPNR.value == "") {
			inputValorCreditosQPNR.value = 0;
		}
		
		var valorCreditosQNR = parseFloat(inputValorCreditosQNR.value);
		var valorCreditosQPNR = parseFloat(inputValorCreditosQPNR.value);
		var valorTotalQNR = 0;
		var valorTotalQPNR = 0;
		if (obj.checked) {
			valorCreditosQNR = valorCreditosQNR + valorFloatQNR;
			valorCreditosQPNR = valorCreditosQPNR + valorFloatQPNR;
			valorTotalQNR = valorCreditosQNR;
			valorTotalQPNR = valorCreditosQPNR;
			atualizarValores(valorTotalQNR,valorTotalQPNR);
			
			
		}else{
			valorCreditosQNR = valorCreditosQNR - valorFloatQNR;
			valorCreditosQPNR = valorCreditosQPNR - valorFloatQPNR;
			valorTotalQNR = valorCreditosQNR;
			valorTotalQPNR = valorCreditosQPNR;
			atualizarValores(valorTotalQNR,valorTotalQPNR);
		}
		
	}
	
	function atualizarValores(valorTotalQNR,valorTotalQPNR){
		
		
		document.getElementById('saldoQNR').value = parseFloat(valorTotalQNR).toFixed(4);
		document.getElementById('saldoQPNR').value = parseFloat(valorTotalQPNR).toFixed(4);
		document.getElementById('valorTransferidoQNR').value = parseFloat(valorTotalQNR).toFixed(4);
		document.getElementById('valorTransferidoQPNR').value = parseFloat(valorTotalQPNR).toFixed(4);
		
	}
	
	addLoadEvent(init);		
					
	</script>

<form:form method="post" action="/pesquisarContratoMigrar" id="contratoForm" name="contratoForm"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarContrato"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="${true}">
	<input name="fluxoAlteracao" type="hidden" id="fluxoAlteracao" value="${contratoVO.fluxoAlteracao}"/>

	<fieldset id="pesquisaImovelContrato" class="conteinerBloco">
				<label class="rotulo " id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:</label>
				<input class="campoTexto campoDesabilitado" type="text" id="numeroContratoMigrar" name="numeroContratoMigrar" readonly="readonly"  value="${contratoVO.numeroContratoMigrar}" ><br />
				
				<label class="rotulo  " id="rotuloNumeroContrato" for="saldoQNR">Saldo QNR:</label>
				<input type="hidden" id="valorCreditosQNR" >
				<input class="campoTexto campoDesabilitado" type="text" id="saldoQNR" name="saldoQNR" readonly="readonly" value="${contratoVO.saldoQNR}" ><br />
<%-- 				<input type="hidden" id="valorTransferidoQNR" name="valorTransferidoQNR" value="${contratoVO.saldoQNR}" ><br /> --%>
				
				<label class="rotulo " id="rotuloNumeroContrato" for="saldoQPNR">Saldo QPNR:</label>
				<input class="campoTexto campoDesabilitado" type="text" id="saldoQPNR" readonly="readonly" name="saldoQPNR"  value="${contratoVO.saldoQPNR}" ><br />
<%-- 				<input type="hidden" id="valorTransferidoQNR" name="valorTransferidoQPNR" value="${contratoVO.saldoQPNR}" ><br /> --%>

</fieldset>
<fieldset id="pesquisaImovelContrato" class="conteinerBloco">
		<display:table class="dataTableGGAS dataTablePesquisa"
			name="${listaPontoConsumoVO}" sort="list" id="pontoConsumoVO"
			excludedParams="" requestURI="#">
			
			
			<c:set var="selecionado" value="false" />
				<display:column headerClass="headerCheckbox"
					style="text-align: center; width: 25px" sortable="false"
					title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto' />">
					<c:forEach items="${contratoVO.idsPontosConsumo}"
						var="idChavePonto" >
						<c:if test="${idChavePonto eq pontoConsumoVO.pontoConsumo.chavePrimaria }">
							<c:set var="selecionado" value="true" />
						</c:if>
					</c:forEach>
					<input type="hidden" value="${pontoConsumoVO.qnr}" name="valorQNR" id="valorQNR">
					<input type="hidden" value="${pontoConsumoVO.qpnr}" name="valorQPNR" id="valorQPNR">
					<input type="checkbox" name="idsPontosConsumo" id="idsPontosConsumo" 
						value="${pontoConsumoVO.pontoConsumo.chavePrimaria}"
						onclick="atualizarValorTotal(this, '<c:out value="${pontoConsumoVO.qnr}"/>', '<c:out value="${pontoConsumoVO.qpnr}"/>');"  
						<c:if test="${selecionado eq true}">
				 			checked
						</c:if>/>
					
				</display:column>
				
			
			<display:column style="width: 30px" title="Ativo">
				<c:choose>
					<c:when test="${pontoConsumoVO.pontoConsumo.habilitado == true}">
						<img alt="Ativo" title="Ativo"
							src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo"
							src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column sortable="true" title="Descri��o"
				style="width: 110px">
				<c:out value='${pontoConsumoVO.pontoConsumo.descricao}' />
			</display:column>
			<display:column sortable="true" title="QNR" style="width: 110px">
				<c:out value='${pontoConsumoVO.qnr}' />
			</display:column>
			<display:column sortable="true" title="QPNR" style="width: 110px">
				<c:out value='${pontoConsumoVO.qpnr}' />
			</display:column>
		</display:table>

	</fieldset>
	<fieldset id="pesquisarContrato" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${contratoVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${contratoVO.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${contratoVO.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${contratoVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${contratoVO.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${contratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${contratoVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${contratoVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${contratoVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${contratoVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${contratoVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${contratoVO.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${contratoVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${contratoVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${contratoVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${contratoVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${contratoVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${contratoVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<fieldset class="coluna" style="margin-top:30px ;margin-right:40px; padding-right: 70px">
				<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:<!-- fix bug --></label>
				<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="9" size="8" value="${contratoVO.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"><br />
			</fieldset>
			
		<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarContrato">
    			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" onclick="pesquisarContratoMigrar();">
    		</vacess:vacess>							
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
			</fieldset>
		</fieldset>
	</fieldset>
			
	<c:if test="${listaContratos ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" decorator="br.com.ggas.web.contrato.contrato.decorator.ContratoResultadoPesquisaDecorator" name="listaContratos" id="contrato" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarContrato">

			<display:column style="width: 25px" media="html" sortable="false" >
			<input type="radio" name="chavePrimariaContratoMigrar" id="chavePrimariaContratoMigrar" onclick="identificarImoveis(${contrato.chavePrimaria});"
						value="${contrato.chavePrimaria}">
			</display:column>
	     	 <display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${contrato.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
		     <display:column sortable="true" sortProperty="clienteAssinatura.nome" title="Nome do Cliente" headerClass="tituloTabelaEsq" style="text-align: left">
		     	<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.clienteAssinatura.nome}'/>
				</a>
		     </display:column>
		     <display:column sortable="true" title="CPF/CNPJ Cliente" style="width: 135px"  sortProperty="clienteAssinatura.numeroDocumentoFormatado">
		     	<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.clienteAssinatura.numeroDocumentoFormatado}'/>
				</a>
		     </display:column>		     
		     <display:column sortable="true" title="N�mero do<br />Contrato" sortProperty="numeroFormatado" style="width: 120px">
				<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.numeroFormatado}'/>
	        	</a>
	    	 </display:column>
		 	<display:column sortable="true" title="Situa��o" style="width: 110px" sortProperty="situacao.descricao">
		 		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.situacao.descricao}'/>
				</a>
	    	</display:column>
	    	<display:column title="Data da<br />Assinatura" style="width: 80px" sortable="true" sortProperty="dataAssinatura">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataAssinatura}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>			     
	    	<display:column title="Data de<br />Vencimento" style="width: 80px" sortable="true" sortProperty="dataAssinatura">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataVencimentoObrigacoes}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>
		</display:table>
	</c:if>
<br/>



<c:if test="${listaPontosConsumoTransferir ne null}">
	<display:table class="dataTableGGAS dataTablePesquisa"
		name="${listaPontosConsumoTransferir}" sort="list"
		id="pontoConsumo" excludedParams="" requestURI="#">
		
		
		<c:set var="selecionado" value="false" />
				<display:column 
					style="text-align: center; width: 25px" sortable="false"
					>
					<c:forEach items="${contratoVO.chavePontoTransferirSaldo}"
						var="idChavePonto" >
						<c:if test="${idChavePonto eq pontoConsumo.chavePrimaria }">
							<c:set var="selecionado" value="true" />
						</c:if>
					</c:forEach>
					
					<input type="radio" name="chavePontoTransferirSaldo" id="chavePontoTransferirSaldo" 
						value="${pontoConsumo.chavePrimaria}"
						<c:if test="${selecionado eq true}">
				 			checked
						</c:if>/>
					
				</display:column>
		
		<display:column sortable="true" title="Im�vel"
				style="width: 110px">
				<c:out value='${pontoConsumo.imovel.nome}' />
			</display:column>
		<display:column sortable="true" title="Ponto de Consumo"
				style="width: 110px">
				<c:out value='${pontoConsumo.descricao}' />
			</display:column>
	</display:table>
	</c:if>
	<fieldset id="pesquisaImovelContrato" class="conteinerBloco">
<%-- 	<c:if test="${not empty listaPontosConsumoTransferir}"> --%>
	<br/>
	<label class="rotulo " id="rotuloNumeroContrato" for="valorTransferidoQNR">Valor Transferido QNR:</label>
				<input class="campoTexto campoHorizontal" type="text" id="valorTransferidoQNR" name="valorTransferidoQNR"  value="${contratoVO.valorTransferidoQNR}" ><br />
				
				<label class="rotulo  " id="rotuloNumeroContrato" for="valorTransferidoQPNR">Valor Transferido QPNR:</label>
				<input class="campoTexto campoHorizontal" type="text" id="valorTransferidoQPNR" name="valorTransferidoQPNR"  value="${contratoVO.valorTransferidoQPNR}" ><br />
<%-- 		</c:if> --%>
	</fieldset>
	<fieldset class="conteinerBotoes">
	<input name="botaoCancelar" id="botaoCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
	<c:if test="${not empty listaPontosConsumoTransferir}">
		<input name="buttonRemover" id="buttonRemover" value="Transferir" class="bottonRightCol2 botaoGrande1" onclick="transferir();" type="button">
		</c:if>
	</fieldset>
</form:form> 
