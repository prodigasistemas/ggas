<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<script lang="javascript">

	$(document).ready(function(){	

		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#pontoConsumo").chromatable({
			width: "738px"
		});

		//Selecionar/Desselecionar todos os checkboxes
		if ($("._thead input[name='checkAllAuto'],#checkAllAuto").length > 0){
			$("._thead input[name='checkAllAuto'],#checkAllAuto").click(function(){				
				//O seletor de filtro :enabled marca apenas os checkboxes habilitados.
				$("tbody input[type='checkbox']:enabled").attr("checked", $("._thead input[name='checkAllAuto'],#checkAllAuto").is(":checked"));
			});
		}

		$("._thead th:first").css({"width":"25px"});
	});

 	function copiarPontoConsumo() {
 		submeter('exibirCopiaDadosPontoConsumoPopupForm', 'copiarDadosPontoConsumo');
 		fecharTela();
 	}
     
 	function fecharTela() {
		if(window.opener != null){
			window.opener.refreshPagina();
		}
		window.close();
 	}

	function selecionarPontosConsumo(check){
		var marcarCampo = check.checked;		
		var chavesPontoConsumo = document.forms[0].arrayIdPontoConsumo;

		for(i = 0; i < chavesPontoConsumo.length; i++){
			chavesPontoConsumo[i].checked = marcarCampo;
		}		
	}
 	
 </script>

<h1 class="tituloInternoPopup">Copiar Ponto de Consumo<a class="linkHelp" href="<help:help>/copiarpontodeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form method="post" name="exibirCopiaDadosPontoConsumoPopupForm" id="exibirCopiaDadosPontoConsumoPopupForm"> 

	<input name="arrayIdPontoConsumo" type="hidden" id="arrayIdPontoConsumo">
	<input name="idPontoConsumoReferenciaCopia" type="hidden" id="idPontoConsumoReferenciaCopia">
	
	<fieldset class="conteinerBloco">
		<legend>Dados do Ponto de Consumo</legend>
		<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo">Descri��o:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumoReferenciaCopia.descricao}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumoReferenciaCopia.enderecoFormatado}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumoReferenciaCopia.cep.cep}"/></span><br />
				<label class="rotulo">Complemento:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumoReferenciaCopia.descricaoComplemento}"/></span><br />
			</fieldset>
		</fieldset>
	</fieldset>
	
    <c:if test="${listPontoConsumoParaCopia ne null}">
        <hr class="linhaSeparadora2" />
        <p class="orientacaoInicialPopup">Para Copiar os dados do Ponto de Consumo acima, selecione os Pontos de Consumo na lista abaixo e clique em <span class="destaqueOrientacaoInicial">Colar</span>.</p>
       
        <fieldset class="conteinerBloco">
        	<legend>Pontos de Consumo</legend>
			<display:table class="dataTableGGAS dataTablePopup" name="listPontoConsumoParaCopia" sort="list" id="pontoConsumo"
			    decorator="br.com.ggas.util.DisplayTagGenericoDecorator" 
			    excludedParams="org.apache.struts.taglib.html.TOKEN acao" 
			    requestURI="exibirCopiaDadosPontoConsumoPopup">
			    
				<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' />">
			        <input type="checkbox" name="arrayIdPontoConsumo" value="${pontoConsumo.chavePrimaria}">
			    </display:column>
			
				<display:column sortable="true" title="Descri��o">
				    <c:out value="${pontoConsumo.descricao}"/>
			    </display:column>
			    <display:column sortable="true" title="Segmento" style="width: 140px">
				    <c:out value="${pontoConsumo.segmento.descricao}"/>
			    </display:column>
			    <display:column sortable="true" title="Situa��o" style="width: 140px">
				    <c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
			    </display:column>  
			</display:table>
		</fieldset>
    </c:if>
	<fieldset class="conteinerBotoesPopup">
	    <c:if test="${listPontoConsumoParaCopia ne null}"> 
            <input name="Button" class="bottonRightCol2" id="botaoCopiar" value="Colar" type="button" onclick="copiarPontoConsumo();">
        </c:if>
        <input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Cancelar" type="button" onclick="fecharTela();">
    </fieldset>	    	
</form>