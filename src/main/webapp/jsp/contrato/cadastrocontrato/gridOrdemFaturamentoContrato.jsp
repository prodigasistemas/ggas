<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


		<c:set var="indexContrato" value="0" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" decorator="br.com.ggas.web.contrato.contrato.decorator.ContratoResultadoPesquisaDecorator"
		name="sessionScope.listaContratosPrincipalEComplementar" id="contrato" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" 
		requestURI="pesquisarContrato">
	     	 
   	         <display:column style="text-align: center;width: 25px"> 
   	         	<c:if test="${ indexContrato > 0 }">
              			<a href="javascript:alterarOrdem(<c:out value="${indexContrato}"/>, 'subirPosicao', ${contrato.chavePrimaria});"><img title="Alterar Posi��o Servi�o" alt="Alterar Posi��o Servi�o" src="/GGAS/imagens/setaCima.png"></a> 
               	</c:if>
               	<c:if test="${ fn:length(listaContratosPrincipalEComplementar) - 1 !=  indexContrato }">
               		<a href="javascript:alterarOrdem(<c:out value="${indexContrato}"/>, 'descerPosicao', ${contrato.chavePrimaria});"><img title="Alterar Posi��o Servi�o" alt="Alterar Posi��o Servi�o" src="/GGAS/imagens/setaBaixo.png"></a>
               	</c:if>
             </display:column>
             
	   	     <display:column title="Ordem de Faturamento" style="width: 100px">
				<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.ordemFaturamento}'/>
	        	</a>
	    	 </display:column>
	    	              
		     <display:column title="N�mero do Contrato" style="width: 100px">
				<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.numeroFormatado}'/>
	        	</a>
	    	 </display:column>
		 
		 	<display:column title="Tipo" style="width: 100px">
		 		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		 		<c:choose>
					<c:when test="${contrato.chavePrimariaPrincipal > '0'}">Complementar</c:when>
					<c:otherwise>Principal</c:otherwise>
				</c:choose>
				</a>
	    	</display:column>
	    	
	    	<display:column title="Situa��o" style="width: 100px">
		 		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.situacao.descricao}'/>
				</a>
	    	</display:column>  
	    		
	    	<display:column title="Data da Assinatura" style="width: 80px">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataAssinatura}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>			     
	    	
	    	<display:column title="Data de Vencimento" style="width: 80px">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataVencimentoObrigacoes}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>
	    	
	    	<c:set var="indexContrato" value="${indexContrato+1}" />
		</display:table>
