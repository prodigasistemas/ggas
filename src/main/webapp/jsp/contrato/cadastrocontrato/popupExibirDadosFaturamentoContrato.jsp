<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<!-- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> -->

<h1 class="tituloInterno">Exibir Dados Necess�rios para Faturamento dos Contratos Selecionados<a class="linkHelp" href="<help:help>/incluiralterarsegmento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">A condi��o m�nima para que um contrato seja apto para faturamento � ter pelo menos um ponto de consumo com todas as colunas da tabela preenchidas.</p>

<form method="post">
	<script>
		function cancelar() {
			window.close();
		}
	</script>
	
	<input name="acao" type="hidden" id="acao" value="exibirDadosFaturamentoContrato">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	
	<fieldset id="conteinerSegmento" class="conteinerPesquisarIncluirAlterar">
		<legend class="conteinerBlocoTitulo">Dados Necess�rios para Faturamento dos Contratos Selecionados</legend>
		<fieldset class="coluna">			

				
			<c:forEach items="${listaContratoDadosFaturamentoVO}" var="contratoDadosFaturamentoVO" varStatus="i">
				<label class="rotulo" for="descricao">Cliente:</label>
				<span class="itemDetalhamento"><c:out value="${contratoDadosFaturamentoVO.nomeCliente}"/></span><br />
				<label class="rotulo" for="descricao">N�mero do Contrato:</label>
				<span class="itemDetalhamento"><c:out value="${contratoDadosFaturamentoVO.numeroContratoFormatado}"/></span><br />
				<label class="rotulo" for="descricao">Apto para Faturamento:</label>
				
				<c:choose>
					<c:when test="${contratoDadosFaturamentoVO.faturavel == true}">
						<span class="itemDetalhamento"> Sim </span><br />
					</c:when>
					<c:otherwise>
						<span class="itemDetalhamento"> N�o </span><br />
					</c:otherwise>
				</c:choose>
				
				<table class="dataTableGGAS dataTableDialog">
					<thead>
						<tr>
							<th style="width: 20%;" > Im�vel
								<table style="width: 100%;"><thead><tr><th>&nbsp;</th></tr></thead></table>
							</th>
							<th style="width: 80%">
								Pontos de Consumo
								<table style="width: 100%">
									<thead>
										<tr>
											 <th style="width: 25%">Descri��o</td>
											 <th style="width: 25%">Segmento</td>
											 <th style="width: 25%">Ramo de Atividade</td>
											 <th style="width: 13%">Nr. S�rie do Medidor</td>
											 <th style="width: 12%">Rota</td>
										</tr>
									</thead>
								</table>
							</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="classeLinha" value="odd"/>
						<c:forEach items="${contratoDadosFaturamentoVO.listaImovelPontoConsumoVO}" var="imovelPontoConsumoVO" varStatus="i">
							<tr class="${classeLinha}">
								<td class="rotuloTabela" style="text-align: left; width: 20%">
										<c:out value="${imovelPontoConsumoVO.nome}"/>
								</td>
								<td class="colunaSubTable">
									<display:table class="subDataTable"  list="${imovelPontoConsumoVO.listaPontoConsumo}" sort="list" id="pontoConsumo" excludedParams="" requestURI="#">
	
										<display:column property="descricao" sortable="false"  headerClass="tituloTabelaEsq" style="width: 25%" />
										<display:column property="segmento.descricao" style="width: 25%" sortable="false" />
										<display:column property="ramoAtividade.descricao" style="width: 25%" sortable="false" />
										<display:column property="instalacaoMedidor.medidor.numeroSerie" style="width: 13%" sortable="false" />
										<display:column property="rota.numeroRota" style="width: 12%" sortable="false" />										
									    	 
									</display:table>
			
								</td>
							</tr>
							<c:choose>
								<c:when test="${classeLinha eq 'odd'}">
									<c:set var="classeLinha" value="even"/>
								</c:when>
								<c:otherwise>
									<c:set var="classeLinha" value="odd"/>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</tbody>
				</table>
				
				<br>
				<hr class="linhaSeparadora" />	
			</c:forEach>
			
		</fieldset>
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol2" type="button" id="button" value="Cancelar" onClick="cancelar();">
	</fieldset>
	</fieldset>
</form>