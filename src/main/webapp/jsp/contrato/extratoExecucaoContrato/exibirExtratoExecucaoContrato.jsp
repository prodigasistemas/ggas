<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<h1 class="tituloInterno">
	Consultar Extrato de Execu��o de Contrato - Geral<a class="linkHelp"
		href="<help:help>/consultarextratodeexecuodecontratogeral.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>

<script type="text/javascript">
	$(document).ready(function() {
		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#contrato,#pontoConsumo").chromatable({
			width : "891px"
		});
	});

	animatedcollapse
			.addDiv('dadosCliente', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosContrato',
			'fade=0,speed=400,persist=1,hide=0');

	function voltar() {
		submeter("extratoExecucaoContratoForm",
				"exibirPesquisaExtratoExecucaoContrato");
	}

	function detalhar() {
		submeter("extratoExecucaoContratoForm",
				"exibirDetalheExtratoExecucaoContrato");
	}
</script>

<form method="post" id="formExtratoExecucaoContrato"
	id="extratoExecucaoContratoForm" name="extratoExecucaoContratoForm">
	<input type="hidden" name="idContrato"
		value="<c:out value="${execucaoContratoVO.idContrato}"/>" />

	<fieldset id="extratoExecucaoContrato" class="detalhamento">

		<fieldset class="conteinerBloco">
			<label class="rotulo" id="rotuloCliente">Modelo:</label> <span
				id="detalhamentoDescricao" class="itemDetalhamento"><c:out
					value="${modeloContrato.descricao}" /></span>
		</fieldset>

		<hr class="linhaSeparadora2" />

		<fieldset class="conteinerBloco">
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]"
				data-openimage="imagens/setaCima.png"
				data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img
				src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Nome:</label> <span
						class="itemDetalhamento itemDetalhamentoMedio"><c:out
							value="${execucaoContratoVO.nomeCliente}" /></span><br /> <label
						class="rotulo" id="rotuloCnpjTexto">CPF/CNPJ:</label> <span
						class="itemDetalhamento"><c:out
							value="${execucaoContratoVO.cnpjCpfCliente}" /></span>
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo" id="rotuloEnderecoTexto">Endere�o:</label> <span
						class="itemDetalhamento itemDetalhamentoMedio"><c:out
							value="${execucaoContratoVO.enderecoCliente}" /></span><br /> <label
						class="rotulo" id="rotuloNumeroContrato">E-mail:</label> <span
						class="itemDetalhamento"><c:out
							value="${execucaoContratoVO.emailCliente}" /></span>
				</fieldset>
			</fieldset>

			<hr class="linhaSeparadora2" />

			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosContrato]"
				data-openimage="imagens/setaCima.png"
				data-closedimage="imagens/setaBaixo.png">Dados do Contrato <img
				src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosContrato" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo" id="rotuloNumeroContrato">N�mero de
						contrato:</label> <span class="itemDetalhamento"><c:out
							value="${execucaoContratoVO.numeroContrato}" /></span><br /> <label
						class="rotulo" id="rotuloDataAssinatura">Data de
						assinatura:</label> <span class="itemDetalhamento"><c:out
							value="${execucaoContratoVO.dataAssinaturaContrato}" /></span><br /> <label
						class="rotulo rotulo2Linhas"
						id="rotuloDataVencimentoObrigacoesContratuais">Data de
						vencimento das<br />obriga��es contratuais:
					</label> <span class="itemDetalhamento itemDetalhamento2Linhas"><c:out
							value="${dataAssinaturaContrato.dataVencimentoObrigacoesContratuais}" /></span>
				</fieldset>

				<fieldset class="colunaFinal">
					<label class="rotulo" id="rotuloQDCContrato">QDC
						Contratual:</label>
					<div id="QDCContrato" class="campoList campoVertical"
						style="overflow: auto; background-color: #FFF; width: 220px; height: 80px">
						<c:forEach items="${execucaoContratoVO.qdcContrato}"
							varStatus="loop">
							<option
								value="<c:out value="${execucaoContratoVO.qdcContrato[loop.index]}"/>">
								<c:out value="${execucaoContratoVO.qdcContrato[loop.index]}" />
							</option>
						</c:forEach>
					</div>
				</fieldset>
			</fieldset>
		</fieldset>

		<hr class="linhaSeparadora1" />

		<fieldset id="pesquisaImovelContrato" class="conteinerBloco">
			<legend>Rela��o dos pontos de consumo</legend>
			<display:table class="dataTableGGAS" name="listaPontoConsumo"
				id="pontoConsumo"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="exibirExtratoExecucaoContrato">
				<display:column property="imovel.nome" sortable="false"
					title="Im�vel" style="width: auto;" />

				<display:column property="descricao" sortable="false"
					sortProperty="pontoConsumo.descricao" title="Descri��o"
					style="width: auto;" />

				<display:column property="segmento.descricao" sortable="false"
					sortProperty="pontoConsumo.segmento.descricao" title="Segmento"
					style="width: auto;" />

				<display:column property="ramoAtividade.descricao" sortable="false"
					sortProperty="pontoConsumo.ramoAtividade.descricao"
					title="Ramo de Atividade" style="width: auto;" />
			</display:table>
		</fieldset>

		<hr class="linhaSeparadora2" />

		<fieldset id="conteinerDadosHistoricoAditivos">
			<legend>Hist�rico de aditivos</legend>
			<display:table class="dataTableGGAS" name="listaHistoricoAditivo"
				id="contrato"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="exibirExtratoExecucaoContrato">
				<display:column title="N�mero" property="numeroAditivo"
					sortable="false" sortProperty="numeroAditivo" style="width: auto;" />

				<display:column title="Descri��o" property="descricaoAditivo"
					sortable="false" sortProperty="descricaoAditivo"
					headerClass="tituloTabelaEsq" style="text-align: left" />

				<display:column title="Data" sortable="false"
					sortProperty="dataAditivo" style="width: auto;">
					<fmt:formatDate value="${contrato.dataAditivo}"
						pattern="dd/MM/yyyy" />
				</display:column>
			</display:table>
		</fieldset>

	</fieldset>

	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Cancelar"
			type="button" onClick="voltar();"> <input name="button"
			class="bottonRightCol2 botaoGrande1 botaoDetalhar" value="Detalhar"
			type="button" onclick="detalhar();">
	</fieldset>

</form>

