<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Extrato de Execu��o de Contrato - Pesquisar Contrato<a class="linkHelp" href="<help:help>/consultadoextratodeexecuodocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.</p>

<script type="text/javascript">

	$(document).ready(function(){
		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		
	}

	function limparFormulario(){
		document.extratoExecucaoContratoForm.numeroContrato.value = "";
		document.extratoExecucaoContratoForm.habilitado[0].checked = true;	
		
    	limparCamposPesquisa();
    	
    	document.extratoExecucaoContratoForm.indicadorPesquisa[0].checked = false;
    	document.extratoExecucaoContratoForm.indicadorPesquisa[1].checked = false;
    	document.extratoExecucaoContratoForm.situacaoContrato.selectedIndex = 0;
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}

	function detalharContrato(id){

		document.extratoExecucaoContratoForm.idContrato.value = id;
		submeter("extratoExecucaoContratoForm", "exibirExtratoExecucaoContrato");
		
	}

	function init() {
		<c:choose>
			<c:when test="${execucaoContratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${execucaoContratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
	}	
	
	addLoadEvent(init);	

</script>


<form method="post" action="pesquisarExtratoExecucaoContrato" id="extratoExecucaoContratoForm" name="extratoExecucaoContratoForm">
	<input name="idContrato" type="hidden" id="idContrato"/>
	 	
	<fieldset id="pesquisarExtratoExecucaoContrato" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${execucaoContratoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${execucaoContratoVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${execucaoContratoVO.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${execucaoContratoVO.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${execucaoContratoVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${execucaoContratoVO.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${execucaoContratoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${execucaoContratoVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${execucaoContratoVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${execucaoContratoVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${execucaoContratoVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${execucaoContratoVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${execucaoContratoVO.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${execucaoContratoVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${execucaoContratoVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${execucaoContratoVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${execucaoContratoVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${execucaoContratoVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${execucaoContratoVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<fieldset class="coluna">
				<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:<!-- fix bug --></label>
				<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="9" size="8" value="${execucaoContratoVO.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"> 
				<br />
				<label class="rotulo" for="habilitado">Situa��o do Contrato:</label>
				<select class="campoSelect" id="situacaoContrato" name="situacaoContrato">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSituacaoContrato}" var="situacaoContrato">
						<option value="<c:out value="${situacaoContrato.chavePrimaria}"/>" <c:if test="${execucaoContratoVO.situacaoContrato == situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${situacaoContrato.descricao}"/>
						</option>		
			    	</c:forEach>
				</select>	
			</fieldset>
			
			<fieldset class="colunaFinal">
				<label class="rotulo" for="habilitado">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="ativo" value="true" <c:if test="${execucaoContratoVO.habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="ativo">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="inativo" value="false" <c:if test="${execucaoContratoVO.habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="inativo">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="todos" value="" <c:if test="${execucaoContratoVO.habilitado eq ''}">checked</c:if>>
				<label class="rotuloRadio" for="todos">Todos</label>
			</fieldset>
									
			<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="pesquisarExtratoExecucaoContrato">
	    			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    		</vacess:vacess>		
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
			</fieldset>
		</fieldset>	
	
	</fieldset>
	
	<c:if test="${listaContratos ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<br /><br />
		<p class="orientacaoInicial">Selecione um contrato clicando em um item da listagem abaixo.</p>
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaContratos" id="contrato" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarExtratoExecucaoContrato">
	     	 
	     	 <display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${contrato.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
			 
			 <display:column sortable="true" title="N�mero do<br />Contrato" sortProperty="numeroFormatado" style="width: 120px">
				<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.numeroFormatado}'/>
	        	</a>
	    	 </display:column>
			 
		     <display:column sortable="true" sortProperty="clienteAssinatura.nome" title="Nome do Cliente" headerClass="tituloTabelaEsq" style="text-align: left">
		     	<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.clienteAssinatura.nome}'/>
				</a>
		     </display:column>
		     
		     <display:column title="Data da<br />Assinatura" style="width: 80px" sortable="true" sortProperty="dataAssinatura">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataAssinatura}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>
	    	
	    	<display:column title="Data de vencimento das<br />obriga��es contratuais" style="width: 150px" sortable="true" sortProperty="dataVencimentoObrigacoes">
	    		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatDate value="${contrato.dataVencimentoObrigacoes}" pattern="dd/MM/yyyy"/>
				</a>
	    	</display:column>
		     	     
		 	<display:column sortable="true" title="Situa��o" style="width: 110px" sortProperty="situacao.descricao">
		 		<a href='javascript:detalharContrato(<c:out value='${contrato.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${contrato.situacao.descricao}'/>
				</a>
	    	</display:column>
	    				     
		</display:table>
	</c:if>
	
</form>