<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<h1 class="tituloInterno">Consultar Extrato de Execu��o de Contrato - Detalhar<a class="linkHelp" href="<help:help>/consultarextratodeexecuodecontratodetalhar.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para Consultar Extrato de Execu��o de Contrato, informe os dados nos campos abaixo <!-- pesquise um contrato selecionando um Cliente ou Im�vel e/ou N�mero do Contrato -->e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />

<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#dataConsultaInicial").change(function(){
			if($('#dataConsultaInicial').val() != '__/__/____'){
				$('#dataConsultaFinal').datepicker('option', 'minDate', $("#dataConsultaInicial").val());
				}else{
					$('#dataConsultaFinal').datepicker('option', 'minDate', '');
					}
		});
		document.forms[0].botaoExibirQdaVolumes.disabled = true;
	});

	function voltar(){

		submeter("extratoExecucaoContratoForm", "exibirExtratoExecucaoContrato");
		
	}

	function cancelar(){

		submeter("extratoExecucaoContratoForm", "exibirPesquisaExtratoExecucaoContrato");
		
	}

	function limparFormulario(){

		document.extratoExecucaoContratoForm.contratoModalidade.selectedIndex = 0;
		document.extratoExecucaoContratoForm.dataConsultaInicial.value = "";
		document.extratoExecucaoContratoForm.dataConsultaFinal.value = "";
		document.extratoExecucaoContratoForm.tipoConsulta.selectedIndex = 0;

	}

	function exibirQuantidadeVolumes(){
		submeter("extratoExecucaoContratoForm", "detalharExtratoExecucaoContrato");
	}

	function configurarCampoModalidadeContrato(){

		var tipoConsultaCompromisso = <c:out value="${codigoCompromisso}"/>
		var consultaSelecionada = document.forms[0].tipoConsulta.value;

		if(consultaSelecionada != tipoConsultaCompromisso){
			document.forms[0].contratoModalidade.value = '-1';
			document.forms[0].contratoModalidade.disabled = true;
			$("#contratoModalidade").addClass("campoDesabilitado");
			$("#rotuloModalidade, #rotuloModalidade .campoObrigatorioSimbolo").addClass("rotuloDesabilitado");

		}else{
			document.forms[0].contratoModalidade.disabled = false;
			$("#rotuloModalidade, #rotuloModalidade .campoObrigatorioSimbolo").removeClass("rotuloDesabilitado");
			$("#contratoModalidade").removeClass("campoDesabilitado");

		} 
	
	}

	function habilitarDetalhamento(){

		var tipoConsultaCompromisso = <c:out value="${codigoCompromisso}"/>
		var consultaSelecionada = document.forms[0].tipoConsulta.value;
		var dataConsultaInicial = $("input#dataConsultaInicial").val();
		var dataConsultaFinal = $("input#dataConsultaFinal").val();
		var contratoModalidade = document.forms[0].contratoModalidade.value;		
		
		if(consultaSelecionada != tipoConsultaCompromisso){//O valor do campo tipo n�o � compromisso
			if(dataConsultaInicial != "" && dataConsultaInicial != "__/__/____" && dataConsultaFinal != "" && dataConsultaFinal != "__/__/____"){			    
				document.forms[0].botaoExibirQdaVolumes.disabled = false;
			}else {
				document.forms[0].botaoExibirQdaVolumes.disabled = true;
				}
		}else{ //O valor do campo tipo � compromisso
			if (dataConsultaInicial != "" && dataConsultaInicial != "__/__/____" && dataConsultaFinal != "" && dataConsultaFinal != "__/__/____" &&
			     contratoModalidade != -1) {
				document.forms[0].botaoExibirQdaVolumes.disabled = false;
			}else{
				document.forms[0].botaoExibirQdaVolumes.disabled = true;
				}
		} 
	}

	function init () {
		configurarCampoModalidadeContrato();
		habilitarDetalhamento();
	}
	
	addLoadEvent(init);	
	
	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosContrato', 'fade=0,speed=400,persist=1,hide=0');
	
</script>

<form method="post" id="formExtratoExecucaoContrato" name="extratoExecucaoContratoForm">
	<input type="hidden" name="idContrato" value="<c:out value="${execucaoContratoVO.idContrato}"/>"/>
	<input type="hidden" name="consultarQdaVolumes" value=""/>
	
	<fieldset id="detalharExtratoExecucaoContrato" class="detalhamento">
		
		<fieldset class="conteinerBloco">
			<label class="rotulo" id="rotuloCliente">Modelo:</label>
			<span id="detalhamentoDescricao" class="itemDetalhamento"><c:out value="${modeloContrato.descricao}"/></span>
		</fieldset>
		
		<hr class="linhaSeparadora2" />
		
		<fieldset class="conteinerBloco">
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					
					<label class="rotulo">Nome:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${execucaoContratoVO.nomeCliente}"/></span><br />
					
					<label class="rotulo" id="rotuloCnpjTexto">CPF/CNPJ:</label>
					<span class="itemDetalhamento"><c:out value="${execucaoContratoVO.cnpjCpfCliente}"/></span><br />
										
				</fieldset>
				<fieldset class="colunaFinal">
					
					<label class="rotulo" id="rotuloEnderecoTexto">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${execucaoContratoVO.enderecoCliente}"/></span><br />
					
					<label class="rotulo" id="rotuloNumeroContrato">E-mail:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${execucaoContratoVO.emailCliente}"/></span><br />
					
				</fieldset>
			</fieldset>
			<hr class="linhaSeparadora2" />
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosContrato]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Contrato <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosContrato" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					
					<label class="rotulo" id="rotuloNumeroContrato">N�mero de contrato:</label>
					<span class="itemDetalhamento"><c:out value="${execucaoContratoVO.numeroContrato}"/></span><br />
					
					<label class="rotulo" id="rotuloDataAssinatura">Data de assinatura:</label>
					<span class="itemDetalhamento"><c:out value="${execucaoContratoVO.dataAssinaturaContrato}"/></span><br />
					
					<label class="rotulo rotulo2Linhas" id="rotuloDataVencimentoObrigacoesContratuais">Data de vencimento das obriga��es contratuais:</label>
					<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${execucaoContratoVO.dataVencimentoObrigacoesContratuais}"/></span><br />
														
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo" id="rotuloQDCContrato">QDC Contratual:</label>
					<div id="QDCContrato" class="campoList campoVertical" style="overflow: auto; background-color: #FFF; width: 220px; height: 80px">
						<c:forEach items="${execucaoContratoVO.qdcContrato}"  varStatus="loop">
							<option value="<c:out value="${execucaoContratoVO.qdcContrato[loop.index]}"/>">
								<c:out value="${execucaoContratoVO.qdcContrato[loop.index]}"/>
							</option>		
						</c:forEach>					
					</div>
				</fieldset>
			</fieldset>
			
			<hr class="linhaSeparadora2" />
			
			<fieldset id="conteinerConfiguracao" class="conteinerBloco">
				
				<label class="rotulo campoObrigatorio" id="rotuloTipoConsulta"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
				<select class="campoSelect campoHorizontal" id="tipoConsulta" name="tipoConsulta" onchange="configurarCampoModalidadeContrato(); habilitarDetalhamento();">
					<c:forEach items="${listaTipoConsulta}" var="tipoConsulta">
						<option value="<c:out value="${tipoConsulta.key}"/>" <c:if test="${execucaoContratoVO.tipoConsulta eq tipoConsulta.key}">selected="selected"</c:if>>
							<c:out value="${tipoConsulta.value}"/>
						</option>		
			    	</c:forEach>
				</select>				
								
				<label class="rotulo rotuloHorizontal campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Data:</label>
				<input class="campoData campoHorizontal" type="text" id="dataConsultaInicial" name="dataConsultaInicial" maxlength="10" value="${execucaoContratoVO.dataConsultaInicial}" onblur="habilitarDetalhamento();">
				<label class="rotuloEntreCampos" for="dataConsultaFinal">at�</label>
				<input class="campoData campoHorizontal" type="text" id="dataConsultaFinal" name="dataConsultaFinal" maxlength="10" value="${execucaoContratoVO.dataConsultaFinal}" onblur="habilitarDetalhamento();">
								
				<label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloModalidade"><span class="campoObrigatorioSimbolo">* </span>Modalidade:</label>				
				<select class="campoSelect campoHorizontal" id="contratoModalidade" name="contratoModalidade" onchange="habilitarDetalhamento();">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaContratoModalidade}" var="contratoModalidade">
						<option value="<c:out value="${contratoModalidade.chavePrimaria}"/>" <c:if test="${execucaoContratoVO.contratoModalidade == contratoModalidade.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${contratoModalidade.descricao}"/>
						</option>		
			    	</c:forEach>
				</select>
								
				<fieldset class="conteinerBotoesPesquisarDir2">
			    	<input name="Button" class="bottonRightCol2 botaoExibir" id="botaoExibirQdaVolumes" value="Exibir" type="button" onclick="exibirQuantidadeVolumes();">		
					<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
				</fieldset>
			</fieldset>
															
		</fieldset>
				
		<fieldset id="conteinerResultadosExtratoExecucaoContrato" class="conteinerBloco">
			<c:if test="${not empty listaApuracaoTOP}">
			<hr class="linhaSeparadora2" />				
			<fieldset class="conteinerBloco">
				<legend>Take or Pay</legend>
				<display:table class="dataTableGGAS" name="listaApuracaoTOP" id="top" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="detalharExtratoExecucaoContrato">
		           <display:column title="Per�odo" style="width: 80px" sortable="false" sortProperty="data">
	    				 <c:out value="${top.periodoApuracaoFormatado}"/>
				   </display:column>
					<display:column title="QDC(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdc" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${top.valorQDC}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="QDS(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${top.valorQDS}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="QDP(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${top.valorQDP}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="TOP - QNR" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${top.valorQNR}" minFractionDigits="2" />
	    			</display:column>
	    			<display:column title="TOP - QPNR" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${top.valorQPNR}" minFractionDigits="2" />
	    			</display:column>
	    			<display:footer>
					  	<tr class="total2">
					  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDCTOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDSTOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDPTOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQNRTOP}" minFractionDigits="2"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQPNRTOP}" minFractionDigits="2"/></td>
					  	</tr>
					 </display:footer>
				</display:table>
			</fieldset>
			</c:if>	
			 
			<c:if test="${not empty listaApuracaoSOP}">
			<hr class="linhaSeparadora2" />
			<fieldset class="conteinerBloco">
				<legend>Ship or Pay</legend>
				<display:table class="dataTableGGAS" name="listaApuracaoSOP" id="sop" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="detalharExtratoExecucaoContrato">
		            <display:column title="Per�odo" style="width: 80px" sortable="false" sortProperty="data">
    						 <c:out value="${sop.periodoApuracaoFormatado}"/>
				     </display:column>
					<display:column title="QDC(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdc" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${sop.valorQDC}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="QDS(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${sop.valorQDS}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="QDP(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${sop.valorQDP}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="SOP - QNR" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${sop.valorQNR}" minFractionDigits="2" />
	    			</display:column>
	    			<display:column title="SOP - QPNR" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
    		    			<fmt:formatNumber value="${sop.valorQPNR}" minFractionDigits="2" />
	    			</display:column>
	    			<display:footer>
					  	<tr class="total2">
					  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDCSOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDSSOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDPSOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQNRSOP}" minFractionDigits="2"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQPNRSOP}" minFractionDigits="2"/></td>
					  	</tr>
					 </display:footer>
				</display:table>									
			</fieldset>
			</c:if>
			
			<c:if test="${not empty listaApuracaoDOP}">
			<hr class="linhaSeparadora2" />
			<fieldset class="conteinerBloco">
				<legend>Delivery or Pay</legend>
				<display:table class="dataTableGGAS" name="listaApuracaoDOP" id="dop" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="detalharExtratoExecucaoContrato">
		          <display:column title="Per�odo" style="width: 80px" sortable="false" sortProperty="data">
	    							   		
				   		<c:choose>
	    					<c:when test="${dop.dataInicioApuracao ne dop.dataFimApuracao}">
	    						<fmt:formatDate value="${dop.dataInicioApuracao}" pattern="dd/MM/yyyy"/>
	    					</c:when>
	    					<c:otherwise>
	    						<c:out value="${dop.periodoApuracaoFormatado}"/>
	    					</c:otherwise>
	    				</c:choose>
				   						   	
				   	</display:column>
					<display:column title="QDC(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdc" style="width: 18%; text-align: right; padding-right: 5px">
	    				
	    				<c:choose>
	    					<c:when test="${dop.dataInicioApuracao ne dop.dataFimApuracao}">
	    						<fmt:formatNumber value="${dop.valorQDC}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:when>
	    					<c:otherwise>
	    						<fmt:formatNumber value="${dop.valorQDCM}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:otherwise>
	    				</c:choose>
	    				    				
	    			</display:column>
	    			<display:column title="QDS(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${dop.valorQDS}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="QDP(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${dop.valorQDP}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="Valor Calculado(R$)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
    		    		<fmt:formatNumber value="${dop.valorCalculado}" minFractionDigits="2" />
	    			</display:column>
	    			<display:column title="DOP - QNR" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
    		    		<fmt:formatNumber value="${dop.valorQNR}" minFractionDigits="2" />
	    			</display:column>
	    			<display:footer>
					  	<tr class="total2">
					  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDCDOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDSDOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDPDOP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalValorCalculadoDOP}" minFractionDigits="2"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQNRDOP}" minFractionDigits="2"/></td>
					  	</tr>
					 </display:footer>
				</display:table>
			</fieldset>													
			</c:if>
			
			<c:if test="${not empty listaApuracaoGasForaEspecificacao}">
			<hr class="linhaSeparadora2" />
			<fieldset class="conteinerBloco">
				<legend>G�s Fora de Especifica��o</legend>
				<display:table class="dataTableGGAS" name="listaApuracaoGasForaEspecificacao" id="gasForaEspecificacao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="detalharExtratoExecucaoContrato">
		          <display:column title="Per�odo" style="width: 80px" sortable="false" sortProperty="data">
	    					
	    				<c:choose>
	    					<c:when test="${gasForaEspecificacao.dataInicioApuracao ne gasForaEspecificacao.dataFimApuracao}">
	    						<fmt:formatDate value="${gasForaEspecificacao.dataInicioApuracao}" pattern="dd/MM/yyyy"/>
	    					</c:when>
	    					<c:otherwise>
	    						<c:out value="${gasForaEspecificacao.periodoApuracaoFormatado}"/>
	    					</c:otherwise>
	    				</c:choose>		 
	    				 				   
				   </display:column>
					<display:column title="QDC(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdc" style="width: 18%; text-align: right; padding-right: 5px">
	    				
	    				<c:choose>
	    					<c:when test="${gasForaEspecificacao.dataInicioApuracao ne gasForaEspecificacao.dataFimApuracao}">
	    						<fmt:formatNumber value="${gasForaEspecificacao.valorQDC}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:when>
	    					<c:otherwise>
	    						<fmt:formatNumber value="${gasForaEspecificacao.valorQDCM}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:otherwise>
	    				</c:choose>
	    				    				
	    			</display:column>
	    			<display:column title="QDS(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${gasForaEspecificacao.valorQDS}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="QDP(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    			   <fmt:formatNumber value="${gasForaEspecificacao.valorQDP}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:column title="Valor Calculado(R$)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
    		    		<fmt:formatNumber value="${gasForaEspecificacao.valorCalculado}" minFractionDigits="2" />
	    			</display:column>
	    			<display:column title="DOP - QNR" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
    		    		<fmt:formatNumber value="${gasForaEspecificacao.valorQNR}" minFractionDigits="2" />
	    			</display:column>
	    			<display:footer>
					  	<tr class="total2">
					  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDCGAS}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDSGAS}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQDPGAS}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalValorCalculadoGAS}" minFractionDigits="2"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalQNRGAS}" minFractionDigits="2"/></td>
					  	</tr>
					 </display:footer>
				</display:table>
			</fieldset>													
			</c:if>
            
           	<c:if test="${not empty listaPenalidadeMaior}">
           	<hr class="linhaSeparadora2" />
           	<fieldset class="conteinerBloco">
				<legend>Retirada a maior</legend>
				<display:table class="dataTableGGAS" name="listaPenalidadeMaior" id="penalidade" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="detalharExtratoExecucaoContrato">
		           <display:column title="Per�odo" style="width: 80px" sortable="false" sortProperty="data">
	  					
	  					<c:choose>
	    					<c:when test="${penalidade.dataInicioApuracao ne penalidade.dataFimApuracao}">
	    						<fmt:formatDate value="${penalidade.dataInicioApuracao}" pattern="dd/MM/yyyy"/>
	    					</c:when>
	    					<c:otherwise>
	    						<c:out value="${penalidade.periodoApuracaoFormatado}"/>
	    					</c:otherwise>
	    				</c:choose>
	    				
				   </display:column>
					<display:column title="QDC(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdc" style="width: 18%; text-align: right; padding-right: 5px">
	    				
	    				<c:choose>
	    					<c:when test="${penalidade.dataInicioApuracao ne penalidade.dataFimApuracao}">
	    						<fmt:formatNumber value="${penalidade.qtdaDiariaContratadaMedia}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:when>
	    					<c:otherwise>
	    						<fmt:formatNumber value="${penalidade.qtdaDiariaContratada}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:otherwise>
	    				</c:choose>	    				
	    				
	    			</display:column>
	    				    			
	    			<display:column title="QDR(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${penalidade.qtdaDiariaRetirada}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			
	    			<display:column title="Retirada a maior(m<span class='expoente'>3</span>)" sortable="false" sortProperty="retiradaMaior" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${penalidade.qtdaDiariaRetiradaMaior}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
                      
	    			<display:column title="Valor Calculado(R$)" sortable="false" sortProperty="penalidade" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${penalidade.valorCalculado}" minFractionDigits="2" />
	    			</display:column>
	    			
	    			<display:column title="Penalidade(R$)" sortable="false" sortProperty="penalidade" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${penalidade.valorCobrado}" minFractionDigits="2" />
	    			</display:column>
	    				    			
	    			<display:footer>
					  	<tr class="total2">
					  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.valorTotalQDC}" minFractionDigits="4" maxFractionDigits="4" type="number" /></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.valorTotalQDR}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.valorTotalRetiradaMaior}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalValorCalculadoRetiradaMaior}" minFractionDigits="2"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalPenalidades}" minFractionDigits="2"/></td>
					  	</tr>
					 </display:footer>
				</display:table>
			</fieldset>
			</c:if>
				
			<c:if test="${not empty listaPenalidadeMenor}">
			<hr class="linhaSeparadora2" />
			<fieldset class="conteinerBloco">
				<legend>Retirada a menor</legend>
				<display:table class="dataTableGGAS" name="listaPenalidadeMenor" id="penalidade" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="detalharExtratoExecucaoContrato">
		           <display:column title="Per�odo" style="width: 80px" sortable="false" sortProperty="data">
    					
    					<c:choose>
	    					<c:when test="${penalidade.dataInicioApuracao ne penalidade.dataFimApuracao}">
	    						<fmt:formatDate value="${penalidade.dataInicioApuracao}" pattern="dd/MM/yyyy"/>
	    					</c:when>
	    					<c:otherwise>
	    						<c:out value="${penalidade.periodoApuracaoFormatado}"/>
	    					</c:otherwise>
	    				</c:choose>
	    				
    				</display:column>
					<display:column title="QDC(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdc" style="width: 18%; text-align: right; padding-right: 5px">
	    				
	    				<c:choose>
	    					<c:when test="${penalidade.dataInicioApuracao ne penalidade.dataFimApuracao}">
	    						<fmt:formatNumber value="${penalidade.qtdaDiariaContratadaMedia}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:when>
	    					<c:otherwise>
	    						<fmt:formatNumber value="${penalidade.qtdaDiariaContratada}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    					</c:otherwise>
	    				</c:choose>	
	    			
	    			</display:column>
	    				    			
	    			<display:column title="QDR(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${penalidade.qtdaDiariaRetirada}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			
	    			<display:column title="Retirada a menor(m<span class='expoente'>3</span>)" sortable="false" sortProperty="retiradaMaior" style="width: 18%; text-align: right; padding-right: 5px">
    		    			<fmt:formatNumber value="${penalidade.qtdaNaoRetirada}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
                      
	    			<display:column title="Valor Calculado(R$)" sortable="false" sortProperty="penalidade" style="width: 18%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${penalidade.valorCalculado}" minFractionDigits="2" />
	    			</display:column>
	    			
	    			<display:column title="Penalidade(R$)" sortable="false" sortProperty="penalidade" style="width: 18%; text-align: right; padding-right: 5px">
   		    			<fmt:formatNumber value="${penalidade.valorCobrado}" minFractionDigits="2" />
	    			</display:column>
	    			
	    			<display:footer>
					  	<tr class="total2">
					  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.valorTotalQDCMenor}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.valorTotalQDRMenor}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.valorTotalRetiradaMenor}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalValorCalculadoRetiradaMenor}" minFractionDigits="2"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalPenalidadesMenor}" minFractionDigits="2"/></td>
					  	</tr>
					 </display:footer>
				</display:table>
			</fieldset>
			</c:if>
									
			<c:if test="${not empty listaApuracaoRecuperacaoVO}">
			<hr class="linhaSeparadora2" />
			<fieldset class="conteinerBloco">
				<legend>Volumes</legend>
				<display:table class="dataTableGGAS" name="listaApuracaoRecuperacaoVO" id="vo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="detalharExtratoExecucaoContrato">
					<display:column title="Data" style="width: 80px" sortable="false" sortProperty="data">
	    				<c:out value="${vo.dataFormatada}"/>
					</display:column>
					
					<display:column title="QDC(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdc" style="width: 15%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${vo.valorQDC}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			
	    			<display:column title="QDS(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qds" style="width: 15%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${vo.valorQDS}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			
	    			<display:column title="QDP(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdp" style="width: 15%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${vo.valorQDP}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			
	    			<display:column title="QDR(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qdr" style="width: 15%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${vo.valorQDR}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			
	    			<display:column title="QR(m<span class='expoente'>3</span>)" sortable="false" sortProperty="qr" style="width: 15%; text-align: right; padding-right: 5px">
	    				<fmt:formatNumber value="${vo.valorQR}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    				<fmt:formatNumber value="${penalidade.qtdaDiariaRetirada}" minFractionDigits="4" maxFractionDigits="4" type="number" />
	    			</display:column>
	    			<display:footer>
					  	<tr class="total2">
					  		<td style="text-align: right; padding-right: 5px">Total Geral:</td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalVolumeQDC}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalVolumeQDS}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalVolumeQDP}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalVolumeQDR}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  		<td style="text-align: right; padding-right: 5px"><fmt:formatNumber value="${execucaoContratoVO.totalVolumeQR}" minFractionDigits="4" maxFractionDigits="4" type="number"/></td>
					  	</tr>
					 </display:footer>
				</display:table>
			</fieldset>
				
			<hr class="linhaSeparadora2" />
				
			<div class="coluna2">
				<label class="rotulo rotulo2Linhas rotuloVertical">Falha de programa��o:</label>
				<div id="paradaNaoProgramada" class="campoList campoVertical" style="overflow: auto; background-color: #FFF; width: 105px">
					<c:forEach items="${execucaoContratoVO.paradaNaoProgramada}" varStatus="loop">
						<option value="<c:out value="${execucaoContratoVO.paradaNaoProgramada[loop.index]}"/>">
							<c:out value="${execucaoContratoVO.paradaNaoProgramada[loop.index]}"/>
						</option>		
					</c:forEach>					
				</div>
			</div>
			
			<div class="coluna2">
				<label class="rotulo rotulo2Linhas rotuloVertical">Falha de fornecimento:</label>
				<div id="falhasFornecimento" class="campoList campoVertical" style="overflow: auto; background-color: #FFF; width: 105px">
					<c:forEach items="${execucaoContratoVO.falhasFornecimento}" varStatus="loop">
						<option value="<c:out value="${execucaoContratoVO.falhasFornecimento[loop.index]}"/>">
							<c:out value="${execucaoContratoVO.falhasFornecimento[loop.index]}"/>
						</option>		
					</c:forEach>					
				</div>
			</div>
			
			<div class="coluna2">
				<label class="rotulo rotulo2Linhas rotuloVertical">Caso Fortuito ou for�a maior:</label>
				<div id="casosFortuitoForcaMaior" class="campoList campoVertical" style="overflow: auto; background-color: #FFF; width: 105px">
					<c:forEach items="${execucaoContratoVO.casosFortuitoForcaMaior}" varStatus="loop">
						<option value="<c:out value="${execucaoContratoVO.casosFortuitoForcaMaior[loop.index]}"/>">
							<c:out value="${execucaoContratoVO.casosFortuitoForcaMaior[loop.index]}"/>
						</option>		
					</c:forEach>					
				</div>
			</div>
			
			<div class="coluna2">
				<label class="rotulo rotulo2Linhas rotuloVertical">Parada programada:</label>
				<div id="paradasProgramada" class="campoList campoVertical" style="overflow: auto; background-color: #FFF; width: 105px">
					<c:forEach items="${execucaoContratoVO.paradasProgramada}" varStatus="loop">
						<span><c:out value="${execucaoContratoVO.paradasProgramada[loop.index]}"/></span>
					</c:forEach>					
				</div>
			</div>
			</c:if>
		</fieldset>
		
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2 botaoVoltar" value="<< Voltar" type="button" onClick="voltar();">    	
    	<input name="button" class="bottonRightCol2 bottonLeftColUltimo" value="Cancelar"  type="button" onclick="cancelar();">
 	</fieldset>
		
</form>