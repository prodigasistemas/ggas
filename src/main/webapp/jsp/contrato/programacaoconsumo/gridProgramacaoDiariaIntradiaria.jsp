<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />
<script type="text/javascript">

$(document).ready(function(){
	
	// Dialog			
	$("#motivoNaoAceite").dialog({
		autoOpen: false,
		width: 333,
		modal: true,
		resizable: false
	});
	
	$("#solicitacao input[type='text']").change(function(){
		$("#inputModificado").attr({value: true });
		inputModificado = true;
	});

	exibirJDialog();
	$().dialog({
		autoOpen: false, 
		width: 340, 
		modal: true, 
		minHeight: 100, 
		maxHeight: 300,
		resizable: false
	});

// 	verificarSaldoInicialQPNR();
	
// 	$('[name=indicadorAceiteLista]').click().click();
	
});



function selecionar_tudo(){ 
	var checkTodos = document.getElementById("checkTodos");
	
	for (i=0;i<document.programacaoConsumoForm.elements.length;i++) 
		
	      if(document.programacaoConsumoForm.elements[i].name == "indicadorAceiteLista")	
	    	  if(checkTodos.checked==1){
	    	
	         document.programacaoConsumoForm.elements[i].checked=1 
	      alert(  document.getElementById(document.programacaoConsumoForm.elements[i].id));
	        
	    	  }else{
	         document.programacaoConsumoForm.elements[i].checked=0 
	         document.programacaoConsumoForm.elements[i].onclick
	    	  }
	} 
	
	
    function exibirComentario(dataFormatada,idPontoConsumo,chaveModalidade){
        var popupComentario;
     
        document.getElementById("dataSolicitacaoComentario").value = dataFormatada;    
        document.getElementById("idPontoConsumo").value = idPontoConsumo;    
        document.getElementById("chaveModalidade").value = chaveModalidade;    
        popupComentario = window.open('exibirComentarioSolicitacaoConsumo?postBack=false','popupComentario','height=600,width=980,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0 ,modal=1');
        
         
        submeter("programacaoConsumoForm", "exibirComentarioSolicitacaoConsumo?", "popupComentario");
            
    }
    
	function exibirMotivoNaoAceite(data, valorQDC, valorQDS, indexCityGate, cont) {
		
// 		$('#dataSolicitacaoEscolhida').val(cont);
		$('#popupIndexCityGate').val(indexCityGate);
		$('#popupIndexSolicitacao').val(cont);
// 		document.getElementById("dataSolicitacaoEscolhida").value = cont;
		$('#dataSolicitacaoPopup').html(data);
// 		document.getElementById("dataSolicitacaoPopup").innerHTML = data;
		$('#valorQDCPopup').html(valorQDC);
// 		document.getElementById("valorQDCPopup").innerHTML = valorQDC;
		$('#valorQDSPopup').html(valorQDS);
// 		document.getElementById("valorQDSPopup").innerHTML = valorQDS;
		$('#valorQDPPopup').html($('#valorQDP_' + indexCityGate + '_' + cont).val());
// 		document.getElementById("valorQDPPopup").innerHTML = valorQDP;
		$('#descricaoMotivoPopup').val($('#descricaoMotivo_' + indexCityGate + '_' + cont).val());
// 		document.getElementById("descricaoMotivoPopup").value = 
// 			document.getElementById("descricaoMotivo" + cont).value;
	
		exibirJDialog("#motivoNaoAceite");
	}
	
	function guardarMotivoNaoAceite() {
// 		var dataSolicitacao = document.getElementById("dataSolicitacaoEscolhida").value;
		var indexCityGate = $('#popupIndexCityGate').val();
		var indexSolicitacao = $('#popupIndexSolicitacao').val();
		var motivo = document.getElementById("descricaoMotivoPopup").value;
		$('#descricaoMotivo_' + indexCityGate + '_' + indexSolicitacao).val(motivo);
// 		document.getElementById("descricaoMotivo" + dataSolicitacao).value = motivo;
		$("#motivoNaoAceite").dialog('close');
	}
	
	function validarValorQDS(qds,qdp,qdc,check){
		var x = document.getElementById(check);
		if(x.checked==1){
		document.getElementById(qdp).value = qds
		}else{document.getElementById(qdp).value = qdc}
	}

	</script>
	
	<input name="dataSolicitacaoComentario" type="hidden" id="dataSolicitacaoComentario" />
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" /> 
	<input name="chaveModalidade" type="hidden" id="chaveModalidade" />
	
	<div id="motivoNaoAceite" title="Motivo do N�o Aceite">
<!-- 		<input type="hidden" id="dataSolicitacaoEscolhida" value=""/> -->
		<input type="hidden" id="popupIndexCityGate" value=""/>
		<input type="hidden" id="popupIndexSolicitacao" value=""/>
		<a class="linkHelp" href="<help:help>/motivodonoaceite.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		<label class="rotulo rotulo2Linhas">Data de Solicita��o:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas" id="dataSolicitacaoPopup"></span><br />
		<label class="rotulo">QDC:</label>
		<span class="itemDetalhamento" id="valorQDCPopup"></span><br />
		<label class="rotulo">QDS:</label>
		<span class="itemDetalhamento" id="valorQDSPopup"></span><br />
		<label class="rotulo">QDP:</label>
		<span class="itemDetalhamento" id="valorQDPPopup"></span><br class="quebraLinha2">
		<label class="rotulo rotuloVertical">Motivo:</label>
		<textarea id="descricaoMotivoPopup" rows="6" onkeypress="return formatarCampoTextoComLimite(event,this,200);"></textarea>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Confirmar" type="button" onclick="guardarMotivoNaoAceite()">
	</div>
	
	<c:set var="indexCityGate" value="0" />
	<c:forEach items="${listaCityGateSolicitacaoConsumoVO}" var="cityGateSolicitacaoConsumoVO">
		
		<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">City Gate(s): ${cityGateSolicitacaoConsumoVO.listaDescricaoCityGate}</label>	
		
		<label class="rotulo" id="rotuloTotalQds" >Total Qds: ${cityGateSolicitacaoConsumoVO.totalQdsFormatado}</label>
			
<%-- 		<label class="rotulo" id="rotuloTotalQdp" >Total Qdp: ${cityGateSolicitacaoConsumoVO.totalQdp}</label>	 --%>
	
		<c:set var="j" value="0" />
		<display:table 
			class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			name="${cityGateSolicitacaoConsumoVO.listaSolicitacaoConsumoVO}" sort="list" id="solicitacao" 
			pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" 
			requestURI="pesquisarProgramacaoConsumoDiariaIntraDiaria" > 
	
			<display:column sortable="false" title="Cliente" style="width: 75px">
				<input name="idPontoConsumoLista" type="hidden" id="idPontoConsumoLista" value="<c:out value="${solicitacao.idPontoConsumo}"/>"  /> 
				<input name="idModalidade" type="hidden" id="idModalidade" value="<c:out value="${solicitacao.idModalidade}"/>"  /> 
				<input name="indicadorTipoProgramacaoConsumo" type="hidden" id="indicadorTipoProgramacaoConsumo" value="<c:out value="${solicitacao.indicadorTipoProgramacaoConsumo}"/>"  /> 
				<input type="hidden" id="dataSolicitacao${solicitacao.dataFormatada}" name="dataSolicitacao" value="<c:out value="${solicitacao.dataFormatada}"/>"/>
				<input type="hidden" name="valorQPNR" id="valorQPNR${solicitacao.dataFormatada}" value="<c:out value="${solicitacao.valorQPNR}"/>"/>
				<c:out value="${solicitacao.nomeCliente}"/>
			</display:column>
			
			<display:column sortable="false" title="Ponto consumo" style="width: 75px">
				<c:out value="${solicitacao.descricaoPontoConsumo}"/>
			</display:column>
			
			<display:column class="QDC" sortable="false" title="QDC (m<span class='expoente'>3</span>/dia)" style="width: 19%; text-align: right; padding-right: 5px">
				<input type="hidden" id="valorQDC${solicitacao.dataFormatada}" name="valorQDC" value="<c:out value="${solicitacao.valorQDC}"/>"/>
				<c:out value="${solicitacao.valorQDC}"/>
			</display:column>
			
			<display:column class="QDS" sortable="false" title="QDS (m<span class='expoente'>3</span>/dia)" style="width: 19%">
				<input type="hidden" name="valorQDS" id="valorQDS${solicitacao.dataFormatada}" value="${solicitacao.valorQDS}"/>
				<c:out value="${solicitacao.valorQDS}"/>
			</display:column>
		
			<display:column class="QDP" sortable="false" style="width: 19%"
				title="QDP (m<span class='expoente'>3</span>/dia)" >
				<c:choose>
					<c:when test="${anoMesAnterior eq 'true' || solicitacao.valorQDC eq null || Long.valueOf(solicitacao.valorQDC) eq 0}">
						<input type="hidden" name="valorQDP" id="valorQDP_${indexCityGate}_${j}" 
							value="<c:out value="${solicitacao.valorQDP}"/>"/>
						<input class="campoTexto" style="text-align: right" 
							type="text" name="disabledValorQDP" 
							value="${solicitacao.valorQDP}" disabled="disabled"/>
					</c:when>
					<c:otherwise>
						<input name="valorQDP" id="valorQDP_${indexCityGate}_${j}"  class="campoTexto" 
							style="text-align: right" type="text" maxlength="17" 
							onblur="aplicarMascaraNumeroInteiro(this);" 
							value="<c:out value="${solicitacao.valorQDP}"/>" 
							onkeypress="return formatarCampoInteiro(event, 11)" />
					</c:otherwise>
				</c:choose>	
			</display:column>
			
			<display:column sortable="false" title="<span style='float: left; margin: 6px 5px 0'>Aceito </span> " style="width: 70px">
	<!-- 				<input type='checkbox' id='checkTodos' class='campoCheckbox' onclick='selecionar_tudo();' /> -->
				<input type="checkbox" id="indicadorAceiteLista_${indexCityGate}_${j}" name="indicadorAceiteLista"  
					value="${j}"  onclick="validarValorQDS('${solicitacao.valorQDS}','valorQDP_${indexCityGate}_${j}','${solicitacao.valorQDC}','indicadorAceiteLista_${indexCityGate}_${j}');"
					<c:if test="${solicitacao.indicadorAceite eq true}" > checked="checked"  </c:if>
				/>
			</display:column>
			
			<display:column class="colunaMotivo" sortable="false" title="Motivo" style="width: 90px; text-align: center">
				<input type="hidden" id="descricaoMotivo_${indexCityGate}_${j}" name="descricaoMotivoNaoAceite" 
					value="<c:out value="${solicitacao.descricaoMotivoNaoAceite}"/>"/>
				<a id="linkMotivo${j}" 
					href="javascript:exibirMotivoNaoAceite('${solicitacao.dataFormatada}','${solicitacao.valorQDC}','${solicitacao.valorQDS}','${indexCityGate}','${j}');">
					<img id="imagemMotivo${solicitacao.dataFormatada}" border="0" 
						<c:choose>
							<c:when test="${solicitacao.descricaoMotivoNaoAceite eq null || solicitacao.descricaoMotivoNaoAceite eq ''}">
								title="Informar motivo pelo qual n�o foi aceito." alt="Informar motivo pelo qual n�o foi aceito."
							</c:when>
							<c:otherwise>
								title="${solicitacao.descricaoMotivoNaoAceite}" alt="${solicitacao.descricaoMotivoNaoAceite}"
							</c:otherwise>
						</c:choose>
					src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/>
				</a>
			</display:column>
			
			<display:column class="colunaComentario" sortable="false" title="Comentario" style="width: 90px; text-align: center">
				<a style="width: 10px" 
					href="javascript:exibirComentario('<c:out value="${solicitacao.dataFormatada}"/>','<c:out value="${solicitacao.idPontoConsumo}"/>','<c:out value="${solicitacao.idModalidade}"/>');">
					<img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>" />
				</a>
			</display:column>
					
			<c:set var="j" value="${j+1}" />
		</display:table>
		
		<c:set var="indexCityGate" value="${indexCityGate + 1}" />
		
    </c:forEach>

