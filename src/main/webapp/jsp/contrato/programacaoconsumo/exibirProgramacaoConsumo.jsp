<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Manter Solicita��o de Consumo<a class="linkHelp" href="<help:help>/visualizaoeinclusodoconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para consultar Solicita��es de Consumo passadas ou futuras do ponto de consumo abaixo, selecione o m�s e o ano desejado e clique em <span class="destaqueOrientacaoInicial">Exibir</span>.
Para registrar uma nova Solicita��o de Consumo, preencha os dados referentes � data da solicita��o clique em <span class="destaqueOrientacaoInicial">Salvar</span>.</p>

<script language="javascript">
	
	<c:choose>
		<c:when test="${programacaoConsumoVO.inputModificado == true}">
			var inputModificado = true;
		</c:when>
		<c:otherwise>
			var inputModificado = false;	
		</c:otherwise>
	</c:choose>

	$(document).ready(function(){
		
		// Dialog			
		$("#motivoNaoAceite").dialog({
			autoOpen: false,
			width: 333,
			modal: true,
			resizable: false
		});
		
		$("#solicitacao input[type='text']").change(function(){
			$("#inputModificado").attr({value: true });
			inputModificado = true;
		});

		$("#listaValidacaoSolicitacoesConsumo").dialog({
			autoOpen: false, 
			width: 340, 
			modal: true, 
			minHeight: 100, 
			maxHeight: 300,
			resizable: false
		});

		verificarSaldoInicialQPNR();
		verificarCheckboxesAceite();
		
	});

	function verificarCheckboxesAceite() {
		$('[name="dataSolicitacao"]').each(function() {
			  var data = $(this).val();
			  controlarBotaoMotivo(data);
		});
	}
	
	function exibirMotivoNaoAceite(data) {
		document.getElementById("dataSolicitacaoEscolhida").value = data;
		document.getElementById("dataSolicitacaoPopup").innerHTML = document.getElementById("dataSolicitacao" + data).value;
		document.getElementById("valorQDCPopup").innerHTML = document.getElementById("valorQDC" + data).value;
		document.getElementById("valorQDSPopup").innerHTML = document.getElementById("valorQDS" + data).value;
		document.getElementById("valorQDPPopup").innerHTML = document.getElementById("valorQDP" + data).value;
		document.getElementById("descricaoMotivoPopup").value = document.getElementById("descricaoMotivo" + data).value;
	
		exibirJDialog("#motivoNaoAceite");
	}

	animatedcollapse.addDiv('dadosClienteContratoPontoConsumo', 'fade=0,speed=400,persist=1,hide=1');

	function exibir() {
		if (inputModificado == true) {
			var confirmacao = window.confirm(' Tem certeza que deseja descartar as modifica��es? ')
			if (confirmacao == true) {
				inputModificado = false;
				document.forms[0].inputModificado.value = false;
				submeter("programacaoConsumoForm", "exibirProgramacaoConsumo");
			} else { 
				return false;
			}
		} else {
			inputModificado = false;
			document.forms[0].inputModificado.value = false;
			submeter("programacaoConsumoForm", "exibirProgramacaoConsumo");
		}
	}
	
	function replicarQDS() {
		
		var diaInicial = document.getElementById("diaInicial").value;
		var diaFinal = document.getElementById("diaFinal").value;
		var repetirValorQDS = document.getElementById("repetirValorQDS").value;
		var repetirValorQDSFloat = converterStringFloat(repetirValorQDS);
		var mes = '${programacaoConsumoVO.mes}';
		var ano = '${programacaoConsumoVO.ano}';
		
		mes = mes+'';
			
		if (mes.length == 1) {
			mes = 0 + mes;
		}
		
		var complementoData = "/" + mes + "/" + ano;
		
		var total = ${fn:length(listaSolicitacaoConsumoVO)};

		var diaInicialInt = parseInt(diaInicial);
		var diaFinalInt = parseInt(diaFinal);

		if (diaInicialInt > total) {
			alert("Dia inicial deve ser menor que " + total + ".");
		} else if (diaFinalInt > total) {
			alert("O m�s selecionado s� tem " + total + " dias. Corrija o dia Final.");
		} else if (diaInicialInt > diaFinalInt) {
			alert("O dia inicial deve ser menor que o dia final.");
		} else {

			for(var i = diaInicialInt; i <= diaFinalInt; i++) {
	
				i = i+'';
				
				if (i.length == 1) {
					i = 0 + i;
				}
				
				document.getElementById("valorQDS" + i + complementoData).value = repetirValorQDS;
				
				var dataSolicitacao = i + complementoData;
				var valorQDS = document.getElementById('valorQDS'+dataSolicitacao);
				controlarCheckAceite(valorQDS,dataSolicitacao);
			}
		}
		
	}

	function preencherQDP() {
		
		var diaInicial = document.getElementById("diaInicial").value;
		var diaFinal = document.getElementById("diaFinal").value;
		var mes = '${programacaoConsumoVO.mes}';
		var ano = '${programacaoConsumoVO.ano}';

		mes = mes+'';
		
		if (mes.length == 1) {
			mes = 0 + mes;
		}
		
		var complementoData = "/" + mes + "/" + ano;
		
		var total = ${fn:length(listaSolicitacaoConsumoVO)};

		var diaInicialInt = parseInt(diaInicial);
		var diaFinalInt = parseInt(diaFinal);

		if (diaInicialInt > total) {
			alert("Dia inicial deve ser menor que " + total + ".");
		} else if (diaFinalInt > total) {
			alert("O m�s selecionado s� tem " + total + " dias. Corrija o dia Final.");
		} else if (diaInicialInt > diaFinalInt) {
			alert("O dia inicial deve ser menor que o dia final.");
		} else {
			var j = 0;
			for(var i = diaInicialInt; i <= total; i++) {
	
				i = i+'';
				
				if (i.length == 1) {
					i = 0 + i;
				}
				var dataSolicitacao = i + complementoData;

				var valorQDS = converterStringFloat(document.getElementById("valorQDS" + i + complementoData).value);
				var valorQPNR = document.getElementById("valorQPNR" + (i + complementoData)).value;
				if (valorQPNR != null && valorQPNR != "") {
					var valorQPNR = valorQPNR;
					var valorQPNRFloat = converterStringFloat(valorQPNR);
					var somaValores = parseFloat(valorQDS) + parseFloat(valorQPNRFloat);
					if (!isNaN(somaValores)) {
						var valorQDP = parseFloat(somaValores).toFixed(2);
						valorQDP = valorQDP.replace(".",",");
						if(document.getElementById("valorQDP"+ dataSolicitacao).value == ""){
							document.getElementById("valorQDP"+ dataSolicitacao).value = valorQDP;
							aplicarMascaraNumeroDecimal(document.getElementById("valorQDP"+ dataSolicitacao),2);
						}
					} else {
						document.getElementById("valorQDP"+ dataSolicitacao).value = "";
					}
				} else {
					var indicadorAceito = document.getElementById("indicadorAceite"+dataSolicitacao);
					if(indicadorAceito != null && !indicadorAceito.checked) {
						if(document.getElementById("valorQDP"+ dataSolicitacao).value == ""){
							var valorQDC = document.getElementById("valorQDC" + dataSolicitacao).value;
							document.getElementById("valorQDP" + dataSolicitacao).value = valorQDC;
						}
					} else if(indicadorAceito != null && indicadorAceito.checked) {
						valorQDS = valorQDS.replace(".",",");
						document.getElementById("valorQDP" + dataSolicitacao).value = valorQDS;
					}
					aplicarMascaraNumeroInteiro(document.getElementById("valorQDP"+ dataSolicitacao));
				}
				j++;
			}
		}
		
		
	}

	function converterStringFloat(valor) {
		if(valor != null && valor != "") {
			while(valor.indexOf(".") != -1) {
				valor = valor.replace(".","");
			}
			valor = valor.replace(",",".");
		}
		return valor;
	}

	function guardarMotivoNaoAceite() {
		var dataSolicitacao = document.getElementById("dataSolicitacaoEscolhida").value;
		var motivo = document.getElementById("descricaoMotivoPopup").value;
		document.getElementById("descricaoMotivo"+dataSolicitacao).value = motivo;
		$("#motivoNaoAceite").dialog('close');
	}
	
	function limparCampos() {
		var total = ${fn:length(listaSolicitacaoConsumoVO)};
		var mes = '${programacaoConsumoVO.mesExibido}';
		var ano = '${programacaoConsumoVO.anoExibido}';

		mes = mes+'';
			
		if (mes.length == 1) {
			mes = 0 + mes;
		}
		
		var complementoData = "/" + mes + "/" + ano;
		var j = 0;
		for(var i = 1; i <= total; i++) {
			
			i = i+'';
			
			if (i.length == 1) {
				i = 0 + i;
			}

		
			var data = i + complementoData;
			
			document.getElementById("valorQDS"+data).value = "";
			document.getElementById("valorQPNR"+data).value = "";
			document.getElementById("valorQDP"+data).value = "";
			document.getElementById("indicadorAceite"+data).checked = false;
			document.getElementById("imagemMotivo"+data).src = "<c:url value='/imagens/icone_exibir_detalhes.png'/>";
			document.getElementById("linkMotivo"+data).href = "javascript:exibirMotivoNaoAceite('"+data+"');";
			document.getElementById("descricaoMotivo"+data).value = "";
			document.getElementById("indicadorAceite"+data).checked = false;
			
			controlarBotaoMotivo(data);
			j++;
		}
		atualizarSaldoARecuperar();		
		document.getElementById("diaInicial").value = "1";
		document.getElementById("diaFinal").value = "1";
		document.getElementById("checkTodos").checked = false;
		document.getElementById("repetirValorQDS").value = "";
		document.getElementById("botaoSalvar").value = "Aplicar";

	}
	
	function calcularQDPCheck(data) {
		var valorQDS = converterStringFloat(document.getElementById('valorQDS' + data).value);
		var indicadorAceite = document.getElementById('indicadorAceite' + data);
		
		if(indicadorAceite.checked) {

			if((valorQDS != undefined) && (valorQDS != '')){
				var somaQDP = valorQDS;
				
				var valorQPNR = document.getElementById("valorQPNR" + data).value;
				
				if(valorQPNR != null && valorQPNR != "") {
					var valorQPNRFloat = converterStringFloat(valorQPNR);
					somaQDP = parseFloat(valorQDS) + parseFloat(valorQPNRFloat);
				}
				
				var valorQDP = parseFloat(somaQDP).toFixed(2);
				valorQDP = valorQDP.replace(".",",");
				document.getElementById('valorQDP'+data).value = valorQDP;
				
			}else{
				var valorQDC = document.getElementById('valorQDC'+data);
				document.getElementById('valorQDP'+data).value = valorQDC.value;
			}
			
		} else {
			
			var valorQDC = document.getElementById('valorQDC'+data);
			document.getElementById('valorQDP'+data).value = valorQDC.value;
			
		}
		
		aplicarMascaraNumeroInteiro(document.getElementById("valorQDP"+data));
		
		controlarBotaoMotivo(data);
	} 
	
	function controlarCheckAceiteQPNR(data) {
		var valorQDS = document.getElementById("valorQDS"+data).value;
		controlarCheckAceite(valorQDS,data);
	}
	
	function controlarCheckAceite(valorQDS,data) {
		var indicadorAceite = document.getElementById('indicadorAceite'+data);
		var valorQPNR = document.getElementById('valorQPNR'+data);
		if(valorQDS == "" && valorQPNR.value == "") {
			indicadorAceite.checked = false;
			//indicadorAceite.disabled = true;
		} else {
			//indicadorAceite.disabled = false;
		}
		controlarBotaoMotivo(data);
	}
	
	function controlarBotaoMotivo(data) {
		var indicadorAceite = document.getElementById("indicadorAceite"+data);
		
		if (indicadorAceite.checked == true || indicadorAceite.disabled) {
			document.getElementById("imagemMotivo"+data).src = "<c:url value='/imagens/16x_editar_pb.gif'/>";
			document.getElementById("linkMotivo"+data).removeAttribute('href');
			$('[id="valorQDP' + data + '"]').attr('readonly', true);
		} else {
			document.getElementById("imagemMotivo"+data).src = "<c:url value='/imagens/icone_exibir_detalhes.png'/>";
			document.getElementById("linkMotivo"+data).href = "javascript:exibirMotivoNaoAceite('"+data+"');";
		}
	}

	function controlarBotoesMotivo() {

		var datas = obterDatasSolicitacoesConsumo(null);

		for(i = 0; i < datas.length; i++){

			var data = datas[i];
			var indicadorCheckAll = document.getElementById("checkTodos");
			var indicadorAceite = document.getElementById("indicadorAceite"+data);
			var valorQDS = document.getElementById("valorQDS"+data);
			var valorQDP = document.getElementById("valorQDP"+data);
			var valorQDC = document.getElementById("valorQDC"+data);

			if(indicadorCheckAll.checked) {

				if(valorQDS != null && valorQDS.value != "") {

					document.getElementById("imagemMotivo"+data).src = "<c:url value='/imagens/16x_editar_pb.gif'/>";
					document.getElementById("linkMotivo"+data).removeAttribute('href');

				}
				
			} else {

				if (valorQDC.value != null && valorQDC.value != 0 && valorQDS != null && valorQDS.value != "") {

					document.getElementById("imagemMotivo"+data).src = "<c:url value='/imagens/icone_exibir_detalhes.png'/>";
					document.getElementById("linkMotivo"+data).href = "javascript:exibirMotivoNaoAceite('"+data+"');";

				}
				
			}		

		}

	}

	function aplicar(elem) {
		if(elem.value == "Aplicar") {
			elem.value = "Salvar";
			inputModificado = true;
			validarSolicitacoesConsumo(null);
		} else {
			document.forms[0].saldoARecuperarAux.value = document.getElementById("saldoARecuperarHidden").value;
			submeter("programacaoConsumoForm", "manterProgramacaoConsumo");
		}
	}
	
	function init () {
		verificarAnoMes();
	}
	
	function verificarAnoMes() {
		var anoMesAnterior = "<c:out value='${anoMesAnterior}'/>";
		if (anoMesAnterior == 'true') {
			document.getElementById("checkTodos").disabled="disabled";
			document.getElementById("botaoLimpar").disabled="disabled";
			document.getElementById("diaInicial").disabled="disabled";
			document.getElementById("diaFinal").disabled="disabled";
			document.getElementById("repetirValorQDS").disabled="disabled";
			document.getElementById("botaoQDS").disabled="disabled";
		}
	}

	function validarValorSaldoARecuperar(estimativa, id){
		var totalEstimativas = somarEstimativas();
		if (estimativa != undefined && estimativa.value != ''){
			
			var valorEstimativa = parseFloat(converterStringFloat(estimativa.value));
			var saldoARecuperar = parseFloat(document.getElementById("saldoARecuperarHidden").value) + valorEstimativa - totalEstimativas;
			if (saldoARecuperar < valorEstimativa){
				$("#"+id).css("color","red");
				alert('O volume informado de estimativa de volume a recuperar ultrapassa o volume do saldo a recuperar.');
			}else{
				saldoARecuperar = saldoARecuperar - valorEstimativa;
				//document.getElementById("saldoARecuperar").value = saldoARecuperar; 
				$("#"+id).css("color","black");
			}
		}
		
		atualizarSaldoARecuperar(totalEstimativas);
	}

	function somarEstimativas(){
		var listaEstimativas = document.getElementsByName("valorQPNR");
		var totalEstimativa = parseFloat(0);
		for (var i = 0; i < listaEstimativas.length; i++) {
			if (listaEstimativas[i].value != undefined && listaEstimativas[i].value != ''){
				var valor = listaEstimativas[i].value.replace(".","");
				totalEstimativa = totalEstimativa + parseFloat(valor);
			}  
		}

		return totalEstimativa;
	}

	function atualizarSaldoARecuperar(){
		var totalEstimativas = somarEstimativas();
		var saldo = parseFloat(document.getElementById("saldoARecuperarHidden").value);
		var campo = document.getElementById("saldoARecuperar"); 
		campo.value = (saldo - totalEstimativas);
		campo.value = campo.value.replace(".",",");
		aplicarMascaraNumeroDecimal(campo,4);
		
		if (saldo >= totalEstimativas){
			var listaEstimativas = document.getElementsByName("valorQPNR");
			for (var i = 0; i < listaEstimativas.length; i++) {
				if (listaEstimativas[i].value != undefined && listaEstimativas[i].value != ''){
					$("#"+listaEstimativas[i].id).css("color","black");
				}
			}
			
		}
	}

	function verificarSaldoInicialQPNR() {

		var listaValorQPNR = document.getElementsByName("valorQPNR");
			
		if (listaValorQPNR != undefined){
			var totalEstimativas = parseFloat(0);
			for (var i = 0; i < listaValorQPNR.length; i++) {
				if (listaValorQPNR[i].value != null && listaValorQPNR[i].value != ''){
					//validarValorSaldoInicialARecuperar(listaValorQPNR[i], listaValorQPNR[i].id);

					var valorEstimativa = parseFloat(converterStringFloat(listaValorQPNR[i].value));
					totalEstimativas = totalEstimativas + valorEstimativa;
					var saldoARecuperar = parseFloat(document.getElementById("saldoARecuperarHidden").value) + valorEstimativa - totalEstimativas;
					if (saldoARecuperar < valorEstimativa){
						$("#"+listaValorQPNR[i].id).css("color","red");
						//alert('O volume informado de estimativa de volume a recuperar ultrapassa o volume do saldo a recuperar.');
						//estimativaVolumeUltrapassaVolumeSaldo = true;
					}else{
						saldoARecuperar = saldoARecuperar - valorEstimativa;
						//document.getElementById("saldoARecuperar").value = saldoARecuperar; 
						$("#"+listaValorQPNR[i].id).css("color","black");
					}
					
				}				
			}
						
			atualizarSaldoARecuperar(totalEstimativas);
		}		
	}

	function configurarChecks(){
		
		var datas = obterDatasSolicitacoesConsumo(null);
		var checkTodos = document.getElementById("checkTodos");

		for(i = 0; i < datas.length; i++){
			var indicadorAceite = document.getElementById("indicadorAceite"+datas[i]);
			
			if(indicadorAceite != undefined && !indicadorAceite.disabled){
				indicadorAceite.checked = checkTodos.checked;
			}			
		}
		
	}

	function validarSolicitacoesConsumo(data){
		
		var idPontoConsumo = document.forms[0].idPontoConsumo.value;
		var datas = obterDatasSolicitacoesConsumo(data);
		var mapa = new Object();
		var exibirMsg = false;

		for(i = 0; i < datas.length; i++){
			var key = datas[i];
			var dados = obterDadosSolicitacao(key);	

			if(dados != null){
				mapa[key] = dados;
			}
		}

		for(i = 0; i < datas.length; i++){
			var indicadorAceite = document.getElementById("indicadorAceite"+datas[i]);
			if (indicadorAceite.checked == false) {
				document.getElementById("imagemMotivo"+datas[i]).src = "<c:url value='/imagens/icone_exibir_detalhes.png'/>";
				document.getElementById("linkMotivo"+datas[i]).href = "javascript:exibirMotivoNaoAceite('"+datas[i]+"');";
				
				var valorQdc = $('[id="valorQDC' + datas[i] + '"]').val();
				$('[id="valorQDP' + datas[i] + '"]').removeAttr('readonly');
				$('[id="valorQDP' + datas[i] + '"]').val(valorQdc);
				
			} else {
				document.getElementById("imagemMotivo"+datas[i]).src = "<c:url value='/imagens/16x_editar_pb.gif'/>";
				document.getElementById("linkMotivo"+datas[i]).removeAttribute('href');
				
				var valorQds = $('[id="valorQDS' + datas[i] + '"]').val();
				$('[id="valorQDP' + datas[i] + '"]').val(valorQds).attr('readonly', true);
				
			};	
		}		

		if(exibirMsg){
			exibirJDialog("#listaValidacaoSolicitacoesConsumo");
			$("#listaValidacaoSolicitacoesConsumo").dialog({
				autoOpen: false, 
				width: 500, 
				modal: true, 
				minHeight: 100, 
				maxHeight: 300,
				resizable: false
			});
		}
		
	}

	var div = 2;
	var start = 2;

	function limparMensagens(){

		$('#itensSolicitacaoConsumo').html('');
		div = 2;
		start = 2;

	}
	
	function adcionarMensagem(key, mensagem){

		var inner = '';
   		var param = '';

   		if(start % div == 0){
			param = "odd";
        }else{
			param = "even";
        }
		
   		inner = inner + '<tr class='+param+'>';		
		inner = inner + '<td>' + key + '</td>';
		inner = inner + '<td>' + mensagem + '</td>';
		inner = inner + '</tr>';
   		start = start + 1;

   		$("#itensSolicitacaoConsumo").prepend(inner);
		
	}

	function setarDadosSolicitacao(key, dados){
		var qdc = document.getElementById("valorQDC"+key);
		var qds = document.getElementById("valorQDS"+key);
		var qpnr = document.getElementById("valorQPNR"+key);
		var qdp = document.getElementById("valorQDP"+key);	

		qdc.value = dados[key]['qdc'];
		qds.value = dados[key]['qds'];
		qpnr.value = dados[key]['qpnr'];
		qdp.value = dados[key]['qdp'];

	}
	
	function obterDadosSolicitacao(key){
		var mapa = null;
		var indicadorAceite = document.getElementById("indicadorAceite"+key);

		if(indicadorAceite != undefined && !indicadorAceite.disabled){

			mapa = new Object();
			var qdc = document.getElementById("valorQDC"+key);
			var qds = document.getElementById("valorQDS"+key);
			var qpnr = document.getElementById("valorQPNR"+key);
			var qdp = document.getElementById("valorQDP"+key);
			

			if(qdc != undefined){
				mapa.qdc = qdc.value;
			}else{
				mapa.qdc = '';
			}

			if(qds != undefined){
				mapa.qds = qds.value;
			}else{
				mapa.qds = '';
			}
			
			if(qpnr != undefined){
				mapa.qpnr = qpnr.value;
			}else{
				mapa.qpnr = '';
			}

			if(qdp != undefined){
				mapa.qdp = qdp.value;
			}else{
				mapa.qdp = '';
			}

			mapa.indicadorAceite = (indicadorAceite.checked);

		}		

		return mapa;
	}

	function obterDatasSolicitacoesConsumo(data){
		
		var datas = new Array();
		
		if(data != null && data != ''){

			datas.push(data)

		}else{

			<c:forEach items="${listaSolicitacaoConsumoVO}" var="solicitacaoConsumoVO">
				datas.push('<c:out value="${solicitacaoConsumoVO.dataFormatada}"/>');
			</c:forEach>

		}
		
		return datas;		
	}

	function cancelar() {
		submeter("programacaoConsumoForm", "cancelarManterSolicitacaoConsumo");		
	}
	
    function exibirComentario(dataFormatada){
        var popupComentario;
       
        document.getElementById("dataSolicitacaoComentario").value = dataFormatada;
     
        //document.forms[0].dataFormatada.value = dataFormatada;
     popupComentario=   window.open('exibirComentarioSolicitacaoConsumo?postBack=false','popupComentario','height=600,width=980,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0 ,modal=1');
  //      if (window.focus) {popupComentario.focus()}
   
       submeter("programacaoConsumoForm", "exibirComentarioSolicitacaoConsumo", "popupComentario");
      
    }
	
	
	addLoadEvent(init);
	
	
	

</script>

<div id="listaValidacaoSolicitacoesConsumo" title="Valida��es Solicita��o de Consumo" style="width: 735px">
	<table class="dataTableGGAS dataTableDialog">
		<thead>
			<tr>
				<th style="width: 25%">Data</th>
				<th>Mensagem</th>
			</tr>
		</thead>
		<tbody id="itensSolicitacaoConsumo"></tbody>
	</table>
	<hr class="linhaSeparadoraPopup" />
</div>

<form:form method="post" action="manterProgramacaoConsumo" name="programacaoConsumoForm">

	<input name="postBack" type="hidden" id="postBack" value="${postBack}"/>
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${programacaoConsumoVO.idPontoConsumo}"/>
	<input name="acao" type="hidden" id="acao" value="manterProgramacaoConsumo"/>
	<input name="inputModificado" type="hidden" id="inputModificado" value="${programacaoConsumoVO.inputModificado}"/>
	<input name="indicadorRetiradaAutomatica" type="hidden" id="indicadorRetiradaAutomatica" value="${indicadorRetiradaAutomatica}"/>
	<input name="anoExibido" type="hidden" id="anoExibido" value="${programacaoConsumoVO.anoExibido}"/>
	<input name="mesExibido" type="hidden" id="mesExibido" value="${programacaoConsumoVO.mesExibido}"/>
	<input name="saldoARecuperarHidden" type="hidden" id="saldoARecuperarHidden" value="${programacaoConsumoVO.saldoARecuperarAux}"/>
	<input name="saldoARecuperarAux" type="hidden" id="saldoARecuperarAux" value="${programacaoConsumoVO.saldoARecuperarAux}"/>
	<input name="chaveModalidadeExibir" type="hidden" id="chaveModalidadeExibir"  value="${programacaoConsumoVO.chaveModalidade}" />
	
	<input name="dataSolicitacaoComentario" type="hidden" id="dataSolicitacaoComentario"   />
	
	<div id="motivoNaoAceite" title="Motivo do N�o Aceite">
		<input type="hidden" id="dataSolicitacaoEscolhida" value=""/>
		<a class="linkHelp" href="<help:help>/motivodonoaceite.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		<label class="rotulo rotulo2Linhas">Data de Solicita��o:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas" id="dataSolicitacaoPopup"></span><br />
		<label class="rotulo">QDC:</label>
		<span class="itemDetalhamento" id="valorQDCPopup"></span><br />
		<label class="rotulo">QDS:</label>
		<span class="itemDetalhamento" id="valorQDSPopup"></span><br />
		<label class="rotulo">QDP:</label>
		<span class="itemDetalhamento" id="valorQDPPopup"></span><br class="quebraLinha2">
		<label class="rotulo rotuloVertical">Motivo:</label>
		<textarea id="descricaoMotivoPopup" rows="6" onkeypress="return formatarCampoTextoComLimite(event,this,200);"></textarea>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol2" value="Confirmar" type="button" onclick="guardarMotivoNaoAceite()">
	</div>
	
	<fieldset id="manterSolicitacaoConsumoSelecao" class="conteinerPesquisarIncluirAlterar">	    
		<fieldset class="conteinerBloco">
			<label class="rotulo">Ponto de Consumo:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${programacaoConsumoVO.descricaoPontoConsumo}"/></span>
		</fieldset>
		<fieldset class="conteinerBloco2">
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosClienteContratoPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">
			Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			<div id="dadosClienteContratoPontoConsumo" class="conteinerDados">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Contrato:</label>
					<span class="itemDetalhamento"><c:out value="${programacaoConsumoVO.numeroContrato}"></c:out></span>
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Im�vel:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${programacaoConsumoVO.nomeImovel}"/></span>
				</fieldset>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco">
		
	    	<label class="rotulo rotuloHorizontal" id="rotuloAno" for="ano">Ano:</label>
			<select tabindex="2" name="ano" id="ano" class="campoSelect campoHorizontal">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaAnos}" var="ano">
		    		<option value="<c:out value="${ano}"/>" <c:if test="${programacaoConsumoVO.ano == ano}">selected="selected"</c:if> >
		    			<c:out value="${ano}"/>
		    		</option>
		    	</c:forEach>
		    </select>
		    
		    <label class="rotulo rotuloHorizontal" id="rotuloMes" for="mes">M�s:</label>
			<select tabindex="3" name="mes" id="mes" class="campoSelect campoHorizontal">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaMeses}" var="mesAno">
		    		<option value="<c:out value="${mesAno.key}"/>" <c:if test="${programacaoConsumoVO.mes == mesAno.key}">selected="selected"</c:if> >
		    			<c:out value="${mesAno.value}"/>
		    		</option>
		    	</c:forEach>
		    </select>	
		</fieldset>
		
		
		<fieldset class="conteinerBloco">
				<legend>Modalidades</legend>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas"
				name="listaContratoPontoConsumoModalidade"  sort="list" id="modalidade" 
				excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			        
			    <display:column sortable="false" style="width: 75px" > 
			    
			    <c:choose>
				<c:when test="${modalidade.chavePrimaria eq programacaoConsumoVO.chaveModalidade }">
			     			<input type="radio" name="chaveModalidade" id="chaveModalidade" 
			     				checked = "true" value="<c:out value="${modalidade.chavePrimaria}"/>" />
			   </c:when>
		   				<c:when test="${modalidade.chavePrimaria ne programacaoConsumoVO.chaveModalidade
		   						&& programacaoConsumoVO.chaveModalidade ne 0 }">
			    			<input type="radio" name="chaveModalidade" id="chaveModalidade" 
			    				value="<c:out value="${modalidade.chavePrimaria}"/>"        />
			   </c:when>
			
			   <c:otherwise>
			   				<input type="radio" name="chaveModalidade" id="chaveModalidade" checked = "true" 
			   					value="<c:out value="${modalidade.chavePrimaria}"/>"        />
		    	</c:otherwise>
		    	</c:choose>
		    	</display:column>
			  
			  
			  	<display:column  sortable="false" title="Descricao" style="width: 85%; text-align: left; padding-right: 5px">
				
				
					<c:out value="${modalidade.contratoModalidade.descricao}"/>
				
				
			</display:column>
			  
		    		 
		    	<
			  
<%-- 			    <display:column  title="Descri��o" headerClass="tituloTabelaEsq" sortProperty="modalidade.contratoModalidade.descricao" property="modalidade.contratoModalidade.descricao"  style="text-align: left; padding-left: 10px" /> --%>
	
			    </display:table>
			</fieldset>
			
		
		<fieldset id="conteinerBotoesExibirProgramacaoConsumo" class="conteinerBotoesPesquisarDirFixo">
			<input name="Button" id="botaoPesquisar" class="bottonRightCol2" value="Exibir" type="button" onclick="exibir()">
			<input name="Button" id="botaoLimpar" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCampos();" >
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraPesquisa" />
	
	<fieldset id="manterSolicitacaoConsumo" class="conteinerPesquisarIncluirAlterar">

		<p class="orientacaoInicial">Para repetir um mesmo valor do QDS para um intervalo de datas, selecione o <span class="destaqueOrientacaoInicial">Dia Inicial</span>, o <span class="destaqueOrientacaoInicial">Dia Final</span>, o <span class="destaqueOrientacaoInicial">Valor QDS</span> que ser� repetido e clique em <span class="destaqueOrientacaoInicial">Repetir Valor QDS</span>.</p>
		
		
		<fieldset id="conteinerRepeticaoValoresQDS">
			<legend>Repetir Valores do QDS:</legend>
			<div class="conteinerDados">
				<label class="rotulo">Dia Inicial:</label>
				<select id="diaInicial" name="diaInicial" class="campoHorizontal">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
				</select>
				
				<label class="rotulo rotuloHorizontal">Dia Final:</label>
				<select id="diaFinal" name="diaFinal" class="campoHorizontal">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
				</select>
				
				<label class="rotulo rotuloHorizontal">Valor QDS:</label>
				<input id="repetirValorQDS" name="repetirValorQDS" class="campoTexto campoHorizontal" 
					type="text" maxlength="17" size="6" onkeypress="return formatarCampoInteiro(event, 11)" 
					onblur="aplicarMascaraNumeroInteiro(this);" value="" />
				
				<input id="botaoQDS" class="botaoInline" name="button" value="Repetir Valor QDS" 
					title="Repetir o valor do primeiro dia para todos os outros" 
					type="button" onclick="replicarQDS()" />
			</div>
		</fieldset>
		
		<fieldset id="conteinerSaldoARecuperar">
			<legend>Saldo a recuperar:</legend>
			<div class="conteinerDados">
				<input id="saldoARecuperar" name="saldoARecuperar" class="campoTexto campoHorizontal" type="text" readonly="readonly" size="12" value="<c:out value="${programacaoConsumoVO.saldoARecuperarAux}"/>" />
			</div>
		</fieldset>
				
		<c:set var="j" value="0" />
		<c:set var="nenhumQDCEncontrado" value="true"/>
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaSolicitacaoConsumoVO" sort="list" id="solicitacao" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			<display:column sortable="false" title="Data" style="width: 75px">
				<input type="hidden" id="dataSolicitacao${solicitacao.dataFormatada}" name="dataSolicitacao" value="<c:out value="${solicitacao.dataFormatada}"/>"/>
				<c:out value="${solicitacao.dataFormatada}"/>
			</display:column>
			<display:column class="QDC" sortable="false" title="QDC (m<span class='expoente'>3</span>/dia)" style="width: 19%; text-align: right; padding-right: 5px">
				<input type="hidden" id="valorQDC${solicitacao.dataFormatada}" name="valorQDC" value="<c:out value="${solicitacao.valorQDC}"/>"/>
				<c:if test="${solicitacao.valorQDC ne '0'}">
					<c:out value="${solicitacao.valorQDC}"/>
				</c:if>
				<c:if test="${nenhumQDCEncontrado eq true && (solicitacao.valorQDC ne '0' && solicitacao.valorQDC ne null)}">
					<c:set var="nenhumQDCEncontrado" value="false" />
				</c:if>
			</display:column>
			
			<display:column class="QDS" sortable="false" title="QDS (m<span class='expoente'>3</span>/dia)" style="width: 19%">
				<c:choose>
					<c:when test="${anoMesAnterior eq 'true' || solicitacao.valorQDC eq null || solicitacao.valorQDC eq '0'}">
						<input type="hidden" name="valorQDS" id="valorQDS${solicitacao.dataFormatada}" 
							value="${solicitacao.valorQDS}"/>
						<input class="campoTexto" style="text-align: right" 
							type="text" name="disabledValorQDS" 
							value="${solicitacao.valorQDS}" readonly="readonly"/>	
					</c:when>
					<c:otherwise>
						<input name="valorQDS"  readonly="readonly" 
							id="valorQDS${solicitacao.dataFormatada}" class="campoTexto" 
							style="text-align: right" type="text" maxlength="17" 
							onkeypress="return formatarCampoDecimal(event, this, 11, 4)" 
							onblur="aplicarMascaraNumeroInteiro(this);" 
							value="<c:out value="${solicitacao.valorQDS}"/>" 
							onchange="controlarCheckAceite(this.value,'${solicitacao.dataFormatada}');" />
					</c:otherwise>
				</c:choose>	
			</display:column>
			<display:column class="QPNR" sortable="false" title="Estimativa de Quantidade a Recuperar" style="width: 19%">
				<c:choose>
					<c:when test="${anoMesAnterior eq 'true' || solicitacao.valorQDC eq null || solicitacao.valorQDC eq '0' || indicadorRetiradaAutomatica}">
						<input type="hidden" name="valorQPNR" id="valorQPNR${solicitacao.dataFormatada}" value="<c:out value="${solicitacao.valorQPNR}"/>"/>
						<input class="campoTexto" style="text-align: right" type="text" name="disabledValorQPNR" value="${solicitacao.valorQPNR}" disabled="disabled"/>	
					</c:when>
					<c:otherwise>
						<input name="valorQPNR" id="valorQPNR${solicitacao.dataFormatada}" 
							class="campoTexto" style="text-align: right" type="text" maxlength="17"  
							onkeypress="return formatarCampoInteiro(event, 11)" onblur="aplicarMascaraNumeroInteiro(this); " 
							value="<c:out value="${solicitacao.valorQPNR}"/>" 
							onchange="controlarCheckAceiteQPNR('${solicitacao.dataFormatada}'); 
								validarValorSaldoARecuperar(this, 'valorQPNR${solicitacao.dataFormatada}');" />
					</c:otherwise>
				</c:choose>	
			</display:column>
			<display:column class="QDP" sortable="false" title="QDP (m<span class='expoente'>3</span>/dia)" style="width: 19%">
				<c:choose>
					<c:when test="${anoMesAnterior eq 'true' || solicitacao.valorQDC eq null || solicitacao.valorQDC eq '0'}">
						<input type="hidden" name="valorQDP" id="valorQDP${solicitacao.dataFormatada}" 
							value="<c:out value="${solicitacao.valorQDP}"/>"/>
						<input class="campoTexto" style="text-align: right" type="text" 
							name="disabledValorQDP" value="${solicitacao.valorQDP}" disabled="disabled"/>	
					</c:when>
					<c:otherwise>
						<input name="valorQDP" id="valorQDP${solicitacao.dataFormatada}" 
							class="campoTexto" style="text-align: right" type="text" 
							maxlength="17" onkeypress="return formatarCampoInteiro(event, 11)" 
							onblur="aplicarMascaraNumeroInteiro(this);" 
							value="<c:out value="${solicitacao.valorQDP}"/>" />
					</c:otherwise>
				</c:choose>	
			</display:column>
			<display:column sortable="false" title="<span style='float: left; margin: 6px 5px 0'>Aceito </span>
				<input type='checkbox' id='checkTodos' class='campoCheckbox' onclick='configurarChecks();controlarBotoesMotivo();validarSolicitacoesConsumo(null);' />" style="width: 70px">
				<c:choose>
					<c:when test="${anoMesAnterior eq 'true' || solicitacao.valorQDC eq null || solicitacao.valorQDC eq '0'}">
						<input type="hidden" id="indicadorAceite${solicitacao.dataFormatada}" name="indicadorAceite${solicitacao.dataFormatada}" />
						<input type="checkbox" id="disabledIndicadorAceite" name="disabledIndicadorAceite" value="true" disabled="disabled"	<c:if test="${solicitacao.indicadorAceite eq true}" > checked="checked" </c:if>	/>
					</c:when>
					<c:otherwise>
						<input type="checkbox" id="indicadorAceite${solicitacao.dataFormatada}" name="indicadorAceite${solicitacao.dataFormatada}" value="true" 
							<c:if test="${solicitacao.indicadorAceite eq true}" > checked="checked" </c:if>
							onclick="validarSolicitacoesConsumo('${solicitacao.dataFormatada}');"  />
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column class="colunaMotivo" sortable="false" title="Motivo" style="width: 90px; text-align: center">
				<input type="hidden" id="descricaoMotivo${solicitacao.dataFormatada}" name="descricaoMotivoNaoAceite" value="<c:out value="${solicitacao.descricaoMotivoNaoAceite}"/>"/>
					<c:choose>
						<c:when test="${anoMesAnterior || solicitacao.valorQDC eq null || solicitacao.valorQDC eq '0' || solicitacao.indicadorAceite eq true || solicitacao.indicadorAceite eq false }">
							<a id="linkMotivo${solicitacao.dataFormatada}"><img id="imagemMotivo${solicitacao.dataFormatada}" border="0" src="<c:url value="/imagens/16x_editar_pb.gif"/>"/></a>
						</c:when>
						<c:otherwise>
							<a id="linkMotivo${solicitacao.dataFormatada}" href="javascript:exibirMotivoNaoAceite('${solicitacao.dataFormatada}');"><img id="imagemMotivo${solicitacao.dataFormatada}" border="0" 
							<c:choose>
								<c:when test="${solicitacao.descricaoMotivoNaoAceite eq null || solicitacao.descricaoMotivoNaoAceite eq ''}">
									title="Informar motivo pelo qual n�o foi aceito." alt="Informar motivo pelo qual n�o foi aceito."
								</c:when>
								<c:otherwise>
									title="${solicitacao.descricaoMotivoNaoAceite}" alt="${solicitacao.descricaoMotivoNaoAceite}"
								</c:otherwise>
							</c:choose>
							src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>
						</c:otherwise>
					</c:choose> 
			</display:column>
			
				<display:column class="colunaComentario" sortable="false" title="Comentario" style="width: 90px; text-align: center">
					<input type="hidden" id="indicadorTipoProgramacaoConsumo${solicitacao.dataFormatada}" name="indicadorTipoProgramacaoConsumo${solicitacao.dataFormatada}" value="<c:out value="${solicitacao.indicadorTipoProgramacaoConsumo}"/>"  />
						<a style="width: 10px"
							href="javascript:exibirComentario('<c:out value="${solicitacao.dataFormatada}"/>');"><img
							border="0"
							src="<c:url value="/imagens/icone_exibir_detalhes.png"/>" /></a>
					</display:column>
			
			<c:set var="j" value="${j+1}" />
	   	</display:table>

	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">	
	    <input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="aplicar(this);" <c:if test="${anoMesAnterior eq true || nenhumQDCEncontrado eq true}">  disabled="disabled" </c:if> >
	</fieldset>
	
</form:form>