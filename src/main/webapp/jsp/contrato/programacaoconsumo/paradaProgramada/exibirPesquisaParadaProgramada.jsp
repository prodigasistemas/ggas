<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script language="javascript">

	function manterParadasProgramadas() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('paradaProgramadaForm', 'exibirManutencaoParadaProgramada');
	    }
	}
	
	function limparFormulario() {
		limparFormularioDadosCliente();
		document.forms[0].numeroContrato.value = "";
	}

	function selecionarClienteParadaProgramada(idSelecionado) {
		var idCliente = document.getElementById("idCliente");
		if (idSelecionado != '' && idCliente != undefined) {
			idCliente.value = idSelecionado;
			submeter('paradaProgramadaForm', 'pesquisarPontosConsumoParadaProgramada');	
		}	
	}

	function retirarContrato(idSelecionado) {
		var idContrato = document.getElementById("idContrato");
		if (idSelecionado != '' && idContrato != undefined) {
			idContrato.value = idSelecionado;
			submeter('paradaProgramadaForm', 'removerClientePesquisaParadaProgramada');	
		}	
	}

</script>
<h1 class="tituloInterno"><c:out value="${tipoParadaDescricao}"></c:out> - Pesquisar Ponto de Consumo<a class="linkHelp" href="<help:help>/consultandopontodeconsumoparainclusodeparada.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<c:if test="${programadaVO.idTipoParada ne 74}">
	<p class="orientacaoInicial">Para manter as futuras Paradas, encontre o(s) Ponto(s) de Consumo de determinado cliente clicando em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span> para selecion�-lo ou preencha o <span class="destaqueOrientacaoInicial">N�mero do Contrato</span> referentes a estes Pontos de Consumo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />Selecione um dos Pontos de Consumo listados e clique em <span class="destaqueOrientacaoInicial">Manter</span>.</p>
</c:if>
<c:if test="${programadaVO.idTipoParada eq 74}">
	<span class="orientacaoInicial">Para manter as Falhas de Fornecimento, encontre o(s) Ponto(s) de Consumo de determinado cliente clicando em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span> para selecion�-lo ou preencha o <span class="destaqueOrientacaoInicial">N�mero do Contrato</span> referentes a estes Pontos de Consumo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />Selecione um dos Pontos de Consumo listados e clique em <span class="destaqueOrientacaoInicial">Manter</span>.</span>
</c:if>

<form:form method="post" action="pesquisarPontosConsumoParadaProgramada" name="paradaProgramadaForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarPontosConsumoParadaProgramada">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="false">
	<input name="idTipoParada" type="hidden" id="idTipoParada" value="${programadaVO.idTipoParada}">
	<input name="idContrato" type="hidden" id="idContrato" value="${programadaVO.idContrato}">
	
	<fieldset id="solicitacaoConsumo" class="conteinerPesquisarIncluirAlterar">
	   <fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.chavePrimaria}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nome}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroCpfCnpj}" />
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoLogradouro}"/>
				<jsp:param name="funcionalidade" value="paradaProgramada"/>									
			</jsp:include>
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:</label>
			<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato"  maxlength="10" size="10" value="${programadaVO.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"><br />
		</fieldset>
				
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarPontosConsumoParadaProgramada">
				<input name="Button" id="botaoPesquisar" class="bottonRightCol2" type="submit" value="Pesquisar">
			</vacess:vacess>
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" type="button" value="Limpar" onclick="limparFormulario();">			
		</fieldset>
	</fieldset>
	
	<c:if test="${listaContratos ne null}">
		<fieldset id="solicitacaoConsumo" class="conteinerPesquisarIncluirAlterar">	
		<c:if test="${empty listaContratos}">
			<hr class="linhaSeparadora" />
			<display:table class="dataTableGGAS" list="${listaContratos}" sort="list" id="contratoPontoConsumo" pagesize="15" excludedParams="" requestURI="#" >
				<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox'>" />
				<display:column sortable="false" title="Ponto de Consumo" style="width: 320px; text-align:left; padding-left: 5px" headerClass="tituloTabelaEsq" />
				<display:column sortable="false" title="Ramo de Atividade" style="width: 150px" />
				<display:column sortable="false" title="Segmento" style="width: 90px" />
			</display:table>
		</c:if>
			<c:forEach items="${listaContratos}" var="contrato">
				<input name="chavesContratos" type="hidden" id="chavesContratos" value="${contrato.chavePrimaria}"></input>
				<input name="anoContratos" type="hidden" id="anoContratos" value="${contrato.anoContrato}"></input>
				<input name="numeroContratos" type="hidden" id="numeroContratos" value="${contrato.numero}"></input>
				<hr class="linhaSeparadora" />
				<label class="rotulo" id="rotuloContrato">Contrato:</label>
				<c:if test="${contrato.chavePrimaria ne '0'}">
					<c:set var="existeElemento" value="true" />
					<span class="itemDetalhamento"><c:out value="${contrato.numeroFormatado}"></c:out></span>				
				</c:if>
				
				<a class="apagarItemPesquisa" onclick="return confirm('Deseja retirar o contrato?');" href="javascript:retirarContrato('${contrato.chavePrimaria}');" title="Retirar Contrato"><img src="<c:url value="/imagens/deletar_x.png"/>" /></a>
				<display:table class="dataTableGGAS" list="${contrato.listaContratoPontoConsumo}" sort="list" id="contratoPontoConsumo" pagesize="15" excludedParams="" requestURI="#" >
					
					<display:column style="text-align: center; width: 25px" sortable="false">
			      		<input type="radio" name="chavesPrimarias" value="${contratoPontoConsumo.pontoConsumo.chavePrimaria}">
			      		<!-- campo hidden para guardar o numero do contrato para cada ponto de consumo -->
			      		<input type="hidden" name="contratoChavesPonto" value="${contratoPontoConsumo.contrato.numero}">
			      		<input type="hidden" name="chavesContratoAditivo" value="${contratoPontoConsumo.contrato.numeroContratoComAditivo}">
			     	</display:column>

				<display:column title="Ponto de Consumo"
					headerClass="tituloTabelaEsq" style="text-align: left">
					<c:out
						value="${contratoPontoConsumo.pontoConsumo.imovel.nome} - ${contratoPontoConsumo.pontoConsumo.descricao} " />
				</display:column>

					<display:column sortable="false" property="pontoConsumo.ramoAtividade.descricao" title="Ramo de Atividade" style="width: 150px" />
					<display:column sortable="false" property="pontoConsumo.segmento.descricao" title="Segmento" style="width: 90px" />
			   	</display:table>
		   	</c:forEach>
		</fieldset>
	</c:if>	

	<fieldset class="conteinerBotoes">
		<c:if test="${existeElemento}">
	    	<input id="buttonManter" name="button" class="bottonRightCol2 botaoGrande1" value="Manter" type="button" onclick="javascript:manterParadasProgramadas();">
	    </c:if>
	</fieldset>

</form:form>