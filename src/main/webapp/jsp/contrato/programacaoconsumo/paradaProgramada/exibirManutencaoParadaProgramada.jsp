<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/DataParadaProgramadaPrototype.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick-pt-BR.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>

<h1 class="tituloInterno">
	<c:if test="${programadaVO.idTipoParada ne 74}">Manter </c:if>
	<c:out value="${tipoParadaDescricao}" />
	<c:if test="${programadaVO.idTipoParada eq 71}">
		<a class="linkHelp" href="<help:help>/paradaprogramada.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${programadaVO.idTipoParada eq 73}">
		<a class="linkHelp" href="<help:help>/paradacasofortuitoouforamaior.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${programadaVO.idTipoParada eq 74}">
		<a class="linkHelp" href="<help:help>/falhadefornecimentogsforadeespecificao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${programadaVO.idTipoParada eq 72}">
		<a class="linkHelp" href="<help:help>/paradanoprogramada.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	</h1>
<c:if test="${programadaVO.idTipoParada ne 74}">
	<p class="orientacaoInicial">Para incluir novas paradas ou remover paradas j� existentes, marque ou desmarque as datas desejadas e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>
</c:if>
<c:if test="${programadaVO.idTipoParada eq 74}">
	<p class="orientacaoInicial">Para incluir ou remover falhas de fornecimento, marque ou desmarque as datas desejadas e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>
</c:if>



<script language="javascript">
	
	var datasProgramadas = new Array();

	<c:set var="j" value="0" />
	<c:forEach items="${pontosConsumo}" var="pontos">
	<c:set var="j" value="${j+1}" />
	</c:forEach>
	<c:if test="${j==1}">
	var date;
	<c:set var="i" value="0" />
	<c:forEach items="${paradasProgramadas}" var="parada">
	 date = new Date('${parada.ano}','${parada.mes}','${parada.dia}');
	 date.indicadorSolicitante = '${parada.nomeSolicitante}';
	 date.volumeFornecimento = '${parada.volumeFornecimento}';
	 date.dataAviso = '${parada.dataAviso}';
	 date.comentario = '${parada.comentario}';
	 datasProgramadas[${i}] = date;
	 <c:set var="i" value="${i+1}" />
     </c:forEach>
    </c:if>	
	
	<c:if test="${not empty programadaVO.datasProgramadas}">
    var datasProgramadasForm = '${programadaVO.datasProgramadas}';
    var arrDatasProgramadasForm = datasProgramadasForm.split(";");
    var arrData;

    </c:if>
    	
	
	$(function() {
		
		$(".campoData").datepicker({
			changeYear : true,
			showOn : 'button',
			buttonImage : '<c:url value="/imagens/calendario.gif"/>',
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy'
		});
	
		$('#dataParadaProgramadaCliente1').datepick({
			multiSelect: 999, numberOfMonths: [2,6], 
			multiSeparator: ';',
    		dateFormat: 'dd/mm/yy',
    		defaultDate: new Date ('<c:out value="${anoAtual}"/>', 00, 01),
    		changeMonth: false,
    		changeYear: true,
    		yearRange: '<c:out value="${intervaloAnosData}"/>',
    		hideIfNoPrevNext: true,
    		onSelect: function(dataStr, data, datepicker){
    			var chavePontoConsumoSelecionado = $('#chavesPrimarias').val();
    			var ultimaDataSelecionada = dataStr.split(';').slice(-1)[0];
   				AjaxService.obterSomaQdcContrato(
   						chavePontoConsumoSelecionado,
   						ultimaDataSelecionada,
   		           	   {callback: function(valorVolumeReferencia) {            		      		         		
   	                	  $('#volumeFornecimento').val(valorVolumeReferencia);
   	                   }}
   	            	);
    	    }
    	});

		if (datasProgramadas != undefined && datasProgramadas.length > 0) {
    		$("#dataParadaProgramadaCliente1").datepick('setDate', datasProgramadas); 
    	}
		//$("#dataParadaProgramadaCliente1").datepick('disable'); 
		
    	<c:choose>
    		<c:when test="${programadaVO.nomeSolicitante eq cdl}">
    	$('#dataParadaProgramadaCliente1').datepick('setIndicadorSolicitante', cdl);
    		</c:when>
    		<c:when test="${programadaVO.nomeSolicitante eq supridora}">
        	$('#dataParadaProgramadaCliente1').datepick('setIndicadorSolicitante', supridora);
        		</c:when>	
    		<c:otherwise>
    	$('#dataParadaProgramadaCliente1').datepick('setIndicadorSolicitante', cliente);	
    		</c:otherwise>	
    	</c:choose>

    	document.getElementById("indicadorSolicitante1").checked = false;
		document.getElementById("indicadorSolicitante2").checked = false;
		document.getElementById("indicadorSolicitante3").checked = false;
    	$(".datepick-disabled").css({"left":"0","top":"28px"});
    	
	});

	function desabilitarIndicadorSolicitante(){
		var CDL = document.getElementById("indicadorSolicitante1");
		var cliente = document.getElementById("indicadorSolicitante2");
		var supridora = document.getElementById("indicadorSolicitante3");
		if(CDL.checked == true){
			cliente.disabled = true;
			supridora.disabled = true;
		}else if(cliente.checked == true){
				CDL.disabled = true;
				supridora.disabled = true;
		}else if(supridora.checked == true){
			cliente.disabled = true;
			CDL.disabled = true;
		}
	}
	
	function limparTela(){
		document.getElementById("comentario").value = "";
		document.getElementById("dataAviso").value = "";
		$('#volumeFornecimento').val("");
		document.getElementById("indicadorSolicitante1").checked = false;
		document.getElementById("indicadorSolicitante2").checked = false;
		document.getElementById("indicadorSolicitante3").checked = false;
		
		submeter('paradaProgramadaForm','limparTela');
	}

	function atualizarDatas() {
		var inputHidden = document.forms[0].datasProgramadas;
		var datasSelecionadas = $('#dataParadaProgramadaCliente1').datepick('getDate');
			
		inputHidden.value = "";
		
		if (datasSelecionadas != undefined && datasSelecionadas != null) {
			var data;
			for (i = 0; i<datasSelecionadas.length; i++) {
				data = datasSelecionadas[i].format("d/m/Y");
				inputHidden.value = inputHidden.value + data + ";"
			}
		}

		submeter('paradaProgramadaForm','atualizarParadaProgramada');
	}

	function salvarDatas() {
		$("#volumeFornecimento").prop("readonly", false);
		$("#volumeFornecimento").removeClass("campoDesabilitado");
		var inputHidden = document.forms[0].datasProgramadas;
		var datasSelecionadas = $('#dataParadaProgramadaCliente1').datepick('getDate');
			
		inputHidden.value = "";
		
		if (datasSelecionadas != undefined && datasSelecionadas != null) {
			var data;
			for (i = 0; i<datasSelecionadas.length; i++) {
				data = datasSelecionadas[i].format("d/m/Y");
				inputHidden.value = inputHidden.value + data + ";"
			}
		}
    	<c:choose>
			<c:when test="${programadaVO.falhaFornecimento ne null}">
				submeter('paradaProgramadaForm','incluirParadaFalhaFornecimento');
			</c:when>
			<c:otherwise>
				submeter('paradaProgramadaForm','incluirParadaProgramada');
			</c:otherwise>	
		</c:choose>
		
	}
	
	function cancelar() {
		submeter('paradaProgramadaForm', 'pesquisarPontosConsumoParadaProgramada');
	}
	
	function definirSolicitanteCliente(indicador) {
		var desabilitado = $('#dataParadaProgramadaCliente1').datepick('isDisabled');
		if (desabilitado != null && desabilitado == true) {
			$('#dataParadaProgramadaCliente1').datepick('enable');
		}
		console.log(indicador);
		
		$('#dataParadaProgramadaCliente1').datepick('setIndicadorSolicitante', indicador);
		if (document.getElementById('botaoAtualizar') != undefined){
			document.getElementById('botaoAtualizar').disabled = false;
		}else{
			document.getElementById('botaoSalvar').disabled = false;
		}
		
	}


	animatedcollapse.addDiv('dadosClienteContratoPontoConsumo', 'fade=0,speed=400,persist=1,hide=1');

	function init() {
		var indicadorSolicitante = '${programadaVO.nomeSolicitante}';
		if(indicadorSolicitante != undefined && indicadorSolicitante != null) {
			if (indicadorSolicitante == 'cdl') {
				definirSolicitanteCliente('cdl');
			} else if(indicadorSolicitante == 'cliente') {
				definirSolicitanteCliente('cliente');
			}else if (indicadorSolicitante == 'supridora') {
				definirSolicitanteCliente('supridora');
			}
			desabilitarIndicadorSolicitante();
		}
		
		var tipoParada = "<c:out value="${programadaVO.idTipoParada}" />";
    	var chaveParadaNaoProgramada = 72;
    	var chaveParadaCasoFortuito = 73;
    	var chaveGasForaEspecificacao = 74;

    	if(tipoParada == chaveParadaNaoProgramada || tipoParada == chaveParadaCasoFortuito || tipoParada == chaveGasForaEspecificacao ) {
    		$("#volumeFornecimento").prop("readonly", true);
			$("#volumeFornecimento").addClass("campoDesabilitado");
    	}

	}
	
	addLoadEvent(init);
	
</script>

<form:form method="post" action="incluirParadaProgramada" name="paradaProgramadaForm" >
	<input name="datasProgramadas" type="hidden" id="datasProgramadas" value="${programadaVO.datasProgramadas}" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${programadaVO.chavesPrimarias[0]}">
	<input name="numeroContrato" type="hidden" id="numeroContrato" value="${programadaVO.numeroContrato}">
	<input name="postBack" type="hidden" id="postBack" value="">
	<input name="idCliente" type="hidden" id="idCliente" value="${programadaVO.idCliente}">
	<input name="acao" type="hidden" id="acao" value="incluirParadaProgramada">
	<input name="idTipoParada" type="hidden" id="idTipoParada" value="${programadaVO.idTipoParada}">
	<c:forEach begin="0" end="${fn:length(programadaVO.chavesContratos)-1}" var="i">
		<input type="hidden" name="chavesContratos" value="${programadaVO.chavesContratos[i]}" />
	</c:forEach>
	
	<c:forEach items="${pontosConsumo}" var="pontoConsumo">
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${pontoConsumo.chavePrimaria}">
	</c:forEach>
	
	<fieldset id="manterParadaProgramada" class="conteinerPesquisarIncluirAlterar">	    
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo2">Pontos de Consumo</legend>
			<c:if test="${listaContratoPonto ne null}">
				<display:table class="dataTableGGAS" name="listaContratoPonto" sort="list" id="contratoPonto" pagesize="15" excludedParams="" requestURI="#" >
					<display:column style="width: 140px" title="Contrato">
						<c:out value="${contratoPonto.numeroContrato}"></c:out>
					</display:column>
					<display:column style="width: 380px" property="nomeImovel" title="Im�vel" />
					<display:column style="width: 380px" property="pontoConsumo" title="Ponto de Consumo" />					
			   	</display:table>
		   	</c:if>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraPesquisa" />
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
	<label class="rotulo campoObrigatorio" id="rotuloPeriodo" for="dataAviso"><span class="campoObrigatorioSimbolo">* </span>Data de Aviso:</label>
		<input class="campoData campoHorizontal" type="text" id="dataAviso" name="dataAviso" maxlength="10" value="${programadaVO.dataAviso}" >
	<label class="rotulo campoObrigatorio" id="rotuloPeriodo" for="dataAviso">Volume Refer�ncia:</label>
	<input class="campoTexto" type="text" id="volumeFornecimento"
				name="volumeFornecimento"
				onkeypress="return formatarCampoInteiro(event);"   value="${programadaVO.volumeFornecimento}"
				size="5" ><br />
	</fieldset>
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
	
		<label class="rotulo" for="indicadorAceite">Indicador Solicitante:</label>
		<input class="campoRadio" id="indicadorSolicitante1" name="nomeSolicitante" onclick="definirSolicitanteCliente(this.value);" value="cdl" type="radio" <c:if test="${programadaVO.nomeSolicitante eq 'cdl'}">checked="checked"</c:if>><img id="legendaParadaProgramadaCDL" src="<c:url value="/imagens/legendaCDL.gif"/>" alt="CDL" title="CDL" /><label id="rotuloLegendaParadaProgramadaCDL" class="rotuloRadio" for="indicadorSolicitante1">CDL</label>
		<input class="campoRadio" id="indicadorSolicitante2" name="nomeSolicitante" onclick="definirSolicitanteCliente(this.value);" value="cliente" type="radio" <c:if test="${programadaVO.nomeSolicitante eq 'cliente'}">checked="checked"</c:if>><img id="legendaParadaProgramadaCliente" src="<c:url value="/imagens/legendaCliente.gif"/>" alt="Cliente" title="Cliente" /><label id="rotuloLegendaParadaProgramadaCliente" class="rotuloRadio" for="indicadorSolicitante2">Cliente</label>
		<input class="campoRadio" id="indicadorSolicitante3" name="nomeSolicitante" onclick="definirSolicitanteCliente(this.value);" value="supridora" type="radio" <c:if test="${programadaVO.nomeSolicitante eq 'supridora'}">checked="checked"</c:if>><img id="legendaParadaProgramadaSupridora" src="<c:url value="/imagens/legendaSupridora.gif"/>" alt="Supridora" title="Supridora" /><label id="rotuloLegendaParadaProgramadaSupridora" class="rotuloRadio" for="indicadorSolicitante3">Supridora</label>

		<!-- Calend�rio -->
		<div id="dataParadaProgramadaCliente1" class="paradaProgramada" onmouseup="desabilitarIndicadorSolicitante();"></div>
	</fieldset>
	
	<fieldset class="conteinerPesquisarIncluirAlterarNovo">		
		<label class="rotulo" for="observacao">Coment�rio:</label>
		<textarea id="comentario" name="comentario" class="campoTexto" rows="5" cols="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTextoComLimite(event,this,70)');">${programadaVO.comentario}</textarea>
		<c:if test="${programadaVO.falhaFornecimento ne null}">
			<input id="botaoAtualizar" name="button" class="bottonRightCol2 botaoGrande2" value="Atualizar" type="button" onclick="atualizarDatas();" <c:if test="${programadaVO.indicadorSolicitante eq ''}">disabled="disabled"</c:if>>
		</c:if>
	</fieldset>

	<c:if test="${programadaVO.falhaFornecimento ne null}">
		<h1 class="tituloInterno"></h1>
		<p class="orientacaoInicial">Para cada data selecionada no calend�rio, preencha a <span class="destaqueOrientacaoInicial">Menor Press�o</span> e a respectiva 
		<span class="destaqueOrientacaoInicial">Unidade de Press�o</span></p>
		<fieldset id="conteinerDadosPressao" class="conteinerBloco">
			<c:set var="i" value="0" />
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="paradasProgramadas" id="paradasProgramadas" sort="list" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
				<display:column sortable="false" sortProperty="pontoConsumo" title="Ponto Consumo"  style="width: 120px">
		        	<c:out value="${paradasProgramadas.pontoConsumo.descricao}"/>
		        	<input type="hidden" name="chavesPontoConsumoParada" value="${paradasProgramadas.pontoConsumo.chavePrimaria}"/>
		        </display:column>
				<display:column sortable="false" sortProperty="data" title="Data"  style="width: 50px">
		        	<c:out value="${paradasProgramadas.dataParada}"/>
		        	<input type="hidden" name="datasPressao" value="${paradasProgramadas.dataParada}"/> 
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="menorPressao" title="Menor Press�o"  style="width: 50px">
		        	<c:choose>
		        		<c:when test="${empty paradasProgramadas.menorPressao}">
		        			<input class="campoTexto" type="text" name="pressoes" id="menorPressao${i}" value="${programadaVO.pressoes[i]}" onblur="aplicarMascaraNumeroDecimal(this,8);" onkeypress="return formatarCampoDecimalPositivo(event,this,5,8);"/>
		        		</c:when>
		        		<c:otherwise>
		        			<input class="campoTexto" type="text" name="pressoes" id="menorPressao${i}" value="${paradasProgramadas.menorPressao}" onblur="aplicarMascaraNumeroDecimal(this,8);" onkeypress="return formatarCampoDecimalPositivo(event,this,5,8);"/>
		        		</c:otherwise>
		        	</c:choose>
		        </display:column>
		        		        
	          	<display:column sortable="false" sortProperty="unidadeMenorPressao" title="Unidade de Press�o"  style="width: 50px">
		        	<select name="unidades" id="unidadeMenorPressao${i}" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaUnidadePressao}" var="unidade">
								<c:choose>
					        		<c:when test="${empty paradasProgramadas.unidadeMenorPressao}">
					        			<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${programadaVO.unidades[i] == unidade.chavePrimaria}">selected="selected"</c:if>>
					        		</c:when>
					        		<c:otherwise>
										<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${paradasProgramadas.unidadeMenorPressao == unidade.chavePrimaria}">selected="selected"</c:if>>
					        		</c:otherwise>
					        	</c:choose>
								<c:out value="${unidade.descricaoAbreviada}"/>
							</option>		
						</c:forEach>	
					</select><br />
		        </display:column>
		        <c:set var="i" value="${i+1}" /> 
			</display:table>
		</fieldset>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
		<input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparTela();">
	    <input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande2" value="Salvar" type="button" onclick="salvarDatas();" <c:if test="${empty programadaVO.nomeSolicitante}">disabled="disabled"</c:if>>	 
	</fieldset>
<token:token></token:token>
</form:form>