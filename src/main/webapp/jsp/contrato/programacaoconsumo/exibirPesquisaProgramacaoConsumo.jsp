<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Solicita��o de Consumo - Pesquisar Ponto de Consumo<a class="linkHelp" href="<help:help>/consultapontodeconsumoparasolicitaesdeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para consultar as futuras Solicita��es de Consumo clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou selecione o m�s e o ano desejado e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.

<script language="javascript">

$(document).ready(function(){
	
	
	$("#dataAtual").datepicker({
		changeYear: true, 
		showOn: 'button', 
		buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
		buttonImageOnly: true, 
		buttonText: 'Exibir Calend�rio', 	
		dateFormat: 'dd/mm/yyyy'
	});
	
	var selecao = '${programacaoConsumoVO.indicadorPesquisa}';

	if(selecao == "indicadorPesquisMensal"){
		 document.getElementById("pesquisarCliente").disabled = false;
		 document.getElementById("pesquisarData").disabled = true; 

	} else {
		 document.getElementById("pesquisarCliente").disabled = true;
		 document.getElementById("pesquisarData").disabled = false; 
		  
	}	
});


function obterDataAtual(){
	
	

	var data = new Date();
    var dia = data.getDate();
    if (dia.toString().length == 1)
      dia = "0"+dia;
    var mes = data.getMonth()+1;
    if (mes.toString().length == 1)
      mes = "0"+mes;
    var ano = data.getFullYear();  
    
	return dia+"/"+mes+"/"+ano;
	
}
	
	function exibirProgramacaoConsumo(idPontoConsumo) {
		document.getElementById("numeroContrato").disabled = false; 
		document.forms[0].idPontoConsumo.value = idPontoConsumo;
		submeter("programacaoConsumoForm", "exibirProgramacaoConsumo");
	}
	
	function validarNumeroContrato(nContrato){
		
		if(nContrato != null){
			if(nContrato.length < 9){
				alert("N�mero do Contrato de ter 9 d�gitos.");
				document.forms[0].numeroContrato.value = "";
			}
		}
	}
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisMensal"){
			
			 document.getElementById("pesquisarCliente").disabled = false;
			 document.getElementById("pesquisarData").disabled = true; 

		}else{

			 document.getElementById("pesquisarCliente").disabled = true;
			 document.getElementById("pesquisarData").disabled = false; 
			
		}	
	}
	
	function pesquisar(){
		
		if( document.getElementById("indicadorPesquisMensal").checked == true){
			submeter('programacaoConsumoForm', 'pesquisarContratosProgramacaoConsumo');
		}
		if(document.getElementById("indicadorPesquisDiarioIntradiario").checked == true){
			if(document.getElementById("dataAtual").value == ""){			    
				document.getElementById('dataAtual').value =obterDataAtual();
			}
		submeter('programacaoConsumoForm', 'pesquisarProgramacaoConsumoDiariaIntraDiaria');
		}
	}
	
	function salvar(){
		submeter("programacaoConsumoForm", "manterProgramacaoConsumoDiariaIntradiaria");
	}
	
	
	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('numeroContrato').value = "";		
	    
		document.getElementById('dataAtual').value = obterDataAtual();
		document.getElementById('opcao1').checked = true  ;
		
	}
	

</script>

<form:form method="post" action="pesquisarContratosProgramacaoConsumo" name="programacaoConsumoForm">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="acao" type="hidden" id="acao" value="pesquisarContratosProgramacaoConsumo">
		<input id="indicadorPesquisMensal" class="campoRadio" type="radio" value="indicadorPesquisMensal" name="indicadorPesquisa"  onclick="desabilitarPesquisaOposta(this);" <c:if test="${programacaoConsumoVO.indicadorPesquisa eq 'indicadorPesquisMensal'}">checked</c:if>>
	<fieldset id="solicitacaoConsumo" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisarCliente" class="colunaEsq" disabled>
	
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${cliente.chavePrimaria}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nome}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>
				<jsp:param name="funcionalidade" value="programacaoConsumo"/>
			</jsp:include>
			
		<fieldset class="colunaFinal">
			<label class="rotulo" id="rotuloNumeroContrato" for="numeroContrato">N�mero do Contrato:</label>
			<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato"  maxlength="9" size="9" onblur="validarNumeroContrato(this.value);" value="${programacaoConsumoVO.numeroContrato}" onkeypress="return formatarCampoInteiro(event);"><br />
		</fieldset>
		
	</fieldset>
	
	<input id="indicadorPesquisDiarioIntradiario" class="campoRadio" type="radio"  style="margin-top: -3px;margin-left: -15px" value="indicadorPesquisDiarioIntradiario" name="indicadorPesquisa" onclick="desabilitarPesquisaOposta(this);" <c:if test="${empty programacaoConsumoVO.indicadorPesquisa or programacaoConsumoVO.indicadorPesquisa eq 'indicadorPesquisDiarioIntradiario'}">checked</c:if> ><br/>
	
	<fieldset id="pesquisarData" class="colunaEsq" style="margin-top: -15px" disable>
			
		<legend >Pesquisar IntraDi�ria/Di�ria</legend> <br/>
		<br/>
		
	    <div class="pesquisarClienteFundo" >
			<label class="rotulo campoObrigatorio" for="dataVerificacao">Data:</label>
			<input class="campoData campoHorizontal" type="text" id="dataAtual" name="dataAtual" maxlength="10" value="${programacaoConsumoVO.dataAtual}" ><br/><br/>
			
			<label class="rotulo campoObrigatorio" for="tipoExibicaoRadio" style="margin-bottom: 0px; "> Op��es : </label>
					
			<input class="campoRadio" type="radio" name="opcao" id="opcao1" value="todos" <c:if test="${programacaoConsumoVO.opcao eq 'todos'}">checked</c:if>   >
			<label class="rotuloRadio" for="recuperacao">Todos</label>
		
			<input class="campoRadio" type="radio" name="opcao" id="opcao" value="QDS>=QDC" <c:if test="${programacaoConsumoVO.opcao eq 'QDS>=QDC'}">checked</c:if> 	>
			<label class="rotuloRadio" for="top" style="margin-right: 0; padding-right: 0">QDS >= QDC</label>
			<input class="campoRadio" type="radio" name="opcao" id="opcao" style=" margin-left: 20px;" value="QDS<QDC" <c:if test="${programacaoConsumoVO.opcao eq 'QDS<QDC'}">checked</c:if> >
			<label class="rotuloRadio" for="top" style="margin-right: 0;">QDS < QDC </label>
			
			<label class="rotulo" for="idRubrica">City Gate:</label>
			<select name="idCityGate" id="idCityGate" class="campoSelect" >
		    	<option value="-1">Todos</option>
				<c:forEach items="${listaCityGate}" var="cityGate">
					<option value="<c:out value="${cityGate.chavePrimaria}"/>" 
						<c:if test="${programacaoConsumoVO.idCityGate == cityGate.chavePrimaria}">selected="selected"</c:if> >
						<c:out value="${cityGate.descricao}"/>
					</option>
				</c:forEach>
			</select>
			
		</div>
		
	</fieldset>
		
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarContratosProgramacaoConsumo">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button"  onclick="pesquisar()">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">		
		</fieldset>		
	</fieldset>	
	
	<c:if test="${listaCityGateSolicitacaoConsumoVO ne null}">
		<div id="gridProgramacaoDiariaIntradiaria">
			<jsp:include page="/jsp/contrato/programacaoconsumo/gridProgramacaoDiariaIntradiaria.jsp"></jsp:include>	
    	</div>
    
    	<input name="Button" class="bottonRightCol2 bottonRightColUltimo" id="botaoSalvar" value="Salvar" type="button"  onclick="salvar()">	
   	</c:if>

	<c:if test="${listaContratoPonto ne null}">
		<fieldset id="solicitacaoConsumo" class="conteinerPesquisarIncluirAlterar">	
			<c:forEach items="${listaContratoPonto}" var="contratoPonto">
				<hr class="linhaSeparadora" />
				<label class="rotulo" id="rotuloContrato">Contrato:</label>
				<c:if test="${contratoPonto.key ne 0}">
					<span class="itemDetalhamento"><c:out value="${contratoPonto.key}"></c:out></span>
				</c:if>
				<display:table class="dataTableGGAS" list="${contratoPonto.value}" sort="list" id="pontoConsumo" pagesize="15" excludedParams="" requestURI="#" >
					<display:column sortable="false" title="Im�vel" style="width: 330px; text-align:left">
						<a href="javascript:exibirProgramacaoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${pontoConsumo.imovel.nome}"/>
			            </a>
			        </display:column>
					<display:column sortable="false" title="Ponto de Consumo" style="width: 330px; text-align:left">
						<a href="javascript:exibirProgramacaoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${pontoConsumo.descricao}"/>
			            </a>
			        </display:column>
					<display:column sortable="false" title="Ramo de Atividade" style="width: 155px">
						<a style="display: block" href="javascript:exibirProgramacaoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${pontoConsumo.ramoAtividade.descricao}"/>
			            </a>
			        </display:column>
					<display:column sortable="false" title="Segmento" style="width: 90px">
						<a href="javascript:exibirProgramacaoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${pontoConsumo.segmento.descricao}"/>
			            </a>
			        </display:column>
			   	</display:table>
		   	</c:forEach>
		</fieldset>
		
	</c:if>
	
	
	

	
	
	
</form:form>