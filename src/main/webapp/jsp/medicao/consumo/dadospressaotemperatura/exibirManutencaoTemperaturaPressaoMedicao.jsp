<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Dados de Temperatura e Press�o<a class="linkHelp" href="<help:help>/dadosdetemperaturaepresso.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para manter os Dados de Temperatura e Press�o, selecione a Tipo de abrang�ncia, a �rea de abrang�ncia e clique em <span class="destaqueOrientacaoInicial">Exibir</span> para gerar a tabela de preenchimento.
Para consultar dados a partir de determinada data, selecione a data no campo Data Inicial e clique em <span class="destaqueOrientacaoInicial">Exibir</span>. Ao finalizar o preenchimento dos dados clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>


<script language="javascript">
	
	animatedcollapse.addDiv('pesquisaDadosTemperaturaPressao', 'fade=0,speed=400,persist=1,hide=0');
	
	<c:choose>
		<c:when test="${temperaturaPressaoMedicaoForm.map.inputModificado == true}">
			var inputModificado = true;
		</c:when>
		<c:otherwise>
			var inputModificado = false;
		</c:otherwise>
	</c:choose>
	
	$(document).ready(function(){

		$("#dataInicial").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#_,.dataVigenciaGeral").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

		$("#temperaturaPressaoMedicao input[type='text'],#temperaturaPressaoMedicao select").change(function(){
			$("#inputModificado").attr({value: true });
			inputModificado = true;
		});


		$("#idAbrangenciaPesquisa").click(
				   function(){
				   var selected = $("select#idAbrangenciaPesquisa option:selected").val();
			   		if (selected != undefined) {
				   			$("#botaoExibirTemperaturaPressao").attr('disabled', false);
				   		}
						
					})
					
		if ($("select#idAbrangenciaPesquisa option:selected").val() != undefined) {
			$("#botaoExibirTemperaturaPressao").attr('disabled', false);
		}
		
	});
	
	function verificarModificacao() {
		if (inputModificado == true) {
			var confirmacao = window.confirm(' Tem certeza que deseja descartar as modifica��es? ')
			if (confirmacao == true) {
				inputModificado = false;
				document.forms[0].inputModificado.value = false;
				submeter('temperaturaPressaoMedicaoForm', 'exibirTemperaturaPressaoMedicao');
			} else { 
				return false;
			}
		} else {
			inputModificado = false;
			document.forms[0].inputModificado.value = false;
			submeter('temperaturaPressaoMedicaoForm', 'exibirTemperaturaPressaoMedicao');
		}
	}
	
	function exibirTemperaturaPressaoMedicao(){
	
		<c:choose>
		<c:when test="${listaTemperaturaPressaoMedicaoVO ne null}">
			if(confirm('<fmt:message key="CONFIRMACAO_DADOS_NAO_SALVOS"/>')){
				submeter('temperaturaPressaoMedicaoForm', 'exibirTemperaturaPressaoMedicao');
			}
		</c:when>
		<c:otherwise>
			submeter('temperaturaPressaoMedicaoForm', 'exibirTemperaturaPressaoMedicao');
		</c:otherwise> 
	</c:choose>		
	}
	
			
	function carregarAreasAbrangencia(elem){	
		var codigoTipoAbrangenciaPesquisa = elem.value;
	   	var selectAreaAbrangencia = document.getElementById("idAbrangenciaPesquisa");
		var botaoExibirTemperaturaPressao = document.getElementById("botaoExibirTemperaturaPressao");	

		 selectAreaAbrangencia = clearOptionsFast(selectAreaAbrangencia);
	   	 
	   	if (codigoTipoAbrangenciaPesquisa != "-1") {
	   		botaoExibirTemperaturaPressao.disabled = false;
	   		selectAreaAbrangencia.disabled = false; 
	   		exibirElemento("imgProcAreas"); 
	   		var answer = false;      		
	     	AjaxService.listarAreasAbrangenciaTemperaturaPressaoMedicao( codigoTipoAbrangenciaPesquisa,     		 
	 			function(areas) {
	         		answer = areas;
	         		if(answer){
		             	for (key in areas){
		             		var area = areas[key];             		
		             		var novaOpcao = new Option(area["descricao"], area["chavePrimaria"]);
							novaOpcao.title = area["descricao"];
							if (document.all) { // IE
								novaOpcao.appendChild(document.createTextNode(area["descricao"]));
				  			} 
							$(novaOpcao).appendTo('#idAbrangenciaPesquisa');
		             	}
		                ocultarElemento("imgProcAreas");
		                selectAreaAbrangencia.disabled = false;
					}                                  
				}
			);         
	   	} else {
	   		botaoExibirTemperaturaPressao.disabled = true;
	   	}
	}
	
	function limparFormulario(){
		document.getElementById('codigoTipoAbrangenciaPesquisa').selectedIndex = 0;
		document.getElementById('dataInicial').value = '';
		carregarAreasAbrangencia(document.getElementById('idAbrangenciaPesquisa'));
		document.getElementById('botaoExibirTemperaturaPressao').disabled = 'disabled';
	}
	
	function onloadTemperaturaPressaoMedicoes() {
		<c:forEach items="${listaTemperaturaPressaoMedicaoVO}" var="temperaturaPressaoMedicao" varStatus="index">		
			<c:if test="${temperaturaPressaoMedicao.habilitado}">
				$('#dataVigencia${index.count-1}').datepicker({ minDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
					buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
			</c:if>		
		</c:forEach>
	}
	
	function salvarDados(){
		var codigo = document.getElementById("idAbrangencia");
		$('#idAbrangenciaPesquisa').val(codigo);
		submeter('temperaturaPressaoMedicaoForm', 'manterTemperaturaPressaoMedicao');
	}
	
	function remover(selecionado){
		var retorno = confirm('Deseja excluir o dado de temperatura e press�o?');
		if (retorno) {
			document.getElementById('indexLista').value = selecionado;
			submeter('temperaturaPressaoMedicaoForm', 'removerTemperaturaPressaoMedicao');		
		}	
	}

	function manterEstadoAjax() {
	  	var select = document.getElementById("codigoTipoAbrangenciaPesquisa");
	  	carregarAreasAbrangencia(select);
	}

	addLoadEvent(manterEstadoAjax);
	addLoadEvent(onloadTemperaturaPressaoMedicoes);


</script>

<form:form method="post" action="exibirTemperaturaPressaoMedicao" name="temperaturaPressaoMedicaoForm">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idAbrangencia" type="hidden" id="idAbrangencia" value="<c:out value="${idAbrangencia}"/>">
	<input name="codigoTipoAbrangencia" type="hidden" id="codigoTipoAbrangencia" value="<c:out value="${codigoTipoAbrangencia}"/>">
	<input name="indexLista" type="hidden" id="indexLista" value="-1">
	<input name="inputModificado" type="hidden" id="inputModificado" value="${temperaturaPressaoMedicaoForm.map.inputModificado}"/>

	<fieldset id="dadosTemperaturaPressao" class="conteinerPesquisarIncluirAlterar">

		<label class="rotulo campoObrigatorio" for="codigoTipoAbrangenciaPesquisa"><span class="campoObrigatorioSimbolo">* </span>Tipo de abrang�ncia:</label>
		<select name="codigoTipoAbrangenciaPesquisa" id="codigoTipoAbrangenciaPesquisa" class="campoSelect campoHorizontal" onchange="carregarAreasAbrangencia(this);">
    		<option value="-1">Selecione</option>
    		<c:forEach items="${listaTipoAbrangencia}" var="tipoAbrangencia">
				<option value="<c:out value="${tipoAbrangencia.key}"/>" <c:if test="${codigoTipoAbrangenciaPesquisa eq tipoAbrangencia.key}">selected="selected"</c:if>>
					<c:out value="${tipoAbrangencia.value}"/>
				</option>		
	    	</c:forEach>	
	    </select>
	    <img style="display:none" class="imgProc campoHorizontal" id="imgProcAreas" alt="Pesquisando..." title="Pesquisando..." src="<c:url value="/imagens/indicator.gif"/>">	    
	    
	    <label id="rotuloDataInicial" class="rotulo rotuloHorizontal" for="dataInicial">Data Inicial:</label>
    	<input class="campoData campoHorizontal" type="text" id="dataInicial" name="dataInicial" value="${dataInicial}" maxlength="10"><br />
	    <br />
	    
		<label class="rotulo campoObrigatorio" for="idAbrangenciaPesquisa"><span class="campoObrigatorioSimbolo">* </span>�rea de abrang�ncia:</label>
		<select name="idAbrangenciaPesquisa" id="idAbrangenciaPesquisa" class="campoSelect" size="10" <c:if test="${empty listaAreasAbrangencia}" > disabled="disabled"</c:if>>
	    </select>
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<vacess:vacess param="alterarCliente">
	    		<input name="Button" class="bottonRightCol2" id="botaoExibirTemperaturaPressao" value="Exibir" type="button" disabled="disabled" onclick="verificarModificacao();">
			</vacess:vacess>	    				
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>	
	
		<c:if test="${listaTemperaturaPressaoMedicaoVO ne null}">	
			<c:set var="total" value="${fn:length(listaTemperaturaPressaoMedicaoVO)}" />		
			<hr class="linhaSeparadoraPesquisa" />
			<c:set var="i" value="0" />	 		
			<display:table class="dataTableGGAS" name="listaTemperaturaPressaoMedicaoVO" sort="list" id="temperaturaPressaoMedicao" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirManutencaoPCSCityGate">
				<display:column sortable="false" title="Data">
					<input type="hidden" id="chavePrimaria" name="chavePrimaria" value="<c:if test="${temperaturaPressaoMedicao.chavePrimaria >= 0}"><c:out value="${temperaturaPressaoMedicao.chavePrimaria}"/></c:if><c:if test="${empty temperaturaPressaoMedicao.chavePrimaria or temperaturaPressaoMedicao.chavePrimaria eq null}">0</c:if>"/>	
					<c:choose>
						<c:when test="${temperaturaPressaoMedicao.habilitado ne true}">
							<input type="hidden" name="dataVigencia" value="<c:out value="${temperaturaPressaoMedicao.dataVigenciaFormatada}"/>" />
							<input type="text" name="disabledDataVigencia" class="campoData campoDesabilitado" value="<c:out value="${temperaturaPressaoMedicao.dataVigenciaFormatada}"/>" size="10" disabled="disabled"/>
						</c:when>
						<c:otherwise>
							<input type="text" id="dataVigencia<c:out value='${i}'/>" name="dataVigencia" class="campoData dataVigenciaGeral" value="<c:out value="${temperaturaPressaoMedicao.dataVigenciaFormatada}"/>" maxlength="10" size="10"/>
						</c:otherwise>
					</c:choose>
					<input type="hidden" name="habilitado" value="<c:out value="${temperaturaPressaoMedicao.habilitado}"/>"/>
				</display:column>
				<display:column sortable="false"  title="Temperatura">		
					<c:choose>
						<c:when test="${temperaturaPressaoMedicao.habilitado ne true}">
							<input type="hidden" name="temperatura" value="<c:out value="${temperaturaPressaoMedicao.temperatura}"/>" />
							<input id="campoTempPress" class="campoTexto campoDesabilitado" type="text" name="disabledTemperatura" value="<c:out value="${temperaturaPressaoMedicao.temperatura}"/>" disabled="disabled">
						</c:when>
						<c:otherwise>
							<input tabindex="${total + i}" id="campoTempPress" class="campoTexto" type="text" name="temperatura" maxlength="15" onkeypress="return formatarCampoDecimal(event,this,5,5);" value="<c:out value="${temperaturaPressaoMedicao.temperatura}"/>">
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column sortable="false"  title="Unidade de Temperatura">
					<c:choose>
						<c:when test="${temperaturaPressaoMedicao.habilitado ne true}">
							<input type="hidden" name="idUnidadeTemperatura" value="<c:out value="${temperaturaPressaoMedicao.idUnidadeTemperatura}"/>"/>
							<select name="disabledUnidadeTemperatura" class="campoSelect campoHorizontal campoDesabilitado" disabled="disabled">
					    		<option value="">Selecione</option>
					    		<c:forEach items="${listaUnidadeTemperatura}" var="unidadeTemperatura">
									<option value="<c:out value="${unidadeTemperatura.chavePrimaria}"/>" <c:if test="${temperaturaPressaoMedicao.idUnidadeTemperatura eq unidadeTemperatura.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${unidadeTemperatura.descricao}"/>
									</option>		
						    	</c:forEach>	
						    </select>
						</c:when>
						<c:otherwise>
							<select id="unidadeTemperatura" tabindex="${total*2 + i}" name="idUnidadeTemperatura" class="campoSelect campoHorizontal">
					    		<option value="">Selecione</option>
					    		<c:forEach items="${listaUnidadeTemperatura}" var="unidadeTemperatura">
									<option value="<c:out value="${unidadeTemperatura.chavePrimaria}"/>" <c:if test="${temperaturaPressaoMedicao.idUnidadeTemperatura eq unidadeTemperatura.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${unidadeTemperatura.descricao}"/>
									</option>		
						    	</c:forEach>	
						    </select>
						</c:otherwise>
					</c:choose>	    
				</display:column>
				<display:column sortable="false"  title="Press�o">
					<c:choose>
						<c:when test="${temperaturaPressaoMedicao.habilitado ne true}">
							<input type="hidden" name="pressao" value="<c:out value="${temperaturaPressaoMedicao.pressao}"/>"/>
							<input id="campoTempPress" class="campoTexto campoDesabilitado" type="text" name="disabledPressao" value="<c:out value="${temperaturaPressaoMedicao.pressao}"/>" disabled="disabled">
						</c:when>
						<c:otherwise>
							<input tabindex="${total*3 + i}" id="campoTempPress" class="campoTexto" type="text" name="pressao" maxlength="15" onkeypress="return formatarCampoDecimal(event,this,5,5);" value="<c:out value="${temperaturaPressaoMedicao.pressao}"/>">
						</c:otherwise>
					</c:choose>		
				</display:column>
				<display:column sortable="false"  title="Unidade de Press�o">
					<c:choose>
						<c:when test="${temperaturaPressaoMedicao.habilitado ne true}">
							<input type="hidden" name="idUnidadePressao" value="<c:out value="${temperaturaPressaoMedicao.idUnidadePressao}"/>"/>
							<select name="disabledUnidadePressao" class="campoSelect campoHorizontal campoDesabilitado" disabled="disabled">
					    		<option value="">Selecione</option>
					    		<c:forEach items="${listaUnidadePressao}" var="unidadePressao">
									<option value="<c:out value="${unidadePressao.chavePrimaria}"/>" <c:if test="${temperaturaPressaoMedicao.idUnidadePressao eq unidadePressao.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${unidadePressao.descricao}"/>
									</option>		
						    	</c:forEach>	
						    </select>
						</c:when>
						<c:otherwise>
							<select id="unidadePressao" tabindex="${total*4 + i}" name="idUnidadePressao" class="campoSelect campoHorizontal">
					    		<option value="">Selecione</option>
					    		<c:forEach items="${listaUnidadePressao}" var="unidadePressao">
									<option value="<c:out value="${unidadePressao.chavePrimaria}"/>" <c:if test="${temperaturaPressaoMedicao.idUnidadePressao eq unidadePressao.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${unidadePressao.descricao}"/>
									</option>		
						    	</c:forEach>	
						    </select>
						</c:otherwise>						
					</c:choose>		
				</display:column>		
				<display:column style="text-align: center;">
					<c:if test="${temperaturaPressaoMedicao.habilitado eq true && temperaturaPressaoMedicao.chavePrimaria ne null && temperaturaPressaoMedicao.chavePrimaria gt 0}">
						<a href="javascript:remover(<c:out value="${i}"/>);"><img id="iconeExcluir" title="Excluir" alt="Excluir"  src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
					</c:if>
				</display:column>     		
				<c:set var="i" value="${i+1}" />	 
			</display:table>
		</c:if>		
	
	<c:if test="${listaTemperaturaPressaoMedicaoVO ne null}">
	<fieldset class="conteinerBotoes">
		<vacess:vacess param="manterTemperaturaPressaoMedicao">			
	    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="salvarDados();">
	    </vacess:vacess>
	</fieldset>
	</c:if>
	<token:token></token:token>
</form:form> 
