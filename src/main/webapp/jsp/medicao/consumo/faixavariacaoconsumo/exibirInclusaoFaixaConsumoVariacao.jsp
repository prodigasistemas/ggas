<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script type="text/javascript">
	$(document).ready(function(){
		//Estilos iniciais para os bot�es de Adicionar e Remover Faixas de Tarifas 
        $("input.botaoAdicionarFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
        $("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});

        $("input.botaoAdicionarFaixas").hover(
            function () {
                $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
            },
            function () {
                $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
            }
        );

        $("input.botaoRemoverFaixas").hover(
            function () {
                $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
            },
            function () {
                $(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
            }
        );		
	});	
	
	function criarListaItensAbilitados(array){
		var total = array.length;
		var jsonString = "";
		
		for(var x = 0; x < total; x++){	
			if (array[x].checked == true){
				jsonString = jsonString + "true,";
			}else{
				jsonString = jsonString + "false,";
			}
		}
		
		return jsonString;		
	}
	
	function salvar(){
	    //capturando valores do grid por javascript
	    var ids = $(".id");	    
	    var listaAbilitados = criarListaItensAbilitados(ids);
	    
	    $("#idSegmento").removeAttr("disabled");
	    $(".menorFaixa").removeAttr("disabled");
	    $(".maiorFaixa").removeAttr("disabled");
	     
	    document.getElementById("abilitados").value = listaAbilitados;
	    
        var url = 'salvarFaixaVariacaoConsumo';
        submeter('faixaConsumoVariacaoForm', url);		
	}

	function limpar(){
		var telaAcao = '${sessionScope.telaAcao}';
		
		document.getElementById("descricao").value = "";
		document.getElementById("habilitado").checked = true;
		
	  	if (telaAcao == 'incluir'){
	  		document.getElementById("idSegmento").selectedIndex = 0;
	  	}
	}
	
	function cancelar(){
		document.getElementById("idSegmento").value = "";
		document.getElementById("idSegmentoFiltro").value = "";
		document.getElementById("descricao").value = "";
		submeter('faixaConsumoVariacaoForm', 'cancelar');
	}
	
	function adicionarFaixaTarifa(chave){	    
	    //capturando valores do grid por javascript
	    var ids = $(".id");	    
	    var listaAbilitados = criarListaItensAbilitados(ids);
	    
	    $("#idSegmento").removeAttr("disabled");
	    $(".idSegmento").removeAttr("disabled");
	    $(".menorFaixa").removeAttr("disabled");
	    $(".maiorFaixa").removeAttr("disabled");
	    
	    document.getElementById("idSegmentoFiltro").value = document.getElementById('idSegmento').value;
	    document.getElementById("abilitados").value = listaAbilitados;
	    
        submeter('faixaConsumoVariacaoForm', 'incluirFaixaVariacaoConsumo');
	}
	
    function removerFaixaTarifa(faixaInferior){
        var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
        if(retorno == true) {
            
        	$("#idSegmento").removeAttr("disabled");
        	$(".idSegmento").removeAttr("disabled");
    	    $(".menorFaixa").removeAttr("disabled");
    	    $(".maiorFaixa").removeAttr("disabled");
    	    
    	    document.getElementById("idSegmentoFiltro").value = document.getElementById('idSegmento').value;
    	    document.getElementById("faixaInicialExclusao").value = faixaInferior;
    	    
            submeter('faixaConsumoVariacaoForm', 'removerFaixaConsumoVariacao');
        }
    }
    
    function mostrarMensagemDeAviso(){
        alert('<fmt:message key="MENSAGEM_AREA_CONSTRUIDA_VINCULADA"/>');
    }
</script>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<c:if test="${sessionScope.telaAcao == 'incluir'}">
	<h1 class="tituloInterno">Inserir Faixa de Varia��o do Consumo<a class="linkHelp" href="<help:help>/faixavariacaoconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
</c:if>
<c:if test="${sessionScope.telaAcao == 'alterar'}">
	<h1 class="tituloInterno">Alterar Faixa de Varia��o do Consumo<a class="linkHelp" href="<help:help>/faixavariacaoconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
</c:if>

<input name="inclusaoAlteracao" type="hidden" id="inclusaoAlteracao" value="${faixaConsumoVariacao.segmento.chavePrimaria}"/>

<form:form method="post" name="faixaConsumoVariacaoForm">
	<input name="acao" type="hidden" id="acao" />
	<input name="faixaFimAdicao" type="hidden" id="faixaFimAdicao" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" />
	<input name="faixaInicialExclusao" type="hidden" id="faixaInicialExclusao" />
	
	<input name="ids" type="hidden" id="ids" />
	<input name="abilitados" type="hidden" id="abilitados" />
	<input name="maioresFaixas" type="hidden" id="maioresFaixas" />
	<input name="menoresFaixas" type="hidden" id="menoresFaixas" />
	<input name="percentuaisInferior" type="hidden" id="percentuaisInferior" />
	<input name="percentuaisSuperior" type="hidden" id="percentuaisSuperior" />
	<input name="altosConsumo" type="hidden" id="altosConsumo" />
	<input name="baixosConsumo" type="hidden" id="baixosConsumo" />
	<input name="estourosConsumo" type="hidden" id="estourosConsumo" />
	
	<input name="idSegmentoFiltro" type="hidden" id="idSegmentoFiltro" />

	<fieldset class="coluna">
		  <label class="rotulo" for="idSegmento" id="rotuloSegmento" style="margin-top: 0px;"><span class="campoObrigatorioSimbolo">* </span>Segmento: </label>
		  <c:if test="${sessionScope.telaAcao == 'incluir'}">
		  	<select name="segmento" class="campoSelect" id="idSegmento">
			  	<option value="-1">Selecione</option>
		          <c:forEach items="${listaSegmentoVariacao}" var="segmento">
		              <option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${faixaConsumoVariacao.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
		                  <c:out value="${segmento.descricao}"/>
		              </option>       
		          </c:forEach>    
		      </select>
		  </c:if>
		  <c:if test="${sessionScope.telaAcao == 'alterar'}">
		  	<select name="segmento" class="campoSelect" id="idSegmento" disabled="disabled">
			  	<option value="-1">Selecione</option>
		          <c:forEach items="${listaSegmentoVariacao}" var="segmento">
		              <option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${faixaConsumoVariacao.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
		                  <c:out value="${segmento.descricao}"/>
		              </option>       
		          </c:forEach>    
	      </select>
		  </c:if>
		  <label class="rotulo labelRotuloPosicionamento">Descri��o:</label>
    <input class="campoTexto campoHorizontal" style="width: 200px" type="text" name="descricao" id="descricao"
    onkeyup="return removerEspacoInicioComLetraMaiuscula(this)" maxlength="20"
    onkeypress="return removerEspacoInicioComLetraMaiuscula(this)" value="${faixaConsumoVariacao.descricao}" />
	</fieldset>
	
	

	<c:if test="${sessionScope.telaAcao == 'alterar'}">
		<fieldset id="faixaCol1" class="colunaDir">
			<label class="rotulo" id="rotuloHabilitado" for="habilitado1">Indicador de Uso:</label> 
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" 
					<c:if test="${habilitado eq true}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Ativo&nbsp;</label> 
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false"
					<c:if test="${habilitado eq false}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Inativo&nbsp;</label>
		</fieldset>
	</c:if>
	<c:if test="${sessionScope.telaAcao == 'incluir'}">
		<br/><br/><br/>
	</c:if>
			
    <fieldset id="tarifaFaixas" class="conteinerBloco" style="padding-top:20px;">
       
       <c:set var="i" value="0" />
       <display:table  class="dataTableGGAS dataTableCabecalho2Linhas" style="font-size:9px;" sort="list" name="sessionScope.listaFaixaVariacao" id="variacao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
          
<%--            <display:column style="text-align: center; width: 100px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>&nbsp;Habilitado" > --%>
<%--                <input type="checkbox" class="id" id="faixasHabilitadas" name="faixasHabilitadas" value='${variacao.chavePrimaria}' <c:if test="${variacao.habilitado == true}">checked="checked"</c:if>> --%>
<%--            </display:column> --%>
                      
           <display:column title="Faixa Inicial (m�)" style="auto">
           		<input type="hidden" id="faixaInferior" name="faixaInferior" value="${variacao.faixaInferior}">
				<input type="text" style="width: 110px; margin-top:-11px;" class="campoTextoMaior" name="faixaInferior2" id="faixaInferior2" 
               		disabled="disabled" value="${variacao.faixaInferior}" onkeypress="return formatarCampoDecimal(event,this, 17, 8);" />
           </display:column>
           
           <display:column title="Faixa Final (m�)" style="auto">
                <c:choose>
                	<c:when test="${fn:length(listaFaixaVariacao) > i+1}">
                		<input type="hidden" id="faixaSuperior" name="faixaSuperior" value="${variacao.faixaSuperior}">
						<input type="text" style="width: 110px; margin-top:-11px;" class="campoTextoMaior" id="faixaSuperior2" name="faixaSuperior2" 
							disabled="disabled" value="${variacao.faixaSuperior}" onkeypress="return formatarCampoDecimal(event,this, 17, 8);">
                	</c:when>
                	<c:otherwise>
						<input type="text" style="width: 110px; margin-top:-11px;" class="campoTextoMaior" id="faixaSuperior" name="faixaSuperior" 
							value="${variacao.faixaSuperior}"  onkeypress="return formatarCampoDecimal(event,this, 17, 8);">
                	</c:otherwise>
                </c:choose>            
           </display:column>
           
           <display:column title="Varia��o<br/>Inferior (%)" style="auto">
               <input type="text" style="width: 70px; margin-top:-11px;" class="campoTextoMaior percentualInferior" id="percentualInferior" name="percentualInferior" value="${variacao.percentualInferior}" onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this,3,2)">
           </display:column>
           
           <display:column title="Varia��o<br/>Superior (%)" style="auto">
               <input type="text" style="width: 70px; margin-top:-11px;" class="campoTextoMaior percentualSuperior" id="percentualSuperior" name="percentualSuperior" value="${variacao.percentualSuperior}" onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this,3,2)">
           </display:column>
           
          	<display:column title="Fator<br/>Baixo Consumo (%)" style="auto">
               <input type="text" style="width: 70px; margin-top:-11px;" class="campoTextoMaior baixoConsumo" id="baixoConsumo" name="baixoConsumo" value="${variacao.baixoConsumo}" onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this,3,2)">
           </display:column>
           
           <display:column title="Fator<br/>Alto Consumo (%)" style="auto">
               <input type="text" style="width: 70px; margin-top:-11px;" class="campoTextoMaior altoConsumo" id="altoConsumo" name="altoConsumo" value="${variacao.altoConsumo}" onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this,3,2)">
           </display:column>
           
           <display:column title="Fator<br/>Estouro Consumo (%)" style="auto">
               <input type="text" style="width: 70px; margin-top:-11px;" class="campoTextoMaior estouroConsumo" id="estouroConsumo" name="estouroConsumo" value="${variacao.estouroConsumo}" onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this,3,2)">
           </display:column>
           
           <display:column style="auto"  title="Faixas" >
               <c:choose>
                  <c:when test="${fn:length(listaFaixaVariacao) == 1}">
                       <input type="button" class="botaoAdicionarFaixas botaoAdicionar" id="botaoAdicionar" 
                       		style="vertical-align: text-bottom; margin-bottom: 6px; margin-top: 5px; " 
							onClick='adicionarFaixaTarifa(<c:out value="${variacao.faixaInferior}"/>);' title="Adicionar" />
                   </c:when>
                   <c:otherwise>
                       <input type="button" class="botaoAdicionarFaixas botaoAdicionar"
                       		style="vertical-align: text-bottom; margin-bottom: 5px; margin-top: 5px; margin-right: -5px;" 
                       		onClick="adicionarFaixaTarifa();" title="Adicionar" />
                       <input type="button" class="botaoRemoverFaixas botaoRemover" id="botaoRemover"
                       		style="vertical-align: text-bottom; margin-bottom: 5px; margin-top: 5px;" 
                        	onClick="removerFaixaTarifa(<c:out value='${variacao.faixaInferior}'/>)" title="Remover" />
                   </c:otherwise>
               </c:choose>
           </display:column>
           
          <c:set var="i" value="${i+1}" />     
       </display:table>
   </fieldset>

	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar()" />
	    
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar();" />
	    
		<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="salvar()" />
	</fieldset>


</form:form>