<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>

<h1 class="tituloInterno">
	Pesquisar Faixa de Varia��o do Consumo<a class="linkHelp"
		href="<help:help>/consultadecomprapeloimvel.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para pesquisar um registro espec�fico, informe os dados nos campos
	abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>,
	ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>
	para exibir todos.<br /> Para incluir uma novo registro clique em <span
		class="destaqueOrientacaoInicial">Incluir</span>
</p>

<form:form method="post"
	action="pesquisarFaixaConsumoVariacao" name="faixaConsumoVariacaoForm" id="faixaConsumoVariacaoForm">

	<script language="javascript">
		function limparCamposPesquisa() {
			document.getElementById("idSegmento").value = '-1';
			document.getElementById("descricaoFiltro").value = "";
			document.getElementById("habilitado3").checked = true;
		}

		function incluir() {
			document.getElementById("idSegmento").value = -1;
			submeter("faixaConsumoVariacaoForm",
					"exibirInclusaoFaixaConsumoVariacao");
		}

		function exibirDetalhamento(chavePrimariaSegmento) {
			document.getElementById("idSegmentoFiltro").value = chavePrimariaSegmento;
			submeter("faixaConsumoVariacaoForm",
					"exibirInclusaoFaixaConsumoVariacao");
		}

		function pesquisar() {
			submeter("faixaConsumoVariacaoForm",
					"pesquisarFaixaConsumoVariacao");
		}
	</script>

	<input name="acao" type="hidden" id="acao" value="pesquisarFaixaConsumoVariacao" />
	<input name="idSegmentoFiltro" type="hidden" id="idSegmentoFiltro" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" />

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="faixaCol1" class="coluna">
			<label class="rotulo" for="idSegmento" id="rotuloSegmento" style="margin-top: 0px;">Segmento:</label>
			<select name="segmento" class="campoSelect" id="idSegmento">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmentoFaixaVariacao}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>"
						<c:if test="${idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}" />
					</option>
				</c:forEach>
			</select> <label class="rotulo" id="rotuloDescricao" for="descricaoFiltro">Descri��o:</label>
			<input class="campoTexto campoHorizontal" type="text"
				id="descricaoFiltro" name="descricao"
				onkeyup="return letraMaiuscula(this)"
				onkeypress="return letraMaiuscula(this)"
				value="${descricao}"><br />
		</fieldset>

		<fieldset id="faixaCol1" class="colunaFinal">
			<label class="rotulo" id="rotuloHabilitado" for="habilitado1">Indicador
				de Uso:</label> <input class="campoRadio" type="radio" name="habilitado"
				id="habilitado1" value="true"
				<c:if test="${habilitado == 'true'}">checked</c:if>><label
				class="rotuloRadio" for="habilitado1">Ativo&nbsp;</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado2"
				value="false"
				<c:if test="${habilitado == 'false'}">checked</c:if>><label
				class="rotuloRadio" for="habilitado2">Inativo&nbsp;</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado3"
				value=""
				<c:if test="${empty habilitado}">checked</c:if>><label
				class="rotuloRadio" for="habilitado3">Todos</label>
		</fieldset>

		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarFaixaConsumoVariacao">
				<input name="Button" class="bottonRightCol2" onclick="pesquisar()"
					id="botaoPesquisar" value="Pesquisar" type="submit">
			</vacess:vacess>
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo"
				value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>
	<br />

	<c:if test="${listaSegmentoVariacao != null}">
		<fieldset class="conteinerBloco">
			<display:table class="dataTableGGAS" name="listaSegmentoVariacao"
				sort="list" id="variacao"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="pesquisarFaixaConsumoVariacao">
				<display:column title="Ativo"
					style="text-align: center; width: 30px">
					<c:choose>
						<c:when test="${variacao.habilitado == true}">
							<img alt="Ativo" title="Ativo"
								src="<c:url value="/imagens/success_icon.png"/>" border="0">
						</c:when>
						<c:otherwise>
							<img alt="Inativo" title="Inativo"
								src="<c:url value="/imagens/cancel16.png"/>" border="0">
						</c:otherwise>
					</c:choose>
				</display:column>

				<display:column title="Segmento">
					<a
						href="javascript:exibirDetalhamento(<c:out value='${variacao.segmento.chavePrimaria}'/>);"><span
						class="linkInvisivel"></span> <c:out
							value='${variacao.segmento.descricao}' /> </a>
				</display:column>
				<display:column title="Descri��o">
					<a
						href="javascript:exibirDetalhamento(<c:out value='${variacao.segmento.chavePrimaria}'/>);"><span
						class="linkInvisivel"></span> <c:out value='${variacao.descricao}' />
					</a>
				</display:column>

			</display:table>

		</fieldset>
	</c:if>


	<fieldset class="conteinerBotoes">
		<vacess:vacess param="exibirInclusaoFaixaConsumoVariacao">
			<input	id="botaoIncluir" 
					name="button"
					class="bottonRightCol2 botaoGrande1 botaoIncluir" 
					value="Incluir"
					type="button" 
					onclick="incluir()" />
		</vacess:vacess>
	</fieldset>

</form:form>

