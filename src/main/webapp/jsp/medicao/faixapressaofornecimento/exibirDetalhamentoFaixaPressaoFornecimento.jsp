<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Press�o de Fornecimento<a href="<help:help>/cadastrodoclientedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form:form method="post" action="exibirAtualizacaoFaixaPressaoFornecimento" enctype="multipart/form-data"  id="faixaPressaoFornecimentoForm" name="faixaPressaoFornecimentoForm">

<script>
	
	function back(){
		$("#voltar").val("true");
		submeter('faixaPressaoFornecimentoForm', 'pesquisarFaixaPressaoFornecimento');
		
	}
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter('faixaPressaoFornecimentoForm', 'exibirAtualizacaoFaixaPressaoFornecimento');
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirAtualizacaoFaixaPressaoFornecimento">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${chavePrimaria}">
<input name="segmento" type="hidden" id="idSegmento" value="${faixaPressaoVO.segmento.chavePrimaria}">
<input name="unidadePressao" type="hidden" id="idUnidadePressao" value="${faixaPressaoVO.unidadePressao.chavePrimaria}">
<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">
<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="">
<input name="voltar" type="hidden" id="voltar" value="">



<fieldset class="detalhamento">
	<fieldset id="detalhamentoGeral" class="conteinerBloco">
		
		<fieldset id="detalhamentoSistemaIntegrante" class="colunaEsq">
			<label class="rotulo" id="rotuloUnidadePressao">Segmento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${faixaPressaoVO.segmento.descricao}"/></span><br />
			<label class="rotulo" id="rotuloUnidadePressao">Unidade de Press�o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${faixaPressaoVO.unidadePressao.descricao}"/></span><br />
						
		</fieldset>
		
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Faixas de Press�o</legend>		
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas"
			name="listaFaixaPressaoFornecimento" sort="list" id="faixaPressao" 
			pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" 
			requestURI="exibirDetalhamentoFaixaPressaoFornecimento" > 
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${faixaPressao.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        <display:column title="Press�o de Fornecimento Minimo" style="text-align: center;">
	        	<c:out value="${faixaPressao.medidaMinimo}"/>
	        </display:column>
	        <display:column title="Press�o de Fornecimento M�ximo" style="text-align: center;">
	        	<c:out value="${faixaPressao.medidaMaximo}"/>
	        </display:column>
	        <display:column title="Classe" style="text-align: center;">
	        	<c:out value="${faixaPressao.entidadeConteudo.descricao}"/>
	        </display:column>
	        
	        <display:column title="Fator �nico Corre��o" style="text-align: center;">
	        	<c:out value="${faixaPressao.numeroFatorCorrecaoPTZPCS}"/>
	        </display:column>	        
	        
	        <display:column title="Corrige PT" style="text-align: center;">
	        	<c:out value="${faixaPressao.indicadorCorrecaoPT.descricao}"/>
	        </display:column>

	        
	        <display:column title="Corrige Z" style="text-align: center;">
	        	<c:out value="${faixaPressao.indicadorCorrecaoZ.descricao}"/>
	        </display:column>
	        
	        <display:column title="Fator Z" style="text-align: center;">
	        	<c:out value="${faixaPressao.numeroFatorZ}"/>
	        </display:column>
						
	   	</display:table>	   	
	</fieldset>
</fieldset>	
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="back();">
    <vacess:vacess param="exibirAtualizacaoFaixaPressaoFornecimento">    
    	<input id="botaoAlterar" name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>

</form:form>
