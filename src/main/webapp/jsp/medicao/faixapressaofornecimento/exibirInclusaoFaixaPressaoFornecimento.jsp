<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<script language="javascript">
$(document).ready(function(){
	
	//Estilos iniciais para os bot�es de Adicionar e Remover Faixas de Tarifas 
	    $("input.botaoAdicionarFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
	    $("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});

	    $("input.botaoAdicionarFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			}
		);

	    $("input.botaoRemoverFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			}
		);
		
		//Remove a Faixa de Tarifa
		//$("#tarifaFaixas .dataTableGGAS tr td input.botaoRemoverFaixas").click(function() {
		//	var linha = $(this).parents("#tarifaFaixas .dataTableGGAS tr");
	//			$(linha).remove();
		//		faixasTarifaClasses();
	//	});

		$("input[name=maiorFaixa]").each(function(){
			aplicarMascaraNumeroInteiro(this);
		});
		$("input[name=menorFaixa]").each(function(){
			aplicarMascaraNumeroInteiro(this);
		});
		habilitarDesabilitarCamposCorrigePTZFatorPTZ();	
		habilitarDesabilitarCamposNumeroFatorCorrecaoFatorPTZ();
		habilitarDesabilitarCampoNumeroCorrecao();
	});

function adicionarFaixaPressao( chave ){
	habilitarCamposDesabilitados("dadosFaixa");
	submeter('faixaPressaoFornecimentoForm','adicionarFaixaPressao');
}

function formatarValoresPressao(){
	formatarCampoDecimalVirgulaPorPontoNaoPermiteNegativo(document.getElementById("faixaInicial"),2,4);
	formatarCampoDecimalVirgulaPorPontoNaoPermiteNegativo(document.getElementById("faixaFinal"),2,4);
}

function removerFaixaPressao(chave){
	habilitarCamposDesabilitados("dadosFaixa");
	document.getElementById('indexFaixaPressaoExlcusao').value = chave;
	submeter('faixaPressaoFornecimentoForm','removerFaixaPressao')
}
	
function limparFormulario(){
	submeter('faixaPressaoFornecimentoForm','limparPressoesSemVinculo')
}

function cancelar(){
	location.href = '<c:url value="/exibirPesquisaFaixaPressaoFornecimento"/>';
}

function habilitarCamposDesabilitados(table_id){

    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    var selects=document.getElementById(table_id).getElementsByTagName('select');
    
    for(var i=0; i<inputs.length; ++i){
    	inputs[i].disabled=false;
    }
    for(var i=0; i<selects.length; ++i){
    	selects[i].disabled=false;
    }
    
}

function habilitarDesabilitarCamposNumeroFatorCorrecaoFatorPTZ(){
	
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	var codigoEntidadeConteudoCorrigiZSimValor=document.getElementById("codigoEntidadeConteudoCorrigiZSimValor").value;
	var codigoEntidadeConteudoCorrigiPTNao=document.getElementById("codigoEntidadeConteudoCorrigiPTNao").value;
	var codigoEntidadeConteudoCorrigiZNao=document.getElementById("codigoEntidadeConteudoCorrigiZNao").value;
	var codigoEntidadeConteudoCorrigiZCromatografia=document.getElementById("codigoEntidadeConteudoCorrigiZCromatografia").value;
	
	for(var i = 2, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3) {
		
		if(selects[j + 1].value != codigoEntidadeConteudoCorrigiZSimValor){
			if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZCromatografia) {
				inputs[i].value = "";
	 			inputs[i].disabled = true;
	 			inputs[i].style.backgroundColor = "#EBEBE4";
			} else {
				inputs[i].disabled = false;
				inputs[i].style.backgroundColor = "white";
			}
    		inputs[i + 1].value = "";
			inputs[i + 1].disabled = true;
			inputs[i + 1].style.backgroundColor = "#EBEBE4";
		}
		
		if(selects[j].value != codigoEntidadeConteudoCorrigiPTNao) {
			inputs[i].value = "";
 			inputs[i].disabled = true;
 			inputs[i].style.backgroundColor = "#EBEBE4";
		}
		
    	if(inputs[i].value == ""){
    		if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    			inputs[i].value = "";
    			inputs[i].disabled = true;
				inputs[i].style.backgroundColor = "#EBEBE4";
    			selects[j].disabled = false;
    			inputs[i + 1].disabled = false;
    			inputs[i + 1].style.backgroundColor = "white";
    		}	
		}
    	
    	if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor && inputs[i + 1].value != ""){
    		inputs[i].disabled = true;
			inputs[i].style.backgroundColor = "#EBEBE4";
		}
	}
	
}

function habilitarDesabilitarNumeroFatorCorrecao(){
	
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	var codigoEntidadeConteudoCorrigiZSimValor=document.getElementById("codigoEntidadeConteudoCorrigiZSimValor").value;
	
	for(var i = 2, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3){    	
    	
    	if(inputs[i].value == ""){
    		if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    			selects[j].disabled = false;
    			selects[j + 1].disabled = false;
    			inputs[i].disabled = false;
    			inputs[i + 1].disabled = false;
    			inputs[i].style.backgroundColor = "white";
    			inputs[i + 1].style.backgroundColor = "white";
    		}	
		}
		
	
	}
}

function habilitarDesabilitarCamposCorrigePTZFatorPTZ(){
	
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	
	for(var i = 2, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3){    	
    	
    	if(inputs[i].value != ""){
    		inputs[i + 1].value = "";
    		inputs[i + 1].disabled = true;
    		selects[j].disabled = true;
    		selects[j + 1].disabled = true;
    		inputs[i + 1].style.backgroundColor = "#EBEBE4";
    	}
    	if(inputs[i].value == "" && inputs[i - 1].disabled != true){    		
    		inputs[i + 1].disabled = false;
    		selects[j].disabled = false;
    		selects[j + 1].disabled = false;
    		inputs[i + 1].style.backgroundColor = "white";
    	}
    	
    	habilitarDesabilitarCamposNumeroFatorCorrecaoFatorPTZ();
    	
    }
    
	
}

function habilitarDesabilitarCampoNumeroCorrecao(){
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	var codigoEntidadeConteudoCorrigiZSimValor=document.getElementById("codigoEntidadeConteudoCorrigiZSimValor").value;
	
	for(var i = 3, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3){    	
    	
    	if(inputs[i].value != "" && selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    		inputs[i - 1].value = "";
    		inputs[i - 1].disabled = true;
    		inputs[i - 1].style.backgroundColor = "#EBEBE4";
    	}
    	if(inputs[i].value == "" && selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    		inputs[i - 1].disabled = false;
    		inputs[i - 1].style.backgroundColor = "white";
    	}
    	
    }
}

</script>

<h1 class="tituloInterno">Incluir Press�o de Fornecimento<a href="<help:help>/cadastroclienteinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form:form method="post" action="incluirAlterarFaixaPressaoFornecimento" name="faixaPressaoFornecimentoForm" id="faixaPressaoFornecimentoForm">

	<input name="acao" type="hidden" id="acao" value="incluirAlterarFaixaPressaoFornecimento">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${faixaPressaoFornecimento.chavePrimaria}"/>
	<input name="codigoEntidadeConteudoCorrigiZSimValor" type="hidden" id="codigoEntidadeConteudoCorrigiZSimValor" value="${codigoEntidadeConteudoCorrigiZSimValor}"/>
	<input name="codigoEntidadeConteudoCorrigiPTNao" type="hidden" id="codigoEntidadeConteudoCorrigiPTNao" value="${codigoEntidadeConteudoCorrigiPTNao}"/>
	<input name="codigoEntidadeConteudoCorrigiZNao" type="hidden" id="codigoEntidadeConteudoCorrigiZNao" value="${codigoEntidadeConteudoCorrigiZNao}"/>
	<input name="codigoEntidadeConteudoCorrigiZCromatografia" type="hidden" id="codigoEntidadeConteudoCorrigiZCromatografia" value="${codigoEntidadeConteudoCorrigiZCromatografia}"/>
	<input name="indexFaixaPressaoExlcusao" type="hidden" id="indexFaixaPressaoExlcusao">
	<input type="hidden" name="fluxo" id="fluxo" value="incluir">
	<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">
	<input name="postBack" type="hidden" id="postBack" value="true">
	
	<fieldset id="pesquisarTarifa" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo campoObrigatorio" for="idSegmento">Segmento:</label>
			<select name="segmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${faixaPressaoFornecimento.segmento.chavePrimaria eq segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
		    
		    <label class="rotulo campoObrigatorio" for="idUnidadePressao"><span class="campoObrigatorioSimbolo">* </span>Unidade de Press�o:</label>
			<select name="unidadePressao" id="idUnidadePressao" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadePressao}" var="unidadePressao">
					<option value="<c:out value="${unidadePressao.chavePrimaria}"/>" <c:if test="${faixaPressaoFornecimento.unidadePressao.chavePrimaria == unidadePressao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidadePressao.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select>
						
		</fieldset>		
	
	<hr class="linhaSeparadora1">
	
	<fieldset id="tarifaFaixas" class="conteinerBloco" style="padding-top:20px;">
	<legend class="conteinerBlocoTitulo">Press�o de Fornecimento:</legend>
       
       <c:set var="i" value="0" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas"
			style="font-size:9px;" sort="list"
			name="sessionScope.listaFaixaPressaoFornecimento" id="dadosFaixa"
			decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			requestURI="#">

			<%--            <display:column style="text-align: center; width: 100px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>&nbsp;Habilitado" > --%>
			<%--                <input type="checkbox" class="id" id="faixasHabilitadas" name="faixasHabilitadas" value='${variacao.chavePrimaria}' <c:if test="${variacao.habilitado == true}">checked="checked"</c:if>> --%>
			<%--            </display:column> --%>

			<display:column title="Press�o Inicial" style="auto">
<!-- 				<input type="hidden" id="faixaInicial2" name="faixaInicial2" -->
<%-- 					value="${dadosFaixa.medidaMinimo}"> --%>
				<input type="text" style="width: 65px; margin-top: -11px;"
					class="campoTextoMaior" name="medidaMinimo" id="faixaInicial"
					 value="${dadosFaixa.medidaMinimo}"
					onkeypress="return formatarCampoDecimalPositivo(event,this,5,4);" />
			</display:column>

			<display:column title="Press�o Final" style="auto">
				<c:choose>
					<c:when test="${fn:length(listaFaixaPressaoFornecimento) > i+1}">
<!-- 						<input type="hidden" id="faixaSuperior" name="faixaSuperior" -->
<%-- 							value="${dadosFaixa.medidaMaximo}"> --%>
						<input type="text" style="width: 65px; margin-top: -11px;"
							class="campoTextoMaior" id="faixaFinal" name="medidaMaximo"
							disabled="disabled" value="${dadosFaixa.medidaMaximo}"
							onkeypress="return formatarCampoDecimalPositivo(event,this,5,4);">
					</c:when>
					<c:otherwise>
						<input type="text" style="width: 65px; margin-top: -11px;"
							class="campoTextoMaior" id="faixaFinal" name="medidaMaximo"
							value="${dadosFaixa.medidaMaximo}"
							onkeypress="return formatarCampoDecimalPositivo(event,this,5,4);">
					</c:otherwise>
				</c:choose>
			</display:column>

			<display:column title="Classe" style="auto">
				<select name="entidadeConteudo" id="classe" class="campoSelect" style="width: 50px; margin-top: -11px;">
						<c:forEach items="${listaClasse}" var="entidadeClasse">
							<option value="<c:out value="${entidadeClasse.chavePrimaria}"/>"
								<c:if test="${dadosFaixa.entidadeConteudo.chavePrimaria == entidadeClasse.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${entidadeClasse.descricao}" />
							</option>
						</c:forEach>
					</select>
			</display:column>

			<display:column title="Fator �nico Corre��o" style="auto">
					<input 	id="numeroFatorCorrecaoPTZPCS"
							type="text" 
							class="campoTexto valorFixoSemImposto" 
							style="width: 55px; 
							margin-top: -11px;"
							name="numeroFatorCorrecaoPTZPCS"
							value="${dadosFaixa.numeroFatorCorrecaoPTZPCS}"			
							onchange="habilitarDesabilitarCamposCorrigePTZFatorPTZ();"			
							onkeypress="return formatarCampoDecimal(event,this,2,4);">
			</display:column>			
			
			<display:column title="Corrige PT" style="auto">
				<select name="indicadorCorrecaoPT" id="corrigiPT" class="campoSelect" style="width: 70px; margin-top: -11px;"
				onchange="habilitarDesabilitarCamposNumeroFatorCorrecaoFatorPTZ();">
						<c:forEach items="${listaCorrigiPT}" var="corrigiPT">
							<option value="<c:out value="${corrigiPT.chavePrimaria}"/>"
								<c:if test="${dadosFaixa.indicadorCorrecaoPT.chavePrimaria == corrigiPT.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${corrigiPT.descricao}" />
							</option>
						</c:forEach>
					</select>
			</display:column>
			
			<display:column title="Corrige Z" style="auto">
				<select name="indicadorCorrecaoZ" id="corrigiZ" class="campoSelect" style="width: 70px; margin-top: -11px;"
						onchange="habilitarDesabilitarCamposNumeroFatorCorrecaoFatorPTZ();">
						<c:forEach items="${listaCorrigiZ}" var="corrigiZ">
							<option value="<c:out value="${corrigiZ.chavePrimaria}"/>"
								<c:if test="${dadosFaixa.indicadorCorrecaoZ.chavePrimaria == corrigiZ.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${corrigiZ.descricao}" />
							</option>
						</c:forEach>
					</select>
			</display:column>

			<display:column title="Fator Z" style="auto">
				<input type="text" class="campoTexto valorVariavelSemImposto" style="width: 40px; margin-top: -11px;"
						id="numeroFatorZ"
						name="numeroFatorZ" value="${dadosFaixa.numeroFatorZ}"
						onchange="habilitarDesabilitarCampoNumeroCorrecao();"
						onkeypress="return formatarCampoDecimal(event,this,2,4);">
			</display:column>

	<display:column title="Faixas"
					style="auto;"> 
 					<c:choose> 
						<c:when 
							test="${fn:length(sessionScope.listaFaixaPressaoFornecimento)  == 1}"> 
							<input type="button" class="botaoAdicionarFaixas botaoAdicionar"
								onClick='adicionarFaixaPressao(<c:out value="${dadosFaixa.medidaMinimo}"/>);'
								title="Adicionar" />
<!-- 							<input type="button" class="botaoRemoverFaixas botaoRemover" -->
<%-- 								onClick="removerFaixaPressao('<c:out value='${i}'/>')" --%>
<!-- 								title="Remover" /> -->
						</c:when>
						<c:otherwise>
							<input type="button" class="botaoAdicionarFaixas botaoAdicionar"
								onClick="adicionarFaixaPressao();" title="Adicionar" />
							<input type="button" class="botaoRemoverFaixas botaoRemover"
									onClick="removerFaixaPressao('<c:out value='${i}'/>')"
									title="Remover" />

						</c:otherwise>
					</c:choose>
				</display:column>

			<c:set var="i" value="${i+1}" />
		</display:table>
	</fieldset>
	
	</fieldset>
	
<fieldset class="conteinerBotoes"> 
	<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="exibirInclusaoFaixaPressaoFornecimento">
    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" onclick="formatarValoresPressao(), habilitarCamposDesabilitados('dadosFaixa'), verificarCamposTabelaDigitados();" type="submit">
    </vacess:vacess>
 </fieldset>
	
</form:form>
