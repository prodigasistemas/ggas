<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<script language="javascript">

$(document).ready(function(){
	
	//Estilos iniciais para os bot�es de Adicionar e Remover Faixas de Tarifas 
	    $("input.botaoAdicionarFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
	    $("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});

	    $("input.botaoAdicionarFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','cursor':'pointer'});
			}
		);

	    $("input.botaoRemoverFaixas").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
			}
		);

		$("input[name=maiorFaixa]").each(function(){
			aplicarMascaraNumeroInteiro(this);
		});
		$("input[name=menorFaixa]").each(function(){
			aplicarMascaraNumeroInteiro(this);
		});
		
		var inputsHidden = $("#dadosFaixa input[type='hidden']");
		
						
	});
	
function habilitarCamposDesabilitados(table_id){
	
    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    var selects=document.getElementById(table_id).getElementsByTagName('select');
    
    for(var i=0; i<inputs.length; ++i){
    	inputs[i].disabled=false;
    }
    for(var i=0; i<selects.length; ++i){
    	selects[i].disabled=false;
    }   
}

function adicionarFaixaPressao( chave ){
	habilitarCamposDesabilitados("dadosFaixa");
	submeter('faixaPressaoFornecimentoForm','adicionarFaixaPressao');
}

function removerFaixaPressao(chave){
	habilitarCamposDesabilitados("dadosFaixa");
	document.getElementById('indexFaixaPressaoExlcusao').value = chave;
	submeter('faixaPressaoFornecimentoForm','removerFaixaPressao')
}
	
function limparFormulario(){
	submeter('faixaPressaoFornecimentoForm','limparPressoesSemVinculo')
}

function cancelar(){
	location.href = '<c:url value="/exibirPesquisaFaixaPressaoFornecimento"/>';
}

function mostrarMensagemDeAviso(){
	alert('Press�o de Fornecimento vinculada a um Contrato Ponto Consumo, n�o pode ser removida.');
}

function habilitarDesabilitarCamposNumeroFatorCorrecaoFatorPTZ(){
	
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	var codigoEntidadeConteudoCorrigiZSimValor=document.getElementById("codigoEntidadeConteudoCorrigiZSimValor").value;
	var codigoEntidadeConteudoCorrigiPTNao=document.getElementById("codigoEntidadeConteudoCorrigiPTNao").value;
	var codigoEntidadeConteudoCorrigiZNao=document.getElementById("codigoEntidadeConteudoCorrigiZNao").value;
	var codigoEntidadeConteudoCorrigiZCromatografia=document.getElementById("codigoEntidadeConteudoCorrigiZCromatografia").value;
	
	for(var i = 2, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3) {
		
		if(selects[j + 1].value != codigoEntidadeConteudoCorrigiZSimValor){
			if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZCromatografia) {
				inputs[i].value = "";
	 			inputs[i].disabled = true;
	 			inputs[i].style.backgroundColor = "#EBEBE4";
			} else {
				inputs[i].disabled = false;
				inputs[i].style.backgroundColor = "white";
			}
    		inputs[i + 1].value = "";
			inputs[i + 1].disabled = true;
			inputs[i + 1].style.backgroundColor = "#EBEBE4";
		}
		
		if(selects[j].value != codigoEntidadeConteudoCorrigiPTNao) {
			inputs[i].value = "";
 			inputs[i].disabled = true;
 			inputs[i].style.backgroundColor = "#EBEBE4";
		}
		
    	if(inputs[i].value == ""){
    		if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    			inputs[i].value = "";
    			inputs[i].disabled = true;
				inputs[i].style.backgroundColor = "#EBEBE4";
    			selects[j].disabled = false;
    			inputs[i + 1].disabled = false;
    			inputs[i + 1].style.backgroundColor = "white";
    		}	
		}
    	
    	if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor && inputs[i + 1].value != ""){
    		inputs[i].disabled = true;
			inputs[i].style.backgroundColor = "#EBEBE4";
		}
	}
	
}

function habilitarDesabilitarNumeroFatorCorrecao(){
	
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	var codigoEntidadeConteudoCorrigiZSimValor=document.getElementById("codigoEntidadeConteudoCorrigiZSimValor").value;
	
	for(var i = 2, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3){    	
    	
    	if(inputs[i].value == ""){
    		if(selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    			selects[j].disabled = false;
    			selects[j + 1].disabled = false;
    			inputs[i].disabled = false;
    			inputs[i + 1].disabled = false;
    			inputs[i].style.backgroundColor = "white";
    			inputs[i + 1].style.backgroundColor = "white";
    		}	
		}
	}
}

function habilitarDesabilitarCamposCorrigePTZFatorPTZ(){
	
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	
	for(var i = 2, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3){    	
    	
    	if(inputs[i].value != ""){
    		inputs[i + 1].value = "";
    		inputs[i + 1].disabled = true;
    		selects[j].disabled = true;
    		selects[j + 1].disabled = true;
    		inputs[i + 1].style.backgroundColor = "#EBEBE4";
    	}
    	if(inputs[i].value == "" && inputs[i - 1].disabled != true){    		
    		inputs[i + 1].disabled = false;
    		selects[j].disabled = false;
    		selects[j + 1].disabled = false;
    		inputs[i + 1].style.backgroundColor = "white";
    	}
    	
    	habilitarDesabilitarCamposNumeroFatorCorrecaoFatorPTZ();
    }
}

function habilitarDesabilitarCampoNumeroCorrecao(){
	
	var inputs = $("#dadosFaixa input[type='text']");
	var selects = $("#dadosFaixa select");
	var codigoEntidadeConteudoCorrigiZSimValor=document.getElementById("codigoEntidadeConteudoCorrigiZSimValor").value;
	
	for(var i = 3, j = 1; i < inputs.length && j < selects.length; i = i + 4, j = j + 3){    	
    	
    	if(inputs[i].value != "" && selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    		inputs[i - 1].value = "";
    		inputs[i - 1].disabled = true;
    		inputs[i - 1].style.backgroundColor = "#EBEBE4";
    	}
    	if(inputs[i].value == "" && selects[j + 1].value == codigoEntidadeConteudoCorrigiZSimValor){
    		inputs[i - 1].disabled = false;
    		inputs[i - 1].style.backgroundColor = "white";
    	}
    }
}
	
</script>

<h1 class="tituloInterno">Alterar Press�o de Fornecimento<a href="<help:help>/cadastroclienteinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form:form method="post" action="incluirAlterarFaixaPressaoFornecimento" id="faixaPressaoFornecimentoForm" name="faixaPressaoFornecimentoForm">

	<input 
		name="acao" 
		type="hidden" 
		id="acao" 
		value="incluirAlterarFaixaPressaoFornecimento">
		
	<input 
		name="chavePrimaria" 
		type="hidden" 
		id="chavePrimaria" 
		value="${chavePrimaria}"/>
		
	<input 
		name="codigoEntidadeConteudoCorrigiZSimValor" 
		type="hidden" 
		id="codigoEntidadeConteudoCorrigiZSimValor" 
		value="${codigoEntidadeConteudoCorrigiZSimValor}"/>
		
	<input 
		name="codigoEntidadeConteudoCorrigiPTNao" 
		type="hidden" 
		id="codigoEntidadeConteudoCorrigiPTNao" 
		value="${codigoEntidadeConteudoCorrigiPTNao}"/>
		
	<input 
		name="codigoEntidadeConteudoCorrigiZNao" 
		type="hidden" id="codigoEntidadeConteudoCorrigiZNao" 
		value="${codigoEntidadeConteudoCorrigiZNao}"/>
		
	<input 
		name="codigoEntidadeConteudoCorrigiZCromatografia" 
		type="hidden" 
		id="codigoEntidadeConteudoCorrigiZCromatografia" 
		value="${codigoEntidadeConteudoCorrigiZCromatografia}"/>
		
	<input 
		name="indexFaixaPressaoExlcusao" 
		type="hidden" 
		id="indexFaixaPressaoExlcusao">
		
	<input 
		name="postBack" 
		type="hidden" 
		id="postBack" 
		value="true">
		
	<input 
		name="fluxo" 
		type="hidden" 
		id="fluxo" 
		value="alterar">
		
	<input 
		name="habilitado" 
		type="hidden" 
		id="habilitado" 
		value="${habilitado}"/>
	
	<fieldset id="pesquisarTarifa" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo campoObrigatorio" for="segmento">Segmento:</label>
			
			<input 
				type="hidden" 
				id="idSegmento" 
				name="segmento" 
				value="${faixaPressaoVO.segmento.chavePrimaria}"  >
			
			<select name="segmento" id="idSegmento" class="campoSelect" disabled="disabled" >
		    	<option value="-1">Selecione</option>
					<option value="<c:out value="${faixaPressaoVO.segmento.chavePrimaria}"/>"selected="selected">
						<c:out value="${faixaPressaoVO.segmento.descricao}"/>
					</option>		
		    </select><br />
		    
		    <label class="rotulo campoObrigatorio" for="idUnidadePressao"><span class="campoObrigatorioSimbolo">* </span>Unidade de Press�o:</label>
		    
		    <input 
		    	type="hidden" 
		    	id="idUnidadePressao" 
		    	name="unidadePressao" 
		    	value="${faixaPressaoVO.unidadePressao.chavePrimaria}">
			
			<select name="unidadePressao" id="idUnidadePressao" class="campoSelect" disabled="disabled">
		    	<option value="-1">Selecione</option>
					<option value="<c:out value="${faixaPressaoVO.unidadePressao.chavePrimaria}"/>" selected="selected">
						<c:out value="${faixaPressaoVO.unidadePressao.descricao}"/>
					</option>		
		    </select>
						
		</fieldset>	
			
	<hr class="linhaSeparadora1">
	
	<fieldset id="tarifaFaixas" class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Press�o de Fornecimento:</legend>
				
		<c:set var="i" value="0" />
		
			<display:table 
				class="dataTableGGAS dataTableCabecalho2Linhas"
				name="listaFaixaPressaoFornecimento" 
				sort="list"
				style="font-size:9px;"
				id="dadosFaixa"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				requestURI="">
				
				<display:column
					style="width: 25px;  line-height:24px; vertical-align:middle;"
					sortable="false"
					title="<input 
								type='checkbox' 
								name='checkAllAuto' 
								id='checkAllAuto'/>">
					<input 
						type="checkbox" 
						name="faixasHabilitadas"
						value="${dadosFaixa.chavePrimaria}"
						<c:if test="${dadosFaixa.habilitado == true}">checked="checked"</c:if>
						<c:if test="${ dadosFaixa.indicadorFaixaVinculada eq true }">disabled="disabled"</c:if>>
					<input 
						type="hidden" 
						name="indicadorFaixaVinculada" 
						id="indicadorFaixaVinculada" 
						value="${dadosFaixa.indicadorFaixaVinculada}">
				</display:column>
				
				<display:column 
					title="Press�o Inicial" 
					style="auto">
					<input 
						type="text" 
						class="campoTexto faixaFinal" 
						disabled="disabled"
						name="medidaMinimo" 
						value="${dadosFaixa.medidaMinimo}"
						style="width: 60px; 
						margin-top: -11px;"
						<c:if test="${ dadosFaixa.indicadorFaixaVinculada eq true }">
						disabled="disabled"</c:if>
						onkeypress="return formatarCampoDecimal(event,this,5,4);">
				</display:column>
				
				<display:column 
					title="Press�o Final" 
					style="auto">
				<c:choose>
					<c:when 
						test="${fn:length(listaFaixaPressaoFornecimento) > i+1}">
						<input 
							type="text" 
							class="campoTexto faixaFinal" 
							name="medidaMaximo"
							style="width: 60px; 
							margin-top: -11px;"
							value="${dadosFaixa.medidaMaximo}"
							disabled="disabled"
							onkeypress="return formatarCampoDecimal(event,this,5,4);">
					</c:when>
					<c:otherwise>
						<input 
							type="text" 
							class="campoTexto faixaFinal" 
							name="medidaMaximo"
							style="width: 60px; 
							margin-top: -11px;"
							value="${dadosFaixa.medidaMaximo}"
							<c:if 
								test="${ dadosFaixa.indicadorFaixaVinculada eq true }">
								disabled="disabled"</c:if>
							onkeypress="return formatarCampoDecimal(event,this,5,4);">
					</c:otherwise>
				</c:choose>
				</display:column>
				
				<display:column 
					title="Classe" 
					style="auto">
					
					<select 
						name="entidadeConteudo" 
						id="classe" 
						class="campoSelect"
						style="width: 70px; 
						margin-top: -11px;"
						<c:if test="${ dadosFaixa.indicadorFaixaVinculada eq true }">disabled="disabled"</c:if>>
						
						<c:forEach items="${listaClasse}" var="entidadeClasse">
							<option value="<c:out value="${entidadeClasse.chavePrimaria}"/>"
								<c:if test="${dadosFaixa.entidadeConteudo.chavePrimaria == entidadeClasse.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${entidadeClasse.descricao}" />
							</option>
						</c:forEach>
					</select>
					
				</display:column>
				
				<display:column 
					title="Fator �nico Corre��o" 
					style="auto">
					
					<input 	
						id="numeroFatorCorrecaoPTZPCS"
						type="text" 
						class="campoTexto valorFixoSemImposto"
						name="numeroFatorCorrecaoPTZPCS"
						value="${dadosFaixa.numeroFatorCorrecaoPTZPCS}"
						style="width: 55px; 
						margin-top: -11px;"
						onkeypress="return formatarCampoDecimal(event,this,2,4);">
						
				</display:column>				
				
				<display:column title="Corrige PT" style="auto">
				
					<select 
						name="indicadorCorrecaoPT" 
						id="corrigiPT" 
						class="campoSelect" 
						style="width: 70px; 
						margin-top: -11px;">
						
						<c:forEach items="${listaCorrigiPT}" var="corrigiPT">
							<option value="<c:out value="${corrigiPT.chavePrimaria}"/>"
								<c:if test="${dadosFaixa.indicadorCorrecaoPT.chavePrimaria == corrigiPT.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${corrigiPT.descricao}" />
							</option>
						</c:forEach>
						
					</select>
						
				</display:column>
				
				<display:column title="Corrige Z" style="auto">
				
					<select 
						name="indicadorCorrecaoZ" 
						id="corrigiZ" 
						class="campoSelect" 
						style="width: 70px; 
						margin-top: -11px;">
						
						<c:forEach items="${listaCorrigiZ}" var="corrigiZ">
							<option value="<c:out value="${corrigiZ.chavePrimaria}"/>"
								<c:if test="${dadosFaixa.indicadorCorrecaoZ.chavePrimaria == corrigiZ.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${corrigiZ.descricao}" />
							</option>
						</c:forEach>
						
					</select>
				</display:column>
				
				<display:column title="Fator Z" style="auto">
					<input 	
						id="numeroFatorZ" 
						type="text" 
						class="campoTexto valorVariavelSemImposto"
						name="numeroFatorZ" 
						value="${dadosFaixa.numeroFatorZ}"
						style="width: 40px; 
						margin-top: -11px;"
						onkeypress="return formatarCampoDecimal(event,this,2,4);">
				</display:column>
				
				<display:column title="Faixas" style="auto">
					<c:choose>
						<c:when
							test="${fn:length(listaFaixaPressaoFornecimento) > 1 && i > 0}">
							<input
								type="button" 
								class="botaoAdicionarFaixas botaoAdicionar"
								onClick='adicionarFaixaPressao(<c:out value="${dadosFaixa.medidaMinimo}"/>);'
								title="Adicionar" />

							<c:if test="${ dadosFaixa.indicadorFaixaVinculada eq true }">
								<input 
									type="button" 
									class="botaoRemoverFaixas botaoRemover"
									onClick="mostrarMensagemDeAviso();" 
									title="Remover" />
							</c:if>
							<c:if test="${ dadosFaixa.indicadorFaixaVinculada eq false }">
								<input 
									type="button"
									class="botaoRemoverFaixas botaoRemover"
									onClick="removerFaixaPressao('<c:out value='${i}'/>')"
									title="Remover" />
							</c:if>
						</c:when>
						<c:otherwise>
							<input 
								type="button" 
								class="botaoAdicionarFaixas botaoAdicionar"
								onClick="adicionarFaixaPressao();" 
								title="Adicionar" />


							<c:if test="${ dadosFaixa.indicadorFaixaVinculada eq true }">
								<c:if
									test="${ fn:length(listaFaixaPressaoFornecimento) > 1 }">
									<input 
										type="button" 
										class="botaoRemoverFaixas botaoRemover"
										onClick="mostrarMensagemDeAviso();" 
										title="Remover" />
								</c:if>

							</c:if>
							<c:if test="${ dadosFaixa.indicadorFaixaVinculada eq false }">
								<c:if
									test="${ fn:length(listaFaixaPressaoFornecimento) > 1 }">
									<input 
										type="button" 
										class="botaoRemoverFaixas botaoRemover"
										onClick="removerFaixaPressao('<c:out value='${i}'/>')"
										title="Remover" />
								</c:if>
							</c:if>



						</c:otherwise>
					</c:choose>
				</display:column>
				
			<c:set var="i" value="${i+1}" />
				
			</display:table>

		</fieldset>
	
	
	</fieldset>
	
<fieldset 
	class="conteinerBotoes"> 
	<input 
		name="Button"
		class="bottonRightCol2" 
		value="Cancelar" type="button" 
		onClick="cancelar();">
		
    <input 
    	name="Button" 
    	class="bottonRightCol2 bottonLeftColUltimo" 
    	value="Limpar" 
    	type="button" 
    	onclick="limparFormulario('dadosFaixa');">
    	
    <vacess:vacess param="exibirAtualizacaoFaixaPressaoFornecimento">
    	<input 
    		id="botaoSalvar" 
    		name="button" 
    		class="bottonRightCol2 botaoGrande1" 
    		value="Salvar"  
    		type="submit" 
    		onclick="habilitarCamposDesabilitados('dadosFaixa');">
    </vacess:vacess>
 </fieldset>
	
</form:form>
