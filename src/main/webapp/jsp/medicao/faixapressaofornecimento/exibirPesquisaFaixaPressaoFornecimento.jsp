<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Pesquisar Press�o de Fornecimento<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script language="javascript">
	
	function removerFaixaPressaoFornecimento(){
		var selecao = verificarSelecao();
	
		if (selecao == true) {	
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('faixaPressaoFornecimentoForm', 'excluirFaixaPressaoFornecimento');
			}
	    }
	}
	
	function alterarFaixaPressaoFornecimento(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('faixaPressaoFornecimentoForm', 'exibirAtualizacaoFaixaPressaoFornecimento');
	    }

	}
	
	function detalharFaixaPressaoFornecimento(indiceChave){
		
		document.forms[0].chavePrimaria.value = indiceChave;		
		submeter("faixaPressaoFornecimentoForm", "exibirDetalhamentoFaixaPressaoFornecimento");
		
	}
	
	function incluir() {
		submeter("faixaPressaoFornecimentoForm", "exibirInclusaoFaixaPressaoFornecimento");
	}
	
	function pesquisar() {
		submeter("faixaPressaoFornecimentoForm", "pesquisarFaixaPressaoFornecimento");
		
	}
	
	function limparFormulario(){
		document.getElementById("idSegmento").value = "-1";
		document.getElementById("idUnidadePressao").value = "-1";
		document.forms[0].habilitado[0].checked = true;
	}
	
	
</script>

<form:form method="post" action="pesquisarFaixaPressaoFornecimento" name="faixaPressaoFornecimentoForm" id="faixaPressaoFornecimentoForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarFaixaPressaoFornecimento">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >	
	
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarTarifa" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" for="idSegmento">Segmento:</label>
			<select name="segmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${faixaPressaoFornecimento.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select><br />
		    
		    <label class="rotulo" for="idUnidadePressao">Unidade de Press�o:</label>
			<select name="unidadePressao" id="idUnidadePressao" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadePressao}" var="unidadePressao">
					<option value="<c:out value="${unidadePressao.chavePrimaria}"/>" <c:if test="${faixaPressaoFornecimento.unidadePressao.chavePrimaria == unidadePressao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidadePressao.descricao}"/>
					</option>		
			    </c:forEach>	
		    </select>
						
		</fieldset>
		
		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
									
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq true}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq false}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			,<vacess:vacess param="pesquisarFaixaPressaoFornecimento">		
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" onclick="pesquisar()" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaFaixaPressaoFornecimentoVO ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS" name="listaFaixaPressaoFornecimentoVO" sort="list" id="faixaPressao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarFaixaPressaoFornecimento">
	        <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${ i }">
	        </display:column>
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${faixaPressao.habilitado}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        <display:column sortable="true" title="Segmento" sortProperty="segmento" style="text-align: center;">
	        	<a href="javascript:detalharFaixaPressaoFornecimento(${ i });"><span class="linkInvisivel"></span>
	            	<c:out value="${faixaPressao.segmento.descricao}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Unidade de Press�o" sortProperty="unidadePressao" style="text-align: center;">
	        	<a href="javascript:detalharFaixaPressaoFornecimento(${ i });"><span class="linkInvisivel"></span>
	            	<c:out value="${faixaPressao.unidadePressao.descricao}"/>
	            </a>
	        </display:column>	
	         <c:set var="i" value="${i+1}" />        
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
	
  		<c:if test="${not empty listaFaixaPressaoFornecimentoVO}">
  			<vacess:vacess param="exibirAtualizacaoFaixaPressaoFornecimento">
  				<input value="Alterar" class="bottonRightCol2" onclick="alterarFaixaPressaoFornecimento()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerFaixaPressaoFornecimento">
				<input id="botaoRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerFaixaPressaoFornecimento()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoFaixaPressaoFornecimento">
   			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>
	
</form:form>
