<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Manter Dados de Press�o e Temperatura</h1>
<p class="orientacaoInicial">Para manter os Dados de Press�o e Temperatura, selecione a Tipo de abrang�ncia, a �rea de abrang�ncia, a Unidade de temperatura, a Unidade de press�o e clique em <span class="destaqueOrientacaoInicial">Exibir</span> para gerar a listagem.<br />Para consultar meses e anos anteriores selecione as op��es correspondentes e clique em <span class="destaqueOrientacaoInicial">Exibir</span>.<br />Preencha os dados do(s) dia(s) na listagem e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>


<script language="javascript">
	$(document).ready(function(){
	   $(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});   
	});
	
	animatedcollapse.addDiv('pesquisaDadosTemperaturaPressao', 'fade=0,speed=400,persist=1,hide=0');
		
</script>

<form>

	<fieldset id="dadosTemperaturaPressao" class="conteinerPesquisarIncluirAlterar">

		<label class="rotulo campoObrigatorio" for="tipoAbrangencia"><span class="campoObrigatorioSimbolo">* </span>Tipo de abrang�ncia:</label>
		<select name="tipoAbrangencia" id="tipoAbrangencia" class="campoSelect campoHorizontal">
    		<option value="-1">Selecione</option>				
	    </select>
	    <img style="display:none" class="imgProc" id="imgProcIdRota" alt="Pesquisando..." title="Pesquisando..." src="<c:url value="/imagens/indicator.gif"/>">
	    
	    <label id="rotuloDataInicial" class="rotulo rotuloHorizontal" for="dataInicial">Data Inicial:</label>
    	<input class="campoData campoHorizontal" type="text" id="dataInicial" name="dataInicial" maxlength="10"><br />
	    <br />
	    
		<label class="rotulo campoObrigatorio" for="areaAbrangencia"><span class="campoObrigatorioSimbolo">* </span>�rea de abrang�ncia:</label>
		<select name="areaAbrangencia" id="areaAbrangencia" class="campoSelect" size="10" disabled="disabled">
    		<option value="-1" title="Selecione">Selecione</option>
	    </select>
	    
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>	    

	</fieldset>
	
</form> 