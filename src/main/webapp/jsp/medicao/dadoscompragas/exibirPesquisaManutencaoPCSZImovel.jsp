<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>

<h1 class="tituloInterno">
	Manter Dados de Compra de G�s - Pesquisar Im�vel<a class="linkHelp"
		href="<help:help>/consultadecomprapeloimvel.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para pesquisar um registro espec�fico, informe os dados nos campos
	abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>
	<!-- , ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p> -->

	<form:form method="post" action="pesquisarImovelManutencaoPCSZ" id="imovelPCSZForm" name="imovelPCSZForm">

		<script language="javascript">
			function limparCamposPesquisa() {
				document.getElementById('matricula').value = "";
				document.getElementById('nome').value = "";
			}

			function exibirManutencaoPCSZImovel(idImovel) {
				
				document.forms[0].idImovel.value = idImovel;
				submeter("imovelPCSZForm","exibirManutencaoPCSZImovel");
			}
			
			function pesquisar(){
				var matricula = $("#matricula").val();
				$("#idImovel").val(matricula);
			}
			
		</script>

		<input name="acao" type="hidden" id="acao" value="pesquisarImovelManutencaoPCSZ" />
		<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
		<input name="chavePrimaria" type="hidden" id="idImovel" value="" />

		<fieldset class="conteinerPesquisarIncluirAlterar">
			<fieldset id="dadosCompraPesquisaimovel">
				
				<label class="rotulo" id="rotuloMatricula" for="matricula">Matr�cula:</label>
				<input	class="campoTexto campoHorizontal" type="text" id="matricula"
						name="matricula" maxlength="8" size="8"
						value="${imovelDadosCompraGasVO.matricula}"
						onkeypress="return formatarCampoInteiro(event)"> 
					
					<label class="rotulo rotuloHorizontal" for="nome">Descri��o:</label> 
					<input 	class="campoTexto campoHorizontal" type="text" id="nome"
							name="nome" maxlength="50" size="40"
							value="${imovelDadosCompraGasVO.nome}"
							onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
			</fieldset>

			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="pesquisarImovelManutencaoPCSZ">
					<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
						value="Pesquisar" type="submit"
						onclick="submeter('imovelPCSZForm', 'pesquisarImovelManutencaoPCSZ'), pesquisar();">
				</vacess:vacess>
				<input name="Button" class="bottonRightCol bottonRightColUltimo"
					value="Limpar" type="button" onclick="limparCamposPesquisa();" />
			</fieldset>
		</fieldset>

		<c:if test="${listaImoveis ne null}">
			<hr class="linhaSeparadoraPesquisa" />
			<display:table export="false" class="dataTableGGAS" name="listaImoveis"
				sort="list" id="imovel" pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="pesquisarImovelManutencaoPCSZ">
				<display:column media="html" sortable="true"
					titleKey="IMOVEL_MATRICULA" style="width: 110px">
					<a
						href='javascript:exibirManutencaoPCSZImovel(<c:out value='${imovel.chavePrimaria}'/>);'>
						<c:out value='${imovel.chavePrimaria}' />
					</a>
				</display:column>
				<display:column media="html" sortable="true" title="Nome"
					style="text-align: left; width: 250px">
					<a
						href='javascript:exibirManutencaoPCSZImovel(<c:out value='${imovel.chavePrimaria}'/>);'>
						<c:out value='${imovel.nome}' />
					</a>
				</display:column>
				<display:column media="html" sortable="false" title="Inscri��o"
					style="width: 85px">
					<a
						href='javascript:exibirManutencaoPCSZImovel(<c:out value='${imovel.chavePrimaria}'/>);'>
						<c:out value='${imovel.inscricao}' />
					</a>
				</display:column>
				<display:column media="html" sortable="false" title="Endere�o"
					style="text-align: left">
					<a
						href='javascript:exibirManutencaoPCSZImovel(<c:out value='${imovel.chavePrimaria}'/>);'>
						<c:out value='${imovel.enderecoFormatado}' />
					</a>
				</display:column>
			</display:table>
		</c:if>

	</form:form>