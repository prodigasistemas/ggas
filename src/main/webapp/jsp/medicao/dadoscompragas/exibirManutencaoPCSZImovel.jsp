<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>

	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,persist=1,hide=0');

	<c:choose>
		<c:when test="${imovelPCSZ.inputModificado == true}">
			var inputModificado = true;
		</c:when>
		<c:otherwise>
			var inputModificado = false;
		</c:otherwise>
	</c:choose>
					
	$(document).ready(function(){
	
		$("#imovelPCSZ input[type='text']").change(function(){
			$("#inputModificado").attr({value: true });
			inputModificado = true;
		});
		
		$("#ano").change(function(){
			if ($("select#ano option:selected").val() != -1){
				$("#mes").attr('disabled', false);
				if ($("select#mes option:selected").val() != -1){
					$("#botaoExibirPCSImovel").attr('disabled', false);
				} else {
					$("#botaoExibirPCSImovel").attr('disabled', true);
				}
			} else {
				$("#mes").attr('disabled', true);
				$("select#mes").attr('value', -1);
				$("#botaoExibirPCSImovel").attr('disabled', true);
			}
		});
		
		$("#mes").change(function(){
			if ($("select#mes option:selected").val() != -1){
				$("#botaoExibirPCSImovel").attr('disabled', false);
			} else {
				$("#botaoExibirPCSImovel").attr('disabled', true);
			}
		});
		
		var anoParam = $("#anoParam").val();
		var mesParam = $("#mesParam").val();
		
		if(anoParam == "" || mesParam == ""){
			$("select#ano").attr('value', -1);
			$("select#mes").attr('value', -1);
		}else{
			$("select#ano").attr('value', anoParam);
			$("select#mes").attr('value', mesParam);
		}
		
		$("#ano,#mes").change();
		
	});
	
	function verificarModificacao() {
		if (inputModificado == true) {
			var confirmacao = window.confirm(' Tem certeza que deseja descartar as modifica��es? ')
			if (confirmacao == true) {
				inputModificado = false;
				document.forms[0].inputModificado.value = false;
				submeter('imovelPCSZForm', 'exibirManutencaoPCSZImovel');
			} else { 
				return false;
			}
		} else {
			inputModificado = false;
			document.forms[0].inputModificado.value = false;
			submeter('imovelPCSZForm', 'exibirManutencaoPCSZImovel');
		}
	}

	function voltar() {
		$("#idImovel").val("");
		submeter("imovelPCSZForm", "pesquisarImovelManutencaoPCSZ");
	}
	
</script>

<h1 class="tituloInterno">Manter PCS do Im�vel<a class="linkHelp" href="<help:help>/visualizaoeinclusodopcsdoimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<form:form method="post" action="manterPCSZImovel" id="imovelPCSZForm" name="imovelPCSZForm">

	<input name="chavePrimaria" type="hidden" id="idImovel" value="${imovelDadosCompraGasVO.chavePrimaria}"/>
	<input name="matricula" type="hidden" id="matricula" value="${imovelDadosCompraGasVO.matricula}"/>
	<input name="nome" type="hidden" id="nome" value="${imovelDadosCompraGasVO.nome}"/>
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value=""/>
	<input name="idMedidor" type="hidden" id="idMedidor" value=""/>
	<input name="acao" type="hidden" id="acao" value="manterPCSZImovel"/>
	<input name="inputModificado" type="hidden" id="inputModificado" value="${imovelPCSZ.inputModificado}"/>
	
	<input name="mesParam" type="hidden" id="mesParam" value="${imovelDadosCompraGasVO.mes}"/>
	<input name="anoParam" type="hidden" id="anoParam" value="${imovelDadosCompraGasVO.ano}"/>

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<a class="linkPesquisaAvancada" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
		<div id="dadosImovel">
			<fieldset class="coluna">
				<label class="rotulo">Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.nome}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.enderecoFormatado}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.cep}"/></span><br />
				<label class="rotulo">Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.descricaoComplemento}"/></span><br />
			</fieldset>
		</div>
	
		<fieldset id="dadosCompraCityGate" class="conteinerPesquisarIncluirAlterar">
	
		    <label class="rotulo rotuloHorizontal" id="rotuloAno" for="ano">Ano:</label>
			<select name="ano" id="ano" class="campoSelect campoHorizontal">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaAnos}" var="ano">
					<option value="<c:out value="${ano}"/>" <c:if test="${imovelPCSZ.ano == ano}">selected="selected"</c:if>>
						<c:out value="${ano}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    
		    <label class="rotulo rotuloHorizontal" id="rotuloMes" for="mes">M�s:</label>
			<select name="mes" id="mes" class="campoSelect campoHorizontal" disabled="disabled">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaMesesAno}" var="mesAno">
					<option value="<c:out value="${mesAno.key}"/>" <c:if test="${imovelPCSZ.mes == mesAno.key}">selected="selected"</c:if>>
						<c:out value="${mesAno.value}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    
		    <input tabindex="4" name="Button" id="botaoExibirPCSImovel" class="bottonRightCol2" disabled="disabled" value="Exibir" type="button" onclick="return verificarModificacao();">	   
	
		</fieldset>			
	</fieldset>
	
	
		<hr class="linhaSeparadoraPesquisa" />
		<c:set var="total" value="${fn:length(listaImovelPCSZVO)}" />
		<display:table class="dataTableGGAS" name="listaImovelPCSZVO" sort="list" id="imovelPCSZ" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirManutencaoPCSZImovel" >   		
			<display:column sortable="false"  title="Data">
				<c:out value="${imovelPCSZ.dataFormatada}"/>							
				<input type="hidden" id="dataVigencia${i}" name="dataVigencia" value="<c:out value="${imovelPCSZ.dataFormatada}"/>"/>				
				<input type="hidden" id="habilitado${i}" name="habilitado" value="<c:out value="${imovelPCSZ.habilitado}"/>"/>			
				<input type="hidden" id="permiteAlteracaoFatorZ${i}" name="permiteAlteracaoFatorZ" value="<c:out value="${imovelPCSZ.permiteAlteracaoFatorZ}"/>"/>				
			</display:column>
			<display:column sortable="false"  title="PCS">					
				<c:choose>
					<c:when test="${imovelPCSZ.habilitado ne true}">
						<input type="hidden" name="medidaPCS" value="<c:out value="${imovelPCSZ.medidaPCS}"/>"/>
						<input class="campoTexto" type="text" name="disabledMedidaPCS" value="<c:out value="${imovelPCSZ.medidaPCS}"/>" disabled="disabled"/>
					</c:when>
					<c:otherwise>
						<input tabindex="${total + i}" class="campoTexto" type="text" name="medidaPCS" maxlength="4" onkeypress="return formatarCampoInteiro(event);" value="<c:out value="${erroDadosInvalidos ? imovelDados.medidaPCS[i + 0] : imovelPCSZ.medidaPCS}"/>">
					</c:otherwise>
				</c:choose>					
			</display:column>
			<display:column sortable="false"  title="Fator Z">
				<c:choose>
					<c:when test="${imovelPCSZ.habilitado && imovelPCSZ.permiteAlteracaoFatorZ}">
						<input tabindex="${total*2 + i}" class="campoTexto" type="text" name="fatorZ" maxlength="11" onkeypress="return formatarCampoDecimal(event,this,2,8);" value="<c:out value="${erroDadosInvalidos ? imovelDados.fatorZ[i + 0] : imovelPCSZ.fatorZ}"/>">
					</c:when>
					<c:otherwise>
						<input type="hidden" name="fatorZ" value="<c:out value="${imovelPCSZ.fatorZ}"/>"/>
						<input class="campoTexto" type="text" name="disabledFatorZ" value="<c:out value="${imovelPCSZ.fatorZ}"/>" disabled="disabled"/>						
					</c:otherwise>
				</c:choose>					
			</display:column>
			<c:set var="i" value="${i+1}" />	 
		</display:table>
	
	
	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();"/>
		<c:if test="${listaImovelPCSZVO ne null}">
			<input name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="submit"/>
		</c:if>
	</fieldset>
	<token:token></token:token>
</form:form>
