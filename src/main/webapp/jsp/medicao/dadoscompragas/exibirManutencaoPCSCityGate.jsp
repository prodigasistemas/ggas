<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">City Gate<a class="linkHelp" href="<help:help>/dadosdacompradegspelacitygate.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para manter os Dados de Compra de G�s desde o �ltimo dia que foi preenchido, selecione o City Gate e clique em <span class="destaqueOrientacaoInicial">Exibir</span> para gerar a tabela de preenchimento.<br />
Para consultar meses e anos anteriores selecione as op��es correspondentes e clique em <span class="destaqueOrientacaoInicial">Exibir</span>.
<br />Ao finalizar o preenchimento dos dados clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>


<script language="javascript">

	<c:choose>
		<c:when test="${cityGateMedicaoForm.inputModificado == true}">
			var inputModificado = true;
		</c:when>
		<c:otherwise>
			var inputModificado = false;	
		</c:otherwise>
	</c:choose>			
					

	$(document).ready(function(){	
	
		/*Ao selecionar algum CityGate, habilita ou desabilita o bot�o Exibir,
		verifica se deve habilitar o botao Importar
		*/
		$("#idCityGate").change(function(){
			carregarAnosDisponiveis(this);
			selecionarCityGate();
			atualizaBotaoImportar();
		});

		/*Permite a sele��o do CityGate a partir do teclado, 
		disparando o carregamento dos anos dispon�veis*/
		$("#idCityGate").keyup(function(event) {
			if (event.keyCode == '38' || event.keyCode == '40'){
				carregarAnosDisponiveis(this);
				selecionarCityGate();
			}
		});

		$("#ano").change(function(){
			selecionarAno();
		});
		
		$("#ano").keypress(function(event){
			if (event.keyCode == '13' && $("#ano").val() == "-1"){
				verificarModificacao();
			}			
		});		

		/*Permite a sele��o do Ano a partir do teclado, e habilita o campo M�s*/
		$("#ano").keyup(function(event){
			selecionarAno();
		});
		
		$("#mes").change(function(){
			selecionarMes();
		});	

		$("#mes").keyup(function(event){
			selecionarMes();
		});		

		$("#cityGateMedicao input[type=text]").change(function(){
			$("#inputModificado").val("true");
			//inputModificado = true;
		});
		
		//Define a apar�ncia dos campos desabilitados. 
		$("#cityGateMedicao input[type=text]:disabled").each(function(){
			$(this).addClass("campoDesabilitado");
		});	
		
		/*Ao selecionar algum CityGate, habilita ou desabilita o bot�o Exibir,
		verifica se deve habilitar o botao Importar
		*/
		$("#arquivo").change(function(){
			atualizaBotaoImportar();
		});
		
		$("#limparArquivo").click(function() {
			$("#arquivo").attr({value: "" });
			atualizaBotaoImportar();
		});
		
	});
	
	
	function verificarModificacao(){
		
		if (inputModificado == true){
			
			var confirmacao = window.confirm(' Tem certeza que deseja descartar as modifica��es? ')
			if (confirmacao == true){
				inputModificado = false;
				document.forms[0].inputModificado.value = false;
				submeter('cityGateMedicaoForm', 'exibirDadosPCSCityGate');
			
			} else { 
				return false;
			}
		} else {
		
			inputModificado = false;
			document.forms[0].inputModificado.value = false;
			
			submeter('cityGateMedicaoForm', 'exibirDadosPCSCityGate');
		
		}
	}
	
	function carregarAnosDisponiveis(elem){
		
		var idCityGate = elem.value;
	   	var selectAnos = document.getElementById("ano");
		 
	   	selectAnos.length=0;
	   	var novaOpcao = new Option("Selecione","-1");
	    selectAnos.options[selectAnos.length] = novaOpcao;
	     
	   	if (idCityGate != "-1") {
	   		selectAnos.disabled = false;      		
	     	AjaxService.listarAnosDisponiveisCityGateMedicao( idCityGate, 
	         	function(anos) {            		      		         		
	             	for (key in anos){
	                 	var novaOpcao = new Option(anos[key], key);
	                     selectAnos.options[selectAnos.length] = novaOpcao;
	                 }
	                 selectAnos.disabled = false;
	             }
	         );
	   	} else {
			selectAnos.disabled = true;
			$("#botaoExibirPCSCityGate").attr("disabled","disabled");
	   	}
	   	
	   	$("#mes").val("-1").attr("disabled","disabled");
	   	$("#botaoExibirPCSCityGate").removeAttr("disabled");
	}
	
	function selecionarCityGate(){
		/*Verifica se algum CityGate est� selecionado 
		e habilita ou desabilita o bot�o Exibir*/
		if($("#idCityGate option:selected").val() != "-1"){
			$("#botaoExibirPCSCityGate").removeAttr("disabled");
			/*Exibe os Dados de Compra do G�s ao pressionar a tecla ENTER
			quando o foco est� no campo CityGate.*/
			$("#idCityGate").keypress(function(event) {
				if (event.keyCode == '13'){
					verificarModificacao();
				}			
			});
		} else {
			$("#botaoExibirPCSCityGate").attr("disabled","disabled");
		}
	}
	
	function selecionarAno(){
		if($("#ano option:selected").val() != "-1" && $("#mes option:selected").val() == -1){
			$("#mes").removeAttr("disabled");
			$("#botaoExibirPCSCityGate").attr("disabled","disabled");
		} 
		if($("#ano option:selected").val() != "-1" && $("#mes option:selected").val() != -1){
			$("#mes").removeAttr("disabled");
			$("#botaoExibirPCSCityGate").removeAttr("disabled");
		}
		if($("#ano option:selected").val() == "-1" && $("#mes option:selected").val() != -1){
			$("#mes").attr("disabled","disabled").val("-1");
		}		
	}

	function selecionarMes(){
		if ($("#mes option:selected").val() != -1){
			$("#botaoExibirPCSCityGate").removeAttr("disabled");
			$("#ano,#mes").keypress(function(event) {
				if (event.keyCode == '13' && $("#ano").val() != "-1"){
					verificarModificacao();
				}			
			});
		} else {
			$("#botaoExibirPCSCityGate").attr("disabled","disabled");
		}
	}
	
	function importarArquivoCityGate() {
		submeter("cityGateMedicaoForm", "importarPlanilhaPCSCityGate");
	}
	
	function atualizaBotaoImportar() {
		if ( ($("#idCityGate option:selected").val() != "-1") &&
			 ($('#arquivo').val() != '') ) {
			$("#botaoImportar").removeAttr("disabled");
			
		} else {
			$("#botaoImportar").attr("disabled","disabled");
		}
	}
	
	function init(){	
		selecionarCityGate();
		atualizaBotaoImportar();
	}	
	addLoadEvent(init);

</script>

<form method="post" action="manterPCSCityGate" id=cityGateMedicaoForm name="cityGateMedicaoForm" enctype="multipart/form-data">
	<input name="acao" type="hidden" id="acao" value="manterPCSCityGate"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="inputModificado" type="hidden" id="inputModificado" value="${cityGateMedicaoForm.inputModificado}"/>
	
	<fieldset id="dadosCompraCityGate" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="linha" >
			<label class="rotulo campoObrigatorio" id="rotuloCityGate" for="idTipoRota"><span class="campoObrigatorioSimbolo">* </span>City Gate:</label>
			<select tabindex="1" name="idCityGate" id="idCityGate" class="campoSelect campoSelect-min campoHorizontal">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaCityGates}" var="cityGate">
					<option value="<c:out value="${cityGate.chavePrimaria}"/>" <c:if test="${idCityGate == cityGate.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${cityGate.descricao}"/>
					</option>		
			    </c:forEach>
		    </select>
		    
		    <label class="rotulo rotuloHorizontal" id="rotuloAno" for="ano">Ano:</label>
			<select tabindex="2" name="ano" id="ano" class="campoSelect campoSelect-min campoHorizontal" <c:if test="${empty listaAnosDisponiveisCityGateMedicao}">disabled="disabled"</c:if>>
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaAnosDisponiveisCityGateMedicao}" var="anoDisponivel">
					<option value="<c:out value="${anoDisponivel}"/>" <c:if test="${ano == anoDisponivel}">selected="selected"</c:if>>
						<c:out value="${anoDisponivel}"/>
					</option>		
			    </c:forEach>	
		    </select>
	    
		    <label class="rotulo rotuloHorizontal" id="rotuloMes" for="mes">M�s:</label>
			<select tabindex="3" name="mes" id="mes" class="campoSelect campoSelect-min campoHorizontal" <c:if test="${empty listaAnosDisponiveisCityGateMedicao}">disabled="disabled"</c:if>>
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaMesesAno}" var="mesAno">
					<option value="<c:out value="${mesAno.key}"/>" <c:if test="${mes == mesAno.key}">selected="selected"</c:if>>
						<c:out value="${mesAno.value}"/>
					</option>		
			    </c:forEach>	
		    </select>
	    
		    <input tabindex="4" name="Button" id="botaoExibirPCSCityGate" class="bottonRightCol2" value="Exibir" type="button" onclick="verificarModificacao();">	    	    
		
		</fieldset>
	    <fieldset class="linha new-section">
	    	<fieldset class="coluna-sm">
			    <div>
				    <label class="rotulo rotuloHorizontal" id="rotuloArquivo" for="arquivo"><span class="campoObrigatorioSimbolo"></span>Arquivo Excel:</label>
					<input class="campoFile-sm campoHorizontal" type="file" id="arquivo" name="arquivo" title="Procurar" />
					<input type="button" value="Limpar Arquivo" class="bottonRightCol2" id="limparArquivo">
				</div>
			</fieldset>
			<fieldset class="coluna-direita">
				<label class="rotulo rotuloHorizontal" id="rotuloDiaInicial" for="diaInicial" >Dia Inicial:</label>
				<input class="campoTexto campoHorizontal" type="text" id="diaInicial"  name="diaInicial" maxlength="2" size="5" onkeypress="return formatarCampoInteiro(event,8);" value="${diaInicial}">
				
				<label class="rotulo rotuloHorizontal" id="rotuloDiaFinal" for="diaFinal">Dia Final:</label>
				<input class="campoTexto campoHorizontal" type="text" id="diaFinal"  name="diaFinal" maxlength="2" size="5" onkeypress="return formatarCampoInteiro(event,8);" value="${diaFinal}">
		
				<input name="button" id="botaoImportar" class="bottonRightCol2 last-button" value="Importar"  type="button" onclick="importarArquivoCityGate();">
			</fieldset>
		</fieldset>
	</fieldset>
	
	<c:if test="${listaCityGateMedicaoVO ne null}">
		<c:set var="total" value="${fn:length(listaCityGateMedicaoVO)}" />
		<hr class="linhaSeparadoraPesquisa" />		
		<display:table class="dataTableGGAS" name="listaCityGateMedicaoVO" sort="list" id="cityGateMedicao" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirManutencaoPCSCityGate">   		
			<display:column sortable="false" title="Data">
				<c:out value="${cityGateMedicao.dataFormatada}"/>							
				<input type="hidden" id="dataMedicao${i}" name="dataMedicao" value="<c:out value="${cityGateMedicao.dataFormatada}"/>"/>				
				<input type="hidden" id="habilitado${i}" name="habilitado" value="<c:out value="${cityGateMedicao.habilitado}"/>"/>			
			</display:column>
			<display:column sortable="false" title="PCS Supridora">					
				<c:choose>
					<c:when test="${cityGateMedicao.habilitado ne true}">
						<input type="hidden" name="ValorSupridora" value="<c:out value="${cityGateMedicao.valorSupridora}"/>"/>
						<input class="campoTexto" type="text" name="disabledSupridora" value="<c:out value="${cityGateMedicao.valorSupridora}"/>" disabled="disabled"/>
					</c:when>
					<c:otherwise>
						<input tabindex="${total + i}" class="campoTexto" type="text" name="ValorSupridora" maxlength="5" onkeypress="return formatarCampoInteiro(event, 5);" onKeyUp="validarCampoInteiroCopiarColar(this);"
							 value="<c:out value="${cityGateMedicao.valorSupridora}"/>">
					</c:otherwise>
				</c:choose>					
			</display:column>
			<display:column sortable="false"  title="PCS Distribuidora">
				<c:choose>
					<c:when test="${cityGateMedicao.habilitado ne true}">
						<input type="hidden" name="valorDistribuidora" value="<c:out value="${cityGateMedicao.valorDistribuidora}"/>"/>
						<input class="campoTexto" type="text" name="disabledDistribuidora" value="<c:out value="${cityGateMedicao.valorDistribuidora}"/>" disabled="disabled"/>
					</c:when>
					<c:otherwise>
						<input tabindex="${total*2 + i}" class="campoTexto" type="text" name="valorDistribuidora" maxlength="5" onkeypress="return formatarCampoInteiro(event, 5);" onKeyUp="validarCampoInteiroCopiarColar(this);" value="<c:out value="${cityGateMedicao.valorDistribuidora}"/>">
					</c:otherwise>
				</c:choose>					
			</display:column>
			<display:column sortable="false"  title="PCS Fixo">
				<c:choose>
					<c:when test="${cityGateMedicao.habilitado ne true}">
						<input type="hidden" name="valorFixo" value="<c:out value="${cityGateMedicao.valorFixo}"/>"/>
						<input class="campoTexto" type="text" name="disabledFixo" value="<c:out value="${cityGateMedicao.valorFixo}"/>" disabled="disabled"/>
					</c:when>
					<c:otherwise>
						<input tabindex="${total*3 + i}" class="campoTexto" type="text" name="valorFixo" maxlength="4" onkeypress="return formatarCampoInteiro(event, 4);" onKeyUp="validarCampoInteiroCopiarColar(this);" value="<c:out value="${cityGateMedicao.valorFixo}"/>">
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column sortable="false"  title="Volume Supridora">
				<c:choose>
					<c:when test="${cityGateMedicao.habilitado ne true}">
						<input type="hidden" name="volumeSupridora" value="<c:out value="${cityGateMedicao.volumeSupridora}"/>"/>
						<input class="campoTexto" type="text" name="disabledVolume" value="<c:out value="${cityGateMedicao.volumeSupridora}"/>" disabled="disabled"/>
					</c:when>
					<c:otherwise>
						<input tabindex="${total*4 + i}" class="campoTexto" type="text" name="volumeSupridora" maxlength="9" onkeypress="return formatarCampoInteiro(event, 9);" onKeyUp="validarCampoInteiroCopiarColar(this);" value="<c:out value="${cityGateMedicao.volumeSupridora}"/>">
					</c:otherwise>
				</c:choose>					
			</display:column>	
			<display:column sortable="false"  title="Volume Distribuidora">
				<c:choose>
					<c:when test="${cityGateMedicao.habilitado ne true}">
							<input type="hidden" name="volumeDistribuidora" value="<c:out value="${cityGateMedicao.volumeDistribuidora}"/>"/>
						<input class="campoTexto" type="text" name="disabledVolume" value="<c:out value="${cityGateMedicao.volumeDistribuidora}"/>" disabled="disabled"/>
					</c:when>
					<c:otherwise>
						<input tabindex="${total*5 + i}" class="campoTexto" type="text" name="volumeDistribuidora" maxlength="9" onkeypress="return formatarCampoInteiro(event, 9);" onKeyUp="validarCampoInteiroCopiarColar(this);" value="<c:out value="${cityGateMedicao.volumeDistribuidora}"/>">
					</c:otherwise>
				</c:choose>					
			</display:column>						
			<c:set var="i" value="${i+1}" />	 
		</display:table>
	</c:if>
	
	<c:if test="${listaCityGateMedicaoVO ne null}">
	<fieldset class="conteinerBotoes">			
	    <input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="submit">
	</fieldset>
	</c:if>
	
	<token:token></token:token>
</form>
