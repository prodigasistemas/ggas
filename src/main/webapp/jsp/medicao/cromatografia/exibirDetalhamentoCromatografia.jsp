<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js"></script>

<h1 class="tituloInterno">Detalhar Cromatografia<a href="<help:help>/cadastrodoclientedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script type="text/javascript">
function voltar(){
	submeter('cromatografiaForm','pesquisarCromatografia');
}

function alterar(){
	submeter('cromatografiaForm','exibirAlteracaoCromatografia');
}
</script>

<form method="post" action="alterarCromatografia" id="cromatografiaForm" name="cromatografiaForm">
	<input type="hidden" id="chavePrimariaCityGate" name="chavePrimariaCityGate" value="${chavePrimariaCityGate}"/>
	<input type="hidden" name="data" id="data" value="${ cromatografia.dataFormatada }">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${ cromatografia.chavePrimaria }">
	
	<!-- ********** CITY GATE ********** -->
	<display:table class="dataTableGGAS" name="listaCityGate" sort="list" id="cityGateDalista" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		<display:column sortable="true" sortProperty="descricao" title="City Gate - Descri��o" style="width: 130px">
            	<c:out value="${cityGateDalista.descricao}"/>
		</display:column>				
		
		<display:column  sortable="true" sortProperty="localidade" title="Localidade" style="width: 130px">
            	<c:out value="${cityGateDalista.localidade.descricao}"/>
		</display:column>
    </display:table>	
    <!-- **************************** -->
    		<br /><br />
   			<label class="rotulo" id="rotuloData" for="data">Data:</label>
			<span class="itemDetalhamento"><c:out value="${cromatografia.dataFormatada}"/></span><br /><br />
	<!-- ********** CAMPOS ********** -->
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
		<!-- *********** PRIMEIRA COLUNA *********** -->
		<fieldset id="cromatografiaDetalhamentoCol1" class="colunaEsq">
			<label class="rotulo" id="rotuloPcs" for="pcs" >PCS(Kcal/m�): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.pcs}"/></span><br />
			
			<label class="rotulo" id="rotuloMetano" for="metano" >Metano(CH4): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.metano}"/></span><br />
			
			<label class="rotulo" id="rotuloNitrogenio" for="nitrogenio" >Nitrog�nio(N2): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nitrogenio}"/></span><br />

			<label class="rotulo" id="rotuloDioxidoCarbono" for="dioxidoCarbono" >Dioxido Carbono(CO2): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.dioxidoCarbono}"/></span><br />
			
			<label class="rotulo" id="rotuloEtano" for="etano" >Etano(C2H6): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.etano}"/></span><br />

			<label class="rotulo" id="rotuloPropano" for="propano" >Propano(C3H8): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.propano}"/></span><br />
			
			<label class="rotulo" id="rotuloAgua" for="agua" >�gua(H2O): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.agua}"/></span><br />
		
			<label class="rotulo" id="rotuloSulfetoHidrogenio" for="sulfetoHidrogenio" >Sulfeto Hidrog�nio(H2s): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.sulfetoHidrogenio}"/></span><br />
		
		</fieldset>
		<!-- ************************************** -->
		
		<!-- *********** SEGUNDA COLUNA *********** -->
		<fieldset id="cromatografiaDetalhamentoCol2" class="colunaDir">
			
			<label class="rotulo" id="rotuloPressao" for="pressao" >Press�o(Kgf/cm� abs): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.pressao}"/></span><br />

			<label class="rotulo" id="rotuloHidrogenio" for="hidrogenio" >Hidrog�nio(H2): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.hidrogenio}"/></span><br />

			<label class="rotulo" id="rotuloMonoxidoCarbono" for="monoxidoCarbono" >Mon�xido Carbono(CO): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.monoxidoCarbono}"/></span><br />
			
			<label class="rotulo" id="rotuloOxigenio" for="oxigenio" >Oxig�nio(O2): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.oxigenio}"/></span><br />
			
			<label class="rotulo" id="rotuloIButano" for="iButano" >i-Butano(C4H10): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.iButano}"/></span><br />

			<label class="rotulo" id="rotuloNButano" for="nButano" >n-Butano(C4H10): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nButano}"/></span><br />


			<label class="rotulo" id="rotuloIPentano" for="iPentano" >i-Pentano(C5H12): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.iPentano}"/></span><br />

			<label class="rotulo" id="rotuloNPentano" for="nPentano" >n-Pentano: </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nPentano}"/></span><br />
			
		</fieldset>
		<!-- ************************************** -->
		
		<!-- *********** TERCEIRA COLUNA ***********-->
		<fieldset id="cromatografiaDetalhamentoCol2" class="colunaDir">
			
			<label class="rotulo" id="rotuloTemperatura" for="temperatura" >Temperatura(�C): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.temperatura}"/></span><br />
	
			<label class="rotulo" id="rotuloNHexano" for="nHexano" >n-Hexano(C6H14): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nHexano}"/></span><br />

			<label class="rotulo" id="rotuloNHeptano" for="nHeptano" >n-Heptano(C7H16): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nHeptano}"/></span><br />

			<label class="rotulo" id="rotuloArgonio" for="argonio" >Arg�nio(Ar): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.argonio}"/></span><br />

			<label class="rotulo" id="rotuloNOctano" for="nOctano" >n-Octano(C8H18): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nOctano}"/></span><br />

			<label class="rotulo" id="rotuloNNonano" for="nNonano" >n-Nonano(C9h20): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nNonano}"/></span><br />

			<label class="rotulo" id="rotuloNDecano" for="nDecano" >n-Decano(C10H22): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.nDecano}"/></span><br />

			<label class="rotulo" id="rotuloHelio" for="helio" >H�lio(He): </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.helio}"/></span><br />
			
		</fieldset>
	</fieldset>
	<br />
			<label class="rotulo" id="rotuloFatorCompressibilidade" for="fatorCompressibilidade" >Fator de Compressibilidade: </label>
			<span class="itemDetalhamento "><c:out value="${cromatografia.fatorFormatado}"/></span><br />
 	<br/>
 	<br/>
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
   		<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
	</fieldset>

</form>