<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- ********** CITY GATE ********** -->
	<display:table class="dataTableGGAS" name="listaCityGate" sort="list" id="cityGateDalista" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		<display:column  sortable="true" sortProperty="descricao" title="Descri��o" style="width: 130px">
            	<c:out value="${cityGateDalista.descricao}"/>
		</display:column>
        
        <display:column sortable="true" sortProperty="localidade" title="Localidade" style="width: 130px">
            	<c:out value="${cityGateDalista.localidade.descricao}"/>
        </display:column>
    </display:table>	
    <!-- **************************** -->
    
	<!-- ********** CAMPOS ********** -->
	<br />
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<!-- ************* DATA ************* -->
		<fieldset id="cromatografiaColTopoLeft" class="colunaEsq">
			<label class="rotulo campoObrigatorio2" id="rotuloDataCromatografia" for="data"><span class="campoObrigatorioSimbolo">* </span>Data:</label>
			<input class="campoData" type="text"  id="dataCromatografia" name="dataCromatografia" maxlength="10" onchange="obterPcs()" value="${dataCromatografia}" />
		<!-- ********************************* -->
	</fieldset>
	<br />
	<br />
	<br />
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
		<!-- *********** PRIMEIRA COLUNA *********** -->
		<fieldset id="cromatografiaCol1" class="colunaEsq">
			<input type="hidden" id="idCityGate" name="idCityGate" value="${idCityGate}" />
			
			<label class="rotulo" id="rotuloPcs" for="pcs" >PCS(Kcal/m�)</label>
			<input class="campoTexto" type="text" name="pcs" id="pcs" size="7" maxlength="7" value="${cromatografia.pcs}"  /><br />
			
			<label class="rotulo" id="rotuloMetano" for="metano" >Metano(CH4)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);" type="text"  name="metano" id="metano" size="7" maxlength="7" value="${cromatografia.metano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
			
			<label class="rotulo" id="rotuloNitrogenio" for="nitrogenio" >Nitrog�nio(N2)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);" type="text" name="nitrogenio" id="nitrogenio" size="7" maxlength="7" value="${cromatografia.nitrogenio}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
			
			<label class="rotulo" id="rotuloDioxidoCarbono" for="dioxidoCarbono" >Dioxido Carbono(CO2)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);" type="text" name="dioxidoCarbono" id="dioxidoCarbono" size="7" maxlength="7" value="${cromatografia.dioxidoCarbono}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
			
			<label class="rotulo" id="rotuloEtano" for="etano" >Etano(C2H6)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);" type="text" name="etano" id="etano" size="7" maxlength="7" value="${cromatografia.etano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloPropano" for="propano" >Propano(C3H8)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="propano" id="propano" size="7" maxlength="7" value="${cromatografia.propano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
			
			<label class="rotulo" id="rotuloAgua" for="agua" >�gua(H2O)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);" type="text" name="agua" id="agua"  maxlength="7" size="7" value="${cromatografia.agua}" />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
		
			<label class="rotulo" id="rotuloSulfetoHidrogenio" for="sulfetoHidrogenio" >Sulfeto Hidrog�nio(H2s)</label>
			<input class="campoTexto  componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="sulfetoHidrogenio" id="sulfetoHidrogenio" size="7" maxlength="7" value="${cromatografia.sulfetoHidrogenio}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
		
		</fieldset>
		<!-- ************************************** -->
		
		<!-- *********** SEGUNDA COLUNA *********** -->
		<fieldset id="cromatografiaCol2" class="colunaDir">
			
			<label class="rotulo" id="rotuloPressao" for="pressao" ><span class="campoObrigatorioSimbolo">* </span>Press�o(Kgf/cm� abs)</label>
			<input class="campoTexto  componente" onkeypress="return formatarCampoDecimal(event,this,4,8);"type="text" name="pressao" id="pressao" size="10" maxlength="13" value="${cromatografia.pressao}"  disabled/><br />

			<label class="rotulo" id="rotuloHidrogenio" for="hidrogenio" >Hidrog�nio(H2)</label>
			<input class="campoTexto  componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="hidrogenio" id="hidrogenio" size="7" maxlength="7" value="${cromatografia.hidrogenio}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloMonoxidoCarbono" for="monoxidoCarbono" >Mon�xido Carbono(CO)</label>
			<input class="campoTexto  componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="monoxidoCarbono" id="monoxidoCarbono" size="7" maxlength="7" value="${cromatografia.monoxidoCarbono}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
			
			<label class="rotulo" id="rotuloOxigenio" for="oxigenio" >Oxig�nio(O2)</label>
			<input class="campoTexto  componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="oxigenio" id="oxigenio" size="7" maxlength="7" value="${cromatografia.oxigenio}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
			
			<label class="rotulo" id="rotuloIButano" for="iButano" >i-Butano(C4H10)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);" type="text" name="iButano" id="iButano" size="7" maxlength="7" value="${cromatografia.iButano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloNButano" for="nButano" >n-Butano(C4H10)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="nButano" id="nButano" size="7" maxlength="7" value="${cromatografia.nButano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloIPentano" for="iPentano" >i-Pentano(C5H12)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="iPentano" id="iPentano" size="7" maxlength="7" value="${cromatografia.iPentano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloNPentano" for="nPentano" >n-Pentano</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="nPentano" id="nPentano" size="7" maxlength="7" value="${cromatografia.nPentano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
			
		</fieldset>
		<!-- ************************************** -->
		
		<!-- *********** TERCEIRA COLUNA ***********-->
		<fieldset id="cromatografiaCol2" class="colunaDir">
			
			<label class="rotulo" id="rotuloTemperatura" for="temperatura" ><span class="campoObrigatorioSimbolo">* </span>Temperatura(�C)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,4,4);"type="text" name="temperatura" id="temperatura" size="10" maxlength="10" value="${cromatografia.temperatura}"  disabled/><br />
	
			<label class="rotulo" id="rotuloNHexano" for="nHexano" >n-Hexano(C6H14)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="nHexano" id="nHexano" size="7" maxlength="7" value="${cromatografia.nHexano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloNHeptano" for="nHeptano" >n-Heptano(C7H16)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="nHeptano" id="nHeptano" size="7" maxlength="7" value="${cromatografia.nHeptano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloArgonio" for="argonio" >Arg�nio(Ar)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="argonio" id="argonio" size="7" maxlength="7" value="${cromatografia.argonio}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloNOctano" for="nOctano" >n-Octano(C8H18)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="nOctano" id="nOctano" size="7" maxlength="7" value="${cromatografia.nOctano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloNNonano" for="nNonano" >n-Nonano(C9h20)</label>
			<input class="campoTexto componente"onkeypress="return formatarCampoDecimal(event,this,2,4);" type="text" name="nNonano" id="nNonano" size="7" maxlength="7" value="${cromatografia.nNonano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotuloNDecano" for="nDecano" >n-Decano(C10H22)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="nDecano" id="nDecano" size="7" maxlength="7" value="${cromatografia.nDecano}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />

			<label class="rotulo" id="rotul3esxoHelio" for="helio" >H�lio(He)</label>
			<input class="campoTexto componente" onkeypress="return formatarCampoDecimal(event,this,2,4);"type="text" name="helio" id="helio" size="7" maxlength="7" value="${cromatografia.helio}"  />
			<label class="rotuloInformativo rotuloHorizontal" style="color: black">%</label><br />
		<br />
		</fieldset>
		<div id="calcularCromatografia">
			<br /> 
			<br /> 
			<!-- *********** BOTAO CALCULAR *********** -->
			<input type="hidden" id="calculado" name="calculado" value="${calculado}"/>
			<input name="button" id="calcularZ" class=" bottonRightCol2" value="Calcular Z"  type="button" onclick="calcularCromatografia();" >
			<input name="button" id="recalcularZ" class="bottonRightCol2" value="Recalcular Z"  type="button" >
			<!-- ************************************** -->
		</div>
	</fieldset>
		
	<fieldset class="conteinerBotoes"> 
			<label class="rotulo" id="rotuloFatorCompressibilidade" for="fatorCompressibilidade" ><span class="campoObrigatorioSimbolo">* </span>Fator de Compressibilidade: </label>
			<input class="campoTexto componente" type="text" name="fatorCompressibilidade" id="fatorCompressibilidade" size="15" maxlength="15" value="${cromatografia.fatorFormatado}"  />
			<br />
 	</fieldset>
 	<br/>
 	<br/>