<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="imovel" %>

<h1 class="tituloInterno">Pesquisar Cromatografia<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir um novo registro, clique em <span class="destaqueOrientacaoInicial">Exibir City Gate</span>, selecione o <span class="destaqueOrientacaoInicial">City Gate</span> e clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>
<script type="text/javascript">

$(document).ready(function(){
	//habilita calend�rio
	$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
});

/*************Pesquisa Imovel***************/
 
 
function exibirPopupPesquisaImovel() {
	popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function selecionarImovel(idSelecionado){
	var idImovel = document.getElementById("idImovel");
	var matriculaImovel = document.getElementById("matriculaImovel");
	var nomeFantasia = document.getElementById("nomeFantasiaImovel");
	var numeroImovel = document.getElementById("numeroImovel");
	var cidadeImovel = document.getElementById("cidadeImovel"); 
	var indicadorCondominio = document.getElementById("condominio");
	var indicadorCondominio = document.getElementById("condominio");
	
	if(idSelecionado != '') {				
		AjaxService.obterImovelPorChave( idSelecionado, {
           	callback: function(imovel) {	           		
           		if(imovel != null){  	           			        		      		         		
	               	idImovel.value = imovel["chavePrimaria"];
	               	matriculaImovel.value = imovel["chavePrimaria"];		               	
	               	nomeFantasia.value = imovel["nomeFantasia"];
	               	numeroImovel.value = imovel["numeroImovel"];
	               	cidadeImovel.value = imovel["cidadeImovel"];
	               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
               	}
        	}, async:false}
        	
        );	        
    } else {
   		idImovel.value = "";
    	matriculaImovel.value = "";
    	nomeFantasia.value = "";
		numeroImovel.value = "";
          	cidadeImovel.value = "";
          	indicadorCondominio.value = "";
   	}

	document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
	document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
	document.getElementById("numeroImovelTexto").value = numeroImovel.value;
	document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
	if(indicadorCondominio.value == 'true') {
		document.getElementById("indicadorCondominioImovelTexto1").checked = true;
	} else {
		document.getElementById("indicadorCondominioImovelTexto2").checked = true;
	}
	//Esconde a lista de cityGates e desmarca os checkboxs desta lista
	$('input:radio').removeAttr('checked');
    $(this).val('check all');  
    $("#cityGates").hide();
}

function limparFormulario(){
	document.getElementById('data').value = "";
	document.getElementById('descricaoCityGate').value = "";
	
	submeter('cromatografiaForm','exibirPesquisaCromatografia');
}

/*******************************************/
 
function pesquisarCityGate() {	
	submeter('cromatografiaForm','pesquisarCityGate');
}

function incluirCromatografia() {
		if(document.forms["cromatografiaForm"].eUmCityGate!=undefined && document.forms["cromatografiaForm"].eUmCityGate.value=="true"){
			document.forms["cromatografiaForm"].chavePrimariaCityGate.value = obterValorUnicoCheckboxSelecionado();
			if(document.forms["cromatografiaForm"].chavePrimariaCityGate.value=="undefined" || document.forms["cromatografiaForm"].chavePrimariaCityGate.value==null){
				alert("Antes de incluir, voc� deve selecionar uma City Gate");
				return;
			}
		}else{
			alert("Antes de incluir, voc� deve selecionar um City Gate");
			return;
		}
		submeter('cromatografiaForm','exibirInclusaoCromatografia');
}

function pesquisaCromatografia() {
	if(document.forms["cromatografiaForm"].eUmCityGate==undefined){
		document.forms["cromatografiaForm"].chavePrimariaCityGate.value=0;
	} else{
		if (document.forms["cromatografiaForm"].eUmCityGate.value=="true"){
			document.forms["cromatografiaForm"].chavePrimariaCityGate.value = obterValorUnicoCheckboxSelecionado();
			if(document.forms["cromatografiaForm"].chavePrimariaCityGate.value=="undefined" || document.forms["cromatografiaForm"].chavePrimariaCityGate.value==null){
				document.forms["cromatografiaForm"].chavePrimariaCityGate.value=0;
			}
			
		}else{
			document.forms["cromatografiaForm"].chavePrimariaCityGate.value=0;
		}
	}
	submeter('cromatografiaForm','pesquisarCromatografia');
}

function alterarCromatografia() {
	if(document.forms["cromatografiaForm"].eUmCityGate!="undefined" && document.forms["cromatografiaForm"].eUmCityGate.value=="false"){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms["cromatografiaForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('cromatografiaForm','exibirAlteracaoCromatografia');
	    }else{
			return;
	    }
	}
}

function detalharCromatografia(chave){
	document.forms["cromatografiaForm"].chavePrimaria.value = chave;
	submeter('cromatografiaForm','exibirDetalhamentoCromatografia');
}
function removerCromatografia(){
	var selecao = verificarSelecao();	
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('cromatografiaForm', 'removerCromatografia');
		}
	}
}

</script>

<form action="pesquisarCromatografia" id="cromatografiaForm" name="cromatografiaForm" method="post" >

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCromatografiaCol1" class="coluna">
			<label class="rotulo" id="rotuloDescricaoCityGate" for="descricaoCityGate	">Descri��o City Gate:</label>
			<input class="campoTexto" type="text" id="descricaoCityGate" name="descricaoCityGate" maxlength="10" value="${descricaoCityGate}">
		</fieldset>
		<fieldset id="pesquisarCromatografiaCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloData" for="data">Data:</label>
			<input class="campoData campoHorizontal" type="text" id="data" name="data" maxlength="10" value="${data}">
			</br>
		</fieldset>	
	
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" > 
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" > 	
		<input name="chavePrimariaCityGate" type="hidden" id="chavePrimariaCityGate" > 	
		
		<br />
		<br />
		<fieldset class="conteinerBotoesEsqPesquisar">
	   		<input name="pesquisarCG" class="bottonRightCol2" id="pesquisarCG" value="Exibir City Gate" type="button" onclick="pesquisarCityGate();" >
		</fieldset>
		<fieldset class="conteinerBotoesDirPesquisar">
			<input name="botaoPesquisar" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisaCromatografia();">
			<input name="limpar" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>	
		<fieldset id="cityGates">
			<c:if test="${listaCityGate ne null}">
				<input type="hidden" id="eUmCityGate" name="eUmCityGate" value="true"  />
				<hr  class="linhaSeparadoraPesquisa" />
				<display:table class="dataTableGGAS" name="listaCityGate" sort="list" id="cityGate" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
					
		 			<display:column style="width: 25px" sortable="false" title="">
			         	<input id="radioCityGateChavesPrimarias" type="radio" name="chavesPrimarias" id="chavesPrimarias" value="${cityGate.chavePrimaria}" />
			        </display:column>
			        
			        <display:column title="Ativo" style="width: 30px">
				     	<c:choose>
							<c:when test="${cityGate.habilitado == true}">
								<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
							</c:otherwise>
						</c:choose>
					</display:column>
		
					<display:column sortable="true" sortProperty="descricao" title="Descri��o" style="width: 130px">
			            	<c:out value="${cityGate.descricao}"/>
					</display:column>				
					
					<display:column  sortable="true" sortProperty="localidade" title="Localidade" style="width: 130px">
			            	<c:out value="${cityGate.localidade.descricao}"/>
					</display:column>
			        
			    </display:table>	
			</c:if>
		</fieldset>
		<fieldset id="listaDeCromatografias">
			<c:if test="${listaCromatografia ne null}">
			<input type="hidden" id="eUmCityGate" name="eUmCityGate" value="false"  />
				<hr  class="linhaSeparadoraPesquisa" />
				<display:table class="dataTableGGAS" name="listaCromatografia" sort="list" id="cromatografia" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		
		 			<display:column style="width: 25px" sortable="false" title="">
			         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" id="chavesPrimarias" value="${cromatografia.chavePrimaria}" />
			        </display:column>
			        
					<display:column sortable="true" sortProperty="data" title="Data" style="width: 130px">
					<a href="javascript:detalharCromatografia(<c:out value='${cromatografia.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            		<c:out value="${cromatografia.dataFormatada}"/>
	           	 	</a>
					</display:column>				
					
					<display:column  sortable="true" sortProperty="cityGate" title="City Gate" style="width: 130px">
					<a href="javascript:detalharCromatografia(<c:out value='${cromatografia.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${cromatografia.cityGate.descricao}"/>
			        </a>
					</display:column>
			        
			        <display:column sortable="true" sortProperty="cityGate.localidade.descricao" title="Localidade" style="width: 130px">
			       	<a href="javascript:detalharCromatografia(<c:out value='${cromatografia.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${cromatografia.cityGate.localidade.descricao}"/>
					</a>
			        </display:column>
			        
			        <display:column sortable="true" sortProperty="pressao" title="Press�o" style="width: 130px">
			        <a href="javascript:detalharCromatografia(<c:out value='${cromatografia.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${cromatografia.pressao}"/>
			        </a>
			        </display:column>

			        <display:column sortable="true" sortProperty="temperatura" title="Temperatura" style="width: 130px">
			        <a href="javascript:detalharCromatografia(<c:out value='${cromatografia.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${cromatografia.temperatura}"/>
			        </a>
			        </display:column>

			        <display:column sortable="true" sortProperty="fatorFormatado" title="Fator de Compressibilidade" style="width: 130px">
			        <a href="javascript:detalharCromatografia(<c:out value='${cromatografia.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${cromatografia.fatorFormatado}"/>
			        </a>
			        </display:column>
			    </display:table>	
			</c:if>
		</fieldset>
	</form>
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaCromatografia}">
			<input name="alterarCromatografia" id="alterarCromatografia" value="Alterar" class="bottonRightCol2" onclick="alterarCromatografia();" type="button">
			<input id="botaoRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerCromatografia()" type="button">
   		</c:if>
		<input name="incluir" id="incluir" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluirCromatografia()" type="button">
	</fieldset>

	