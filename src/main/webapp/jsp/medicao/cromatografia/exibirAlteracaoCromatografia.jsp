<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js"> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js"></script>

<h1 class="tituloInterno">Alterar Cromatografia<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script type="text/javascript">
$(document).ready(function(){
	//habilita calend�rio
	$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy',
	  onSelect: function(dateText) {
		  $(this).change();
	  }
	});
	
	var metano = "${cromatografia.metano}";
	var pressao = "${cromatografia.pressao}";
	var temperatura = "${cromatografia.temperatura}";
	var agua = "${cromatografia.agua}";
	var iButano = "${cromatografia.iButano}";
	var nHeptano = "${cromatografia.nHeptano}";
	var argonio = "${cromatografia.argonio}";
	var iPentano = "${cromatografia.iPentano}";
	var nHexano = "${cromatografia.nHexano}";
	var dioxidoCarbono = "${cromatografia.dioxidoCarbono}";
	var nitrogenio = "${cromatografia.nitrogenio}";
	var etano = "${cromatografia.etano}";
	var monoxidoCarbono = "${cromatografia.monoxidoCarbono}";
	var nNonano = "${cromatografia.nNonano}";
	var helio = "${cromatografia.helio}";
	var nButano = "${cromatografia.nButano}";
	var nOctano = "${cromatografia.nOctano}";
	var hidrogenio = "${cromatografia.hidrogenio}";
	var nDecano = "${cromatografia.nDecano}";
	var nPentano = "${cromatografia.nPentano}";
	var oxigenio = "${cromatografia.oxigenio}";
	var propano = "${cromatografia.propano}";
	var sulfetoHidrogenio = "${cromatografia.sulfetoHidrogenio}";
	var fatorCompressibilidade = "${cromatografia.fatorCompressibilidade}";
	var pcs = "${cromatografia.pcs}";
	
	var componentes = [metano, pressao, temperatura, agua, iButano, nHeptano, argonio, iPentano, nHexano, dioxidoCarbono, nitrogenio, etano, monoxidoCarbono, nNonano, helio, nButano, nOctano, hidrogenio, nDecano, nPentano, oxigenio, propano, sulfetoHidrogenio, fatorCompressibilidade, pcs];
	var nomeComponente = new Array("metano", "pressao", "temperatura", "agua", "iButano", "nHeptano", "argonio", "iPentano", "nHexano", "dioxidoCarbono", "nitrogenio", "etano", "monoxidoCarbono", "nNonano", "helio", "nButano", "nOctano", "hidrogenio", "nDecano", "nPentano", "oxigenio", "propano", "sulfetoHidrogenio", "fatorCompressibilidade", "pcs");
	for (i = 0; i < componentes.length; i++) {
		var item = String(componentes[i]);
		if(item.indexOf(".")!=-1){
			var itemAux=item.replace(".",",");
			document.getElementById(String(nomeComponente[i])).value = itemAux;
		}
	}
	
	$("#recalcularZ").hide();
	$("#pcs").attr("disabled", "disabled");
	<c:choose>
		<c:when test="${calculado eq 'true'}">
		$(".componente").attr("disabled", "disabled");
   	 	$("#calcularZ").hide();
   	 	$("#recalcularZ").show();
		</c:when>
	</c:choose>	
	
	$("#recalcularZ").click(function(){
	    $("#recalcularZ").hide();
	    $("#calcularZ").show();
	    $(".componente").removeAttr("disabled");
	    $("#calculado").val("false");
	});
});


function cancelar(){
	location.href = '<c:url value="/exibirPesquisaCromatografia"/>';
}

function removerEspacoInicio(elem){
	return elem.replace(/^\s{1}/,"");
}
function tratarDecimal(){
	$("input:text").each(function(){	
        this.value=this.value.replace(/,/g, ".");
    });
}
function alterar(){
	if($("#calculado").val()=="false"){
		alert("Execute o c�lculo da Cromatografia!");
		return;
	}
	$("#pcs").removeAttr("disabled");
	$(".componente").removeAttr("disabled");
	tratarDecimal();
	submeter('cromatografiaForm','alterarCromatografia');
}
function calcularCromatografia(){
	var metano = document.getElementById("metano").value;
	if(metano==""){ metano="0,0"; }
	
	var pressao = document.getElementById("pressao").value;
	if(pressao=="" || parseFloat(pressao.replace(",", "."))==0){
		alert("Informe a Press�o");
		return;
	}
	var temperatura = document.getElementById("temperatura").value;
	if(temperatura==""|| parseFloat(temperatura.replace(",", "."))==0 ){
		alert("Informe a Temperatura");
		return;
	}
	var agua = document.getElementById("agua").value;
	if(agua==""){ agua="0,0"; }
	
	var iButano = document.getElementById("iButano").value; 
	if(iButano==""){ iButano="0,0"; }
	
	var nHeptano = document.getElementById("nHeptano").value; 
	if(nHeptano==""){ nHeptano="0,0"; }
	
	var argonio = document.getElementById("argonio").value;
	if(argonio==""){ argonio="0,0"; }
	
	var iPentano = document.getElementById("iPentano").value;
	if(iPentano==""){ iPentano="0,0"; }
	
	var nHexano = document.getElementById("nHexano").value;
	if(nHexano==""){ nHexano="0,0"; }
	
	var dioxidoCarbono = document.getElementById("dioxidoCarbono").value;
	if(dioxidoCarbono==""){ dioxidoCarbono="0,0"; }
	
	var nitrogenio = document.getElementById("nitrogenio").value;
	if(nitrogenio==""){ nitrogenio="0,0"; }
	
	var etano = document.getElementById("etano").value;
	if(etano==""){ etano="0,0"; }
	
	var monoxidoCarbono = document.getElementById("monoxidoCarbono").value;
	if(monoxidoCarbono==""){ monoxidoCarbono="0,0"; }
	
	var nNonano = document.getElementById("nNonano").value;
	if(nNonano==""){ nNonano="0,0"; }
	
	var helio = document.getElementById("helio").value;
	if(helio==""){ helio="0,0"; }
	
	var nButano = document.getElementById("nButano").value;
	if(nButano==""){ nButano="0,0"; }
	
	var nOctano = document.getElementById("nOctano").value;
	if(nOctano==""){ nOctano="0,0"; }
	
	var hidrogenio = document.getElementById("hidrogenio").value;
	if(hidrogenio==""){ hidrogenio="0,0"; }
	
	var nDecano = document.getElementById("nDecano").value;
	if(nDecano==""){ nDecano="0,0"; }
	
	var nPentano = document.getElementById("nPentano").value;
	if(nPentano==""){ nPentano="0,0"; }
	
	var oxigenio = document.getElementById("oxigenio").value;
	if(oxigenio==""){ oxigenio="0,0"; }
	
	var propano = document.getElementById("propano").value;
	if(propano==""){ propano="0,0"; }
	
	var sulfetoHidrogenio = document.getElementById("sulfetoHidrogenio").value;
	if(sulfetoHidrogenio==""){ sulfetoHidrogenio="0,0"; }
	
	var fatorCompressibilidade = document.getElementById("fatorCompressibilidade").value;
	
	var somaDosComponentes =  parseFloat(metano.replace(",", ".")) + parseFloat(nitrogenio.replace(",", ".")) + parseFloat(dioxidoCarbono.replace(",", ".")) + parseFloat(etano.replace(",", ".")) + parseFloat(propano.replace(",", ".")) + parseFloat(agua.replace(",", ".")) + parseFloat(sulfetoHidrogenio.replace(",", ".")) + parseFloat(hidrogenio.replace(",", ".")) + parseFloat(monoxidoCarbono.replace(",", ".")) + parseFloat(oxigenio.replace(",", ".")) + parseFloat(iButano.replace(",", ".")) + parseFloat(nButano.replace(",", ".")) + parseFloat(iPentano.replace(",", ".")) + parseFloat(nPentano.replace(",", ".")) + parseFloat(nHexano.replace(",", ".")) + parseFloat(nHeptano.replace(",", ".")) + parseFloat(nOctano.replace(",", ".")) + parseFloat(nNonano.replace(",", ".")) + parseFloat(nDecano.replace(",", ".")) + parseFloat(helio.replace(",", ".")) + parseFloat(argonio.replace(",", "."));
	somaDosComponentes = parseFloat(somaDosComponentes.toFixed(4));
	if(somaDosComponentes!=100){
		alert("A soma de todos os componentes deve ser igual a 100");
		return;
	}
	//A ordem dos componentes n�o podem ser alteradas no array.  
	var campos = metano+";"+nitrogenio+";"+dioxidoCarbono+";"+etano+";"+propano+";"+agua+";"+sulfetoHidrogenio+";"+hidrogenio+";"+monoxidoCarbono+";"+oxigenio+";"+iButano+";"+nButano+";"+iPentano+";"+nPentano+";"+nHexano+";"+nHeptano+";"+nOctano+";"+nNonano+";"+nDecano+";"+helio+";"+argonio+";"+pressao+";"+temperatura+";" +0;
	var nomeComponentes = "metano,nitrogenio,dioxidoCarbono,etano,propano,agua,sulfetoHidrogenio,hidrogenio,monoxidoCarbono,oxigenio,iButano,nButano,iPentano,nPentano,nHexano,nHeptano,nOctano,nNonano,nDecano,helio,argonio,pressao,temperatura,fatorCompressibilidade";

	AjaxService.calcularCromatografia(campos, nomeComponentes, {
          	callback: function(map) {	           		
            	document.getElementById("metano").value = map["metano"];
            	document.getElementById("pressao").value = map["pressao"];
            	document.getElementById("temperatura").value = map["temperatura"];
            	document.getElementById("agua").value = map["agua"];
            	document.getElementById("iButano").value = map["iButano"];
            	document.getElementById("nHeptano").value = map["nHeptano"];
            	document.getElementById("argonio").value = map["argonio"]; 
            	document.getElementById("iPentano").value = map["iPentano"];
            	document.getElementById("nHexano").value = map["nHexano"];
            	document.getElementById("dioxidoCarbono").value = map["dioxidoCarbono"];
            	document.getElementById("nitrogenio").value = map["nitrogenio"];
            	document.getElementById("etano").value = map["etano"];
            	document.getElementById("monoxidoCarbono").value = map["monoxidoCarbono"];
            	document.getElementById("nNonano").value = map["nNonano"];
            	document.getElementById("helio").value = map["helio"];
            	document.getElementById("nButano").value = map["nButano"];
            	document.getElementById("nOctano").value = map["nOctano"];
            	document.getElementById("hidrogenio").value = map["hidrogenio"];
            	document.getElementById("nDecano").value = map["nDecano"];
            	document.getElementById("nPentano").value = map["nPentano"]; 
            	document.getElementById("oxigenio").value = map["oxigenio"];
            	document.getElementById("propano").value = map["propano"];
            	document.getElementById("sulfetoHidrogenio").value = map["sulfetoHidrogenio"];
            	document.getElementById("fatorCompressibilidade").value = map["fatorCompressibilidade"];
              	
            	$(".componente").attr("disabled", "disabled");
           	 	$("#calcularZ").hide();
           	 	$("#recalcularZ").show();
           	 	$("#calculado").val("true");
           	 	
       	}, async:false});	        
}
function validarData(){

    var data = $("#dataCromatografia").val();
    var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    if (data == null || data == "" || !pattern.test(data))
    {
        return false;
    }
    else{
        return true
    }
}
function obterPcs(){
	if(!validarData()){
		alert("Data Inv�lida");
		return;
	}
	var data = $("#dataCromatografia").val();
	var idCityGate = $("#idCityGate").val();
	
	AjaxService.obterPcs(data, idCityGate, {
      	callback: function(map) {	           		
      		$("#pcs").val(map["pcs"]);
      		$("#dataCromatografia").val(map["dataString"]);
      		document.getElementById("dataCromatografia").value = map["dataString"];
      		$("#idCityGate").val(map["idCityGate"]);
   	}, async:false});	   
}
</script>

<form method="post" action="alterarCromatografia" id="cromatografiaForm" name="cromatografiaForm" modelAttribute="Cromatografia">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${ cromatografia.chavePrimaria }">
	<input type="hidden" name="versao" id="versao" value="${ cromatografia.versao }">
	
	<jsp:include page="/jsp/medicao/cromatografia/camposCromatografia.jsp"></jsp:include>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="button" onclick="alterar();">
	</fieldset>

</form>