<style>
    #tablePesquisaPontoConsumo thead th:nth-child(1){
        width: 20px !important;
    }
</style>
<script>
    $(document).ready(function() {
        $(".campoData").datepicker({
            changeYear: true,
            maxDate: '+0d',
            showOn: 'button',
            buttonImage: $('#imagemCalendario').val(),
            buttonImageOnly: true,
            buttonText: 'Exibir Calend�rio',
            dateFormat: 'dd/mm/yy'
        });
    });
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<input name="imagemCalendario" type="hidden" id="imagemCalendario" value="<c:url value="/imagens/calendario.png"/>">
<div class="modal fade" id="modalPontoConsumo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Pesquisar pontos de consumo n�o roteirizados</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-xl-12">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label class="float-none">Situa��o do Contrato:</label>
                            <select id="situacaoContrato" class="form-control form-control-sm">
                                <option value="-1">Selecione</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <label class="float-none">Situa��o do Medidor:</label>
                            <select id="situacaoMedidor"  class="form-control form-control-sm">
                                <option value="-1">Selecione</option>
                                <option value="1">Ativo</option>
                                <option value="0">Aguardando Ativa��o</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12">
                            <label>Data de Assinatura do Contrato:</label>
                            <div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
                                <input class="form-control form-control-sm campoData" type="text" id="dataInicioAssinaturaContrato" maxlength="10">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">at�</span>
                                </div>
                                <input class="form-control form-control-sm campoData" type="text" id="dataFimAssinaturaContrato" maxlength="10">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12">
                            <label>Data de Ativa��o do Medidor:</label>
                            <div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
                                <input class="form-control form-control-sm campoData" type="text" id="dataInicioAtivacaoMedidor" name="dataInicio" maxlength="10">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">at�</span>
                                </div>
                                <input class="form-control form-control-sm campoData" type="text" id="dataFimAtivacaoMedidor" name="dataFim" maxlength="10">
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12 col-lg-12 col-md-12 pt-3">
                            <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary float-right mr-2" id="botaoBuscarPontoConsumo">Pesquisar</button>
                        </div>
                    </div>
				</div>
				<div class="containerTabelaPontoConsumo" style="display: none">
					<hr/>
					<div class="table-responsive pt-3">
						<table id="tablePesquisaPontoConsumo" class="table table-bordered table-striped table-hover" style="width: 100% !important;">
							<thead class="thead-ggas-bootstrap">
                                <th scope="col" class="text-center" style="width: 20px !important"></th>
								<th scope="col" class="text-center">Im�vel</th>
								<th scope="col" class="text-center">Ponto de Consumo</th>
								<th scope="col" class="text-center">Segmento</th>
								<th scope="col" class="text-center">Ramo de Atividade</th>
								<th scope="col" class="text-center">Situa��o Consumo</th>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
