<!--
 Copyright (C) <2011> GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

 Este arquivo ? parte do GGAS, um sistema de gest?o comercial de Servi?os de Distribui??o de G?s

 Este programa ? um software livre; voc? pode redistribu?-lo e/ou
 modific?-lo sob os termos de Licen?a P?blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers?o 2 da Licen?a.

 O GGAS ? distribu?do na expectativa de ser ?til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl?cita de
 COMERCIALIZA??O ou de ADEQUA??O A QUALQUER PROP?SITO EM PARTICULAR.
 Consulte a Licen?a P?blica Geral GNU para obter mais detalhes.

 Voc? deve ter recebido uma c?pia da Licen?a P?blica Geral GNU
 junto com este programa; se n?o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/rota/pesquisa/index.js"></script>

<script type="text/javascript">
	
function excluirRota(){
	var selecao = verificarSelecao();
	if (selecao == true) {
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('rotaForm', 'excluirRota');
		}
    }
}
</script>
<div class="bootstrap">
	<form:form method="post" action="pesquisarRota" id="rotaForm" name="rotaForm">

		<div class="card">
			<div class="card-header">
            	<h5 class="card-title mb-0">Pesquisar Rota</h5>
        	</div>
        	<div class="card-body">
        		<input name="acao" type="hidden" id="acao" value="pesquisarRota">
				<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
				<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
				<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
				
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em
        			<strong>Pesquisar</strong>, ou clique apenas em <strong>Pesquisar</strong> para exibir todos. Para incluir um novo
        			 registro clique em <strong>Incluir</strong>
   			 	</div>
        		<div class="row">
        			<div class="col-md-6">
        				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idPeriodicidade" >Periodicidade da Rota:</label>
								<select name="periodicidade" id="idPeriodicidade" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaPeriodicidade}" var="periodicidade">
									<option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${rota.periodicidade.chavePrimaria == periodicidade.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${periodicidade.descricao}"/>
									</option>		
			   			 		</c:forEach>
								</select>
           		  			 </div>
          				</div> 
          				
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idTipoLeitura">Meio de Leitura:</label>
								<select name="tipoLeitura" id="idTipoLeitura" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaTipoLeitura}" var="tipoLeiturista">
									<option value="<c:out value="${tipoLeiturista.chavePrimaria}"/>" <c:if test="${rota.tipoLeitura.chavePrimaria == tipoLeiturista.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoLeiturista.descricao}"/>
									</option>			
			   			 		</c:forEach>
								</select>
           		  			 </div>
          				</div> 
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             		 			 <label for="numeroRota">Rota:</label>
                                 <input class="form-control form-control-sm" type="text" name="numeroRota" style="text-transform: uppercase"
                                        id="numeroRota" maxlength="30" size="30" value="${rota.numeroRota}" 
                                        onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumericoSemHifen(event)');"/>
             		 		</div>
             		 	</div>
             		 	
             		 	<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idGrupoFaturamento">Grupo de Faturamento:</label>
								<select name="grupoFaturamento" id="idGrupoFaturamento" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaGrupoFaturamento}" var="grupo">
										<option value="<c:out value="${grupo.chavePrimaria}"/>" <c:if test="${rota.grupoFaturamento.chavePrimaria == grupo.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${grupo.descricao}"/>
										</option>		
		    						</c:forEach>
								</select>
           		  			 </div>
          				</div> 

        			</div>	
        			<div class="col-md-6">
        				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idSetorComercial">Setor Comercial:</label>
								<select name="setorComercial" id="idSetorComercial" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaSetorComercial}" var="setorComercial">
										<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${rota.setorComercial.chavePrimaria == setorComercial.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${setorComercial.descricao}"/>
										</option>
		    						</c:forEach>
								</select>
           		  			 </div>
          				</div> 
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idEmpresa">Empresa:</label>
								<select name="empresa" id="idEmpresa" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaEmpresa}" var="empresa">
										<option value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${rota.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${empresa.cliente.nome}"/>
										</option>
		   							</c:forEach>
								</select>
           		  			 </div>
          				</div> 
        			
        				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idLeiturista">Leiturista:</label>
								<select name="leiturista" id="idLeiturista" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaLeiturista}" var="leiturista">
										<option value="<c:out value="${leiturista.chavePrimaria}"/>" <c:if test="${rota.leiturista.chavePrimaria == leiturista.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${leiturista.funcionario.nome}"/>
										</option>		
									</c:forEach>
								</select>
           		  			 </div>
          				</div>
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             		 			 <label for="numeroPontoConsumo">N�mero m�ximo de pontos de consumo:</label>
                                 <input class="form-control form-control-sm" type="text" name="numeroMaximoPontosConsumo"
                                        id="numeroMaximoPontosConsumo"  maxlength="4" size="4" value="${rota.numeroMaximoPontosConsumo}" 
                                        onkeypress="return formatarCampoInteiro(event)"/>
             		 		</div>
             		 	</div> 
        			
        			</div>

        		</div><!-- fim da primeira row -->
        		
        		<div class="row mt-3">
                    <div class="col align-self-end text-right">
                    	<vacess:vacess param="pesquisarRota">
                        	<button class="btn btn-primary btn-sm" id="botaoPesquisar" type="submit">
                            	<i class="fa fa-search"></i> Pesquisar
                        	</button>
                        </vacess:vacess>
                        <button class="btn btn-secondary btn-sm" id="botaoLimpar" type="button"
                                onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                    </div>
                </div>
                <c:if test="${listaRota ne null}">
				<hr/>
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="rota" width="100%">
         	 			<thead class="thead-ggas-bootstrap">
         	 				<tr>
                 	 			<th class="text-center">
                     				<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>
                     			</th>
                    			<th scope="col" class="text-center">Ativo</th>
                    			<th scope="col" class="text-center">Nome da Rota</th>
                    			<th scope="col" class="text-center">Grupo de Faturamento</th>
                    			<th scope="col" class="text-center">Meio de Leitura</th>
                    			<th scope="col" class="text-center">Empresa</th>
                    			<th scope="col" class="text-center">Leiturista</th>
                    			<th scope="col" class="text-center">Periodicidade</th>
                    			<th scope="col" class="text-center">Setor Comercial</th>
                  			</tr>
         	 			</thead>
         	 			<tbody>
         	 				<c:forEach items="${listaRota}" var="rota">
         	 				<tr>
         	 					<td class="text-center">
         	 						<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias"
                                               value="${rota.chavePrimaria}">
         	 					</td>
         	 					<td class="text-center">
         	 						<c:choose>
										<c:when test="${rota.habilitado == true}">
											<i class="fas fa-check-circle"></i>
										</c:when>
										<c:otherwise>
											<i class="fas fa-times-circle"></i>
										</c:otherwise>				
									</c:choose>
         	 					</td>
         	 					<td class="text-center">
         	 						<a href="javascript:detalharRota(<c:out value='${rota.chavePrimaria}'/>);">
            							<c:out value="${rota.numeroRota}"/>
           							 </a>  
         	 					</td>
         	 					<td class="text-center">
         	 						<a href="javascript:detalharRota(<c:out value='${rota.chavePrimaria}'/>);">
            							<c:out value="${rota.grupoFaturamento.descricao}"/>
           							 </a>
         	 					</td>
         	 					<td class="text-center">
         	 						<a title="<c:out value="${rota.tipoLeitura.descricao}"/>" href="javascript:detalharRota(<c:out value='${rota.chavePrimaria}'/>);">
            							<c:out value="${rota.tipoLeitura.chavePrimaria}"/>
            						</a>
         	 					</td>
         	 					<td class="text-center">
         	 						<a title="<c:out value="${rota.empresa.cliente.nome}"/>" href="javascript:detalharRota(<c:out value='${rota.chavePrimaria}'/>);">
            							<c:out value="${rota.empresa.cliente.chavePrimaria}"/>
            						</a>
         	 					</td>
         	 					<td class="text-center">
         	 						<a title="<c:out value="${rota.leiturista.funcionario.nome}"/>" href="javascript:detalharRota(<c:out value='${rota.chavePrimaria}'/>);">
            							<c:out value="${rota.leiturista.funcionario.chavePrimaria}"/>
            						</a>
         	 					</td>
         	 					<td class="text-center">
         	 						<a href="javascript:detalharRota(<c:out value='${rota.chavePrimaria}'/>);">
            							<c:out value="${rota.periodicidade.descricao}"/>
           							</a>
         	 					</td>
         	 					<td class="text-center">
         	 						<a href="javascript:detalharRota(<c:out value='${rota.chavePrimaria}'/>);">
            							<c:out value="${rota.setorComercial.descricao}"/>
           							 </a>
         	 					</td>
         	 				</tr>
         	 				</c:forEach>
         	 			</tbody>
					</table>
				</div>
        		</c:if>
        		
        	</div><!-- fim do card-body -->
        	<div class="card-footer">
        		<div class="row">
        			<div class="col-sm-12">
						<button id="pesquisarPontoConsumo" type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-left">
							<i class="fas fa-search"></i> Pontos de consumo n�o roteirizados
        				<c:if test="${not empty listaRota}">
        					<vacess:vacess param="alterarRota">
        						<button id="botaoAlterar" type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-left" onclick="alterarRota()">
        						<i class="fas fa-pencil-alt"></i> Alterar
    						</button>
        					</vacess:vacess>
        					<vacess:vacess param="excluirRota">
        						<button id="botaoRemover" type="button" class="btn btn-danger btn-sm ml-1 mt-1 float-left" onclick="excluirRota()">
        							<i class="far fa-trash-alt"></i> Remover
    							</button>
        					</vacess:vacess>
        				</c:if>
        				<vacess:vacess param="exibirInclusaoRota">
        					<button id="botaoIncluir" value="Incluir" class="btn btn-sm btn-primary float-right ml-1 mt-1" onclick="incluir();">
                       			<i class="fa fa-plus"></i> Incluir
                   			</button>
        				</vacess:vacess>
        			</div>
        		</div>
        	</div>
		</div><!-- fim do card principal -->
	</form:form>
</div>
<jsp:include page="modalPesquisarPontoConsumoRota.jsp"/>
<script src="${ctxWebpack}/dist/modulos/medicao/rota/incluir/index.js"></script>
