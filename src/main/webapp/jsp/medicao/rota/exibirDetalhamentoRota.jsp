<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<div class="bootstrap">
	<form:form method="post" action="pesquisarRota" id="rotaForm" name="rotaForm">
		<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoRota" id="rotaForm" name="rotaForm">
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${rota.chavePrimaria}">
		<input name="idPeriodicidade" type="hidden" id="idPeriodicidade" value="${rota.periodicidade.chavePrimaria}">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Detalhar Rota</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Para modificar as informa��es deste registro clique em <strong>Alterar</strong>.
   			 	</div>
   			 	
   			 	<div class="row">
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					<p>Periodicidade: <span class="font-weight-bold">
  								<c:out value='${rota.periodicidade.descricao}'/></span></p>
   			 				</li>
   			 				<li class="list-group-item">
   			 					<p>Meio de Leitura: <span class="font-weight-bold">
  								<c:out value='${rota.tipoLeitura.descricao}'/></span></p>
   			 				</li>
   			 				<li class="list-group-item">
   			 					<p>Nome da Rota: <span class="font-weight-bold">
  								<c:out value='${rota.numeroRota}'/></span></p>
   			 				</li>
   			 				<li class="list-group-item">
   			 					<p>Grupo de Faturamento: <span class="font-weight-bold">
  								<c:out value='${rota.grupoFaturamento.descricao}'/></span></p>
   			 				</li>
   			 			</ul>
   			 		</div>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					<p>Setor Comercial: <span class="font-weight-bold">
  								<c:out value='${rota.setorComercial.descricao}'/></span></p>
   			 				</li>
   			 				<li class="list-group-item">
   			 					<p>Empresa: <span class="font-weight-bold">
  								<c:out value='${rota.empresa.cliente.nome}'/></span></p>
   			 				</li>
   			 				<li class="list-group-item">
   			 					<p>Leiturista: <span class="font-weight-bold">
  								<c:out value='${rota.leiturista.funcionario.nome}'/></span></p>
   			 				</li>
   			 				<li class="list-group-item">
   			 					<p>N�mero M�ximo de Pontos de Consumo:<span class="font-weight-bold">
  								<c:out value="${rota.numeroMaximoPontosConsumo}"/></span></p>
   			 				</li>
   			 			</ul>
   			 		</div>
   			 		
   			 	</div><!--  fim da primeira row -->
   			 	
   			 	<!-- <div id="divGoogleMaps">
					<hr />
					<div class="row">
						<div id="map_canvas"></div>
					</div>	
				</div> -->
				
				<hr />
				<div class="row">
					<div class="col-md-12">
						<c:if test="${listaPontoConsumoSequenciados ne null}"  >
							<h5>Pontos de Consumo Associados</h5>
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover" id="pontoConsumo" style="width:100%">
									<thead class="thead-ggas-bootstrap">
										<tr>
   			 								<th scope="col" class="text-center"></th>
   			 								<th scope="col" class="text-center">Im�vel</th>
   			 								<th scope="col" class="text-center">Ponto de Consumo</th>
   			 								<th scope="col" class="text-center">Endere�o</th>
   			 							</tr>
									</thead>
									<tbody>
										<c:forEach items="${listaPontoConsumoSequenciados}" var="pontoConsumo">
											<tr>
												<td class="text-center">
													<c:out value="${i+1}"></c:out>
												</td>
												<td class="text-center">
													<c:out value="${pontoConsumo.imovel.nome}" />
												</td>
												<td class="text-center">
													<c:out value="${pontoConsumo.descricao}" />
												</td>
												<td class="text-center">
													<c:out value="${pontoConsumo.imovel.enderecoFormatado}" />
												</td>
											</tr>
											<c:set var="i" value="${i+1}" />
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:if>
					</div>
				</div><!-- segunda row -->
				<hr/>
				<div class="row">
					<div class="col-md-12">
					<c:if test="${listaImovel ne null}">
						<h5>Im�veis Previstos</h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="imovel" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
   			 							<th scope="col" class="text-center">Im�vel</th>
   			 							<th scope="col" class="text-center">Endere�o</th>
   			 						</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaImovel}" var="imovel">
										<tr>
											<td class="text-center">
													<c:out value="${imovel.nome}" />
											</td>
											<td class="text-center">
												<c:out value="${imovel.enderecoFormatado}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</c:if>
					</div>
				</div><!-- terceira row -->
   			 	
			</div><!-- fim do card-body -->
			
			<div class="card-footer">
				<div class="row">
      				<div class="col-sm-12">
      					<button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="voltar();">
                            <i class="fas fa-undo-alt"></i> Voltar
                        </button>
                        <vacess:vacess param="exibirAlteracaoRota"> 
                        	<button id="botaoAlterar" class="btn btn-sm btn-primary float-right ml-1 mt-1"  type="button" onclick="alterar();">
                       			<i class="fas fa-edit"></i> Alterar
                   			</button>
                        </vacess:vacess>   
      				</div>
      			</div>
      		</div>
		</div><!-- Fim do card -->
	</form:form>
</div>
<script src="${pageContext.request.contextPath}/js/rota/detalhamento/detalhamento.js" type="application/javascript"></script>