<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>


 

 
<div class="bootstrap">
	<form:form method="post" action="incluirRota" id="rotaForm" name="rotaForm">
		<input name="acao" type="hidden" id="acao" value="incluirRota"/>
		<input name="listaIdsPontoConsumoRota" type="hidden" id="listaIdsPontoConsumoRota"/>
		<input name="listaIdsImoveisPais" type="hidden" id="listaIdsImoveisPais"/>
		<input name="chavePontoRemover" type="hidden" id="chavePontoRemover"/>
		<input name="chavesPrimariasOrdenacaoPontoConsumo" type="hidden" id="chavesPrimariasOrdenacaoPontoConsumo"/>
		<input name="ordenarTodos" type="hidden" id="ordenarTodos"/>
		<input name="listaChavePontoRemover" type="hidden" id="listaChavePontoRemover"/>
		<input name="exibirInclusao" type="hidden" id="exibirInclusao" value="${true}"/>
		<input name="postBack" type="hidden" id="postBack" value="true">



		<div id="dialogImovelPai" title="Confirma��o" style="display: none;">
			<table>
				<tr>
					<td colspan="2"><label id="texto">Deseja selecionar
							todos os Pontos de Consumo deste Imovel?</label></td>
				</tr>
				<tr>
					<td><input name="button" id="botaoCancelar"
						style="margin-right: 47px; margin-left: 51px;"
						class="bottonRightCol2 botaoGrande1" value="N�o"
						onclick="adicionarPontoConsumoRotaConfirmacaoNao()" type="submit">
					</td>
					<td><input name="button" id="botaoConfirmar"
						class="bottonRightCol2 botaoGrande1" value="Sim"
						onclick="adicionarPontoConsumoRotaConfirmacaoSim()" type="submit">
					</td>
				</tr>
			</table>
		</div>



		<div class="card">
			<div class="card-header">
            	<h5 class="card-title mb-0">Incluir Rota</h5>
        	</div>
        	<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Informe os dados abaixo e clique em Salvar para finalizar.
   			 	</div>
   			 	<div class="row">
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idPeriodicidade">Periodicidade da Rota: <span class="text-danger">*</span></label>
								<select name="periodicidade" id="idPeriodicidade" class="form-control form-control-sm" onchange="carregarPontoConsumo();">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaPeriodicidade}" var="periodicidade">
									<option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${rota.periodicidade.chavePrimaria == periodicidade.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${periodicidade.descricao}"/>
									</option>		
			   			 		</c:forEach>
								</select>
           		  			 </div>
          				</div> 
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idTipoLeitura">Tipo de Leitura: <span class="text-danger">*</span></label>
								<select name="tipoLeitura" id="idTipoLeitura" class="form-control form-control-sm" onchange="carregarPontoConsumo();">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaTipoLeitura}" var="tipoLeitura">
										<option value="<c:out value="${tipoLeitura.chavePrimaria}"/>" <c:if test="${rota.tipoLeitura.chavePrimaria == tipoLeitura.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${tipoLeitura.descricao}"/>
										</option>
									</c:forEach>
								</select>
           		  			 </div>
          				</div> 
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             		 			 <label for=chavePrimaria>Rota: <span class="text-danger">*</span></label>
                                 <input class="form-control form-control-sm" type="text" name="numeroRota" style="text-transform: uppercase"
                                        id="numeroRota" maxlength="30" size="30" value="${rota.numeroRota}" 
                                        onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumericoSemHifen(event)');"/>
             		 		</div>
             		 	</div>
             		 	
             		 	<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idGrupoFaturamento">Grupo de Faturamento: <span class="text-danger">*</span></label>
								<select name="grupoFaturamento" id="idGrupoFaturamento" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaGrupoFaturamento}" var="grupoFaturamento">
										<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${rota.grupoFaturamento.chavePrimaria == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${grupoFaturamento.descricao}"/>
										</option>	
		    						</c:forEach>
								</select>
           		  			 </div>
          				</div> 

   			 		</div>
   			
        			<div class="col-md-6">
        				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idSetorComercial">Setor Comercial:</label>
								<select name="setorComercial" id="idSetorComercial" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaSetorComercial}" var="setorComercial">
										<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${rota.setorComercial.chavePrimaria == setorComercial.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${setorComercial.descricao}"/>
										</option>
		    						</c:forEach>
								</select>
           		  			 </div>
          				</div> 
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idEmpresa">Empresa: <span class="text-danger">*</span></label>
								<select name="empresa" id="idEmpresa" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaEmpresa}" var="empresa">
										<option value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${rota.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${empresa.cliente.nome}"/>
										</option>
		   							</c:forEach>
								</select>
           		  			 </div>
          				</div> 
        			
        				<div class="form-row">
             		 		<div class="col-md-10">
             					<label for="idLeiturista">Leiturista:</label>
								<select name="leiturista" id="idLeiturista" class="form-control form-control-sm">
								<option value="-1">Selecione</option>
									<c:forEach items="${listaLeiturista}" var="leiturista">
										<option value="<c:out value="${leiturista.chavePrimaria}"/>" <c:if test="${rota.leiturista.chavePrimaria == leiturista.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${leiturista.funcionario.nome}"/>
										</option>		
									</c:forEach>
								</select>
           		  			 </div>
          				</div>
          				
          				<div class="form-row">
             		 		<div class="col-md-10">
             		 			 <label for="numeroMaximoPontosConsumo">N�mero m�ximo de pontos de consumo:</label>

                                 <input class="form-control form-control-sm" type="text" name="numeroMaximoPontosConsumo"
                                        id="numeroMaximoPontosConsumo"  maxlength="4" size="4" value="${rota.numeroMaximoPontosConsumo}" 
                                        onkeypress="return formatarCampoInteiro(event)"/>
             		 		</div>
             		 	</div> 
   			 		</div>
   			 		
   			 	</div><!-- fim da primeira row -->
				<div class="row mt-3">
                    <div class="col align-self-end text-right">
                    	<c:if test="${aceitaInclusaoPontoConsumo eq 0}">
                    	<button value="Adicionar Ponto de Consumo" type="button" class="btn btn-sm btn-primary float-right ml-1 mt-1" onclick="exibirPopupAdicionarPontoConsumoRota();">
                       		<i class="fa fa-plus"></i> Adicionar Ponto de Consumo
                   		</button>
                   		</c:if>
                    </div>
                </div><!-- combo de bot�es -->
				
				<c:set var="i" value="0" />
				<c:if test="${listaPontoConsumoSequenciados ne null && not empty listaPontoConsumoSequenciados}"  >
				<hr/>
					<div id="gridPontoConsumoRotaSequenciados">
						<div class="row">
							<jsp:include page="/jsp/medicao/rota/gridPontoConsumoRotaSequenciados.jsp"></jsp:include>
						</div>
					</div>
				</c:if>	
				
				<div class="row" style="padding-top: 32px;">
				<div class="col-md-6">
					<div class="input-group mt-4">
             			<label>Pontos de Consumo da Rota: <B>${fn:length(listaPontoConsumoSequenciados)}</B></label>
             			</div>
             	  </div>
             	</div>  
				
				<c:if test="${listaPontoConsumoRotaExibicao ne null  && not empty listaPontoConsumoRotaExibicao}">
				<hr/>
					<div id="gridPontoConsumoRota">
						<div class="row">
							<jsp:include page="/jsp/medicao/rota/gridPontoConsumoRota.jsp"></jsp:include>
						</div>
					</div>
				</c:if>
				
				
				
             	  
				
        	</div><!-- fim do card-body -->
        	<div class="card-footer">
        		<div class="row">
        			<div class="col-sm-12">
        				 <button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="cancelar();">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                        <button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                        <vacess:vacess param="incluirRota">
                        	<button id="botaoIncluir" value="Incluir" class="btn btn-sm btn-success float-right ml-1 mt-1"  type="button" onclick="incluir();">
                       			<i class="fas fa-save"></i> Salvar
                   			</button>
                        </vacess:vacess>
        			</div>
        		</div>
        	</div>
        	
		</div><!-- Fim do card -->
		<token:token></token:token>
	</form:form>
</div>
 
<style>
#pesquisaRotaCol2 label.rotulo {
    width: 137px; 
}
td { white-space:normal}

.expandeRetraiImovel{
		cursor:pointer;height: 80px;font-size: 18px;color: blue;
		}
		
		 
		
		#pontoConsumoRota_wrapper > last-child{
		    position: relative;
		}
		
		#pontoConsumoRota_info{
			height: 100%;
		}
		.card-body{
		    margin-bottom: 2%;
		    }
	
</style>

 
 
<script src="${pageContext.request.contextPath}/js/rota/inclusao/inclusao.js" type="application/javascript"></script>
 <script>

 function incluir(){
 	
 	var valorMaximo = document.getElementById('numeroMaximoPontosConsumo').value;	
 	var contador = 0;
 	
	<c:forEach items="${sessionScope.listaPontoConsumoSequenciados}" var="pontoConsumo">
	contador++;
	</c:forEach>
 	//$("input[type=hidden][id='idPC']").each(function() { contador++; });
 	
 	if(valorMaximo != '' && contador > valorMaximo){
 		//alert('<fmt:message key="ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO"/>' );
 		$('#erroValidacao').remove()
 		$('.card-header').append('<div id="erroValidacao" class="notification failure hideit" onclick="$(\'.card-header\').hide()"><p><strong>Erro: </strong><fmt:message key="ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO"/></p></div>')
		$('.card-header').append('<input id="focoErro" type="text" style="width:0;height:0;position:absolute;"/>')
		$('.card-header').show()
		$('#focoErro').focus()
		$('#focoErro').remove()
 		return;
 	}
 	
 	submeter('rotaForm', 'incluirRota');
 }
 </script>
