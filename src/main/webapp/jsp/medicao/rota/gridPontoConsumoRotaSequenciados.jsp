<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="dialogImovelPaiSequenciado" title="Confirma��o" style="display: none;">
			<table>
				<tr>
					<td colspan="2"><label id="texto">Deseja selecionar
							todos os Pontos de Consumo deste Imovel?</label></td>
				</tr>
				<tr>
					<td><input name="button" id="botaoCancelar"
						style="margin-right: 47px; margin-left: 51px;"
						class="bottonRightCol2 botaoGrande1" value="N�o"
						onclick="adicionarPontoConsumoRotaConfirmacaoNaoSequenciado()" type="submit">
					</td>
					<td><input name="button" id="botaoConfirmar"
						class="bottonRightCol2 botaoGrande1" value="Sim"
						onclick="adicionarPontoConsumoRotaConfirmacaoSimSequenciado()" type="submit">
					</td>
				</tr>
			</table>
		</div>


<div class="col-md-10" style="height: 300px">
	<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover" id="pontoConsumo" width="100%" ">
			<thead class="thead-ggas-bootstrap">
         	 	<tr>
         	 		<th data-orderable="false"></th>
         	 		<th ></th>
                	<th class="text-center" data-orderable="false">
                 		<input type='checkbox' name='checkAllPontoConsumoRotaSequenciados' id='checkAllPontoConsumoRotaSequenciados' />
                    </th>
                    <th scope="col" class="text-center">Im�vel</th>
                    <th scope="col" class="text-center">Ponto de Consumo</th>
                    <th scope="col" class="text-center">Endere�o</th>
                 </tr>
         	</thead>
         	<tbody>
         	 
         		<c:forEach items="${sessionScope.listaPontoConsumoSequenciadosAgrupados}" var="pontoConsumoRota">
         		<tr>
         			<td class="text-center">
         	 			<c:if test="${not empty pontoConsumoRota.listaPontosConsumoImoveisFilho}">
         	 				
							<span   onclick="expandeImoveisFilhosSequenciados(this, 'ImoveissequenciadosFilhos${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}')")" class="expandeRetraiImovelSequenciado">+</span>
							<fieldset id="ImoveissequenciadosFilhos${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}" class="pontos-consumo-filhos" style="display:none;position: absolute;left: 0px;height: 28px;display: none;width:100%;">		
							<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover tabelaInternaSequencial" 
							id="pontoConsumoAgrupadoTabela${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}"
							style="margin-top: 10px;margin-left: 6px;width:97.8%">
								
								<thead class="thead-ggas-bootstrap">
					         	 	<tr>
					         	 		<th scope="col" class="text-center sorting" onclick="ordenatabela('pontoConsumoAgrupadoTabela${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}', this, 1, 1)"></th>
					         	 	 
					                	<th class="text-center" data-orderable="false">
					                 	
					                    </th>
					                    <th scope="col" class="text-center sorting" onclick="ordenatabela('pontoConsumoAgrupadoTabela${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}', this, 3, 2)" tabindex="0" aria-controls="pontoConsumo" rowspan="1" colspan="1" style="width: 216px;" aria-label="Im�vel: activate to sort column descending" aria-sort="ascending">Im�vel</th>
					                    <th scope="col" class="text-center sorting"  onclick="ordenatabela('pontoConsumoAgrupadoTabela${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}', this, 4, 2)" tabindex="0" aria-controls="pontoConsumo" rowspan="1" colspan="1" style="width: 300px;" aria-label="Ponto de Consumo: activate to sort column ascending">Ponto de Consumo</th>
					                    <th scope="col" class="text-center sorting"  onclick="ordenatabela('pontoConsumoAgrupadoTabela${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}', this, 5, 2)" tabindex="0" aria-controls="pontoConsumo" rowspan="1" colspan="1" style="width: 294px;" aria-label="Endere�o: activate to sort column ascending">Endere�o</th>
					                 </tr>
					         	</thead>
									<tbody>
								
								
								<c:forEach varStatus="j" var="pontoConsumoAgrupado" items="${pontoConsumoRota.listaPontosConsumoImoveisFilho}">
									 
								<tr class="${j.index % 2 == 0?'even':'odd'};text-center;" style="margin-left:10px">
							     	<td>
								     	${pontoConsumoAgrupado.numeroSequenciaLeitura}
							     	</td>
							     	
							     	<td style="text-align: center;">
							<input type="checkbox" name="chavesPrimarias" id="idPai${pontoConsumoAgrupado.imovel.chavePrimaria + pontoConsumoAgrupado.chavePrimaria}" value="${pontoConsumoAgrupado.chavePrimaria}"  class="<c:if test="${not empty pontoConsumoAgrupado.imovel.idImovelCondominio}">${pontoConsumoAgrupado.imovel.idImovelCondominio}</c:if><c:if test="${ empty pontoConsumoAgrupado.imovel.idImovelCondominio}">${pontoConsumoAgrupado.imovel.chavePrimaria}</c:if>" tabindex="13" style="padding: 0px;">
									</td>
									<td class="text-center" style="text-align: center;">${pontoConsumoAgrupado.imovel.nome}</td>
									<td class="text-center" style="text-align: center;">${pontoConsumoAgrupado.descricao} </td>
									<td class="text-center" style="text-align: center;">${pontoConsumoAgrupado.imovel.enderecoFormatado}</td>
								</tr>	
							
								</c:forEach>	
								</tbody>
								</table>
								</div>
						    </fieldset>
						</c:if>
						 
         	 		</td>
         	 		<td>${pontoConsumoRota.numeroSequenciaLeitura}</td>
         	 		 
         			<td class="text-center" >
         	 			<input id="checkboxChavesPrimariasSequencial${pontoConsumoRota.chavePrimaria}" type="checkbox" name="chavesPrimarias"
                             onclick="adicionaRemoveIdImovelSequencial(this,'${pontoConsumoRota.chavePrimaria}', '','${pontoConsumoRota.imovel.chavePrimaria}','pontoConsumoAgrupadoTabela${pontoConsumoRota.imovel.chavePrimaria}${pontoConsumoRota.numeroSequenciaLeitura}' )"  
                                            value="${pontoConsumoRota.chavePrimaria}">
         	 		</td>
         	 		<td class="text-center"  >
         	 			<c:out value="${pontoConsumoRota.imovel.nome}"/>
         	 		</td>
         	 		<td class="text-center" >
         	 			<c:out value="${pontoConsumoRota.descricao}"/>
         	 		</td>
         	 		<td class="text-center"  >
         	 			<c:out value="${pontoConsumoRota.imovel.enderecoFormatado}"/>
         	 		</td>
         	 		 
         	 		 
         		</tr>
          
         		</c:forEach>
         	</tbody>
		</table>
	</div>
</div>
<div class="col-md-2 mt-5">
	<div class="form-row">
         	 <label>Digite a posi��o em que deseja ordenar a listagem e clique em Mover:</label>
         	  <div class="input-group mb-3">
         	  	<input class="form-control form-control-sm" type="text" 
         	  			id="posicao" name="posicao" 
                        size="3" maxlength="3"  value="${rotaForm.map.posicao}"
                         onkeypress="return formatarCampoInteiro(event);"/>
  				 <div class="input-group-append">
    			<button class="btn btn-secondary btn-sm" value="Mover" type="button" onClick="moverPontoConsumo();">
    			<i class="fas fa-angle-double-right"></i> Mover</button>
 			 </div>    
        </div>
    </div>
</div>
<style>

.expandeRetraiImovelSequenciado{
		cursor:pointer;height: 80px;font-size: 18px;color: blue;
		}
		
</style>
<script src="${pageContext.request.contextPath}/js/rota/pesquisa/pontoConsumoSequenciados.js" 
				type="application/javascript"></script>
				
				 