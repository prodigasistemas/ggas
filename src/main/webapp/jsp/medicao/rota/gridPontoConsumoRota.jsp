<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="col-md-9" style="padding-top: 20px;">
<h5 >Pontos de Consumo para Associa��o</h5>
	<div class="table-responsive" style="height:310px">
		<table class="table table-bordered table-striped table-hover" id="pontoConsumoRota" width="100%">
			<thead class="thead-ggas-bootstrap">
         	 	<tr>
                    <th scope="col" class="text-center">&nbsp;</th>
                	<th class="text-center" data-orderable="false">
                 		<input type='checkbox' name='checkAllPontoConsumoRota' id='checkAllPontoConsumoRota' />
                    </th>
                    <th scope="col" class="text-center">Descri��o</th>
                    <th scope="col" class="text-center">Segmento</th>
                    <th scope="col" class="text-center">Ramo de Atividade</th>
                    <th scope="col" class="text-center">Situa��o Consumo</th>
                    <th scope="col" class="text-center">A��o</th>
                 </tr>
         	</thead>
         	<tbody>
         		<c:forEach items="${sessionScope.listaPontoConsumoRotaExibicao}" var="pontoConsumoRota"> 
         		<tr>
         			<td class="text-center">
         	 			<c:if test="${not empty pontoConsumoRota.listaPontosConsumoImoveisFilho}">
							<span   onclick="expandeImoveisFilhos(this, 'Imoveisfilhos${pontoConsumoRota.imovel.chavePrimaria}')")" class="expandeRetraiImovel">+</span>
							<fieldset id="Imoveisfilhos${pontoConsumoRota.imovel.chavePrimaria}" style="display:none;position: absolute;left: 0px;height: 28px;display: none;width:100%;">		
							<table class="table table-bordered table-striped table-hover" style="margin-top: 5px;margin-left: 10px;width:99%">
								<c:forEach varStatus="j" var="pontoConsumoAgrupado" items="${pontoConsumoRota.listaPontosConsumoImoveisFilho}">
								<tr class="${j.index % 2 == 0?'even':'odd'};text-center;" style="margin-left:10px">
							     	<td style="text-align: center;" >&nbsp;</td>
							     	<td style="text-align: center;">
										<input type="checkbox" name="idPai${pontoConsumoAgrupado.imovel.chavePrimaria + pontoConsumoAgrupado.chavePrimaria}" value="${pontoConsumoAgrupado.chavePrimaria}"   tabindex="13" style="padding: 0px;">
									</td>
									<td class="text-center" style="text-align: center;">${pontoConsumoAgrupado.descricao}</td>
									<td class="text-center" style="text-align: center;">${pontoConsumoAgrupado.segmento.descricao} </td>
									<td class="text-center" style="text-align: center;">${pontoConsumoAgrupado.ramoAtividade.descricao}</td>
									<td class="text-center" style="text-align: center;">${pontoConsumoAgrupado.situacaoConsumo.descricao}</td>
									<td class="text-center" style="text-align: center;">
			         	 				<input name="idPC" type="hidden" id="idPC" value="${pontoConsumoAgrupado.chavePrimaria}"/>
										<a onclick="return confirm('Deseja excluir o ponto de consumo ?');" href="javascript:removerPontoConsumo(<c:out value="${pontoConsumoAgrupado.chavePrimaria}"/>);">
										 <i class="far fa-trash-alt"></i></a>
			         	 			</td>
									</tr>		
								</c:forEach>	
								</table>
								<input id="InputImovelFilho${pontoConsumoRota.imovel.chavePrimaria}" style="display:none" />
						    </fieldset>
						</c:if>
         	 		</td>
         			<td class="text-center" >
         	 			<input id="checkboxChavesPrimarias${pontoConsumoRota.chavePrimaria}" type="checkbox" name="chavesPrimariasPontoConsumo"
                             onclick="adicionaRemoveIdImovel(this,'${pontoConsumoRota.chavePrimaria}', '','${pontoConsumoRota.imovel.chavePrimaria}' )"  
                                            value="${pontoConsumoRota.chavePrimaria}">
         	 		</td>
         	 		<td class="text-center"  >
         	 			<c:out value="${pontoConsumoRota.descricao}"/>
         	 		</td>
         	 		<td class="text-center" >
         	 			<c:out value="${pontoConsumoRota.segmento.descricao}"/>
         	 		</td>
         	 		<td class="text-center"  >
         	 			<c:out value="${pontoConsumoRota.ramoAtividade.descricao}"/>
         	 		</td>
         	 		<td class="text-center"  >
         	 			<c:out value="${pontoConsumoRota.situacaoConsumo.descricao}"/>
         	 		</td>
         	 		<td class="text-center"  >
         	 			<input name="idPC" type="hidden" id="idPC" value="${pontoConsumoRota.chavePrimaria}"/>
         	 			<c:if test="${pontoConsumoRota.chavePrimaria != '0'}">
							<a onclick="return confirm('Deseja excluir o ponto de consumo ?');" href="javascript:removerPontoConsumo(<c:out value="${pontoConsumoRota.chavePrimaria}"/>);">
						 		<i class="far fa-trash-alt"></i>
						 	</a>
						 </c:if>
						<c:if test="${pontoConsumoRota.chavePrimaria == '0'}"> 
							<c:forEach varStatus="j" var="pontoConsumoAgrupado" items="${pontoConsumoRota.listaPontosConsumoImoveisFilho}">
								<c:if test="${not empty idsTodosImoveis}">
									<c:set var="idsTodosImoveis" value="${idsTodosImoveis},${pontoConsumoAgrupado.chavePrimaria}"/>
								</c:if>
								<c:if test="${empty idsTodosImoveis}">
									<c:set var="idsTodosImoveis" value="${pontoConsumoAgrupado.chavePrimaria}"/>
								</c:if>
						</c:forEach>
						 	<a onclick="return confirm('Deseja excluir todos os ponto de consumo deste imovel?');" href="javascript:removerPontoConsumoAgrupado('${idsTodosImoveis}');">
								 <i class="far fa-trash-alt"></i>
						   </a>	
						 <c:set var="idsTodosImoveis" value=""/>
						</c:if> 
         	 		</td>
         		</tr>
         		</c:forEach>
         	</tbody>
		</table>	
	</div>
</div>

<div class="col-md-3 mt-5">
	<div class="form-row">
         <label>Digite a posi��o inicial em que deseja inserir os
           pontos de consumo selecionados na listagem e clique em Ordenar:</label>
         	  <div class="input-group mb-3">
         	  	<input class="form-control form-control-sm" type="text" name="posicaoOrdenacao"
                          id="posicaoOrdenacao" size="3" maxlength="3" value="${rotaForm.posicaoOrdenacao}" 
                          onkeypress="return formatarCampoInteiro(event);"/>
  				 <div class="input-group-append">
    			<button class="btn btn-secondary btn-sm" value="Ordenar" type="button" onclick="ordenar()">
    			<i class="fas fa-list-ol"></i> Ordenar</button>
 			 </div>
			</div>      
    </div>
    	<button class="btn btn-secondary btn-sm" value="Ordenar Todos" type="button" onclick="ordenaTodosPontosConsumo()">
    	<i class="fas fa-list-ol"></i> Ordenar Todos</button>
</div>
 
<script src="${pageContext.request.contextPath}/js/rota/pesquisa/pontoConsumoRota.js" type="application/javascript"></script>