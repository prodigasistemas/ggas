<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<style>
	.ui-datepicker-trigger {
		margin: 5px 5px 0 5px;
		float: left;
	}
</style>
 <script>
	 $(document).ready(function() {
		 $(".campoData").datepicker({
			 changeYear: true,
			 maxDate: '+0d',
			 showOn: 'button',
			 buttonImage: '<c:url value="/imagens/calendario.gif"/>',
			 buttonImageOnly: true,
			 buttonText: 'Exibir Calend�rio',
			 dateFormat: 'dd/mm/yy'
		 });
	 });
 </script>
 
 


<div id="dialogImovelPai"   title="Confirma��o" style="display:none;">
	<table>
	<tr>
		<td colspan="2">
			<label id="texto" >Deseja selecionar todos os Pontos de Consumo deste Imovel?</label>
		</td>
	</tr>
	<tr>
	<td>
	 	<input name="button" id="botaoCancelar" style="margin-right: 47px;margin-left: 51px;" class="bottonRightCol2 botaoGrande1" value="N�o"  onclick="adicionarPontoConsumoRotaConfirmacaoNao()" type="submit" >
	</td>
	<td>
	    <input name="button" id="botaoConfirmar" class="bottonRightCol2 botaoGrande1" value="Sim"  onclick="adicionarPontoConsumoRotaConfirmacaoSim()" type="submit" >
	</td>
	</tr>
	</table>
</div>

<h1 class="tituloInternoPopup">Pesquisar Ponto de Consumo<a href="<help:help>/consultandocliente.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicialPopup">Para pesquisar um ponto de consumo, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form method="post" action="pesquisarPontoConsumoRota">

	<input name="multSelect" type="hidden" id="multSelect" value='true'/>
	<input name="acao" type="hidden" id="acao" value="pesquisarPontoConsumoRota"/>
	<input name="idSelecionado" type="hidden" id="idSelecionado" value=""/>
	<input name="habilitado" type="hidden" id="habilitado" value="true"/>
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias"/>
	
	<fieldset id="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisaRotaCol1" class="coluna">
		<label class="rotulo">Descri��o P.Consumo:</label>
		<input class="campoTexto campoAlinhado" type="text" id="descricaoPontoConsumo" name="descricaoPontoConsumo" maxlength="50" size="35" value="${imovelPesquisaVO.descricaoPontoConsumo}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		
		<label class="rotulo " id="rotuloSegmento idSegmentoPontoConsumoPosition" for="idSegmentoPontoConsumo">Segmento:</label>
		<select name="idSegmentoPontoConsumo" id="idSegmentoPontoConsumo" class="campoSelect campo2Linhas" onchange="carregarRamosAtividadePontoConsumo2(this);">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaSegmento}" var="segmento">
				<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${imovelPesquisaVO.idSegmentoPontoConsumo == segmento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${segmento.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<br />
		<label class="rotulo" id="rotuloRamoAtividade" for="idRamoAtividadePontoConsumo">Ramo de Atividade:</label>
		<select name="idRamoAtividadePontoConsumo" id="idRamoAtividadePontoConsumo" class="campoSelect campo2Linhas" <c:if test="${empty listaRamoAtividadePontoConsumo}">disabled="disabled""</c:if>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaRamoAtividadePontoConsumo}" var="ramoAtividade">
				<option value="<c:out value="${ramoAtividade.chavePrimaria}"/>" <c:if test="${imovelPesquisaVO.idRamoAtividadePontoConsumo == ramoAtividade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${ramoAtividade.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<br />
		<label class="rotulo">Situa��o do Contrato:</label>
		<select name="situacaoContrato" class="campoSelect campo2Linhas">
			<option value="-1">Selecione</option>
			<option value="1" <c:if test="${imovelPesquisaVO.situacaoContrato == 1}">selected="selected"</c:if>>Ativo</option>
			<option value="0" <c:if test="${imovelPesquisaVO.situacaoContrato == 0}">selected="selected"</c:if>>Inativo</option>
		</select>

		<label class="rotulo" for="dataInicioAssinaturaContrato">Data de Assinatura do Contrato:</label>

		<input class="campoData campoHorizontal" type="text" id="dataInicioAssinaturaContrato" name="dataInicioAssinaturaContrato" maxlength="10" value="${imovelPesquisaVO.dataInicioAssinaturaContrato}">
		<label class="rotuloEntreCampos" for="dataFimAssinaturaContrato">a</label>
		<input class="campoData" type="text" id="dataFimAssinaturaContrato" name="dataFimAssinaturaContrato" maxlength="10" value="${imovelPesquisaVO.dataFimAssinaturaContrato}">
	</fieldset>
		<fieldset id="pesquisaRotaCol2" class="colunaFinal">
			<label class="rotulo">Descri��o Imovel:</label>
			<input class="campoTexto campoAlinhado" type="text" id="nome" name="nome" maxlength="50" size="25" value="${imovelPesquisaVO.nome}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />

			<label class="rotulo">Matricula Imovel:</label>
			<input class="campoTexto campoAlinhado" type="text" id="matricula" name="matriculaImovel" maxlength="50" size="25" value="${imovelPesquisaVO.matriculaImovel}" onkeypress="return formatarCampoInteiro(event)"><br />

			<label class="rotulo">Descri��o | Condominio:</label>
			<input class="campoTexto campoAlinhado" type="text" id="nomeImovelCondominio" name="nomeImovelCondominio" maxlength="50" size="25" value="${imovelPesquisaVO.nomeImovelCondominio}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />

			<label class="rotulo">Situa��o do Medidor:</label>
			<select name="situacaoMedidor" class="campoSelect campo2Linhas" style="min-width: 208px;">
				<option value="-1">Selecione</option>
				<option value="1" <c:if test="${imovelPesquisaVO.situacaoMedidor == 1}">selected="selected"</c:if>>Ativo</option>
				<option value="0" <c:if test="${imovelPesquisaVO.situacaoMedidor == 0}">selected="selected"</c:if>>Aguardando Ativa��o</option>
			</select>
		<label class="rotulo" for="dataInicioAtivacaoMedidor">Data de Ativa��o do Medidor:</label>

		<input class="campoData campoHorizontal" type="text" id="dataInicioAtivacaoMedidor" name="dataInicioAtivacaoMedidor" maxlength="10" value="${imovelPesquisaVO.dataInicioAtivacaoMedidor}">
		<label class="rotuloEntreCampos" for="dataFimAtivacaoMedidor" style="margin-left: 0px;">a</label>
		<input class="campoData" type="text" id="dataFimAtivacaoMedidor" name="dataFimAtivacaoMedidor" maxlength="10" value="${imovelPesquisaVO.dataFimAtivacaoMedidor}">

		
	</fieldset>	
	
	</fieldset>
	
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="window.close();">
	    <input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparDados();">
		
		<c:if test="${listaPontoConsumo ne null && not empty listaPontoConsumo}">
	    	<input name="button" class="bottonRightCol2 botaoGrande1" value="Adicionar Pontos de Consumo"  type="button" onclick="adicionarPontoConsumoRota();">
	    </c:if>
	    <input name="button" id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Pesquisar"  onclick="$('input:checkbox').removeAttr('checked');" type="submit" style="margin-right:10px;">
	 </fieldset>
	
	
	<c:if test="${listaPontoConsumo ne null}">
	 
		<display:table class="dataTableGGAS dataTablePopup" style="width:101%"  name="listaPontoConsumo" sort="list" id="pontoConsumo" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoRota">
			
			<display:column style="text-align: center; width: 5%;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="chavesPrimarias" value="${pontoConsumo.chavePrimaria}" onclick="adicionaRemoveIdImovel(this,'${pontoConsumo.chavePrimaria}', '${pontoConsumo.imovel.idImovelCondominio}','${pontoConsumo.imovel.chavePrimaria}' )">
	      		<input type="hidden" id="idImovelPai${pontoConsumo.chavePrimaria}" name="idImovelPai${pontoConsumo.chavePrimaria}" value="<c:if test="${empty pontoConsumo.imovel.idImovelCondominio}">S</c:if><c:if test="${not empty pontoConsumo.imovel.idImovelCondominio}">N</c:if>">
	     	</display:column>
			
			<display:column  style="text-align: center; width: 5%;"> 
				<c:if test="${not empty pontoConsumo.listaPontosConsumoImoveisFilho}">
					<span   onclick="expandeImoveisFilhos(this, 'Imoveisfilhos${pontoConsumo.imovel.chavePrimaria}')")" class="expandeRetraiImovel">+</span>
					<fieldset id="Imoveisfilhos${pontoConsumo.imovel.chavePrimaria}" style="display:none;position: absolute;left: 0px;height: 28px;display: none;">		
					<table class="dataTableGGAS dataTablePopup tabelaInterna" style="margin-top: 5px;margin-left: 10px;width:98%">
						<c:forEach varStatus="j" var="pontoConsumoAgrupado" items="${pontoConsumo.listaPontosConsumoImoveisFilho}">
						<tr class="${j.index % 2 == 0?'even':'odd'}"">
						<td>&nbsp;</td>
							<td style="text-align: center; width: 0%;${j.index % 2 == 0?'background-color:white':''}">
					        </td>
					     	<td style="width: 10%;text-align: center; ${j.index % 2 == 0?'background-color:white':''}">
					<input type="checkbox" name="idPai${pontoConsumoAgrupado.imovel.chavePrimaria + pontoConsumoAgrupado.chavePrimaria}" value="${pontoConsumoAgrupado.chavePrimaria}"  class="<c:if test="${not empty pontoConsumoAgrupado.imovel.idImovelCondominio}">${pontoConsumoAgrupado.imovel.idImovelCondominio}</c:if><c:if test="${ empty pontoConsumoAgrupado.imovel.idImovelCondominio}">${pontoConsumoAgrupado.imovel.chavePrimaria}</c:if>" tabindex="13" style="padding: 0px;">
							</td>
							<td style="text-align: center;${j.index % 2 == 0?'background-color:white;':''}width:20%;">${pontoConsumoAgrupado.imovel.nome}</td>
							<td style="text-align: center;${j.index % 2 == 0?'background-color:white;':''}width:20%;">${pontoConsumoAgrupado.descricao} </td>
							<td style="text-align: center;${j.index % 2 == 0?'background-color:white;':''}width:15%;">${pontoConsumoAgrupado.segmento.descricao}</td>
							<td style="text-align: center;${j.index % 2 == 0?'background-color:white;':''}width:20%;">${pontoConsumoAgrupado.ramoAtividade.descricao}</td>
							<td style="text-align: center;${j.index % 2 == 0?'background-color:white;':''}width:20%">${pontoConsumoAgrupado.situacaoConsumo.descricao}</td>
							</tr>		
						</c:forEach>	
						</table>
				    </fieldset>
				</c:if>
				<!--  span style="margin-left:10px">${pontoConsumo.imovel.nome}</span-->
				
			</display:column>
			
			<display:column property="imovel.nome" sortable="true" title="Imovel" style="text-align: center; width: 20%;"></display:column>
			
			<display:column property="descricao" style="text-align: center; width: 20%" sortable="true" title="Ponto de Consumo" />
			
			<display:column property="segmento.descricao" style="text-align: center; width: 15%"  sortable="true" title="Segmento" />
			
			<display:column property="ramoAtividade.descricao" style="text-align: center; width: 20%;" sortable="true" title="Ramo de Atividade" />
						
			<display:column property="situacaoConsumo.descricao" style="text-align: center; width: 20%;" sortable="false" title="Situa��o Consumo" />
			
			
		</display:table>
	</c:if>
 
</form>
<style>

td { white-space:normal}

.expandeRetraiImovel{
		cursor:pointer;height: 80px;font-size: 18px;color: blue;
		}
</style>

<script>
var msgsPopupRota = {
	selecioneRegistros : 'Selecione um ou mais registros para realizar a opera��o',
	registrosFilhosSemRota : 'H� registros filhos que ficar�o sem rota'
};
</script>
<script src="${pageContext.request.contextPath}/js/rota/pesquisa/popUpPontoConsumoRota.js" type="application/javascript"></script>


</form>

