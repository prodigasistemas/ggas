<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 
 <%@ page contentType="text/html; charset=iso-8859-1" %>
 
 <%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
 
 <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="row">
    <div class="col-sm-12" style="margin-top: 10px">
        <table id="parametrizacao" class="table table-bordered table-striped table-hover" width="100%">
            <thead class="thead-ggas-bootstrap">
            <th class="text-center">Ativo</th>
            <th class="text-center">Segmento</th>
            <th class="text-center">Ramo de Atividade</th>
            <th class="text-center">Modalidade de Medi��o</th>
            <th class="text-center">Bloqueia Faturamento</th>
            <th class="text-center"></th>
            <th class="text-center"></th>
            </thead>
            <tbody id="corpoParametrizacao">


            <c:if test="${empty listaAnormalidadeSegmentoParametros}">
                <tr>
                    <td colspan="6" onclick="integraca()">
                        Nenhum registro
                    </td>
                </tr>
            </c:if>
            </tbody>

        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        <c:forEach var="listar" varStatus="status" items="${listaAnormalidadeSegmentoParametros}">

        var codigo = ${listar.chavePrimaria};

        var habilitado = ${listar.habilitado};
        var segmentoDescricao = "${listar.segmento.descricao}";
        var modalidadeMedicaoCodigo = "${listar.modalidadeMedicao.codigo}";
        var ramoAtividadeDescricao = "${listar.ramoAtividade.descricao}";
        var bloqueiFatura = true;
        if(bloqueiFatura) {
            bloqueiFatura = ${listar.bloqueiaFaturamento};
        }

        var ativo = "";
        var segmento = "";
        var ramoAtividade = "";
        var modalidadeAnormalidade = "";
        var bloqueioFaturamentoAnormalidade = "";

        if(habilitado){
            ativo = '<td style="text-align: center"><i class="fa fa-check-circle text-success"></i> Sim</td>';
        }else{
            ativo = '<td style="text-align: center"><i class="fa fa-times-circle text-danger"></i> N�o</td>';
        }

        segmento = '<td style="text-align: center">' + segmentoDescricao + '</td>';

        ramoAtividade = '<td style="text-align: center">' + ramoAtividadeDescricao + '</td>';

        if(modalidadeMedicaoCodigo == 1){
           modalidadeAnormalidade = '<td style="text-align: center"> Individual </td>';
        }else if(modalidadeMedicaoCodigo == 2){
            modalidadeAnormalidade = '<td style="text-align: center"> Coletiva </td>';
        }else{
        	modalidadeAnormalidade = '<td style="text-align: center"> </td>';
        	modalidadeMedicaoCodigo = 3;
        }

        if(bloqueiFatura){
            bloqueioFaturamentoAnormalidade = '<td style="text-align: center">Sim</td>';
        }else{
            bloqueioFaturamentoAnormalidade = '<td style="text-align: center">N�o</td>';
        }

        var editar = '<td style="text-align: center;    text-decoration: underline;\n' +
            '    color: #4c8fbd;cursor:pointer;"><a onclick="carregarParaAlterarParametrizacao(${status.index},${listar.segmento.chavePrimaria},'+modalidadeMedicaoCodigo+',${listar.bloqueiaFaturamento},${listar.ramoAtividade.chavePrimaria})" > Editar </a></td>';
        var remover = '<td style="text-align: center;    text-decoration: underline;\n' +
            '    color: #4c8fbd;cursor:pointer;"><a  onclick="excluirParametrizacao(${listar.chavePrimaria})"> Excluir </a></td>'

        var linha = "<tr>"
            + ativo
            + segmento
            + ramoAtividade
            + modalidadeAnormalidade
            + bloqueioFaturamentoAnormalidade
            + editar
            + remover
        "</tr>"

        $("#corpoParametrizacao").append(linha);

        </c:forEach>
    });
</script>
 
