<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<c:choose>
    <c:when test="${anormalidade.ehTipoAnormalidadeConsumo}">
        <c:set var="acao" value="alterarAnormalidadeConsumo"/>
    </c:when>
    <c:otherwise>
        <c:set var="acao" value="alterarAnormalidadeLeitura"/>
    </c:otherwise>
</c:choose>

<form:form method="post" action="${acao}" name="anormalidadeForm">
    <input name="postBack" type="hidden" id="postBack" value="true">
    <input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${anormalidade.chavePrimaria}">
    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${anormalidade.chavePrimaria}">
    <input name="versao" type="hidden" id="versao" value="${anormalidade.versao}">
    <input name="ehTipoAnormalidadeConsumo" type="hidden" id="ehTipoAnormalidadeConsumo" value="${anormalidade.ehTipoAnormalidadeConsumo}">
    <input name="indexParametrizacao" type="hidden" id="indexParametrizacao" value="-1">
    <input name="chavePrimariaParametrizacao" type="hidden" id="chavePrimariaParametrizacao" value="-1">

    <c:choose>
        <c:when test="${anormalidade.ehTipoAnormalidadeConsumo}">
            <c:set var="descAnormalidade">Consumo</c:set>
        </c:when>
        <c:otherwise>
            <c:set var="descAnormalidade">Leitura</c:set>
        </c:otherwise>
    </c:choose>

	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Alterar Anormalidade de ${descAnormalidade}</h5>
		</div>
		<div class="card-body">

			<div class="alert alert-primary fade show" role="alert">
				<i class="fa fa-question-circle"></i>
				Informe os dados abaixos e clique em <b>'Salvar'</b> para alterar a Anormalidade de ${descAnormalidade} selecionada
			</div>

			<input hidden value="false" name="ehTipoAnormalidadeConsumo"/>

			<div class="row">
				<div class="col-sm-12">
					<label for="descricao">Descri��o <span class="text-danger">*</span></label>
                    <input id="descricao"
                           <c:if test="${anormalidade.ehTipoAnormalidadeConsumo}">readonly</c:if>
                           type="text" class="form-control form-control-sm" name="descricao" maxlength="30" size="40"
						   value="${anormalidade.descricao}" />
				</div>
				<div class="col-sm-12">
					<label for="descricaoAbreviada">Descri��o abreviada</label>
					<input type="text"
                           <c:if test="${anormalidade.ehTipoAnormalidadeConsumo}">readonly</c:if>
                           class="form-control form-control-sm" id="descricaoAbreviada" name="descricaoAbreviada" maxlength="5"
                           size="5"
						   value="${anormalidade.descricaoAbreviada}" />
				</div>

                <div class="col-sm-12">
                    <label for="numeroOcorrenciaConsecutivasAnormalidade">Regra para aplica��o da anormalidade</label>
                    <textarea <c:if test="${anormalidade.ehTipoAnormalidadeConsumo}">readonly</c:if> name="regra"
                              oninput='return formatarCampoTextoComLimite(event,this,120);'
                              maxlength="120"
                              class="form-control form-control-sm">${anormalidade.regra}</textarea>
                </div>

                <c:if test="${anormalidade.ehTipoAnormalidadeConsumo}">

                    <div class="col-sm-12">
                        <label for="numeroOcorrenciaConsecutivasAnormalidade">N�mero de ocorr�ncias consecutivas da anormalidade</label>
                        <input type="text" class="form-control form-control-sm" id="numeroOcorrenciaConsecutivasAnormalidade"
                               name="numeroOcorrenciaConsecutivasAnormalidade" maxlength="8" size="8"
                               onkeypress="return formatarCampoInteiro(event)"  value="${anormalidade.numeroOcorrenciaConsecutivasAnormalidade}" />
                    </div>
                    <div class="col-sm-12">
                        <label for="mensagemConsumo">Mensagem impressa em conta </label>
                        <input class="form-control form-control-sm" type="text"
                               id="mensagemConsumo" name="mensagemConsumo" maxlength="100" size="100" value="${anormalidade.mensagemConsumo}">
                    </div>
                </c:if>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <label class="w-100">Indicador de uso</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="indicadorUsoSim" name="habilitado" value="true"
                               <c:if test="${anormalidade.habilitado eq 'true'}">checked</c:if>>
                        <label class="custom-control-label" for="indicadorUsoSim">Ativo</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="indicadorUsoNao" name="habilitado" value="false"
                               <c:if test="${anormalidade.habilitado eq null || anormalidade.habilitado eq 'false'}">checked</c:if>>
                        <label class="custom-control-label" for="indicadorUsoNao">Inativo</label>
                    </div>
                </div>
            </div>

            <c:if test="${anormalidade.ehTipoAnormalidadeConsumo}">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="w-100">Bloquear faturamento?</label>
                        <div class="custom-control custom-control-inline custom-radio">
                            <input type="radio" class="custom-control-input" id="bloquearFaturamentoNao" name="bloquearFaturamento" value="false"
                                   <c:if test="${anormalidade.bloquearFaturamento eq null || anormalidade.bloquearFaturamento eq 'false'}">checked</c:if>>
                            <label class="custom-control-label" for="bloquearFaturamentoNao">N�o</label>
                        </div>
                        <div class="custom-control custom-control-inline custom-radio">
                            <input type="radio" class="custom-control-input" id="bloquearFaturamentoSim" name="bloquearFaturamento" value="true"
                                   <c:if test="${anormalidade.bloquearFaturamento eq 'true'}">checked</c:if>>
                            <label class="custom-control-label" for="bloquearFaturamentoSim">Sim</label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <label class="w-100">Ignorar a��o autom�tica?</label>
                        <div class="custom-control custom-control-inline custom-radio">
                            <input type="radio" class="custom-control-input" id="ignorarAcaoAutoNao" name="ignorarAcaoAutomatica"
                                   value="false"
                                   <c:if test="${anormalidade.ignorarAcaoAutomatica eq null || anormalidade.ignorarAcaoAutomatica eq 'false'}">checked</c:if>>
                            <label class="custom-control-label" for="ignorarAcaoAutoNao">N�o</label>
                        </div>
                        <div class="custom-control custom-control-inline custom-radio">
                            <input type="radio" class="custom-control-input" id="ignorarAcaoAutoSim" name="ignorarAcaoAutomatica"
                                   value="true"
                                   <c:if test="${anormalidade.ignorarAcaoAutomatica eq 'true'}">checked</c:if>>
                            <label class="custom-control-label" for="ignorarAcaoAutoSim">Sim</label>
                        </div>
                    </div>
                </div>
            </c:if>


            <c:if test="${not anormalidade.ehTipoAnormalidadeConsumo}">
                <hr/>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="mensagemLeitura">Mensagem impressa em conta </label>
                        <input class="form-control form-control-sm" type="text"
                               id="mensagemLeitura" name="mensagemLeitura" maxlength="100" size="100" value="${anormalidade.mensagemLeitura}">
                    </div>
                    <div class="col-sm-12">
                        <label for="descricaoAbreviada">Orienta��es para manuten��o</label>
                        <input type="text" class="form-control form-control-sm" id="mensagemManutencao" name="mensagemManutencao" maxlength="100"
                               size="100" value="${anormalidade.mensagemManutencao}" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label for="mensagemPrevencao">Orienta��es para preven��o</label>
                        <input class="form-control form-control-sm" type="text"
                               id="mensagemPrevencao" name="mensagemPrevencao" maxlength="100" size="100" value="${anormalidade.mensagemPrevencao}">
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-sm-6">
                        <h5>Com Leitura Informada</h5>

                        <div class="row">
                            <div class="col-sm-12">
                                <label for="mensagemPrevencao">A��o para a leitura <span class="text-danger">*</span></label>
                                <select class="form-control form-control-sm" type="text"
                                        id="acaoAnormalidadeComLeitura" name="acaoAnormalidadeComLeitura">
                                    <option value="-1">Selecione</option>
                                    <c:forEach items="${listaAcaoAnormalidadeComLeitura}" var="acaoAnormalidadeComLeitura">
                                        <option value="<c:out value="${acaoAnormalidadeComLeitura.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeComLeitura == acaoAnormalidadeComLeitura.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${acaoAnormalidadeComLeitura.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-sm-12">
                                <label for="mensagemPrevencao">A��o para o consumo <span class="text-danger">*</span></label>
                                <select class="form-control form-control-sm" type="text"
                                        id="acaoAnormalidadeComConsumo" name="acaoAnormalidadeComConsumo">
                                    <option value="-1">Selecione</option>
                                    <c:forEach items="${listaAcaoAnormalidadeConsumoComLeitura}" var="acaoAnormalidadeComConsumo">
                                        <option value="<c:out value="${acaoAnormalidadeComConsumo.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeComConsumo == acaoAnormalidadeComConsumo.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${acaoAnormalidadeComConsumo.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <h5>Sem Leitura Informada</h5>

                        <div class="row">
                            <div class="col-sm-12">
                                <label for="idAcaoAnormalidadeSemLeitura">A��o para a leitura <span class="text-danger">*</span></label>
                                <select class="form-control form-control-sm" type="text"
                                        id="idAcaoAnormalidadeSemLeitura" name="acaoAnormalidadeSemLeitura">
                                    <option value="-1">Selecione</option>
                                    <c:forEach items="${listaAcaoAnormalidadeSemLeitura}" var="acaoAnormalidadeSemLeitura">
                                        <option value="<c:out value="${acaoAnormalidadeSemLeitura.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeSemLeitura == acaoAnormalidadeSemLeitura.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${acaoAnormalidadeSemLeitura.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <label for="idAcaoAnormalidadeSemConsumo">A��o para o consumo <span class="text-danger">*</span></label>
                                <select class="form-control form-control-sm" type="text"
                                        id="idAcaoAnormalidadeSemConsumo" name="acaoAnormalidadeSemConsumo">
                                    <option value="-1">Selecione</option>
                                    <c:forEach items="${listaAcaoAnormalidadeConsumoSemLeitura}" var="acaoAnormalidadeSemConsumo">
                                        <option value="<c:out value="${acaoAnormalidadeSemConsumo.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeSemConsumo == acaoAnormalidadeSemConsumo.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${acaoAnormalidadeSemConsumo.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>


                <div class="row">
                    <div class="col-sm-3">
                        <label class="w-100">Bloquear faturamento?</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="bloquearFaturamentoSim" name="bloquearFaturamento" value="true"
                                   <c:if test="${anormalidade.bloquearFaturamento eq 'true'}">checked</c:if>>
                            <label class="custom-control-label" for="bloquearFaturamentoSim">Sim</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="bloquearFaturamentoNao" name="bloquearFaturamento" value="false"
                                   <c:if test="${anormalidade.bloquearFaturamento eq null || anormalidade.bloquearFaturamento eq 'false'}">checked</c:if>>
                            <label class="custom-control-label" for="bloquearFaturamentoNao">N�o</label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="w-100">Aceita Leitura?</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="aceitaLeituraSim" name="aceitaLeitura" value="true"
                                   <c:if test="${anormalidade.aceitaLeitura eq 'true'}">checked</c:if>>
                            <label class="custom-control-label" for="aceitaLeituraSim">Sim</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="aceitaLeituraNao" name="aceitaLeitura" value="false"
                                   <c:if test="${anormalidade.aceitaLeitura eq null || anormalidade.aceitaLeitura eq 'false'}">checked</c:if>>
                            <label class="custom-control-label" for="aceitaLeituraNao">N�o</label>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <label class="w-100">Relativo ao Medidor?</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="relativoMedidorSim" name="relativoMedidor" value="true"
                                   <c:if test="${anormalidade.relativoMedidor eq 'true'}">checked</c:if>>
                            <label class="custom-control-label" for="relativoMedidorSim">Sim</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="relativoMedidorNao" name="relativoMedidor" value="false"
                                   <c:if test="${anormalidade.relativoMedidor eq null || anormalidade.relativoMedidor eq 'false'}">checked</c:if>>
                            <label class="custom-control-label" for="relativoMedidorNao">N�o</label>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <label class="w-100">Exige foto na leitura?</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="exigeFotoSim" name="exigeFoto" value="true"
                                   <c:if test="${anormalidade.exigeFoto eq 'true'}">checked</c:if>>
                            <label class="custom-control-label" for="exigeFotoSim">Sim</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="exigeFotoSimNao" name="exigeFoto" value="false"
                                   <c:if test="${anormalidade.exigeFoto eq null || anormalidade.exigeFoto eq 'false'}">checked</c:if>>
                            <label class="custom-control-label" for="exigeFotoSimNao">N�o</label>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${CODIGO_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_ZERO}">
                <c:if test="${listaAnormalidadeSegmentoParametros ne null}">
                    <hr/>
                    <h5>Parametriza��o</h5>
                    <p>Adicione a parametriza��o de acordo com Segmento e Modalidade de Medi��o para um comportamento mais espec�fico para essa
                        anormalidade.</p>

                    <div class="row">
                        <div class="col-sm-12">
                            <label for="segmentoAnormalidade">
                                Segmento <span class="text-danger">*</span>
                            </label>
                            <select name="segmentoAnormalidade" id="segmentoAnormalidade" class="form-control form-control-sm" onchange="carregarRamoAtividade(this, -1);">
                                <option value="-1">Selecione</option>
                                <c:forEach items="${listaSegmentos}" var="segmento">
                                    <option value="<c:out value="${segmento.chavePrimaria}"/>"
                                            <c:if test="${anormalidadeParametro.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
                                        <c:out value="${segmento.descricao}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="segmentoAnormalidade">
                                Ramo de Atividade <span class="text-danger">*</span>
                            </label>
                            <select name="ramoAtividade" id="ramoAtividade" class="form-control form-control-sm">
                                <option value="-1">Selecione</option>
                                <c:forEach items="${listaRamoAtividade}" var="ramoAtividade">
                                    <option value="<c:out value="${ramoAtividade.chavePrimaria}"/>"
                                           <c:if test="${anormalidadeParametro.ramoAtividade.chavePrimaria == ramoAtividade.chavePrimaria}">selected="selected"</c:if>> 
                                        <c:out value="${ramoAtividade.descricao}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>

                            <div class="row" style="margin-left: 1px;">
                                <div class="col-sm-6">
                                    <label for="segmentoAnormalidade" class="w-100">
                                        Tipo de Medi��o <span class="text-danger">*</span>
                                    </label>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="modalidadeMedicao" name="modalidadeMedicao" value="false"
                                               <c:if test="${anormalidadeParametro.modalidadeMedicao eq '1'}">checked</c:if>>
                                        <label class="custom-control-label" for="modalidadeMedicao">Individual</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="tipoMedicaoNao" name="modalidadeMedicao" value="true"
                                               <c:if test="${anormalidadeParametro.modalidadeMedicao eq '2'}">checked</c:if>>
                                        <label class="custom-control-label" for="tipoMedicaoNao">Coletiva</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="tipoMedicaoVazia" name="modalidadeMedicao" value="false">
                                        <label class="custom-control-label" for="tipoMedicaoVazia">N�o Identificada</label>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <label for="segmentoAnormalidade" class="w-100">
                                        Bloquear Faturamento <span class="text-danger">*</span>
                                    </label>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="bloqueiaFaturamentoParametroSim" name="bloqueiaFaturamento" value="true">
                                        <label class="custom-control-label" for="bloqueiaFaturamentoParametroSim">Sim</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="bloqueiaFaturamentoParametroNao" name="bloqueiaFaturamento" value="false">
                                        <label class="custom-control-label" for="bloqueiaFaturamentoParametroNao">N�o</label>
                                    </div>
                                </div>

                                <div class="col-sm-12" style="margin-top: 10px;">
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="botaoAdicionarParametrizacao" onclick="adicionarParametrizacao()">
                                        <i class="fa fa-check"></i> Salvar Parametriza��o
                                    </a>
                                </div>

                            </div>



                    </div>


                    <div class="row">
                        <div class="col-sm-12">
                            <jsp:include page="/jsp/medicao/anormalidade/gridParametrizacao.jsp" />
                        </div>
                    </div>

                </c:if>
            </c:if>

            <div class="row mt-2">
                <div class="col-sm-12">
                    <span><span class="text-danger">*</span> <i class="text-muted">campos obrigat�rios</i></span>
                </div>
            </div>

		</div>

		<div class="card-footer">
			<a href="voltaAnormalidade" class="btn btn-secondary float-left btn-sm mt-sm-1">
				<i class="fa fa-chevron-circle-left"></i> Voltar
			</a>
            <vacess:vacess param="alterarAnormalidadeConsumo">
                <button type="submit" class="btn btn-primary float-right btn-sm mt-sm-1" onclick="salvar()" id="salvar">
                    <i class="fa fa-check"></i> Salvar
                </button>
            </vacess:vacess>
		</div>

	</div>

    <token:token></token:token>
</form:form>


<script>
			
		function limparFormulario(){
			document.forms[0].descricao.value = "";
			document.forms[0].descricaoAbreviada.value = "";

			document.forms[0].bloquearFaturamentoSim.checked = false;
			document.forms[0].bloquearFaturamentoNao.checked = true;

			<c:if test="${not anormalidade.ehTipoAnormalidadeConsumo}">
				document.forms[0].mensagemLeitura.value = "";
				document.forms[0].mensagemManutencao.value = "";
				document.forms[0].mensagemPrevencao.value = "";
				document.forms[0].idAcaoAnormalidadeSemConsumo.value = "-1";
				document.forms[0].idAcaoAnormalidadeComConsumo.value = "-1";
				document.forms[0].idAcaoAnormalidadeSemLeitura.value = "-1";
				document.forms[0].idAcaoAnormalidadeComLeitura.value = "-1";

				document.forms[0].aceitaLeituraSim.checked = false;
				document.forms[0].aceitaLeituraNao.checked = true;

				document.forms[0].geraOrdemServicoSim.checked = false;
				document.forms[0].geraOrdemServicoNao.checked = true;

				document.forms[0].relativoMedidorSim.checked = false;
				document.forms[0].relativoMedidorNao.checked = true;
			</c:if>
		}

		function salvar() {
			<c:choose>
				<c:when test="${anormalidade.ehTipoAnormalidadeConsumo}">
					submeter('anormalidadeForm','alterarAnormalidadeConsumo');			
				</c:when>
				<c:otherwise>
					submeter('anormalidadeForm','alterarAnormalidadeLeitura');				
				</c:otherwise>
			</c:choose>				
		}
		
		function cancelar() {
			location.href = '<c:url value="/exibirPesquisaAnormalidade"/>';
		}

		function carregarParaAlterarParametrizacao(indexParametrizacao, segmentoParametro, modalidadeMedicao, bloqueiaFaturamento, ramoAtividade) {
			$("#segmentoAnormalidade").val(segmentoParametro);		

			var idSegmento = document.getElementById("segmentoAnormalidade");
			if (idSegmento != undefined && ramoAtividade > 0) {
				carregarRamoAtividade(idSegmento, ramoAtividade);
			}else if(idSegmento != undefined){
				carregarRamoAtividade(idSegmento, -1);
			}	
			
			if (modalidadeMedicao == 1) {
				$("#modalidadeMedicao").prop("checked", true);
				$("#tipoMedicaoNao").prop("checked", false);
				$("#tipoMedicaoVazia").prop("checked", false);
			} else if(modalidadeMedicao == 2){
				$("#modalidadeMedicao").prop("checked", false);
				$("#tipoMedicaoNao").prop("checked", true);
				$("#tipoMedicaoVazia").prop("checked", false);
			}else{
				$("#tipoMedicaoVazia").prop("checked", true);
				$("#tipoMedicaoNao").prop("checked", false);
				$("#modalidadeMedicao").prop("checked", false);
			}
			if (bloqueiaFaturamento == true) {	
				$("#bloqueiaFaturamentoParametroSim").prop("checked", true);
				$("#bloqueiaFaturamentoParametroNao").prop("checked", false);
			} else {
				$("#bloqueiaFaturamentoParametroSim").prop("checked", false);
				$("#bloqueiaFaturamentoParametroNao").prop("checked", true);
			}
			$("#indexParametrizacao").val(indexParametrizacao);			
		} // final carregarParaAlterarParametro()
		
		function adicionarParametrizacao() {
			var chavePrimaria = $('#chavePrimaria').val();
			var indexParametrizacao = $("#indexParametrizacao").val();
			var segmentoAnormalidade = $("#segmentoAnormalidade").val();
			var ramoAtividade = $('#ramoAtividade').val()
            var tipoMedicaoIndividual = $('#modalidadeMedicao').prop('checked') ? "1" : "2";
         
			if (segmentoAnormalidade < 0 || ((tipoMedicaoIndividual == "2" && !$('#tipoMedicaoNao').prop("checked")) && ramoAtividade < 0))
				return;

			if(tipoMedicaoIndividual == "2" && !$('#tipoMedicaoNao').prop("checked")){
				tipoMedicaoIndividual = null;
			}
			
			var bloqueiaFaturamentoParametroSim = $('#bloqueiaFaturamentoParametroSim').prop('checked');
			var habilitado = true;
			
			var url = "adicionarOuAlterarParametrizacao?chavePrimaria=" + chavePrimaria
					+"&acao=adicionarOuAlterarParametrizacao"
					+"&indexParametrizacao=" + indexParametrizacao
					+"&segmentoAnormalidade=" + segmentoAnormalidade
					+"&habilitado=" + habilitado
					+"&modalidadeMedicao=" + tipoMedicaoIndividual
					+"&bloqueiaFaturamento=" + bloqueiaFaturamentoParametroSim
					+"&ramoAtividade=" + ramoAtividade;
			
			var isSucesso = carregarFragmento('gridParametrizacao', url);
			
			if (isSucesso) {
				limparCampos();
			}
            location.reload();
		} // final adicionarParametrizacao()

        function removerParametrizao() {
            var chavePrimaria = $('#chavePrimaria').val();
            var indexParametrizacao = $("#indexParametrizacao").val();
            var segmentoAnormalidade = $("#segmentoAnormalidade").val();
            var tipoMedicaoIndividual = $('#modalidadeMedicao').prop('checked') ? "1" : "2";

            if (segmentoAnormalidade < 0 || (tipoMedicaoIndividual == "2" && !$('#tipoMedicaoNao').prop("checked")))
                return;
            var bloqueiaFaturamentoParametroSim = $('#bloqueiaFaturamentoParametroSim').prop('checked');
            var habilitado = true;

            var url = "removerParametrizacao?chavePrimaria=" + chavePrimaria
                +"&acao=removerParametrizacao";
            var isSucesso = carregarFragmento('gridParametrizacao', url);

            if (isSucesso) {
                limparCampos();
            }
            location.reload();
        } // final adicionarParametrizacao()


		function limparCampos() {
			$("#indexParametrizacao").val(-1);
			$("#segmentoAnormalidade").val(-1);
			$("#tipoMedicaoIndividual").prop("checked", false);
			$("#tipoMedicaoNao").prop("checked", false);
			$("#bloqueiaFaturamentoParametroSim").prop("checked", false);
			$("#bloqueiaFaturamentoParametroNao").prop("checked", false);
			$("#chavePrimariaParametrizacao").val('-1');
		}

		function excluirParametrizacao(chavePrimariaParametrizacao) {
			$("#chavePrimariaParametrizacao").val(chavePrimariaParametrizacao);
			var chavePrimaria = $('#chavePrimaria').val();
			var url = "removerParametrizacao?chavePrimaria=" + chavePrimaria
					+"&acao=removerParametrizacao"
					+"&chavePrimariaParametrizacao=" + chavePrimariaParametrizacao;
			carregarFragmento('gridParametrizacao', url);
			limparCampos();
            location.reload();
		}

		function carregarRamoAtividade(elem, ramoAtividade){
			var idSegmento = elem.value;
			var selectRamosAtividade = document.getElementById("ramoAtividade");
//			var idRamoAtividade = '${processo.operacao.chavePrimaria}';
		    
			selectRamosAtividade.length=0;
	      	var novaOpcao = new Option("Selecione","-1");
	      	selectRamosAtividade[selectRamosAtividade.length] = novaOpcao;
	        
	      	if (idSegmento != "-1") {
	      		selectRamosAtividade.disabled = false;
	        	AjaxService.consultarRamoAtividadePorSegmento( idSegmento, 
	            	function(listaRamosAtividade) {            		      		         		
	                	for (key in listaRamosAtividade){
	                    	var novaOpcao = new Option(listaRamosAtividade[key], key);
//	                    	if (idRamoAtividade == key) {
//	                    		novaOpcao.selected = true;
//	                    	}
	                    	selectRamosAtividade.options[selectRamosAtividade.length] = novaOpcao;
	                    }
	                	$('#ramoAtividade').val(ramoAtividade);
	                }
	            );
	      	} else {
	      		selectRamosAtividade.disabled = true;
	      	}
		}

		function init() {
			
			var idSegmento = document.getElementById("segmentoAnormalidade");

			if (idSegmento != undefined) {
				carregarRamoAtividade(idSegmento, -1);
			}	
			
		}

		addLoadEvent(init);

</script>
