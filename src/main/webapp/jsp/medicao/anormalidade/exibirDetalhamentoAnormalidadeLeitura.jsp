<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<div class="card">
	<div class="card-header">
		<h5 class="card-title mb-0">Detalhamento de Anormalidade de Leitura</h5>
	</div>
	<div class="card-body">

		<div class="alert alert-primary fade show" role="alert">
			<i class="fa fa-question-circle"></i>
			Visualize as informa��es da anormalidade selecionada.<br/>
			Para modificar as informa��es deste registro clique em <b>'Alterar'</b>
		</div>

		<h5>Geral</h5>
		<div class="row">
			<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${anormalidade.chavePrimaria}">

			<div class="col-sm-12">
				<label><b>Descri��o: </b></label>
				<label>${anormalidade.descricao}</label>
			</div>

			<div class="col-sm-12">
				<label><b>Descri��o Abreviada: </b></label>
				<label>${anormalidade.descricaoAbreviada}</label>
			</div>
		</div>

		<hr/>
		<h5>Mensagens</h5>

		<div class="row">
			<div class="col-sm-12">
				<label><b>Mensagem impressa em conta: </b></label>
				<label>${anormalidade.mensagemLeitura}</label>
			</div>

			<div class="col-sm-12">
				<label><b>Orienta��es para manunten��o: </b></label>
				<label>${anormalidade.mensagemManutencao}</label>
			</div>

			<div class="col-sm-12">
				<label><b>Orienta��es para preven��o: </b></label>
				<label>${anormalidade.mensagemPrevencao}</label>
			</div>
		</div>

		<hr/>

		<div class="row">
			<div class="col-sm-6">
				<h5>Com Leitura Informada</h5>
				<div class="row">
					<div class="col-sm-12">
						<label><b>A��o para a leitura: </b></label>
						<label>${descricaoAnormalidadeComLeitura}</label>
					</div>
					<div class="col-sm-12">
						<label><b>A��o para a consumo: </b></label>
						<label>${descricaoAnormalidadeComConsumo}</label>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<h5>Sem Leitura Informada</h5>
				<div class="row">
					<div class="col-sm-12">
						<label><b>A��o para a leitura: </b></label>
						<label>${descricaoAnormalidadeSemLeitura}</label>
					</div>
					<div class="col-sm-12">
						<label><b>A��o para a consumo: </b></label>
						<label>${descricaoAnormalidadeSemConsumo}</label>
					</div>
				</div>
			</div>
		</div>

		<hr/>

		<h5>Outros</h5>

		<div class="row">
			<div class="col-sm-12">
				<label><b>Aceita leitura: </b></label>
				<label>
					<c:choose>
						<c:when test="${aceitaLeitura eq true}">
							Sim
						</c:when>
						<c:otherwise>
							N�o
						</c:otherwise>
					</c:choose>
				</label>
			</div>
			<div class="col-sm-12">
				<label><b>Relativo ao Medidor: </b></label>
				<label>
					<c:choose>
						<c:when test="${relativoMedidor eq true}">
							Sim
						</c:when>
						<c:otherwise>
							N�o
						</c:otherwise>
					</c:choose>
				</label>
			</div>
			<div class="col-sm-12">
				<label><b>Bloquear Faturamento: </b></label>
				<label>
					<c:choose>
						<c:when test="${bloquearFaturamento eq true}">Sim</c:when>
						<c:otherwise>N�o</c:otherwise>
					</c:choose>
				</label>
			</div>
			<div class="col-sm-12">
				<label><b>Exige foto na leitura: </b></label>
				<label>
					<c:choose>
						<c:when test="${anormalidade.exigeFoto eq true}">Sim</c:when>
						<c:otherwise>N�o</c:otherwise>
					</c:choose>
				</label>
			</div>
		</div>

	</div>

	<div class="card-footer">
		<div class="row">
			<div class="col-sm-12">
				<a href="voltaAnormalidade" class="btn btn-secondary float-left btn-sm"><i class="fa fa-chevron-circle-left"></i> Voltar</a>
				<vacess:vacess param="alterarAnormalidadeLeitura">
					<a href="exibirAlteracaoAnormalidadeLeitura?chavePrimaria=${anormalidade.chavePrimaria}"
					   class="btn btn-primary float-right btn-sm"><i
							class="fa fa-edit"></i>
						Alterar</a>
				</vacess:vacess>
			</div>
		</div>
	</div>

</div>
