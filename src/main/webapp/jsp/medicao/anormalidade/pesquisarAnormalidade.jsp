<%--
  ~ Copyright (C) <2019> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2019 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<link href="${ctxWebpack}/dist/modulos/medicao/anormalidade/pesquisa/index.css" rel="stylesheet" type="text/css">

<div class="card">
    <div class="card-header">
        <h5 class="card-title mb-0">Lista de Anormalidades</h5>
    </div>

    <div class="card-body">

        <div class="alert alert-primary fade show" role="alert">
            <i class="fa fa-question-circle"></i>
            Utilize esta tela para acompanhar as anormalidades de leitura e consumo.
            <br/>Preencha os campos abaixo para filtrar os registros de anormalidades e clique no bot�o <b>'Pesquisar'</b> para exibir sua
            pesquisa.
        </div>

        <form id="anormalidadeForm" action="pesquisarAnormalidade" name="anormalidadeForm" method="post">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body bg-light">
                            <h5>Filtrar Anormalidades</h5>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="descricao">Descri��o</label>
                                    <input id="descricao" maxlength="100" name="descricao" class="form-control form-control-sm"
										   value="${anormalidade.descricao}"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="tipoAnormalidade">Tipo de Anormalidade</label>
                                    <select id="tipoAnormalidade" class="form-control form-control-sm"
                                            name="ehTipoAnormalidadeConsumo">
                                        <option value="false" <c:if test="${anormalidade.ehTipoAnormalidadeConsumo eq null}">checked</c:if>>Selecione</option>
                                        <option value="false" <c:if test="${not anormalidade.ehTipoAnormalidadeConsumo eq 'false'}">selected
                                        </c:if>>
                                            Anormalidade de Leitura
                                        </option>
                                        <option value="true" <c:if test="${anormalidade.ehTipoAnormalidadeConsumo eq 'true'}">selected</c:if>>
                                            Anormalidade de Consumo
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="indicadorUso">Indicador de Uso</label>
                                    <select id="indicadorUso" class="form-control form-control-sm"
                                            name="habilitado">
                                        <option value="" <c:if test="${habilitado eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${habilitado  eq 'true'}">selected</c:if>>Ativo
                                        </option>
                                        <option value="false" <c:if test="${habilitado  eq 'false'}">selected</c:if>>
                                            Inativo
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="row hide" id="containerLeitura">
                                <div class="col-md-6 mt-3">
                                    <h6>Com Leitura Informada</h6>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="acaoAnormalidadeComLeitura">A��o para a leitura:</label>
                                            <select name="acaoAnormalidadeComLeitura" id="acaoAnormalidadeComLeitura"
                                                    class="form-control form-control-sm">
                                                <option value="-1">Selecione</option>
                                                <c:forEach items="${listaAcaoAnormalidadeComLeitura}" var="acaoAnormalidadeComLeitura">
                                                    <option value="<c:out value="${acaoAnormalidadeComLeitura.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeComLeitura == acaoAnormalidadeComLeitura.chavePrimaria}">selected="selected"</c:if>>
                                                        <c:out value="${acaoAnormalidadeComLeitura.descricao}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="col-sm-12">
                                            <label for="acaoAnormalidadeComConsumo">A��o para o consumo:</label>
                                            <select id="acaoAnormalidadeComConsumo" class="form-control form-control-sm"
                                                    name="acaoAnormalidadeComConsumo">
                                                <option value="-1">Selecione</option>
                                                <c:forEach items="${listaAcaoAnormalidadeConsumoComLeitura}" var="acaoAnormalidadeComConsumo">
                                                    <option value="<c:out value="${acaoAnormalidadeComConsumo.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeComConsumo == acaoAnormalidadeComConsumo.chavePrimaria}">selected="selected"</c:if>>
                                                        <c:out value="${acaoAnormalidadeComConsumo.descricao}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6 mt-3">
                                    <h6>Sem Leitura Informada</h6>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="acaoAnormalidadeSemLeitura">A��o para a leitura:</label>
                                            <select name="acaoAnormalidadeSemLeitura" id="acaoAnormalidadeSemLeitura"
                                                    class="form-control form-control-sm">
                                                <option value="-1">Selecione</option>
                                                <c:forEach items="${listaAcaoAnormalidadeSemLeitura}" var="acaoAnormalidadeSemLeitura">
                                                    <option value="<c:out value="${acaoAnormalidadeSemLeitura.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeSemLeitura == acaoAnormalidadeSemLeitura.chavePrimaria}">selected="selected"</c:if>>
                                                        <c:out value="${acaoAnormalidadeSemLeitura.descricao}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="col-sm-12">
                                            <label for="acaoAnormalidadeSemConsumo">A��o para o consumo:</label>
                                            <select id="acaoAnormalidadeSemConsumo" class="form-control form-control-sm"
                                                    name="acaoAnormalidadeSemConsumo">
                                                <option value="-1">Selecione</option>
                                                <c:forEach items="${listaAcaoAnormalidadeConsumoSemLeitura}" var="acaoAnormalidadeSemConsumo">
                                                    <option value="<c:out value="${acaoAnormalidadeSemConsumo.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeSemConsumo == acaoAnormalidadeSemConsumo.chavePrimaria}">selected="selected"</c:if>>
                                                        <c:out value="${acaoAnormalidadeSemConsumo.descricao}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <button id="botaoLimpar" data-style="expand-left" type="button"
                                            class="btn btn-secondary btn-sm float-right">
                                        <i class="fa fa-times"></i> Limpar</span>
                                    </button>
                                    <button id="botaoPesquisar" data-style="expand-left" type="submit"
                                            class="btn btn-primary btn-sm float-right mr-1">
                                        <i class="fa fa-search"></i> Pesquisar</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <token:token></token:token>
        </form>

        <div class="row mt-3 hide" id="containerTabelaAnormalidadeLeitura">
            <div class="col-sm-12">
                <div class="ocultarTabela containerTabela">
                    <table id="table-anormalidades-leitura" class="table table-bordered table-striped table-hover" width="100%">
                        <thead class="thead-ggas-bootstrap">
                            <th class="text-center" style="width: 35px; max-width: 35px">

                            </th>
                            <th class="text-center" style="width: 50px; max-width: 50px">Ativo</th>
                            <th class="text-left" style="width: 100px; max-width: 100px">Descri��o</th>
                            <th class="text-left" style="width: 350px; max-width: 350px">Regra para Aplica��o da Anormalidade</th>
                            <th class="text-left max-width-pesquisa-anormalidade">A��o Anorma. Sem Consumo</th>
                            <th class="text-left max-width-pesquisa-anormalidade">A��o Anorma. Com Consumo</th>
                            <th class="text-left max-width-pesquisa-anormalidade">A��o Anorma. Sem Leitura</th>
                            <th class="text-left max-width-pesquisa-anormalidade">A��o Anorma. Com Leitura</th>
                        </thead>
                        <tbody>
                        <c:forEach items="${listaAnormalidadeLeitura}" var="anormalidade">
                            <tr>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1" data-identificador-check="chk${anormalidade.chavePrimaria}">
                                         <input id="chk${anormalidade.chavePrimaria}" type="checkbox"  name="chavesAnormalidadeLeitura"
                                         	data-chaveprimaria="${anormalidade.chavePrimaria}"
                                 		 	class="custom-control-input" 
                                 	     	value="${anormalidade.chavePrimaria}">
                                        <label class="custom-control-label p-0" for="chk${anormalidade.chavePrimaria}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <c:choose>
                                        <c:when test="${anormalidade.habilitado == true}">
                                            <i class="fa fa-check-circle text-success"></i> Sim
                                        </c:when>
                                        <c:otherwise>
                                            <i class="fa fa-times-circle text-danger"></i> N�o
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <a id="${anormalidade.descricao}" class="text-left linkGoDetalhamentoLeitura"  data-chavePrimaria="${anormalidade.chavePrimaria}"
                                       href="javascript:void(0)">
                                        ${anormalidade.descricao}
                                    </a>
                                </td>
                                <td class="text-left">${anormalidade.regra}</td>
                                <td class="text-left">${anormalidade.acaoAnormalidadeSemConsumo.descricao}</td>
                                <td class="text-left">${anormalidade.acaoAnormalidadeComConsumo.descricao}</td>
                                <td class="text-left">${anormalidade.acaoAnormalidadeSemLeitura.descricao}</td>
                                <td class="text-left">${anormalidade.acaoAnormalidadeComLeitura.descricao}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row mt-3 hide" id="containerTabelaAnormalidadeConsumo">
            <div class="col-sm-12">
                <div class="ocultarTabela containerTabela">
                    <table id="table-anormalidades-consumo" class="table table-bordered table-striped table-hover" width="100%">
                        <thead class="thead-ggas-bootstrap">
                        <th class="text-center"></th>
                        <th class="text-center">Ativo</th>
                        <th class="text-left">Descri��o</th>
                        <th class="text-left max-width-pesquisa-anormalidade">Regra para Aplica��o da Anormalidade</th>
                        <th class="text-center max-width-pesquisa-anormalidade">N�mero de ocorr�ncias consecutivas da anormalidade</th>
                        </thead>
                        <tbody>
                        <c:forEach items="${listaAnormalidadeConsumo}" var="anormalidade">
                            <tr>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
                                        <input id="chk${anormalidade.chavePrimaria}" type="checkbox"  name="chavesPrimariasConsumo"
                                               data-chaveprimaria="${anormalidade.chavePrimaria}"
                                               class="custom-control-input"
                                               value="${anormalidade.chavePrimaria}">
                                        <label class="custom-control-label p-0" for="chk${anormalidade.chavePrimaria}"></label>
                                    </div>
                                </td>
								<td class="text-center">
									<c:choose>
										<c:when test="${anormalidade.habilitado == true}">
											<i class="fa fa-check-circle text-success"></i> Sim
										</c:when>
										<c:otherwise>
											<i class="fa fa-times-circle text-danger"></i> N�o
										</c:otherwise>
									</c:choose>
								</td>
                                <td class="text-left">
                                    <a class="linkGoDetalhamentoConsumo" data-chavePrimaria="${anormalidade.chavePrimaria}"
                                       href="javascript:void(0)">
                                            ${anormalidade.descricao}
                                    </a>
                                </td>
                                <td class="text-left">
                                    ${anormalidade.regra}
                                </td>
                                <td class="text-center">
                                    ${anormalidade.numeroOcorrenciaConsecutivasAnormalidade}
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer">
        <vacess:vacess param="exibirInclusaoAnormalidade">
            <a href="exibirInclusaoAnormalidade" id="buttonIncluir" class="hide btn btn-sm btn-primary float-right mr-1 mt-1">
                <i class="fa fa-plus"></i> Incluir
            </a>
        </vacess:vacess>
        
        <vacess:vacess param="exibirAlteracaoAnormalidade">
            <a href="javascript:void(0)" id="btnAlterar" class="btn btn-primary float-left btn-sm mr-1 mt-1">
                <i class="fa fa-edit"></i> Alterar
            </a>
            <a href="javascript:void(0)" class="hide btn btn-danger float-left btn-sm mr-1 mt-1" id="btnRemover">
                <i class="fa fa-trash"></i> Remover
            </a>
        </vacess:vacess>

    </div>

</div>

<script src="${ctxWebpack}/dist/modulos/medicao/anormalidade/pesquisa/index.js"></script>

