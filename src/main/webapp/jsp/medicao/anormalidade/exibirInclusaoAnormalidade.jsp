<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<form:form method="post" action="incluirAnormalidade" name="anormalidadeForm">
<input name="postBack" type="hidden" id="postBack" value="true">
<div class="card">
	<div class="card-header">
		<h5 class="card-title mb-0">Incluir Anormalidade de Leitura</h5>
	</div>
	<div class="card-body">

		<div class="alert alert-primary fade show" role="alert">
			<i class="fa fa-question-circle"></i>
			Informe os dados abaixos e clique em <b>'Salvar'</b> para cadastrar uma nova anormalidade de leitura
		</div>

		<input hidden value="false" name="ehTipoAnormalidadeConsumo"/>

		<div class="row">
			<div class="col-sm-6">
				<label for="descricao">Descri��o <span class="text-danger">*</span></label>
				<input id="descricao" type="text" class="form-control form-control-sm" name="descricao" maxlength="30" size="40"
					   value="${anormalidade.descricao}" />
			</div>
			<div class="col-sm-6">
				<label for="descricaoAbreviada">Descri��o abreviada</label>
				<input type="text" class="form-control form-control-sm" id="descricaoAbreviada" name="descricaoAbreviada" maxlength="5" size="5"
					   value="${anormalidade.descricaoAbreviada}" />
			</div>
		</div>

		<hr/>

		<div class="row">
			<div class="col-sm-6">
				<label for="mensagemLeitura">Mensagem impressa em conta </label>
				<input class="form-control form-control-sm" type="text"
					   id="mensagemLeitura" name="mensagemLeitura" maxlength="100" size="100" value="${anormalidade.mensagemLeitura}">
			</div>
			<div class="col-sm-6">
				<label for="descricaoAbreviada">Orienta��es para manuten��o</label>
				<input type="text" class="form-control form-control-sm" id="mensagemManutencao" name="mensagemManutencao" maxlength="100"
					   size="100" value="${anormalidade.mensagemManutencao}" />
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<label for="mensagemPrevencao">Orienta��es para preven��o</label>
				<input class="form-control form-control-sm" type="text"
					   id="mensagemPrevencao" name="mensagemPrevencao" maxlength="100" size="100" value="${anormalidade.mensagemPrevencao}">
			</div>
			<div class="col-sm-6">
				<label for="numeroOcorrenciaConsecutivasAnormalidade">N�mero de ocorr�ncias consecutivas da anormalidade</label>
				<input type="text" class="form-control form-control-sm" id="numeroOcorrenciaConsecutivasAnormalidade"
					   name="numeroOcorrenciaConsecutivasAnormalidade" maxlength="8" size="8"
					   onkeypress="return formatarCampoInteiro(event)"  value="${anormalidade.numeroOcorrenciaConsecutivasAnormalidade}" />
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<label for="numeroOcorrenciaConsecutivasAnormalidade">Regra para aplica��o da anormalidade</label>
				<textarea name="regra" class="form-control form-control-sm"
						  oninput='return formatarCampoTextoComLimite(event,this,120);'
						  maxlength="120"
				>${anormalidade.regra}</textarea>
			</div>
		</div>

		<hr/>

		<div class="row">
			<div class="col-sm-6">
				<h5>Com Leitura Informada</h5>

				<div class="row">
					<div class="col-sm-12">
						<label for="mensagemPrevencao">A��o para a leitura <span class="text-danger">*</span></label>
						<select class="form-control form-control-sm" type="text"
							   id="acaoAnormalidadeComLeitura" name="acaoAnormalidadeComLeitura">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaAcaoAnormalidadeComLeitura}" var="acaoAnormalidadeComLeitura">
								<option value="<c:out value="${acaoAnormalidadeComLeitura.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeComLeitura == acaoAnormalidadeComLeitura.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${acaoAnormalidadeComLeitura.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>

					<div class="col-sm-12">
						<label for="mensagemPrevencao">A��o para o consumo <span class="text-danger">*</span></label>
						<select class="form-control form-control-sm" type="text"
								id="acaoAnormalidadeComConsumo" name="acaoAnormalidadeComConsumo">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaAcaoAnormalidadeConsumoComLeitura}" var="acaoAnormalidadeComConsumo">
								<option value="<c:out value="${acaoAnormalidadeComConsumo.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeComConsumo == acaoAnormalidadeComConsumo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${acaoAnormalidadeComConsumo.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>


			<div class="col-sm-6">
				<h5>Sem Leitura Informada</h5>

				<div class="row">
					<div class="col-sm-12">
						<label for="idAcaoAnormalidadeSemLeitura">A��o para a leitura <span class="text-danger">*</span></label>
						<select class="form-control form-control-sm" type="text"
								id="idAcaoAnormalidadeSemLeitura" name="acaoAnormalidadeSemLeitura">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaAcaoAnormalidadeSemLeitura}" var="acaoAnormalidadeSemLeitura">
								<option value="<c:out value="${acaoAnormalidadeSemLeitura.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeSemLeitura == acaoAnormalidadeSemLeitura.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${acaoAnormalidadeSemLeitura.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-sm-12">
						<label for="idAcaoAnormalidadeSemConsumo">A��o para o consumo <span class="text-danger">*</span></label>
						<select class="form-control form-control-sm" type="text"
								id="idAcaoAnormalidadeSemConsumo" name="acaoAnormalidadeSemConsumo">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaAcaoAnormalidadeConsumoSemLeitura}" var="acaoAnormalidadeSemConsumo">
								<option value="<c:out value="${acaoAnormalidadeSemConsumo.chavePrimaria}"/>" <c:if test="${anormalidade.acaoAnormalidadeSemConsumo == acaoAnormalidadeSemConsumo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${acaoAnormalidadeSemConsumo.descricao}"/>
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
		</div>

		<hr/>

		<div class="row">
			<div class="col-sm-3">
				<label class="w-100">Aceita Leitura?</label>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="aceitaLeituraSim" name="aceitaLeitura" value="true"
						   <c:if test="${anormalidade.aceitaLeitura eq 'true'}">checked</c:if>>
					<label class="custom-control-label" for="aceitaLeituraSim">Sim</label>
				</div>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="aceitaLeituraNao" name="aceitaLeitura" value="false"
						   <c:if test="${anormalidade.aceitaLeitura eq null || anormalidade.aceitaLeitura eq 'false'}">checked</c:if>>
					<label class="custom-control-label" for="aceitaLeituraNao">N�o</label>
				</div>
			</div>

			<div class="col-sm-3">
				<label class="w-100">Relativo ao Medidor?</label>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="relativoMedidorSim" name="relativoMedidor" value="true"
						   <c:if test="${anormalidade.relativoMedidor eq 'true'}">checked</c:if>>
					<label class="custom-control-label" for="relativoMedidorSim">Sim</label>
				</div>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="relativoMedidorNao" name="relativoMedidor" value="false"
						   <c:if test="${anormalidade.relativoMedidor eq null || anormalidade.relativoMedidor eq 'false'}">checked</c:if>>
					<label class="custom-control-label" for="relativoMedidorNao">N�o</label>
				</div>
			</div>

			<div class="col-sm-3">
				<label class="w-100">Bloquear Faturamento?</label>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="bloquearFaturamentoSim" name="bloquearFaturamento" value="true"
						   <c:if test="${anormalidade.bloquearFaturamento eq 'true'}">checked</c:if>>
					<label class="custom-control-label" for="bloquearFaturamentoSim">Sim</label>
				</div>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="bloquearFaturamentoNao" name="bloquearFaturamento" value="false"
						   <c:if test="${anormalidade.bloquearFaturamento eq null || anormalidade.bloquearFaturamento eq 'false'}">checked</c:if>>
					<label class="custom-control-label" for="bloquearFaturamentoNao">N�o</label>
				</div>
			</div>

			<div class="col-sm-3">
				<label class="w-100">Exige foto na leitura?</label>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="exigeFotoSim" name="exigeFoto" value="true"
						   <c:if test="${anormalidade.exigeFoto eq 'true'}">checked</c:if>>
					<label class="custom-control-label" for="exigeFotoSim">Sim</label>
				</div>
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="exigeFotoSimNao" name="exigeFoto" value="false"
						   <c:if test="${anormalidade.exigeFoto eq null || anormalidade.exigeFoto eq 'false'}">checked</c:if>>
					<label class="custom-control-label" for="exigeFotoSimNao">N�o</label>
				</div>
			</div>
		</div>

		<div class="row mt-2">
			<div class="col-sm-12">
				<span><span class="text-danger">*</span> <i class="text-muted">campos obrigat�rios</i></span>
			</div>
		</div>

	</div>

	<div class="card-footer">
		<a href="voltaAnormalidade" class="btn btn-secondary float-left btn-sm mt-sm-1">
			<i class="fa fa-chevron-circle-left"></i> Voltar
		</a>
		<button type="submit" class="btn btn-primary float-right btn-sm mt-sm-1" id="salvar">
			<i class="fa fa-check"></i> Salvar
		</button>
	</div>

</div>

<token:token></token:token>
</form:form>
