<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<div class="card">
	<div class="card-header">
		<h5 class="card-title mb-0">Detalhamento de Anormalidade de Consumo</h5>
	</div>
	<div class="card-body">

		<div class="alert alert-primary fade show" role="alert">
			<i class="fa fa-question-circle"></i>
			Visualize as informa��es da anormalidade de consumo selecionada.<br/>
			Para modificar as informa��es deste registro clique em <b>'Alterar'</b>
		</div>

		<div class="row">
			<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${anormalidade.chavePrimaria}">

			<div class="col-sm-12">
				<label><b>Descri��o: </b></label>
				<label>${anormalidade.descricao}</label>
			</div>

			<div class="col-sm-12">
				<label><b>Descri��o abreviada: </b></label>
				<label>${anormalidade.descricaoAbreviada}</label>
			</div>

			<div class="col-sm-12">
				<label><b>Regra para Aplica��o da Anormalidade: </b></label>
				<label>${anormalidade.regra}</label>
			</div>

			<div class="col-sm-12">
				<label><b>N�mero de ocorr�ncias consecutivas: </b></label>
				<label>${anormalidade.numeroOcorrenciaConsecutivasAnormalidade}</label>
			</div>

			<div class="col-sm-12">
				<label><b>Bloquear Faturamento: </b></label>
				<label>
					<c:choose>
					<c:when test="${anormalidade.bloquearFaturamento eq true}">
						Sim
					</c:when>
					<c:otherwise>
						N�o
					</c:otherwise>
					</c:choose>
				</label>
			</div>

			<div class="col-sm-12">
				<label><b>Ignorar a��o autom�tica: </b></label>
				<label>
					<c:choose>
					<c:when test="${anormalidade.ignorarAcaoAutomatica eq true}">
						Sim
					</c:when>
					<c:otherwise>
						N�o
					</c:otherwise>
					</c:choose>
				</label>
			</div>

			<div class="col-sm-12">
				<label><b>Indicador de Uso: </b></label>
				<label>
					<c:choose>
						<c:when test="${anormalidade.habilitado eq true}">
							Ativo
						</c:when>
						<c:otherwise>
							Inativo
						</c:otherwise>
					</c:choose>
				</label>
			</div>

			<div class="col-sm-12">
				<label><b>Mensagem impressa em conta: </b></label>
				<label>${anormalidade.mensagemConsumo}</label>
			</div>

			<c:if test="${CODIGO_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_ZERO}">
				<hr/>
				<div class="col-sm-12">
					<label>Parametriza��o</label>
					<jsp:include page="/jsp/medicao/anormalidade/gridParametrizacao.jsp" />
				</div>
			</c:if>
		</div>

	</div>

	<div class="card-footer">
		<div class="row">
			<div class="col-sm-12">
				<a href="voltaAnormalidade" class="btn btn-secondary float-left btn-sm"><i class="fa fa-chevron-circle-left"></i> Voltar</a>
			</div>
		</div>
	</div>

</div>
