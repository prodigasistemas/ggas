<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<h1 class="tituloInterno">
	Consultar Hist�rico de Leitura e Consumo<a class="linkHelp"
		href="<help:help>/histricodeleituraeconsumo.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>

<p class="orientacaoInicial">
	Para pesquisar um registro espec�fico, informe os dados nos campos
	abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>,
	ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>
	para exibir todos.
</p>
<script>
	$(document).ready(function() {
		habilitaRotas();
		habilitarBotaoPesquisar();
		$("select#idGrupoFaturamento").change(function() {
			habilitarBotaoPesquisar();
		});

		$("#cepImovel").blur(function() {

			habilitarBotaoPesquisar();
		});

		$("input#botaoPesquisar").attr("disabled", "disabled");
	});

	function exibirHistoricoLeituraConsumoImovel(idImovel) {
		document.forms[0].idImovel.value = idImovel;
		submeter(
				"historicoLeituraMedicaoForm",
				"exibirHistoricoLeituraConsumoImovel");

	}
	
	function pesquisar(){
		var emailClienteTexto = document.forms[0].emailClienteTexto.value;
		
		if(emailClienteTexto == ""){
			$("#emailCliente").prop("name", "");
		}
	}

	function limparFormulario() {
		
		document.forms[0].nomeClienteTexto.value = "";
		document.forms[0].documentoFormatadoTexto.value = "";
		document.forms[0].enderecoFormatadoTexto.value = "";
		document.forms[0].emailClienteTexto.value = "";
		
		document.forms[0].nome.value = "";
		document.forms[0].complementoImovel.value = "";
		document.forms[0].matricula.value = "";
		document.forms[0].numeroImovel.value = "";
		document.getElementById("pontoConsumoLegado").value = "";
		document.forms[0].indicadorCondominioAmbos[2].checked = true;
		document.forms[0].habilitado[0].checked = true;
		document.forms[0].cepImovel.value = "";
		
		document.forms[0].idGrupoFaturamento.value = "-1";
		document.forms[0].idRota.value = "-1";
		
		document.forms[0].idCliente.value = "";
		document.forms[0].nomeCompletoCliente.value = "";
		document.forms[0].documentoFormatado.value = "";
		document.forms[0].enderecoFormatadoCliente.value = "";
		document.forms[0].emailCliente.value = "";
		carregarRotasPorGrupo("-1");
		habilitarBotaoPesquisar();
	}

	function carregarRotasPorGrupo(codGrupoFaturamento) {
		var selectRotas = document.getElementById("idRota");
		var idRota = "${historicoLeituraConsumoVO.idRota}";
		selectRotas.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectRotas.options[selectRotas.length] = novaOpcao;
		AjaxService.listarRotaPorGrupoFaturamento(codGrupoFaturamento, {
			callback : function(rotas) {
				for (key in rotas) {
					var novaOpcao = new Option(rotas[key], key);
					if (key == idRota) {
						novaOpcao.selected = true;
					}
					selectRotas.options[selectRotas.length] = novaOpcao;
				}
				ordernarSelect(selectRotas)
			},
			async : false
		});
		habilitaRotas();
	}

	function habilitaRotas() {
		if ($("#idRota option").length > 1) {
			$("#idRota").removeAttr("disabled")
					.removeClass("campoDesabilitado");
			$("#rotuloRota").removeClass("rotuloDesabilitado");
		} else {
			$("#idRota").attr("disabled", "disabled").addClass(
					"campoDesabilitado");
			$("#rotuloRota").addClass("rotuloDesabilitado");
		}
	}

	function exibirPopupPesquisaCliente() {
		popup = window
				.open(
						'exibirPesquisaClientePopup',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes ,modal=yes');
	}

	function selecionarCliente(idSelecionado) {
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document
				.getElementById("nomeCompletoCliente");
		var documentoFormatado = document.getElementById("documentoFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document
				.getElementById("enderecoFormatadoCliente");

		if (idSelecionado != '') {
			AjaxService.obterClientePorChave(idSelecionado, {
				callback : function(cliente) {
					if (cliente != null) {
						idCliente.value = cliente["chavePrimaria"];
						nomeCompletoCliente.value = cliente["nome"];
						if (cliente["cnpj"] != undefined) {
							documentoFormatado.value = cliente["cnpj"];
						} else {
							if (cliente["cpf"] != undefined) {
								documentoFormatado.value = cliente["cpf"];
							} else {
								documentoFormatado.value = "";
							}
						}
						emailCliente.value = cliente["email"];
						enderecoFormatado.value = cliente["enderecoFormatado"];
					}
				},
				async : false
			}

			);
		} else {
			idCliente.value = "";
			nomeCompletoCliente.value = "";
			documentoFormatado.value = "";
			emailCliente.value = "";
			enderecoFormatado.value = "";
		}

		document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
		document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
		document.getElementById("emailClienteTexto").value = emailCliente.value;
		document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
		habilitarBotaoPesquisar()
	}

	function habilitarBotaoPesquisar() {

		var idCliente = document.getElementById("idCliente").value;
		var cepImovel = document.getElementById("cepImovel").value;
		var matricula = document.getElementById("matricula").value;
		var idGrupo = document.getElementById("idGrupoFaturamento").value;
		var descImovel = document.getElementById("nome").value;
		var complementoImovel = document.getElementById("complementoImovel").value;

		if ((idCliente != "" && idCliente != "0")
				|| (cepImovel != "_____-___" && cepImovel != "")
				|| (idGrupo != "-1") || (matricula != "")
				|| (complementoImovel != "") || (descImovel != "")) {
			$("input#botaoPesquisar").removeAttr("disabled");
		} else {
			$("input#botaoPesquisar").attr("disabled", "disabled");
		}

	}
</script>
<form method="post" id="historicoLeituraMedicaoForm" name="historicoLeituraMedicaoForm"
	action="pesquisaImovelConsultaHistoricoLeituraConsumo">
	<input name="acao" type="hidden" id="acao"
		value="pesquisaImovelConsultaHistoricoLeituraConsumo" />
	<input name="idImovel" type="hidden" id="idImovel" value="" />
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa"
		value="true">

	<fieldset id="consultarHistoricoLeituraConsumo"
		class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<legend>
				<span class="campoObrigatorioSimbolo">** </span> Pesquisar Cliente
			</legend>
			<div class="pesquisarClienteFundo">
				<p class="orientacaoInterna">
					Clique em <span class="destaqueOrientacaoInicial">Pesquisar
						Cliente</span> para selecionar um Cliente.
				</p>
				<input name="idCliente" type="hidden" id="idCliente"
					value="${clientePopupVO.idCliente}"> <input
					name="nomeCliente" type="hidden" id="nomeCompletoCliente"
					value="${clientePopupVO.nomeCliente}">
				<input name="documentoCliente" type="hidden"
					id="documentoFormatado"
					value="${clientePopupVO.documentoCliente}">
				<input name="enderecoCliente" type="hidden"
					id="enderecoFormatadoCliente"
					value="${clientePopupVO.enderecoCliente}">
				<input name="emailCliente" type="hidden" id="emailCliente"
					value="${clientePopupVO.emailCliente}"> 
				<input
					name="Button" id="botaoPesquisarCliente" class="bottonRightCol2"
					title="Pesquisar cliente" value="Pesquisar cliente"
					onclick="exibirIndicador(); exibirPopupPesquisaCliente();"
					type="button">
				<br> <label class="rotulo"
					id="rotuloCliente" for="nomeClienteTexto">Cliente:</label> <input
					class="campoDesabilitado" type="text" id="nomeClienteTexto"
					name="nomeCliente" maxlength="50" size="50"
					disabled="disabled"
					value="${clientePopupVO.nomeCliente}"><br />
				<label class="rotulo" id="rotuloCnpjTexto"
					for="documentoFormatadoTexto">CPF/CNPJ:</label> <input
					class="campoDesabilitado" type="text" id="documentoFormatadoTexto"
					name="documentoCliente" maxlength="18" size="18"
					disabled="disabled"
					value="${clientePopupVO.documentoCliente}"><br />
				<label class="rotulo" id="rotuloEnderecoTexto"
					for="enderecoFormatadoTexto">Endere�o:</label>
				<textarea class="campoDesabilitado" id="enderecoFormatadoTexto"
					name="enderecoCliente" rows="2" cols="37"
					disabled="disabled">${clientePopupVO.enderecoCliente}</textarea>
				<br /> <label class="rotulo" id="rotuloEmailClienteTexto"
					for="emailClienteTexto">E-mail:</label> <input
					class="campoDesabilitado" type="text" id="emailClienteTexto"
					name="emailCliente" maxlength="80" size="40"
					disabled="disabled"
					value="${clientePopupVO.emailCliente}"><br />
			</div>
		</fieldset>

		<fieldset id="pesquisarImovel" class="colunaDir4">
			<legend>
				<span class="campoObrigatorioSimbolo">** </span> Pesquisar Im�vel
			</legend>
			<div class="pesquisarImovelFundo">
				<label class="rotulo" id="rotuloNomeFantasia" for="nome">Descri��o:</label>
				<input class="campoTexto" type="text" id="nome" name="nome"
					maxlength="50" size="37"
					value="${historicoLeituraConsumoVO.nome}"
					onblur="habilitarBotaoPesquisar()"
					onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
				<label class="rotulo" id="rotuloComplemento" for="complementoImovel">Complemento:</label>
				<input class="campoTexto" type="text" id="complementoImovel"
					name="complementoImovel" maxlength="25" size="30"
					value="${historicoLeituraConsumoVO.complementoImovel}"
					onblur="habilitarBotaoPesquisar()"
					onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
				<br /> <label class="rotulo" id="rotuloMatricula" for="matricula">Matr�cula:</label>
				
				<input class="campoTexto" type="text" id="matricula"
					name="matricula" maxlength="9" size="9"
					value="${historicoLeituraConsumoVO.matricula}"
					onkeypress="return formatarCampoInteiro(event)"
					onblur="habilitarBotaoPesquisar() "
					onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"><br />
					<label class="rotulo" id="rotuloMatricula" for="numeroImovel">N�mero do im�vel:</label>
					
				<input class="campoTexto campo2Linhas" type="text"
					id="numeroImovel" name="numeroImovel" maxlength="9" size="9"
					value="${historicoLeituraConsumoVO.numeroImovel}"
					onkeypress="return formatarCampoNumeroEndereco(event, this);"><br />
					
				<label class="rotulo" id="rotuloPontoConsumoLegado" for="pontoConsumoLegado">C&oacute;digo do Ponto </br> de Consumo:</label>
				<input class="campoTexto" type="text" id="pontoConsumoLegado"
					name="pontoConsumoLegado" maxlength="9" size="9"
					value="${historicoLeituraConsumoVO.pontoConsumoLegado}"
					onkeypress="return formatarCampoInteiro(event)"><br />
				
				<label class="rotulo" id="rotuloImovelCondominio" for="condominioSim">O im�vel � condom�nio?</label> 
				<input class="campoRadio" type="radio"
					value="true" name="indicadorCondominioAmbos" id="condominioSim"
					<c:if test="${historicoLeituraConsumoVO.indicadorCondominioAmbos eq 'true'}">checked</c:if>><label
					class="rotuloRadio">Sim</label> 
				
				<input class="campoRadio"
					type="radio" value="false" name="indicadorCondominioAmbos"
					id="condominioNao"
					<c:if test="${historicoLeituraConsumoVO.indicadorCondominioAmbos eq 'false'}">checked</c:if>><label
					class="rotuloRadio">N�o</label> 
				
				<input class="campoRadio"
					type="radio" value="ambos" name="indicadorCondominioAmbos"
					id="condominioAmbos"
					<c:if test="${historicoLeituraConsumoVO.indicadorCondominioAmbos ne 'true' && historicoLeituraConsumoVO.indicadorCondominioAmbos ne 'false'}">checked</c:if>><label
					class="rotuloRadio">Todos</label><br /> <br /> <label
					class="rotulo" for="habilitado">Indicador de Uso:</label>
				
				<input
					class="campoRadio" type="radio" name="habilitado"
					id="habilitadoSim" value="true"
					<c:if test="${historicoLeituraConsumoVO.habilitado eq 'true'}">checked</c:if>>
					<label class="rotuloRadio" for="indicadorUso">Ativo</label>
				
				<input
					class="campoRadio" type="radio" name="habilitado"
					id="habilitadoNao" value="false"
					<c:if test="${historicoLeituraConsumoVO.habilitado eq 'false'}">checked</c:if>>
					<label class="rotuloRadio" for="indicadorUso">Inativo</label>
				
				<input
					class="campoRadio" type="radio" name="habilitado"
					id="habilitadoAmbos" value="ambos"
					<c:if test="${historicoLeituraConsumoVO.habilitado ne 'true' && historicoLeituraConsumoVO.habilitado ne 'false'}">checked</c:if>>
					<label class="rotuloRadio">Todos</label><br /> <br />
					
				<fieldset class="exibirCep" id="exibirCep">
					<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
						<jsp:param name="cepObrigatorio" value="false" />
						<jsp:param name="idCampoCep" value="cepImovel" />
						<jsp:param name="numeroCep" value="${historicoLeituraConsumoVO.cepImovel}" />
					</jsp:include>
				</fieldset>
			</div>
		</fieldset>

		<fieldset id="consultarHistoricoLeituraConsumoCamposExtras">

			<label class="rotulo2Linhas" id="rotuloGrupoFaturamento"
				for="idGrupoFaturamento"><span
				class="campoObrigatorioSimbolo">** </span>Grupo de Faturamento:</label> <select
				name="idGrupoFaturamento" id="idGrupoFaturamento"
				class="campoSelect campoHorizontal"
				onchange="carregarRotasPorGrupo(this.value);">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaGrupoFaturamento}" var="grupo">
					<option value="<c:out value="${grupo.chavePrimaria}"/>"
						<c:if test="${historicoLeituraConsumoVO.idGrupoFaturamento == grupo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${grupo.descricao}" />
					</option>
				</c:forEach>
			</select> <label class="rotulo rotuloHorizontal" id="rotuloRota" for="idRota">Rota:</label>
			<select name="idRota" id="idRota" class="campoSelect campoHorizontal">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaRota}" var="rota">
					<option value="<c:out value="${rota.chavePrimaria}"/>"
						<c:if test="${historicoLeituraConsumoVO.idRota == rota.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${rota.numeroRota}" />
					</option>
				</c:forEach>
			</select> <img style="display: none" class="imgProc" id="imgProcIdRota"
				alt="Pesquisando..." title="Pesquisando..."
				src="<c:url value="/imagens/indicator.gif"/>"><br /> <br />

		</fieldset>

		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess
				param="pesquisaImovelConsultaHistoricoLeituraConsumo">
				<input name="Button" class="bottonRightCol2" id="botaoPesquisar" onclick="pesquisar()"
					value="Pesquisar" type="submit">
			</vacess:vacess>
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo "
				value="Limpar" type="button" onclick="limparFormulario();">
		</fieldset>

	</fieldset>

	<c:if test="${listaImoveis ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table export="false"
			class="dataTableGGAS dataTableCabecalho2Linhas" name="listaImoveis"
			sort="list" id="imovel" pagesize="15"
			requestURI="pesquisaImovelConsultaHistoricoLeituraConsumo">
			<display:column media="html" sortable="true"
				title="Matr�cula<br />do Im�vel" style="width: 80px"
				sortProperty="chavePrimaria">
				<a
					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span
					class="linkInvisivel"></span> <c:out
						value='${imovel.chavePrimaria}' /> </a>
			</display:column>
			<display:column media="pdf" title="Matr�cula<br />do Im�vel">
				<a
					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span
					class="linkInvisivel"></span> <c:out
						value='${imovel.chavePrimaria}' /> </a>
			</display:column>
			<display:column media="html" sortable="true" title="Nome"
				style="text-align: left; width: 260px" sortProperty="nome">
				<a
					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span
					class="linkInvisivel"></span> <c:out value='${imovel.nome}' /> </a>
			</display:column>
			<display:column media="pdf" title="Nome">
				<a
					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span
					class="linkInvisivel"></span> <c:out value='${imovel.nome}' /> </a>
			</display:column>
			<%-- 			<display:column media="html" sortable="false" title="Inscri��o"> --%>
			<!-- 				<a -->
			<%-- 					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span --%>
			<%-- 					class="linkInvisivel"></span> <c:out value='${imovel.inscricao}' /> --%>
			<!-- 				</a> -->
			<%-- 			</display:column> --%>
			<%-- 			<display:column media="pdf" title="Inscri��o"> --%>
			<!-- 				<a -->
			<%-- 					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span --%>
			<%-- 					class="linkInvisivel"></span> <c:out value='${imovel.inscricao}' /> --%>
			<!-- 				</a> -->
			<%-- 			</display:column> --%>
			<display:column media="html" sortable="false" title="Endere�o"
				style="text-align: left; width: 420px">
				<a
					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span
					class="linkInvisivel"></span> <c:out
						value='${imovel.enderecoFormatado}' /> </a>
			</display:column>
			<display:column media="pdf" title="Endere�o">
				<a
					href="javascript:exibirHistoricoLeituraConsumoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span
					class="linkInvisivel"></span> <c:out
						value='${imovel.enderecoFormatado}' /> </a>
			</display:column>
		</display:table>
	</c:if>
	<p class="legenda">
		<span class="campoObrigatorioSimbolo">** </span>� necess�rio escolher
		ao menos um dos filtros, para a gera��o do relat�rio
	</p>
</form>