<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<script type="text/javascript">
	window.opener.esconderIndicador();

	$(document).ready(function() {
		// Dialog			
		configurarModalExportacao({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400});
		
		$("*[name='formatoImpressaoRadio']").change(function(){
			formatoImpressao = ($(this).attr('value'));
        	$("#opcaoExpotacaoSelecionada").val(formatoImpressao);
        	
        });
		
		campoMarcado = new Array();
		
		$("input[type=checkbox][name='exibirFiltrosCheck[]']:checked").each(function(){
		    campoMarcado.push($(this).val());
		});
		
	});

	//Corrige a exibi��o janela de op��es do relat�rio para todos os browsers
	function exibirExportarRelatorio(){
		var exibirFiltros = $("#exibirFiltrosCheck").val();
		$("#exibirFiltros").val(exibirFiltros)
		exibirOpcoesExportacao();
	}
	
	
	function gerar() {
		$(".mensagens").hide();			
		submeter("0", 'gerarRelatorioConsumoClienteMedidor');
	}

</script>

<h1 class="tituloInternoPopup">
	Detalhamento Hist�rico Consumo
	<a class="linkHelp"
		href="<help:help>/detalhamentohistricoconsumodirio.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');">
	</a>
</h1>

<form method="post">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${idPontoConsumo}"/>
	<input name="idMedidor" type="hidden" id="idMedidor" value="${idMedidor}"/>
	<input name="anoMesFaturamento" type="hidden" id="anoMesFaturamento" value="${anoMesFaturamento}"/>
	<input name="numeroCiclo" type="hidden" id="numeroCiclo" value="${numeroCiclo}"/>
	<input name="opcaoExpotacaoSelecionada" type="hidden" id="opcaoExpotacaoSelecionada" value="PDF"/>
	<input name="exibirFiltros" type="hidden" id="exibirFiltros" value=""/>
	
	<input name="idCliente" type="hidden" id="idCliente" value="${clientePopupVO.idCliente}"/>
	<input name="nomeCliente" type="hidden" id="nomeCliente" value="${clientePopupVO.nomeCliente}"/>
	<input name="documentoCliente" type="hidden" id="documentoCliente" value="${clientePopupVO.documentoCliente}"/>
	<input name="emailCliente" type="hidden" id="emailCliente" value="${clientePopupVO.emailCliente}"/>
	<input name="enderecoCliente" type="hidden" id="enderecoCliente" value="${clientePopupVO.enderecoCliente}"/>
	<input name="indicadorPesquisa" type="hidden" id="indicadorPesquisa" value="${clientePopupVO.indicadorPesquisa}"/>
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Hist�ricos de Consumo Di�rios do Ciclo</legend>
		
		<c:forEach items="${listaHistConsumoMedidor}" var="listaHistConsumoMedidor">
		
			<display:table id="historicoConsumo" 
				list="${listaHistConsumoMedidor.value}"
				class="dataTableGGAS dataTableMenor2 dataTableCabecalho2Linhas"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				export="false" sort="list" pagesize="30" 
				requestURI="exibirDetalhamentoHistoricoConsumoPontoConsumo">
				
				<display:column media="html" title="N�mero de S�rie">
					<c:out value='${historicoConsumo.historicoAtual.historicoInstalacaoMedidor.medidor.numeroSerie}' />
				</display:column>

				<display:column media="html" title="Data Leitura">
					<fmt:formatDate
						value="${historicoConsumo.historicoAtual.dataLeituraInformada}"
						pattern="dd/MM/yyyy" />
				</display:column>

				<display:column media="html" title="Consumo">
					<fmt:formatNumber value="${historicoConsumo.consumo}" type="number"
						maxFractionDigits="4" />
				</display:column>
				<display:column sortable="false" title="Fator PTZ">
					<fmt:formatNumber value="${historicoConsumo.fatorPTZ}"
						type="number" minFractionDigits="4" maxFractionDigits="4" />
				</display:column>

				<display:column media="html" title="Consumo Medido">
					<fmt:formatNumber value="${historicoConsumo.consumoMedido}"
						type="number" minFractionDigits="4" maxFractionDigits="4" />
				</display:column>

				<display:column sortable="false" title="Fator PCS">
					<fmt:formatNumber value="${historicoConsumo.fatorPCS}"
						type="number" minFractionDigits="4" maxFractionDigits="4" />
				</display:column>

				<display:column media="html" title="Consumo Apurado">
					<fmt:formatNumber value="${historicoConsumo.consumoApurado}"
						type="number" minFractionDigits="4" maxFractionDigits="4" />
				</display:column>

				<display:column media="html" title="Cr�dito Consumo">
					<fmt:formatNumber value="${historicoConsumo.consumoCredito}"
						type="number" minFractionDigits="4" maxFractionDigits="4" />
				</display:column>

				<display:column sortable="false" title="Fator Corre��o">
					<fmt:formatNumber value="${historicoConsumo.fatorCorrecao}"
						type="number" minFractionDigits="4" maxFractionDigits="4" />
				</display:column>

				<display:column media="html" title="M�dia Refer�ncial">
					<fmt:formatNumber value="${historicoConsumo.consumoApuradoMedio}"
						type="number" minFractionDigits="4" maxFractionDigits="4" />
				</display:column>

				<display:column media="html" title="Anormalidade">
					<span alt="${historicoConsumo.anormalidadeConsumo.descricao}"
						title="${historicoConsumo.anormalidadeConsumo.descricao}">
						${historicoConsumo.anormalidadeConsumo.chavePrimaria}</span>
				</display:column>

				<display:column media="html" title="Dias de Consumo">
					<c:out value='${historicoConsumo.diasConsumo}' />
				</display:column>

				<display:column media="html" title="Tipo de Consumo">
					<c:out value='${historicoConsumo.tipoConsumo.descricao}' />
				</display:column>

				<display:column media="html" title="Varia��o Consumo">
					<fmt:formatNumber type="number" maxFractionDigits="2" 
						value="${historicoConsumo.consumoApuradoPercentualConsumoMedio}" />%
			    </display:column>
			</display:table>
		</c:forEach>
	</fieldset>

	<fieldset class="conteinerBotoesPopup">
		<input name="Button" class="bottonRightCol2 botaoGrande1" id="botaoFechar"
			value="Fechar" type="button" onclick="window.close();">
		<input name="Button" class="bottonRightCol2 botaoGrande1" id="botaoGerar"
			value="Gerar" type="button" onclick="exibirExportarRelatorio();">
	</fieldset>
	<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp"/>
</form>