<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInternoPopup">Detalhamento Hist�rico Leitura<a class="linkHelp" href="<help:help>/histricodeleituraeconsumodetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<c:if test="${historicoLeituraConsumoVO.permitirAlteracao}">
<p class="orientacaoInicialPopup">Selecione um Hist�rico de Leitura abaixo para alterar os dados. Para sair sem alterar do dados, clique em <span class="destaqueOrientacaoInicial">Fechar</span> abaixo da tabela.</p>
</c:if>
<script>

window.opener.esconderIndicador();

function selecionarHistoricoMedicao(idSelecionado) {
	window.opener.selecionarHistoricoMedicao(idSelecionado);
	window.close();
}

</script>

<form>
	<fieldset>
		<legend class="conteinerBlocoTitulo">Hist�rico de Medi��o</legend>
		<!-- inserir tabela listando o Hist�rico de Leitura -->
		<display:table id="historicoMedicao" class="dataTableGGAS dataTablePopup dataTableCabecalho2Linhas" export="false" name="listaHistoricoMedicao" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoHistoricoLeituraAnaliseExcecao">
	 
	     	<display:column media="html" title="M�s / Ano-Ciclo">
	     		<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<c:out value='${historicoMedicao.anoMesLeituraFormatado}'/>-<c:out value='${historicoMedicao.numeroCiclo}'/>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<c:out value='${historicoMedicao.anoMesLeituraFormatado}'/>-<c:out value='${historicoMedicao.numeroCiclo}'/>
	     			</c:otherwise>
	     		</c:choose>
			</display:column>
			
			<display:column media="html" title="Data / Hora<br />Leitura Considerada">
				<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<fmt:formatDate value="${historicoMedicao.dataLeituraFaturada}" pattern="dd/MM/yyyy: HH:mm"/>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<fmt:formatDate value="${historicoMedicao.dataLeituraFaturada}" pattern="dd/MM/yyyy: HH:mm"/>
	     			</c:otherwise>
	     		</c:choose>
	     	</display:column>
	     	
	     	<display:column media="html" title="Leitura Considerada">
	     		<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<fmt:formatNumber value="${historicoMedicao.numeroLeituraFaturada}" type="number"/>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<fmt:formatNumber value="${historicoMedicao.numeroLeituraFaturada}" type="number"/>
	     			</c:otherwise>
	     		</c:choose>
	     	</display:column>
		    
		    <display:column sortable="false"  title="Anormalidade<br />Considerada">
		    	<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<span title="${historicoMedicao.anormalidadeLeituraFaturada.descricao}">${historicoMedicao.anormalidadeLeituraFaturada.chavePrimaria}</span>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<span title="${historicoMedicao.anormalidadeLeituraFaturada.descricao}">${historicoMedicao.anormalidadeLeituraFaturada.chavePrimaria}</span>
	     			</c:otherwise>
	     		</c:choose>
			</display:column>		    
			    
		    <display:column media="html" title="Data<br />Anterior">
		    	<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<fmt:formatDate value="${historicoMedicao.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<fmt:formatDate value="${historicoMedicao.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>
	     			</c:otherwise>
	     		</c:choose>
		    </display:column>	 
	
		    
		     <display:column media="html" title="Leitura<br />Anterior">
		     	<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<fmt:formatNumber value="${historicoMedicao.numeroLeituraAnterior}" type="number"/>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<fmt:formatNumber value="${historicoMedicao.numeroLeituraAnterior}" type="number"/>
	     			</c:otherwise>
	     		</c:choose>
		    </display:column>
			
			<display:column sortable="false" title="Volume<br />Apurado">
				<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<fmt:formatNumber value="${historicoMedicao.consumoInformado}" maxFractionDigits="4"/>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<fmt:formatNumber value="${historicoMedicao.consumoInformado}" maxFractionDigits="4"/>
	     			</c:otherwise>
	     		</c:choose>				
			</display:column>
		
		    <display:column sortable="false"  title="Medidor">
		    	<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<span alt="Data instala��o: <fmt:formatDate value="${historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoMedicao.historicoInstalacaoMedidor.medidor.digito}" title="Data instala��o: <fmt:formatDate value="${historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoMedicao.historicoInstalacaoMedidor.medidor.digito}">${historicoMedicao.historicoInstalacaoMedidor.medidor.numeroSerie}</span>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<span alt="Data instala��o: <fmt:formatDate value="${historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoMedicao.historicoInstalacaoMedidor.medidor.digito}" title="Data instala��o: <fmt:formatDate value="${historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoMedicao.historicoInstalacaoMedidor.medidor.digito}">${historicoMedicao.historicoInstalacaoMedidor.medidor.numeroSerie}</span>
	     			</c:otherwise>
	     		</c:choose>
			</display:column>	    

		    <display:column media="html" title="Situa��o<br />Atual">
		    	<c:choose>
	     			<c:when test="${historicoLeituraConsumoVO.permitirAlteracao}">
	     				<a href='javascript:selecionarHistoricoMedicao(<c:out value='${historicoMedicao.chavePrimaria}'/>);'>
							<c:out value='${historicoMedicao.situacaoLeitura.descricao}'/>
						</a>
	     			</c:when>
	     			<c:otherwise>
	     				<c:out value='${historicoMedicao.situacaoLeitura.descricao}'/>
	     			</c:otherwise>
	     		</c:choose>				
		    </display:column>	
	    </display:table>
    </fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2 botaoGrande1" value="Fechar" type="button" onclick="window.close();">
 	</fieldset>
</form>