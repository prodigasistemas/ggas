<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>

	var charts = [],
	defaultOptions,
	options;
	
	Highcharts.visualize = function(table, options) {

		//Itens do eixo X
		options.xAxis.categories = [];
				    
		//Dados das s�ries
		options.series = [];
		var seriesIndex = 0;
		
		$('thead tr th', table).each(function(i, tr) {
		 
		    var th = this;
	    	
		  	/* Seleciona quais colunas da tabela ser�o exibidas (�ndice de base zero).
			Ex.: if(i == 2 || i == 3 || i == 9) <-- exibe a 3�, 4� e 8� colunas  */
		    if(i == 5){
		    	
			    options.series[seriesIndex] = {
			        name: $(th).text(),
			        data: []
			    }
			    
				var coluna = ++i;
			    
			    $('tbody tr td:nth-child('+ coluna +')', table).each(function(j){ 
			    	
			    	// alert($(this).text());
				    var num = $(this).text().trim().replace(/\./g,"").replace(",",".");
				    if (num=='') { num = 0; } 
					options.series[seriesIndex].data.push(parseFloat(num));
			    
			    });
			    
			    $('tbody tr td:nth-child(1)', table).each(function(j){
			    	options.xAxis.categories.push($(this).text());
			    });
			    
			    ++seriesIndex;
		    }
		    
		});
		
		// alert(JSON.stringify(options));
		
		charts[charts.length] = new Highcharts.Chart(options);
		
	}
	
	defaultOptions = {
			
		chart: {
		  renderTo: 'conteinerGraficoConsumo',
		  type: 'line'
		},
		credits: {
		  enabled: 0
		},
		title: {
		  text: 'Consumo Apurado do Per�odo'
		},
		xAxis: {
			labels: {
            align: 'right'
		  }
		},
		yAxis: {			
		  	title: {
				text: 'm�'
			},
			min: 0,
			labels: {
	            align: 'left',
	            x: 3,
	            y: 16,
	            formatter: function() {
	               return Highcharts.numberFormat(this.value, 0, ',', '.');
	            }
	         }
		},
		legend: {
	        enabled: false
	    },

		plotOptions: {
           series: {
                connectNulls: false
            },
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
	   
	};    
	
	
$(document).ready(function(){	

	//Tabela com cabe�alho fixo e rolagem do dados		
	$("#historicoMedicao,#historicoConsumo").chromatable({
		width: "100%"
	});
	
	//Gr�ficos do Highcharts	
	// Create a graph showing the outcome
	Highcharts.visualize($('#historicoConsumo'), defaultOptions);
	
	// Create a graph showing the income
	options = Highcharts.merge(defaultOptions, options);


});

animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,persist=1,hide=0');

function exibirHistoricoMedicaoConsumoPonto(idPontoConsumo) {
	document.forms[0].idPontoConsumo.value = idPontoConsumo;
	submeter("historicoLeituraMedicaoForm", "exibirHistoricoMedicaoConsumoPonto");
}

function exibirDetalhamentoHistoricoLeituraPontoConsumo(idPontoConsumo, anoMesLeitura, numeroCiclo){
	var popupHistoricoLeitura;
	document.forms[0].idPontoConsumo.value = idPontoConsumo;
	document.forms[0].anoMesLeitura.value = anoMesLeitura;
	document.forms[0].numeroCiclo.value = numeroCiclo;
	var popupHistoricoLeitura = window.open('about:blank','popupHistoricoLeitura','height=600,width=985,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,modal=1');
	if (window.focus) {popupHistoricoLeitura.focus()}
	exibirIndicador();
	submeter("historicoLeituraMedicaoForm", "exibirDetalhamentoHistoricoLeituraPontoConsumo", "popupHistoricoLeitura");
}

function exibirDetalhamentoHistoricoConsumoPontoConsumo(idPontoConsumo, anoMesFaturamento, numeroCiclo, chavePrimariaHistoricoConsumo){
	document.forms[0].idPontoConsumo.value = idPontoConsumo;
	document.forms[0].anoMesFaturamento.value = anoMesFaturamento;
	document.forms[0].numeroCiclo.value = numeroCiclo;	
	document.forms[0].chavePrimariaHistoricoConsumo.value = chavePrimariaHistoricoConsumo;
	var popupHistoricoConsumo = window.open('about:blank','popupHistoricoConsumo','height=600,width=990,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,modal=1');
	if (window.focus) {popupHistoricoConsumo.focus()}
	exibirIndicador();
	submeter("historicoLeituraMedicaoForm", "exibirDetalhamentoHistoricoConsumoPontoConsumo","popupHistoricoConsumo");
}

function voltar() {
	submeter("historicoLeituraMedicaoForm", "pesquisaImovelConsultaHistoricoLeituraConsumo");
}

function exibirHistorico(idImovel) {
	document.forms[0].idImovel.value = idImovel;
	submeter("historicoLeituraMedicaoForm", "exibirHistoricoLeituraConsumoImovel");
}

</script>


<h1 class="tituloInterno">Hist�rico de Leitura e Consumo<a class="linkHelp" href="<help:help>/histricodeleituraeconsumo1.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>


<form method="post" action="exibirDetalhamentoHistoricoLeituraPontoConsumo" id="historicoLeituraMedicaoForm" name="historicoLeituraMedicaoForm">
	<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}"/>
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value=""/>
	<input name="anoMesLeitura" type="hidden" id="anoMesLeitura" value=""/>
	<input name="anoMesFaturamento" type="hidden" id="anoMesFaturamento" value=""/>
	<input name="numeroCiclo" type="hidden" id="numeroCiclo" value=""/>
	<input name="chavePrimariaHistoricoConsumo" type="hidden" id="chavePrimariaHistoricoConsumo" value=""/>
	
	<input name="idCliente" type="hidden" id="idCliente" value="${clientePopupVO.idCliente}"/>
	<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${clientePopupVO.nomeCliente}"/>
	<input name="documentoFormatado" type="hidden" id="documentoFormatado" value="${clientePopupVO.documentoCliente}"/>
	<input name="emailCliente" type="hidden" id="emailCliente" value="${clientePopupVO.emailCliente}"/>
	<input name="enderecoFormatadoCliente" type="hidden" id="enderecoFormatadoCliente" value="${clientePopupVO.enderecoCliente}"/>
	
	<input name="nome" type="hidden" id="nome" value="${historicoLeituraConsumoVO.nome}"/>
	<input name="complementoImovel" type="hidden" id="complementoImovel" value="${historicoLeituraConsumoVO.complementoImovel}"/>
	<input name="matricula" type="hidden" id="matricula" value="${historicoLeituraConsumoVO.matricula}"/>
	<input name="numeroImovel" type="hidden" id="numeroImovel" value="${historicoLeituraConsumoVO.numeroImovel}"/>
	<input name="indicadorCondominioAmbos" type="hidden" id="v" value="${historicoLeituraConsumoVO.indicadorCondominioAmbos}"/>
	<input name="habilitado" type="hidden" id="habilitado" value="${historicoLeituraConsumoVO.habilitado}"/>
	<input name="cepImovel" type="hidden" id="cepImovel" value="${historicoLeituraConsumoVO.cepImovel}"/>	
	
	<input name="idGrupoFaturamento" type="hidden" id="idGrupoFaturamento" value="${historicoLeituraConsumoVO.idGrupoFaturamento}"/>
	<input name="idRota" type="hidden" id="idRota" value="${hhistoricoLeituraConsumoVO.idRota}"/>
	

	<fieldset id="historicoLeituraConsumo" class="detalhamento">		
		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo">Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.nome}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.enderecoFormatado}"/></span>
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.cep}"/></span><br />
				<label class="rotulo">Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.descricaoComplemento}"/></span>
				<label class="rotulo rotulo2Linhas" id="rotuloCodigoLegado" for="pontoConsumo">C�digo do Ponto de Consumo:</label>
				<span class="itemDetalhamento itemDetalhamento4Linhas" id="pontoConsumo"><c:out value="${pontoConsumo.chavePrimaria}"/></span><br />
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset id="intervaloReferencia" class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Intervalo de Refer�ncia</legend>
					
			<label class="rotulo rotuloHorizontal" id="rotuloMesInicial" for="mesInicial">M�s inicial:</label>
			<select name="mesInicial" id="mesInicial" class="campoSelect campoHorizontal">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaMesesAno}" var="mesAno">
					<option value="<c:out value="${mesAno.key}"/>" <c:if test="${historicoLeituraConsumoVO.mesInicial == mesAno.key}">selected="selected"</c:if>>
						<c:out value="${mesAno.value}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    
		    <label class="rotulo rotuloHorizontal" id="rotuloAnoInicial" for="anoInicial">Ano inicial:</label>
			<select name="anoInicial" id="anoInicial" class="campoSelect campoHorizontal">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaAnos}" var="ano">
					<option value="<c:out value="${ano}"/>" <c:if test="${historicoLeituraConsumoVO.anoInicial == ano}">selected="selected"</c:if>>
						<c:out value="${ano}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    
		    <label class="rotulo rotuloHorizontal" id="rotuloMesFinal" for="mesFinal">M�s Final:</label>
			<select name="mesFinal" id="mesFinal" class="campoSelect campoHorizontal">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaMesesAno}" var="mesAno">
					<option value="<c:out value="${mesAno.key}"/>" <c:if test="${historicoLeituraConsumoVO.mesFinal == mesAno.key}">selected="selected"</c:if>>
						<c:out value="${mesAno.value}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    
		    <label class="rotulo rotuloHorizontal" id="rotuloAnoFinal" for="anoFinal">Ano Final:</label>
			<select name="anoFinal" id="anoFinal" class="campoSelect campoHorizontal">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaAnos}" var="ano">
					<option value="<c:out value="${ano}"/>" <c:if test="${historicoLeituraConsumoVO.anoFinal == ano}">selected="selected"</c:if>>
						<c:out value="${ano}"/>
					</option>		
			    </c:forEach>	
		    </select>
		    
		    <input tabindex="4" name="Button" class="bottonRightCol2" value="Exibir" type="button" onclick="exibirHistorico(<c:out value='${imovel.chavePrimaria}'/>);">	   

		</fieldset>
			
		<c:if test="${empty listaPontosConsumo == false}">
			<hr class="linhaSeparadora1" />
			<fieldset id="listaPontosConsumo" class="conteinerBloco">		
				<legend class="conteinerBlocoTitulo">Pontos de Consumo</legend>
				<!-- inserir abaixo da legenda a tabela listando os pontos de consumo -->
			<display:table decorator="br.com.ggas.web.medicao.leitura.ConsultarHistoricoLeituraMedicaoDecorator" export="false" class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontosConsumo" sort="list" id="pontoVO" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirHistoricoLeituraConsumoImovel">
		  			
			     	<display:column media="html" title="Matr�cula Im�vel" style="width: auto;">
			     		<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
							<c:out value='${pontoVO.pontoConsumo.imovel.chavePrimaria}'/>
			        	</a>
				    </display:column>

				    <display:column media="html" sortable="false" title="Nome" style="text-align: left; width: auto;">
				    	<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
							<c:out value='${pontoVO.pontoConsumo.descricao}'/>
			        	</a>
				    </display:column>	
		     
				    <display:column media="html" title="Situa��o" style="width: auto;">
				    	<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
				    		<c:out value='${pontoVO.pontoConsumo.situacaoConsumo.descricao}'/>
				    	</a>
				    </display:column>

				    <display:column media="html" sortable="false" title="Medidor">
				    	<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
				    		<c:out value='${pontoVO.pontoConsumo.instalacaoMedidor.medidor.numeroSerie}'/>
				    	</a>
				    </display:column> 

				    <display:column media="html" title="Refer�ncia / Ciclo" style="width: auto;">
				    	<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
				    		<c:out value='${pontoVO.referencia}'/>-<c:out value='${pontoVO.ciclo}'/>
				    	</a>
				    </display:column>
	
				    <display:column media="html" title="Grupo de faturamento" style="width: auto;">
				    	<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
					    	<c:out value='${pontoVO.pontoConsumo.rota.grupoFaturamento.descricao}'/>
					    </a>
				    </display:column> 
	
				    <display:column media="html" title="Rota">
				    	<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
					    	<c:out value='${pontoVO.pontoConsumo.rota.numeroRota}'/>
					    </a>
				    </display:column>
				    
				    <display:column media="html" title="Seq.<br/>leitura">
				    	<a href='javascript:exibirHistoricoMedicaoConsumoPonto(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'>
					    	<c:out value='${pontoVO.pontoConsumo.numeroSequenciaLeitura}'/>
					    </a>
				    </display:column>
				    
				    <display:column media="html" title="Leiturista">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
							<span alt="<c:out value='${pontoVO.pontoConsumo.rota.leiturista.funcionario.nomeFoneFormatado}'/>"  title="<c:out value='${pontoVO.pontoConsumo.rota.leiturista.funcionario.nomeFoneFormatado}'/>">
								<c:out value='${pontoVO.pontoConsumo.rota.leiturista.funcionario.matricula}'/>
							</span>
						</a>
				    </display:column>
		    
				</display:table>		
			</fieldset>	
		</c:if>	
		
		<c:if test="${empty listaHistoricoMedicao == false || empty pontoConsumo == false}">
			<hr class="linhaSeparadoraDetalhamento" />
			<a name="ancoraHistoricoMedicao"></a>
			<fieldset class="conteinerBloco">				
				<legend class="conteinerBlocoTitulo">Hist�rico de Medi��o</legend>
				<!-- inserir tabela listando o Hist�rico de Leitura -->
				<display:table id="historicoMedicao" class="dataTableGGAS dataTableCabecalho2Linhas" export="false" name="listaHistoricoMedicao" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirHistoricoLeituraConsumoImovel">
					
					<display:column media="html" title="M�s / Ano-Ciclo" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><c:out value='${historicoMedicao.anoMesLeituraFormatado}'/>-<c:out value='${historicoMedicao.numeroCiclo}'/></a>
							</c:when>
							<c:otherwise>
								<c:out value='${historicoMedicao.anoMesLeituraFormatado}'/>-<c:out value='${historicoMedicao.numeroCiclo}'/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column media="html" title="Data / Hora Leitura" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><fmt:formatDate value="${historicoMedicao.dataLeituraInformada}" pattern="dd/MM/yyyy: HH:mm"/></a> 
							</c:when>
							<c:otherwise>
								<fmt:formatDate value="${historicoMedicao.dataLeituraInformada}" pattern="dd/MM/yyyy: HH:mm"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column media="html" title="Leitura">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
							 	<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoMedicao.numeroLeituraInformada}" type="number"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoMedicao.numeroLeituraInformada}" type="number"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column sortable="false"  title="Anormalidade<br />Informada" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><span title="${historicoMedicao.anormalidadeLeituraInformada.descricao}">${historicoMedicao.anormalidadeLeituraInformada.chavePrimaria}</span></a>
							</c:when>
							<c:otherwise>
								<a title="Detalhar Hist�rico Leitura"><span class="linkInvisivel"></span><span title="${historicoMedicao.anormalidadeLeituraInformada.descricao}">${historicoMedicao.anormalidadeLeituraInformada.chavePrimaria}</span></a>
							</c:otherwise>
						</c:choose>
					</display:column>		    
					
					<display:column media="html" title="Data<br />Considerada" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><fmt:formatDate value="${historicoMedicao.dataLeituraFaturada}" pattern="dd/MM/yyyy"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatDate value="${historicoMedicao.dataLeituraFaturada}" pattern="dd/MM/yyyy"/>
							</c:otherwise>
						</c:choose>
					</display:column>			
					
					<display:column media="html" title="Leitura<br />Considerada">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoMedicao.numeroLeituraFaturada}" type="number"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoMedicao.numeroLeituraFaturada}" type="number"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column sortable="false"  title="Anormalidade<br />Considerada" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><span alt="${historicoMedicao.anormalidadeLeituraFaturada.descricao}" title="${historicoMedicao.anormalidadeLeituraFaturada.descricao}">${historicoMedicao.anormalidadeLeituraFaturada.chavePrimaria}</span></a>
							</c:when>
							<c:otherwise>
								<a title="Detalhar Hist�rico Leitura"><span class="linkInvisivel"></span><span alt="${historicoMedicao.anormalidadeLeituraFaturada.descricao}" title="${historicoMedicao.anormalidadeLeituraFaturada.descricao}">${historicoMedicao.anormalidadeLeituraFaturada.chavePrimaria}</span></a>
							</c:otherwise>
						</c:choose>
					</display:column>	
					
					<display:column media="html" title="Leiturista">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><span alt="<c:out value='${historicoMedicao.leiturista.funcionario.nomeFoneFormatado}'/>"  title="<c:out value='${historicoMedicao.leiturista.funcionario.nomeFoneFormatado}'/>"><c:out value='${historicoMedicao.leiturista.funcionario.matricula}'/></span></a>		
							</c:when>
							<c:otherwise>
								<c:out value='${historicoMedicao.leiturista.funcionario.matricula}'/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column sortable="false" title="Medidor" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><span alt="Data instala��o: <fmt:formatDate value="${historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoMedicao.historicoInstalacaoMedidor.medidor.digito}" title="Data instala��o: <fmt:formatDate value="${historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoMedicao.historicoInstalacaoMedidor.medidor.digito}">${historicoMedicao.historicoInstalacaoMedidor.medidor.numeroSerie}</span></a>
							</c:when>
							<c:otherwise>
								<c:out value='${historicoMedicao.historicoInstalacaoMedidor.medidor.numeroSerie}'/>
							</c:otherwise>
						</c:choose>
					</display:column>		    
					
					<display:column media="html" title="Situa��o Atual">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoMedicao.anoMesLeitura},${historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><c:out value='${historicoMedicao.situacaoLeitura.descricao}'/></a>					
							</c:when>
							<c:otherwise>
								<c:out value='${historicoMedicao.situacaoLeitura.descricao}'/>
							</c:otherwise>
						</c:choose>
					</display:column>		    
					
				</display:table>
			</fieldset>
		</c:if>	
		
		<c:if test="${empty listaHistoricoConsumo == false || empty pontoConsumo == false}">
			<hr class="linhaSeparadoraDetalhamento" />
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Hist�rico de Consumo</legend>
				<!-- inserir tabela listando o Hist�rico de Medi��o -->
				<display:table id="historicoConsumo" class="dataTableGGAS dataTableMenor2 dataTableCabecalho2Linhas" export="false" name="listaHistoricoConsumo" sort="list" pagesize="20" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirHistoricoLeituraConsumoImovel">
					
					<display:column media="html" title="M�s / Ano-Ciclo" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><c:out value='${historicoConsumo.anoMesFaturamentoFormatado}'/>-<c:out value='${historicoConsumo.numeroCiclo}'/></a>
							</c:when>
							<c:otherwise>
								<c:out value='${historicoConsumo.anoMesFaturamentoFormatado}'/>-<c:out value='${historicoConsumo.numeroCiclo}'/>
							</c:otherwise>
						</c:choose>
					</display:column>
				    
					<display:column media="html" title="Consumo (m<span class='expoente'>3</span>)">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.consumo}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.consumo}" type="number" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column media="html" title="Consumo Virtual<br /> M�dia (m<span class='expoente'>3</span>)">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.consumoCreditoMedia}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>	
								<c:if test="${historicoConsumo.consumoCreditoMedia ne null && historicoConsumo.consumoCreditoMedia ne 0}">															
									<fmt:formatNumber value="${historicoConsumo.consumoCreditoMedia}" type="number" maxFractionDigits="4"/>
								</c:if>									
							</c:otherwise>
						</c:choose>
					</display:column>
							    
					
					<display:column media="html" title="Consumo Medido (m<span class='expoente'>3</span>)">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.consumoMedido}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.consumoMedido}" type="number" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>	
					
					<display:column sortable="false" title="Fator PCS" >
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.fatorPCS}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.fatorPCS}" type="number" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>	    
					
					<display:column media="html" title="Consumo Apurado (m<span class='expoente'>3</span>)">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.consumoApurado}" type="number" minFractionDigits="4" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.consumoApurado}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>		    

					<display:column media="html" title="Consumo<br /> Di�rio (m<span class='expoente'>3</span>)" >
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.consumoDiario}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.consumoDiario}" type="number" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column media="html" title="Cr�dito Consumo (m<span class='expoente'>3</span>)">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.consumoCredito}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.consumoCredito}" type="number" maxFractionDigits="4"/>																								
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column sortable="false" title="Fator PTZ" >
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.fatorPTZ}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.fatorPTZ}" type="number" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>
										
					<display:column sortable="false" title="Fator Corre��o" >
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.fatorCorrecao}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.fatorCorrecao}" type="number" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column media="html" title="M�dia Refer�ncial">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.consumoApuradoMedio}" type="number" maxFractionDigits="4"/></a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.consumoApuradoMedio}" type="number" maxFractionDigits="4"/>
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column media="html" title="Anormalidade" >
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><span alt="${historicoConsumo.anormalidadeConsumo.descricao}" title="${historicoConsumo.anormalidadeConsumo.descricao}">${historicoConsumo.anormalidadeConsumo.chavePrimaria}</span></a>							</c:when>
							<c:otherwise>
								<a title="Detalhar Hist�rico Consumo"><span class="linkInvisivel"></span><span alt="${historicoConsumo.anormalidadeConsumo.descricao}" title="${historicoConsumo.anormalidadeConsumo.descricao}">${historicoConsumo.anormalidadeConsumo.chavePrimaria}</span></a>
							</c:otherwise>
						</c:choose>
					</display:column>	    
					
					<display:column media="html" title="Dias de Consumo">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><c:out value='${historicoConsumo.diasConsumo}'/></a>
							</c:when>
							<c:otherwise>
								<c:out value='${historicoConsumo.diasConsumo}'/>
							</c:otherwise>
						</c:choose>
					</display:column>		    
					
					<display:column media="html" title="Tipo de Consumo">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><c:out value='${historicoConsumo.tipoConsumo.descricao}'/></a>
							</c:when>
							<c:otherwise>
								<c:out value='${historicoConsumo.tipoConsumo.descricao}'/>
							</c:otherwise>
						</c:choose>
					</display:column>		    
					
					<display:column media="html" title="Varia��o Consumo" style="width: auto;">
						<c:choose>
							<c:when test="${historicoConsumo.pontoConsumo.rota.tipoLeitura.chavePrimaria eq 1}">
								<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo},${historicoConsumo.chavePrimaria})"><span class="linkInvisivel"></span><fmt:formatNumber value="${historicoConsumo.variacaoConsumo}" maxFractionDigits="2"/>%</a>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${historicoConsumo.variacaoConsumo}" maxFractionDigits="2"/>%
							</c:otherwise>
						</c:choose>
					</display:column>		    
				</display:table>
			</fieldset>
		</c:if>
		
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset id="conteinerGraficoConsumo" class="conteinerBloco" style="width: 100%; height: 400px"></fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
	 </fieldset>
	
</form>
