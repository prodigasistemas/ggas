<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Analisar Exce��es de Leituras e Consumos<a class="linkHelp" href="<help:help>/analisandoexceesdeleituraseconsumos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.</p>

<script>
	
	function pesquisar(){
		submeter("excecaoLeituraConsumoForm", "pesquisarExcecaoLeituraConsumo");
	}

	function gerarRelatorio(){
	
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_GERAR_RELATORIO"/>');
			if(retorno == true) {
				submeter("excecaoLeituraConsumoForm", "gerarRelatorioAnaliseExcecaoConsumo");
			}
	    }
	}
	
	function carregarRotas(elem) {	
		var idGrupoFaturamento = elem.value;
	  	var selectRotas = document.getElementById("idsRota");

	  	selectRotas.length=0;
  	      		
	   	AjaxService.listarRotaPorGrupoFaturamento( idGrupoFaturamento, 
	       	function(rotas) {            		      		         		
	           	for (key in rotas){
	             var novaOpcao = new Option(rotas[key], key);
	         	selectRotas.options[selectRotas.length] = novaOpcao;
	        	}
	        	ordernarSelect(selectRotas);
	    	}
	    );
	}
	
function carregarSetoresComerciais(elem) {
		
		var idLocalidade = elem.value;
		if(idLocalidade=="-1") {
			enableAndDisableSetorComercial(true);
			return true;
		} else {
			enableAndDisableSetorComercial(false);
		}
		var selectsetoresComerciais = document.getElementById("idSetorComercial");

		selectsetoresComerciais.length = 0;

		AjaxService.listarSetoresComerciaisPorLocalidade(idLocalidade, function(
				setores) {
			for (key in setores) {
				var novaOpcao = new Option(setores[key], key);
				selectsetoresComerciais.options[selectsetoresComerciais.length] = novaOpcao;
			}
			var novaOpcao = new Option("Selecione", "-1");
			selectsetoresComerciais.options[selectsetoresComerciais.length] = novaOpcao;
			selectsetoresComerciais.value="-1";
			ordernarSelect(selectsetoresComerciais);
		});

	}
	
	function enableAndDisableSetorComercial(value) {
		document.getElementById('idSetorComercial').disabled = value;
	}
	
	function detalhar(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("excecaoLeituraConsumoForm", "exibirDetalhamentoExcecaoLeituraConsumoAnalise");
	}
	
	function limparFormulario(ele){
		limparFormularios(ele);
		limparFormularioDadosCliente();
		document.getElementById("condominioSim").checked = false;
		document.getElementsByName("indicadorCondominio")[2].checked = true;
		document.getElementById('idSetorComercial').disabled = true;
		var anormalidadesLeitura = document.getElementById('idsAnormalidadeLeitura');		
		for (i=0; i<anormalidadesLeitura.length; i++){
			anormalidadesLeitura.options[i].selected = false;
		}
		var anormalidadesConsumo = document.getElementById('idsAnormalidadeConsumo');		
		for (i=0; i<anormalidadesConsumo.length; i++){
			anormalidadesConsumo.options[i].selected = false;
		}
		document.getElementsByName('analisada')[1].checked = true;
		document.getElementsByName('comOcorrencia')[0].checked = true;
		document.getElementsByName("indicadorImpedeFaturamento")[2].checked = true;
		$("#idsRota").empty();
	}

	function init() {
		$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});		
		var mensagensEstado = $(".mensagens").css("display");
		if(mensagensEstado == "none"){
			<c:if test="${listaHistoricoConsumo ne null}">
				$.scrollTo($("#historicoConsumo"),650);
			</c:if>			
		}

		$("#anoMesLeitura").inputmask("9999/99",{placeholder:"_"});
		
		if(document.getElementById('idLocalidade').value=="-1") {
			enableAndDisableSetorComercial(true);
		}
	}
	
	
	addLoadEvent(init);
	animatedcollapse.addDiv('pesquisaAvancadaPesquisaExcecaoLeituraConsumo', 'fade=0,speed=400,persist=1,hide=1');
</script>

<form:form method="post" name="excecaoLeituraConsumoForm" action="pesquisarExcecaoLeituraConsumo">
	<input name="acao" type="hidden" id="acao" value="pesquisarExcecaoLeituraConsumo"/>
	<input name="chavePrimarias" type="hidden" id="chavePrimarias">
	<input name="idGrupoFaturamentoRetorno" type="hidden" id="idGrupoFaturamentoRetorno" value="${analiseExcecaoLeituraVO.idGrupoFaturamento}">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarExcecaoLeituraConsumo" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaGeral" class="conteinerBloco">
			<fieldset id="pesquisaGeralCol1" class="colunaEsq">
				<label class="rotulo" id="rotuloLocalidade" for="idLocalidade">Localidade:</label>
				<select name="idLocalidade" id="idLocalidade" class="campoSelect campo2Linhas" 
				onchange="carregarSetoresComerciais(this);">
			    	<option value="-1">Selecione</option>
					<c:forEach items="${listaLocalidade}" var="localidade">
						<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${analiseExcecaoLeituraVO.idLocalidade == localidade.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${localidade.descricao}"/>
						</option>		
				    </c:forEach>	
			    </select><br /> 
				    
			    <label class="rotulo rotulo2Linhas" id="rotuloSetorComercial" for="idSetorComercial">Setor Comercial:</label>
				<select name="idSetorComercial" id="idSetorComercial" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSetorComercial}" var="setorComercial">
						<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${analiseExcecaoLeituraVO.idSetorComercial == setorComercial.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${setorComercial.descricao}"/>
						</option>		
					</c:forEach>	
				</select><br />
				
				<label class="rotulo rotulo2Linhas ajusteCampo" id="rotuloGrupoFaturamento" for="idGrupoFaturamento">Grupo de Faturamento:</label>
				<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="campoSelect campo2Linhas" onchange="carregarRotas(this);">
			    	<option value="-1">Selecione</option>
					<c:forEach items="${listaGrupoFaturamento}" var="grupoFaturamento">
						<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${analiseExcecaoLeituraVO.idGrupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${grupoFaturamento.descricao}"/>
						</option>
				    </c:forEach>
			    </select><br />
			    
			    <label class="rotulo campo2Linhas" id="rotuloRota" for="idsRota">Rota:</label>
				<select name="idsRota" id="idsRota" class="campoList campo2Linhas" multiple="multiple">
					<c:forEach items="${listaRota}" var="rota">
						<c:set var="selecionado" value="false"/>
						<c:forEach items="${analiseExcecaoLeituraVO.idsRota}" var="idRota">
							<c:if test="${idRota eq rota.chavePrimaria}">
								<c:set var="selecionado" value="true" />
							</c:if>
						</c:forEach>
						<option value="<c:out value="${rota.chavePrimaria}"/>"<c:if test="${selecionado eq true}"> selected="selected"</c:if> title="<c:out value="${rota.numeroRota}"/>">
							<c:out value="${rota.numeroRota}"/>
						</option>		
					</c:forEach>
				</select><br />
				    
			    <label class="rotulo" id="rotuloLocalidade" for="idSegmento">Segmento:</label>
			    <select name="idSegmento" id="idSegmento" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSegmento}" var="segmento">
						<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${analiseExcecaoLeituraVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${segmento.descricao}"/>
						</option>		
					</c:forEach>
				</select><br />
				
				<label class="rotulo rotulo2Linhas" id="rotuloSituacaoPontoConsumo" for="idsSituacaoPontoConsumo">Situa��o do Ponto de Consumo:</label>
			    <select name="idSituacaoPontoConsumo" id="idSituacaoPontoConsumo" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSituacaoPontoConsumo}" var="situacao">
						<option value="<c:out value="${situacao.chavePrimaria}"/>" <c:if test="${analiseExcecaoLeituraVO.idSituacaoPontoConsumo == situacao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${situacao.descricao}"/>
						</option>		
					</c:forEach>
				</select><br />
				
				<label class="rotulo rotulo2Linhas" id="rotuloAnormalidadeLeitura" for="idsAnormalidadeLeitura">Anormalidades de Leitura:</label>
				    <select name="idsAnormalidadeLeitura" id="idsAnormalidadeLeitura" class="campoList" multiple="multiple">				
						<c:forEach items="${listaAnormalidadeLeitura}" var="anormalidadeLeitura">
							<c:set var="selecionado" value="false"/>
							<c:forEach items="${analiseExcecaoLeituraVO.idsAnormalidadeLeitura}" var="idAnormalidade">
								<c:if test="${idAnormalidade eq anormalidadeLeitura.chavePrimaria}">
									<c:set var="selecionado" value="true" />
								</c:if>
							</c:forEach>
							<option value="<c:out value="${anormalidadeLeitura.chavePrimaria}"/>"<c:if test="${selecionado eq true}"> selected="selected"</c:if> title="<c:out value="${anormalidadeLeitura.descricao}"/>">
								<c:out value="${anormalidadeLeitura.descricao}"/>
							</option>		
						</c:forEach>
					</select>
				
			</fieldset>
				
			<fieldset id="pesquisaGeralCol2">
                  
					<label class="rotulo" id="rotuloNumeroOcorrenciaAnormalidadeConsumo" for="numeroOcorrenciaAnormalidadeConsumo">N�mero de <br> Ocorr�ncias:</label>
					<input class="campoTexto" type="text" id="numeroOcorrenciaAnormalidadeConsumo" name="numeroOcorrenciaAnormalidadeConsumo" maxlength="1" 
						value="<c:out value="${analiseExcecaoLeituraVO.numeroOcorrenciaAnormalidadeConsumo}"/>" onkeypress="return formatarCampoInteiro(event);" />
					
					<label class="rotulo" id="rotuloAnormalidadeConsumo" for="idsAnormalidadeConsumo">Anormalidades de Consumo:</label>
				    <select name="idsAnormalidadeConsumo" id="idsAnormalidadeConsumo" class="campoList" multiple="multiple">				
						<c:forEach items="${listaAnormalidadeConsumo}" var="anormalidadeConsumo">
							<c:set var="selecionado" value="false"/>
							<c:forEach items="${analiseExcecaoLeituraVO.idsAnormalidadeConsumo}" var="idAnormalidade">
								<c:if test="${idAnormalidade eq anormalidadeConsumo.chavePrimaria}">
									<c:set var="selecionado" value="true" />
								</c:if>
							</c:forEach>
							<option value="<c:out value="${anormalidadeConsumo.chavePrimaria}"/>"<c:if test="${selecionado eq true}"> selected="selected"</c:if> title="<c:out value="${anormalidadeConsumo.descricao}"/>">
								<c:out value="${anormalidadeConsumo.descricao}"/>
							</option>		
						</c:forEach>
					</select><br />
					<label class="rotulo" id="rotuloTipoConsumo" for="tipoConsumo">Tipo de Consumo:</label>
					<select name="idTipoConsumo" id="idTipoConsumo" class="campoSelect campo2Linhas">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTiposConsumo}" var="tipoConsumo">
							<option value="<c:out value="${tipoConsumo.chavePrimaria}"/>" <c:if test="${analiseExcecaoLeituraVO.idTipoConsumo == tipoConsumo.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipoConsumo.descricao}"/>
							</option>		
						</c:forEach>	
					</select><br />
				
					<label class="rotulo" id="rotuloConsumoLido" for="consumoLido">Consumo Lido:</label>
					<input class="campoTexto campo2Linhas" type="text" id="consumoLido" name="consumoLido" onkeypress="return formatarCampoDecimal(event,this,9,4);" value="<c:out value="${analiseExcecaoLeituraVO.consumoLido}"/>">
					
					<label class="rotulo rotulo2Linhas" id="rotuloVariacaoConsumoRelacaoMedia" for="percentualVariacaoConsumo">Varia��o do Consumo <br />em Rela��o � M�dia:</label>
					<input class="campoTexto" type="text" id="percentualVariacaoConsumo" name="percentualVariacaoConsumo" onkeypress="return formatarCampoDecimal(event,this,3,2);" value="<c:out value="${analiseExcecaoLeituraVO.percentualVariacaoConsumo}"/>">
					
					<fieldset class="conteinerBloco">
						<label class="rotulo" for="analisadaTodas">Leitura Analisada:</label>
						<input class="campoRadio" type="radio" name="analisada" id="analisadaSim" value="true" <c:if test="${analiseExcecaoLeituraVO.analisada eq 'true'}">checked</c:if>>
						<label class="rotuloRadio" for="analisadaSim">Sim</label>
						<input class="campoRadio" type="radio" name="analisada" id="analisadaNao" value="false" <c:if test="${analiseExcecaoLeituraVO.analisada  eq 'false'}">checked</c:if>>
						<label class="rotuloRadio" for="analisadaNao">N�o</label>
						<input class="campoRadio" type="radio" name="analisada" id="analisadaTodas" value="" <c:if test="${empty analiseExcecaoLeituraVO.analisada}">checked</c:if>>
						<label class="rotuloRadio" for="analisadaTodas">Todas</label>
						<br class="quebraLinha2" />
						<label class="rotulo" for="comOcorrenciaTodas">Com Ocorr�ncia:</label>
						<input class="campoRadio" type="radio" name="comOcorrencia" id="comOcorrenciaSim" value="true" <c:if test="${analiseExcecaoLeituraVO.comOcorrencia eq 'true'}">checked</c:if>>
						<label class="rotuloRadio" for="comOcorrenciaSim">Sim</label>
						<input class="campoRadio" type="radio" name="comOcorrencia" id="comOcorrenciaNao" value="false" <c:if test="${analiseExcecaoLeituraVO.comOcorrencia  eq 'false'}">checked</c:if>>
						<label class="rotuloRadio" for="comOcorrenciaNao">N�o</label>
						<input class="campoRadio" type="radio" name="comOcorrencia" id="comOcorrenciaTodas" value="" <c:if test="${empty analiseExcecaoLeituraVO.comOcorrencia}">checked</c:if>>
						<label class="rotuloRadio" for="comOcorrenciaTodas">Todas</label>
						
						<label class="rotulo">Anormalidade <br> impede faturamento:</label>
						<input class="campoRadio" type="radio" name="indicadorImpedeFaturamento" id="indicadorImpedeFaturamentoSim" value="true" <c:if test="${analiseExcecaoLeituraVO.indicadorImpedeFaturamento eq 'true'}">checked</c:if>>
						<label class="rotuloRadio">Sim</label>
						<input class="campoRadio" type="radio" name="indicadorImpedeFaturamento" id="indicadorImpedeFaturamentoNao" value="false" <c:if test="${analiseExcecaoLeituraVO.indicadorImpedeFaturamento  eq 'false'}">checked</c:if>>
						<label class="rotuloRadio">N�o</label>
						<input class="campoRadio" type="radio" name="indicadorImpedeFaturamento" id="indicadorImpedeFaturamentoTodas" value="" <c:if test="${empty analiseExcecaoLeituraVO.indicadorImpedeFaturamento}">checked</c:if>>
						<label class="rotuloRadio">Todas</label>
						
						<label class="rotulo rotulo2Linhas" >Per�odo da Leitura:<span>* </span></label>
						<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataLeituraInicio" name="dataLeituraInicio" maxlength="10" value="${analiseExcecaoLeituraVO.dataLeituraInicio}">
						<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
						<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataLeituraFim" name="dataLeituraFim" maxlength="10" value="${analiseExcecaoLeituraVO.dataLeituraFim}">
			                
			                     
             		<div class="col-md-6">
             			<label class="rotulo rotulo2Linhas" for="anoMesLeitura">Ano/M�s de Referncia:<span>* </span></label>
                         <input class="campoTexto campoHorizontal" type="text" id="anoMesLeitura" name="anoMesLeitura" 
                         value="<c:out value="${analiseExcecaoLeituraVO.anoMesLeitura}"/>" >
             			</div>
             			 <br />
             			 
             	<label class="rotulo rotulo2Linhas" id="rotuloCiclo" for="ciclo">Ciclo:<span>* </span></label>   					
				<select name="numeroCiclo" id="numeroCiclo" class="campoSelect campo2Linhas campoHorizontal">
					<option value="-1">Selecione</option>
						<c:forEach items="${listaCiclo}" var="ciclo">
							<option value="<c:out value="${ciclo}"/>"
								<c:if test="${analiseExcecaoLeituraVO.numeroCiclo == ciclo}">selected="selected"</c:if>>
								<c:out value="${ciclo}" />
							</option>
						</c:forEach>
				</select>

						
						
					</fieldset>
				</fieldset>
			</fieldset>
		</fieldset>

		
		<a class="linkExibirDetalhes" href="#" rel="toggle[pesquisaAvancadaPesquisaExcecaoLeituraConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pesquisa Avan�ada <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="pesquisaAvancadaPesquisaExcecaoLeituraConsumo" class="conteinerBloco">
			<fieldset id="pesquisarCliente" class="colunaEsq">
				<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
					<jsp:param name="idCampoIdCliente" value="idCliente"/>
					<jsp:param name="idCliente" value="${cliente.chavePrimaria}"/>
					<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
					<jsp:param name="nomeCliente" value="${cliente.nome}"/>
					<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
					<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
					<jsp:param name="idCampoEmail" value="emailCliente"/>
					<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
					<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
					<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>				
				</jsp:include>		
			</fieldset>
			
			<fieldset id="pesquisarImovel" class="colunaDir2">
				<legend>Pesquisar Im�vel</legend>
				<div class="pesquisarImovelFundo">			
					<label class="rotulo" id="rotuloNomeImovel" for="nomeImovel">Descri��o:</label>
					<input class="campoTexto" type="text" id="nomeImovel" name="nomeImovel" maxlength="50" size="37" value="${analiseExcecaoLeituraVO.nomeImovel}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">		
					<label class="rotulo" id="rotuloComplemento" for="complementoImovel">Complemento:</label>
					<input class="campoTexto" type="text" id="complementoImovel" name="complementoImovel" maxlength="25" size="30" value="${analiseExcecaoLeituraVO.complementoImovel}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"><br />
					<label class="rotulo" id="rotuloMatricula" for="matriculaImovel">Matr�cula:</label>
					<input class="campoTexto" type="text" id="matriculaImovel" name="matriculaImovel" maxlength="9" size="9" value="${analiseExcecaoLeituraVO.matriculaImovel}" onkeypress="return formatarCampoInteiro(event)"><br />
					<label class="rotulo" id="rotuloMatricula" for="numeroImovel">N�mero do im�vel:</label>
					<input class="campoTexto campo2Linhas" type="text" id="numeroImovel" name="numeroImovel" maxlength="9" size="9" value="${analiseExcecaoLeituraVO.numeroImovel}" onkeypress="return formatarCampoNumeroEndereco(event, this);"><br />
					<label class="rotulo rotulo2Linhas" id="rotuloPontoConsumoLegado" for="pontoConsumoLegado">C&oacute;digo do Ponto <br /> de Consumo:</label>
					<input class="campoTexto campo2Linhas" type="text" id="pontoConsumoLegado" name="pontoConsumoLegado" maxlength="20" size="20" value="${analiseExcecaoLeituraVO.pontoConsumoLegado}" onkeypress="return formatarCampoInteiro(event);"><br />					
					<label class="rotulo" id="rotuloImovelCondominio" for="condominioSim">O im�vel � condom�nio?</label>
					<input class="campoRadio" type="radio" value="true" name="indicadorCondominio" id="condominioSim" <c:if test="${analiseExcecaoLeituraVO.indicadorCondominio eq 'true'}">checked</c:if> ><label class="rotuloRadio">Sim</label>
					<input class="campoRadio" type="radio" value="false" name="indicadorCondominio" id="condominioNao" <c:if test="${analiseExcecaoLeituraVO.indicadorCondominio eq 'false'}">checked</c:if> ><label class="rotuloRadio">N�o</label>
					<input class="campoRadio" type="radio" value="" name="indicadorCondominio" id="condominioAmbos" <c:if test="${analiseExcecaoLeituraVO.indicadorCondominio ne 'true' && analiseExcecaoLeituraVO.indicadorCondominio ne 'false'}">checked</c:if>><label class="rotuloRadio">Todos</label><br /><br />
					<fieldset class="exibirCep">
						<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
							<jsp:param name="cepObrigatorio" value="false"/>
							<jsp:param name="idCampoCep" value="cepImovel"/>
							<jsp:param name="numeroCep" value="${analiseExcecaoLeituraVO.cepImovel}"/>
						</jsp:include>		
					</fieldset>
				</div>	
			</fieldset>
		</fieldset>
		
		<fieldset id="conteinerBotoesPesquisaExcecaoLeituraConsumoPesquisar" class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarExcecaoLeituraConsumo">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisar();" />
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario(this.form);">		
		</fieldset>		
	</fieldset>
	

<a name="ancoraPesquisaExcecaoLeituraConsumo"></a>	
<c:if test="${listaHistoricoConsumo ne null}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaHistoricoConsumo" sort="list" id="historicoConsumo" pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarExcecaoLeituraConsumo">
		<display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
      		<input type="checkbox" name="chavesPrimarias" value="${historicoConsumo.pontoConsumo.chavePrimaria}">
		</display:column>     	
		<display:column media="html" title="Ponto de Consumo" style="text-align: left" headerClass="tituloTabelaEsq">
	    	<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	    		<c:choose>
		    		<c:when test="${historicoConsumo.pontoConsumo.imovel.nome ne null}">
		    			<c:out value="${historicoConsumo.pontoConsumo.imovel.nome} - ${historicoConsumo.pontoConsumo.descricao}"/>
		    		</c:when>
		    		<c:otherwise>
		    			<c:out value="${historicoConsumo.pontoConsumo.descricao}"/>
		    		</c:otherwise>
		    	</c:choose>
        	</a>
		</display:column>
		<display:column media="html" title="Situa��o" style="width: 88px">
     		<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
     			<c:out value="${historicoConsumo.pontoConsumo.situacaoConsumo.descricao}"/>
     		</a>
		</display:column>
		<display:column media="html" sortable="false" title="Matr�cula<br />Im�vel" style="width: 74px">
	    	<a title="<c:out value="${historicoConsumo.pontoConsumo.imovel.nome}"/>" href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	    		<c:out value="${historicoConsumo.pontoConsumo.imovel.chavePrimaria}"/>
	    	</a>
		</display:column>	      
		<display:column media="html" title="Segmento" style="width: 77px">
		    <a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    	<c:out value="${historicoConsumo.pontoConsumo.segmento.descricao}"/>
		    </a>
		</display:column>
		<display:column media="html" title="Lido" style="width: 118px">
	    	<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	    		<fmt:formatNumber value="${historicoConsumo.consumo}" maxFractionDigits="4"/>
	    	</a>
		</display:column> 
		<display:column media="html" title="Apurado" style="width: 118px">
	    	<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				 ${historicoConsumo.consumoApurado} 
			</a>
		</display:column>
		<display:column media="html" title="Anorm.<br />Leitura" style="width: 83px">
	    	<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	    		<c:if test="${historicoConsumo.historicoAtual  ne null && historicoConsumo.historicoAtual.anormalidadeLeituraFaturada ne null}">
		    		<c:out value="${historicoConsumo.historicoAtual.anormalidadeLeituraFaturada.descricao}"/>
		    	</c:if>
		    </a>	
		</display:column>
		<display:column media="html" title="Anorm.<br />Consumo" style="width: 83px">
	    	<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    	<c:if test="${historicoConsumo.anormalidadeConsumo ne null}">
		    		<c:out value="${historicoConsumo.anormalidadeConsumo.descricao}"/>
		    	</c:if>
		    </a>
		</display:column>
		<display:column media="html" title="C�digo do Ponto de Consumo" style="width: 83px">
	    	<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    	<c:if test="${historicoConsumo.anormalidadeConsumo ne null}">
		    		<c:out value="${historicoConsumo.pontoConsumo.codigoLegado}"/>
		    	</c:if>
		    </a>
		</display:column>	
		<display:column media="html" title="Analisada" style="width: 55px">
			<a href='javascript:detalhar(<c:out value='${historicoConsumo.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    	<c:choose>
		    		<c:when test="${historicoConsumo.historicoAtual  ne null && historicoConsumo.historicoAtual.analisada == true}">
		    			Sim
		    		</c:when>
		    		<c:otherwise>
		    			N�o
		    		</c:otherwise>
		    	</c:choose>
		    </a>
		</display:column>     
	</display:table>
	
	<fieldset class="conteinerBotoes">
		<vacess:vacess param="gerarRelatorioAnaliseExcecaoConsumo">
			<input name="buttonRemover" value="Relat�rio An�lise de Consumo" class="bottonRightCol2" onclick="gerarRelatorio()" type="button">
		</vacess:vacess>
	</fieldset>
</c:if>

</form:form>
