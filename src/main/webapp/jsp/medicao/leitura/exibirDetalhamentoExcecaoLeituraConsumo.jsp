<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�oo Comercial (Billing) de Servi�o�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa �� um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>

$(document).ready(function(){
	// $("#valorLeituraMedida").priceFormat({limit: true, limit: 11, centsLimit: 4,  prefix: '', thousandsSeparator: '', centsSeparator: ','});


	// Datepicker
	$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

});

function voltar(){
	submeter("excecaoLeituraConsumoForm", "pesquisarExcecaoLeituraConsumo");
}

function exibirAlteracaoDadosLeitura(anoMesLeitura,idHistoricoMedicao, numeroLeituraFaturada, dataLeituraFaturada, descricaoAnormalidadeLeituraFaturada,
			numeroLeituraInformada, dataLeituraInformada, idAnormalidadeLeituraInformada, numeroLeituraAnterior, dataLeituraAnterior, idAnormalidadeLeituraAnterior,
			idLeiturista, consumoInformado, descricaoSituacao, numeroMedidor, creditoVolume, informadoCliente, vazaoCorretor, consumoAnoAnterior, consumoAnoAtual){
	// inputs
	document.forms[0].idHistoricoMedicao.value = idHistoricoMedicao;
	document.forms[0].valorLeituraMedida.value = numeroLeituraInformada;
	document.forms[0].dataRealizacaoLeitura.value = dataLeituraInformada;
	document.forms[0].idAnormalidadeLeituraIdentificada.value = idAnormalidadeLeituraInformada;
	document.forms[0].valorLeituraAnterior.value = numeroLeituraAnterior;
	document.forms[0].dataLeituraAnterior.value = dataLeituraAnterior;
	// document.forms[0].idAnormalidadeLeituraAnterior.value = idAnormalidadeLeituraAnterior;
	document.forms[0].volumeApurado.value = consumoInformado;
	document.forms[0].comentarioAlteracaoLeitura.value = "";
	document.forms[0].salvarAlteracaoLeitura.disabled = false;

	if (vazaoCorretor != undefined && vazaoCorretor != '') {
		document.getElementById('campoObrigatorioLeiturista').style.display = 'none';
	} else {
		document.getElementById('campoObrigatorioLeiturista').style.display = '';
	}

	if (informadoCliente != undefined && informadoCliente == "true") {
		document.forms[0].informadoCliente.checked = true;
		document.forms[0].idLeiturista.value = "-1";
		document.forms[0].idLeiturista.disabled = true;
	} else {
		document.forms[0].informadoCliente.checked = false;
		document.forms[0].idLeiturista.disabled = false;
		document.forms[0].idLeiturista.value = idLeiturista;
	}

	if (creditoVolume != null) {
		document.forms[0].creditoVolume.disabled = "";
		document.forms[0].creditoVolume.value = creditoVolume;
		document.forms[0].alterarHistoricoMedicaoDiario.value = 'false';
	} else {
		document.forms[0].creditoVolume.disabled = "disabled";
		document.forms[0].creditoVolume.value = "";
		document.forms[0].alterarHistoricoMedicaoDiario.value = 'true';
	}

	// spans
	document.getElementById('valorAnoMesReferencia').innerText = anoMesLeitura;
	document.getElementById('valorAnoMesReferencia').textContent = anoMesLeitura;
	document.getElementById('valorConsiderado').innerText = numeroLeituraFaturada;
	document.getElementById('valorConsiderado').textContent = numeroLeituraFaturada;
	document.getElementById('dataConsiderada').innerText = dataLeituraFaturada;
	document.getElementById('dataConsiderada').textContent = dataLeituraFaturada;
	document.getElementById('anormalidadeConsiderada').innerText = descricaoAnormalidadeLeituraFaturada;
	document.getElementById('anormalidadeConsiderada').textContent = descricaoAnormalidadeLeituraFaturada;
	document.getElementById('medidor').innerText = numeroMedidor;
	document.getElementById('medidor').textContent = numeroMedidor;
	document.getElementById('situacao').innerText = descricaoSituacao;

	if(consumoAnoAtual != undefined && consumoAnoAtual != null) {
		document.getElementById('valorConsumoAnoAtual').textContent = consumoAnoAtual;
	}

	if(consumoAnoAnterior != undefined && consumoAnoAnterior != null) {
		document.getElementById('valorConsumoAnoAnterior').textContent = consumoAnoAnterior;
	}

	// hiddens
	document.forms[0].anoMesLeituraCiclo.value = anoMesLeitura;
	document.forms[0].valorLeituraFaturada.value = numeroLeituraFaturada;
	document.forms[0].dataLeituraFaturada.value = dataLeituraFaturada;
	document.forms[0].descricaoAnormalidadeFaturada.value = descricaoAnormalidadeLeituraFaturada;
	document.forms[0].numeroSerieMedidor.value = numeroMedidor;
	document.forms[0].descricaoSituacao.value = descricaoSituacao;
	document.forms[0].modificouLeituraAnterior.value = false;

	//Exibe o formul�rio Alterar Dados de Leitura
	animatedcollapse.show('alterarDadosLeituraBloco');

	//Rolagem da tela para posicionar o formul�rio no alto da tela
	$.scrollTo($('#alterarDadosLeituraBloco'),800);
}

	animatedcollapse.addDiv('alterarDadosLeituraBloco', 'fade=0,speed=400,persist=0,hide=1');

function alterarDadosFaturamento(){
	document.forms[0].idPontoConsumo.value = '${analiseExcecaoLeituraVO.chavePrimaria}';

		if(validarDataLeituraInformada() && confirmarVolumeInformado()){
			if (confirmarExecucaoConsistir()) {
				document.forms[0].executarConsistir.value = true;
			} else {
				document.forms[0].executarConsistir.value = false;
			}

			submeter("excecaoLeituraConsumoForm", "alterarDadosFaturamento");
		}


}

function confirmarExecucaoConsistir(){
	return confirm('Deseja consistir a medi��o? ');
}

function confirmarVolumeInformado(){
		var leituraAnterior = document.forms[0].valorLeituraAnterior.value;
		var leituraAtual 	 = document.forms[0].valorLeituraMedida.value;
		return confirm('Cofirma novo volume informado ? ' + (leituraAtual.replace(".", "") - leituraAnterior.replace(".", "")));
}

function validarDataLeituraInformada(){

	var dataLeituraInformada = trim(document.forms[0].dataRealizacaoLeitura.value);
	var dataLeituraAnterior = trim(document.forms[0].dataLeituraAnterior.value);
	var retorno = true;

	if((dataLeituraInformada != '') && (dataLeituraAnterior != '')){
		if(dataLeituraInformada == dataLeituraAnterior){
			if(confirm('A data da leitura informada � igual a data da leitura anterior. Deseja continuar?')){
				retorno = true;
			}else{
				retorno = false;
			}
		}
	}

	return retorno;
}

function limparDadosLeitura(){
	// inputs
	document.forms[0].idHistoricoMedicao.value = "";
	document.forms[0].valorLeituraMedida.value = "";
	document.forms[0].dataRealizacaoLeitura.value = "";
	document.forms[0].idAnormalidadeLeituraIdentificada.value = "-1";
	document.forms[0].valorLeituraAnterior.value = "";
	document.forms[0].dataLeituraAnterior.value = "";
	//document.forms[0].idAnormalidadeLeituraAnterior.value = "-1";
	document.forms[0].comentarioAlteracaoLeitura.value = "";
	document.forms[0].idLeiturista.value = "-1";
	document.forms[0].idLeiturista.disabled = false;
	document.forms[0].salvarAlteracaoLeitura.disabled = true;
	document.forms[0].volumeApurado.value = "";
	document.forms[0].creditoVolume.value = "";
	document.forms[0].informadoCliente.checked = false;

	// spans
	document.getElementById('valorAnoMesReferencia').innerText = '';
	document.getElementById('valorAnoMesReferencia').textContent = '';
	document.getElementById('valorConsiderado').innerText = '';
	document.getElementById('valorConsiderado').textContent = '';
	document.getElementById('dataConsiderada').innerText = '';
	document.getElementById('dataConsiderada').textContent = '';
	document.getElementById('anormalidadeConsiderada').innerText = '';
	document.getElementById('anormalidadeConsiderada').textContent = '';
	document.getElementById('medidor').innerText = '';
	document.getElementById('medidor').textContent = '';
	document.getElementById('situacao').innerText = '';
	document.getElementById('valorConsumoAnoAnterior').textContent = '';
	// hiddens
	document.forms[0].anoMesLeituraCiclo.value = "";
	document.forms[0].valorLeituraFaturada.value = "";
	document.forms[0].dataLeituraFaturada.value = "";
	document.forms[0].descricaoAnormalidadeFaturada.value = "";
	document.forms[0].numeroSerieMedidor.value = "";
	document.forms[0].descricaoSituacao.value = "";
	document.forms[0].modificouLeituraAnterior.value = true;

}

function exibirAlteracaoDadosConsumo(idHistoricoConsumo, consumo, consumoApurado){
	document.forms[0].idHistoricoConsumo.value = idHistoricoConsumo;
	document.forms[0].consumo.value = consumo;
	document.forms[0].consumoApurado.value = consumoApurado;
	document.forms[0].comentarioAlteracaoConsumo.value = "";
	document.forms[0].salvarAlteracaoConsumo.disabled = false;
}

function limparDadosConsumo(){
	document.forms[0].idHistoricoConsumo.value = "";
	document.forms[0].consumo.value = "";
	document.forms[0].consumoApurado.value = "";
	document.forms[0].comentarioAlteracaoConsumo.value = "";
	document.forms[0].salvarAlteracaoConsumo.disabled = true;
}

	/*function alterarDadosConsumo(){
		document.forms[0].idPontoConsumo.value = '${analiseExcecaoLeituraVO.chavePrimaria}';
		submeter("excecaoLeituraConsumoForm", "alterarDadosConsumo");
	}*/

function exibirMedicaoHistoricoComentarios(idPontoConsumo, anoMesLeitura, numeroCiclo){
	
	exibirIndicador();
	document.forms[0].idPontoConsumo.value = idPontoConsumo;
	document.forms[0].anoMesLeitura.value = anoMesLeitura;
	document.forms[0].numeroCiclo.value = numeroCiclo;
	var popupMedicaoHistoricoComentarios = window.open('about:blank','popupMedicaoHistoricoComentarios','height=600,width=968,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,modal=1');
	if (window.focus) {popupMedicaoHistoricoComentarios.focus()}
	var acao = "exibirMedicaoHistoricoComentarios";
	submeter("excecaoLeituraConsumoForm", acao, "popupMedicaoHistoricoComentarios");
}

function exibirDetalhamentoHistoricoLeituraPontoConsumo(idPontoConsumo, anoMesLeitura, numeroCiclo, permitirAlteracao){
	exibirIndicador();
	var popupHistoricoLeitura;
	document.forms[0].idPontoConsumo.value = idPontoConsumo;
	document.forms[0].anoMesLeitura.value = anoMesLeitura;
	document.forms[0].numeroCiclo.value = numeroCiclo;
	$("#permitirAltercao").val(permitirAlteracao);
	var popupHistoricoLeitura = window.open('about:blank','popupHistoricoLeitura','height=600,width=980,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0 ,modal=1');
	if (window.focus) {popupHistoricoLeitura.focus()}
	var acao = "exibirDetalhamentoHistoricoLeituraAnaliseExcecao?permitirAlteracao="+permitirAlteracao;
	submeter("excecaoLeituraConsumoForm", acao, "popupHistoricoLeitura");
}

function exibirDetalhamentoHistoricoConsumoPontoConsumo(idPontoConsumo, anoMesFaturamento, numeroCiclo){
	exibirIndicador();
	document.forms[0].idPontoConsumo.value = idPontoConsumo;
	document.forms[0].anoMesFaturamento.value = anoMesFaturamento;
	document.forms[0].numeroCiclo.value = numeroCiclo;
	var popupHistoricoConsumo = window.open('about:blank','popupHistoricoConsumo','height=600,width=980,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0 ,modal=1');
	if (window.focus) {popupHistoricoConsumo.focus()}
	submeter("excecaoLeituraConsumoForm", "exibirDetalhamentoHistoricoConsumoPontoConsumo","popupHistoricoConsumo");
}

function detalhar(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("excecaoLeituraConsumoForm", "exibirDetalhamentoExcecaoLeituraConsumoAnalise");
}

function selecionarHistoricoMedicao(idSelecionado){
	limparDadosLeitura();
	if(idSelecionado != '') {
		AjaxService.obterHistoricoMedicaoPorChave( idSelecionado, {
	           	callback: function(historicoMedicao) {
	           		if(historicoMedicao != null){
	           			var anoMesLeitura = historicoMedicao['anoMesLeituraFormatado'];

	           			var numeroLeituraFaturada = '';
	           			if(historicoMedicao['numeroLeituraFaturada'] != undefined){
	           				numeroLeituraFaturada = historicoMedicao['numeroLeituraFaturada'];
	           			}

	           			var descricaoAnormalidadeLeituraFaturada = '';
	           			if(historicoMedicao['descricaoAnormalidadeLeituraFaturada'] != undefined){
	           				descricaoAnormalidadeLeituraFaturada = historicoMedicao['descricaoAnormalidadeLeituraFaturada'];
	           			}
	           			var dataLeituraFaturada = '';
	           			if(historicoMedicao['dataLeituraFaturada'] != undefined){
	           				dataLeituraFaturada = historicoMedicao['dataLeituraFaturada'];
	           			}
	           			var numeroLeituraInformada = historicoMedicao['numeroLeituraInformada'];
	           			var dataLeituraInformada = historicoMedicao['dataLeituraInformada'];
	           			var idAnormalidadeLeituraInformada = historicoMedicao['idAnormalidadeLeituraInformada'];
	           			var numeroLeituraAnterior = '';
	           			if(historicoMedicao['numeroLeituraAnterior'] != undefined){
	           				numeroLeituraAnterior = historicoMedicao['numeroLeituraAnterior'];
	           			}
	           			var dataLeituraAnterior = '';
	           			if(historicoMedicao['dataLeituraAnterior'] != undefined){
	           				dataLeituraAnterior = historicoMedicao['dataLeituraAnterior'];
	           			}
	           			var idAnormalidadeLeituraAnterior = historicoMedicao['idAnormalidadeLeituraAnterior'];
	           			var idLeiturista = historicoMedicao['idLeiturista'];
	           			var consumoInformado = '';
	           			if(historicoMedicao['consumoInformado'] != undefined){
	           				consumoInformado = historicoMedicao['consumoInformado'];
	           			}
	           			var descricaoSituacao = historicoMedicao['descricaoSituacao'];
	           			var numeroMedidor = '';
	           			if(historicoMedicao['numeroMedidor'] != undefined){
	           				numeroMedidor = historicoMedicao['numeroMedidor'];
	           			}

	           			var vazaoCorretor = '';
	           			if(historicoMedicao['vazaoCorretor'] != undefined){
	           				vazaoCorretor = historicoMedicao['vazaoCorretor'];
	           			}

		               	exibirAlteracaoDadosLeitura(anoMesLeitura, idSelecionado, numeroLeituraFaturada, dataLeituraFaturada, descricaoAnormalidadeLeituraFaturada,
						numeroLeituraInformada, dataLeituraInformada, idAnormalidadeLeituraInformada, numeroLeituraAnterior, dataLeituraAnterior, idAnormalidadeLeituraAnterior,
						idLeiturista, consumoInformado, descricaoSituacao, numeroMedidor, undefined, undefined, vazaoCorretor);
	               	}
	        	}, async:false}

	        );
	}
}


function verificarConcessaoCredito() {
	var idHistoricoMedicao = document.forms[0].idHistoricoMedicao.value;
	var valorLeituraAnteriorModificado = document.forms[0].valorLeituraAnterior.value;
	var modificouLeituraAnterior = document.forms[0].modificouLeituraAnterior.value;

	if (valorLeituraAnteriorModificado != '' && modificouLeituraAnterior == 'true' && document.forms[0].alterarHistoricoMedicaoDiario.value == 'false') {

		AjaxService.verificarConcessaoCredito( idHistoricoMedicao, valorLeituraAnteriorModificado, {
	           	callback: function(verificarConcessaoCredito) {
	           		var houveCredito = verificarConcessaoCredito['houveCredito'];
	           		var credito = verificarConcessaoCredito['credito'];
	           		var diferenca = verificarConcessaoCredito['diferenca'];
	           		if (houveCredito == 'true') {
	           			var confirmacao = confirm("Confirma a gera��o autom�tica de cr�dito no valor de " + diferenca + "?");
	           			if (confirmacao == true) {
	           				document.forms[0].creditoVolume.value = credito;
	           			}

	           		}
	           	}, async:false})
	    }
	}

	function habilitarLeiturista(valor) {
		if (valor) {
			$("#idLeiturista").val("-1");
			$("#idLeiturista").attr("disabled",true);
		} else {
			$("#idLeiturista").attr("disabled",false);
		}
	}

	animatedcollapse.addDiv('conteinerDadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=1');

	function init() {
		var informadoCliente = "${analiseExcecaoLeituraVO.informadoCliente}";
		if (informadoCliente != undefined && informadoCliente == "true") {
			$("#idLeiturista").val("-1");
			$("#idLeiturista").attr("disabled",true);
		}

		var mensagensEstado = $(".mensagens").css("display");
		if(mensagensEstado == "block"){
			animatedcollapse.show('alterarDadosLeituraBloco');
			$.scrollTo($(".mensagens"),800);
		} else {
			animatedcollapse.hide('alterarDadosLeituraBloco');
		}
	}

	addLoadEvent(init);

</script>


<h1 class="tituloInterno">Detalhamento de Dados de Leitura e Consumo<a class="linkHelp" href="<help:help>/detalhamentodedadosdeleituraeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>


<form:form method="post" name="excecaoLeituraConsumoForm" action="exibirDetalhamentoExcecaoLeituraConsumo">
<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${pontoConsumo.chavePrimaria}" />
<input type="hidden" name="permitirAlteracao" id="permitirAlteracao" value="false" />
<input type="hidden" name="idHistoricoMedicao" id="idHistoricoMedicao" value="<c:out value="${analiseExcecaoLeituraVO.idHistoricoMedicao}"/>"/>
<input type="hidden" name="idHistoricoConsumo" id="idHistoricoConsumo" value="<c:out value="${analiseExcecaoLeituraVO.idHistoricoConsumo}"/>"/>
<input type="hidden" name="tela" id="tela" value="detalhamento"/>
<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value=""/>
<input name="anoMesLeitura" type="hidden" id="anoMesLeitura" value=""/>
<input name="anoMesFaturamento" type="hidden" id="anoMesFaturamento" value=""/>
<input name="numeroCiclo" type="hidden" id="numeroCiclo" value=""/>
<input type="hidden" name="modificouLeituraAnterior" id="modificouLeituraAnterior" value="<c:out value="${analiseExcecaoLeituraVO.modificouLeituraAnterior}"/>"/>
<input name="alterarHistoricoMedicaoDiario" type="hidden" id="alterarHistoricoMedicaoDiario" value="${analiseExcecaoLeituraVO.alterarHistoricoMedicaoDiario}"/>
<input type="hidden" name="gerarLog"/>
<input type="hidden" name="executarConsistir" id="executarConsistir"/>


<fieldset class="detalhamento">
	<fieldset class="conteinerBloco">
		<a class="linkPesquisaAvancada" data-closedimage="imagens/setaBaixo.png" data-openimage="imagens/setaCima.png" rel="toggle[conteinerDadosPontoConsumo]" href="#">
			Dados do Ponto de Consumo
			<img border="0" src="imagens/setaCima.png"/>
		</a>

		<div id="conteinerDadosPontoConsumo" class="conteinerDados">
			<fieldset id="dadosPontosConsumoCol1" class="coluna">
				<label class="rotulo" id="rotuloDescricao">Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio">

				<c:choose>
		    		<c:when test="${pontoConsumo.imovel.nome ne null}">
		    			<c:out value="${pontoConsumo.imovel.nome} - ${pontoConsumo.descricao}"/>
		    		</c:when>
		    		<c:otherwise>
		    			<c:out value="${pontoConsumo.descricao}"/>
		    		</c:otherwise>
		    	</c:choose>
				</span><br />

				<label class="rotulo" id="rotuloCliente">Cliente:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${clientePrincipal.nome}"/></span><br />
				<label class="rotulo" id="rotuloSituacao">Situa��o:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.situacaoConsumo.descricao}"/></span><br />
				<label class="rotulo" id="rotuloEndereco">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${pontoConsumo.enderecoFormatado}"/></span><br />
				<label class="rotulo" id="rotuloSegmento">Segmento:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.segmento.descricao}"/></span><br />
				<label class="rotulo" id="rotuloMatriculaImovel">Matr�cula do Im�vel:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.imovel.chavePrimaria}"/></span><br />
			</fieldset>

			<fieldset id="dadosPontosConsumoCol2" class="colunaFinal">
				<label class="rotulo" id="rotuloIndicadorImovelCondominio" >Indicador de Im�vel condom�nio:</label>
				<span class="itemDetalhamento">
					<c:choose>
						<c:when test="${pontoConsumo.imovel.condominio}">
							Sim
						</c:when>
						<c:otherwise>
							N�o
						</c:otherwise>
					</c:choose>
				</span><br />
				<label class="rotulo" id="rotuloNumeroSerieMedidor" >N�mero de S�rie do Medidor:</label>
				<span class="itemDetalhamento">
					<c:if test="${pontoConsumo.instalacaoMedidor ne null}">
						<c:out value="${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}"/>
					</c:if>
				</span><br />
				<label class="rotulo" id="rotuloDataInstalacaoMedidor" >Data de Instala��o do Medidor:</label>
				<span class="itemDetalhamento">
					<c:if test="${pontoConsumo.instalacaoMedidor ne null}">
						<fmt:formatDate value="${pontoConsumo.instalacaoMedidor.data}" pattern="dd/MM/yyyy HH:mm"/>
					</c:if>
				</span><br />
				<label class="rotulo" id="rotuloRota" >Rota:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.rota.numeroRota}"/></span><br />
				<label class="rotulo" id="rotuloGrupoFaturamento" >Grupo de Faturamento:</label>
				<span class="itemDetalhamento itemDetalhamentoPequeno"><c:out value="${pontoConsumo.rota.grupoFaturamento.descricao}"/></span><br />
				<label class="rotulo" id="rotuloCodigoLegado" >C�digo do Ponto de Consumo:</label>
				<span class="itemDetalhamento itemDetalhamentoPequeno"><c:out value="${pontoConsumo.codigoLegado}"/></span><br />
			</fieldset>
		</div>
	</fieldset>

	<c:if test="${empty listaPontosConsumo == false}">
			<hr class="linhaSeparadoraDetalhamento" />
			<fieldset id="listaPontosConsumo" class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Pontos de Consumo</legend>
				<!-- inserir abaixo da legenda a tabela listando os pontos de consumo -->
				<display:table decorator="br.com.ggas.web.medicao.leitura.ConsultarHistoricoLeituraMedicaoDecorator" export="false" class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontosConsumo" sort="list" id="pontoVO" pagesize="5" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoExcecaoLeituraConsumoAnalise">

			     	<display:column title="Matr�cula Im�vel" style="width: 70px">
			     		<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
							<c:out value='${pontoVO.pontoConsumo.imovel.chavePrimaria}'/>
			        	</a>
				    </display:column>

				    <display:column sortable="false" title="Nome" style="text-align: left">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
							<c:out value='${pontoVO.pontoConsumo.descricao}'/>
			        	</a>
				    </display:column>

				    <display:column title="Situa��o" style="width: 80px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				    		<c:out value='${pontoVO.pontoConsumo.situacaoConsumo.descricao}'/>
				    	</a>
				    </display:column>

				    <display:column sortable="false" title="Medidor" style="width: 150px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				    		<c:out value='${pontoVO.pontoConsumo.instalacaoMedidor.medidor.numeroSerie}'/>
				    	</a>
				    </display:column>

				    <display:column title="Refer�ncia / Ciclo" style="width: 80px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				    		<c:out value='${pontoVO.referencia}'/>-<c:out value='${pontoVO.ciclo}'/>
				    	</a>
				    </display:column>

				    <display:column title="Grupo de faturamento" style="width: 100px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				    		<c:out value='${pontoVO.pontoConsumo.rota.grupoFaturamento.descricao}'/>
				    	</a>
				    </display:column>

				    <display:column title="Rota" style="width: 90px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				    		<c:out value='${pontoVO.pontoConsumo.rota.numeroRota}'/>
				    	</a>
				    </display:column>

				    <display:column title="Seq. leitura" style="width: 40px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
				    		<c:out value='${pontoVO.pontoConsumo.numeroSequenciaLeitura}'/>
				    	</a>
				    </display:column>

				    <display:column title="Leiturista" style="width: 45px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
							<span alt="<c:out value='${pontoVO.pontoConsumo.rota.leiturista.funcionario.nomeFoneFormatado}'/>"  title="<c:out value='${pontoVO.pontoConsumo.rota.leiturista.funcionario.nomeFoneFormatado}'/>">
								<c:out value='${pontoVO.pontoConsumo.rota.leiturista.funcionario.matricula}'/>
							</span>
						</a>
				    </display:column>
				    <display:column title="C�digo <br/> Ponto de Consumo" style="width: 45px">
				    	<a href='javascript:detalhar(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
							<c:out value='${pontoVO.pontoConsumo.codigoLegado}'/>
						</a>
				    </display:column>
				</display:table>
			</fieldset>
		</c:if>

	<hr class="linhaSeparadora1" />

	<fieldset id="alterarDadosLeituraBloco" class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Alterar Dados de Leitura</legend>

		<fieldset id="dadosFixosFundo">
			<label class="rotulo" id="rotuloValorAnoMesReferencia">M�s / Ano-Ciclo:</label>
			<span id="valorAnoMesReferencia" class="itemDetalhamento campoHorizontal"><c:out value="${analiseExcecaoLeituraVO.anoMesLeituraCiclo}"/></span>
			<input type="hidden" id="anoMesLeituraCiclo" name="anoMesLeituraCiclo" value="${analiseExcecaoLeituraVO.anoMesLeituraCiclo}">

			<label class="rotulo rotuloHorizontal" id="rotuloMedidor" for="medidor" >Medidor:</label>
			<span id="medidor" class="itemDetalhamento campoHorizontal"><c:out value="${analiseExcecaoLeituraVO.numeroSerieMedidor}"/></span>
			<input type="hidden" id="numeroSerieMedidor" name="numeroSerieMedidor" value="${analiseExcecaoLeituraVO.numeroSerieMedidor}">

			<label class="rotulo rotuloHorizontal" id="rotuloSituacao" for="situacao" >Situa��o:</label>
			<span id="situacao" class="itemDetalhamento campoHorizontal"><c:out value="${analiseExcecaoLeituraVO.descricaoSituacao}"/></span>
			<input type="hidden" id="descricaoSituacao" name="descricaoSituacao" value="${analiseExcecaoLeituraVO.descricaoSituacao}">
		</fieldset>

		<fieldset id="leituraInformada" class="colunaEsq">
			<legend>Informada:</legend>
			<div id="leituraInformadaFundo">
				<label class="rotulo rotuloVertical" id="rotuloValorLeituraMedida" for="valorLeituraMedida">Leitura Informada:</label>
				<input class="campoTexto campoVertical" type="text" id="valorLeituraMedida" name="valorLeituraMedida" maxlength="9" onkeypress="return formatarCampoInteiro(event);" value="<c:out value="${analiseExcecaoLeituraVO.valorLeituraMedida}"/>"><br />

				<label class="rotulo rotuloVertical campoObrigatorio" id="rotuloDataRealizacaoLeitura" for="dataRealizacaoLeitura"><span class="campoObrigatorioSimbolo">* </span>Data da Leitura Informada:</label>
				<input class="campoData campoVertical" type="text" id="dataRealizacaoLeitura" name="dataRealizacaoLeitura" maxlength="10" value="<c:out value="${analiseExcecaoLeituraVO.dataRealizacaoLeitura}"/>"><br />

				<label class="rotulo rotuloVertical" id="rotuloAnormalidadeLeituraIdentificada" for="idAnormalidadeLeituraIdentificada" >Anormalidade Informada:</label>
				<select class="campoSelect campoVertical" name="idAnormalidadeLeituraIdentificada" id="idAnormalidadeLeituraIdentificada">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaAnormalidadeLeitura}" var="anormalidadeLeitura">
						<option value="<c:out value="${anormalidadeLeitura.chavePrimaria}"/>"<c:if test="${analiseExcecaoLeituraVO.idAnormalidadeLeituraIdentificada eq anormalidadeLeitura.chavePrimaria}"> selected="selected"</c:if>>
							<c:out value="${anormalidadeLeitura.descricao}"/>
						</option>
					</c:forEach>
				</select><br />
			</div>
		</fieldset>

		<fieldset id="leituraAnterior" class="colunaEsq">
			<legend>Anterior:</legend>
			<div id="leituraAnteriorFundo">
				<label class="rotulo rotuloVertical campoObrigatorio" id="rotuloValorLeituraAnterior" for="valorLeituraAnterior"><span class="campoObrigatorioSimbolo">* </span>Leitura Anterior:</label>
				<input class="campoTexto campoVertical" type="text" id="valorLeituraAnterior" name="valorLeituraAnterior" maxlength="9" onchange="this.form.modificouLeituraAnterior.value = true;" onkeypress="return formatarCampoInteiro(event);" value="<c:out value="${analiseExcecaoLeituraVO.valorLeituraAnterior}"/>"><br />

				<label class="rotulo rotuloVertical campoObrigatorio" id="rotuloDataRealizacaoLeitura" for="dataLeituraAnterior"><span class="campoObrigatorioSimbolo">* </span>Data da Leitura Anterior:</label>
				<input class="campoData campoVertical" type="text" id="dataLeituraAnterior" name="dataLeituraAnterior" maxlength="10" value="<c:out value="${analiseExcecaoLeituraVO.dataLeituraAnterior}"/>"><br />

			</div>
		</fieldset>

		<fieldset id="leituraConsiderada" class="colunaEsq">
			<legend>Considerada:</legend>
			<div id="leituraConsideradaFundo">
				<label class="rotulo rotuloVertical"  id="rotuloValorLeituraConsiderado" for="valorConsiderado">Leitura Considerada:</label>
				<span id="valorConsiderado" class="itemDetalhamento campoVertical"><c:out value="${analiseExcecaoLeituraVO.valorLeituraFaturada}"/></span><br />
				<input type="hidden" id="valorLeituraFaturada" name="valorLeituraFaturada" value="${analiseExcecaoLeituraVO.valorLeituraFaturada}">

				<label class="rotulo rotuloVertical" id="rotuloDataRealizacaoLeitura" for="dataConsiderada">Data da Leitura Considerada:</label>
				<span id="dataConsiderada" class="itemDetalhamento campoVertical"><c:out value="${analiseExcecaoLeituraVO.dataLeituraFaturada}"/></span><br />
				<input type="hidden" id="dataLeituraFaturada" name="dataLeituraFaturada" value="${analiseExcecaoLeituraVO.dataLeituraFaturada}">

				<label class="rotulo rotuloVertical" id="rotuloAnormalidadeConsiderada" for="anormalidadeConsiderada" >Anormalidade Considerada:</label>
				<span id="anormalidadeConsiderada" class="itemDetalhamento campoVertical"><c:out value="${analiseExcecaoLeituraVO.descricaoAnormalidadeFaturada}"/></span>
				<input type="hidden" id="descricaoAnormalidadeFaturada" name="descricaoAnormalidadeFaturada" value="${analiseExcecaoLeituraVO.descricaoAnormalidadeFaturada}">
			</div>
		</fieldset>

		<fieldset id="consumoAnoAnterior" class="colunaEsq">
			<legend>Consumo Ano Anterior:</legend>
			<div id="leituraConsideradaFundo">
				<label class="rotulo rotuloVertical"  id="rotuloValorLeituraConsiderado" for="valorConsiderado">Consumo Ano Atual:</label>
				<span id="valorConsumoAnoAtual" class="itemDetalhamento campoVertical"><c:out value=""/></span><br/>
				<label class="rotulo rotuloVertical"  id="rotuloValorLeituraConsiderado" for="valorConsiderado">Consumo Ano Anterior:</label>
				<span id="valorConsumoAnoAnterior" class="itemDetalhamento campoVertical"><c:out value=""/></span><br/>
			</div>
		</fieldset>

		<fieldset id="outrosDados" class="colunaEsq">
			<legend>Outros Dados:</legend>
			<div id="outrosDadosFundo">
				<label class="rotulo rotuloVertical campoObrigatorio" id="rotuloLeiturista" for="idLeiturista" >
				<span id="campoObrigatorioLeiturista" class="campoObrigatorioSimbolo">* </span>Leiturista:
				</label>
				<select name="idLeiturista" id="idLeiturista" class="campoSelect campoVertical">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaLeiturista}" var="leiturista">
						<option value="<c:out value="${leiturista.chavePrimaria}"/>"<c:if test="${analiseExcecaoLeituraVO.idLeiturista eq leiturista.chavePrimaria}"> selected="selected"</c:if>>
							<c:out value="${leiturista.funcionario.nome}"/>
						</option>
					</c:forEach>
				</select><br />

				<label class="rotulo rotuloVertical <c:if test="${comentarioObrigatorio eq true}">campoObrigatorio</c:if>" id="rotuloComentarioAlteracaoLeitura" for="comentarioAlteracaoLeitura"><c:if test="${comentarioObrigatorio eq true}"><span class="campoObrigatorioSimbolo">* </span></c:if>Coment�rio:</label>
				<textarea class="campoTexto campoVertical" id="comentarioAlteracaoLeitura" name="comentarioAlteracaoLeitura" onkeypress="return formatarCampoTextoComLimite(event,this,1000);"><c:out value="${analiseExcecaoLeituraVO.comentarioAlteracaoLeitura}"/></textarea><br />

				<label class="rotulo rotuloVertical" id="rotuloValorLeituraConsiderada" for="valorLeituraConsiderada">Consumo Informado:</label>
				<input class="campoTexto campoVertical" type="text" id="volumeApurado" name="volumeApurado" maxlength="17" onkeypress="return formatarCampoDecimalPositivo(event,this,7,4);" value="<c:out value="${analiseExcecaoLeituraVO.volumeApurado}"/>">

				<label class="rotulo rotuloVertical" id="rotuloCreditoVolume" for="creditoVolume">Cr�dito Volume:</label>
				<input class="campoTexto campoVertical" type="text" id="creditoVolume" name="creditoVolume" maxlength="9" onkeypress="return formatarCampoInteiro(event);" value="<c:out value="${analiseExcecaoLeituraVO.creditoVolume}"/>" <c:if test="${analiseExcecaoLeituraVO.alterarHistoricoMedicaoDiario eq true}">disabled="disabled"</c:if>>

				<label class="rotulo rotuloVertical" id="rotuloCreditoVolume" for="creditoVolume">Informado pelo Cliente:</label>
				<input class="campoCheckbox campoHorizontal" type="checkbox" id="informadoCliente" name="informadoCliente" value="true" <c:if test="${analiseExcecaoLeituraVO.informadoCliente eq 'true'}">checked="checked"</c:if><c:if test="${analiseExcecaoLeituraVO.alterarHistoricoMedicaoDiario eq true}">disabled="disabled"</c:if> onclick="habilitarLeiturista(this.checked);">
			</div>
		</fieldset>

		<p id="legendaAlterarDadosLeituraBloco" class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para altera��o de dados de leitura.</p>
<fieldset class="conteinerBotoes">
	<input name="Button" class="bottonRightCol" value="Limpar" type="button" onclick="limparDadosLeitura();">
    <input name="Button" id="salvarAlteracaoLeitura" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="<c:if test="${gerarCreditoVolumeAutomatico == true}">verificarConcessaoCredito();</c:if>alterarDadosFaturamento();" <c:if test="${analiseExcecaoLeituraVO.idHistoricoMedicao eq null || analiseExcecaoLeituraVO.idHistoricoMedicao eq ''}"> disabled="disabled"</c:if>>
</fieldset>

		<hr class="linhaSeparadoraDetalhamento" />
	</fieldset>

	<fieldset id="historicoLeitura" class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Hist�rico de Leitura</legend>
		<display:table class="dataTableGGAS dataTableMenor2 dataTableCabecalho2Linhas" name="listaHistoricoMedicaoVO" sort="list" id="historicoVO" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoExcecaoLeituraConsumoAnalise" >
			<display:column title="M�s / Ano-Ciclo" style="width: 60px">
	     		<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
	     			<c:out value='${historicoVO.historicoMedicao.anoMesLeituraFormatado}'/>-<c:out value='${historicoVO.historicoMedicao.numeroCiclo}'/>
	     		</a>
		    </display:column>

			<display:column sortable="false" title="Data Considerada" style="width: 70px">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
					<fmt:formatDate value="${historicoVO.historicoMedicao.dataLeituraFaturada}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>

			<display:column title="Leitura<br />Considerada">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
		    		<fmt:formatNumber value="${historicoVO.historicoMedicao.numeroLeituraFaturada}" maxFractionDigits="4"/>
		    	</a>
		    </display:column>

			<display:column sortable="false" title="Anormalidade Considerada" style="width: 85px">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
					<span alt="${historicoVO.historicoMedicao.anormalidadeLeituraFaturada.descricao}" title="${historicoVO.historicoMedicao.anormalidadeLeituraFaturada.descricao}">${historicoVO.historicoMedicao.anormalidadeLeituraFaturada.chavePrimaria}</span>
				</a>
			</display:column>

			<display:column sortable="false" title="Data<br />Anterior" style="width: 70px">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
					<fmt:formatDate value="${historicoVO.dataLeituraUltimoDiaCicloAnterior}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>

			<display:column title="Leitura<br />Anterior">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
		    		<fmt:formatNumber value="${historicoVO.historicoMedicao.numeroLeituraAnterior}" maxFractionDigits="4"/>
		    	</a>
		    </display:column>

			<display:column sortable="false" title="Consumo<br />Informado (m<span class='expoente'>3</span>)">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
					<c:out value="${historicoVO.historicoMedicao.consumoInformado}" />
				</a>
			</display:column>

			<display:column sortable="false" title="Cr�dito<br />Volume (m<span class='expoente'>3</span>)">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${historicoVO.historicoMedicao.creditoVolume}" maxFractionDigits="4"/>
				</a>
			</display:column>

			<display:column sortable="false" title="Leiturista" style="width: 50px">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
					<span alt="<c:out value='${historicoVO.historicoMedicao.leiturista.funcionario.nomeFoneFormatado}'/>"  title="<c:out value='${historicoVO.historicoMedicao.leiturista.funcionario.nomeFoneFormatado}'/>">
						<c:out value='${historicoVO.historicoMedicao.leiturista.funcionario.matricula}'/>
					</span>
				</a>
			</display:column>

			<display:column sortable="false" title="Medidor" style="width: 75px">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span>
					<span alt="Data instala��o: <fmt:formatDate value="${historicoVO.historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoVO.historicoMedicao.historicoInstalacaoMedidor.medidor.digito}" title="Data instala��o: <fmt:formatDate value="${historicoVO.historicoMedicao.historicoInstalacaoMedidor.data}" type="both" pattern="dd/MM/yyyy" />, M�ximo de d�gitos: ${historicoVO.historicoMedicao.historicoInstalacaoMedidor.medidor.digito}">${historicoVO.historicoMedicao.historicoInstalacaoMedidor.medidor.numeroSerie}</span>
				</a>
			</display:column>

			<display:column sortable="false" title="Situa��o" style="width: 75px">
				<a title="Detalhar Hist�rico Leitura" href="javascript:exibirDetalhamentoHistoricoLeituraPontoConsumo(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo}, <c:out value="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}"/>)"><span class="linkInvisivel"></span><c:out value='${historicoVO.historicoMedicao.situacaoLeitura.descricao}'/></a>
			</display:column>

			<display:column sortable="false" title="Altera��es<br /> Realizadas" style="width: 55px">
				<c:if test="${historicoVO.existeComentario eq true}">
					<a title="Registro alterado. Clique para visualizar os coment�rios." href="javascript:exibirMedicaoHistoricoComentarios(${pontoConsumo.chavePrimaria},${historicoVO.historicoMedicao.anoMesLeitura},${historicoVO.historicoMedicao.numeroCiclo})"><span class="linkInvisivel"></span><img title="Visualizar Coment�rios" alt="Visualizar Coment�rios"  src="<c:url value="/imagens/icone_exibir_detalhes.png"/>" border="0"></a>
				</c:if>
			</display:column>

			<display:column title="Alterar<br />Dados" style="text-align: center; width: 50px">
				<c:set var="anormalidadeFaturada" value=""/>
				<c:if test="${historicoVO.historicoMedicao.anormalidadeLeituraFaturada ne null}">
					<c:set var="anormalidadeFaturada" value="${historicoVO.historicoMedicao.anormalidadeLeituraFaturada.descricao}"/>
				</c:if>
				<c:set var="anormalidadeInformada" value="-1"/>
				<c:if test="${historicoVO.historicoMedicao.anormalidadeLeituraInformada ne null}">
					<c:set var="anormalidadeInformada" value="${historicoVO.historicoMedicao.anormalidadeLeituraInformada.chavePrimaria}"/>
				</c:if>
				<c:set var="anormalidadeAnterior" value="-1"/>
				<c:if test="${historicoVO.historicoMedicao.anormalidadeLeituraAnterior ne null}">
					<c:set var="anormalidadeAnterior" value="${historicoVO.historicoMedicao.anormalidadeLeituraAnterior.chavePrimaria}"/>
				</c:if>
				<c:set var="leiturista" value="-1"/>
				<c:if test="${historicoVO.historicoMedicao.leiturista ne null}">
					<c:set var="leiturista" value="${historicoVO.historicoMedicao.leiturista.chavePrimaria}"/>
				</c:if>
				<c:if test="${historicoVO.historicoMedicao.anoMesCicloFormatado >= anoMesReferencia}">
					<a href="javascript:exibirAlteracaoDadosLeitura('${historicoVO.historicoMedicao.anoMesLeituraFormatado}'+'-'+'${historicoVO.historicoMedicao.numeroCiclo}','${historicoVO.historicoMedicao.chavePrimaria}'
						,'<fmt:formatNumber value="${historicoVO.historicoMedicao.numeroLeituraFaturada}" maxFractionDigits="0"/>','<fmt:formatDate value="${historicoVO.historicoMedicao.dataLeituraFaturada}" pattern="dd/MM/yyyy"/>','${anormalidadeFaturada}'
						,'<fmt:formatNumber value="${historicoVO.historicoMedicao.numeroLeituraInformada}" maxFractionDigits="0"/>','<fmt:formatDate value="${historicoVO.historicoMedicao.dataLeituraInformada}" pattern="dd/MM/yyyy"/>','${anormalidadeInformada}'
						,'<fmt:formatNumber value="${historicoVO.historicoMedicao.numeroLeituraAnterior}" maxFractionDigits="0"/>','<fmt:formatDate value="${historicoVO.historicoMedicao.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>','${anormalidadeAnterior}'
						,'${leiturista}', '<fmt:formatNumber value="${historicoVO.historicoMedicao.consumoInformado}" maxFractionDigits="4"/>', '${historicoVO.historicoMedicao.situacaoLeitura.descricao}'
						,'${historicoVO.historicoMedicao.historicoInstalacaoMedidor.medidor.numeroSerie}','<fmt:formatNumber value="${historicoVO.historicoMedicao.creditoVolume}" maxFractionDigits="0"/>'
						,undefined, '${historicoVO.historicoMedicao.historicoInstalacaoMedidor.vazaoCorretor}', '<fmt:formatNumber value="${historicoVO.consumoAnoAnterior}" maxFractionDigits="0"/>', '<fmt:formatNumber value="${historicoVO.consumoAnoAtual}" maxFractionDigits="0"/>');">
						<img title="Alterar Dados de Leitura" alt="Alterar Dados de Leitura"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0">
					</a>
				</c:if>

			</display:column>
	   	</display:table>
	</fieldset>

	<hr class="linhaSeparadoraDetalhamento" />





	<%--
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Alterar Dados de Consumo</legend>
		<a name="alterarDadosConsumo"></a>
		<label class="rotulo campoObrigatorio" id="rotuloConsumoLido" for="consumo"><span class="campoObrigatorioSimbolo">* </span>Consumo Lido:</label>
		<input class="campoTexto" type="text" id="consumo" name="consumo" maxlength="9" onkeypress="return formatarCampoInteiro(event);" value="<c:out value="${analiseExcecaoLeituraVO.consumo}"/>"><br />

		<label class="rotulo campoObrigatorio" id="rotuloConsumoApurado" for="consumoApurado"><span class="campoObrigatorioSimbolo">* </span>Consumo Apurado:</label>
		<input class="campoTexto" type="text" id="consumoApurado" name="consumoApurado" maxlength="9" onkeypress="return formatarCampoInteiro(event);" value="<c:out value="${analiseExcecaoLeituraVO.consumoApurado}"/>"><br />

		<label class="rotulo campoObrigatorio" id="comentarioAlteracaoConsumo" for="comentarioAlteracaoLeitura"><span class="campoObrigatorioSimbolo">* </span>Coment�rio:</label>
		<textarea class="campoTexto" id="comentarioAlteracaoConsumo" name="comentarioAlteracaoConsumo" onkeypress="return formatarCampoTextoComLimite(event,this,1000);"><c:out value="${analiseExcecaoLeituraVO.comentarioAlteracaoConsumo}"/></textarea><br />

		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios apenas para altera��o de dados de consumo.</p>
	</fieldset>
	<fieldset class="conteinerBotoesAba">
		<input name="Button" class="bottonRightCol" value="Limpar" type="button" onclick="limparDadosConsumo();">
	    <input name="Button" id="salvarAlteracaoConsumo" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="alterarDadosConsumo();" <c:if test="${analiseExcecaoLeituraVO.idHistoricoConsumo eq null || analiseExcecaoLeituraVO.idHistoricoConsumo eq ''}"> disabled="disabled"</c:if>>
	</fieldset>	--%>


	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Hist�rico de Consumo</legend>
		<display:table class="dataTableGGAS dataTableMenor2 dataTableCabecalho2Linhas" name="listaHistoricoConsumo" sort="list" id="historicoConsumo" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoExcecaoLeituraConsumoAnalise" >
			<display:column title="M�s / Ano-Ciclo" style="width: 60px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})">
					<c:out value='${historicoConsumo.anoMesFaturamentoFormatado}'/>-<c:out value='${historicoConsumo.numeroCiclo}'/>
				</a>
			 </display:column>

			<display:column sortable="false" title="Consumo (m<span class='expoente'>3</span>)" style="width: 90px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
			   		<fmt:formatNumber value="${historicoConsumo.consumo}" type="number" maxFractionDigits="4"/>
			   	</a>
			</display:column>

			<display:column sortable="false" title="Consumo<br />Apurado (m<span class='expoente'>3</span>)" style="width: 90px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
			   	 	<fmt:formatNumber value="${historicoConsumo.consumoApurado}" minFractionDigits="${escalaConsumoApurado}"/>
			    </a>
			</display:column>

			<display:column sortable="false" title="Consumo<br />Di�rio (m<span class='expoente'>3</span>)" >
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
			   		<fmt:formatNumber value="${historicoConsumo.consumoDiario}" type="number" maxFractionDigits="4"/>
			   	</a>
			</display:column>

			<display:column sortable="false" title="Fator PTZ" >
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${historicoConsumo.fatorPTZ}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
				</a>
			</display:column>

			<display:column sortable="false" title="Fator PCS" style="width: 60px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${historicoConsumo.fatorPCS}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
				</a>
			</display:column>

			<display:column sortable="false" title="Fator<br />Corre��o" style="width: 60px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${historicoConsumo.fatorCorrecao}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
				</a>
			</display:column>

			<display:column sortable="false" title="M�dia<br />Referencial (m<span class='expoente'>3</span>)" style="width: 60px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${historicoConsumo.consumoApuradoMedio}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
				</a>
			</display:column>

			<display:column sortable="false" title="Anormalidade" style="width: 85px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					<span title="${historicoConsumo.anormalidadeConsumo.descricao}">${historicoConsumo.anormalidadeConsumo.chavePrimaria}</span>
				</a>
			</display:column>
			<display:column sortable="false" title="Dias de<br />Consumo" style="width: 55px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					${historicoConsumo.diasConsumo}
				</a>
			</display:column>
<display:column sortable="false" title="Tipo de<br />Consumo" style="width: 85px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					${historicoConsumo.tipoConsumo.descricao}
				</a>
			</display:column>
			<display:column sortable="false" title="Varia��o<br />Consumo" style="width: 60px">
				<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoPontoConsumo(${pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${historicoConsumo.consumoApuradoPercentualConsumoMedio}" maxFractionDigits="2"/>%
				</a>
			</display:column>
	   	</display:table>
	</fieldset>

</fieldset>

<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
</fieldset>
<token:token></token:token>
</form:form>
