<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<script>

function recuperarPickList(form){
	<c:forEach items="${listaConsultaParametro}" var="parametro">
	
		var lista = document.getElementById(<c:out value="${parametro.chavePrimaria}"/>);
		
		if (lista.multiple != 'undefined' && lista.multiple==true){
			for (i=0; i<lista.length; i++){
				lista.options[i].selected = true;
			}
		}
		
	</c:forEach>
	return true;
}

</script>

<fieldset id="parametrosExecucaoConsulta" class="conteinerBloco">

   	<c:forEach items="${listaConsultaParametro}" var="parametro">
		<c:if test="${parametro.entidadeConteudo.descricao.toUpperCase() == 'ALFANUM�RICO'}">
			
			<c:choose>
				<c:when test="${parametro.nomeCampo == 'C�digo da Rota'}">
				    <label class="rotulo campo2Linhas" id="rotuloRota" for="idsRota">Rota:</label>
					<select id="idsRotas" 
					name="<c:out value="${parametro.chavePrimaria}"/>" class="campoList campo2Linhas" multiple="multiple">
						<c:forEach items="${listaRota}" var="rota">
							<c:set var="selecionado" value="false"/>
							<option value="<c:out value="${rota.chavePrimaria}"/>"<c:if test="${selecionado eq true}"> selected="selected"</c:if> title="<c:out value="${rota.numeroRota}"/>">
								<c:out value="${rota.numeroRota}"/>
							</option>		
						</c:forEach>
					</select>
				 </c:when>
				 <c:otherwise>
				    <label class="rotulo campoObrigatorio" id="rotulo<c:out value="${parametro.chavePrimaria}"/>" for="<c:out value="${parametro.chavePrimaria}"/>"><span class="campoObrigatorioSimbolo">* </span><c:out value="${parametro.nomeCampo}"/>:</label>
				 
					<input class="campoTexto" type="text" id="<c:out value="${parametro.chavePrimaria}"/>" 
					name="<c:out value="${parametro.chavePrimaria}"/>" maxlength="${parametro.numeroMaximoCaracteres}" 
					size="${parametro.numeroMaximoCaracteres}" value=""/>
					
								<c:choose>
									<c:when test="${parametro.mascara ne null}">
										<script>
											$('#${parametro.chavePrimaria}').inputmask('${parametro.mascara}'. {placeholder:" "});
										</script>							
									</c:when>
								</c:choose>				 
				 </c:otherwise>
				</c:choose>
			
		</c:if>
		<c:if test="${parametro.entidadeConteudo.descricao.toUpperCase() == 'BOLEANO'}">
			<input type="checkbox" class="campoCheckbox" id="<c:out value="${parametro.chavePrimaria}"/>" name="<c:out value="${parametro.chavePrimaria}"/>"/>
		</c:if>
		<c:if test="${parametro.entidadeConteudo.descricao.toUpperCase() == 'DATA'}">
			<input class="campoData" type="text" id="<c:out value="${parametro.chavePrimaria}"/>" name="<c:out value="${parametro.chavePrimaria}"/>" maxlength="${parametro.numeroMaximoCaracteres}" size="${parametro.numeroMaximoCaracteres}" value="">
		</c:if>	
		<c:if test="${parametro.entidadeConteudo.descricao.toUpperCase() == 'NUM�RICO'}">

				<c:choose>
					<c:when test="${parametro.fk eq true}">
						<c:choose>
							<c:when test="${parametro.multivalorado eq false}">
								<select id='<c:out value="${parametro.chavePrimaria}"/>' name='<c:out value="${parametro.chavePrimaria}"/>' class="campoSelect">
									<c:forEach var="lista" items="${listas}" >
										<c:if test="${lista.key eq parametro.chavePrimaria}"> 
											<c:forEach var="elemento" items="${lista.value}">						
												<option value="<c:out value="${elemento.chavePrimaria}"/>">
													<c:out value="${elemento.label}"/>
												</option>											
											</c:forEach>
									  	</c:if>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
																					               
						        <select id='lista<c:out value="${parametro.chavePrimaria}"/>' name='lista<c:out value="${parametro.chavePrimaria}"/>' class="campoList" multiple="multiple"
						            onDblClick='moveSelectedOptions(this,document.forms[0].lista<c:out value="${parametro.chavePrimaria}"/>,true)'>
									<c:forEach var="lista" items="${listas}" >
										<c:if test="${lista.key eq parametro.chavePrimaria}"> 
											<c:forEach var="elemento" items="${lista.value}">						
												<option value="<c:out value="${elemento.chavePrimaria}"/>">
													<c:out value="${elemento.label}"/>
												</option>											
											</c:forEach>
									  	</c:if>
									</c:forEach>
						          </select>

	            <input type="button" name="right" value="&gt;&gt;" class="bottonRightCol"
	                   onClick='moveSelectedOptions(document.getElementById("<c:out value="lista${parametro.chavePrimaria}"/>"),document.getElementById("<c:out value="${parametro.chavePrimaria}"/>"),true)'>
	              <input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7"
	                   onClick='moveAllOptions(document.getElementById("<c:out value="lista${parametro.chavePrimaria}"/>"),document.getElementById("<c:out value="${parametro.chavePrimaria}"/>"),true)'>
	              <input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
	                   onClick='moveSelectedOptions(document.getElementById("<c:out value="${parametro.chavePrimaria}"/>"),document.getElementById("<c:out value="lista${parametro.chavePrimaria}"/>"),true)'>
	              <input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7"
	                   onClick='moveAllOptions(document.getElementById("<c:out value="${parametro.chavePrimaria}"/>"),document.getElementById("<c:out value="lista${parametro.chavePrimaria}"/>"),true)'>

						        <select id='<c:out value="${parametro.chavePrimaria}"/>' name='<c:out value="${parametro.chavePrimaria}"/>' class="campoList" multiple="multiple"
						            onDblClick='moveSelectedOptions(this,document.forms[0].lista<c:out value="${parametro.chavePrimaria}"/>,true)'>
						          </select>
														
							</c:otherwise>
						</c:choose>	
					</c:when>
					<c:otherwise>
						<input class="campoTexto" type="text" id="<c:out value="${parametro.chavePrimaria}"/>" name="<c:out value="${parametro.chavePrimaria}"/>" maxlength="${parametro.numeroMaximoCaracteres}" size="${parametro.numeroMaximoCaracteres}" onkeypress="return formatarCampoInteiro(event,${parametro.numeroMaximoCaracteres});">
						
						<c:choose>
							<c:when test="${parametro.mascara ne null}">
								<script>

									$('#${parametro.chavePrimaria}').inputmask('${parametro.mascara}', {placeholder:""});

								</script>							
							</c:when>
							<c:otherwise>
								<c:set var="mask" value="9"/>
								<c:forEach var="i" begin="1" end="${parametro.numeroMaximoCaracteres}" step="1">
									<c:set var="qtd" value="${mask}${qtd}"/>
								</c:forEach>
									<script>			

										$('#${parametro.chavePrimaria}').inputmask('${qtd}', {placeholder:""});

									</script>
							</c:otherwise>
						</c:choose>
												
					</c:otherwise>
				</c:choose>		
		</c:if>
		<br class="quebraLinha2"/>
    </c:forEach>

</fieldset>