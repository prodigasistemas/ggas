<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
 
<script>
	
	function cancelar(){
		location.href = '<c:url value="/exibirExportacaoDispositivosMoveis"/>';
	}

	function adicionarColuna(form){
		var combo = form.idColuna;
		var selecionados = 0;
		for (var i=0; i<combo.options.length; i++) {
			if (combo.options[i].selected){
				selecionados++;
				if (form.colunasSelecionadas.value.length==0){
					form.colunasSelecionadas.value += combo.options[i].value;
				}else{
					form.colunasSelecionadas.value += ", " + combo.options[i].value;
				}
			}
		}
		if (selecionados == 0){
			alert("Selecione um item da lista!");
		}
	}		
	
</script>

<h1 class="tituloInterno">Alterar Consulta de Exporta��o de Dados para Dispositivos M�veis...<a class="linkHelp" href="<help:help>/alterandoaconsultadeexportaodedadosparadispositivosmveis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Altere as informa��es desejadas e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.</p>

<form:form method="post" action="alterarExportacaoDispositivosMoveis" id="dispositivosMoveisForm" name="dispositivosMoveisForm" >

	<input name="acao" type="hidden" id="acao" value="alterarExportacaoDispositivosMoveis"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${exportacaoDadosVO.chavePrimaria}"/>
	<input name="indexLista" type="hidden" id="indexLista" value="${exportacaoDadosVO.indexLista}">
	
	<fieldset id="conteinerAlterarExportacaoDadosDispositivosMoveis" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<fieldset class="conteinerLista1">
				<label class="rotulo rotuloVertical" for="colunas">Colunas:</label><br />
				<select name="idColuna" id="idColuna" class="campoList listaCurta1a campoVertical" multiple="multiple" onDblClick=this.form.btAdicionar.click();>
					<c:forEach items="${colunas}" var="coluna">
						<option value="<c:out value="${coluna.nome}"/>"><c:out value="${coluna.nome}"/></option>			
					</c:forEach>
				</select>
				<fieldset class="conteinerBotoesCampoList1a" style="width: 105px" >
					<input type="button" name="btAdicionar" value="Adicionar &gt;&gt;" class="bottonRightCol2 botoesLargosIE7"
			        style="width: 105px" onClick="adicionarColuna(this.form);">
				</fieldset>
			</fieldset>
		
			<fieldset class="camposSelecionados rotuloListaLonga2">
				<label class="rotulo rotuloVertical" for="colunas">Colunas Selecionadas:</label><br />
				<textarea class="campoList listaCurta2a campoVertical" id="colunasSelecionadas" style="padding-bottom: 0; height: 128px" name="colunasSelecionadas">${sqlCampos ne 'null' ? sqlCampos : none}</textarea>
			</fieldset>
		</fieldset>
		<fieldset class="conteinerBloco">
			<label for="rotuloSql" class="rotuloInformativo rotuloHorizontal" style="margin-right: 160px; padding-top: 0">OBS: Clique duplo ou selecione e clique em <span class="destaqueOrientacaoInicial">Adicionar</span>.</label>
			<label for="rotuloSql" class="rotuloInformativo rotuloHorizontal" style="padding-top: 0">OBS: Ordenar de acordo com a coluna desejada.</label>
		</fieldset>
	
		<fieldset class="conteinerBloco">
			<label class="rotulo rotuloVertical" for="restricoes">Restri��es:</label>
			<textarea class="campoVertical" cols="100" rows="10" id="sql" name="sql" style="text-transform: uppercase" onblur="toUpperCase(this)">${sql}</textarea>
			<label for="restricoes" class="rotuloInformativo rotuloInformativo2Linhas rotuloHorizontal" style="clear: left; margin-top: 0; padding-top: 0">OBS: Desprezar a Clausula WHERE, inicie as restri��es com AND. Ex: AND LEMO_CD = {CODIGO}.</label>
		</fieldset>
		
		<hr class="linhaSeparadora1" />	
		
		<p class="orientacaoInicial">Preencha os dados do par�metro relacionando cada identificador entre chaves ex: {CODIGO} na Declara��o SQL acima com o campo C�digo de cada par�metro abaixo. Preencha os dados restantes do par�metro e clique em <span class="destaqueOrientacaoInicial">Adicionar</span>.</p>
		<fieldset id="conteinerConsultaExportacaoParametros" class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Par�metros:</legend>
			<jsp:include page="/jsp/medicao/leitura/exportarimportardadosdispositivosmoveis/incluirParametroConsultaExportacao.jsp" >
				<jsp:param name="actionAdicionarParametro" value="adicionarParametroDispositivosMoveisFluxoInclusao" />
				<jsp:param name="actionAlterarParametro" value="adicionarParametroDispositivosMoveisFluxoAlteracao" />
				<jsp:param name="actionRemoverParametro" value="removerParametroDispositivosMoveisFluxoAlteracao" />
			</jsp:include>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">	 
	    <input name="Button" class="bottonRightCol2" id="botaoCancelarParametro" name="botaoCancelarParametro" value="Cancelar" type="button" onclick="cancelar();">
	    <input name="Button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="submit">
	</fieldset>
	<token:token></token:token>
</form:form>
  