<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
 
<script>

	$(function() {
		// limpa os campos do formul�rio
		$("input#limparArquivo").click(function() {
			$(".file").attr({value: "" });
		});
	});

	/*function MultiSelector( list_target, max ){
	
		// Where to write the list
		this.list_target = list_target;
		// How many elements?
		this.count = 0;
		// How many elements?
		this.id = 0;
		// Is there a maximum?
		if( max ){
			this.max = max;
		} else {
			this.max = -1;
		};
		
		/**
		 * Add a new file input element
		 */

		/*this.addElement = function( element ){
	
			// Make sure it's a file input element
			if( element.tagName == 'INPUT' && element.type == 'file' ){
	
				// Element name -- what number am I?
				element.name = 'file_' + this.id;

				// Element id -- what number am I? *
				element.id = 'file_' + this.id++;
	
				// Add reference to this object
				element.multi_selector = this;
	
				// What to do when a file is selected
				element.onchange = function(){
	
					// New file input
					var new_element = document.createElement( 'input' );
					new_element.type = 'file';
	
					// Add new element
					this.parentNode.insertBefore( new_element, this );
	
					// Apply 'update' to element
					this.multi_selector.addElement( new_element );
	
					// Update list
					this.multi_selector.addListRow( this );
	
					// Hide this: we can't use display:none because Safari doesn't like it
					this.style.position = 'absolute';
					this.style.left = '-1000px';
	
				};
				// If we've reached maximum number, disable input element
				if( this.max != -1 && this.count >= this.max ){
					element.disabled = true;
				};
	
				// File element counter
				this.count++;
				// Most recent element
				this.current_element = element;
				
			} else {
				// This can only be applied to file input elements!
				alert( 'Error: not a file input element' );
			};
	
		};
	
		/**
		 * Add a new row to the list of files
		 */
		/*this.addListRow = function( element ){
	
			// Row div
			var new_row = document.createElement( 'div' );
	
			// Delete button
			var new_row_button = document.createElement( 'input' );
			new_row_button.type = 'button';
			new_row_button.value = 'Delete';
	
			// References
			new_row.element = element;
	
			// Delete function
			new_row_button.onclick= function(){
	
				// Remove element from form
				this.parentNode.element.parentNode.removeChild( this.parentNode.element );
	
				// Remove this row from the list
				this.parentNode.parentNode.removeChild( this.parentNode );
	
				// Decrement counter
				this.parentNode.element.multi_selector.count--;
	
				// Re-enable input element (if it's disabled)
				this.parentNode.element.multi_selector.current_element.disabled = false;
	
				// Appease Safari
				//    without it Safari wants to reload the browser window
				//    which nixes your already queued uploads
				return false;
			};
	
			// Set row value
			new_row.innerHTML = element.value;
	
			// Add button
			new_row.appendChild( new_row_button );
	
			// Add it to the list
			this.list_target.appendChild( new_row );
			
		};
	
	};*/

	function exibirProcessos(){
		submeter('dispositivosMoveisForm', 'pesquisarProcesso');
	}
	
</script>

<h1 class="tituloInterno">Importar Dados de Dispositivos M�veis<a class="linkHelp" href="<help:help>/importaodedadosdedispositivosmveis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os arquivos XML ou TXT desejados e depois clique em <span class="destaqueOrientacaoInicial">Importar</span>.</p>

<form:form method="post" action="importarLeituraDispositivosMoveis" id="dispositivosMoveisForm" name="dispositivosMoveisForm" enctype="multipart/form-data">
	<!--  --><input name="acao" type="hidden" id="acao" value="importarLeituraDispositivosMoveis"/>
	<input name="listaIdProcesso" type="hidden" id="listaIdProcesso" value="${listaIdProcesso}"/>

	<fieldset id="conteinerImportarDadosDispositivosMoveis" class="conteinerPesquisarIncluirAlterar">			
		<label class="rotulo rotuloVertical" for="arquivo">Arquivo(s):</label>
		<input id="arquivo" class="campoFileMulti multi" type="file" name="file_1" title="Procurar" accept="xml" />
		<br class="quebraLinha2" />
		
<!--		<label class="rotulo" for="arquivos_selecionados">Arquivos selecionados:</label><br class="quebraLinha2" />-->
<!--		<div id="arquivos_selecionados"></div>-->
<!--		<script>-->
<!--			var multi_selector = new MultiSelector( document.getElementById('arquivos_selecionados'), 10 );-->
<!--			multi_selector.addElement( document.getElementById( 'arquivo' ) );-->
<!--		</script>-->
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<vacess:vacess param="importarLeituraDispositivosMoveis">
			<input name="buttonImportar" class="bottonRightCol2 bottonRightColUltimo" value="Importar" type="submit">
		</vacess:vacess>
	</fieldset>
	
	<c:if test="${listaProcesso ne null}">
		<br />
		
		<h1 class="tituloInterno">Processos Gerados</h1>
		<p class="orientacaoInicial">Seguem abaixo as informa��es gerais dos processos criados para importar os arquivos informados. Para mais detalhes clique em <span class="destaqueOrientacaoInicial">Exibir Processos</span>.</p>
		<br />
		
		<display:table class="dataTableGGAS" name="listaProcesso" sort="list" id="processo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	    	<display:column style="text-align: center;" property="chavePrimaria" sortProperty="chavePrimaria" sortable="true" titleKey="ENTIDADE_ROTULO_PROCESSO" />	 
			<display:column style="text-align: left; padding-left: 5px" property="descricao" sortProperty="descricao" sortable="true" title="Descri��o" />
	    </display:table>
	    
	    <fieldset class="conteinerBotoes">
	    	<vacess:vacess param="pesquisarProcesso">
				<input name="buttonExibirProcessos" class="bottonRightCol2 bottonRightColUltimo" value="Exibir Processos" type="button" onclick="exibirProcessos();">
			</vacess:vacess>
		</fieldset>
	</c:if>	
	
</form:form>
