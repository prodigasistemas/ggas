<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
 
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/selectbox.js"></script>

<script>
	function alterar(){
		submeter('dispositivosMoveisForm', 'exibirAlteracaoExportacaoDispositivosMoveis');
	}

	function reexportar(){
		$("#isReexportar").val(true);
		$(".mensagens").hide();
		exibirIndicador();
		
		if($("#idsRotas").length) {
			verificarSelectRotas();
		}
		submeter('dispositivosMoveisForm', 'exportarDispositivosMoveis');
	}
	
	function exportar(){
		$(".mensagens").hide();
		if($("#idsRotas").length) {
			verificarSelectRotas();
		}
		submeter('dispositivosMoveisForm', 'exportarDispositivosMoveis');
	}

	function exibirRelatorio(){

		<c:if test="${exibirRelatorio eq true}">
			var iframe = document.getElementById("download");
			iframe.src = 'exibirExportacaoDispositivosMoveisRelatorio';
			
			var iframe2 = document.getElementById("download2");
			iframe2.src = 'exibirExportacaoDispositivosMoveisDadosLeitura';
		</c:if>
		
	}

	function init(){
		exibirRelatorio();	
	}
	
	function verificarSelectRotas() {
		var rotasSelecionadas = $("#idsRotas option:selected").val(); 
		
		if (typeof rotasSelecionadas == 'undefined' ) {
			$('#idsRotas').children().attr("selected", true); 
		}
	}

	addLoadEvent(init);
	
</script>

<h1 class="tituloInterno">Exportar Dados para Dispositivos M�veis<a class="linkHelp" href="<help:help>/exportandoumarquivo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os filtros necess�rios e depois clique ddd em <span class="destaqueOrientacaoInicial">Exportar</span>.</p>

<form:form method="post" action="exportarDispositivosMoveis" id="dispositivosMoveisForm" name="dispositivosMoveisForm">
	<input name="acao" type="hidden" id="acao" value="exportarDispositivosMoveis"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${exportacaoDadosVO.chavePrimaria}"/>
	<input name="isReexportar" type="hidden" id="isReexportar" value="false"/>
	
	<fieldset id="conteinerExportarDadosDispositivosMoveis" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="consulta" class="conteinerBloco">
			<jsp:include page="/jsp/medicao/leitura/exportarimportardadosdispositivosmoveis/dispositosMoveisParametrosExecucaoConsulta.jsp" />	
			
			<hr class="linhaSeparadora1" />
			
			<label class="rotulo campoObrigatorio" id="rotuloSeparador" for="separador"><span class="campoObrigatorioSimbolo">* </span>Separador:</label>
			<input class="campoTexto campoHorizontal" style="text-align: center" type="text" id="separador" name="separador" maxlength="1" size="1" value=";">
			<label for="rotuloSeparador" class="rotuloInformativo2Linhas rotuloHorizontal" style="width: 250px; margin-top: -4px; margin-left: 5px">OBS: o separador padr�o para o formato CSV � o ponto e v�rgula( ; ).</label>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonAlterar" class="bottonRightCol2" value="Alterar" type="button" onclick="alterar();">
		<fieldset class="conteinerBotoesDirFixo">
			<input name="buttonReexportar" class="bottonRightCol2" value="Reexportar" type="button" onclick="reexportar();">
			<input name="buttonExportar" class="bottonRightCol2 bottonRightColUltimo" value="Exportar" type="button" onclick="exportar();">
		</fieldset>
	</fieldset>
	
	<iframe id="download" src ="" width="0" height="0"></iframe>
	
	<iframe id="download2" src ="" width="0" height="0"></iframe>
	<token:token></token:token>
</form:form>
  