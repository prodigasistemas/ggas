<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

	function selecionarFuncionario(idSelecionado){
		var idFuncionario = document.getElementById("idFuncionario");
		var nomeCompletoFuncionario = document.getElementById("nomeCompletoFuncionario");
		var empresaFuncionario = document.getElementById("empresaFuncionario");
		var matriculaFuncionario = document.getElementById("matriculaFuncionario");
		
		if(idSelecionado != '') {				
			AjaxService.obterFuncionarioPorChave( idSelecionado, {
	           	callback: function(funcionario) {	           		
	           		if(funcionario != null){  	           			        		      		         		
		               	idFuncionario.value = funcionario["chavePrimaria"];
		               	matriculaFuncionario.value = funcionario["matricula"];
		               	nomeCompletoFuncionario.value = funcionario["nome"];
		               	empresaFuncionario.value = funcionario["nomeEmpresa"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idFuncionario.value = "";
        	nomeCompletoFuncionario.value = "";
        	matriculaFuncionario.value = "";
        	empresaFuncionario.value = "";
       	}
        
        document.getElementById("nomeFuncionarioTexto").value = nomeCompletoFuncionario.value;
        document.getElementById("matriculaFuncionarioTexto").value = matriculaFuncionario.value;
        document.getElementById("empresaFuncionarioTexto").value = empresaFuncionario.value;
	}
	
	function exibirPopupPesquisaLeiturista() {
		popup = window.open('exibirPesquisaFuncionarioPopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function limparFormulario(){
		document.getElementById('idFuncionario').value = "";
        document.getElementById('nomeCompletoFuncionario').value = "";
        document.getElementById('empresaFuncionario').value = "";
        document.getElementById('matriculaFuncionario').value = "";
        
        document.getElementById('nomeFuncionarioTexto').value = "";
        document.getElementById('matriculaFuncionarioTexto').value = "";
        document.getElementById('empresaFuncionarioTexto').value = "";
	}

	function cancelar(){	
		location.href = '<c:url value="/exibirPesquisaLeiturista"/>';	
	}

</script>


<h1 class="tituloInterno">Incluir Leiturista<a class="linkHelp" href="<help:help>/leituristainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form:form method="post" action="incluirLeiturista">
<input name="acao" type="hidden" id="acao" value="incluirLeiturista"/>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${leiturista.chavePrimaria}"/>
<input name="postBack" type="hidden" id="postBack" value="true">

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset class="colunaEsq pesquisarFuncionario">
		<legend><span class="campoObrigatorioSimboloTabs">* </span>Pesquisar Funcion�rio</legend>
		<div class="conteinerDados">
			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Funcion�rio</span> para selecionar um Funcion�rio.</p>
			<input name="funcionario" type="hidden" id="idFuncionario" value="${funcionario.chavePrimaria}">
			<input name="nomeCompletoFuncionario" type="hidden" id="nomeCompletoFuncionario" value="${funcionario.nome}">
			<input name="empresaFuncionario" type="hidden" id="empresaFuncionario" value="${funcionario.empresa.cliente.nome}">
			<input name="matriculaFuncionario" type="hidden" id="matriculaFuncionario" value="${funcionario.matricula}">
			<input name="Button" id="botaoPesquisarFuncionario" class="bottonRightCol" title="Pesquisar Funcionario"  value="Pesquisar Funcionario" onclick="exibirPopupPesquisaLeiturista();" type="button"><br >
			
			<label class="rotulo" id="rotuloCliente" for="nomeFuncionarioTexto">Funcion�rio:</label>
			<input class="campoDesabilitado" type="text" id="nomeFuncionarioTexto" name="nomeFuncionarioTexto"  maxlength="50" size="48" disabled="disabled" value="${funcionario.nome}"><br />
			<label class="rotulo" id="rotuloCnpjTexto" for="matriculaFuncionarioTexto">Matr�cula:</label>
			<input class="campoDesabilitado" type="text" id="matriculaFuncionarioTexto" name="matriculaFuncionarioTexto"  maxlength="18" size="18" disabled="disabled" value="${funcionario.matricula}"><br />
			<label class="rotulo" id="rotuloEmailClienteTexto" for="empresaFuncionarioTexto">Empresa:</label>
			<input class="campoDesabilitado" type="text" id="empresaFuncionarioTexto" name="empresaFuncionarioTexto"  maxlength="80" size="40" disabled="disabled" value="${funcionario.empresa.cliente.nome}"><br />	
		</div>
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campo obrigat�rio para inclus�o de um Leiturista.</p>
	</fieldset>
	
</fieldset>
<fieldset class="conteinerBotoes"> 
	<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="incluirLeiturista">
    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" value="Incluir"  type="submit">
    </vacess:vacess>
 </fieldset>
</form:form> 