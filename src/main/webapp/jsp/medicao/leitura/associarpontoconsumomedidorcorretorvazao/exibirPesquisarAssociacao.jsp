<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Associa��o<a class="linkHelp" href="<help:help>/associaesdopontodeconsumoemedidorcorretordevazopesquisadospontosdeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar associa��es entre Pontos de Consumo e Medidor / Corretor de Vaz�o selecione um Cliente, Im�vel ou preencha os campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>. Para criar uma nova associa��o selecione pelo menos um Cliente ou Im�vel e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.</p>

<script type="text/javascript">

	$(document).ready(function(){
		verificarIndicadoresPesquisa();
		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		$("#indicadorPesquisaMedidor").click(habilitaMedidor);

		function atualizarInputsMedidor() {
			var desabilitar = isAssociacaoEmLote();
			$('#numeroMedidor, #numeroCorretorVazao').prop('disabled', desabilitar);
			if (desabilitar) $('#numeroMedidor, #numeroCorretorVazao').val('');
		}

		atualizarInputsMedidor();

		$("input:radio[name=indicadorAssociacaoEmLote]").change(function() {
			atualizarInputsMedidor();
		});

		/*-- IN�CIO: Comportamento dos radiobuttons (Medidor e Corretor de Vaz�o),
		select (A��o) e bot�o (OK) na parte inferior da tela */
		//Destaca o Ponto de Consumo e Habilita os radiobuttons (Medidor e Corretor de Vaz�o)
		$("table#pontoConsumo input:radio").click(function(){
			$("table#pontoConsumo tr").removeClass("selectedRow");
			$(this).closest("tr").addClass("selectedRow");
			$("#conteinerInferiorPesquisarAssociacao input:radio").removeAttr("disabled");
			habilitaOK();
		});
		$("table#medidor input:radio").click(function(){
			$("table#pontoConsumo tr").removeClass("selectedRow");
			$(this).closest("tr").addClass("selectedRow");
			$("#conteinerInferiorPesquisarAssociacao input:radio").removeAttr("disabled");
			habilitaOKMedidorIndependente();

		});

		if($("table#pontoConsumo input:radio").is(":checked")){
			$("#conteinerInferiorPesquisarAssociacao input:radio").removeAttr("disabled");
			$("table#pontoConsumo input:radio:checked").closest("tr").addClass("selectedRow");
			habilitaOK();
		}

		//Habilita/Desabilita o bot�o OK ao selecionar uma a��o com o mouse
		$("#operacaoMedidor").change(function(){
			habilitaOK();
		});

		//Habilita/Desabilita o bot�o OK ao selecionar uma a��o pelo teclado
		$("#operacaoMedidor").keyup(function(event){
			habilitaOK();

		});

		//Exibe a tela de associa��o correspondente ao pressionar a tecla ENTER no campo select (A��o)
		$("#operacaoMedidor").keypress(function(event){
			if (event.keyCode == '13' && $("#operacaoMedidor").val() != -1){
				exibirAssociar();
			}
		});

		//Habilita/Desabilita o bot�o OK ao selecionar uma a��o com o mouse
		$("#operacaoMedidorIndependente").change(function(){
			habilitaOKMedidorIndependente();
		});

		//Habilita/Desabilita o bot�o OK ao selecionar uma a��o pelo teclado
		$("#operacaoMedidorIndependente").keyup(function(event){
			habilitaOKMedidorIndependente();

		});

		//Exibe a tela de associa��o correspondente ao pressionar a tecla ENTER no campo select (A��o)
		$("#operacaoMedidorIndependente").keypress(function(event){
			if (event.keyCode == '13' && $("#operacaoMedidor").val() != -1){
				exibirAssociar();
			}
		});

	});

	function verificarIndicadoresPesquisa(){

		var indicador = ${associacaoVO.indicadorPesquisa ne 'indicadorPesquisaMedidor'};
		if(indicador){
				habilitaCliente();
				habilitaImovel();
				pesquisarMedidor(false);
				animatedcollapse.show('pesquisarMedidorClienteOuImovel');
				animatedcollapse.hide('pesquisarMedidor');
				document.getElementById('numeroMedidor').disabled = false;
				document.getElementById('numeroCorretorVazao').disabled = false;
		} else {
			pesquisarCliente(false);
			pesquisarImovel(false);
			habilitaMedidor();
			document.getElementById('numeroMedidor').disabled = true;
			document.getElementById('numeroCorretorVazao').disabled = true;
			animatedcollapse.show('pesquisarMedidor');
			animatedcollapse.hide('pesquisarMedidorClienteOuImovel');
		}
	}

	function habilitaOK(){
		var idReferencia = "";
		if (isAssociacaoEmLote()) {
			idReferencia = getCheckedValue(document.associacaoForm.idImovel);
		} else {
			idReferencia = getCheckedValue(document.associacaoForm.idPontoConsumo);
		}
		var codigoAssociacao = getCheckedValue(document.associacaoForm.tipoAssociacao);
		if(codigoAssociacao == "" || idReferencia == ""){
			document.getElementById("operacaoMedidor").disabled = true;
			$("input[value=OK]").attr("disabled","disabled");
		}

		if($("#operacaoMedidor option:selected").val() != -1){
			$("input[value=OK]").removeAttr("disabled");
		} else {
			$("input[value=OK]").attr("disabled","disabled");
		}
	}

	function habilitaOKMedidorIndependente(){

		var medidor = getCheckedValue(document.associacaoForm.idMedidor);
		var codigoAssociacao = getCheckedValue(document.associacaoForm.tipoAssociacao);

		if(codigoAssociacao == "" || medidor ==""){
			document.getElementById("operacaoMedidorIndependente").disabled = true;
			$("#OKMedidorIndependente").attr("disabled","disabled");
		}

		if($("#operacaoMedidorIndependente option:selected").val() != -1){
			$("#OKMedidorIndependente").removeAttr("disabled");
		} else {
			$("#OKMedidorIndependente").attr("disabled","disabled");
		}

	}

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloNumeroSerie,#rotuloTipo,#rotuloModelo,#rotuloMarca").addClass("rotuloDesabilitado")
		$("#rotuloNumeroMedidor,#rotuloNumeroCorretor").removeClass("rotuloDesabilitado")
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")

	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloNumeroSerie,#rotuloTipo,#rotuloMarca,#rotuloModelo").addClass("rotuloDesabilitado")
		$("#rotuloNumeroMedidor,#rotuloNumeroCorretor").removeClass("rotuloDesabilitado")
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};
	function habilitaMedidor(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
		$("#rotuloNumeroMedidor,#rotuloNumeroCorretor").addClass("rotuloDesabilitado")
		$("#rotuloNumeroSerie,#rotuloTipo,#rotuloMarca,#rotuloModelo").removeClass("rotuloDesabilitado")
	};

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			pesquisarMedidor(false);
			document.getElementById('numeroMedidor').disabled = false;
			document.getElementById('numeroCorretorVazao').disabled = false;
			limparCamposPesquisa();
		}else if(selecao == "indicadorPesquisaCliente"){
			pesquisarCliente(true);
			pesquisarImovel(false);
			pesquisarMedidor(false);
			document.getElementById('numeroMedidor').disabled = false;
			document.getElementById('numeroCorretorVazao').disabled = false;
			limparCamposPesquisa();
		}else{
			pesquisarMedidor(true);
			pesquisarImovel(false);
			pesquisarCliente(false);
			document.getElementById('numeroMedidor').disabled = true;
			document.getElementById('numeroCorretorVazao').disabled = true;
			limparCamposPesquisa();

		}
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");

		if(idSelecionado != '') {
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {
	           		if(imovel != null){
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}

	        );
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}

		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}
	function setarStatus(descricao, medidor, corretorVazao){
		document.getElementById('status').value = descricao;
		document.getElementById('medidor').value = medidor;
		document.getElementById('corretorVazao').value = corretorVazao;
		obterOperacoes();
	}
	function setarStatusMedidorIndependente(idMedidor, descricao, medidor, corretorVazao){
		document.getElementById('idMedidorIndependente').value = idMedidor;
		document.getElementById('statusMedidorIndependente').value = descricao;
		document.getElementById('medidor').value = medidor;
		document.getElementById('corretorVazao').value = corretorVazao;
		obterOperacoesMedidorIndependente("");
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}

	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	function pesquisarMedidor(valor){
		document.getElementById('numeroSerie').disabled = !valor;
		document.getElementById('marca').disabled = !valor;
		document.getElementById('modelo').disabled = !valor;
		document.getElementById('tipo').disabled = !valor;
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function limparCamposPesquisa(){
		limparFormularioDadosCliente();
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";

		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

	}

	function limparFormulario(){

    	limparCamposPesquisa();

    	limparFormularios(document.associacaoForm);

       	pesquisarCliente(false);
		pesquisarImovel(false);
		pesquisarMedidor(false);

	}

	function exibirAssociarLote(){
		submeter("associacaoForm", "exibirAssociacaoLote");
	}
	function exibirAssociar(){
		submeter("associacaoForm", "exibirAssociacao");
	}
	function exibirAssociarIndependente(){

		$("#numeroSerie").val("");

		submeter("associacaoForm", "exibirAssociacaoIndependente");

	}

	function isAssociacaoEmLote() {
		var opcaoEscolhida = $( 'input[name=indicadorAssociacaoEmLote]:checked' ).val();
		return opcaoEscolhida == 'true';
	}

	function obterOperacoesLote() {
		var codigoAssociacao = getCheckedValue(document.associacaoForm.tipoAssociacao);
		var idImovel = $( 'input[name=idImovel]:checked' ).val();
		if (idImovel > 0 && $('input[name=tipoAssociacao]').prop('disabled')) $('input[name=tipoAssociacao]').prop('disabled', false);
		var valorSelecionado = '<c:out value="${associacaoVO.idOperacaoMedidor}"/>';

		if(codigoAssociacao != ""){
			var select = document.associacaoForm.idOperacaoMedidor;
			select.disabled = false;
			select.length = 0;
			select.options[select.length] = new Option('Selecione', '-1');

			AjaxService.obterAcoesAssociacaoLote(codigoAssociacao, idImovel, {
				callback: function(operacoes) {
					console.log(JSON.stringify(operacoes));
					for(key in operacoes){
						var novaOpcao = new Option(operacoes[key], key);
						if(valorSelecionado == key){
							novaOpcao.selected = true;
						}
						select.options[select.length] = novaOpcao;
						habilitaOK();
					}
				}
				, async:false}
			);
		}
	}

	function obterOperacoes(){
		var codigoAssociacao = getCheckedValue(document.associacaoForm.tipoAssociacao);
		var select = document.associacaoForm.idOperacaoMedidor;
		var status = document.getElementById('status').value;
		var medidor = document.getElementById('medidor').value;
		var corretorVazao = document.getElementById('corretorVazao').value;
		var valorSelecionado = '<c:out value="${associacaoVO.idOperacaoMedidor}"/>';
		$('input[name=tipoAssociacao]').prop('disabled', false);

		if(codigoAssociacao != ""){
			select.disabled = false;
			select.length = 0;
			select.options[select.length] = new Option('Selecione', '-1');

			AjaxService.obterAcoesAssociacao(codigoAssociacao, status, medidor, corretorVazao, false, {
				callback: function(operacoes) {
					for(key in operacoes){
						var novaOpcao = new Option(operacoes[key], key);
						if(valorSelecionado == key){
							novaOpcao.selected = true;
						}
						select.options[select.length] = novaOpcao;
						habilitaOK();
					}
				}
				, async:false}
			);
		}
	}

	function obterOperacoesMedidorIndependente(codigoTipoAssociacao){
		var select = document.associacaoForm.idOperacaoMedidorIndependente;
		var status = document.getElementById('statusMedidorIndependente').value;
		var medidor = document.getElementById('medidor').value;
		var corretorVazao = document.getElementById('corretorVazao').value;
		var isAssociacaoMedidorIndependente = true;
		var valorSelecionado = '<c:out value="${associacaoVO.idOperacaoMedidorIndependente}"/>';

		if(codigoTipoAssociacao != ""){
			select.disabled = false;
			select.length = 0;
			select.options[select.length] = new Option('Selecione', '-1');

			AjaxService.obterAcoesAssociacao(codigoTipoAssociacao, status, medidor, corretorVazao, isAssociacaoMedidorIndependente, {
				callback: function(operacoes) {
					for(key in operacoes){
						var novaOpcao = new Option(operacoes[key], key);
						if(valorSelecionado == key){
							novaOpcao.selected = true;
						}
						select.options[select.length] = novaOpcao;
						habilitaOKMedidorIndependente();
					}
				}
				, async:false}
			);
		}else{
			select.disabled = true;
			select.length = 0;
			select.options[select.length] = new Option('Selecione', '-1');

			$("#tipoAssociacaoCorretor").prop('checked', false);
			$("#tipoAssociacaoMedidor").prop('checked', false);
		}
	}

	function init() {

		<c:choose>
			<c:when test="${associacaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				pesquisarMedidor(false);
				habilitaCliente();
				document.getElementById('numeroMedidor').disabled = false;
				document.getElementById('numeroCorretorVazao').disabled = false;
				animatedcollapse.show('pesquisarMedidorClienteOuImovel');
				animatedcollapse.hide('pesquisarMedidor');
			</c:when>
			<c:when test="${associacaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				pesquisarMedidor(false);
				habilitaImovel();
				document.getElementById('numeroMedidor').disabled = false;
				document.getElementById('numeroCorretorVazao').disabled = false;
				animatedcollapse.show('pesquisarMedidorClienteOuImovel');
				animatedcollapse.hide('pesquisarMedidor');
			</c:when>
			<c:when test="${associacaoVO.indicadorPesquisa eq 'indicadorPesquisaMedidor'}">
				pesquisarCliente(false);
				pesquisarImovel(false);
				habilitaMedidor();
				document.getElementById('numeroMedidor').disabled = true;
				document.getElementById('numeroCorretorVazao').disabled = true;
				animatedcollapse.show('pesquisarMedidor');
				animatedcollapse.hide('pesquisarMedidorClienteOuImovel');
			</c:when>
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
				pesquisarMedidor(false);
				animatedcollapse.show('pesquisarMedidorClienteOuImovel');
			</c:otherwise>
		</c:choose>

		var mensagensEstado = $(".mensagens").css("display");
		if(mensagensEstado == "none"){
			<c:if test="${listaPontoConsumoAssociacao ne null}">
				$.scrollTo($("#pontoConsumo"),800);
			</c:if>
		}

		if(document.associacaoForm.tipoAssociacao != undefined){
			document.associacaoForm.tipoAssociacao[0].checked = false;
	    	document.associacaoForm.tipoAssociacao[1].checked = false;
		}

		if(document.getElementById('checkPontoConsumo') != null){
			document.getElementById('checkPontoConsumo').checked = false;
		}

    	$("table#pontoConsumo input:radio:checked").closest("tr").addClass("selectedRow");

    	$("input[value=OK]").attr("disabled","disabled");
		// hhabilitaOK();

	}
	animatedcollapse.addDiv('pesquisarMedidorClienteOuImovel', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('pesquisarMedidor', 'fade=0,speed=400,persist=1,hide=0');
	addLoadEvent(init);

</script>


<form:form method="post" action="pesquisarAssociacao" id="associacaoForm" name="associacaoForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarAssociacao"/>
	<input name="idContrato" type="hidden" id="idContrato"/>
	<input type="hidden" name="medidor" id="medidor" value="${associacaoVO.medidor}"/>
	<input type="hidden" name="status"  id="status" value="${associacaoVO.status}"/>
	<input type="hidden" name="statusMedidorIndependente"  id="statusMedidorIndependente" value=""/>
	<input type="hidden" name="corretorVazao"  id="corretorVazao" value="${associacaoVO.corretorVazao}"/>
	<input type="hidden" name="idMedidorIndependente"  id="idMedidorIndependente" />


	<fieldset id="pesquisarAssociacaoPontoConsumoMedidorCorretor" class="conteinerPesquisarIncluirAlterar">
		<a class="linkPesquisaAvancada" href="#" id="pesquisarMedidorClienteOuImovelLink" rel="toggle[pesquisarMedidorClienteOuImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Filtro associa��o Ponto de Consumo / Medidor / Corretor de Vaz�o<img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="pesquisarMedidorClienteOuImovel">
			<fieldset id="pesquisarCliente" class="colunaEsq">
				<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${associacaoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
				<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
					<jsp:param name="idCampoIdCliente" value="idCliente"/>
					<jsp:param name="idCliente" value="${cliente.chavePrimaria}"/>
					<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
					<jsp:param name="nomeCliente" value="${cliente.nome}"/>
					<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
					<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
					<jsp:param name="idCampoEmail" value="emailCliente"/>
					<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
					<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
					<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>
					<jsp:param name="dadosClienteObrigatorios" value="false"/>
					<jsp:param name="possuiRadio" value="true"/>
				</jsp:include>
			</fieldset>

			<fieldset id="pesquisarImovel" class="colunaDir3">
				<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${associacaoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
				<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
				<div class="pesquisarImovelFundo">
					<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
					<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}">
					<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nome}">
					<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.chavePrimaria}">
					<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
					<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.quadraFace.endereco.cep.municipio.descricao}">
					<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">

					<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
					<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
					<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
					<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
					<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
					<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
					<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
					<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
					<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.municipio.descricao}"><br />
					<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
				</div>
			</fieldset>
			<fieldset class="conteinerBloco">
				<fieldset class="associacaoCamposLivres">
					<label class="rotulo" id="rotuloindicadorAssociacaoEmLote" >Associa��o de medidor/corretor de vaz�o em lote:</label>
					<input class="campoRadio" type="radio" id="indicadorAssociacaoEmLoteSim" name="indicadorAssociacaoEmLote" value="true" <c:if test="${associacaoVO.indicadorAssociacaoEmLote == 'true'}">checked="checked"</c:if>><label class="rotuloRadio">Sim</label>
					<input class="campoRadio" type="radio" id="indicadorAssociacaoEmLoteNao" name="indicadorAssociacaoEmLote" value="false" <c:if test="${associacaoVO.indicadorAssociacaoEmLote != 'true'}">checked="checked"</c:if>><label class="rotuloRadio">N�o</label>
					<label class="rotulo" id="rotuloNumeroMedidor" for="numeroMedidor">N� do medidor associado ao Ponto de Consumo:</label>
					<input class="campoTexto" type="text" id="numeroMedidor" name="numeroMedidor" maxlength="20" size="20" value="${associacaoVO.numeroMedidor}" onkeyup="letraMaiuscula(this);" >
					<br />
					<label class="rotulo" id="rotuloNumeroCorretor" for="numeroMedidor">N� do corretor de vaz�o associado ao Ponto de Consumo:</label>
					<input class="campoTexto" type="text" id="numeroCorretorVazao" name="numeroCorretorVazao" maxlength="20" size="20" value="${associacaoVO.numeroCorretorVazao}" onkeyup="letraMaiuscula(this);">
				</fieldset>
			</fieldset>
		</fieldset>
		<br />
		<br />
		<fieldset class="conteinerBloco">
			<a class="linkPesquisaAvancada" href="#" rel="toggle[pesquisarMedidor]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Filtro associa��o Medidor Independente / Corretor de Vaz�o <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="pesquisarMedidor">
				<input class="campoRadio" type="radio" value="indicadorPesquisaMedidor" id="indicadorPesquisaMedidor" name="indicadorPesquisa" <c:if test="${associacaoVO.indicadorPesquisa eq 'indicadorPesquisaMedidor'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
				<legend class="legendIndicadorPesquisa">Pesquisar Medidor Independente</legend>
				<div class="pesquisarImovelFundo">
					<p class="orientacaoInterna">Filtros utilizados para pesquisar <span class="destaqueOrientacaoInicial">Medidores Independentes</span> que compoem o <span class="destaqueOrientacaoInicial">Medidor Virtual</span>.</p>
					<label class="rotulo" id="rotuloNumeroSerie" for="numeroMedidor">N�mero de s�rie:</label>
					<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${associacaoVO.numeroSerie}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/><br />

					<label class="rotulo" id="rotuloTipo" for="tipo">Tipo:</label>
					<select name="idTipo" id="tipo" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoMedidor}" var="tipo">
							<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${associacaoVO.idTipo == tipo.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${tipo.descricao}"/>
							</option>
					    </c:forEach>
					</select>
					<label class="rotulo" id="rotuloMarca" for="marca">Marca:</label>
					<select name="idMarca" id="marca" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaMarcaMedidor}" var="marca">
							<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${associacaoVO.idMarca == marca.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${marca.descricao}"/>
							</option>
					    </c:forEach>
					</select><br />

					<label class="rotulo" id="rotuloModelo" for="modelo">Modelo:</label>
					<select name="idModelo" id="modelo" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaModeloMedidor}" var="modelo">
							<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${associacaoVO.idModelo == modelo.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${modelo.descricao}"/>
							</option>
					    </c:forEach>
					</select><br />
				</div>
			</fieldset>

			</fieldset>
			<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDirFixo">
				<vacess:vacess param="pesquisarAssociacao">
	    			<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    		</vacess:vacess>
				<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
			</fieldset>
		</fieldset>

		<c:if test="${listaPontoConsumoAssociacao ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<fieldset class="conteinerBloco">
			<p class="orientacaoInicial">Para associar um Corretor de Vaz�o a um Ponto de Consumo com Medidor, selecione alguma associa��o da listagem abaixo e clique em <span class="destaqueOrientacaoInicial">Associar</span>.</p>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumoAssociacao" id="pontoConsumo" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAssociacao">
		    	<display:column  style="width: 25px">
		    		<input type="radio" name="idPontoConsumo" id="checkPontoConsumo" value="<c:out value="${pontoConsumo.chavePrimaria}"/>"  onclick="javascript:setarStatus('<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>', '<c:out value="${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}"/>', '<c:out value="${pontoConsumo.instalacaoMedidor.vazaoCorretor.numeroSerie}"/>')"/>
		    	</display:column>

		    	<display:column sortable="true" title="Im�vel" sortProperty="imovel.nome" property="imovel.nome" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px" />

		    	<display:column sortable="true" title="Ponto de Consumo" sortProperty="descricao" property="descricao" headerClass="tituloTabelaEsq" style="width: 230px; text-align: left; padding-left: 10px" />

				<display:column sortable="true" title="Status" sortProperty="situacaoConsumo.descricao" property="situacaoConsumo.descricao" style="width: 160px" />

		    	<display:column sortable="true" title="Medidor" sortProperty="medidor.numeroSerie" property="instalacaoMedidor.medidor.numeroSerie" style="width: 120px" />

		    	<display:column sortable="true" title="Corretor" sortProperty="vazaoCorretor.numeroSerie" property="instalacaoMedidor.vazaoCorretor.numeroSerie" style="width: 120px" />
			</display:table>

			<fieldset id="conteinerInferiorPesquisarAssociacao" class="colunaDir">
				<c:forEach items="${listaTipoAssociacao}" var="tipoAssociacao">
					<input class="campoRadio" type="radio" id="tipoAssociacao<c:out value="${tipoAssociacao.key}"/>" name="tipoAssociacao" value="<c:out value="${tipoAssociacao.key}"/>" <c:if test="${associacaoVO.tipoAssociacao eq tipoAssociacao.key}">checked="checked"</c:if> onclick="obterOperacoes();" disabled="disabled" />
					<label class="rotuloRadio" for="tipoAssociacao<c:out value="${tipoAssociacao.key}"/>"><c:out value="${tipoAssociacao.value}"/></label>
		    	</c:forEach>

				<label class="rotulo rotuloHorizontal campoObrigatorio" id="rotuloOperacaoMedidor" for="habilitado"><span class="campoObrigatorioSimbolo">* </span>A��o:</label>
				<select class="campoSelect campoHorizontal" id="operacaoMedidor" name="idOperacaoMedidor" disabled="disabled" style="width: 130px">
					<option value="-1">Selecione</option>
				</select>

				<input id="botao-OK" type="button" class="bottonRightCol2 bottonRightColUltimo" value="OK" onclick="exibirAssociar();" disabled="disabled">
			</fieldset>
		</fieldset>
		</c:if>
	<c:if test="${listaImovelAssociacao ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<fieldset class="conteinerBloco">
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaImovelAssociacao" id="imovel" partialList="true" sort="external" pagesize="15"
						   size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAssociacao">
				<display:column  style="width: 25px">
					<input type="radio" name="idImovel" id="checkImovel" value="<c:out value="${imovel.chavePrimaria}"/>"  onclick="javascript:obterOperacoesLote()"/>
				</display:column>

				<display:column sortable="true" title="Im�vel" sortProperty="nome" property="nome" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px" />

			</display:table>

			<fieldset id="conteinerInferiorPesquisarAssociacao" class="colunaDir">
				<c:forEach items="${listaTipoAssociacao}" var="tipoAssociacao">
					<input class="campoRadio" type="radio" id="tipoAssociacao<c:out value="${tipoAssociacao.key}"/>" name="tipoAssociacao"
						   value="<c:out value="${tipoAssociacao.key}"/>" <c:if test="${associacaoVO.tipoAssociacao eq tipoAssociacao.key}">checked="checked"</c:if> onclick="obterOperacoesLote();" disabled="disabled" />
					<label class="rotuloRadio" for="tipoAssociacao<c:out value="${tipoAssociacao.key}"/>"><c:out value="${tipoAssociacao.value}"/></label>
				</c:forEach>

				<label class="rotulo rotuloHorizontal campoObrigatorio" id="rotuloOperacaoMedidor" for="habilitado"><span class="campoObrigatorioSimbolo">* </span>A��o:</label>
				<select class="campoSelect campoHorizontal" id="operacaoMedidor" name="idOperacaoMedidor" disabled="disabled" style="width: 130px">
					<option value="-1">Selecione</option>
				</select>

				<input id="botao-OK" type="button" class="bottonRightCol2 bottonRightColUltimo" value="OK" onclick="exibirAssociarLote();" disabled="disabled">
			</fieldset>
		</fieldset>
	</c:if>
		<c:if test="${listaMedidorIndependente ne null}">
			<fieldset class="conteinerBloco">
				<display:table class="dataTableGGAS" name="listaMedidorIndependente" sort="list" id="medidor" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAssociacao">
					<display:column style="width: 25px" sortable="false" title="">
			         	<input type="radio" name="idMedidor" id="idMedidor" value="${medidor.chavePrimaria}"
			         			onclick="javascript:setarStatusMedidorIndependente('<c:out value="${medidor.chavePrimaria}"/>',
			         			'<c:out value="${medidor.situacaoAssociacaoMedidorIndependente.descricao}"/>',
			         			'<c:out value="${medidor.numeroSerie}"/>',
			         			'<c:out value="${medidor.instalacaoMedidor.vazaoCorretor.numeroSerie}"/>')"/>
			        </display:column>
			        <display:column sortable="true" title="Status" sortProperty="situacaoAssociacaoMedidorIndependente.descricao" property="situacaoAssociacaoMedidorIndependente.descricao" style="width: 200px"  />
			        <display:column sortable="true" title="Medidor" sortProperty="numeroSerie" property="numeroSerie" style="width: 200px"  />
			        <display:column sortable="true" title="Corretor de Vaz�o" sortProperty="instalacaoMedidor.vazaoCorretor.numeroSerie" property="instalacaoMedidor.vazaoCorretor.numeroSerie" style="width: 200px" />
			  	</display:table>
		 	</fieldset>
		 	<fieldset id="conteinerInferiorPesquisarAssociacao" class="colunaDir">

				<input class="campoRadio" type="radio" id="tipoAssociacaoMedidor" name="tipoAssociacao" value="1"
						onclick="obterOperacoesMedidorIndependente('1');" disabled="disabled" />
				<label class="rotuloRadio" for="tipoAssociacaoMedidor" value="Medidor" >Medidor</label>

				<input class="campoRadio" type="radio" id="tipoAssociacaoCorretor" name="tipoAssociacao" value="2"
						onclick="obterOperacoesMedidorIndependente('2');" disabled="disabled" />
				<label class="rotuloRadio" for="tipoAssociacaoCorretor" value="Corretor" >Corretor</label>

				<label class="rotulo rotuloHorizontal campoObrigatorio" id="rotuloOperacaoMedidor" for="habilitado"><span class="campoObrigatorioSimbolo">* </span>A��o:</label>
				<select class="campoSelect campoHorizontal" id="operacaoMedidorIndependente" name="idOperacaoMedidorIndependente" disabled="disabled" style="width: 130px">
					<option value="-1">Selecione</option>
				</select>

				<input type="button" id="OKMedidorIndependente" class="bottonRightCol2 bottonRightColUltimo" value="OK " onclick="exibirAssociarIndependente();" disabled="disabled">
			</fieldset>
		</c:if>
	</fieldset>

</form:form>
