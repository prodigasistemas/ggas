<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript" src="/ggas_mantenedor/js/utils.js"></script>

<h1 class="tituloInterno">
	<c:out value="${tituloAssociacao}"/>
	<c:if test="${dados eq 'dadosInstalacao'}">
		<a class="linkHelp" href="<help:help>/instalaodosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosSubstituicao'}">
		<a class="linkHelp" href="<help:help>/substituiodosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>					
	</c:if>
	<c:if test="${dados eq 'dadosAtivacao'}">
		<a class="linkHelp" href="<help:help>/ativaodosmedidores.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosRetirada'}">
		<a class="linkHelp" href="<help:help>/retiradadosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosBloqueio'}">
		<a class="linkHelp" href="<help:help>/retiradadosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosReativacao'}">
		<a class="linkHelp" href="<help:help>/retiradadosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>			
</h1>
<p class="orientacaoInicial">Para completar a associa��o, preencha os campo abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.</p>

<script type="text/javascript">
	var isViradaMedidor = false;
	var isComparacao=false;

	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,group=clientes,persist=1,hide=0');

	$(document).ready(function(){
						
		<c:choose>
			<c:when test="${not empty dataInicioInstalacao and not empty dataLimiteInstalacao}">
				$('#dataMedidor').datepicker({changeYear: true, dateFormat: 'dd/mm/yy', minDate: new Date(${dataInicioInstalacao}) , maxDate: '+0d' , showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio'});		
				$('#dataCorretorVazao').datepicker({changeYear: true, dateFormat: 'dd/mm/yy', minDate: new Date(${dataInicioInstalacao}) , maxDate: '+0d' , showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio'});		
			</c:when>
			<c:otherwise>
				$("#dataMedidor").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
				$("#dataMedidor").datepicker('option', 'maxDate', '+0d');
				$("#dataCorretorVazao").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
				$("#dataCorretorVazao").datepicker('option', 'maxDate', '+0d');
			</c:otherwise>
		</c:choose>
		
		if(document.associacaoForm.horaCorretorVazao != undefined){
			$("#horaCorretorVazao").inputmask("99:99");
		}	
		
		var indicadorAtualizacaoCadastral = "${sessionScope.indicadorAtualizacaoCadastral}";	
		if(indicadorAtualizacaoCadastral != ''){
			$('#botaoCancelar').attr("disabled","disabled");		
		}
	});
	
	function salvarAssociacao(){ 
		if(validar()){
			exibirIndicador();
			submeter("associacaoForm", "salvarAssociacao"); 
		}
	}

	function salvarAssociacaoMedidorIndependente(){
		submeter("associacaoForm", "salvarAssociacaoMedidorIndependente");
	}
	
	function cancelar(){
		submeter("associacaoForm", "pesquisarAssociacao");
	}		
	
	function cancelarMedidorIndependente(){
		$("#medidor").val("");
		$("#idMedidorIndependente").val("");
		$("#numeroSerieMedidorAtual").val("");
		
		submeter("associacaoForm", "pesquisarMedidorAssociacao");
	}
		
</script>

<form:form action="salvarAssociacao" id="associacaoForm" name="associacaoForm">
		
	<input type="hidden" name="indicadorPesquisa" id="indicadorPesquisa" value="${associacaoVO.indicadorPesquisa}">
	<input type="hidden" name="acao" value="salvarAssociacao"/>
	<input type="hidden" name="idPontoConsumo" value="${associacaoVO.idPontoConsumo}"/>
	<input type="hidden" name="idOperacaoMedidor" value="${associacaoVO.idOperacaoMedidor}"/>
	<input type="hidden" name="idOperacaoMedidorIndependente" value="${associacaoVO.idOperacaoMedidorIndependente}"/>
	<input type="hidden" name="tipoAssociacao" value="${associacaoVO.tipoAssociacao}"/>
	<input type="hidden" name="status" id="status" value="${associacaoVO.status}"/>
	<input type="hidden"  id="medidor" name="medidor" value="${associacaoVO.medidor}"/>
	<input type="hidden" name="exibirDadosFiltro" value="true"/>
	<input type="hidden" name="isAssociacaoMedidorIndependente" value="<c:out value="${associacaoVO.isAssociacaoMedidorIndependente}" />"/>
	<input type="hidden" id="idMedidorIndependente" name="idMedidorIndependente" value="${associacaoVO.idMedidorIndependente}"/>
	<input type="hidden" name="numeroMedidor" value="${associacaoVO.numeroMedidor}">
	<input type="hidden" name="numeroCorretorVazao" value="${associacaoVO.numeroCorretorVazao}">
	<input type="hidden" name="idCliente" value="${associacaoVO.idCliente}"/>
	<input type="hidden" name="idImovel" value="${associacaoVO.idImovel}"/>

	<input type="hidden" id="resultadoPontoDeConsumo" value="${contratoPontoConsumo.faixaPressaoFornecimento.chavePrimaria}"/>
	<input type="hidden" id="contratoFormatado" value="${contratoFormatado}"/>
	<input type="hidden" id="isSalvarPressao" value="${isSalvarPressao}"/>
	<input type="hidden" id="pressaoFornecimentoCampo" value="${pressaoFornecimentoCampo.chavePrimaria}"/>

	<fieldset id="associacaoPontoConsumoMedidorCorretor" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<c:if test="${associacaoVO.isAssociacaoMedidorIndependente != true}">
				<a class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosPontoConsumo" class="conteinerDados conteinerBloco">
					
					<fieldset class="coluna detalhamentoColunaLarga">
						<label class="rotulo">Descri��o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargoNovo"><c:out value="${descricaoPontoConsumo}"/></span><br />
										
						<label class="rotulo">Press�o:</label>
						<span class="itemDetalhamento"><c:out value="${pressao}"/></span><br />
						
						<label class="rotulo">Fator Corre��o:</label>
						<span class="itemDetalhamento"><c:out value="${fatorCorrecao}"/></span>
					</fieldset>
					
					<fieldset class="colunaFinal">
						<label class="rotulo" id="rotuloCnpjTexto">Endere�o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${enderecoPontoConsumo}"/></span><br />
						
						<label class="rotulo" id="rotuloEnderecoTexto">Cep:</label>
						<span class="itemDetalhamento"><c:out value="${cepPontoConsumo}"/></span><br />
						
						<label class="rotulo" id="rotuloNumeroContrato">Complemento:</label>
						<span class="itemDetalhamento"><c:out value="${complementoPontoConsumo}"/></span>
					</fieldset>
				</fieldset>
			<hr class="linhaSeparadora1" />
			</c:if>
			
			<fieldset id="dadosMedidorCorretorVazao" class="conteinerBloco">
				<c:if test="${tipoAssociacaoPontoConsumoMedidor eq true}">
					<c:if test="${dados eq 'dadosInstalacao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosInstalacaoMedidor.jsp"/>
					</c:if>
					<c:if test="${dados eq 'dadosSubstituicao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosSubstituicaoMedidor.jsp"/>
					</c:if>
					<c:if test="${dados eq 'dadosAtivacao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosAtivacaoMedidor.jsp"/>
					</c:if>
					<c:if test="${dados eq 'dadosRetirada'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosRetiradaMedidor.jsp"/> 
					</c:if>
					<c:if test="${dados eq 'dadosBloqueio'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosBloqueioMedidor.jsp"/> 
					</c:if>
					<c:if test="${dados eq 'dadosReativacao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosBloqueioMedidor.jsp"/> 
					</c:if>	
					<c:if test="${dados eq 'dadosInstalacaoAtivacao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosInstalacaoAtivacaoMedidor.jsp"/>
					</c:if>
					<c:if test="${dados eq 'dadosBloqueioReativacao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosBloqueioReativacaoMedidor.jsp"/>
					</c:if>											
				</c:if>
			
				<c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
					<c:if test="${dados eq 'dadosInstalacao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosInstalacaoCorretorVazao.jsp"/>
					</c:if>
					<c:if test="${dados eq 'dadosSubstituicao'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosSubstituicaoCorretorVazao.jsp"/>
					</c:if>
					
					<c:if test="${dados eq 'dadosRetirada'}">
						<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosRetiradaCorretorVazao.jsp"/>
					</c:if>
									
				</c:if>			
			</fieldset>					
		
		</fieldset>
		
		<fieldset class="conteinerBotoes"> 		
			<c:if test="${associacaoVO.isAssociacaoMedidorIndependente != true}">
				<input name="botaoCancelar" id="botaoCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
	    		<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="button" onclick="salvarAssociacao()">
	    	</c:if>
	    	<c:if test="${associacaoVO.isAssociacaoMedidorIndependente == true}">
				<input name="botaoCancelar" id="botaoCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelarMedidorIndependente();">
	    		<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="button" onclick="salvarAssociacaoMedidorIndependente()">
	    	</c:if>
	 	</fieldset>	
	
	</fieldset>
	
</form:form>