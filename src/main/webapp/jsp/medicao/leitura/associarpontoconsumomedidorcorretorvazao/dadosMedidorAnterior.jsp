<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<legend>Medidor <c:out value="${param.complemento}"/></legend>
<div id="dadosMedidorAnterior" class="pesquisarClienteFundo">
	<input type="hidden" name="chaveMedidorAnterior" value="<c:out value="${chaveMedidorAnterior}"/>">
	<input type="hidden" name="numeroDigitosMedidorAnterior" value="<c:out value="${numeroDigitosMedidorAnterior}"/>" >	
		
	<label class="rotulo">N�mero de S�rie:</label> 
	<span class="itemDetalhamento"><c:out value="${numeroSerieMedidorAnterior}"/></span><br/>
	
	<label class="rotulo">D�gitos de Leitura:</label> 
	<span class="itemDetalhamento"><c:out value="${numeroDigitosMedidorAnterior}"/></span><br/>
	
	<label class="rotulo">Tipo:</label>
	<span class="itemDetalhamento"><c:out value="${tipoMedidorAnterior}"/></span><br/>
	
	<label class="rotulo">Modelo:</label>
	<span class="itemDetalhamento"><c:out value="${modeloMedidorAnterior}"/></span><br/>
		
	<label class="rotulo">Marca:</label>
	<span class="itemDetalhamento"><c:out value="${marcaMedidorAnterior}"/></span>
	
	<label class="rotulo">Perfil:</label>
	<span class="itemDetalhamento" id="modoUso"><c:out value="${modoUso}"/></span>
</div>