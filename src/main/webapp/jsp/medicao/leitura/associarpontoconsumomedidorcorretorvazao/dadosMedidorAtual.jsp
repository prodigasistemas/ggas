<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">

	$(document).ready(function(){
		obterMedidor($("#numeroSerieMedidorAtual").val());
	});

	function obterMedidor(numeroSerie){
		numeroSerie = trim(numeroSerie);
		if(numeroSerie != ""){
			document.associacaoForm.numeroSerieMedidorAtual.value = numeroSerie;
			
			AjaxService.obterMedidorPorNumeroSerie(numeroSerie, {
				callback: function(medidores) {
					if(medidores != null){
						if(medidores['2'] != undefined){
							gerarSelecaoMedidor(medidores);
						}else{
							for(key in medidores){
								var medidor = medidores[key];
								if(medidor["disponivel"] == 'true'){
									popularDadosMedidor(medidor);
								}else{
									alert('<fmt:message key="ERRO_MEDIDOR_NAO_DISPONIVEL_PARA_OPERACAO"/>');
									limparDados();
								}
							}
						}
					}else{
						alert('<fmt:message key="ERRO_MEDIDOR_NAO_ENCONTRADO_ASSOCIACAO_MEDIDOR"/>');
						limparDados();
					}
				}
				, async:false
			});

		}else{
			limparDadosMedidor();
		}
		configurarDigitosLeituraMedidor();
	}

	function exibirPopupPesquisaMedidor() {
		exibirIndicador();
		popup = window.open('exibirPesquisaMedidorPopup?postBack=true&medidor='+$('#chavePrimaria').val(),'popup','height=750,width=850,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function gerarSelecaoMedidor(medidores){
		exibirJDialog("#listaMedidores");

		var inner = '';
   		var param = '';
   		var div = 2;
   		var start = 2;
   		$('#itensListaMedidores').html('');
		for(key in medidores){
		
			if(start % div == 0){
				param = "odd";
            }else{
				param = "even";
	        }

			var chavePrimaria = medidores[key]["chavePrimaria"];
			var tipoMedidor = medidores[key]["tipo"] != undefined ? medidores[key]["tipo"] : '';
			var modelo = medidores[key]["modelo"] != undefined ? medidores[key]["modelo"] : '';
			var marca = medidores[key]["marca"] != undefined ? medidores[key]["marca"] : '';
			var modoUso = medidores[key]["modoUso"] != undefined ? medidores[key]["modoUso"] : '';
			var numeroDigitos = medidores[key]["numeroDigitos"];
			var disponivel = medidores[key]["disponivel"];

			inner = inner + '<tr class='+param+' style="cursor: pointer;" onclick="selecionarMedidor('+ chavePrimaria + ',' + "'" + tipoMedidor + "'" +  ',' + "'" + modelo + "'" + ',' + "'" + marca + "'" + ',' + numeroDigitos + ',' + disponivel + ',' + modoUso + ');">'

			inner = inner + '<td>' + document.associacaoForm.numeroSerieMedidorAtual.value + '</td>';
			inner = inner + '<td>' + tipoMedidor + '</td>';
			inner = inner + '<td>' + modelo + '</td>';
			inner = inner + '<td>' + marca + '</td>';
			inner = inner + '<td>' + modoUso + '</td>';

			inner = inner + '</tr>';
       		start = start + 1;
		}

		$("#itensListaMedidores").prepend(inner);
		

	}

	function selecionarMedidor(chavePrimaria, tipoMedidor, modelo, marca, numeroDigitos, disponivel, modoUso){
		if(disponivel == true){
			$("#listaMedidores").dialog('close');

			document.associacaoForm.chaveMedidorAtual.value = chavePrimaria;

			document.associacaoForm.tipoMedidorAtual.value = tipoMedidor;
	   		$("#tipoMedidorAtual").html(tipoMedidor);

	   		document.associacaoForm.modeloMedidorAtual.value = modelo;
	   		$("#modeloMedidorAtual").html(modelo);

	   		document.associacaoForm.marcaMedidorAtual.value = marca;
	   		$("#marcaMedidorAtual").html(marca);
	   		
	   		document.associacaoForm.modoUso.value = modoUso;
	   		$("#modoUso").html(modoUso);

	   		document.associacaoForm.numeroDigitosMedidorAtual.value = numeroDigitos;
	   		$("#numeroDigitosMedidorAtual").html(numeroDigitos);

			configurarDigitosLeituraMedidor();
		}else{
			alert('<fmt:message key="ERRO_MEDIDOR_NAO_DISPONIVEL_PARA_OPERACAO"/>');
		}
	}

	function popularDadosMedidor(medidor){
		if(medidor["chavePrimaria"] != undefined){
       		document.associacaoForm.chaveMedidorAtual.value = medidor["chavePrimaria"];
       	}

		if(medidor["tipo"] != undefined){
       		document.associacaoForm.tipoMedidorAtual.value = medidor["tipo"];
       		$("#tipoMedidorAtual").html(medidor["tipo"]);
       	}

		if(medidor["modelo"] != undefined){
       		document.associacaoForm.modeloMedidorAtual.value = medidor["modelo"];
       		$("#modeloMedidorAtual").html(medidor["modelo"]);
       	}

		if(medidor["marca"] != undefined){
       		document.associacaoForm.marcaMedidorAtual.value = medidor["marca"];
       		$("#marcaMedidorAtual").html(medidor["marca"]);
       	}
		if(medidor["modoUso"] != undefined){
       		document.associacaoForm.modoUso.value = medidor["modoUso"];
       		$("#modoUso").html(medidor["modoUso"]);
       		if(medidor["modoUso"]=="Virtual"){
       			$(".campoNaoObrigatorioMV").hide();
       		}else{
       			$(".campoNaoObrigatorioMV").show();
       		}
       	}else{
       		$(".campoNaoObrigatorioMV").show();
       	}
	
       	if(medidor["numeroDigitos"] != undefined){
       		document.associacaoForm.numeroDigitosMedidorAtual.value = medidor["numeroDigitos"];
       		$("#numeroDigitosMedidorAtual").html(medidor["numeroDigitos"]);
       	}
       	
	}

	function limparDadosMedidor(){
		document.associacaoForm.numeroDigitosMedidorAtual.value = "";
		document.associacaoForm.numeroSerieMedidorAtual.value = "";
		document.associacaoForm.chaveMedidorAtual.value = "";
		document.associacaoForm.tipoMedidorAtual.value = "";
		document.associacaoForm.modeloMedidorAtual.value = "";
		document.associacaoForm.marcaMedidorAtual.value = "";
		document.associacaoForm.modoUso.value = "";
		$("#tipoMedidorAtual,#modeloMedidorAtual,#marcaMedidorAtual, #modoUso, #numeroDigitosMedidorAtual").html("");
	}
	
	function limparDados(){
		document.associacaoForm.numeroDigitosMedidorAtual.value = "";
		document.associacaoForm.chaveMedidorAtual.value = "";
		document.associacaoForm.tipoMedidorAtual.value = "";
		document.associacaoForm.modeloMedidorAtual.value = "";
		document.associacaoForm.marcaMedidorAtual.value = "";
		document.associacaoForm.modoUso.value = "";
		$("#tipoMedidorAtual,#modeloMedidorAtual,#marcaMedidorAtual, #modoUso, #numeroDigitosMedidorAtual").html("");
	}

	function configurarDigitosLeituraMedidor(){
		
		var campoLeitura = document.associacaoForm.leituraAtual;
		
		var qtdaDigitos = document.associacaoForm.numeroDigitosMedidorAtual.value;

		if(campoLeitura != undefined){
			if(campoLeitura.value.length > qtdaDigitos){
				campoLeitura.value = "";
			}
			campoLeitura.maxLength = qtdaDigitos;
		}
	}

	$(document).ready(function(){
		//Exibe a janela modal que exibe a lista de medidores com mesma numera��o
		exibirJDialog("#listaMedidores");
		//Par�metros da janela modal que exibe a lista de medidores com mesma numera��o
		$("#listaMedidores").dialog({
			autoOpen: false,
			width: 450,
			modal: true,
			minHeight: 90,
			resizable: false
		});

	});

</script>

<!-- Conte�do da janela modal que exibe a lista de medidores com mesma numera��o -->
<!-- <div  id="listaMedidores" title="Medidores com mesma numera��o"> -->
<!-- 	<p class="orientacaoInicial">Para selecionar, clique em um dos <span class="destaqueOrientacaoInicial">Medidores</span> abaixo.</p> -->
<!-- 	<table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDialog"> -->
<!-- 		<thead> -->
<!-- 			<tr> -->
<!-- 				<th style="width: 25%">N�mero de S�rie</th> -->
<!-- 				<th style="width: 25%">Tipo</th> -->
<!-- 				<th style="width: 25%">Modelo</th> -->
<!-- 				<th style="width: 25%">Marca</th> -->
<!-- 			</tr> -->
<!-- 		</thead> -->
<!-- 		<tbody id="itensListaMedidores"></tbody> -->
<!-- 	</table> -->
<!-- 	<hr class="linhaSeparadoraPopup" /> -->
<!-- </div> -->

<legend>Medidor <c:out value="${param.complemento}"/></legend>
<div class="conteinerDados" style="width: 235px">
	<input name="Button" id="botaoPesquisarMedidor" class="bottonRightCol2" style="float: right; margin-right: 0; width: 140px" title="Pesquisar Medidor"  value="Pesquisar" onclick="exibirPopupPesquisaMedidor();" type="button">
	<hr class="linhaSeparadora"/>
	<fieldset style="width: 235px">
		<input type="hidden" name="chaveMedidorAtual" value="<c:out value="${associacaoVO.chaveMedidorAtual}"/>" />

		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>N�mero de S�rie:</label>
		<input id="numeroSerieMedidorAtual" type="text" class="campoTexto" name="numeroSerie" value="<c:out value="${associacaoVO.numeroSerie}"/>" size="10" maxlength="20" onblur="obterMedidor(this.value);"><br />

		<label class="rotulo">D�gitos de Leitura:</label>
		<input type="hidden" name="numeroDigitosMedidorAtual" value="<c:out value="${associacaoVO.numeroDigitosMedidorAtual}"/>">
		<span class="itemDetalhamento" id="numeroDigitosMedidorAtual"><c:out value="${associacaoVO.numeroDigitosMedidorAtual}"/></span><br />

		<label class="rotulo">Tipo:</label>
		<input type="hidden" name="tipoMedidorAtual" value="<c:out value="${associacaoVO.tipoMedidorAtual}"/>">
		<span class="itemDetalhamento" id="tipoMedidorAtual"><c:out value="${associacaoVO.tipoMedidorAtual}"/></span><br />

		<label class="rotulo">Modelo:</label>
		<input type="hidden" name="modeloMedidorAtual" value="<c:out value="${associacaoVO.modeloMedidorAtual}"/>">
		<span class="itemDetalhamento" id="modeloMedidorAtual"><c:out value="${associacaoVO.modeloMedidorAtual}"/></span><br />

		<label class="rotulo">Marca:</label>
		<input type="hidden" name="marcaMedidorAtual" value="<c:out value="${associacaoVO.marcaMedidorAtual}"/>">
		<span class="itemDetalhamento" id="marcaMedidorAtual"><c:out value="${associacaoVO.marcaMedidorAtual}"/></span>
		
		<label class="rotulo">Perfil:</label>
	    <input type="hidden" name="modoUso" value="<c:out value="${associacaoVO.modoUso}"/>">
		<span class="itemDetalhamento" id="modoUso"><c:out value="${associacaoVO.modoUso}"/></span>
		
		
	</fieldset>
</div>
