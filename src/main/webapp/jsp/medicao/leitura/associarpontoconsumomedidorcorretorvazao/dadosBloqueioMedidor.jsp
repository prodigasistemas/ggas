<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">

	function validar(){

		return true;
		
	}

</script>

<fieldset class="coluna2">
	<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosMedidorAnterior.jsp"/>
</fieldset>

<fieldset class="coluna2 coluna2Ajuste">
	<input type="hidden" name="contemCorretorVazao" value="<c:out value="${associacaoVO.contemCorretorVazao}"/>">	

	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Data:</label>
	<input class="campoData" type="text" id="dataMedidor" name="dataMedidor" value="${associacaoVO.dataMedidor}" maxlength="10">
	<br class="quebraLinha2" />

	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Leitura:</label>
	<input class="campoTexto" type="text" id="leituraAnterior" name="leituraAnterior" value="<c:out value="${associacaoVO.leituraAnterior}"/>" size="15" maxlength="<c:out value="${numeroDigitosMedidorAnterior}"/>" onpaste="return false" ondrop="return false" onkeypress="return formatarCampoInteiro(event)" onkeyup="return validarCampoInteiroCopiarColar(this)"><br/>
	
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Funcion�rio:</label>
	<select class="campoSelect" id="funcionario" name="funcionario" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaFuncionario}" var="funcionario">
			<option value="<c:out value="${funcionario.chavePrimaria}"/>" <c:if test="${associacaoVO.funcionario == funcionario.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${funcionario.nome}"/>
			</option>		
    	</c:forEach>
	</select><br/>
	
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Motivo:</label>
	<select class="campoSelect" id="medidorMotivoOperacao" name="medidorMotivoOperacao" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMedidorMotivoOperacao}" var="medidorMotivoOperacao">
			<option value="<c:out value="${medidorMotivoOperacao.chavePrimaria}"/>" <c:if test="${associacaoVO.medidorMotivoOperacao == medidorMotivoOperacao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${medidorMotivoOperacao.descricao}"/>
			</option>		
    	</c:forEach>
	</select><br/>
	
	<label class="rotulo campoObrigatorio">N�mero do Lacre 1:</label>
	<input class="campoTexto" type="text" id="lacre" name="lacre" value="<c:out value="${associacaoVO.lacre}"/>" size="11" maxlength="9" onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" onkeypress="return formatarCampoInteiro(event);"><br/>
	
	<label class="rotulo campoObrigatorio">N�mero do Lacre 2:</label>
	<input class="campoTexto" type="text" id="lacreDois" name="lacreDois" value="<c:out value="${associacaoVO.lacreDois}"/>" size="11" maxlength="9" onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" onkeypress="return formatarCampoInteiro(event);"><br/>
	
	<label class="rotulo campoObrigatorio">N�mero do Lacre 3:</label>
	<input class="campoTexto" type="text" id="lacreTres" name="lacreTres" value="<c:out value="${associacaoVO.lacreTres}"/>" size="11" maxlength="9" onkeyup="return letraMaiuscula(this)" onkeypress="return letraMaiuscula(this)" onkeypress="return formatarCampoInteiro(event);"><br/>
</fieldset>