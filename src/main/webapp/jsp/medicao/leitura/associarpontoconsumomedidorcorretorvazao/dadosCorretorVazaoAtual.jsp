<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">

	function obterCorretorVazao(numeroSerie){
		numeroSerie = trim(numeroSerie);
		if(numeroSerie != ""){
			document.associacaoForm.numeroSerieCorretorVazaoAtual.value = numeroSerie;
			
			AjaxService.obterCorretorVazaoPorNumeroSerie(numeroSerie, {
				callback: function(corretorVazaoList) {
					if(corretorVazaoList != null){
						if(corretorVazaoList['2'] != undefined) {
							gerarSelecaoVazaoCorretor(corretorVazaoList);
						} else {
							for(key in corretorVazaoList) {
								var corretorVazao = corretorVazaoList[key];
								if(corretorVazao["disponivel"] == 'true'){
									if(corretorVazao["chavePrimaria"] != undefined){
					               		document.associacaoForm.chaveCorretorVazaoAtual.value = corretorVazao["chavePrimaria"];
					               	}
									if(corretorVazao["modelo"] != undefined){
					               		document.associacaoForm.modeloCorretorVazaoAtual.value = corretorVazao["modelo"];
					               		$("#modeloCorretorVazaoAtual").html(corretorVazao["modelo"]);
					               	}
									if(corretorVazao["marca"] != undefined){
					               		document.associacaoForm.marcaCorretorVazaoAtual.value = corretorVazao["marca"];
					               		$("#marcaCorretorVazaoAtual").html(corretorVazao["marca"]);
					               	}
									if(corretorVazao["numeroDigitos"] != undefined){
							       		document.associacaoForm.numeroDigitosCorretorVazaoAtual.value = corretorVazao["numeroDigitos"];
					               		$("#numeroDigitosCorretorVazaoAtual").html(corretorVazao["numeroDigitos"]);
							       	}
								}else{
									alert('<fmt:message key="ERRO_CORRETOR_VAZAO_NAO_DISPONIVEL_PARA_OPERACAO"/>');
									limparDadosCorretorVazao();
								}
							}
						}
					}else{
						alert('<fmt:message key="ERRO_CORRETOR_VAZAO_NAO_ENCONTRADO_ASSOCIACAO_MEDIDOR"/>');
						limparDadosCorretorVazao();
					}
				}
				, async:false}
			);
		}else{
			limparDadosCorretorVazao();
		}

		configurarDigitosLeitura();
	}

	function limparDadosCorretorVazao(){
		document.associacaoForm.numeroDigitosCorretorVazaoAtual.value = 0;
		document.associacaoForm.numeroSerieCorretorVazaoAtual.value = "";
		document.associacaoForm.chaveCorretorVazaoAtual.value = "";
		document.associacaoForm.modeloCorretorVazaoAtual.value = "";
		document.associacaoForm.marcaCorretorVazaoAtual.value = "";
		$("#modeloCorretorVazaoAtual,#marcaCorretorVazaoAtual","#numeroDigitosCorretorVazaoAtual").html("");
	}

	function configurarDigitosLeitura(){

		var campoLeitura = document.associacaoForm.leituraAtualCorretorVazao;
		var qtdaDigitos = document.associacaoForm.numeroDigitosCorretorVazaoAtual.value;

		if(campoLeitura != undefined){

			if(campoLeitura.value.length > qtdaDigitos){
				campoLeitura.value = "";
			}

			campoLeitura.maxLength = qtdaDigitos;
		}
	}
	
	function exibirPopupPesquisaCorretorVazao() {
		exibirIndicador();
		popup = window.open('exibirPesquisaCorretorVazaoPopup?postBack=true','popup','height=750,width=850,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function gerarSelecaoVazaoCorretor(corretorVazaoList){
		exibirJDialog("#listaCorretorvazao");

		var inner = '';
   		var param = '';
   		var div = 2;
   		var start = 2;

   		$('#itensListaCorretorVazao').html('');
		for(key in corretorVazaoList){

			if(start % div == 0){
				param = "odd";
            }else{
				param = "even";
	        }

			var chavePrimaria = corretorVazaoList[key]["chavePrimaria"];
			var modelo = corretorVazaoList[key]["modelo"] != undefined ? corretorVazaoList[key]["modelo"] : '';
			var marca = corretorVazaoList[key]["marca"] != undefined ? corretorVazaoList[key]["marca"] : '';
			var numeroDigitos = corretorVazaoList[key]["numeroDigitos"];
			var disponivel = corretorVazaoList[key]["disponivel"];

			inner = inner + '<tr class='+param+' style="cursor: pointer;" onclick="selecionarCorretor('+ chavePrimaria + ',' + "'" + modelo + "'" + ',' + "'" + marca + "'" + ',' + numeroDigitos + ',' + disponivel + ');">'

			inner = inner + '<td>' + document.associacaoForm.numeroSerieCorretorVazaoAtual.value + '</td>';
			inner = inner + '<td>' + modelo + '</td>';
			inner = inner + '<td>' + marca + '</td>';

			inner = inner + '</tr>';
       		start = start + 1;
		}

		$("#itensListaCorretorVazao").prepend(inner);
		

	}
	
	function selecionarCorretor(chavePrimaria, modelo, marca, numeroDigitos, disponivel){
		if(disponivel == true){
			$("#listaCorretorvazao").dialog('close');

			document.associacaoForm.chaveCorretorVazaoAtual.value = chavePrimaria;

			document.associacaoForm.modeloCorretorVazaoAtual.value = modelo;
	   		$("#modeloCorretorVazaoAtual").html(modelo);

	   		document.associacaoForm.marcaCorretorVazaoAtual.value = marca;
	   		$("#marcaCorretorVazaoAtual").html(marca);

	   		document.associacaoForm.numeroDigitosCorretorVazaoAtual.value = numeroDigitos;
	   		$("#numeroDigitosCorretorVazaoAtual").html(numeroDigitos);

			configurarDigitosLeitura();
		}else{
			alert('<fmt:message key="ERRO_CORRETOR_VAZAO_NAO_DISPONIVEL_PARA_OPERACAO"/>');
		}
	}
	
	$(document).ready(function(){
		//Exibe a janela modal que exibe a lista de medidores com mesma numera��o
		exibirJDialog("#listaCorretorvazao");
		//Par�metros da janela modal que exibe a lista de medidores com mesma numera��o
		$("#listaCorretorvazao").dialog({
			autoOpen: false,
			width: 450,
			modal: true,
			minHeight: 90,
			resizable: false
		});
	});


</script>

<legend>Corretor de Vaz�o <c:out value="${param.complemento}"/></legend>
<div class="conteinerDados">
	<input name="Button" id="botaoPesquisarCorretorVazao" class="bottonRightCol2" title="Pesquisar Corretor Vaz�o"  value="Pesquisar Corretor Vaz�o" onclick="exibirPopupPesquisaCorretorVazao();" type="button">
	<hr class="linhaSeparadora"/>
	<fieldset style="width: 235px">
		<input type="hidden" name="chaveCorretorVazaoAtual" value="<c:out value="${associacaoVO.chaveCorretorVazaoAtual}"/>">

		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>N�mero de S�rie:</label>
		<input type="text" class="campoTexto" name="numeroSerieCorretorVazaoAtual" value="<c:out value="${associacaoVO.numeroSerieCorretorVazaoAtual}"/>" size="10" maxlength="20" onblur="obterCorretorVazao(this.value);"><br />

		<label class="rotulo">D�gitos de Leitura:</label>
		<input type="hidden" name="numeroDigitosCorretorVazaoAtual" value="<c:out value="${associacaoVO.numeroDigitosCorretorVazaoAtual}"/>">
		<span class="itemDetalhamento" id="numeroDigitosCorretorVazaoAtual"><c:out value="${associacaoVO.numeroDigitosCorretorVazaoAtual}"/></span>

		<label class="rotulo">Modelo:</label>
		<input type="hidden" name="modeloCorretorVazaoAtual" value="<c:out value="${associacaoVO.modeloCorretorVazaoAtual}"/>">
		<span class="itemDetalhamento" id="modeloCorretorVazaoAtual"><c:out value="${associacaoVO.modeloCorretorVazaoAtual}"/></span><br />

		<label class="rotulo">Marca:</label>
		<input type="hidden" name="marcaCorretorVazaoAtual" value="<c:out value="${associacaoVO.marcaCorretorVazaoAtual}"/>">
		<span class="itemDetalhamento" id="marcaCorretorVazaoAtual"><c:out value="${associacaoVO.marcaCorretorVazaoAtual}"/></span>
	</fieldset>
</div>