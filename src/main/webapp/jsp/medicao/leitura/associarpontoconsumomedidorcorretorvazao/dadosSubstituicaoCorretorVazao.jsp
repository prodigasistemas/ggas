<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">

	function validar(){

		return true;

	}
	
</script>

<fieldset class="coluna2">
	<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosCorretorVazaoAtual.jsp">
		<jsp:param name="complemento" value="Novo"/>
	</jsp:include>
</fieldset>

<fieldset class="colunaEsq2">
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Data:</label>
	<input class="campoData" type="text" id="dataCorretorVazao" name="dataCorretorVazao" value="${associacaoVO.dataCorretorVazao}" maxlength="10">
	<br class="quebraLinha2" />

	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Hora:</label>
	<input type="text" class="campoTexto" id="horaCorretorVazao" name="horaCorretorVazao" value="<c:out value="${associacaoVO.horaCorretorVazao}"/>" size="2"><br/>

	<label class="rotulo rotulo2Linhas campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Leitura de<br />Instala��o:</label>
	<input class="campoTexto" type="text" id="leituraAtualCorretorVazao" name="leituraAtualCorretorVazao" value="<c:out value="${associacaoVO.leituraAtualCorretorVazao}"/>" size="15" maxlength="<c:out value="${associacaoVO.numeroDigitosCorretorVazaoAtual}"/>" onpaste="return false" ondrop="return false" onkeypress="return formatarCampoInteiro(event)" onkeyup="return validarCampoInteiroCopiarColar(this);">
	<br />

	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Local:</label>
	<select class="campoSelect" id="localInstalacaoCorretorVazao" name="localInstalacaoCorretorVazao" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMedidorLocalInstalacao}" var="localInstalacaoCorretorVazao">
			<option value="<c:out value="${localInstalacaoCorretorVazao.chavePrimaria}"/>" <c:if test="${associacaoVO.localInstalacaoCorretorVazao == localInstalacaoCorretorVazao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${localInstalacaoCorretorVazao.descricao}"/>
			</option>
    	</c:forEach>
	</select><br />

	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Funcion�rio:</label>
	<select class="campoSelect" id="funcionarioCorretorVazao" name="funcionarioCorretorVazao" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaFuncionario}" var="funcionarioCorretorVazao">
			<option value="<c:out value="${funcionarioCorretorVazao.chavePrimaria}"/>" <c:if test="${associacaoVO.funcionarioCorretorVazao == funcionarioCorretorVazao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${funcionarioCorretorVazao.nome}"/>
			</option>
    	</c:forEach>
	</select><br/>

	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Motivo:</label>
	<select class="campoSelect" id="medidorMotivoOperacao" name="medidorMotivoOperacao" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMedidorMotivoOperacao}" var="medidorMotivoOperacao">
			<option value="<c:out value="${medidorMotivoOperacao.chavePrimaria}"/>" <c:if test="${associacaoVO.medidorMotivoOperacao == medidorMotivoOperacao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${medidorMotivoOperacao.descricao}"/>
			</option>
    	</c:forEach>
	</select>
</fieldset>

<hr class="linhaSeparadora2" />

<fieldset id="dadosMedidorCorretorVazaoAnterior" class="conteinerBloco">
	<fieldset class="coluna2">
		<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosCorretorVazaoAnterior.jsp">
			<jsp:param name="complemento" value="Atual"/>
		</jsp:include>
	</fieldset>

	<fieldset class="colunaEsq2">
		<label class="rotulo rotulo2Linhas campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Leitura de<br />Retirada:</label>
		<input type="text" class="campoTexto" id="leituraAnteriorCorretorVazao" name="leituraAnteriorCorretorVazao" value="<c:out value="${associacaoVO.leituraAnteriorCorretorVazao}"/>" size="15" maxlength="<c:out value="${numeroDigitosCorretorVazaoAnterior}"/>" onpaste="return false" ondrop="return false" onkeypress="return formatarCampoInteiro(event)" onkeyup="return validarCampoInteiroCopiarColar(this)"">
	</fieldset>
</fieldset>

