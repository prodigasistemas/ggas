<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">

	$(document).ready(function(){
		isComparacao = true;
		isViradaMedidor = false;

		<c:choose>
			<c:when test="${associacaoVO.instalarCorretorVazao ne true}">
				$("#fieldsetInstalacaoCorretorVazao").hide();			
			</c:when>
			<c:otherwise>
				$("#fieldsetInstalacaoCorretorVazao").show();
			</c:otherwise>
		</c:choose>
		
		var modoUso= '${associacaoVO.modoUso}';
		if(modoUso=="Virtual"){
			$(".campoNaoObrigatorioMV").hide();
		}else{
			$(".campoNaoObrigatorioMV").show();
		}

		$(".pressaoFornecimentoCampo").on('change', function(){
			var isSalvarPressao = false;

			var idResultado = $('#resultadoPontoDeConsumo').val();
			var contrato = $('#contratoFormatado').val();
			var idPressao = $(this).val();

			if (idPressao != idResultado){
				var msgConfirm = confirm("A press�o de fornecimento inserida � diferente da press�o cadastrada no contrato (" +contrato+ "), confirma a altera��o?");
				if (msgConfirm == true) {
					isSalvarPressao = true;
	 			}
			}
			console.log(idPressao +" - "+idResultado);
		});
	});
	
	function exibirDadosCorretorVazao() {
		$('#fieldsetInstalacaoCorretorVazao').slideDown('slow');
	}
	
	function esconderDadosCorretorVazao() {
		$('#fieldsetInstalacaoCorretorVazao').slideUp('slow');
	}

	function validar(){

		return true;
		
	}

</script>
<c:if test="${associacaoVO.isAssociacaoMedidorIndependente !=true}">
	<fieldset class="coluna2">
		<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosMedidorAtual.jsp"/>
	</fieldset>
</c:if>
<fieldset class="colunaEsq2">
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Data:</label>
	<input class="campoData" type="text" id="dataMedidor" name="dataMedidor" value="${associacaoVO.dataMedidor}" maxlength="10">
	<br class="quebraLinha2" />
	
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo campoNaoObrigatorioMV">* </span>Leitura:</label>
	<input class="campoTexto" type="text" id="leituraAtual" name="leituraAtual" value="<c:out value="${associacaoVO.leituraAtual}"/>" size="15" maxlength="<c:out value="${associacaoVO.numeroDigitosMedidorAtual}"/>" onpaste="return false" ondrop="return false" onkeypress="return formatarCampoInteiro(event)" onkeyup="return validarCampoInteiroCopiarColar(this)" ><br />
	
	<label class="rotulo" >Local:</label>
	<select class="campoSelect" id="localInstalacaoMedidor" name="localInstalacaoMedidor" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMedidorLocalInstalacao}" var="localInstalacaoMedidor">
			<option value="<c:out value="${localInstalacaoMedidor.chavePrimaria}"/>" <c:if test="${associacaoVO.localInstalacaoMedidor == localInstalacaoMedidor.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${localInstalacaoMedidor.descricao}"/>
			</option>		
    	</c:forEach>
	</select>
	<br />
	
	<label class="rotulo" >Prote��o:</label>
	<select class="campoSelect" id="medidorProtecao" name="medidorProtecao" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMedidorProtecao}" var="medidorProtecao">
			<option value="<c:out value="${medidorProtecao.chavePrimaria}"/>" <c:if test="${associacaoVO.medidorProtecao == medidorProtecao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${medidorProtecao.descricao}"/>
			</option>		
    	</c:forEach>
	</select>
	<br />
	
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Funcion�rio:</label>
	<select class="campoSelect" id="funcionario" name="funcionario" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaFuncionario}" var="funcionario">
			<option value="<c:out value="${funcionario.chavePrimaria}"/>" <c:if test="${associacaoVO.funcionario == funcionario.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${funcionario.nome}"/>
			</option>		
    	</c:forEach>
	</select>
    <br />
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Motivo:</label>
	<select class="campoSelect" id="medidorMotivoOperacao" name="medidorMotivoOperacao" style="width: 200px">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaMedidorMotivoOperacao}" var="medidorMotivoOperacao">
			<option value="<c:out value="${medidorMotivoOperacao.chavePrimaria}"/>" <c:if test="${associacaoVO.medidorMotivoOperacao == medidorMotivoOperacao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${medidorMotivoOperacao.descricao}"/>
			</option>		
    	</c:forEach>
	</select>
	<br />
	<label class="rotulo campoObrigatorio">Press�o de fornecimento em<br> campo:</label>
	<select class="campoSelect pressaoFornecimentoCampo" id="pressaoFornecimentoCampo" name="pressaoFornecimentoCampo"
		title="<c:if test="${contratoPontoConsumo.pontoConsumo == null}">Esse ponto de consumo n�o possui contrato inclu�do no GGAS</c:if>"
		<c:if test="${contratoPontoConsumo.pontoConsumo == null}">disabled="disabled""</c:if>
		style="width: 200px">
		<option>Selecione</option>
		 <c:forEach items="${listaPressaoFornecimentoCampo}" var="pressaoFornecimentoCampo">
			<option value="<c:out value="${pressaoFornecimentoCampo.chavePrimaria}"/>">
				<c:out value="${pressaoFornecimentoCampo.medidaMinimo}"/> <c:out value="${pressaoFornecimentoCampo.unidadePressao.descricaoAbreviada}"/>
			</option>
		</c:forEach>
	</select>
	<br />
	<br />
	<br />
</fieldset>
	
	
<hr class="linhaSeparadora2" />
	
<fieldset class="conteinerBloco">
	<label class="rotulo campoObrigatorio" for="instalarCorretorVazao"><span class="campoObrigatorioSimbolo">* </span>Instalar Corretor de Vaz�o?</label>
	<input class="campoRadio" type="radio" value="true" name="instalarCorretorVazao" id="instalarCorretorVazao" <c:if test="${associacaoVO.instalarCorretorVazao eq 'true'}">checked</c:if>  onclick="exibirDadosCorretorVazao()"><label class="rotuloRadio">Sim</label>
	<input class="campoRadio" type="radio" value="false" name="instalarCorretorVazao" id="instalarCorretorVazao" <c:if test="${associacaoVO.instalarCorretorVazao ne 'true'}">checked</c:if> onclick="esconderDadosCorretorVazao()"><label class="rotuloRadio">N�o</label>
	<br/><br/>
	<fieldset id="fieldsetInstalacaoCorretorVazao">
		<jsp:include page="/jsp/medicao/leitura/associarpontoconsumomedidorcorretorvazao/dadosInstalacaoCorretorVazao.jsp"/>
	</fieldset>
</fieldset>
