<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript" src="/ggas_mantenedor/js/utils.js"></script>

<h1 class="tituloInterno">
	<c:out value="${tituloAssociacao}"/>
	<c:if test="${dados eq 'dadosInstalacao'}">
		<a class="linkHelp" href="<help:help>/instalaodosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosSubstituicao'}">
		<a class="linkHelp" href="<help:help>/substituiodosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosAtivacao'}">
		<a class="linkHelp" href="<help:help>/ativaodosmedidores.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosRetirada'}">
		<a class="linkHelp" href="<help:help>/retiradadosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosBloqueio'}">
		<a class="linkHelp" href="<help:help>/retiradadosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${dados eq 'dadosReativacao'}">
		<a class="linkHelp" href="<help:help>/retiradadosmedidorescorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
</h1>
<p class="orientacaoInicial">Para completar a associa��o, preencha os campos abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.</p>

<script type="text/javascript">

	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,group=clientes,persist=1,hide=0');

	$(document).ready(function(){

		$("#dataMedidor").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#dataMedidor").datepicker('option', 'maxDate', '+0d');
		var indicadorAtualizacaoCadastral = "${sessionScope.indicadorAtualizacaoCadastral}";
		if(indicadorAtualizacaoCadastral != ''){
			$('#botaoCancelar').attr("disabled","disabled");
		}
		if(document.associacaoForm.horaCorretorVazao != undefined){
			$("#horaCorretorVazao").inputmask("99:99");
		}

		$('.checkPontoConsumo').click(function(){
		    var marcado = $(this).is(':checked');
            var linha = $(this).closest('tr');
            if(!marcado) {
                limparInfoLinha(linha);
            }
        });

        $('.limparMedidor').click(function(){
            var celula = $(this).closest('td');
            celula.children('input[name=chaveMedidorLote]').first().val('');
            celula.children('.labelNumSerieMedidor').first().html('');
            celula.children('input[name=botaoBuscarMedidor]').first().show();
            $(this).hide();
        });

        $('.limparMedidorCorretorVazao').click(function(){
            var celula = $(this).closest('td');
            celula.children('input[name=chaveCorretorVazaoLote]').first().val('');
            celula.children('.labelNumSerieMedidorCorretorVazao').first().html('');
            celula.children('input[name=botaoBuscarMedidor]').first().show();
            $(this).hide();
        });

		$('input[type=number][max]:not([max=""])').live('input', function(ev) {
			var $this = $(this);
			var maxlength = $this.attr('max').length;
			var value = $this.val();
			if (value && value.length >= maxlength) {
				$this.val(value.substr(0, maxlength));
			}
		});
	});

	function limparInfoLinha(linha) {
        linha.find('.inputLeitura').children('input[name=leituraLote]').first().val('');
        linha.find('.infoMedidor').children('input[name=chaveMedidorLote]').first().val('');
        linha.find('.infoMedidor').children('.labelNumSerieMedidor').first().html('');
    }

    function exibirMensagemErro(mensagem) {
        var $mensagemSpring = $('.mensagensSpring');
        $mensagemSpring.html(
            $("<div>", {class : 'failure notification hideit'}).append(
                $("<p>").append(
                    $("<strong>").html("Erro: "), mensagem
                )
            )
        ).show().click(function(){
            $mensagemSpring.hide()
        });
        document.getElementsByClassName("mensagensSpring")[0].scrollIntoView();
    }

	function dadosValidos() {
        var valido = true;
		var qtdMarcado = 0;
        $('.checkPontoConsumo:checkbox:enabled').each(function(){
            var linha = $(this).closest('tr');
            var nomePontoConsumo = linha.find('.nomePontoConsumo').html();
            if ($(this).is(':checked')) {
                qtdMarcado++;
                var inputLeitura = linha.find('.inputLeitura').children('input[name=leituraLote]').first();
                if (inputLeitura.val() == '') {
                    valido = false;
                    exibirMensagemErro('Informe o valor de leitura para o ponto de consumo: ' + nomePontoConsumo);
                    return;
                }
                <c:if test="${tipoAssociacaoPontoConsumoMedidor eq true}">
                    <c:if test="${dados ne 'dadosAtivacao'}">
                        var idMedidor = linha.find('.infoMedidor').children('input[name=chaveMedidorLote]').first().val();
                        if (!idMedidor) {
                            valido = false;
                            exibirMensagemErro('Informe um medidor para o ponto de consumo: ' + nomePontoConsumo);
                            return;
                        }
                    </c:if>
                </c:if>
                <c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
                    var idMedidor = linha.find('.infoMedidorCorretorVazao').children('input[name=chaveCorretorVazaoLote]').first().val();
                    if (!idMedidor) {
                        valido = false;
                        exibirMensagemErro('Informe um medidor para o ponto de consumo: ' + nomePontoConsumo);
                        return;
                    }
                </c:if>
            } else {
                <c:if test="${tipoAssociacaoPontoConsumoMedidor eq true}">
                    <c:if test="${dados ne 'dadosAtivacao'}">
                    var idMedidor = linha.find('.infoMedidor').children('input[name=chaveMedidorLote]').first().val();
                    if (idMedidor) {
                        valido = false;
                        exibirMensagemErro('Existe um medidor associado a um ponto de consumo n�o selecionado: ' + nomePontoConsumo);
                        return;
                    }
                    </c:if>
                </c:if>
                <c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
                var idMedidor = linha.find('.infoMedidorCorretorVazao').children('input[name=chaveCorretorVazaoLote]').first().val();
                if (idMedidor) {
                    valido = false;
                    exibirMensagemErro('Existe um medidor associado a um ponto de consumo n�o selecionado: ' + nomePontoConsumo);
                    return;
                }
                </c:if>
            }
        });
		if (qtdMarcado == 0) {
            exibirMensagemErro('Selecione algum ponto de consumo para realizar a opera��o.');
			return false;
		}
		return valido;
	}

	function salvarAssociacao(){
		if (dadosValidos()) {
			var $myForm = $('#associacaoForm');
			if($myForm[0].checkValidity()) {
				// Desabilitar todos os inputs de leitura que n�o est�o com o checkbox de ponto de consumo marcado
				$('.checkPontoConsumo:checkbox:enabled').not(':checked').closest('tr').find('.inputLeitura').children('input[name=leituraLote]').prop('disabled', true);
				// Desabilitar todos os inputs de medidor que n�o est�o com o checkbox de ponto de consumo marcado
				$('.checkPontoConsumo:checkbox:enabled').not(':checked').closest('tr').find('.infoMedidor').children('input[name=chaveMedidorLote]').prop('disabled', true);
				$('.checkPontoConsumo:checkbox:enabled').not(':checked').closest('tr').find('.infoMedidorCorretorVazao').children('input[name=chaveCorretorVazaoLote]').prop('disabled', true);
				exibirIndicador();
				submeter("associacaoForm", "salvarAssociacaoLote");
			} else {
				validarDadosComuns();
			}
		}
	}

	function validarDadosComuns() {
		var campos = [];

		if(!$('#dataMedidor')[0].checkValidity()) {
			campos.push("Data");
		}
		if(!$('#funcionario')[0].checkValidity()) {
			campos.push("Funcion�rio");
		}
		<c:if test="${dados ne 'dadosAtivacao'}">
			if(!$('#medidorMotivoOperacao')[0].checkValidity()) {
				campos.push("Motivo");
			}
			<c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
				if(!$('#horaCorretorVazao')[0].checkValidity()) {
					campos.push("Hora");
				}
			</c:if>
		</c:if>
		if (campos.length > 0) {
			var mensagem = "O(s) campo(s) "+campos.join(", ")+" � (s�o) de preenchimento obrigat�rio";
			exibirMensagemErro(mensagem);
		}
	}

	function cancelar(){
		submeter("associacaoForm", "pesquisarAssociacao");
	}

	// Busca de medidor

	function obterMedidor(numeroSerie, pontoConsumo){
		numeroSerie = trim(numeroSerie);
		if(numeroSerie != ""){
			AjaxService.obterMedidorPorNumeroSerie(numeroSerie, {
				callback: function(medidores) {
					if(medidores != null){
						if(medidores['2'] == undefined){
							for(key in medidores){
								var medidor = medidores[key];
								if(medidor["disponivel"] == 'true'){
									if (medidorAindaNaoFoiInserido(medidor["chavePrimaria"])) {
                                        $('input[value="'+pontoConsumo+'"]').prop('checked', true);
										$('#chaveMedidor' + pontoConsumo).val(medidor["chavePrimaria"]);
										$('#labelNumeroSerieMedidor' + pontoConsumo).html(numeroSerie);
										configurarDigitosLeitura(pontoConsumo, medidor["numeroDigitos"]);
                                        var celulaInfoMedidor = $('input[value='+pontoConsumo+']').closest('tr').find('.infoMedidor');
                                        celulaInfoMedidor.children('input[name=botaoBuscarMedidor]').first().hide();
                                        celulaInfoMedidor.children('.limparMedidor').first().show();
									} else {
										exibirMensagemErro('<fmt:message key="ERRO_MEDIDOR_JA_INSERIDO"/>');
									}
								}else{
									exibirMensagemErro('<fmt:message key="ERRO_MEDIDOR_NAO_DISPONIVEL_PARA_OPERACAO"/>');
								}
							}
						} else {
							exibirMensagemErro('<fmt:message key="ERRO_MEDIDOR_NAO_ENCONTRADO_ASSOCIACAO_MEDIDOR"/>');
						}
					}else{
						exibirMensagemErro('<fmt:message key="ERRO_MEDIDOR_NAO_ENCONTRADO_ASSOCIACAO_MEDIDOR"/>');
					}
				}
				, async:false
			});

		}
	}

	function medidorAindaNaoFoiInserido(id) {
		var podeInserir = true;
		$('input[name=chaveMedidorLote]').each(function(){
			if($(this).val() == id) {
				podeInserir = false;
				return;
			}
		});
		return podeInserir;
	}

	function corretorVazaoAindaNaoFoiInserido(id) {
		var podeInserir = true;
		$('input[name=chaveCorretorVazaoLote]').each(function(){
			if($(this).val() == id) {
				podeInserir = false;
				return;
			}
		});
		return podeInserir;
	}

	function configurarDigitosLeitura(pontoConsumo, qtdaDigitos){

		var campoLeitura = $('#leituraLote' + pontoConsumo);

		if(campoLeitura != undefined){
			if(campoLeitura.val().length > qtdaDigitos){
				campoLeitura.val("");
			}
			campoLeitura.prop('max', "9".repeat(qtdaDigitos));
		}
	}

	function obterCorretorVazao(numeroSerie, pontoConsumo){
		numeroSerie = trim(numeroSerie);
		if(numeroSerie != ""){
			AjaxService.obterCorretorVazaoPorNumeroSerie(numeroSerie, {
				callback: function(corretorVazaoList) {
					if(corretorVazaoList != null){
						if(corretorVazaoList['2'] == undefined){
							for(key in corretorVazaoList) {
								var corretorVazao = corretorVazaoList[key];
								if(corretorVazao["disponivel"] == 'true'){
									if (corretorVazaoAindaNaoFoiInserido(corretorVazao["chavePrimaria"])) {
                                        $('input[value="'+pontoConsumo+'"]').prop('checked', true);
										$('#chaveCorretorVazao' + pontoConsumo).val(corretorVazao["chavePrimaria"]);
										$('#labelNumeroSerieCorretorVazao' + pontoConsumo).html(numeroSerie);
										configurarDigitosLeitura(pontoConsumo, corretorVazao["numeroDigitos"]);
										var celulaInfoMedidor = $('input[value='+pontoConsumo+']').closest('tr').find('.infoMedidorCorretorVazao');
                                        celulaInfoMedidor.children('input[name=botaoBuscarMedidor]').first().hide();
                                        celulaInfoMedidor.children('.limparMedidorCorretorVazao').first().show();
									} else {
										exibirMensagemErro('<fmt:message key="ERRO_CORRETOR_VAZAO_JA_INSERIDO"/>');
									}
								}else{
									exibirMensagemErro('<fmt:message key="ERRO_CORRETOR_VAZAO_NAO_DISPONIVEL_PARA_OPERACAO"/>');
								}
							}
						}
					}else{
						exibirMensagemErro('<fmt:message key="ERRO_CORRETOR_VAZAO_NAO_ENCONTRADO_ASSOCIACAO_MEDIDOR"/>');
					}
				}
				, async:false}
			);
		}
	}

	function exibirPopupPesquisaMedidor(pontoConsumo) {
		exibirIndicador();
		popup = window.open('exibirPesquisaMedidorPopup?postBack=true&lote=true&pontoConsumo='+pontoConsumo,'popup','height=750,width=850,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function exibirPopupPesquisaCorretorVazao(pontoConsumo) {
		exibirIndicador();
		popup = window.open('exibirPesquisaCorretorVazaoPopup?postBack=true&lote=true&pontoConsumo='+pontoConsumo,'popup','height=750,width=850,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}



</script>

<form:form action="salvarAssociacaoLote" id="associacaoForm" name="associacaoForm">

	<input type="hidden" name="indicadorPesquisa" id="indicadorPesquisa" value="${associacaoVO.indicadorPesquisa}">
	<input type="hidden" name="acao" value="salvarAssociacaoLote"/>
	<input type="hidden" name="idOperacaoMedidor" value="${associacaoVO.idOperacaoMedidor}"/>
	<input type="hidden" name="tipoAssociacao" value="${associacaoVO.tipoAssociacao}"/>
	<input type="hidden" name="exibirDadosFiltro" value="true"/>
	<input type="hidden" name="indicadorAssociacaoEmLote" value="true"/>
	<input type="hidden" name="idCliente" value="${associacaoVO.idCliente}"/>
	<input type="hidden" name="idImovel" value="${associacaoVO.idImovel}"/>
	<input type="hidden" name="instalarCorretorVazao" value="${tipoAssociacaoPontoConsumoCorretorVazao eq true}"/>

	<fieldset id="associacaoPontoConsumoMedidorCorretor" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<c:if test="${associacaoVO.isAssociacaoMedidorIndependente != true}">
				<a class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosImovel" class="conteinerDados conteinerBloco">

					<fieldset class="coluna detalhamentoColunaLarga">
						<label class="rotulo">Descri��o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargoNovo"><c:out value="${descricaoImovel}"/></span><br />

						<label class="rotulo" id="rotuloEnderecoTexto">Cep:</label>
						<span class="itemDetalhamento"><c:out value="${cepImovel}"/></span><br />
					</fieldset>

					<fieldset class="colunaFinal">
						<label class="rotulo" id="rotuloCnpjTexto">Endere�o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${enderecoImovel}"/></span><br />

						<label class="rotulo" id="rotuloNumeroContrato">Complemento:</label>
						<span class="itemDetalhamento"><c:out value="${complementoImovel}"/></span>
					</fieldset>
				</fieldset>
			<hr class="linhaSeparadora1" />
			</c:if>
			<fieldset id="dadosMedidorCorretorVazao" class="conteinerBloco">
				<fieldset class="colunaEsq2">
					<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Data:</label>
					<input required class="campoData" type="text" id="dataMedidor" name="dataMedidor" value="${associacaoVO.dataMedidor}" maxlength="10">
					<br class="quebraLinha2" />
					<c:if test="${dados ne 'dadosAtivacao'}">
						<label class="rotulo">Local:</label>
						<select class="campoSelect" id="localInstalacaoMedidor" name="localInstalacaoMedidor" style="width: 200px">
							<option value="">Selecione</option>
							<c:forEach items="${listaMedidorLocalInstalacao}" var="localInstalacaoMedidor">
								<option value="<c:out value="${localInstalacaoMedidor.chavePrimaria}"/>" <c:if test="${associacaoVO.localInstalacaoMedidor == localInstalacaoMedidor.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${localInstalacaoMedidor.descricao}"/>
								</option>
							</c:forEach>
						</select>
						<br />
					</c:if>
					<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Funcion�rio:</label>
					<select required class="campoSelect" id="funcionario" name="funcionario" style="width: 200px">
						<option value="">Selecione</option>
						<c:forEach items="${listaFuncionario}" var="funcionario">
							<option value="<c:out value="${funcionario.chavePrimaria}"/>" <c:if test="${associacaoVO.funcionario == funcionario.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${funcionario.nome}"/>
							</option>
						</c:forEach>
					</select>
					<br />
				</fieldset>
				<c:if test="${dados ne 'dadosAtivacao'}">
					<fieldset class="colunaEsq2">
						<c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
							<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Hora:</label>
							<input required class="campoTexto" type="text" id="horaCorretorVazao" name="horaCorretorVazao" value="<c:out value="${associacaoVO.horaCorretorVazao}"/>" size="2">
							<br />
						</c:if>
						<c:if test="${tipoAssociacaoPontoConsumoMedidor eq true}">
							<label class="rotulo">Prote��o:</label>
							<select class="campoSelect" id="medidorProtecao" name="medidorProtecao" style="width: 200px">
								<option value="">Selecione</option>
								<c:forEach items="${listaMedidorProtecao}" var="medidorProtecao">
									<option value="<c:out value="${medidorProtecao.chavePrimaria}"/>" <c:if test="${associacaoVO.medidorProtecao == medidorProtecao.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${medidorProtecao.descricao}"/>
									</option>
								</c:forEach>
							</select>
							<br />
						</c:if>
						<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Motivo:</label>
						<select required class="campoSelect" id="medidorMotivoOperacao" name="medidorMotivoOperacao" style="width: 200px">
							<option value="">Selecione</option>
							<c:forEach items="${listaMedidorMotivoOperacao}" var="medidorMotivoOperacao">
								<option value="<c:out value="${medidorMotivoOperacao.chavePrimaria}"/>" <c:if test="${associacaoVO.medidorMotivoOperacao == medidorMotivoOperacao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${medidorMotivoOperacao.descricao}"/>
								</option>
							</c:forEach>
						</select>
						<br />
						<br />
						<br />
						<br />
					</fieldset>
				</c:if>
			</fieldset>
			<hr class="linhaSeparadora1" />
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumoFilho" id="pontoConsumo"
					sort="external" pagesize="${quantidadePontoConsumoFilho}" size="${quantidadePontoConsumoFilho}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirAssociacaoLote">

				<c:if test="${tipoAssociacaoPontoConsumoMedidor eq true}">
					<c:if test="${dados eq 'dadosInstalacao'}">
						<c:set var="pontoConsumoValidoParaOperacao" value="${pontoConsumo.instalacaoMedidor eq null
						&& pontoConsumo.situacaoConsumo.descricao eq 'AGUARDANDO ATIVACAO'}" />
					</c:if>
					<c:if test="${dados eq 'dadosAtivacao'}">
						<c:set var="pontoConsumoValidoParaOperacao" value="${pontoConsumo.instalacaoMedidor ne null &&
						pontoConsumo.instalacaoMedidor.dataAtivacao eq null}" />
					</c:if>
					<c:if test="${dados eq 'dadosInstalacaoAtivacao'}">
						<c:set var="pontoConsumoValidoParaOperacao" value="${pontoConsumo.instalacaoMedidor eq null
						&& pontoConsumo.situacaoConsumo.descricao eq 'AGUARDANDO ATIVACAO'}" />
					</c:if>
				</c:if>
				<c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
					<c:if test="${dados eq 'dadosInstalacao'}">
						<c:set var="pontoConsumoValidoParaOperacao" value="${pontoConsumo.instalacaoMedidor.vazaoCorretor eq null &&
						pontoConsumo.instalacaoMedidor.medidor ne null}" />va
					</c:if>
					<c:if test="${dados eq 'dadosAtivacao'}">
						<c:set var="pontoConsumoValidoParaOperacao" value="${pontoConsumo.instalacaoMedidor.vazaoCorretor ne null}" />
					</c:if>
					<c:if test="${dados eq 'dadosInstalacaoAtivacao'}">
						<c:set var="pontoConsumoValidoParaOperacao" value="${pontoConsumo.instalacaoMedidor.vazaoCorretor eq null}" />
					</c:if>
				</c:if>

				<display:column  style="width: 10px">
					<input type="checkbox" name="idPontoConsumoLote" class="checkPontoConsumo" style="margin: 5px;" value="<c:out value="${pontoConsumo.chavePrimaria}"/>"
						   <c:if test="${!pontoConsumoValidoParaOperacao}">disabled</c:if>
                            <c:if test="${pontosMarcados[pontoConsumo.chavePrimaria] ne null}">checked</c:if>/>
				</display:column>

				<display:column title="Ponto de Consumo" sortProperty="descricao" property="descricao" class="nomePontoConsumo"
                                headerClass="tituloTabelaEsq" style="width: 230px; text-align: left; padding-left: 10px" />

				<display:column title="Status" sortProperty="situacaoConsumo.descricao" property="situacaoConsumo.descricao" headerClass="tituloTabelaEsq" style="width: 230px; text-align: left; padding-left: 10px" />

				<c:if test="${tipoAssociacaoPontoConsumoMedidor eq true}">
					<display:column  title="Leitura" headerClass="tituloTabelaEsq" class="inputLeitura" style="width: 230px; text-align: left; padding-left: 10px">
						<c:if test="${pontoConsumoValidoParaOperacao}">
							<input type="number" min="0" name="leituraLote" id="leituraLote${pontoConsumo.chavePrimaria}"
								   <c:if test="${digitoMedidorMarcado[pontoConsumo.chavePrimaria] ne null}">max="${digitoMedidorMarcado[pontoConsumo.chavePrimaria]}" </c:if>
                                   <c:if test="${leituraMarcada[pontoConsumo.chavePrimaria] ne null}">value="${leituraMarcada[pontoConsumo.chavePrimaria]}" </c:if>/>
						</c:if>
					</display:column>
				</c:if>

				<display:column sortable="false" title="Medidor" class="infoMedidor" headerClass="tituloTabelaEsq" style="width: 230px; text-align: left; padding-left: 10px">
					<c:choose>
						<c:when test="${pontoConsumoValidoParaOperacao}">
							<c:choose>
								<c:when test="${pontoConsumo.instalacaoMedidor eq null && dados ne 'dadosAtivacao'}">
									<label id="labelNumeroSerieMedidor${pontoConsumo.chavePrimaria}" class="labelNumSerieMedidor">
                                        <c:if test="${numMedidorMarcado[pontoConsumo.chavePrimaria] ne null}">${numMedidorMarcado[pontoConsumo.chavePrimaria]}</c:if>
                                    </label>
									<input id="chaveMedidor${pontoConsumo.chavePrimaria}" type="hidden" name="chaveMedidorLote"
                                           <c:if test="${idMedidorMarcado[pontoConsumo.chavePrimaria] ne null}">value="${idMedidorMarcado[pontoConsumo.chavePrimaria]}" </c:if>/>
									<input name="botaoBuscarMedidor" class="bottonRightCol2 botaoPesquisarMedidorCorretor"
										   value="Pesquisar" type="button"
										   onclick="exibirPopupPesquisaMedidor(<c:out value="${pontoConsumo.chavePrimaria}"/>);" />
                                    <input class="bottonRightCol2 botaoPesquisarMedidorCorretor limparMedidor" style="display: none"
                                           value="Limpar" type="button"/>
								</c:when>
								<c:otherwise>
										<label id="labelNumeroSerieMedidor${pontoConsumo.chavePrimaria}">
												${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}</label>
										<input id="chaveMedidor${pontoConsumo.chavePrimaria}" type="hidden" name="chaveMedidorLote"
											   value="${pontoConsumo.instalacaoMedidor.medidor.chavePrimaria}" />
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<label id="labelNumeroSerieMedidor${pontoConsumo.chavePrimaria}">
									${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}</label>
						</c:otherwise>
					</c:choose>
				</display:column>

				<c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
					<display:column  title="Leitura de Instala��o" headerClass="tituloTabelaEsq" class="inputLeitura" style="width: 230px; text-align: left; padding-left: 10px">
						<c:if test="${pontoConsumoValidoParaOperacao}">
							<input type="number" min="0" name="leituraLote" id="leituraLote${pontoConsumo.chavePrimaria}"
								   <c:if test="${digitoMedidorCorretorVazaoMarcado[pontoConsumo.chavePrimaria] ne null}">max="${digitoMedidorCorretorVazaoMarcado[pontoConsumo.chavePrimaria]}" </c:if>
                                   <c:if test="${leituraMarcada[pontoConsumo.chavePrimaria] ne null}">value="${leituraMarcada[pontoConsumo.chavePrimaria]}" </c:if>/>
						</c:if>
					</display:column>
				</c:if>

				<c:if test="${tipoAssociacaoPontoConsumoCorretorVazao eq true}">
					<display:column sortable="false" title="Corretor de Vaz�o" class="infoMedidorCorretorVazao" headerClass="tituloTabelaEsq"
									style="width: 230px; text-align: left; padding-left: 10px">
						<c:choose>
							<c:when test="${pontoConsumoValidoParaOperacao}">
								<label id="labelNumeroSerieCorretorVazao${pontoConsumo.chavePrimaria}"  class="labelNumSerieMedidorCorretorVazao">
                                    <c:if test="${numMedidorCorretorVazaoMarcado[pontoConsumo.chavePrimaria] ne null}">${numMedidorCorretorVazaoMarcado[pontoConsumo.chavePrimaria]}</c:if>
                                </label>
								<input id="chaveCorretorVazao${pontoConsumo.chavePrimaria}" type="hidden" name="chaveCorretorVazaoLote"
                                       <c:if test="${idMedidorCorretorVazaoMarcado[pontoConsumo.chavePrimaria] ne null}">value="${idMedidorCorretorVazaoMarcado[pontoConsumo.chavePrimaria]}" </c:if>/>
								<input name="botaoBuscarMedidor" class="bottonRightCol2 botaoPesquisarMedidorCorretor"
									value="Pesquisar" type="button" onclick="exibirPopupPesquisaCorretorVazao(<c:out value="${pontoConsumo.chavePrimaria}"/>);" />
                                <input class="bottonRightCol2 botaoPesquisarMedidorCorretor limparMedidorCorretorVazao" style="display: none"
                                       value="Limpar" type="button" />
							</c:when>
							<c:otherwise>
								<label id="labelNumeroSerieMedidor${pontoConsumo.chavePrimaria}">
										${pontoConsumo.instalacaoMedidor.vazaoCorretor.numeroSerie}</label>
							</c:otherwise>
						</c:choose>
					</display:column>
				</c:if>

			</display:table>

		</fieldset>

		<fieldset class="conteinerBotoes">
			<input name="botaoCancelar" id="botaoCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
	    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="button" onclick="salvarAssociacao()">
	 	</fieldset>

	</fieldset>

</form:form>
