<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">
	window.opener.esconderIndicador();

	animatedcollapse.addDiv('dadosMedidor', 'fade=0,speed=400,persist=1,hide=0');

	function exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(idPontoConsumo, anoMesFaturamento, numeroCiclo){
		document.forms[0].idPontoConsumo.value					= idPontoConsumo;
		document.forms[0].anoMesFaturamento.value				= anoMesFaturamento;
		document.forms[0].numeroCiclo.value						= numeroCiclo;
		
		var popupHistoricoConsumoMedidor = window.open('about:blank','popupHistoricoConsumoMedidor','height=600,width=990,toolbar=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,modal=1');

		if (window.focus) {
			popupHistoricoConsumoMedidor.focus()
		}

		exibirIndicador();
		submeter("historicoLeituraMedicaoForm", "exibirDetalhamentoHistoricoConsumoMedidoresIndependentes","popupHistoricoConsumoMedidor");
	}
</script>

<h1 class="tituloInternoPopup">Detalhamento Hist�rico Consumo<a class="linkHelp" href="<help:help>/detalhamentohistricoconsumodirio.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form name="historicoLeituraMedicaoForm" method="post">
	<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoHistConsumoMedidores"/>
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value=""/>
	<input name="idMedidor" type="hidden" id="idMedidor" value="${medidor.chavePrimaria}"/>
	<input name="anoMesFaturamento" type="hidden" id="anoMesFaturamento" value=""/>
	<input name="numeroCiclo" type="hidden" id="numeroCiclo" value=""/>
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Hist�ricos de Consumo Di�rios do Ciclo</legend>
		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosMedidor]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Medidor <img src="imagens/setaBaixo.png" border="0"/></a>
		<fieldset id="dadosMedidor" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo" id="rotuloNumeroSerie">N�mero de S�rie:</label>
				<span class="itemDetalhamento"><c:out value="${medidor.numeroSerie}"/></span><br />
				
				<label class="rotulo" id="modoUsoRotulo" for="modoUso">Perfil:</label>
				<input class="campoRadio" type="radio" name="modoUso" id=modoUsoNormal onclick="habilitarAbaComposicao();" value="${modoUsoNormal}" disabled
					<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoNormal || medidor.modoUso eq null || medidor.modoUso eq ''}">checked</c:if>>
				<label class="rotuloRadio" for="modoUsoNormal">Normal</label>
				<input class="campoRadio" type="radio" name="modoUso" id="modoUsoVirtual" onclick="habilitarAbaComposicao();" value="${modoUsoVirtual}" disabled
					<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">checked</c:if>>
				<label class="rotuloRadio" for="modoUsoVirtual">Virtual</label> 
				<input class="campoRadio" type="radio" name="modoUso" id="modoUsoIndependente" onclick="habilitarAbaComposicao();" value="${modoUsoIndependente}" disabled
					<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoIndependente}">checked</c:if>>
				<label class="rotuloRadio" for="modoUsoIndependente">Independente</label>
				 
				<label class="rotulo" id="rotuloModelo">Modelo:</label>
				<span class="itemDetalhamento"><c:out value="${medidor.modelo.descricao}"/></span><br />
				<label class="rotulo" id="rotuloTipo">Tipo:</label>
				<span class="itemDetalhamento"><c:out value="${medidor.tipoMedidor.descricao}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo" id="rotuloMarca">Marca:</label>
				<span id="itemDetalhamentoMarca" class="itemDetalhamento"><c:out value="${medidor.marcaMedidor.descricao}"/></span><br />
				<label class="rotulo" id="rotuloAnoFabricacao">Ano de Fabrica��o:</label>
				<span id="itemDetalhamentoAnoFabricacao" class="itemDetalhamento"><c:out value="${medidor.anoFabricacao}"/></span><br />
				<label class="rotulo" id="rotuloTombamento">Tombamento:</label>
				<span id="itemDetalhamentoTombamento" class="itemDetalhamento"><c:out value="${medidor.tombamento}"/></span><br />
			</fieldset>
		</fieldset>
		<!-- inserir tabela listando o Hist�rico de Medi��o -->
		<display:table id="historicoConsumo" class="dataTableGGAS dataTableMenor2 dataTableCabecalho2Linhas" export="false" name="listaHistoricoConsumo" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoHistoricoConsumoPontoConsumo">
	  		<display:column media="html" title="M�s / Ano-Ciclo">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>
	  			<c:out value='${historicoConsumo.anoMesFaturamentoFormatado}'/>-<c:out value='${historicoConsumo.numeroCiclo}'/>
		    </display:column>
				    
			<display:column media="html" title="Data Leitura">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>
				<fmt:formatDate value="${historicoConsumo.historicoAtual.dataLeituraInformada}" pattern="dd/MM/yyyy"/>
		    </display:column>	    
						    
		    <display:column media="html" title="Consumo">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>				
				<fmt:formatNumber value="${historicoConsumo.consumo}" type="number" maxFractionDigits="4"/>
		    </display:column>
		    <display:column sortable="false"  title="Fator PTZ" >
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<fmt:formatNumber value="${historicoConsumo.fatorPTZ}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
		    </display:column>
		    
		    <display:column media="html" title="Consumo Medido">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
		   		 <fmt:formatNumber value="${historicoConsumo.consumoMedido}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
		    </display:column>
		    
		    <display:column sortable="false"  title="Fator PCS" >
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
		    	<fmt:formatNumber value="${historicoConsumo.fatorPCS}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
		    </display:column>
		    
		    <display:column media="html" title="Consumo Apurado">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<fmt:formatNumber value="${historicoConsumo.consumoApurado}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
		    </display:column>
		    
		     <display:column media="html" title="Cr�dito Consumo">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<fmt:formatNumber value="${historicoConsumo.consumoCredito}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
		    </display:column>
		    
		    
		    
		
		    					
			<display:column sortable="false"  title="Fator Corre��o" >
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<fmt:formatNumber value="${historicoConsumo.fatorCorrecao}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
			</display:column>				  
		    
		    <display:column media="html" title="M�dia Refer�ncial">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
		    	<fmt:formatNumber value="${historicoConsumo.consumoApuradoMedio}" type="number" minFractionDigits="4" maxFractionDigits="4"/>
		    </display:column>
		    
		     <display:column media="html" title="Anormalidade" >
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<span alt="${historicoConsumo.anormalidadeConsumo.descricao}" title="${historicoConsumo.anormalidadeConsumo.descricao}">${historicoConsumo.anormalidadeConsumo.chavePrimaria}</span>
			</display:column>
		    
		    <display:column media="html" title="Dias de Consumo">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<c:out value='${historicoConsumo.diasConsumo}'/>
		    </display:column>
		    
		    <display:column media="html" title="Tipo de Consumo">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<c:out value='${historicoConsumo.tipoConsumo.descricao}'/>
		    </display:column>
		    
		     <display:column media="html" title="Varia��o Consumo">
		  		<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
					<a title="Detalhar Hist�rico Consumo" href="javascript:exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(${historicoConsumo.pontoConsumo.chavePrimaria},${historicoConsumo.anoMesFaturamento},${historicoConsumo.numeroCiclo})"><span class="linkInvisivel"></span>
				</c:if>	
				<fmt:formatNumber value="${historicoConsumo.consumoApuradoPercentualConsumoMedio}" maxFractionDigits="2"/>%
		    </display:column>
  		</display:table>
	</fieldset>			

	<fieldset class="conteinerBotoesPopup"> 
		<input name="Button" class="bottonRightCol2 botaoGrande1" value="Fechar" type="button" onclick="window.close();">
	</fieldset>
</form>