<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<!--[if lt IE 10]>
    <style type="text/css">
    .conteinerBotoesPesquisarDirFixo {
	    position: relative;
	    top: 5px;
	    left: 10px;
	}
    </style>
<![endif]-->

<h1 class="tituloInterno">Pesquisar Leiturista<a class="linkHelp" href="<help:help>/consultandoleiturista.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarLeiturista" name="leituristaForm"> 

<script language="javascript">

	function selecionarFuncionario(idSelecionado){
		var idFuncionario = document.getElementById("idFuncionario");
		var nomeCompletoFuncionario = document.getElementById("nomeCompletoFuncionario");
		var empresaFuncionario = document.getElementById("empresaFuncionario");
		var matriculaFuncionario = document.getElementById("matriculaFuncionario");
		
		if(idSelecionado != '') {				
			AjaxService.obterFuncionarioPorChave( idSelecionado, {
	           	callback: function(funcionario) {	           		
	           		if(funcionario != null){  	           			        		      		         		
		               	idFuncionario.value = funcionario["chavePrimaria"];
		               	matriculaFuncionario.value = funcionario["matricula"];
		               	nomeCompletoFuncionario.value = funcionario["nome"];
		               	empresaFuncionario.value = funcionario["nomeEmpresa"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idFuncionario.value = "";
        	nomeCompletoFuncionario.value = "";
        	matriculaFuncionario.value = "";
        	empresaFuncionario.value = "";
       	}
        
        document.getElementById("nomeFuncionarioTexto").value = nomeCompletoFuncionario.value;
        document.getElementById("matriculaFuncionarioTexto").value = matriculaFuncionario.value;
        document.getElementById("empresaFuncionarioTexto").value = empresaFuncionario.value;
	}

	function limparFormulario(){
		document.getElementById('idFuncionario').value = "";
        document.getElementById('nomeCompletoFuncionario').value = "";
        document.getElementById('empresaFuncionario').value = "";
        document.getElementById('matriculaFuncionario').value = "";
        
        document.getElementById('nomeFuncionarioTexto').value = "";
        document.getElementById('matriculaFuncionarioTexto').value = "";
        document.getElementById('empresaFuncionarioTexto').value = "";
        
        document.forms[0].habilitado[0].checked = true;
	}

	function incluir() {		
		location.href = '<c:url value="/exibirInclusaoLeiturista"/>';
	}

	function exibirPopupPesquisaLeiturista() {
		popup = window.open('exibirPesquisaFuncionarioPopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function removerLeiturista(){
	
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('leituristaForm', 'removerLeiturista');
			}
    	}
	}

	function detalharLeiturista(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("leituristaForm", "exibirDetalhamentoLeiturista");
	}

	function alterarLeiturista(){	
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("leituristaForm", "exibirAlteracaoLeiturista");
		}
	}

</script>

<input name="acao" type="hidden" id="acao" value="pesquisarLeiturista"/>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
<input name="chavePrimarias" type="hidden" id="chavePrimarias">
<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset class="colunaEsq pesquisarFuncionario">
		<legend>Pesquisar Funcion�rio</legend>
		<div class="conteinerDados">
			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Funcion�rio</span> para selecionar um Funcion�rio.</p>
			<input name="idFuncionario" type="hidden" id="idFuncionario" value="${funcionario.chavePrimaria}">
			<input name="nomeCompletoFuncionario" type="hidden" id="nomeCompletoFuncionario" value="${funcionario.nome}">
			<input name="empresaFuncionario" type="hidden" id="empresaFuncionario" value="${funcionario.empresa.cliente.nome}">
			<input name="matriculaFuncionario" type="hidden" id="matriculaFuncionario" value="${funcionario.matricula}">
			<input name="Button" id="botaoPesquisarFuncionario" class="bottonRightCol" title="Pesquisar Funcionario"  value="Pesquisar Funcionario" onclick="exibirPopupPesquisaLeiturista();" type="button"><br >
			
			<label class="rotulo" id="rotuloCliente" for="nomeFuncionarioTexto">Funcion�rio:</label>
			<input class="campoDesabilitado" type="text" id="nomeFuncionarioTexto" name="nomeFuncionarioTexto"  maxlength="50" size="48" disabled="disabled" value="${funcionario.nome}"><br />
			<label class="rotulo" id="rotuloCnpjTexto" for="matriculaFuncionarioTexto">Matr�cula:</label>
			<input class="campoDesabilitado" type="text" id="matriculaFuncionarioTexto" name="matriculaFuncionarioTexto"  maxlength="18" size="18" disabled="disabled" value="${funcionario.matricula}"><br />
			<label class="rotulo" id="rotuloEmailClienteTexto" for="empresaFuncionarioTexto">Empresa:</label>
			<input class="campoDesabilitado" type="text" id="empresaFuncionarioTexto" name="empresaFuncionarioTexto"  maxlength="80" size="40" disabled="disabled" value="${funcionario.empresa.cliente.nome}"><br />	
		</div>
	</fieldset>
	
	<fieldset class="leituristaCol2">
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Todos</label>
	</fieldset>
	
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<vacess:vacess param="pesquisarLeiturista">
    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
    	</vacess:vacess>		
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>

</fieldset>

<c:if test="${listaLeituristas ne null}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="listaLeituristas" sort="list" id="leiturista" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarLeiturista">
     	<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${leiturista.chavePrimaria}">
     	</display:column>
     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${leiturista.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	    <display:column sortable="true" title="Nome" style="text-align: center" sortProperty="funcionario.nome">
	    	<a href="javascript:detalharLeiturista(<c:out value='${leiturista.chavePrimaria}'/>);">
				<c:out value='${leiturista.funcionario.nome}'/>
        	</a>
	    </display:column>
	</display:table>	
</c:if>

	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaLeituristas}">
			<vacess:vacess param="exibirAlteracaoLeiturista">
				<input name="buttonRemover" value="Alterar" id="botaoAlterar" class="bottonRightCol2" onclick="alterarLeiturista()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerLeiturista">
				<input name="buttonRemover" value="Remover" id="botaoRemover" class="bottonRightCol bottonLeftColUltimo" onclick="removerLeiturista()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInclusaoLeiturista">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>

</form:form> 
