<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>



<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	


<h1 class="tituloInterno">Detalhar Corretor de vaz�o<a class="linkHelp" href="<help:help>/cadastrodocorretordevazodetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form method="post" action="exibirDetalhamentoVazaoCorretor" enctype="multipart/form-data" id="detalhamentoCorretorVazao" name="vazaoCorretorForm">

<script>
	
	function voltar(){
		submeter('vazaoCorretorForm', 'voltar');
	}
	
	function alterar(){
		submeter('vazaoCorretorForm', 'exibirAlteracaoVazaoCorretor');
	}
	
</script>

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${vazaoCorretor.chavePrimaria}">
<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">
<input name="habilitadoPesquisa" type="hidden" id="habilitadoPesquisa" value="${habilitado}">
<input name="numeroSerie" type="hidden" id="numeroSeriePesquisa" value="${vazaoCorretorTmp.numeroSerie}">
<input name="modelo" type="hidden" id="modeloPesquisa" value="${vazaoCorretorTmp.modelo.chavePrimaria}">
<input name="marcaCorretor" type="hidden" id="marcaCorretorPesquisa" value="${vazaoCorretorTmp.marcaCorretor.chavePrimaria}">

<fieldset class="detalhamento">
	<fieldset id="vazaoCorretorCol1" class="coluna detalhamentoColunaLarga">
		<label class="rotulo" id="rotuloNumeroSerie">N�mero de s�rie:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.numeroSerie}"/></span><br />
		<label class="rotulo" id="rotuloNumeroMarca">Marca:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.marcaCorretor.descricao}"/></span><br />
		<label class="rotulo" id="rotuloModelo">Modelo:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.modelo.descricao}"/></span><br />
		<label class="rotulo" id="rotuloSituacao">Situa��o:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.situacaoMedidor.descricao}"/></span><br />
		<label class="rotulo" id="rotuloAnoFabricacao" for="numeroAnoFabricacao">Ano de Fabrica��o:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.numeroAnoFabricacao}"/></span><br />
		<label class="rotulo" id="rotuloNumeroTipoMostrador">Tipo Mostrador:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.tipoMostrador.descricao}"/></span><br />
		<label class="rotulo" id="rotuloProtocoloComunicacao">Protocolo de Comunica��o:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.protocoloComunicacao.descricao}"/></span><br />
		<label class="rotulo rotulo2Linhas" id="rotuloNumeroDigitosLeituraVazaoCorretor">N�mero de D�gitos de Leitura:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${vazaoCorretor.numeroDigitos}"/></span><br />
		<label class="rotulo" id="rotuloAnoFabricacao">N�mero do Tombamento:</label>
        <span id="itemDetalhamentoAnoFabricacao" class="itemDetalhamento"><c:out value="${vazaoCorretor.tombamento}"/></span><br />
	</fieldset>
	
	<fieldset id="vazaoCorretorCol2" class="colunaFinal">		
	    <label class="rotulo" id="rotuloLocalArmazenagem">Local Armazenagem:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.localArmazenagem.descricao}"/></span>
	
		<label class="rotulo" id="rotuloTipoTransdutorPressao">Tipo de Transdutor de Press�o:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.tipoTransdutorPressao.descricao}"/> <c:out value="${vazaoCorretor.tipoTransdutorPressao.descricaoAbreviada}"/></span><br />
		
		<label class="rotulo" id="rotuloPressaoMaximaTransdutor">Press�o M�xima de Trabalho:</label>
		<span class="itemDetalhamento"><c:out value="${vazaoCorretor.pressaoMaximaTransdutor.pressao}"/> <c:out value="${vazaoCorretor.pressaoMaximaTransdutor.unidade.descricaoAbreviada}"/> </span><br />
		
		<label class="rotulo rotulo2Linhas" id="rotuloTipoTransdutorTemperatura">Tipo de Transdutor de Temperatura:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${vazaoCorretor.tipoTransdutorTemperatura.descricao}"/></span><br />
		
		<label class="rotulo rotulo2Linhas" id="rotuloTemperaturaMaximaTransdutor">Temperatura M�xima de Trabalho:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${vazaoCorretor.temperaturaMaximaTransdutor.temperatura}"/> <c:out value="${vazaoCorretor.temperaturaMaximaTransdutor.unidade.descricaoAbreviada}"/></span><br />

		<label class="rotulo" id="rotuloCorretorPressao">Corrige Press�o?</label>
		<c:choose>
			<c:when test="${vazaoCorretor.correcaoPressao == true}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${vazaoCorretor.correcaoPressao == false}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
		</c:choose>
		
		<label class="rotulo" id="rotuloCorretorTemperatura">Corrige Temperatura?</label>
		<c:choose>
			<c:when test="${vazaoCorretor.correcaoTemperatura == true}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${vazaoCorretor.correcaoTemperatura == false}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
		</c:choose>
		
		<label class="rotulo" id="rotuloControleVazao">Controla Vaz�o?</label>
		<c:choose>
			<c:when test="${vazaoCorretor.controleVazao == true}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${vazaoCorretor.controleVazao == false}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
		</c:choose>
		
	</fieldset>
	<c:if test="${empty listaVazaoCorretorHistorico == false }">
	<hr class="linhaSeparadoraDetalhamento" />
	<fieldset id="instalacaoMedidor" class="conteinerBloco">
		<a name="ancoraInstalacaoMedidor"></a>
		<legend class="conteinerBlocoTitulo">Hist�rico de Corretor de Vaz�o</legend>
		<display:table id="historicoCorretor" class="dataTableGGAS" export="false" name="listaVazaoCorretorHistorico" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
			<display:column media="html" sortProperty="pontoConsumo.descricaoFormatada" property="pontoConsumo.descricaoFormatada"  title="Ponto de Consumo" style="text-align: left; padding-left: 5px;"/>
			<display:column media="html" title="Medidor">
				<c:out value='${historicoCorretor.medidor.numeroSerie}'/>
			</display:column>
			<display:column media="html" title="Local">
				<c:out value='${historicoCorretor.localInstalacao.descricao}'/>
			</display:column>
			<display:column media="html" title="Opera��o">
				<c:out value='${historicoCorretor.operacaoMedidor.descricao}'/>
			</display:column>
			<display:column media="html" title="Motivo Opera��o">
				<c:out value='${historicoCorretor.motivoOperacaoMedidor.descricao}'/>
			</display:column>
		</display:table>
	</fieldset>
</c:if>
<hr class="linhaSeparadoraDetalhamento" />
</fieldset>

<fieldset class="detalhamento">
	<legend class="conteinerBlocoTituloGrande">Informa��es de Instala��o</legend>
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Corretor de Vaz�o</legend>
		<fieldset id="detalhamentoMedidorCol1" class="coluna detalhamentoColunaLarga">
			<label class="rotulo" id="rotuloNumeroSerie" for="numeroSerieMedidor">Data:</label>
			<span class="itemDetalhamento"><c:out value="${dataInstalacao}"/></span><br />
			
			<label class="rotulo" id="rotuloTipo">Hora:</label>
			<span class="itemDetalhamento"><c:out value="${horaInstalacao}"/></span><br />
			
			<label class="rotulo" id="rotuloMarca">Leitura:</label>
			<span class="itemDetalhamento"><c:out value="${leituraInstalacao}"/></span><br />
			
		</fieldset>
		
		<fieldset id="detalhamentoMedidorCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloCapacidadeMinima">Local:</label>
			<span class="itemDetalhamento"><c:out value="${localInstalacao}"/></span><br />
			
			<label class="rotulo" id="rotuloCapacidadeMaxima">Funcion�rio:</label>
			<span class="itemDetalhamento"><c:out value="${funcionarioInstalacao}"/></span><br />
			
			<label class="rotulo" id="rotuloAnoFabricacao" >Motivo:</label>
			<span class="itemDetalhamento"><c:out value="${motivoInstalacao}"/></span><br />
			
		</fieldset>
	</fieldset>
		
	<hr class="linhaSeparadoraDetalhamento" />

	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Cliente</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo">Nome:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
				<label class="rotulo">CPF/CNPJ:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cpfFormatado}"/><c:out value="${cnpjFormatado}"/></span>				
		</fieldset>
			
		<fieldset class="colunaFinal detalhamentoClienteImovelPontoConsumoCol">
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${clienteEnderecoFormatado}"/></span><br />
				<label class="rotulo">E-mail:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
		</fieldset>
	</fieldset>
	<hr class="linhaSeparadoraDetalhamento" />
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Im�vel</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo">Descri��o:</label>
			<input type="hidden" name="nomeFantasiaImovel" value="${pontoConsumo.imovel.nome}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.imovel.nome}"/></span><br />
			<label class="rotulo">Matr�cula:</label>
			<input type="hidden" name="matriculaImovel" value="${pontoConsumo.chavePrimaria}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.chavePrimaria}"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal detalhamentoClienteImovelPontoConsumoCol">
		<label class="rotulo">Cidade:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.imovel.quadraFace.endereco.cep.nomeMunicipio}"/></span><br />
			<label class="rotulo">Endere�o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.imovel.enderecoFormatado}"/></span>
		</fieldset>
	</fieldset>
	<hr class="linhaSeparadoraDetalhamento" />

	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Ponto de Consumo</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo" id="rotuloDataMaximaInstalacao" >Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.descricao}"/></span><br />
			<label class="rotulo" id="rotuloNumeroDigitosLeitura" >Endere�o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.enderecoFormatado}"/></span><br />
			
		</fieldset>
		
		<fieldset class="colunaFinal detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo" id="rotuloPressaoMaximaTrabalho" >CEP:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.quadraFace.endereco.cep.cep}"/></span>
				
			<label class="rotulo" id="rotuloFaixaTemperaturaTrabalho">Complemento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.descricaoComplemento}"/></span><br />
		</fieldset>
	</fieldset>
</fieldset>

	
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">    
    <vacess:vacess param="exibirAlteracaoVazaoCorretor">  
    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();" <c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
    </vacess:vacess>
</fieldset>
	
</form>
