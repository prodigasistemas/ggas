<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/js/jquery.price_format.1.3.js'></script>

<script>
$(document).ready(function(){
	$("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
});

$(function(){				
	$('#fatorK').priceFormat({limit: true, limit: 13, centsLimit: 4,  prefix: '', thousandsSeparator: '', centsSeparator: ','}); 
});

function limparFormulario(ele){
	limparFormularios(ele);
	document.getElementsByName("corretorPressao")[0].checked = true;
	document.getElementsByName("corretorTemperatura")[0].checked = true;
	document.getElementsByName("controleVazao")[0].checked = true;
// 	document.getElementsByName("linearizacaoFatorK")[0].checked = true;
}

function init () {
	
}

function cancelar() {
	location.href = '<c:url value="/exibirPesquisaVazaoCorretor"/>';
}

addLoadEvent(init);

</script>

<h1 class="tituloInterno">Incluir Corretor de Vaz�o<a class="linkHelp" href="<help:help>/corretoresdevazoinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para incluir um corretor de vaz�o, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="incluirVazaoCorretor" id="vazaoCorretorForm" name="vazaoCorretorForm">
	<fieldset id="tabs" style="display: none">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${vazaoCorretor.chavePrimaria}"/>
		
		<ul>
			<li><a href="#corretorAbaIdentificacao"><span class="campoObrigatorioSimboloTabs">* </span>Identifica��o</a></li>
			<li><a href="#corretorAbaCaracteristicas"><span class="campoObrigatorioSimboloTabs">* </span>Caracter�sticas</a></li>
		</ul>
		<fieldset id="corretorAbaIdentificacao">
			<fieldset id="corretorAbaIdentificacaoCol1" class="colunaEsq">
				<label class="rotulo campoObrigatorio2 rotuloNumeroSerieCss" id="rotuloNumeroSerie" for="numeroSerie"><span class="campoObrigatorioSimbolo">* </span>N�mero de s�rie:</label>
				<input class="campoTexto" type="text" id="numeroSerie" name="numeroSerie" maxlength="20" size="20" value="${vazaoCorretor.numeroSerie}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"><br />			
				
				<label class="rotulo campoObrigatorio3 rotuloMarcaCss" id="rotuloMarca" for="marca"><span class="campoObrigatorioSimbolo">* </span>Marca:</label>
				<select name="marcaCorretor" id="marca" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaMarca}" var="marca">
						<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${vazaoCorretor.marcaCorretor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${marca.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />

				<label class="rotulo campoObrigatorio3" id="rotuloModelo" for="modelo">Modelo:</label>
				<select name="modelo" id="modelo" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaModelo}" var="modelo">
						<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${vazaoCorretor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${modelo.descricao}"/>
						</option>		
				    </c:forEach>
				</select>
			</fieldset>
			<fieldset id="corretorAbaIdentificacaoCol2" class="colunaEsq">
				<label class="rotulo campoObrigatorio3" id="rotuloAnoFabricacao" for="numeroAnoFabricacao">Ano de Fabrica��o:</label>			
				<select name="numeroAnoFabricacao" id="numeroAnoFabricacao" class="campoSelect">
					<option value="">Selecione</option>
					<c:forEach items="${listaAnosFabricacao}" var="ano">
					<option value="<c:out value="${ano}"/>" <c:if test="${vazaoCorretor.numeroAnoFabricacao == ano}">selected="selected"</c:if>>
						<c:out value="${ano}"/>
					</option>		
				    </c:forEach>
				</select><br />
					
				<label class="rotulo campoObrigatorio3" id="rotuloTipo" for="codigoTipoMostrador">Tipo Mostrador:</label>
				<select name="tipoMostrador" id="codigoTipoMostrador" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoMostrador}" var="tipo">
						<option value="<c:out value="${tipo.codigo}"/>" <c:if test="${vazaoCorretor.tipoMostrador.codigo == tipo.codigo}">selected="selected"</c:if>>
							<c:out value="${tipo.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />
					
				<label class="rotulo campoObrigatorio3" id="rotuloProtocoloComunicacao">Protocolo de Comunica��o:</label>
				<select name="protocoloComunicacao" id="idProtocoloComunicacao" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaProtocoloComunicacao}" var="protocolo">
						<option value="<c:out value="${protocolo.chavePrimaria}"/>" <c:if test="${vazaoCorretor.protocoloComunicacao.chavePrimaria == protocolo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${protocolo.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />
				<label class="rotulo" id="rotuloTombamento" for="tombamento">N�mero do Tombamento:</label>
				<input class="campoTexto" type="text" id="tombamento" name="tombamento" maxlength="20" size="10" value="${vazaoCorretor.tombamento}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);">
			</fieldset>
			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Corretor de Vaz�o.</p>	
		</fieldset> 
		<fieldset id="corretorAbaCaracteristicas">
			<fieldset id="corretorAbaCaracteristicasCol1" class="colunaEsq">
				<label class="rotulo campoObrigatorio2" id="rotuloNumeroDigitosLeitura">N�mero de D�gitos para Leitura:</label>
				<select name="numeroDigitos" id="numeroDigitosLeituraVazaoCorretor" class="campoSelect numeroDigitosLeituraVazaoCorretorCss">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaNumeroDigitosLeitura}" var="numDigitos">
						<option value="<c:out value="${numDigitos}"/>" <c:if test="${vazaoCorretor.numeroDigitos == numDigitos}">selected="selected"</c:if>>
							<c:out value="${numDigitos}"/>
						</option>		
				    </c:forEach>
				</select><br/>
				<label class="rotulo campoObrigatorio3" id="rotuloPressaoMaximaTransdutor">Press�o M�xima de Trabalho:</label>
				<select name=pressaoMaximaTransdutor id="idPressaoMaximaTransdutor" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaPressaoMaximaTransdutor}" var="pressao">
						<option value="<c:out value="${pressao.chavePrimaria}"/>" <c:if test="${vazaoCorretor.pressaoMaximaTransdutor.chavePrimaria == pressao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${pressao.pressao} ${pressao.unidade.descricaoAbreviada}"/>
						</option>		
				    </c:forEach>
				</select><br />
				<label class="rotulo rotuloLocalArmazenagemCss" id="rotuloLocalArmazenagem" for="localArmazenagem"><span class="campoObrigatorioSimbolo">* </span>Local de Armazenagem:</label>
 				<select name="localArmazenagem" id="localArmazenagem" class="campoSelect">
					<option value="-1">Selecione</option>
						<c:forEach items="${listaLocalArmazenagem}" var="localArmazenagem">
						<option value="<c:out value="${localArmazenagem.chavePrimaria}"/>" <c:if test="${vazaoCorretor.localArmazenagem.chavePrimaria == localArmazenagem.chavePrimaria}">selected</c:if>>
						<c:out value="${localArmazenagem.descricao}"/>
						</option>		
 						</c:forEach>
				</select><br/>	
				<label class="rotulo campoObrigatorio3" id="rotuloTipoTransdutorPressao">Tipo de Transdutor de Press�o:</label>
				<select name="tipoTransdutorPressao" id="idTipoTransdutorPressao" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoTransdutorPressao}" var="transdutorPressao">
						<option value="<c:out value="${transdutorPressao.chavePrimaria}"/>" <c:if test="${vazaoCorretor.tipoTransdutorPressao.chavePrimaria == transdutorPressao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${transdutorPressao.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />
					
				<label class="rotulo" id="rotuloTipoTransdutorTemperatura">Tipo de Transdutor de Temperatura:</label>
				<select name="tipoTransdutorTemperatura" id="idTipoTransdutorTemperatura" class="campoSelect campo2Linhas idTipoTransdutorTemperaturaCss">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoTransdutorTemperatura}" var="transdutorTemperatura">
						<option value="<c:out value="${transdutorTemperatura.chavePrimaria}"/>" <c:if test="${vazaoCorretor.tipoTransdutorTemperatura.chavePrimaria == transdutorTemperatura.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${transdutorTemperatura.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />
			</fieldset>
			<fieldset id="corretorAbaCaracteristicasCol2" class="colunaEsq">
				<label class="rotulo rotulo2Linhas" id="rotuloTemperaturaMaximaTransdutor">Temperatura M�xima de Trabalho:</label>
				<select name="temperaturaMaximaTransdutor" id="idTemperaturaMaximaTransdutor" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTemperaturaMaximaTransdutor}" var="temperatura">
						<option value="<c:out value="${temperatura.chavePrimaria}"/>" <c:if test="${vazaoCorretor.temperaturaMaximaTransdutor.chavePrimaria == temperatura.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${temperatura.temperatura} ${temperatura.unidade.descricaoAbreviada}"/>
						</option>		
				    </c:forEach>
				</select><br />
					
				<label class="rotulo rotulo2Linhas" id="rotuloCorretorPressao">Corrige Press�o:</label>
				<select name="correcaoPressao" id="corretorPressao" class="campoSelect campo2Linhas corretorPressaoCss">
					<option value="">Selecione</option>
					<option value="true" ${vazaoCorretor.correcaoPressao eq true ? 'selected' : 'null'} >Sim</option>
					<option value="false" ${vazaoCorretor.correcaoPressao eq false ? 'selected' : 'null'} >N�o</option>
				</select><br />
					
				<label class="rotulo rotulo2Linhas rotuloCorretorTemperaturaCss" id="rotuloCorretorTemperatura">Corrige Temperatura:</label>
				<select name="correcaoTemperatura" id="corretorTemperatura" class="campoSelect campo2Linhas">
					<option value="">Selecione</option>
					<option value="true" ${vazaoCorretor.correcaoTemperatura eq true ? 'selected' : 'null'} >Sim</option>
					<option value="false" ${vazaoCorretor.correcaoTemperatura eq false ? 'selected' : 'null'} >N�o</option>
				</select><br />
	
				<label class="rotulo rotulo2Linhas rotuloControleVazaoCss" id="rotuloControleVazao">Controla Vaz�o:</label>
				<select name="controleVazao" id="controleVazao" class="campoSelect campo2Linhas">
					<option value="">Selecione</option>
					<option value="true" ${vazaoCorretor.controleVazao eq true ? 'selected' : 'null'} >Sim</option>
					<option value="false" ${vazaoCorretor.controleVazao eq false ? 'selected' : 'null'} >N�o</option>
				</select><br />
			</fieldset>
			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Corretor de Vaz�o.</p>
		</fieldset> 
	</fieldset>
	<!-- 				<label class="rotulo rotulo2Linhas" id="rotuloLinearizacaoFatorK">Lineariza Fator K:</label> -->
	<!-- 				<select name="linearizacaoFatorK" id="linearizacaoFatorK" class="campoSelect campo2Linhas"> -->
	<!-- 					<option value="">Selecione</option> -->
	<!-- 					<option value="true">Sim</option> -->
	<!-- 					<option value="false">N�o</option> -->
	<!-- 				</select><br /> -->
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario(this.form);">
	    <vacess:vacess param="incluirVazaoCorretor">
	    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1" value="Incluir"  type="submit">
	    </vacess:vacess>
	 </fieldset>
	<token:token></token:token>
</form:form> 