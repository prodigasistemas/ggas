<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Hist�rico dos Corretores de Vaz�o<a class="linkHelp" href="<help:help>/histricodasoperaesdemedidorconsulta.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.</p>

<form:form method="post" action="pesquisarHistoricoVazaoCorretor" id="vazaoCorretorForm" name="vazaoCorretorForm">
<script>

		
	function exibirDetalhamentoHistoricoVazaoCorretor(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("vazaoCorretorForm", "exibirDetalhamentoHistoricoVazaoCorretor");
	}
	
	function exibirDetalhamentoHistoricoVazaoImovel(idImovel){
		document.forms[0].idImovel.value = idImovel;
		submeter("vazaoCorretorForm", "exibirDetalhamentoHistoricoVazaoImovel");
	}
	
	
	function limparFormulario() {
			document.vazaoCorretorForm.matricula.value = "";	
			document.vazaoCorretorForm.cepImovel.value = "";	
	}
	
	function pesquisar(){
		var elem = document.forms[0].indicadorPesquisa;
		var selecao = getCheckedValue(elem);
		
		if(selecao == "indicadorPesquisaImovel"){
			  submeter("vazaoCorretorForm", "pesquisarHistoricoVazaoImovel");
		}else{		
			  submeter("vazaoCorretorForm", "pesquisarHistoricoVazaoCorretor");
		}	
		
	}
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			//$('.corretorVirtual').prop('disabled', true);
			modificarComportamentoCamposImovel(false);
			modificarComportamentoCamposVazao(true);
			limparCamposPesquisa();
		}else{
			//$('.corretorVirtual').prop('disabled', false);
			modificarComportamentoCamposVazao(false);
			modificarComportamentoCamposImovel(true);
			limparCamposPesquisa();
		}	
	}
	
	function modificarComportamentoCamposImovel(valor){
		document.getElementById('nome').disabled = valor;
		document.getElementById('cep').disabled = valor;
		document.getElementById('matricula').disabled = valor;
		document.getElementById('complementoImovel').disabled = valor;
		document.getElementById('numeroImovel').disabled = valor;
		document.getElementById('condominioSim').disabled = valor;
		document.getElementById('condominioNao').disabled = valor;
		document.getElementById('condominioAmbos').disabled = valor;
		document.getElementById('habilitado1').disabled = valor;
		document.getElementById('habilitado2').disabled = valor;
		document.getElementById('habilitado3').disabled = valor;
		document.getElementById('rotuloNomeImovel').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloComplemento').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloMatricula').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloNumeroImovel').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloImovelCondominio').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUso').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloCepFalse').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloCondominioSim').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloCondominioNao').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloCondominioAmbos').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUsoAtivo').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUsoInativo').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUsoTodos').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloNomeEmpresa').className = 'rotulo';
		document.getElementById('rotuloMarca').className = 'rotulo';
		document.getElementById('rotuloModelo').className = 'rotulo';
	}
	function modificarComportamentoCamposVazao(valor){
		document.getElementById('numeroSerie').disabled = valor;
		document.getElementById('marcaCorretor').disabled = valor;
		document.getElementById('modelo').disabled = valor;
		document.getElementById('rotuloNomeImovel').className = 'rotulo';
		document.getElementById('rotuloComplemento').className = 'rotulo';
		document.getElementById('rotuloMatricula').className = 'rotulo';
		document.getElementById('rotuloNumeroImovel').className = 'rotulo';
		document.getElementById('rotuloImovelCondominio').className = 'rotulo';
		document.getElementById('rotuloIndicadorUso').className = 'rotulo';
		document.getElementById('rotuloCepFalse').className = 'rotulo';
		document.getElementById('rotuloCondominioSim').className = 'rotuloRadio';
		document.getElementById('rotuloCondominioNao').className = 'rotuloRadio';
		document.getElementById('rotuloCondominioAmbos').className = 'rotuloRadio';
		document.getElementById('rotuloIndicadorUsoAtivo').className = 'rotuloRadio';
		document.getElementById('rotuloIndicadorUsoInativo').className = 'rotuloRadio';
		document.getElementById('rotuloIndicadorUsoTodos').className = 'rotuloRadio';
		document.getElementById('rotuloNomeEmpresa').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloMarca').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloModelo').className = 'rotulo rotuloDesabilitado';
	}
	
	function limparCamposPesquisa(){

		document.getElementById('nome').value = "";
		document.getElementById('complementoImovel').value = "";
		document.getElementById('matricula').value = "";
		document.forms[0].numeroImovel.value = "";
		document.forms[0].condominio[2].checked = true;
		document.forms[0].habilitado[0].checked = true;	
		document.getElementById('cep').value = "";
		//document.forms[0].corretorVirtual[2].checked = true;
		document.getElementById('numeroSerie').value = "";
		document.getElementById('marcaCorretor').value = "-1";
		document.getElementById('modelo').value = "-1";
	}
	
	function habilitarMatriculaCondominio() {	
		document.forms[0].matriculaCondominio.disabled = false;
	}
	
	function desabilitarMatriculaCondominio() {	
		document.forms[0].matriculaCondominio.value = "";
		document.forms[0].matriculaCondominio.disabled = true;
	}
	
	function manterDadosTela(){
		var elem = document.forms[0].indicadorPesquisa;
		<c:choose>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				modificarComportamentoCamposVazao(true);
			</c:when>
			<c:otherwise>
				modificarComportamentoCamposImovel(true);
			</c:otherwise>
		</c:choose>
	}
	
	function init () {
		manterDadosTela();
	}
	
	addLoadEvent(init);

</script>

	<input name="idImovel" type="hidden" id="idImovel" value="${imovel}"/>
	<input name="idVazao" type="hidden" id="idVazao" value="${vazao.chavePrimaria}"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value=""/>

	<fieldset id="consultarHistoricoInstalacaoMedidor" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarMedidor" class="colunaEsq">		
			<input class="campoRadio" type="radio" value="indicadorPesquisaVazao" name="indicadorPesquisa" id="indicadorPesquisaMedidor" <c:if test="${indicadorPesquisa eq 'indicadorPesquisaVazao'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Corretor</legend>
			<div class="pesquisarClienteFundo">
				<label class="rotulo" id="rotuloNomeEmpresa" for="nomeCompletoCliente" >N�mero de s�rie:</label>							
				<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${vazao.numeroSerie}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" /><br />
								
				<label class="rotulo" id="rotuloMarca" for="marca">Marca:</label>
				<select name="idMarca" id="marcaCorretor" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaMarca}" var="marca">
						<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${vazao.idMarca == marca.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${marca.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />	
				<label class="rotulo" id="rotuloModelo" for="modelo">Modelo:</label>
				<select name="idModelo" id="modelo" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaModelo}" var="modelo">
						<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${vazao.idModelo == modelo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${modelo.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />	
			</div>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${imovel.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>

			<div class="pesquisarImovelFundo">
				<label class="rotulo" id="rotuloNomeImovel" for="nomeImovel">Descri��o:</label>
				<input class="campoTexto" type="text" id="nome" name="nome" size="37" value="${imovel.nome}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
				<label class="rotulo" id="rotuloComplemento" for="complementoImovel">Complemento:</label>
				<input class="campoTexto" type="text" id="complementoImovel" name="descricaoComplemento" maxlength="25" size="30" value="${imovel.descricaoComplemento}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');"><br />
				<label class="rotulo" id="rotuloMatricula" for="matricula">Matr�cula:</label>
				<input class="campoTexto" type="text" id="matricula" name="matricula" maxlength="9" size="9" value="${imovel.matricula}" onkeypress="return formatarCampoInteiro(event)"><br />
				<label class="rotulo" id="rotuloNumeroImovel" for="numeroImovel">N�mero do im�vel:</label>
				<input class="campoTexto campo2Linhas" type="text" id="numeroImovel" name="numeroImovel" maxlength="9" size="9" value="${imovel.numeroImovel}" onkeypress="return formatarCampoNumeroEndereco(event, this);"><br />

				<label class="rotulo" id="rotuloImovelCondominio" for="condominioSim">O im�vel � condom�nio?</label>
				<input class="campoRadio" type="radio" value="true" name="condominio" id="condominioSim" <c:if test="${indicadorCondominioAmbos eq true}">checked</c:if> onclick="desabilitarMatriculaCondominio();"><label id="rotuloCondominioSim" class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" value="false" name="condominio" id="condominioNao" <c:if test="${indicadorCondominioAmbos eq false}">checked</c:if> onclick="habilitarMatriculaCondominio();"><label id="rotuloCondominioNao" class="rotuloRadio">N�o</label>
				<input class="campoRadio" type="radio" value="" name="condominio" id="condominioAmbos" <c:if test="${empty indicadorCondominioAmbos}">checked</c:if> onclick=""><label id="rotuloCondominioAmbos" class="rotuloRadio">Todos</label><br /><br />

				<label class="rotulo" id="rotuloIndicadorUso" for="habilitado">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${habilitado eq true}">checked</c:if>>
				<label id="rotuloIndicadorUsoAtivo" class="rotuloRadio" for="indicadorUso">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${habilitado eq false}">checked</c:if>>
				<label id="rotuloIndicadorUsoInativo" class="rotuloRadio" for="indicadorUso">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado3" value="" <c:if test="${empty habilitado}">checked</c:if>>
				<label id="rotuloIndicadorUsoTodos" class="rotuloRadio" for="habilitado">Todos</label><br /><br />

				<fieldset class="exibirCep">
					<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
						<jsp:param name="cepObrigatorio" value="false"/>
						<jsp:param name="idCampoCep" value="cep"/>
						<jsp:param name="numeroCep" value="${imovel.cep}"/>
					</jsp:include>		
				</fieldset>

			</div>

		</fieldset>
	
		<fieldset class="conteinerBotoesPesquisarDirFixoCss">
<%-- 			<vacess:vacess param="pesquisarHistoricoVazaoCorretor"> --%>
				<input name="Button" id="botaoPesquisar" class="bottonRightCol2" type="button" value="Pesquisar" onclick="pesquisar();" />
<%-- 			</vacess:vacess> --%>
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();" />
		</fieldset>
		
	</fieldset>	
		
	<c:if test="${listaImoveis ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaImoveis" id="imovel" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarHistoricoVazaoImovel">
	     	<display:column sortable="true" sortProperty="chavePrimaria" titleKey="IMOVEL_MATRICULA">
	     		<a href="javascript:exibirDetalhamentoHistoricoVazaoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${imovel.chavePrimaria}'/>
				</a>
		    </display:column>
		    <display:column sortable="true" sortProperty="nome" title="Nome" style="text-align: left">
		    	<a href="javascript:exibirDetalhamentoHistoricoVazaoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${imovel.nome}'/>
	        	</a>
		    </display:column>		     
		    <display:column sortable="false" title="Inscri��o">
		    	<a href="javascript:exibirDetalhamentoHistoricoVazaoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		    		<c:out value='${imovel.inscricao}'/>
		    	</a>
		    </display:column> 
		    <display:column sortable="false" title="Endere�o" style="text-align: left">
		    	<a href="javascript:exibirDetalhamentoHistoricoVazaoImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		    		<c:out value='${imovel.enderecoFormatado}'/>
		    	</a>
		    </display:column> 
		</display:table>
	</c:if>

<c:if test="${listaVazao ne null}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="listaVazao" sort="list" id="vazao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarHistoricoVazaoCorretor">
        
        <display:column sortable="true"  sortProperty="numeroSerie" title="N�mero de S�rie">
        	<a href="javascript:exibirDetalhamentoHistoricoVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
            	<c:out value="${vazao.numeroSerie}"/>
            </a>
        </display:column>
        
        
        <display:column sortable="true" title="Situa��o" >
            <a href="javascript:exibirDetalhamentoHistoricoVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
                <c:out value="${vazao.situacaoMedidor.descricao}"/>
            </a>         
        </display:column>
        
        
        <display:column sortable="true" title="Modelo" >
            <a href="javascript:exibirDetalhamentoHistoricoVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
                <c:out value="${vazao.modelo.descricao}"/>
            </a>         
        </display:column>
        
        <display:column sortable="true" title="Marca" >        	
            <a href="javascript:exibirDetalhamentoHistoricoVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
                <c:out value="${vazao.marcaCorretor.descricao}"/>
            </a>                
        </display:column>
        
        <display:column sortable="true" title="Ano de fabrica��o" >
        	<a href="javascript:exibirDetalhamentoHistoricoVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">        	
                <c:out value="${vazao.numeroAnoFabricacao}"/>
            </a>                
        </display:column>
        
        <display:column sortable="true" title="Tombamento" >
        	<a href="javascript:exibirDetalhamentoHistoricoVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
        	   <c:out value="${vazao.tombamento}"/>
            </a>
        </display:column>
    </display:table>	
</c:if>


</form:form>