<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	

<h1 class="tituloInternoPopup">Pesquisar Corretor de Vaz�o<a class="linkHelp" href="<help:help>/consultandocorretordevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="/pesquisarCorretorVazaoPopup" id="pesquisaCorretorVazaoForm" name="pesquisaCorretorVazaoForm">
<input name="idSelecionado" type="hidden" id="idSelecionado" value="">
<input name="habilitado" type="hidden" id="habilitado" value="true">
<input type="hidden" id="pontoConsumo" name="pontoConsumo" value="${pontoConsumo}" />
<input type="hidden" id="lote" name="lote" value="${lote}" />

<script language="javascript">

	window.opener.esconderIndicador();
	
	function selecionarNoLink(id) {
		<c:choose>
		<c:when test="${lote == true}">
		window.opener.obterCorretorVazao(id, ${pontoConsumo});
		</c:when>
		<c:otherwise>
		window.opener.obterCorretorVazao(id);
		</c:otherwise>
		</c:choose>
		window.close();
	}
	
	function selecionar(elem){
		document.getElementById('idSelecionado').value = elem.value;	
	}
	
	function obterCorretorVazao() {
		window.opener.obterCorretorVazao(document.getElementById('idSelecionado').value);
		window.close();
	}
	
	function limparFormulario(){
		document.pesquisaCorretorVazaoForm.numeroSerie.value = "";
		document.pesquisaCorretorVazaoForm.idMarca.value = "-1";
		document.pesquisaCorretorVazaoForm.idModelo.value = "-1";	
	}
	
	function pesquisarCorretorVazao() {
		$("#botaoPesquisar").attr('disabled','disabled');
		submeter('pesquisaCorretorVazaoForm', 'pesquisarCorretorVazaoPopup');
		}
	
	function init() {
		<c:if test="${listaVazao ne null}">
			$.scrollTo($('#vazao'),800);
		</c:if>
	}
	addLoadEvent(init);

</script>

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisaVazaoCorretorCol1" class="coluna">
		<label class="rotulo" id="rotuloNumeroSerie" for="numeroSerie" >N�mero de s�rie:</label>
		<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${vazaoCorretor.numeroSerie}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumerico(event)');" onkeyup="letraMaiuscula(this);"/><br />
		<label class="rotulo" id="rotuloModelo" for="modelo">Modelo:</label>
		<select name=modelo id="idModelo" class="campoSelect">
			<option value="-1">Selecione</option> 
			<c:forEach items="${listaModelo}" var="modelo">
				<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${vazaoCorretor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modelo.descricao}"/>
				</option>		
		    </c:forEach>
		</select><br />
	</fieldset>
	
	<fieldset id="pesquisaVazaoCorretorCol2" class="colunaFinal">
		<label class="rotulo" id="rotuloMarca" for="marca">Marca:</label>
		<select name="marcaCorretor" id="idMarca" class="campoSelect">
			<option value="-1">Selecione</option> 
			<c:forEach items="${listaMarca}" var="marca">
				<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${vazaoCorretor.marcaCorretor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${marca.descricao}"/>
				</option>		
		    </c:forEach>
		</select><br />	
	</fieldset>
	<fieldset class="conteinerBotoesPopup">	
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="window.close();">
		<input name="Button" class="bottonRightCol" value="Limpar" type="button" onclick="limparFormulario();">
		<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarCorretorVazao();">
	</fieldset>
</fieldset>
<input type="hidden" id="retornaId" name="retornaId" value="${retornaId}" />
<c:if test="${listaVazao ne null}">
	<c:choose>
		<c:when test="${retornaId}">
			<display:table class="dataTableGGAS dataTablePopup" name="listaVazao" sort="list" id="vazao" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarCorretorVazaoPopup">
		        <display:column sortable="true"  sortProperty="numeroSerie" title="N�mero de S�rie">
			        <a href="javascript:selecionarNoLink('<c:out value="${vazao.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
		            	<c:out value="${vazao.numeroSerie}"/>
		            </a>
		        </display:column>
		        <display:column sortable="true" title="Modelo" style="width: 15.5%">
			        <a href="javascript:selecionarNoLink('<c:out value="${vazao.chavePrimaria}"/>');"><span class="linkInvisivel"></span>        
		            	<c:out value="${vazao.modelo.descricao}"/>
		            </a>        
		        </display:column>
		        <display:column sortable="true" title="Marca"  style="width: 15.5%">
					<a href="javascript:selecionarNoLink('<c:out value="${vazao.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
		            	<c:out value="${vazao.marcaCorretor.descricao}"/>
		            </a>                        
		        </display:column>
		        <display:column sortable="true" title="Ano de fabrica��o"  style="width: 20%">
			        <a href="javascript:selecionarNoLink('<c:out value="${vazao.chavePrimaria}"/>');"><span class="linkInvisivel"></span>                	
		            	<c:out value="${vazao.numeroAnoFabricacao}"/>
					</a>
		        </display:column>
		    </display:table>	
		</c:when>
		<c:otherwise>
			<display:table class="dataTableGGAS dataTablePopup" name="listaVazao" sort="list" id="vazao" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarCorretorVazaoPopup">
		        <display:column sortable="true"  sortProperty="numeroSerie" title="N�mero de S�rie">
			        <a href="javascript:selecionarNoLink('<c:out value="${vazao.numeroSerie}"/>');"><span class="linkInvisivel"></span>
		            	<c:out value="${vazao.numeroSerie}"/>
		            </a>
		        </display:column>
		        <display:column sortable="true" title="Modelo" style="width: 15.5%">
			        <a href="javascript:selecionarNoLink('<c:out value="${vazao.numeroSerie}"/>');"><span class="linkInvisivel"></span>        
		            	<c:out value="${vazao.modelo.descricao}"/>
		            </a>        
		        </display:column>
		        <display:column sortable="true" title="Marca"  style="width: 15.5%">
					<a href="javascript:selecionarNoLink('<c:out value="${vazao.numeroSerie}"/>');"><span class="linkInvisivel"></span>
		            	<c:out value="${vazao.marcaCorretor.descricao}"/>
		            </a>                        
		        </display:column>
		        <display:column sortable="true" title="Ano de fabrica��o"  style="width: 20%">
			        <a href="javascript:selecionarNoLink('<c:out value="${vazao.numeroSerie}"/>');"><span class="linkInvisivel"></span>                	
		            	<c:out value="${vazao.numeroAnoFabricacao}"/>
					</a>
		        </display:column>
		    </display:table>	
		</c:otherwise>
	</c:choose>
</c:if>

</form:form>
