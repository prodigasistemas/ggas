<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>



<script>

	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,persist=1,hide=0');
	
	
	
    function exibirDetalhamentoHistoricoOperacaoVazaoCorretor(chave){
		
		if(chave==null){
			alert('Corretor de Vaz�o Inexistente');
		}
		else{
           document.forms[0].chavePrimaria.value = chave;
		   submeter("vazaoCorretorForm", "exibirDetalhamentoHistoricoOperacaoVazaoCorretor");
		}
	}
	
    
    function exibirHistoricoVazaoCorretorPontoConsumo(idPontoConsumo) {
		document.forms[0].idPontoConsumo.value = idPontoConsumo;
		submeter("vazaoCorretorForm", "exibirDetalhamentoHistoricoVazaoCorretorImovel");
	}
    
    function exibirHistoricoVazaoCorretor(chave){
    	document.forms[0].chavePrimaria.value = chave;
    	submeter("vazaoCorretorForm", "exibirDetalhamentoHistoricoVazaoCorretor");
    }
    
	
	function voltar() {
		<c:choose>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				submeter("vazaoCorretorForm", "voltarHistorico");
			</c:when>
			<c:otherwise>
				submeter("vazaoCorretorForm", "voltarHistorico");
			</c:otherwise>
		</c:choose>
	}

</script>
<h1 class="tituloInterno">Hist�rico dos Corretores de Vaz�o - Sele��o de Ponto de Consumo<a class="linkHelp" href="<help:help>/histricodasoperaesdomedidorporpontodeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<form:form styleId="formHistoricooInstalacaoMedidorPontoConsumo" method="post" action="exibirDetalhamentoHistoricoVazaoImovel" id="vazaoCorretorForm" name="vazaoCorretorForm">

<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}"/>
<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value=""/>

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value=""/>

<fieldset class="detalhamento">

	<a class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
	<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
		<fieldset class="coluna">
			<label class="rotulo">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.nome}"/></span><br />
			<label class="rotulo">Endere�o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.enderecoFormatado}"/></span><br />
		</fieldset>
		<fieldset class="colunaFinal">
			<label class="rotulo">CEP:</label>
			<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.cep}"/></span><br />
			<label class="rotulo">Complemento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.descricaoComplemento}"/></span><br />
		</fieldset>
	</fieldset>
</fieldset>	

<hr class="linhaSeparadora1" />	

<fieldset id="pontoVO" class="detalhamento">
		
   <c:if test="${listaPontosConsumo ne null}">
        <display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontosConsumo" sort="list" id="pontoVO" pagesize="15" size="${pageSize}" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoHistoricoVazaoImovel">
        
			<hr class="linhaSeparadora1" />
	  	
	  			<display:column media="html" title="Ponto de Consumo" style="auto">
	  				<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>
					</a>
			    </display:column>
		
			    <display:column media="html" sortable="false" title="Descri��o" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.descricao}'/>
		        	</a>
			    </display:column>
			   
			    <display:column media="html" title="Situa��o" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.situacaoConsumo.descricao}'/>
			    	</a>
			    </display:column>
			   
			    <display:column media="html" sortable="false" title="Medidor" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.instalacaoMedidor.medidor.numeroSerie}'/>
					</a>
			    </display:column> 
			   
			   
			   <display:column media="html" sortable="false" title="Corretor" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.instalacaoMedidor.vazaoCorretor.numeroSerie}'/>
					</a>
			    </display:column>
			   
			   
			    <display:column media="html" title="Refer�ncia / Ciclo" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.referencia}'/>-<c:out value='${pontoVO.ciclo}'/>
			    	</a>
			    </display:column>
			    <display:column media="html" title="Grupo de<br />faturamento" style="auto">
			   	<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.rota.grupoFaturamento.descricao}'/>
			    	</a>
			    </display:column> 
			    <display:column media="html" title="Rota" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.rota.numeroRota}'/>
			    	</a>
			    </display:column>
			    <display:column media="html" title="Sequ�ncia<br />de leitura" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.numeroSequenciaLeitura}'/>
			    	</a>
			    </display:column>
			    <display:column media="html" title="Empresa leiturista" style="auto">
			    		<a href='javascript:exibirHistoricoVazaoCorretorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.rota.empresa.cliente.nome}'/>
					</a>
			    </display:column>
			</display:table>		
		</c:if>
	
	<c:if test="${not empty listaVazaoCorretorHistorico || not empty pontoConsumo}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset id="instalacaoMedidor" class="conteinerBloco">
			<a name="ancoraVazaoCorretor"></a>
			<legend class="conteinerBlocoTitulo">Hist�rico de Opera��o dos Corretores de Vaz�o no Ponto de Consumo</legend>

			<display:table id="listaVazaoCorretor" class="dataTableGGAS" export="false" name="listaVazaoCorretorHistorico" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirHistoricoInstalacaoMedidorImovel">
		     	<display:column media="html" title="N�mero de S�rie"  style="auto">
		     		<a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${listaVazaoCorretor.vazaoCorretor.numeroSerie}'/>
			        </a>
			    </display:column>
			    
			    <display:column media="html" title="Opera��o" style="auto">
			    	<a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${listaVazaoCorretor.operacaoMedidor.descricao}'/>
			    	</a>
		    	</display:column>
			    
			    <display:column media="html" title="Data" style="auto">
			    	<a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
		 				<fmt:formatDate value="${listaVazaoCorretor.dataRealizada}" type="both" pattern="dd/MM/yyyy" />
		 			</a>
			    </display:column>
			    
			    <display:column media="html" title="Leitura" style="auto">
			    	<a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
		 				<c:out value='${listaVazaoCorretor.leitura}'/>
		 			</a>
			    </display:column>
			    
- 			    <display:column media="html" title="Local de Instala��o" style="auto"> 
 			   		<a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span> 
 		 				<c:out value='${listaVazaoCorretor.localInstalacao.descricao}'/> 
		 			</a>  
 			    </display:column> 

			    <display:column media="html" title="Funcion�rio" style="auto">
			   		<a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
		 				<c:out value='${listaVazaoCorretor.funcionario.nome}'/>
		 			</a> 
			    </display:column>
			    
			    <display:column media="html" title="Press�o" style="auto">
			    	<a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
				    	<c:out value='${listaVazaoCorretor.vazaoCorretor.pressaoMaximaTransdutor.pressao}'/>
				    	<c:out value='${listaVazaoCorretor.vazaoCorretor.pressaoMaximaTransdutor.unidade.descricaoAbreviada}'/>
				    </a> 
		    	</display:column>
		    	
		    	<display:column media="html" title="Motivo" style="auto">
		    	    <a href='javascript:exibirHistoricoVazaoCorretor(<c:out value='${listaVazaoCorretor.vazaoCorretor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
					    	<c:out value='${listaVazaoCorretor.motivoOperacaoMedidor.descricao}'/>
					</a>    	
			    </display:column>
			    
		    </display:table>
 		</fieldset> 
	</c:if>	

	</fieldset>	
	<br/>

	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
	</fieldset>
</form:form>