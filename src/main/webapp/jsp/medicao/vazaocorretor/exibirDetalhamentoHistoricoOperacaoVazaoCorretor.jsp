<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Hist�rico de Opera��o dos Corretores de Vaz�o - Detalhamento<a class="linkHelp" href="<help:help>/histricodasoperaesdomedidor.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="exibirDetalhamentoHistoricoOperacaoVazaoCorretor" id="vazaoCorretorForm" name="vazaoCorretorForm">
	
	<script>

	animatedcollapse.addDiv('dadosVazao', 'fade=0,speed=400,persist=1,hide=0');
	
	
	function voltar() {
		submeter("vazaoCorretorForm", "exibirDetalhamentoHistoricoVazaoImovel");
	}
	
	
	

   </script>
	
	

	<input name="idImovel" type="hidden" id="idImovel" value="${vazaoCorretor.idImovel}"/>
	<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value=""/>
    <input name="indicadorPesquisa" type="hidden" id="indicadorPesquisa" value="${vazaoCorretor.indicadorPesquisa}"/>
	

	<fieldset class="detalhamento">

		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosVazao]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Corretor <img src="imagens/setaBaixo.png" border="0"/></a>
		<fieldset id="dadosVazao" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo" id="rotuloNumeroSerie">N�mero de S�rie:</label>
				<span class="itemDetalhamento"><c:out value="${vazao.numeroSerie}"/></span><br />
				<label class="rotulo" id="rotuloModelo">Modelo:</label>
				<span class="itemDetalhamento"><c:out value="${vazao.modelo.descricao}"/></span><br />
				<label class="rotulo" id="rotuloModelo">Situa��o:</label>
				<span class="itemDetalhamento"><c:out value="${vazao.situacaoMedidor.descricao}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo" id="rotuloMarca">Marca:</label>
				<span id="itemDetalhamentoMarca" class="itemDetalhamento"><c:out value="${vazao.marcaCorretor.descricao}"/></span><br />
				<label class="rotulo" id="rotuloAnoFabricacao">Ano de Fabrica��o:</label>
				<span id="itemDetalhamentoAnoFabricacao" class="itemDetalhamento"><c:out value="${vazao.numeroAnoFabricacao}"/></span><br />
				<label class="rotulo" id="rotuloAnoFabricacao">Tombamento:</label>
				<span id="itemDetalhamentoAnoFabricacao" class="itemDetalhamento"><c:out value="${vazao.tombamento}"/></span><br />
			</fieldset>
		</fieldset>
	</fieldset>
	<hr class="linhaSeparadora1" />
	<br/>
	
	<fieldset>
	<c:if test="${listaVazaoCorretorHistorico ne null}">
      <display:table class="dataTableGGAS dataTableAba2" name="listaVazaoCorretorHistorico" sort="list" id="vazao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoHistoricoOperacaoVazaoCorretor">
			<hr class="linhaSeparadora1" />
		  
			     	<display:column media="html" title="N�mero de S�rie"  style="text-align: center;">
			     			<c:out value='${vazao.vazaoCorretor.numeroSerie}'/>
				    </display:column>
				    
				    <display:column media="html" title="Opera��o" style="width: 75px">
				    		<c:out value='${vazao.operacaoMedidor.descricao}'/>
			    	</display:column>
				    
				    <display:column media="html" title="Data" style="width: 75px">
			 				<fmt:formatDate value="${vazao.dataRealizada}" type="both" pattern="dd/MM/yyyy" />
				    </display:column>
				    
				    <display:column media="html" title="Leitura" style="width: 75px">
			 				<c:out value='${vazao.leitura}'/>
				    </display:column>
				    				    
				    <display:column media="html" title="Funcion�rio" style="width: 150px">
			 				<c:out value='${vazao.funcionario.nome}'/>
				    </display:column>
				    				    
                    <display:column media="html" title="Press�o" >
                         <c:out value="${vazao.vazaoCorretor.pressaoMaximaTransdutor.pressao}"/>
                         <c:out value="${vazao.vazaoCorretor.pressaoMaximaTransdutor.unidade.descricaoAbreviada}"/>
                    </display:column>				    				    
				  
			</display:table>
		</c:if>
	</fieldset>
	
	
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
	</fieldset>
</form:form>