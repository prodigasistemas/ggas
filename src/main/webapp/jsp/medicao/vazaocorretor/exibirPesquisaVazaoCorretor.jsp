<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	


<h1 class="tituloInterno">Pesquisar Corretor de vaz�o<a class="linkHelp" href="<help:help>/consultadoscorretoresdevazo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarVazaoCorretor" id="vazaoCorretorForm" name="vazaoCorretorForm">

<script language="javascript">

function excluirVazaoCorretor(){
	
	var selecao = verificarSelecao();
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('vazaoCorretorForm', 'excluirVazaoCorretor');
		}
    }
}

function alterarVazaoCorretor(){
	
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('vazaoCorretorForm', 'exibirAlteracaoVazaoCorretor');
    }
}

function detalharVazaoCorretor(chave){
	document.forms[0].chavePrimaria.value = chave;
	submeter("vazaoCorretorForm", "exibirDetalhamentoVazaoCorretor");
}

function incluir() {
	location.href = '<c:url value="/exibirInclusaoVazaoCorretor"/>';
}

function limparFormulario(){	
	limparFormularios(vazaoCorretorForm);
	document.forms[0].habilitado[0].checked = true;
}

</script>

<input name="chavePrimaria" type="hidden" id="chavePrimaria" >

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="pesquisaVazaoCorretorCol1" class="coluna">
		<label class="rotulo" id="rotuloNumeroSerie" for="numeroSerie" >N�mero de s�rie:</label>
		<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${vazaoCorretor.numeroSerie}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumerico(event)');" onkeyup="letraMaiuscula(this);"/><br />
		<label class="rotulo" id="rotuloModelo" for="modelo">Modelo:</label>
		<select name="modelo" id="modelo" class="campoSelect">
			<option value="-1">Selecione</option> 
			<c:forEach items="${listaModelo}" var="modelo">
				<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${vazaoCorretor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${modelo.descricao}"/>
				</option>		
		    </c:forEach>
		</select><br />
	</fieldset>
	
	<fieldset id="pesquisaVazaoCorretorCol2" class="colunaFinal">
		<label class="rotulo" id="rotuloMarca" for="marca">Marca:</label>
		<select name="marcaCorretor" id="marca" class="campoSelect">
			<option value="-1">Selecione</option> 
			<c:forEach items="${listaMarca}" var="marca">
				<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${vazaoCorretor.marcaCorretor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${marca.descricao}"/>
				</option>		
		    </c:forEach>
		</select><br />
		<label class="rotulo" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq true}">checked = "checked"</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq false}">checked = "checked"</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked = "checked"</c:if>>
		<label class="rotuloRadio" for="habilitado">Todos</label>
	</fieldset>
	
	<fieldset class="conteinerBotoesPesquisarDirFixo">
		<vacess:vacess param="pesquisarVazaoCorretor">
    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
    	</vacess:vacess>		
		<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
	</fieldset>
</fieldset>

<c:if test="${listaVazao ne null}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="listaVazao" sort="list" id="vazao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarVazaoCorretor">
        <display:column style="text-align: center;" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${vazao.chavePrimaria}">
        </display:column>
        <display:column title="Ativo">
	     	<c:choose>
				<c:when test="${vazao.habilitado == true}">
					<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
				</c:when>
				<c:otherwise>
					<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
				</c:otherwise>				
			</c:choose>
		</display:column>
        <display:column sortable="true"  sortProperty="numeroSerie" title="N�mero de S�rie">
        	<a href="javascript:detalharVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
            	<c:out value="${vazao.numeroSerie}"/>
            </a>
        </display:column>
        <display:column sortable="true" title="Modelo" >
            <a href="javascript:detalharVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
            	<c:out value="${vazao.modelo.descricao}"/>
            </a>
        </display:column>
        <display:column sortable="true" title="Marca" >
			<a href="javascript:detalharVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
            	<c:out value="${vazao.marcaCorretor.descricao}"/>
            </a>            
        </display:column>
        <display:column sortable="true" title="Ano de fabrica��o" >        	
            <a href="javascript:detalharVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">
            	<c:out value="${vazao.numeroAnoFabricacao}"/>
            </a>            
        </display:column>
        <display:column sortable="true" title="Press�o M�xima" >
        	<c:if test="${vazao.pressaoMaximaTransdutor ne null}">
        		<a href="javascript:detalharVazaoCorretor(<c:out value='${vazao.chavePrimaria}'/>);">        	
            		<c:out value="${vazao.pressaoMaximaTransdutor.pressao} ${vazao.pressaoMaximaTransdutor.unidade.descricaoAbreviada}"/>
            	</a>
            </c:if>            
        </display:column>
    </display:table>	
</c:if>

<fieldset class="conteinerBotoes">
	<c:if test="${not empty listaVazao}">
		<vacess:vacess param="exibirAlteracaoVazaoCorretor">
			<input id="botaoAlterar" name="buttonRemover" value="Alterar" type="button" 
				class="bottonRightCol2" onclick="alterarVazaoCorretor()" 
				<c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
		</vacess:vacess>
		<vacess:vacess param="excluirVazaoCorretor">
			<input id="botaoRemover" name="buttonRemover" value="Remover" 
				class="bottonRightCol2 bottonLeftColUltimo" 
				onclick="excluirVazaoCorretor()" type="button"
				<c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
		</vacess:vacess>
	</c:if>
	<vacess:vacess param="exibirInclusaoVazaoCorretor">
		<input id="botaoIncluir" name="button" value="Incluir" 
			class="bottonRightCol2 botaoGrande1" 
			onclick="incluir()" type="button"
			<c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
	</vacess:vacess>
</fieldset>
<token:token></token:token>
</form:form>
