<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Hist�rico dos Corretores de Vaz�o - Detalhamento<a class="linkHelp" href="<help:help>/histricodasoperaesdomedidor.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="exibirDetalhamentoHistoricoVazaoCorretor" id="vazaoCorretorForm" name="vazaoCorretorForm">
	
	<script>

	animatedcollapse.addDiv('dadosVazao', 'fade=0,speed=400,persist=1,hide=0');
	
	
	function voltar() {
		<c:choose>
			<c:when test="${indicadorPesquisa eq 'indicadorPesquisaVazao'}">
				submeter("vazaoCorretorForm", "voltarHistorico");
			</c:when>
			<c:otherwise>
				submeter("vazaoCorretorForm", "voltarHistorico");
			</c:otherwise>
		</c:choose>
	}
	

   </script>
	<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}"/>
	<%-- 
	
	<input name="idVazao" type="text" id="idVazao" value="<c:out value="${vazao.chavePrimaria}"/>"/>
	<input name="indicadorPesquisa" type="text" id="indicadorPesquisa" value="${indicadorPesquisa}"/>
	<input name="condominio" type="text" id="condominio" value="${indicadorCondominioAmbos}"/>
	<input name="habilitado" type="text" id="habilitado" value="${habilitado}"/>
	
	<input name="numeroSerie" type="text" id="numeroSerie" value="${vazaoCorretorTmp.numeroSerie}" />
	<input name="marcaCorretor" type="text" id="marcaCorretor" value="${vazaoCorretorTmp.marcaCorretor}" />
	<input name="modelo" type="text" id="modelo" value="${vazaoCorretorTmp.modelo}" />
	--%>
	
	<fieldset class="detalhamento">

		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosVazao]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Corretor <img src="imagens/setaBaixo.png" border="0"/></a>
		<fieldset id="dadosVazao" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo" id="rotuloNumeroSerie">N�mero de S�rie:</label>
				<span class="itemDetalhamento"><c:out value="${vazao.numeroSerie}"/></span><br />
				<label class="rotulo" id="rotuloModelo">Modelo:</label>
				<span class="itemDetalhamento"><c:out value="${vazao.modelo.descricao}"/></span><br />
				<label class="rotulo" id="rotuloModelo">Situa��o:</label>
				<span class="itemDetalhamento"><c:out value="${vazao.situacaoMedidor.descricao}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo" id="rotuloMarca">Marca:</label>
				<span id="itemDetalhamentoMarca" class="itemDetalhamento"><c:out value="${vazao.marcaCorretor.descricao}"/></span><br />
				<label class="rotulo" id="rotuloAnoFabricacao">Ano de Fabrica��o:</label>
				<span id="itemDetalhamentoAnoFabricacao" class="itemDetalhamento"><c:out value="${vazao.numeroAnoFabricacao}"/></span><br />
				<label class="rotulo" id="rotuloAnoFabricacao">Tombamento:</label>
				<span id="itemDetalhamentoAnoFabricacao" class="itemDetalhamento"><c:out value="${vazao.tombamento}"/></span><br />
			</fieldset>
		</fieldset>
	</fieldset>
	<hr class="linhaSeparadora1" />
	<br/><br/><br/>
	<fieldset id="tabs">
		<ul>
			<li><a href="#operacoesMedidor">Opera��es</a></li>
			<li><a href="#movimentacoesMedidor">Movimenta��es</a></li>
		</ul>
		<fieldset id="operacoesMedidor">
			<jsp:include page="/jsp/medicao/vazaocorretor/abaOperacoes.jsp">
				<jsp:param name="actionAdicionarFuncao" value="historicoCorretorFluxoOperacoes" />
			</jsp:include>
		</fieldset>
		<fieldset id="movimentacoesMedidor">
			<jsp:include page="/jsp/medicao/vazaocorretor/abaMovimentacoes.jsp">
				<jsp:param name="actionAdicionarParametro" value="/historicoCorretorFluxoMovimentacoes" />				
			</jsp:include>
		</fieldset>
   </fieldset>
	
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="history.back();">
	</fieldset>
</form:form>	