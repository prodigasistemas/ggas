<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script language="javascript">

	window.opener.esconderIndicador();

	$(document).ready(function(){	
		/*Executa a pesquisa quando a tecla ENTER � pressionada, 
		caso o cursor esteja em um dos campos do formul�rio*/
		$('#pesquisarMedidorPopup>:text, #pesquisarMedidorPopup>:radio').keypress(function(event) {
			if (event.keyCode == '13') {
				pesquisarCliente();
			}
	   	});
		
		if($("#indicador").val()==""){
			document.pesquisaMedidorForm.modoUso[0].checked = true;
		}
	});
	
	
	function selecionar(elem){
		document.getElementById('idSelecionado').value = elem.value;	
	}

	function selecionarNoLink(id) {
		<c:choose>
		<c:when test="${lote == true}">
			window.opener.obterMedidor(id, ${pontoConsumo});
		</c:when>
		<c:otherwise>
			window.opener.obterMedidor(id);
		</c:otherwise>
		</c:choose>
		window.close();
	}
	
	function obterMedidor(id) {
		window.opener.obterMedidor(document.getElementById('idSelecionado').value);
		window.close();
	}
		
	function limparFormulario() {
		document.pesquisaMedidorForm.numeroSerie.value = "";
		document.pesquisaMedidorForm.tipo.value = "-1";
		document.pesquisaMedidorForm.marca.value = "-1";
		document.pesquisaMedidorForm.modelo.value = "-1";
		document.pesquisaMedidorForm.modoUso[0].checked = true;
		document.pesquisaMedidorForm[0].habilitado[0].checked = true;
	}

	function pesquisarImovel() {
		$("#botaoPesquisar").attr('disabled','disabled');
		$('form[name="pesquisaMedidorForm"] :input[name="listaChavePrimaria"]').remove();
		$('form[name="pesquisaMedidorForm"]').append(window.opener.$('form[name="medidorForm"] :input[name="listaChavePrimaria"]').clone());
		submeter('pesquisaMedidorForm', 'pesquisarMedidorPopup');
  	}

	function init() {
		<c:if test="${listaMedidor ne null}">
			$.scrollTo($('#medidor'),800);
		</c:if>
	}
	addLoadEvent(init);

</script>


<h1 class="tituloInternoPopup">Pesquisar Medidor<a class="linkHelp" href="<help:help>/consultandomedidor.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicialPopup">Para pesquisar um medidor, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form:form method="post" action="pesquisarMedidorPopup" id="pesquisaMedidorForm" name="pesquisaMedidorForm"> 

	<input name="idSelecionado" type="hidden" id="idSelecionado" value="">
	<input name="habilitado" type="hidden" id="habilitado" value="true">
	<input type="hidden" id="indicador" name="indicador" value="${modoUso}" />
	<input type="hidden" id="pontoConsumo" name="pontoConsumo" value="${pontoConsumo}" />
	<input type="hidden" id="lote" name="lote" value="${lote}" />
	
	<fieldset id="pesquisaMedidorPopup" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna pesquisaMedidorPopupCol1">
			<label class="rotulo" id="rotuloNomeEmpresa" for="nomeCompletoCliente" >N�mero de s�rie:</label>
			<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${medidor.numeroSerie}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/><br />
			
			<label class="rotulo" id="rotuloTipo" for="tipo">Tipos:</label>
			<select name="tipoMedidor" id="tipo" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoMedidor}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${medidor.tipoMedidor.chavePrimaria == tipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipo.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			<input type="hidden" id="composicaoVirtual" name="composicaoVirtual" value="${composicaoVirtual}" />
			
			<c:if test="${composicaoVirtual}">
				<c:set var="disabled" value="disabled" />				
				<input type="hidden" id="modoUso" name="modoUso" value="${modoUsoIndependente}" />
			</c:if>
			
			<label class="rotulo" id="modoUsoRotulo" for="modoUso">Perfil:</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" ${disabled} value="${modoUsoNormal}" <c:if test="${modoUso eq modoUsoNormal}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Normal</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" ${disabled} value="${modoUsoVirtual}" <c:if test="${modoUso eq modoUsoVirtual}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Virtual</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" ${disabled} value="${modoUsoIndependente}" <c:if test="${modoUso eq modoUsoIndependente || composicaoVirtual eq true}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Independente</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" ${disabled} value="null"
				<c:if test="${(modoUso eq 'null') 
					&& (composicaoVirtual eq null || composicaoVirtual eq false || composicaoVirtual eq '' || empty(composicaoVirtual))}">checked</c:if>>

			<label class="rotuloRadio" for="modoUso">Todos</label>
			
			

			
		</fieldset>

		<fieldset class="colunaFinal">
			<label class="rotulo" id="rotuloMarca" for="marca">Marca:</label>
			<select name="marcaMedidor" id="marca" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMarcaMedidor}" var="marca">
					<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${medidor.marcaMedidor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${marca.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />	
			
			<label class="rotulo" id="rotuloModelo" for="modelo">Modelo:</label>
			<select name="modelo" id="modelo" class="campoSelect">
				<option value="-1">Selecione</option> 
				<c:forEach items="${listaModeloMedidor}" var="modelo">
					<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${medidor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${modelo.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
		</fieldset>								
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input name="ButtonCancelar" class="bottonRightCol" value="Cancelar" type="button" onclick="window.close();">
		<input name="ButtonLimpar" class="bottonRightCol" value="Limpar" type="button" onclick="limparFormulario();">
		<input name="buttonPesquisar" class="bottonRightCol2 botaoGrande1" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarImovel();">
	</fieldset>
	<input type="hidden" id="retornaId" name="retornaId" value="${retornaId}" />
	<c:if test="${listaMedidor ne null}">
		<c:choose>
			<c:when test="${retornaId}">
				<display:table class="dataTableGGAS dataTablePopup" name="listaMedidor" id="medidor" partialList="true" sort="external" pagesize="15" size="${pageSize}" requestURI="pesquisarMedidorPopup">
			        <display:column sortable="true" title="N�mero de S�rie" sortProperty="numeroSerie" style="">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.numeroSerie}"/>
			            </a>
			        </display:column>
			        
			        <display:column sortable="true" title="Modelo" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.modelo.descricao}"/>
			            </a>
			        </display:column>
			        <display:column sortable="true" title="Tipo" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.tipoMedidor.descricao}"/>
			            </a>
			       	</display:column>
			        <display:column sortable="true" title="Marca" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.marcaMedidor.descricao}"/>
			            </a>
			        </display:column>
			        <display:column sortable="true" title="Ano de fabrica��o" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.chavePrimaria}"/>');"><span class="linkInvisivel"></span>	
			            	<c:out value="${medidor.anoFabricacao}"/>
			            </a>            
			        </display:column>
			  	</display:table>
			
			</c:when>
			<c:otherwise>
				<display:table class="dataTableGGAS dataTablePopup" name="listaMedidor" id="medidor" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarMedidorPopup">
			        <display:column sortable="true" title="N�mero de S�rie" sortProperty="numeroSerie" style="">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.numeroSerie}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.numeroSerie}"/> 
			            </a>
			        </display:column>
			        
			        <display:column sortable="true" title="Modelo" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.numeroSerie}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.modelo.descricao}"/>
			            </a>
			        </display:column>
			        <display:column sortable="true" title="Tipo" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.numeroSerie}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.tipoMedidor.descricao}"/>
			            </a>
			       	</display:column>
			        <display:column sortable="true" title="Marca" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.numeroSerie}"/>');"><span class="linkInvisivel"></span>
			            	<c:out value="${medidor.marcaMedidor.descricao}"/>
			            </a>
			        </display:column>
			        <display:column sortable="true" title="Ano de fabrica��o" style="width: 15.5%">
			        	<a href="javascript:selecionarNoLink('<c:out value="${medidor.numeroSerie}"/>');"><span class="linkInvisivel"></span>	
			            	<c:out value="${medidor.anoFabricacao}"/>
			            </a>            
			        </display:column>
			  	</display:table>
			</c:otherwise>
		</c:choose>
		
	 </c:if>
 	
</form:form>
