<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script>

	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,persist=1,hide=0');
	
	
	function exibirDetalhamentoHistoricoMedidor(chave){
		
		if(chave==null){
			alert('Medidor inexistente');
		}else{
		document.forms[0].idMedidor.value = chave;
		submeter("historicoInstalacaoMedidorForm", "exibirDetalhamentoHistoricoMedidor");
		}
	}
	
	function exibirHistoricoMedidor(chave){
		
		if(chave==null){
			alert('Medidor inexistente');
		}else{
	    	document.forms[0].idMedidor.value = chave;
	    	submeter("historicoInstalacaoMedidorForm", "exibirDetalhamentoHistoricoMedidor");
		}
	}
	
	function exibirHistoricoInstalacaoMedidorPontoConsumo(idPontoConsumo) {
		document.forms[0].idPontoConsumo.value = idPontoConsumo;
		submeter("historicoInstalacaoMedidorForm", "exibirHistoricoInstalacaoMedidorPontoConsumo");
	}
	

	function voltar() {
	
		<c:choose>
			<c:when test="${consultarHistoricoInstalacaoMedidorVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				submeter("historicoInstalacaoMedidorForm","pesquisarHistoricoMedidorImovel");
			</c:when>
			<c:otherwise>
				submeter("historicoInstalacaoMedidorForm", "pesquisarHistoricoMedidor");
			</c:otherwise>
		</c:choose>
	}

</script>
<h1 class="tituloInterno">Hist�rico do Medidor - Sele��o de Ponto de Consumo<a class="linkHelp" href="<help:help>/histricodasoperaesdomedidorporpontodeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<form:form styleId="formHistoricooInstalacaoMedidorPontoConsumo" method="post" action="exibirDetalhamentoHistoricoMedidorImovel" id="historicoInstalacaoMedidorForm" name="historicoInstalacaoMedidorForm">

<input name="idImovel" type="hidden" id="idImovel" value="${imovel.chavePrimaria}"/>
<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${consultarHistoricoInstalacaoMedidorVO.idPontoConsumo}"/>

<input name="numeroSerie" type="hidden" id="numeroSerie" value="${consultarHistoricoInstalacaoMedidorVO.numeroSerie}"/>
<input name="idTipo" type="hidden" id="idTipo" value="${consultarHistoricoInstalacaoMedidorVO.idTipo}"/>
<input name="idMarca" type="hidden" id="idMarca" value="${consultarHistoricoInstalacaoMedidorVO.idMarca}"/>
<input name="idModelo" type="hidden" id="idModelo" value="${consultarHistoricoInstalacaoMedidorVO.idModelo}"/>
<input name="medidorVirtual" type="hidden" id="medidorVirtual" value="${consultarHistoricoInstalacaoMedidorVO.medidorVirtual}"/>

<input name="nome" type="hidden" id="nome" value="${consultarHistoricoInstalacaoMedidorVO.nome}"/>
<input name="complementoImovel" type="hidden" id="complementoImovel" value="${consultarHistoricoInstalacaoMedidorVO.complementoImovel}"/>
<input name="matricula" type="hidden" id="matricula" value="${consultarHistoricoInstalacaoMedidorVO.matricula}"/>
<input name="numeroImovel" type="hidden" id="numeroImovel" value="${consultarHistoricoInstalacaoMedidorVO.numeroImovel}"/>
<input name="pontoConsumoLegado" type="hidden" id="pontoConsumoLegado" value="${consultarHistoricoInstalacaoMedidorVO.pontoConsumoLegado}"/>
<input name="habilitado" type="hidden" id="habilitado" value="${consultarHistoricoInstalacaoMedidorVO.habilitado}"/>
<input name="indicadorCondominioAmbos" type="hidden" id="indicadorCondominioAmbos" value="${consultarHistoricoInstalacaoMedidorVO.indicadorCondominioAmbos}"/>
<input name="cepImovel" type="hidden" id="cepImovel" value="${consultarHistoricoInstalacaoMedidorVO.cepImovel}"/>
<input name="idMedidor" type="hidden" id="idMedidor" value="${consultarHistoricoInstalacaoMedidorVO.idMedidor}"/>
<input name="indicadorPesquisa" type="hidden" id="indicadorPesquisa" value="${consultarHistoricoInstalacaoMedidorVO.indicadorPesquisa}"/>

<fieldset class="detalhamento">
	<a class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
	<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
		<fieldset class="coluna">
			<label class="rotulo">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.nome}"/></span><br />
			<label class="rotulo">Endere�o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.enderecoFormatado}"/></span><br />
		</fieldset>
		<fieldset class="colunaFinal">
			<label class="rotulo">CEP:</label>
			<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.cep}"/></span><br />
			<label class="rotulo">Complemento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.descricaoComplemento}"/></span><br />
		</fieldset>
	</fieldset>
</fieldset>	
	
	
<hr class="linhaSeparadora1" />	

<fieldset id="pontoVO" class="detalhamento">
		
<%--    <c:if test="${listaPontosConsumo ne null}"> --%>
<c:if test="${empty listaPontosConsumo == false}">
        <display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontosConsumo" sort="list" id="pontoVO" pagesize="15" size="${pageSize}" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoHistoricoMedidorImovel">
        
			<hr class="linhaSeparadora1" />
	  	
	  			<display:column media="html" title="Ponto de Consumo" style="width: 70px">
	  				<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>
					</a>
			    </display:column>
		
			    <display:column media="html" sortable="false" title="Descri��o" style="text-align: left">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.descricao}'/>
		        	</a>
			    </display:column>
			   
			    <display:column media="html" title="Situa��o" style="width: 80px text-aling: center">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.situacaoConsumo.descricao}'/>
			    	</a>
			    </display:column>
			   
			    <display:column media="html" sortable="false" title="Medidor">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.instalacaoMedidor.medidor.numeroSerie}'/>
					</a>
			    </display:column> 
			   
			    <display:column media="html" title="Refer�ncia / Ciclo" style="width: 80px">
			    			<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.referencia}'/>-<c:out value='${pontoVO.ciclo}'/>
			    	</a>
			    </display:column>
			    <display:column media="html" title="Grupo de<br />faturamento" style="width: 90px">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.rota.grupoFaturamento.descricao}'/>
			    	</a>
			    </display:column> 
			    <display:column media="html" title="Rota">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.rota.numeroRota}'/>
			    	</a>
			    </display:column>
			    <display:column media="html" title="Sequ�ncia<br />de leitura">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.numeroSequenciaLeitura}'/>
			    	</a>
			    </display:column>
			    <display:column media="html" title="Empresa leiturista">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${pontoVO.pontoConsumo.rota.empresa.cliente.nome}'/>
					</a>
			    </display:column>
			    <display:column media="html" title="C�digo do Ponto de Consumo">
			    		<a href='javascript:exibirHistoricoInstalacaoMedidorPontoConsumo(<c:out value='${pontoVO.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${pontoVO.pontoConsumo.codigoLegado}'/>
			    	</a>
			    </display:column>
			</display:table>		
	</c:if>
	

	<c:if test="${empty listaHistoricoOperacaoMedidor == false || empty pontoConsumo == false}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset id="instalacaoMedidor" class="conteinerBloco">
			<a name="ancoraInstalacaoMedidor"></a>
			<legend class="conteinerBlocoTitulo">Hist�rico de Opera��o de Medidores no Ponto de Consumo</legend>

			<display:table id="historicoOperacaoMedidor" class="dataTableGGAS" export="false" name="listaHistoricoOperacaoMedidor" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoHistoricoMedidorImovel">
		     	<display:column media="html" title="N�mero de S�rie"  style="auto">
		     		<a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
						<c:out value='${historicoOperacaoMedidor.medidor.numeroSerie}'/>
			        </a>
			    </display:column>
			    
			    <display:column media="html" title="Opera��o" style="auto">
			    	<a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			    		<c:out value='${historicoOperacaoMedidor.operacaoMedidor.descricao}'/>
			    	</a>
		    	</display:column>
			    
			    <display:column media="html" title="Data" style="auto">
			    	<a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
		 				<fmt:formatDate value="${historicoOperacaoMedidor.dataRealizada}" type="both" pattern="dd/MM/yyyy" />
		 			</a>
			    </display:column>
			    
			    <display:column media="html" title="Leitura" style="auto">
			    	<a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
		 				<c:out value='${historicoOperacaoMedidor.numeroLeitura}'/>
		 			</a>
			    </display:column>
			    
- 			    <display:column media="html" title="Local de Instala��o" style="auto"> 
 			   		<a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span> 
 		 				<c:out value='${historicoOperacaoMedidor.medidorLocalInstalacao.descricao}'/> 
		 			</a>  
 			    </display:column> 

			    <display:column media="html" title="Funcion�rio" style="auto">
			   		<a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
		 				<c:out value='${historicoOperacaoMedidor.funcionario.nome}'/>
		 			</a> 
			    </display:column>
			    
			    <display:column media="html" title="Press�o" style="auto">
			    	<a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
				    	<c:out value='${historicoOperacaoMedidor.medidaPressaoAnterior}'/>
				    	<c:out value='${historicoOperacaoMedidor.unidadePressaoAnterior.descricaoAbreviada}'/>
				    </a> 
		    	</display:column>
		    	
		    	<display:column media="html" title="Motivo" style="auto">
		    	    <a href='javascript:exibirHistoricoMedidor(<c:out value='${historicoOperacaoMedidor.medidor.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
					    	<c:out value='${historicoOperacaoMedidor.motivoOperacaoMedidor.descricao}'/>
					</a>    	
			    </display:column>
			    
		    </display:table>
 		</fieldset> 
	</c:if>	
	
	
	
	</fieldset>	
	<br/>
	
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
	</fieldset>
</form:form>	