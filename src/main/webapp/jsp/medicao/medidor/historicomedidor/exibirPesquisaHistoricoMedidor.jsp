<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Hist�rico do Medidor<a class="linkHelp" href="<help:help>/histricodasoperaesdemedidorconsulta.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.</p>

<form:form method="post" action="pesquisarHistoricoMedidor" id="historicoInstalacaoMedidorForm" name="historicoInstalacaoMedidorForm">
<script>
				
	function exibirDetalhamentoHistoricoMedidor(chave){
		document.forms[0].idMedidor.value = chave;
		submeter("historicoInstalacaoMedidorForm", "exibirDetalhamentoHistoricoMedidor");
	}
	
	function exibirDetalhamentoHistoricoMedidorImovel(idImovel){
		document.forms[0].idImovel.value = idImovel;
		
		submeter("historicoInstalacaoMedidorForm", "exibirDetalhamentoHistoricoMedidorImovel");
	}
	
	
	function limparFormulario() {
			document.historicoInstalacaoMedidorForm.matricula.value = "";	
			document.historicoInstalacaoMedidorForm.cepImovel.value = "";	
	}
	
	function pesquisar(){
		var elem = document.forms[0].indicadorPesquisa;
		var selecao = getCheckedValue(elem);
		
		if(selecao == "indicadorPesquisaImovel"){
			  submeter("historicoInstalacaoMedidorForm", "pesquisarHistoricoMedidorImovel");
		}else{		
			  submeter("historicoInstalacaoMedidorForm", "pesquisarHistoricoMedidor");
		}	
		
	}
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			$('.medidorVirtual').prop('disabled', true);
			modificarComportamentoCamposImovel(false);
			modificarComportamentoCamposMedidor(true);
			limparCamposPesquisa();
		}else{
			$('.medidorVirtual').prop('disabled', false);
			modificarComportamentoCamposMedidor(false);
			modificarComportamentoCamposImovel(true);
			limparCamposPesquisa();
		}	
	}
	
	function modificarComportamentoCamposImovel(valor){
		document.getElementById('cepImovel').disabled = valor;
		document.getElementById('matricula').disabled = valor;
		document.getElementById('complementoImovel').disabled = valor;
		document.getElementById('numeroImovel').disabled = valor;
		document.getElementById('pontoConsumoLegado').disabled = valor;
		document.getElementById('condominioSim').disabled = valor;
		document.getElementById('condominioNao').disabled = valor;
		document.getElementById('condominioAmbos').disabled = valor;
		document.getElementById('habilitado1').disabled = valor;
		document.getElementById('habilitado2').disabled = valor;
		document.getElementById('habilitado3').disabled = valor;
		document.getElementById('nome').disabled = valor;
		document.getElementById('rotuloNomeImovel').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloComplemento').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloMatricula').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloNumeroImovel').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloPontoConsumoLegado').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloImovelCondominio').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUso').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloCepFalse').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloCondominioSim').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloCondominioNao').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloCondominioAmbos').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUsoAtivo').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUsoInativo').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloIndicadorUsoTodos').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('rotuloNomeEmpresa').className = 'rotulo';
		document.getElementById('rotuloTipo').className = 'rotulo';
		document.getElementById('rotuloMarca').className = 'rotulo';
		document.getElementById('rotuloModelo').className = 'rotulo';
		document.getElementById('medidorVirtualRotulo').className = 'rotulo';
		document.getElementById('medidorVirtualRotuloRadio1').className = 'rotuloRadio';
		document.getElementById('medidorVirtualRotuloRadio2').className = 'rotuloRadio';
		document.getElementById('medidorVirtualRotuloRadio3').className = 'rotuloRadio';
	}
	function modificarComportamentoCamposMedidor(valor){
		document.getElementById('numeroSerie').disabled = valor;
		document.getElementById('tipo').disabled = valor;
		document.getElementById('marca').disabled = valor;
		document.getElementById('modelo').disabled = valor;
		document.getElementById('rotuloNomeImovel').className = 'rotulo';
		document.getElementById('rotuloComplemento').className = 'rotulo';
		document.getElementById('rotuloMatricula').className = 'rotulo';
		document.getElementById('rotuloNumeroImovel').className = 'rotulo';
		document.getElementById('rotuloPontoConsumoLegado').className = 'rotulo';
		document.getElementById('rotuloImovelCondominio').className = 'rotulo';
		document.getElementById('rotuloIndicadorUso').className = 'rotulo';
		document.getElementById('rotuloCepFalse').className = 'rotulo';
		document.getElementById('rotuloCondominioSim').className = 'rotuloRadio';
		document.getElementById('rotuloCondominioNao').className = 'rotuloRadio';
		document.getElementById('rotuloCondominioAmbos').className = 'rotuloRadio';
		document.getElementById('rotuloIndicadorUsoAtivo').className = 'rotuloRadio';
		document.getElementById('rotuloIndicadorUsoInativo').className = 'rotuloRadio';
		document.getElementById('rotuloIndicadorUsoTodos').className = 'rotuloRadio';
		document.getElementById('rotuloNomeEmpresa').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloTipo').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloMarca').className = 'rotulo rotuloDesabilitado';
		document.getElementById('rotuloModelo').className = 'rotulo rotuloDesabilitado';
		document.getElementById('medidorVirtualRotulo').className = 'rotulo rotuloDesabilitado';
		document.getElementById('medidorVirtualRotuloRadio1').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('medidorVirtualRotuloRadio2').className = 'rotuloRadio rotuloDesabilitado';
		document.getElementById('medidorVirtualRotuloRadio3').className = 'rotuloRadio rotuloDesabilitado';
	}
	
	function limparCamposPesquisa(){
		document.getElementById('cepImovel').value = "";
		document.getElementById('matricula').value = "";
		document.forms[0].complementoImovel.value = "";	
		document.forms[0].indicadorCondominioAmbos[2].checked = true;	
		document.forms[0].numeroImovel.value = "";	
		document.forms[0].habilitado[0].checked = true;	
		document.getElementById('nome').value = "";
		document.getElementById('numeroSerie').value = "";
		document.getElementById('tipo').value = "-1";
		document.getElementById('marca').value = "-1";
		document.getElementById('modelo').value = "-1";
		document.forms[0].medidorVirtual[2].checked = true;	
		document.getElementById('pontoConsumoLegado').value = "";
	}
	
	function habilitarMatriculaCondominio() {	
		document.forms[0].matriculaCondominio.disabled = false;
	}
	
	function desabilitarMatriculaCondominio() {	
		document.forms[0].matriculaCondominio.value = "";
		document.forms[0].matriculaCondominio.disabled = true;
	}
	
	function manterDadosTela(){
		var elem = document.forms[0].indicadorPesquisa;
		<c:choose>
			<c:when test="${consultarHistoricoInstalacaoMedidorVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				modificarComportamentoCamposMedidor(true);
			</c:when>
			<c:otherwise>
				modificarComportamentoCamposImovel(true);
			</c:otherwise>
		</c:choose>
	}
	
	function init () {
		manterDadosTela();
	}
	
	addLoadEvent(init);

</script>

	<input name="acao" type="hidden" id="acao" value="pesquisarHistoricoMedidor"/>
	<input name="idImovel" type="hidden" id="idImovel" value=""/>
	<input name="idMedidor" type="hidden" id="idMedidor" value=""/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="consultarHistoricoInstalacaoMedidor" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarMedidor" class="colunaEsq">		
			<input class="campoRadio" type="radio" value="indicadorPesquisaMedidor" name="indicadorPesquisa" id="indicadorPesquisaMedidor" <c:if test="${consultarHistoricoInstalacaoMedidorVO.indicadorPesquisa eq 'indicadorPesquisaMedidor'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Medidor</legend>
			<div class="pesquisarClienteFundo">
				<label class="rotulo" id="rotuloNomeEmpresa" for="nomeCompletoCliente" >N�mero de s�rie:</label>
				<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${consultarHistoricoInstalacaoMedidorVO.numeroSerie}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" /><br />
				<label class="rotulo" id="rotuloTipo" for="tipo">Tipo:</label>
				<select name="idTipo" id="tipo" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoMedidor}" var="tipo">
						<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${consultarHistoricoInstalacaoMedidorVO.idTipo == tipo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipo.descricao}"/>
						</option>
				    </c:forEach>
				</select><br />
				<label class="rotulo" id="rotuloMarca" for="marca">Marca:</label>
				<select name="idMarca" id="marca" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaMarcaMedidor}" var="marca">
						<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${consultarHistoricoInstalacaoMedidorVO.idMarca == marca.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${marca.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />	
				<label class="rotulo" id="rotuloModelo" for="modelo">Modelo:</label>
				<select name="idModelo" id="modelo" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaModeloMedidor}" var="modelo">
						<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${consultarHistoricoInstalacaoMedidorVO.idModelo == modelo.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${modelo.descricao}"/>
						</option>		
				    </c:forEach>
				</select><br />	
				<label class="rotulo" id="medidorVirtualRotulo" for="medidorVirtual">Perfil:</label>
				<input class="campoRadio medidorVirtual" type="radio" name="medidorVirtual" id="medidorVirtual" value="true" <c:if test="${consultarHistoricoInstalacaoMedidorVO.medidorVirtual eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="medidorVirtual" id="medidorVirtualRotuloRadio1">Sim</label>
				<input class="campoRadio medidorVirtual" type="radio" name="medidorVirtual" id="medidorVirtual" value="false" <c:if test="${consultarHistoricoInstalacaoMedidorVO.medidorVirtual eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="medidorVirtual" id="medidorVirtualRotuloRadio2">N�o</label>
				<input class="campoRadio medidorVirtual" type="radio" name="medidorVirtual" id="medidorVirtual" value="" <c:if test="${empty consultarHistoricoInstalacaoMedidorVO.medidorVirtual}">checked</c:if>>
				<label class="rotuloRadio" for="medidorVirtual" id="medidorVirtualRotuloRadio3">Todos</label>
				<br />
			</div>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${consultarHistoricoInstalacaoMedidorVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">			
				<label class="rotulo" id="rotuloNomeImovel" for="nomeImovel">Descri��o:</label>
				<input class="campoTexto" type="text" id="nome" name="nome" size="37" value="${consultarHistoricoInstalacaoMedidorVO.nome}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
				<label class="rotulo" id="rotuloComplemento" for="complementoImovel">Complemento:</label>
				<input class="campoTexto" type="text" id="complementoImovel" name="complementoImovel" maxlength="25" size="30" value="${consultarHistoricoInstalacaoMedidorVO.complementoImovel}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');">
				<label class="rotulo" id="rotuloMatricula" for="matricula">Matr�cula:</label>
				<input class="campoTexto" type="text" id="matricula" name="matricula" maxlength="9" size="9" value="${consultarHistoricoInstalacaoMedidorVO.matricula}" onkeypress="return formatarCampoInteiro(event)"><br />
				<label class="rotulo" id="rotuloNumeroImovel" for="numeroImovel">N�mero do im�vel:</label>
				<input class="campoTexto" type="text" id="numeroImovel" name="numeroImovel" maxlength="9" size="9" value="${consultarHistoricoInstalacaoMedidorVO.numeroImovel}" onkeypress="return formatarCampoNumeroEndereco(event, this);"><br />
				<label class="rotulo rotulo2Linhas" id="rotuloPontoConsumoLegado" for="pontoConsumoLegado">C�digo de Ponto <br /> de Consumo:</label>
				<input class="campoTexto campo2Linhas" type="text" id="pontoConsumoLegado" name="pontoConsumoLegado" maxlength="9" size="9" value="${consultarHistoricoInstalacaoMedidorVO.pontoConsumoLegado}" onkeypress="return formatarCampoInteiro(event);"><br />
				<label class="rotulo" id="rotuloImovelCondominio" for="condominioSim">O im�vel � condom�nio?</label>
				<input class="campoRadio" type="radio" value="true" name="indicadorCondominioAmbos" id="condominioSim" <c:if test="${consultarHistoricoInstalacaoMedidorVO.indicadorCondominioAmbos eq 'true'}">checked</c:if> onclick="desabilitarMatriculaCondominio();"><label id="rotuloCondominioSim" class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" value="false" name="indicadorCondominioAmbos" id="condominioNao" <c:if test="${consultarHistoricoInstalacaoMedidorVO.indicadorCondominioAmbos ne 'true'}">checked</c:if> onclick="habilitarMatriculaCondominio();"><label id="rotuloCondominioNao" class="rotuloRadio">N�o</label>
				<input class="campoRadio" type="radio" value="" name="indicadorCondominioAmbos" id="condominioAmbos" <c:if test="${consultarHistoricoInstalacaoMedidorVO.indicadorCondominioAmbos ne 'true' && consultarHistoricoInstalacaoMedidorVO.indicadorCondominioAmbos ne 'false'}">checked</c:if> onclick=""><label id="rotuloCondominioAmbos" class="rotuloRadio">Todos</label><br /><br />
				<label class="rotulo" id="rotuloIndicadorUso" for="habilitado">Indicador de Uso:</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${consultarHistoricoInstalacaoMedidorVO.habilitado eq true}">checked</c:if>>
				<label id="rotuloIndicadorUsoAtivo" class="rotuloRadio" for="indicadorUso">Ativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${consultarHistoricoInstalacaoMedidorVO.habilitado eq false}">checked</c:if>>
				<label id="rotuloIndicadorUsoInativo" class="rotuloRadio" for="indicadorUso">Inativo</label>
				<input class="campoRadio" type="radio" name="habilitado" id="habilitado3" value="" <c:if test="${empty consultarHistoricoInstalacaoMedidorVO.habilitado}">checked</c:if>>
				<label id="rotuloIndicadorUsoTodos" class="rotuloRadio" for="habilitado">Todos</label><br /><br />
				<fieldset class="exibirCep">
					<jsp:include page="/jsp/cadastro/localidade/pesquisarCEP.jsp">
						<jsp:param name="cepObrigatorio" value="false"/>
						<jsp:param name="idCampoCep" value="cepImovel"/>
						<jsp:param name="numeroCep" value="${consultarHistoricoInstalacaoMedidorVO.cepImovel}"/>
					</jsp:include>		
				</fieldset>
			</div>
		</fieldset>
		<fieldset class="conteinerBotoesPesquisarDirFixo">
<%-- 			<vacess:vacess param="pesquisarHistoricoMedidor"> --%>
				<input name="Button" id="botaoPesquisar" class="bottonRightCol2" type="button" value="Pesquisar" onclick="pesquisar();" />
<%-- 			</vacess:vacess> --%>
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();" />
		</fieldset>
		
	</fieldset>	
		
	<c:if test="${listaImoveis ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaImoveis" id="imovel" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarHistoricoMedidorImovel">
	     	<display:column sortable="true" sortProperty="chavePrimaria" titleKey="IMOVEL_MATRICULA">
	     		<a href="javascript:exibirDetalhamentoHistoricoMedidorImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${imovel.chavePrimaria}'/>
				</a>
		    </display:column>
		    <display:column sortable="true" sortProperty="nome" title="Nome" style="text-align: left">
		    	<a href="javascript:exibirDetalhamentoHistoricoMedidorImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${imovel.nome}'/>
	        	</a>
		    </display:column>		     
		    <display:column sortable="false" title="Inscri��o">
		    	<a href="javascript:exibirDetalhamentoHistoricoMedidorImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		    		<c:out value='${imovel.inscricao}'/>
		    	</a>
		    </display:column> 
		    <display:column sortable="false" title="Endere�o" style="text-align: left">
		    	<a href="javascript:exibirDetalhamentoHistoricoMedidorImovel(<c:out value='${imovel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		    		<c:out value='${imovel.enderecoFormatado}'/>
		    	</a>
		    </display:column> 
		</display:table>
	</c:if>
	
<c:if test="${medidores ne null}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="medidores" id="medidor" partialList="true" sort="external" pagesize="15" size="${pageSize}" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarHistoricoMedidor">
        <display:column media="html" sortable="true" title="N�mero de S�rie" sortProperty="numeroSerie" style="width: 16.6%">
        	<a href="javascript:exibirDetalhamentoHistoricoMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
            	<c:out value="${medidor.numeroSerie}"/>
            </a>
        </display:column>
        <display:column sortable="true" title="Modelo" style="width: 16.6%" sortProperty="modelo.descricao">
        	<a href="javascript:exibirDetalhamentoHistoricoMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
            	<c:out value="${medidor.modelo.descricao}"/>
            </a>            
        </display:column>
        <display:column sortable="true" title="Tipo" style="width: 16.6%" sortProperty="tipoMedidor.descricao">
        	<a href="javascript:exibirDetalhamentoHistoricoMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>        	
            	<c:out value="${medidor.tipoMedidor.descricao}"/>
            </a>
        </display:column>
        <display:column sortable="true" title="Marca" style="width: 16.6%" sortProperty="marcaMedidor.descricao">
        	<a href="javascript:exibirDetalhamentoHistoricoMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
            	<c:out value="${medidor.marcaMedidor.descricao}"/>
            </a>
        </display:column>
        <display:column sortable="true" title="Ano de fabrica��o" style="width: 16.6%" sortProperty="anoFabricacao">
        	<a href="javascript:exibirDetalhamentoHistoricoMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
            	<c:out value="${medidor.anoFabricacao}"/>
            </a>
        </display:column>
        <display:column sortable="true" title="Tombamento" style="width: 17%" sortProperty="tombamento">
        	<a href="javascript:exibirDetalhamentoHistoricoMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
            	<c:out value="${medidor.tombamento}"/>
            </a>
        </display:column>
        <display:column sortable="true" title="Medidor Virtual" style="width: 17%" sortProperty="medidorVirtual">
        	<a href="javascript:exibirDetalhamentoHistoricoMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
            	<c:choose>
            		<c:when test="${not empty medidor.codigoMedidorSupervisorio}">Sim</c:when>
            		<c:otherwise>N�o</c:otherwise>
            	</c:choose>
            	
            </a>
        </display:column>
    </display:table>

</c:if>

</form:form>