<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>


function exibirHistoricoMedidorPontoConsumo(chave){
	document.forms[0].idImovel.value = chave;
	submeter("historicoInstalacaoMedidorForm", "exibirDetalhamentoHistoricoMedidorImovel");	
}

</script>
<c:if test="${listaHistoricoOperacaoMedidorVO ne null}">
 <display:table class="dataTableGGAS" name="listaHistoricoOperacaoMedidorVO" sort="list" id="historicoOperacaoMedidor" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoHistoricoMedidor">
			<hr class="linhaSeparadora1" />
		  
			     	<display:column media="html" title="Ponto de Consumo">
			     		<a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.imovel.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
							<c:out value='${historicoOperacaoMedidor.descricaoPontoConsumo}'/>
						</a>	
				    </display:column>
				    
				    <display:column media="html" title="Opera��o">
				    <a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
				    		<c:out value='${historicoOperacaoMedidor.operacaoMedidor.descricao}'/>
				    </a>		
			    	</display:column>
				    
				    <display:column media="html" title="Data">
				      <a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			 				<fmt:formatDate value="${historicoOperacaoMedidor.dataRealizada}" type="both" pattern="dd/MM/yyyy" />
			 		  </a>
				    </display:column>
				    
				    <display:column media="html" title="Leitura">
				      <a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			 				<c:out value='${historicoOperacaoMedidor.numeroLeitura}'/>
			 		  </a>	
				    </display:column>
				    
				    <display:column media="html" title="Local de Instala��o">
				       <a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			 				<c:out value='${historicoOperacaoMedidor.medidorLocalInstalacao.descricao}'/>
			 		   </a>
				    </display:column>

				    <display:column media="html" title="Funcion�rio">
				        <a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
			 				<c:out value='${historicoOperacaoMedidor.funcionario.nome}'/>
			 			</a>	
				    </display:column>
				    
				    <display:column media="html" title="Press�o" >
				      <a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
					    	<c:out value='${historicoOperacaoMedidor.medidaPressaoAnterior}'/>
					    	<c:out value='${historicoOperacaoMedidor.unidadePressaoAnterior.descricaoAbreviada}'/>
					  </a>  	
			    	</display:column>
				    
				    <display:column media="html" title="Motivo" >
				      <a href='javascript:exibirHistoricoMedidorPontoConsumo(<c:out value='${historicoOperacaoMedidor.pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
					    	<c:out value='${historicoOperacaoMedidor.motivoOperacaoMedidor.descricao}'/>
					  </a>  	
			    	</display:column>
			</display:table>
		</c:if>



