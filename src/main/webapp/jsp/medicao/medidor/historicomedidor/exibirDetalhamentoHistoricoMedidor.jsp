<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Hist�rico do Medidor<a class="linkHelp" href="<help:help>/histricodasoperaesdomedidor.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="exibirDetalhamentoHistoricoMedidor" id="historicoInstalacaoMedidorForm" name="historicoInstalacaoMedidorForm">
	<script>
		
		$(document).ready(function(){
			habilitarAbaComposicao();
		});

		animatedcollapse.addDiv('dadosMedidor', 'fade=0,speed=400,persist=1,hide=0');
		animatedcollapse.addDiv('composicaoVirtual', 'fade=0,speed=400,persist=1,hide=0');
		
				
		function voltar() {
			<c:choose>
				<c:when test="${consultarHistoricoInstalacaoMedidorVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
					submeter("historicoInstalacaoMedidorForm", "pesquisarHistoricoMedidorImovel");
				</c:when>
				<c:otherwise>					
					submeter("historicoInstalacaoMedidorForm", "pesquisarHistoricoMedidor");
				</c:otherwise>
			</c:choose>
		}

		function habilitarAbaComposicao(){
			if($('#modoUsoVirtual').prop('checked')){
				$("#composicaoMedidor").removeAttr("disabled");
				$("#abaComposicaoObrigatoria").show();
			}else{
				$("#composicaoMedidor").attr("disabled", "disabled");
				$("#abaComposicaoObrigatoria").hide();
			}
			
		}
	</script>
	<input name="idMedidor" type="hidden" id="idMedidor" value="${medidor.chavePrimaria}"/>
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value=""/>
	<input name="idImovel" type="hidden" id="hidden" value=""/>
	<input name="indicadorPesquisa" type="hidden" id="indicadorPesquisa" value="${consultarHistoricoInstalacaoMedidorVO.indicadorPesquisa}"/>
	
	<input name="numeroSerie" type="hidden" id="numeroSerie" value="${consultarHistoricoInstalacaoMedidorVO.numeroSerie}"/>
	<input name="idTipo" type="hidden" id="idTipo" value="${consultarHistoricoInstalacaoMedidorVO.idTipo}"/>
	<input name="idMarca" type="hidden" id="idMarca" value="${consultarHistoricoInstalacaoMedidorVO.idMarca}"/>
	<input name="idModelo" type="hidden" id="idModelo" value="${consultarHistoricoInstalacaoMedidorVO.idModelo}"/>
	<input name="medidorVirtual" type="hidden" id="medidorVirtual" value="${consultarHistoricoInstalacaoMedidorVO.medidorVirtual}"/>
	
	<input name="nome" type="hidden" id="nome" value="${consultarHistoricoInstalacaoMedidorVO.nome}"/>
	<input name="complementoImovel" type="hidden" id="complementoImovel" value="${consultarHistoricoInstalacaoMedidorVO.complementoImovel}"/>
	<input name="matricula" type="hidden" id="matricula" value="${consultarHistoricoInstalacaoMedidorVO.matricula}"/>
	<input name="numeroImovel" type="hidden" id="numeroImovel" value="${consultarHistoricoInstalacaoMedidorVO.numeroImovel}"/>
	<input name="pontoConsumoLegado" type="hidden" id="pontoConsumoLegado" value="${consultarHistoricoInstalacaoMedidorVO.pontoConsumoLegado}"/>
	<input name="habilitado" type="hidden" id="habilitado" value="${consultarHistoricoInstalacaoMedidorVO.habilitado}"/>
	<input name="indicadorCondominioAmbos" type="hidden" id="indicadorCondominioAmbos" value="${consultarHistoricoInstalacaoMedidorVO.indicadorCondominioAmbos}"/>
	<input name="cepImovel" type="hidden" id="cepImovel" value="${consultarHistoricoInstalacaoMedidorVO.cepImovel}"/>
	
	
	
	<fieldset class="detalhamento">

		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosMedidor]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Medidor <img src="imagens/setaBaixo.png" border="0"/></a>
		<fieldset id="dadosMedidor" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo" id="rotuloNumeroSerie">N�mero de S�rie:</label>
				<span class="itemDetalhamento"><c:out value="${medidor.numeroSerie}"/></span><br />
				
				<label class="rotulo" id="modoUsoRotulo" for="modoUso">Perfil:</label>
				<input class="campoRadio" type="radio" name="modoUso" id=modoUsoNormal onclick="habilitarAbaComposicao();" value="${modoUsoNormal}" disabled
					<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoNormal || medidor.modoUso eq null || medidor.modoUso eq ''}">checked</c:if>>
				<label class="rotuloRadio" for="modoUsoNormal">Normal</label>
				<input class="campoRadio" type="radio" name="modoUso" id="modoUsoVirtual" onclick="habilitarAbaComposicao();" value="${modoUsoVirtual}" disabled
					<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">checked</c:if>>
				<label class="rotuloRadio" for="modoUsoVirtual">Virtual</label> 
				<input class="campoRadio" type="radio" name="modoUso" id="modoUsoIndependente" onclick="habilitarAbaComposicao();" value="${modoUsoIndependente}" disabled
					<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoIndependente}">checked</c:if>>
				<label class="rotuloRadio" for="modoUsoIndependente">Independente</label>
				 
				<label class="rotulo" id="rotuloModelo">Modelo:</label>
				<span class="itemDetalhamento"><c:out value="${medidor.modelo.descricao}"/></span><br />
				<label class="rotulo" id="rotuloTipo">Tipo:</label>
				<span class="itemDetalhamento"><c:out value="${medidor.tipoMedidor.descricao}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo" id="rotuloMarca">Marca:</label>
				<span id="itemDetalhamentoMarca" class="itemDetalhamento"><c:out value="${medidor.marcaMedidor.descricao}"/></span><br />
				<label class="rotulo" id="rotuloAnoFabricacao">Ano de Fabrica��o:</label>
				<span id="itemDetalhamentoAnoFabricacao" class="itemDetalhamento"><c:out value="${medidor.anoFabricacao}"/></span><br />
				<label class="rotulo" id="rotuloTombamento">Tombamento:</label>
				<span id="itemDetalhamentoTombamento" class="itemDetalhamento"><c:out value="${medidor.tombamento}"/></span><br />
			</fieldset>
		</fieldset>
		<c:if test="${medidorForm.map.modoUso == modoUsoVirtual}">
			<c:if test="${listaMedidor ne null }">
				<fieldset class="conteinerBloco">
					<legend class="conteinerBlocoTitulo">Composi��o</legend>
					<display:table class="dataTableGGAS" name="listaMedidor" sort="list" id="medidorVirtualVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
				        <display:column sortable="false" sortProperty="numeroSerie" style="width: 400px;" title="Medidor - N�mero de S�rie">
			            	<c:out value="${medidorVirtualVO.numeroSerie}"/>
			            	<input type="hidden" id="listaChavePrimaria" name="listaChavePrimaria" value="${medidorVirtualVO.id}"/>
							<input type="hidden" id="listaNumeroSerie" name="listaNumeroSerie" value="${medidorVirtualVO.numeroSerie}" />
				     	</display:column>	
			   			<display:column title="Opera��o">
		   					<c:if test="${medidorVirtualVO.operacao=='+'}">
			   					+
		   					</c:if>
		   					<c:if test="${medidorVirtualVO.operacao=='-'}">
			   					-
		   					</c:if>
				        </display:column>
				    </display:table>	
				</fieldset>
			</c:if>
		</c:if>
	</fieldset>
	<hr class="linhaSeparadora1" />
	<br/>
	<fieldset id="tabs" style="display: none;">
		<ul>
			<li><a href="#operacoesMedidor">Opera��es</a></li>
			<li><a href="#movimentacoesMedidor">Movimenta��es</a></li>
			<c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">
				<li><a href="#composicaoMedidor"><span class="campoObrigatorioSimboloTabs" id="abaComposicaoObrigatoria">* </span>Composi��o</a></li>
			</c:if>
		</ul>
		<fieldset id="operacoesMedidor">
			<jsp:include page="/jsp/medicao/medidor/historicomedidor/abaOperacoes.jsp">
				<jsp:param name="actionAdicionarFuncao" value="historicoMedidorFluxoOperacoes" />
			</jsp:include>
		</fieldset>
		<fieldset id="movimentacoesMedidor">
			<jsp:include page="/jsp/medicao/medidor/historicomedidor/abaMovimentacoes.jsp">
				<jsp:param name="actionAdicionarParametro" value="/historicoMedidorFluxoMovimentacoes" />				
			</jsp:include>
		</fieldset>
		<fieldset id="composicaoMedidor">
			<jsp:include page="/jsp/medicao/medidor/historicomedidor/abaComposicao.jsp">
				<jsp:param name="actionAdicionarParametro" value="/historicoMedidorFluxoMovimentacoes" />				
			</jsp:include>
		</fieldset>
   </fieldset>

	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="history.back();">
	</fieldset>
</form:form>