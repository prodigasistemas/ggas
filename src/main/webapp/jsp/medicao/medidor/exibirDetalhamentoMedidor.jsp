<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>


<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	


<h1 class="tituloInterno">Detalhar Medidor<a class="linkHelp" href="<help:help>/detalhamentodocadastromedidor.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="exibirDetalhamentoMedidor" enctype="multipart/form-data" styleId="detalhamentoMedidor" id="medidorForm" name="medidorForm" modelAttribute="medidor">

<script>
	
	function voltar() {
		submeter("medidorForm", "pesquisarMedidor");
	}
	
	function voltarHistoricoMovimentacaoMedidor(){	
		submeter("medidorForm", "voltarHistoricoMovimentacaoMedidor");
	}
	
	function alterar(){
		document.forms[0].postBack.value = false;
		submeter('medidorForm', 'exibirAtualizarMedidor');
	}
	
</script>

<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoMedidor">
<input name="postBack" type="hidden" id="postBack" value="false">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${medidor.chavePrimaria}">

<fieldset class="detalhamento">
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Identifica��o</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoMedidorCol">
			<label class="rotulo" id="rotuloControleVazao">Perfil:</label>
			<input name="modoUso" type="hidden" value="${medidor.modoUso.chavePrimaria == modoUsoNormal}">
			<c:choose>
				<c:when test="${medidor.modoUso.chavePrimaria == modoUsoNormal}">
					<span class="itemDetalhamento"><c:out value="Medidor Normal"/></span><br />
				</c:when>
				<c:when test="${medidor.modoUso.chavePrimaria == modoUsoVirtual}">
					<span class="itemDetalhamento"><c:out value="Medidor Vitual"/></span><br />
				</c:when>
				<c:when test="${medidor.modoUso.chavePrimaria == modoUsoIndependente}">
					<span class="itemDetalhamento"><c:out value="Medidor Independente"/></span><br />
				</c:when>
			</c:choose>
			
			<label class="rotulo" id="rotuloNumeroSerie" for="numeroSerie">N�mero de s�rie:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.numeroSerie}"/></span><br />
			
			<label class="rotulo" id="rotuloTipo">Tipo:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.tipoMedidor.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloMarca">Marca:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.marcaMedidor.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloModelo" >Modelo:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.modelo.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloSituacao">Situa��o:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.situacaoMedidor.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloDiametro">Di�metro:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.diametroMedidor.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloEnderecoRemoto">Endere�o Remoto:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.codigoMedidorSupervisorio}"/></span><br />
		</fieldset>
		
		<fieldset class="colunaFinal detalhamentoMedidorCol">
			<label class="rotulo" id="rotuloCapacidadeMinima">Capacidade M�nima:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.capacidadeMinima.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloCapacidadeMaxima">Capacidade M�xima:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.capacidadeMaxima.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloAnoFabricacao" >Ano de Fabrica��o:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.anoFabricacao}"/></span><br />

			<label class="rotulo" id="rotuloDataAquisicao" >Data de Aquisi��o:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${medidor.dataAquisicao}" pattern="dd/MM/yyyy"/></span><br />
			
			<label class="rotulo" id="rotuloAnoCalibracao" >Intervalo de Calibra��o:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.anoCalibracao}"/><c:if test="${not empty medidor.anoCalibracao}">(em anos)</c:if></span><br />
			
			<label class="rotulo" id="rotuloDataUltimaCalibracao" >Data da Ultima Calibra��o:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${medidor.dataUltimaCalibracao}" pattern="dd/MM/yyyy"/></span><br />
			
			<label class="rotulo" id="rotuloTombamento" >N�mero do Tombamento:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.tombamento}"/></span>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
		
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Caracter�sticas</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoMedidorCol">
			<label class="rotulo" id="rotuloDataMaximaInstalacao" >Data M�xima de Instala��o:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${medidor.dataMaximaInstalacao}" pattern="dd/MM/yyyy"/></span><br />
			
			<label class="rotulo" id="rotuloNumeroDigitosLeitura" >N�mero de D�gitos para Leitura:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.digito}"/></span><br />
			
			<label class="rotulo" id="rotuloFatorK" >Fator K:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.fatorK.fatorK}"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal detalhamentoMedidorCol">
			<label class="rotulo" id="rotuloPressaoMaximaTrabalho" >Press�o m�xima de trabalho:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.pressaoMaximaTrabalho}"/></span>
			<span class="itemDetalhamento"><c:out value="${medidor.unidadePressaoMaximaTrabalho.descricao}"/></span><br />
			
			<label class="rotulo rotulo2Linhas" id="rotuloFaixaTemperaturaTrabalho">Faixa de temperatura de trabalho:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${medidor.faixaTemperaturaTrabalho.descricao}"/></span><br />
			
			<label class="rotulo" id="rotuloLocalArmazenagem">Local de Armazenagem:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.localArmazenagem.descricao}"/></span><br />
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
	<c:if test="${medidor.modoUso.chavePrimaria == modoUsoVirtual}">
		<c:if test="${listaMedidor ne null }">
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Composi��o</legend>
				<display:table class="dataTableGGAS" name="listaMedidor" sort="list" id="medidorVirtualVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			        <display:column sortable="false" sortProperty="numeroSerie" style="width: 400px;" title="Medidor - N�mero de S�rie">
		            	<c:out value="${medidorVirtualVO.numeroSerie}"/>
		            	<input type="hidden" id="listaChavePrimaria" name="listaChavePrimaria" value="${medidorVirtualVO.id}"/>
						<input type="hidden" id="listaNumeroSerie" name="listaNumeroSerie" value="${medidorVirtualVO.numeroSerie}" />
			     	</display:column>	
		   			<display:column title="Opera��o">
	   					<c:if test="${medidorVirtualVO.operacao=='+'}">
		   					+
	   					</c:if>
	   					<c:if test="${medidorVirtualVO.operacao=='-'}">
		   					-
	   					</c:if>
			        </display:column>
			    </display:table>	
			</fieldset>
		</c:if>
	</c:if>

	<c:if test="${empty listaHistoricoOperacaoMedidor == false }">
       	<hr class="linhaSeparadoraDetalhamento" />
		<fieldset id="instalacaoMedidor" class="conteinerBloco">
			<a name="ancoraInstalacaoMedidor"></a>
			<legend class="conteinerBlocoTitulo">Hist�rico de Opera��o</legend>

			<display:table id="historicoOperacaoMedidor" class="dataTableGGAS" export="false" name="listaHistoricoOperacaoMedidor" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
		     
		     	<display:column media="html" sortProperty="pontoConsumo.descricaoFormatada" property="pontoConsumo.descricaoFormatada"  
		     		title="Ponto de Consumo" style="text-align: left; padding-left: 5px;" />
					
			    <display:column media="html" title="Opera��o">
			    	<c:out value='${historicoOperacaoMedidor.operacaoMedidor.descricao}'/>
		    	</display:column>
			    
			    <display:column media="html" title="Data">
		 			<fmt:formatDate value="${historicoOperacaoMedidor.dataRealizada}" type="both" pattern="dd/MM/yyyy" />
			    </display:column>
			    
			    <display:column media="html" title="Leitura">
		 			<c:out value='${historicoOperacaoMedidor.numeroLeitura}'/>
			    </display:column>
			    
			    <display:column media="html" title="Local de Instala��o">
		 			<c:out value='${historicoOperacaoMedidor.medidorLocalInstalacao.descricao}'/>
			    </display:column>

			    <display:column media="html" title="Funcion�rio">
		 			<c:out value='${historicoOperacaoMedidor.funcionario.nome}'/>
			    </display:column>
			    
			    <display:column media="html" title="Press�o">
			    	<c:out value='${historicoOperacaoMedidor.medidaPressaoAnterior}'/>
			    	<c:out value='${historicoOperacaoMedidor.unidadePressaoAnterior.descricaoAbreviada}'/>
		    	</display:column>
		    	
		    </display:table>
		</fieldset>
	</c:if>
	<hr class="linhaSeparadoraDetalhamento" />			        
</fieldset>
	
<fieldset class="detalhamento">
	<legend class="conteinerBlocoTituloGrande">Informa��es de Instala��o</legend>
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Medidor</legend>
		<fieldset id="vazaoCorretorCol1" class="coluna2 detalhamentoColunaLarga">
			<label class="rotulo rotuloMedidorCss" id="rotuloNumeroSerie">Data:</label>
			<span class="itemDetalhamento"><c:out value="${dataInstalacao}"/></span><br />
			<label class="rotulo rotuloMedidorCss" id="rotuloNumeroMarca">Leitura:</label>
			<span class="itemDetalhamento"><c:out value="${leituraInstalacao}"/></span><br />
			<label class="rotulo rotuloMedidorCss" id="rotuloModelo">Local:</label>
			<span class="itemDetalhamento"><c:out value="${localInstalacao}"/></span><br />
		</fieldset>
	
		<fieldset id="vazaoCorretorCol2" class="colunaFinal2">		
			<label class="rotulo rotuloMedidorCss" id="rotuloTipoTransdutorPressao">Prote��o:</label>
			<span class="itemDetalhamento"><c:out value="${protecaoInstalacao}"/></span><br />
			<label class="rotulo rotuloMedidorCss" id="rotuloPressaoMaximaTransdutor">Funcion�rio:</label>
			<span class="itemDetalhamento"><c:out value="${funcionarioInstalacao}"/></span><br />
			<label class="rotulo rotuloMedidorCss" id="rotuloTipoTransdutorTemperatura">Motivo:</label>
			<span class="itemDetalhamento"><c:out value="${motivoInstalacao}"/></span><br />
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Cliente</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo">Nome:</label>
				<input type="hidden" name="nomeCompletoCliente" value="${nomeCompletoCliente}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${nomeCompletoCliente}"/></span><br />
				<label class="rotulo">CPF/CNPJ:</label>
				<input type="hidden" name="documentoFormatado" value="${documentoFormatado}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${documentoFormatado}"/></span>			
		</fieldset>			
		<fieldset class="colunaFinal detalhamentoClienteImovelPontoConsumoCol">
				<label class="rotulo">Endere�o:</label>
				<input type="hidden" name="enderecoFormatadoCliente" value="${enderecoFormatadoCliente}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${enderecoFormatadoCliente}"/></span><br />
				<label class="rotulo">E-mail:</label>
				<input type="hidden" name="emailCliente" value="${emailCliente}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${emailCliente}"/></span>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />
	
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Im�vel</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo">Descri��o:</label>
			<input type="hidden" name="nomeFantasiaImovel" value="${nomeFantasiaImovel}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${nomeFantasiaImovel}"/></span><br />
			<label class="rotulo">Matr�cula:</label>
			<input type="hidden" name="matriculaImovel" value="${matriculaImovel}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${matriculaImovel}"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo">Cidade:</label>
			<input type="hidden" name="cidadeImovel" value="${cidadeImovel}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cidadeImovel}"/></span><br />
			<label class="rotulo">Endere�o:</label>
			<input type="hidden" name="enderecoImovel" value="${enderecoImovel}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${enderecoImovel}"/></span>
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento" />

	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Ponto de Consumo</legend>
		<fieldset class="coluna detalhamentoColunaLarga detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo" id="rotuloDataMaximaInstalacao" >Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${descricaoPontoConsumo}"/></span><br />
			<label class="rotulo" id="rotuloNumeroDigitosLeitura" >Endere�o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${enderecoPontoConsumo}"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal detalhamentoClienteImovelPontoConsumoCol">
			<label class="rotulo" id="rotuloPressaoMaximaTrabalho" >CEP:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cepPontoConsumo}"/></span><br />
				
			<label class="rotulo" id="rotuloFaixaTemperaturaTrabalho">Complemento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${complementoPontoConsumo}"/></span>
		</fieldset>
	</fieldset>
</fieldset>    			        
<fieldset class="conteinerBotoes"> 
	<c:choose>
		<c:when test="${origemHistoricoMovimentacao}">
			<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltarHistoricoMovimentacaoMedidor();">    
		</c:when>
		<c:otherwise>
			<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">    
			<vacess:vacess param="exibirAtualizarMedidor">
		    	<input id="botaoAlterar" name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();" <c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
		    </vacess:vacess>
		</c:otherwise>
	</c:choose>
</fieldset>
	
</form:form>
