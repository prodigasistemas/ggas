<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script>

$(document).ready(function(){
	$("input.botaoRemoverFaixas").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'10px','cursor':'pointer'});
	$("#dataAquisicao").datepicker({changeYear: true, maxDate: '+0d',yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$("#dataUltimaCalibracao").datepicker({changeYear: true, maxDate: '+0d',yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	
	habilitarDesabilitarAbaComposicao()
	habilitarDesabilitarEnderecoRemoto();
});

function habilitarDesabilitarAbaComposicao(){
	if(document.forms['medidorForm'].modoUso[1].checked!= '1'){
		$("#medidorAbaComposicao").attr("disabled", "disabled");
		$("#abaComposicaoObrigatoria").hide();
		$("#leituraObrigatoria").show();
	}else{
		$("#medidorAbaComposicao").removeAttr("disabled");
		$("#abaComposicaoObrigatoria").show();
		$("#leituraObrigatoria").hide();
	}
	
}
function habilitarDesabilitarEnderecoRemoto(){
	if(document.forms['medidorForm'].modoUso[2].checked!= '1'){
		$("#enderecoRemoto").attr("disabled", "disabled");
		$("#enderecoRemoto").val("");
		$("#campoObrigatorioMI").hide();
		$("#rotuloEnderecoRemoto").addClass('rotuloDesabilitado');
		$("#enderecoRemoto").removeAttr("style");
	}else{
		$("#enderecoRemoto").removeAttr("disabled");
		$("#campoObrigatorioMI").show();
		$("#rotuloEnderecoRemoto").removeClass('rotuloDesabilitado');
	}
	
}
function exibirPopupPesquisaMedidor() {
	exibirIndicador();
	popup = window.open('exibirPesquisaMedidorPopup?retornaId=true&composicaoVirtual='+true,'popup','height=750,width=850,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function obterMedidor(idMedidorVirtual){
	idMedidorVirtual = trim(idMedidorVirtual);
	if(idMedidorVirtual != ""){
		document.forms['medidorForm'].idMedidorVirtual.value = idMedidorVirtual;
		submeter("medidorForm", "carregarListaMedidorDoAlterar#medidorAbaComposicao");
	}else{
		limparDadosMedidor();
	}
	return;
}

function removerMedidor(id){
	document.getElementById('medidorVirtualVO').value = id; 
	submeter("medidorForm", "removerItemListaMedidorDoAlterar#medidorAbaComposicao");
}

function limparFormulario() {
	<c:if test="${not bloquearAlteracaoNumeroSerie}">
	document.medidorForm.numeroSerie.value = "";
	</c:if>  
	document.medidorForm.tipoMedidor.value = "-1";
	document.medidorForm.marcaMedidor.value = "-1";
	<c:if test="${not bloquearAlteracaoMedidor}">
	document.medidorForm.idSituacao.value = "-1";
	</c:if>
	document.medidorForm.diametro.value = "-1";
	document.medidorForm.capacidadeMinima.value = "-1";
	document.medidorForm.capacidadeMaxima.value = "-1";
	document.medidorForm.anoFabricacao.value = "";
	document.medidorForm.dataAquisicao.value = "";
	document.medidorForm.anoCalibracao.value = "";
	document.medidorForm.dataUltimaCalibracao.value = "";
	document.medidorForm.tombamento.value = "";
	document.medidorForm.dataMaximaInstalacao.value = "";
	document.medidorForm.digito.value = "-1";
	document.medidorForm.fatorK.value = "-1";
	document.medidorForm.modelo.value = "-1";		
	document.medidorForm.numeroDigitosLeitura.value = "";
	document.medidorForm.pressaoMaximaTrabalho.value = "";
	document.medidorForm.unidadePressaoMaximaTrabalho.value = "-1";
	document.medidorForm.faixaTemperaturaTrabalho.value = "-1";
	document.medidorForm.localArmazenagem.value = "-1";
}

function alteraIdLocalArmazenagem() {
	document.medidorForm.localArmazenagem.value = document.medidorForm.disabledLocalArmazenagem.value;
}

function cancelar() {
	location.href = '<c:url value="/exibirPesquisaMedidor"/>';
}

</script>

<h1 class="tituloInterno">Alterar Medidor</h1>
<p class="orientacaoInicial">Para alterar um Medidor, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form:form method="post" action="alterarMedidor" id="medidorForm" name="medidorForm" ModelAttribute="MedidorImpl">
<input name="acao" type="hidden" id="acao" value="alterarMedidor"/>
<input type="hidden" id="medidorVirtualVO" name="medidorVirtualVO" value="" />
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${medidor.chavePrimaria}"/>
<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>


<fieldset id="tabs" style="display: none">

	<ul>
		<li><a href="#medidorAbaIdentificacao"><span class="campoObrigatorioSimboloTabs">* </span>Identifica��o</a></li>
		<li><a href="#medidorAbaCaracteristicas"><span class="campoObrigatorioSimboloTabs">* </span>Caracter�sticas</a></li>
		<li><a href="#medidorAbaHistoricoOperacao">Hist�rico de Opera��o</a></li>
		<li><a href="#medidorAbaComposicao"><span id="abaComposicaoObrigatoria" class="campoObrigatorioSimboloTabs">* </span>Composi��o</a></li>
		
	</ul>
		
	<fieldset id="medidorAbaIdentificacao">
		
		<a class="linkHelp" href="<help:help>/abaidentificaocadastrodomedidor.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>

		<fieldset class="colunaEsq" id="medidorAbaIdentificacaoCol1">
			<label class="rotulo" id="modoUsoRotulo" for="modoUso">Perfil:</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" onclick="habilitarDesabilitarAbaComposicao(); habilitarDesabilitarEnderecoRemoto()" value="${modoUsoNormal}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoNormal || medidor.modoUso eq null || medidor.modoUso eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Normal</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" onclick="habilitarDesabilitarAbaComposicao(); habilitarDesabilitarEnderecoRemoto()" value="${modoUsoVirtual}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Virtual</label> 
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" onclick="habilitarDesabilitarAbaComposicao(); habilitarDesabilitarEnderecoRemoto()" value="${modoUsoIndependente}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoIndependente}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Independente</label> 
			<label class="rotulo" id="rotuloNumeroSerieAlterar" for="numeroSerie"><span class="campoObrigatorioSimbolo">* </span>N�mero de s�rie:</label>

			<c:choose>
				<c:when test="${bloquearAlteracaoNumeroSerie}">
					<input class="campoTexto" type="hidden" id="numeroSerie" name="numeroSerie" value="${medidor.numeroSerie}">
					<input class="campoTexto" type="text" id="numeroSerieDesabilitado" name="numeroSerieDesabilitado" maxlength="20" size="20" value="${medidor.numeroSerie}" disabled="disabled">				
				</c:when>
				<c:otherwise>					
					<input class="campoTexto" type="text" id="numeroSerie" name="numeroSerie" maxlength="20" size="20" value="${medidor.numeroSerie}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumerico(event)');" onkeyup="letraMaiuscula(this);">				
				</c:otherwise>
			</c:choose>
			
			<label class="rotulo campoObrigatorio" id="rotuloTipo" for="tipo"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
			<select name="tipoMedidor" id="tipo" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoMedidor}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${medidor.tipoMedidor.chavePrimaria == tipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipo.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			
			<label class="rotulo campoObrigatorio" id="rotuloMarca" for="marca"><span class="campoObrigatorioSimbolo">* </span>Marca:</label>
			<select name="marcaMedidor" id="marca" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMarcaMedidor}" var="marca">
					<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${medidor.marcaMedidor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${marca.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			
			<label class="rotulo" id="rotuloModelo" for="modelo">Modelo:</label>
			<select name="modelo" id="modelo" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaModeloMedidor}" var="modelo">
					<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${medidor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${modelo.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			
			<label class="rotulo campoObrigatorio" id="rotuloSituacao" for="situacao"><span class="campoObrigatorioSimbolo">* </span>Situa��o:</label>
			<select name="situacaoMedidor" id="idSituacao" class="campoSelect" <c:if test="${bloquearAlteracaoMedidor}">disabled="disabled"</c:if>>				
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSituacaoMedidor}" var="situacao">
					<option value="<c:out value="${situacao.chavePrimaria}"/>"<c:if test="${medidor.situacaoMedidor.chavePrimaria == situacao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${situacao.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			<c:if test="${bloquearAlteracaoMedidor}"><input name="situacaoMedidor" type="hidden" id="idSituacao" value="${medidor.situacaoMedidor.chavePrimaria}" /> </c:if>		
			
			<label class="rotulo" id="rotuloDiametro" for="diametro">Di�metro:</label>
			<select name="diametroMedidor" id="diametro" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaDiametroMedidor}" var="diametro">
					<option value="<c:out value="${diametro.chavePrimaria}"/>" <c:if test="${medidor.diametroMedidor.chavePrimaria == diametro.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${diametro.descricao}"/>
					</option>
			    </c:forEach>
			</select>
			
			<label class="rotulo" id="rotuloEnderecoRemoto" for="enderecoRemoto">
				<span id="campoObrigatorioMI" class="campoObrigatorioSimbolo">* </span> Endere�o Remoto:
			</label>
			<input class="campoTexto" type="text" id="enderecoRemoto" name="codigoMedidorSupervisorio" maxlength="30" size="30" value="${medidor.codigoMedidorSupervisorio}">
		</fieldset>
		
		<fieldset id="medidorAbaIdentificacaoCol2">		
		
			<label class="rotulo campoObrigatorio" id="rotuloCapacidadeMinima" for="capacidadeMinima"><span class="campoObrigatorioSimbolo">* </span>Capacidade M�nima:</label>
			<select name="capacidadeMinima" id="capacidadeMinima" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaCapacidadeMedidor}" var="capacidade">
					<option value="<c:out value="${capacidade.chavePrimaria}"/>" <c:if test="${medidor.capacidadeMinima.chavePrimaria == capacidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${capacidade.descricao}"/>
					</option>
			    </c:forEach>
			</select>
		
			<label class="rotulo campoObrigatorio" id="rotuloCapacidadeMaxima" for="capacidadeMaxima"><span class="campoObrigatorioSimbolo">* </span>Capacidade M�xima:</label>
			<select name="capacidadeMaxima" id="capacidadeMaxima" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaCapacidadeMedidor}" var="capacidade">
					<option value="<c:out value="${capacidade.chavePrimaria}"/>" <c:if test="${medidor.capacidadeMaxima.chavePrimaria == capacidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${capacidade.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			
			<label class="rotulo" id="rotuloAnoFabricacao" for="anoFabricacao">Ano de Fabrica��o:</label>
			<select name="anoFabricacao" id="anoFabricacao" class="campoSelect">
				<option value="">Selecione</option>
				<c:forEach items="${listaAnosFabricacao}" var="ano">
				<option value="<c:out value="${ano}"/>" <c:if test="${medidor.anoFabricacao == ano}">selected="selected"</c:if>>
					<c:out value="${ano}"/>
				</option>		
			    </c:forEach>
			</select>
			
			<div style="margin-top: 90px">
				<label class="rotulo campoObrigatorio" id="rotuloDataAquisicao" for="dataAquisicao"><span class="campoObrigatorioSimbolo">* </span>Data de Aquisi��o:</label>
				<input class="campoData" type="text" id="dataAquisicao" name="dataAquisicao" maxlength="10" value="\<fmt:formatDate value="${medidor.dataAquisicao}" pattern="dd/MM/yyyy"/>" />
			</div>
			
			<div style="margin-top: 0px">
				<label class="rotulo" id="rotuloVidaUtil" for="anoCalibracao">Intervalo de Calibra��o:</label>
				<input class="campoTexto" type="text" id="anoCalibracao" name="anoCalibracao" maxlength="3" size="3" value="${medidor.anoCalibracao}" 
					onkeypress="return formatarCampoInteiro(event);" /> &nbsp; <div style="margin: 1px" > (em anos)</div>
			</div>
			<p></p>
			<div style="margin-top: 15px; ">
				<label class="rotulo" id="rotuloDataUltimaCalibracao" for="dataUltimaCalibracao">Data da Ultima Calibra��o:</label>
				<input class="campoData" type="text" id="dataUltimaCalibracao" name="dataUltimaCalibracao" maxlength="10"
					 value="<fmt:formatDate value="${medidor.dataUltimaCalibracao}" pattern="dd/MM/yyyy"/>" />
			</div>
			
			<label class="rotulo" id="rotuloTombamento" for="tombamento">N�mero do Tombamento:</label>
			<input class="campoTexto" type="text" id="tombamento" name="tombamento" maxlength="20" size="10" value="${medidor.tombamento}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoAlfaNumerico(event)');" onkeyup="letraMaiuscula(this);">
			
			<label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
		    <input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${medidor.habilitado eq 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
		   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${medidor.habilitado eq 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
		</fieldset>
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Medidor.</p>	
	</fieldset>
	
	<fieldset id="medidorAbaCaracteristicas">

		<a class="linkHelp" href="<help:help>/abacaractersticascadastrodomedidor.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
		
		<fieldset class="colunaEsq" id="medidorAbaCaracteristicaCol1">			

			<label class="rotulo" id="rotuloDataMaximaInstalacao" for="dataMaximaInstalacao">Data M�xima de Instala��o:</label>
			<input class="campoData" type="text" id="dataMaximaInstalacao" name="dataMaximaInstalacao" maxlength="10" value="\<fmt:formatDate value="${medidor.dataMaximaInstalacao}" pattern="dd/MM/yyyy"/>" >
			
			<label class="rotulo campoObrigatorio" id="rotuloNumeroDigitosLeitura" for="numeroDigitosLeitura"><span id="leituraObrigatoria" class="campoObrigatorioSimbolo">* </span>N�mero de D�gitos para Leitura:</label>
			<input class="campoTexto" type="text" id="numeroDigitosLeitura" name="digito" onkeypress="return formatarCampoInteiro(event);" maxlength="1" size="10" value="${medidor.digito}">
			
			<label class="rotulo" id="rotuloFatorK" for="idFatorK">Fator K:</label>
			<select name="fatorK" id="idFatorK" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaFatorK}" var="fatorK">
					<option value="<c:out value="${fatorK.chavePrimaria}"/>" <c:if test="${medidor.fatorK.chavePrimaria == fatorK.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${fatorK.fatorK}"/>
					</option>		
			    </c:forEach>
			</select>
			
			<label class="rotulo" id=rotuloMedidaPressao for="medidaPressao">
				 Medida Press�o:
			</label>
			<input class="campoTexto" type="text" id="medidaPressao" name="medidaPressao" onkeypress="return formatarCampoDecimal(event,this,5,4);" maxlength="22" size="22" value="${medidor.medidaPressao}">
		
			<label class="rotulo" id=rotuloMedidaVazaoInstantanea for="medidaVazaoInstantanea">
				 Medida Vaz�o:
			</label>
			<input class="campoTexto" type="text" id="medidaVazaoInstantanea" name="medidaVazaoInstantanea" onkeypress="return formatarCampoDecimal(event,this,5,4);" maxlength="22" size="22" value="${medidor.medidaVazaoInstantanea}">
			
			<label class="rotulo" id="unidadePressao" for="unidadePressao">Unidade Press�o:</label>
			<select name="unidadePressao" id="unidadePressao" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadePressao}" var="unidadePressao">
				<option value="<c:out value="${unidadePressao.chavePrimaria}"/>" <c:if test="${medidor.unidadePressao.chavePrimaria == unidadePressao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadePressao.descricao}"/>
				</option>		
			    </c:forEach>
			</select>			
			
		</fieldset>

		<fieldset id="medidorAbaCaracteristicaCol2">
		
			<label class="rotulo" id="rotuloPressaoMaximaTrabalho" for="pressaoMaximaTrabalho">Press�o m�xima de trabalho:</label>
			<input class="campoTexto campoHorizontal" type="text" id="pressaoMaximaTrabalho" name="pressaoMaximaTrabalho" onkeypress="return formatarCampoDecimal(event,this,5,4);" maxlength="11" size="9" value="${medidor.pressaoMaximaTrabalho}">
			<select name="unidadePressaoMaximaTrabalho" id=unidadePressaoMaximaTrabalho class="campoSelect campoHorizontal2">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadePressao}" var="unidade">
					<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${medidor.unidadePressaoMaximaTrabalho.chavePrimaria eq unidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidade.descricao}"/>
					</option>		
				</c:forEach>
			</select>	
			
			<label class="rotulo" id="rotuloFaixaTemperaturaTrabalho" for="faixaTemperaturaTrabalho">Faixa de temperatura de trabalho:</label>
			<select name="faixaTemperaturaTrabalho" id="faixaTemperaturaTrabalho" class="campoSelect campoHorizontal">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaFaixaTemperaturaTrabalho}" var="temperatura">
					<option value="<c:out value="${temperatura.chavePrimaria}"/>" <c:if test="${medidor.faixaTemperaturaTrabalho.chavePrimaria == temperatura.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${temperatura.descricaoFormatada}"/>
					</option>
			    </c:forEach>
			</select>
			
			<label class="rotulo" id="rotuloLocalArmazenagem" for="localArmazenagem"><span class="campoObrigatorioSimbolo">* </span>Local de Armazenagem:</label>
			<input type="hidden" id="localArmazenagem" name="localArmazenagem" value="<c:out value="${medidor.localArmazenagem.chavePrimaria}"/>" >
			<select id="disabledLocalArmazenagem" name="disabledLocalArmazenagem" class="campoSelect LocalArmazenagemCss" <c:if test="${bloquearAlteracaoLocalArmazenagem}">disabled="disabled"</c:if> onchange="alteraIdLocalArmazenagem();">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaLocalArmazenagem}" var="localArmazenagem">
					<option value="<c:out value="${localArmazenagem.chavePrimaria}"/>" <c:if test="${medidor.localArmazenagem.chavePrimaria == localArmazenagem.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${localArmazenagem.descricao}"/>
					</option>
			    </c:forEach>
			</select>		
		</fieldset>			
	</fieldset>
    <fieldset id="medidorAbaHistoricoOperacao">
		<c:if test="${!empty listaHistoricoOperacaoMedidor}">
			<fieldset id="instalacaoMedidor" class="conteinerBloco">
				<display:table id="historicoOperacaoMedidor" class="dataTableGGAS dataTableAba" export="false" name="listaHistoricoOperacaoMedidor" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
			     	
				    <display:column media="html" sortProperty="pontoConsumo.descricaoFormatada" property="pontoConsumo.descricaoFormatada"  
				    	title="Ponto de Consumo" style="text-align: left; padding-left: 5px;" />
				   
				    <display:column media="html" title="Opera��o" style="width: 150px;">
				    	<c:out value='${historicoOperacaoMedidor.operacaoMedidor.descricao}'/>
			    	</display:column>
				    
				    <display:column media="html" title="Data" style="width: 80px;">
			 			<fmt:formatDate value="${historicoOperacaoMedidor.dataRealizada}" type="both" pattern="dd/MM/yyyy" />
				    </display:column>
				    
				    <display:column media="html" title="Leitura" style="width: 70px;">
			 			<c:out value='${historicoOperacaoMedidor.numeroLeitura}'/>
				    </display:column>
				    
				    <display:column media="html" title="Local de Instala��o" style="width: 120px;">
			 			<c:out value='${historicoOperacaoMedidor.medidorLocalInstalacao.descricao}'/>
				    </display:column>

				    <display:column media="html" title="Funcion�rio" style="width: 120px;">
			 			<c:out value='${historicoOperacaoMedidor.funcionario.nome}'/>
				    </display:column>
				    
				    <display:column media="html" title="Press�o" style="width: 80px;">
				    	<c:out value='${historicoOperacaoMedidor.medidaPressaoAnterior}'/>
				    	<c:out value='${historicoOperacaoMedidor.unidadePressaoAnterior.descricaoAbreviada}'/>
			    	</display:column>
			    	
			    </display:table>
			</fieldset>
		</c:if>			        	
	</fieldset>	
		<fieldset id="medidorAbaComposicao">
		<c:if test="${listaMedidor  ne null }">
			<display:table class="dataTableGGAS" name="listaMedidor" sort="list" id="medidorVirtualVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		        <display:column sortable="faslse" sortProperty="numeroSerie" style="width: 400px;" title="Medidor">
	            	<c:out value="${medidorVirtualVO.numeroSerie}"/>
	            	<input type="hidden" id="listaChavePrimaria" name="listaChavePrimaria" value="${medidorVirtualVO.id}"/>
					<input type="hidden" id="listaNumeroSerie" name="listaNumeroSerie" value="${medidorVirtualVO.numeroSerie}" />
		     	</display:column>	
	   			<display:column title="Opera��o">
	   				<select id="listaOperacao" name="listaOperacao" style="width: 50px;">
	   					<c:choose>
	   						<c:when test="${medidorVirtualVO.operacao ne null}">
			   					<c:if test="${medidorVirtualVO.operacao=='+'}">
				   					<option value="+" selected="selected"> + </option>
				   					<option value="-">-</option>
			   					</c:if>
			   					<c:if test="${medidorVirtualVO.operacao=='-'}">
				   					<option value="+">+</option>
				   					<option value="-" selected="selected"> - </option>
			   					</c:if>
	   						</c:when>
	   						<c:otherwise>
	   							<option value="+" selected="selected" > + </option>
				   				<option value="-"> - </option>
	   						</c:otherwise>
	   					</c:choose>
	   				</select>
		        </display:column>
		        <display:column title="" >
		        	<div style="padding-top: 15px;">
		        		<input type='button' name="btnRemoverMedidor" class='botaoRemoverFaixas' onClick="removerMedidor('<c:out value='${medidorVirtualVO.id}'/>');" title='Remover'/>
		        	</div>
			    </display:column>
		    </display:table>	
		    
		</c:if>
		<br />
		<input name="Button" id="botaoPesquisarMedidor" class="bottonRightCol2" margin-right: 0; width: 140px" title="Pesquisar Medidor"  value="Pesquisar Medidor" onclick="exibirPopupPesquisaMedidor();" type="button">
	<input type="hidden" id="idMedidorVirtual" name="idMedidorVirtual">
	</fieldset>
</fieldset>


<fieldset class="conteinerBotoes"> 
	<input name="ButtonCancelar" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="ButtonLimpar" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <input id="botaoSalvar" name="ButtonSalvar" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit">
 </fieldset>
</form:form> 
