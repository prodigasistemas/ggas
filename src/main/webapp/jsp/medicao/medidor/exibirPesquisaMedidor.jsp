<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Pesquisar Medidor<a class="linkHelp" href="<help:help>/consultadosmedidores.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarMedidor" id="medidorForm" name="medidorForm">

	<script language="javascript">
	
	$(document).ready(function(){
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
			document.medidorForm.modoUso[0].checked = true;
		}
	});
	
	function removerMedidor(){
		var selecao = verificarSelecao();
		if (selecao == true) {	
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('medidorForm', 'removerMedidor');
			}
	    }
	}
	
	function alterarMedidor(){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('medidorForm', 'exibirAtualizarMedidor');
	    }
	}
	
	function detalharMedidor(chave){
		document.forms[0].chavePrimaria.value = chave;
		submeter("medidorForm", "exibirDetalhamentoMedidor");
	}
	
	function incluir() {
		location.href = '<c:url value="/exibirInclusaoMedidor"/>';
	}
	
	function limparFormulario(){
		document.medidorForm.numeroSerie.value = "";
		document.medidorForm.modoUso.value = "";
		document.medidorForm.tipoMedidor.value = "-1";
		document.medidorForm.marcaMedidor.value = "-1";
		document.medidorForm.modelo.value = "-1";
		document.medidorForm.localArmazenagem.value = "-1";
		document.medidorForm.modoUso[0].checked = true;
		document.forms[0].habilitado[0].checked = true;
	}
	
	</script>
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1" class="coluna">
			<label class="rotulo" id="rotuloNomeEmpresa" for="nomeCompletoCliente" >N�mero de s�rie:</label>
			<input class="campoTexto" type="text" name="numeroSerie" id="numeroSerie" maxlength="20" size="20" value="${medidor.numeroSerie}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/><br />
			
			<label class="rotulo" id="rotuloTipo" for="tipo">Tipo:</label>
			<select name="tipoMedidor" id="tipo" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoMedidor}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${medidor.tipoMedidor.chavePrimaria == tipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipo.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			
			<label class="rotulo" id="modoUsoRotulo" for="modoUso">Perfil:</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" value="${modoUsoNormal}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoNormal}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Normal</label>
			<input class="campoRadio" type="radio" name="modoUso" id="modoUso" value="${modoUsoVirtual}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoVirtual}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Virtual</label>
			<input class="campoRadio modoUso" type="radio" name="modoUso" id="modoUso" value="${modoUsoIndependente}" <c:if test="${medidor.modoUso.chavePrimaria eq modoUsoIndependente}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Independente</label>
			<input class="campoRadio modoUso" type="radio" name="modoUso" id="modoUso" value="null" <c:if test="${medidor.modoUso eq null || medidor.modoUso eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="modoUso">Todos</label>
		</fieldset>
		
		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloMarca" style="width: 150px" for="marca">Marca:</label>
			<select name="marcaMedidor" id="marca" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMarcaMedidor}" var="marca">
					<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${medidor.marcaMedidor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${marca.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />	
			
			<label class="rotulo" id="rotuloModelo" for="modelo" style="width: 150px">Modelo:</label>
			<select name="modelo" id="modeloMedidor" class="campoSelect">
				<option value="-1">Selecione</option> 
				<c:forEach items="${listaModeloMedidor}" var="modelo">
					<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${medidor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${modelo.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
			<label class="rotulo" id="rotuloLocalArmazenagem" for="localArmazenagem" style="width: 150px">Local de Armazenagem:</label>
			<select name="localArmazenagem" id="localArmazenagem" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaLocalArmazenagem}" var="localArmazenagem">
				<option value="<c:out value="${localArmazenagem.chavePrimaria}"/>" <c:if test="${medidor.localArmazenagem.chavePrimaria == localArmazenagem.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${localArmazenagem.descricao}"/>
				</option>		
			    </c:forEach>
			</select>
			<label class="rotulo" for="habilitado" style="width: 150px">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<%--<vacess:vacess param="pesquisarMedidor">--%>		
	    		<input name="ButtonPesquisar" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	<%--</vacess:vacess>--%>		
			<input name="ButtonLimpar" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${medidores ne null}">
		<hr class="linhaSeparadoraPesquisa" />
	    <display:table class="dataTableGGAS dataTableCabecalho2Linhas" decorator="br.com.ggas.web.cadastro.imovel.decorator.ImovelResultadoPesquisaDecorator"  name="medidores" id="medidor" partialList="true" sort="external" pagesize="15" size="${pageSize}"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarMedidor">
	     	<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${medidor.chavePrimaria}">
	        </display:column>
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${medidor.habilitado == 1}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        <display:column sortable="true" title="N�mero de S�rie" sortProperty="numeroSerie" style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${medidor.numeroSerie}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Modelo" sortProperty="modelo" style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${medidor.modeloMedidor}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Tipo" sortProperty="tipoMedidor" style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${medidor.tipoMedidor}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Marca" sortProperty="marcaMedidor" style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${medidor.marcaMedidor}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Vaz�o" sortProperty="anoFabricacao" style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>        	
	            		<c:choose>
		            		<c:when test="${medidor.vazao eq null}"> 
		            			<c:out value="${medidor.medidaVazaoInstantaneaMedidor}"></c:out>
		            		</c:when>
							<c:otherwise> 
		            			<c:out value="${medidor.vazao}"/>
							</c:otherwise>
						</c:choose>
	            </a>            
	        </display:column>
	        <display:column sortable="true" title="Press�o" sortProperty="tombamento"  style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            		<c:choose>
		            		<c:when test="${medidor.pressao eq null}"> 
		            			<c:out value="${medidor.medidaPressaoMedidor}"></c:out>
		            		</c:when>
							<c:otherwise> 
		            			<c:out value="${medidor.pressao}"/>
							</c:otherwise>
						</c:choose>	            	
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Unidade Press�o" sortProperty="tombamento"  style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            		<c:choose>
		            		<c:when test="${medidor.unidadePressao eq null}"> 
		            			<c:out value="${medidor.unidadePressaoMedidor}"></c:out>
		            		</c:when>
							<c:otherwise> 
		            			<c:out value="${medidor.unidadePressao}"/>
							</c:otherwise>
						</c:choose>	            	        		
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Local Armazenagem" sortProperty="localArmazenagem" style="width: 15.5%">
	        	<a href="javascript:detalharMedidor(<c:out value='${medidor.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${medidor.localMedidor}"/>
	            </a>
	        </display:column>
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty medidores}">
  			<%--<vacess:vacess param="exibirAtualizarMedidor">--%>
  				<input	id="botaoAlterar"
  						value="Alterar" class="bottonRightCol2" 
  						onclick="alterarMedidor()" 
  						type="button"
  					<c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
  			<%--</vacess:vacess>--%>
  			<%--<vacess:vacess param="removerMedidor">--%>
				<input 	id="botaoRemover"
						value="Remover" 
						class="bottonRightCol2 bottonLeftColUltimo" 
						onclick="removerMedidor()" 
						type="button"
					<c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
			<%--</vacess:vacess>--%>
   		</c:if>
   		<%--<vacess:vacess param="exibirInclusaoMedidor">--%>
   			<input 	id="botaoIncluir"
   					name="buttonIncluir" value="Incluir" 
   					class="bottonRightCol2 botaoGrande1" 
   					onclick="incluir()" type="button"
   				<c:if test="${existeCadastroBens}">disabled="disabled"</c:if>>
   		<%--</vacess:vacess>--%>
	</fieldset>
	
</form:form>
