<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Pesquisar Dados de Medi��o<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">

$(document).ready(function(){
	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$("#mesAnoReferencia").inputmask("99/9999",{placeholder:"_"});
	
	$( "#chaveGrupoFaturamento" ).change(function() {
		carregarRotaPorGrupoFaturamento(this.value, "-1");
	});
	
	var confirmacaoLeitura = "<c:out value="${confirmacaoLeitura}"/>";
	esconderMostrarAnormalidadeLeitura(confirmacaoLeitura);
	
	var grupoFaturamento = $("#chaveGrupoFaturamento").val();
	var chaveRota = "<c:out value="${leituraMovimento.chaveRota}"/>";
	if(chaveRota == ""){
		chaveRota = "-1";
	}
	carregarRotaPorGrupoFaturamento(grupoFaturamento, chaveRota);
	
});

function esconderMostrarAnormalidadeLeitura(confirmacaoLeitura){
	if(confirmacaoLeitura != "false"){
		$("#divAnormaLidadeLeitura").show();			
	}else{
		$("#divAnormaLidadeLeitura").hide();
		$("#chaveAnormaLidade").val("-1");
	}
}


function pesquisar() {
	var chaveAnormalidadeLeitura = $("#chaveAnormaLidade").val();
	$("#chaveAnormalidadeLeitura").val(chaveAnormalidadeLeitura);
	submeter('dadosMedicaoForm','pesquisarDadosMedicao');		
}


function detalhar(chave, anoMesFaturamento, processado){
	$("#idRota").val(chave);
	$("#anoMesFaturamento").val(anoMesFaturamento);
	$("#processado").val(processado);
	submeter('dadosMedicaoForm','exibirDetalhamentoDadosMedicao');
}

function limparFormulario(){
	limparFormularios(document.dadosMedicaoForm);
	document.forms['dadosMedicaoForm'].confirmacaoLeitura[2].checked = true;
	$("#divAnormaLidadeLeitura").show();
}

function carregarRotaPorGrupoFaturamento(chaveGrupoFaturamento, chaveRota){
	var noCache = "noCache=" + new Date().getTime();
	$("#divDadosMedicaoRota").load("carregarRotaPorGrupoFaturamento?chaveGrupoFaturamento=" + chaveGrupoFaturamento + "&chaveRotaGrupoFaturamento=" + chaveRota + "&" + noCache);
}

function encerrarRota() {
	var selecao = verificarSelecao();
	var checarEncerrarRota = true;
	if (selecao == true) {
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_ENCERRAR_ROTA"/>');
		if(retorno == true) {
			
			$('#leituraMovimento tr').filter(':has(:checkbox:checked)').each(function() {
			    
				AjaxService.checarEncerrarRotasCronogramaAberto($(this).find("td:eq(2)").text().trim(), $(this).find("td:eq(3)").text().trim(), {
					callback: function(retorno) {
						if(!retorno) {
							checarEncerrarRota = false;
							return false;
						}
					}, async:false}
		        );	
				

			}); 
			
			if(checarEncerrarRota) {
				submeter('dadosMedicaoForm','encerrarRota');
			} else {
				alert ("A ref�rencia de uma ou mais rotas selecionadas est� diferente do cronograma em aberto!");
			}
		}
    }
}

</script>

<form action="pesquisarDadosMedicao" id="dadosMedicaoForm" name="dadosMedicaoForm" method="post" modelAttribute="DadosMedicaoVO">
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
<!-- 		<input name="chaveRota" type="hidden" id="chaveRota" > -->
		<input type="hidden" id="idRota" name="idRota" value="${idRota}">
		<input type="hidden" id="anoMesFaturamento" name="anoMesFaturamento" value="${anoMesFaturamento}">
		<input type="hidden" id="processado" name="processado">
		<input type="hidden" id="chaveAnormalidadeLeitura" name="chaveAnormalidadeLeitura">
		<input type="hidden" id="situacaoLeituraMovimento" name="situacaoLeituraMovimento">
		<input type="hidden" id="idImovel" name="idImovel">
		<input type="hidden" id="numeroSerieMedidor" name="numeroSerieMedidor" />
		<input type="hidden" id="dataLeituraFiltro" name="dataLeituraFiltro">
				
		<fieldset id="materialCol1" class="coluna">
			
			<label class="rotulo">Grupo de Faturamento:</label>
			<select id="chaveGrupoFaturamento" class="campoSelect" name="chaveGrupoFaturamento">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaGrupoFaturamento}" var="grupo">
				<option value="<c:out value="${grupo.chavePrimaria}"/>" <c:if test="${leituraMovimento.chaveGrupoFaturamento == grupo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${grupo.descricao}"/>
				</option>
				</c:forEach>
			</select>	
			
			<label class="rotulo">Rota:</label>
			<div id="divDadosMedicaoRota">
				<jsp:include page="/jsp/medicao/dadosmedicao/selectDadosMedicaoRota.jsp"></jsp:include>
			</div>
			
			<label class="rotulo rotulo2Linhas" id="rotuloReferencia" for="referencia">Refer�ncia de Leitura:</label>
			<input class="campoTexto campoHorizontal" type="text" size="5" id="mesAnoReferencia" name="mesAnoReferencia" onkeypress="return formatarCampoInteiro(event,6);" value="${leituraMovimento.mesAnoReferencia}">

				
			<label class="rotulo">Ciclo:</label><br />
			<select id="ciclo" name="ciclo" class="campoSelect">
			<option value="-1">Selecione</option>
				<c:forEach begin="1" end="4" var="numeroCiclo">
					<option value="${numeroCiclo}" <c:if test="${leituraMovimento.ciclo == numeroCiclo}">selected="selected"</c:if>>${numeroCiclo}</option>
				</c:forEach>
			</select>
				
		</fieldset>
		
		<fieldset id="materialCol2" class="colunaFinal">
			<label class="rotulo" >Per�odo da Leitura:</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataLeituraInicio" name="dataLeituraInicio" maxlength="10" value="${leituraMovimento.dataLeituraInicio}">
			<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataLeituraFim" name="dataLeituraFim" maxlength="10" value="${leituraMovimento.dataLeituraFim}">
		
			<label class="rotulo rotulo2Linhas">Ocorr�ncia de Medi��o:</label>
			<input class="campoRadio" type="radio" name="confirmacaoLeitura" id="confirmacaoLeitura" value="true" onclick="esconderMostrarAnormalidadeLeitura(this.value)" <c:if test="${confirmacaoLeitura eq 'true'}">checked</c:if>>
			<label class="rotuloRadio">Com Ocorr�ncia</label>
			<input class="campoRadio" type="radio" name="confirmacaoLeitura" id="confirmacaoLeitura" value="false" onclick="esconderMostrarAnormalidadeLeitura(this.value)" <c:if test="${confirmacaoLeitura eq 'false'}">checked</c:if>>
			<label class="rotuloRadio">Sem Ocorr�ncia</label>
			<input class="campoRadio" type="radio" name="confirmacaoLeitura" id="confirmacaoLeitura" value="" onclick="esconderMostrarAnormalidadeLeitura(this.value)" <c:if test="${confirmacaoLeitura eq ''}">checked</c:if>>
			<label class="rotuloRadio">Todos</label>
			</br>
			
			<label class="rotulo rotulo2Linhas">Processados:</label>
			<input class="campoRadio" type="radio" name="indicadorProcessado" id="indicadorProcessado" value="4" <c:if test="${leituraMovimento.indicadorProcessado eq '4'}">checked</c:if>>
			<label class="rotuloRadio">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorProcessado" id="indicadorProcessado" value="3"  <c:if test="${leituraMovimento.indicadorProcessado eq '3'}">checked</c:if>>
			<label class="rotuloRadio">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorProcessado" id="indicadorProcessado" value=""  <c:if test="${leituraMovimento.indicadorProcessado eq ''}">checked</c:if>>
			<label class="rotuloRadio">Todos</label>
			</br>
			
			<div id="divAnormaLidadeLeitura">
				<label class="rotulo">Anormalidade:</label>
				<select id="chaveAnormaLidade" name="chaveAnormaLidade" class="campoSelect">
				<option value="-1">Selecione</option>
					<c:forEach items="${listaAnormalidadeLeitura}" var="anormalidadeLeitura">
						<option value="<c:out value="${anormalidadeLeitura.chavePrimaria}"/>" <c:if test="${leituraMovimento.chaveAnormalidadeLeitura == anormalidadeLeitura.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${anormalidadeLeitura.descricao}"/>
						</option>
					</c:forEach>
				</select>
			</div>
			
		</fieldset>	
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="exibirPesquisaDadosMedicao">		
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisar();">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>	

<c:if test="${listaLeituraMovimento ne null}">
	<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
	<span style="margin-right: 20px">Leitura Sem Anormalidade</span>
		
	<img id="imagem" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
	<span style="margin-right: 20px">Leitura Com Anormalidade</span>

	
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaLeituraMovimento" sort="list" id="leituraMovimento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	     	<display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
      			<input type="checkbox" name="chavesPrimarias" class="checkboxRotas" value="${leituraMovimento.chaveRota}">
			</display:column>   
		
			<display:column sortable="false" sortProperty="descricaoAnormalidade" title="Anormalidade" style="width: 130px">
				<c:choose>
					<c:when test="${leituraMovimento.quantidadeAnormalidade == 0}">
	            		<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">	            			
					</c:when>
					<c:otherwise>
	            		<img id="imagem" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>				
			
			<display:column sortable="true" sortProperty="referenciaCiclo" title="Refer�ncia Ciclo">
	            <c:choose>
					<c:when test="${leituraMovimento.processado == 4}">
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'processado');"><span class="linkInvisivel"></span>
			            	<c:out value="${leituraMovimento.referenciaCiclo}"/>
			            </a>
					</c:when>
					<c:otherwise>
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'naoProcessado');"><span class="linkInvisivel"></span>
			            	<c:out value="${leituraMovimento.referenciaCiclo}"/>
			            </a>
					</c:otherwise>
				</c:choose>

			</display:column>				
			
			<display:column sortable="true" sortProperty="descricaoGrupoFaturamento" title="Grupo de Faturamento">
	            <c:choose>
					<c:when test="${leituraMovimento.processado == 4}">
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'processado');"><span class="linkInvisivel"></span>
			            	<c:out value="${leituraMovimento.descricaoGrupoFaturamento}"/>
			            </a>
					</c:when>
					<c:otherwise>
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'naoProcessado');"><span class="linkInvisivel"></span>
			            	<c:out value="${leituraMovimento.descricaoGrupoFaturamento}"/>
			            </a>
					</c:otherwise>
				</c:choose>
			</display:column>				
			
			<display:column sortable="true" sortProperty="numeroRota" title="Rota">
	            <c:choose>
					<c:when test="${leituraMovimento.processado == 4}">
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'processado');"><span class="linkInvisivel"></span>
			            	<c:out value="${leituraMovimento.numeroRota}"/>
			            </a>
					</c:when>
					<c:otherwise>
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'naoProcessado');"><span class="linkInvisivel"></span>
			            	<c:out value="${leituraMovimento.numeroRota}"/>
			            </a>
					</c:otherwise>
				</c:choose>
			</display:column>				

			<display:column sortable="true" sortProperty="quantidadePontoConsumo" title="Quantidade de Pontos de Consumo">
	            <c:choose>
					<c:when test="${leituraMovimento.processado == 4}">
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'processado');"><span class="linkInvisivel"></span>
	            			<c:out value="${leituraMovimento.quantidadePontoConsumo}"/>
	           			</a>
					</c:when>
					<c:otherwise>
						<a href="javascript:detalhar(<c:out value='${leituraMovimento.chaveRota}'/>, <c:out value='${leituraMovimento.anoMesFaturamento}'/>, 'naoProcessado');"><span class="linkInvisivel"></span>
	            			<c:out value="${leituraMovimento.quantidadePontoConsumo}"/>
	           			</a>
					</c:otherwise>
				</c:choose>
			</display:column>				
			
			<display:column sortable="false" sortProperty="processado" title="Processado">
	            <c:choose>
					<c:when test="${leituraMovimento.processado == 4}">
						Sim
					</c:when>
					<c:otherwise>
						N�o
					</c:otherwise>
				</c:choose>
			</display:column>				

			
	    </display:table>
	   	<fieldset class="conteinerBotoesPesquisarDirFixo">	
			<input name="Button" class="bottonRightCol2" value="Encerrar Rota" type="button" onclick="encerrarRota();">		
		</fieldset>
			
	</c:if>
	
</form>
