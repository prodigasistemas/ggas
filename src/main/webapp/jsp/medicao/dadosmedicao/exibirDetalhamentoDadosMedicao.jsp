<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js" integrity="sha512-jTgBq4+dMYh73dquskmUFEgMY5mptcbqSw2rmhOZZSJjZbD2wMt0H5nhqWtleVkyBEjmzid5nyERPSNBafG4GQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">

$(document).ready(function(){
	
// 	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	//$("#dataLeituraFiltro").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

	var datepicker = $.fn.datepicker.noConflict();
	$.fn.bootstrapDP = datepicker;  
	$('.bootstrapDP').bootstrapDP({
								format: 'dd/mm/yyyy',
								language: 'pt-BR'
							});
	
	
	$('#leituraMovimento').dataTable( {
		"paging": true,
		<c:if test="${param.leituraMovimento_length != null}" >
		"pageLength": <c:out value="${param.leituraMovimento_length}" />,
		</c:if>
		"language": {
            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
		}
	} );
	
	Inputmask("decimal", {
	    radixPoint: ',',
	    groupSeparator: '.',
	    digits: 2,
	    inputtype: "text"
	}).mask('.valorLeitura');
	
	$('input[name$="dataLeitura"]').inputmask("99/99/9999",{placeholder:"_"});

	var fluxoDetalhar = "<c:out value="${ fluxoDetalhar }" />";
	
	if(fluxoDetalhar != ""){
		$("#leiturista").prop("disabled", true);
	}
	
	var processado = "<c:out value="${ processado }" />";
	if(processado != "naoProcessado"){
		$("#botaoSalvar").prop("disabled", true);
		$("#leiturista").prop("disabled", true);
	}

	var opt = {
			autoOpen: false,			
			width: 800,
			modal: true,
			minHeight: 120,
			resizable: false
	};

	$("a[rel=modal]").click( function(ev){
		 $("#fotoColetorPopup").dialog(opt).dialog("open");
    });
	
});

function selecionarFoto(idRota, anoMesFaturamento, leituraMovimentoId){
	var idRota = idRota;
	var anoMesFaturamento = anoMesFaturamento;
	var leituraMovimentoId = leituraMovimentoId;

	$("#fotoColetorPopup").html("");
	if(idRota != '' || anoMesFaturamento != '') {
		AjaxService.obterFotoPorIdDadosMedicao(idRota, anoMesFaturamento, leituraMovimentoId, {
			callback: function(urlFoto) {
				 var inner = '';
				 inner = inner +'<center>';
				 inner = inner + '<img src="<c:url value="' + urlFoto + '"/>" style=" width:100%;">';
				 inner = inner +'</center>';
				 $("#fotoColetorPopup").append(inner);
			}, async:false}
        );	 
	}  
} 


function alterarDadosMedicao(idLeituraMovimento) {
	payload = {
		chaveRota: parseInt($('#chaveRota').val()),
		anoMesFaturamento: $('#anoMesFaturamento').val(),
		chaveLeiturista: parseInt($('#leiturista option:selected').val()),
		dataLeitura: $('#dataLeitura_'+idLeituraMovimento).val(),
		dataLeituraAnterior: $('#dataLeituraAnterior_'+idLeituraMovimento).val(),
		valorLeitura:  parseFloat($('#valorLeitura_'+idLeituraMovimento).val().replaceAll('.','').replace(',','.')),
		valorPressao: parseFloat($('#valorPressao_'+idLeituraMovimento).val()),
		valorTemperatura: parseFloat($('#valorTemperatura_'+idLeituraMovimento).val()),
		anormalidadeLeitura: parseInt($('#anormalidadeLeitura_'+idLeituraMovimento+' option:selected').val()),
		observacoesCadastrais: $('#observacoesCadastrais_'+idLeituraMovimento).val(),
		chavePrimaria: idLeituraMovimento
	}
	if (!payload.dataLeitura && payload.anormalidadeLeitura != 35) {
		$('#dataLeitura_'+idLeituraMovimento).addClass('is-invalid');
		$('#erro_dataLeitura_'+idLeituraMovimento).text('Data de leitura � obrigat�rio');
		return false
	};
	$('#dataLeitura_'+idLeituraMovimento).removeClass('is-invalid');
	if (payload.valorLeitura !== 0 && !payload.valorLeitura && payload.anormalidadeLeitura != 35) {
		$('#valorLeitura_'+idLeituraMovimento).addClass('is-invalid');
		$('#erro_valorLeitura_'+idLeituraMovimento).text('Valor da leitura � obrigat�rio');
		return false
	};
	$('#valorLeitura'+idLeituraMovimento).removeClass('is-invalid');
	
	console.log(payload);
	$.ajax({
		type: 'POST',
		url: '/ggas/alterarDadosMedicaoAJAX',
		data: JSON.stringify(payload),
		success: function(data) {
			try {
				if (data.erro) {
					$('#salvar_'+idLeituraMovimento).removeClass('btn-outline-primary');
					$('#salvar_'+idLeituraMovimento).addClass('btn-outline-danger');
					$('#erro_leituraMovimento_'+idLeituraMovimento).removeClass('hide');
					$('#erro_leituraMovimento_'+idLeituraMovimento).text(data.erro);
					return false;
				}
				$('#salvar_'+idLeituraMovimento).removeClass('btn-outline-primary');
				$('#salvar_'+idLeituraMovimento).addClass('btn-outline-success');
			} catch (e) {
				$('#salvar_'+idLeituraMovimento).removeClass('btn-outline-primary');
				$('#salvar_'+idLeituraMovimento).addClass('btn-outline-danger');
				$('#erro_leituraMovimento_'+idLeituraMovimento).removeClass('hide');
				$('#erro_leituraMovimento_'+idLeituraMovimento).text('Ocorreu um erro ao tentar salvar os dados de medi��o');
			}
		},
		beforeSend: function(jqXHR, settings) {
			$('#erro_leituraMovimento_'+idLeituraMovimento).addClass('hide');
			$('#salvar_'+idLeituraMovimento).removeClass('btn-outline-danger');
			$('#wait_'+idLeituraMovimento).removeClass('hide');
			$('#salvar_'+idLeituraMovimento).addClass('hide');
		},
		complete: function(data) {
			$('#wait_'+idLeituraMovimento).addClass('hide');
			$('#salvar_'+idLeituraMovimento).removeClass('hide');
		},
		contentType: 'application/json'
	});
	return true;
}

function salvarAlteracao() {
	var datas = $('input[name="dataLeitura"]').val();
	var dataFormAnterior = $('input[name="dataLeituraAnterior"]').val();
	
   	removerVirgulaNumero('valorTemperatura');
    removerVirgulaNumero('valorPressao');
	
	var i = 0;
	var aux = 0;
	var anormalidadeLeitura = $( "#anormalidadeLeitura option:selected" ).val();
	
	var textoAnormalidadeLeitura = $( "#anormalidadeLeitura option:selected" ).text().trim();
	
	if (textoAnormalidadeLeitura == "Ponto n�o deve ser faturado"
				|| (datas != '' && document.getElementById("valorLeitura").value != '')) {
				
		AjaxService.validarDatas(datas, dataFormAnterior, anormalidadeLeitura,
			{callback: function(numero) {
    			if(numero == 0){
    				submeter('dadosMedicaoForm','alterarDadosMedicao');
    			}else{
    				aux = numero;
    			}
    		}, async: false}
		);

		if(aux == 1){
			alert("Erro: A data da leitura informada n�o pode ser maior que a data corrente ou menor que a data da �ltima leitura " + dataFormAnterior+"." );
		}if(aux == 2){
			alert("Erro: A data da leitura informada n�o pode ser maior que a data corrente ou menor que a data da �ltima leitura " + dataFormAnterior+"." );
		}if(aux == 3){
			alert("Erro: Data da leitura � de preenchimento obrigat�rio.");
		}
	}else{
		alert("Erro: Data da leitura e a leitura s�o de preenchimento obrigat�rio.");
	}


}

function removerVirgulaNumero(name) {
    $( "[name='" + name + "']" ).each(function(index) {
        $(this).val($(this).val().replace(',','.'));
    });
}

function cancelar() {
	$("#chaveRota").val("-1");
	submeter('dadosMedicaoForm','pesquisarDadosMedicao');
}

function limparFormulario(){
	limparFormularios(document.dadosMedicaoForm);
}

function exibirPopupPesquisaImovel() {
	popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function selecionarImovel(idSelecionado){
	var idImovel = document.getElementById("idImovel");
	var matriculaImovel = document.getElementById("matriculaImovel");
	var nomeFantasia = document.getElementById("nomeFantasiaImovel");
	var numeroImovel = document.getElementById("numeroImovel");
	var cidadeImovel = document.getElementById("cidadeImovel");
	var indicadorCondominio = document.getElementById("condominio");
	
	if(idSelecionado != '') {				
		AjaxService.obterImovelPorChave( idSelecionado, {
           	callback: function(imovel) {	           		
           		if(imovel != null){  	           			        		      		         		
	               	idImovel.value = imovel["chavePrimaria"];
	               	matriculaImovel.value = imovel["chavePrimaria"];		               	
	               	nomeFantasia.value = imovel["nomeFantasia"];
	               	numeroImovel.value = imovel["numeroImovel"];
	               	cidadeImovel.value = imovel["cidadeImovel"];
	               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
               	}
        	}, async:false}
        	
        );	        
    } else {
   		idImovel.value = "";
    	matriculaImovel.value = "";
    	nomeFantasia.value = "";
		numeroImovel.value = "";
          	cidadeImovel.value = "";
          	indicadorCondominio.value = "";
   	}

	document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
	document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
	document.getElementById("numeroImovelTexto").value = numeroImovel.value;
	document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
	if(indicadorCondominio.value == 'true') {
		document.getElementById("indicadorCondominioImovelTexto1").checked = true;
	} else {
		document.getElementById("indicadorCondominioImovelTexto2").checked = true;
	}
}

function limparFormularioFiltro(){
	$("#dataLeituraFiltro").val("");
	$("#numeroSerieMedidor").val("");
	$("#nomeImovelTexto").val("");
	$("#matriculaImovelTexto").val("");
	$("#numeroImovelTexto").val("");
	$("#cidadeImovelTexto").val("");
	$("#nomeFantasiaImovel").val("");
	$("#matriculaImovel").val("");
	$("#numeroImovel").val("");
	$("#cidadeImovel").val("");
	$("#condominio").val("");
	$("#idImovel").val("");
	$("#situacaoLeituraMovimentoTodos").attr('checked','checked');
	$("#situacaoLeituraMovimentoSim").removeAttr('checked');
	$("#situacaoLeituraMovimentoNao").removeAttr('checked');
	$("#indicadorCondominioImovelTexto1").removeAttr('checked');
	$("#indicadorCondominioImovelTexto2").removeAttr('checked');
}

function pesquisarDadosMedicao(){
	exibirIndicador();
	submeter('dadosMedicaoForm', 'exibirDetalhamentoDadosMedicao');
}

animatedcollapse.addDiv('mostrarFormLeituraMovimento', 'fade=0,speed=400,group=rota,persist=1,hide=0');

</script>
<div class="card">
	<div class="card-header">
		<h5 class="card-title">Inserir Dados de Medi��o<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h5>
	</div>
	<div class="card-body">
		<div class="alert alert-primary fade show" role="alert">
			<i class="fa fa-question-circle"></i>
			Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Salvar</span>
		</div>
		
		<form id="dadosMedicaoForm" name="dadosMedicaoForm" method="post" modelAttribute="DadosMedicaoVO">
			<fieldset>
				<input name="chaveRota" type="hidden" id="chaveRota" value="${leituraMovimento.rota.chavePrimaria}">
				<input name="anoMesFaturamento" type="hidden" id="anoMesFaturamento" value="${leituraMovimento.anoMesFaturamento}"> 	
				<input name="chaveLeiturista" type="hidden" id="chaveLeiturista" value="${chaveLeiturista}">
				<input name="idRota" type="hidden" id="idRota" value="${idRota}">
				<input name="anoMesFaturamento" type="hidden" id="anoMesFaturamento" value="${anoMesFaturamento}">
				<input type="hidden" id="situacaoLeituraMovimento" name="situacaoLeituraMovimento">
				
				<div class="container-fluid">
					<div class="row">
						<div class="col">
							<div class="card">
								<div class="card-header">
									<h6 >Informa��es da Rota</h6>
								</div>
								<div class="card-body">
									<div class="container-fluid">
										<div class="row">
											<label class="rotulo">Ref�rencia Ciclo:</label>
											<span class="itemDetalhamento "><c:out value="${leituraMovimento.anoMesFaturamento}-${leituraMovimento.ciclo}"/></span>
										</div>
										<div class="row">
											<label class="rotulo">Grupo de Faturamento:</label>
											<span class="itemDetalhamento "><c:out value="${leituraMovimento.grupoFaturamento.descricao}"/></span>
										</div>
										<div class="row">
											<label class="rotulo">Rota:</label>
											<span class="itemDetalhamento "><c:out value="${leituraMovimento.rota.numeroRota}"/></span>
										</div>
										<div class="row">
											<label class="rotulo">Setor Comercial:</label>
											<span class="itemDetalhamento "><c:out value="${leituraMovimento.rota.setorComercial.descricao}"/></span>
										</div>
										<div class="row">
											<label class="rotulo">Meio de Leitura:</label>
											<span class="itemDetalhamento "><c:out value="${leituraMovimento.tipoLeitura.descricao}"/></span>
										</div>
										<div class="row">
											<label class="rotulo">Data Prevista de Leitura:</label>
											<span class="itemDetalhamento "><fmt:formatDate value="${leituraMovimento.dataProximaLeituraPrevista}" pattern="dd/MM/yyyy"/></span>
										</div>
										<div class="row">
											<label class="rotulo">Empresa:</label>
											<span class="itemDetalhamento "><c:out value="${leituraMovimento.rota.empresa.cliente.nome}"/></span>
										</div>
										<div class="row">
											<label class="rotulo">Leiturista:</label>
											<select id="leiturista" name="leiturista" class="form-control select2-control">
												<option value="-1">Selecione</option>
												<c:forEach items="${listaLeiturista}" var="leiturista">
													<option value="<c:out value='${leiturista.chavePrimaria}'/>" <c:if test="${leituraMovimento.leiturista.chavePrimaria == leiturista.chavePrimaria || chaveLeiturista == leiturista.chavePrimaria}">selected="selected"</c:if>>
														<c:out value="${leiturista.funcionario.nome}"/>
													</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="card">
								<div class="card-header">
									<h6>Pesquisar Im�vel</h6>
								</div>
								<div class="card-body">
									<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
									<input name="idImovel" type="hidden" id="idImovel" value="${idImovel}">
									<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${nomeFantasiaImovel}">
									<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${matriculaImovel}">
									<input name="numeroImovel" type="hidden" id="numeroImovel" value="${numeroImovel}">
									<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${cidadeImovel}">
									<input name="condominio" type="hidden" id="condominio" value="${condominio}">
							
							
									<a name="Button" id="botaoPesquisarImovel" class="btn btn-primary btn-sm"
										onclick="exibirPopupPesquisaImovel();" role="button" href="#">
										<i class="fa fa-search"></i>
										Pesquisar Imovel
									</a>
							
									<div class="container-fluid">
										<div class="form-group">
											<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
											<input class="form-control campoDesabilitado" style="width: 100%;" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${nomeFantasiaImovel}" />
										</div>
										<div class="form-group">
											<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
											<input class="form-control campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${matriculaImovel}" />
										</div>
											<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
											<input class="form-control campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${numeroImovel}"/>
										<div class="form-group">
											<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
											<input class="form-control campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${cidadeImovel}"/>
										</div>
										<div class="form-group">
											<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto">O im�vel � condom�nio:
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${condominio == 'true'}">checked="checked"</c:if>/>
													<label class="form-check-label" for="indicadorImovelTexto1">Sim</label>
												</div>
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${condominio == 'false'}">checked="checked"</c:if>/>
													<label class="form-check-label" for="indicadorImovelTexto2">N�o</label>
												</div>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="card">
								<div class="card-header">
									<h6>Filtros Leitura</h6>
								</div>
								<div class="card-body">
									<fieldset class="form-group border p-2" style="padding-top: 25px;">
										<div class="form-group">
											<label class="rotulo">Data Leitura:</label>
											<input class="form-control bootstrapDP" type="text" id="dataLeituraFiltro" name="dataLeituraFiltro" maxlength="10" value="${dataLeituraFiltro}">
										</div>
										<div class="form-group">
											<label class="rotulo">N� S�rie do Medidor:</label>
											<input class="form-control" type="text" id="numeroSerieMedidor" name="numeroSerieMedidor" size="9" value="${numeroSerieMedidor}" /><br />
										</div>
										<div class="form-group">
											<label class="rotulo">Processados: 
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" name="processado" id="situacaoLeituraMovimentoSim" value="processado" <c:if test="${processado eq 'processado'}">checked</c:if>>
													<label class="form-check-label">Sim</label>
												</div>
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" name="processado" id="situacaoLeituraMovimentoNao" value="naoProcessado"  <c:if test="${processado eq 'naoProcessado'}">checked</c:if>>
													<label class="form-check-label">N�o</label>
												</div>
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" name="processado" id="situacaoLeituraMovimentoTodos" value=""  <c:if test="${processado eq '' || situacaoLeituraMovimento == null}">checked</c:if>>
													<label class="form-check-label">Todos</label>
												</div>
											</label>
										</div>
										
										<div class="form-group" >
											<label class="rotulo rotulo2Linhas">Dados de Leitura: 
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" name="indicadorValorLeitura" id="indicadorValorLeitura" value="true" <c:if test="${indicadorValorLeitura eq 'true'}">checked</c:if>>
													<label class="form-check-label">Sem Leitura</label>
												</div>
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" name="indicadorValorLeitura" id="indicadorValorLeitura" value="false"  <c:if test="${indicadorValorLeitura eq 'false'}">checked</c:if>>
													<label class="form-check-label">Com Leitura Zero</label>
												</div>
												<div class="form-check form-check-inline">
													<input class="form-check-input" type="radio" name="indicadorValorLeitura" id="indicadorValorLeitura" value=""  <c:if test="${indicadorValorLeitura eq ''}">checked</c:if>>
													<label class="form-check-label">Todos</label>
												</div>
											</label>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDir">
					<button name="Button" class="btn btn-primary" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarDadosMedicao()">
						<i class="fa fa-search"></i>
						Pesquisar
					</button>
					<button name="Button" class="btn btn-secondary" value="Limpar" type="button" onclick="limparFormularioFiltro();">
						<i class="far fa-trash-alt"></i>
						Limpar
					</button>		
				</fieldset>
				<br />
				<c:if test="${processado eq 'naoProcessado'}">
					<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>Data da Leitura � de preenchimento obrigat�rio.</p>
				</c:if>
			</fieldset>	
		
		
			<c:if test="${listaLeituraMovimento ne null}">
			<br>
				<img id="imagem" style="padding-left: 20px;" src="<c:url value="/imagens/circle_green.png"/>" border="0">
				<span>Ativo</span>
				<img id="imagem" style="padding-left: 20px;" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
				<span>Aguardando Ativa��o</span>
				<img id="imagem" style="padding-left: 20px;" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				<span>Bloqueado</span>
				<hr class="linhaSeparadoraPesquisa" />
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="leituraMovimento">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">Ponto de Consumo</th>
								<th scope="col" class="text-center">Situa��o</th>
								<th scope="col" class="text-center">S�rie do Medidor</th>
								<th scope="col" class="text-center">Dt. Leitura Anterior</th>
								<th scope="col" class="text-center">Leitura Anterior</th>
								<th scope="col" class="text-center">Data da Leitura</th>
								<th scope="col" class="text-center">Leitura</th>
								<th scope="col" class="text-center">Press�o</th>
								<th scope="col" class="text-center">Temperatura</th>
								<th scope="col" class="text-center">Anormalidade de Leitura</th>
								<th scope="col" class="text-center">Foto</th>
								<th scope="col" class="text-center">Observa��o</th>
								<th scope="col" class="text-center"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaLeituraMovimento}" var="leituraMovimento">
								<tr id="leituraMovimento_${leituraMovimento.chavePrimaria}">
									<td class="text-center">
										<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${leituraMovimento.chavePrimaria}">
										<a title="PONTO DE CONSUMO: ${leituraMovimento.pontoConsumo.descricao}&#13;CLIENTE: ${leituraMovimento.nomeCliente}&#13;IM�VEL/PONTO DE CONSUMO: ${leituraMovimento.pontoConsumo.enderecoFormatado}&#13;OBSERVA��O: ${leituraMovimento.observacaoPontoConsumo} ">
										<c:out value="${leituraMovimento.sequencialLeitura}"/> -
										<c:out value="${leituraMovimento.pontoConsumo.imovel.nome}"/> /
										<c:out value="${leituraMovimento.pontoConsumo.descricao}"/>
									</td>
									<td class="text-center">
										<c:if test='${leituraMovimento.pontoConsumo.situacaoConsumo.descricao eq "Ativo"}'>
											<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
										</c:if>
										<c:if test='${leituraMovimento.pontoConsumo.situacaoConsumo.descricao eq "Aguardando Ativa��o"}'>
											<img id="imagem" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
										</c:if>
										<c:if test='${leituraMovimento.pontoConsumo.situacaoConsumo.descricao eq "Bloqueado"}'>
											<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
										</c:if>
										<c:if test='${leituraMovimento.pontoConsumo.situacaoConsumo.descricao eq "Suspenso"}'>
											<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
										</c:if>
										<c:if test='${leituraMovimento.pontoConsumo.situacaoConsumo.descricao eq "Bloqueado Tecnicamente"}'>
											<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
										</c:if>
									</td>
									<td class="text-right">
										<a title="S�RIE DO MEDIDOR: ${leituraMovimento.numeroSerieMedidor}&#13;LOCAL DE INSTALA��O: ${leituraMovimento.localInstalacaoMedidor}&#13;">
											<c:out value="${leituraMovimento.numeroSerieMedidor}"/>
										</a>
										<input type="hidden" id="numeroSerieMedidor__${leituraMovimento.chavePrimaria}" name="numeroSerieMedidor__${leituraMovimento.chavePrimaria}" value="${leituraMovimento.numeroSerieMedidor}">
									</td>
									<td class="text-center">
										<fmt:formatDate value="${leituraMovimento.dataLeituraAnterior}" pattern="dd/MM/yy"/>
									</td>
									<td class="text-right">
										<fmt:formatNumber value="${leituraMovimento.leituraAnterior}"/>
									</td>
									<td class="text-center">
										<c:choose>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria eq '4'}">
												<fmt:formatDate value="${leituraMovimento.dataLeitura}" pattern="dd/MM/yy"/>
											</c:when>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria ne '4'}">
												<input style="width: 120px" class="form-control bootstrapDP" type="text" id="dataLeitura_${leituraMovimento.chavePrimaria}" name="dataLeitura_${leituraMovimento.chavePrimaria}" maxlength="10" size="6" value="<fmt:formatDate value="${leituraMovimento.dataLeitura}" pattern="dd/MM/yyyy"/>" >
											</c:when>
										</c:choose>
										<div class="invalid-feedback" id="erro_dataLeitura_${leituraMovimento.chavePrimaria}"></div>
										<input name="dataLeituraAnterior" type="hidden" id="dataLeituraAnterior_${leituraMovimento.chavePrimaria}" value="<fmt:formatDate value="${leituraMovimento.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>" >
									</td>
									<td class="text-right">
										<c:choose>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria eq '4'}">
												<fmt:formatNumber value="${leituraMovimento.valorLeitura}"/>
											</c:when>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria ne '4'}">
												<input style="width: 120px" class="form-control valorLeitura" onPaste="return false;" type="text" size="12" id="valorLeitura_${leituraMovimento.chavePrimaria}" name="valorLeitura_${leituraMovimento.chavePrimaria}" value="<fmt:formatNumber value="${leituraMovimento.valorLeitura}"/>" >
												<div class="invalid-feedback" id="erro_valorLeitura_${leituraMovimento.chavePrimaria}"></div>
											</c:when>
										</c:choose>
									</td>
									<td class="text-right">
										<c:choose>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria eq '4'}">
													<c:out value="${leituraMovimento.pressaoInformada}"/>
											</c:when>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria ne '4'}">
												<input style="width: 100px" class="form-control" onPaste="return false;" type="text" size="5" id="valorPressao_${leituraMovimento.chavePrimaria}" name="valorPressao_${leituraMovimento.chavePrimaria}"
													onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,4);"
													onkeypress="return formatarCampoDecimal(event,this,4,4);"
													value="<fmt:formatNumber value='${leituraMovimento.pressaoInformada}' type='number' minFractionDigits='4' maxFractionDigits='4' />">
											</c:when>
										</c:choose>
									</td>
									<td class="text-right">
									<c:choose>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria eq '4'}">
													<c:out value="${leituraMovimento.temperaturaInformada}"/>
											</c:when>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria ne '4'}">
												<input style="width: 100px" class="form-control" onPaste="return false;" type="text" size="5" id="valorTemperatura_${leituraMovimento.chavePrimaria}" name="valorTemperatura_${leituraMovimento.chavePrimaria}"
													onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,4);"
													onkeypress="return formatarCampoDecimal(event,this,4,4);"
													value="<fmt:formatNumber value='${leituraMovimento.temperaturaInformada}' type='number' minFractionDigits='4' maxFractionDigits='4'/>">
											</c:when>
										</c:choose>
									</td>
									<td>
										<c:choose>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria eq '4'}">
												<c:forEach items="${listaAnormalidadeLeitura}" var="anormalidadeLeitura">
													 <c:if test="${leituraMovimento.codigoAnormalidadeLeitura == anormalidadeLeitura.chavePrimaria}">
																<c:out value="${anormalidadeLeitura.descricao}"/>
													 </c:if>
												</c:forEach>
											</c:when>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria ne '4'}">
												<select name="anormalidadeLeitura_${leituraMovimento.chavePrimaria}" id="anormalidadeLeitura_${leituraMovimento.chavePrimaria}" class="form-control select2-control"
													onchange="verificaLeituraPorAnormalidadeSelecionada(this.value, ${leituraMovimento.leituraAnterior}, ${leituraMovimento.consumoAnterior}, ${leituraMovimento.consumoMedio})">
													<option value="-1">Selecione</option>
														<c:forEach items="${listaAnormalidadeLeitura}" var="anormalidadeLeitura">
															<option value="<c:out value="${anormalidadeLeitura.chavePrimaria}"/>" <c:if test="${leituraMovimento.codigoAnormalidadeLeitura == anormalidadeLeitura.chavePrimaria}">selected="selected"</c:if>>
																<c:out value="${anormalidadeLeitura.descricao}"/>
															</option>
														</c:forEach>
													</select>
											</c:when>
										</c:choose>
									</td>
									
									<td class="text-center">
										<c:if test='${leituraMovimento.fotoColetor ne null}'>
											<a id="fotoColetor" rel="modal" onclick="selecionarFoto(${idRota}, ${anoMesFaturamento}, ${leituraMovimento.chavePrimaria})">
												<img id="fotoColetor" src="<c:url value="/imagens/images.png"/>" border="0" style="width:40px; height:40px;" >
											</a>
											<div class="window" id="fotoColetorPopup"  title="Foto Coletor" style="display :none;">
											</div>
										</c:if>
									</td>
									
									<td class="text-center">
										<c:choose>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria eq '4'}">
												<c:out value="${leituraMovimento.observacaoLeitura}"/>
											</c:when>
											<c:when test="${leituraMovimento.situacaoLeitura.chavePrimaria ne '4'}">
												<!-- <textarea class="form-control" 
														  name="observacoesCadastrais" 
														  id="observacoesCadastrais" cols="25" rows="8"
														  maxlength="100" onkeyup="letraMaiuscula(this);" 
														  onblur="this.value = removerEspacoInicialFinal(this.value);"
														  onkeypress="return formatarCampoTextoLivreComLimite(event,this,800);"
														  onpaste="return formatarCampoTextoLivreComLimite(event,this,800);">
													
											    	${leituraMovimento.observacoesCadastrais}
												</textarea> -->
												<input type="text"
														class="form-control"
														maxlength="100"
														name="observacoesCadastrais_${leituraMovimento.chavePrimaria}"
														id="observacoesCadastrais_${leituraMovimento.chavePrimaria}"
														aria-describedby="helpId"
														placeholder=""
														onkeyup="letraMaiuscula(this);"
														onblur="this.value = removerEspacoInicialFinal(this.value);"
														onkeypress="return formatarCampoTextoLivreComLimite(event,this,800);"
														onpaste="return formatarCampoTextoLivreComLimite(event,this,800);"
														value="${leituraMovimento.observacoesCadastrais}"/>
											</c:when>
										</c:choose>
									</td>
									<td class="text-center h5">
										<c:if  test="${leituraMovimento.situacaoLeitura.chavePrimaria ne '4'}">
											<span role="button" id="salvar_${leituraMovimento.chavePrimaria}" class="btn btn-outline-primary" onclick="return alterarDadosMedicao(${leituraMovimento.chavePrimaria});" ><i class="fa fa-check-square fa-3"></i></span>
											<span id="wait_${leituraMovimento.chavePrimaria}" class="hide"> <i class="fa fa-spinner fa-spin" aria-hidden="true"></i></span>
											<div class="alert alert-danger hide" role="alert" id="erro_leituraMovimento_${leituraMovimento.chavePrimaria}"></div>
										</c:if>
									</td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:if>
		</form>
		
		
			<fieldset class="conteinerBotoes"> 
				<button name="Button" class="btn btn-primary" type="button" onClick="cancelar();">Voltar</button>
				<%-- <button name="Button" class="btn btn-primary" type="button" onclick="limparFormulario();">Limpar</button>
				
				<vacess:vacess param="alterarDadosMedicao">
					<button id="botaoSalvar" class="btn btn-primary" type="button" onclick="salvarAlteracao();">Salvar</button>
				</vacess:vacess> --%>
			 </fieldset>
			 
	</div>
</div>

