<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<h1 class="tituloInterno">Manter Dados de Compra de G�s - City Gate</h1>
<p class="orientacaoInicial">Para manter os Dados de Compra desde o �ltimo dia que foi preenchido, selecione o City Gate e clique em <span class="destaqueOrientacaoInicial">Exibir</span> para gerar a listagem.<br />Para consultar meses e anos anteriores selecione as op��es correspondentes e clique em <span class="destaqueOrientacaoInicial">Exibir</span>.<br />Preencha os dados do(s) dia(s) na listagem e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>


<script language="javascript">
		
		
		
</script>

<form>

	<input type="hidden" name="acao" id="acao" value="atualizarRamal">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${ramalForm.map.chavePrimaria}">
	<input name="versao" type="hidden" id="versao" value="${ramalForm.map.versao}">

	<fieldset id="dadosCompraCityGate" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo campoObrigatorio" for="cityGate"><span class="campoObrigatorioSimbolo">* </span>City Gate:</label>
		<select name="cityGate" id="cityGate" class="campoSelect campoHorizontal">
    		<option value="-1">Selecione</option>				
	    </select>
		
		<label class="rotulo rotuloHorizontal" for="mes">M�s:</label>
		<select name="mes" id="mes" class="campoSelect campoHorizontal">
    		<option value="-1">Selecione</option>
	    </select>
	    
	    <label class="rotulo rotuloHorizontal" for="ano">Ano:</label>
		<select name="ano" id="ano" class="campoSelect campoHorizontal">
    		<option value="-1">Selecione</option>
	    </select>
	    
	    <input name="Button" class="bottonRightCol2" value="Exibir" type="submit">

	</fieldset>
	
</form> 