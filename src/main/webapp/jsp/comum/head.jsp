<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" />

<!-- A tag <meta> abaixo for�a o IE8 a n�o rendereizar como o IE7 (Standards Mode)  -->
<meta http-equiv="X-UA-Compatible" content="IE=8" />

<title>GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s <fmt:bundle basename="constantes">Build: [<fmt:message key="VERSAO_BUILD" />] [<fmt:message key="VERSAO_DATA" />]</fmt:bundle></title>
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/reset.css"/>" />
<!-- <link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/cabecalho.css"/>" /> -->
<!-- <link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/menubarra.css"/>" /> -->
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/estiloscss.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/dropdown.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/jquery.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/jquery-ui-1.10.3.min.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/jquery.datepick.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/displaytag.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/chromatable.css"/>" />

<!-- GGAS 2.0 - NOVOS ESTILOS -->
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/style_ie.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/style_menu.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/style_notifications.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/style.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/BreadCrumb.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/jquery.mCustomScrollbar.css"/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/jquery-ui-timepicker-addon.css"/>" />

<!-- GGAS 2.0 - NOVOS ESTILOS -->

<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/imagens/favicon.ico"/>" />

<!--[if lte IE 6]>
	<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/dropdown_ie.css"/>" />
	<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/estiloscss_ie6.css"/>" />
<![endif]-->

<!--[if lte IE 7]>
	<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/estiloscss_ie7.css"/>" />
<![endif]-->

<!--[if IE 6]>
	<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/estiloscss_ie6only.css"/>" />
<![endif]-->

<!--[if IE 8]>
	<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/estiloscss_ie8.css"/>" />
<![endif]-->

<c:set var="version" value="1" scope="application"/>
<%--<c:set var="ctxWebpack" value="${ctx}" scope="application" />--%>
<%--<c:if test="${environment.isDevelopment()}">--%>
	<%--<c:set var="ctxWebpack" value="http://localhost:9000" scope="application"/>--%>
<%--</c:if>--%>
<c:set var="ctxWebpack" value="http://192.168.0.101:9000" scope="application"/>

<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/interface/AjaxService.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-1.8.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-ui-1.10.3.custom.min.js"/>"></script>
<%--<script type="text/javascript" src="<c:url value="/js/jquery-ui-1.10.3.min.js"/>"></script>--%>
<script type="text/javascript" src="<c:url value="/js/ui.datepicker-pt-BR.js"/>"></script>
<%-- <script type="text/javascript" src="<c:url value="/js/masks.js"/>"></script> --%>

<script type="text/javascript" src="<c:url value="/js/jquery.metadata.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.inputmask.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.price_format.1.3.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/autoNumeric-1.7-5.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.chromatable.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.hoverIntent.minified.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.scrollTo-min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/highcharts.js?v5.0.7"/>"></script>
<script type="text/javascript" src="<c:url value="/js/modules/exporting.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.jScale.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.activity-indicator-1.0.0.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.filestyle.mini.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/selectbox.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-ui-timepicker-addon.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-ui-timepicker-addon-pt-BR.js"/>"></script>

<!-- GGAS 2.0 NOVOS JAVASCRIPTS -->
<script type="text/javascript" src="<c:url value="/js/ctmin.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.easing.1.3.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.jBreadCrumb.1.1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.mCustomScrollbar.concat.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.fastLiveFilter.js"/>"></script>
<!-- GGAS 2.0 NOVOS JAVASCRIPTS -->

<script type="text/javascript" src="<c:url value="/js/utils.js"/>" charset="UTF-8"></script>

<script type="text/javascript" src="<c:url value="/js/animatedcollapse.js"/>">
	//Animated Collapsible DIV v2.4- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
	//This notice MUST stay intact for legal use
	//Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
</script>

<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
<script type="text/javascript" src="js/jquery.mask.min.js"></script>

<script>



	//-- Bloco JQuery --//
	$(document).ready(function(){

		dwr.engine._errorHandler = function(){};

		dwr.engine._warningHandler = function(){};


		$('body').on('blur change', 'input', function(){
	        $(this).val(removerEspacoInicialFinal($(this).val()));
		});

		$("form").submit(function(){
			exibirIndicador();
		});

		/*Exibe o Indicador de processamento e esconde o conte�do enquanto
		as telas n�o est�o completamente carregadas*/
		$(".menuDeslizanteConteiner a, a#pagina_inicial").click(function(event){
			if (event.ctrlKey == false){
				$("#conteudo").hide();
				exibirIndicador();
			}
		});

		//Exibe o conte�do apenas ap�s o carregamento completo da p�gina
		$("#conteudo").show();


		/*Impede o uso do bot�o direito do mouse nos campos input de texto,
		exceto na tela de Medi��o > Dados de Compra de G�s > City Gate */
		$("input[type=text]").each(function(){
			if(!$("cityGateMedicaoForm")){
				$(this).bind("contextmenu", function(){return false;});
			}
		});

		// estiliza o <input type="file"...
		$("input.campoFile").filestyle({
			image: "<c:out value='${pageContext.request.contextPath}'/>/imagens/botaoProcurar.gif",
			imageheight: 35,
			imagewidth: 98,
			width: 191
		});
		// limpa o campo <input type="file"...
		$("input#limparArquivo, #limparForm").click(function() {
			$("input.file").attr({value: "" });
		});
		// Altera a imagem de fundo do bot�o "Procurar"
		$("div#containerInputFile").mouseover(
			function(){
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/botaoProcurarOver.gif) 0 0 no-repeat','background-position':'right'});
			}
		).mouseout(
			function(){
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/botaoProcurar.gif) 0 0 no-repeat','background-position':'right'});
			}
		);

		$(".MultiFile-wrap input.campoFileMulti").each(function(){
			$(".MultiFile-list").before('<div class="MultiFile-listHeader"><span class="buttonMultiFile">Adicionar</span></div>');
			var multiFileListPosition = $(".MultiFile-listHeader").position();
			$(this, + ".buttonMultiFile").css({top: multiFileListPosition.top});
		}).hover(
			function(){
				$(this).next(".MultiFile-listHeader").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/fundoTbody_hover.gif) repeat-x'});
			},
			function(){
				$(this).next(".MultiFile-listHeader").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/fundoTbody.gif) repeat-x'});
			}
		);

		//Efeito mouseover nos cabe�alhos de tabela com links
		$(".dataTableGGAS thead tr th").hover(
			function () {
				if($(this).children().is("a")){
					$("a", this).css("text-decoration","underline");
					if($(this).parents("table").is(".dataTableCabecalho2Linhas")){
						$(this).css({"background":"url('imagens/fundoTbody2Linhas_hover.gif') repeat-x","cursor":"pointer"});
					} else {
						$(this).css({"background":"url('imagens/fundoTbody_hover.gif') repeat-x"});
					}

				}
			},
			function () {
				if($(this).children().is("a")){
					$("a", this).css("text-decoration","none");
					if($(this).parents("table").is(".dataTableCabecalho2Linhas")){
						$(this).css({"background":"url('imagens/fundoTbody2Linhas.gif') repeat-x","cursor":"pointer"});
					} else {
						$(this).css("background","url('imagens/fundoTbody.gif') repeat-x");
					}
				}
			}
		);

		/*Permite reordenar a coluna da tabela clicando em qualquer �rea da
		c�lula do cabe�alho, mesmo fora do link.*/
		$(".dataTableGGAS thead tr th").click(function(){
			var href = $("a", this).attr("href");
			if(href) {
				window.location = href;
			}
		});

		//Efeito mouseover nas tabelas de resultados de pesquisa (displaytag)
	   	$("tr.odd,tr.even").hover(
			function() {
				if($(this).has('span.linkInvisivel').length > 0){
					$(this).addClass("selectedRow");
				}
			},
			function() {
				if($(this).has('span.linkInvisivel').length > 0){
					$(this).removeClass("selectedRow");
				}
			}
		);

	   	//Verificar se existe um elemento tabs
		if ( $("#tabs").length > 0 ) {
			var $tabs = $('#tabs').tabs();

			$("#tabs").attr({style: "display: block" });

			$("#tabs").bind('tabsselect', function(event, ui) {
 				manterAbaSelecionada(this.form,ui.index);
			});

			<c:if test="${not empty abaId}">
			$tabs.tabs('option', 'active', ${abaId});
			</c:if>
		}

		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() {$(this).addClass('ui-state-hover');},
			function() {$(this).removeClass('ui-state-hover');}
		);


		//Selecionar/Desselecionar todos os checkboxes
		if ($("._thead input[name='checkAllAuto'],#checkAllAuto").length > 0){
			$("._thead input[name='checkAllAuto'],#checkAllAuto").click(function(){
				//O seletor de filtro :enabled marca apenas os checkboxes habilitados.
				$("tbody input[type='checkbox']:enabled").attr("checked", $("._thead input[name='checkAllAuto'],#checkAllAuto").is(":checked"));
			});
		}

		//Mascaramento do campo da data
		try {
            $(".campoData").inputmask("99/99/9999", {placeholder: "_", greedy: false});
            $(".campoData2").inputmask("99/99/9999", {placeholder: "_", greedy: false});
        } catch (e) {
		    // Evitar travamento, e conseguir seguir o fluxo...
		}

		//IN�CIO: Testes de script para campos desabilitados
		/*$(":input:disabled").each(function(){
			$(this).addClass("campoDesabilitado");
			$(this).change(function(){
				$(":enabled").removeClass("campoDesabilitado");
				$("disabled").addClass("campoDesabilitado");
			});

			//$("input.campoValorReal").removeClass("campoValorReal").addClass("campoValorRealDesabilitado");
		});

		$("select").each(function(){
		$(this).change(function(){
			$("select:enabled,input[type=text]:enabled,textarea:enabled").removeClass("campoDesabilitado");
			$("select:disabled,input[type=text]:disabled,textarea:disabled").addClass("campoDesabilitado");
		});

		/*
		$("select:disabled,input[type=text]:disabled,textarea:disabled").addClass("campoDesabilitado");
		$("select").change(function(){
			$("select:enabled,input[type=text]:enabled,textarea:enabled").removeClass("campoDesabilitado");
			$("select:disabled,input[type=text]:disabled,textarea:disabled").addClass("campoDesabilitado");
		});
		$(":input").blur(function(){
			$("select:enabled,input[type=text]:enabled,textarea:enabled").removeClass("campoDesabilitado");
			$("select:disabled,input[type=text]:disabled,textarea:disabled").addClass("campoDesabilitado");
		});

		var campoValorReal = $("input.campoValorReal").attr("disabled");
		if ($("input.campoValorReal").attr("disabled")=="disabled") {
			$(this).removeClass("campoValorReal");
			$(this).addClass("campoValorRealDesabilitado");
		}
		*/
		//FIM: Testes de script para campos desabilitados



    	//Popup de confirma��o de logout
    	$("#confirmacaoLogout").dialog({
        	autoOpen: false,
            modal: true,
	        buttons: {

		        "Sim" : function() {
	    			location.href = '<c:url value="/efetuarLogoff"/>';
		         },
		        "N�o" : function() {
		        	$(this).dialog("close");
		        }
		    },
			draggable: false,
			resizable: false
		});

    	//Popup informativo sobre timeout de sess�o
    	$("#avisoTimeoutSessao").dialog({
        	autoOpen: false,
            modal: true,
	        buttons: {
		        "OK" : function() {
		    		$.post('<c:url value="/index"/>');
			       	timeOutSegundos = '${pageContext.request.session.maxInactiveInterval}';
    				$(this).dialog("close");
				}
		    },
			draggable: false,
			resizable: false
		});

    	//Popup para altera��o de senha do usu�rio
    	$("#alterarSenhaPopup").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Alterar" : function() {
					//var form = $('#alterarSenhaPopup form:first-child');
    				submeter("alterarSenhaForm", "alterarSenha");
				},
				"Cancelar" : function() {
					$(this).dialog("close");
				}
			},
			draggable: false,
			resizable: false
		});

    	//Popup informativo sobre o sistema
    	$("#infoSistemaPopup").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Fechar" : function() {
					$(this).dialog("close");
				}
			},
			draggable: false,
			resizable: false
		});

		//Janela popup da ajuda contextual do sistema
    	$("#janelaHelp").dialog({
			autoOpen: false,
			width: 800,
			height: 500,
			dialogClass: 'janelaHelpDialog'
		});

    	//Popup informativo sobre pend�ncias
    	$("#pendenciasPopup").dialog({
        	autoOpen: false,
            modal: true,
	        buttons: {
		        "OK" : function() {
		    	   	$(this).dialog("close");
				}
		    },
			draggable: false,
			resizable: false
		});

    	//Adiciona as setas de indica��o de ordenamento da pesquisa
		$(".dataTableGGAS th.order1 a").append(" <img src='<c:url value="/imagens/setaCima.png"/>' />");
	   	$(".dataTableGGAS th.order2 a").append(" <img src='<c:url value="/imagens/setaBaixo.png"/>' />");


	   	/*----------------------------------*/
	   	/*-- Scripts de navega��o de tela --*/
	   	/*----------------------------------*/
	   	//Atribui o foco ao primeiro link ou campo do formul�rio vis�vel
		$("form fieldset :input:visible:enabled, form a.linkExibirDetalhes, form a.linkPesquisaAvancada, form a.linkConferirCEPConteiner")
	  	//Exclui os objetos que n�o far�o parte da sele��o
		.not("#pesquisarCliente :text, #pesquisarCliente textarea, #pesquisarImovel :text, #pesquisarImovel .pesquisarImovelFundo :radio, input[readonly=readonly], :file, #formContratoPontoConsumo fieldset#tabs *, #conteinerDetalharContrato *")
		//Seleciona apenas o primeiro objeto da pilha, coloca o foco neste campo e itera sobre eles
		.first().focus().each(function(){
			//Verifica se � um bot�o e atribui uma borda maior que a dos campos
			if($(this).is(".bottonRightCol2,.bottonRightCol")){
				$(this).css({"border":"2px solid #027DDA"});
			}
			//Verifica se � um radio ou checkbox e atribui um outline ao inv�s de borda
			else if($(this).is(":radio,:checkbox")){
				if($.browser.msie && $.browser.version.substr(0,1)<=7){
					$(this).css({"border":"1px solid #027DDA"});
				} else {$(this).css({"outline":"1px solid #027DDA"});}
			}
			//Verifica se � um link e atribui uma borda
			else if($(this).is("a")){
				$(this).css({"border":"1px solid #027DDA"});
			}
			//Todos os outros elementos
			else {$(this).css({"border":"1px solid #027DDA","background-color":"#E3F3FF"});}
	   	});

		//IN�CIO: Atributo tabindex e destaque dos campos
	   	//Seleciona os objetos afetados
        $("form a.linkExibirDetalhes, form a.linkPesquisaAvancada, form a.linkConferirCEPConteiner, ul.ui-tabs-nav li a, .ui-tabs-panel :input:enabled, form :text:visible, form :submit:visible, form :button:visible, form :radio:visible, form :password:visible, form :checkbox:visible, form select:visible, form textarea:visible, .bootstrap form input:enabled")
		//Exclui os objetos que n�o far�o parte da sele��o
	   	.not("#pesquisarCliente :text, #pesquisarCliente textarea, #pesquisarImovel :text, #pesquisarImovel .pesquisarImovelFundo :radio, input[readonly=readonly], #formContratoPontoConsumo fieldset#tabs *, #conteinerDetalharContrato *")
	   	//Itera sobre os objetos restantes
	   	.each(function(i){
			//Adiciona a propriedade tabindex a todos os elementos vis�veis do formul�rio
	   		$(this).attr("tabindex",i+1);

			//Seleciona apenas os links <a> e exclui o restante dos objetos.
			$(this).filter("a").focusin(function(){
				$(this).css({"border":"1px solid #027DDA"});
			}).focusout(function(){
				$(this).css({"border":"none"});
			}).end();//Retorna ao conjunto completo de objetos.

			/*Exclui apenas os links <a> de todo o conjunto de objetos HTML*/
	   		$(this).not("a").not('.form-control').not('.btn').focusin(function(){//Evento disparado ao receber o foco
	   			//Verifica se � um bot�o e atribui uma borda maior que a dos campos
				if($(this).is(".bottonRightCol2,.bottonRightCol")){
					$(this).css({"border":"2px solid #027DDA"});
				}
				//Verifica se � um radio ou checkbox e atribui um outline ao inv�s de borda
				else if($(this).is(":radio,:checkbox")){
					if($.browser.msie && $.browser.version.substr(0,1)<=7){
						$(this).css({"border":"1px solid #027DDA"});
					} else {$(this).css({"outline":"1px solid #027DDA"});}
				}
				else {
					$(this).css({"border":"1px solid #027DDA","background-color":"#E3F3FF"});
				}
	   		}).focusout(function(){//Evento disparado ao perder o foco
	   			//Seleciona apenas os campos input (exclui checkbox e radio) e textarea
	   			$(this).filter(":text, :button, :submit, textarea, :password").not('.form-control').each(function(){
		   			//Verifica se o label do campo tem a classe "campoObrigatorio" e o valor do campo � vazio ou tem m�scara de data n�o preenchida
	   				if($(this).prev("label").hasClass("campoObrigatorio") && $(this).val() == "" || $(this).val() == "__/__/____"){
	   					$(this).css({"border":"1px solid #FF8F8F","background-color":"#FFEFEF"});
					} //Verifica se o elemento � um bot�o
					else if($(this).is(".bottonRightCol2,.bottonRightCol")){
		   				$(this).css({"border":"2px outset #ECE9D8"});
		   			} //Verifica se o elemento est� desabilitado
		   			else if($(this).attr("disabled") != undefined && $(this).attr("disabled") == true) {//Verifica se o campo possui o atributo disabled
						$(this).css({"background-color":"#EBEBE4","border":"1px solid #7F9DB9"});
					} else { //Define os estilos para todos os outros campos
			   			$(this).css({"background-color":"#FFF","border":"1px solid #7F9DB9"});
			   		}
			   	}).end()//Retorna ao conjunto anterior com todos os objetos.

			  	//Seleciona apenas os campos select, radio e checkbox
			   	.filter("select,:radio,:checkbox").each(function(){
			   		//Verifica se o label do campo tem a classe "campoObrigatorio" e se o seu valor � igual a -1
					if($(this).prev("label").hasClass("campoObrigatorio") == true && $(this).val() == "-1"){
						$(this).css({"border":"1px solid #FF8F8F","background-color":"#FFEFEF"});
					}
					//Verifica se � um radio ou checkbox e atribui um outline ao inv�s de borda
					else if($(this).is(":radio,:checkbox")){
						//Verifica se o browser � IE 7 ou menor
						if($.browser.msie && $.browser.version.substr(0,1)<=7){
							$(this).css({"border":"none"});
						} else {$(this).css({"outline":"none"});}
					} else {$(this).css({"border":"1px solid #7F9DB9","background-color":"#FFF"});}
				});
			});
		});
	  	//FIM: Tabindex e destaque dos campos

		/*Impede que as telas de detalhamento sejam roladas at� o final, devido ao foco no bot�o Cancelar*/
		if($("form fieldset").is(".detalhamento")){$.scrollTo($("#mainContainer"),0);}

		//Redimensiona a marca da CDL proporcionalmente
		$("#nomeCDL").jScale({h:"41px"});

	   	//Remove o espa�o interno nos checkboxes das tabelas
		$("table :checkbox").css("padding","0");

		/*Remove a borda cinza da �ltima c�lula do cabe�alho das tabelas, gerada pelo
		estilo: .dataTableGGAS th {border-right: 1px solid #CBCBCB;} no arquivo displaytag.css*/
		$(".dataTableGGAS thead tr th:last-child").each(function(){
			$(this).css("border-right","1px solid #91B2C1");
		});



		//Define a largura do menu
		var larguraMenu = 0
		$("#menuBarra > div").each(function(){
			var wid = $(this).css("width");
			larguraMenu += Math.round(wid.slice(0,-2));
		});
		$("#menuBarra").css("width",function(){return larguraMenu;});

		/* Define o tamanho da borda arredondada inferior do menu a partir
		da largura total (considerando margins e paddings laterais) do div
		mais externo, que cont�m os links do menu e as bordas arredondadas */
		$(".menuDeslizanteConteiner").each(function(){
			$(this).children(".xbottom").css("width",function(){return $(".menuDeslizanteConteiner").outerWidth();});
		});

		//Verifica a largura dos campos <select> no IE7 e IE8 e reduz para 230px caso ultrapasse este valor.
		verificaLarguraSelect();

		redimensionarImagem();

	});/*-- FIM: Bloco JQuery -- */

	/*
	Exibe, esconde ou exibe e esconde determinado elemento e seu conte�do (se houver) com efeito deslizante.
	Par�metros:
	1) idElemento: o valor do atributo id (passado como String);
	2) opcao (passado como String):
		a) exibirEsconder: alterna as a��es, exibindo quando est� escondido e escondendo quando est� exibido;
		b) exibir: apenas exibe;
		c) esconder: apenas esconde;
	*/
	function exibirEsconderDeslizante(idElemento,opcao){
		var conjunto = $("#"+idElemento+", #"+idElemento+" *");
		if(opcao == "exibirEsconder"){
			$(conjunto).toggle("slow");
		} else if (opcao == "exibir"){
			$(conjunto).slideDown("slow");
		} else if (opcao == "esconder"){
			$(conjunto).slideUp("slow");
		}
	}


	/*Verifica se dois campos de intervalo de data foram preenchidos corretamente e habilita um bot�o*/
	function validarDatasLiberarBotao(dataInicial,dataFinal,botao){
		if($("#" + dataInicial).val() != "" && $("#" + dataInicial).val() != "__/__/____" &&
			/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/.test($("#" + dataInicial).val()) &&
			$("#" + dataFinal).val() != "" && $("#" + dataFinal).val() != "__/__/____" &&
			/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/.test($("#" + dataFinal).val())){
				$("#" + botao).removeAttr("disabled");
		} else {
			$("#" + botao).attr("disabled","disabled");
		}
	}

	//Verifica a largura dos campos <select> no IE7 e IE8 e reduz para 230px caso ultrapasse este valor.
	function verificaLarguraSelect() {
		/* Hack para o IE8: contorna o problema do <select> que possui <option>
    	com conte�do maior que a largura m�xima de 230px. Tamb�m captura o conte�do
    	do <option> e define no atributo title */
	   	if($.browser.msie) {
			$("select").each(function (i){
				var larguraSelect = $(this).width();
				if (larguraSelect > 230){
					$(this).css("width","230px");
					$("option", this).each(function (){
						var conteudoOption = $(this).html();
						$(this).attr("title",conteudoOption)
					});
				}
			});
	   	}
	}

	//Aplica��o da tradu��o do calend�rio para o portugu�s e defini��es gerais
	$.datepicker.setDefaults($.extend({showMonthAfterYear: false, onSelect: function(){$(this).focus();}}, $.datepicker.regional['pt-BR']));

	animatedcollapse.ontoggle=function($, divobj, state){ }
	animatedcollapse.init()

	<c:if test="${empty (pageContext.request.session.maxInactiveInterval) == false}">
		var timer;
		var timeOutSegundos = '${pageContext.request.session.maxInactiveInterval}';
		var timeOutMensagem = '300';

		function timeOutSessao() {
			if (timeOutSegundos != '' && parseInt(timeOutSegundos) > 0) {
				if (parseInt(timeOutSegundos) <= parseInt(timeOutMensagem)) {
					exibirJDialog("#avisoTimeoutSessao");
			    }
			   	timeOutSegundos--;
			}
		    timer = setTimeout("timeOutSessao()",1000);
		}

		addLoadEvent(timeOutSessao);
	</c:if>

	function confirmacaoLogout() {
		exibirJDialog("#confirmacaoLogout");
	}

	function alterarSenha() {
		exibirJDialog("#alterarSenhaPopup");
	}

	function exibirInfoSistema() {
		exibirJDialog("#infoSistemaPopup");
	}


	function carregarFragmento(alvoId, url, exibirMsgErro){
	  	var noCache = "&noCache=" + new Date().getTime();


	  	var retorno = $.ajax({
	          type: "GET",
	          url: url+noCache,
	          async: false,
	      }).responseText;

	 // O IE n�o tem a fun��o startsWith
	  	if (typeof String.prototype.startsWith != 'function') {
	  	  // see below for better implementation!
	  	  String.prototype.startsWith = function (str){
	  	    return this.indexOf(str) == 0;
	  	  };
	  	}

	  	retorno = $.trim(retorno);

	  	//houve erro
	 	if(retorno.startsWith('<div class="notification ')){

	 		if(exibirMsgErro != false){
		 		$('.mensagensSpring').removeAttr('style');
				$('.mensagensSpring').append(retorno);
				//fazer scroll at� o topo
		 		$.scrollTo($(".mensagensSpring"),800);
	 		}

			return false;

	 	}else{ // sucesso, carrega alvo
	 		$('#'+alvoId).html(retorno);
	 		$('.mensagensSpring').hide();
	 		return true;
	 	}

	  }

	function carregarFragmentoPOST(alvoId, url, form, exibirMsgErro){

	  	//var noCache = "&noCache=" + new Date().getTime();

	  	var retorno = $.ajax({
	        type: "POST",
	        url: url,
	        data: new FormData(form),
	        enctype: 'multipart/form-data;charset=ISO-8859-1',
	        cache: false,
	        processData: false,  // tell jQuery not to process the data
	        contentType: false,  // tell jQuery not to set contentType
	        async: false
	      }).responseText;

	 // O IE n�o tem a fun��o startsWith
	  	if (typeof String.prototype.startsWith != 'function') {
	  	  // see below for better implementation!
	  	  String.prototype.startsWith = function (str){
	  	    return this.indexOf(str) == 0;
	  	  };
	  	}

	  	retorno = $.trim(retorno);

	  	//houve erro
	 	if(retorno.startsWith('<div class="notification ')){

		 	var divErro = $('.mensagensSpring');
		 	if (divErro.offset() == undefined){
		 		divErro = $('.divErrors');
		 	}


	 		if(exibirMsgErro != false){
	 			divErro.removeAttr('style');
	 			divErro.html(retorno);
				//fazer scroll at� o topo
				$('html, body').animate({
			        scrollTop: divErro.offset().top
			   	 }, 2000);
	 		}

			return false;

	 	}else{ // sucesso, carrega alvo
	 		$('#'+alvoId).html(retorno);
	 		$('.mensagensSpring').hide();
	 		return true;
	 	}

	  }

	function removerEspacoInicio(elem){

    	return elem.replace(/^\s{1}/,"");

    }

	//redimensiona qualquer imagem na tela que tenha a classe imagemGgas
	//(class="imagemGgas") para o tamanho maximo de 320px
	//preservando a razao largura/altura da imagem
	function redimensionarImagem() {
		$(".imagemGgas").each(
	        function(){
	        	var width = $(this).width();
	        	var height = $(this).height();

	        	if (width > height) {
	        		$(this).width("320px");
				} else {
					$(this).height("320px");
				}
	        }
		);
    }

	function getPathIconeImpressora() {
		return '<c:url value="/imagens/icone_impressora.png" />';
    }

	function getPathIconeCalendario() {
		return '<c:url value="/imagens/calendario.gif"/>';
    }

</script>

<script src="${pageContext.request.contextPath}/js/lib/dataTables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.bootstrap4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.responsive.min.js"></script>
<script src="${pageContext.request.contextPath}/js/lib/dataTables/responsive.bootstrap4.min.js"></script>

<!-- GGAS 2.0 - MENU -->
	<script type="text/javascript">

    </script>
<!-- GGAS 2.0 - MENU -->

<!-- GGAS 2.0 - MIOLO DE PAO -->
    <script type="text/javascript">
        jQuery(document).ready(function()
        {
        	$("#breadCrumb0").jBreadCrumb();
        })
    </script>
<!-- GGAS 2.0 - MIOLO DE PAO -->

<!-- GGAS 2.0 - NOTIFICA��ES -->
    <script type="text/javascript">
    $(document).ready(function() {


        // Closing notifications
        // this is the class that we will target
        $(".hideit").click(function() {
        //fades the notification out
            $(this).fadeOut(700);
        });

    });

    </script>
<!-- GGAS 2.0 - NOTIFICA��ES -->

