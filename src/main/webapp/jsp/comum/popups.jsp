<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dataAtual" class="java.util.Date" />

<div style="display: none" id="avisoTimeoutSessao" title="Sess�o do sistema">Aten��o! Essa p�gina est� sem atividade h� algum tempo. Caso n�o haja atividade sua sess�o poder� expirar.</div>
<div style="display: none" id="confirmacaoLogout" title="Encerrar sess�o">Deseja sair do sistema?</div>


<script>

$(document).ready(function(){


});

function carregarRamosAtividadePontoConsumo(elem) {	
	var idSegmentoPontoConsumo = elem.value;
  	var selectRamoAtividade = document.getElementById("idRamoAtividade");

  	selectRamoAtividade.length=0;
  	var novaOpcao = new Option("Selecione","-1");
    selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
  	  
  	if(idSegmentoPontoConsumo != '-1'){	      		
       	AjaxService.listarRamoAtividadePorSegmento( idSegmentoPontoConsumo, {
       		callback:function(ramosAtividade) {            		      		         		
       			for (ramo in ramosAtividade){
               		for (key in ramosAtividade[ramo]){
	                	var novaOpcao = new Option(ramosAtividade[ramo][key], key);
	            		selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
               		}
            	}
            	selectRamoAtividade.disabled = false;
            	$("#idRamoAtividade").removeClass("campoDesabilitado");
        	}, async:false}
        );
    } else {	    	
    	selectRamoAtividade.disabled = true;
    	$("#idRamoAtividade").addClass("campoDesabilitado");
    }          	
}

</script>


<div style="display: none; text-align: left;" id="alterarSenhaPopup" title="Alterar Senha" >
	<form method="post" action="alterarSenha" name="alterarSenhaForm" id="alterarSenhaForm">
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Senha Atual:</label>
		<input class="campoTexto" type="password" id="senhaAtualHidden" name="senhaAtualHidden" maxlength="20" size="20" />
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Nova Senha:</label>
		<input class="campoTexto" type="password" id="senhaNovaHidden" name="senhaNovaHidden" maxlength="20" size="20" />
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Nova Senha:</label>
		<input class="campoTexto" type="password" id="confirmacaoSenhaNovaHidden" name="confirmacaoSenhaNovaHidden" maxlength="20" size="20" />
	</form>
</div>


<div style="display: none" id="infoSistemaPopup" title="Informa��es do Sistema">
    <h1 id="boasVindasTitulo">Bem Vindo ao GGAS:</h1>
    <h2 id="boasVindasSubtitulo">Sistema de Gest�o Comercial (Billing)<br />de Servi�os de Distribui��o de G�s</h2>
    Data Atual: <span id="boasVindasData"><fmt:formatDate value="${dataAtual}" type="both" pattern="dd/MM/yyyy" /></span><br />
    Usu�rio: <span id="boasVindasUsu�rio"><c:out value="${sessionScope.usuarioLogado.login}"/></span><br />
    Papel: <span id="boasVindasGrupo">
    <c:forEach items="${sessionScope.usuarioLogado.papeis}" var="papel">
   		<c:out value="${papel.descricao}"/>&nbsp
	</c:forEach>
   </span><br />
    Data do �ltimo Acesso: <span id="boasVindasUltimoAcesso"><fmt:formatDate value="${sessionScope.usuarioLogado.ultimoAcesso}" type="both" pattern="dd/MM/yyyy HH:mm:ss" /></span><br /><br />
    Servidor: <span id="boasVindasUsu�rio">${pageContext.request.serverName} (${pageContext.request.localAddr})</span><br/>
    ID Sess�o: <span id="boasVindasUsu�rio">${pageContext.request.session.id}</span><br/>
    Build: <fmt:bundle basename="constantes">[<fmt:message key="VERSAO_BUILD" />] [<fmt:message key="VERSAO_DATA" />]</fmt:bundle>
</div>

<div style="display: none" id="janelaHelp" title="Ajuda">
	<iframe id="iframeHelp" frameborder="0" src="<help:help>/default.htm</help:help>"></iframe>
</div>

<div style="display: none; text-align: left;" id="pontoConsumoPopup" title="Ponto Consumo" >
	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Segmento:</label>
	<select name="idSegmento" id="idSegmento" class="campoSelect" onchange="carregarRamosAtividadePontoConsumo(this);">
		<option value="-1">Selecione</option>
		<c:forEach items="${listaSegmento}" var="segmento">
			<option value="<c:out value="${segmento.chavePrimaria}"/>">
				<c:out value="${segmento.descricao}"/>
			</option>		
		</c:forEach>
	</select><br/>
	<label class="rotulo rotulo2Linhas campoObrigatorio"><span class="campoObrigatorioSimbolo" style="width: 105px">* </span>Ramo de Atividade:</label>
	<select name="idRamoAtividade" id="idRamoAtividade" class="campoSelect campo2Linhas" <c:if test="${empty listaRamoAtividadePontoConsumo}">disabled="disabled""</c:if>>
		<option value="-1">Selecione</option>
		<c:forEach items="${listaRamoAtividadePontoConsumo}" var="ramoAtividade">
			<option value="<c:out value="${ramoAtividade.chavePrimaria}"/>">
				<c:out value="${ramoAtividade.descricao}"/>
			</option>		
		</c:forEach>
	</select><br/>
	
	<!-- Campo esta sendo carregado com ajaxService -->
	<label class="rotulo rotulo2Linhas campoObrigatorio"><span id="spanRotaImovelLote" class="campoObrigatorioSimbolo" style="width: 105px">* </span>Rota:</label>
	<select name="idRota" id="idRota" class="campoSelect campo2Linhas">
		<option value="-1">Selecione</option>
	</select>
	
</div>

<jsp:include page="/jsp/controleacesso/autorizacoesPendentesPopup.jsp" />
