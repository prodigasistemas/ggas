<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script lang="text/javascript" >
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

$(document).ready(function(){
	
		  
			
function updateRightLeftButtons () {
    if (306 > (parseInt($('#contentScroller').css('marginLeft')) * -1)) {
        $('#left-button').hide();
    } else {
        $('#left-button').show();
    }

	var p =$('li#favorito:last');
	var position = p.position(); 
	if(typeof position!='undefined') {
    if(position.left < 500) {
    	$('#right-button').hide();
    } else {
    	$('#right-button').show();
        }
	}
   
};
$('#right-button').click(function() {
    updateRightLeftButtons();
    if ($(this).is(':visible'))
    $('#contentScroller').animate({
        marginLeft: "-=436px"
    }, 1000, updateRightLeftButtons);
});
$('#left-button').click(function() {
    updateRightLeftButtons();
    if ($(this).is(':visible'))
    $('#contentScroller').animate({
        marginLeft: "+=436px"
    }, 1000, updateRightLeftButtons);
});
updateRightLeftButtons();
$('#left-button').dblclick(function(e){
    e.preventDefault();
  });
  $('#right-button').dblclick(function(e){
	    e.preventDefault();
	  });
  
  $('#left-button').click(function(){
	    var btn = $(this);
	    btn.prop('disabled',true);
	    window.setTimeout(function(){ 
	        btn.prop('disabled',false);
	    },1000);
	});  
  
  $('#right-button').click(function(){
	    var btn = $(this);
	    btn.prop('disabled',true);
	    window.setTimeout(function(){ 
	        btn.prop('disabled',false);
	    },1000);
	});
});


function removerFavorito(chavePrimaria, chavePrimariaMenu) {
	AjaxService.removerFavorito(chavePrimaria);
	$('#'+chavePrimariaMenu+'-img')[0].setAttribute('src','images/ico_fav_off.png');
	$('#'+chavePrimaria).show();
	$('#'+chavePrimaria+"-remove").parent().remove();
	$('#'+chavePrimaria+"-remove").hide();
}


jQuery("document").ready(function($){
    
    var nav = $('.nav-container');
    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });
 
});

</script>
<!-- GGAS 2.0 - TOPO -->
<div class="nav-container"><!-- PARA DEIXAR O TOPO FIXO -->
<div class="nav"><!-- PARA DEIXAR O TOPO FIXO -->
<div id="header">	    
    <!--LOGOMARCA-->
    <div class="ggas_header_logo"> <a id="pagina_inicial" title="In�cio" href="<c:url value="/exibirHome"/>"> <img src="images/logo_ggas.png" width="100" height="27" alt="logo" title="logo" /> </a> </div>
    <!--LOGOMARCA--> 
    
    <!--PERFIL-->
    <div class="ggas_header_perfil">
          <ul>
        <li><a ><img src="images/ico_perfil.png" class="ggas_header_perfil_ico" width="30" height="30" alt="home" title="home" /></a></li>
        <li>Bem-vindo <c:out value="${sessionScope.usuarioLogado.login}"></c:out>!</li>
      </ul>
        </div>
    <!--PERFIL--> 
    
    <!-- FAVORITOS -->
    <div id="scrollerOut" class="scroller2">
    <a href="#" id="left-button" class="left-button">
        <img  src="imagens/buttonPrevious.png"/>
    </a>  
    <a href="#" id="right-button" class="buttonNext">
        <img   src="imagens/buttonNext.png"/>                               
    </a>
    
     <div id="imgThumbs" class="scroller">
     	<div class="ico_fav">
    		<img src="imagens/ico_fav.png" class="ico_fav2" /><a class="ico_fav3">�ltimos itens salvos</a>
    	</div>
     
    	<div id="contentScroller" class="contentScroller">      
    		<c:forEach items="${listaFavoritos}" var="favorito" >
    			&nbsp;&nbsp;&nbsp;&nbsp;
    			<li style="display: inline" id="favorito">
            	<a id="${favorito.chavePrimaria}-nome" class="favorito1" href="<c:url value="${favorito.menu.url}"/>"><c:out value="${favorito.menu.descricao}" /></a>
            	<a id="${favorito.chavePrimaria}-remove" class="favorito2" onclick="javascript:removerFavorito(${favorito.chavePrimaria},${favorito.menu.chavePrimaria});">
					<img alt="" class="close_tag_favoritos"  src="imagens/close_tag_favoritos.png" />
				</a>
				</li>
			</c:forEach>                                        
     	</div>
    
     </div>
</div>
<!-- FAVORITOS -->

   <!--CONTROLE-->
    <div class="ggas_header_controle">
          <ul>
        <li><a id="pagina_inicial" title="In�cio" href="<c:url value="/exibirHome"/>"><img src="images/ico_home.png" width="30" height="30" alt="home" title="home" /></a></li>
        <li><a id="info_sistema" title="Informa��o" onclick="exibirInfoSistema(); return false;" href="javascript:void(0)"><img src="images/ico_info.png" width="30" height="30" alt="Informa��o" title="Informa��o" /></a></li>
        <li><a id="alterar_senha" title="Senha" onclick="alterarSenha(); return false;" href="javascript:void(0)"><img src="images/ico_pass.png" width="30" height="30" alt="Senha" title="Senha" /></a></li>
        <li><a id="sair_sistema" title="Sair" onclick="confirmacaoLogout(); return false;" href="javascript:void(0)"><img src="images/ico_logout.png" width="30" height="30" alt="Sair" title="Sair" /></a></li>
      </ul>
        </div>
    <!--CONTROLE-->
  </div>
  </div><!-- PARA DEIXAR O TOPO FIXO -->
</div><!-- PARA DEIXAR O TOPO FIXO -->
<!-- GGAS 2.0 - TOPO -->  