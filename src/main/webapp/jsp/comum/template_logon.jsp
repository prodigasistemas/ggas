<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- Cabe�alho HTML gen�rico para todos os templates -->
	<tiles:insertAttribute name="head" />
	
</head>

<body class="bodyLogon">
	<!--[if lt IE 7]>
	<div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;'>
	  <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'><a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;'><img src='<c:out value='${pageContext.request.contextPath}'/>/imagens/ie6nomore-cornerx.jpg' style='border: none;' alt='Close this notice'/></a></div>
	  <div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>
	    <div style='width: 75px; float: left;'><img src='<c:out value='${pageContext.request.contextPath}'/>/imagens/ie6nomore-warning.jpg' alt='AVISO!'/></div>
	    <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>
	      <div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>Voc� est� usando um navegador desatualizado</div>
	      <div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>Para utilizar melhor esse sistema, por favor, atualize seu navegador.</div>
	    </div>
	    <div style='width: 75px; float: left;'><a href='http://pt-br.www.mozilla.com/pt-BR/' target='_blank'><img src='<c:out value='${pageContext.request.contextPath}'/>/imagens/ie6nomore-firefox.jpg' style='border: none;' alt='Baixar Firefox 3.5'/></a></div>
	    <div style='width: 75px; float: left;'><a href='http://www.microsoft.com/downloads/details.aspx?FamilyID=341c2ad5-8c3d-4347-8c03-08cdecd8852b&DisplayLang=pt-br' target='_blank'><img src='<c:out value='${pageContext.request.contextPath}'/>/imagens/ie6nomore-ie8.jpg' style='border: none;' alt='Baixar Internet Explorer 8'/></a></div>
	 </div>
	</div>
	<![endif]-->
	<div id="mainContainer">
		<tiles:insertAttribute name="erromensagem" />
		<tiles:insertAttribute name="corpo"/>
		<tiles:insertAttribute name="rodape"/>			
	</div>

	</body>
</html>
