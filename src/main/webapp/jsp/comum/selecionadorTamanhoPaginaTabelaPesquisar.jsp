<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">


</script>

<div class="selecionadorTamanhoPagina">
	Tamanho da P�gina:
	<c:choose>
		<c:when test="${tamanhoPagina == 15}">
			<strong>15</strong>,	
		</c:when>
		<c:otherwise>
			 <a href="${param.acao}tamanhoPagina=15"  title="Visualizar 15 registros por tela">15</a>, 
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${tamanhoPagina == 30}">
			<strong>30</strong>,	
		</c:when>
		<c:otherwise>
			 <a href="${param.acao}tamanhoPagina=30"  title="Visualizar 30 registros por tela">30</a>, 
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${tamanhoPagina == 100}">
			<strong>100</strong>,	
		</c:when>
		<c:otherwise>
			 <a href="${param.acao}tamanhoPagina=100"  title="Visualizar 100 registros por tela">100</a>, 
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${tamanhoPagina == 200}">
			<strong>200</strong>,
		</c:when>
		<c:otherwise>
			 <a href="${param.acao}tamanhoPagina=200" title="Visualizar 200 registros por tela">200</a>,
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${tamanhoPagina == 500}">
			<strong>500</strong>
		</c:when>
		<c:otherwise>
			 <a href="${param.acao}tamanhoPagina=500" title="Visualizar 500 registros por tela">500</a>
		</c:otherwise>
	</c:choose>
</div>