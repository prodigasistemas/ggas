<!--
 Copyright (C) <2018> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2018 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dataAtual" class="java.util.Date" />

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s <fmt:bundle basename="constantes">Build: [<fmt:message key="VERSAO_BUILD" />] [<fmt:message key="VERSAO_DATA" />]</fmt:bundle></title>

    <%--Contexto da aplica��o--%>
    <c:set var="ctx" value="${pageContext.request.contextPath}" scope="application"/>

    <%--Para mais informa��es leia o arquivo web/README.md --%>
    <c:set var="ctxWebpack" value="http://localhost:9000" scope="application"/>
    <c:if test="${isProduction}">
        <c:set var="ctxWebpack" value="${pageContext.request.contextPath}" scope="application"/>
    </c:if>


    <jsp:include page="../importsHeader.jsp" />
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/jquery.dataTables.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.responsive.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/responsive.bootstrap4.min.js"></script>

    <%-- O css abaixo � gerado pelo webpack, para mais informa��es leia o arquivo na pasta web/README.md--%>
    <link href="${ctxWebpack}/dist/layout/inicializacao/index.css" rel="stylesheet" type="text/css">
	
	<tiles:importAttribute name="bootstrapDatepicker" ignore="true" />
	<c:if test="${bootstrapDatepicker eq 'true'}">
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css" integrity="sha512-TQQ3J4WkE/rwojNFo6OJdyu6G8Xe9z8rMrlF9y7xpFbQfW5g8aSWcygCQ4vqRiJqFsDsE1T6MoAOMJkFXlrI9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.pt-BR.min.js" integrity="sha512-mVkLPLQVfOWLRlC2ZJuyX5+0XrTlbW2cyAwyqgPkLGxhoaHNSWesYMlcUjX8X+k45YB8q90s88O7sos86636NQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	</c:if>
	
</head>

<tiles:importAttribute name="responsivo" ignore="true" />
<c:if test="${responsivo eq 'true'}">
    <c:set var="classeBody" value="responsivo"/>
</c:if>

<body class="${classeBody}">
    <input type="hidden" id="ctx" value="${ctx}"/>
    <div class="wrapper">

        <jsp:include page="topbar.jsp" />
        <jsp:include page="sidebar.jsp" />

        <c:if test="${responsivo eq 'true'}">
            <div class="be-content">
                <div class="main-content container-fluid bootstrap">
                        <tiles:insert name="erromensagem" />
                        <tiles:insert name="corpo" />
                        <tiles:insert name="popups" />
                </div>
            </div>
        </c:if>

    </div>

    <c:if test="${responsivo eq 'false'}">
        <div id="conteudo">
            <div class="breadCrumbHolder module">
                <div id="breadCrumb" class="breadCrumb module">
                    <ul>
                        <tiles:useAttribute name="breadcrumb" />
                        <ul>

                            <c:if test="${param.tela eq 'atividadeEconomica'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Atividade Econ�mica  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'profissao'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Profiss�o  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tipoContato'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Tipo de Contato  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tipoEndereco'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Tipo de Endere�o  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tipoCliente'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Tipo de Pessoa  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tipoRelacionamentoCliente'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Tipo de Relacionamento  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tipoTelefone'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Tipo de Telefone  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'situacaoCliente'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Pessoa  </a></li>
                                <li><a href="#">Situa��o da Pessoa  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'areaConstruida'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Im�vel  </a></li>
                                <li><a href="#">�rea Constru�da  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'padraoConstrucao'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Im�vel  </a></li>
                                <li><a href="#">Padr�o de Constru��o  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'pavimentoCalcada'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Im�vel  </a></li>
                                <li><a href="#">Pavimento da Cal�ada  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'pavimentoRua'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Im�vel  </a></li>
                                <li><a href="#">Pavimento de Rua  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'perfilImovel'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Im�vel  </a></li>
                                <li><a href="#">Perfil de Im�vel  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tipoBotijao'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Im�vel  </a></li>
                                <li><a href="#">Tipo de Cilindro  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tipoSegmento'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Im�vel  </a></li>
                                <li><a href="#">Tipo de Segmento  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'perfilQuadra'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Localiza��o  </a></li>
                                <li><a href="#">Perfil de Quadra  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'areaTipo'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Localiza��o  </a></li>
                                <li><a href="#">Tipo de �rea  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'zeis'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Localiza��o  </a></li>
                                <li><a href="#">Zeis  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'setorCensitario'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Localiza��o  </a></li>
                                <li><a href="#">Setor Censit�rio  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'redeDiametro'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Operacional  </a></li>
                                <li><a href="#">Di�metro de Rede  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'redeMaterial'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Operacional  </a></li>
                                <li><a href="#">Material de Rede  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'zonaBloqueio'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Operacional  </a></li>
                                <li><a href="#">Zona de Bloqueio  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'unidadeNegocio'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Empresa  </a></li>
                                <li><a href="#">Unidade de Neg�cio  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'unidadeTipo'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Empresa  </a></li>
                                <li><a href="#">Tipo de Unidade  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'corretorMarca'}">
                                <li><a href="#">Medi��o </a></li>
                                <li><a href="#">Corretor de Vaz�o </a></li>
                                <li><a href="#">Marca do Corretor de Vaz�o  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'corretorModelo'}">
                                <li><a href="#">Medi��o </a></li>
                                <li><a href="#">Corretor de Vaz�o </a></li>
                                <li><a href="#">Modelo do Corretor de Vaz�o  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'medidorMarca'}">
                                <li><a href="#">Medi��o </a></li>
                                <li><a href="#">Medidor </a></li>
                                <li><a href="#">Marca do Medidor  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'medidorModelo'}">
                                <li><a href="#">Medi��o </a></li>
                                <li><a href="#">Medidor </a></li>
                                <li><a href="#">Modelo do Medidor  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'periodicidade'}">
                                <li><a href="#">Faturamento </a></li>
                                <li><a href="#">Faturamento </a></li>
                                <li><a href="#">Periodicidade  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'grupoFaturamento'}">
                                <li><a href="#">Faturamento </a></li>
                                <li><a href="#">Faturamento </a></li>
                                <li><a href="#">Grupo de Faturamento  </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'tributo'}">
                                <li><a href="#">Faturamento </a></li>
                                <li><a href="#">Tributo </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'microrregiao'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Localiza��o </a></li>
                                <li><a href="#">Regi�o e Microrregi�o </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'cityGate'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Operacional </a></li>
                                <li><a href="#">City Gate </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'unidadeFederacao'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Localiza��o </a></li>
                                <li><a href="#">Unidade Federativa </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'municipio'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Localiza��o </a></li>
                                <li><a href="#">Munic�pio </a></li>
                            </c:if>

                            <c:if test="${param.tela eq 'tronco'}">
                                <li><a href="#">Cadastro </a></li>
                                <li><a href="#">Operacional </a></li>
                                <li><a href="#">Tronco </a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'calendario'}">
                                <li><a href="#">Configura&ccedil;&atilde;o</a></li>
                                <li><a href="#">Calend&aacute;rio</a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'constantesSistema'
									|| param.acao eq 'carregarConstantes'
									|| param.acao eq 'exibirConstanteSistema'}">
                                <li><a href="#">Configura&ccedil;&atilde;o</a></li>
                                <li><a href="#">Constantes do Sistema</a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'entidadeClasse'}">
                                <li><a href="#">Configura&ccedil;&atilde;o</a></li>
                                <li><a href="#">Entidade Classe</a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'entidadeConteudo'}">
                                <li><a href="#">Configura&ccedil;&atilde;o</a></li>
                                <li><a href="#">Entidade Conte&uacute;do</a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'unidadeClasse'}">
                                <li><a href="#">Configura&ccedil;&atilde;o</a></li>
                                <li><a href="#">Unidade Classe</a></li>
                            </c:if>
                            <c:if test="${param.tela eq 'canalAtendimento'}">
                                <li><a href="#">Atendimento </a></li>
                                <li><a href="#">Chamado </a></li>
                                <li><a href="#">Canal de Atendimento </a></li>
                            </c:if>
                            <c:forEach items="${breadcrumb}" var="item">
                                <li><a href="${item.link}">${item.value}</a></li>
                            </c:forEach>
                        </ul>
                    </ul>
                </div>
            </div>
            <div id="frame">
            </div>
            <div class="main_container">
                <div id="inner">
                    <tiles:insert name="erromensagem" />
                    <tiles:insert name="corpo" />
                    <tiles:insert name="popups" />
                </div>
            </div>
        </div>
    </c:if>

    <div class="app-carregando" style="display: none">
        <img src="${pageContext.request.contextPath}/imagens/loading.gif"/>
        <div class="app-carregando--backdrop"/>
    </div>

</body>

<!-- GGAS 2.0 NOVOS JAVASCRIPTS -->

<script type="text/javascript" src="<c:url value="/js/utils.js"/>" charset="UTF-8"></script>
<script src="${ctxWebpack}/dist/layout/inicializacao/index.js"></script>
<tiles:insert name="rodape" />

<script type="text/javascript">

    dwr.engine._errorHandler = function(){};
    dwr.engine._warningHandler = function(){};

</script>


</html>
