<!--
 Copyright (C) <2018> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2018 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dataAtual" class="java.util.Date" />

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás <fmt:bundle basename="constantes">Build: [<fmt:message key="VERSAO_BUILD" />] [<fmt:message key="VERSAO_DATA" />]</fmt:bundle></title>

    <%--Contexto da aplicação--%>
    <c:set var="ctx" value="${pageContext.request.contextPath}" scope="application"/>

    <%--Para mais informações leia o arquivo web/README.md --%>
    <c:set var="ctxWebpack" value="http://localhost:9000" scope="application"/>
    <c:if test="${isProduction}">
        <c:set var="ctxWebpack" value="${pageContext.request.contextPath}" scope="application"/>
    </c:if>

    <jsp:include page="../importsHeader.jsp" />
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/jquery.dataTables.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.responsive.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/responsive.bootstrap4.min.js"></script>

    <link href="css/ggas_home.css" rel="stylesheet" type="text/css">
    <link href="${ctxWebpack}/dist/layout/inicializacao/index.css" rel="stylesheet" type="text/css">
</head>

<tiles:importAttribute name="responsivo" ignore="true" />
<c:if test="${responsivo eq 'true'}">
    <c:set var="classeBody" value="responsivo"/>
</c:if>

<body class="${classeBody}">
    <input type="hidden" id="ctx" value="${ctx}"/>
    <div class="wrapper">

        <jsp:include page="topbar.jsp" />
        <jsp:include page="sidebar.jsp" />

    </div>
    <tiles:insertAttribute name="erromensagem" />
    <tiles:insertAttribute name="corpo" />
    <tiles:insertAttribute name="popups" />

    <div class="app-carregando" style="display: none">
        <img src="${pageContext.request.contextPath}/imagens/loading.gif"/>
        <div class="app-carregando--backdrop"></div>
    </div>

</body>

<!-- GGAS 2.0 NOVOS JAVASCRIPTS -->

<script type="text/javascript" src="<c:url value="/js/utils.js"/>" charset="UTF-8"></script>
<script src="${ctxWebpack}/dist/layout/inicializacao/index.js"></script>
<tiles:insertAttribute name="rodape" />
<script type="text/javascript">

    dwr.engine._errorHandler = function(){};
    dwr.engine._warningHandler = function(){};

</script>


</html>
