<!--
 Copyright (C) <2018> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2018 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dataAtual" class="java.util.Date" />

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s <fmt:bundle basename="constantes">Build: [<fmt:message key="VERSAO_BUILD" />] [<fmt:message key="VERSAO_DATA" />]</fmt:bundle></title>

    <%--Contexto da aplica��o--%>
    <c:set var="ctx" value="${pageContext.request.contextPath}" scope="application"/>

    <%--Para mais informa��es leia o arquivo web/README.md --%>
    <c:set var="ctxWebpack" value="http://localhost:9000" scope="application"/>
    <c:if test="${isProduction}">
        <c:set var="ctxWebpack" value="${pageContext.request.contextPath}" scope="application"/>
    </c:if>
	
	<jsp:include page="../importsHeader.jsp"></jsp:include>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/jquery.dataTables.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/dataTables.responsive.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/lib/dataTables/responsive.bootstrap4.min.js"></script>

    <link href="${ctxWebpack}/dist/layout/inicializacao/index.css" rel="stylesheet" type="text/css">
</head>


<tiles:useAttribute name="responsivo" ignore="true" />
<c:if test="${responsivo}">
    <c:set var="classeBody" value="responsivo"/>
</c:if>

<body class="${classeBody}">
    <input type="hidden" id="ctx" value="${ctx}"/>

        <c:if test="${responsivo}">
                <div class="container-fluid bootstrap">
                        <tiles:insertAttribute name="erromensagem" />
                        <tiles:insertAttribute name="corpo" />
                        <tiles:insertAttribute name="popups" />
                </div>
        </c:if>


    <div class="app-carregando" style="display: none">
        <img src="${pageContext.request.contextPath}/imagens/loading.gif"/>
        <div class="app-carregando--backdrop"/>
    </div>

</body>

<!-- GGAS 2.0 NOVOS JAVASCRIPTS -->

<script type="text/javascript" src="<c:url value="/js/utils.js"/>" charset="UTF-8"></script>
<script src="${ctxWebpack}/dist/layout/inicializacao/index.js"></script>

<c:if test="${isCaixaAlta != null && isCaixaAlta}">
    <script type="text/javascript" src="<c:url value="/js/uppercase.js"/>" charset="UTF-8"></script>
</c:if>

</html>