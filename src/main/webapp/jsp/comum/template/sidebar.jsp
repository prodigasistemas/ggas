<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle shadow" href="javascript:void(0)">Menu</a>
        <div class="left-sidebar-spacer shadow">
            <div class="nav-search">
                <input id="inputPesquisar" placeholder="Pesquise um item no menu" type="text" class="form-control form-control-sm pesquisar"/>
            </div>
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements hide">
                        <li class="active"><a href="<c:url value="/exibirHome"/>"><i class="icon fa fa-home"></i><span>P�gina Inicial</span></a>
                        </li>
                        <c:forEach items="${modulosMenu}" var="modulo">
                            <c:if test="${not empty modulo.listaMenu}">
                                <li id="${modulo.chavePrimaria}" class="parent">
                                    <a href="javascript:void(0)"><i class="icon fa"></i><span>${modulo.descricao}</span></a>
                                    <ul class="sub-menu">
                                        <c:forEach items="${modulo.listaMenu}" var="nivel1">

                                            <c:if test="${not empty nivel1.listaMenu or (nivel1.url ne null and nivel1.indicadorMostrarMenu)}">

                                                <c:set var="classeParent" value="parent"/>
                                                <c:if test="${empty nivel1.listaMenu or nivel1.indicadorExibirSemFilhos}">
                                                    <c:set var="classeParent" value=""/>
                                                </c:if>
                                                <li id="${nivel1.chavePrimaria}" class="${classeParent}">
                                                    <c:set var="url" value="javascript:void(0)"/>
                                                    <c:if test="${nivel1.url ne null and (empty nivel1.listaMenu or nivel1.indicadorExibirSemFilhos)}">
                                                        <c:set var="url" value="${nivel1.url}"/>
                                                    </c:if>

                                                    <c:set var="classeFavorito" value="far"/>
                                                    <c:if test="${nivel1.favorito}">
                                                        <c:set var="classeFavorito" value="fa text-warning"/>
                                                    </c:if>

                                                    <a href="${url}"><span>${nivel1.descricao}</span></a>

                                                    <c:if test="${nivel1.url ne null and (empty nivel1.listaMenu or nivel1.indicadorExibirSemFilhos)}">
                                                            <span data-chavePrimaria="${nivel1.chavePrimaria}"
                                                                  title="Adicionar / Remover dos favoritos"
                                                                  class="addFavorito fa-star fa-fw fa-lg ${classeFavorito}">
                                                            </span>
                                                    </c:if>

                                                    <c:if test="${not empty nivel1.listaMenu}">

                                                        <ul class="sub-menu">
                                                            <c:forEach items="${nivel1.listaMenu}" var="nivel2">
                                                                <c:if test="${nivel2.url ne null && nivel2.indicadorMostrarMenu == true}">
                                                                    <li id="${nivel2.chavePrimaria}">
                                                                        <c:set var="url" value="javascript:void(0)"/>
                                                                        <c:if test="${nivel2.url ne null}">
                                                                            <c:set var="url" value="${nivel2.url}"/>
                                                                        </c:if>

                                                                        <c:set var="classeFavorito" value="far"/>
                                                                        <c:if test="${nivel2.favorito}">
                                                                            <c:set var="classeFavorito" value="fa text-warning"/>
                                                                        </c:if>

                                                                        <a href="${url}"><span>${nivel2.descricao}</span></a>


                                                                        <span data-chavePrimaria="${nivel2.chavePrimaria}"
                                                                              title="Adicionar / Remover dos favoritos"
                                                                              class="addFavorito fa-star fa-fw fa-lg ${classeFavorito}">
                                                                            </span>
                                                                    </li>
                                                                </c:if>

                                                            </c:forEach>
                                                        </ul>

                                                    </c:if>

                                                </li>

                                            </c:if>

                                        </c:forEach>
                                    </ul>
                                </li>
                            </c:if>
                        </c:forEach>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
