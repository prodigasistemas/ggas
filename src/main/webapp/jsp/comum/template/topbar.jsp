<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<nav class="navbar navbar-expand fixed-top be-top-header">
    <div class="container-fluid">
        <div class="be-navbar-header"><a class="navbar-brand" href="<c:url value="/exibirHome"/>"></a>
        </div>
        <div class="be-right-navbar">
            <div class="page-title"><span> <i class="fa fa-user fa-xs fa-fw"></i> Bem-vindo <c:out value="${sessionScope.usuarioLogado.login}"></c:out>!</span></div>

            <ul class="nav navbar-nav menu-usuario-mobile">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="user-name fa fa-lg fa-user"></span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <div class="user-info">
                            <div class="user-name"><i class="fa fa-user-circle"></i> Usu�rio <c:out value="${sessionScope.usuarioLogado.login}"></c:out></div>
                        </div>
                        <a class="dropdown-item info_sistema" href="javascript:void(0)"><span class="icon fa fa-info-circle"></span>Informa��es do Sistema</a>
                        <a class="dropdown-item alterar_senha" href="javascript:void(0)"><span class="icon fa fa-key"></span>Alterar Senha</a>
                        <a class="dropdown-item sair_sistema" href="javascript:void(0)"><span class="icon fa fa-power-off"></span>Sair do Sistema</a>
                    </div>
                </li>
            </ul>

            <c:if test="${not empty listaFavoritos}">
                <ul class="nav navbar-nav favoritos">
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle nav-link" data-toggle="dropdown">
                            <i class="fa fa-star text-warning fa-fw"></i> <span>�ltimos Itens Salvos</span>
                        </a>
                        <ul class="dropdown-menu be-notifications">
                            <li>
                                <div class="list">
                                    <div class="be-scroller">
                                        <div class="content">
                                            <ul>
                                                <c:forEach items="${listaFavoritos}" var="favorito" >
                                                    <li class="notificacao">
                                                        <div class="notification-info">
                                                            <div class="text">
                                                                <i class="fa fa-star text-warning fa-sm fa-fw"></i><span>${favorito.menu.descricao}</span>
                                                            </div>
                                                        </div>
                                                        <a title="Remover Item"
                                                           data-chavePrimaria="${favorito.chavePrimaria}"
                                                           href="javascript:void(0)" class="float-right removerFavorito">
                                                            <i class="fa fa-trash-alt text-muted"></i>
                                                        </a>
                                                        <a title="Ir at� a p�gina" href="${favorito.menu.url}" class="float-right">
                                                            <i class="fa fa-paper-plane"></i>
                                                        </a>
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </c:if>

            <ul class="nav navbar-nav float-right be-user-nav">
                <li class="nav-item">
                    <a id="info_sistema" title="Clique para obter informa��es do sistema"
                       class="info_sistema nav-link be-toggle-right-sidebar"
                       href="javascript:void(0)" role="button" aria-expanded="false">
                        <i class="fa fa-lg fa-info-circle"></i>
                    </a>

                </li>
                <li class="nav-item">
                    <a id="alterar_senha" title="Clique para alterar sua senha" class="alterar_senha nav-link be-toggle-right-sidebar"
                       href="javascript:void(0)" role="button" aria-expanded="false">
                        <i class="fa fa-lg fa-key"></i>
                    </a>

                </li>
                <li class="nav-item">
                    <a id="sair_sistema" title="Clique para sair do sistema" class="sair_sistema nav-link be-toggle-right-sidebar"
                       href="javascript:void(0)" role="button" aria-expanded="false">
                        <i class="fa fa-lg fa-sign-out-alt"></i>
                    </a>

                </li>
            </ul>
        </div>
    </div>
</nav>