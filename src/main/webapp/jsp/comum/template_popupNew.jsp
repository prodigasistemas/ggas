<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dataAtual" class="java.util.Date" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />

<!-- Cabe�alho HTML gen�rico para todos os templates -->
<tiles:insertAttribute name="head" />
<!-- Aten��o: inserir todo e qualquer script a partir desta linha -->


</head>

<body>
	<div id="mainContainerPopup">
	<tiles:insertAttribute name="topo"/>
			
		<!-- Conteiner do Conte�do -->
		<div id="conteudoPopup">
			<div id="framePopup">
				<div id="innerPopup">				
					<div id="conteinerGeral">
						<tiles:insertAttribute name="erromensagem" />
						<tiles:insertAttribute name="corpo" />
						<tiles:insertAttribute name="rodape" />
						<tiles:insertAttribute name="popups" />
					</div>
				</div>				
			</div>
		</div>
	</div>
</body>
</html>
