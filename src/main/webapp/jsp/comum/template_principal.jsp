<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dataAtual" class="java.util.Date" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- Cabe�alho HTML gen�rico para todos os templates -->
	<tiles:insertAttribute name="head"/>
	<!-- Aten��o: inserir todo e qualquer script a partir desta linha -->
	
	<script>
		$(document).ready(function(){
			$("#nomeCDL,#menuBarraLinks").show();
			
			//ajuste nas mensagens de erro que aparecem quando nenhuma tela est� carregada.
			var innerConteudo = $("#inner").length;			
			var notificacao = $(".notification").size();
			
			if ((innerConteudo == 0) && (notificacao > 0)){
				$(".hideit").css("width", "73%");
				$(".hideit").css("margin-left", "20%");
			}
		});
		
		// Inicia com o menu aberto
		//animatedcollapse.addDiv('menuBarraLinks', 'fade=0,speed=400,persist=0,hide=0')
	</script>
	
</head>

<body>
	<div id="mainContainer">
		<tiles:insertAttribute name="topo"/>		
		<tiles:insertAttribute name="menuBarra"/>
		
		<!-- Conteiner do Conte�do -->
		<div id="conteudo">
			<tiles:insertAttribute name="erromensagem"/>
			<tiles:insertAttribute name="corpo"/>
			<tiles:insertAttribute name="popups"/>
			<tiles:insertAttribute name="rodape"/>
		</div>
	</div>

</body>
</html>
