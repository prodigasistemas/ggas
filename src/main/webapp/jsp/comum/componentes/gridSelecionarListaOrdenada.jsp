
<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="indexListaEntidadesSelecionadas" value="0" />

<display:table class="dataTableGGAS dataTableGridListaEntidadesSelecionadas"
	name="requestScope.listaEntidadesSelecionadas" sort="list" id="entidade"
	pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao"
	requestURI="#">
				
	<display:column sortable="false"
			headerClass="tituloTabelaEsq" title="Ordem"
			style="text-align: left; padding-left: 10px; width: 50px" > 
			<c:out value="${indexListaEntidadesSelecionadas + 1}"/>
	</display:column>
	
	<display:column sortable="false"
		headerClass="tituloTabelaEsq" title="Descri��o do(a) ${requestScope.labelEntidade}"
		style="text-align: left; padding-left: 10px" >
		<input type="hidden" name="cpAdicionados" value="${entidade.chavePrimaria}" />
		<c:out value="${entidade.descricao}"/>
	</display:column>

	<display:column
		style="text-align: center; width: 25px; min-width: 25px;">
		
		<c:if test="${ indexListaEntidadesSelecionadas > 0 }">
			<a href="javascript:alterarOrdemListaEntidadesSelecionadas(${indexListaEntidadesSelecionadas}, ${indexListaEntidadesSelecionadas - 1})"><img
				title="Subir Posi��o" alt="Subir Posi��o"
				src="<c:url value="/imagens/setaCima.png"/>"></a>
		</c:if>

		<c:if test="${ fn:length(listaEntidadesSelecionadas) - 1 !=  indexListaEntidadesSelecionadas }">
			<a href="javascript:alterarOrdemListaEntidadesSelecionadas(${indexListaEntidadesSelecionadas}, ${indexListaEntidadesSelecionadas + 1})"><img
				title="Descer Posi��o" alt="Descer Posi��o"
				src="<c:url value="/imagens/setaBaixo.png"/>"></a>
		</c:if>

	</display:column>
			
	<display:column
		style="text-align: center; width: 25px; min-width: 25px;">
	
		<a onclick="return confirm('Deseja excluir o(a) ${requestScope.labelEntidade}?');"
			href="javascript:removerEntidade(${indexListaEntidadesSelecionadas});"><img
			title="Excluir ${requestScope.labelEntidade}" alt="Excluir ${requestScope.labelEntidade}"
			src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a>
		
	</display:column>
			
	<c:set var="indexListaEntidadesSelecionadas" value="${indexListaEntidadesSelecionadas+1}" />
	
</display:table>

