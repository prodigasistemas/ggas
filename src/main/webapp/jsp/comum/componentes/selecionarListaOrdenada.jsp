
<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="br.com.ggas.util.Constantes"%>
<%@page import="br.com.ggas.util.MensagemUtil"%>
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

	function adicionarEntidade() {

		var chaveSelecionada = $("#entidadeSelecionada").val();
		if (chaveSelecionada != "-1") {
			var chavesAdicionadas = getChavesAdicionadas();
			if(chavesAdicionadas.length < ${numeroMaximoEntidade}){
				var chaveNaoAdicionada = $.inArray(chaveSelecionada, chavesAdicionadas) < 0;
				if (chaveNaoAdicionada) {
					chavesAdicionadas.push(chaveSelecionada);
					var url = montarUrlRecarregar(chavesAdicionadas);
					$("#gridSelecionarListaOrdenada").load(url);
				} else {
					alert("<%= MensagemUtil.obterMensagem(Constantes.ERRO_ENTIDADE_ADICIONADA, 
								StringUtils.lowerCase(request.getParameter("labelEntidade"))) %>");				
				}
			} else {
				alert("<%= MensagemUtil.obterMensagem(Constantes.ERRO_MAXIMO_SELECIONAR_LISTA_ORDENADA) %>");
			}
		} else {
			alert("<%= MensagemUtil.obterMensagem(Constantes.ERRO_SEM_ENTIDADE_ADICIONADA, 
							StringUtils.lowerCase(request.getParameter("labelEntidade"))) %>");
		}
	}
	
	function removerEntidade(indiceRemovido){
		var chavesAdicionadas = getChavesAdicionadas();
		removerElementoLista(chavesAdicionadas, indiceRemovido);
		var url = montarUrlRecarregar(chavesAdicionadas);
		$("#gridSelecionarListaOrdenada").load(url);
	}
	
	function alterarOrdemListaEntidadesSelecionadas(indiceInicial, indiceFinal){
		var chavesAdicionadas = getChavesAdicionadas();
		moverElementoLista(chavesAdicionadas, indiceInicial, indiceFinal);
		var url = montarUrlRecarregar(chavesAdicionadas);
		$("#gridSelecionarListaOrdenada").load(url);
	}
	
	function montarUrlRecarregar(chavesAdicionadas) {
		var data = new Date().getTime();
		return "${nomeMetodoRecarregarGrid}"
				+ "?${nomeParametroCpsAdicionadas}=" + chavesAdicionadas.join(",") 
				+ "&noCache=" + data;
	}
	
	function getChavesAdicionadas() {
		return $("[name=cpAdicionados]").map(function() {return $(this).val();}).get();
	}
	
</script>

<fieldset id="divSelecionarListaOrdenada" class="colunaEsq" style="clear: both">
	<div class="pesquisarClienteFundo">
		
		<legend><c:if test="${numeroMinimoEntidade > 0}"><span class="campoObrigatorioSimbolo">* </span></c:if>${param.labelTitulo}</legend>

		<label class="rotulo" >Tipo de ${param.labelEntidade}:</label> 
		
		<select class="campoSelect" name="entidadeSelecionada" id="entidadeSelecionada">
			<option value="-1">Selecione</option>
			<c:forEach items="${requestScope.listaEntidades}" var="entidade">
				<option value="<c:out value="${entidade.chavePrimaria}"/>"
					<c:if test="${entidadeSelecionada.chavePrimaria == entidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${entidade.descricao}" />
				</option>
			</c:forEach>
		</select>
		
		<input name="Button" id="adicionar_linha" type="button"
			class="bottonRightCol2" value="Adicionar"
			style="margin-left: 5px; height: 23px; margin-top: 2px;"
			onclick="adicionarEntidade();">

		<c:set var="labelEntidade" value="${param.labelEntidade}" scope="request" />
		<div id="gridSelecionarListaOrdenada">
			<jsp:include page="/jsp/comum/componentes/gridSelecionarListaOrdenada.jsp"></jsp:include>       
		</div>

	</div>
	
</fieldset>
