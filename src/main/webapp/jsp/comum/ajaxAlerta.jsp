<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	
		<div class="notification mensagemAlerta hideit">
			<p>
				<strong>Alerta: </strong>
				<c:out value='${msg}' />
			</p>
		</div>
		
<script type="text/javascript">
    $(document).ready(function() {
    
    
        // Closing notifications 
        // this is the class that we will target
        $(".hideit").click(function() {
        //fades the notification out	
            $(this).fadeOut(700);
        });
        
    });	
        
    </script>