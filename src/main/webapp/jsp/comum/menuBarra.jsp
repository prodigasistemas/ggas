<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/interface/AjaxService.js"/>"></script>

<script type="text/javascript" src="<c:url value="js/jquery.ui.touch-punch.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="js/facescroll.js"/>">
/***********************************************
* FaceScroll custom scrollbar (c) Dynamic Drive (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for this script and 100s more.
***********************************************/
</script>
<script type="text/javascript">
    jQuery(function(){ // on page DOM load
        $('#FaceScroll').alternateScroll();
        $('#demo2').alternateScroll({ 'vertical-bar-class': 'styled-v-bar', 'hide-bars': false });             
    });
</script>


<!-- GGAS 2.0 - MENU -->
<script type="text/javascript">
        $(document).ready(function () {
            var nav = $('.nav-container-menu');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 0) {
                    nav.addClass("f-menu");
                } else {
                    nav.removeClass("f-menu");
                }
            });
            $("#accordion").ctAccordion();
            $('#searchInput').fastLiveFilter('#accordion');

            //create accordion
            $("#searchInput").keyup(function() {

                 if (!this.value) {
                     var list = jQuery('#accordion');
                    $('a', list).removeAttr('style');
            }

});

            //search clicked - lets find something!
            $("#button").click(function () {
                //search value
                $('#accordion').ctAccordion('search', $('#searchInput').val());
                return false;
            });
            
            $('[name="imgEstrela"]').click(function() {
                        return false;   
                    });
            
            //remove itens vazios do menu
            //se o menu está vazio é porque o usuário não tem permissão
            $($(".menuContainer").get().reverse()).each(function(index, value) {
                
                if ($(this).find("ul > li").length == 0){ 
                    $(this).remove();
                }
            });            
     
       });
        
            
        
        function adicionarFavorito(chavePrimariaMenu) {
            var retorno = AjaxService.adicionarFavorito(chavePrimariaMenu, {
                callback: function(retorno) {
                if(retorno!=null) {
                   // alert(retorno);
                }
                }
                , async: false});
            location.reload();



        }
        
    </script>
<!-- GGAS 2.0 - MENU -->

<!-- FIM: Menu Barra -->
<!--<div class="nav-container-menu">-->
    <div class="ggas_search">
        <input type="text" id="searchInput" class="searchInput" name="search" value="Pesquise um item no menu..." onFocus="javascript:this.value=''"onBlur="javascript:this.value='Pesquise um item no menu...'" style="background-color:#fff; border:1px solid #d7ceaf; height:22px; width:98%; font-size:12px; color:#757575; font-style:italic;">
    </div>
    <!--</div>-->
<div id="sideLeft">

<div id="FaceScroll">
    <!--ATENÇÃO &nbsp;&nbsp; NECESSÁRIO PRA IDENTAÇÃO-->
    <ul id="accordion" class="skin-classic ctAccordion">
        <c:forEach items="${modulosMenu}" var="modulo">
            <li class="menuContainer"><a href="#" class="head">${modulo.descricao}</a>
                <ul id="${modulo.chavePrimaria}" style="display: none;">
                    <c:forEach items="${modulo.listaMenu}" var="moduloNivel1">
                        <c:if test="${not empty moduloNivel1.listaMenu}" >
                        <li id="${moduloNivel1.descricao}" class="menuContainer closed" >
                            <c:if test="${moduloNivel1.url ne null}" >
                            <a href="<c:url value="${moduloNivel1.url}" />" class="head">${moduloNivel1.descricao}</a>
                            </c:if>
                            <c:if test="${moduloNivel1.url eq null}" >
                            <a href="<c:url value="#" />" class="head">${moduloNivel1.descricao}</a>
                            </c:if>
                            <c:if test="${not empty moduloNivel1.listaMenu}" >
                                <ul id="${moduloNivel1.chavePrimaria}-ul" style="display: none;">
                                    <c:forEach items="${moduloNivel1.listaMenu}" var="moduloNivel2">
                                    <!-- indicadorMenuItem serve para indicar que o menu é um menu que vai disparar uma página -->
	                                <!-- indicadorExibirMenu serve para indicar se o menu vai ser exibido ou não de acordo com o controle de acesso -->
	                                <!-- dúvidas visualizar classe MenuImpl -->                                                                           
                                        <c:if test="${moduloNivel2.url ne null && moduloNivel2.indicadorMostrarMenu == true}">
	                                        <li class="closed" id="${moduloNivel2.chavePrimaria}" >
	                                        <c:if test="${moduloNivel2.url ne null}">
	                                            <a href="<c:url value="${moduloNivel2.url}"/>">${moduloNivel2.descricao}
	                                            <img src="images/ggas_enter.png" style="float:left; margin-right:10px;" /><!-- ICON PARA INDICATIVO DE NIVEL -->
	                                            <img name="imgEstrela" id="${moduloNivel2.chavePrimaria}-img" onclick="javascript:adicionarFavorito(${moduloNivel2.chavePrimaria})" style="float:right" src="<c:if test="${moduloNivel2.favorito}">images/ico_fav.png</c:if><c:if test="${not moduloNivel2.favorito}">images/ico_fav_off.png</c:if>">
	                                            </a>
	                                        </c:if> 
	                                        <c:if test="${moduloNivel2.url eq null}">
	                                            <a href="#">${moduloNivel2.descricao}
	                                            <img src="images/ggas_enter.png" style="float:left; margin-right:10px;" /><!-- ICON PARA INDICATIVO DE NIVEL -->
	                                            <img name="imgEstrela" id="${moduloNivel2.chavePrimaria}-img" onclick="javascript:adicionarFavorito(${moduloNivel2.chavePrimaria})" style="float:right" src="<c:if test="${moduloNivel2.favorito}">images/ico_fav.png</c:if><c:if test="${not moduloNivel2.favorito}">images/ico_fav_off.png</c:if>">
	                                            </a>
	                                        </c:if>
	                                        </li>
	                                	</c:if>
                                    </c:forEach>
                                </ul>
                            </c:if>
                            </li>
                            </c:if>
                            <c:if test="${empty moduloNivel1.listaMenu}">
                            <!-- indicadorMenuItem serve para indicar que o menu é um menu que vai disparar uma página -->
	                        <!-- indicadorExibirMenu serve para indicar se o menu vai ser exibido ou não de acordo com o controle de acesso -->
	                        <!-- dúvidas visualizar classe MenuImpl -->                                                                                   
                            	<c:if test="${moduloNivel1.url ne null && moduloNivel1.indicadorMostrarMenu == true}">
	                                <li id="${moduloNivel1.chavePrimaria}" class="closed link">
	                                    <!-- implementa o novo estilo para o segundo nivel e habilita para virar favorito -->
	                                    <!-- indicadorMenuItem serve para indicar que o menu é um menu que vai disparar uma página -->
	                                    <!-- indicadorExibirMenu serve para indicar se o menu vai ser exibido ou não de acordo com o controle de acesso -->
	                                    <!-- dúvidas visualizar classe MenuImpl -->                                                                                   
	                                    <c:if test="${moduloNivel1.url ne null && moduloNivel1.indicadorMostrarMenu == true}" >
	                                        <a href="<c:url value="${moduloNivel1.url}"/>">${moduloNivel1.descricao}
	                                            <img src="images/ggas_enter.png" style="float:left; margin-right:10px;" /><!-- ICON PARA INDICATIVO DE NIVEL -->
	                                            <img name="imgEstrela" id="${moduloNivel1.chavePrimaria}-img" onclick="javascript:adicionarFavorito(${moduloNivel1.chavePrimaria})" style="float:right" src="<c:if test="${moduloNivel1.favorito}">images/ico_fav.png</c:if><c:if test="${not moduloNivel1.favorito}">images/ico_fav_off.png</c:if>">
	                                        </a>
	                                    </c:if>
	                                </li>
	                            </c:if>
                            </c:if>
                    </c:forEach>
                </ul></li>
        </c:forEach>
    </ul>
</div>
</div>
<!--MENU-->
<!-- #sideLeft -->