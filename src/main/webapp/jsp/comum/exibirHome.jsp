<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dataAtual" class="java.util.Date" />
<jsp:useBean id="controladorAlcada" class="br.com.ggas.controleacesso.impl.ControladorAlcadaImpl" />
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<!-- --------- Alertify ----------  -->

<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/alertify.min.css"/>" />
<script type="text/javascript" src="<c:url value="/js/alertify.min.js"/>"></script>

<c:if test="${not empty mensagemTela}">
	<script>
		alertify.set('notifier', 'position', 'top-right');
		alertify.warning('<h1 class="mensagemErro">${mensagemTela}</h1>');
	</script>
</c:if>

<!-- ----------------------------- -->
<div class="ggas_home_geral">
<div class="ggas_home_esq"> <span class="ggas_home_title">Bem Vindo ao GGAS:</span> <br />
    <br />
    
    <span class="ggas_home_txt">Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s<br />
    Data Atual: <span id="boasVindasData"><fmt:formatDate value="${dataAtual}" type="both" pattern="dd/MM/yyyy" /></span><br />
    Usu�rio: <span id="boasVindasUsu�rio"><c:out value="${sessionScope.usuarioLogado.login}"/></span><br />
    Papel: <span id="boasVindasGrupo">
    <c:forEach items="${sessionScope.usuarioLogado.papeis}" var="papel">
   		<c:out value="${papel.descricao}"/>&nbsp
	</c:forEach>
   </span><br />
    Data do �ltimo Acesso: <span id="boasVindasUltimoAcesso"><fmt:formatDate value="${sessionScope.usuarioLogado.ultimoAcesso}" type="both" pattern="dd/MM/yyyy HH:mm:ss" /></span><br /><br />
    Servidor: <span id="boasVindasUsu�rio">${pageContext.request.serverName} (${pageContext.request.localAddr})</span><br/>
    ID Sess�o: <span id="boasVindasUsu�rio">${pageContext.request.session.id}</span><br/>
    Build: <fmt:bundle basename="constantes">[v.<fmt:message key="VERSAO_BUILD" />] [<fmt:message key="VERSAO_DATA" />]</fmt:bundle>
    </span>
</div>

  <div class="ggas_home_dir">
    <div id="links">
          <img src="imagens/icon_pendencias.png" width="19" height="19" alt="Pendencias">
           <span class="ggas_home_list_title">Autoriza��es pendentes</span>
      <div style="padding-top:20px;">
		 <div id="conteinerPendencias"></div>
      </div>
    </div>
  </div>
</div>