
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>


	function carregarConstantes() {
		submeter('constanteSistemaForm', 'carregarConstantes');
	}

	function salvar(){
		if(confirm('Confirma as alterações?')){
			submeter('constanteSistemaForm', 'salvarConstanteSistema');
		}
	}

	function cancelar() {
		window.location = "exibirConstanteSistema";
	}

	function init() {

	}


    function consultarValores(obj, idConstante) {

        AjaxService.consultarValores(obj.value,

            function (valores) {
                console.log('valores', valores);
                var $dropdown = $("#valorConstante"+idConstante);
                $dropdown.empty();
                $dropdown.append($("<option />").val('-1').text('Selecione...'));
                $.each(valores, function() {
                    $dropdown.append($("<option />").val(this.id).text(this.descricao));
                });
            }
        );
    }
	
	addLoadEvent(init);

</script>

<h1 class="tituloInterno">Constantes do Sistema<a class="linkHelp" href="<help:help>/controledosparmetros.htm</help:help>" target="right"
                                                  onclick="exibirJDialog('#janelaHelp');"></a></h1>

<p class="orientacaoInicial">
    Selecione uma tabela para exibir as constantes asssociadas.<br/>
</p>

<form id="constanteSistemaForm" name="constanteSistemaForm" action="exibirConstanteSistema" method="post">

	<label class="rotulo formParametrizacaoPosition">Tabela:</label>
	<select name="tabela" id="tabela" class="campoSelect campoSelectPosition" style="min-width:662px;">
		<option value="-1">Selecione...</option>
		<c:forEach items="${listaTabelas}" var="tabela">
			<option onFocus="this.style.height='auto'" value="<c:out value="${tabela.chavePrimaria}"/>" <c:if test="${constanteSistemaForm.chavePrimaria == tabela.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${tabela.descricao}"/>
			</option>
		</c:forEach>
	</select>
	<input name="Button" class="bottonRightCol botaoCampo1 leftBtm" value="Ok" type="button" onclick="carregarConstantes();">
	<br /><br />
	
	<c:if test="${not empty listaValoresConstanteSistemaVO}">

        <h1 class="tituloInterno">${tabelaSelecionada}</h1>

        <display:table class="dataTableGGAS" name="listaValoresConstanteSistemaVO" sort="list" id="constanteVO"
                       decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">

            <display:column title="Constante" style="text-align:left; padding-left:2px; width: 50%;">
                <input type="hidden" id="chaveConstante" name="chaveConstante" value="${constanteVO.constanteSistema.chavePrimaria}"/>
                <c:out value="${constanteVO.constanteSistema.nome}"/>
            </display:column>


            <c:if test="${not empty constanteVO.listaPreValores}">
                <display:column title="Tipo de assunto" style="text-align:left; padding-left:2px; width: 20%;">
                    <select id="valorPreConstante" class="campoSelect" onchange="consultarValores(this, ${constanteVO.constanteSistema.chavePrimaria})">
                        <option value="-1">Selecione...</option>
                        <c:forEach items="${constanteVO.listaPreValores}" var="valor">
                            <option value="<c:out value="${valor.chavePrimaria}"/>"
                                    <c:if test="${valor.chavePrimaria == constanteVO.valorPre}">selected="selected"</c:if>>
                                <c:out value="${valor.descricao}"/>
                            </option>
                        </c:forEach>
                    </select>
                </display:column>
            </c:if>

            <display:column title="Valor" style="text-align:left; padding-left:2px;">

                <c:if test="${not empty constanteVO.listaValores}">
                    <select name="valorConstante" id="valorConstante${constanteVO.constanteSistema.chavePrimaria}" class="campoSelect">
                        <option value="-1">Selecione...</option>
                        <c:forEach items="${constanteVO.listaValores}" var="valor">
                            <option value="<c:out value="${valor.chavePrimaria}"/>"
                                    <c:if test="${valor.chavePrimaria == constanteVO.constanteSistema.valor}">selected="selected"</c:if>>
                                <c:out value="${valor.descricao}"/>
                            </option>
                        </c:forEach>
                    </select>
                </c:if>
                <c:if test="${empty constanteVO.listaValores}">
                    <select name="valorConstante" id="valorConstante" class="campoSelect">
                        <option value="-1">Selecione...</option>
                    </select>
                </c:if>
            </display:column>

        </display:table>

        <fieldset class="conteinerBotoes">
            <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Cancelar" type="button" onclick="cancelar();">
            <vacess:vacess param="alterarParametros.do">
                <input name="Button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="salvar();">
            </vacess:vacess>
        </fieldset>

    </c:if>

    <c:if test="${not empty listaConstanteSistema}">

		<h1 class="tituloInterno">${tabelaSelecionada}</h1>
	
		<display:table class="dataTableGGAS" name="listaConstanteSistema" sort="list" id="constante" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
	
	        <display:column title="Constante" style="text-align:left; padding-left:2px; width: 50%;">
	        	<input type="hidden" id="chaveConstante" name="chaveConstante" value="${constante.chavePrimaria}"/>
	       		<c:out value="${constante.nome}"/>
	        </display:column>
	        
	        <display:column title="Valor" style="text-align:left; padding-left:2px; width: 50%;">
	        	<c:if test="${listaValoresConstanteSistema eq null || empty listaValoresConstanteSistema}">
					<input type="text" class="campoTexto" id="valorConstante" name="valorConstante" size="50" maxlength="255" value="${constante.valor}" />
				</c:if>
				<c:if test="${not empty listaValoresConstanteSistema}">
					<select name="valorConstante" id="valorConstante" class="campoSelect">
						<option value="-1">Selecione...</option>
						<c:forEach items="${listaValoresConstanteSistema}" var="valor">
							<option value="<c:out value="${valor.chavePrimaria}"/>" <c:if test="${valor.chavePrimaria == constante.valor}">selected="selected"</c:if>>
								<c:out value="${valor.descricao}"/>
							</option>
						</c:forEach>
					</select>
				</c:if>
	        </display:column>
	        		
	    </display:table>
	    
		<fieldset class="conteinerBotoes">
			<input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Cancelar" type="button" onclick="cancelar();">
			<vacess:vacess param="alterarConstanteSistema">
				<input name="Button" class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="salvar();">
			</vacess:vacess>
		</fieldset>
	
	</c:if>

</form>
