<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Conta Cont�bil<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<p class="orientacaoInicial">
    Para Alterar uma Conta Cont�bil clique em <span class="destaqueOrientacaoInicial">Alterar</span>.<br/>
    Para voltar para tela de Pesquisa clique em <span class="destaqueOrientacaoInicial">Voltar</span>.<br />
</p>

<script type="text/javascript">

function alterar(chavePrimaria) {
	document.forms[0].chavePrimaria.value = chavePrimaria;
	submeter('contaContabilForm', 'exibirAlterarContaContabil');	
}

function voltar(){
	submeter('contaContabilForm', 'pesquisarContaContabil');
	//location.href = '<c:url value="exibirPesquisaContaContabil"/>';
}	

</script>

<form:form method="post" action="exibirAlterarContaContabil" name="contaContabilForm" id="contaContabilForm">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${contaContabil.chavePrimaria}">
	
<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1">

			<label class="rotulo" id="rotuloConta" for="numeroConta">N�mero:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:out value="${contaContabil.numeroConta}"/></span>

			<label class="rotulo" id="rotuloNome" for="nomeConta">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:out value="${contaContabil.nomeConta}"/></span>
				
			<label class="rotulo ">Indicador de Uso:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:choose>
					<c:when test="${contaContabil.habilitado == true}">
						<c:out value=" Ativo"/>
					</c:when>
					<c:otherwise>
						<c:out value=" Inativo"/>
					</c:otherwise>
				</c:choose>
			</span><br/>			
	</fieldset>
	<fieldset class="conteinerBotoes"> 
		<input name="ButtonVoltar" class="bottonRightCol" value="Voltar" type="button" onClick="voltar();">
	    <%--<vacess:vacess param="exibirAlterarContaContabil">--%>
	    	<input name="chavePrimaria" class="bottonRightCol2 botaoGrande1" value="Alterar" id="botaoAlterar" type="button" onClick="alterar('<c:out value="${contaContabil.chavePrimaria}"/>');">
	    <%--</vacess:vacess>--%>
	</fieldset>
</fieldset>
	
</form:form> 