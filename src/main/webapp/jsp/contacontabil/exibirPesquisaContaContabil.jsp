<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Conta Cont�bil<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<p class="orientacaoInicial">
    Para pesquisar um registro espec�fico, informe o(s) dado(s) no(s) campo(s) abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>
    ou clique apenas em  <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. Para incluir um novo registro clique em
    <span class="destaqueOrientacaoInicial">Incluir</span>.<br />
</p>

<form:form method="post" action="pesquisarContaContabil" name="materContaContabilForm" modelAttribute="EntidadeNegocioImpl">

<script type="text/javascript">

	$().ready(function(){
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
		$("input#nomeConta").keyup(removeExtraManterEspaco).blur(removeExtraManterEspaco);
		formatarNumeroContaContabil();		
		$("input#numeroConta").inputmask(document.getElementById('mascaraNumeroConta').value,{placeholder:"_"});
	});
	
	function formatarNumeroContaContabil(){
		
		var mask = '${mascaraNumeroConta}';
		var mascaraFormatada = '';
		for (i = 0; i < mask.length; i++) {
			if(mask.charAt(i) == '.') {
				mascaraFormatada += ".";
			} else if(mask.charAt(i) == '-') {
				mascaraFormatada += "-";
			} else if(mask.charAt(i) == '*') {
				mascaraFormatada += "*";
			} else {
				mascaraFormatada += "9";
			}
		}
		document.getElementById('mascaraNumeroConta').value = mascaraFormatada;
	}
	
	function alterarContaContabil(){	
		var verficaSelecao = verificarSelecaoApenasUm();
		if (verficaSelecao == true) {
			document.getElementById('habilitado').value = document.forms[0].habilitado.value;
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();			
			submeter('materContaContabilForm', 'exibirAlterarContaContabil');
		}
	}
	
	function removerContaContabil(){
		var selecao = verificarSelecao();		
		
		if (selecao == true) {	
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('materContaContabilForm', 'removerContaContabil');
			}
	    }
	}
	
	function incluirContaContabil(){
		
		location.href = '<c:url value="exibirInclusaoContaContabil"/>';	
	}
	
	function detalharContaContabil(chavePrimaria){
		
		document.forms[0].chavePrimaria.value = chavePrimaria;		
		submeter('materContaContabilForm', 'exibirDetalharContaContabil');	
	}
	
	function limparFormulario(){
		limparFormularios(materContaContabilForm);
		document.forms[0].habilitado[0].checked = true;
	}
	

</script>

		
	<input type="hidden" name="chavePrimaria" id="chavePrimaria"/>
	<input type="hidden" name="chavePrimarias" id="chavePrimarias"/>
	<input type="hidden" name="mascaraNumeroConta" id="mascaraNumeroConta" value="${mascaraNumeroConta}"/>	
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1" class="coluna">
			
			<label class="rotulo" id="rotuloNumero" for="numeroConta">N�mero:</label>
			<input class="campoTexto" type="text" id="numeroConta" name="numeroConta" maxlength="20" size="20" 
				value="${contaContabil.numeroConta}" />
			<br />
			<label class="rotulo" id="rotuloNome" for="nomeConta">Descri��o:</label>
			<input class="campoTexto" type="text" id="nomeConta" name="nomeConta" maxlength="40" size="40" onkeyup="toUpperCase(this);"
				value="${contaContabil.nomeConta}"/>			
		</fieldset>		
										
		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
		<%-- <vacess:vacess param="exibirPesquisaContaContabil">--%>
			<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
					value="Pesquisar" type="submit">
		<%-- </vacess:vacess>--%>
		<input name="Button" class="bottonRightCol2 " id="Limpar" 
			value="Limpar" type="button" onclick="limparFormulario();">						
		</fieldset>
			
	</fieldset>
		
		<c:if test="${listaContaContabil ne null}">
			<hr class="linhaSeparadoraPesquisa" />
			<display:table class="dataTableGGAS" name="listaContaContabil"
				sort="list" id="contaContabil"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="pesquisarContaContabil">
				<display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
     				<input type="checkbox" name="chavesPrimarias" value="${contaContabil.chavePrimaria}">
			</display:column>
			<display:column title="Ativo" style="width: 30px">
					<c:choose>
						<c:when test="${contaContabil.habilitado == true}">
							<img alt="Ativo" title="Ativo"
								src="<c:url value="/imagens/success_icon.png"/>" border="0">
						</c:when>
						<c:otherwise>
							<img alt="Inativo" title="Inativo"
								src="<c:url value="/imagens/cancel16.png"/>" border="0">
						</c:otherwise>
					</c:choose>
			</display:column>
				<display:column style="text-align: center;" title="N�mero Conta"
						sortable="true" sortProperty="numeroConta" > 
						<a href='javascript:detalharContaContabil("<c:out value='${contaContabil.chavePrimaria}'/>");'><span class="linkInvisivel"></span>
				<c:out value='${contaContabil.numeroConta}'/></a>
			</display:column>
			<display:column style="text-align: center;" title="Descri��o Conta"
					sortable="true" sortProperty="nomeConta" >
					<a href='javascript:detalharContaContabil("<c:out value='${contaContabil.chavePrimaria}'/>");'><span class="linkInvisivel"></span>
				<c:out value='${contaContabil.nomeConta}' /></a>
			</display:column>
		</display:table>
	</c:if>	

<c:if test="${empty listaContaContabil}">
	<fieldset class="conteinerBotoes">		
		<%-- <vacess:vacess param="exibirInclusaoContaContabil">--%>
			<input name="button" value="Incluir" id="botaoIncluir" class="bottonRightCol2 botaoGrande1" onclick="incluirContaContabil()" type="button">		
		<%--</vacess:vacess>--%>				
	</fieldset>
</c:if>	
<c:if test="${not empty listaContaContabil}">
	<fieldset class="conteinerBotoes">
		<%-- <vacess:vacess param="exibirAlterarContaContabil">--%>
			<input name="buttonAlterar" value="Alterar" id="botaoAlterar" class="bottonRightCol2 botaoAlterar" onclick="alterarContaContabil()" type="button">
		<%-- </vacess:vacess>--%>
		<%-- <vacess:vacess param="removerContaContabil">--%>
			<input name="buttonRemover" value="Remover" id="botaoRemover" class="bottonRightCol2 botaoRemover" id="Remover" onclick="removerContaContabil()" type="button">
		<%-- </vacess:vacess>--%>
		<%-- <vacess:vacess param="exibirInclusaoContaContabil">--%>		
			<input name="button" value="Incluir" id="botaoIncluir" class="bottonRightCol2 botaoGrande1" onclick="incluirContaContabil()" type="button">		
		<%-- </vacess:vacess>--%>				
	</fieldset>
</c:if>

</form:form> 