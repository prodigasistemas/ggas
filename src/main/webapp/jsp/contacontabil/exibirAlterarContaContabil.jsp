<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Conta Cont�bil<a class="linkHelp" href="<help:help>/consultadoslanamentoscontbeis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<p class="orientacaoInicial">
    Para Alterar uma Conta Cont�bil, modifique no(s) campo(s) desejado(s) e
    clique em <span class="destaqueOrientacaoInicial">Salvar</span>.
    Para cancelar clique em <span class="destaqueOrientacaoInicial">Cancelar</span>.
</p>

<script type="text/javascript">

$().ready(function(){
	$("input#nomeConta").keyup(removeExtraManterEspaco).blur(removeExtraManterEspaco);	
	document.getElementById('mascaraNumeroConta').value = formatarMascarasJSP('${mascaraNumeroConta}');		
	$("input#numeroConta").inputmask(document.getElementById('mascaraNumeroConta').value,{placeholder:"_"});
});

function limparFormulario(){
    
	limparFormularios(contaContabilForm);
    document.forms[0].habilitado[0].checked = true;
}

function cancelar(){
	submeter('contaContabilForm', 'pesquisarContaContabil');
    //location.href = '<c:url value="exibirPesquisaContaContabil"/>';
}
function salvar(){	
	var mascara = document.getElementById('mascaraNumeroConta').value;		
	var numeroContaFormatado = document.getElementById('numeroConta').value;	
	var numeroConta = validarNumeroMascara(mascara, numeroContaFormatado);	

	submeter('contaContabilForm', 'salvarAlteracaoContaContabil');
}

    

</script>

<form:form method="post" action="exibirPesquisaContaContabil" id="contaContabilForm" name="contaContabilForm">
    <input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${contaContabil.chavePrimaria}">
    <input type="hidden" name="mascaraNumeroConta" id="mascaraNumeroConta" value="${mascaraNumeroConta}"/>
    
        <fieldset id="conteinerPesquisarIncluirAlterar">        
            <fieldset id="pesquisaMedicaoCol1">
            <label class="rotulo" id="rotuloNumero" for="numeroConta"><span class="campoObrigatorioSimbolo">* </span>N�mero:</label>
            <input class="campoTexto" type="text" id="numeroConta" name="numeroConta" size="22" 
                <c:out value="${contaContabil.numeroConta}"/> value="${contaContabil.numeroConta}">
            <br />

            <label class="rotulo" id="rotuloNome" for="nomeConta"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
            <input class="campoTexto" type="text" id="nomeConta" name="nomeConta" maxlength="40" size="40" onkeyup="toUpperCase(this);"            
                <c:out value="${contaContabil.nomeConta}"/> value="${contaContabil.nomeConta}">
            <br />                      
        </fieldset>
        
        <fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
                                    
            <label class="rotulo" for="habilitado">Indicador de Uso:</label>
            <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${contaContabil.habilitado eq 'true'}">checked</c:if>>
            <label class="rotuloRadio" for="indicadorUso">Ativo</label>
            <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${contaContabil.habilitado eq 'false'}">checked</c:if>>
            <label class="rotuloRadio" for="indicadorUso">Inativo</label>
        </fieldset>
        
    <fieldset class="conteinerBotoes"> 
        <input name="ButtonCancelar" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
        <input name="ButtonLimpar" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
        <%--<vacess:vacess param="exibirAlterarContaContabil">--%>
            <input name="buttonSalvar" class="bottonRightCol2 botaoGrande1" value="Salvar" id="botaoSalvar" onclick="salvar();" type="button">
        <%--</vacess:vacess>--%>
    </fieldset>
</fieldset>
    
</form:form> 