<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>

<h1 class="tituloInterno">Controle de Acesso de Usu�rios<a class="linkHelp" href="<help:help>/inclusoalteraodosacessos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Selecione a Empresa, a Unidade Organizacional e o Funcion�rio. Em seguida preencha os campos restantes, monte o Perfil e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>



<script>
	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	});

	function consultarUnidadesOrganizacionais(obj) {
	     var idEmpresa = obj.options[obj.selectedIndex].value;
	     var selectUnidOrg = document.getElementById("idUnidadeOrganizacional");
	 
	     selectUnidOrg.length=0;
	     var novaOpcao = new Option("Selecione","-1");
	     selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
	     
	     if (idEmpresa != "-1") {
		     AjaxService.consultarUnidadesOrganizacionais(idEmpresa, 
		     	function(unidadeOrganizacional) {
		        	for (key in unidadeOrganizacional){
		            	var novaOpcao = new Option(unidadeOrganizacional[key], key);
		                selectUnidOrg.options[selectUnidOrg.length] = novaOpcao;
		            }
		        }
		     );
		     document.getElementById("idUnidadeOrganizacional").disabled = false;
		 }else{
			 limparCampos();
			 }            
	}

	function consultarFuncionarios(obj) {
	     var idUnidadeOrganizacional = obj.options[obj.selectedIndex].value;
	     var selectFunc = document.getElementById("funcionario");
	 
	     selectFunc.length=0;
	     var novaOpcao = new Option("Selecione","-1");
	     selectFunc.options[selectFunc.length] = novaOpcao;
	     
	     if (idUnidadeOrganizacional != "-1") {
		     AjaxService.consultarFuncionarios(idUnidadeOrganizacional, 
		     	function(funcionario) {
		        	for (key in funcionario){
		            	var novaOpcao = new Option(funcionario[key], key);
		            	selectFunc.options[selectFunc.length] = novaOpcao;
		            }
		        }
		     );
		     document.getElementById("funcionario").disabled = false;
		 }else{
			 limparCampos();
		 }              
	}

	function preencherLoginEmailFunc(obj) {
		
	     var idFuncionario = obj.options[obj.selectedIndex].value;
	     var email = document.getElementById("email");
	     var login = document.getElementById("loginFunc");
		 var usuarioDominio = document.getElementById("usuarioDominio");
	     var senhaExpirada = document.getElementById("senhaExpirada");
	     var periodicidadeSenha = document.getElementById("periodicidadeSenha");

	     if (idFuncionario != "-1") {
		     AjaxService.consultarFuncionario(idFuncionario, 
		        function(funcionario) {
		    	 if(funcionario != null){
		    		 if(funcionario["email"] != undefined && funcionario["email"][0] != null){
		    			 email.value = funcionario["email"][0];
			    		}else{
			    		 email.value = '';
				    	}
		    		  if( funcionario["login"] != undefined && funcionario["login"][0] != null){
		    			  login.value = funcionario["login"][0];
			    		}else{
			    		  login.value = '';
				    	}
					 if( funcionario["usuarioDominio"] != undefined && funcionario["usuarioDominio"][0] != null){
						 usuarioDominio.value = funcionario["usuarioDominio"][0];
					 }else{
						 usuarioDominio.value = '';
					 }
		    		  if(funcionario["periodicidadeSenha"] != undefined && funcionario["periodicidadeSenha"][0].value != null){
		    			  periodicidadeSenha.value = funcionario["periodicidadeSenha"][0];
			    		}else{
			    		  periodicidadeSenha.value = '';
				    	}
				    	
		    		  if(funcionario["senhaExpirada"] != undefined && funcionario["senhaExpirada"][0]  != null){
		    			  senhaExpirada.checked = funcionario["senhaExpirada"][0];
			    		}else{
			    			senhaExpirada.checked = null;
				    	}

	    				var selectOperacoesCadastradas = document.getElementById("papelCadastrado");
						removeChildrenFromNode(selectOperacoesCadastradas);
						
						if(funcionario["listaPapel"] != null){
							for (i=0; i < funcionario["listaPapel"].length; i++){

								var id = i;
								var aux = i;
								var novaOpcao = new Option(funcionario["listaPapel"][i], funcionario["listaPapel"][++i]);
								novaOpcao.label = funcionario["listaPapel"][aux];
								novaOpcao.id = funcionario["listaPapel"][++aux];
								selectOperacoesCadastradas.appendChild(novaOpcao);
							}
						}
	    				var selectOperacoesSelecionadas = document.getElementById("papelSelecionado");
						removeChildrenFromNode(selectOperacoesSelecionadas);

						if(funcionario["listaPapeisSelecionados"] != null){
							for (i=0; i < funcionario["listaPapeisSelecionados"].length; i++){

								var id = i;
								var aux = i;
								var novaOpcao = new Option(funcionario["listaPapeisSelecionados"][i], funcionario["listaPapeisSelecionados"][++i]);
								novaOpcao.label = funcionario["listaPapeisSelecionados"][aux];
								novaOpcao.id = funcionario["listaPapeisSelecionados"][++aux];
								selectOperacoesSelecionadas.appendChild(novaOpcao);
							}
						}
			    	}
		        }
		     );
		 }            
	}

	function removeChildrenFromNode(node) {
		if(node.hasChildNodes()) {
			while(node.childNodes.length >= 1 ) {
				node.removeChild(node.firstChild);
			}
		}
	}
	
	function salvar(){
		verificarChavePrimaria();
		var lista = document.getElementById('papelSelecionado');		
		for (i=0; i<lista.length; i++){
			lista.options[i].selected = true;
		}
		submeter('controleDeAcessoForm', 'incluirControleAcessoFunc');

	}

	function verificarCampos(){
	     var idUnidadeOrganizacional = document.getElementById("idUnidadeOrganizacional");
	     var idEmpresa = document.getElementById("idEmpresa");

	     if(idEmpresa.value != -1){
	    	 document.getElementById("idUnidadeOrganizacional").disabled = false;
		 }else{
			 document.getElementById("idUnidadeOrganizacional").disabled = true;
			 }
		 
	     if(idUnidadeOrganizacional.value != -1){
	    	 document.getElementById("funcionario").disabled = false;
		 }else{
			 document.getElementById("funcionario").disabled = true;
			 }
	}	

	function cancelar(){	
		document.forms[0].retornaTela.value = true;
		submeter("controleDeAcessoForm", "exibirPesquisaControleAcessoFunc");
	}

	function limparCampos(){
		
		 document.getElementById("idUnidadeOrganizacional").disabled = true;
		 document.getElementById("funcionario").disabled = true;
		 document.getElementById("funcionario").value = '-1';

		 document.getElementById("loginFunc").value = "";
		 document.getElementById("periodicidadeSenha").value = "";
		 document.getElementById("email").value = "";
		 document.getElementById("senhaExpirada").checked = false;
		 
	}

	function limparCamposFuncionario(){
		 var chavePrimaria = document.getElementById("chavePrimaria");

	     if(chavePrimaria.value == -1){
			document.getElementById("funcionario").disabled = true;
			document.getElementById("funcionario").value = '-1';

			document.getElementById("loginFunc").value = "";
			document.getElementById("periodicidadeSenha").value = "";
			document.getElementById("email").value = "";
			document.getElementById("senhaExpirada").checked = false;
		}
	}

	function verificarChavePrimaria(){
		
		 var idFuncionario = document.getElementById("funcionario");
		 if(idFuncionario.value == "-1"){
			 $("#chavePrimaria").val(-1);
		 }
	}
	
	function init() {
		verificarCampos();
	}

	addLoadEvent(init);
</script>

<form:form method="post" action="incluirControleAcessoFunc" enctype="multipart/form-data" name="controleDeAcessoForm">
<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}"/>
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${controleDeAcesso.funcionario.usuario.chavePrimaria}">
<input name="retornaTela" type="hidden" id="habilitado" value="${retornaTela}"/>
<input name="detalharFuncionario" type="hidden" id="detalharFuncionario" value="${detalharFuncionario}" >
<input name="alterarFuncionario" type="hidden" id="alterarFuncionario" value="${alterarFuncionario}" >
<fieldset id="conteinerControleAcessoFuncionarios" class="conteinerPesquisarIncluirAlterar">	
	<fieldset class="coluna">
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Empresa:</label>
		<select name="empresa" id="idEmpresa" class="campoSelect"  onchange="consultarUnidadesOrganizacionais(this);">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaEmpresa}" var="empresa">
				<option value="<c:out value="${empresa.chavePrimaria}"/>" <c:if test="${controleDeAcesso.empresa.chavePrimaria == empresa.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${empresa.cliente.nome}"/>
				</option>
			</c:forEach>
		</select><br />
			
		<label class="rotulo rotulo2Linhas campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Unidade<br />Organizacional:</label>
		<select name="unidadeOrganizacional" id="idUnidadeOrganizacional" class="campoSelect campo2Linhas" onchange="consultarFuncionarios(this);verificarCampos();" >
			<option value="-1">Selecione</option>
			<c:forEach items="${listaUnidadeOrganizacional}" var="unidadeOrganizacional">
				<option value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>" <c:if test="${controleDeAcesso.unidadeOrganizacional.chavePrimaria == unidadeOrganizacional.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadeOrganizacional.descricao}"/>
				</option>
			</c:forEach>
		</select><br />
			
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Funcion�rio:</label>
		<select name="funcionario" id="funcionario" class="campoSelect" onchange="preencherLoginEmailFunc(this);verificarCampos();limparCamposFuncionario();" >
			<option value="-1">Selecione</option>
			<c:forEach items="${listaFuncionarios}" var="funcionario">
				<option value="<c:out value="${funcionario.chavePrimaria}"/>" <c:if test="${controleDeAcesso.funcionario.chavePrimaria == funcionario.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${funcionario.nome}"/>
				</option>
			</c:forEach>
		</select><br />
		
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Login:</label>
		<input class="campoTexto" type="text" id="loginFunc" name="login" maxlength="80" size="30" value="${controleDeAcesso.login}" ><br />

		<label class="rotulo rotulo2Linhas">Usu�rio do Dominio:</label>
		<input class="campoTexto" type="text" id="usuarioDominio" name="usuarioDominio" maxlength="80" size="30" value="${controleDeAcesso.usuarioDominio}" style="margin-top: 10px;"><br />
		 
		<label class="rotulo rotulo2Linhas">Periodicidade de<br />troca da senha:</label>
		<input class="campoTexto campo2Linhas" type="text" id="periodicidadeSenha" name="periodicidadeSenha" onkeypress="return formatarCampoNumeroEndereco(event, this);"
			style="margin-top: 20px;" size="5" maxlength="3" value="${controleDeAcesso.periodicidadeSenha}"><br />
				
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Email:</label>
		<input class="campoTexto" type="text" id="email" name="email" maxlength="80" size="30" value="${controleDeAcesso.email}"><br />

		<label class="rotulo rotulo2Linhas" for="adminAtendimento" >Administrador do m�dulo de atendimento: </label>
		<input class="campoTexto campo2Linhas" type="checkbox" id="adminAtendimento" name="adminAtendimento" style="margin-top: 20px;"
			value="true" <c:if test="${controleDeAcesso.adminAtendimento == 'true'}">checked="checked"</c:if> ><br />
		  
		<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Bloqueado:</label>
		<input class="campoCheckbox" type="checkbox" id="senhaExpirada" name="senhaExpirada" value="true" <c:if test="${controleDeAcesso.senhaExpirada == 'true'}">checked="checked"</c:if>><br />
	</fieldset>
	
	<fieldset class="colunaFinal">
		<fieldset id="conteinerSegmentos" class="conteinerCampoList">
			<legend class="conteinerBlocoTitulo">Perfis</legend>
			<label class="rotulo rotuloVertical rotuloCampoList campoObrigatorio" for="papelCadastrado" ><span class="campoObrigatorioSimbolo">* </span>Cadastrados:</label>		
			<select id="papelCadastrado" name="papelCadastrado" class="campoList campoVertical" multiple="multiple"
				onDblClick="moveSelectedOptions(document.forms[0].papelCadastrado,document.forms[0].papelSelecionado,true)">
			   	<c:forEach items="${listaPapel}" var="servico">
					<option value="<c:out value="${servico.chavePrimaria}"/>"> <c:out value="${servico.descricao}"/></option>		
			    </c:forEach>
		  	</select>
		  	<fieldset class="conteinerBotoesCampoList1a">
				<input id="botaoMoverDireita" type="button" name="right" value="&gt;&gt;" class="bottonRightCol" 
			   		onClick="moveSelectedOptions(document.forms[0].papelCadastrado,document.forms[0].papelSelecionado,true)">
			  	<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7"
			   		onClick="moveAllOptions(document.forms[0].papelCadastrado,document.forms[0].papelSelecionado,true)">
			  	<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
			   		onClick="moveSelectedOptions(document.forms[0].papelSelecionado,document.forms[0].papelCadastrado,true)">
			  	<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" 
			   		onClick="moveAllOptions(document.forms[0].papelSelecionado,document.forms[0].papelCadastrado,true)">
			</fieldset>
			<label class="rotulo rotuloVertical rotuloCampoList2" for="papelSelecionado" >Selecionados:</label><br />
		  	<select id="papelSelecionado" name="papelSelecionado" multiple="multiple" class="campoList campoList2 campoVertical"
		   		onDblClick="moveSelectedOptions(document.forms[0].papelSelecionado,document.forms[0].papelCadastrado,true)">
			   	<c:forEach items="${listaPapeisSelecionados}" var="servico">
					<option value="<c:out value="${servico.chavePrimaria}"/>"> <c:out value="${servico.descricao}"/></option>		
			    </c:forEach>
		  	</select>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para inclus�o de Empresas.</p>
</fieldset>

<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
    <vacess:vacess param="exibirInclusaoControleAcessoFunc">
    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button"  onclick="salvar();">
    </vacess:vacess>
</fieldset>
</fieldset>
</form:form>
