<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>

<h1 class="tituloInterno">Detalhamento de Controle de Acesso<a class="linkHelp" href="<help:help>/detalhamentodeusurio.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>



<script>
	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

	});

	
	function alterar(){
		document.forms[0].detalharFuncionario.value = false;
		document.forms[0].alterarFuncionario.value = true;
		submeter("controleDeAcessoForm", "exibirInclusaoControleAcessoFunc");
	}

	function gerar(){
		submeter("controleDeAcessoForm", "gerarRelatorio");
	}

	function cancelar(){	
		document.forms[0].retornaTela.value = true;
		submeter("controleDeAcessoForm", "exibirPesquisaControleAcessoFunc");
	}

</script>

<form:form method="post" action="incluirControleAcessoFunc" enctype="multipart/form-data" name="controleDeAcessoForm">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${controleDeAcesso.funcionario.usuario.chavePrimaria}">
<input name="idPapel" type="hidden" id="idPapel" value="${controleDeAcesso}"/>
<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}"/>
<input name="descricao" type="hidden" id="habilitado" value="${controleDeAcesso}"/>
<input name="retornaTela" type="hidden" id="habilitado" value="${retornaTela}"/>
<input name="detalharFuncionario" type="hidden" id="detalharFuncionario" >
<input name="alterarFuncionario" type="hidden" id="alterarFuncionario" >

<fieldset id="conteinerControleAcessoFuncionarios" class="conteinerPesquisarIncluirAlterar">	
	<fieldset class="coluna">
	
		<label class="rotulo">Empresa: </label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${controleDeAcesso.empresa.cliente.nome}"/></span><br />
	
		<label class="rotulo rotulo2Linhas">Unidade<br /> Organizacional: </label>
		<span class="itemDetalhamento2Linhas itemDetalhamentoLargo"><c:out value="${controleDeAcesso.unidadeOrganizacional.descricao}"/></span><br />
	
		<label class="rotulo">Nome:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${controleDeAcesso.funcionario.nome}"/></span><br />
	    
		<label class="rotulo">Login:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${controleDeAcesso.login}"/></span><br />

		<label class="rotulo rotulo2Linhas">Usu�rio do Dominio:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo" style="margin-top: 5px"><c:out value="${controleDeAcesso.usuarioDominio}"/></span><br />
	    
		<label class="rotulo">E-Mail:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${controleDeAcesso.email}"/></span><br />
		
		<label class="rotulo rotulo2Linhas">Periodicidade de<br />troca da senha:</label>
		<span class="itemDetalhamento2Linhas itemDetalhamentoLargo"><c:out value="${controleDeAcesso.periodicidadeSenha}"/></span><br />
		
		<label class="rotulo rotulo2Linhas">Administrador do m�dulo de atendimento: </label>
		<c:choose>
		<c:when test="${controleDeAcesso.adminAtendimento == 'true'}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="Sim"/></span><br />
		</c:when>
		<c:otherwise>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="N�o"/></span><br />
		</c:otherwise>	
		</c:choose>
		<label class="rotulo">Bloqueado:</label>
		<c:choose>
		<c:when test="${controleDeAcesso.senhaExpirada == 'true'}">
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="Sim"/></span><br />
		</c:when>
		<c:otherwise>
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="N�o"/></span><br />
		</c:otherwise>	
		</c:choose>
		
	</fieldset>
	
	<fieldset class="colunaFinal">
		<fieldset id="conteinerSegmentos" class="conteinerCampoList">
			<legend class="conteinerBlocoTitulo">Perfis</legend>
			<label class="rotulo rotuloVertical rotuloCampoList campoObrigatorio" for="papelCadastrado" >Selecionados:</label><br />		
			   	<c:forEach items="${listaPapeisSelecionados}" var="servico">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${servico.descricao}"/></span><br />
			    </c:forEach>
		  	
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
   	<fieldset class="conteinerBotoesDirFixo">
   	<vacess:vacessmenu param="exibirPesquisaControleAcessoFunc">
   		<input name="Button" class="bottonRightCol2 botaoGerarRelatorio" value="Gerar Relat�rio" type="button" onclick="gerar();">
    </vacess:vacessmenu>	
    <vacess:vacessmenu param="exibirInclusaoControleAcessoFunc">
    	<input name="Button" class="bottonRightCol2 bottonRightColUltimo botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacessmenu>
    </fieldset> 
</fieldset>
</fieldset>
</form:form>
