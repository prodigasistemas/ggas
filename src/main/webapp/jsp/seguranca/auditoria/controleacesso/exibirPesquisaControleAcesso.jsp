<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Controle de Acesso<a class="linkHelp" href="<help:help>/consultadosusurios.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para restringir a pesquisa de usu�rios a um Nome e/ou Perfil espec�fico, preencha os respectivos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>. Para exibir todos os usu�rios cadastrados apenas clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<form:form method="post" action="pesquisarUsuario" name="controleDeAcessoForm">

	<script language="javascript">
	
		function limparFormulario() {		
			document.forms[0].descricao.value = "";
			document.forms[0].habilitado[0].checked = true;		
			document.forms[0].idPerfil.value = -1;
		}	
			
		
		function alterarUsuario() {
			document.forms[0].detalharFuncionario.value = false;
			document.forms[0].alterarFuncionario.value = true;
			alterarFuncionario
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("controleDeAcessoForm", "exibirInclusaoControleAcessoFunc");
		    }
		}
		
		function excluir(){	
			var selecao = verificarSelecaoSemMensagem();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('controleDeAcessoForm', 'excluirControleAcessoFunc');
				}
		    } else {
		    	alert ("Selecione um ou mais registros para realizar a opera��o!");
		    }
		}
		
		function detalharFuncionario(chave){
			document.forms[0].chavePrimaria.value = chave;
			document.forms[0].detalharFuncionario.value = true;
			document.forms[0].alterarFuncionario.value = false;
			
			submeter("controleDeAcessoForm", "exibirInclusaoControleAcessoFunc");
		}	
			

		function pesquisarUsuario(){	
			document.forms[0].idPapel.value = document.forms[0].idPerfil.value;	
			submeter("controleDeAcessoForm", "pesquisarUsuario");
		}

		function incluirUsuario(){	
			document.forms[0].chavePrimaria.value = null;
			document.forms[0].detalharFuncionario.value = false;
			document.forms[0].alterarFuncionario.value = false;
			submeter("controleDeAcessoForm", "exibirInclusaoControleAcessoFunc");
		}

	</script>
	
	<input name="detalharFuncionario" type="hidden" id="detalharFuncionario" >
	<input name="alterarFuncionario" type="hidden" id="alterarFuncionario" >
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="excluirFuncionario" type="hidden" id="excluirFuncionario" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idPapel" type="hidden" id="idPapel" value="${idPerfil}"/>
	<input name="acao" type="hidden" id="acao" value="pesquisarUsuario">
	
	<fieldset id="conteinerPesquisarPerfil" class="conteinerPesquisarIncluirAlterar">
		
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
		<label class="rotulo" id="rotuloNome" for="descricao" >Nome:</label>
		<input class="campoTexto campoHorizontal" id="descricao" type="text" name="descricao" maxlength="30" size="45" tabindex="3" value="${descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		
		<label class="rotulo rotuloVertical rotuloCampoList campoObrigatorio">Perfil:</label><br />		
			<select name="idPerfil" id="idPerfil" class="campoSelect" >
			<option value="-1">Selecione</option>
			<c:forEach items="${listaPapel}" var="perfil">
				<option value="<c:out value="${perfil.chavePrimaria}"/>" <c:if test="${idPerfil == perfil.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${perfil.descricao}"/>
				</option>
			</c:forEach>
		  	</select><br />
		  	
		  </fieldset>
		  
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="colunaFinal">
		
		<label class="rotulo rotuloHorizontal" id="rotuloIndicadorUso" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Todos</label><br /><br />
		
		 </fieldset>
		
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarUsuario();">
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaUsuarios ne null && not empty listaUsuarios}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="listaUsuarios" sort="list" id="usuario" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarUsuario">
	        <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${usuario.chavePrimaria}">
	     	</display:column>
	        <display:column title="Status" style="width: 30px">
		     	<c:choose>
					<c:when test="${usuario.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
			<display:column sortable="true" sortProperty="login" title="Login" headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href="javascript:detalharFuncionario(<c:out value='${usuario.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${usuario.login}"/>
	            </a>
	        </display:column>
	        <display:column sortable="false" sortProperty="descricao" title="Nome" headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href="javascript:detalharFuncionario(<c:out value='${usuario.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${usuario.funcionario.nome}"/>
	            </a>
	        </display:column>
	        <display:column sortable="false" sortProperty="descricao" title="Email" headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href="javascript:detalharFuncionario(<c:out value='${usuario.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${usuario.funcionario.email}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
     <vacess:vacessmenu param="exibirInclusaoControleAcessoFunc">
    	<c:if test="${not empty listaUsuarios}">
				<input id="botaoAlterar" name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarUsuario();" type="button">
				<input id="botaoRemover" type="button" name="buttonRemover" value="Remover" class="bottonRightCol" onclick="excluir();">
		</c:if>
			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluirUsuario();" type="button">
	 </vacess:vacessmenu>
	</fieldset>
	    
</form:form>



	