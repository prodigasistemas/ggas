<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Alterar Perfil<a class="linkHelp" href="<help:help>/inclusoalteraodosperfis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Utilize os campos abaixo para alterar os dados b�sicos do Perfil.</p>

<link rel="stylesheet" media="all" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/jquery.treeview.css" />

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.treeview.js"></script>

<script>
	var listaOperacoes = null;

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaPapel"/>';
	}

	function alterar(){
		salvarEstadoArvore();
		//document.forms[0].postBack.value = false;
		submeter("controlePerfilAcessoForm", "atualizarPapel");
	}

	function limparFuncionalidades() {
		var selectFuncionalidades = document.getElementById("idFuncionalidades");


	  	selectFuncionalidades.length = 0;
	  	while (selectFuncionalidades.firstChild) {
	  		selectFuncionalidades.removeChild(selectFuncionalidades.firstChild);
		}

		return selectFuncionalidades;
	}

	function carregarFuncionalidadesCadastradas(idModulo) {	
		var selectFuncionalidades = limparFuncionalidades();
		limparFuncionalidades();
	   	AjaxService.listarFuncionalidadesPorModulos(idModulo, 
	       	function(pontos) {            		      		         		
				var optGroup = null;
           		for (key in pontos){
					var id = key;

					if (pontos[key][1] == 'true') {
						if (optGroup != null) {
		           			selectFuncionalidades.appendChild(optGroup);
						}
						optGroup = document.createElement("optgroup");
						optGroup.label = pontos[key][0];
						optGroup.id = pontos[key][0];
					} else if (pontos[key][1] == 'false') { 
						if (optGroup != null) {
							selectFuncionalidades.appendChild(optGroup);
						}
						var novaOpcao = new Option(pontos[key][0], key);
						novaOpcao.label = pontos[key][0];
						novaOpcao.pai = null;
						selectFuncionalidades.appendChild(novaOpcao);
					} else {
						var novaOpcao = new Option(pontos[key][0], key);
						novaOpcao.label = pontos[key][0];
						novaOpcao.pai = optGroup.label;
						if (optGroup != null) {
							optGroup.appendChild(novaOpcao);
						} 
					}
	        	}
				if (optGroup != null) {
					selectFuncionalidades.appendChild(optGroup);
				}
	    	}
	    );
	}

	function existeCampo(idFuncionalidade){

		var retorno = false;
		var campo = document.forms[0].idFuncionalidadesSelecionadas;

		for(i = 0; i < campo.length; i++){
			if(idFuncionalidade == campo[i].value){
				retorno = true;
				break;
			}
		}

		return retorno;	
	}

	function moveSelectedOptionsEspecialComOptGroup(from,to) {
		// Unselect matching options, if required
		if (arguments.length>3) {
			var regex = arguments[3];
			if (regex != "") {
				unSelectMatchingOptions(from,regex);
			}
		}
		// Move them over
		if (!hasOptions(from)) { return; }
		var newOption = undefined;
		var to_option_index = to.selectedIndex;
		var to_total = to.options.length;
		to.selectedIndex = -1;
		for (var i=0; i<from.options.length; i++) {
			var o = from.options[i];
			if (o.selected) {
				if (!hasOptions(to)) {
				 	var index = 0; 
				 } else {
				 	 var index=to.options.length; 
				 }	
			 	newOption = new Option( o.text, o.value, false, false);
			 	newOption.pai = o.pai

				var optGroup = null;
				if (newOption.pai != null && newOption.pai != '') {  
				 	for (var j=0; j<to.children.length; j++) {
						if (instanceOf(to.children[j],'optgroup') && to.children[j].id == newOption.pai) {
							optGroup = to.children[j];
							optGroup.appendChild(newOption);
							break;
						}
					}
					if (optGroup == null) {
						optGroup = document.createElement('optgroup');
						optGroup.label = newOption.pai;
						optGroup.id = newOption.pai;
						optGroup.appendChild(newOption);
						to.appendChild(optGroup);
					}
				}
				if (optGroup == null) {
					to.options[index] = newOption;
				}
			}
		}
			
		// Delete them from original
		for (var i=(from.options.length-1); i>=0; i--) {
			var o = from.options[i];
			if (o.selected) {
				if (o.pai != null && o.pai != '') {
					
					for (var j=0; j<from.children.length; j++) {
						if (instanceOf(from.children[j],'optgroup') && from.children[j].id == o.pai && from.children[j].children.length <= 1) {
							from.removeChild(from.children[j]);
						}
					}
					from.options[i] = null;
				} else {
					from.options[i] = null;
				}
			}
		}
			
		if (to_option_index > -1) {
			var count = (to_total) - (to_option_index+1);
			for (var i=0; i<count; i++) {
				moveOptionUp(to) 
			}
		}	

		from.selectedIndex = -1;
		to.selectedIndex = -1;
	}

	function instanceOf( element , type ) {
	    elementType = element.tagName.toLowerCase();
	    type = type.toLowerCase();
	 
	    return type == elementType;
	}

	function carregarOperacoes(idFuncionalidade) {
		if (idFuncionalidade != ""){
			AjaxService.carregarOperacoes(idFuncionalidade, 
			       	function(pontos) {            		      		         		
						var selectOperacoes = document.getElementById("idOperacoes");
						removeChildrenFromNode(selectOperacoes);
	       				for (key in pontos){
							var id = key;
							var novaOpcao = new Option(pontos[key], key);
							novaOpcao.label = pontos[key];
							selectOperacoes.appendChild(novaOpcao);
			        	}
			    	}
			    );
		}
	}

	function removeChildrenFromNode(node) {
		if(node.hasChildNodes()) {
			while(node.childNodes.length >= 1 ) {
				node.removeChild(node.firstChild);
			}
		}
	}

	function adicionarOperacoes() {
		var selectModulo = document.getElementById("idModulo");
		var chaveModulo = selectModulo.value;

		var selectFuncionalidades = document.getElementById("idFuncionalidades");
		var chaveFuncionalidades = selectFuncionalidades.value;
		
		var selectOperacoes = document.getElementById("idOperacoes");
		var chavesOperacoes = ''; 
		for (var i=0; i<selectOperacoes.options.length; i++){
			if (selectOperacoes.options[i].selected) {
				chavesOperacoes += selectOperacoes.options[i].value;
				if (i<selectOperacoes.options.length) {
					chavesOperacoes += ',';
				}	
			}
		}

		document.getElementById("moduloHidden").value = chaveModulo;
		document.getElementById("funcionalidadeHidden").value = chaveFuncionalidades;
		document.getElementById("operacoesHidden").value = chavesOperacoes;

		submeter("controlePerfilAcessoForm", "adicionarPermissao");
		
	}

	function limpar() {
		$(".campoCheckbox").attr("checked", false);
		document.getElementById("descricao").value = "";
		document.getElementById("idResponsavel").selectedIndex = 0;
		document.getElementById("email").value = "";
		document.getElementById("descricaoPapel").innerHTML = "";
	}

	function carregarEmail(chaveFuncionario) {
		if (chaveFuncionario > 0) {
			AjaxService.obterFuncionarioPorChave(chaveFuncionario, 
			       	function(funcionario) {            		      		         		
						var campoEmail = document.getElementById("email");
						campoEmail.value = funcionario.email;
			    	}
			    );
		} else {
			var campoEmail = document.getElementById("email");
			campoEmail.value = '';
		}
	}

	function limparOperacoes() {
		var selectOperacoes = document.getElementById("idOperacoes");


		selectOperacoes.length = 0;
	  	while (selectOperacoes.firstChild) {
	  		selectOperacoes.removeChild(selectOperacoes.firstChild);
		}
	}

	function limparFormulario() {
		
		document.forms[0].descricao.value = "";
		document.forms[0].idResponsavel.value = "-1";
		document.forms[0].email.value = "";
		document.forms[0].status[0].checked = true;
		document.forms[0].descricaoPapel.value = "";
		document.forms[0].idModulo.value = "-1";

		document.forms[0].idFuncionalidades.value = "";
		document.forms[0].idOperacoes.value = "";
		limparFuncionalidades();
		limparOperacoes();
		
	}
	
	function clickOperacaoItem(checkBox){
		
	}
	
	function montarOperacaoList(funcionalidadeItemMenu){
		var operacaoList = "<ul>";
		
		if (funcionalidadeItemMenu.operacoes.length > 0){
			for(var x = 0; x < funcionalidadeItemMenu.operacoes.length; x++){
				var currentItem = funcionalidadeItemMenu.operacoes[x];
				var currentItemValue = JSON.stringify(currentItem);
				
				var marcadorCheckBoxOperacaoSistema = "opsi_";
				var inputCheckBox = "<input id='"+marcadorCheckBoxOperacaoSistema + currentItem.idOperacaoSistema+"' class='campoCheckbox' onchange='clickOperacaoItem(this)' type='checkbox' name='chkOperacao' value='"+currentItemValue+"'/>";
			
				var item = '<li class="closed">'+inputCheckBox+'<span class="file">'+currentItem.descricaoOperacao+'</span></li>'; 
				
				operacaoList = operacaoList + item;
			}
		}else{
			for(var x = 0; x < funcionalidadeItemMenu.subFuncionalidades.length; x++){
				var currentItem = funcionalidadeItemMenu.subFuncionalidades[x];
				var subMenuOperacaoList = montarOperacaoList(funcionalidadeItemMenu.subFuncionalidades[x]);
				operacaoList = operacaoList + '<li id="'+currentItem.id+'" class="closed"><span class="folder">'+currentItem.descricao+'</span>'+subMenuOperacaoList+'</li>';
			}
		}
		
		operacaoList = operacaoList + "</ul>";
		
		return operacaoList;
	}
	
	function montarFuncionalidadeList(itemMenu){
		var funcionalidadeList = "<ul>";
			for(var x = 0; x < itemMenu.listaFuncionalidade.length; x++){
				var currentItem = itemMenu.listaFuncionalidade[x];
				var subMenuOperacaoList = montarOperacaoList(itemMenu.listaFuncionalidade[x]);
				
				funcionalidadeList = funcionalidadeList + '<li id="'+currentItem.id+'" class="closed"><span class="folder">'+currentItem.descricao+'</span>'+subMenuOperacaoList+'</li>';   
			}			
		
		funcionalidadeList = funcionalidadeList + "</ul>";
		
		return funcionalidadeList;
	}
	
	function onClickTreeItem(idMenu){
	
	}
	
	function clickOperacaoItem(obj){
		jsonOperacaoItem = JSON.parse(obj.value);
		
		jsonOperacaoItem.isSelecionado = !jsonOperacaoItem.isSelecionado;
		
		obj.value = JSON.stringify(jsonOperacaoItem);
	}
	
	function salvarEstadoArvore(){
		var arvore = document.getElementById("browser");
		arvore = encodeArvoreHTML(arvore.innerHTML);
		document.getElementById("arvoreOperacao").value = arvore;
	}
	
	function marcarItensSelecionados(){
		for(var x = 0; x < listaOperacoes.length; x++){
			var chaveOperacaoSistemaCheckBox = "opsi_"+listaOperacoes[x].idOperacaoSistema;
			var operacao = document.getElementById(chaveOperacaoSistemaCheckBox);
			
			if (operacao != undefined){
				var operacaoItemCheckBox = JSON.parse(operacao.value);
				
				operacaoItemCheckBox.isSelecionado = true;
				
				operacao.checked = true;
				operacao.value = JSON.stringify(operacaoItemCheckBox);				
			}
		}
	}
	
	function init () {
		listaOperacoes = JSON.parse('${operacaoItemLista}');
		
		//se a arvore j� estiver carregada e for um postback da p�gina
		//carregar o estado que a �rvore estava antes e manter os checks selecionados
		<c:if test="${not empty arvoreOperacao}">
			var arvore = '<c:out value="${arvoreOperacao}" />';
			$("#browser").append(decodeArvoreHTML(arvore));
			$("#browser").treeview({persist: "cookie"});
			
			$("#browser").find(".campoCheckbox").each(function(){
				var jsonString = this.value;
				var jsonObject = JSON.parse(jsonString);
				
				if (jsonObject.isSelecionado == true){
					$(this).attr("checked", "checked");
				}
			});
		</c:if>		
		
		<c:if test="${empty arvoreOperacao}">
			AjaxService.listarArvorePermissaoCompleta(
	      			{callback: function(listaMenu) {            		      		         		
				var listaMenuItem = JSON.parse(listaMenu);
	      		
				for(var x = 0; x < (listaMenuItem.length); x++){
					var descricao = listaMenuItem[x].descricao;
	        			var idMenu = listaMenuItem[x].id;
	        			var option = "";
	        			var funcionalidadeList = montarFuncionalidadeList(listaMenuItem[x]);
	        			
	            		option = option + '<li id="'+idMenu+'" class="closed"><span class="folder">'+descricao+'</span>'+funcionalidadeList+'</li>';        			
					
	            		$("#browser").append(option);
				}
				
		            }, async: false}
	            );	
			
			$.cookie("treeview", null);
			$("#browser").treeview({persist: "cookie"});
			
			//$("#browser").find(".hitarea").each(function () {
			//	$(this).unbind("click");
			//});
			
			marcarItensSelecionados();
		</c:if>
	}

	addLoadEvent(init);
		
</script>

<form:form method="post" action="exibirAlteracaoPapel" id="controlePerfilAcessoForm" name="controlePerfilAcessoForm"> 

<input type="hidden" name="acao" id="acao" value="pesquisarPapeis">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${papel.chavePrimaria}"/>

<input type="hidden" name="arvoreOperacao" id="arvoreOperacao">
<input type="hidden" name="moduloHidden" id="moduloHidden" value="${moduloHidden}">
<input type="hidden" name="funcionalidadeHidden" id="funcionalidadeHidden" value="${funcionalidadeHidden}">
<input type="hidden" name="operacoesHidden" id="operacoesHidden" value="${operacoesHidden}">

<fieldset id="conteinerIncluirAtualizarPapel" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="conteinerBloco">
	<fieldset class="coluna">
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
		<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="50" size="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${papel.descricao}"><br />
	    
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Respons�vel:</label>
		<select name="idResponsavel" id="idResponsavel" class="campoSelect" onchange="carregarEmail(this.value)">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaResponsavel}" var="responsavel">
				<option value="<c:out value="${responsavel.chavePrimaria}"/>" title="<c:out value="${responsavel.nome}"/>" <c:if test="${papel.funcionario.chavePrimaria == responsavel.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${responsavel.nome}"/>
				</option>
			</c:forEach>
			</select><br />
	    
		<label class="rotulo">E-Mail:</label>
		<input class="campoTexto" type="text" id="email" name="email" maxlength="50" size="50" value="${papel.funcionario.email}" readonly="readonly"><br />
		
 		<label class="rotulo rotulo2Linhas"><span class="campoObrigatorioSimbolo">* </span>Status:</label> 
 		<input class="campoRadio" type="radio" name="habilitado" id="status" value="true" <c:if test="${papel.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label> 
 	   	<input class="campoRadio" type="radio" name="habilitado" id="status" value="false" <c:if test="${papel.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label> 
	</fieldset>
	
	<fieldset class="colunaFinal">
			<label class="rotulo rotuloVertical campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o do Perfil:</label>
			<textarea id="descricaoPapel" class="campoVertical" name="complemento" maxlength="255"><c:out value="${papel.complemento}"/></textarea>
	</fieldset>
</fieldset>

<hr class="linhaSeparadora1" />

<fieldset class="conteinerBloco">
	<legend class="conteinerBlocoTitulo"><span class="campoObrigatorioSimbolo">* </span>Permiss�es</legend>
	<ul id="browser" class="filetree">

	</ul>	   
</fieldset>
   
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input id="botaoLimpar" name="Button" class="bottonRightCol2 bottonLeftColUltimo botaoRemover" value="Limpar" type="button" onclick="limpar();">
    <fieldset class="conteinerBotoesDirFixo">
    <vacess:vacess param="atualizarPapel">
		<input id="botaoSalvar" name="Button" class="bottonRightCol2 bottonRightColUltimo botaoAlterar" value="Salvar" type="button" onclick="alterar();">
	</vacess:vacess>
	</fieldset>	
	    
</fieldset>
<token:token></token:token>
</form:form> 