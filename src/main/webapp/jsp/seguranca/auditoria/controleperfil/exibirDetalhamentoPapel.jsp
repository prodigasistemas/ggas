<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<h1 class="tituloInterno">Detalhar Perfil<a class="linkHelp" href="<help:help>/detalhamentodoperfil.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar esse Perfil clique em <span class="destaqueOrientacaoInicial">Alterar</span>. Para Emitir Relat�rio clique em <span class="destaqueOrientacaoInicial">Gerar Relat�rio</span>.</p>

<script>

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaPapel"/>';
	}

	function alterar(){
		document.forms[0].postBack.value = false;
		submeter("controlePerfilAcessoForm", "exibirAlteracaoPapel");
	}

	function gerarRelatorio(){
		submeter("controlePerfilAcessoForm", "gerarRelatorioPapel");
	}

</script>

<form:form method="post" action="exibirDetalhamentoPapel" id="controlePerfilAcessoForm" name="controlePerfilAcessoForm"> 


<input type="hidden" name="acao" id="acao" value="exibirDetalhamentoPapel">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${papel.chavePrimaria}"/>

<fieldset id="conteinerDetalharPerfil" class="detalhamento">
	<fieldset class="coluna detalhamentoColunaLarga">
		<label class="rotulo">Nome:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${papel.descricao}"/></span><br />
	    
		<label class="rotulo">Respons�vel:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${papel.funcionario.nome}"/></span><br />
	    
		<label class="rotulo">E-Mail:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${papel.funcionario.email}"/></span><br />
		
		<label class="rotulo">Status:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${papel.habilitado ? 'Ativo' : 'Inativo'}"/></span><br />
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo rotuloVertical">Descri��o do Perfil:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${papel.complemento}"/></span><br />
	</fieldset>
</fieldset>

<c:if test="${listaAdicionados ne null and not empty listaAdicionados}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="listaAdicionados" sort="list" id="vo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="/exibirDetalhamentoPapel">
		<display:column sortable="false" title="M�dulo" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px; width: 130px">
			<c:out value="${vo.modulo.descricao}"/>
		</display:column>
		<display:column title="Funcionalidade" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px;">
			<c:out value="${vo.funcionalidade.descricao}"/>
		</display:column>
		<display:column title="Opera��es" style="width: 370px">
			<c:out value="${vo.operacoesFormatado}"/>
		</display:column>
	</display:table>
</c:if>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
   	<fieldset class="conteinerBotoesDirFixo">
   		<input name="Button" class="bottonRightCol2 botaoGerarRelatorio" value="Gerar Relat�rio" type="button" onclick="gerarRelatorio();">
    <vacess:vacess param="atualizarPapel">
    	<input name="Button" class="bottonRightCol2 bottonRightColUltimo botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>    
    </fieldset> 
</fieldset>

</form:form> 