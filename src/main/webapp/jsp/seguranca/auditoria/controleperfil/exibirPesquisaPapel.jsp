<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Perfil<a class="linkHelp" href="<help:help>/consultadosperfis.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para restringir a pesquisa de perfis a um Nome e/ou Perfil espec�fico, preencha os respectivos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />
Para exibir todos os perfis cadastrados apenas clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<script>
	
		function limparFormulario() {		
			document.forms[0].descricao.value = "";
			document.forms[0].habilitado[0].checked = true;		
		}	
		
	    	function gerarRelatorio(){
	    		var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();	    		
				submeter("controlePerfilAcessoForm", "gerarRelatorioPapel");
			}
	    	}
			
		function incluir() {		
			location.href = '<c:url value="/exibirInclusaoPapel"/>';
		}
		
		function alterar(chave) {
		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("controlePerfilAcessoForm", "exibirAlteracaoPapel");
		    }
		}
		
		function excluir(){	
			var selecao = verificarSelecaoSemMensagem();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('controlePerfilAcessoForm', 'removerPapeis');
				}
		    } else {
		    	alert ("Selecione um ou mais registros para realizar a opera��o!");
		    }
		}
		
		function detalharPapel(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("controlePerfilAcessoForm", "exibirDetalhamentoPapel");
		}	
			
		function removerListaPerfil(){		
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('controlePerfilAcessoForm', 'removerPapel');
				}
	    	}
		}	
		
	</script>

<form:form method="post" action="pesquisarPapel" id="controlePerfilAcessoForm" name="controlePerfilAcessoForm">

	<input name="acao" type="hidden" id="acao" value="pesquisarPapel">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="conteinerPesquisarPerfil" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo" id="rotuloNome" for="descricao" >Nome:</label>
		<input class="campoTexto campoHorizontal" id="descricao" type="text" name="descricao" maxlength="30" size="45" tabindex="3" value="${papel.descricao eq null ? descricao : papel.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');">
		
		<label class="rotulo rotuloHorizontal" id="rotuloIndicadorUso" for="habilitado">Indicador de Uso:</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Ativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Inativo</label>
		<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Todos</label><br /><br />
		
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<vacess:vacess param="pesquisarPapel">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaPapeis ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaPapeis" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" sort="list" id="papel" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPapel">
	        <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${papel.chavePrimaria}">
	     	</display:column>
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${papel.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	        <display:column sortable="true" sortProperty="descricao" title="Nome" headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href="javascript:detalharPapel(<c:out value='${papel.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${papel.descricao}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
    	<c:if test="${not empty listaPapeis}">
			<input id="botaoAlterar" name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterar();" type="button">
			<input id="botaoRemover" type="button" name="buttonRemover" value="Remover" class="bottonRightCol2" onclick="excluir();">
			<input name="Button" class="bottonRightCol2" value="Gerar Relat�rio" type="button" onclick="gerarRelatorio()">
		</c:if>
		<vacess:vacess param="inserirPapel">
			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>
	    
</form:form>
