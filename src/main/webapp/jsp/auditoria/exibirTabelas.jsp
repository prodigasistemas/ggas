<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Tabelas<a class="linkHelp" href="<help:help>/auditoriadetabelas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar uma tabela espec�fica, selecione uma op��o ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todas.</p>

<script language="javascript">

	function pesquisarTabelas() {
		submeter("auditoriaForm","exibirTabelasSelecionadas");
	}

	
	function alterarTabela() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("auditoriaForm","exibirAlterarTabela");
		}
	}

	function detalharTabela(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("auditoriaForm","exibirDetalhesTabela");
	}
	
</script>

<form:form method="post" action="exibirTabelasSelecionadas" id="auditoriaForm" name="auditoriaForm">
	<input name="acao" type="hidden" id="acao" value="exibirTabelasSelecionadas"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${chavePrimaria}">
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		  
		<label class="rotulo">Tabelas:</label>
		<select class="campoSelect campoHorizontal" style="margin-right: 40px" id="idTabela" name="idTabela">
			<option value="-1">Todas</option>
			<c:forEach items="${listaTabelas}" var="obj">
				<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${idTabela == obj.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${obj.nome}"/>
				</option>		
		    </c:forEach>
		</select>
				
		<vacess:vacess param="exibirTabelasSelecionadas">
			<input name="buttonRemover" value="Pesquisar" class="bottonRightCol2 botaoAditar" type="button" onclick="pesquisarTabelas();">
		</vacess:vacess> 
	</fieldset>
				
	<c:if test="${listaTabelasSelecionadas ne null}">
		
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaTabelasSelecionadas" sort="list" id="tabela" pagesize="15" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirTabelasSelecionadas">
			
			<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      		<input type="checkbox" name="chavesPrimarias"  value="${tabela.chavePrimaria}">
	     	</display:column>
	     	
			<display:column sortable="true" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq" class="conteudoTabelaEsq">
				<a href="javascript:detalharTabela(<c:out value='${tabela.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${tabela.nome}'/>
				</a>
			</display:column>
		     
		     <display:column sortable="true" sortProperty="mnemonico" title="Mnem�nico" style="width: 135px">
		     	<a href="javascript:detalharTabela(<c:out value='${tabela.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${tabela.mnemonico}'/>
				</a>
		     </display:column>		     
		    
		     <display:column  title="Audit�vel"  style="width: 120px">
		     	<c:choose>
					<c:when test="${tabela.auditavel == true}">
						<img alt="Sim" title="Sim" src="<c:url value="/imagens/check2.gif"/>" border="0"> Sim
					</c:when>
					<c:otherwise>
						<img alt="N�o" title="N�o" src="<c:url value="/imagens/cancel16.png"/>" border="0"> N�o
					</c:otherwise>
				</c:choose>
	    	 </display:column>
		</display:table>
		
		<fieldset class="conteinerBotoes">
			<vacess:vacess param="exibirAlterarTabela">
				<input name="buttonRemover" value="Salvar" class="bottonRightCol2 botaoGrande1" onclick="alterarTabela();" type="button">
			</vacess:vacess>
		</fieldset>
	</c:if>	
	 
</form:form>