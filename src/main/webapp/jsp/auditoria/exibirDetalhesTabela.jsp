<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Auditoria da Tabela<a class="linkHelp" href="<help:help>/detalhamentodastabelasauditadas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script language="javascript">

	function voltar(){	
		submeter("auditoriaForm", "exibirTabelasSelecionadas");
	}
	
	function alterar(){
		submeter("auditoriaForm", "exibirAlterarTabela");
	}		
	
</script>

<form:form method="post" action="exibirDetalhesTabela" id="auditoriaForm" nome="auditoriaForm"> 

	<input name="acao" type="hidden" id="acao" value="exibirDetalhesTabela"/>
 	<input name="idTabela" type="hidden" id="idTabela" value="${idTabela}">
 	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${chavePrimaria}">
 	
		<fieldset class="detalhamento">
			<label class="rotulo" id="rotuloCliente">Tabela:</label>
			<span id="detalhamentoDescricao" class="itemDetalhamento"><c:out value="${nomeTabelaSelecionada}"/></span>
		</fieldset>
			
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaColunasTabelaSelecionada" sort="list" id="coluna" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhesTabela">
			<display:column sortable="true" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq" class="conteudoTabelaEsq2">
					<c:out value='${coluna.descricao}'/>
			</display:column>
		     
		     <display:column  title="Audit�vel"  style="width: 100px">
				<c:choose>
					<c:when test="${coluna.auditavel == true}">
						<img alt="Sim" title="Sim" src="<c:url value="/imagens/check2.gif"/>" border="0"> Sim
					</c:when>
					<c:otherwise>
						<img alt="N�o" title="N�o" src="<c:url value="/imagens/cancel16.png"/>" border="0"> N�o
					</c:otherwise>
				</c:choose>
	    	 </display:column>
		</display:table>
		
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
		<vacess:vacess param="alterarTabela">
			<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>
	</fieldset>

</form:form> 