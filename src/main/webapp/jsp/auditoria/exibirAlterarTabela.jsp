<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Auditoria da Tabela<a class="linkHelp" href="<help:help>/alteraodastabelasdeauditoria.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar uma tabela espec�fica, selecione uma op��o ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todas.</p>

<script language="javascript">

	function cancelar(){	
		submeter("auditoriaForm", "exibirTabelasSelecionadas");
	}
	
	function alterar(){
		submeter('auditoriaForm', 'alterarTabela');
	}		
	
</script>

<form:form method="post" action="alterarTabela" id="auditoriaForm" name="auditoriaForm"> 
	<input name="acao" type="hidden" id="acao" value="alterarTabela"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${chavePrimaria}">
	<input name="idTabela" type="hidden" id="idTabela" value="${idTabela}">
 	
		<fieldset class="conteinerPesquisarIncluirAlterar">
			<label class="rotulo" id="rotuloCliente">Tabela:</label>
			<span id="detalhamentoDescricao" class="itemDetalhamento"><c:out value="${nomeTabelaSelecionada}"/></span>
		</fieldset>
 
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaColunasTabelaSelecionada" sort="list" id="coluna" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirAlterarTabela">
			
			<display:column sortable="true" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq" class="conteudoTabelaEsq2">
					<c:out value='${coluna.descricao}'/>
			</display:column>
			
			<display:column style="text-align: center; width: 100px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>&nbsp;&nbsp;&nbsp;Audit�vel" >
	      		<input type="checkbox" name="auditavel" value='${coluna.chavePrimaria}' <c:if test="${coluna.auditavel == true}">checked="checked"</c:if>>
	     	</display:column>
	     	
		</display:table>
		
	<fieldset class="conteinerBotoes">
		<input name="button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
		<vacess:vacess param="alterarTabela">
			<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>
	</fieldset>

</form:form> 