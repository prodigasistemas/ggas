<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Detalhar Cr�dito/D�bito a Realizar<a class="linkHelp" href="<help:help>/detalhandoumcrditodbitoarealizar.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<form method="post" action="voltarParaPesquisarCreditoDebito" enctype="multipart/form-data" id="creditoDebitoForm" name="creditoDebitoForm" >

<script>

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=0');

	function autorizar(chave){
		$("#chavePrimaria").val(chave);
		submeter('creditoDebitoForm', 'autorizarCreditoDebito');
	}
	
</script>

	<input name="idCliente" type="hidden" id="${creditoDebitoVO.idCliente}">
	<input name="idImovel" type="hidden" id="${creditoDebitoVO.idImovel}">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${creditoDebitoVO.chavePrimaria}"/>

	<fieldset class="detalhamento">
	
		<!-- Autoriza��o de registro pendente -->	
		<c:if test="${hasPermissaoAutorizar ne null && hasPermissaoAutorizar}">
			<fieldset>
				<label class="rotulo" id="rotuloStatusCreditoDebito" for="status">O Cr�dito/D�bito est� pendente de an�lise. Deseja autorizar o Cr�dito/D�bito?</label>
				<input class="campoRadio" type="radio" name="status" id="statusCreditoDebitoAutorizado" tabindex="30" value="${statusAutorizado}">
				<label class="rotuloRadio" for="statusCreditoDebitoAutorizado">Autorizar</label>
				<input class="campoRadio" type="radio" name="status" id="statusCreditoDebitoNaoAutorizado" tabindex="31" value="${statusNaoAutorizado}">
				<label class="rotuloRadio" for="statusCreditoDebitoNaoAutorizado">N�o Autorizar</label>			
			
				<input name="button" class="bottonRightCol2 botaoGrande1" value="Ok" type="button" onclick="autorizar(<c:out value='${creditoDebitoVO.chavePrimaria}'/>);" <c:if test="${hasPermissaoAutorizar ne true}">disabled</c:if> >
			</fieldset>
		</c:if>
		
		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosCliente" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo">Cliente:</label>
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.nomeCompletoCliente}"/></span><br />
				<label class="rotulo">CPF/CNPJ:</label>
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.documentoFormatado}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoFormatadoCliente}"/></span><br />
				<label class="rotulo">E-mail:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.emailCliente}"/></span><br />
			</fieldset>
		</fieldset>
		
		<a class="linkExibirDetalhesSeguinte" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo">Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.descricaoPontoConsumo}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoPontoConsumo}"/></span><br />
				<label class="rotulo" style="white-space: normal;">C�digo Ponto Consumo:</label>
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.codigoPontoConsumo}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.cepPontoConsumo}"/></span><br />
				<label class="rotulo">Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.complementoPontoConsumo}"/></span><br />
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadora1">
			
		<fieldset id="detalhamentoCreditoDebito" class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Dados do Cr�dito/D�bito</legend>	
			<fieldset id="detalhamentoCreditoDebitoCol1" class="coluna">
				<label class="rotulo rotulo2Linhas" >Tipo de Lan�amento:</label>
				<span class="itemDetalhamento itemDetalhamento2Linhas">
				<c:choose>
					<c:when test="${creditoDebitoVO.indicadorCreditoDebito == 'credito'}">				
						Cr�dito
					</c:when>
					<c:when test="${creditoDebitoVO.indicadorCreditoDebito == 'debito'}">				
						D�bito
					</c:when>
				</c:choose>	
				</span><br />
				<label class="rotulo">Rubrica:</label>
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.descricaoRubrica}"/></span><br class="quebraLinha"/>
				<c:if test="${not empty(creditoDebitoVO.quantidade)}">
					<label class="rotulo" >Quantidade:</label>
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.quantidade}"/></span><br />
				</c:if>
				<label class="rotulo" >Valor:</label>
				<span class="itemDetalhamento"><c:if test="${not empty(creditoDebitoVO.valor)}">R$ </c:if><c:out value="${creditoDebitoVO.valor}"/></span><br />
				<c:if test="${quantidade ne null}">
					<label class="rotulo">Quantidade:</label>
					<span class="itemDetalhamento"><c:out value="${quantidade}"/></span>	
				</c:if>
				<c:if test="${valorUnitario ne null}">
					<label class="rotulo">Valor Unit�rio:</label>
					<span class="itemDetalhamento"><c:out value="${valorUnitario}"/></span>	
				</c:if>
			</fieldset>
			
			<fieldset id="detalhamentoCreditoDebitoCol2" class="colunaMeio">
				<c:choose>					
					<c:when test="${creditoDebitoVO.descricaoOrigemCredito eq ''}">
						<label class="rotulo" >Valor da Entrada:</label>
						<span class="itemDetalhamento"><c:if test="${not empty(creditoDebitoVO.valorEntrada)}">R$ </c:if><c:out value="${creditoDebitoVO.valorEntrada}"/></span>
						
						<c:if test="${not empty(creditoDebitoVO.taxaJuros)}">
							<label class="rotulo" >Juros:</label>
							<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.taxaJuros}"/> % <c:out value="${creditoDebitoVO.descricaoPeriodicidadeJuros}"/></span><br />
						</c:if>																			
					</c:when>					
					<c:otherwise>
						<label class="rotulo" >Origem do Cr�dito:</label>
						<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.descricaoOrigemCredito}"/></span><br />				
					</c:otherwise>
				</c:choose>		
				<label class="rotulo" >Parcelas:</label>
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.parcelas}"/></span><br />		
				<c:if test="${not empty(creditoDebitoVO.descricaoPeriodicidade)}">
					<label class="rotulo" >Periodicidade:</label>
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.descricaoPeriodicidade}"/></span><br />
				</c:if>	<br/>
					
				<c:if test="${creditoDebitoVO.tipoDocumento != ''}">
					<label class="rotulo" >Tipo Documento:</label>
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.tipoDocumento}"/></span><br />
					<label class="rotulo" >N� do Documento:</label>
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.numeroDocumento}"/></span><br />
					<c:if test="${creditoDebitoVO.numeroNotaFiscal != ''}">
						<label class="rotulo" >N� da Nota Fiscal:</label>
						<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.numeroNotaFiscal}"/></span><br />
					</c:if>					
				</c:if>					
			</fieldset>
			
			<fieldset id="detalhamentoCreditoDebitoCol3" class="colunaFinal">		
				<label class="rotulo rotulo2Linhas" >Complemento da Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamento2Linhas itemDetalhamentoPequeno2"><c:out value="${creditoDebitoVO.complementoDescricao}"/></span><br class="quebra2Linhas" />		
				<c:if test="${creditoDebitoVO.indicadorCreditoDebito == 'credito'}">
					<label class="rotulo" >Inicio do cr�dito:</label>
				</c:if>
				<c:if test="${creditoDebitoVO.indicadorCreditoDebito == 'debito'}">
					<label class="rotulo" >Iniciar cobran�a:</label>
				</c:if>
				<c:if test="${creditoDebitoVO.dataInicioCobranca ne null}">
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.dataInicioCobranca}"/></span>	
				</c:if>
				<c:if test="${creditoDebitoVO.idInicioCobranca ne null}">
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.qtdDiasCobranca}"/> dias ap�s <c:out value="${creditoDebitoVO.descricaoEventoInicioCobranca}"/></span>	
				</c:if>
				<c:if test="${motivoCancelamento ne null}">
					<label class="rotulo rotulo2Linhas" >Motivo do Cancelamento:</label>
					<span class="itemDetalhamento itemDetalhamento2Linhas itemDetalhamentoPequeno2"><c:out value="${motivoCancelamento}"/></span>	
				</c:if>
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadoraDetalhamento">
		
		<c:if test="${listaDadosParcelas ne null}">
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Parcelas</legend>	
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaDadosParcelas" sort="list" id="dadosParcela" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
				   	<display:column property="numeroParcela" title="Parcelas" style="width: 65px"/>    
				   	<display:column title="Saldo Inicial (R$)">
				   		<fmt:formatNumber value="${dadosParcela.saldoInicial}" minFractionDigits="2"/>
				   	</display:column>
			   		<display:column title="Juros (R$)">
			   			<fmt:formatNumber value="${dadosParcela.juros}" minFractionDigits="2"/>
			   		</display:column>
			   		<display:column title="Amortiza��o (R$)">
			   			<fmt:formatNumber value="${dadosParcela.amortizacao}" minFractionDigits="2"/>
			   		</display:column>
				   	<display:column title="Total">
				   		<fmt:formatNumber value="${dadosParcela.total}" minFractionDigits="2"/>
				   	</display:column>
				   	<display:column title="Saldo Final (R$)">
				   		<fmt:formatNumber value="${dadosParcela.saldoFinal}" minFractionDigits="2"/>
				   	</display:column>
				   	<display:column title="Data de <br/>Pagamento" style="width: 75px">
				   		<fmt:formatDate value="${dadosParcela.dataPagamento}" pattern="dd/MM/yyyy"/>
				   	</display:column>
				   	<display:column title="Data de <br/>Vencimento" style="width: 75px">
				   		<fmt:formatDate value="${dadosParcela.dataVencimento}" pattern="dd/MM/yyyy"/>
				   	</display:column>
			    </display:table>			    
			</fieldset>
		</c:if>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Voltar" type="submit">    
	</fieldset>

</form>
