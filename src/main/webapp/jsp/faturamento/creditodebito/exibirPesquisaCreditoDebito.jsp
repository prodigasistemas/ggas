<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Cr�dito / D�bito a Realizar<a class="linkHelp" href="<help:help>/consultadocrditodbitoarealizar.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar o Cr�ditos / D�bitos selecione um cliente clicando em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span>, ou um im�vel clicando em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span></p>


<script type="text/javascript">

	$(document).ready(function(){
		// Dialog			
		$("#cancelamentoLancamentoPopup").dialog({
			autoOpen: false,
			width: 310,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
		$("#spanPesquisarPessoa").html("**&nbsp;");

		$('#numeroDocumento').on('input', function(){
			var numeroDocumentoValor = $('#numeroDocumento').val().trim();
			var possuiValor = Boolean(numeroDocumentoValor);
			if(possuiValor){
			    console.debug('#numeroDocumento ativar');
				ativarBotaoPesquisar();
			} else{
                console.debug('#numeroDocumento desativar');
				desativarBotaoPesquisar();
			}
		});
		$('#numeroDocumento').trigger('input');

        init();
	});
	
	function pesquisar() {
		submeter('creditoDebitoForm','pesquisarCreditoDebito');
	}
	
	function incluir() {
		submeter('creditoDebitoForm','exibirInclusaoCreditoDebito');
	}
	
	function exibirCancelarLancamento() {

		var selecao = verificarSelecao();
		if (selecao == true) {
			exibirJDialog("#cancelamentoLancamentoPopup");
	    }
	}

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
		}
		document.getElementById("botaoPesquisar").disabled = true;	
		document.getElementById("botaoIncluir").disabled = true;
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('numeroDocumento').value = "";
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		document.getElementById("botaoPesquisar").disabled = true;
		document.getElementById("botaoIncluir").disabled = true;

		document.getElementById('status').value = 0;
		document.getElementById('statusAutorizado').checked = false;
		document.getElementById('statusPendente').checked = false;
		document.getElementById('statusNaoAutorizado').checked = false;
		
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true&funcaoParametro=\'ativarBotaoPesquisar\'','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	enderecoImovel.value = imovel["enderecoImovel"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
            enderecoImovel.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

		ativarBotaoPesquisar();
	}

	function init() {
	    console.debug('init');
		<c:choose>
			<c:when test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				ativarBotaoPesquisar();
			</c:when>
			<c:when test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				ativarBotaoPesquisar();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>
		gerarRelatorio();
	}	

	function ativarBotaoPesquisar() {
		document.getElementById("botaoPesquisar").disabled = false;
		document.getElementById("botaoIncluir").disabled = false;
	}
	function desativarBotaoPesquisar() {
		document.getElementById("botaoPesquisar").disabled = true;
		document.getElementById("botaoIncluir").disabled = true;
	}

	function cancelarLancamento() {
		var motivoCancelamento = document.getElementById('motivoCancelamento');
		document.getElementById('idMotivoCancelamento').value = motivoCancelamento.value;
		submeter('creditoDebitoForm','cancelarLancamentoCreditoDebito');
	}

	function detalharCreditoDebito(idCreditoDebito) {
		document.getElementById('chavePrimaria').value = idCreditoDebito;
		submeter('creditoDebitoForm','exibirDetalhamentoCreditoDebito');
	}

	function gerarRelatorio() {
	<c:if test="${not empty gerarRelatorioExtratoDebito && gerarRelatorioExtratoDebito == true}">
		location.href = '<c:url value="/gerarRelatorioExtratoDebito"/>';
	</c:if>
	}
	
	function imprimirSegundaVia(idNota) {
		document.getElementById('chavePrimaria').value = idNota;
		submeter('0', 'imprimirSegundaViaCreditoDebitoRealizar');
	}
	
</script>

<form method="post" id="formPesquisarCreditoDebito" name="creditoDebitoForm" action="pesquisarCreditoDebito">
	
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${creditoDebitoVO.chavePrimaria}">
	<input name="idMotivoCancelamento" type="hidden" id="idMotivoCancelamento">
	
	<div id="cancelamentoLancamentoPopup" title="Cancelar Lan�amento">
		<label class="rotulo">Selecione o motivo do cancelamento do lan�amento:</label><br/>
		
		<select name="motivoCancelamento" id="motivoCancelamento" class="campoSelect">
	    	<option value="-1">Selecione</option>				
			<c:forEach items="${listaMotivoCancelamento}" var="motivoCancelamento">
				<option value="<c:out value="${motivoCancelamento.chavePrimaria}"/>" <c:if test="${creditoDebitoVO.idMotivoCancelamento == motivoCancelamento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${motivoCancelamento.descricao}"/>
				</option>		
			</c:forEach>
		</select><br class="quebraLinha"/>	
		
		<input type="button" class="bottonRightCol2" value="Ok" onclick="cancelarLancamento();">
	</div>
	
	<fieldset id="pesquisarCreditoDebito" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${creditoDebitoVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${creditoDebitoVO.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${creditoDebitoVO.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${creditoDebitoVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${creditoDebitoVO.enderecoFormatadoCliente}"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPesquisar"/>
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="isPesquisaObrigatoria" value="true"/>
			</jsp:include>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa"><span class="campoObrigatorioSimbolo">** </span>Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${creditoDebitoVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${creditoDebitoVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${creditoDebitoVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${creditoDebitoVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${creditoDebitoVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${creditoDebitoVO.condominio}">
				<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${creditoDebitoVO.enderecoImovel}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${creditoDebitoVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${creditoDebitoVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${creditoDebitoVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${creditoDebitoVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${creditoDebitoVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${creditoDebitoVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset style="clear:both;">
			<label class="rotulo rotuloHorizontal" for="status">Al�ada:</label>
			<input class="campoRadio" type="radio" name="status" id="statusAutorizado" value="${statusAutorizado}" <c:if test="${creditoDebitoVO.status eq statusAutorizado}">checked</c:if>>
			<label class="rotuloRadio" for="statusAutorizado">Autorizado</label>
			<input class="campoRadio" type="radio" name="status" id="statusPendente" value="${statusPendente}" <c:if test="${creditoDebitoVO.status eq statusPendente}">checked</c:if>>
			<label class="rotuloRadio" for="statusPendente">Pendente</label>
			<input class="campoRadio" type="radio" name="status" id="statusNaoAutorizado" value="${statusNaoAutorizado}" <c:if test="${creditoDebitoVO.status eq statusNaoAutorizado}">checked</c:if>>
			<label class="rotuloRadio" for="statusNaoAutorizado">N�o Autorizado</label>
			<input class="campoRadio" type="radio" name="status" id="status" value="0">
			<label class="rotuloRadio" for="status">Todos</label><br />
			
			<label class="rotulo"><span class="campoObrigatorioSimbolo">** </span>N�mero da Fatura Origem:</label>
			<input class="campoTexto campoHorizontal" type="text" id="numeroDocumento" name="numeroDocumento" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${creditoDebitoVO.numeroDocumento}">
			
		</fieldset>	
		
		<fieldset id="conteinerBotoesPesquisarDirPesquisarCreditoDebito" class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2" type="button" id="botaoPesquisar" value="Pesquisar" onclick="pesquisar();" disabled="disabled">
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>
	
	<c:if test="${listaCreditoDebito ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" sort="list" id="creditoDebito" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarCreditoDebito">
			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${creditoDebito.chavePrimaria}" <c:if test="${creditoDebito.indicadorCancelamentoBloqueado}"> disabled</c:if>>
	     	</display:column>
			<display:column title="Tipo de<br/>Lan�amento" style="width: 75px">
				<a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:choose>
						<c:when test="${creditoDebito.creditoOrigem eq null}">D�bito</c:when>
						<c:otherwise>Cr�dito</c:otherwise>
					</c:choose>
				</a>
			</display:column>
			
			<display:column title="Status" headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
	            	<c:out value="${creditoDebito.status}"/>	           
	            </a>
	        </display:column>
	        
			<display:column title="Rubrica" style="width: 200px">
				<a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${creditoDebito.rubrica}"/>
				</a>
			</display:column>    
			<display:column title="Cobrado/<br/>Total" style="width: 55px">
				<a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	            	<c:out value="${creditoDebito.numeroPrestacaoCobrada}"/>/<c:out value="${creditoDebito.quantidadeTotalPrestacoes}"/>
	            </a>
			</display:column>    
			<display:column title="Valor<br/>Total (R$)" style="text-align: right">
				<a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${creditoDebito.valor}" minFractionDigits="2"/>
				</a>
			</display:column>
			<!-- valorEntrada est� sendo usado como saldo devedor aqui na tela. -->
			<display:column title="Saldo (R$)" style="text-align: right">
				<a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<fmt:formatNumber value="${creditoDebito.saldo}" minFractionDigits="2"/>
				</a>
			</display:column>
			<display:column title="Situa��o" style="width: 100px">
				<a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value="${creditoDebito.situacao}"/>
				</a>
			</display:column>
			<display:column title="C�digo</br>Ponto Consumo" headerClass="tituloTabelaEsq">
				<a href='javascript:detalharCreditoDebito(<c:out value='${creditoDebito.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
	            	<c:out value="${creditoDebito.codigoPontoConsumo}"/>
				</a>
			</display:column>
	        <display:column title="2� Via" style="width: 40px" sortable="false">
				<a href='javascript:imprimirSegundaVia(<c:out value='${creditoDebito.chavePrimaria}'/>);'>
			    	<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via" title="Imprimir 2� Via" border="0" style="margin-top: 5px;" >
			    </a>				
			</display:column>			
	    </display:table>	
	</c:if>
	<fieldset class="conteinerBotoes">
		<c:if test="${listaCreditoDebito ne null}">
			<input name="buttonCancelar" value="Cancelar Lan�amento" class="bottonRightCol bottonLeftColUltimo" onclick="exibirCancelarLancamento();" type="button">
		</c:if>
		<vacess:vacess param="exibirInclusaoCreditoDebito">
			<input name="buttonIncluir" value="Incluir" class="bottonRightCol2 botaoGrande1 bottonLeftColUltimo" id="botaoIncluir" onclick="incluir();" type="button" disabled="disabled">
		</vacess:vacess>
	</fieldset>
</form>
