<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Incluir Cr�dito / D�bito a Realizar<a class="linkHelp" href="<help:help>/inclusodecrditodbitoarealizar.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para inserir um Cr�dito / D�bito a
Realizar selecione o Ponto de Consumo, preencha os campos abaixo e
clique em <span class="destaqueOrientacaoInicial">Gerar Parcelas</span>.
Para finalizar clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>


<script type="text/javascript">

	$(document).ready(function(){
		
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', minDate: '+0d'});

		
		$("input#indicadorInicioCobrancaApos + label").removeClass("rotuloDesabilitado");
		$("input#dataInicioCobranca").removeClass("campoDesabilitado");
		$("input#indicadorInicioCobrancaDias + label").addClass("rotuloDesabilitado");
		$("input#indicadorInicioCobrancaDias + label span").addClass("campoObrigatorioSimboloDesabilitado");
		$("input#qtdDiasCobranca").addClass("campoDesabilitado");
				
	});

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('conteinerPontoConsumo', 'fade=0,speed=400,persist=0,hide=0');
	
	animatedcollapse.addDiv('conteinerPeriodicidade', 'fade=0,speed=400,hide=0');
	
	animatedcollapse.addDiv('containerInicioCredito', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('conteinerOrigemCredito', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('conteinerValorEntrada', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('conteinerTaxaJuros', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('containerInicioCobranca', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('containerMelhorDiaVencimento', 'fade=0,speed=400,hide=1');
	
	function gerarParcelas() {
		//atualizarValor();
		
		if (!document.getElementById('valorUnitario').disabled) {
			document.getElementById('valorUnitarioHidden').value = document.getElementById('valorUnitario').value;
		}
		
		
		submeter('creditoDebitoForm','gerarParcelasCreditoDebito');
	}

	function salvar() {
		submeter('creditoDebitoForm','salvarParcelasCreditoDebito');
	}

	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaCreditoDebito"/>';
	}

	function limparFormulario() {
		document.getElementById('idRubrica').value = '-1';
		document.getElementById('idCreditoOrigem').value = "-1";
		document.getElementById('quantidade').value = "";
		document.getElementById('valor').value = "";
		document.getElementById('valorEntrada').value = "";
		document.getElementById('parcelas').value = "";
		document.getElementById('taxaJuros').value = "";
		document.getElementById('idPeriodicidadeJuros').value = "-1";
		document.getElementById('idPeriodicidade').value = "-1";
		document.getElementById('complementoDescricao').value = "";
		document.getElementById('dataInicioCredito').value = "";
		document.getElementById('dataInicioCobranca').value = "";
		if (document.getElementById('qtdDiasCobranca') != undefined) {
		document.getElementById('qtdDiasCobranca').value = "";
		}
		if (document.getElementById('idInicioCobranca') != undefined) {
		document.getElementById('idInicioCobranca').value = "-1";
		}
		document.getElementById('valor').innerHTML = "";
		document.getElementById('valorUnitario').value = "";
		if (document.getElementById('melhorDiaVencimento') != undefined) {
			document.getElementById('melhorDiaVencimento').value = "-1";
		}
	}

	
	function validarTextArea(e, elem, limiteCaracteres){
		var estouroLimite = false;
		var reDigits = /[A-Za-z�-��-�0-9\s\-\b\.\,\\\!\@\#\$\%\&\*\(\)\{\}\[\]\;\<\>\/\+\-\=\_]+$/;		
		var whichCode = (window.event) ? e.keyCode : e.which;
		key = String.fromCharCode(whichCode);
		
		if(elem.value.length == limiteCaracteres && validarPadrao(/[A-Za-z�-��-�0-9\s\-\b\.\,\\\!\@\#\$\%\&\*\(\)\{\}\[\]\;\<\>\/\+\-\=\_]+$/,key)){
			estouroLimite = true;		
		}
			
	    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroLimite;	
	}

	function selecionarPontoConsumo(idImovel) {
		document.getElementById('idImovel').value = idImovel;
		submeter('creditoDebitoForm','exibirInclusaoCreditoDebito');
	}

	function habilitarDesabilitarCampos(valor) {		
		if (!valor) {
			//D�bito
			animatedcollapse.hide('conteinerOrigemCredito');
			document.getElementById('idCreditoOrigem').value = "-1";
			animatedcollapse.show('conteinerValorEntrada');
			animatedcollapse.show('conteinerTaxaJuros');
			animatedcollapse.show('containerInicioCobranca');							
			document.getElementById('indicadorInicioCobrancaApos').checked = true;
			if (document.getElementById('indicadorInicioCobrancaDias') != undefined) {
			document.getElementById('indicadorInicioCobrancaDias').checked = false;
			}
					
				
			document.getElementById('dataInicioCobranca').disabled = false;
			$("input#dataInicioCobranca + img").css({'display':'block'});
			if (document.getElementById('qtdDiasCobranca') != undefined) {
			document.getElementById('qtdDiasCobranca').disabled = true;
			}
			if (document.getElementById('idInicioCobranca') != undefined) {
			document.getElementById('idInicioCobranca').disabled = true;		
			}
			animatedcollapse.hide('containerInicioCredito');
			document.getElementById('dataInicioCredito').value = "";
			
		//	validarContratoPorCliente();
			
		} else {
			//Cr�dito
			animatedcollapse.show('conteinerOrigemCredito');
			animatedcollapse.hide('conteinerValorEntrada');
			document.getElementById('valorEntrada').value = "";
			animatedcollapse.hide('conteinerTaxaJuros');
			document.getElementById('taxaJuros').value = "";
			document.getElementById('idPeriodicidadeJuros').value = "-1";
			document.getElementById('dataInicioCobranca').value = "";
			document.getElementById('indicadorInicioCobrancaApos').checked = true;
			habilitarInicioCobrancaApos();
			animatedcollapse.show('containerInicioCredito');
			animatedcollapse.hide('containerInicioCobranca');
		}

		document.getElementById('valorEntrada').disabled = valor;
		
		if (valor == true) {
			document.getElementById('valorEntrada').className = 'campoTexto';
			document.getElementById('valorEntrada').size = 13;
		}	else {
			document.getElementById('valorEntrada').className = 'campoTexto campoValorReal';
			document.getElementById('valorEntrada').size = 10;
		}
		document.getElementById('taxaJuros').disabled = valor;
		document.getElementById('idPeriodicidadeJuros').disabled = valor;
		document.getElementById('idCreditoOrigem').disabled = !valor;
	}

	function verificarPeriodicidade(elem) {
		var idPontoConsumo = '${creditoDebitoVO.idPontoConsumo}';		
		//if (idPontoConsumo != '' && idPontoConsumo != '0') {
			//var id = 'idPontoConsumo';
			//id = id.concat(idPontoConsumo);			
			//var radioSelecionado = document.getElementById(id);
			//if (radioSelecionado != undefined) {
			//	radioSelecionado.checked=true;
			//}
		//}
		if (elem.value != -1) {    		
        	AjaxService.isPeriodicidadeMensal(elem.value, {
	           	callback: function(isPeriodicidadeMensal) {   		
	           		if ((isPeriodicidadeMensal)&& ((idPontoConsumo==null) || (idPontoConsumo==0) ||((idPontoConsumo=='')))) {
	           			animatedcollapse.show('containerMelhorDiaVencimento');
			        } else {
			        	animatedcollapse.hide('containerMelhorDiaVencimento');
			        }
                }, async:false}
            ); 
      	} else {
      		animatedcollapse.hide('containerMelhorDiaVencimento');
        }

	}	

	function init() {
		
		
		var indicadorCreditoDebito = '${creditoDebitoVO.indicadorCreditoDebito}';
		var idInicioCobranca = document.getElementById('idInicioCobranca');
		var qtdDiasCobranca = document.getElementById('qtdDiasCobranca');
		
		if (indicadorCreditoDebito != null && indicadorCreditoDebito == 'true') {
			habilitarDesabilitarCampos(false);
			animatedcollapse.show('containerInicioCobranca');
		}

		if (indicadorCreditoDebito != null && indicadorCreditoDebito == 'false') {
			habilitarDesabilitarCampos(true);
			animatedcollapse.show('containerInicioCredito');
		}
		var indInicioCobranca = "${creditoDebitoVO.indicadorInicioCobranca}";
		if(indInicioCobranca != null && indInicioCobranca == 'true'){
			habilitarInicioCobrancaApos();
		}else if (indInicioCobranca != null && indInicioCobranca == 'false'){					
			habilitarInicioCobranca();
		}

		var idPontoConsumo = '${creditoDebitoVO.idPontoConsumo}';		
		if (idPontoConsumo != '' && idPontoConsumo != '0') {
			var id = 'idPontoConsumo';
			id = id.concat(idPontoConsumo);			
			var radioSelecionado = document.getElementById(id);
			if (radioSelecionado != undefined) {
				radioSelecionado.checked=true;
			}
		}
		
		var idRubrica = document.getElementById('idRubrica');		
		if (idRubrica != null) {
			carregarCampos(idRubrica, true);
		}
		
		var idPeriodicidade = document.getElementById('idPeriodicidade');
		if (idPeriodicidade != null && idPeriodicidade.value != '' && idPeriodicidade.value != 0) {
			verificarPeriodicidade(idPeriodicidade);
		}
		
		document.getElementById('valor').innerHTML = 'R$ ' + '${creditoDebitoVO.valorHidden}'; 
		document.getElementById('valorHidden').value = '${creditoDebitoVO.valorHidden}';
		document.getElementById('valorUnitario').value = '${creditoDebitoVO.valorUnitarioHidden}'; 
		document.getElementById('valorUnitarioHidden').value = '${creditoDebitoVO.valorUnitarioHidden}';

	}

	function habilitarInicioCobrancaApos() {
		document.getElementById('dataInicioCobranca').disabled = false;
		$("input#indicadorInicioCobrancaApos + label").removeClass("rotuloDesabilitado");
		$("input#indicadorInicioCobrancaApos + label span").removeClass("campoObrigatorioSimboloDesabilitado");
		$("input#dataInicioCobranca").removeClass("campoDesabilitado");
		$("input#dataInicioCobranca + img").css({'display':'block'});
		if (document.getElementById('qtdDiasCobranca') != undefined) {
		document.getElementById('qtdDiasCobranca').value = '';
		document.getElementById('qtdDiasCobranca').disabled = true;
		}
		if (document.getElementById('idInicioCobranca') != undefined) {
		document.getElementById('idInicioCobranca').value = '-1';
		document.getElementById('idInicioCobranca').disabled = true;
		}
		$("input#indicadorInicioCobrancaDias + label").addClass("rotuloDesabilitado");
		$("input#indicadorInicioCobrancaDias + label span").addClass("campoObrigatorioSimboloDesabilitado");
		$("input#qtdDiasCobranca").addClass("campoDesabilitado");
	}

	function habilitarInicioCobranca() {
		document.getElementById('indicadorInicioCobrancaDias').checked = true;
		document.getElementById('dataInicioCobranca').value = '';
		document.getElementById('dataInicioCobranca').disabled = true;
		$("input#indicadorInicioCobrancaApos + label").addClass("rotuloDesabilitado");
		$("input#indicadorInicioCobrancaApos + label span").addClass("campoObrigatorioSimboloDesabilitado");
		$("input#dataInicioCobranca").addClass("campoDesabilitado");
		$("input#dataInicioCobranca + img").css({'display':'none'});
		$("input#indicadorInicioCobrancaDias + label").removeClass("rotuloDesabilitado");
		$("input#indicadorInicioCobrancaDias + label span").removeClass("campoObrigatorioSimboloDesabilitado");
		$("input#qtdDiasCobranca").removeClass("campoDesabilitado");
		document.getElementById('qtdDiasCobranca').disabled = false;
		document.getElementById('idInicioCobranca').disabled = false;
	}

	function carregarCampos(elem, inicial) {		
		var valor = document.getElementById('valorUnitario');
		var taxaJuros = document.getElementById('taxaJuros');
		var periodicidadeTaxaJuros = document.getElementById('idPeriodicidadeJuros');
		var indicadorPeriodicidadeJuros;
		var indicadorEntradaObrigatoria;
		var indicadorAlteracaoValor;
		
		if (elem.value != -1) {
			
        	AjaxService.obterValorRubrica(elem.value, {
	           	callback: function(rubrica) {   		
	           		var valorReferencia = rubrica['valor'];
	           		if(valorReferencia != undefined && valorReferencia != ''){		           		
		           		if (!inicial) {	  
        					valor.value = valorReferencia;
		           		}
	           		}else{		           		
	           			if (!inicial) {
	           				valor.value = "";
	           			}
	           		}
        			indicadorPeriodicidadeJuros = rubrica['indicadorTaxaJuros'];
        			indicadorEntradaObrigatoria = rubrica['indicadorEntradaObrigatoria'];
        			indicadorAlteracaoValor = rubrica['indicadorAlteracaoValor'];
        			
        			if (!indicadorAlteracaoValor) {
        				document.getElementById('valorUnitarioHidden').value = rubrica['valor'];
        			}
                }, async:false}
            ); 
      	}
      	valor = document.getElementById('valorUnitario');
		var tipoDebito = document.getElementById('indicadorDebitoSim');
      	if (indicadorPeriodicidadeJuros != null && indicadorPeriodicidadeJuros == 'true')  {
    		if (tipoDebito != null && tipoDebito.value == 'true') {
	      		taxaJuros.disabled = false;
	          	periodicidadeTaxaJuros.disabled = false;
	          	animatedcollapse.show('conteinerTaxaJuros');	          	
    		}    		
      	} else {
      		taxaJuros.disabled = true;
          	periodicidadeTaxaJuros.disabled = true;
          	animatedcollapse.hide('conteinerTaxaJuros');          	
        }

      	
        if (indicadorEntradaObrigatoria == 'true') {
            //Ativa campo obrigat�rio
            $("label[for=valorEntrada] span.campoObrigatorioSimbolo").css("display","");
            $("label[for=valorEntrada]").addClass("campoObrigatorio");
        } else {
        	//Desativa campo obrigat�rio
        	$("label[for=valorEntrada] span.campoObrigatorioSimbolo").css("display","none");
        	$("label[for=valorEntrada]").removeClass("campoObrigatorio");
        }
        
        if(indicadorAlteracaoValor == 'true') {
        	document.getElementById('valorUnitario').disabled = false;
        } else {    
        	document.getElementById('valorUnitarioHidden').value = valor.value;
        	document.getElementById('valorUnitario').disabled = true;
        }
        
        atualizarValor();
	}

	function carregarRubrica(valor) {
		var campoRubrica = document.getElementById('idRubrica');		
		campoRubrica.length = 0;

		var novaRubrica = new Option("Selecione","-1");
		var novaOpcao = new Option("Selecione","-1");
		
		campoRubrica.options[campoRubrica.length] = novaRubrica;

		if (valor != -1) {    		
        	AjaxService.listarRubricasPorTipoCreditoDebito(valor, 
	           	function(listaRubrica) {
	           		for (key in listaRubrica) {
		           		var novaRubrica = new Option(listaRubrica[key], key);
		           		campoRubrica.options[campoRubrica.length] = novaRubrica
					}
				}
        	);
		}
	}

	function atualizarValor() {
		var quantidade = document.getElementById("quantidade");
		var valorUnitario = document.getElementById("valorUnitario");
		var valor = document.getElementById("valor");
		var valorRetorno = null; 
		if (quantidade != null && quantidade.value != '' && valorUnitario != null && valorUnitario.value != '') {
			var valorQuantidade = quantidade.value;
						
			AjaxService.obterMultiplicacaoValores(quantidade.value, valorUnitario.value, 
		           	function(listaValorRetorno) {
		           		for (key in listaValorRetorno) {
							valorRetorno = listaValorRetorno[key];

							var valorInteiroRetorno = valorRetorno;
							valorInteiroRetorno = Math.floor((valorInteiroRetorno*10000+0.5)/10000) + '';
							if (valorInteiroRetorno.length > 12) {
								valor.innerHTML = '';
								alert('O Valor total � superior ao suportado');
								document.getElementById("botaoGerarParcelas").disabled = true;
								document.getElementById('valorHidden').value = '';
								return false;
							} else {
								valorRetorno = formataMoeda(valorRetorno);
								document.getElementById('valorHidden').value = valorRetorno;
								valor.innerHTML = 'R$ ' + valorRetorno;
								document.getElementById("botaoGerarParcelas").disabled = false;
							}
 						}
					}
	        	);
			
		} else {
			valor.innerHTML = "";
			valorRetorno = "";
			document.getElementById('valorHidden').value = '';
		}
	} 

	function formataMoeda(num) {

		   x = 0;

		   if(num<0) {
		      num = Math.abs(num);
		      x = 1;
		   }   

		   if(isNaN(num)) {
			   num = "0";
		   }

		   cents = Math.floor((num*10000+0.5)%10000);

		   num = Math.floor((num*10000+0.5)/10000).toString();

		   if(cents < 10) {
			   cents = "0" + cents;
		   } else if(cents < 100) {
			   cents = "" + cents;
		   } else if(cents < 1000) {
			   cents = "" + cents;
		   }
		   
	       for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	         num = num.substring(0,num.length-(4*i+3))+'.'+num.substring(num.length-(4*i+3));   

           ret = num + ',' + cents.toString().substring(0,2);   

           if (x == 1) 
               ret = ' - ' + ret;

           return ret;
	}
	
	/*function validarContratoPorCliente() {
		var idPontoConsumo = null;
		if (document.forms[0].idPontoConsumo != null) {
			for (var i=0; i < document.forms[0].idPontoConsumo.length; i++)  {
			   if (document.forms[0].idPontoConsumo[i].checked) {
			      	idPontoConsumo = document.forms[0].idPontoConsumo[i].value;
			      	break;				
			   }
			}	
		}
		
	    if (idPontoConsumo == null || idPontoConsumo == '') {
	    	habilitarInicioCobrancaApos();		           				
			document.getElementById('indicadorInicioCobrancaApos').checked = true;
			document.getElementById('indicadorInicioCobrancaApos').disabled = true;
			document.getElementById('indicadorInicioCobrancaDias').checked = false;
			document.getElementById('indicadorInicioCobrancaDias').disabled = true;
	    }
	}

	function validarContratoPorPontoConsumo() {
		var idPontoConsumo = null;
		for (var i=0; i < document.forms[0].idPontoConsumo.length; i++)  {
		   if (document.forms[0].idPontoConsumo[i].checked) {
		      	idPontoConsumo = document.forms[0].idPontoConsumo[i].value;
		      	break;				
		   }
		}		   
	    if (idPontoConsumo != null && idPontoConsumo != '') {
		    animatedcollapse.show('conteinerPeriodicidade');
		    document.getElementById('indicadorInicioCobrancaApos').disabled = false;
			document.getElementById('indicadorInicioCobrancaDias').disabled = false;
	    }
	
	}*/

	
	
	addLoadEvent(init);

</script>

<form method="post" id="creditoDebitoForm" name="creditoDebitoForm">
	<token:token></token:token>
	<input name="idCliente" type="hidden" id="idCliente" value="${creditoDebitoVO.idCliente}">
	<input name="idImovel" type="hidden" id="idImovel" value="${creditoDebitoVO.idImovel}">
	<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${creditoDebitoVO.nomeCompletoCliente}">
	<input name="documentoFormatado" type="hidden" id="documentoFormatado" value="${creditoDebitoVO.documentoFormatado}">
	<input name="enderecoFormatadoCliente" type="hidden" id="enderecoFormatadoCliente" value="${creditoDebitoVO.enderecoFormatadoCliente}">
	<input name="emailCliente" type="hidden" id="emailCliente" value="${creditoDebitoVO.emailCliente}">
	<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${creditoDebitoVO.nomeFantasiaImovel}">
	<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${creditoDebitoVO.matriculaImovel}">
	<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${creditoDebitoVO.cidadeImovel}">
	<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${creditoDebitoVO.enderecoImovel}">
	<input name="exibirtaxaJuros" type="hidden" id="exibirtaxaJuros">
	<input name="valorHidden" type="hidden" id="valorHidden" value="${creditoDebitoVO.valorHidden}">
	<input name="valorUnitarioHidden" type="hidden" id="valorUnitarioHidden" value="${creditoDebitoVO.valorUnitarioHidden}">


	<fieldset id="incluirCreditoDebito" class="conteinerPesquisarIncluirAlterar conteinerCreditoDebitoRealizar">
		<c:choose>
			<c:when test="${creditoDebitoVO.idImovel ne null && creditoDebitoVO.idImovel > 0}">
				<a class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png"
				data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
					<fieldset class="coluna">
						<label class="rotulo">Descri��o:</label>
						<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.nomeFantasiaImovel}" /></span><br />
						<label class="rotulo">Matr�cula:</label>
						<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.matriculaImovel}" /></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">Cidade:</label>
						<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.cidadeImovel}" /></span><br />
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoImovel}" /></span><br />
					</fieldset>
				</fieldset>
			</c:when>
			<c:otherwise>
				<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">
					Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosCliente" class="conteinerDadosDetalhe">
					<fieldset class="coluna"><label class="rotulo">Cliente:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.nomeCompletoCliente}" /></span><br />
						<label class="rotulo">CPF/CNPJ:</label> <span class="itemDetalhamento"><c:out value="${creditoDebitoVO.documentoFormatado}" /></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoFormatadoCliente}" /></span><br />
						<label class="rotulo">E-mail:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.emailCliente}" /></span>
						<br />
					</fieldset>
				</fieldset>
			</c:otherwise>
		</c:choose>

		<hr class="linhaSeparadora1" />

		<c:if test="${listaPontoConsumo ne null}">
			<a class="linkExibirDetalhes" href="#" rel="toggle[conteinerPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">
				Im�veis e Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="conteinerPontoConsumo" class="conteinerBloco">
				<display:table class="dataTableGGAS" name="listaPontoConsumo" sort="list" id="pontoConsumo" style="width: 100%"
					excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
					<display:column style="text-align: center" sortable="false">
						<input type="radio" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"
							onclick="selecionarPontoConsumo(${pontoConsumo.imovel.chavePrimaria})"
							<c:if test="${creditoDebitoVO.idPontoConsumo == pontoConsumo.chavePrimaria}">checked="checked"</c:if>/>
					</display:column>
					<display:column title="Im�vel - Ponto Consumo">
						<c:choose>
							<c:when test="${nomeImovel ne null}">
								<c:out value="${nomeImovel}" /> - <c:out value="${pontoConsumo.chavePrimaria}" /> - <c:out value="${pontoConsumo.descricao}" />
							</c:when>
							<c:otherwise>
								<c:out value="${pontoConsumo.imovel.nome}" /> - <c:out value="${pontoConsumo.chavePrimaria}" />-<c:out value="${pontoConsumo.descricao}" />
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column property="segmento.descricao" title="Segmento" />
					<display:column property="situacaoConsumo.descricao" title="Situa��o" />
				</display:table>
			</fieldset>
		</c:if>

		<hr class="linhaSeparadora1" />
	
		<fieldset id="dadosCreditoDebitoRealizar" class="conteinerBloco">
			<fieldset class="coluna">
				<fieldset>
					<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Tipo de Lan�amento:</label>
					<input class="campoRadio" type="radio" id="indicadorDebitoSim" name="indicadorCreditoDebito" value="true" 
						onclick="habilitarDesabilitarCampos(false); carregarRubrica(true);"
						<c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'true'}">checked="checked" </c:if>>
					<label class="rotuloRadio" for="indicadorDebitoSim">D�bito</label>
					<input class="campoRadio" type="radio" id="indicadorDebitoNao" name="indicadorCreditoDebito" value="false"
						onclick="habilitarDesabilitarCampos(true); carregarRubrica(false);"
						<c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'false'}">checked="checked" </c:if>>
					<label class="rotuloRadio" for="indicadorDebitoNao">Cr�dito</label> 
					<br class="quebraLinha" />
			
					<label class="rotulo campoObrigatorio" for="idRubrica"><span class="campoObrigatorioSimbolo">* </span>Rubrica:</label>
					<select name="idRubrica" id="idRubrica" class="campoSelect" onchange="carregarCampos(this)">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaRubrica}" var="rubrica">
							<option value="<c:out value="${rubrica.chavePrimaria}"/>"
								<c:if test="${creditoDebitoVO.idRubrica == rubrica.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${rubrica.descricao}" />
							</option>
						</c:forEach>
					</select>
					<br class="quebraLinha" />
				
					<fieldset id="conteinerOrigemCredito">
						<label class="rotulo campoObrigatorio" for="idCreditoOrigem"><span class="campoObrigatorioSimbolo">* </span>Origem do Cr�dito:</label>
						<select name="idCreditoOrigem" id="idCreditoOrigem" class="campoSelect" disabled="disabled">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCreditoOrigem}" var="creditoOrigem">
								<option value="<c:out value="${creditoOrigem.chavePrimaria}"/>"
									<c:if test="${creditoDebitoVO.idCreditoOrigem == creditoOrigem.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${creditoOrigem.descricao}" />
								</option>
							</c:forEach>
						</select>
					</fieldset>
				</fieldset>
		
				<label class="rotulo campoObrigatorio" for="idCreditoOrigem"><span class="campoObrigatorioSimbolo">* </span>Quantidade:</label>
				<input class="campoTexto" type="text" id="quantidade" name="quantidade" maxlength="20" size="22" value="${creditoDebitoVO.quantidade}"		
					onkeypress="return formatarCampoDecimalPositivo(event, this, 15, 4);" onchange="atualizarValor()">
				<br class="quebraLinha2" />
				<label class="rotulo campoObrigatorio" for="valorUnitario"><span class="campoObrigatorioSimbolo">* </span>Valor Unit�rio:</label>
				<input	class="campoTexto campoValorReal valorUnitario" type="text" id="valorUnitario" name="valorUnitario" size="20" maxlength="19"
					value="${creditoDebitoVO.valorUnitario}" onkeypress="return formatarCampoDecimalPositivo(event, this, 14, 2);"
					onblur="aplicarMascaraNumeroDecimal(this, 2);" onchange="aplicarMascaraNumeroDecimal(this, 2); atualizarValor()">
				<br class="quebraLinha" />	
				
				<label class="rotulo rotuloValorTotal" for="valor">Valor Total:</label>
				<span id="valor" class="itemDetalhamento"><c:out value="${creditoDebitoVO.valor}"> </c:out></span>
				<br class="quebraLinha" />			
				<fieldset id="conteinerValorEntrada">
					<label class="rotulo campoObrigatorio" for="valorEntrada"> <span class="campoObrigatorioSimbolo">* </span>Valor de Entrada:</label>
					<input class="campoTexto campoValorReal" type="text" id="valorEntrada" name="valorEntrada" maxlength="16"
						value="${creditoDebitoVO.valorEntrada}" disabled="disabled" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);"
						onblur="aplicarMascaraNumeroDecimal(this, 2);" onchange="aplicarMascaraNumeroDecimal(this, 2);">
				</fieldset>
				<label class="rotulo campoObrigatorio labelParcelas" for="parcelas"> <span class="campoObrigatorioSimbolo">* </span>Parcelas:</label>
				<input class="campoTexto parcelas" type="text" id="parcelas" name="parcelas" maxlength="3" size="1" value="${creditoDebitoVO.parcelas}"
					onkeypress="return formatarCampoInteiro(event,3);"><br />
				<br class="quebraLinha" />
				<fieldset id="conteinerTaxaJuros">
					<label class="rotulo campoObrigatorio" for="taxaJuros"><span id="spanAsteriscoTaxaJuros" class="campoObrigatorioSimbolo">* </span>Taxa de Juros:</label>
					<input class="campoTexto campoHorizontal" type="text" id="taxaJuros" name="taxaJuros" size="3" maxlength="6" value="${creditoDebitoVO.taxaJuros}"
						disabled="disabled" onkeypress="return formatarCampoDecimalPositivo(event, this, 3, 2)" onblur="aplicarMascaraNumeroDecimal(this, 2);">
					<label class="rotuloHorizontal rotuloInformativo" for="taxaJuros">%</label>
					<select name=idPeriodicidadeJuros id="idPeriodicidadeJuros" class="campoSelect" disabled="disabled">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPeriodicidadeJuros}" var="periodicidadeJuros">
							<option value="<c:out value="${periodicidadeJuros.chavePrimaria}"/>"
								<c:if test="${creditoDebitoVO.idPeriodicidadeJuros == periodicidadeJuros.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${periodicidadeJuros.descricao}" />
							</option>
						</c:forEach>
					</select>
				</fieldset>
				<fieldset id="conteinerPeriodicidade" >
					<label class="rotulo campoObrigatorio" for="idPeriodicidade"><span class="campoObrigatorioSimbolo">* </span>Periodicidade:</label>
					<select name="idPeriodicidade" id="idPeriodicidade" class="campoSelect campoSelectObrigatorio"  onchange="verificarPeriodicidade(this)">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaPeriodicidade}" var="periodicidade">
							<option value="<c:out value="${periodicidade.chavePrimaria}"/>"
								<c:if test="${creditoDebitoVO.idPeriodicidade == periodicidade.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${periodicidade.descricao}" />
							</option>
						</c:forEach>
					</select>
					
					<br />
					
					<fieldset id="containerMelhorDiaVencimento">
						<label class="rotulo rotulo2Linhas campoObrigatorio" for="melhorDiaVencimento"><span class="campoObrigatorioSimbolo">* </span>Melhor dia para vencimento:</label>
						<select id="melhorDiaVencimento" class="campoSelect campo2Linhas" name="melhorDiaVencimento">
							<option value="-1">Selecione</option>
							<c:forEach begin="1" end="31" var="dia">
								<option value="${dia}" <c:if test="${creditoDebitoVO.melhorDiaVencimento eq dia}">selected="selected"</c:if>>${dia}</option>
						  	</c:forEach>
						</select>
					</fieldset>
				</fieldset>
			</fieldset>
	
			<fieldset class="colunaFinal"><label class="rotulo rotuloVertical"	for="complementoDescricao">Descri��o:</label>
				<textarea id="complementoDescricao" name="complementoDescricao" class="campoTexto campoVertical" rows="3" 
					onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>',
					 'formatarCampoNome(event)');" onkeypress="return validarTextArea(event,this,${tamanhoDescricao});">${creditoDebitoVO.complementoDescricao}</textarea>
				<br class="quebra2Linhas" />
		
				<fieldset id="containerInicioCredito">
					<legend>In�cio do Cr�dito</legend>
					<fieldset class="pesquisarClienteFundo">
						<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Iniciar cr�dito ap�s:</label>
						<input class="campoData" id="dataInicioCredito" name="dataInicioCredito" type="text" size="8" value="${creditoDebitoVO.dataInicioCredito}">
					</fieldset>
				</fieldset>
				<fieldset id="containerInicioCobranca">
				<legend>In�cio da Cobran�a</legend>
				<fieldset class="pesquisarClienteFundo">
					<input type="radio" class="campoRadio" name="indicadorInicioCobranca" id="indicadorInicioCobrancaApos" value="true"
						<c:if test="${creditoDebitoVO.indicadorInicioCobranca eq 'true' || creditoDebitoVO.indicadorInicioCobranca eq ''}">checked="checked"</c:if>
						onclick="habilitarInicioCobrancaApos();"> 
					<label class="rotulo rotuloHorizontal campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Iniciar cobran�a ap�s:</label>
					<input class="campoData campoHorizontal" name="dataInicioCobranca" id="dataInicioCobranca" type="text" size="8"
						value="${creditoDebitoVO.dataInicioCobranca}">
					<br class="quebraLinha2" />
					<c:if test="${creditoDebitoVO.idImovel ne null && creditoDebitoVO.idImovel > 0}">
					<input type="radio" class="campoRadio" name="indicadorInicioCobranca" id="indicadorInicioCobrancaDias" value="false"
						<c:if test="${creditoDebitoVO.indicadorInicioCobranca eq 'false'}">checked="checked"</c:if> onclick="habilitarInicioCobranca();"> 
					<label class="rotulo rotuloHorizontal campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Iniciar cobran�a:</label> 
					<input class="campoTexto campoHorizontal" name="qtdDiasCobranca" id="qtdDiasCobranca" type="text" size="1" value="${creditoDebitoVO.qtdDiasCobranca}" disabled="disabled" onkeypress="return formatarCampoInteiro(event,3);"> 
					<label class="rotuloHorizontal rotuloInformativo"> dias ap�s </label> 
					<select name="idInicioCobranca" id="idInicioCobranca" class="campoSelect" disabled="disabled">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaAposInicioCobranca}" var="inicioCobranca">
							<option value="<c:out value="${inicioCobranca.chavePrimaria}"/>"
								<c:if test="${creditoDebitoVO.idInicioCobranca == inicioCobranca.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${inicioCobranca.descricao}" />
							</option>
						</c:forEach>
					</select>
				</c:if>
				
				</fieldset>
			</fieldset>
		</fieldset>
	
		<fieldset id="containerBotoesInclusaoCreditoDebito" class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2" type="button" id="botaoGerarParcelas" value="Gerar Parcelas" onclick="gerarParcelas();"> <input
			class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
		</fieldset>
	</fieldset>
	
	<c:if test="${listaDadosParcelas ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaDadosParcelas" sort="list" id="dadosParcela"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			<display:column property="numeroParcela" title="Parcelas" />
			<display:column title="Saldo Inicial (R$)">
				<fmt:formatNumber value="${dadosParcela.saldoInicial}" minFractionDigits="2" />
			</display:column>
			<c:if test="${dadosParcela.juros ne null}">
				<display:column title="Juros (R$)">
					<fmt:formatNumber value="${dadosParcela.juros}" minFractionDigits="2" />
				</display:column>
			</c:if>
			<c:if test="${dadosParcela.amortizacao ne null}">
				<display:column title="Amortiza��o (R$)">
					<fmt:formatNumber value="${dadosParcela.amortizacao}" minFractionDigits="2" />
				</display:column>
			</c:if>
			<display:column title="Total">
				<fmt:formatNumber value="${dadosParcela.total}" minFractionDigits="2" />
			</display:column>
			<display:column title="Saldo Final (R$)">
				<fmt:formatNumber value="${dadosParcela.saldoFinal}" minFractionDigits="2" />
			</display:column>
		</display:table>
	</c:if>
	</fieldset>
	<fieldset class="conteinerBotoes">
		<input class="bottonRightCol" type="button" value="Cancelar" onclick="cancelar();">
		<c:if test="${listaDadosParcelas ne null}">
			<input id="botaoSalvar" class="bottonRightCol2 botaoGrande1" type="button" value="Salvar" onclick="salvar();">
		</c:if>
	</fieldset>
</form>
