
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<div id="modalDetalhamentoLeitura"  class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalhamento de <span id="detalhamentoLeitura"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 divSelectAnormalidade">
                        <label for="detalhamentoAnormalidade" class="float-none">Anormalidade de Leitura</label>
                        <select class="form-control form-control-sm" id="detalhamentoAnormalidade">
                            <option value=""></option>
                            <c:forEach var="anormalidade" items="${anormalidades}">
                                <option value="${anormalidade.chavePrimaria}">${anormalidade.descricao}</option>
                            </c:forEach>
                        </select>
                        <input type="hidden" id="chaveAlterarAnormalidade" />
                    </div>                
                    <div class="col-sm-12">
                        <label for="detalhamentoAnormalidade">Anormalidade de Consumo</label>
                        <input type="text" disabled="" class="form-control form-control-sm" id="detalhamentoAnormalidadeConsumo" value="">
                    </div>
                    <div class="col-sm-12">
                        <label for="detalhamentoAnormalidade">Leitura Anterior</label>
                        <input type="text" class="form-control form-control-sm" id="leituraAnterior" onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this, 6,2)" value="">
                    </div>
                    <div class="col-sm-12">
                        <label for="detalhamentoAnormalidade">Data Leitura Anterior</label>
                        <input type="text" class="form-control form-control-sm" id="dataLeituraAnterior" value="">
                    </div> 
                    <div class="col-sm-12">
                        <label for="detalhamentoAnormalidade">Leitura Atual</label>
                        <input type="text" class="form-control form-control-sm" id="leituraAtual" onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this, 6,2)" value="">
                    </div>
                    <div class="col-sm-12">
                        <label for="detalhamentoAnormalidade">Data Leitura Atual</label>
                        <input type="text" class="form-control form-control-sm" id="dataLeituraAtual" value="">
                    </div>                                                                                                     
                    <div class="col-sm-12">
                        <label for="detalhamentoOrigem">Origem da Medi��o</label>
                        <input type="text" disabled="" class="form-control form-control-sm" id="detalhamentoOrigem" value="">
                    </div>
                    
					<input type="hidden" id="chaveHistoricoMedicao" value="">
					<input type="hidden" id="chavePontoConsumo" value="">
					
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="salvarConsitir" class="btn btn-primary btn-sm float-right mr-1">
                    <i class="fa fa-check"></i> Salvar e Consistir
                </button>
                            
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">
                    <i class="fa fa-times fa-fw"></i>Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
