
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<style>
    #tablePesquisaPontoConsumo td {
        word-break: break-word;
    }
    #tablePesquisaPontoConsumo thead th:nth-child(1){
        width: 10px !important;
    }
    #tablePesquisaPontoConsumo thead th:nth-child(2){
        width: 20px !important;
    }
</style>
<div class="modal fade" id="modalPontoConsumo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Buscar Ponto de Consumo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-xl-10" >
                            <input id="descricaoPontoConsumo_modal" class="form-control form-control-sm" placeholder="Digite 3 ou mais caracteres" autofocus/>
                        </div>
                        <div class="col-xl-2" >
                            <button id="botaoBuscarPontoConsumo" name="button" class="btn btn-primary btn-sm mb-1 mr-1" type="button">Pesquisar</button>
                        </div>
                    </div>
                </div>
                <div class="containerTabelaPontoConsumo" style="display: none">
                    <hr/>
                    <div class="table-responsive pt-3">
                        <table id="tablePesquisaPontoConsumo" class="table table-bordered table-striped table-hover" style="width: 100% !important;">
                            <thead class="thead-ggas-bootstrap">
                            <th scope="col" class="text-center" style="width: 10px !important">
                                <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
                                    <input id="checkAllPontoConsumo" type="checkbox"  name="checkAllPontoConsumo" class="custom-control-input">
                                    <label class="custom-control-label p-0" for="checkAllPontoConsumo"></label>
                                </div>
                            </th>
                            <th scope="col" class="text-center" style="width: 10px !important"></th>
                            <th scope="col" class="text-center">Im�vel</th>
                            <th scope="col" class="text-center">Ponto de Consumo</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnEscolherPontosConsumo">Adicionar</button>
            </div>
        </div>
    </div>
</div>
