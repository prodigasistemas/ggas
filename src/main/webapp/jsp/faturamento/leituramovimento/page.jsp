<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>
 <%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script>
    $(document).ready(function(){
        $('#chkEmLeitura').removeAttr("disabled");
        $('#chkGerado').removeAttr("disabled");
        $('#chkLeituraRetornada').removeAttr("disabled");
        $('#chkProcessado').removeAttr("disabled");

        $('#chkEmLeitura').prop("checked", true);
        $('#chkGerado').prop("checked", true);
        
		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'yyyy/mm',
			language: 'pt-BR',
		    viewMode: "months", 
		    minViewMode: "months"
		});

		$('.bootstrapDP').inputmask("9999/99",{placeholder:"_"});
		
    });
    
    function selecionarFoto(idRota, anoMesFaturamento, leituraMovimentoId){
    	var idRota = idRota;
    	var anoMesFaturamento = anoMesFaturamento;
    	var leituraMovimentoId = leituraMovimentoId;

    	$("#fotoColetorPopup").html("");
    	if(idRota != '' || anoMesFaturamento != '') {
    		AjaxService.obterFotoPorIdDadosMedicao(idRota, anoMesFaturamento, leituraMovimentoId, {
    			callback: function(urlFoto) {
    				 var inner = '';
    				 inner = inner +'<center>';
    				 inner = inner + '<img src="<c:url value="' + urlFoto + '"/>" style=" width:100%;">';
    				 inner = inner +'</center>';
    				 $("#fotoColetorPopup").append(inner);
    			}, async:false}
            );	 
    	}
    	
    	
    } 
    
</script>
<style>
    .select2-selection--multiple:before {
        visibility:hidden;
    }
</style>


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link type="text/css" rel="stylesheet" href="${ctx}/js/lib/daterangepicker/daterangepicker.css"/>
<link type="text/css" rel="stylesheet" href="${ctx}/js/lib/datetimepicker/bootstrap-datetimepicker.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctxWebpack}/dist/modulos/faturamento/analiseMedicao/index.css"/>
<input type="hidden" value="${idCronograma}" id="cronograma"/>
<div class="card">
    <div class="card-header">
        <h5 class="card-title mb-0">An�lise de Medi��o</h5>
    </div>
    <div class="card-body">
        <c:choose>
            <c:when test="${empty idCronograma}">
                <div class="alert alert-primary fade show" role="alert">
                    <i class="fa fa-question-circle"></i>
                    Utilize esta tela para acompanhar os registros de medi��o. <br/>Preencha os campos abaixo para filtrar os registros de medi��o e clique no bot�o
                    <b>'Pesquisar'</b> para exibir sua pesquisa.<br/>Para os registros que ainda
                    n�o foram processados, voc� pode alterar as informa��es de medi��o clicando no �cone de edi��o <b><i class="fa fa-sm fa-edit"><c:out value=""/></i></b>
                    e posteriormente em <b>Salvar Leituras</b>.
                    <br/> Caso queira executar o processo de 'Registrar Leituras', acesse a fun��o atrav�s do menu <b>Cronograma</b>.
                </div>
            </c:when>
            <c:otherwise>
                <div class="alert alert-primary fade show" role="alert">
                    <i class="fa fa-question-circle"></i>
                    Utilize esta tela para acompanhar os registros de medi��o, para o cronograma selecionando anteriormente.
                    <br/>Preencha os campos abaixo para filtrar os registros de medi��o e clique no bot�o <b>'Pesquisar'</b> para exibir sua pesquisa.
                    <br/>Voc� pode alterar as informa��es de medi��o dos registros clicando no �cone de edi��o <b><i class="fa fa-edit fa-sm"><c:out value=""/></i></b>
                    e posteriormente em <b>Salvar Leituras</b>.
                    <br/> Ao finalizar as altera��es, clique em <b>Registrar Leituras</b> para executar o processo.
                </div>
            </c:otherwise>
        </c:choose>

        <form id="formPesquisarLeituras">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body bg-light">
                            <h5>Filtrar Leituras</h5>
                            <div class="row">
                                <div class="col-md-12 col-lg-3">
                                    <label for="grupoFaturamento">Grupo de Faturamento <span class="text-danger">*</span></label>
                                    <select class="form-control form-control-sm" name="grupoFaturamento" id="grupoFaturamento"
                                            <c:if test="${not empty idCronograma}"> disabled="" </c:if>>
                                        <option></option>
                                        <c:forEach var="grupo" items="${grupos}">
                                            <option value="${grupo.chavePrimaria}">${grupo.descricao}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-md-12 col-lg-3">
                                    <label class="float-none" for="rota">Rota Utilizada <span class="text-danger">*</span></label>
                                    <select class="form-control form-control-sm" name="rotaUtilizada" id="rota" multiple></select>
                                </div>
                                
                                <div class="col-md-12 col-lg-3">
                                    <label for="referencia">Refer�ncia <span class="text-danger">*</span></label>
                                    <input class="form-control form-control-sm bootstrapDP" name="referencia" id="referencia" value="${referencia}"
                                           maxlength="10" type="text"
                                            <c:if test="${not empty  idCronograma}"> disabled="" </c:if>>
                                    </input>
                                </div>
                                
                                <div class="col-md-12 col-lg-3">
                                    <c:if test="${empty ciclo}">
                                        <c:set var="ciclo" value="1"/>
                                    </c:if>
                                    <label for="grupoFaturamento">Ciclo <span class="text-danger">*</span></label>
                                    <input class="form-control form-control-sm" name="ciclo" id="ciclo" value="${ciclo}"
                                           maxlength="2"
                                            <c:if test="${not empty  idCronograma}"> disabled="" </c:if>
                                           onkeypress=" return formatarCampoInteiro(event, 255)">
                                    </input>
                                </div>                                
                                <div class="col-md-12 col-lg-6">
                                    <label for="rota">Segmento</label>
                                    <select class="form-control form-control-sm" id="segmento">
                                        <option></option>
                                        <c:forEach var="segmento" items="${segmentos}">
                                            <option value="${segmento.descricao}">${segmento.descricao}</option>
                                        </c:forEach>
                                    </select>
                                </div>

                                <div class="col-md-12 col-lg-6">
                                    <label for="periodoLeitura">Per�odo da Leitura</label>
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar-alt text-secondary"><c:out value=""/></i></span>
                                        </div>
                                        <input  type="text" class="form-control form-control-sm" id="periodoLeitura" value="${range}"/>
                                        <div class="input-group-append">
                                            <a class="input-group-text" href="javascript:void(0)" id="limparData" title="Clique para limpar o campo de data">
                                                <i class="fa fa-times text-secondary"><c:out value=""/></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-6">
                                    <label for="selectPontoConsumo" class="float-none">Ponto de Consumo</label>
                                    <select id="selectPontoConsumo" class="form-control form-control-sm" multiple></select>
                                </div>

                                <div class="col-sm-12 col-md-6">
                                    <label for="rota" class="float-none">Situa��o</label>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-4 col-xl-3">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input id="chkGerado" type="checkbox"
                                                       class="custom-control-input"
                                                       name="situacaoLeitura"
                                                <c:if test="${not empty idCronograma}"> disabled </c:if>
                                                <c:if test="${empty idCronograma}"> checked </c:if>
                                                       value="1">
                                                <label class="custom-control-label pt-1 check-default" for="chkGerado">Gerado</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-8 col-xl-3">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input id="chkEmLeitura" type="checkbox"
                                                       class="custom-control-input"
                                                       name="situacaoLeitura"
                                                <c:if test="${not empty idCronograma}"> disabled </c:if>
                                                <c:if test="${empty idCronograma}"> checked </c:if>
                                                       value="2">
                                                <label class="custom-control-label pt-1 check-warning" for="chkEmLeitura">Em Leitura</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-4 col-xl-3">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input id="chkLeituraRetornada" type="checkbox"
                                                       class="custom-control-input"
                                                       name="situacaoLeitura"
                                                       checked
                                                <c:if test="${not empty idCronograma}"> disabled </c:if>
                                                       value="3">
                                                <label class="custom-control-label pt-1 check-info" for="chkLeituraRetornada">Leitura Informada</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-8 col-xl-3">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input id="chkProcessado" type="checkbox"
                                                       class="custom-control-input"
                                                       name="situacaoLeitura"
                                                <c:if test="${not empty idCronograma}"> disabled </c:if>
                                                <c:if test="${empty idCronograma}"> checked </c:if>
                                                       value="4">
                                                <label class="custom-control-label pt-1 check-success" for="chkProcessado">Processado</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <fieldset class="form-group border p-2">
                                        <legend class="w-auto">Anormalidade de Leitura:</legend>
                                        <label for="anormalidade" class="float-none">Anormalidade</label>
                                        <select class="form-control form-control-sm" id="anormalidade" multiple>
                                            <c:forEach var="anormalidade" items="${anormalidades}">
                                                <option value="${anormalidade.chavePrimaria}">${anormalidade.descricao}</option>
                                            </c:forEach>
                                        </select>

                                        <label for="rota" class="float-none">Anormalidade bloqueia faturamento?</label><br>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="anormLeituraBloqueiaTodos" name="anormLeituraBloqueia"
                                                   class="custom-control-input" value="" checked>
                                            <label class="custom-control-label" for="anormLeituraBloqueiaTodos">Todos</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="anormLeituraBloqueiaSim" name="anormLeituraBloqueia"
                                                   class="custom-control-input" value="true">
                                            <label class="custom-control-label" for="anormLeituraBloqueiaSim">Sim</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="anormLeituraBloqueiaNao" name="anormLeituraBloqueia"
                                                   class="custom-control-input" value="false">
                                            <label class="custom-control-label" for="anormLeituraBloqueiaNao">N�o</label>
                                        </div>
                                        <br>
                                        <label for="rota" class="float-none">Possui Anormalidade?</label><br>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="possuiAnormalidadeLeituraTodos" name="possuiAnormalidadeLeitura"
                                                   class="custom-control-input" value="" checked>
                                            <label class="custom-control-label" for="possuiAnormalidadeLeituraTodos">Todos</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="possuiAnormalidadeLeituraSim" name="possuiAnormalidadeLeitura"
                                                   class="custom-control-input" value="true">
                                            <label class="custom-control-label" for="possuiAnormalidadeLeituraSim">Sim</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="possuiAnormalidadeLeituraNao" name="possuiAnormalidadeLeitura"
                                                   class="custom-control-input" value="false">
                                            <label class="custom-control-label" for="possuiAnormalidadeLeituraNao">N�o</label>
                                        </div>                                        
                                    </fieldset>
                                </div>

                                <div class="col-md-12 col-lg-6">
                                    <fieldset class="form-group border p-2">
                                        <legend class="w-auto">Anormalidade de Consumo:</legend>
                                        <label for="anormalidadeConsumo" class="float-none">Anormalidade:</label>
                                        <select class="form-control form-control-sm" id="anormalidadeConsumo" multiple>
                                            <c:forEach var="anormalidadeConsumo" items="${anormalidadeConsumo}">
                                                <option value="${anormalidadeConsumo.chavePrimaria}">${anormalidadeConsumo.descricao}</option>
                                            </c:forEach>
                                        </select>

                                        <label for="rota" class="float-none">Anormalidade bloqueia faturamento?</label><br>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="anormConsumoBloqueiaTodos" name="anormConsumoBloqueia"
                                                   class="custom-control-input" value="" checked>
                                            <label class="custom-control-label" for="anormConsumoBloqueiaTodos">Todos</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="anormConsumoBloqueiaSim" name="anormConsumoBloqueia"
                                                   class="custom-control-input" value="true">
                                            <label class="custom-control-label" for="anormConsumoBloqueiaSim">Sim</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="anormConsumoBloqueiaNao" name="anormConsumoBloqueia"
                                                   class="custom-control-input" value="false">
                                            <label class="custom-control-label" for="anormConsumoBloqueiaNao">N�o</label>
                                        </div>
                                        <br>
                                        <label for="rota" class="float-none">Possui Anormalidade?</label><br>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="possuiAnormalidadeConsumoTodos" name="possuiAnormalidadeConsumo"
                                                   class="custom-control-input" value="" checked>
                                            <label class="custom-control-label" for="possuiAnormalidadeConsumoTodos">Todos</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="possuiAnormalidadeConsumoSim" name="possuiAnormalidadeConsumo"
                                                   class="custom-control-input" value="true">
                                            <label class="custom-control-label" for="possuiAnormalidadeConsumoSim">Sim</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="possuiAnormalidadeConsumoNao" name="possuiAnormalidadeConsumo"
                                                   class="custom-control-input" value="false">
                                            <label class="custom-control-label" for="possuiAnormalidadeConsumoNao">N�o</label>
                                        </div>                                            
                                    </fieldset>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <button id="botaoPesquisar" data-style="expand-left" type="submit" class="btn btn-primary btn-sm float-right ladda-button">
                                        <i class="fa fa-search"></i> <span class="ladda-label">Pesquisar</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="row mt-3">
            <div class="col-sm-12">
                <div class="ocultarTabela containerTabela">
                    <table id="tabelaLeituras" class="table table-bordered table-striped table-hover">
                        <thead class="thead-ggas-bootstrap">
                            <th scope="col" class="text-center">
                                <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
                                    <input id="checkAllAuto" type="checkbox"  name="checkAllAuto" class="custom-control-input">
                                    <label class="custom-control-label p-0" for="checkAllAuto"></label>
                                </div>
                            </th>
                            <th class="text-center">Tipo Anormalidade</th>
                            <th class="text-center anormalidade">Anormalidade de Leitura</th>
                            <th class="text-center" style="width:90px;">Situa��o</th>
                            <th class="text-center" >Ponto de Consumo</th>
                            <th class="text-center" >M�s/Ano Ciclo</th>
                            <th class="text-center" >Situa��o PC</th>
                            <th class="text-center">Leitura Anterior</th>
                            <th class="text-center" >Leitura Atual</th>
                            <th class="text-center">Data de Leitura</th>
                            <th class="text-center">Press�o</th>
                            <th class="text-center">Temperatura</th>
							<th class="text-center">Foto</th>
                            <th class="text-center observacaoLeitura">Observa��o</th>							                            
                            <th class="text-center">Volume Medido</th>
                            <th class="text-center">FC PTZ</th>
                            <th class="text-center">Volume PTZ</th>
                            <th class="text-center">Anormalidade de Consumo</th>
                            <th class="text-center">Tipo de Consumo</th>
                            <th class="text-center">Consumo Apurado</th>
                            <th class="text-center acoes"></th>
                            <th class="text-center historico">Hist�rico</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col-sm-12">
                <c:if test="${not empty idCronograma and not empty rotas}">

                    <input id="idModulo" type="hidden" value="${atividadeRegistrarLeitura.modulo.chavePrimaria}"/>
                    <input id="idOperacao" type="hidden" value="${atividadeRegistrarLeitura.operacao.chavePrimaria}"/>
                    <input id="idGrupoFaturamento" type="hidden" value="${grupoFaturamento}"/>
                    <input id="idRota" type="hidden" value="${rotas.get(0)}"/>
                    <button type="button" id="registrarLeituras" class="btn btn-primary btn-sm float-right">
                        Registrar Leituras
                    </button>
                </c:if>

                <button type="button" id="solicitarReleitura" class="btn btn-primary btn-sm float-right mr-1" disabled="true">
                    Solicitar Releitura
                </button>

                <button type="button" id="salvarLeituras" class="btn btn-secondary btn-sm float-right mr-1" disabled="true">
                    <i class="fa fa-check"></i> Salvar Leituras
                </button>
            </div>
        </div>
    </div>
</div>

<jsp:include page="modalPesquisaPontoConsumo.jsp"/>
<jsp:include page="alterarDadosMedicao.jsp"/>
<jsp:include page="detalhamentoLeitura.jsp"/>
<jsp:include page="modalHistoricoLeitura.jsp"/>

<script src="${ctx}/js/lib/moment/moment-with-locales.min.js"></script>
<script src="${ctx}/js/lib/daterangepicker/daterangepicker.js"></script>
<script src="${ctx}/js/lib/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="${ctxWebpack}/dist/modulos/faturamento/analiseMedicao/index.js"></script>

