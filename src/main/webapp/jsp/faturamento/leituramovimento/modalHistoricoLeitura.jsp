
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<div id="modalHistoricoLeitura"  class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hist�rico de Leituras</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
		        <div class="row mt-3">
		            <div class="col-sm-12">
		                <div class="ocultarTabela containerTabelaHistorico">
		                    <table id="tabelaHistoricoLeitura" class="table table-bordered table-striped table-hover">
		                        <thead class="thead-ggas-bootstrap">
		                            <th class="text-center" >M�s/Ano Ciclo</th>
		                            <th class="text-center anormalidade">Anormalidade de Leitura</th>
		                            <th class="text-center">Data de Leitura Anterior</th>
		                            <th class="text-center">Leitura Anterior</th>		                            
		                            <th class="text-center">Data de Leitura</th>
		                            <th class="text-center">Leitura Atual</th>
		                            <th class="text-center">Consumo Medido</th>
		                            <th class="text-center">Consumo Apurado</th>
		                            <th class="text-center">Anormalidade de Consumo</th>
		                            <th class="text-center">FC PTZ</th>
		                            <th class="text-center">FC PCS</th>
									<th class="text-center">Quantidade dias de Consumo</th>		                            
		                        </thead>
		                        <tbody></tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
            </div>  
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">
                    <i class="fa fa-times fa-fw"></i>Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
