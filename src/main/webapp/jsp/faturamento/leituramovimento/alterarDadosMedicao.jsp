<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<!--
Copyright (C) <2018> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2018 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="modalAlterarDadosMedicao"  class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Alterar Dados de Medi��o</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label><b>Ponto de Consumo:</b> </label><label id="descricaoPontoConsumo"></label>
                    </div>
                    <div class="col-sm-12 divAlterarLeitura">
                        <label for="inputAlterarLeitura">Valor da Leitura</label>
                        <input id="inputAlterarLeitura" class="form-control form-control-sm"
                               onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this, 6,2)" />
                        <input type="hidden" id="chaveAlterarLeitura" />
                    </div>
                    <div class="col-sm-12 divAlterarPressao">
                        <label for="inputAlterarPressao">Valor da Press�o</label>
                        <input id="inputAlterarPressao" class="form-control form-control-sm"
                               onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this, 6,3)" />
                    </div>
                    <div class="col-sm-12 divAlterarTemperatura">
                        <label for="inputAlterarTemperatura">Valor da Temperatura</label>
                        <input id="inputAlterarTemperatura" class="form-control form-control-sm"
                               onkeypress="return formatarCampoDecimalPontoNaoPermiteNegativo(event,this, 6,1)" />
                    </div>
                    <div class="col-sm-12 divAlterarObservacao">
                        <label for="inputAlterarObservacao">Observa��o da Leitura</label>
                        <textarea id="inputAlterarObservacao" class="form-control form-control-sm"
                                  oninput='return formatarCampoTextoComLimite(event,this,100);'
                                  maxlength="100"
                                  ></textarea>
                        <small class="float-left">* Limite de at� 100 caracteres</small>
                    </div>
                    <div class="col-sm-12 divSelectAnormalidade">
                        <label for="selectAlterarAnormalidade" class="float-none">Anormalidade de Leitura</label>
                        <select class="form-control form-control-sm" id="selectAlterarAnormalidade">
                            <option value=""></option>
                            <c:forEach var="anormalidade" items="${anormalidades}">
                                <option value="${anormalidade.chavePrimaria}">${anormalidade.descricao}</option>
                            </c:forEach>
                        </select>
                        <input type="hidden" id="chaveAlterarAnormalidade" />
                    </div>
                    <div class="col-sm-12 divAlterarDataLeitura">
                        <label for="inputAlterarDataLeitura">Data de Leitura:</label>
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar-alt text-secondary"><c:out value=""/></i></span>
                            </div>
                            <input  type="text" class="form-control form-control-sm" id="inputAlterarDataLeitura"/>
                            <div class="input-group-append">
                                <a class="input-group-text" href="javascript:void(0)" id="limparAlterarDataLeitura"
                                   title="Clique para limpar o campo de data">
                                    <i class="fa fa-times text-secondary"><c:out value=""/></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">
                    <i class="fa fa-times fa-fw"></i>Cancelar
                </button>
                <button id="confirmarDadosMedicao" type="button" class="btn btn-sm btn-primary" data-dismiss="modal">
                    <i class="fa fa-check fa-fw"></i>Confirmar
                </button>
            </div>
        </div>
    </div>
</div>
