<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>

<h1 class="tituloInterno">
	Pesquisar Periodicidade <a class="linkHelp"
		href="<help:help></help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para pesquisar um registro espec�fico, informe os dados nos campos
	abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>,
	ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>
	para exibir todos. Para incluir uma novo registro clique em <span
		class="destaqueOrientacaoInicial">Incluir</span>
</p>


<script>
	$(document).ready(function() {

		var max = 0;

		$('.rotulo').each(function() {
			if ($(this).width() > max)
				max = $(this).width();
		});
		$('.rotulo').width(max);
	});

	function limparFormulario() {
		document.forms[0].habilitado[0].checked = true;
		$("#descricao").val("");
		$("#descricaoAbreviada").val("");
	}

	function pesquisar() {

		submeter("periodicidadeForm", "pesquisarPeriodicidade");

	}

	function detalhar(chave) {
		document.forms['periodicidadeForm'].chavePrimaria.value = chave;
		submeter("periodicidadeForm", "exibirDetalhamentoPeriodicidade");
	}

	function incluir() {
		location.href = '<c:url value="/exibirInserirPeriodicidade"/>';
	}

	function alterar() {
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms['periodicidadeForm'].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('periodicidadeForm', 'exibirAtualizarPeriodicidade');
		}
	}

	function remover() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				document.forms[0].chavePrimaria.value = "";
				submeter('periodicidadeForm', 'removerPeriodicidade');
			}
		}
	}
</script>


<form:form method="post" action="pesquisarPeriodicidade"
	id="periodicidadeForm" name="periodicidadeForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria">

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
			<label class="rotulo" id="rotuloDescricao" for="descricao">Descri��o:</label>
			<input class="campoTexto campoHorizontal" type="text"
				name="descricao" id="descricao" maxlength="30" size="30"
				value="${periodicidade.descricao}"
				onblur="this.value = removerEspacoInicialFinal(this.value);"
				onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');" />
			<br />
			<label class="rotulo" id="rotuloDescricaoAbreviada" for="descricaoAbreviada">Descri��o Abreviada:</label>
            <input class="campoTexto" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${periodicidade.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
            <br />
		</fieldset>

		<fieldset class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value="true"
				<c:if test="${habilitado eq true}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value="false"
				<c:if test="${habilitado eq false}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label> <input
				class="campoRadio" type="radio" name="habilitado" id="habilitado"
				value="" <c:if test="${empty habilitado}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>

		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarPeriodicidade">
				<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
					value="Pesquisar" type="submit">
			</vacess:vacess>
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo"
				id="limpar" value="Limpar" type="button"
				onclick="limparFormulario();">
		</fieldset>
	</fieldset>

	<c:if test="${listaPeriodicidade ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaPeriodicidade" sort="list"
			id="periodicidade"
			decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
			pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="pesquisarPeriodicidade">
			<display:column style="text-align: center; width: 25px"
				sortable="false"
				title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
				<input id="checkboxChavesPrimarias" type="checkbox"
					name="chavesPrimarias" value="${periodicidade.chavePrimaria}">
			</display:column>
			<display:column title="Ativo" style="width: 30px">
				<c:choose>
					<c:when test="${periodicidade.habilitado == true}">
						<img alt="Ativo" title="Ativo"
							src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo"
							src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column sortable="true" sortProperty="descricao"
				title="Descri��o">
				<a
					href="javascript:detalhar(<c:out value='${periodicidade.chavePrimaria}'/>);">
					<c:out value="${periodicidade.descricao}" />
				</a>
			</display:column>

			<display:column sortable="true" sortProperty="descricao"
				title="M�s Civil para Medi��o Di�ria">
				<a href="javascript:detalharTabelaAuxiliar(<c:out value='${periodicidade.chavePrimaria}'/>);">
				<span class="linkInvisivel"></span> <c:choose>
						<c:when test="${periodicidade.consideraMesCivil eq true}">
							<c:out value="Sim" />
						</c:when>
						<c:otherwise>
							<c:out value="N�o" />
						</c:otherwise>
					</c:choose> </a>
			</display:column>

		</display:table>
	</c:if>

	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaPeriodicidade}">
			<vacess:vacess param="atualizarPeriodicidade">
				<input id="botaoAlterar" name="Alterar" value="Alterar"
					class="bottonRightCol2" onclick="alterar()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerPeriodicidade">
				<input id="botaoRemover" name="Remover" value="Remover"
					class="bottonRightCol2" onclick="remover();" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="inserirPeriodicidade">
			<input id="botaoIncluir" name="button" value="Incluir"
				class="bottonRightCol2 botaoGrande1 botaoIncluir"
				onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>

</form:form>