<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="pt-BR" />

<h1 class="tituloInterno">
	Incluir Periodicidade <a class="linkHelp"
		href="<help:help>/*</help:help>" target="right"
		onclick="exibirJDialog('#janelaHelp');"></a>
</h1>

<p class="orientacaoInicial">
	Informe os dados abaixo e clique em <span
		class="destaqueOrientacaoInicial">Salvar</span> para finalizar.
</p>

<script>
	$(document).ready(function() {
		var max = 0;

		$('.rotulo').each(function() {
			if ($(this).width() > max)
				max = $(this).width();
		});
		$('.rotulo').width(max);

	});

	function limpar() {
		document.getElementById('quantidadeDias').value = "";
		document.getElementById('numeroMinimoDiasCiclo').value = "";
		document.getElementById('numeroMaximoDiasCiclo').value = "";
		document.getElementById('numeroCiclo').value = "";
		document.getElementById('descricao').value = "";
		document.getElementById('descricaoAbreviada').value = "";
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisarPeriodicidade"/>';
	}

	function init() {
		var inputDescricao = document.getElementById("descricao");
		var inputAbreviada = document.getElementById("descricaoAbreviada");

		inputAbreviada.maxLength = 5;
		inputDescricao.maxLength = 20;
	}
	
	addLoadEvent(init);
</script>

<form:form method="post" action="inserirPeriodicidade"
	id="periodicidadeForm" name="periodicidadeForm">

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
			<label class="rotulo campoObrigatorio"><span
				class="campoObrigatorioSimbolo">* </span>Descri��o:</label> <input
				class="campoTexto campoHorizontal" type="text" name="descricao"
				id="descricao" maxlength="20" size="30"
				value="${periodicidade.descricao}"
				onblur="this.value = removerEspacoInicialFinal(this.value);"
				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />

			<br /> <label class="rotulo">Descri��o Abreviada:</label> <input
				class="campoTexto campoHorizontal" type="text"
				name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6"
				size="6" value="${periodicidade.descricaoAbreviada}"
				onblur="this.value = removerEspacoInicialFinal(this.value);"
				onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
			
			<br /> <label class="rotulo campoObrigatorio"><span
				class="campoObrigatorioSimbolo">* </span>Quantidade Dias:</label> <input
				class="campoTexto campoHorizontal" type="text" name="quantidadeDias"
				id="quantidadeDias" maxlength="3" size="3"
				value="${periodicidade.quantidadeDias}"
				onkeypress="return formatarCampoInteiro(event)" /> 
			<br /> 
			
			<label class="rotulo campoObrigatorio"><span
				class="campoObrigatorioSimbolo">* </span>Quantidade Ciclo:</label> <input
				class="campoTexto campoHorizontal" type="text" name="quantidadeCiclo"
				id="numeroCiclo" maxlength="1" size="1"
				value="${periodicidade.quantidadeCiclo}"
				onkeypress="return formatarCampoInteiro(event)" /> 
			<br />
			
		</fieldset>

		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>N�mero M�nimo de dias:</label>
            <input class="campoTexto campoHorizontal" type="text" name="numeroMinimoDiasCiclo" id="numeroMinimoDiasCiclo" maxlength="3" size="3" value="${periodicidade.numeroMinimoDiasCiclo}" onkeypress="return formatarCampoInteiro(event)"/>
            <br />
            
            <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>N�mero M�ximo de dias:</label>
            <input class="campoTexto campoHorizontal" type="text" name="numeroMaximoDiasCiclo" id="numeroMaximoDiasCiclo" maxlength="3" size="3" value="${periodicidade.numeroMaximoDiasCiclo}" onkeypress="return formatarCampoInteiro(event)"/>
            <br />
            
            <br />
            <label class="rotulo" >M�s Civil para Medi��o Di�ria:</label>
            <input class="campoRadio" type="radio" value="true" name="consideraMesCivil" id="indicadorConsideraMesCivil" <c:if test="${periodicidade.consideraMesCivil eq 'true'}">checked</c:if>><label class="rotuloRadio">Sim</label>
            <input class="campoRadio" type="radio" value="false" name="consideraMesCivil" id="indicadorConsideraMesCivil" <c:if test="${periodicidade.consideraMesCivil ne 'true'}">checked</c:if>><label class="rotuloRadio">N�o</label>
        	<br/>
        	<br/>
		</fieldset>
		
		<p class="legenda">
			<span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios
		</p>
	</fieldset>

	<fieldset class="conteinerBotoes">
		<input name="Button" class="bottonRightCol2" value="Cancelar"
			type="button" onclick="javascript:cancelar();"> 
			
		<input name="Button" class="bottonRightCol2 bottonLeftColUltimo"
			id="limparFormulario" value="Limpar" type="button"
			onclick="javascript:limpar();"> 
			
		<input name="button"
			class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"
			type="submit" id="botaoSalvar" onclick="javascript:salvar();">
	</fieldset>

</form:form>
