<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%--<link type="text/css" rel="stylesheet" href="${ctxWebpack}/dist/modulos/faturamento/anormalidade/index.css"/>--%>

<div class="card">
    <div class="card-header">
        <h5 class="card-title mb-0">Lista de Anormalidade de Faturamento</h5>
    </div>
    <div class="card-body">

        <div class="alert alert-primary fade show" role="alert">
            <i class="fa fa-question-circle"></i>
            Utilize esta tela para acompanhar as anormalidades de faturamento.
            <br/>Preencha os campos abaixo para filtrar os registros de anormalidades e clique no bot�o <b>'Pesquisar'</b> para exibir sua
            pesquisa.
        </div>

        <form id="formPesquisarLeituras">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body bg-light">
                            <h5>Filtrar Anormalidades</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="descricao">Descri��o</label>
                                    <input id="descricao" maxlength="100" name="descricao" class="form-control form-control-sm" value="${descricaoAnormalidade}"/>
                                </div>
                                <div class="col-md-3">
                                    <label for="indicadorUso">Indicador de Uso</label>
                                    <select id="indicadorUso" class="form-control form-control-sm"
                                            name="indicadorUso">
                                        <option value="" <c:if test="${anormalidadeHabilitado eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${anormalidadeHabilitado  eq 'true'}">selected</c:if>>Ativo
                                        </option>
                                        <option value="false" <c:if test="${anormalidadeHabilitado  eq 'false'}">selected</c:if>>
                                            Inativo
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="indicadorImpedeFaturamento">Impedir faturamento</label>
                                    <select id="indicadorImpedeFaturamento" class="form-control form-control-sm"
                                            name="indicadorImpedeFaturamento">
                                        <option value="" <c:if test="${anormalidadeIndicadorImpedeFaturamento eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${anormalidadeIndicadorImpedeFaturamento  eq 'true'}">selected</c:if>>Sim
                                        </option>
                                        <option value="false" <c:if test="${anormalidadeIndicadorImpedeFaturamento  eq 'false'}">selected</c:if>>
                                            N�o
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <button id="botaoPesquisar" data-style="expand-left" type="submit"
                                            class="btn btn-primary btn-sm float-right ladda-button">
                                        <i class="fa fa-search"></i> <span class="ladda-label">Pesquisar</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="row mt-3">
            <div class="col-sm-12">
                <div class="ocultarTabela containerTabela">
                    <table id="table-anormalidades" class="table table-bordered table-striped table-hover" width="100%">
                        <thead class="thead-ggas-bootstrap">
                        <th class="text-center" width="20%">Ativo</th>
                        <th class="text-center" width="60%">Nome</th>
                        <th class="text-center" width="10%">Impedir Faturamento</th>
                        <th class="text-center" width="15%">A��es</th>
                        </thead>
                        <tbody>
                        <c:forEach items="${anormalidades}" var="anormalidade">
                            <tr>
                                <td class="text-center">
                                    <c:choose>
                                        <c:when test="${anormalidade.habilitado == true}">
                                            <i class="fa fa-check-circle text-success"></i> Sim
                                        </c:when>
                                        <c:otherwise>
                                            <i class="fa fa-times-circle text-danger"></i> N�o
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>${anormalidade.descricao}</td>
                                <td class="text-center">
                                    <c:choose>
                                        <c:when test="${anormalidade.indicadorImpedeFaturamento == true}">
                                            <i class="fa fa-check-circle text-success"></i> Sim
                                        </c:when>
                                        <c:otherwise>
                                            <i class="fa fa-times-circle text-danger"></i> N�o
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="text-center">
                                    <button type="button"
                                            data-chavePrimaria="${anormalidade.chavePrimaria}"
                                            data-habilitado="${anormalidade.habilitado}"
                                            data-descricao="${anormalidade.descricao}"
                                            data-indicadorImpedeFaturamento="${anormalidade.indicadorImpedeFaturamento}"
                                            class="btn btn-primary btn-sm alterar">
                                        <i class="fa fa-edit"></i> Alterar
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="alterarAnormalidade.jsp"/>


<script src="${ctxWebpack}/dist/modulos/faturamento/anormalidade/index.js"></script>

