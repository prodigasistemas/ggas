<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<div id="modalAlterarAnormalidade" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Alterar Anormalidade de Faturamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formAlterarAnormalidade">
                    <input type="hidden" id="chavePrimariaAnormalidadeForm"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="descricaoForm">Descri��o <span class="text-danger">*</span></label>
                            <input readonly type="text" required class="form-control form-control-sm" id="descricaoForm" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Impedir Faturamento do Ponto de Consumo? <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-md-12 margin-top-menor">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" required
                                       type="radio" name="impedirFaturamentoForm"
                                       id="impedirFaturamento" value="true">
                                <label class="custom-control-label"
                                       for="impedirFaturamento">Sim</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" required
                                       name="impedirFaturamentoForm"
                                       id="impedirFaturamentoFalse"
                                       value="false">
                                <label class="custom-control-label"
                                       for="impedirFaturamentoFalse">N�o</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label>Indicador de Uso <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-md-12 margin-top-menor">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input"
                                       type="radio" name="indicadorUsoForm"
                                       id="indicadorUsoForm" value="true">
                                <label class="custom-control-label"
                                       for="indicadorUsoForm">Ativo</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio"
                                       name="indicadorUsoForm"
                                       id="indicadorUsoFalse"
                                       value="false">
                                <label class="custom-control-label"
                                       for="indicadorUsoFalse">Inativo</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">
                    <i class="fa fa-times fa-fw"></i> Cancelar
                </button>
                <button id="salvarAnormalidade" type="button" class="btn btn-sm btn-primary">
                    <i class="fa fa-check fa-fw"></i> Salvar
                </button>
            </div>
        </div>
    </div>
</div>
