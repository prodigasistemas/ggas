<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>

<h1 class="tituloInterno">
	Alterar S�rie de Nota Fiscal<a class="linkHelp"
		href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">Preencha os campos abaixo para realizar a altera��o
</p>

<form:form method="post" name="manterSerieNotaFiscalForm" action="alterarSerieNotaFiscal">

	<script>	
			
		$(document).ready(function(){
			
			//Define o mascaramento para os campos de 
			$("#anoMes").inputmask("99/9999",{placeholder:"_"});
	
		});
		
		function limparFormulario(){
			
			document.getElementById('numero').value = "";
			document.getElementById('descricao').value = "";
			document.getElementById('sequenciaNumeracao').value = "";
			if (document.getElementById('anoMes').disabled==false)
				document.getElementById('anoMes').value = "";
			if (document.getElementById('operacao').disabled==false)
				document.getElementById('operacao').value = "-1";	
			if (document.getElementById('segmentoA').disabled==false)
				document.getElementById('segmentoA').value = "-1";
			if (document.getElementById('tipoFaturamento').disabled==false)
				document.getElementById('tipoFaturamento').value = "-1";
			
			if (document.forms[0].indicadorContingenciaScan.disabled==false) {
				document.forms[0].indicadorContingenciaScan[0].checked = true;
				document.forms[0].indicadorContingenciaScan[1].checked = false;				
			}
			if (document.forms[0].indicadorControleMensal.disabled==false) {
				document.forms[0].indicadorControleMensal[0].checked = true;
				document.forms[0].indicadorControleMensal[1].checked = false;
			}
			if (document.forms[0].indicadorSerieEletronica.disabled==false) {
				document.forms[0].indicadorSerieEletronica[0].checked = true;
				document.forms[0].indicadorSerieEletronica[1].checked = false;
			}
			document.forms[0].habilitado[0].checked = true;
		}
		
		function cancelar() {		
			location.href = '<c:url value="exibirPesquisaSerieNotaFiscal"/>';
		}
		
		function habilitarAnoMes() {
			
			if (document.forms[0].indicadorControleMensal[0].checked == true){ 
				var serieMensal = true;
			}else{
				var serieMensal = false;
			}
			
	        var tagSpan = document.getElementById("spanAnoMesObrigatorio");
	        if (tagSpan != undefined) {
	        	tagSpan.style.display = "none";  
	        }   
			if (serieMensal){
				tagSpan.style.display = "block";  
				document.getElementById("anoMes").disabled = false;
			} else {
				tagSpan.style.display = "none";  
				document.getElementById("anoMes").value = '';
				document.getElementById("anoMes").disabled = true;
			}
	        
		}	
		
	</script>
	
	<input name="acao" type="hidden" id="acao"	value="alterarSerieNotaFiscal" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${serieNotaFiscal.chavePrimaria}"/>
	<input name="idSerie" type="hidden" id="idSerie" value="${serieNotaFiscal.chavePrimaria}"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true"/>
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1" class="coluna">
			<label class="rotulo rotuloNumeroAlterar" id="rotuloNumero" for="numero" ><span class="campoObrigatorioSimbolo">* </span>C�digo:</label>
			<input class="campoTexto" type="text" name="numero" id="numero" maxlength="9" size="10" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${serieNotaFiscal.numero}"/>
			<br class="quebraLinha"/>
			<label class="rotulo rotuloDescricaoAlterar" id="rotuloDescricao" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${serieNotaFiscal.descricao}"/>
			<br class="quebraLinha"/>

			<label class="rotulo" id="rotuloOperacao"
				for="operacao"><span class="campoObrigatorioSimbolo">* </span>Opera��o:</label>
			<select name="tipoOperacao" id="operacao" <c:if test="${serieEmUso eq 'true'}">disabled</c:if>
				class="campoSelect campo2Linhas operacaoAlterar">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaOperacao}" var="operacaoVariavel">
				<option value="<c:out value="${operacao.chavePrimaria}"/>"
					<c:if test="${serieNotaFiscal.tipoOperacao.chavePrimaria == operacaoVariavel.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${operacaoVariavel.descricao}" />
				</option>
				</c:forEach>
			</select>
		</fieldset>
		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloSequencial" for="sequencial" ><span class="campoObrigatorioSimbolo">* </span>Sequencial:</label>
			<input class="campoTexto sequenciaNumeracaoAlterar" type="text" name="sequenciaNumeracao" id="sequenciaNumeracao" maxlength="9" size="10" onkeypress="return formatarCampoInteiro(event);" value="<c:if test="${empty serieVO.sequenciaNumeracao}">${serieNotaFiscal.sequenciaNumeracao }</c:if><c:if test="${not empty serieVO.sequenciaNumeracao}">${serieVO.sequenciaNumeracao}</c:if>"/>
			<br class="quebraLinha"/>
			<label class="rotulo rotuloTipofaturamento" id="rotuloTipofaturamento"
				for="operacao"><span class="campoObrigatorioSimbolo">* </span>Tipo faturamento:</label>
			<select name="tipoFaturamento" id="tipoFaturamento" <c:if test="${serieEmUso eq 'true'}">disabled</c:if>
				class="campoSelect campo2Linhas tipoFaturamentoAlterar">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoFaturamento}" var="tipoFaturamentoVariavel">
				<option value="<c:out value="${tipoFaturamento.chavePrimaria}"/>"
					<c:if test="${serieNotaFiscal.tipoFaturamento.chavePrimaria == tipoFaturamentoVariavel.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoFaturamentoVariavel.descricao}" />
				</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>
	
	<h1 class="tituloInterno"></h1>
		
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1" class="colunaEsquerda">									
			<label class="rotulo" for="indicadorControleMensal"><span class="campoObrigatorioSimbolo">* </span>S�rie mensal:</label>
			<input class="campoRadio" type="radio" name="indicadorControleMensal" id="indicadorControleMensal" value="true" <c:if test="${serieNotaFiscal.indicadorControleMensal eq 'true'}">checked</c:if> onclick="habilitarAnoMes();" <c:if test="${serieEmUso eq 'true'}">disabled</c:if>>
			<label class="rotuloRadio" for="indicadorControleMensal">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorControleMensal" id="indicadorControleMensal" value="false" <c:if test="${serieNotaFiscal.indicadorControleMensal eq 'false'}">checked</c:if> onclick="habilitarAnoMes();" <c:if test="${serieEmUso eq 'true'}">disabled</c:if>>
			<label class="rotuloRadio" for="indicadorControleMensal">N�o</label>
			<span id="spanAnoMesObrigatorio">
			<label class="rotulo" for="anoMes"><span class="campoObrigatorioSimbolo">* </span> M�s/Ano:</label>
			<input class="campoTexto" type="text" name="anoMes" id="anoMes" maxlength="7" size="7" value="${anoMes}"
				onkeypress="return formatarCampoInteiro(event,7);" <c:if test="${serieEmUso eq 'true'}">disabled</c:if> />
			</span>
			<label class="rotulo" for="indicadorSerieEletronica"><span class="campoObrigatorioSimbolo">* </span>Eletr�nica:</label>
			<input class="campoRadio" type="radio" name="indicadorSerieEletronica" id="indicadorSerieEletronica" value="true" <c:if test="${serieNotaFiscal.indicadorSerieEletronica eq 'true'}">checked</c:if> <c:if test="${serieEmUso eq 'true'}">disabled</c:if>>
			<label class="rotuloRadio" for="indicadorSerieEletronica">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorSerieEletronica" id="indicadorSerieEletronica" value="false" <c:if test="${serieNotaFiscal.indicadorSerieEletronica eq 'false'}">checked</c:if> <c:if test="${serieEmUso eq 'true'}">disabled</c:if>>
			<label class="rotuloRadio" for="indicadorSerieEletronica">N�o</label>

			<label class="rotulo rotulo2Linhas" for="indicadorContingenciaScan"><span class="campoObrigatorioSimbolo">* </span>Conting�ncia SCAN:</label>
			<input class="campoRadio" type="radio" name="indicadorContingenciaScan" id="indicadorContingenciaScan" value="true" <c:if test="${serieNotaFiscal.indicadorContingenciaScan eq 'true'}">checked</c:if> <c:if test="${serieEmUso eq 'true'}">disabled</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorContingenciaScan" id="indicadorContingenciaScan" value="false" <c:if test="${serieNotaFiscal.indicadorContingenciaScan eq 'false'}">checked</c:if> <c:if test="${serieEmUso eq 'true'}">disabled</c:if>>
			<label class="rotuloRadio" for="indicadorContingenciaScan">N�o</label>
			
			<c:if test="${serieEmUso ne 'true'}">
				<script language="javascript">habilitarAnoMes();</script>
			</c:if>
			
			<label class="rotulo" id="rotuloSegmentoN" for="segmento" style="margin-top: 0px;" >Segmento:</label>
			<select name="segmento" id="segmentoA" <c:if test="${serieEmUso eq 'true'}">disabled</c:if>
				class="campoSelect segmentoN" style="margin: 0px;">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
				<option value="<c:out value="${segmento.chavePrimaria}"/>"
					<c:if test="${serieNotaFiscal.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${segmento.descricao}" />
				</option>
				</c:forEach>
			</select>
			
			<label class="rotulo" for="habilitado" ><span class="campoObrigatorioSimbolo">* </span>Indicador de uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${serieNotaFiscal.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Sim</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${serieNotaFiscal.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">N�o</label>
			
		</fieldset>
	</fieldset>
	
	<c:if test="${serieEmUso eq 'true'}">
		<input name="tipoOperacao" type="hidden" id="tipoOperacao" value="${serieNotaFiscal.tipoOperacao}"/>
		<input name="tipoFaturamento" type="hidden" id="tipoFaturamento" value="${serieNotaFiscal.tipoFaturamento}"/>
		<input name="indicadorControleMensal" type="hidden" id="indicadorControleMensal" value="${serieNotaFiscal.indicadorControleMensal}"/>
		<input name="indicadorSerieEletronica" type="hidden" id="indicadorSerieEletronica" value="${serieNotaFiscal.indicadorSerieEletronica}"/>
		<input name="indicadorContingenciaScan" type="hidden" id="indicadorContingenciaScan" value="${serieNotaFiscal.indicadorContingenciaScan}"/>
		<input name="segmento" type="hidden" id="segmento" value="${serieNotaFiscal.segmento}"/>
		<input name="anoMes" type="hidden" id="anoMes" value="${anoMes}"/>
	</c:if>
	
	<fieldset class="conteinerBotoes">		
		<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
		<input name="Button" class="bottonRightCol2" value="Limpar" type="button" onclick="limparFormulario();">
		<input name="button" value="Alterar" class="bottonRightCol2 botaoGrande1" type="submit">		
						
	</fieldset>

</form:form>
