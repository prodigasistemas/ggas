<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js" charset="iso-8859-1"></script>


<h1 class="tituloInterno">
	Pesquisar S�rie Nota Fiscal<a class="linkHelp"
		href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para pesquisar um registro espec�fico, informe os dados nos campos
	abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>,
	ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span>
	para exibir todos.<br /> Para incluir um novo registro clique em <span
		class="destaqueOrientacaoInicial">Incluir</span>
</p>

<form:form method="post" name="manterSerieNotaFiscalForm" action="pesquisarSerieNotaFiscal">

	<script>
	
		function limparFormulario(){
			document.getElementById('numero').value = "";
			document.getElementById('descricao').value = "";
			document.getElementById('operacao').value = "-1";	
			document.getElementById('segmento').value = "-1";
			document.getElementById('tipoFaturamento').value = "-1";
			document.forms[0].habilitado[2].checked = true;
			document.forms[0].indicadorControleMensal[2].checked = true;
			document.forms[0].indicadorSerieEletronica[2].checked = true;
			document.forms[0].indicadorContingenciaScan[2].checked = true;
			
		}
		
		function incluir() {		
			location.href = '<c:url value="exibirInclusaoSerieNotaFiscal"/>';
		}
		
		function remover(){
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('manterSerieNotaFiscalForm', 'removerSerieNotaFiscal');
				}
		    }
		}
		
		function alterar(){
			var selecao = verificarSelecaoApenasUmSemMensagem();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("manterSerieNotaFiscalForm", "exibirAlteracaoSerieNotaFiscal");
		    } else {
		    	alert ("Selecione apenas um registro para realizar a opera��o!");
		    }
		} 
		
		function detalhar(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter('manterSerieNotaFiscalForm', 'exibirDetalheSerieNotaFiscal');			
		}
			
	</script>

	<input name="acao" type="hidden" id="acao"	value="pesquisarSerieNotaFiscal" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria"/>	
	<input name="nome" type="hidden" id="nome"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true"/>
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1" class="colunaEsq">
			<label class="rotulo" id="rotuloNumero" for="numero" >C�digo:</label>
			<input class="campoTexto" type="text" name="numero" id="numero" maxlength="9" size="10" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${serieNotaFiscal.numero}"/><br />				
			
			<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${serieNotaFiscal.descricao}"/><br />
			
			<label class="rotulo" id="rotuloOperacao"
				for="operacao">Opera��o:</label> 
			<select name="tipoOperacao" id="operacao"
				class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaOperacao}" var="operacao">
					<option value="<c:out value="${operacao.chavePrimaria}"/>"
						<c:if test="${serieNotaFiscal.tipoOperacao.chavePrimaria == operacao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${operacao.descricao}" />
					</option>
				</c:forEach>
			</select> 						
		</fieldset>
		<fieldset id="pesquisaMedicaoCol1" class="colunaFinal detalhamentoColunaMedia">			
			<label class="rotulo" id="rotuloTipoFaturamento"
				for="tipoFaturamento">Tipo de Faturamento:</label> 
			<select name="tipoFaturamento" id="tipoFaturamento"
				class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoFaturamento}" var="tipoFaturamento">
				<option value="<c:out value="${tipoFaturamento.chavePrimaria}"/>"
					<c:if test="${serieNotaFiscal.tipoFaturamento.chavePrimaria == tipoFaturamento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoFaturamento.descricao}" />
				</option>
				</c:forEach>
			</select><br/>
			<label class="rotulo" id="rotuloSegmento"
				for="segmento">Segmento:</label> 
			<select name="segmento" id="segmento"
				class="campoSelect campo2Linhas" style="margin:15px 0 0 0\9;">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSegmento}" var="segmento">
				<option value="<c:out value="${segmento.chavePrimaria}"/>"
					<c:if test="${serieNotaFiscal.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${segmento.descricao}" />
				</option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>
	<br/>		
	<hr class="linhaSeparadoraPesquisa" />
	<fieldset id="pesquisaMedicaoCol1" class="colunaEsq">									
		<label class="rotulo" for="habilitado">S�rie mensal?</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorControleMensal" id="indicadorControleMensal" value="true" <c:if test="${serieVO.indicadorControleMensal eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorControleMensal">Sim</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorControleMensal" id=indicadorControleMensal value="false" <c:if test="${serieVO.indicadorControleMensal eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorControleMensal">N�o</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorControleMensal" id="indicadorControleMensal" value="" <c:if test="${empty serieVO.indicadorControleMensal}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorControleMensal">Todos</label>
		<label class="rotulo" for="habilitado">S�rie eletr�nica?</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorSerieEletronica" id="indicadorSerieEletronica" value="true" <c:if test="${serieVO.indicadorSerieEletronica eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorSerieEletronica">Sim</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorSerieEletronica" id="indicadorSerieEletronica" value="false" <c:if test="${serieVO.indicadorSerieEletronica eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorSerieEletronica">N�o</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorSerieEletronica" id="indicadorSerieEletronica" value="" <c:if test="${empty serieVO.indicadorSerieEletronica}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorSerieEletronica">Todos</label>
	</fieldset>
	<fieldset id="pesquisaMedicaoCol1" class="coluna2">										
		<label class="rotulo" for="habilitado">Contig�ncia SCAN?</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorContingenciaScan" id="indicadorContingenciaScan" value="true" <c:if test="${serieVO.indicadorContingenciaScan eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorContingenciaScan">Sim</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorContingenciaScan" id="indicadorContingenciaScan" value="false" <c:if test="${serieVO.indicadorContingenciaScan eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorContingenciaScan">N�o</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="indicadorContingenciaScan" id="indicadorContingenciaScan" value="" <c:if test="${empty serieVO.indicadorContingenciaScan}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorContingenciaScan">Todos</label>
		<label class="rotulo" for="habilitado">Indicador de uso:</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${serieVO.habilitado eq 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Ativo</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${serieVO.habilitado eq 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="indicadorUso">Inativo</label>
		<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty serieVO.habilitado}">checked</c:if>>
		<label class="rotuloRadio" for="habilitado">Todos</label><br/>
	</fieldset>
		
	<fieldset class="conteinerBotoesPesquisarDir">
		<input name="Button" class="bottonRightCol2" id="botaoPesquisar"
				value="Pesquisar" type="submit">
		<input name="Button" class="bottonRightCol2 bottonRightCol1" id="Limpar"
		 value="Limpar" type="button" onclick="limparFormulario();">
	</fieldset>
	
	<fieldset id="tabelaSerie" class="coluna2">
		<c:if test="${listaSerie ne null}">			
			<display:table class="dataTableGGAS" name="listaSerie"
				sort="list" id="serie"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				pagesize="15"
				style="width: 910px"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="pesquisarSerieNotaFiscal">

				<display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	      			<input type="checkbox" name="chavesPrimarias" id="chavesPrimarias" value="${serie.chavePrimaria}">
				</display:column>
				
				<display:column title="Ativo" style="width: 30px">
					<c:choose>
						<c:when test="${serie.habilitado == true}">
							<img alt="Ativo" title="Ativo"
								src="<c:url value="/imagens/success_icon.png"/>" border="0">
						</c:when>
						<c:otherwise>
							<img alt="Inativo" title="Inativo"
								src="<c:url value="/imagens/cancel16.png"/>" border="0">
						</c:otherwise>
					</c:choose>
				</display:column>	
				<display:column style="text-align: center;" title="C�digo"
	 				sortable="true" sortProperty="numero" >
		        	<a href="javascript:detalhar(<c:out value='${serie.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${serie.numero}"/>
		            </a>
	 			</display:column>
	 			<display:column style="text-align: center;" title="Descri��o"
	 				sortable="true" sortProperty="descricao" >
		 			<a href="javascript:detalhar(<c:out value='${serie.chavePrimaria}'/>);"><span class="linkInvisivel"></span>				
						<c:out value='${serie.descricao}'/>
					</a>
				</display:column>
				<display:column style="text-align: center;" title="Opera��o">
		 			<a href="javascript:detalhar(<c:out value='${serie.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
						<c:out value='${serie.tipoOperacao.descricao}'/>
					</a>	
				</display:column>
				<display:column style="text-align: center; width: 60px" title="Tipo de Faturamento">
					<a href="javascript:detalhar(<c:out value='${serie.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
						<c:out value='${serie.tipoFaturamento.descricao}'/>
					</a>
				</display:column>
				<display:column style="text-align: center;" title="Mensal" >
					<a href="javascript:detalhar(<c:out value='${serie.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
					<c:out value='${serie.indicadorControleMensalFormatado}'/>
					</a>
				</display:column>
				<display:column style="text-align: center;" title="Eletr�nica" >
					<a href="javascript:detalhar(<c:out value='${serie.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
						<c:out value='${serie.indicadorSerieEletronicaFormatado}'/>
					</a>
				</display:column>
				<display:column style="text-align: center;" title="SCAN" >
					<a href="javascript:detalhar(<c:out value='${serie.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
						<c:out value='${serie.indicadorContingenciaScanFormatado}'/>
					</a>
				</display:column>
			</display:table>
		</c:if>	
	</fieldset>

	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaSerie}">
			<input name="buttonAlterar" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterar()" type="button">
			<input name="buttonRemover" value="Remover" class="bottonRightCol2 botaoRemover" id="Remover" onclick="remover()" type="button">	
		</c:if>	
		<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">		
	</fieldset>

</form:form>
