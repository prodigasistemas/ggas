<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<h1 class="tituloInterno">
	 Detalhar S�rie Nota Fiscal<a class="linkHelp"
		href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
		target="right" onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">
	Para alterar este registro clique em <span class="destaqueOrientacaoInicial">Alterar</span>
</p>

<form:form method="post" name="manterSerieNotaFiscalForm" action="pesquisarSerieNotaFiscal">

<script>	
				
		function cancelar() {		
			location.href = '<c:url value="exibirPesquisaSerieNotaFiscal"/>';
		}
		
		function alterar(chave) {
			document.forms[0].chavePrimaria.value = chave;
			submeter('manterSerieNotaFiscalForm','exibirAlteracaoSerieNotaFiscal');
		}
		
	</script>
	
	<script>
	//Script para Alinhamento de formul�rios
	$(document).ready(function() {
	var max = 0;
	$("label").each(function(){
	if ($(this).width() > max)
	max = $(this).width();
	});
	$("label").width(max);
	});
</script>
	
	<input name="acao" type="hidden" id="acao"	value="alterarSerieNotaFiscal" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${serie.chavePrimaria}"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true"/>
	<input name="nome" type="hidden" id="nome"/>	
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1" class="coluna">
			<label class="rotulo" id="rotuloNumero" for="numero">N�mero:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:out value="${serie.numero}"/></span><br/>		
			
			<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:out value="${serie.descricao}"/></span><br/>
			
			<label class="rotulo" id="rotuloOperacao" for="operacao">Opera��o:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:out value="${serie.tipoOperacao.descricao}"/></span><br/>
			 						
		</fieldset>
		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloSequencial" for="sequenciaNumeracao" >Sequencial:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:out value="${serie.sequenciaNumeracao}"/></span><br/>
			
			<label class="rotulo" id="rotuloTipofaturamento" for="tipoFaturamento">Tipo faturamento:</label> 
			<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:out value="${serie.tipoFaturamento.descricao}"/></span><br/>			
		</fieldset>
	</fieldset>
	
	<h1 class="tituloInterno"></h1>
		
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaMedicaoCol1" class="colunaEsquerda">									
			<label class="rotulo" for="habilitado">S�rie mensal:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:choose>
					<c:when test="${serie.indicadorControleMensal== true}">
						<c:out value="Sim"/>
					</c:when>
					<c:otherwise>
						<c:out value="N�o"/>
					</c:otherwise>
				</c:choose>
			</span>
			<label class="rotulo" for="habilitado">M�s/Ano:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
			<c:out value="${serie.anoMesFormatado}"/></span>
			<br class="quebraLinha"/>		
			<label class="rotulo" for="habilitado">Eletr�nica:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:choose>
					<c:when test="${serie.indicadorSerieEletronica== true}">
						<c:out value="Sim"/>
					</c:when>
					<c:otherwise>
						<c:out value="N�o"/>
					</c:otherwise>
				</c:choose>
			</span>
			<br class="quebraLinha"/>		
			<label class="rotulo" for="habilitado">Conting�ncia SCAN:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo">
				<c:choose>
					<c:when test="${serie.indicadorContingenciaScan== true}">
						<c:out value="Sim"/>
					</c:when>
					<c:otherwise>
						<c:out value="N�o"/>
					</c:otherwise>
				</c:choose>
			</span>
			<br class="quebraLinha"/>			
			<label class="rotulo" id="rotuloSegmentoNovo" for="segmento" >Segmento:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo" id="itemDetalhamentoNovo">
				<c:out value="${serie.segmento.descricao}"/>
			</span>			
			<br class="quebraLinha"/>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes">		
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
		<input name="button" value="Alterar" class="bottonRightCol2 botaoGrande1" onclick="alterar(${serie.chavePrimaria})" type="button">		
	</fieldset>

</form:form>