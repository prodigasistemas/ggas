<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ page import="br.com.ggas.faturamento.cronograma.impl.CronogramaFaturamentoImpl" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="constante" class="br.com.ggas.faturamento.cronograma.impl.CronogramaFaturamentoImpl">

</jsp:useBean>

<h1 class="tituloInterno">Pesquisar Cronograma<a class="linkHelp" href="<help:help>/consultandoumcronogramadefaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" action="pesquisarAcompanhamentoCronograma">

	<script lang="javascript">
		$(document).ready(function(){

			//Define o mascaramento para os campos de 
			$("#mesAnoFaturamentoInicial,#mesAnoFaturamentoFinal").inputmask("99/9999",{placeholder:"_"});
							
		});
		
		function removerCronograma(){
			var formulario = document.forms[0];
			var flag = 0;
			var chaveSelecionada;
						
			if (formulario != undefined && formulario.chavesPrimarias != undefined) {
				var total = formulario.chavesPrimarias.length;
				if (total != undefined) {
					for (var i = 0; i< total; i++) {
						if(formulario.chavesPrimarias[i].checked == true){
							flag++;
							chaveSelecionada = formulario.chavesPrimarias[i].value;
						}
					}
				}
			}
			
			var selecao = verificarSelecao();
			if (selecao == true) {
				if (flag > 1) {
					alert('N�o � poss�vel remover dois ou mais cronogramas de faturamento simultaneamente.');
				} else {
					if (selecao == true) {	
						var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
						if(retorno == true) {
							document.getElementById("chavePrimaria").value = chaveSelecionada;
							submeter('cronogramaForm', 'removerCronograma');
						}
					}
				}
			}
		}

		function alterarCronograma(){			
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('cronogramaForm', 'exibirAlteracaoCronograma');
		    }
		}

		function detalharCronograma(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("cronogramaForm", "exibirDetalhamentoCronogramaAcompanhamento")
		}
		
		function incluirCronograma() {
			location.href = '<c:url value="/exibirInclusaoCronograma"/>';
		}		
		
		function limparFormulario(ele){
			
			limparFormularios(ele);
			document.forms[0].habilitadoAtivo.checked = true;
		}

	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarCronograma">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarCronograma" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" for="grupoFaturamento" >Grupo de Faturamento:</label>
			<select name="grupoFaturamento" id="grupoFaturamento" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaGrupoFaturamentos}" var="grupoFaturamento">
					<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${cronogramaVO.grupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${grupoFaturamento.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
			
			<label class="rotulo" for="periodicidade" >Periodicidade:</label>
			<select name="periodicidade" id="periodicidade" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaPeriodicidades}" var="periodicidade">
					<option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${cronogramaVO.periodicidade == periodicidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${periodicidade.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
			
			<label class="rotulo" for="situacao" >Status:</label>
			<select name="situacao" id="situacao" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSituacao}" var="situacao">
					<option value="<c:out value="${situacao}"/>" <c:if test="${cronogramaVO.situacao == situacao}">selected="selected"</c:if>>
						<c:out value="${situacao}"/>
					</option>		
			    </c:forEach>
			</select>
		</fieldset>
		
		<fieldset class="colunaFinal">
		    <label class="rotulo rotulo2Linhas" id="rotuloMesAnoFaturamento" for="mesAnoFaturamento" >Per�odo Refer�ncia Faturamento:</label>
			<input class="campoTexto campo2Linhas campoHorizontal" type="text" name="mesAnoFaturamentoInicial" id="mesAnoFaturamentoInicial" maxlength="7" size="6" value="${cronogramaVO.mesAnoFaturamentoInicial}" onkeypress="return formatarCampoInteiro(event,6);"/>
			<label class="rotuloEntreCampos" for="mesAnoFaturamentoFinal">a</label>
			<input class="campoTexto campo2Linhas campoHorizontal" type="text" name="mesAnoFaturamentoFinal" id="mesAnoFaturamentoFinal" maxlength="7" size="6" value="${cronogramaVO.mesAnoFaturamentoFinal}" onkeypress="return formatarCampoInteiro(event,6);"/>
			<label class="rotuloInformativo rotuloInformativo2Linhas rotuloHorizontal" for="mesAnoFaturamentoFinal">mm/aaaa</label>
				
			
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoAtivo" value="true" <c:if test="${cronogramaVO.habilitado eq 'true'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoInativo" value="false" <c:if test="${cronogramaVO.habilitado eq 'false'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoInativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoTodos" value="" <c:if test="${cronogramaVO.habilitado eq ''}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoTodos">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDir">
			<vacess:vacess param="pesquisarCronograma">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario(this.form);">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaCronograma ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCronograma" sort="list" id="cronograma" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarCronograma">
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${cronograma.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        
	        <display:column sortable="true" sortProperty="grupoFaturamento" title="Grupo de Faturamento">
	        	<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${cronograma.grupoFaturamento.descricao}"/>
	            </a>
	        </display:column>
	        
	         <display:column sortable="true" title="M�s/Ano-Ciclo" style="width: 150px">
	        	<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${cronograma.anoMesFaturamentoFormatado}"/>-<c:out value="${cronograma.numeroCiclo}"/>
	            </a>
	        </display:column>
	        
	         <display:column sortable="true" sortProperty="grupoFaturamento" title="Periodicidade" style="width: 150px">
	        	<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${cronograma.grupoFaturamento.periodicidade.descricao}"/>
	            </a>
	        </display:column>
	        
	         <display:column sortable="true" sortProperty="grupoFaturamento" title="Dia para Vencimento" style="width: 150px">
	        	<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${cronograma.grupoFaturamento.diaVencimento}"/>
	            </a>
	        </display:column>
	         <display:column sortable="false" sortProperty="grupoFaturamento" title="Status" style="width: 150px">
	        	<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:if test="${cronograma.anoMesFaturamento < cronograma.grupoFaturamento.anoMesReferencia}">
	            		${constante.situacaoConcluido}
	            	</c:if>
	            	<c:if test="${cronograma.anoMesFaturamento eq cronograma.grupoFaturamento.anoMesReferencia}">
	            		<c:if test="${cronograma.numeroCiclo < cronograma.grupoFaturamento.numeroCiclo}">
	            			${constante.situacaoConcluido}
	            		</c:if>
	            		<c:if test="${cronograma.numeroCiclo eq cronograma.grupoFaturamento.numeroCiclo}">
	            			${constante.situacaoAndamento}
	            		</c:if>
	            		<c:if test="${cronograma.numeroCiclo > cronograma.grupoFaturamento.numeroCiclo}">
	            			${constante.situacaoAgendado}
	            		</c:if>
	            	</c:if>
	            	<c:if test="${cronograma.anoMesFaturamento > cronograma.grupoFaturamento.anoMesReferencia}">
	            		${constante.situacaoAgendado}
	            	</c:if>	            	
	            </a>
	        </display:column>
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">

	</fieldset>
	
</form>
