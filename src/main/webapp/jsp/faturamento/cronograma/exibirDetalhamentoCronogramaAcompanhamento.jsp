<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>
		$(document).ready(function(){
			iniciarDatatable("#cronogramaRota");
			iniciarDatatable("#detalhamentoRota");
		});
	
		function voltar(){	
			location.href = '<c:url value="/exibirPesquisaCronograma"/>';
		}
		
		function alterar(){
			document.forms[0].postBack.value = false;
			document.getElementById("chaveAtividadeSistema").value = 0;
			submeter('cronogramaForm', 'exibirAlteracaoCronograma');
		}

		function detalharRotas(chave,descricao){
			document.forms[0].chaveAtividadeSistema.value = chave;
			document.forms[0].descricaoAtividade.value = descricao;
			submeter('cronogramaForm', 'exibirDetalhamentoCronogramaAcompanhamento');
		}

		
		function trazerPontosDeConsumoPorRota(idRota){
			//chaveAtividadeSistema
			document.getElementById("codigoRotaDetalhamento").value = idRota;
			submeter('cronogramaForm', 'exibirDetalhamentoRotaCronogramaAcompanhamento');
		}
		
		function exibirAnaliseAnormalidadeLeitura(idPontoConsumo, idCronograma) {
			document.getElementById("idPontoConsumo").value = idPontoConsumo;
			document.getElementById("chavePrimaria").value = idCronograma;
			submeter('cronogramaForm', 'exibirAnaliseExcecaoLeitura');
		}

		function exibirAnaliseAnormalidadeFatura(idPontoConsumo, idCronograma) {
			document.getElementById("idPontoConsumo").value = idPontoConsumo;
			document.getElementById("chavePrimaria").value = idCronograma;
			submeter('cronogramaForm', 'exibirAnaliseExcecaoFatura');
		}
		
		function desfazerCronograma(){
			var selecao = verificarSelecao();
			if (selecao == true) {
				submeter('cronogramaForm', 'desfazerCronograma');
			}
			
		}
		
		
	</script>
	

<div class="bootstrap">
	<form:form method="post" action="exibirDetalhamentoCronograma"  id="cronogramaForm">
		<input type="hidden" name="acao" id="acao" value="exibirDetalhamentoCronograma">
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="chaveAtividadeSistema" type="hidden" id="chaveAtividadeSistema" value="${cronogramaVO.chaveAtividadeSistema}"/>
		<input name="descricaoAtividade" type="hidden" id="descricaoAtividade" value="${cronogramaVO.descricaoAtividade}"/>
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cronogramaVO.chavePrimaria}"/>
		<input name="dataInicio" type="hidden" id="dataInicio" value="${cronogramaVO.dataInicio}"/>
		<input name="dataFim" type="hidden" id="dataFim" value="${cronogramaVO.dataFim}"/>
		<input name="grupoFaturamento" type="hidden" id="grupoFaturamento" value="${cronogramaVO.grupoFaturamento}"/>
		<input name="codigoRotaDetalhamento" type="hidden" id="codigoRotaDetalhamento" value="${cronogramaVO.codigoRotaDetalhamento}"/>
		<input name="idRota" type="hidden" id="idRota" />
		<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" />
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">
					Detalhar Cronograma
				<span class="float-right">
					<a type="Button" title="Atualizar" onClick="window.location.reload();"><i class="fas fa-sync-alt"></i></a>
				</span>	
				</h5>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<ul class="list-group">
							<li class="list-group-item">
								Grupo de Faturamento: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.descricaoGrupoFaturamento}'/></span>
  							</li>
  							<li class="list-group-item">
  								Ano/M�s-Ciclo: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.anoMesCiclo}'/></span>
  							</li>
  							<li class="list-group-item">
								Periodicidade: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.descricaoPeriodicidade}'/></span>
							</li>
  							
						</ul>
					</div>
					<div class="col-md-6">
						<ul class="list-group">
							<c:choose>
								<c:when test="${tipoLeitura eq 1}">
								<li class="list-group-item">
									Período de Leitura do Supervisório: 
									<c:choose>
										<c:when test="${cronogramaVO.dataInicioLeituraSuper eq ''}">
											<span class="font-weight-bold">Pontos de Consumo N�o Integrados</span>
										</c:when>
										<c:otherwise>
											<span class="font-weight-bold"><c:out value='${cronogramaVO.dataInicioLeituraSuper}'/>
												 a <c:out value='${cronogramaVO.dataFinalLeituraSuper}'/></span>
										</c:otherwise>
									</c:choose>
  								</li>
								</c:when>
							</c:choose>
							<li class="list-group-item">
								Quantidade de Rotas: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.qtdRotas}'/></span>
							</li>
							<li class="list-group-item">
								Quantidade de Medidores Ativos: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.qtdMedidores}'/></span>
							</li>
						</ul>
					</div>
				</div>
				
				<hr/>
				
				<div class="row">
					<div class="col-md-12">
   			 			<h5>Atividades</h5>
   			 			<div class="alert alert-primary fade show" role="alert">
         					<i class="fa fa-question-circle"></i>
         					Para exibir as rotas de cada Atividade, clique em uma delas na listagem abaixo.
   			 			</div>

   			 			<div class="table-responsive">
   			 				<table class="table table-bordered table-striped table-hover" id="atividade" style="width:100%">
   			 					<thead class="thead-ggas-bootstrap">
   			 						<tr>
   			 							<th scope="col" class="text-center">Atividade</th>
   			 							<th scope="col" class="text-center">Obrigat�ria</th>
   			 							<th scope="col" class="text-center">Predecessora</th>
   			 							<th scope="col" class="text-center">Data Prevista</th>
   			 							<th scope="col" class="text-center">Cronograma</th>
   			 							<th scope="col" class="text-center">Qtd. de Pontos de Consumo</th>
   			 							<th scope="col" class="text-center">Data de Realiza��o</th>
   			 							<th scope="col" class="text-center">A��o</th>
   			 						</tr>
   			 					</thead>
   			 					<tbody>
   			 						<c:forEach items="${listaAtividadeFaturamentos}" var="atividade">
   			 						<tr>
   			 							<td class="text-center">
   			 								<a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
			           						 <c:out value="${atividade.atividadeSistema.descricao}"/></a>
   			 							</td>
   			 							<td class="text-center">
   			 								<a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
		        							<c:choose>
											<c:when test="${atividade.atividadeSistema.obrigatoria == true}">
												<c:out value="Sim"/>
											</c:when>
											<c:otherwise>
												<c:out value="Não"/>
											</c:otherwise>
											</c:choose></a>
   			 							</td>
   			 							<td>
   			 								<a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
		            						<c:out value="${atividade.atividadeSistema.atividadePrecedente.descricao}"/></a>
   			 							</td>
   			 							<td  class="text-center">
   			 								<a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
			        						<c:out value="${atividade.dataFimFormatada}"/></a>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:choose>
												<c:when test="${not empty atividade.dataRealizacaoFormatada}">
													<span class="badge badge-pill badge-success">Atualizado</span>
												</c:when>
												<c:when test="${atividade.atividadeSistema.descricao eq 'INTEGRAR CLIENTES'}">
													<span class="badge badge-pill badge-info">Atividade</span>
												</c:when>
											</c:choose>
   			 							</td>

										<td class="text-center">
											<c:if test="${atividade.atividadeSistema.descricao  != 'ENCERRAR CICLO FATURAMENTO'}">
												${mapaPontoConsumoCronograma.getOrDefault(atividade.atividadeSistema, '-')}			
												/ ${mapaPontoConsumoTotaisCronograma.getOrDefault(atividade.atividadeSistema, '-')}
											</c:if>
										</td>
										<td class="text-center">
   			 								<a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
			        						<c:out value="${atividade.dataRealizacaoFormatada}"/></a>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:choose>
		        								<c:when test="${atividade.atividadeSistema.descricao eq 'INTEGRAR CLIENTES'}">
		        									<a href="exibirPesquisaMedicaoSupervisorio?tela=medicaoSupervisorio">
		        									<i title="Processar Tarefa" class="fas fa-cog"></i></a>
		        								</c:when>
		        								<c:when test="${atividade.atividadeSistema.descricao eq 'AN�LISE DE ANORMALIDADE DE FATURAMENTO'}">
		        									<a href="exibirPesquisaExcecaoLeituraFaturamento?tela=excecaoLeituraFaturamento">
		        									<i title="Processar Tarefa" class="fas fa-cog"></i></a>
		        								</c:when>
                                                <c:when test="${atividade.atividadeSistema.descricao eq 'REALIZAR LEITURA'}">
                                                    <a href="analiseMedicao?rotas=${chaveRotaRealizarLeitura}&grupoFaturamento=${chaveGrupoFaturamentoRealizarLeitura}&idModulo=${atividade.atividadeSistema.modulo.chavePrimaria}&idOperacao=${atividade.atividadeSistema.operacao.chavePrimaria}&idCronograma=${cronogramaVO.chavePrimaria}">
                                                        <i title="Processar Tarefa" class="fas fa-cog"></i></a>
                                                </c:when>
		        								<c:otherwise>
		        		 							<a href="exibirExecutarProcesso?idModulo=${atividade.atividadeSistema.modulo.chavePrimaria}&idOperacao=${atividade.atividadeSistema.operacao.chavePrimaria}&idGrupoFaturamento=${cronogramaVO.grupoFaturamento}">
			         								<i title="Processar Tarefa" class="fas fa-cog"></i></a>
		        								</c:otherwise>
		        							</c:choose>
   			 							</td>
   			 						</tr>
									</c:forEach>
   			 					</tbody>
   			 				</table>
   			 			</div>
   			 		</div>
				</div><!-- fim da row -->
				<hr />
				<div class="row">
					<div class="col-md-12">
						<h5>Rotas</h5>
   			 			<div class="alert alert-primary fade show" role="alert">
         					<i class="fa fa-question-circle"></i>
         					A coluna Mais de 1 atividade por Leiturista exibir� o valor Sim se houver mais de
         					uma rota alocada para o mesmo leiturista na mesma data. Clique em uma rota abaixo para obter seus respectivos pontos de consumo.
   			 			</div>

   			 			<ul class="list-group">
  							<li class="list-group-item">Atividade: <span><c:out value='${cronogramaVO.descricaoAtividade}'/></span></li>
  							<li class="list-group-item">Per�odo: <span><c:out value='${cronogramaVO.dataInicio}'/> a <c:out value='${cronogramaVO.dataFim}'/></span></li>
 						</ul><br/>

 						<div class="table-responsive">
 						<table class="table table-bordered table-striped table-hover" id="cronogramaRota" style="width:100%">
 							<thead class="thead-ggas-bootstrap">
 								<tr>
 									<th class="text-center" data-orderable="false">
                     					<input type='checkbox' name='checkAllAuto' id='checkAllAuto' />
                     				</th>
 									<th scope="col" class="text-center">Rota</th>
 									<th scope="col" class="text-center">Data Prevista</th>
 									<th scope="col" class="text-center">Data de Realiza��o</th>
 									<th scope="col" class="text-center">Data do Ciclo Anterior</th>
 									<th scope="col" class="text-center">Dias de Leitura</th>
 									<th scope="col" class="text-center">Leiturista</th>
 									<th scope="col" class="text-center">Mais de 1 atividade por Leiturista</th>
 								</tr>
 							</thead>
 							<tbody>
 								<c:forEach items="${listaCronogramaAtividadeRotaVO}" var="cronogramaRota">
 								<tr>
 									<td class="text-center">
         	 							<input type="checkbox" name="chavesPrimarias" value="${cronogramaRota.rota.chavePrimaria}">
         	 						</td>
 									<td class="text-center">
 										<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')">
		           						<c:out value="${cronogramaRota.rota.numeroRota}"/></a>
		           					</td>
		           					<td class="text-center">
		           						<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')">
		        						<c:out value="${cronogramaRota.dataPrevista}"/></a>
		           					</td>
		           					<td class="text-center">
		           						<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')">
		        						<c:out value="${cronogramaRota.dataRealizada}"/></a>
		           					</td>
		           					<td class="text-center">
		           						<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')">
		           						<c:out value="${cronogramaRota.dataCiclo}"/></a>
		           					</td>
		           					<td class="text-center">
		           						<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')">
		            					<c:out value="${cronogramaRota.diasLeitura}"/></a>
		           					</td>
		           					<td class="text-center">
		           						<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')">
		       							<c:out value="${cronogramaRota.rota.leiturista.funcionario.nome}"/></a>
		           					</td>
		           					<td class="text-center">
		           						<c:if test="${cronogramaRota.emAlertaFormatado eq 'Sim'}" >
											<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')" title="O leiturista está alocado para as rotas: <c:out value='${cronogramaRota.rotasEmAlertaFormatado}' />">	<c:out value='${cronogramaRota.emAlertaFormatado}' /> </a>
										</c:if>
										<c:if test="${cronogramaRota.emAlertaFormatado ne 'Sim'}" >
											<a href="javascript:trazerPontosDeConsumoPorRota('${cronogramaRota.rota.chavePrimaria}')">
											<c:out value='${cronogramaRota.emAlertaFormatado}' /></a>
										</c:if>
		           					</td>
 								</tr>
 								</c:forEach>
 							</tbody>
 						</table>
 						</div>
 						<div>
 						</div>
 						<hr/>
 					<div class="row">
					<div class="col-md-12">
 						<h5>Pontos de Consumo</h5>
 						<div class="alert alert-primary fade show" role="alert">
         					<i class="fa fa-question-circle"></i>
         						Pontos de consumo da rota listados abaixo.
   			 			</div>
   			 			
   			 			<div class="table-responsive">
   			 				<table class="table table-bordered table-striped table-hover" id="detalhamentoRota"  style="width:100%">
   			 					<thead class="thead-ggas-bootstrap">
   			 						<tr>
   			 							<th scope="col" class="text-center">Status do Processamento</th>
   			 							<th scope="col" class="text-center">C�digo</th>
   			 							<th scope="col" class="text-center">Ponto de Consumo</th>
   			 							<th scope="col" class="text-center">Consumo (m�)</th>
   			 							<th scope="col" class="text-center">Segmento</th>
   			 						</tr>
   			 					</thead>

   			 					<tbody>
   			 					<c:forEach items="${listaDetalhamentoRota}" var="detalhamentoRota">
   			 						<tr>
   			 							<td class="text-center">
   			 								<a href="#">
		         								<c:if test="${detalhamentoRota.situacao eq 'PROCESSADO_SEM_ANORMALIDADE'}">
		           									<span class="badge badge-pill badge-success">Sem Anormalidade</span>
		           								</c:if>
		           								<c:if test="${detalhamentoRota.situacao eq 'PRONTO_PROCESSAR'}">
		           									<span class="badge badge-pill badge-secondary">Pronto</span>
		           								</c:if>
		               							<c:if test="${detalhamentoRota.situacao eq 'PROCESSADO_ANORMALIDADE_ANALISADA'}">
		           									<span class="badge badge-pill badge-warning">Com Anormalidade Analisada</span>
		           								</c:if>
		           								<c:if test="${detalhamentoRota.situacao eq 'PROCESSADO_COM_ANORMALIDADE_BLOQUEIO'}">
		           									<span class="badge badge-pill badge-danger">Com Anormalidade e Bloqueio</span>
		           								</c:if>
		           								<c:if test="${detalhamentoRota.situacao eq 'PROCESSADO_COM_ANORMALIDADE'}">
		           									<span class="badge badge-pill badge-warning">Com Anormalidade</span>
		           								</c:if>
		           								<c:if test="${detalhamentoRota.situacao eq 'NAO_ESTA_PRONTO_PARA_PROCESSAR'}">
		           									<span class="badge badge-pill badge-info">N�o est� Pronto</span>
		           								</c:if>
		           		
		           							</a>
											<c:if test="${detalhamentoRota.situacao eq 'PROCESSADO_COM_ANORMALIDADE_BLOQUEIO' or detalhamentoRota.situacao eq 'PROCESSADO_COM_ANORMALIDADE'}">
												<c:if test="${cronogramaVO.chaveAtividadeSistema eq chaveConsistirLeitura and not faturado}">
													<a href="javascript:exibirAnaliseAnormalidadeLeitura('${detalhamentoRota.codigoPontoConsumo}', '${cronogramaVO.chavePrimaria}')">
														<i class="fa fa-search"><c:out value=""/></i>
													</a>
												</c:if>
												<c:if test="${cronogramaVO.chaveAtividadeSistema eq chaveFaturarGrupo}">
													<a href="javascript:exibirAnaliseAnormalidadeFatura('${detalhamentoRota.codigoPontoConsumo}', '${cronogramaVO.chavePrimaria}')">
														<i class="fa fa-search"><c:out value=""/></i>
													</a>
												</c:if>
											</c:if>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:out value="${detalhamentoRota.codigoPontoConsumo}"/>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:out value="${detalhamentoRota.descricaoPontoConsumo}"/>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:out value="${detalhamentoRota.consumo}"/>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:out value="${detalhamentoRota.descricaoSegmento}"/>
   			 							</td>
   			 						</tr>
   			 						</c:forEach>
   			 					</tbody>
   			 				</table>
   			 			</div>
					</div>
					</div>


					</div>

				</div><!-- fim da row -->

			</div><!-- card-body -->

			<div class="card-footer">
				<div class="row">
      				<div class="col-sm-12">
                        <button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="voltar();">
                            <i class="fas fa-undo-alt"></i> Voltar
                        </button>
                        <vacess:vacess param="desfazerCronograma">
                        <c:if test="${cronogramaVO.chaveAtividadeSistema eq 3 || cronogramaVO.chaveAtividadeSistema eq 4 || cronogramaVO.chaveAtividadeSistema eq 1}" >
                       		<button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="desfazerCronograma();">
                            	<i class="far fa-trash-alt"></i> Desfazer Cronograma
                       		</button>
                        </c:if>
                        </vacess:vacess>
                        <vacess:vacess param="alterarCronograma">
                  		<button id="buttonAlterar" class="btn btn-sm btn-primary float-right ml-1 mt-1"  type="button" onclick="alterar();">
                       		<i class="fas fa-edit"></i> Alterar
                   		</button>
             		 	</vacess:vacess>
      				</div>
      			</div>
			</div>
		</div><!-- Card -->

	</form:form>
</div>
