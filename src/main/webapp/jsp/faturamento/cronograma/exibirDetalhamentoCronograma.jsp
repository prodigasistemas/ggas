
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<h1 class="tituloInterno">Detalhar Cronograma<a class="linkHelp" href="<help:help>/detalhamentodocronogramadefaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form id="detalhamentoCronograma" method="post" action="exibirDetalhamentoCronograma"> 

	<script>
	
		function voltar(){	
			location.href = '<c:url value="/exibirPesquisaCronograma"/>';
		}
		
		function alterar(){
			document.forms[0].postBack.value = false;
			document.getElementById("chaveAtividadeSistema").value = 0;
			submeter('cronogramaForm', 'exibirAlteracaoCronograma');
		}

		function detalharRotas(chave,descricao){
			document.forms[0].chaveAtividadeSistema.value = chave;
			document.forms[0].descricaoAtividade.value = descricao;
			submeter('cronogramaForm', 'exibirDetalhamentoCronograma');
		}

		function init() {
			<c:if test="${listaCronogramaAtividadeRotaVO ne null}">
				$.scrollTo($('#conteinerCronogramaRotas'),800);
			</c:if>
		}
		addLoadEvent(init);
		
	</script>
	
	<input type="hidden" name="acao" id="acao" value="exibirDetalhamentoCronograma">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chaveAtividadeSistema" type="hidden" id="chaveAtividadeSistema" value="${cronogramaVO.chaveAtividadeSistema}"/>
	<input name="descricaoAtividade" type="hidden" id="descricaoAtividade" value="${cronogramaVO.descricaoAtividade}"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cronogramaVO.chavePrimaria}"/>
	<input name="dataInicio" type="hidden" id="dataInicio" value="${cronogramaVO.dataInicio}"/>
	<input name="dataFim" type="hidden" id="dataFim" value="${cronogramaVO.dataFim}"/>
	
	<fieldset id="conteinerCronograma" class="detalhamento">
		<fieldset class="coluna2">
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Grupo de Faturamento:</label>
			<span class="itemDetalhamento"><c:out value='${cronogramaVO.descricaoGrupoFaturamento}'/></span><br />
	
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>M�s/Ano-Ciclo:</label>
			<span class="itemDetalhamento"><c:out value='${cronogramaVO.mesAnoCiclo}'/></span><br />
		</fieldset>
		
		<fieldset class="colunaFinal">
			<fieldset class="conteinerDados">
				<label class="rotulo">Periodicidade:</label>
				<span class="itemDetalhamento"><c:out value='${cronogramaVO.descricaoPeriodicidade}'/></span>
				
				<label class="rotulo">Qtd. de Rotas:</label>
				<span class="itemDetalhamento"><c:out value='${cronogramaVO.qtdRotas}'/></span>
				
				<label class="rotulo">Qtd. de Medidores:</label>
				<span class="itemDetalhamento"><c:out value='${cronogramaVO.qtdMedidores}'/></span>
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset id="conteinerAtividades" class="conteinerBloco">
			<legend>Atividades</legend>
			<p class="orientacaoInicial">Para exibir as rotas de cada Atividade, clique em uma delas na listagem abaixo.</p>
			
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaAtividadeFaturamentos" sort="list" id="atividade" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoCronograma">
		        <display:column sortable="false" sortProperty="atividade" title="Atividade">
			            <a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
			            <c:out value="${atividade.atividadeSistema.descricao}"/>
					</a>
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="obrigatoria" title="Obrigat�ria" style="width: 150px">
		            <a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
		        	<c:choose>
						<c:when test="${atividade.atividadeSistema.obrigatoria == true}">
							<c:out value="Sim"/>
						</c:when>
						<c:otherwise>
							<c:out value="N�o"/>
						</c:otherwise>
					</c:choose>
				</a>
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="atividadePredecessora" title="Predecessora" style="width: 150px">
			        <a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
		            	<c:out value="${atividade.atividadeSistema.atividadePrecedente.descricao}"/>
		            </a>
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="getdataFimFormatada" title="Data Prevista" style="width: 90px">
			         <a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
			        	<c:out value="${atividade.dataFimFormatada}"/>
			        </a>
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="getdataRealizacaoFormatada" title="Data Realizada" style="width: 90px">
			         <a href="javascript:detalharRotas(<c:out value='${atividade.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividade.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
			        	<c:out value="${atividade.dataRealizacaoFormatada}"/>
			        </a>
		        </display:column>
		    </display:table>	
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset id="conteinerCronogramaRotas" class="conteinerBloco">
			<legend>Rotas</legend>
			<p class="orientacaoInicial">A coluna <span class="destaqueOrientacaoInicial">Mais de 1 atividade por Leiturista</span> exibir� o valor <span class="destaqueOrientacaoInicial">Sim</span> se houver mais de uma rota alocada para o mesmo leiturista na mesma data.
			Passe o mouse por cima para saber as rotas.</p>
			
			<label class="rotulo">Atividade:</label>
			<span class="itemDetalhamento"><c:out value='${cronogramaVO.descricaoAtividade}'/></span>
			<label id="rotuloPeriodo" class="rotulo rotuloHorizontal">Per�odo:</label>
			<span class="itemDetalhamento"><c:out value='${cronogramaVO.dataInicio}'/></span>
			<label class="rotuloEntreCampos">a</label>
			<span class="itemDetalhamento"><c:out value='${cronogramaVO.dataFim}'/></span>
			
			
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCronogramaAtividadeRotaVO" sort="list" id="cronogramaRota" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirDetalhamentoCronograma">
		        <display:column sortable="false" sortProperty="numeroRota" title="Rota"  style="width: 50px">
		           	<c:out value="${cronogramaRota.rota.numeroRota}"/>
		        </display:column>
		        
		         <display:column sortable="false" sortProperty="dataPrevista" title="Data Prevista" style="width: 80px">
		        	<c:out value="${cronogramaRota.dataPrevista}"/>
		        </display:column>
		        
		         <display:column sortable="false" sortProperty="dataRealizada" title="Data da<br /> Realiza��o" style="width: 80px">
		        	<c:out value="${cronogramaRota.dataRealizada}"/>
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="dataCicloAnterior" title="Data Ciclo<br />Anterior" style="width: 80px">
		           	<c:out value="${cronogramaRota.dataCiclo}"/>
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="diasLeitura" title="Dias de<br />Leitura" style="width: 50px">
		            <c:out value="${cronogramaRota.diasLeitura}"/>
		        </display:column>
		        
		        <display:column sortable="false" sortProperty="nome" title="Leiturista">
		       		<c:out value="${cronogramaRota.rota.leiturista.funcionario.nome}"/>
		        </display:column>
		        
				<display:column sortable="false"  title="Mais de 1 atividade<br />por Leiturista" style="width: 120px">
					<c:if test="${cronogramaRota.emAlertaFormatado eq 'Sim'}" >
						<a href="#" title="O leiturista est� alocado para as rotas: <c:out value='${cronogramaRota.rotasEmAlertaFormatado}' />">	<c:out value='${cronogramaRota.emAlertaFormatado}' /> </a>
					</c:if>		
					<c:if test="${cronogramaRota.emAlertaFormatado ne 'Sim'}" >
						<c:out value='${cronogramaRota.emAlertaFormatado}' />
					</c:if>
				</display:column>
		    </display:table>
		</fieldset>
		
	</fieldset>
	
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="voltar();">
	    <vacess:vacess param="alterarCronograma">
			<input id="buttonAlterar" name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>	       
	</fieldset>

</form> 
