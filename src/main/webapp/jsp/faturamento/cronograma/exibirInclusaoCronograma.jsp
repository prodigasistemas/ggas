<!--
 Copyright (C) <2011> GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

 Este arquivo ? parte do GGAS, um sistema de gest?o comercial de Servi?os de Distribui??o de G?s

 Este programa ? um software livre; voc? pode redistribu?-lo e/ou
 modific?-lo sob os termos de Licen?a P?blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers?o 2 da Licen?a.

 O GGAS ? distribu?do na expectativa de ser ?til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl?cita de
 COMERCIALIZA??O ou de ADEQUA??O A QUALQUER PROP?SITO EM PARTICULAR.
 Consulte a Licen?a P?blica Geral GNU para obter mais detalhes.

 Voc? deve ter recebido uma c?pia da Licen?a P?blica Geral GNU
 junto com este programa; se n?o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
<script>
		$.extend( true, $.fn.dataTable.defaults, {
			"stateSave": true,
			"paginate": false,
    		"searching": true,
    		"info": false,
    		"ordering": false,
    		"language": {
                "zeroRecords": "Nenhum registro foi encontrado",
                "search": "Procurar:",
                "processing": "Processando..."
                }
		} );

		$(document).ready(function(){

			$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button',
				buttonImage: '<c:url value="/imagens/calendario.png"/>', 
				buttonImageOnly: true, buttonText: 'Exibir Calend?rio', dateFormat: 'dd/mm/yy'});
			$("#mesAnoPartidaInput").inputmask("99/9999",{placeholder:"_"});
			
			$('#cronogramaFaturamento').DataTable();
		});

		$('#checkAllAuto').on('click', function(){
            var nodes =  $('#cronogramaFaturamento').dataTable().fnGetNodes(); 
            if($('#checkAllAuto').is(':checked')){
           	 $('.checkboxId', nodes).prop('checked', true).attr('checked', 'checked');
            }else{ 
           	 $('.checkboxId', nodes).prop('checked', false).removeAttr('checked');
           	
            }
           });

		function incluirCronograma(){
			document.forms[0].indexLista.value = -1;
			submeter("cronogramaForm", "incluirCronograma");
		}

		function removerErro(e){
			if(e.style.borderColor == 'red' && e.value != ''){
				e.style.borderColor = '';
			}

		}

		function validaDados(){
			$('#campoListaBranco').css('display','none');
			var listaDuracoes = document.getElementsByName('duracoes');
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');

			var verificaCamposBrancoDuracao = false;
			var veriicaCamposBrancodataPrevista = false

			if($('#grupoFaturamento').val() == '-1'){
				alert('Grupo de faturamento ? de preenchimento obrigat?rio.');
				return false;
			}

			for(var i=0;i<listaDuracoes.length;i++){
				if(listaDuracoes[i].outerHTML.indexOf('readonly') == -1 && listaDuracoes[i].value == ''){
					listaDuracoes[i].style.borderColor = 'red';
					verificaCamposBrancoDuracao = true;
				}
			}
			for(var i=0;i<listaDatasPrevistas.length;i++){
				if(listaDatasPrevistas[i].outerHTML.indexOf('readonly') == -1 && listaDatasPrevistas[i].value == ''){
					listaDatasPrevistas[i].style.borderColor = 'red';
					veriicaCamposBrancodataPrevista = true;
				}
			}

			if(veriicaCamposBrancodataPrevista || verificaCamposBrancoDuracao){
				$('#camposVermelhos').css('display','block');
				return false;
			}else{
				$('#camposVermelhos').css('display','none');
			}


			 var quantidade = document.getElementById("quantidadeCronogramas").value;
			 var listaChavesPrimariasMarcadas = new Array();
			 
			 if(quantidade == "" ){
					alert ("Quantidade de Cronogramas ? de preenchimento obrigat?rio");
			 }else{
				selecao = true;
			 	if (!$('input[name="chavesPrimariasMarcadas"]:checked').length > 0) {
			    	 selecao = false;
			 	}
			 
				 if (selecao == true) {
				    var i = 0;
		            $('input[name="chavesPrimariasMarcadas"]').each(function () {
		                if (this.checked) {
		                	listaChavesPrimariasMarcadas[i] = $(this).val();
		                    i++;
		                }
		                
		            });
		            var aux = 0;
		            AjaxService.validarAtividadesObrigatorias( listaChavesPrimariasMarcadas, 
		            		{callback:	function(numero) {            		      		         		
		            			if(numero == 1){
		            				gerar();
		            			}else{
		            				aux = numero;
		            			}
		            		}, async: false}
		        		);

                        if(aux == 2){
		        			alert("Erro: Selecione as atividades obrigatorias.");                    
			 	         }
				 }else {
				 alert("Selecione alguma atividade para incluir no cronograma.");
			 	}
			 }
		  }

		
		function limpar(){
			document.cronogramaForm.grupoFaturamento.value = "-1";
		}

		function cancelar() {
			location.href = '<c:url value="/exibirPesquisaCronograma"/>';
		}

		function detalharAtividade(){
			$.scrollTo($('#conteinerCronogramaRotas'),800);
		}
		function detalharRotas(chave,descricao){
			document.forms[0].chaveAtividadeSistema.value = chave;
			document.forms[0].descricaoAtividade.value = descricao;
			submeter("cronogramaForm", "exibirInclusaoCronograma");
		}

		function gerar() {
			
			document.getElementById("mesAnoPartida").value = document.getElementById("mesAnoPartidaInput").value;
			document.getElementById("cicloPartida").value = document.getElementById("cicloPartidaInput").value;		
			document.getElementById("grupoFaturamento").value = document.getElementById("grupoFaturamento").value;	
			submeter("cronogramaForm", "gerarCronograma");
		}

		function init () {
			
			carregarDadosGrupoFaturamento(document.getElementById("grupoFaturamento").value);	
			$('input[name="chavesPrimariasMarcadas"]').trigger("change");
		}
		
		function preencherCalendario(){
			
			var chavesPrimariasAtividades = document.getElementById("chavesPrimariasAtividades").value;
			var chavesPrimariasMarcadasAtividades = document.getElementById("chavesPrimariasMarcadasAtividades").value;
			
			var chavesPrimariasAtividadesAux = chavesPrimariasAtividades.split(",");
			var chavesPrimariasMarcadasAtividadesAux = chavesPrimariasMarcadasAtividades.split(",");
			
			var i = 0;
			
			for (; i < chavesPrimariasAtividadesAux.length;) {
				var isObrigatorio = false;
				
					if(chavesPrimariasAtividadesAux[i] == chavesPrimariasMarcadasAtividadesAux[i]){
						$("#datasPrevista"+ chavesPrimariasAtividadesAux[i]).datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend?rio', dateFormat: 'dd/mm/yy'});
						isObrigatorio = true;
						
						break;
					}else{
						break;
					}
					
				
				
				if(!isObrigatorio){
					$("#datasPrevista"+ chavesPrimariasAtividadesAux[i]).datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend?rio', dateFormat: 'dd/mm/yy'}).datepicker('disable');
				}
				i++;
			}
		}
		
		function carregarDadosGrupoFaturamento(idGrupoFaturamento, isMesAnoPartidaInputModificado){			
			var idCronogramaFaturamento = null;
			var idGrupo = document.getElementById("grupoFaturamento").value;
			var idGrupoHidden = document.getElementById("grupoFaturamentoHidden").value;
			var descPeriodicidade = document.getElementById("descPeriodicidade");
			var referenciaAtual = document.getElementById("referenciaAtual");
			var qtdRotas = document.getElementById("qtdRotas");
			var qtdMedidores = document.getElementById("qtdMedidores");
			var mesAnoPartida = document.getElementById("mesAnoPartida");
			var campoCicloPartidaInput = document.getElementById("cicloPartidaInput");
			var campoCicloPartida = document.getElementById("cicloPartida");
			var cronogramasAbertos = document.getElementById("cronogramasAbertos");
			var qtdeMaximaCronogramasAbertos = document.getElementById("qtdeMaximaCronogramasAbertos");
			var primeiraEntrada = document.getElementById("primeiraEntrada").value;
			var listaChaveAtividades = "";
			var listaDatasPrevistas = "";
			var listaDatasIniciais = "";
			var listaDatasFinais = "";
			var mesAnoPartidaInput = null;
			var cicloPartidaInput = null;
			var primeiroCronograma;
			
			
			if(isMesAnoPartidaInputModificado != undefined && isMesAnoPartidaInputModificado != null){
				mesAnoPartidaInput = document.getElementById("mesAnoPartidaInput").value;
				cicloPartidaInput = document.getElementById("cicloPartidaInput").value;
			}

			$('input[type="checkbox"]:checked').each(function() {
				limparCheck($(this));
				$("#datasPrevista"+ $(this).val()).prop('readonly', true);
				$("#duracao"+ $(this).val()).prop('readonly', true);
				$(this).removeAttr('checked');
			});

	      	if (idGrupo != "-1") {		      	
		      	AjaxService.obterGrupoFaturamento( idGrupo,idCronogramaFaturamento,mesAnoPartidaInput,cicloPartidaInput,
		           	function(cronogramaFaturamento) {
		      		if(cronogramaFaturamento != null){
			      		document.getElementById("campoAnterior").value = "";			      		
		      			document.getElementById("aviso").style.display = 'block';
		      			descPeriodicidade.innerHTML= cronogramaFaturamento["descPeriodicidade"];
		      			qtdRotas.innerHTML= cronogramaFaturamento["qtdRotas"];
		      			referenciaAtual.innerHTML = cronogramaFaturamento["referenciaAtual"];
		      			qtdMedidores.innerHTML= cronogramaFaturamento["qtdMedidores"];
		      			cronogramasAbertos.innerHTML= cronogramaFaturamento["cronogramasAbertos"];
		      			qtdeMaximaCronogramasAbertos.innerHTML= cronogramaFaturamento["qtdeMaximaCronogramasAbertos"];

		      			$(".campoData").datepicker("destroy");
		    			$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button',
		    				buttonImage: '<c:url value="/imagens/calendario.png"/>', 
		    				defaultDate: "01/" + cronogramaFaturamento["mesAnoPartida"],
		    				buttonImageOnly: true, buttonText: 'Exibir Calend?rio', dateFormat: 'dd/mm/yy'});

		      			if (idGrupo != idGrupoHidden || primeiraEntrada == 1){
			      			if(primeiraEntrada == 1){
				      			document.getElementById("primeiraEntrada").value = 2;
				      		}
				      		
		      				mesAnoPartida.value = cronogramaFaturamento["mesAnoPartida"];
		      				
		      				document.getElementById("mesAnoPartidaInput").value = cronogramaFaturamento["mesAnoPartida"];
      						
      						
      						document.getElementById("grupoFaturamentoHidden").value = document.getElementById("grupoFaturamento").value;

      						listaChaveAtividades = cronogramaFaturamento["listaChaveAtividades"];

      						if(listaChaveAtividades != undefined){
      							for (var i = 0; i < listaChaveAtividades.length; i++) {
          							if(listaChaveAtividades.value == undefined ||  
          								listaChaveAtividades.value == ''){
              								limparAtividades();
                  					}
          						}
          						
          					}else{
          						limparAtividades();
                  			}
          						
		      			}
		      			campoCicloPartidaInput.value = cronogramaFaturamento["qtdCiclos"];
		      			campoCicloPartida.value = cronogramaFaturamento["qtdCiclos"];
		      			primeiroCronograma = cronogramaFaturamento["primeiroCronograma"];
		      			
						if (cronogramaFaturamento["listaChaveAtividades"] != undefined){
							listaChaveAtividades = cronogramaFaturamento["listaChaveAtividades"];
			      			listaDatasPrevistas = cronogramaFaturamento["listaDatasPrevistas"];
			      			listaDatasIniciais = cronogramaFaturamento["listaDatasIniciais"];
			      			listaDatasFinais = cronogramaFaturamento["listaDatasFinais"];
			      			
			      			popularDatasPrevistas(listaDatasPrevistas, listaChaveAtividades, listaDatasIniciais, listaDatasFinais);
						}
		      			
		    			if (primeiroCronograma == "true"){
		    				document.getElementById("mesAnoPartidaInput").disabled = false;
		    				document.getElementById("cicloPartidaInput").disabled = false;		    				
		    				
		    			} else {
		    				document.getElementById("mesAnoPartidaInput").disabled = true;
		    				document.getElementById("cicloPartidaInput").disabled = true;
		    				
						}
		      		  }else{
		      			alert(" Data inv�lida.");

		      		  }
					}
				);
			} else {			
				descPeriodicidade.innerHTML= "";
      			qtdRotas.innerHTML = "";
      			referenciaAtual.innerHTML ="";
      			qtdMedidores.innerHTML= "";
      			cronogramasAbertos.innerHTML= "";
      			qtdeMaximaCronogramasAbertos.innerHTML = "";
      			
      			campoCicloPartidaInput.value ="";
      			campoCicloPartida.value ="";
      			document.getElementById("mesAnoPartida").value = "";
      			document.getElementById("mesAnoPartidaInput").value = "";
      			document.getElementById("aviso").style.display = 'none';
      			document.getElementById("primeiraEntrada").value = 1;
      			
			}
		}

		function limparDuracoes(){
			var listaDuracoes = document.getElementsByName('duracoes');
			for (var i = 0; i < listaDuracoes.length; i++) {
				listaDuracoes[i].value = "";
			}
		}
		
		function limparDatasPrevistas(){
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');
			for (var i = 0; i < listaDatasPrevistas.length; i++) {
				listaDatasPrevistas[i].value = "";
			}
		}

		function limparAtividades(){
			limparDuracoes();
			limparDatasPrevistas();
		}
		
		function limparCronograma() {
			
			limparAtividades();
			
			document.getElementById('checkAllAuto').checked = false;
			document.getElementById('cicloPartidaInput').value = "";
			document.getElementById('primeiraEntrada').value = "1";
			
			var listaChavesPrimariasMarcadas = document.getElementsByName('chavesPrimariasMarcadas');
			for (var i = 0; i < listaChavesPrimariasMarcadas.length; i++) {
				if (!listaChavesPrimariasMarcadas[i].disabled) {
					listaChavesPrimariasMarcadas[i].checked = false;
				}
			}
			
		    tags = document.getElementsByTagName('input');
		    for(i = 0; i < tags.length; i++) {
		        switch(tags[i].type) {
		            case 'text':
		                tags[i].value = '';
		                break;
		        }
		    }

		    tags = document.getElementsByTagName('select');
		    for(i = 0; i < tags.length; i++) {
		        if(tags[i].type == 'select-one') {
		            tags[i].selectedIndex = 0;
		        }
		        else {
		            for(j = 0; j < tags[i].options.length; j++) {
		                tags[i].options[j].selected = -1;
		            }
		        }
		    }
					   
		}

		 function habilitaDesabilitaTodosCampos(){
	            $("input:checkbox[name=chavesPrimariasMarcadas]:checked").each(function(){
	            	habilitaDesabilitaCampo(this);
	            });           
	            $("input:checkbox[name=chavesPrimariasMarcadas]:not(:checked)").each(function(){
	            	habilitaDesabilitaCampo(this);
	            });
			}
		
		function habilitaDesabilitaCampo(campoCheck){
			if(campoCheck.checked == true){
				$("#datasPrevista"+ campoCheck.value).prop('readonly', false);
				$("#duracao"+ campoCheck.value).prop('readonly', false);
				var campoAnterior = $("#campoAnterior").val();
				
					if(campoAnterior != ""){
					 var duracaoAnterior = parseInt($("#duracao" + campoAnterior).val(),10);
					 var dataSugerida = $("#datasPrevista" + campoAnterior).datepicker('getDate');
					 
					 if(!isNaN(duracaoAnterior)){
						 $("#duracao" + campoCheck.value).val(duracaoAnterior);
						 dataSugerida.setDate(dataSugerida.getDate()+duracaoAnterior);
					 }
					 
					 if(dataSugerida!=null){			 	
					 	$("#datasPrevista" + campoCheck.value).datepicker('setDate',dataSugerida);
					 }
				    
					}
					$("#campoAnterior").val($('input[type="checkbox"]:checked').last().val());
 	
			}else{
				$("#campoAnterior").val($('input[type="checkbox"]:checked').last().val());
				limparCheck(campoCheck);
				$("#datasPrevista"+ campoCheck.value).prop('readonly', true);
				$("#duracao"+ campoCheck.value).prop('readonly', true);
                            	
			}

			var datasPrevistas = $("#datasPrevista"+ campoCheck.value);
			datasPrevistas[0].style.borderColor = '';
			var duracao =  $("#duracao"+ campoCheck.value);
			duracao[0].style.borderColor = '';

		}

		function limparCheck(check){
			var listaChavesPrimariasMarcadas = document.getElementsByName('chavesPrimariasMarcadas');
			var listaDuracoes = document.getElementsByName('duracoes');
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');
			
			for (var i = 0; i < listaChavesPrimariasMarcadas.length; i++) {
				if (check.value == listaChavesPrimariasMarcadas[i].value){
					listaDuracoes[i].value = "";
					listaDatasPrevistas[i].value = "";
				}
			}
			
		}

		function popularDatasPrevistas(listaDatas, listaChaveAtividades,listaDatasIniciais, listaDatasFinais){
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');
			var listaChavesPrimarias = document.getElementsByName('chavesPrimariasCronograma');
			
			var $campoData= $(".campoData"); 
			
			if (listaChaveAtividades != undefined){
				
				if(listaDatas != undefined){
					var datasPrevistas = listaDatas.split(",");
				}
				
				var chaveAtividades = listaChaveAtividades.split(",");
				var datasIniciais = listaDatasIniciais.split(",");
				var datasFinais = listaDatasFinais.split(",");
				
				document.getElementById("listaDatasAtividadesIniciais").value = datasIniciais;
    			document.getElementById("listaDatasAtividadesFinais").value = datasFinais;

				for (var i = 0; i < listaChavesPrimarias.length; i++) {
					for (var j = 0; j < chaveAtividades.length; j++){
						if (chaveAtividades[j] == listaChavesPrimarias[i].value){
							
							if(listaDatas != undefined){
								listaDatasPrevistas[i].value = datasPrevistas[j];
							}
							
							$("#datasPrevista"+listaChavesPrimarias[i].value).datepicker(
									{dateFormat: 'dd/mm/yy', minDate: datasIniciais[j], 
										maxDate: datasFinais[j] , showOn: 'button', 
										buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, 
										buttonText: 'Exibir Calend�rio', onClose: function() {validarDuracaoDias(this);}});
							
						}
					}
				}
				
				$('input[name="datasPrevistas"]').each(function(){
					$(this).datepicker('setDate', $(this)[0].getAttribute('value'));
				});
			}
		}
		
		function validarDuracaoDias(datepicker){
			$('#cronogramaFaturamento > tbody > tr').each(function(i){
				var duracao = $(this).find('td > input[name="duracoes"]').val();
				var dataPrevista = $(this).find('td > div > input[name="datasPrevistas"]').val();
				var listaDatasAtividadesIniciais = document.getElementById("listaDatasAtividadesIniciais").value;
				var check = $(this).find('td > input[name="chavesPrimariasMarcadas"]')[0];

				if (duracao != undefined && duracao != "" && dataPrevista != undefined && dataPrevista != "") {
					AjaxService.validarDuracaoDias(i+1, duracao, listaDatasAtividadesIniciais, dataPrevista,  
						{ callback: function(lancouExcecao) {	   
								if(lancouExcecao["lancouExcecao"] == true){
									alert(" A dura��o de dias est�o ultrapassando o limite de intervalo de datas.");
									limparCheck(check);
								}
						}
						, async: false }
					);
				}
			});

			var datas = $('#cronogramaFaturamento > tbody > tr > ')
				.find('td > div > input[name="datasPrevistas"]')
				.map(function(){
					return $(this).datepicker('getDate');
				});
			datas.each(function(i){
				if(datas[i] > datas[i+1]) {
					alert("As datas finais precisam estar ordenadas.");
					var check = $(datepicker).parents('tr').find('td > input[name="chavesPrimariasMarcadas"]')[i];
					limparCheck(check);
				}
			});
		}
		
		addLoadEvent(init);

        function fecharModal(){
            $('#camposVermelhos').css('display','none')
        };


</script>

<div class="bootstrap">
	<form:form method="post" action="incluirCronograma"  id="cronogramaForm" name="cronogramaForm">
			<input name="acao" type="hidden" id="acao" value="inserirCronograma"> 
			<input name="postBack" type="hidden" id="postBack" value="true">
			<input name="indexLista" type="hidden" id="indexLista" value="${cronogramaVO.indexLista}">
			<input name="chaveAtividadeSistema" type="hidden" id="chaveAtividadeSistema" value="${cronogramaVO.chaveAtividadeSistema}"/>
			<input name="descricaoAtividade" type="hidden" id="descricaoAtividade" value="${cronogramaVO.descricaoAtividade}"/>
			<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cronogramaVO.chavePrimaria}"/>
			<input name="dataInicio" type="hidden" id="dataInicio" value="${cronogramaVO.dataInicio}"/>
			<input name="dataFim" type="hidden" id="dataFim" value="${cronogramaVO.dataFim}"/>
			<input name="versao" type="hidden" id="versao" value="${cronogramaVO.versao}">	
			<input name="cicloPartida" type="hidden" id="cicloPartida" value="${cronogramaVO.cicloPartida}">
			<input name="dataInicialAtividade" type="hidden" id="dataInicialAtividade" value="${cronogramaVO.dataInicialAtividade}">
			<input name="dataFinalAtividade" type="hidden" id="dataFinalAtividade" value="${cronogramaVO.dataFinalAtividade}">
			<input name="primeiroCronograma" type="hidden" id="primeiroCronograma" value="${cronogramaVO.primeiroCronograma}">
			<input name="mesAnoPartida" type="hidden" id="mesAnoPartida" value="${cronogramaVO.mesAnoPartida}">
			<input name="grupoFaturamentoHidden" type="hidden" id="grupoFaturamentoHidden" value="${cronogramaVO.grupoFaturamento}">
			<input name="primeiraEntrada" type="hidden" id="primeiraEntrada" value="1">
			<input name="campoAnterior" type="hidden" id="campoAnterior" value="">
			<input name="listaDatasAtividadesIniciais" type="hidden" id="listaDatasAtividadesIniciais" value="${cronogramaVO.listaDatasAtividadesIniciais}">
			<input name="listaDatasAtividadesFinais" type="hidden" id="listaDatasAtividadesFinais" value="${cronogramaVO.listaDatasAtividadesFinais}">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Incluir Cronograma</h5>
			</div>
			
			<div class="card-body">
				
			
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Para incluir um Cronograma de Faturamento preencha os dados e configure as Atividades do Cronograma na 
         			lista abaixo e clique em <strong>Salvar</strong>.
   			 	</div>
				<div class="alert alert-danger alert-dismissible fade show" role="alert" id="camposVermelhos" style="background-color: #c23838 !important;color: #FFF;font-weight: bold; display: none;">
					<strong>Campos obrigatrios no preenchidos, verificar campos destacados em vermelho </strong>
					<button type="button" class="close" onclick="fecharModal();">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

   			 	<div class="row">
   			 		<div class="col-md-6">
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 					<label for="grupoFaturamento">Grupo de Faturamento: <span class="text-danger">*</span></label>
             					<select name="grupoFaturamento" id="grupoFaturamento" class="form-control form-control-sm" 
             						 onchange="carregarDadosGrupoFaturamento(this, null)">
                  					<option value="-1">Selecione</option>
                  					<c:forEach items="${listaGrupoFaturamentos}" var="grupoFaturamento">
                      					<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${cronogramaVO.grupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${grupoFaturamento.descricao}"/>
					 					</option>
                  					</c:forEach>
           						</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="mesAnoPartidaInput">M�s/Ano de Partida:</label>
        		 				<input type="text" aria-label="mesAnoPartidaInput" id="mesAnoPartidaInput"
                            		class="form-control form-control-sm" readonly
                            		onkeypress="return formatarCampoInteiro(event,6);"
                            		name="mesAnoPartidaInput" maxlength="7"
                            		value="${cronogramaVO.mesAnoPartida}">
        		 				
        		 			</div>
        		 		</div>
        		 		<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="cicloPartidaInput">Ciclo de Partida:</label>
        		 				<input type="text" aria-label="cicloPartidaInput" id="cicloPartidaInput"
                            		class="form-control form-control-sm" readonly
                            		onkeypress="return formatarCampoInteiro(event,6);"
                            		name="cicloPartidaInput" maxlength="7"
                            		value="${cronogramaVO.cicloPartida}">
        		 			</div>
        		 		</div>
        		 		<div class="form-row">
        		 			<div class="col-md-10">
        		 				<label for="quantidadeCronogramas">Quantidade de Cronogramas: <span class="text-danger">*</span></label>
        		 				<input type="text" aria-label="quantidadeCronogramas" id="quantidadeCronogramas"
                            		class="form-control form-control-sm"
                            		onkeypress="return formatarCampoInteiro(event,6);"
                            		name="quantidadeCronogramas" maxlength="7"
                            		value="${cronogramaVO.quantidadeCronogramas}" required>
                            	<p id="aviso" style="display: none;"> Quantidade de cronogramas disponveis para inclus�o:
                            	<span id="qtdeMaximaCronogramasAbertos"><c:out value='${cronogramaVO.qtdeMaximaCronogramasAbertos}'/></span></p>
			
        		 			</div>
        		 		</div>
        		 		
   			 		</div>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
  							<li class="list-group-item">
  								<p>Periodicidade: <span class="font-weight-bold" id="descPeriodicidade">
  								<c:out value='${cronogramaVO.descPeriodicidade}'/></span></p>
  							</li>
  							<li class="list-group-item">
  								<p>Rotas: <span class="font-weight-bold" id="qtdRotas">
  								<c:out value='${cronogramaVO.qtdRotas}'/></span></p>
  							</li>
  							<li class="list-group-item">
  								<p>Refer�ncia Atual: <span class="font-weight-bold" id="referenciaAtual">
  								<c:out value='${cronogramaVO.referenciaAtual}'/></span></p>
  							</li>
 							<li class="list-group-item">
 								<p>Medidores: <span class="font-weight-bold" id="qtdMedidores">
 								<c:out value='${cronogramaVO.qtdMedidores}'/></span></p>
 							</li>
  							<li class="list-group-item">
  								<p>Cronogramas Abertos: <span class="font-weight-bold" id="cronogramasAbertos">
 								<c:out value='${cronogramaVO.cronogramasAbertos}'/></span> </p>
  							</li>
						</ul>			 			
   			 		</div>
   			 	
   			 	</div>
   			 	<hr/>
   			 	<h5>Atividades</h5>
   			 	<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Configure as Atividades abaixo e, se necessrio, selecione outras Atividades opcionais que faro parte do Cronograma e clique em <strong>Salvar</strong>.<br />
   			 	</div>
   			 
   			 	<div class="table-responsive">
   			 		 <table class="table table-bordered table-striped table-hover" id="cronogramaFaturamento" width="100%">
         	 			<thead class="thead-ggas-bootstrap">
         	 				<tr>
                 	 			<th class="text-center">
                     				<input type='checkbox' name='checkAllAuto' id='checkAllAuto' onchange="habilitaDesabilitaTodosCampos()"/>
                     			</th>
                    			<th scope="col" class="text-center">Sequ�ncia</th>
                    			<th scope="col" class="text-center">Atividade</th>
                   			 	<th scope="col" class="text-center">Obrigat�ria</th>
                    			<th scope="col" class="text-center">Predecessora</th>
                    			<th scope="col" class="text-center">Dura��o</th>
                    			<th scope="col" class="text-center">Data Limite</th>
                  			</tr>
         	 			</thead>
         	 			<tbody>
         	 				<c:forEach items="${listaCronogramaAtividadeFaturamentoVO}" var="cronogramaFaturamento">
         	 				<tr>
         	 					<td class="text-center">
         	 					 	<input type="hidden" name="chavesPrimariasCronograma" value="${cronogramaFaturamento.atividadeSistema.chavePrimaria}"/>
         	 					 	<input name="chavesPrimariasMarcadas" type="checkbox"
                                          value="${cronogramaFaturamento.atividadeSistema.chavePrimaria}" onchange="habilitaDesabilitaCampo(this)">
               					</td>
               					<td class="text-center">
               						<c:out value="${cronogramaFaturamento.atividadeSistema.sequencia}"/>
               					</td>
               					<td class="text-center">
               						<c:out value="${cronogramaFaturamento.atividadeSistema.descricao}"/>
               					</td>
               					<td class="text-center">
               						<c:choose>
	         							<c:when test="${cronogramaFaturamento.atividadeSistema.obrigatoria == true}">
	         								<c:out value="Sim"/>
	         							</c:when>
	         							<c:otherwise>
	         								<c:out value="No"/>
            							</c:otherwise>
            						</c:choose>
               					</td>
           
               					<td class="text-center">
               						<c:out value="${cronogramaFaturamento.atividadeSistema.atividadePrecedente.descricao}"/>
               					</td>
               					<td>
               						<c:choose>
				     					<c:when test="${cronogramaFaturamento.duracao > 0}">
				       						<input class="form-control form-control-sm" type="text" name="duracoes" id="duracao${cronogramaFaturamento.atividadeSistema.chavePrimaria}" 
				       							maxlength="2" size="2" onblur="validarDuracaoDias(this);removerErro(this);" value="<c:out value="${cronogramaFaturamento.duracao}"/>" onkeypress="return formatarCampoInteiro(event)" readonly/>
				    				 	</c:when>
		
				     					<c:otherwise>
				       						<input class="form-control form-control-sm" type="text" name="duracoes" id="duracao${cronogramaFaturamento.atividadeSistema.chavePrimaria}"
				       					 		maxlength="2" size="2" onblur="validarDuracaoDias(this);removerErro(this);" value="" onkeypress="return formatarCampoInteiro(event)" readonly/>
				      					</c:otherwise>
		        					</c:choose>
               					</td>
               					<td>
               					 <div class="input-group input-group-sm" id="dataLimite">

                                    <input type="text" aria-label="Data Limite"
                                          class="form-control form-control-sm campoData"
                                          id="datasPrevista${cronogramaFaturamento.atividadeSistema.chavePrimaria}"
										   onblur="removerErro(this);"
                                          name="datasPrevistas" size="10" readonly
                                          value="${cronogramaFaturamento.dataPrevistaFormatada}">
                                </div>

               					</td>
         	 				</tr>
         	 		  
         	 				</c:forEach>
         	 				
         	 			</tbody>
         	 		</table>
   			 	</div><!-- Lista -->
			
			</div>
			<div class="card-footer">
				<div class="row">
      				<div class="col-sm-12">
                        <button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="cancelar();">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                        <button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="limparCronograma();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                        <vacess:vacess param="incluirCronograma">
                  		<button id="buttonGerar" value="Gerar" class="btn btn-sm btn-primary float-right ml-1 mt-1"  type="button" onclick="validaDados();">
                       		<i class="fa fa-plus"></i> Gerar
                   		</button>
             		 	</vacess:vacess>      				
      				</div>
      			</div>
			</div>
		</div>
	</form:form>
</div>
