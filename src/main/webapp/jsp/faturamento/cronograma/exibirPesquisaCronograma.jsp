<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2018 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="br.com.ggas.faturamento.cronograma.impl.CronogramaFaturamentoImpl" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="constante" class="br.com.ggas.faturamento.cronograma.impl.CronogramaFaturamentoImpl">

</jsp:useBean>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>


<script type="text/javascript">
		$(document).ready(function(){
			iniciarDatatable('#cronograma', {"order" : [[2, 'asc']]});
			
			//Define o mascaramento para os campos de 
			$("#mesAnoFaturamentoInicial,#mesAnoFaturamentoFinal").inputmask("99/9999",{placeholder:"_"});
					
		});
		
		function removerCronograma(){
			var formulario = document.forms[0];
			var flag = 0;
			var chaveSelecionada;

			if (formulario != undefined && formulario.chavesPrimarias != undefined) {

				var chavesPrimarias = formulario.chavesPrimarias;

				if(NodeList.prototype.isPrototypeOf(formulario.chavesPrimarias)) {
					chavesPrimarias = Array.prototype.slice.call(chavesPrimarias)
				}
				chavesPrimarias = [].concat(chavesPrimarias);

				var total = chavesPrimarias.length;
				if (total != undefined) {
					for (var i = 0; i< total; i++) {
						if(chavesPrimarias[i].checked == true){
							flag++;
							chaveSelecionada = chavesPrimarias[i].value;
						}
					}
				}
			}
			
			var selecao = verificarSelecao();
			if (selecao == true) {
				if (flag > 1) {
					alert('N�o � poss�vel remover dois ou mais cronogramas de faturamento simultaneamente.');
				} else {
					if (selecao == true) {	
						var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
						if(retorno == true) {
							document.getElementById("chavePrimaria").value = chaveSelecionada;

							submeter('pesquisarCronogramaForm', 'removerCronograma');

						}
					}
				}
			}
		}

		function alterarCronograma(){			
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('pesquisarCronogramaForm', 'exibirAlteracaoCronograma');
		    }
		}

		function detalharCronograma(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("pesquisarCronogramaForm", "exibirDetalhamentoCronogramaAcompanhamento")
		}
		
		function incluirCronograma() {
			submeter('pesquisarCronogramaForm', 'exibirInclusaoCronograma');
		}		
		
		function limparFormulario(){
			$(":text").val("");
			$("select").val("-1");
			$("#ativo").attr("checked","checked");
		}
		
		function escalonarLeiturista(){
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('pesquisarCronogramaForm', 'exibirEscalonamentoLeiturista');
		    }
		}
		
	</script>
<div class="bootstrap">
  <form:form action="pesquisarCronograma" id="pesquisarCronogramaForm" name="pesquisarCronogramaForm" method="post">
  		<div class="card">
  			<div class="card-header">
            	<h5 class="card-title mb-0">Pesquisar Cronograma</h5>
        	</div>
        	<div class="card-body">
        		<input name="acao" type="hidden" id="acao" value="pesquisarCronograma">
				<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
				<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
				<input name="dataAnterior" type="hidden" id="dataAnterior" value="">
				<input type="hidden" id="grupoFaturamento" value="">
				
				
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em
        			<b>Pesquisar</b>, ou clique apenas em <b>Pesquisar</b> para exibir todos. Para incluir um novo
        			 registro clique em <b>Incluir</b>
   			 	</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-row">
							<div class="col-md-12">
								<label for="grupoFaturamento">Grupo de Faturamento:</label>
								<select name="grupoFaturamento" id="grupoFaturamento" class="form-control form-control-sm">
									<option value="-1">Selecione</option>
									<c:forEach items="${listaGrupoFaturamentos}" var="grupoFaturamento">
										<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${cronogramaVO.grupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${grupoFaturamento.descricao}"/>
										</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-row">
							<div class="col-md-12">
								<label for="periodicidade" >Periodicidade:</label>
								<select name="periodicidade" id="periodicidade" class="form-control form-control-sm">
									<option value="-1">Selecione</option>
									<c:forEach items="${listaPeriodicidades}" var="periodicidade">
										<option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${cronogramaVO.periodicidade == periodicidade.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${periodicidade.descricao}"/>
										</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-row">
							<div class="col-md-12">
								<label for="situacao" >Status:</label>
								<select name="situacao" id="situacao" class="form-control form-control-sm">
									<option value="-1">Selecione</option>
									<c:forEach items="${listaSituacao}" var="situacao">
										<option value="<c:out value="${situacao}"/>" <c:if test="${cronogramaVO.situacao == situacao}">selected="selected"</c:if>>
											<c:out value="${situacao}"/>
										</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
					<div class="col-md-6">
						<div class="form-row">
							<div class="col-md-12">
								<label for="ultimaEtapa" >�ltima A��o Executada:</label>
								<select name="ultimaEtapa" id="ultimaEtapa" class="form-control form-control-sm">
									<option value="-1">Selecione</option>
									<c:forEach items="${atividadesSistema}" var="atividadeSistema">
										<option value="<c:out value="${atividadeSistema.chavePrimaria}"/>"
												<c:if test="${cronogramaVO.ultimaEtapa == atividadeSistema.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${atividadeSistema.descricao}"/>
										</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12">
								<label for="mesAnoFaturamento">Per�odo Refer�ncia do Faturamento:</label>
								<div class="input-group input-group-sm">

									<input type="text" aria-label="mesAnoFaturamentoInicial" id="mesAnoFaturamentoInicial"
										   class="form-control form-control-sm"
										   onkeypress="return formatarCampoInteiro(event,6);"
										   name="mesAnoFaturamentoInicial" maxlength="7"
										   value="${cronogramaVO.mesAnoFaturamentoInicial}">
									<div class="input-group-prepend">
										<span class="input-group-text">at�</span>
									</div>
									<input type="text" aria-label="mesAnoFaturamentoFinal" id="mesAnoFaturamentoFinal"
										   class="form-control form-control-sm"
										   onkeypress="return formatarCampoInteiro(event,6);"
										   name="mesAnoFaturamentoFinal" maxlength="7"
										   value="${cronogramaVO.mesAnoFaturamentoFinal}">
								</div>
							</div>
						</div>

						<div class="form-row">
							<label for="habilitado" class="col-md-12">Indicador de Uso:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoAtivo" name="habilitado" class="custom-control-input" value="true"
										   <c:if test="${cronogramaVO.habilitado eq 'true'}">checked="checked"</c:if>>
									<label class="custom-control-label" for="habilitadoAtivo">Ativo</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoInativo" name="habilitado" class="custom-control-input" value="false"
										   <c:if test="${cronogramaVO.habilitado eq 'false'}">checked="checked"</c:if>>
									<label class="custom-control-label" for="habilitadoInativo">Inativo</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoTodos" name="habilitado" class="custom-control-input" value=""
										   <c:if test="${cronogramaVO.habilitado eq ''}">checked="checked"</c:if>>
									<label class="custom-control-label" for="habilitadoTodos">Todos</label>
								</div>
							</div>
						</div>
					</div>
				</div>

   			 <div class="row mt-3">
                    <div class="col align-self-end text-right">
                        <button class="btn btn-primary btn-sm" id="botaoPesquisar" type="submit">
                            <i class="fa fa-search"></i> Pesquisar
                        </button>
                        <button class="btn btn-secondary btn-sm" id="botaoLimpar" type="button"
                                onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                    </div>
                </div>
                
             <c:if test="${listaCronograma ne null}">
		<hr/>
		
		<div class="table-responsive">
		
		 <table class="table table-bordered table-striped table-hover" id="cronograma" style="width:100%">
         	 <thead class="thead-ggas-bootstrap">
                 <tr>
                 	 <th class="text-center" data-orderable="false">
                     	<input type='checkbox' name='checkAllAuto' id='checkAllAuto' disabled/>
                     </th>
                    <th scope="col" class="text-center">Ativo</th>
                    <th scope="col" class="text-center">Grupo de Faturamento</th>
                    <th scope="col" class="text-center">Ano/M�s-Ciclo</th>
                    <th scope="col" class="text-center">Periodicidade</th>
                    <th scope="col" class="text-center">Status</th>
                    <th scope="col" class="text-center">Per�odo de Leitura</th>
                  </tr>
               </thead>
               <tbody>
               		<c:forEach items="${listaCronograma}" var="cronograma">
               			<tr>
               				<td class="text-center">
               				 	<input id="checkboxChavesPrimarias${cronograma.chavePrimaria}" type="checkbox" name="chavesPrimarias"
                                               value="${cronograma.chavePrimaria}">
               				</td>
               				<td class="text-center">
               					<c:choose>
									<c:when test="${cronograma.habilitado == true}">
										<i class="fas fa-check-circle text-success" title="Ativo"></i>
									</c:when>
									<c:otherwise>
										<i class="fas fa-ban text-danger" title="Inativo"></i>
									</c:otherwise>
								</c:choose>
               				</td>               				
               				<td class="text-center">
               					<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);">
	            					<c:out value="${cronograma.grupoFaturamento.descricao}"/>
	           					</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);">
	            					<c:out value="${cronograma.anoMesFaturamentoMascara}"/>-<c:out value="${cronograma.numeroCiclo}"/>
	            				</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);">
	            					<c:out value="${cronograma.grupoFaturamento.periodicidade.descricao}"/>
	            				</a>
               				</td>
               				<td class="text-center">
               				<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);">
	            				<c:if test="${cronograma.anoMesFaturamento < cronograma.grupoFaturamento.anoMesReferencia}">
	            					${constante.situacaoConcluido}
	            				</c:if>
	            				<c:if test="${cronograma.anoMesFaturamento eq cronograma.grupoFaturamento.anoMesReferencia}">
	            					<c:if test="${cronograma.numeroCiclo < cronograma.grupoFaturamento.numeroCiclo}">
	            						${constante.situacaoConcluido}
	            					</c:if>
	            					<c:if test="${cronograma.numeroCiclo eq cronograma.grupoFaturamento.numeroCiclo}">
	            						${constante.situacaoAndamento}
	            					</c:if>
	            					<c:if test="${cronograma.numeroCiclo > cronograma.grupoFaturamento.numeroCiclo}">
	            						${constante.situacaoAgendado}
	            					</c:if>
	            				</c:if>
	            				<c:if test="${cronograma.anoMesFaturamento > cronograma.grupoFaturamento.anoMesReferencia}">
	            					${constante.situacaoAgendado}
	            				</c:if>	            	
	            				</a>
               				</td>
               				<td class="text-center">
               					<a href="javascript:detalharCronograma(<c:out value='${cronograma.chavePrimaria}'/>);">
	            					<c:out value="${cronograma.periodoLeitura}"/>
	            				</a>
               				</td>
               			</tr>
               		</c:forEach>
               </tbody>
			</table>
			</div>
		</c:if>
            
        	
        	</div>
        	<div class="card-footer">
        	 <div class="row">
      			<div class="col-sm-12">
             		<vacess:vacess param="exibirInclusaoCronograma">
                  		<button id="botaoIncluir" value="Incluir" class="btn btn-sm btn-primary float-right ml-1 mt-1" onclick="incluirCronograma();">
                       		<i class="fa fa-plus"></i> Incluir
                   		</button>
             		 </vacess:vacess>
              		<c:if test="${listaCronograma ne null}">
              			<vacess:vacess param="exibirAlteracaoCronograma">
    						<button id="buttonAlterar" type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-left" onclick="alterarCronograma();">
        						<i class="fas fa-pencil-alt"></i> Alterar
    						</button>
						</vacess:vacess>
						<vacess:vacess param="removerCronograma">
    						<button name="buttonRemover" type="button" class="btn btn-danger btn-sm ml-1 mt-1 float-left" onclick="removerCronograma();">
        						<i class="far fa-trash-alt"></i> Remover
    						</button>
						</vacess:vacess>
						<vacess:vacess param="exibirEscalonamentoLeiturista">
    						<button name="buttonEscalonarLeituristas"  type="button" class="btn btn-secondary btn-sm ml-1 mt-1 float-left"
            				onclick="escalonarLeiturista();">
        						<i class="fas fa-users"></i> Escalonar Leituristas
    						</button>
						</vacess:vacess>
              		</c:if>
       			 </div>
     		</div>
  	
   	</div>
     	
  	</div>
  </form:form>
</div>

