<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>
	$.extend( true, $.fn.dataTable.defaults, {
		"stateSave": true,
		"paginate": false,
		"searching": true,
		"info": false,
		"ordering": false,
		"language": {
       		"zeroRecords": "Nenhum registro foi encontrado",
        	"search": "Procurar:",
        	"processing": "Processando..."
        }
	} );

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button',
			buttonImage: '<c:url value="/imagens/calendario.png"/>', 
			buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
			$('#cronogramaFaturamento').DataTable();
			iniciarDatatable("#cronogramaRota");
	});
		
		function limpar(){
				
		}

		function detalharRotas(chave,descricao){
			document.forms[0].chaveAtividadeSistema.value = chave;
			document.forms[0].descricaoAtividade.value = descricao;
			submeter('cronogramaForm', 'exibirAlteracaoCronograma');
		}
		function cancelar() {
			location.href = '<c:url value="/exibirPesquisaCronograma"/>';
		}

		function detalharAtividade(){
			document.forms[0].chavePrimaria.value = chave;
			$.scrollTo($('#conteinerCronogramaRotas'),800);
		}


		function aplicarCronogramaRotasAlterar() {
			submeter('cronogramaForm', 'aplicarCronogramaRotasAlteracao');
			$.scrollTo($('#conteinerAtividades'),800);
		}
		
		function salvar(){
			submeter('cronogramaForm', 'alterarCronograma');
		}
				
		function init () {
			
			onloadCronogramaRota();
			carregarDadosGrupoFaturamento(document.getElementById("grupoFaturamento").value);
			$('input[name="chavesPrimariasMarcadas"]').trigger("change");
		}

		function limparAtividades(){

			var listaDuracoes = document.getElementsByName('duracoes');
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');

			for (var i = 0; i < listaDuracoes.length; i++) {
				listaDuracoes[i].value = "";
			}
			
			for (var i = 0; i < listaDatasPrevistas.length; i++) {
				listaDatasPrevistas[i].value = "";
			}
		}

		function habilitaDesabilitaTodosCampos(){
            $("input:checkbox[name=chavesPrimariasMarcadas]:checked").each(function(){
            	habilitaDesabilitaCampo(this);
            });           
            $("input:checkbox[name=chavesPrimariasMarcadas]:not(:checked)").each(function(){
            	habilitaDesabilitaCampo(this);
            });
		}
		
		function habilitaDesabilitaCampo(campoCheck){
			if(campoCheck.checked == true){
				$("#dataPrevista"+ campoCheck.value).datepicker('enable');
				$("#datasPrevista"+ campoCheck.value).prop('readonly', false);
				$("#duracao"+ campoCheck.value).prop('readonly', false);
 	
			}else{
				$("#campoAnterior").val($('input[type="checkbox"]:checked').last().val());
				limparCheck(campoCheck);
				$("#datasPrevista"+ campoCheck.value).prop('readonly', true);
				$("#duracao"+ campoCheck.value).prop('readonly', true);
			}
			
		}

		function limparCheck(check){
			var listaChavesPrimariasMarcadas = document.getElementsByName('chavesPrimariasMarcadas');
			var listaDuracoes = document.getElementsByName('duracoes');
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');
			
			for (var i = 0; i < listaChavesPrimariasMarcadas.length; i++) {
				if (check.value == listaChavesPrimariasMarcadas[i].value){
					listaDuracoes[i].value = "";
					listaDatasPrevistas[i].value = "";
				}
			}
			
		}

		function onloadCronograma() {
			
			<c:forEach items="${listaCronogramaAtividadeFaturamentoVO}" var="cronogramaAtividadeFaturamentoVO" varStatus="index">
		    	$('#dataPrevista${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}').datepicker({dateFormat: 'dd/mm/yy', minDate: '${cronogramaAtividadeFaturamentoVO.dataInicio}', maxDate: '${cronogramaAtividadeFaturamentoVO.dataPrevista}' , showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', onClose: function() {validarDuracaoDias(this);}});
			</c:forEach>

		}
		
		function onloadCronogramaRota() {
			
			<c:forEach items="${listaCronogramaAtividadeRotaVO}" var="cronograma" varStatus="index">
				$('#dataPrevistaRota${cronograma.rota.chavePrimaria}').datepicker({ minDate: '${cronograma.dataInicio}' , maxDate: '${cronograma.dataPrevista}' , showOn: 'button', 
					buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'
				});		
			</c:forEach>
		}
		
		function limparDuracoes(){
			var listaDuracoes = document.getElementsByName('duracoes');
			for (var i = 0; i < listaDuracoes.length; i++) {
				listaDuracoes[i].value = "";
			}
		}
		
		function limparDatasPrevistas(){
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');
			for (var i = 0; i < listaDatasPrevistas.length; i++) {
				listaDatasPrevistas[i].value = "";
			}
		}

		function limparAtividades(){
			limparDuracoes();
			limparDatasPrevistas();
		}
		
		function popularDatasPrevistas(listaDatas, listaChaveAtividades,listaDatasIniciais, listaDatasFinais){
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');
			var listaChavesPrimarias = document.getElementsByName('chavesPrimariasCronograma');
			
			var $campoData= $(".campoData"); 
			//$campoData.datepicker( "destroy" );
			
			if (listaChaveAtividades != undefined){
				
				if(listaDatas != undefined){
					var datasPrevistas = listaDatas.split(",");
				}
				
				var chaveAtividades = listaChaveAtividades.split(",");
				var datasIniciais = listaDatasIniciais.split(",");
				var datasFinais = listaDatasFinais.split(",");
				
				document.getElementById("listaDatasAtividadesIniciais").value = datasIniciais;

				for (var i = 0; i < listaChavesPrimarias.length; i++) {
					for (var j = 0; j < chaveAtividades.length; j++){
						if (chaveAtividades[j] == listaChavesPrimarias[i].value){
							
							if(listaDatas != undefined){
								listaDatasPrevistas[i].value = datasPrevistas[j];
							}
							
							$("#dataPrevista"+listaChavesPrimarias[i].value).datepicker(
									{dateFormat: 'dd/mm/yy', minDate: datasIniciais[j], 
										maxDate: datasFinais[j] , showOn: 'button', 
										buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, 
										buttonText: 'Exibir Calend�rio', onClose: function() {validarDuracaoDias(this);}});
							
						}
					}
				}
				
				$('input[name="datasPrevistas"]').each(function(){
					$(this).datepicker('setDate', $(this)[0].getAttribute('value'));
				});
			}
		}
		
		function carregarDadosGrupoFaturamento(idGrupoFaturamento, isMesAnoPartidaInputModificado){			
			var idCronogramaFaturamento = null;
			var idGrupo = document.getElementById("grupoFaturamento").value;
			var descPeriodicidade = document.getElementById("descPeriodicidade");
			var referenciaAtual = document.getElementById("referenciaAtual");
			var qtdRotas = document.getElementById("qtdRotas");
			var qtdMedidores = document.getElementById("qtdMedidores");
			var mesAnoPartida = document.getElementById("mesAnoPartida");
			var campoCicloPartidaInput = document.getElementById("cicloPartidaInput");
			var campoCicloPartida = document.getElementById("cicloPartida");
			var listaChaveAtividades = "";
			var listaDatasPrevistas = "";
			var listaDatasIniciais = "";
			var listaDatasFinais = "";
			var mesAnoPartidaInput = null;
			var cicloPartidaInput = null;
			var primeiroCronograma;
			
			if(isMesAnoPartidaInputModificado != undefined && isMesAnoPartidaInputModificado != null){
				mesAnoPartidaInput = document.getElementById("mesAnoPartidaInput").value;
				cicloPartidaInput = document.getElementById("cicloPartidaInput").value;
			}
			

	      	if (idGrupo != "-1") {		      	
		      	AjaxService.obterGrupoFaturamento( idGrupo,idCronogramaFaturamento,mesAnoPartidaInput,cicloPartidaInput,
		           	function(cronogramaFaturamento) {
		      		if(cronogramaFaturamento != null){
		      			descPeriodicidade.innerHTML = cronogramaFaturamento["descPeriodicidade"];
		      			qtdRotas.innerHTML = cronogramaFaturamento["qtdRotas"];
		      			qtdMedidores.innerHTML = cronogramaFaturamento["qtdMedidores"];
		      			campoCicloPartida.value = cronogramaFaturamento["qtdCiclos"];
		      			primeiroCronograma = cronogramaFaturamento["primeiroCronograma"];
		      			
						if (cronogramaFaturamento["listaChaveAtividades"] != undefined){
							listaChaveAtividades = cronogramaFaturamento["listaChaveAtividades"];
			      			listaDatasPrevistas = cronogramaFaturamento["listaDatasPrevistas"];
			      			listaDatasIniciais = cronogramaFaturamento["listaDatasIniciais"];
			      			listaDatasFinais = cronogramaFaturamento["listaDatasFinais"];
			      			
			      			popularDatasPrevistas(listaDatasPrevistas, listaChaveAtividades, listaDatasIniciais, listaDatasFinais);
						}
		      		  }else{
		      			alert(" Data inv�lida.");
		      		  }
					}
				);
			} else {			
				descPeriodicidade.innerHTML = "";
      			qtdRotas.innerHTML = "";
      			referenciaAtual.innerHTML ="";
      			qtdMedidores.innerHTML = "";
      			campoCicloPartidaInput.value = 1;
      			campoCicloPartida.value = 1;
      			document.getElementById("mesAnoPartida").value = "";
      			document.getElementById("mesAnoPartidaInput").value = "";
			}
		}
		
		function validarDuracaoDias(datepicker){
			$('#cronogramaAtividadeFaturamentoVO > tbody > tr').each(function(i){
				var duracao = $(this).find('td > input[name="duracoes"]').val();
				var dataPrevista = $(this).find('td > div > input[name="datasPrevistas"]').val();
				var listaDatasAtividadesIniciais = document.getElementById("listaDatasAtividadesIniciais").value;
				var check = $(this).find('td > input[name="chavesPrimariasMarcadas"]')[0];

				if (duracao != undefined && duracao != "" && dataPrevista != undefined && dataPrevista != "") {
					AjaxService.validarDuracaoDias(i+1, duracao, listaDatasAtividadesIniciais, dataPrevista,  
						{ callback: function(lancouExcecao) {	   
								if(lancouExcecao["lancouExcecao"] == true){
									alert(" A dura��o de dias est� ultrapassando o limite de intervalo de datas.");
									limparCheck(check);
								}
						}
						, async: false }
					);
				}
			});

			var datas = $('#cronogramaAtividadeFaturamentoVO > tbody > tr')
				.find('td > div > input[name="datasPrevistas"]')
				.map(function(){
					return $(this).datepicker('getDate');
				});
			datas.each(function(i){
				if(datas[i] > datas[i+1]) {
					alert("As datas finais precisam estar ordenadas.");
					var check = $(datepicker).parents('tr').find('td > input[name="chavesPrimariasMarcadas"]')[0];
					limparCheck(check);
				}
			});
		}
		
		addLoadEvent(init);
		
</script>

<div class="bootstrap">
  <form:form action="exibirAlteracaoCronograma" id="alterarCronogramaForm" name="cronogramaForm" method="post">

	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Alterar Cronograma</h5>
		</div>
		<div class="card-body">
			<input name="acao" type="hidden" id="acao" value="alterarCronograma">
    		<input name="postBack" type="hidden" id="postBack" value="true">
    		<input name="chaveAtividadeSistema" type="hidden" id="chaveAtividadeSistema" value="${cronogramaVO.chaveAtividadeSistema}"/>
			<input name="descricaoAtividade" type="hidden" id="descricaoAtividade" value="${cronogramaVO.descricaoAtividade}"/>
			<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cronogramaVO.chavePrimaria}"/>
			<input name="dataInicio" type="hidden" id="dataInicio" value="${cronogramaVO.dataInicio}"/>
			<input name="grupoFaturamento" type="hidden" id="grupoFaturamento" value="${cronogramaVO.grupoFaturamento}">
			<input name="dataFim" type="hidden" id="dataFim" value="${cronogramaVO.dataFim}"/>
			<input name="versao" type="hidden" id="versao" value="${cronogramaVO.versao}">
			<input name="dataInicialAtividade" type="hidden" id="dataInicialAtividade" value="${cronogramaVO.dataInicialAtividade}">
			<input name="dataFinalAtividade" type="hidden" id="dataFinalAtividade" value="${cronogramaVO.dataFinalAtividade}">
			<input name="mesAnoPartida" type="hidden" id="mesAnoPartida" value="${cronogramaVO.mesAnoPartida}">
			<input name="cicloPartida" type="hidden" id="cicloPartida" value="${cronogramaVO.cicloPartida}">
			<input name="chaveCronogramaFaturamento" type="hidden" id="chaveCronogramaFaturamento" value="${cronogramaVO.chaveCronogramaFaturamento}">
			<input name="listaDatasAtividadesIniciais" type="hidden" id="listaDatasAtividadesIniciais" value="${cronogramaVO.listaDatasAtividadesIniciais}">
		
			<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Informe os dados abaixo e clique em <strong>Salvar</strong> para finalizar.
   			</div>
   			
   			<div class="row">
					<div class="col-md-6">
						<p>Grupo de Faturamento: <span class="font-weight-bold">
  						<c:out value='${cronogramaVO.descricaoGrupoFaturamento}'/></span></p>
  						
  						<p>Ano/M�s-Ciclo: <span class="font-weight-bold">
  						<c:out value='${cronogramaVO.anoMesCiclo}'/></span></p>
					</div>
					<div class="col-md-6">
						<ul class="list-group">
							<li class="list-group-item">
								<p>Periodicidade: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.descricaoPeriodicidade}'/></span></p>
							</li>
							<li class="list-group-item">
								<p>Quantidade de Rotas: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.qtdRotas}'/></span></p>
							</li>
							<li class="list-group-item">
								<p>Quantidade de Medidores: <span class="font-weight-bold">
  								<c:out value='${cronogramaVO.qtdMedidores}'/></span></p>
							</li>
						</ul>
					</div>
				</div><!-- fim da row -->
				
				<hr/>
				<h5>Atividades</h5>
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Configure as Atividades abaixo e, se necess�rio, selecione outras Atividades opcionais que far�o parte do Cronograma e clique em <strong>Salvar</strong>.
         			 Para exibir as rotas de cada Atividade, clique em uma delas na listagem abaixo.
   				</div>
				<div class="table-responsive">
   			 		 <table class="table table-bordered table-striped table-hover" id="cronogramaFaturamento" width="100%">
         	 			<thead class="thead-ggas-bootstrap">
         	 				<tr>
                 	 			<th class="text-center">
                     				<input type='checkbox' name='checkAllAuto' id='checkAllAuto' onchange="habilitaDesabilitaTodosCampos()"/>
                     			</th>
                    			<th scope="col" class="text-center">Sequ�ncia</th>
                    			<th scope="col" class="text-center">Atividade</th>
                    			<th scope="col" class="text-center">Predecessora</th>
                    			<th scope="col" class="text-center">Dura��o</th>
                    			<th scope="col" class="text-center">Data Limite</th>
                  			</tr>
         	 			</thead>
         	 			<tbody>
         	 				<c:forEach items="${listaCronogramaAtividadeFaturamentoVO}" var="cronogramaAtividadeFaturamentoVO">
         	 				<tr>
         	 					<td class="text-center">
         	 					 	 <input type="hidden" name="chavesPrimariasCronograma" value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}"/>
									<c:choose>
									<c:when test="${cronogramaAtividadeFaturamentoVO.atividadeSistema.obrigatoria == true && cronogramaAtividadeFaturamentoVO.chavePrimaria ne null}">
										<input type="checkbox" name="chavesPrimariasDisabled" value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}" disabled checked="checked">
										<input type="hidden" name="chavesPrimariasMarcadas" value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}"/>
									</c:when>
									<c:otherwise>
									<c:choose>
										<c:when test="${cronogramaAtividadeFaturamentoVO.indicadorMarcado ne null && cronogramaAtividadeFaturamentoVO.indicadorMarcado == true}">
											<input type="checkbox" name="chavesPrimariasMarcadas" value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}" checked="checked" onchange="habilitaDesabilitaCampo(this)"/>
										</c:when>
									<c:otherwise>
										<input type="checkbox" name="chavesPrimariasMarcadas" value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}" onchange="habilitaDesabilitaCampo(this)"/>
									</c:otherwise>
									</c:choose>
									</c:otherwise>
									</c:choose>
               					</td>
               					<td class="text-center">
               						<a href="javascript:detalharRotas(<c:out value='${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}'/>,'<c:out value='${cronogramaAtividadeFaturamentoVO.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
		            					<c:out value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.sequencia}"/>
		            				</a>
               					</td>
               					<td class="text-center">
               						<a href="javascript:detalharRotas(<c:out value='${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}'/>,'<c:out value='${cronogramaAtividadeFaturamentoVO.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
		            					<c:out value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.descricao}"/>
		            				</a>
               					</td>
               					<td class="text-center">
               						<a href="javascript:detalharRotas(<c:out value='${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}'/>,'<c:out value='${cronogramaAtividadeFaturamentoVO.atividadeSistema.descricao}'/>')"><span class="linkInvisivel"></span>
		            					<c:out value="${cronogramaAtividadeFaturamentoVO.atividadeSistema.atividadePrecedente.descricao}"/>
		            				</a>
               					</td>
           
               					<td>
               						<c:choose>
				     					<c:when test="${cronogramaAtividadeFaturamentoVO.duracao > 0}">
				       						<input class="form-control form-control-sm" type="text" name="duracoes" id="duracao${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}" 
				       							maxlength="2" size="2" onblur="validarDuracaoDias(this);" value="<c:out value="${cronogramaAtividadeFaturamentoVO.duracao}"/>" onkeypress="return formatarCampoInteiro(event)" readonly/>
				    				 	</c:when>
		
				     					<c:otherwise>
				       						<input class="form-control form-control-sm" type="text" name="duracoes" id="duracao${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}"
				       					 		maxlength="2" size="2" onblur="validarDuracaoDias(this);" value="" onkeypress="return formatarCampoInteiro(event)" readonly/>
				      					</c:otherwise>
		        					</c:choose>
               					</td>
               					<td>
               					 <div class="input-group input-group-sm" id="dataLimite">

                                    <input type="text" aria-label="Data Limite"
                                          class="form-control form-control-sm campoData"
                                          id="datasPrevista${cronogramaAtividadeFaturamentoVO.atividadeSistema.chavePrimaria}"
                                          name="datasPrevistas" size="10" readonly
                                          value="<fmt:formatDate value="${cronogramaAtividadeFaturamentoVO.dataPrevista}" pattern="dd/MM/yyyy"/>">
                                </div>

               					</td>
         	 				</tr>
         	 		  
         	 				</c:forEach>
         	 				
         	 			</tbody>
         	 		</table>
   			 	</div><!-- Lista -->
   			 	<hr/>
				<h5>Rotas</h5>
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-primary fade show" role="alert">
         					<i class="fa fa-question-circle"></i>
         					Para alterar a Data Prevista de alguma Rota desta atividade, informe a nova data e clique em <strong>Aplicar</strong>.
							A coluna Mais de 1 atividade por Leiturista exibir� o valor Sim se houver mais de uma rota alocada para o mesmo leiturista na mesma data.
 							Passe o mouse por cima para saber as rotas.
   			 			</div>
   			 			
   			 			<ul class="list-group">
  							<li class="list-group-item">Atividade: <span><c:out value='${cronogramaVO.descricaoAtividade}'/></span></li>
  							<li class="list-group-item">Per�odo: <span><c:out value='${cronogramaVO.dataInicio}'/> a <c:out value='${cronogramaVO.dataFim}'/></span></li>
 						</ul><br/>
   			 			
   			 			<div class="table-responsive">
   			 				 <table class="table table-bordered table-striped table-hover" id="cronogramaRota" width="100%">
   			 				 	<thead class="thead-ggas-bootstrap">
   			 				 		<tr>
   			 				 			<th>Rota</th>
   			 				 			<th>Data Prevista</th>
   			 				 			<th>Data do Ciclo Anterior</th>
   			 				 			<th>Dias de Leitura</th>
   			 				 			<th>Leiturista</th>
   			 				 			<th>Mais de 1 atividade por Leiturista</th>
   			 				 		</tr>
   			 				 	</thead>
   			 				 	<tbody>
   			 				 		<c:forEach items="${listaCronogramaAtividadeRotaVO}" var="cronogramaRota">
   			 				 		<tr>
   			 				 			<td>
   			 				 				<c:out value="${cronogramaRota.rota.numeroRota}"/>
		           							<input type="hidden" name="chavesPrimariasCronogramaRota" value="${cronogramaRota.rota.chavePrimaria}">
   			 				 			</td>
   			 				 			<td>
   			 				 				<div class="input-group input-group-sm" id="dataLimite">
   			 				 					<input type="text" aria-label="Data Limite"
                                            	class="form-control form-control-sm campoData"
                                            	id="dataPrevistaRota${cronogramaRota.rota.chavePrimaria}"
                                            	name="datasPrevistasRotas" size="10" readonly
                                           	 	value="${cronogramaRota.dataPrevista}" pattern="dd/MM/yyyy">
                                            </div>
   			 				 			</td>
   			 				 			<td>
   			 				 				<c:out value="${cronogramaRota.dataCiclo}"/>
   			 				 			</td>
   			 				 			<td>
   			 				 				<c:out value="${cronogramaRota.diasLeitura}"/>
   			 				 			</td>
   			 				 			<td>
   			 				 				<c:out value="${cronogramaRota.rota.leiturista.funcionario.nome}"/>
   			 				 			</td>
   			 				 			<td>
   			 				 				<c:if test="${cronogramaRota.emAlertaFormatado eq 'Sim'}" >
												<a href="#" title="O leiturista est� alocado para as rotas: <c:out value='${cronogramaRota.rotasEmAlertaFormatado}' />">	<c:out value='${cronogramaRota.emAlertaFormatado}' /> </a>
											</c:if>		
											<c:if test="${cronogramaRota.emAlertaFormatado ne 'Sim'}" >
												<c:out value='${cronogramaRota.emAlertaFormatado}' />
											</c:if>
   			 				 			</td>
   			 				 		</tr>
   			 				 		</c:forEach>
   			 				 	</tbody>
   			 				 </table>
   			 			</div>

					</div>
				</div>

		</div><!-- card-body -->
		<div class="card-footer">
        	 <div class="row">
      			<div class="col-sm-12">
      				  <button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="cancelar();">
                            <i class="fas fa-times"></i> Cancelar
                       </button>
                      
                       	<button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="limparAtividades();">
                           	<i class="far fa-trash-alt"></i> Limpar
                       	</button>
                       	
                       	<button class="btn btn-primary btn-sm float-left ml-1 mt-1" type="button" onclick="aplicarCronogramaRotasAlterar();">
                           	<i class="fas fa-check-circle"></i> Aplicar 
                       	</button>
                       
                       <vacess:vacess param="alterarCronograma">
                  	   <button id="buttonSalvar" class="btn btn-sm btn-success float-right ml-1 mt-1"  type="button" onclick="salvar();">
                       		<i class="fas fa-edit"></i> Salvar
                   		</button>
             		 </vacess:vacess>    
      			</div>
      		</div>
      	</div>
		
	</div><!-- fim do card -->
	
  </form:form>
</div> 
