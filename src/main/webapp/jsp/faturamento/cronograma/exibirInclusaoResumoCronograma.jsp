<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>

	$(document).ready(function(){

		<c:forEach items="${sessionScope.cronogramaAtividadeSistema.listaCronogramaAtividadeFaturamentoVO}" var="atividade" varStatus="index">
	    	$('#dataPrevista${atividade.atividadeSistema.chavePrimaria}').datepicker({dateFormat: 'dd/mm/yy', minDate: '${atividade.dataInicialAtividade}', maxDate: '${atividade.dataFinalAtividade}' , showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio'});
		</c:forEach>
		
		iniciarDatatable("#cronogramaRota");

	});

		function voltar() {
			location.href = '<c:url value="/exibirInclusaoCronograma"/>';
		}

		function cancelar() {
			location.href = '<c:url value="/exibirPesquisaCronograma"/>';
		}
	
		function salvar(){
			submeter('cronogramaForm', 'incluirCronograma');
		}

		function init () {
			onloadCronograma();
			carregarDadosGrupoFaturamento(document.getElementById("grupoFaturamento").value);
		}

		function carregarDadosGrupoFaturamento(idGrupoFaturamento){
			var descPeriodicidade = document.getElementById("descPeriodicidade");
			var referenciaAtual = document.getElementById("referenciaAtual");
			var qtdRotas = document.getElementById("qtdRotas");
			var qtdMedidores = document.getElementById("qtdMedidores");
			var mesAnoPartida = document.getElementById("mesAnoPartida");
			var campoCicloPartida = document.getElementById("cicloPartida");
			
	      	if (idGrupoFaturamento != "-1") {
		      	AjaxService.obterGrupoFaturamento( idGrupoFaturamento, null, null,null,
		           	function(cronogramaFaturamento) {	   
		      			descPeriodicidade.innerHTML = cronogramaFaturamento["descPeriodicidade"];
		      			qtdRotas.innerHTML = cronogramaFaturamento["qtdRotas"];
		      			referenciaAtual.innerHTML = cronogramaFaturamento["referenciaAtual"];
		      			qtdMedidores.innerHTML = cronogramaFaturamento["qtdMedidores"];
		      			mesAnoPartida.value = cronogramaFaturamento["mesAnoPartida"];
		      			campoCicloPartida.value = cronogramaFaturamento["qtdCiclos"];
		      			
		           	}
				);
			} else {			
				descPeriodicidade.innerHTML = "";
      			qtdRotas.innerHTML = "";
      			referenciaAtual.innerHTML ="";
      			qtdMedidores.innerHTML = "";
      			campoCicloPartida.value = "";
			}
		}
		
		function carregarDadosReferenciaCiclo(anoMesCiclo){
			document.forms[0].anoMesCiclo.value = anoMesCiclo;
			submeter('cronogramaForm', 'obterDadosMesAnoCiclo');
		}

		function aplicarAtividadeSistema(){
			submeter('cronogramaForm', 'aplicarAtividadeSistema');
		}
		
		function aplicarCronogramaRotas() {
			submeter('cronogramaForm', 'aplicarCronogramaRotas');
			$.scrollTo($('#conteinerAtividades'),800);
		}
		
		function detalharRotas(chave,descricao, dataInicialAtividade, dataFinalAtividade){
			document.forms[0].chaveAtividadeSistema.value = chave;
			document.forms[0].descricaoAtividade.value = descricao;
			submeter('cronogramaForm', 'exibirDetalhamentoRotas');
		}

		function limpar(){
			var listaChavesPrimariasMarcadas = document.getElementsByName('chavesPrimariasMarcadas');
			var listaDuracoes = document.getElementsByName('duracoes');
			var listaDatasPrevistas = document.getElementsByName('datasPrevistas');
			
			for (var i = 0; i < listaChavesPrimariasMarcadas.length; i++) {
	
					listaDuracoes[i].value = "";
					listaDatasPrevistas[i].value = "";
			}
			
		}
		
		function onloadCronograma() {
			<c:forEach items="${sessionScope.listaCronogramaAtividadeRotaVO}" var="cronograma" varStatus="index">
				$('#dataPrevistaRota${cronograma.rota.chavePrimaria}').datepicker({ minDate: '${cronograma.dataInicio}' , maxDate: '${cronograma.dataPrevista}' , showOn: 'button', 
					buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'
				});		
			</c:forEach>
		}

		function validarDuracaoDias(idAtividade, dataInicialAtividade){			
			var duracao = document.getElementById("duracao"+idAtividade).value;
			var dataPrevista = document.getElementById("dataPrevista"+idAtividade).value;
			
	      	if (duracao != undefined && duracao != "" && dataPrevista != undefined && dataPrevista != "") {		      	
		      	AjaxService.validarDuracaoDias(idAtividade, duracao, dataInicialAtividade, dataPrevista,  
		           	function(lancouExcecao) {	   

			      		if(lancouExcecao["lancouExcecao"] == true){
			      			
			      			alert(" A dura��o de dias est� ultrapassando o limite de intervalo de datas.");
		      			}
		      		
					}
				);
			} 
		}
		
		addLoadEvent(init);
		
	</script>
	



<div class="bootstrap">
	<form:form method="post" action="incluirCronograma" id="cronogramaForm" name="cronogramaForm">
		<input name="acao" type="hidden" id="acao" value="inserirCronograma">
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="grupoFaturamento" type="hidden" id="grupoFaturamento" value="${cronogramaVO.grupoFaturamento}">
		<input name="qtdAtividadesPorCronograma" type="hidden" id="qtdAtividadesPorCronograma" value="${cronogramaVO.qtdAtividadesPorCronograma}" >
		<input name="quantidadeCronogramas" type="hidden" id="quantidadeCronogramas" value="${cronogramaVO.quantidadeCronogramas}"/>
		<input name="anoMesCiclo" type="hidden" id="anoMesCiclo" value="${cronogramaVO.mesAnoCiclo}"/>
		<input name="chaveAtividadeSistema" type="hidden" id="chaveAtividadeSistema" value="${cronogramaVO.chaveAtividadeSistema}"/>
		<input name="descricaoAtividade" type="hidden" id="descricaoAtividade" value="${cronogramaVO.descricaoAtividade}"/>
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cronogramaVO.chavePrimaria}"/>
		<input name="mesAnoPartida" type="hidden" id="mesAnoPartida" value="${cronogramaVO.mesAnoPartida}"/>
		<input name="cicloPartida" type="hidden" id="cicloPartida" value="${cronogramaVO.cicloPartida}"/>
		<input name="dataInicialAtividade" type="hidden" id="dataInicialAtividade" value="${cronogramaVO.dataInicialAtividade}"/>
		<input name="dataFinalAtividade" type="hidden" id="dataFinalAtividade" value="${cronogramaVO.dataFinalAtividade}"/>
		<input name="chavesPrimariasAtividades" type="hidden" id="chavesPrimariasAtividades" value="${cronogramaVO.chavesPrimariasAtividades}"/>
	
		<div class="card">
		
			<div class="card-header">
				<h5 class="card-title mb-0">Incluir Cronograma - Resumo por Ciclo</h5>
			</div>
			<div class="card-body">
			
				<div class="alert alert-primary fade show" role="alert">
         			<i class="fa fa-question-circle"></i>
         			Para incluir um Cronograma de Faturamento preencha os dados e configure as Atividades do Cronograma na lista abaixo.
   			 	</div>
   			 	<div class="row">
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
   			 				<li class="list-group-item">
   			 					<p>Grupo de Faturamento
   			 					<span class="font-weight-bold"><c:out value="${grupoFaturamento.descricao}"/></span></p>
   			 				</li>	
   			 				<li class="list-group-item">
   			 					<p>M�s/Ano de Partida
   			 					<span id="mesAnoPartida" class="font-weight-bold"><c:out value="${cronogramaVO.mesAnoPartida}"/></span></p>
   			 				</li>
   			 				<li class="list-group-item">	
   			 					<p>Ciclo de Partida
   			 					<span id="cicloPartida" class="font-weight-bold"><c:out value="${cronogramaVO.cicloPartida}"/></span></p>
   			 				</li>	
   			 				<li class="list-group-item">
   			 					<p>Quantidade de Cronogramas
   			 					<span class="font-weight-bold"><c:out value="${cronogramaVO.quantidadeCronogramas}"/></span></p>
   			 				</li>
   			 			</ul>
   			 		
   			 		</div>
   			 		<div class="col-md-6">
   			 			<ul class="list-group">
  							<li class="list-group-item">
  								<p>Periodicidade: <span class="font-weight-bold" id="descPeriodicidade">
  								<c:out value='${cronogramaVO.descPeriodicidade}'/></span></p>
  							</li>
  							<li class="list-group-item">
  								<p>Rotas: <span class="font-weight-bold" id="qtdRotas">
  								<c:out value='${cronogramaVO.qtdRotas}'/></span></p>
  							</li>
  							<li class="list-group-item">
  								<p>Refer�ncia Atual: <span class="font-weight-bold" id="referenciaAtual">
  								<c:out value='${cronogramaVO.referenciaAtual}'/></span></p>
  							</li>
  							<li class="list-group-item">
  								<p>Cronogramas Abertos: <span class="font-weight-bold" id="cronogramasAbertos">
 								<c:out value='${cronogramaVO.cronogramasAbertos}'/></span></p>
  							</li>
						</ul>			 
   			 		</div>
   			 	</div><!-- Primeira Linha -->
   			 	
   			 	<hr />
   			 	
   			 	<div class="row">
   			 		<div class="col-md-12">
   			 		<div class="table-responsive">
   			 			 <table class="table table-bordered table-striped table-hover" id="cronograma" width="100%">
   			 			 	<thead class="thead-ggas-bootstrap">
   			 			 		<tr>
   			 			 			<th scope="col" class="text-center">M�s/Ano Refer�ncia</th>
                    				<th scope="col" class="text-center">Ciclo</th>
                    			</tr>
   			 			 	</thead>
   			 			 	<tbody>
   			 			 		<c:forEach items="${sessionScope.listaCronogramaAtividadeSistemaVO}" var="cronograma">
   			 			 		<tr>
   			 			 			<td class="text-center">
   			 			 				<a href="javascript:carregarDadosReferenciaCiclo(<c:out value='${cronograma.anoMes}'/><c:out value='${cronograma.ciclo}'/>)"><span class="linkInvisivel"></span>
											<c:out value="${cronograma.mesAnoFormatado}"/>
										</a>
   			 			 			</td>
   			 			 			<td class="text-center">
   			 			 				<a href="javascript:carregarDadosReferenciaCiclo(<c:out value='${cronograma.anoMes}'/><c:out value='${cronograma.ciclo}'/>)"><span class="linkInvisivel"></span>
											<c:out value="${cronograma.ciclo}"/>
										</a>
   			 			 			</td>
   			 			 		</tr>
   			 			 		</c:forEach>
   			 			 	</tbody>
   			 			 </table>
   			 		</div>
   			 		</div>
   			 	</div>
   			 	<hr />
   			 	<div class="row">
   			 	<div class="col-md-12">
   			 		<c:if test="${sessionScope.cronogramaAtividadeSistema ne null && not empty sessionScope.cronogramaAtividadeSistema}">
   			 		<h5>M�s/Ano - Ciclo: <c:out value='${sessionScope.cronogramaAtividadeSistema.mesAnoCicloFormatado}'/></h5>
   			 		<div class="table-responsive">
   			 			<table class="table table-bordered table-striped table-hover" id="cronograma" width="100%">
   			 				<thead class="thead-ggas-bootstrap">
   			 			 		<tr>
   			 			 			<th class="text-center">
                     					<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>
                     				</th>
   			 			 			<th scope="col" class="text-center">Sequ�ncia</th>
                    				<th scope="col" class="text-center">Descri��o</th>
                    				<th scope="col" class="text-center">Preced�ncia</th>
                    				<th scope="col" class="text-center">Dura��o (Dias)</th>
                    				<th scope="col" class="text-center">Data Prevista</th>
                    			</tr>
   			 			 	</thead>
   			 			 	<tbody>
   			 			 		<c:forEach items="${sessionScope.cronogramaAtividadeSistema.listaCronogramaAtividadeFaturamentoVO}" var="atividadeVO">
   			 			 		<tr>
   			 			 			<td class="text-center">
   			 			 				<input type="hidden" name="chavesPrimariasCronograma" value="${atividadeVO.atividadeSistema.chavePrimaria}"/>
   			 			 				<c:choose>
											<c:when test="${atividadeVO.atividadeSistema.obrigatoria == true}">
												<input type="checkbox" name="chavesPrimariasDisabled" value="${atividadeVO.atividadeSistema.chavePrimaria}" disabled checked="checked">
												<input type="hidden" name="chavesPrimariasMarcadas" value="${atividadeVO.atividadeSistema.chavePrimaria}"/>
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="chavesPrimariasMarcadas" value="${atividadeVO.atividadeSistema.chavePrimaria}" checked="checked" onchange="limparCheck(this)"/>
											</c:otherwise>
										</c:choose>
   			 			 			</td>
   			 			 			<td class="text-center">
   			 			 				<a href="javascript:detalharRotas(<c:out value='${atividadeVO.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividadeVO.atividadeSistema.descricao}'/>','<c:out value='${atividadeVO.dataInicialAtividade}'/>','<c:out value='${atividadeVO.dataFinalAtividade}'/>');"><span class="linkInvisivel"></span>
		            						<c:out value="${atividadeVO.atividadeSistema.sequencia}"/>
		            					</a>
   			 			 			</td>
   			 			 			<td class="text-center">
   			 			 				<a href="javascript:detalharRotas(<c:out value='${atividadeVO.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividadeVO.atividadeSistema.descricao}'/>','<c:out value='${atividadeVO.dataInicialAtividade}'/>','<c:out value='${atividadeVO.dataFinalAtividade}'/>');"><span class="linkInvisivel"></span>
		            						<c:out value="${atividadeVO.atividadeSistema.descricao}"/>
		            					</a>
   			 			 			</td>
   			 			 			<td class="text-center">
   			 			 				<a href="javascript:detalharRotas(<c:out value='${atividadeVO.atividadeSistema.chavePrimaria}'/>,'<c:out value='${atividadeVO.atividadeSistema.descricao}'/>','<c:out value='${atividadeVO.dataInicialAtividade}'/>','<c:out value='${atividadeVO.dataFinalAtividade}'/>');"><span class="linkInvisivel"></span>
		            						<c:out value="${atividadeVO.atividadeSistema.atividadePrecedente.descricao}"/>
		           						</a>
   			 			 			</td>
   			 			 			<td class="text-center">
   			 			 			
   			 			 				<c:choose>
		        							<c:when test="${atividadeVO.duracao > 0}">
		       									<input  class="form-control form-control-sm" type="text" name="duracoes" id="duracao${atividadeVO.atividadeSistema.chavePrimaria}" maxlength="2" size="2" value="${atividadeVO.duracao}" onblur="validarDuracaoDias(${atividadeVO.atividadeSistema.chavePrimaria}, '<c:out value='${atividadeVO.dataInicialAtividade}'/>');" onkeypress="return formatarCampoInteiro(event)"/>
		        							</c:when>
		
		       								<c:otherwise>
		       									<input   class="form-control form-control-sm" type="text" name="duracoes" id="duracao${atividadeVO.atividadeSistema.chavePrimaria}" maxlength="2" size="2" value="" onblur="validarDuracaoDias(${atividadeVO.atividadeSistema.chavePrimaria}, '<c:out value='${atividadeVO.dataInicialAtividade}'/>');" onkeypress="return formatarCampoInteiro(event)"/>
		      								</c:otherwise>
		        						</c:choose>
   			 			 			</td>
   			 			 			<td class="text-center">
   			 			 				<div class="input-group input-group-sm" id="dataLimite">
   			 			 					<input type="text" id="dataPrevista${atividadeVO.atividadeSistema.chavePrimaria}" 
   			 			 					class="form-control form-control-sm campoData"
   			 			 					name="datasPrevistas" maxlength="10" value="<c:out value="${atividadeVO.dataPrevistaFormatada}"/>" size="10" readonly/>
   			 			 				</div>
   			 			 			</td>
   			 			 		</tr>
   			 			 		</c:forEach>
   			 			 	</tbody>
   			 			</table>
   			 		</div>
   			 		 <button class="btn btn-success btn-sm float-left ml-1 mt-1"  id="btnCadastrar" name="btnCadastrar" type="button" onclick="aplicarAtividadeSistema();">
                           <i class="fas fa-check"></i> Aplicar
                     </button>
   			 		
   			 		</c:if>
   			 		</div>
   			 	</div>
   			 	<c:if test="${sessionScope.listaCronogramaAtividadeRotaVO ne null && not empty sessionScope.listaCronogramaAtividadeRotaVO}">
				<hr />
   			 		<div class="row">
   			 			<div class="col-md-12">
   			 			<h5>Rotas</h5>
   			 			<div class="alert alert-primary fade show" role="alert">
         					<i class="fa fa-question-circle"></i>
         					Para alterar a Data Prevista de alguma Rota desta atividade, informe a nova data e clique em Aplicar.
							A coluna Mais de 1 atividade por Leiturista exibir� o valor Sim se houver mais de uma rota alocada para o mesmo leiturista na mesma data.
							Passe o mouse por cima para saber as rotas.
   			 			</div>
   			 			
   			 			<ul class="list-group">
  							<li class="list-group-item">Atividade: <span><c:out value='${cronogramaVO.descricaoAtividade}'/></span></li>
  							<li class="list-group-item">Dura��o: <span><c:out value='${cronogramaVO.dataInicialAtividade}'/> a <c:out value='${cronogramaVO.dataFinalAtividade}'/></span></li>
 						</ul><br/>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="cronogramaRota" width="100%">
   			 					<thead class="thead-ggas-bootstrap">
   			 						<tr>
   			 							<th class="text-center">Rota</th>
   			 							<th class="text-center">Data Limite</th>
   			 							<th class="text-center">Data de Ciclo</th>
   			 							<th class="text-center">Dias de Leitura</th>
   			 							<th class="text-center">Leiturista</th>
   			 							<th class="text-center">Mais de 1 atividade por Leiturista</th>
   			 						</tr>
   			 					</thead>
   			 					<tbody>
   			 						<c:forEach items="${sessionScope.listaCronogramaAtividadeRotaVO}" var="cronogramaRota">
   			 						<tr>
   			 							<td class="text-center">
   			 								<c:out value="${cronogramaRota.rota.numeroRota}"/>
		           							<input type="hidden" name="chavesPrimariasCronogramaRota" value="${cronogramaRota.rota.chavePrimaria}">
   			 							</td>
   			 							<td class="text-center">
   			 								<input type="text" id="dataPrevistaRota${cronogramaRota.rota.chavePrimaria}" name="datasPrevistasRotas" maxlength="10" value="${cronogramaRota.dataPrevista}" size="10" readonly="readonly">
   			 							</td>
   			 							<td class="text-center">
   			 								<c:out value="${cronogramaRota.dataCiclo}"/>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:out value="${cronogramaRota.diasLeitura}"/>
   			 							</td>
   			 							<td>
   			 								<c:out value="${cronogramaRota.rota.leiturista.funcionario.nome}"/>
   			 							</td>
   			 							<td class="text-center">
   			 								<c:if test="${cronogramaRota.emAlertaFormatado eq 'Sim'}" >
												<a href="#" title="O leiturista est� alocado para as rotas: <c:out value='${cronogramaRota.rotasEmAlertaFormatado}' />">	<c:out value='${cronogramaRota.emAlertaFormatado}' /> </a>
											</c:if>		
											<c:if test="${cronogramaRota.emAlertaFormatado ne 'Sim'}" >
												<c:out value='${cronogramaRota.emAlertaFormatado}' />
											</c:if>
   			 							</td>
   			 						</tr>
   			 						</c:forEach>
   			 					</tbody>
   			 				</table>	
   			 			</div>
   			 			<button class="btn btn-success btn-sm float-left ml-1 mt-1 botaoAplicar" type="button" onclick="aplicarCronogramaRotas();">
                           <i class="fas fa-check"></i> Aplicar
                    	 </button>
   			 			</div>
   			 		</div>
				</c:if>
				
				
				
			</div><!--Card-body  -->
			<div class="card-footer">
				<div class="row">
      				<div class="col-sm-12">
                        <button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="cancelar();">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                        <button class="btn btn-primary btn-sm float-left ml-1 mt-1" type="button" onclick="voltar();">
                            <i class="fas fa-undo"></i> Voltar
                        </button>
                        <button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="limpar();">
                            <i class="far fa-trash-alt"></i></i> Limpar
                        </button>
                        <vacess:vacess param="incluirCronograma">
                  		<button id="buttonGerar" value="Salvar" class="btn btn-sm btn-success float-right ml-1 mt-1"  type="button" onclick="salvar();">
                       		<i class="fas fa-save"></i> Salvar
                   		</button>
             		 	</vacess:vacess>      				
      				</div>
      			</div>
			</div>
		
		</div>
	
	</form:form>
</div>
