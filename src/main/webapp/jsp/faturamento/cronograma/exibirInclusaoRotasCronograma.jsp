<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/dhtmlxcombo.css">


<h1 class="tituloInterno">Incluir Cronograma - Rotas por Atividade</h1>
<p class="orientacaoInicial">Para incluir um Cronograma de Faturamento preencha os dados e configure as Atividades do Cronograma na lista abaixo.</p>


<form method="post" action="incluirRotasCronograma" id="cronogramaForm" name="cronogramaForm" >

	<script>

		$(document).ready(function(){
			
		    $(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		    
		});
		
		function limpar(){
			document.cronogramaForm.grupoFaturamento.value = "-1";
		}

		function voltar() {
			location.href = '<c:url value="/exibirInclusaoCronograma"/>';
		}
	
		function aplicar(){
			location.href = '<c:url value="/exibirInclusaoCronograma"/>';
		}

		function init () {
			
		}
		
		addLoadEvent(init);

		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="inserirCronograma">
	<input name="postBack" type="hidden" id="postBack" value="true">
	
	<fieldset id="conteinerCronogramaRotas" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna detalhamentoColunaLarga">
			<label class="rotulo">Grupo de Faturamento:</label>
			<span class="itemDetalhamento"><c:out value="${rubricaForm.map.grupoFaturamento.descricao}"/></span><br />
			
			<label class="rotulo">M�s/Ano de Partida:</label>
			<span class="itemDetalhamento"><c:out value="${rubricaForm.map.atividade.descricao}"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo">M�s/Ano - Ciclo:</label>
			<span class="itemDetalhamento"><c:out value="${rubricaForm.map.mesAnoCiclo}"/></span><br />
			
			<label class="rotulo">Data Prevista:</label>
			<span class="itemDetalhamento"><c:out value="${rubricaForm.map.dataPrevistaInicial}"/></span>
			<label class="rotuloEntreCampos" for="dataParcelamentoFinal">a</label>
			<span class="itemDetalhamento"><c:out value="${rubricaForm.map.dataPrevistaFinal}"/></span>
		</fieldset>
	</fieldset>
		
	<hr class="linhaSeparadora2" />
		<p class="orientacaoInicial">Configure as Atividades abaixo e, se necess�rio, selecione outras Atividades opcionais que far�o parte do Cronograma e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.<br />
		Para exibir as rotas de cada Atividade, clique em uma delas na listagem abaixo.</p>
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="rotas" sort="list" id="rota" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarCronograma">
	        <display:column sortable="true" sortProperty="rota" title="Rota"  style="width: 50px">
            	<c:out value="${cronograma.rota.sequencia}"/>
	        </display:column>
	        
	         <display:column sortable="true" sortProperty="dataPrevista" title="Data Prevista" style="width: 90px">
	        	<input class="campoData" type="text" id="dataPrevista" name="dataPrevista" maxlength="10" value="${cronogramaForm.map.rota.dataPrevista}">
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="dataCicloAnterior" title="Data Ciclo<br />Anterior" style="width: 75px">
            	<c:out value="${cronograma.rota.dataCicloAnterior}"/>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="diasLeitura" title="Dias de<br />Leitura" style="width: 50px">
	            <c:out value="${cronograma.rota.diasLeitura}"/>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="qtdLeitura" title="Qtd.<br />Leitura" style="width: 50px">
	        	<c:out value="${cronograma.rota.qtdLeitura}"/>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="leiturista" title="Leiturista">
        		<c:out value="${cronograma.rota.leiturista}"/>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="rotasConflitantes" title="+ 1 Atividade<br />/Leiturista">
	        	<a href="javascript:detalharAtividade(<c:out value='${cronograma.rota.atividadesLeiturista}'/>);"><span class="linkInvisivel"></span>
	        		<c:out value="${cronograma.rota.atividadesLeiturista}"/>
	        	</a>
	        </display:column>
	    </display:table>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar();">
	    <vacess:vacess param="incluirRotasCronograma">
	    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Aplicar"  type="button" onclick="aplicar();">
	    </vacess:vacess>
	</fieldset>
</form>