<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page import="br.com.ggas.faturamento.cronograma.impl.CronogramaFaturamentoImpl" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<jsp:useBean id="constante" class="br.com.ggas.faturamento.cronograma.impl.CronogramaFaturamentoImpl">

</jsp:useBean>


<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>
		$(document).ready(function(){

			//Define o mascaramento para os campos de 
			$("#mesAnoFaturamento").inputmask("99/9999",{placeholder:"_"});
					
		});
		
		function salvarEscalonamento(){
			submeter('cronogramaForm', 'salvarEscalonamento');
			
		}
		
		function escalonarAutomaticamente(){
			submeter('cronogramaForm','escalonarLeituristaAutomaticamente');
		}
		
		
		function exibirHistoricoRota( chaveRota, chaveCronograma ) {

			if(chaveRota != ""){	      		
		       	AjaxService.obterDadosHistoricoRotaCronograma( chaveRota, chaveCronograma, { 
		           	callback: function(dadosHistorico) {
		           		var inner = '';
		           		var param = '';
		           		var div = 2;
		           		var start = 2;

		           		$('#corpoDadosHistorico').html('');
		           		for(var key=0; key<dadosHistorico.length; key++) {
			           	
			               	if(start % div == 0){
								param = "odd";
				            }else{
								param = "even";
					        }
		               		inner = inner + '<tr class='+param+'><td>'+dadosHistorico[key][0]+'</td>';
		               		inner = inner + '<td>'+dadosHistorico[key][1]+' </td>';
		               		inner = inner + '<td>'+dadosHistorico[key][2]+'</td></tr>';
		               		start = start + 1;					
		            	}
		               	$("#corpoDadosHistorico").prepend(inner);
		        	}, async: false }
		        );
		    }
			$('#historico').modal('show');
			
		}
		
		function cancelar() {
			location.href = '<c:url value="/exibirPesquisaCronograma"/>';
		}
				
		
	</script>
<div class="bootstrap">
	<form:form method="post" action="pesquisarCronograma"  id="cronogramaForm">
		<input type="hidden" id="escalonamentoRealizado" name="escalonamentoRealizado" value="${ cronogramaVO.escalonamentoRealizado }">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${cronogramaVO.chavePrimaria}"/>
		
		<div class="modal fade" id="historico" tabindex="-1" role="dialog">
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
     			 <div class="modal-header">
       			 <h5 class="modal-title">Hist�rico</h5>
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        			  <span aria-hidden="true">&times;</span>
        			</button>
    			 </div>
      			<div class="modal-body">
      				<div class="table-responsive">
        			 	<table class="table table-bordered table-striped table-hover">
        			 		<thead>
								<tr>
									<th>Rota</th>
									<th>Leiturista</th>
									<th>Leiturista Suplente</th>
								</tr>
							</thead>
							<tbody id="corpoDadosHistorico"></tbody>
        				</table>
        			</div>
      			</div>
      			<div class="modal-footer">
        			<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      			</div>
    		</div>
  		</div>
	</div>
	
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Escalonar Leituristas</h5>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<p>Grupo de Faturamento: <span class="font-weight-bold">
  						<c:out value='${descricaoGrupoFatu}'/></span></p>
  						
  						<p>Ano/M�s-Ciclo: <span class="font-weight-bold">
  						<c:out value='${anoMesCiclo}'/></span></p>
					</div>
					<div class="col-md-6">
						<ul class="list-group">
							<li class="list-group-item">
								<p>Periodicidade: <span class="font-weight-bold">
  								<c:out value='${descricaoPeriodicidade}'/></span></p>
							</li>
							<li class="list-group-item">
								<p>Quantidade de Rotas: <span class="font-weight-bold">
  								<c:out value='${qtdRotas}'/></span></p>
							</li>
							<li class="list-group-item">
								<p>Quantidade de Medidores: <span class="font-weight-bold">
  								<c:out value='${qtdMedidores}'/></span></p>
							</li>
						</ul>
					</div>
				</div><!-- fim da row -->
				<hr />
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
   			 		 		<table class="table table-bordered table-striped table-hover" id="cronogramaFaturamentoVO" width="100%">
   			 		 			<thead class="thead-ggas-bootstrap">
   			 		 				<tr>
   			 		 					<th>Rota</th>
   			 		 					<th>Data Prevista</th>
   			 		 					<th>Data da Realiza��o</th>
   			 		 					<th>Leiturista Anterior</th>
   			 		 					<th>Leiturista</th>
   			 		 					<th>Leiturista Suplente</th>
   			 		 					<th>Hist�rico</th>
   			 		 				</tr>
   			 		 			</thead>
   			 		 			<tbody>
   			 		 				<c:forEach items="${listaCronogramaFaturamentoVO}" var="cronogramaFaturamentoVO">
   			 		 				<tr>
   			 		 					<td><c:out value="${ cronogramaFaturamentoVO.rota.numeroRota }"/></td>
   			 		 					<td>
   			 		 						<c:if test="${ cronogramaFaturamentoVO.dataPrevista ne null }">
	            								<fmt:formatDate value="${cronogramaFaturamentoVO.dataPrevista  }" pattern="dd/MM/yyyy"/>
											</c:if>
   			 		 					</td>
   			 		 					<td>
   			 		 						<c:if test="${ cronogramaFaturamentoVO.dataRealizacao ne null }">
	            								<fmt:formatDate value="${cronogramaFaturamentoVO.dataRealizacao  }" pattern="dd/MM/yyyy"/>
											</c:if>
   			 		 					</td>
   			 		 					<td><c:out value="${cronogramaFaturamentoVO.leituristaAnterior.funcionario.nome}"/></td>
   			 		 					<td>
   			 		 						<select name="leituristaAtual" id="leituristaAtual" class="form-control form-control-sm" >
											<option value="-1">Selecione</option>
											<c:forEach items="${ listaLeiturista }" var="leiturista">
												<option value="<c:out value="${ leiturista.chavePrimaria }"/>" <c:if test="${ cronogramaFaturamentoVO.leituristaAtual.chavePrimaria == leiturista.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${ leiturista.funcionario.nome }"/>
												</option>		
											</c:forEach>
											</select>
				
   			 		 					</td>
   			 		 					<td>
   			 		 						<select name="leituristaSuplente" id="leituristaSuplente" class="form-control form-control-sm" >
											<option value="-1">Selecione</option>
											<c:forEach items="${ listaLeiturista }" var="leituristaSuplente">
												<option value="<c:out value="${ leituristaSuplente.chavePrimaria }"/>" <c:if test="${ cronogramaFaturamentoVO.leituristaSuplente.chavePrimaria == leituristaSuplente.chavePrimaria}">selected="selected"</c:if>>
												<c:out value="${leituristaSuplente.funcionario.nome}"/>
												</option>		
											</c:forEach>
											</select>
   			 		 					</td>
   			 		 					<td>
   			 		 						<a href="javascript:exibirHistoricoRota(<c:out value='${ cronogramaFaturamentoVO.rota.chavePrimaria }'/>, <c:out value='${ cronogramaFaturamentoVO.chavePrimaria }'/>);">
   			 		 						<i class="fas fa-list-ul"></i></a>
   			 		 					</td>
   			 		 				</tr>
   			 		 				</c:forEach>
   			 		 			</tbody>
   			 		 		</table>
   			 		 	</div>
					</div>
				</div>
			</div><!-- card-body -->
			<div class="card-footer">
				<div class="row">
      				<div class="col-sm-12">
      				<c:if test="${listaCronogramaFaturamentoVO ne null}">
      					<button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="cancelar();">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                        <button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="escalonarAutomaticamente()">
                            <i class="fas fa-address-card"></i> Escalonar Automaticamente
                       	</button>
                       	<vacess:vacess param="exibirInclusaoCronograma">
                       	<button id="botaoIncluir" class="btn btn-sm btn-success float-right ml-1 mt-1"  type="button" onclick="salvarEscalonamento()">
                       		<i class="fas fa-save"></i> Salvar
                   		</button>
                       	</vacess:vacess>
      				</c:if>
      				</div>
      			</div>
			</div>	
		</div><!-- fim do card -->
	</form:form>
</div>

