<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Alterar Grupo Faturamento<a href="<help:help>'/cadastrodasentidadescontedoinclusoalterao.htm'</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script language="javascript">

    $(document).ready(function(){

         var max = 0;

          $('.rotulo').each(function(){
                  if ($(this).width() > max)
                     max = $(this).width();   
              });
          $('.rotulo').width(max);
         
          $("input[name='indicadorContiCascataTarifa']").on("click", function(){
        	  verificarExibicaoVencFaturaIgual();        	  
          });
          
          init();
    });
    
    function verificarExibicaoVencFaturaIgual(){
    	if($("input[name='indicadorContiCascataTarifa']:checked").val() == 'false'){
       		$("#exibirVencFaturaIgual").prop("hidden", true);
   	  	}else{
   			$("#exibirVencFaturaIgual").prop("hidden", false);
   	  	}	
    }
    
    function init(){
    	 verificarRotaPorGrupoFaturamento(document.getElementById('chavePrimaria').value);
    	 verificarExibicaoVencFaturaIgual();
    }
    
    function verificarRotaPorGrupoFaturamento(chavePrimaria){
        AjaxService.listarRotaPorGrupoFaturamento(chavePrimaria,
                function(rotas) {  
                    for (key in rotas){
                        
                        document.getElementById('tipoLeitura').className = 'campoDesabilitado';
                        document.getElementById('tipoLeitura').disabled = true;
                        
                        document.getElementById('periodicidade').className = 'campoDesabilitado';
                        document.getElementById('periodicidade').disabled = true;
                    }
            }
         ); 
            
        }
    
    function limpar(){
 	   document.getElementById('descricao').value = "";
 	   document.getElementById('descricaoAbreviada').value = "";
       document.getElementById('diaVencimento').value = "";
       document.getElementById('numeroDeCiclo').value = "";
       document.getElementById('periodicidade').value = "-1";
       document.getElementById('tipoLeitura').value = "-1";
    }
    
    function salvar(){
    	 document.getElementById('periodicidade').disabled = false; 
         document.getElementById('tipoLeitura').disabled = false; 
        submeter('grupoFaturamentoForm', 'alterarGrupoFaturamento');

    }

    function cancelar(){    
    	location.href='exibirPesquisaGrupoFaturamento';
    }
    
</script>

<form:form method="post" id="grupoFaturamentoForm" name="grupoFaturamentoForm" enctype="multipart/form-data" action="alterarGrupoFaturamento">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${grupoFaturamento.chavePrimaria}">
<input name="referenciaAnoMes" type="hidden" id="referenciaAnoMes"value="${anoMesReferencia}">
<fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
        
        	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
	        <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${grupoFaturamento.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
	        <br />
	      	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o Abreviada:</label>
        	<input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="5" size="5" value="${grupoFaturamento.descricaoAbreviada}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        	<br />
	        <br />
            <label class="rotulo campoObrigatorio" for="anoMesReferencia"><span class="campoObrigatorioSimbolo">* </span>M�s/Ano:</label>
            <input class="campoDesabilitado" type="text" name="anoMesReferencia" id="anoMesReferencia" readonly="readonly" maxlength="6" size="6" value="${grupoFaturamento.anoMesReferencia}" onkeypress="return formatarCampoInteiro(event,6);" />
            <br /> 
           
            <label class="rotulo campoObrigatorio" id="numeroCiclo"><span class="campoObrigatorioSimbolo">* </span>N�mero do ciclo de faturamento atual:</label>
            <input class="campoTexto campoHorizontal" type="text" name="numeroCiclo" id="numeroDeCiclo" maxlength="3" size="3" value="${grupoFaturamento.numeroCiclo}" onkeypress="return formatarCampoInteiro(event)"/>
            <br />
        </fieldset>
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal colunaFinalAjuste">
       		 <label class="rotulo">Dia padr�o para vencimento das faturas:</label>
            <input class="campoTexto campoHorizontal" type="text" name="diaVencimento" id="diaVencimento" maxlength="2" size="2" value="${diaVencimento}" onkeypress="return formatarCampoInteiro(event)"/>
            <br />
            
            <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Periodicidade:</label>
            <select name="periodicidade" class="campoSelect" id="periodicidade" >
                <option value="-1">Selecione</option>
                <c:forEach items="${listaPeriodicidades}" var="periodicidade">
                    <option value="<c:out value="${periodicidade.chavePrimaria}"/>" <c:if test="${grupoFaturamento.periodicidade.chavePrimaria == periodicidade.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${periodicidade.descricao}"/>
                    </option>       
                </c:forEach>    
            </select><br />
            
            <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Meio de Leitura:</label>
            <select name="tipoLeitura" class="campoSelect" id="tipoLeitura" >
                <option value="-1">Selecione</option>
                <c:forEach items="${listaTiposLeitura}" var="tipoLeitura">
                    <option value="<c:out value="${tipoLeitura.chavePrimaria}"/>" <c:if test="${grupoFaturamento.tipoLeitura.chavePrimaria == tipoLeitura.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${tipoLeitura.descricao}"/>
                    </option>       
                </c:forEach>    
            </select><br />
            
            <label class="rotulo" >Ind. data venc. a partir data final do ciclo fat.:</label>
            <input class="campoRadio" type="radio" value="true" name="indicadorDtVencimentoUltimoDiaCiclo" id="indicadorDtVencimentoUltimoDiaCiclo" <c:if test="${grupoFaturamento.indicadorDtVencimentoUltimoDiaCiclo eq 'true'}">checked</c:if>><label class="rotuloRadio">Sim</label>
            <input class="campoRadio" type="radio" value="false" name="indicadorDtVencimentoUltimoDiaCiclo" id="indicadorDtVencimentoUltimoDiaCiclo" <c:if test="${grupoFaturamento.indicadorDtVencimentoUltimoDiaCiclo ne 'true'}">checked</c:if>><label class="rotuloRadio">N�o</label>                        
            <br/>
        <label class="rotulo" for="habilitado">Indicador de Uso:</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${grupoFaturamento.habilitado eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Ativo</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${grupoFaturamento.habilitado eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Inativo</label><br />
        <c:if test="${isControlarCiclos}">
        	<label class="rotulo" >Continuidade da cascata tarif�ria no <br/>faturamento de fechamento de m�s?.:</label>                        
            <input class="campoRadio" type="radio" value="true" name="indicadorContiCascataTarifa" id="indicadorContiCascataTarifSim" <c:if test="${grupoFaturamento.indicadorContinuidadeCascataTarifa eq 'true'}">checked</c:if>><label class="rotuloRadio">Sim</label>
            <input class="campoRadio" type="radio" value="false" name="indicadorContiCascataTarifa" id="indicadorContiCascataTarifNao" <c:if test="${grupoFaturamento.indicadorContinuidadeCascataTarifa ne 'true'}">checked</c:if>><label class="rotuloRadio">N�o</label>                        
            <br/>
            <div hidden id="exibirVencFaturaIgual">
	            <label class="rotulo" >Vencimento iguais das faturas?.:</label>                        
	            <input class="campoRadio" type="radio" value="true" name="indicadorVencimIguaisFatura" id="indicadorVencimIguaisFaturSim" <c:if test="${grupoFaturamento.indicadorVencimentoIgualFatura eq 'true'}">checked</c:if>><label class="rotuloRadio">Sim</label>
	            <input class="campoRadio" type="radio" value="false" name="indicadorVencimIguaisFatura" id="indicadorVencimIguaisFaturNao" <c:if test="${grupoFaturamento.indicadorVencimentoIgualFatura ne 'true'}">checked</c:if>><label class="rotuloRadio">N�o</label>                        
            </div>
            <br/>
        </c:if>
        </fieldset>
        <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
</fieldset>
        
	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limpar();">
	    <input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" id="botaoSalvar" onclick="javascript:salvar();">
	</fieldset>
	<token:token></token:token>
</form:form>
