<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Detalhar Grupo de Faturamento<a href="<help:help>'/entidadecontedodetalhamento.htm'</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>
		$(document).ready(function(){
			  var max = 0;
		
		    $('.rotulo').each(function(){
		            if ($(this).width() > max)
		               max = $(this).width();   
		        });
		    $('.rotulo').width(max);
			
		});

		function voltar(){	
			
			submeter('grupoFaturamentoForm', 'exibirPesquisaGrupoFaturamento');
		}
		
		function alterar(){
			
			submeter('grupoFaturamentoForm', 'exibirAlteracaoGrupoFaturamento');
		}

</script>

<form:form method="post" id="grupoFaturamentoForm" name="grupoFaturamentoForm">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${grupoFaturamento.chavePrimaria}">
	<fieldset class="conteinerPesquisarIncluirAlterar">
			<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
					<label class="rotulo">Descri��o:</label>
					<span class="itemDetalhamento"><c:out value="${grupoFaturamento.descricao}"/></span>
					<br /><br />
							
					<label class="rotulo">Descri��o Abreviada:</label>
					<span class="itemDetalhamento"><c:out value="${grupoFaturamento.descricaoAbreviada}"/></span>
					<br /><br />
					
					 <label class="rotulo" for="anoMesReferencia">M�s/Ano:</label>
					<span class="itemDetalhamento"><c:out value="${anoMesReferencia}"/></span>
				    <br /> 
				   
					<label class="rotulo" id="numeroCiclo">N�mero do ciclo de faturamento atual:</label>
					<span class="itemDetalhamento"><c:out value="${grupoFaturamento.numeroCiclo}"/></span>
					<br />
			</fieldset>
		
			<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
					<label class="rotulo" id="diaVencimento">Dia padr�o para vencimento das faturas:</label>
					<span class="itemDetalhamento"><c:out value="${grupoFaturamento.diaVencimento}"/></span>
					<br />
					
					<label class="rotulo">Periodicidade:</label>
					<span class="itemDetalhamento"><c:out value="${grupoFaturamento.periodicidade.descricao}"/></span>
					
					<label class="rotulo">Meio de Leitura:</label>
					<span class="itemDetalhamento"><c:out value="${grupoFaturamento.tipoLeitura.descricao}"/></span>
					
					<label class="rotulo" id="indicadorDtVencimentoUltimoDiaCiclo">Ind. data venc. a partir data final do ciclo fat.:</label>
					<span class="itemDetalhamento">
						<c:if test="${grupoFaturamento.indicadorDtVencimentoUltimoDiaCiclo eq 'true'}">Sim</c:if>
				   		<c:if test="${grupoFaturamento.indicadorDtVencimentoUltimoDiaCiclo eq 'false'}">N�o</c:if>
					</span>
					<span class="itemDetalhamento"><c:out value="${grupoFaturamento.consideraMesCivil}"/></span>
					
					<c:if test="${isControlarCiclos}">
			        	<label class="rotulo" >Continuidade da cascata tarif�ria no <br/>faturamento de fechamento de m�s?.:</label>
			        	<span class="itemDetalhamento">
			        		<c:if test="${grupoFaturamento.indicadorContinuidadeCascataTarifa eq 'true'}">Sim</c:if>
				        	<c:if test="${grupoFaturamento.indicadorContinuidadeCascataTarifa eq 'false'}">N�o</c:if>
			        	</span>
			        	<br/>
						<label class="rotulo" >Vencimento iguais das faturas?.:</label>
						<span class="itemDetalhamento">
							<c:if test="${grupoFaturamento.indicadorVencimentoIgualFatura eq 'true'}">Sim</c:if>
							<c:if test="${grupoFaturamento.indicadorVencimentoIgualFatura eq 'false'}">N�o</c:if>
						</span>
					</c:if>
            					
			</fieldset>
	</fieldset>

	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    
    <vacess:vacess param="exibirAtualizarGrupoFaturamento">    
    	<input name="buttonAlterar" class="bottonRightCol2 botaoGrande1 botaoAlterar" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
   
</fieldset>
</form:form>
