<!--
Copyright (C) <2011> GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

Este arquivo ? parte do GGAS, um sistema de gest?o comercial de Servi?os de Distribui??o de G?s

Este programa ? um software livre; voc? pode redistribu?-lo e/ou
modific?-lo sob os termos de Licen?a P?blica Geral GNU, conforme
publicada pela Free Software Foundation; vers?o 2 da Licen?a.

O GGAS ? distribu?do na expectativa de ser ?til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl?cita de
COMERCIALIZA??O ou de ADEQUA??O A QUALQUER PROP?SITO EM PARTICULAR.
Consulte a Licen?a P?blica Geral GNU para obter mais detalhes.

Voc? deve ter recebido uma c?pia da Licen?a P?blica Geral GNU
junto com este programa; se n?o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaExcecaoLeituraFaturamento"/>';
	}
</script>
<div class="bootstrap">
	<form:form method="post">
		<input name="acao" type="hidden" id="acao" value="incluirFatura">
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="idCliente" type="hidden" id="idCliente" value="${faturaForm.map.idCliente}">
		<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.map.idPontoConsumo}">

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Detalhar Anormalidade de Faturamento</h5>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body bg-light">
								<h5>Geral</h5>
								<div class="row">
									<div class="col-xl-3 col-lg-3 col-md-3">
										<label><b>M�s/Ano-Ciclo:</b>
											<c:out value="${mesAno}" />
											<c:if test="${historicoFaturamento.numeroCiclo ne ''}">-<c:out value="${historicoFaturamento.numeroCiclo}" /></c:if>
										</label>
									</div>
									<div class="col-xl-3 col-lg-3 col-md-3">
										<label><b>Contrato:</b> <c:out value="${contrato}" /></label>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-12 col-lg-12 col-md-12">
										<table class="table table-bordered table-striped table-hover" style="width: 100% !important;">
											<thead class="thead-ggas-bootstrap">
												<th class="text-center">Im�vel - Ponto Consumo</th>
												<th class="text-center" >Segmento</th>
												<th class="text-center" >C�digo do Ponto de Consumo</th>
												<th class="text-center" >N� Medidor</th>
											</thead>
											<tbody>
												<input type="hidden" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"/>
												<tr>
													<td class="text-center">
														<c:choose>
															<c:when test="${pontoConsumo.imovel.nome ne null && fn:trim(pontoConsumo.imovel.nome) ne ''}">
																<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>
															</c:when>
															<c:otherwise>
																<c:out value="${pontoConsumo.descricao}"/>
															</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center"><c:out value="${pontoConsumo.segmento.descricao}"/></td>
													<td class="text-center"><c:out value="${pontoConsumo.chavePrimaria}"/></td>
													<td class="text-center">
														<c:if test="${pontoConsumo.instalacaoMedidor ne null && pontoConsumo.instalacaoMedidor.medidor ne null}">
															<c:out value="${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}"/>
														</c:if>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="card mt-3">
							<div class="card-body bg-light">
								<h5>Leitura Consumo</h5>
								<div class="row">
									<div class="col-xl-12 col-lg-12 col-md-12">
										<table class="table table-bordered table-striped table-hover" style="width: 100% !important;">
											<thead class="thead-ggas-bootstrap">
											<th class="text-center">Leitura Atual</th>
											<th class="text-center" >Informada pelo cliente</th>
											<th class="text-center" >Data da Leitura Atual</th>
											<th class="text-center" >Leiturista</th>
											<th class="text-center" >Leitura Anterior</th>
											</thead>
											<tbody>
											<c:if test="${historicoConsumo ne null && historicoConsumo.historicoAtual ne null}">
												<tr>
													<td class="text-center">
														<c:choose>
															<c:when test="${historicoConsumo.historicoAtual.numeroLeituraFaturada ne null}">
																<c:out value="${historicoConsumo.historicoAtual.numeroLeituraFaturada}"/>
															</c:when>
															<c:otherwise>
																<c:out value="${historicoConsumo.historicoAtual.numeroLeituraInformada}"/>
															</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${historicoConsumo.historicoAtual.indicadorInformadoCliente}">
																Sim
															</c:when>
															<c:otherwise>
																N�o
															</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${historicoConsumo.historicoAtual.dataLeituraFaturada ne null}">
																<fmt:formatDate value="${historicoConsumo.historicoAtual.dataLeituraFaturada}" pattern="dd/MM/yyyy"/>
															</c:when>
															<c:otherwise>
																<fmt:formatDate value="${historicoConsumo.historicoAtual.dataLeituraInformada}" pattern="dd/MM/yyyy"/>
															</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center"><c:out value="${historicoConsumo.historicoAtual.leiturista.funcionario.nome}" /></td>
													<td class="text-center"><c:out value="${historicoConsumo.historicoAtual.numeroLeituraAnterior}"/></td>
												</tr>
											</c:if>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="card mt-3">
							<div class="card-body bg-light">
								<h5>Anormalidades</h5>
								<div class="row">
									<div class="col-xl-12 col-lg-12 col-md-12">
										<table class="table table-bordered table-striped table-hover" style="width: 100% !important;">
											<thead class="thead-ggas-bootstrap">
												<th class="text-center">Anormalidade</th>
												<th class="text-center">Tipo</th>
												<th class="text-center" >Bloqueia Faturamento</th>
												<th class="text-center" >Data do Apontamento</th>
											</thead>
											<tbody>
											<c:forEach items="${anormalidades}" var="anormalidade">
												<tr>
													<td class="text-center"><c:out value="${anormalidade.descricao}"/></td>
													<td class="text-center"><c:out value="${anormalidade.tipo}"/></td>
													<td class="text-center"><c:out value="${anormalidade.bloqueiaFaturamento  ? 'Sim' : 'N�o'}"/></td>
													<td class="text-center"><fmt:formatDate value="${anormalidade.dataApontamento}" pattern="dd/MM/yyyy"/></td>
												</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<c:if test="${simulado}">
							<div class="card mt-3">
								<div class="card-body bg-light">
									<h5>Fatura</h5>
									<div class="row">
										<div class="col-xl-12 col-lg-12 col-md-12">
                                            <c:if test="${historicoConsumo eq null}">
                                                <label class="text-danger">N�o foi poss�vel obter os valores da fatura devido a falta de informa��es de consumo</label>
                                            </c:if>
                                            <c:if test="${historicoConsumo ne null}">
                                                <label class="text-danger">Valores simulados de acordo com a tabela de pre�os do contrato, podendo haver altera��es no momento do faturamento do ponto de consumo</label>
                                            </c:if>

											<table class="table table-bordered table-striped table-hover" style="width: 100% !important;">
												<thead class="thead-ggas-bootstrap">
												<th class="text-center">Descri��o</th>
												<th class="text-center">Tipo</th>
												<th class="text-center" >Data In�cio</th>
												<th class="text-center" >Data Final</th>
												<th class="text-center" >Tarifa - Vig�ncia</th>
												<th class="text-center" >Faixas</th>
												<th class="text-center" >Quantidade</th>
												<th class="text-center" >Unidade</th>
												<th class="text-center" >Valor Unit�rio (R$)</th>
												<th class="text-center" >Valor (R$)</th>
												</thead>
												<tbody>
												<c:forEach items="${itensFatura}" var="item">
													<tr>
														<td class="text-center"><c:out value="${item.descricao}"/></td>
														<td class="text-center"><c:out value="${item.tipo}"/></td>
														<td class="text-center"><fmt:formatDate value="${item.dataInicio}"></fmt:formatDate></td>
														<td class="text-center"><fmt:formatDate value="${item.dataFim}"></fmt:formatDate></td>
														<td class="text-center"><c:out value="${item.tarifaVigencia}"/></td>
														<td class="text-center"><c:out value="${item.faixas}"/></td>
														<td class="text-center"><fmt:formatNumber value="${item.quantidade}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber></td>
														<td class="text-center">m<span class="expoente">3</span></td>
														<td class="text-right"><fmt:formatNumber value="${item.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber></td>
														<td class="text-right"><fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber></td>
													</tr>
												</c:forEach>
												</tbody>
												<tfoot>
													<tr>
														<th class="text-center" colspan="9"><b>Valor total da fatura</b></th>
														<th class="text-right"><b><fmt:formatNumber value="${totalFatura}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber></b></th>
													</tr>
												</tfoot>
											</table>
											<c:if test="${tributos ne null && not empty tributos}">
												<table class="table table-bordered table-striped table-hover" style="width: 100% !important;">
													<thead class="thead-ggas-bootstrap">
													<th class="text-center" style="width: 25%">Tributo</th>
													<th class="text-center" style="width: 25%">Al�quota</th>
													<th class="text-center" style="width: 25%">Base de C�lculo (R$)</th>
													<th class="text-center" style="width: 25%">Valor (R$)</th>
													</thead>
													<tbody>
													<c:forEach items="${tributos}" var="tributoVO">
														<tr>
															<td class="text-center"><c:out value="${tributoVO.tributoAliquota.tributo.descricao}"/></td>
															<td class="text-center">
																<fmt:formatNumber value="${tributoVO.percentual}" minFractionDigits="2" maxFractionDigits="2"/>%
															</td>
															<td class="text-right">
																<fmt:formatNumber value="${tributoVO.baseCalculo}" minFractionDigits="2" maxFractionDigits="2"/>
															</td>
															<td class="text-right">
																<fmt:formatNumber value="${tributoVO.valor}" minFractionDigits="2" maxFractionDigits="2"/>
															</td>
														</tr>
													</c:forEach>
													</tbody>
												</table>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${listaDadosGerais ne null}">
							<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
								<div class="card mt-3">
									<div class="card-body bg-light">
										<h5>Fatura</h5>
										<div class="row">
											<div class="col-xl-12 col-lg-12 col-md-12">
												<table class="table table-bordered table-striped table-hover" style="width: 100% !important;">
													<thead class="thead-ggas-bootstrap">
													<th class="text-center">Descri��o</th>
													<th class="text-center">Tipo</th>
													<th class="text-center" >Data In�cio</th>
													<th class="text-center" >Data Final</th>
													<th class="text-center" >Quantidade</th>
													<th class="text-center" >Unidade</th>
													<th class="text-center" >Valor Unit�rio (R$)</th>
													<th class="text-center" >Valor (R$)</th>
													</thead>
													<tbody>
													<c:forEach items="${dadosGeraisItensFatura.listaDadosItensFaturaExibicao}" var="item">
														<tr>
															<td class="text-center">
																<c:choose>
																	<c:when test="${item.descricaoDesconto ne null}">
																		<c:out value="${item.descricaoDesconto}"/>
																	</c:when>
																	<c:when test="${item.creditoDebitoARealizar ne null && item.creditoDebitoARealizar.creditoDebitoNegociado ne null
							   						&& item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica ne null && item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao ne null}">
																		<c:out value="${item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao}"/>
																	</c:when>
																	<c:when test="${item.faturaItem ne null && item.faturaItem.rubrica ne null && item.pontoConsumo ne null
							   						&& item.pontoConsumo.instalacaoMedidor ne null && item.pontoConsumo.instalacaoMedidor.medidor ne null}">
																		<c:out value="${item.faturaItem.rubrica.descricaoImpressao}"/>
																	</c:when>
																	<c:otherwise>
																		<c:out value="${item.rubrica.descricaoImpressao}"/>
																	</c:otherwise>
																</c:choose>
															</td>
															<td class="text-center">
																<c:choose>
																	<c:when test="${item.indicadorCredito eq true}">
																		<c:out value="Cr�dito"></c:out>
																	</c:when>
																	<c:otherwise>
																		<c:out value="D�bito"></c:out>
																	</c:otherwise>
																</c:choose>
															</td>
															<td class="text-center"><fmt:formatDate value="${item.dataInicial}"></fmt:formatDate></td>
															<td class="text-center"><fmt:formatDate value="${item.dataFinal}"></fmt:formatDate></td>
															<td class="text-center"><fmt:formatNumber value="${item.faturaItem.quantidade}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber></td>
															<td class="text-center">
																<c:choose>
																	<c:when test="${item.itemFatura ne null}">
																		m<span class="expoente">3</span>
																	</c:when>
																</c:choose>
															</td>
															<td class="text-right">
																<c:choose>
																	<c:when test="${item.indicadorCredito eq true}">
																		-<fmt:formatNumber value="${item.faturaItem.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
																	</c:when>
																	<c:otherwise>
																		<fmt:formatNumber value="${item.faturaItem.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
																	</c:otherwise>
																</c:choose>
															</td>
															<td class="text-right">
																<c:choose>
																	<c:when test="${item.valorTotal ne null}">
																		<c:choose>
																			<c:when test="${item.indicadorCredito eq true}">
																				-<fmt:formatNumber value="${item.faturaItem.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																			</c:when>
																			<c:otherwise>
																				<fmt:formatNumber value="${item.faturaItem.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																			</c:otherwise>
																		</c:choose>
																	</c:when>
																	<c:otherwise>
																		<c:choose>
																			<c:when test="${item.indicadorCredito eq true}">
																				-<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																			</c:when>
																			<c:otherwise>
																				<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																			</c:otherwise>
																		</c:choose>
																	</c:otherwise>
																</c:choose>
															</td>
														</tr>
													</c:forEach>
													</tbody>
													<tfoot>
														<tr>
															<th colspan="7">Valor total da fatura</th>
															<th><fmt:formatNumber value='${dadosGeraisItensFatura.valorTotalFatura}' minFractionDigits="2" maxFractionDigits="2"/></th>
														</tr>
													</tfoot>
												</table>
                                                <c:if test="${dadosGeraisItensFatura.listaDadosTributoVO ne null && not empty dadosGeraisItensFatura.listaDadosTributoVO}">
                                                    <table class="table table-bordered table-striped table-hover" style="width: 100% !important;">
                                                        <thead class="thead-ggas-bootstrap">
                                                            <th class="text-center" style="width: 25%">Tributo</th>
                                                            <th class="text-center" style="width: 25%">Al�quota</th>
                                                            <th class="text-center" style="width: 25%">Base de C�lculo (R$)</th>
                                                            <th class="text-center" style="width: 25%">Valor (R$)</th>
                                                        </thead>
                                                        <tbody>
                                                        <c:forEach items="${dadosGeraisItensFatura.listaDadosTributoVO}" var="tributoVO">
                                                            <tr>
                                                                <td class="text-center"><c:out value="${tributoVO.tributoAliquota.tributo.descricao}"/></td>
                                                                <td class="text-center">
                                                                    <fmt:formatNumber value="${tributoVO.tributoAliquota.valorAliquota}" minFractionDigits="2" maxFractionDigits="2"/>%
                                                                </td>
                                                                <td class="text-right">
                                                                    <fmt:formatNumber value="${tributoVO.baseCalculo}" minFractionDigits="2" maxFractionDigits="2"/>
                                                                </td>
                                                                <td class="text-right">
                                                                    <fmt:formatNumber value="${tributoVO.valor}" minFractionDigits="2" maxFractionDigits="2"/>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </c:if>
											</div>
										</div>
									</div>
								</div>
							</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
            <div class="card-footer">
                <div class="form-row mt-2">
                    <div class="col-md-12">
                        <div class="row justify-content-between">
                            <div class="col-md-6 col-sm-12 mt-1">
                                <button name="button" class="btn btn-default btn-sm mb-1 mr-1" type="button" onclick="voltar()">
									<i class="fa fa-reply"></i> Voltar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</form:form>
</div>

