<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>
	
	$(document).ready(function(){
		$("#cpfCliente").inputmask("999.999.999-99", {placeholder: "_"});
		$("#cnpjCliente").inputmask("99.999.999/9999-99", {placeholder: "_"});
		$("#referencia").inputmask("9999/99",{placeholder:"_"});
		$(".campoData").datepicker({
			changeYear: true,
			maxDate: '-0d',
			showOn: 'button',
			buttonImage: $('#imagemCalendario').val(),
			buttonImageOnly: true,
			buttonText: 'Exibir Calend�rio',
			dateFormat: 'dd/mm/yy'
		});
		
		$("tr.odd").hover(
			function () {
				$("tr.odd .selectedRowColumn").css("background-color","#DEE9EE");
			},
			function () {
				$("tr.odd .selectedRowColumn").css("background","none");
			}
		);
		$("tr.even").hover(
			function () {
				$("tr.even .selectedRowColumn").css("background-color","#FFF");
		},
			function () {
				$("tr.even .selectedRowColumn").css("background","none");
			}
		);
		
	});

	function carregarRotas(elem) {	
		var idGrupoFaturamento = elem.value;
	  	var selectRotas = document.getElementById("idsRota");
	
	  	selectRotas.length=0;
	  	      		
	   	AjaxService.listarRotaPorGrupoFaturamento( idGrupoFaturamento, 
	       	function(rotas) {            		      		         		
	           	for (key in rotas){
	             var novaOpcao = new Option(rotas[key], key);
	         	selectRotas.options[selectRotas.length] = novaOpcao;
	        	}
	        	ordernarSelect(selectRotas);
	    	}
	    );
				
	}
	
	function carregarSetoresComerciais(elem) {
		
		var idLocalidade = elem.value;
		if(idLocalidade=="-1") {
			enableAndDisableSetorComercial(true);
			return true;
		} else {
			enableAndDisableSetorComercial(false);
		}
		var selectsetoresComerciais = document.getElementById("idSetorComercial");

		selectsetoresComerciais.length = 0;

		AjaxService.listarSetoresComerciaisPorLocalidade(idLocalidade, function(
				setores) {
			for (key in setores) {
				var novaOpcao = new Option(setores[key], key);
				selectsetoresComerciais.options[selectsetoresComerciais.length] = novaOpcao;
			}
			var novaOpcao = new Option("Selecione", "-1");
			selectsetoresComerciais.options[selectsetoresComerciais.length] = novaOpcao;
			selectsetoresComerciais.value="-1";
			ordernarSelect(selectsetoresComerciais);
		});

	}
	
	function enableAndDisableSetorComercial(value) {
		document.getElementById('idSetorComercial').disabled = value;
	}

	function gerarRelatorio(){
		submeter("excecaoLeituraFaturaForm", "gerarRelatorioAnaliseFatura");
	}

	function marcarAnalisada(){
		submeter('excecaoLeituraFaturaForm', 'alterarDadosAnomaliaFaturamento');
	}
	function incluirFaturaAnomalia(){
		submeter('excecaoLeituraFaturaForm', 'incluirFaturaAnomalia');
	}
	function detalhar(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("excecaoLeituraFaturaForm", "exibirDetalhamentoExcecaoLeituraConsumo");
	}
	
	
	function limparFormulario(){
		document.getElementById('dataGeracaoInicial').value='';
		document.getElementById('dataGeracaoFinal').value='';
		document.getElementById('dataMedicaoInicial').value='';
		document.getElementById('dataMedicaoFinal').value='';
		document.getElementById('idLocalidade').value='-1';
		document.getElementById('idSetorComercial').value='-1';
		document.getElementById('idSetorComercial').disabled = true;
		document.getElementById('idGrupoFaturamento').value='-1';	
		document.getElementById('idSituacaoPontoConsumo').value='-1';
		document.getElementById('idsRota').length='0';
		document.getElementById('idSegmento').value='-1';
		document.getElementById('idTipoConsumo').value='-1';
		document.getElementById('referencia').value='';
		document.getElementById('ciclo').value='-1';
		document.getElementById('percentualVariacaoValorFatura').value='';
		document.getElementsByName("analisada")[2].checked = true;
		document.getElementsByName("indicadorImpedeFaturamento")[2].checked = true;
        document.getElementById('idAnormalidadeFaturamento').value='-1';
		limparFormularioDadosCliente();
		limparFormularioDadosImovel();
		$('#aba-pessoa-fisica').removeClass('show');
		$('#aba-imovel').removeClass('show');
		$('#aba-pessoa-juridica').removeClass('show');
	}
	function limparFormularioDadosCliente() {
		$('#cpfCliente').val('');
		$('#rgCliente').val('');
		$('#nomeCliente').val('');
		$('#numeroPassaporte').val('');
		$('#cnpjCliente').val('');
		$('#nomeFantasiaCliente').val('');
	}
	function limparFormularioDadosImovel() {
		document.getElementById('cepImovel').value='';
		document.getElementById('matriculaImovel').value='';
		document.getElementById('numeroImovel').value='';
		document.getElementById('descricaoComplemento').value='';
		document.getElementById("condominioImovelTodos").checked = true;
		document.getElementById('nomeImovel').value='';
	}

	function init() {
		var mensagensEstado = $(".mensagens").css("display");
		if(mensagensEstado == "none"){
			<c:if test="${listaHistoricoAnomalia ne null}">
				$.scrollTo($("#historicoFatura"),650);
			</c:if>			
		}
		if(document.getElementById('idLocalidade').value=="-1") {
			enableAndDisableSetorComercial(true);
		}
	}
	addLoadEvent(init);
	
	animatedcollapse.addDiv('pesquisaAvancadaPesquisaExcecaoLeituraConsumo', 'fade=0,speed=400,persist=1,hide=1');

</script>
<link type="text/css" rel="stylesheet" href="${ctxWebpack}/dist/modulos/faturamento/analiseAnormalidadeFaturamento/pesquisa/index.css"/>
<div class="bootstrap">
	<input name="imagemCalendario" type="hidden" id="imagemCalendario" value="<c:url value="/imagens/calendario.png"/>">
	<form:form method="post" action="pesquisarExcecaoLeituraFatura" id="excecaoLeituraFaturaForm" name="excecaoLeituraFaturaForm">
		<input name="acao" type="hidden" id="acao" value="pesquisarExcecaoLeituraConsumo"/>
		<input name="chavePrimarias" type="hidden" id="chavePrimarias">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
		<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
		<input name="idSelecionado" type="hidden" id="idSelecionado" value="">
		<input name="pesquisaAutomatica" type="hidden" id="pesquisaAutomatica" value="${pesquisaAutomatica}">

		<div id="indicadorPesquisa" style="display: none">
			<img src="<c:url value="/imagens/indicator2.gif"/>" width="132px" height="132px"/>
		</div>

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">An�lise de Anormalidade de Faturamento</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i>
					Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em
					<b>Pesquisar</b>, ou clique apenas em <b>Pesquisar</b> para exibir todos.
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body bg-light">
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label id="rotuloLocalidade" for="idLocalidade">Localidade:</label>
										<select name="idLocalidade" id="idLocalidade" class="form-control form-control-sm" onchange="carregarSetoresComerciais(this);">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaLocalidade}" var="localidade">
												<option value="<c:out value="${localidade.chavePrimaria}"/>" <c:if test="${analiseExcecaoFaturaVO.idLocalidade == localidade.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${localidade.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label id="rotuloSetorComercial" for="idSetorComercial">Setor Comercial:</label>
										<select name="idSetorComercial" id="idSetorComercial" class="form-control form-control-sm rotuloSetorComercial1">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSetorComercial}" var="setorComercial">
												<option value="<c:out value="${setorComercial.chavePrimaria}"/>" <c:if test="${analiseExcecaoFaturaVO.idSetorComercial == setorComercial.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${setorComercial.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label id="rotuloGrupoFaturamento" for="idGrupoFaturamento">Grupo de Faturamento:</label>
										<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="form-control form-control-sm idGrupoFaturamento" onchange="carregarRotas(this);">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaGrupoFaturamento}" var="grupoFaturamento">
												<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${analiseExcecaoFaturaVO.idGrupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${grupoFaturamento.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label class="float-none" for="idsRota">Rota:</label>
										<select name="idsRota" id="idsRota" class="form-control form-control-sm" multiple>
											<c:forEach items="${listaRota}" var="rota">
												<c:forEach items="${analiseExcecaoFaturaVO.idsRota}" var="idRota">
													<option value="<c:out value="${rota.chavePrimaria}"/>" <c:if test="${idRota eq rota.chavePrimaria}">selected="selected"</c:if>>
														<c:out value="${rota.numeroRota}"/>
													</option>
												</c:forEach>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idSegmento">Segmento:</label>
										<select name="idSegmento" id="idSegmento" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSegmento}" var="segmento">
												<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${analiseExcecaoFaturaVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${segmento.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idSituacaoPontoConsumo">Situa��o do Ponto do Consumo:</label>
										<select name="idSituacaoPontoConsumo" id="idSituacaoPontoConsumo" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSituacaoPontoConsumo}" var="situacao">
												<option value="<c:out value="${situacao.chavePrimaria}"/>" <c:if test="${analiseExcecaoFaturaVO.idSituacaoPontoConsumo == situacao.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${situacao.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idTipoConsumo">Tipo de Consumo:</label>
										<select name="idTipoConsumo" id="idTipoConsumo" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaTiposConsumo}" var="tipoConsumo">
												<option value="<c:out value="${tipoConsumo.chavePrimaria}"/>" <c:if test="${analiseExcecaoFaturaVO.idTipoConsumo == tipoConsumo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${tipoConsumo.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="referencia">Ano/M�s de Refer�ncia:</label>
										<input class="form-control form-control-sm" type="text" size="5" id="referencia" name="referencia" onkeypress="return formatarCampoInteiro(event,6);" value="${analiseExcecaoFaturaVO.referencia}">
									</div>
								</div>
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="ciclo">Ciclo:</label>
										<select name="ciclo" id="ciclo" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaCiclos}" var="cicloAtual">
												<option value="<c:out value="${cicloAtual}"/>" <c:if test="${cicloAtual == analiseExcecaoFaturaVO.ciclo}">selected="selected"</c:if>>
													<c:out value="${cicloAtual}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="percentualVariacaoValorFatura">Varia��o de valor maior que:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm" type="text" id="percentualVariacaoValorFatura" name="percentualVariacaoValorFatura" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" maxlength="10" value="${analiseExcecaoFaturaVO.percentualVariacaoValorFatura}">
											<div class="input-group-prepend">
												<span class="input-group-text">%</span>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label class="float-none">Analisada: </label><br>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="analisada" id="analisadaSim" class="custom-control-input"
												   value="true" <c:if test="${analiseExcecaoFaturaVO.analisada eq 'true'}">checked</c:if>>
											<label class="custom-control-label" for="analisadaSim">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="analisada" id="analisadaNao" class="custom-control-input"
												   value="false" <c:if test="${analiseExcecaoFaturaVO.analisada eq 'false'}">checked</c:if>>
											<label class="custom-control-label" for="analisadaNao">N�o</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="analisada" id="analisadaTodas" class="custom-control-input"
												   value="" <c:if test="${empty analiseExcecaoFaturaVO.analisada}">checked</c:if>>
											<label class="custom-control-label" for="analisadaTodas">Todas</label>
										</div>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label class="float-none">Anormalidade impede faturamento: </label><br>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="indicadorImpedeFaturamento" id="indicadorImpedeFaturamentoSim" class="custom-control-input"
												   value="true" <c:if test="${analiseExcecaoFaturaVO.indicadorImpedeFaturamento eq 'true'}">checked</c:if>>
											<label class="custom-control-label" for="indicadorImpedeFaturamentoSim">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="indicadorImpedeFaturamento" id="indicadorImpedeFaturamentoNao" class="custom-control-input"
												   value="false" <c:if test="${analiseExcecaoFaturaVO.indicadorImpedeFaturamento eq 'false'}">checked</c:if>>
											<label class="custom-control-label" for="indicadorImpedeFaturamentoNao">N�o</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="indicadorImpedeFaturamento" id="indicadorImpedeFaturamentoTodas" class="custom-control-input"
												   value="" <c:if test="${empty analiseExcecaoFaturaVO.indicadorImpedeFaturamento}">checked</c:if>>
											<label class="custom-control-label" for="indicadorImpedeFaturamentoTodas">Todas</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-6 col-lg-6 col-md-12">
										<label>Per�odo de Emiss�o:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm campoData" type="text" id="dataGeracaoInicial" name="dataGeracaoInicial" maxlength="10" value="${analiseExcecaoFaturaVO.dataGeracaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm campoData" type="text" id="dataGeracaoFinal" name="dataGeracaoFinal" maxlength="10" value="${analiseExcecaoFaturaVO.dataGeracaoFinal}">
										</div>
									</div>
									<div class="col-xl-6 col-lg-6 col-md-12">
										<label>Per�odo de Medi��o:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm campoData" type="text" id="dataMedicaoInicial" name="dataMedicaoInicial" maxlength="10" value="${analiseExcecaoFaturaVO.dataMedicaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm campoData" type="text" id="dataMedicaoFinal" name="dataMedicaoFinal" maxlength="10" value="${analiseExcecaoFaturaVO.dataMedicaoFinal}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-md-12">
										<label for="idAnormalidadeFaturamento">Anormalidade de Faturamento:</label>
										<select name="idAnormalidadeFaturamento" id="idAnormalidadeFaturamento" class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaAnormalidadeFatura}" var="anormalidade">
												<option value="<c:out value="${anormalidade.chavePrimaria}"/>" <c:if test="${analiseExcecaoFaturaVO.idAnormalidadeFaturamento == anormalidade.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${anormalidade.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-md-6">
										<h5>Filtro Cliente</h5>
										<div class="accordion">
											<div class="card">
												<div class="card-header p-0">
													<h5 class="mb-0">
														<button class="btn btn-link btn-sm" type="button"
																onclick="$('#aba-pessoa-fisica').toggleClass('show');"
																aria-expanded="true" aria-controls="collapseOne">
															Pessoa F�sica <i id="caret-aba-pessoa-fisica" class="fa fa-caret-down"></i>
														</button>
													</h5>
												</div>

												<div id="aba-pessoa-fisica" class="collapse" aria-labelledby="headingOne"
													 data-parent="#aba-pessoa-fisica">
													<div class="card-body">
														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" id="rotuloCpf" for="cpfCliente">CPF:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="cpfCliente"
																	   name="cpfCliente" maxlength="14" size="14"  value="${analiseExcecaoFaturaVO.cpfCliente}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" for="rgCliente">RG:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="rgCliente"
																	   name="rgCliente" maxlength="13" size="14" value="${analiseExcecaoFaturaVO.rgCliente}"
																	   onkeypress="return formatarCampoInteiro(event);">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" id="rotuloNome"
																   for="nomeCliente">Nome:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="nomeCliente" name="nomeCliente" value="${analiseExcecaoFaturaVO.nomeCliente}"
																	   maxlength="30" size="30" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" id="rotuloPassaporte"
																   for="numeroPassaporte">Passaporte:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="numeroPassaporte" name="numeroPassaporte" maxlength="13"
																	   onblur="this.value = removerEspacoInicialFinal(this.value);" onkeypress="return formatarCampoNome(event);limparFormulario();"
																	   onkeyup="letraMaiuscula(this);" size="13" value="${analiseExcecaoFaturaVO.numeroPassaporte}">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="accordion">
											<div class="card">
												<div class="card-header p-0">
													<h5 class="mb-0">
														<button class="btn btn-link btn-sm" type="button"
																onclick="$('#aba-pessoa-juridica').toggleClass('show');"
																aria-expanded="true" aria-controls="collapseOne">
															Pessoa Jur�dica <i id="caret-aba-pessoa-juridica" class="fa fa-caret-down"></i>
														</button>
													</h5>
												</div>

												<div id="aba-pessoa-juridica" class="collapse" aria-labelledby="headingOne"
													 data-parent="#aba-pessoa-juridica">
													<div class="card-body" style="border-bottom: 1px solid #dfdfdf;">
														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" id="rotuloCnpj"
																   for="cnpjCliente">CNPJ:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="cnpjCliente"
																	   name="cnpjCliente" maxlength="18"
																	   size="18" value="${analiseExcecaoFaturaVO.cnpjCliente}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" id="rotuloNomeFantasia"
																   for="nomeFantasiaCliente">Nome Fantasia:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text"
																	   id="nomeFantasiaCliente"
																	   name="nomeFantasiaCliente"
																	   maxlength="30" size="30" value="${analiseExcecaoFaturaVO.nomeFantasiaCliente}"
																	   onblur="this.value = removerEspacoInicialFinal(this.value);"
																	   onkeyup="letraMaiuscula(this);">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<h5>Filtro Im�vel</h5>
										<div class="accordion">
											<div class="card">
												<div class="card-header p-0">
													<h5 class="mb-0">
														<button class="btn btn-link btn-sm" type="button" onclick="$('#aba-imovel').toggleClass('show');"
																aria-expanded="true" aria-controls="collapseOne">
															Im�vel <i id="caret-aba-imovel" class="fa fa-caret-down"></i>
														</button>
													</h5>
												</div>

												<div id="aba-imovel" class="collapse" aria-labelledby="headingOne"
													 data-parent="#aba-imovel">
													<div class="card-body" style="border-bottom: 1px solid #dfdfdf;">
														<jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
															<jsp:param name="cepObrigatorio" value="false"/>
															<jsp:param name="idCampoCep" value="cepImovel"/>
															<jsp:param name="chamado" value="true"/>
															<jsp:param name="numeroCep" value="${analiseExcecaoFaturaVO.cepImovel}"/>

														</jsp:include>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" id="rotuloNumeroImovel"
																   for="numeroImovel">N�mero do Im�vel:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="numeroImovel" value="${analiseExcecaoFaturaVO.numeroImovel}"
																	   name="numeroImovel" maxlength="9" size="9" onkeypress="return formatarCampoNumeroEndereco(event, this);">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																   for="descricaoComplemento">Complemento:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="descricaoComplemento" name="complementoImovel" maxlength="25" size="30"
																	   value="${analiseExcecaoFaturaVO.complementoImovel}" onkeyup="letraMaiuscula(this);">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" for="idPontoConsumo">C�digo do Ponto de Consumo:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="idPontoConsumo" name="idPontoConsumo" maxlength="30" size="30"
																	   onkeypress="return formatarCampoInteiro(event);" value="${analiseExcecaoFaturaVO.idPontoConsumo}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" for="nomeImovel">Descri��o:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="nomeImovel" name="nomeImovel" maxlength="30" size="30" value="${analiseExcecaoFaturaVO.nomeImovel}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right" for="matriculaImovel">Matr�cula:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text" id="matriculaImovel"
																	   name="matriculaImovel" maxlength="9" size="9" onkeypress="return formatarCampoInteiro(event)" value="${analiseExcecaoFaturaVO.matriculaImovel}">
															</div>
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<label class="col-md-4 pr-2 text-md-right">Imovel � Condom�nio:</label>
																<div class="custom-control custom-radio custom-control-inline ml-1 mt-1">
																	<input class="custom-control-input" type="radio" name="indicadorCondominio" id="condominioImovelSim" value="true"
																		   <c:if test="${analiseExcecaoFaturaVO.indicadorCondominio eq 'true'}">checked</c:if>>
																	<label class="custom-control-label" for="condominioImovelSim">Sim</label>
																</div>

																<div class="custom-control custom-radio custom-control-inline ml-1">
																	<input class="custom-control-input" type="radio" name="indicadorCondominio" id="condominioImovelNao" value="false"
																		   <c:if test="${analiseExcecaoFaturaVO.indicadorCondominio eq 'false'}">checked</c:if>>
																	<label class="custom-control-label" for="condominioImovelNao">N�o</label>
																</div>
																<div class="custom-control custom-radio custom-control-inline ml-1">
																	<input class="custom-control-input" type="radio" name="indicadorCondominio" id="condominioImovelTodos" value=""
																		   <c:if test="${empty analiseExcecaoFaturaVO.indicadorCondominio}">checked</c:if>>
																	<label class="custom-control-label" for="condominioImovelTodos">Todos</label>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mt-3">
					<div class="col-sm-12">
						<button type="button" class="btn btn-secondary btn-sm float-right" onclick="limparFormulario();">
							<i class="fa fa-times"></i> Limpar
						</button>
						<vacess:vacess param="pesquisarExcecaoLeituraFatura">
							<button id="botaoPesquisar" type="button" class="btn btn-primary btn-sm float-right mr-1">
								<i class="fa fa-search"></i> Pesquisar
							</button>
						</vacess:vacess>
					</div>
				</div>

				<div class="row mt-3">
					<div class="col-sm-12">
						<div class="ocultarTabela containerTabela">
							<table id="tableAnaliseAnormalidade" class="table table-bordered table-striped table-hover" style="width: 100% !important;">
								<thead class="thead-ggas-bootstrap">
									<th scope="col" class="text-center">
										<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="checkAllAuto" type="checkbox"  name="checkAllAuto" class="custom-control-input">
											<label class="custom-control-label p-0" for="checkAllAuto"></label>
										</div>
									</th>
									<th class="text-center">Im�vel</th>
									<th class="text-center" >Ponto de Consumo</th>
									<th class="text-center" >Segmento</th>
									<th class="text-center" >Anormalidade</th>
									<th class="text-center">Tarifa(s)</th>
									<th class="text-center" >Fatura</th>
									<th class="text-center">Refer�ncia</th>
									<th class="text-center">Ciclo</th>
									<th class="text-center">Data de Emiss�o</th>
									<th class="text-center">Data de Resolu��o</th>
									<th class="text-center">Analisada</th>
									<th class="text-center">Usu�rio</th>
									<th class="text-center">Detalhes</th>
								</thead>
								<tbody></tbody>
							</table>
							<div class="row mt-3">
								<div class="col-sm-12">
									<vacess:vacess param="gerarRelatorioAnaliseFatura">
										<button id="gerarRelatorio" type="button" class="btn btn-primary btn-sm float-right">
											Relat�rio An�lise de Faturas
										</button>
									</vacess:vacess>
									<vacess:vacess param="alterarDadosAnomaliaFaturamento">
										<button id="naoFaturar" type="button" class="btn btn-primary btn-sm float-right mr-1">
											N�o Faturar
										</button>
									</vacess:vacess>
									<vacess:vacess param="incluirFaturaAnomalia">
										<button id="resolverAnormalidade" type="button" class="btn btn-primary btn-sm float-right mr-1">
											Resolver Anormalidade
										</button>
									</vacess:vacess>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</form:form>
</div>
<script src="${ctxWebpack}/dist/modulos/faturamento/analiseAnormalidadeFaturamento/pesquisa/index.js"></script>
