<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Alterar Tributo<a href="<help:help>/consultadostributos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>

    $(document).ready(function(){
        var max = 0;
  
        $('.rotulo').each(function(){
                if ($(this).width() > max)
                   max = $(this).width();   
            });
        $('.rotulo').width(max);
    
	  //Datepicker
	    $(".campoData").datepicker({
	    	changeYear : true,
	    	showOn : 'button',
	    	buttonImage : '<c:url value="/imagens/calendario.gif"/>',
	    	buttonImageOnly : true,
	    	buttonText : 'Exibir Calend�rio',
	    	dateFormat : 'dd/mm/yy'
	    });
	    $(".ui-datepicker-trigger").css("margin-top","5px");
	   
	    <c:if test="${sucessoManutencaoLista}">
		    limparTributoAliquota();
		</c:if>
		
		<c:if test="${habilatarBotaoAlterar}">
	    	
			botaoAlterarTributoAliquota.disabled = false;
	        botaoLimparTributoAliquota.disabled = false;
	        botaoIncluirTributoAliquota.disabled = true;
		
		</c:if>
		
		<c:choose>
			<c:when test="${tributoAliquota.indicadorReducaoBaseCalculo}">
		    	showDadosReducaoBaseCalculo();
		   	</c:when>
		  	<c:otherwise>
		    	hideDadosReducaoBaseCalculo();
		   	</c:otherwise>
	    </c:choose>
	    
    });
    
    function salvar(){
    	submeter('tributoForm', 'atualizarTributo');
    }

    function cancelar(){    
    	location.href='exibirPesquisaTributo';
    }
 
	function limparCampos() {

		document.getElementById('descricao').value = "";
        document.getElementById('descricaoAbreviada').value = "";
        document.getElementById('esferaPoder').value = "-1";
        document.forms[0].indicadorServico.checked = false;
        document.forms[0].indicadorProduto.checked = false;

	}
	
    function habilitarDesabilitarCampos(campo) {
    	
    	var labelValorAliquotaTributo = '';
    	
    	if (campo.value > 0) {
    		labelValorAliquotaTributo = ' ' + $(campo.selectedOptions).text().trim() + ' ';
    	}
    	
    	$('#labelValorAliquotaTributo').text("Al�quota" + labelValorAliquotaTributo + ":");

    }
	
    function habilitarDesabilitarMunicipio(){
        
        if(document.forms[0].indicadorFeriadoMunicipal[0].checked == true){
            document.getElementById('idMunicipio').disabled = true;
        }else{
            document.getElementById('idMunicipio').disabled = false;
        } 
    }

    function exibirAlteracaoTributoAliquota(
    		indice, descricaoDataVigencia, chaveMunicipio, 
    		chaveTipoAplicacaoTributoAliquota, conteudoValorTributoAliquota,
    		indicadorReducaoBaseCalculo, chaveTipoAplicacaoReducaoBaseCalculo, 
    		valorReducaoBaseCalculo) {
    	
        if (indice != "") {
            document.forms[0].indexLista.value = indice;
        }

        if(descricaoDataVigencia != ""){
            document.getElementById('dataVigencia').value = descricaoDataVigencia;
        }

        if(chaveMunicipio != ""){
            document.getElementById('idMunicipio').value = chaveMunicipio;
        }

        if(conteudoValorTributoAliquota != 0){
            document.getElementById('conteudoValorTributoAliquota').value = conteudoValorTributoAliquota;
        }
        
        if(chaveTipoAplicacaoTributoAliquota != ""){
        	$('#idValoresAliquota').val(chaveTipoAplicacaoTributoAliquota);
        }        

        habilitarDesabilitarCampos(document.getElementById('idValoresAliquota'));
        
        if(indicadorReducaoBaseCalculo == "true"){
        	clickIndicadorReducaoBaseCalculoSim();
        	$('#idValoresAliquotaReducaoBaseCalculo').val(chaveTipoAplicacaoReducaoBaseCalculo);
        	$('#valorReducaoBaseCalculo').val(valorReducaoBaseCalculo);
        } else {
        	clickIndicadorReducaoBaseCalculoNao();
        	$('#idValoresAliquotaReducaoBaseCalculo').val("-1");
        	$('#valorReducaoBaseCalculo').val("");
        }
            
		var botaoAlterarTributoAliquota = document.getElementById("botaoAlterarTributoAliquota");
		var botaoLimparTributoAliquota = document.getElementById("botaoLimparTributoAliquota"); 
		var botaoIncluirTributoAliquota = document.getElementById("botaoIncluirTributoAliquota");   
		botaoAlterarTributoAliquota.disabled = false;
		botaoLimparTributoAliquota.disabled = false;
		botaoIncluirTributoAliquota.disabled = true;

    }
    
    function dadosReducaoBaseCalculo() {
    	if ($('#indicadorReducaoBaseCalculoSim').is(":checked")) {
    		showDadosReducaoBaseCalculo();
    	}
    }
    
    function showDadosReducaoBaseCalculo() {
    	$("#fsDadosReducaoBaseCalculo").show();
    }
    
    function clickIndicadorReducaoBaseCalculoSim() {
    	$('#indicadorReducaoBaseCalculoSim').click();
    }
    
    function hideDadosReducaoBaseCalculo() {
    	$("#fsDadosReducaoBaseCalculo").hide();
    }
    
    function clickIndicadorReducaoBaseCalculoNao() {
    	$('#indicadorReducaoBaseCalculoNao').click();
    }

    function incluirTributoAliquota(form) {
         document.forms[0].indexLista.value = -1;
         submeter('tributoForm', "incluirTributoAliquota");
    }

    function alterarTributoAliquota(form) {
         submeter('tributoForm', 'incluirTributoAliquota');
    }

    function removerTributoAliquota(indice) {
        
         document.forms[0].indexLista.value = indice;
         
         submeter('tributoForm', 'removerTributoAliquota');

         botaoAlterarTributoAliquota.disabled = true;
         botaoLimparTributoAliquota.disabled = false;
         botaoIncluirTributoAliquota.disabled = false;
    }
    
    function limparTributoAliquota() {

        document.getElementById('idMunicipio').value = "-1";
        document.getElementById('idValoresAliquota').value = "-1";
        document.getElementById('idValoresAliquotaReducaoBaseCalculo').value = "-1";
        document.getElementById('conteudoValorTributoAliquota').value = "";
        document.getElementById('valorReducaoBaseCalculo').value = "";
        document.getElementById('dataVigencia').value = "";
        clickIndicadorReducaoBaseCalculoNao();
            
        var botaoAlterarTributoAliquota = document.getElementById("botaoAlterarTributoAliquota");
        var botaoLimparTributoAliquota = document.getElementById("botaoLimparTributoAliquota"); 
        var botaoIncluirTributoAliquota = document.getElementById("botaoIncluirTributoAliquota");   
        
        botaoAlterarTributoAliquota.disabled = true;
        botaoLimparTributoAliquota.disabled = false;
        botaoIncluirTributoAliquota.disabled = false;
        
    }
    
    function validarCampoInteiro(campo){
    	if (campo.value!=""){
    		var strNumeros = ",01233456789";
    		var tam = campo.value.length;
    		var texto = campo.value;
    		for (i = 0; i < tam; i++){
    			if (strNumeros.indexOf(texto.charAt(i))==-1){
    				campo.value='';
    				break;
    			}
    		}
    	}
    }
	
</script>

<form:form method="post" enctype="multipart/form-data" action="atualizarTributo" id="tributoForm" name="tributoForm">
<input name="postBack" type="hidden" id="postBack" value="true">

<input type="hidden" id="habilitadoOriginal" value="${tributo.habilitado}" />	

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tributo.chavePrimaria}">
<input name="versao" type="hidden" id="versao" value="${tributo.versao}"> 
<input name="indexLista" type="hidden" id="indexLista">
<input name="acao" type="hidden" id="acao" value="alterar"> 
  

<fieldset class="conteinerPesquisarIncluirAltualizar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
	        <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o	:</label>
	        <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="50" size="30" value="${tributo.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/>
	        <br />

        	<label class="rotulo campoObrigatorio">Descri��o Abreviada:</label>
        	<input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="5" size="6" value="${tributo.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/>
        
        <label class="rotulo" id="rotuloEsferaPoder"><span class="campoObrigatorioSimbolo">* </span>Esfera de Poder:</label> <select name="esferaPoder" class="campoSelect" id="esferaPoder">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaEsferaPoder}" var="esferaPoder">
					<option value="<c:out value="${esferaPoder.chavePrimaria}"/>"
						<c:if test="${tributo.esferaPoder.chavePrimaria == esferaPoder.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${esferaPoder.descricao}" />
					</option>
				</c:forEach>
			</select>
        
    </fieldset>
    
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal colunaFinalAjuste">
           <br />
          
        <label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
        <input class="campoCheckbox" type="checkbox" name="indicadorProduto" id="indicadorProduto" value="true" <c:if test="${tributo.indicadorProduto == 'true'}">checked="checked"</c:if>>
        <label class="checkbox" for="indicadorUso">Produto</label>
        <input class="campoCheckbox" type="checkbox" name="indicadorServico" id="indicadorServico" value="true" <c:if test="${tributo.indicadorServico == 'true'}">checked="checked"</c:if> >
        <label class="checkbox" for="indicadorUso">Servi�o</label><br />
        
        <label class="rotulo" id="rotuloIndicadorPadrao" for="indicadorPadrao">Indicador Padr�o:</label>
        <input class="campoRadio" type="radio" name="indicadorPadrao" id="indicadorPadrao" value="true" checked="checked" <c:if test="${tributo.indicadorPadrao eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorPadrao">Sim</label>
        <input class="campoRadio" type="radio" name="indicadorPadrao" id="indicadorPadrao" value="false" <c:if test="${tributo.indicadorPadrao eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorPadrao">N�o</label>
        
        <label class="rotulo" for="habilitado">Indicador de Uso:</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Ativo</label>
        <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
        <label class="rotuloRadio" for="indicadorUso">Inativo</label><br />
        <input type="hidden" id="habilitadoOriginal" value="${habilitado}" />
        
        </fieldset>
    
    <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>
    
    <hr class="linhaSeparadora1" />
    
    <fieldset class="conteinerBloco4">
			<legend class="conteinerBlocoTitulo">Tributo Al�quota</legend>
			<fieldset id="segmentoRamoAtividade" class="conteinerDados">

				<label class="rotulo campoObrigatorio">
				<span class="campoObrigatorioSimbolo2">* </span>Data Vig�ncia:</label>
				<input class="campoData" type="text" id="dataVigencia" name="dataVigencia"
					maxlength="10" value="${dataVigencia}">
					
					
				
				<label class="rotulo campoObrigatorio">
					<span class="campoObrigatorioSimbolo2">* </span>Tipo de Aplica��o de Al�quota:</label> 
				<select name="tipoAplicacaoTributoAliquota" class="campoSelect" id="idValoresAliquota"
					onchange="habilitarDesabilitarCampos(this);">
					
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoTributoAliquota}" var="tipoTributoAliquota">
						<option value="<c:out value="${tipoTributoAliquota.chavePrimaria}"/>"
							<c:if test="${tributoAliquota.tipoAplicacaoTributoAliquota.chavePrimaria == tipoTributoAliquota.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoTributoAliquota.descricao}" />
						</option>
					</c:forEach>
				</select>

				<fieldset id="containerValorTributoAliquota" style="clear: both;">
				
					<label class="rotulo campoObrigatorio">
						<span class="campoObrigatorioSimbolo2">* </span>
						<span id="labelValorAliquotaTributo">Al�quota:</span>
					</label> 
					
					<input type="text" id="conteudoValorTributoAliquota" class="campoTexto"
						name="valorAliquota" maxlength="15" 
						value="${valorAliquota}"
						onkeyup="validarCampoInteiro(this)"
						onkeypress="return formatarCampoDecimalPositivo(event, this, 7, 8);">
						
				</fieldset>

				<fieldset style="clear: both">
					<label class="rotulo">Munic�pio:</label> 
					<select name="municipio" class="campoSelect" id="idMunicipio">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaMunicipios}" var="municipio">
							<option value="<c:out value="${municipio.chavePrimaria}"/>"
								<c:if test="${tributoAliquota.municipio.chavePrimaria == municipio.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${municipio.descricao}" />
							</option>
						</c:forEach>
					</select>
				</fieldset>
				
				<fieldset style="clear: both">
					<label class="rotulo">
						Indicador Redu��o Base de C�lculo:
					</label>
					
					<input type="radio" class="campoRadio"
						id="indicadorReducaoBaseCalculoSim"
						name="indicadorReducaoBaseCalculo"
						value="true" onclick="showDadosReducaoBaseCalculo()"
						<c:if test="${tributoAliquota.indicadorReducaoBaseCalculo}">checked="checked"</c:if> >
						
					<label class="rotuloRadio"
						onclick="clickIndicadorReducaoBaseCalculoSim()">Sim</label>
						
					<input type="radio" class="campoRadio"
						id="indicadorReducaoBaseCalculoNao"
						name="indicadorReducaoBaseCalculo"
						value="false" onclick="hideDadosReducaoBaseCalculo()" 
						<c:if test="${!tributoAliquota.indicadorReducaoBaseCalculo}">checked="checked"</c:if> >
						
					<label class="rotuloRadio"
						onclick="clickIndicadorReducaoBaseCalculoNao()">N�o</label>
						
				</fieldset>
				
				<fieldset id="fsDadosReducaoBaseCalculo" style="clear: both; display: none;">
					<label class="rotulo">
						<span class="campoObrigatorioSimbolo2">* </span>
						Tipo de Aplica��o da </br>Redu��o da Base de C�lculo:
					</label> 
					<select name="tipoAplicacaoReducaoBaseCalculo" class="campoSelect2Linhas" 
						id="idValoresAliquotaReducaoBaseCalculo">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTipoReducaoBaseCalculo}" var="valoresAliquota">
							<option value="<c:out value="${valoresAliquota.chavePrimaria}"/>"
								<c:if test="${tributoAliquota.tipoAplicacaoReducaoBaseCalculo.chavePrimaria == valoresAliquota.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${valoresAliquota.descricao}" />
							</option>
						</c:forEach>
					</select>
				
					<label class="rotulo">
						<span class="campoObrigatorioSimbolo2">* </span>
						Valor Redu��o Base de C�lculo:
					</label>
					<input type="text" maxlength="15" class="campoTexto"
						name="valorReducaoBaseCalculo" id="valorReducaoBaseCalculo"
						value="${valorReducaoBaseCalculo}"
						onkeyup="validarCampoInteiro(this)"
						onkeypress="return formatarCampoDecimalPositivo(event, this, 7, 8);">
				</fieldset>
				
				<p class="legenda">
					<span class="campoObrigatorioSimbolo2">* </span>
					<span>campo obrigat�rio para adi��o de Tributo Al�quota.</span>
				</p>
				
				<fieldset class="conteinerBotoesTributoInclusao"
					style="clear: both; padding-top: 20px;">
					<input class="bottonRightCol2" id="botaoLimparTributoAliquota"
						name="botaoLimparTributoAliquota" value="Limpar" type="button"
						onclick="limparTributoAliquota();"> 
					<input class="bottonRightCol2 bottonLeftColUltimo"
						id="botaoIncluirTributoAliquota"
						name="botaoIncluirTributoAliquota" value="Adicionar" type="button"
						onclick="incluirTributoAliquota(this.form);"> 
					<input class="bottonRightCol2 bottonLeftColUltimo"
						id="botaoAlterarTributoAliquota"
						name="botaoAlterarTributoAliquota" disabled="disabled"
						value="Alterar" type="button"
						onclick="alterarTributoAliquota(this.form);">
				</fieldset>

				<c:set var="i" value="0" />
				<display:table class="dataTableGGAS"
					name="sessionScope.listaTributoAliquota" sort="list"
					id="tributoAliquota" pagesize="15"
					excludedParams="org.apache.struts.taglib.html.TOKEN isPaginacao"
					requestURI="#">
						
					<display:column sortable="false" title="Data Vig�ncia" style="text-align: center; width: 90px">
						<fmt:formatDate value="${tributoAliquota.dataVigencia}" pattern="dd/MM/yyyy"/>
					</display:column>
					
					<display:column sortable="false" title="Al�quota"
							style="width: 130px">
						<fmt:formatNumber value="${tributoAliquota.valorAliquota}" />
					</display:column>
					
					<display:column style="text-align: center; width: 90px"
						title="Tipo" sortable="false">
						<span>${tributoAliquota.tipoAplicacaoTributoAliquota.descricao}</span>
					</display:column>
					
					<display:column property="municipio.descricao" sortable="false"
						title="Munic�pio" />
					
					<display:column style="text-align: center; width: 90px"
						title="Ind. Redu��o" sortable="false">
						
						<c:choose>
							<c:when test="${tributoAliquota.indicadorReducaoBaseCalculo}">
								<span>Sim</span>
							</c:when>
							<c:otherwise>
								<span>N�o</span>
							</c:otherwise>
						</c:choose>
						
					</display:column>

					<display:column style="text-align: center; width: 60px" title="A��es">
						<a href="javascript:exibirAlteracaoTributoAliquota('${i}',
								'<fmt:formatDate value="${tributoAliquota.dataVigencia}" pattern="dd/MM/yyyy"/>', 
								'${tributoAliquota.municipio.chavePrimaria}', 
								'${tributoAliquota.tipoAplicacaoTributoAliquota.chavePrimaria}', 
								'<fmt:formatNumber value="${tributoAliquota.valorAliquota}" 
									maxFractionDigits="8" type="number" />',
								'${tributoAliquota.indicadorReducaoBaseCalculo}', 
								'${tributoAliquota.tipoAplicacaoReducaoBaseCalculo.chavePrimaria}', 
								'<fmt:formatNumber value="${tributoAliquota.valorReducaoBaseCalculo}" 
									maxFractionDigits="8" type="number" />');">
							<img title="Alterar Tributo Al�quota"
							alt="Alterar Tributo Al�quota"
							src="<c:url value="/imagens/16x_editar.gif"/>">
						</a>

						<a onclick="return confirm('Deseja excluir o Tributo Al�quota?');"
							href="javascript:removerTributoAliquota(<c:out value="${i}"/>);">
							<img title="Excluir Tributo Al�quota"
							alt="Excluir Tributo Al�quota"
							src="<c:url value="/imagens/deletar_x.png"/>">
						</a>
					</display:column>
					
					<c:set var="i" value="${i+1}" />
					
				</display:table>
			</fieldset>
		</fieldset>

		<c:if test="${acaoExecucao ne ''}">
			<script language="javascript">
                
                    var botaoAlterarTributoAliquota = document.getElementById("botaoAlterarTributoAliquota");
                    var botaoLimparTributoAliquota = document.getElementById("botaoLimparTributoAliquota"); 
                    var botaoIncluirTributoAliquota = document.getElementById("botaoIncluirTributoAliquota");   
                    
                    var acaoExecucao = '<%= request.getParameter("acaoExecucao") %>';
    
                    if (acaoExecucao == "alterarAliquotaTelaAlteracao") {
                        botaoAlterarTributoAliquota.disabled = false;
                        botaoLimparTributoAliquota.disabled = false;
                        botaoIncluirTributoAliquota.disabled = true;
                    }
                    
                    if (acaoExecucao == "incluirAliquotaTelaAlteracao") {
                        botaoAlterarTributoAliquota.disabled = true;
                        botaoLimparTributoAliquota.disabled = false;
                        botaoIncluirTributoAliquota.disabled = false;
                    }
                
                </script>
		</c:if>
    
</fieldset>
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limparCampos();">
    
    <vacess:vacess param="atualizarTributo">
        <input name="button" id="botaoSalvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" onclick="javascript:salvar();">
    </vacess:vacess>
</fieldset>

<token:token></token:token>

</form:form>
