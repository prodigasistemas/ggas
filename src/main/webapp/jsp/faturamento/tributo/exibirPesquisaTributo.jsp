<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
 

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/DataParadaProgramadaPrototype.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick-pt-BR.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>


<h1 class="tituloInterno">Pesquisar Tributo<a href="<help:help>/consultadostributos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos. 
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" id="tributoForm" name="tributoForm" action="pesquisarTributo">

    <script language="javascript">

    $(document).ready(function(){
    var max = 0;

      $('.rotulo').each(function(){
              if ($(this).width() > max)
                 max = $(this).width();   
          });
      $('.rotulo').width(max);
    });

    function remover(){
        	
    		var selecao = verificarSelecao();
    		if (selecao == true) {
    			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
    			if(retorno == true) {
    				
    				submeter('tributoForm', 'removerTributo');
    			}
  			  }
    }
    
    function alterar(){
        
        var selecao = verificarSelecaoApenasUm();
        if (selecao == true) {  
            document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
            
            submeter('tributoForm', 'exibirAtualizarTributo');
        }
    }
    

    
    function detalhar(chave){

        document.forms[0].chavePrimaria.value = chave;
        submeter('tributoForm', 'exibirDetalhamentoTributo');
    }
    
    function incluirTabelaAuxiliar() {
        location.href='exibirInserirTributo';
    }
    
    function limparFormulario(){
    	limparFormularios(tributoForm);
    	
    	document.tributoForm.indicadorServico[2].checked = true;
    	document.tributoForm.habilitado[0].checked = true;
    }
    
    addLoadEvent(init);

    </script>
    
    <input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	    
    <fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
            <label class="rotulo" id="rotuloDescricao" for="descricao">Descri��o:</label>
            <input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${tributo.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/><br />
            <label class="rotulo" id="rotuloDescricaoAbreviada" for="descricaoAbreviada">Descri��o Abreviada:</label>
            <input class="campoTexto" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${tributo.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/><br /> 
            
            <label class="rotulo" id="rotuloEsferaPoder">Esfera de Poder:</label> <select name="esferaPoder" class="campoSelect" id="esferaPoder">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaEsferaPoder}" var="esferaPoder">
					<option value="<c:out value="${esferaPoder.chavePrimaria}"/>"
						<c:if test="${tributo.esferaPoder.chavePrimaria == esferaPoder.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${esferaPoder.descricao}" />
					</option>
				</c:forEach>
			</select>

		</fieldset>
         <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
         
         		<label class="rotulo">Tipo:</label>
                <input class="campoRadio" type="radio" name="indicadorServico" id="indicadorServico" value="true"  <c:if test="${indicadorServico eq 'true'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorServico">Produto</label>
                <input class="campoRadio" type="radio" name="indicadorServico" id="indicadorServico" value="false"  <c:if test="${indicadorServico eq 'false'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorServico">Servi�o</label>
                <input class="campoRadio" type="radio" name="indicadorServico" id="indicadorServico" value="" <c:if test="${empty indicadorServico}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorServico">Todos</label><br /><br />
         
        		<label class="rotulo">Indicador de Uso:</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true"  <c:if test="${habilitado eq 'true'}">checked</c:if>>
                <label class="rotuloRadio" for="habilitado">Ativo</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false"  <c:if test="${habilitado eq 'false'}">checked</c:if>>
                <label class="rotuloRadio" for="habilitado">Inativo</label>
                <input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
                <label class="rotuloRadio" for="habilitado">Todos</label><br /><br />
                
        <fieldset class="conteinerBotoesPesquisarDirFixo">
        	
        	<br>
               <vacess:vacess param="pesquisarTributo">
                   <input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
               </vacess:vacess>
               <input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="javascript:limparFormulario();">
            </fieldset>
        </fieldset>
        
        </fieldset>
        
        <c:if test="${listaTributo ne null}">
            <hr class="linhaSeparadoraPesquisa" />
             <display:table class="dataTableGGAS" name="listaTributo" sort="list" id="tributo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarTributo">
                    <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
                        <input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${tributo.chavePrimaria}">
                    </display:column>
                    
                    <display:column title="Ativo" style="width: 30px">
                        <c:choose>
                            <c:when test="${tributo.habilitado == true}">
                                <img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
                            </c:when>
                            <c:otherwise>
                                <img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
                            </c:otherwise>
                        </c:choose>
                    </display:column>
				 <display:column sortable="true" sortProperty="descricao" title="Descri��o">
                     <a href="javascript:detalhar(<c:out value='${tributo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
                        <c:out value="${tributo.descricao}"/>
                    </a>
                </display:column>
                
                 <display:column sortable="true" sortProperty="descricao" title="Esfera de Poder">
                     <a href="javascript:detalhar(<c:out value='${tributo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
                        <c:out value="${tributo.esferaPoder.descricao}"/>
                    </a>
                </display:column>
                
                 <display:column  title="Servi�o"  style="width: 120px">
                <c:choose>
                    <c:when test="${tributo.indicadorServico == true}">
                        <img alt="Sim" title="Sim" src="<c:url value="/imagens/check2.gif"/>" border="0"> 
                    </c:when>
                    <c:otherwise>
                         
                    </c:otherwise>
                </c:choose>
             </display:column>
             
             
             <display:column  title="Produto"  style="width: 120px">
                <c:choose>
                    <c:when test="${tributo.indicadorProduto == true}">
                        <img alt="Sim" title="Sim" src="<c:url value="/imagens/check2.gif"/>" border="0"> 
                    </c:when>
                    <c:otherwise>
                         
                    </c:otherwise>
                </c:choose>
             </display:column>
 
            </display:table>
        </c:if>
    
    <fieldset class="conteinerBotoes">
        <c:if test="${not empty listaTributo}">
            <vacess:vacess param="atualizarTributo">
                <input name="buttonRemover" id="botaoAlterar" value="Alterar" class="bottonRightCol2  botaoAlterar" onclick="javascript:alterar();" type="button">
            </vacess:vacess>
            <vacess:vacess param="removerTributo">
                <input name="buttonRemover" id="botaoRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="javascript:remover();" type="button">
            </vacess:vacess>
        </c:if>
        
        <vacess:vacess param="inserirTributo">
            <input id="incluir" name="button" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Incluir" onclick="javascript:incluirTabelaAuxiliar();" type="button">
        </vacess:vacess>
    </fieldset>
</form:form>
