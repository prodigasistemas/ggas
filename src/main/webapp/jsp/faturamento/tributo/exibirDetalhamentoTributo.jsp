<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Detalhar Tributo<a href="<help:help>/consultadostributos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>
		$(document).ready(function(){
			  var max = 0;
		
		    $('.rotulo').each(function(){
		            if ($(this).width() > max)
		               max = $(this).width();   
		        });
		    $('.rotulo').width(max);
			
		});

	 function alterar(){
            submeter('tributoForm', 'exibirAtualizarTributo');
	 }
	 
	 function voltar(){    
	    	location.href='exibirPesquisaTributo';
	 }
	 
</script>

<form method="post" enctype="multipart/form-data" id="tributoForm" name="tributoForm" >
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tributo.chavePrimaria}">
<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">

<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
		
		<label class="rotulo">Descri��o:</label>
		<span class="itemDetalhamento"><c:out value="${tributo.descricao}"/></span>
		<br /><br />
		
		<label class="rotulo">Descri��o Abreviada:</label>
		<span class="itemDetalhamento"><c:out value="${tributo.descricaoAbreviada}"/></span>
		<br /><br />
		
	    <label class="rotulo">Esfera de Poder:</label>
	    <span class="itemDetalhamento"><c:out value="${tributo.esferaPoder.descricao}"/></span>
	    
		</fieldset>

		<fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloIndicadorPadrao"
				for="indicadorPadrao">Indicador Padr�o:</label> <span
				class="itemDetalhamento"><c:out
					value="${tributo.indicadorPadrao ? 'Sim' : 'N�o'}" /></span> <label
				class="rotulo">Tipo:</label> <span class="itemDetalhamento">
				
				<c:out value="${tributo.indicadorServico ? 'Servi�o' : ''}" /><c:if test="${tributo.indicadorServico and tributo.indicadorProduto}">,</c:if>
				<c:out value="${tributo.indicadorProduto ? 'Produto' : ''}" />
				
			</span><br />
		</fieldset>

		<hr class="linhaSeparadora1" />

		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Tributo Al�quota</legend>
			<br />
			<c:set var="i" value="0" />
			<display:table class="dataTableGGAS"
				name="listaTributoAliquota" sort="list"
				id="tributoAliquota" pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN"
				requestURI="#">
					
					<display:column sortable="false" title="Data Vig�ncia">
						<fmt:formatDate value="${tributoAliquota.dataVigencia}" pattern="dd/MM/yyyy"/>
					</display:column>
					
				<display:column sortable="false" title="Al�quota"
						style="width: 130px">
					<fmt:formatNumber value="${tributoAliquota.valorAliquota}"/>
				</display:column>
				
				<display:column style="width: 130px"
					title="Tipo" sortable="false">
					<span>${tributoAliquota.tipoAplicacaoTributoAliquota.descricao}</span>
				</display:column>

				<display:column property="municipio.descricao" sortable="false"
					title="Munic�pio" />
				
				<display:column style="text-align: center; width: 90px"
					title="Ind. Redu��o" sortable="false">
					
					<c:choose>
						<c:when test="${tributoAliquota.indicadorReducaoBaseCalculo}">
							<span>Sim</span>
						</c:when>
						<c:otherwise>
							<span>N�o</span>
						</c:otherwise>
					</c:choose>
					
				</display:column>

				<c:set var="i" value="${i+1}" />
			</display:table>
		</fieldset>
	

</fieldset>
	<fieldset class="conteinerBotoes">
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    
    <vacess:vacess param="atualizarTributo">    
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoAlterar" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
   
</fieldset>
</form>
