<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Rubrica<a class="linkHelp" href="<help:help>/consultaderubrica.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" id="rubricaForm" name="rubricaForm" action="pesquisarRubrica">

	<script language="javascript">
		
		function removerRubrica(){
			
			var selecao = verificarSelecao();
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('rubricaForm', 'removerRubrica');
				}
		    }
		}
		
		function alterarRubrica(){
			
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('rubricaForm', 'exibirAlterarRubrica');
		    }
		}
		
		function detalharRubrica(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("rubricaForm", "exibirDetalhamentoRubrica");
		}
		
		function incluir() {
			location.href = '<c:url value="/exibirInclusaoRubrica"/>';
		}		
		
		function limparFormulario(){
			document.rubricaForm.descricao.value = "";
			document.rubricaForm.descricaoImpressao.value = "";
			document.forms[0].habilitadoAtivo.checked = true;
			document.forms[0].radioRegulamentadaTodos.checked = true;
		}

	</script>

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarRubrica" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" for="descricao" >Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="32" value="${rubrica.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" /><br />
			
			<label class="rotulo" for="descricaoImpressao" >Descri��o para impress�o:</label>
			<input class="campoTexto" type="text" name="descricaoImpressao" id="descricaoImpressao" maxlength="25" size="26" value="${rubrica.descricaoImpressao}"/><br />
			
			<label class="rotulo" for="indicadorRegulamentada">Regulamentada:</label>
			<input class="campoRadio" type="radio" name="indicadorRegulamentada"  id="radioRegulamentadaSim" value="true" <c:if test="${rubrica.indicadorRegulamentada eq 'true'}">checked="checked"</c:if> ><label class="rotuloRadio" for="radioRegulamentadaSim">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorRegulamentada"  id="radioRegulamentadaNao" value="false" <c:if test="${rubrica.indicadorRegulamentada eq 'false'}">checked="checked"</c:if> ><label class="rotuloRadio" for="radioRegulamentadaNao">N�o</label>
			<input class="campoRadio" type="radio" name="indicadorRegulamentada"  id="radioRegulamentadaTodos" value="" <c:if test="${empty rubrica.indicadorRegulamentada}">checked="checked"</c:if> ><label class="rotuloRadio" for="radioRegulamentadaTodos">Todos</label>
		
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoAtivo" value="true" <c:if test="${habilitado eq 'true'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoInativo" value="false" <c:if test="${habilitado eq 'false'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoInativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoTodos" value="" <c:if test="${empty habilitado}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="habilitadoTodos">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarRubrica">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${rubricas ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="rubricas" sort="list" id="rubrica" decorator="br.com.ggas.web.faturamento.rubrica.decorator.RubricaResultadoPesquisaDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarRubrica">
	        
	        <display:column property="chavePrimariaComLock" style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"/>
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${rubrica.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>
	        
	        <display:column sortable="true" sortProperty="descricao" title="Descri��o">
	        	<a href="javascript:detalharRubrica(<c:out value='${rubrica.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${rubrica.descricao}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="descricaoImpressao" title="Descri��o para impress�o" style="width: 250px">
	        	<a href="javascript:detalharRubrica(<c:out value='${rubrica.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${rubrica.descricaoImpressao}"/>
	            </a>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="financiamentoTipo.descricao" title="Tipo de<br />Financiamento" style="width: 100px">
	        	<a href="javascript:detalharRubrica(<c:out value='${rubrica.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${rubrica.financiamentoTipo.descricao}"/>
	            </a>
	        </display:column>
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty rubricas}">
  			<vacess:vacess param="exibirAlterarRubrica">
  				<input id="botaoAlterar" name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarRubrica()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerRubrica">
				<input id="botaoRemover" name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerRubrica()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoRubrica">
   			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>
	
</form:form>
