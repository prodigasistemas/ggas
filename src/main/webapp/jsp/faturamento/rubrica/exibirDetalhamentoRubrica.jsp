<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Rubrica<a class="linkHelp" href="<help:help>/detalhamentoderubrica.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form id="detalhamentoRubrica" method="post" name="rubricaForm" action="exibirDetalhamentoRubrica"> 

	<script>
	
		function voltar(){	
			submeter("rubricaForm", "voltarRubrica");
		}
		
		function alterar(){
			document.forms[0].postBack.value = false;
			submeter('rubricaForm', 'exibirAlterarRubrica');
		}		
		
	</script>
	
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${rubrica.chavePrimaria}"/>
	<input name="habilitadoPesquisa" type="hidden" id="habilitadoPesquisa" value="${habilitado}">
	<input name="habilitado" type="hidden" id="habilitado" value="${habilitado}">
	
	<fieldset id="conteinerRubrica" class="detalhamento">
		<fieldset class="coluna2 detalhamentoColunaMedia">
			<label class="rotulo">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${rubrica.descricao}"/></span><br />
			
			<label class="rotulo rotulo2Linhas">Descri��o para impress�o:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${rubrica.descricaoImpressao}"/></span><br />
			
			<label class="rotulo">Regulamentada:</label>
			<span class="itemDetalhamento">
				<c:if test="${rubrica.indicadorRegulamentada eq 'true'}"><c:out value="Sim"/></c:if>
				<c:if test="${rubrica.indicadorRegulamentada eq 'false'}"><c:out value="N�o"/></c:if>
			</span><br />
			
			<c:if test="${rubrica.indicadorRegulamentada eq false}">
				<label class="rotulo" for="valorReferencia">Valor Refer�ncia:</label>
				<span class="itemDetalhamento">
					<c:if test="${rubrica.valorReferencia != ''}">R$ <c:out value="${valorReferencia}"/></c:if>
				</span><br />				
		
				<label class="rotulo">Valor M�ximo:</label>
				<span class="itemDetalhamento">
				<c:if test="${rubrica.valorMaximo != ''}">R$ <c:out value="${valorMaximo}"/></c:if>
				</span>
			</c:if>
			
			<label class="rotulo" for="unidade">Unidade:</label>
			<span class="itemDetalhamento"><c:out value="${rubrica.unidade.descricao}"/></span><br />
			
			<label class="rotulo" for="tipoRubrica">Tipo de rubrica:</label>
			<span class="itemDetalhamento"><c:out value="${rubrica.financiamentoTipo.descricao}"/></span><br />
			
			<label class="rotulo" for="tipoRubrica">Item de Faturamento:</label>
			<span class="itemDetalhamento">
				<c:if test="${rubrica.indicadorItemFaturamento eq 'true'}"><c:out value="Sim"/></c:if>
				<c:if test="${rubrica.indicadorItemFaturamento eq 'false'}"><c:out value="N�o"/></c:if>
			</span><br />

			<label class="rotulo" for="itemFatura">Item da Fatura:</label>
			<span class="itemDetalhamento"><c:out value="${rubrica.itemFatura.descricao}"/></span><br />
						
			<label class="rotulo rotulo2Linhas" for="lancamentoItemContabil">Lan�amento do item contabil:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${rubrica.lancamentoItemContabil.descricao}"/></span><br />
						
			<label class="rotulo" for="usoExclusivoSistema">Exclusivo Sistema:</label>
		    <span class="itemDetalhamento">
			    <c:if test="${rubrica.indicadorUsoExclusivoSistema eq 'true'}"><c:out value="Sim"/></c:if>
				<c:if test="${rubrica.indicadorUsoExclusivoSistema eq 'false'}"><c:out value="N�o"/></c:if>
			</span><br />
			<label class="rotulo" for="compoeNF">Comp�e NF:</label>
		    <span class="itemDetalhamento">
			    <c:if test="${rubrica.indicadorComposicaoNotaFiscal eq 'true'}"><c:out value="Sim"/></c:if>
				<c:if test="${rubrica.indicadorComposicaoNotaFiscal eq 'false'}"><c:out value="N�o"/></c:if>
			</span>
		    
			<label class="rotulo rotulo2Linhas" for="permiteDescricaoComplementar">Permite descri��o complementar:</label>
			<span class="itemDetalhamento itemDetalhamento2Linhas">
				<c:if test="${rubrica.permiteDescricaoComplementar eq 'true'}"><c:out value="Sim"/><br /></c:if>
				<c:if test="${rubrica.permiteDescricaoComplementar eq 'false'}"><c:out value="N�o"/><br /></c:if>
			</span>
			<br />
			
			<label class="rotulo" for="compoeNF">Nomeclatura Mercosul:</label>
		    <span class="itemDetalhamento">
		    <c:out value="${rubrica.nomeclaturaComumMercosul.descricao}"/>
			</span>
			
			<br />
			
			<label class="rotulo" for="compoeNF">C�digo CEST:</label>
		    <span class="itemDetalhamento">
		    <c:out value="${rubrica.codigoCEST}"/>
			</span>
						
		</fieldset>
		
		<fieldset class="colunaFinal2">
			<fieldset class="conteinerLista1">
			    <legend class="conteinerBlocoTitulo">Tributos</legend>
			    <fieldset class="conteinerDados">
					<span class="itemDetalhamento" id="tributosDisponiveis">
				   		<c:forEach items="${listaRubricasTributo}" var="rubricaTributo">
				   			<c:out value="${rubricaTributo.tributo.descricao}"/>
						    <br />
					    </c:forEach> 
				    </span>
				</fieldset>
			</fieldset>
			
			<fieldset id="conteinerCobrancaRubrica">
				<legend class="conteinerBlocoTitulo">Cobran�a</legend>
					<fieldset class="conteinerDados">
						<label class="rotulo" for="indicadorMulta">Multa:</label>
						<span class="itemDetalhamento">
							<c:if test="${rubrica.indicadorCobrancaMulta eq 'true'}"><c:out value="Sim"/></c:if>
							<c:if test="${rubrica.indicadorCobrancaMulta eq 'false'}"><c:out value="N�o"/></c:if>
						</span>
						<br class="quebraLinha2" />
						<label id="rotuloJurosCobrancaRubrica" class="rotulo rotuloHorizontal" for="indicadorJuros">Juros:</label>
						<span class="itemDetalhamento">
							<c:if test="${rubrica.indicadorCobrancaJuros eq 'true'}"><c:out value="Sim"/></c:if>
							<c:if test="${rubrica.indicadorCobrancaJuros eq 'false'}"><c:out value="N�o"/></c:if>
						</span>
				</fieldset>
			</fieldset>
			
			<fieldset id="conteinerFinanciamentoRubrica">
				<legend class="conteinerBlocoTitulo">Condi��es de Financiamento</legend>
			  	<fieldset class="conteinerDados">
				  	<label class="rotulo" for="numeroMaximoParcelas">N�mero m�ximo de parcelas:</label>
				  	<span class="itemDetalhamento"><c:out value="${rubrica.numeroMaximoParcela}"/></span><br />
				  					  	
				  	<label class="rotulo" for="indicadorExigeEntrada">Exige entrada:</label>
				  	<span class="itemDetalhamento">
				  	<c:if test="${rubrica.indicadorEntradaObrigatoria eq 'true'}"><c:out value="Sim"/></c:if>
					<c:if test="${rubrica.indicadorEntradaObrigatoria eq 'false'}"><c:out value="N�o"/></c:if>
					</span><br />
				  	
				  	<label class="rotulo">Percentual de m�nimo para entrada:</label>
				  	<span class="itemDetalhamento">
				  	<c:if test="${not empty rubrica.percentualMinimoEntrada}"><c:out value="${percentualMinimoEntrada}"/>%</c:if>
				  	</span><br />
				  	
				  	<label class="rotulo">Sistema de amortiza��o:</label>
				  	<span class="itemDetalhamento"><c:out value="${rubrica.amortizacao.descricao}"/></span>				  	
				</fieldset>
			</fieldset>
			<br/>
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<c:if test="${rubrica.habilitado eq 'true'}"><span class="itemDetalhamento"><c:out value="Ativo"/></span><br /></c:if>
			<c:if test="${rubrica.habilitado eq 'false'}"><span class="itemDetalhamento"><c:out value="Inativo"/></span><br /></c:if>
		</fieldset>
		
		<c:if test="${rubrica.indicadorRegulamentada eq true}">
			<hr class="linhaSeparadora1">
		
			<fieldset class="conteinerBloco">			
				<c:set var="i" value="0" />
				<display:table class="dataTableGGAS" name="listaRubricasValorRegulamentado" sort="list" id="rubricaValorRegulamentado" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
					<display:column  sortable="false" title="Data de inicio" style="width: 80px">
						<fmt:formatDate value="${rubricaValorRegulamentado.dataInicioVigencia}" pattern="dd/MM/yyyy"/>
					</display:column>
					
					<display:column  sortable="false" title="Data final" style="width: 80px">
						 <fmt:formatDate value="${rubricaValorRegulamentado.dataFimVigencia}" pattern="dd/MM/yyyy"/>
					</display:column>
						  
					<display:column  sortable="false" title="Valor" style="width: 150px">
						<fmt:formatNumber value="${rubricaValorRegulamentado.valorReferencia}" minFractionDigits="2" type="currency"/>
					</display:column>
					
					<display:column  sortable="false" title="Observa��o">
						<c:out value="${rubricaValorRegulamentado.observacao}"/>
					</display:column>
									
					<c:set var="i" value="${i+1}" />
				</display:table>			
			</fieldset>
		</c:if>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
		
		<vacess:vacess param="exibirAlterarRubrica">
			<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
		</vacess:vacess>	       
	</fieldset>
	
</form:form> 
