<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/dhtmlxcombo.css">


<h1 class="tituloInterno">Incluir Rubrica<a class="linkHelp" href="<help:help>/incluiralterarrubrica.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>


<form:form method="post" id="rubricaForm" name="rubricaForm" action="inserirRubrica">

	<script>

		$(document).ready(function(){
			
			//Esconde e exibe os campo da rubrica regulamentada
			$("input#radioRegulamentadaSim").click(function(){
				animatedcollapse.show('regulamentada');
				$("label[for=valorReferencia]").addClass("campoObrigatorio");
				$("label[for=valorReferencia] span").removeAttr("style");
			});
			$("input#radioRegulamentadaNao").click(function(){
				animatedcollapse.hide('regulamentada');
				$("label[for=valorReferencia]").removeClass("campoObrigatorio");
				$("label[for=valorReferencia] span").css({"display":"none"});
			});

			//Habilita ou n�o o item de fatura
			$("input#indicadorItemFaturamentoSim").click(function(){
				var selectItemFatura = document.getElementById("itemFatura");
				selectItemFatura.disabled = false;
				$("label[for='itemFatura']").removeClass("rotuloDesabilitado");
				$("span[id='idObrigatorioAsteriscoItemFatura']").html("* ");
				//controlarCamposItemFatura(selectItemFatura);
			});
			$("input#indicadorItemFaturamentoNao").click(function(){
				document.forms[0].itemFatura.value = '-1';
				var selectItemFatura = document.getElementById("itemFatura");
	   			selectItemFatura.disabled = true;
				$("label[for='itemFatura']").addClass("rotuloDesabilitado");
				$("span[id='idObrigatorioAsteriscoItemFatura']").removeClass("rotuloDesabilitado");
	   			$("span[id='idObrigatorioAsteriscoItemFatura']").html("");
	   			controlarCamposItemFatura(selectItemFatura);
			});

		    $(".campoData").datepicker({minDate: '+0d',changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		});
		
		function limpar(){
			document.forms[0].descricao.value = '';
			document.forms[0].descricaoImpressao.value = '';
			document.forms[0].valorReferencia.value = '';
			document.forms[0].dataInicioVigencia.value = '';
			document.forms[0].valorMaximo.value = '';
			document.forms[0].observacao.value = '';
			document.forms[0].unidade.value = '-1';
			document.forms[0].tipoRubrica.value = '-1';
			document.forms[0].itemFatura.value = '-1';
			document.forms[0].lancamentoItemContabil.value = '-1';
			document.forms[0].nomeclaturaComumMercosul.value = '-1';
			document.forms[0].codigoCEST.value = '';

			moveAllOptionsEspecial(document.forms[0].idTributosAssociados,document.forms[0].idTributosDisponiveis,true);
						
			document.forms[0].numeroMaximoParcelas.value = ''; 
			document.forms[0].percentualMinimoEntrada.value = '';
			document.forms[0].sistemaAmortizacao.value = '-1';
			document.forms[0].indicadorExigeEntrada1.checked = true;
			document.forms[0].radioRegulamentadaNao.checked = true;
			document.forms[0].usoExclusivoSistema2.checked = true;
			document.forms[0].compoeNF1.checked = true;
			document.forms[0].permiteDescricaoComplementar1.checked = true;
			document.forms[0].indicadorMulta1.checked = true;
			document.forms[0].indicadorJuros1.checked = true;
			
			document.getElementById('itemFatura').onchange();
			document.getElementById('tipoRubrica').onchange();	

			animatedcollapse.hide('regulamentada');
			animatedcollapse.show('naoRegulamentada');					
		}

		function carregarCampoLancamentoItemContabil(elem) {		
			var valor = document.getElementById('valorUnitario');
			var taxaJuros = document.getElementById('taxaJuros');
			var periodicidadeTaxaJuros = document.getElementById('idPeriodicidadeJuros');
						
			if (elem.value != -1) {    		
	        	AjaxService.obterIndicadorTipoLAIC(elem.value, {
		           	callback: function(laic) {   		
		           		var indicadorTipoLAIC = laic['indicadorTipoLAIC'];
		           		if (indicadorTipoLAIC) {
			           		habilitarDesabilitarCamposAmortizacao(indicadorTipoLAIC);
		           		} 	           		
	                }, async:false}
	            ); 
	      	}		    
		}

		function habilitarDesabilitarCamposAmortizacao(indicador) {
			 if (indicador == 'true') {
				//Ativa campo obrigat�rio
				$("#sistemaAmortizacao,#percentualMinimoEntrada,#indicadorExigeEntrada1,#indicadorExigeEntrada2").removeAttr("disabled");
				$("label[for=indicadorExigeEntrada],label[for=indicadorExigeEntrada1],label[for=indicadorExigeEntrada2],label[for=percentualMinimoEntrada],label[for=sistemaAmortizacao]").removeClass("rotuloDesabilitado");
          			
			} else {
				$("#sistemaAmortizacao,#percentualMinimoEntrada,#indicadorExigeEntrada1,#indicadorExigeEntrada2").attr("disabled","disabled");
				$("label[for=indicadorExigeEntrada],label[for=indicadorExigeEntrada1],label[for=indicadorExigeEntrada2],label[for=percentualMinimoEntrada],label[for=sistemaAmortizacao]").addClass("rotuloDesabilitado");
			}
		}
		
		function voltar() {
			location.href = '<c:url value="/exibirPesquisaRubrica"/>';
		}

		function controlarCamposTipoRubrica(elem) {
			var idTipoRubrica = elem.value;
	   		
	   		if (idTipoRubrica != '-1') {
	   			if (idTipoRubrica==document.getElementById("tipoRubricaServico").value || 
	   					idTipoRubrica==document.getElementById("tipoRubricaParcelamento").value) {
	   				$("span[id='idObrigatorioAsteriscoValorMaximo']").html("* ");
	   			} else {
	   				$("span[id='idObrigatorioAsteriscoValorMaximo']").html("");
	   			}
			} else {
				$("span[id='idObrigatorioAsteriscoValorMaximo']").html("");
			}
		}
		
		function controlarCamposItemFatura(elem) {
			
			var itemFatura = elem.value;
			
			var selectTipoRubrica = document.getElementById("tipoRubrica");
			var tributosDisponiveis = document.getElementById("idTributosDisponiveis");
			var tributosDisponiveisSelecionados = document.getElementById("idTributosAssociados");  
			
			var botaoEsquerdo = document.getElementById("botaoDirTop");
			var botaoDireito = document.getElementById("botaoDireita");
			var botaoEsquerdoTodos = document.getElementById("botaoEsquerdaTodos");
			var botaoDireitoTodos = document.getElementById("botaoDireitaTodos");
			
			var numeroMaximoParcelas = document.getElementById("numeroMaximoParcelas");
			var percentualMinimoEntrada = document.getElementById("percentualMinimoEntrada");
			var sistemaAmortizacao = document.getElementById("sistemaAmortizacao");
			var exigeEntradaSim = document.getElementById("indicadorExigeEntrada1");
			var exigeEntradaNao = document.getElementById("indicadorExigeEntrada2");

			var usoExclusivoSistema1 = document.getElementById("usoExclusivoSistema1");
			var usoExclusivoSistema2 = document.getElementById("usoExclusivoSistema2");			
			
			if (itemFatura != '-1' && !elem.disabled) {
			
				$("legend:contains('Tributos'),legend:contains('Condi��es de Financiamento')").addClass("rotuloDesabilitado").css({'border-bottom':'1px solid #BFBFBF'});
				
				$("label[for='idTributosDisponiveis']").addClass("rotuloDesabilitado");
				tributosDisponiveis.disabled = true;
				$("label[for='idTributosAssociados']").addClass("rotuloDesabilitado");
				tributosDisponiveisSelecionados.disabled = true;

				moveAllOptionsEspecial(tributosDisponiveisSelecionados,tributosDisponiveis,true);

				botaoEsquerdo.disabled = true;
				botaoDireito.disabled = true;
				botaoEsquerdoTodos.disabled = true;
				botaoDireitoTodos.disabled = true;
				
				$("label[for='numeroMaximoParcelas']").addClass("rotuloDesabilitado");
				$("input#numeroMaximoParcelas").addClass("campoDesabilitado");
				numeroMaximoParcelas.value = "";				
				numeroMaximoParcelas.disabled = true;
				
				$("label[for='percentualMinimoEntrada']").addClass("rotuloDesabilitado");
				$("input#percentualMinimoEntrada").addClass("campoDesabilitado");
				percentualMinimoEntrada.value = "";
				percentualMinimoEntrada.disabled = true;
				
				$("label[for='sistemaAmortizacao']").addClass("rotuloDesabilitado");
				sistemaAmortizacao.value = -1;
				sistemaAmortizacao.disabled = true;

				$("label[for='indicadorExigeEntrada']").addClass("rotuloDesabilitado");
				$("label[for='indicadorExigeEntrada1']").addClass("rotuloDesabilitado");
				$("label[for='indicadorExigeEntrada2']").addClass("rotuloDesabilitado");
				exigeEntradaSim.checked = true;
				exigeEntradaSim.disabled = true;

				exigeEntradaNao.checked = false;				
				exigeEntradaNao.disabled = true;

				usoExclusivoSistema1.checked = true;
				usoExclusivoSistema2.checked = false;
				usoExclusivoSistema2.disabled = true;
				
				document.getElementById("indicadorItemFaturamentoSim").checked = true;
				document.getElementById("indicadorItemFaturamentoNao").checked = false;
				
				var selectItemFatura = document.getElementById("itemFatura");
				selectItemFatura.disabled = false;
				$("label[for='itemFatura']").removeClass("rotuloDesabilitado");
				$("span[id='idObrigatorioAsteriscoItemFatura']").html("* ");
				
			} else {
				
				$("label[for='tipoRubrica']").removeClass("rotuloDesabilitado");
				selectTipoRubrica.disabled = false;
				$("span[id='idObrigatorioAsteriscoRubrica']").removeClass("rotuloDesabilitado");
	   			$("span[id='idObrigatorioAsteriscoRubrica']").html("* ");
				
				$("label[for='idTributosDisponiveis']").removeClass("rotuloDesabilitado");
				tributosDisponiveis.disabled = false;
				
				$("label[for='idTributosAssociados']").removeClass("rotuloDesabilitado");
				tributosDisponiveisSelecionados.disabled = false;
				
				botaoEsquerdo.disabled = false;
				botaoDireito.disabled = false;
				botaoEsquerdoTodos.disabled = false;
				botaoDireitoTodos.disabled = false;

				$("label[for='numeroMaximoParcelas']").removeClass("rotuloDesabilitado");
				$("input#numeroMaximoParcelas").removeClass("campoDesabilitado");
				numeroMaximoParcelas.disabled = false;
				
				$("label[for='percentualMinimoEntrada']").removeClass("rotuloDesabilitado");
				$("input#percentualMinimoEntrada").removeClass("campoDesabilitado");
				percentualMinimoEntrada.disabled = false;
				
				$("label[for='sistemaAmortizacao']").removeClass("rotuloDesabilitado");
				sistemaAmortizacao.disabled = false;
				
				$("label[for='indicadorExigeEntrada']").removeClass("rotuloDesabilitado");
				$("label[for='indicadorExigeEntrada1']").removeClass("rotuloDesabilitado");
				$("label[for='indicadorExigeEntrada2']").removeClass("rotuloDesabilitado");
				exigeEntradaSim.disabled = false;
				exigeEntradaNao.disabled = false;

				usoExclusivoSistema2.disabled = false;
				
				document.getElementById("indicadorItemFaturamentoSim").checked = false;
				document.getElementById("indicadorItemFaturamentoNao").checked = true;
				
				var selectItemFatura = document.getElementById("itemFatura");
	   			selectItemFatura.disabled = true;
				$("label[for='itemFatura']").addClass("rotuloDesabilitado");
				$("span[id='idObrigatorioAsteriscoItemFatura']").removeClass("rotuloDesabilitado");
	   			$("span[id='idObrigatorioAsteriscoItemFatura']").html("");
	   			
			}
		}
		
		function definirIndicador(val){
			document.forms[0].indicadorExigeEntrada3.value = val.value;
		}
			
		function incluir(){
			var lista = document.getElementById('idTributosAssociados');
			var indicador = document.getElementById('indicadorExigeEntrada1').disabled;
			
			for (i=0; i<lista.length; i++){
				lista.options[i].selected = true;
			}
			
			if(indicador){
				document.forms[0].indicadorExigeEntrada3.value = true;
			}
			
			submeter('rubricaForm', 'inserirRubrica');
		}

		function init () {
				
			if(${rubrica.indicadorRegulamentada eq 'true'}) {
				animatedcollapse.show('regulamentada');
				$("label[for=valorReferencia]").addClass("campoObrigatorio");
				$("label[for=valorReferencia] span").removeAttr("style");
			}
			else {
				animatedcollapse.hide('regulamentada');
				$("label[for=valorReferencia]").removeClass("campoObrigatorio");
				$("label[for=valorReferencia] span").css({"display":"none"});
			}

			if(${rubrica.indicadorItemFaturamento eq 'true'}) {
				var selectItemFatura = document.getElementById("itemFatura");
				selectItemFatura.disabled = false;
				$("label[for='itemFatura']").removeClass("rotuloDesabilitado");
				$("span[id='idObrigatorioAsteriscoItemFatura']").html("* ");
				controlarCamposItemFatura(selectItemFatura);
			}
			else {
				document.forms[0].itemFatura.value = '-1';
				var selectItemFatura = document.getElementById("itemFatura");
	   			selectItemFatura.disabled = true;
				$("label[for='itemFatura']").addClass("rotuloDesabilitado");
				$("span[id='idObrigatorioAsteriscoItemFatura']").removeClass("rotuloDesabilitado");
	   			$("span[id='idObrigatorioAsteriscoItemFatura']").html("");
	   			controlarCamposItemFatura(selectItemFatura);
			}
			
			controlarCamposTipoRubrica(document.getElementById("tipoRubrica"));
			controlarCamposItemFatura(document.getElementById("itemFatura"));			
			
		}
		
		addLoadEvent(init);
		
		animatedcollapse.addDiv('regulamentada','fade=0,speed=400,persist=1,hide=0');
		animatedcollapse.addDiv('naoRegulamentada','fade=0,speed=400,persist=1,hide=1');
		
	</script>
	<input name="tipoRubricaServico" type="hidden" id="tipoRubricaServico" value="${tipoRubricaServico}"/>
	<input name="tipoRubricaParcelamento" type="hidden" id="tipoRubricaParcelamento" value="${tipoRubricaParcelamento}"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="habilitado" type="hidden" id="habilitado" value="${true}">
	<fieldset id="conteinerRubrica" class="conteinerPesquisarIncluirAlterar">
	<input class="campoRadio" type="hidden" name="indicadorEntradaObrigatoria" id="indicadorExigeEntrada3" value="${true}">
		<fieldset class="coluna2">
	
			<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="33" value="${rubrica.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/><br />
	
			<label class="rotulo rotulo2Linhas campoObrigatorio" for="descricaoImpressao"><span class="campoObrigatorioSimbolo">* </span>Descri��o para impress�o:</label>
			<input class="campoTexto campoObrigatorio2Linhas" type="text" name="descricaoImpressao" id="descricaoImpressao" maxlength="25" size="33" value="${rubrica.descricaoImpressao}"/><br />
			
			<label class="rotulo campoObrigatorio" for="indicadorRegulamentada"><span class="campoObrigatorioSimbolo">* </span>Regulamentada:</label>
			<input class="campoRadio" type="radio" name="indicadorRegulamentada" id="radioRegulamentadaSim" value="${true}" <c:if test="${rubrica.indicadorRegulamentada eq 'true'}">checked="checked"</c:if> ><label class="rotuloRadio" for="radioRegulamentadaSim">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorRegulamentada" id="radioRegulamentadaNao" value="${false}" <c:if test="${rubrica.indicadorRegulamentada eq 'false' or empty rubrica.indicadorRegulamentada}">checked="checked"</c:if>><label class="rotuloRadio" for="radioRegulamentadaNao">N�o</label><br class="quebraRadio" />
			
			<label class="rotulo campoObrigatorio" for="valorReferencia"><span class="campoObrigatorioSimbolo">* </span>Valor de refer�ncia:</label>
			<input class="campoTexto campoValorReal valorReferencia" type="text" id="valorReferencia" name="valorReferencia" maxlength="15" size="15" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);" value="${valorReferencia}"><br class="quebraLinha" />
			
			<div id="regulamentada" style="display: none;">
				<label class="rotulo rotulo2Linhas campoObrigatorio" for="dataInicioVigencia"><span class="campoObrigatorioSimbolo">* </span>Data de in�cio da vig�ncia:</label>
				<input class="campoData campoObrigatorio2Linhas" type="text" id="dataInicioVigencia" name="dataInicioVigencia" maxlength="10" value="\<fmt:formatDate value="${dataInicioVigencia}" pattern="dd/MM/yyyy"/>"><br />
				
				<label class="rotulo" for="descricao">Observa��o:</label>
				<input class="campoTexto" type="text" name="observacao" id="observacao" maxlength="50" size="33" value="${observacao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
			</div>
			
			<label class="rotulo campoObrigatorio" for="valorMaximo"><span class="campoObrigatorioSimbolo" id="idObrigatorioAsteriscoValorMaximo">* </span>Valor M�ximo:</label>
			<input class="campoTexto campoValorReal valorReferencia" type="text" id="valorMaximo" name="valorMaximo" maxlength="15" size="15" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);" value="${valorMaximo}"/>
						
			<label class="rotulo" for="unidade"><span class="campoObrigatorioSimbolo">* </span>Unidade:</label>
			<select name="unidade" id="unidade" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidade}" var="unidade">
					<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${rubrica.unidade.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${unidade.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
			
			<label class="rotulo campoObrigatorio" for="tipoRubrica"><span class="campoObrigatorioSimbolo" id="idObrigatorioAsteriscoRubrica">* </span>Tipo de rubrica:</label>
			<select name="financiamentoTipo" id="tipoRubrica" class="campoSelect" onchange="controlarCamposTipoRubrica(this);">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoRubrica}" var="tipoRubrica">
					<option value="<c:out value="${tipoRubrica.chavePrimaria}"/>" <c:if test="${rubrica.financiamentoTipo.chavePrimaria == tipoRubrica.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoRubrica.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
			
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Item de Faturamento:</label>
		    <input class="campoRadio" type="radio" name="indicadorItemFaturamento" id="indicadorItemFaturamentoSim" value="true" <c:if test="${rubrica.indicadorItemFaturamento eq 'true'}">checked</c:if>><label class="rotuloRadio" for="indicadorItemFaturamentoSim">Sim</label>
		   	<input class="campoRadio" type="radio" name="indicadorItemFaturamento" id="indicadorItemFaturamentoNao" value="false" <c:if test="${rubrica.indicadorItemFaturamento eq 'false' or empty rubrica.indicadorItemFaturamento}">checked</c:if>><label class="rotuloRadio" for="indicadorItemFaturamentoNao">N�o</label><br class="quebraRadio2LinhasAntes" />
			
			<label class="rotulo campoObrigatorio" for="itemFatura"><span class="campoObrigatorioSimbolo" id="idObrigatorioAsteriscoItemFatura">* </span>Item da Fatura:</label>
			<select name="itemFatura" id="itemFatura" class="campoSelect campo2Linhas" onchange="controlarCamposItemFatura(this);">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaItemFatura}" var="itemFatura">
					<option value="<c:out value="${itemFatura.chavePrimaria}"/>" <c:if test="${rubrica.itemFatura.chavePrimaria == itemFatura.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${itemFatura.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
			
			<label class="rotulo rotulo2Linhas campoObrigatorio" for="lancamentoItemContabil"><span class="campoObrigatorioSimbolo">* </span>Lan�amento do item contabil:</label>
			<select name="lancamentoItemContabil" id="lancamentoItemContabil" class="campoSelect campoSelect2Linhas" onchange="carregarCampoLancamentoItemContabil(this)">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaLancamentoItemContabil}" var="lancamentoItemContabil">
					<option value="<c:out value="${lancamentoItemContabil.chavePrimaria}"/>" <c:if test="${rubrica.lancamentoItemContabil.chavePrimaria == lancamentoItemContabil.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${lancamentoItemContabil.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />
			
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Exclusivo Sistema:</label>
		    <input class="campoRadio" type="radio" name="indicadorUsoExclusivoSistema" id="usoExclusivoSistema1" value="true" <c:if test="${rubrica.indicadorUsoExclusivoSistema eq 'true'}">checked</c:if>><label class="rotuloRadio" for="usoExclusivoSistema1">Sim</label>
		   	<input class="campoRadio" type="radio" name="indicadorUsoExclusivoSistema" id="usoExclusivoSistema2" value="false" <c:if test="${rubrica.indicadorUsoExclusivoSistema eq 'false'}">checked</c:if> checked><label class="rotuloRadio" for="usoExclusivoSistema2">N�o</label><br class="quebraRadio" />
			
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Comp�e NF:</label>
		    <input class="campoRadio" type="radio" name="indicadorComposicaoNotaFiscal" id="compoeNF1" value="true" <c:if test="${rubrica.indicadorComposicaoNotaFiscal eq 'true' or empty rubrica.indicadorComposicaoNotaFiscal}">checked</c:if>><label class="rotuloRadio" for="compoeNF1">Sim</label>
		   	<input class="campoRadio" type="radio" name="indicadorComposicaoNotaFiscal" id="compoeNF2" value="false" <c:if test="${rubrica.indicadorComposicaoNotaFiscal eq 'false'}">checked</c:if>><label class="rotuloRadio" for="compoeNF2">N�o</label><br class="quebraRadio2LinhasAntes" />
			
			<label class="rotulo rotulo2Linhas campoObrigatorio" for="permiteDescricaoComplementar"><span class="campoObrigatorioSimbolo">* </span>Permite descri��o complementar:</label>
		    <input class="campoRadio campoRadio2Linhas permiteDescricaoComplementar" type="radio" name="permiteDescricaoComplementar" id="permiteDescricaoComplementar1" value="true" <c:if test="${rubrica.permiteDescricaoComplementar eq 'true' or empty rubrica.permiteDescricaoComplementar}">checked</c:if>><label class="rotuloRadio rotuloRadio2Linhas" for="permiteDescricaoComplementar1">Sim</label>
		   	<input class="campoRadio campoRadio2Linhas permiteDescricaoComplementar" type="radio" name="permiteDescricaoComplementar" id="permiteDescricaoComplementar2" value="false" <c:if test="${rubrica.permiteDescricaoComplementar eq 'false'}">checked</c:if>><label class="rotuloRadio rotuloRadio2Linhas" for="permiteDescricaoComplementar2">N�o</label>
			
			<br class="quebraLinha2" />
			
			<label class="rotulo" for="nomeclaturaComumMercosul">Nomeclatura Mercosul:</label>
			<select name="nomeclaturaComumMercosul" id="nomeclaturaComumMercosul" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaNomeclaturaComumMercosul}" var="obj">
					<option value="<c:out value="${obj.chavePrimaria}"/>" <c:if test="${rubrica.nomeclaturaComumMercosul.chavePrimaria == obj.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${obj.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
			
			<br class="quebraLinha2" />
			
			<label class="rotulo campoObrigatorio" for="codigoCEST">C�digo CEST:</label>
			<input class="campoTexto" type="text" name="codigoCEST" id="codigoCEST" maxlength="7" size="10" value="${rubrica.codigoCEST}" onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);"/><br />
	
		
		</fieldset>
		
		<fieldset class="colunaFinal2">
			<fieldset class="conteinerLista1">
			    <legend class="conteinerBlocoTitulo">Tributos</legend>
				<label class="rotulo rotuloVertical" for="idTributosDisponiveis">Dispon�veis:</label>
				<select id="idTributosDisponiveis" class="campoList campoVertical" multiple="multiple"
				onDblClick="moveSelectedOptionsEspecial(document.forms[0].idTributosDisponiveis,document.forms[0].idTributosAssociados,true);">
				   	<c:forEach items="${listaTributos}" var="tributo">
						<option value="<c:out value="${tributo.chavePrimaria}"/>" title="<c:out value="${tributo.descricao}"/>"> <c:out value="${tributo.descricao}"/></option>		
					</c:forEach>			
				</select>
				
				<fieldset class="conteinerBotoesCampoList1a">
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" id="botaoDireita" 
						onClick="moveSelectedOptionsEspecial(document.forms[0].idTributosDisponiveis,document.forms[0].idTributosAssociados,true);">
					<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
						onClick="moveAllOptionsEspecial(document.forms[0].idTributosDisponiveis,document.forms[0].idTributosAssociados,true);">
					<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
						onClick="moveSelectedOptionsEspecial(document.forms[0].idTributosAssociados,document.forms[0].idTributosDisponiveis,true);">
					<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
						onClick="moveAllOptionsEspecial(document.forms[0].idTributosAssociados,document.forms[0].idTributosDisponiveis,true);">
				</fieldset>
			</fieldset>
		
			<fieldset class="conteinerLista2a">
				<label class="rotulo rotuloVertical" for="idTributosAssociados">Associados:</label>
				<select id="idTributosAssociados" class="campoList campoList2 campoVertical" name="idTributosAssociados" multiple="multiple" 
					onDblClick="moveSelectedOptionsEspecial(document.forms[0].idTributosAssociados,document.forms[0].segmentosDisponiveis,true);">
					<c:forEach items="${listaTributosAssociados}" var="tributo">
						<option value="<c:out value="${tributo.chavePrimaria}"/>"> <c:out value="${tributo.descricao}"/></option>
					</c:forEach>
				</select>
			</fieldset>
			
			<fieldset id="conteinerCobrancaRubrica">
				<legend class="conteinerBlocoTitulo">Cobran�a</legend>
					<fieldset class="conteinerDados">
						<label class="rotulo" for="indicadorMulta">Multa:</label>
						<input class="campoRadio" type="radio" name="indicadorCobrancaMulta" id="indicadorMulta1" value="true" <c:if test="${rubrica.indicadorCobrancaMulta eq 'true'  or empty rubrica.indicadorCobrancaMulta}">checked</c:if>><label class="rotuloRadio" for="indicadorMulta1">Sim</label>
						<input class="campoRadio" type="radio" name="indicadorCobrancaMulta" id="indicadorMulta2" value="false" <c:if test="${rubrica.indicadorCobrancaMulta eq 'false'}">checked</c:if>><label class="rotuloRadio" for="indicadorMulta2">N�o</label>
						
						<label id="rotuloJurosCobrancaRubrica" class="rotulo rotuloHorizontal" for="indicadorJuros">Juros:</label>
						<input class="campoRadio" type="radio" name="indicadorCobrancaJuros" id="indicadorJuros1" value="true" <c:if test="${rubrica.indicadorCobrancaJuros eq 'true' or empty rubrica.indicadorCobrancaJuros }">checked</c:if>><label class="rotuloRadio" for="indicadorJuros1">Sim</label>
	   					<input class="campoRadio" type="radio" name="indicadorCobrancaJuros" id="indicadorJuros2" value="false" <c:if test="${rubrica.indicadorCobrancaJuros eq 'false'}">checked</c:if>><label class="rotuloRadio" for="indicadorJuros2">N�o</label>
				</fieldset>
			</fieldset>
		
			<fieldset id="conteinerFinanciamentoRubrica">
				<legend class="conteinerBlocoTitulo">Condi��es de Financiamento</legend>
			  	<fieldset class="conteinerDados">
				  	<label class="rotulo" for="numeroMaximoParcelas">N�mero m�ximo de parcelas:</label>
				  	<input class="campoTexto" type="text" name="numeroMaximoParcela" id="numeroMaximoParcelas" size="3" maxlength="3" value="${rubrica.numeroMaximoParcela}" onkeypress="return formatarCampoInteiro(event)"><br />
				  	
				  	<label class="rotulo" for="indicadorExigeEntrada">Exige entrada:</label>
				    <input class="campoRadio" type="radio" name="entradaObrigatoria" id="indicadorExigeEntrada1" value="${true}" <c:if test="${rubrica.indicadorEntradaObrigatoria eq 'true'}">checked</c:if> checked onclick="definirIndicador(this);"><label class="rotuloRadio" for="indicadorExigeEntrada1">Sim</label>
				   	<input class="campoRadio" type="radio" name="entradaObrigatoria" id="indicadorExigeEntrada2" value="${false}" <c:if test="${rubrica.indicadorEntradaObrigatoria eq 'false'}">checked</c:if> onclick="definirIndicador(this);"><label class="rotuloRadio" for="indicadorExigeEntrada2">N�o</label>
				   	<br class="quebraRadio">
				  	
				  	<label class="rotulo" for="percentualMinimoEntrada">Percentual m�nimo para entrada:</label>
				  	<input class="campoTexto campoHorizontal" type="text" name="percentualMinimoEntrada" id="percentualMinimoEntrada" maxlength="3" size="3" value="${rubrica.percentualMinimoEntrada}" onkeypress="return formatarCampoInteiro(event);">
				  	<label class="rotuloInformativo">%</label>
				  	<br class="quebraLinha2" />
				  	
				  	<label class="rotulo" for="sistemaAmortizacao">Sistema de amortiza��o:</label>
				  	<select name="amortizacao" id="sistemaAmortizacao" class="campoSelect">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaAmortizacao}" var="amortizacao">
							<option value="<c:out value="${amortizacao.chavePrimaria}"/>" <c:if test="${rubrica.amortizacao.chavePrimaria == amortizacao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${amortizacao.descricao}"/>
							</option>		
					    </c:forEach>
					</select>
				</fieldset>
			</fieldset>
		</fieldset>
		
		
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Rubrica.</p>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar();">
	    <vacess:vacess param="inserirRubrica">
	    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="button" onclick="incluir();">
	    </vacess:vacess>
	</fieldset>
	<token:token></token:token>
</form:form>