<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<h1 class="tituloInterno">Simular C�lculo com Dados Reais<a class="linkHelp" href="<help:help>/simulandocalculodefornecimentodegascomdadosreais.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar os Simular C�lculo do Fornecimento de G�s, selecione um Cliente ou Im�vel e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.</p>

<script type="text/javascript">
	
	$(document).ready(function(){	

	  	//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};			
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
		}
		document.getElementById("botaoExibir").disabled = true;		
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		document.getElementById("botaoExibir").disabled = true;
		
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true&funcaoParametro=\'ativarBotaoExibir\'','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");	
		var enderecoImovel = document.getElementById("enderecoImovel");	
		
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];	               	
		               	enderecoImovel.value = imovel["enderecoImovel"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
            enderecoImovel.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;

		ativarBotaoExibir();
	}

	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document.getElementById("nomeCompletoCliente");
		var cnpjFormatado = document.getElementById("cnpjFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document.getElementById("enderecoFormatado");		
		
		if(idSelecionado != '') {				
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];		               	
		               	if(cliente["cnpj"] != undefined ){
		               		cnpjFormatado.value = cliente["cnpj"];
		               	} else {
			               	cnpjFormatado.value = "";			               	
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	cnpjFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
        
        document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
        document.getElementById("cnpjFormatadoTexto").value = cnpjFormatado.value;
        document.getElementById("emailClienteTexto").value = emailCliente.value;
        document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
	}

	function pesquisarSimulacaoDadosReais() {		
		submeter("simulacaoCalculoForm","exibirPontosConsumoSimulacao");
	}

	function exibirExtratoDebitosPontoConsumo(idPonto) {
		document.getElementById('idCliente').value = "";
		document.getElementById('idPontoConsumo').value = idPonto;
		submeter('extratoDebitoForm', 'exibirExtratoDebitos');
	}

	//Persiste as op��es e dados do formul�rio ap�s a submiss�o da pesquisa
	function init() {
		<c:choose>
			<c:when test="${simulacaoDadosReaisVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
				ativarBotaoExibir()
			</c:when>
			<c:when test="${simulacaoDadosReaisVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
				ativarBotaoExibir()
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
				document.getElementById("botaoExibir").disabled = true;
			</c:otherwise>				
		</c:choose>	
		
		var idCliente = '${simulacaoDadosReaisVO.idCliente}';
		var idImovel = '${simulacaoDadosReaisVO.idImovel}';
		
		if(idCliente != ''){
			selecionarCliente(idCliente);
		} 
		if(idImovel != '') {
			selecionarImovel(idImovel);
		}
	}
	addLoadEvent(init);	

	function exibirPontoConsumo(idPontoConsumo){
		
	}

	function ativarBotaoExibir() {
		document.getElementById("botaoExibir").disabled = false;		
	}

</script>	

<form:form method="post" name="simulacaoCalculoForm" action="pesquisarSimulacaoCalculo">
	<input name="acao" type="hidden" id="acao" value="pesquisarSimulacaoCalculo">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idSimulacaoCalculo" type="hidden" id="idSimulacaoCalculo">
	
	<fieldset id="pesquisarSimulacaoCalculo" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${simulacaoCalculoForm.map.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${simulacaoCalculoForm.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${simulacaoCalculoForm.map.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${simulacaoCalculoForm.map.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${simulacaoCalculoForm.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${simulacaoCalculoForm.map.enderecoFormatadoCliente}"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoExibir"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>
		</fieldset>
	
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${simulacaoCalculoForm.map.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${simulacaoCalculoForm.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${simulacaoCalculoForm.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${simulacaoCalculoForm.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${simulacaoCalculoForm.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${simulacaoCalculoForm.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${simulacaoCalculoForm.map.condominio}">
				<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${simulacaoCalculoForm.map.enderecoImovel}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${simulacaoCalculoForm.map.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${simulacaoCalculoForm.map.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${simulacaoCalculoForm.map.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${simulacaoCalculoForm.map.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${simulacaoCalculoForm.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${simulacaoCalculoForm.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBotoesDirFixo3">
			<input class="bottonRightCol2 botaoExibir" id="botaoExibir" type="button" value="Exibir" onclick="pesquisarSimulacaoDadosReais();" disabled="disabled"/>
			<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();" />
		</fieldset>	
	</fieldset>

</form:form> 