<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Simular C�lculo com Dados Reais<a class="linkHelp" href="<help:help>/simulandocalculodefornecimentodegascomdadosreais.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<script type="text/javascript">
	
	$(document).ready(function(){	
		//Efeito mouseover nas tabelas de resultados de pesquisa (displaytag)
	   	$("table#pontoConsumo tr.odd,table#pontoConsumo tr.even").hover(
			function () {
				$(this).addClass("selectedRow");
			},
			function () {
				$(this).removeClass("selectedRow");
			}
		);

		//Adiciona o t�tulo "Total" e estilos na primeira c�lula da linha totalizadora.
		$("table#fornecimentoGas tr.total td:first,table#simularFornecimentoGas tr.total td:first").text("Total:").css({'text-align':'right','padding-right':'3px'});

		//Adiciona o atributo title �s c�lulas e joga o valor da pr�pria c�lula como valor do atributo.
		$("table.dataTableMenor tr td").each(function(){
			var conteudoTabela = $(this).text();
			$(this).attr({'title': conteudoTabela});
		});

		$(".rotuloEntreCampos").css("margin-top","0")
		$("#idMesAnoCiclo").change(function(){
			if($(this).val()!="-1"){
				animatedcollapse.show('conteinerDadosAtuais');
			} else {
				animatedcollapse.hide('conteinerDadosAtuais');
			}
		});

		if($("#idMesAnoCiclo").val() != "-1"){
			animatedcollapse.show('conteinerDadosAtuais');
		} else {
			animatedcollapse.hide('conteinerDadosAtuais');
		}

		animatedcollapse.hide('parametrosSimulacao');
		
	});
	
	//var tarifaSimulada = document.getElementById("idTarifaSimulada");
	//carregarVigenciasFaixas(tarifaSimulada);
	var mostrarValores = "${mostrarValores}";
	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('conteinerDadosAtuais', 'fade=0,speed=400,hide=1');
	if(mostrarValores != "1"){
		animatedcollapse.addDiv('parametrosSimulacao', 'fade=0,speed=400,hide=1');
		animatedcollapse.addDiv('ConteinerResultadoCalculoReal', 'fade=0,speed=400,hide=1');
		animatedcollapse.addDiv('ConteinerResultadoCalculoSimulacao', 'fade=0,speed=400,hide=1');
	}

	function selecionarPontoConsumo(idPontoConsumo){
		document.forms[0].chavePrimaria.value = idPontoConsumo;
		document.getElementById("mostrarValores").value = 1;
		document.forms[0].idTarifaSimulada.value = -1;
		document.forms[0].idMesAnoCiclo.value = -1;
		submeter("simulacaoCalculoForm", "selecionarPontoConsumo");
	}

	function carregarConsumoApurado(elem){
		var idHistoricoConsumo = elem.value
		visualizarResultadoReal();
		carregarConsumo(idHistoricoConsumo);
		atualizarSerieTemporal(AjaxService.obterSeriePCS, atualizarSeriePCSCallback);
	    atualizarSerieTemporal(AjaxService.obterSeriePTZ, atualizarSeriePTZCallback);
	}

	function carregarConsumo(idHistoricoConsumo){
		var consumo = document.getElementById("consumo");
		var consumoApurado = document.getElementById("consumoApurado");
		var dataPeriodoInicial = document.getElementById("dataPeriodoInicial");
		var dataPeriodoFinal = document.getElementById("dataPeriodoFinal");
		var diasConsumo = document.getElementById("diasConsumo");
		var idContrato = document.getElementById("idContrato").value;
      	if (idHistoricoConsumo != "-1") {
	      	AjaxService.obterHistoricoConsumo(idHistoricoConsumo, idContrato,
	           	function(historico) {
	      			consumo.innerHTML = historico["consumo"];
	      			consumoApurado.innerHTML = historico["consumoApurado"];
	      			dataPeriodoInicial.innerHTML = historico["dataPeriodoInicial"];
	      			dataPeriodoFinal.innerHTML = historico["dataPeriodoFinal"];
	      			diasConsumo.innerHTML = historico["diasConsumo"];
	           	}
			);
		} else {
			consumo.innerHTML = "";
			consumoApurado.innerHTML = "";
			dataPeriodoInicial.innerHTML = "";
			dataPeriodoFinal.innerHTML = "";
			diasConsumo.innerHTML = "";
		}
	}

	function calcular(){
		submeter("simulacaoCalculoForm", "calcularDadosReais");
	}


	function carregarVigenciasFaixas(elem){
		var codTarifa = elem.value;
		carregarVigencias(codTarifa);
	}

	function carregarVigencias(codTarifa){
		var selectVigencia = document.getElementById("idVigenciasTarifa");
		visualizarResultadoSimulado();
    	
      	selectVigencia.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectVigencia.options[selectVigencia.length] = novaOpcao;

      	if (codTarifa != "-1") {
      		selectVigencia.disabled = false;      		
        	AjaxService.obterVigencia( codTarifa, {
        			callback: function(tarifaVigencia) {            		      		         		
                	for (key in tarifaVigencia){
                		var item = tarifaVigencia[key];
                    	var novaOpcao = new Option(item[1], item[0]);
                       	selectVigencia.options[selectVigencia.length] = novaOpcao;
                    }
                }, async:false} 
            );

        	var idVigenciasTarifa = "${simulacaoDadosReaisVO.idTarifaAtual}";
        	if(idVigenciasTarifa != undefined && idVigenciasTarifa != ''){
           		//selectVigencia.value = idVigenciasTarifa;
        	}
      	} else {
      		selectVigencia.disabled = true;      	
      	}
	}

	function manterCheckBoxDasTabelas(valor){

		var selectVigencia = "chavePontoConsumo" + "${simulacaoDadosReaisVO.idPontoConsumo}";	

		if (document.getElementById(selectVigencia) != undefined) {
			document.getElementById(selectVigencia).checked = valor;
		}
		
	}

	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaSimulacaoCalculoDadosReais"/>';
	}

	function visualizarResultadoReal(){

		var idMesAnoCiclo = document.getElementById("idMesAnoCiclo").value;		
		var idMesAnoCicloTemp = '<c:out value="${simulacaoDadosReaisVO.idMesAnoCiclo}"/>';	
		var mostrarValores = document.getElementById("mostrarValores").value;
		if(idMesAnoCiclo == idMesAnoCicloTemp || mostrarValores == 1 ){
			//document.getElementById("mostrarValores").value = 0;
			//animatedcollapse.show('ConteinerResultadoCalculoReal');
			$("ConteinerResultadoCalculoReal").show();
		}
		else{
			//animatedcollapse.hide('ConteinerResultadoCalculoReal');
			$("ConteinerResultadoCalculoReal").hide();
		}
		
	}

	function visualizarResultadoSimulado(){

		var idTarifaSimulada = document.getElementById("idTarifaSimulada").value;
		var idVigenciaSimulada = document.getElementById("idVigenciasTarifa").value;

		var idTarifaSimuladaTemp = '<c:out value="${simulacaoDadosReaisVO.idTarifaSimulada}"/>';
		var idVigenciaSimuladaTemp = '<c:out value="${simulacaoDadosReaisVO.idTarifaAtual}"/>';
		var mostrarValores = document.getElementById("mostrarValores").value;
		
		//alert(idTarifaSimulada + " = " + idTarifaSimuladaTemp + " --- " + document.getElementById("idVigenciasTarifa").value + " = " + idVigenciaSimuladaTemp)
		if((idTarifaSimulada == idTarifaSimuladaTemp && idVigenciaSimulada == idVigenciaSimuladaTemp) || mostrarValores == 1){
			//alert("simulada.show");
			//document.getElementById("mostrarValores").value = 0;
			//animatedcollapse.show('ConteinerResultadoCalculoSimulacao');
			$("ConteinerResultadoCalculoSimulacao").show();
		}
		else{
		    //alert("simulada.hide");
			//animatedcollapse.hide('ConteinerResultadoCalculoSimulacao');
		    $("ConteinerResultadoCalculoSimulacao").hide();
		}
		
	}

	function init() {

		manterCheckBoxDasTabelas(true);
		var idHistoricoConsumo = '<c:out value="${simulacaoDadosReaisVO.idMesAnoCiclo}"/>';
		carregarConsumo(idHistoricoConsumo);

		var idTarifa = '<c:out value="${simulacaoDadosReaisVO.idTarifaSimulada}"/>';
		if(idTarifa != undefined && idTarifa != ""){
			carregarVigencias(idTarifa);
			var idVigenciasTarifa = '<c:out value="${simulacaoDadosReaisVO.idVigenciaTarifa}"/>';
			if(idVigenciasTarifa != undefined && idVigenciasTarifa != ''){
				document.getElementById('idVigenciasTarifa').value = idVigenciasTarifa;
			}
		}

		visualizarResultadoReal();

		// Exibe os par�metros de simula��o somente ap�s a sele��o do ponto de consumo
		if($("table#pontoConsumo tr").hasClass("clickedRow")){
			//animatedcollapse.show('parametrosSimulacao');
			$("parametrosSimulacao").show();
		}
		
		atualizarSerieTemporal(AjaxService.obterSeriePCS, atualizarSeriePCSCallback);
	    atualizarSerieTemporal(AjaxService.obterSeriePTZ, atualizarSeriePTZCallback);
	}
	
	addLoadEvent(init);	

	function atualizarSeriePCSCallback(coordenadas) {
		var seriePCS = GraficoPTZePCS.get('PCS');
		var x = $.map(coordenadas, function(fatorCorrecao,data){ return [[parseInt(data),fatorCorrecao]] });
		seriePCS.setData(x);
	}
	
	function atualizarSeriePTZCallback(coordenadas) {
		var seriePTZ = GraficoPTZePCS.get('PTZ');
		var x = $.map(coordenadas, function(fatorCorrecao,data){ return [[parseInt(data),fatorCorrecao]] });
		seriePTZ.setData(x);
	}
	
	function atualizarSerieTemporal(funcaoObterCoordenadas, funcaoCallback) {
		var idHistoricoConsumo = $('select[name="idMesAnoCiclo"]').val();
		funcaoObterCoordenadas(idHistoricoConsumo, funcaoCallback);
	}
	
	$(function () {
		Highcharts.setOptions({
	        global: {
	            timezoneOffset: 3 * 60
	        },
	        lang: {
	            months: ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
	            shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
	            weekdays: ['Domingo', 'Segunda', 'Ter�a', 'Quarta', 'Quinta', 'Sexta', 'S�bado']
	        }
	    });
	    GraficoPTZePCS = Highcharts.chart('graficoPTZePCS', {
	        chart: {
	            type: 'line',
	            width: 500,
	            height: 400
	        },
	        title: {
	            text: 'Fatores de Corre��o'
	        },
	        exporting: {
	            enabled: false
	   		},
	   		navigator: {
	            enabled: true
	        },
	        tooltip: {
	        	shared: true,
	            crosshairs: true
	        },
	        xAxis: {
	            type: 'datetime'
	        },
	        yAxis: {
	            title: {
	                text: ''
	            }
	        },
	        credits: {
	  		  enabled: 0
	  		},
	  		series: [{
	            id: 'PCS',
	            name: 'PCS',
	            data: []
	        }, {
	            id: 'PTZ',
	            name: 'PTZ',
	            data: []
	        }],
	        plotOptions: {
	        	series: {
	                connectNulls: false
	            },
	            line: {
	                dataLabels: {
	                    enabled: true
	                },
	                enableMouseTracking: true
	            }
	        }
	    });
	});

</script>	

<form:form method="post" name="simulacaoCalculoForm" action="exibirPontosConsumoSimulacao">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${simulacaoDadosReaisVO.chavePrimaria}">
	<input name="idCliente" type="hidden" id="idCliente" value="${simulacaoDadosReaisVO.idCliente}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumoSelecionado" value="${simulacaoDadosReaisVO.idPontoConsumo}">
	<input name="idImovel" type="hidden" id="idImovel" value="${simulacaoDadosReaisVO.idImovel}">
	<input name="mostrarValores" type="hidden" id="mostrarValores" value="${mostrarValores}">
	<input name="idContrato" type="hidden" id="idContrato" value="${simulacaoDadosReaisVO.idContrato}">
	
	<fieldset id="exibirSimularCalculo" class="conteinerPesquisarIncluirAlterar">
		<input type="hidden" id="idCliente" name="idCliente" value="${simulacaoDadosReaisVO.idCliente }" />
		
		<c:if test="${simulacaoDadosReaisVO.idCliente ne null && simulacaoDadosReaisVO.idCliente > 0}">
		<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosCliente" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label name="nomeCompletoCliente" id="nomeCompletoCliente" class="rotulo">Nome:</label>
				<span class="itemDetalhamento"><c:out value="${cliente.nome}"/></span><br />
				<label class="rotulo">CPF/CNPJ:</label>
				<span class="itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
				<label class="rotulo">E-mail:</label>
				<span class="itemDetalhamento"><c:out value="${cliente.emailPrincipal}"/></span><br />
			</fieldset>
		</fieldset>
		</c:if>
		
		<input type="hidden" id="idImovel" name="idImovel" value="${simulacaoDadosReaisVO.idImovel}" />
		
		<c:if test="${simulacaoDadosReaisVO.idImovel ne null && simulacaoDadosReaisVO.idImovel > 0}">
		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo">Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.nome}"/></span><br />
				<label class="rotulo">Matr�cula:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.chavePrimaria}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">Cidade:</label>
				<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.nomeMunicipio}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${imovel.enderecoFormatado}"/></span><br />
			</fieldset>
		</fieldset>
		</c:if>
		
		<c:if test="${listaPontoConsumo ne null}">
			<br class="quebraLinha" />
			<p class="orientacaoInicial">Selecione um Ponto de Consumo abaixo.</p>
			<hr class="linhaSeparadora" />
			<display:table class="dataTableGGAS" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.web.faturamento.simulacaocalculo.decorator.PontoConsumoSimularCalculoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarSimulacaoCalculo">
		    	 <c:choose>
		    	 	<c:when test="${simulacaoDadosReaisVO.idCliente ne null && simulacaoDadosReaisVO.idCliente > 0}">
		    	 		<display:column sortable="true" title="Im�vel - Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left">
			     			<a href='javascript:selecionarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
								<c:out value="${pontoConsumo.imovel.nome}"/><c:out value=" - "/><c:out value="${pontoConsumo.descricao}"/>
							</a>
			    		 </display:column>
		    	 	</c:when>
		    	 	<c:otherwise>
		    	 		<display:column sortable="true" title="Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left">
			     			<a href='javascript:selecionarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
								<c:out value="${pontoConsumo.descricao}"/>
							</a>
			    		 </display:column>
		    	 	</c:otherwise>
		    	 </c:choose>
				<display:column property="cityGates" title="City Gates" href="javascript:selecionarPontoConsumo(${pontoConsumo.chavePrimaria});" />
		    	<display:column sortable="false" title="Segmento" style="width: 100px">
		    		<a href='javascript:selecionarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
						<c:out value="${pontoConsumo.segmento.descricao}"/>
					</a>
		    	</display:column>
		    	<display:column sortable="false" title="Situa��o" style="width: 100px">
		    		<a href='javascript:selecionarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
						<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
					</a>
		    	</display:column>		    	
			</display:table>
		</c:if>

		<c:if test="${listaPontoConsumoAgrupados ne null && not empty listaPontoConsumoAgrupados}">
		<hr class="linhaSeparadora" />
			<label class="rotulo">Contrato com faturamento agrupado:</label><span class="itemDetalhamento">${simulacaoDadosReaisVO.idContrato}</span>
			<display:table class="dataTableGGAS" name="listaPontoConsumoAgrupados" sort="list" id="pontoConsumoAgrupado" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarSimulacaoCalculo">
	    	 
    	 		<display:column title="Ponto de Consumo do Contrato" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
					<c:out value="${pontoConsumoAgrupado.descricao}"/>
	    		 </display:column> 	
			     			 	
		    	<display:column title="Segmento" style="width: 100px">
					<c:out value="${pontoConsumoAgrupado.segmento.descricao}"/>
		    	</display:column>
		    	<display:column title="Situa��o" style="width: 100px">
					<c:out value="${pontoConsumoAgrupado.situacaoConsumo.descricao}"/>
		    	</display:column>		    	
			</display:table>
		</c:if>
		
		<hr class="linhaSeparadora" />
		
		<fieldset id="parametrosSimulacao" class="conteinerBloco">
			<p class="orientacaoInicial">Selecione os par�metros de simula��o abaixo e clique em <span class="destaqueOrientacaoInicial">Calcular</span>.</p>
			<fieldset class="coluna">
				<legend class="conteinerBlocoTitulo">Dados Atuais:</legend>
				<fieldset class="conteinerDados">
					<fieldset> <!-- Este fieldset extra corrige um problema de exibi��o no IE -->
						<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>M�s / Ano-Ciclo:</label>
						<select name="idMesAnoCiclo" id="idMesAnoCiclo" class="campoSelect" onchange="carregarConsumoApurado(this)">
			    			<option value="-1">Selecione</option>				
							<c:if test="${listaHistoricoConsumo ne null}">				
								<c:forEach items="${listaHistoricoConsumo}" var="consumo">							
									<option value="<c:out value="${consumo.chavePrimaria}"/>" 
									<c:if test="${simulacaoDadosReaisVO.idMesAnoCiclo == consumo.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${consumo.anoMesFaturamentoFormatado}"/> - <c:out value="${consumo.numeroCiclo}"/>
									</option>
								</c:forEach>
							</c:if>
				 		</select><br />
				 	</fieldset>
		 			<fieldset id="conteinerDadosAtuais">
		 				<label class="rotulo">Consumo:</label>
						<span id="consumo" class="itemDetalhamento"></span>
						<label class="rotuloInformativo">m<span class="expoente">3</span></label><br class="quebraLinha2" />
				 		<label class="rotulo">Consumo Apurado:</label>
						<span id="consumoApurado" class="itemDetalhamento"></span>
						<label class="rotuloInformativo">m<span class="expoente">3</span></label><br class="quebraLinha2" />
						
						<label class="rotulo">Per�odo de consumo:</label>
						<span id="dataPeriodoInicial" class="itemDetalhamento"></span><label class="rotuloEntreCampos">a:</label><span id="dataPeriodoFinal" class="itemDetalhamento"></span><br class="quebraLinha2" />
						<label class="rotulo">Dias de consumo:</label><span id="diasConsumo" class="itemDetalhamento"></span><br class="quebraLinha2" />
					</fieldset>
					<label class="rotulo">Tarifa:</label>
					<select name="idTarifaAtual" id="idTarifaAtual" class="campoSelect" >
		    			<option value="-1">Selecione</option>
		    			<c:forEach items="${listaTarifasPontoConsumo}" var="objTarifaAtual">
							<option value="<c:out value="${objTarifaAtual.chavePrimaria}"/>" <c:if test="${simulacaoDadosReaisVO.idTarifaAtual == objTarifaAtual.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${objTarifaAtual.descricao}"/>
							</option>		
					    </c:forEach>
		 			</select>
		 			
				</fieldset>
				<legend class="conteinerBlocoTitulo">Simula��o:</legend>
				<fieldset class="conteinerDados">
					<label class="rotulo campoObrigatorio" for="idTarifaSimulada"><span class="campoObrigatorioSimbolo">* </span>Tarifa Simulada:</label>
					<select name="idTarifaSimulada" id="idTarifaSimulada" class="campoSelect" onchange="carregarVigenciasFaixas(this);" >
		    			<option value="-1">Selecione</option>
		    			<c:forEach items="${listaTarifa}" var="objTarifa">
							<option value="<c:out value="${objTarifa.chavePrimaria}"/>" <c:if test="${simulacaoDadosReaisVO.idTarifaSimulada == objTarifa.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${objTarifa.descricao}"/>
							</option>		
					    </c:forEach>
		 			</select><br />
						
					<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Vig�ncia:</label>				
					<select name="idVigenciaTarifa" id="idVigenciasTarifa" class="campoSelect" onchange="visualizarResultadoSimulado();" <c:if test="${empty listaVigenciaTarifa}">disabled="disabled"</c:if>>
		    			<option value="-1">Selecione</option>
		    			<c:forEach items="${listaVigenciaTarifa}" var="objVigenciasTarifa">
							<option value="<c:out value="${objVigenciasTarifa.chavePrimaria}"/>" <c:if test="${simulacaoDadosReaisVO.idVigenciaTarifa == objVigenciasTarifa.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${objVigenciasTarifa.descricao}"/>
							</option>		
					    </c:forEach>
		 			</select><br />
					
				</fieldset>
				<input name="button" value="Calcular" class="bottonRightCol2 botaoGrande1" id="botaoExibir" onclick="calcular();" type="button">
			</fieldset>
			
			<fieldset class="colunaEsq2">
				<fieldset id="graficoPTZePCS" />				
			</fieldset>
		</fieldset>		
		
		<hr class="linhaSeparadora" />	
		
		<c:if test="${sessionScope.listaFornecimentoGas ne null}">	
			
			<fieldset id="ConteinerResultadoCalculoReal">
			
				<fieldset id="conteinerFornecimentoGas" class="conteinerBloco">
					<legend class="conteinerBlocoTitulo">Fornecimento de G�s Atual</legend>
					
					<fieldset class="conteinerTipoValor">
						<span class="rotulo valorTotal">Valor Total L�q. (R$)</span><span class="rotulo valores">Valor Vari�vel (R$)</span><span class="rotulo valores">Valor Fixo (R$)</span>
					</fieldset>

					<display:table class="dataTableGGAS dataTableMenor dataTableCabecalho2Linhas" name="sessionScope.listaFornecimentoGas" sort="list" id="fornecimentoGas" decorator="org.displaytag.decorator.TotalTableDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
						<display:column title="Faixa /<br />(m<span class='expoente'>3</span>)" class="colunasConsumoValor">
							<c:choose>
								<c:when test="${fornecimentoGas.tarifaVigenciaFaixa.medidaInicio eq null}">
									<c:out value="0"/> - <c:out value="99999999999999999"/>	
								</c:when>
								<c:otherwise>
									<c:out value="${fornecimentoGas.tarifaVigenciaFaixa.medidaInicio}"/> - <c:out value="${fornecimentoGas.tarifaVigenciaFaixa.medidaFim}"/>
								</c:otherwise>
							</c:choose>
						</display:column>
						<display:column title="Consumo/<br />(m<span class='expoente'>3</span>)" property="consumoApurado"   total="true" class="colunasConsumoValor"/>
						<display:column title="Valor s/<br />Imposto" property="valorFixoSemImpostoSemDescontoFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor do<br />Desconto" property="valorDescontoFixoTotalFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />s/ Imposto" property="valorFixoSemImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />c/ Imposto" property="valorFixoComImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor s/<br />Imposto" property="valorVariavelSemImpostoSemDescontoFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor do<br />Desconto" property="valorDescontoVariavelTotalFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />s/ Imposto" property="valorVariavelSemImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />c/ Imposto" property="valorVariavelComImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaAtual}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor s/<br />Imposto" property="valorTotalSemImpostoFormatado" format="{0,number,#,##0.00}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor c/<br />Imposto" property="valorTotalComImpostoFormatado" format="{0,number,#,##0.00}" total="true" class="colunasConsumoValor"/>
					</display:table>
				</fieldset>
				
			</fieldset>	
			
			<fieldset id="ConteinerResultadoCalculoSimulacao">
				<hr class="linhaSeparadora" />
				
				<fieldset class="conteinerBloco">
					<legend class="conteinerBlocoTitulo">Fornecimento de G�s Simulado</legend>
					
					<fieldset class="conteinerTipoValor">
						<span class="rotulo valorTotal">Valor Total L�q. (R$)</span><span class="rotulo valores">Valor Vari�vel (R$)</span><span class="rotulo valores">Valor Fixo (R$)</span>
					</fieldset>
					
					<display:table class="dataTableGGAS dataTableMenor dataTableCabecalho2Linhas" name="sessionScope.listaSimularFornecimentoGas" sort="list" id="simularFornecimentoGas" decorator="org.displaytag.decorator.TotalTableDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
						<display:column title="Faixa /<br />(m<span class='expoente'>3</span>)" class="colunasConsumoValor">
							<c:out value='${simularFornecimentoGas.tarifaVigenciaFaixa.medidaInicio}'/> - <c:out value='${simularFornecimentoGas.tarifaVigenciaFaixa.medidaFim}'/>
				        </display:column>
						<display:column title="Consumo /<br />(m<span class='expoente'>3</span>)" property="consumoFaturado" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor s/<br />Imposto" property="valorFixoSemImpostoSemDesconto" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor do<br />Desconto" property="valorDescontoFixoTotal" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />s/ Imposto" property="valorFixoSemImpostoComDesconto" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />c/ Imposto" property="valorFixoComImpostoComDesconto" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor s/<br />Imposto" property="valorVariavelSemImpostoSemDesconto" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor do<br />Desconto" property="valorDescontoVariavelTotal" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />s/ Imposto" property="valorVariavelSemImpostoComDesconto" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor L�q.<br />c/ Imposto" property="valorVariavelComImpostoComDesconto" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor s/<br />Imposto" property="valorTotalSemImpostoFormatado" format="{0,number,#,##0.00}" total="true" class="colunasConsumoValor"/>
						<display:column title="Valor c/<br />Imposto" property="valorTotalComImpostoFormatado" format="{0,number,#,##0.00}" total="true" class="colunasConsumoValor"/>
					</display:table>
				</fieldset>	
			
				<hr class="linhaSeparadora" />
				
				<fieldset id="conteinerAliquotasTributacao" class="conteinerBloco">
					<legend class="conteinerBlocoTitulo">Tributa��o</legend>
					<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="sessionScope.listaTributacao" sort="list" id="tributacao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
						<display:column title="Tributo" style="width: 25%">
							<c:choose>
								<c:when test="${tributacao.tributoAliquota eq null}">
									<c:out value="ICMS Substituto" />
								</c:when>
								<c:otherwise>
									<c:out value='${tributacao.tributoAliquota.tributo.descricao}'/>
								</c:otherwise>
							</c:choose>
						</display:column>
						<display:column title="Al�quota" style="width: 25%">
							<c:choose>
								<c:when test="${tributacao.tributoAliquota eq null}">
									<c:out value="-" />
								</c:when>
								<c:otherwise>
									<fmt:formatNumber value="${tributacao.tributoAliquota.valorAliquota}" minFractionDigits="2" type="percent"/>	
								</c:otherwise>
							</c:choose>
						</display:column>
						<display:column title="Base de C�lculo" style="width: 25%">
							<fmt:formatNumber value="${tributacao.baseCalculo}" minFractionDigits="2" type="currency"/>
						</display:column>				
						<display:column title="Valor" style="width: 25%">
							<fmt:formatNumber value="${tributacao.valor}" minFractionDigits="2" type="currency"/>
						</display:column>	
					</display:table>
					
				</fieldset>
				
				<hr class="linhaSeparadora" />
			</fieldset>
		</c:if>
		
		
		<input name="button" value="Cancelar" class="bottonRightCol2 botaoGrande1" id="botaoCancelar" onclick="cancelar();" type="button">
		
	</fieldset>	
	<token:token></token:token>
</form:form> 