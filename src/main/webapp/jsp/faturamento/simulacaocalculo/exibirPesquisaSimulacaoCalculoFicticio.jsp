<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Simular C�lculo do Fornecimento de G�s - Dados Fict�cios<a class="linkHelp" href="<help:help>/simulandocomdadosfictcios.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<script type="text/javascript">
	
	$(document).ready(function(){	

		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
		//Adiciona o t�tulo "Total" e estilos na primeira c�lula da linha totalizadora.
	   	$("table#fornecimentoGas tr.total td:first,table#simularFornecimentoGas tr.total td:first").text("Total:").css({'text-align':'right','padding-right':'3px'});

		//Adiciona o atributo title �s c�lulas e joga o valor da pr�pria c�lula como valor do atributo.
	   	$("table.dataTableMenor tr td").each(function(){
			var conteudoTabela = $(this).text();
			$(this).attr({'title': conteudoTabela});
		});
	});
		
	animatedcollapse.addDiv('ConteinerResultadoCalculoSimulacao', 'fade=0,speed=400,hide=1');
	
	function calcular(){
		submeter("simulacaoCalculoForm", "calcularDadosFicticios");
	}

	function cancelar(){
		
		location.href = '<c:url value="/exibirPesquisaSimulacaoCalculoDadosFicticios"/>';
	}


	function init() {
		
		animatedcollapse.show('ConteinerResultadoCalculoSimulacao');
		
	}
	
	addLoadEvent(init);	

</script>

<form method="post" action="exibirPesquisaSimulacaoCalculoDadosFicticios" name="simulacaoCalculoForm" id="simulacaoCalculoForm">
	
	<token:token></token:token>
	<input name="postBack" type="hidden" id="postBack" value="true">
	
	<fieldset id="pesquisarSimulacaoCalculoFicticio" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="conteinerBloco">
			<fieldset class="coluna">	
				<label class="rotulo campoObrigatorio" for="idTarifaSimulada">
				<span class="campoObrigatorioSimbolo">* </span>Tarifa:</label>
				<select name="idTarifa" id="idTarifa" class="campoSelect">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTarifa}" var="objTarifa">
						<option value="<c:out value="${objTarifa.chavePrimaria}"/>" <c:if test="${idTarifa == objTarifa.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${objTarifa.descricao}" /></option>
					</c:forEach>
				</select> <br />
				
				<label class="rotulo campoObrigatorio" for="consumoApurado"><span class="campoObrigatorioSimbolo">* </span>Consumo:</label> 
				<input type="text" value="${consumoApurado}" name="consumoApurado" id="consumoApurado" class="campoTexto campoHorizontal" maxlength="20" size="20" onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 9)">
				<label for="" class="rotuloHorizontal rotuloInformativo">m<span class="expoente">3</span></label>
			</fieldset>
		
			<fieldset class="colunaFinal">
				<label class="rotulo campoObrigatorio" for="dataInicio"><span class="campoObrigatorioSimbolo">* </span>Per�odo de Consumo:</label> 
				<input class="campoData campoHorizontal" type="text" id="dataInicio" name="dataInicio" maxlength="10" value="${dataInicio}"> 
				<label class="rotuloEntreCampos" id="rotuloEntreCamposData"  for="dataFim">a</label> 
				<input class="campoData campoHorizontal" type="text" id="dataFim" name="dataFim" maxlength="10" value="${dataFim}">
			</fieldset>
			
			<fieldset class="conteinerBotoesDirFixo2">
				<vacess:vacess param="calcularDadosFicticios">
					<input name="button" value="Calcular" class="bottonRightCol2 botaoCalcular" id="botaoCalcular" onclick="calcular();" type="button">
				</vacess:vacess>
				<input name="button" value="Limpar" class="bottonRightCol2 bottonRightColUltimo" id="botaoCancelar" onclick="cancelar();" type="button">
			</fieldset>
		</fieldset>
		
		<c:if test="${sessionScope.listaFornecimentoGasSimulado ne null}">
			<hr class="linhaSeparadora1" />
					
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Fornecimento de G�s Simulado:</legend>
								
				<fieldset class="conteinerTipoValor">
					<span class="rotulo valorTotal">Valor Total L�q. (R$)</span><span class="rotulo valores">Valor Vari�vel (R$)</span><span class="rotulo valores">Valor Fixo (R$)</span>
				</fieldset>
								
				<display:table class="dataTableGGAS dataTableMenor dataTableCabecalho2Linhas" name="sessionScope.listaFornecimentoGasSimulado" sort="list" id="simularFornecimentoGas" decorator="org.displaytag.decorator.TotalTableDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
					<display:column title="Faixa /<br />(m<span class='expoente'>3</span>)" class="colunasConsumoValor">								
						<c:out value='${simularFornecimentoGas.tarifaVigenciaFaixa.medidaInicio}'/> - <c:out value='${simularFornecimentoGas.tarifaVigenciaFaixa.medidaFim}'/>						        
			        </display:column>
					<display:column title="Consumo/<br />(m<span class='expoente'>3</span>)"   class="colunasConsumoValor" >
					<fmt:formatNumber value="${simularFornecimentoGas.consumoApurado}" minFractionDigits="${escalaConsumoApurado}"/>
					</display:column>
					<display:column title="Valor s/<br />Imposto" property="valorFixoSemImpostoSemDescontoFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor do<br />Desconto" property="valorDescontoFixoTotalFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor L�q.<br />s/ Imposto" property="valorFixoSemImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor L�q.<br />c/ Imposto" property="valorFixoComImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor s/<br />Imposto" property="valorVariavelSemImpostoSemDescontoFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor do<br />Desconto" property="valorDescontoVariavelTotalFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor L�q.<br />s/ Imposto" property="valorVariavelSemImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor L�q.<br />c/ Imposto" property="valorVariavelComImpostoComDescontoFormatado" format="{0,number,#,##${maskTarifaSimulada}}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor s/<br />Imposto" property="valorTotalSemImpostoFormatado" format="{0,number,#,##0.00}" total="true" class="colunasConsumoValor" />
					<display:column title="Valor c/<br />Imposto" property="valorTotalComImpostoFormatado" format="{0,number,#,##0.00}" total="true" class="colunasConsumoValor" />						
				</display:table>
			</fieldset>	
				
			<hr class="linhaSeparadora1" />
						
			<fieldset id="conteinerAliquotasTributacao" class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Tributa��o</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="sessionScope.listaTributacao" sort="list" id="tributacao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
					<display:column title="Tributo" style="width: 25%">
						<c:out value='${tributacao.tributoAliquota.tributo.descricao}'/>
					</display:column>
					<display:column title="Al�quota" style="width: 25%">
						<fmt:formatNumber value="${tributacao.tributoAliquota.valorAliquota}" minFractionDigits="2" type="percent"/>							
					</display:column>
					<display:column title="Base de C�lculo" style="width: 25%">
						<fmt:formatNumber value="${tributacao.baseCalculo}" minFractionDigits="2" type="currency"/>
					</display:column>				
					<display:column title="Valor" style="width: 25%">
						<fmt:formatNumber value="${tributacao.valor}" minFractionDigits="2" type="currency"/>
					</display:column>	
				</display:table>
			</fieldset>
		</c:if>
	</fieldset>
</form>
