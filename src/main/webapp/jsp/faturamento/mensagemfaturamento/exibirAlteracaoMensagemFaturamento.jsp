<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/dhtmlxcombo.css">


<h1 class="tituloInterno">Alterar Mensagens de Faturamento<a class="linkHelp" href="<help:help>/inclusoalteraodemensagensdefaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>


<form method="post" action="alterarMensagemFaturamento" name="mensagemFaturamentoForm" id="mensagemFaturamentoForm">

	<script>

		$(document).ready(function(){


			//Esconde e exibe os campo da mensagemFaturamento regulamentada
			$("input#radioRegulamentadaSim").click(function(){
				animatedcollapse.show('regulamentada');
				animatedcollapse.hide('naoRegulamentada');
			});
			$("input#radioRegulamentadaNao").click(function(){
				animatedcollapse.hide('regulamentada');
				animatedcollapse.show('naoRegulamentada');
			});

			//Datepicker
			$("#dataInicioVigencia,#dataFinalVigencia,.dataTarifaVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
			$("#dataFinalVigencia").datepicker('option', 'minDate', '+0d');
			configuraDatePickerDataInicioVigencia();
		});
		
		function configuraDatePickerDataInicioVigencia(){
		    var usada = <c:out value="${usada}"/>;
		    if(usada) {
 		    	$("#dataInicioVigencia").datepicker('disable');
		    } else {
		    	$("#dataInicioVigencia").datepicker('option', 'minDate', '+0d');
		    }
		}
		
		function limpar(){
			document.getElementById("idTipoMensagem").value = "-1";
			document.getElementById("dataInicioVigencia").value = "";
			document.getElementById("dataFinalVigencia").value = "";
			moveAllOptions(document.forms[0].segmentosSelecionados,document.forms[0].segmentosDisponiveis,true);
			atualizarListaRamoAtividade();
			moveAllOptions(document.forms[0].formaCobrancaSelecionados,document.forms[0].formaCobrancaDisponiveis,true);

			moveAllOptions(document.forms[0].dataVencimentoSelecionados,document.forms[0].dataVencimentoDisponiveis,true);
			var idImovel = document.getElementById("idImovel").value = "";
			var matriculaImovel = document.getElementById("matriculaImovel").value = "";
			var nomeFantasia = document.getElementById("nomeFantasiaImovel").value = "";
			var enderecoImovel = document.getElementById("enderecoImovel").value = "";
			var indicadorCondominio = document.getElementById("condominio").value = "";
			document.getElementById("nomeImovelTexto").value = "";
			document.getElementById("matriculaImovelTexto").value = "";
			document.getElementById("enderecoImovelTexto").value = "";
			document.getElementById("indicadorCondominioImovelTexto1").checked = false;
			document.getElementById("indicadorCondominioImovelTexto2").checked = false;


			document.getElementById("mensagem").value = "";		;		
		}
		
		function cancelar() {
			location.href = '<c:url value="/exibirPesquisaMensagemFaturamento"/>';
		}

		function selecionarListas() {
			var listaSegmentosdisponiveis = document.getElementById('segmentosDisponiveis');		
			if (listaSegmentosdisponiveis != undefined) {
				for (i=0; i<listaSegmentosdisponiveis.length; i++){
					listaSegmentosdisponiveis.options[i].selected = true;
				}
			}
			var listaSegmentosSelecionados = document.getElementById('segmentosSelecionados');		
			if (listaSegmentosSelecionados != undefined) {
				for (i=0; i<listaSegmentosSelecionados.length; i++){
					listaSegmentosSelecionados.options[i].selected = true;
				}
			}
			var listaRamoAtividadeDisponiveis = document.getElementById('ramoAtividadeDisponiveis');		
			if (listaRamoAtividadeDisponiveis != undefined) {
				for (i=0; i<listaRamoAtividadeDisponiveis.length; i++){
					listaRamoAtividadeDisponiveis.options[i].selected = true;
				}
			}
			var listaRamoAtividadeSelecionados = document.getElementById('ramoAtividadeSelecionados');		
			if (listaRamoAtividadeSelecionados != undefined) {
				for (i=0; i<listaRamoAtividadeSelecionados.length; i++){
					listaRamoAtividadeSelecionados.options[i].selected = true;
				}
			}
			var listaFormaCobrancaDisponiveis = document.getElementById('formaCobrancaDisponiveis');		
			if (listaFormaCobrancaDisponiveis != undefined) {
				for (i=0; i<listaFormaCobrancaDisponiveis.length; i++){
					listaFormaCobrancaDisponiveis.options[i].selected = true;
				}
			}
			var listaFormaCobrancaSelecionados = document.getElementById('formaCobrancaSelecionados');		
			if (listaFormaCobrancaSelecionados != undefined) {
				for (i=0; i<listaFormaCobrancaSelecionados.length; i++){
					listaFormaCobrancaSelecionados.options[i].selected = true;
				}
			}
			var listaVencimentoDisponiveis = document.getElementById('dataVencimentoDisponiveis');		
			if (listaVencimentoDisponiveis != undefined) {
				for (i=0; i<listaVencimentoDisponiveis.length; i++){
					listaVencimentoDisponiveis.options[i].selected = true;
				}
			}
			var listaVencimentoSelecionados = document.getElementById('dataVencimentoSelecionados');		
			if (listaVencimentoSelecionados != undefined) {
				for (i=0; i<listaVencimentoSelecionados.length; i++){
					listaVencimentoSelecionados.options[i].selected = true;
				}
			}
		}
		
		function alterar(){
			selecionarListas();
			var datepickerInicioVigenciaDesabilitado = $("#dataInicioVigencia").prop('disabled');
			if(datepickerInicioVigenciaDesabilitado) {
				$("#dataInicioVigencia").datepicker('enable');
				submeter('mensagemFaturamentoForm', 'alterarMensagemFaturamento');
				$("#dataInicioVigencia").datepicker('disable');
			} else {
				submeter('mensagemFaturamentoForm', 'alterarMensagemFaturamento');
			}
		}

		function init () {

		}
		
		addLoadEvent(init);

		function exibirPopupPesquisaImovel() {
			popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=525,width=640,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
		}

		function selecionarImovel(idSelecionado){
			var idImovel = document.getElementById("idImovel");
			var matriculaImovel = document.getElementById("matriculaImovel");
			var nomeFantasia = document.getElementById("nomeFantasiaImovel");
			var enderecoImovel = document.getElementById("enderecoImovel");
			var indicadorCondominio = document.getElementById("condominio");
			
			if(idSelecionado != '') {				
				AjaxService.obterImovelPorChave( idSelecionado, {
		           	callback: function(imovel) {	           		
		           		if(imovel != null){  	           			        		      		         		
			               	idImovel.value = imovel["chavePrimaria"];
			               	matriculaImovel.value = imovel["chavePrimaria"];		               	
			               	nomeFantasia.value = imovel["nomeFantasia"];
			               	enderecoImovel.value = imovel["enderecoImovel"];
			               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	}
		        	}, async:false}
		        	
		        );	        
	        } else {
	       		idImovel.value = "";
	        	matriculaImovel.value = "";
	        	nomeFantasia.value = "";
	        	enderecoImovel.value = "";
				indicadorCondominio.value = "";
	       	}
		
			document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
			document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
			document.getElementById("enderecoImovelTexto").value = enderecoImovel.value;
			if(indicadorCondominio.value == 'true') {
				document.getElementById("indicadorCondominioImovelTexto1").checked = true;
			} else {
				document.getElementById("indicadorCondominioImovelTexto2").checked = true;
			}
		}

		function contains(pColecao,pValor) {
			for (var i=0; i<pColecao.length; i=i+1) {
				if (pColecao[i].value == pValor){
					return true;
				}
			}

			return false;	
		}

		function atualizarListaRamoAtividade() {
			var selectListRamoAtividadeDisponiveis = document.getElementById('ramoAtividadeDisponiveis');
			selectListRamoAtividadeDisponiveis.length = 0;
			
			var segmentosSelecionados = document.getElementById('segmentosSelecionados');
			var chavesSelecionadas = new Array(); 
			for (var i=0; i<segmentosSelecionados.options.length; i=i+1) {
				chavesSelecionadas[i] = segmentosSelecionados.options[i].value;
			}
			AjaxService.obterRamosAtividadePorSegmentos( chavesSelecionadas, { 
	           	callback: function(listaRamosAtividade) {
				var selectListRamoAtividadeSelecionados = document.getElementById('ramoAtividadeSelecionados');
				var novasOpcoes = new Array();
				for (key in listaRamosAtividade){
						var novaOpcao = new Option(listaRamosAtividade[key], key);
						novaOpcao.title = listaRamosAtividade[key];

						if (selectListRamoAtividadeSelecionados.length > 0 
								&& contains(selectListRamoAtividadeSelecionados.options,key)) {

							novasOpcoes[novasOpcoes.length] = novaOpcao;
						} else {
							selectListRamoAtividadeDisponiveis.options[selectListRamoAtividadeDisponiveis.length] = novaOpcao;
						}
				}
				selectListRamoAtividadeSelecionados.length = 0;
				for (var count = 0; count < novasOpcoes.length; count++) {
					selectListRamoAtividadeSelecionados.options[count] = novasOpcoes[count];
				}
	       	  }
	       	 , async: false}
	       	);	
		}
		
	</script>
	
	<input type="hidden" name="acao" id="acao" value="exibirDetalhamentoMensagemFaturamento">
	<input type="hidden" name="postBack" id="postBack" value="true">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chavePrimaria}"/>
	<input type="hidden" name="versao" id="versao" value="${versao}">	
	
	<fieldset id="conteinerMensagemFaturamento" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<fieldset id="conteinerTipoMensagemVigencia">
				<label class="rotulo campoObrigatorio" for="tipoMensagem"><span class="campoObrigatorioSimbolo">* </span>Tipo de Mensagem:</label>
				<select name="idTipoMensagem" id="idTipoMensagem" class="campoSelect">
			    	<option value="-1">Selecione</option>
					<c:forEach items="${listaTiposMensagem}" var="tipoMensagem">
						<option value="<c:out value="${tipoMensagem.chavePrimaria}"/>" <c:if test="${idTipoMensagem == tipoMensagem.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${tipoMensagem.descricao}"/>
						</option>		
					</c:forEach>
				</select><br />
				
				<label class="rotulo campoObrigatorio" for="dataInicioVigencia"><span class="campoObrigatorioSimbolo">* </span>Per�odo de Vig�ncia:</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id=dataInicioVigencia name="dataInicioVigencia" maxlength="10" value="${dataInicioVigencia}">
				<label class="rotuloEntreCampos" for="dataFinalVigencia">a</label>
				<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFinalVigencia" name="dataFinalVigencia" maxlength="10" value="${dataFinalVigencia}">
			</fieldset>
			<fieldset id="conteinerSegmentos" class="conteinerCampoList">
				<legend class="conteinerBlocoTitulo">Segmentos</legend>
				<label class="rotulo rotuloVertical rotuloCampoList" for="segmentosDisponiveis">Dispon�veis:</label>
				<select id="segmentosDisponiveis" name="segmentosDisponiveis" class="campoList campoVertical" multiple="multiple"
					onDblClick="moveSelectedOptionsEspecial(document.forms[0].segmentosDisponiveis,document.forms[0].segmentosSelecionados,true); atualizarListaRamoAtividade();">
				   	<c:forEach items="${segmentosDisponiveis}" var="segmentoDisp">
						<option value="<c:out value="${segmentoDisp.chavePrimaria}"/>"> <c:out value="${segmentoDisp.descricao}"/></option>		
				    </c:forEach>
			  	</select>
			  	<fieldset class="conteinerBotoesCampoList1a">
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" 
				   		onClick="moveSelectedOptionsEspecial(document.forms[0].segmentosDisponiveis,document.forms[0].segmentosSelecionados,true); atualizarListaRamoAtividade();">
				  	<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7"
				   		onClick="moveAllOptionsEspecial(document.forms[0].segmentosDisponiveis,document.forms[0].segmentosSelecionados,true); atualizarListaRamoAtividade();">
				  	<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
				   		onClick="moveSelectedOptionsEspecial(document.forms[0].segmentosSelecionados,document.forms[0].segmentosDisponiveis,true); atualizarListaRamoAtividade();">
				  	<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" 
				   		onClick="moveAllOptionsEspecial(document.forms[0].segmentosSelecionados,document.forms[0].segmentosDisponiveis,true); atualizarListaRamoAtividade();">
				</fieldset>
				<label class="rotulo rotuloVertical rotuloCampoList2" for="segmentosDisponiveis">Selecionados:</label><br />
			  	<select id="segmentosSelecionados" name="segmentosSelecionados" multiple="multiple" class="campoList campoList2 campoVertical"
			   		onDblClick="moveSelectedOptionsEspecial(document.forms[0].segmentosSelecionados,document.forms[0].segmentosDisponiveis,true); atualizarListaRamoAtividade();">
				   	<c:forEach items="${segmentosSelecionados}" var="segmentoSelec">
						<option value="<c:out value="${segmentoSelec.chavePrimaria}"/>"> <c:out value="${segmentoSelec.descricao}"/></option>		
				    </c:forEach>
			  	</select>
			</fieldset>
			
			<fieldset class="conteinerCampoList">
				<legend class="conteinerBlocoTitulo">Ramos de Atividade</legend>
				<label class="rotulo rotuloVertical rotuloCampoList" for="ramoAtividadeDisponiveis">Dispon�veis:</label>
				<select id="ramoAtividadeDisponiveis" name="ramoAtividadeDisponiveis" class="campoList campoVertical" multiple="multiple"
					onDblClick="moveSelectedOptions(document.forms[0].ramoAtividadeDisponiveis,document.forms[0].ramoAtividadeSelecionados,true)" style="width:400px;">
				   	<c:forEach items="${ramoAtividadeDisponiveis}" var="ramoAtividadeDisp">
						<option title="<c:out value="${ramoAtividadeDisp.descricao}"/>" value="<c:out value="${ramoAtividadeDisp.chavePrimaria}"/>"><c:out value="${ramoAtividadeDisp.descricao}"/></option>		
				    </c:forEach>
			  	</select>
			  	<fieldset class="conteinerBotoesCampoList1a">
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" 
				   		onClick="moveSelectedOptions(document.forms[0].ramoAtividadeDisponiveis,document.forms[0].ramoAtividadeSelecionados,true)">
				  	<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7"
				   		onClick="moveAllOptions(document.forms[0].ramoAtividadeDisponiveis,document.forms[0].ramoAtividadeSelecionados,true)">
				  	<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
				   		onClick="moveSelectedOptions(document.forms[0].ramoAtividadeSelecionados,document.forms[0].ramoAtividadeDisponiveis,true)">
				  	<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" 
				   		onClick="moveAllOptions(document.forms[0].ramoAtividadeSelecionados,document.forms[0].ramoAtividadeDisponiveis,true)">
				</fieldset>
				<label class="rotulo rotuloVertical" for="ramoAtividadeSelecionados">Selecionados:</label><br />
			  	<select id="ramoAtividadeSelecionados" name="ramoAtividadeSelecionados" multiple="multiple" class="campoList campoList2"
			   		onDblClick="moveSelectedOptions(document.forms[0].ramoAtividadeSelecionados,document.forms[0].ramoAtividadeDisponiveis,true)" style="width:400px;">
				   	<c:forEach items="${ramoAtividadeSelecionados}" var="ramoAtividadeSelec">
						<option title="<c:out value="${ramoAtividadeSelec.descricao}"/>" value="<c:out value="${ramoAtividadeSelec.chavePrimaria}"/>"><c:out value="${ramoAtividadeSelec.descricao}"/></option>		
				    </c:forEach>
			  	</select>
			</fieldset>
			
			<fieldset id="conteinerFormasCobranca" class="conteinerCampoList">
				<legend class="conteinerBlocoTitulo">Formas de Cobran�a</legend>
				<label class="rotulo rotuloVertical rotuloCampoList" for="formaCobrancaDisponiveis">Dispon�veis:</label>
				<select id="formaCobrancaDisponiveis" name="formaCobrancaDisponiveis" class="campoList campoVertical" multiple="multiple"
					onDblClick="moveSelectedOptions(document.forms[0].formaCobrancaDisponiveis,document.forms[0].formaCobrancaSelecionados,true)">
				   	<c:forEach items="${formaCobrancaDisponiveis}" var="tipoCobrancaDisp">
						<option value="<c:out value="${tipoCobrancaDisp.chavePrimaria}"/>"> <c:out value="${tipoCobrancaDisp.descricao}"/></option>		
				    </c:forEach>
			  	</select>
			  	<fieldset class="conteinerBotoesCampoList1a">
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" 
				   		onClick="moveSelectedOptions(document.forms[0].formaCobrancaDisponiveis,document.forms[0].formaCobrancaSelecionados,true)">
				  	<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7"
				   		onClick="moveAllOptions(document.forms[0].formaCobrancaDisponiveis,document.forms[0].formaCobrancaSelecionados,true)">
				  	<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
				   		onClick="moveSelectedOptions(document.forms[0].formaCobrancaSelecionados,document.forms[0].formaCobrancaDisponiveis,true)">
				  	<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" 
				   		onClick="moveAllOptions(document.forms[0].formaCobrancaSelecionados,document.forms[0].formaCobrancaDisponiveis,true)">
				</fieldset>
				<label class="rotulo rotuloVertical rotuloCampoList2" for="formaCobrancaSelecionados">Selecionados:</label><br />
			  	<select id="formaCobrancaSelecionados" name="formaCobrancaSelecionados" multiple="multiple" class="campoList campoList2"
			   		onDblClick="moveSelectedOptions(document.forms[0].formaCobrancaSelecionados,document.forms[0].formaCobrancaDisponiveis,true)">
				   	<c:forEach items="${formaCobrancaSelecionados}" var="tipoCobrancaSelec">
						<option value="<c:out value="${tipoCobrancaSelec.chavePrimaria}"/>"> <c:out value="${tipoCobrancaSelec.descricao}"/></option>		
				    </c:forEach>
			  	</select>
			</fieldset>
		</fieldset>
		
	<fieldset class="colunaFinal">
			<fieldset id="conteinerDiaVencimento" class="conteinerCampoList">
				<legend class="conteinerBlocoTitulo">Dia(s) de Vencimento</legend>
				<label class="rotulo rotuloVertical rotuloCampoList" for="dataVencimentoDisponiveis">Dispon�veis:</label>
				<select id="dataVencimentoDisponiveis" name="dataVencimentoDisponiveis" class="campoList campoVertical" multiple="multiple"
					onDblClick="moveSelectedOptionsNumber(document.forms[0].dataVencimentoDisponiveis,document.forms[0].dataVencimentoSelecionados,true)">
				   	<c:forEach items="${dataVencimentoDisponiveis}" var="i">
						<option value="<c:out value="${i}"/>">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${i}"/>
						</option>		
				    </c:forEach>
			  	</select>
			  	<fieldset class="conteinerBotoesCampoList1a">
					<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" 
				   		onClick="moveSelectedOptionsNumber(document.forms[0].dataVencimentoDisponiveis,document.forms[0].dataVencimentoSelecionados,true)">
				  	<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7"
				   		onClick="moveAllOptionsNumber(document.forms[0].dataVencimentoDisponiveis,document.forms[0].dataVencimentoSelecionados,true)">
				  	<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
				   		onClick="moveSelectedOptionsNumber(document.forms[0].dataVencimentoSelecionados,document.forms[0].dataVencimentoDisponiveis,true)">
				  	<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" 
				   		onClick="moveAllOptionsNumber(document.forms[0].dataVencimentoSelecionados,document.forms[0].dataVencimentoDisponiveis,true)">
				</fieldset>
				<label class="rotulo rotuloVertical rotuloCampoList2" for="dataVencimentoSelecionados">Selecionados:</label><br />
			  	<select id="dataVencimentoSelecionados" name="dataVencimentoSelecionados" multiple="multiple" class="campoList campoList2"
			   		onDblClick="moveSelectedOptionsNumber(document.forms[0].dataVencimentoSelecionados,document.forms[0].dataVencimentoDisponiveis,true)">
				   	<c:forEach items="${dataVencimentoSelecionados}" var="j">
						<option value="<c:out value="${j}"/>">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${j}"/>
						</option>		
				    </c:forEach>
			  	</select>
			</fieldset>
			
			<fieldset id="pesquisarImovel">
				<legend>Pesquisar Im�vel</legend>
				<div class="pesquisarImovelFundo">
					<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
					<input name="idImovel" type="hidden" id="idImovel" value="${idImovel}">
					<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${nomeFantasiaImovel}">
					<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${matriculaImovel}">
					<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${enderecoImovel}">
					<input name="condominio" type="hidden" id="condominio" value="${condominio}">
	
					<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
					<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
					<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${nomeFantasiaImovel}"><br />
					<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
					<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${matriculaImovel}"><br />
					<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">Endere�o:</label>
					<textarea name="enderecoImovelTexto" id="enderecoImovelTexto" class="campoDesabilitado" rows="2" cols="1" disabled="disabled">
						<c:out value="${enderecoImovel}"/>
					</textarea>

					
					<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
				</div>
			</fieldset>
			
			<label class="rotulo rotuloVertical" for="mensagem"><span class="campoObrigatorioSimbolo">* </span>Mensagem:</label>
			<textarea class="campoVertical" name="mensagem" id="mensagem" rows="6" 
			onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="1"/>', 'formatarCampoNome(event)');">
				${mensagem}
			</textarea>
		
		</fieldset>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Mensagens de Faturamento.</p>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar()">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar();">
	    <input id="botaoAlterar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="button" onclick="alterar();">
	</fieldset>
</form>