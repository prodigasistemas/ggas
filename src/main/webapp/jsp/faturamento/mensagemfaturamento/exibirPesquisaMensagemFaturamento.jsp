<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Mensagens de Faturamento<a class="linkHelp" href="<help:help>/consultadasmensagensdefaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" action="pesquisarMensagemFaturamento" name="mensagemFaturamentoForm" id="mensagemFaturamentoForm">

	<script language="javascript">
		$(document).ready(function(){	

			$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		});
		
		function removerMensagmFaturamento(){
			
			var selecao = verificarSelecao();
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('mensagemFaturamentoForm', 'removerMensagemFaturamento');
				}
		    }
		}
		
		function alterarMensagemFaturamento(){
		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('mensagemFaturamentoForm', 'exibirAlteracaoMensagemFaturamento');
		    }
		}
		
		function detalharMensagemFaturamento(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("mensagemFaturamentoForm", "exibirDetalhamentoMensagemFaturamento");
		}
		
		function incluir() {
			location.href = '<c:url value="exibirInclusaoMensagemFaturamento"/>';
		}		
		
		function limparFormulario(){
			document.getElementById('idTipoMensagem').value = '-1';
			document.getElementById('idSegmento').value = '-1';
			document.getElementById('dataInicioVigencia').value = '';
			document.getElementById('dataFinalVigencia').value = '';
			document.mensagemFaturamentoForm.habilitado[0].checked = true;
		}

		function removerMensagemFaturamento() {

			var selecao = verificarSelecao();
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('mensagemFaturamentoForm', 'removerMensagemFaturamento');
				}
		    }
		}

	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarMensagemFaturamento">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarMensagemFaturamento" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" for="idTipoMensagem">Tipo de Mensagem:</label>
			<select name="idTipoMensagem" id="idTipoMensagem" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaTiposMensagem}" var="tipoMensagem">
					<option value="<c:out value="${tipoMensagem.chavePrimaria}"/>" <c:if test="${mensagemFaturamentoVO.idTipoMensagem == tipoMensagem.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoMensagem.descricao}"/>
					</option>		
				</c:forEach>
			</select>
			<br />			
			<label class="rotulo" for="indicadorRegulamentada">Segmento:</label>
			<select name="idSegmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>
				<c:forEach items="${segmentosDisponiveis}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${mensagemFaturamentoVO.idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>		
				</c:forEach>
			</select>
		</fieldset>
		<fieldset class="colunaFinal">
			<label class="rotulo" for="dataInicioVigencia">Per�odo de Vig�ncia:</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id=dataInicioVigencia name="dataInicioVigencia" maxlength="10" value="${mensagemFaturamentoVO.dataInicioVigencia}">			
			<label class="rotuloEntreCampos" for="dataFinalVigencia">a</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFinalVigencia" name="dataFinalVigencia" maxlength="10" value="${mensagemFaturamentoVO.dataFinalVigencia}">
			<br />
	    	<label class="rotulo" id="rotuloHabilitado" for="habilitado1">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${mensagemFaturamentoVO.habilitado eq true}">checked</c:if>><label class="rotuloRadio" for="habilitado1">Ativo</label>
		   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${mensagemFaturamentoVO.habilitado eq false}">checked</c:if>><label class="rotuloRadio" for="habilitado2">Inativo</label>
		   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado3" value="" <c:if test="${empty mensagemFaturamentoVO.habilitado}">checked</c:if>><label class="rotuloRadio" for="habilitado3">Todos</label><br/><br />
		</fieldset>
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarMensagemFaturamento">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaMensagensFaturamento ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaMensagensFaturamento" sort="list" id="mensagemFaturamento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarMensagemFaturamento">
	        <display:column sortable="false" style="text-align: center; width: 25px" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input id="checkboxChavesPrimarias"type="checkbox" name="chavesPrimarias" value="${mensagemFaturamento.chavePrimaria}">
	        </display:column>
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${mensagemFaturamento.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	        
	        <display:column sortable="true" sortProperty="tipoMensagem.descricao" title="Tipo de Mensagem" style="width: 100px">
	        	<a href="javascript:detalharMensagemFaturamento('<c:out value="${mensagemFaturamento.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
	            	<c:out value="${mensagemFaturamento.tipoMensagem.descricao}"/>
	            </a>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="dataInicioVigencia" title="In�cio da Vig�ncia" style="width: 75px">
	        	<a href="javascript:detalharMensagemFaturamento('<c:out value="${mensagemFaturamento.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
	            	<c:out value="${mensagemFaturamento.dataInicioVigenciaFormatada}"/>
	            </a>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="dataFimVigencia" title="Final da Vig�ncia" style="width: 75px">
	        	<a href="javascript:detalharMensagemFaturamento('<c:out value="${mensagemFaturamento.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
	            	<c:out value="${mensagemFaturamento.dataFimVigenciaFormatada}"/>
	            </a>
	        </display:column>
	        
	        <display:column title="Segmento" style="width: 80px">
	        	<a href="javascript:detalharMensagemFaturamento('<c:out value="${mensagemFaturamento.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
	            	<c:if test="${mensagemFaturamento.listaNaoRepetidaSegmentos ne null}">
	            		<c:forEach var="segmento" items="${mensagemFaturamento.listaNaoRepetidaSegmentos}">
	            			<c:out value="${segmento.descricao}"/>
	            		</c:forEach>
	            	</c:if>
	            </a>
	        </display:column>
	        
	        <display:column title="Mensagem">
	        	<a href="javascript:detalharMensagemFaturamento('<c:out value="${mensagemFaturamento.chavePrimaria}"/>');"><span class="linkInvisivel"></span>
	            	<span class="colunaConteudoLargo" style="width: 480px"><c:out value="${mensagemFaturamento.descricao}"/></span>
	            </a>
	        </display:column>
	    </display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaMensagensFaturamento}">
  			<vacess:vacess param="exibirAlteracaoMensagemFaturamento">
  				<input id="botaoAlterar" name="buttonRemover" value="Alterar" class="bottonRightCol2 botaoAlterar" onclick="alterarMensagemFaturamento()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerMensagemFaturamento">
				<input id="botaoRemover" name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerMensagemFaturamento()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoMensagemFaturamento">
   			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>
	
</form>
