
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>



<h1 class="tituloInterno">Detalhar Mensagens de Faturamento<a class="linkHelp" href="<help:help>/detalhamentodemensagensdefaturamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form Id="detalhamentoMensagemFaturamento" name="mensagemFaturamentoForm" id="mensagemFaturamentoForm" method="post" action="exibirDetalhamentoMensagemFaturamento"> 

	<script>

		
	
		function voltar(){	
			location.href = '<c:url value="/exibirPesquisaMensagemFaturamento"/>';
		}
		
		function alterar(){
			document.forms[0].postBack.value = false;
			submeter('mensagemFaturamentoForm', 'exibirAlteracaoMensagemFaturamento');
		}		
		
	</script>
	
	<input type="hidden" name="acao" id="acao" value="exibirDetalhamentoMensagemFaturamento">
	<input type="hidden" name="postBack" id="postBack" value="true">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chavePrimaria}"/>
	<input type="hidden" name="versao" id="versao" value="${versao}">	
	
	<fieldset id="conteinerMensagemFaturamento" class="detalhamento">
		<fieldset class="coluna">
			<fieldset id="conteinerTipoMensagemVigencia">
				<label class="rotulo">Tipo de Mensagem:</label>
				<span class="itemDetalhamento"><c:out value="${descricaoTipoMensagem}"/></span><br />
				<label class="rotulo">Per�odo de Vig�ncia:</label>
				<span class="itemDetalhamento"><c:out value="${dataInicioVigencia}"/></span>
				<label class="rotuloEntreCampos">a</label>
				<span class="itemDetalhamento"><c:out value="${dataFinalVigencia}"/></span>
			</fieldset>
			
			<fieldset>
				<legend>Segmentos e Ramos de Atividade</legend>
				<c:set var="x" value="0" />
				<display:table class="dataTableGGAS" name="listaSegmentoRamoAtividade" id="segmentoRamoAtividade" sort="list" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="">
					<display:column media="html" title="Segmento">
						<c:out value='${segmentoRamoAtividade.descricao}'/>
					</display:column>
					<display:column media="html" title="Ramo Atividade">
						<c:out value='${segmentoRamoAtividade.descricaoAbreviada}'/>
					</display:column>
					<c:set var="x" value="${x + 1}" />
				</display:table>
			</fieldset>
					</br>
			<fieldset id="conteinerFormasCobranca" class="conteinerCampoList">
				<legend class="conteinerBlocoTitulo">Formas de Cobran�a</legend>
				<c:choose>
			    	<c:when test="${not empty formaCobrancaSelecionados}">
		    		<ul class="detalhamentoLista">
		    		<c:forEach items="${formaCobrancaSelecionados}" var="mensagemCobranca">
				   		<li class="itemDetalhamentoLista"><c:out value="${mensagemCobranca.formaCobranca.descricao}"/></li>
					</c:forEach>
				    </ul>
			    	</c:when>
			    	<c:otherwise>
			    		<label class="rotuloInformativo">N�o h� Formas de Cobran�a selecionadas</label>
			    	</c:otherwise>
			    </c:choose>
				
			</fieldset>
		</fieldset>
		
		<fieldset class="colunaFinal">
			<fieldset id="conteinerDiaVencimento" class="conteinerCampoList">
				<legend class="conteinerBlocoTitulo">Dia(s) de Vencimento</legend>
			   		<c:if test="${dataVencimentoSelecionados ne null}">
				   		<c:forEach items="${dataVencimentoSelecionados}" var="mensagemVencimento">
				   			<span class="itemDetalhamentoLista"><c:out value="${mensagemVencimento.diaVencimento}"/></span>
					    </c:forEach> 
				    </c:if>
			</fieldset>
			
			<fieldset id="pesquisarImovel">
				<legend>Im�vel</legend>
				<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
					<label class="rotulo">Descri��o:</label>
					<span class="itemDetalhamento itemDetalhamentoPequeno2"><c:out	value="${nomeFantasiaImovel}" /></span><br />
					<label class="rotulo">Matr�cula:</label>
					<span class="itemDetalhamento itemDetalhamentoPequeno2"><c:out value="${matriculaImovel}" /></span><br />
					<label class="rotulo">Endere�o:</label> 
					<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${enderecoImovel}" /></span><br />
				</fieldset>
			</fieldset>
						
			<label class="rotulo rotuloVertical" for="mensagem">Mensagem:</label>
			<span class="itemDetalhamento itemDetalhamentoLargo2"><c:out value="${mensagem}"/><br /></span>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="voltar();">
		<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">	       
	</fieldset>

</form> 