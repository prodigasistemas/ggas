<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>

	$(document).ready(function(){

		iniciarDatatable('#listaPontoConsumo', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});
		
		$("input[name=dataVencimento]").datepicker({
			changeYear: true, 
			//maxDate: '+0d', 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});
	});
</script>
			<div class="row">
				<div class="col-md-6">
					<ul class="list-group">
						<li class="list-group-item">Cliente: <span
							class="font-weight-bold"> <c:out value="${cliente.nome}"/></span>
						</li>
						<li class="list-group-item">Contrato: <span
							class="font-weight-bold"><c:out value="${faturaForm.numeroContrato}"/></span>
						</li>
						<li class="list-group-item">N�mero de S�rie: <span
							class="font-weight-bold"> <c:out value="${faturaForm.numeroSerie}"/></span>
						</li>						
					</ul>
				</div>

				<div class="col-md-6">
					<ul class="list-group">
						<li class="list-group-item">Tipo de Nota: <span
							class="font-weight-bold"> <c:out value="${faturaForm.tipoNota}"/></span>
						</li>
						<li class="list-group-item">Chave de Acesso: <span
							class="font-weight-bold"><c:out value="${faturaForm.chaveAcesso}"/></span>
						</li>
					</ul>
				</div>
			</div>
			
			<hr>
			
			<c:if test="${listaPontoConsumo ne null && (faturaForm.idImovel ne null || faturaForm.idImovel ne 0)}">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover"
						name="listaPontoConsumo" id="listaPontoConsumo" width="100%">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">Im�vel Ponto Consumo</th>
								<th scope="col" class="text-center">Segmento</th>
								<th scope="col" class="text-center">N� Medidor</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaPontoConsumo}"	var="pontoConsumo">
								<tr>
									<td class="text-center">
								   		<input type="hidden" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"/>
								   		
								   		<c:choose>
								   			 <c:when test="${nomeImovel ne null}">
								   			 	<c:out value="${nomeImovel}"/> - <c:out value="${pontoConsumo.descricao}"/>
								   			 </c:when>
								   			 
								   			 <c:otherwise>
								   			 	<c:choose>
								   			 		<c:when test="${pontoConsumo.imovel.nome ne null && fn:trim(pontoConsumo.imovel.nome) ne ''}">
														<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>
													</c:when>
													<c:otherwise>
														<c:out value="${pontoConsumo.descricao}"/>
													</c:otherwise>
												</c:choose>							
								   			 </c:otherwise>
								   		</c:choose>								
									</td>
									<td class="text-center">
										<c:out value="${pontoConsumo.segmento.descricao}"/>								
									</td>
									<td class="text-center">
			                            <c:if test="${pontoConsumo.instalacaoMedidor ne null && pontoConsumo.instalacaoMedidor.medidor ne null}">
											<c:out value="${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}"/>
										</c:if>								
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:if>

