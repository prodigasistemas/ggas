<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<div class="card">
	<div class="card-header">
		<h5 class="card-title">
			Refaturar Fatura
		</h5>
	</div>
	<div class="card-body">
		<div class="alert alert-primary fade show" role="alert">
			<i class="fa fa-exclamation-circle"></i> Para refaturar uma Fatura clique em <span class="destaqueOrientacaoInicial">Avan�ar</span>
		</div>


<script type="text/javascript">

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=0');

	function cancelar() {
		submeter('faturaForm','pesquisarFatura');
	}

	function consistir() {
		// Setando a data dos campos de data de leitura nos campos hidden, caso o usu�rio selecione a data pelo calend�rio.
		var datasLeituraAtual = $("input[name=dataLeituraAtual]");
		$("input[name=dataLeitura]").each(function(indice, elem){
			datasLeituraAtual[indice].value = elem.value;
		});		
		submeter('faturaForm','consistirLeituraRefaturar');
	}
	
	$(document).ready(function(){

		iniciarDatatable('#pontoConsumo', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});
	});

	function alterarMotivoRefaturamento(){
		$("#descricaoMotivoInclusao").val($('#idMotivoRefaturamento').find(":selected").text().trim());
	}

</script>

<form:form method="post" action="exibirResumoRefaturarFatura" name="faturaForm">
	<input name="acao" type="hidden" id="acao" value="exibirResumoRefaturarFatura">
	<input name="indexLista" type="hidden" id="indexLista" value="-1">
	<input name="tela" type="hidden" id="tela" value="exibirRefaturar">
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}" />
	<input name="idFatura" type="hidden" id="idFatura" value="${faturaForm.idFatura}" />
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="indicadorRefuramento" type="hidden" id="indicadorRefuramento" value="${indicadorRefuramento}"/>
	<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaLista">
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias${chavePrimariaLista}" value="${chavePrimariaLista}">
	</c:forEach>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${faturaForm.numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${faturaForm.numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${faturaForm.codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
	<input name="numeroCiclo" type="hidden" id="numeroCiclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
	<input name="anoMesReferencia" type="hidden" id="anoMesReferencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
	<input name="numeroNotaFiscalInicial" type="hidden" id="numeroNotaFiscalInicial" value="<c:out value="${faturaForm.numeroNotaFiscalInicial}"/>">
	<input name="numeroNotaFiscalFinal" type="hidden" id="numeroNotaFiscalFinal" value="<c:out value="${faturaForm.numeroNotaFiscalFinal}"/>">
	<input name="tipoFatura" type="hidden" id="tipoFatura" value="<c:out value="${faturaForm.tipoFatura}"/>">
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="indicadorComplementar" type="hidden" id="indicadorComplementar" value="<c:out value="${faturaForm.indicadorComplementar}"/>">
	<input name="tipoDocumento" type="hidden" id="chavePrimaria" value="1">
	<input name="idSituacaoPagamento" type="hidden" id="idSituacaoPagamento" value="<c:out value="${faturaForm.idSituacaoPagamento}"/>">
	<input name="idPontoConsumo" type="hidden"  id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	<input name="indicadorPesquisaPontos" type="hidden" id="indicadorPesquisaPontos" value="${faturaForm.indicadorPesquisaPontos}">
	<input type="hidden" name="descricaoMotivoInclusao" id="descricaoMotivoInclusao"/>
	
	
		<c:if test="${fatura.idCliente ne null && fatura.idCliente > 0}">
			<a id="linkDadosCliente" class="linkPesquisaAvancada linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Cliente:</label>
					<input name="nomeCompletoCliente" type="hidden" value="${cliente.nome}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
					<label class="rotulo">CPF/CNPJ:</label>
					<input name="documentoFormatado" type="hidden" value="${cliente.numeroDocumentoFormatado}">
					<span class="itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Endere�o:</label>
					<input name="enderecoFormatadoCliente" type="hidden" value="${cliente.enderecoFormatadoCliente}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${fatura.enderecoFormatadoCliente}"/></span><br />
					<label class="rotulo">E-mail:</label>
					<input name="emailCliente" type="hidden" value="${cliente.emailPrincipal}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
				</fieldset>
			</fieldset>
			<label class="rotulo rotuloHorizontal">M�s/Ano:</label>
			<span class="itemDetalhamento"><c:out value="${faturaForm.anoMesFormatado}"/></span><br class="quebra2Linhas"/>
		</c:if>
			
		<c:if test="${listaPontoConsumo ne null}">
			<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			
			<div class="table-responsive" id="dadosPontoConsumo">
				<table class="table table-bordered table-striped table-hover" id="pontoConsumo" width="100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" class="text-center">Descricao</th>
							<th scope="col" class="text-center">Endere�o</th>
							<th scope="col" class="text-center">CEP</th>
							<th scope="col" class="text-center">Complemento</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
							<tr>
								<td class="text-center"><c:out value="${pontoConsumo.descricao}"/></td>
								<td class="text-center"><c:out value="${pontoConsumo.enderecoFormatado}"/></td>
								<td class="text-center"><c:out value="${pontoConsumo.cep.cep}"/></td>
								<td class="text-center"><c:out value="${pontoConsumo.descricaoComplemento}"/></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			
			</br>
			
			<div class="row mb-2">
				<div class="col-md-6">
					<div class="form-row">
						<div class="col-md-12">
							<label for="mesAnoCiclo" class="rotulo">M�s/Ano-Ciclo:</label>
							<span id="mesAnoCiclo" class="itemDetalhamento valorMesAnoCiclo"><c:out value="${faturaForm.anoMesFormatado}"/>-<c:out value="${faturaForm.numeroCiclo}"/></span>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12">
							<label for="idMotivoRefaturamento" class="rotulo rotuloHorizontal campoObrigatorio">Motivo de Refaturamento:<span class="text-danger">* </span></label>
							<div class="input-group input-group-sm">
							<select name="idMotivoRefaturamento" id="idMotivoRefaturamento" class="form-control"
							onchange="alterarMotivoRefaturamento();">
							   	<option value="-1">Selecione</option>				
								<c:forEach items="${listaMotivoRefaturar}" var="motivoRefaturar">
									<option value="<c:out value='${motivoRefaturar.chavePrimaria}'/>" <c:if test="${faturaForm.idMotivoRefaturamento == motivoRefaturar.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${motivoRefaturar.descricao}"/>
									</option>		
								</c:forEach>
							</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>									
</c:if>



			<div class="card mt-3">
				<div class="card-header">
					<ul class="nav nav-tabs card-header-tabs" id="menuRefaturarAbas"
						role="selecaoTabs">
						<c:if test="${(itensFatura ne null && not empty itensFatura)}">
							<li class="nav-item"><a class="nav-link active"
								id="geralFatura-tab" data-toggle="tab" href="#geralFatura" role="tab"
								aria-controls="geralFatura"><i class="fa fa-coins"></i><strong>
										Geral</strong> <span class="text-danger">*</span></a></li>
						</c:if>
						<c:if test="${(itensFatura ne null && not empty itensFatura)}">
							<li class="nav-item"><a class="nav-link" id="leituraConsumoFatura-tab"
								data-toggle="tab" href="#leituraConsumoFatura" role="tab"
								aria-controls="leituraConsumoFatura"><i class="fas fa-tags"></i><strong>
										Leitura e Consumo</strong> <span class="text-danger">*</span></a></li>
						</c:if>
						<c:if test="${listaCreditoDebito ne null && not empty listaCreditoDebito}">
							<li class="nav-item"><a class="nav-link" id="debitosCreditosFatura-tab"
								data-toggle="tab" href="#debitosCreditosFatura" role="tab"
								aria-controls="debitosCreditosFatura"><i class="fas fa-money-bill-wave"></i><strong>
										D�bitos e Cr�ditos </strong><span class="text-danger">*</span></a></li>
						</c:if>
						<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes}">
							<li class="nav-item"><a class="nav-link" id="dadosFaturasPendentes-tab"
								data-toggle="tab" href="#dadosFaturasPendentes" role="tab"
								aria-controls="dadosFaturasPendentes"><i class="fas fa-wallet"></i><strong>
										Faturas N�o Cobradas </strong><span class="text-danger">*</span></a></li>
						</c:if>						
					</ul>
				</div>

				<div class="card-body">
					<div class="tab-content">
						<c:if test="${(itensFatura ne null && not empty itensFatura)}">
							<div class="tab-pane fade active show" id="geralFatura"
								role="tabpanel" aria-labelledby="geralFatura-tab">
								<jsp:include page="/jsp/faturamento/fatura/abaGeralRefaturarFatura.jsp"/>
							</div>
						</c:if>
						<c:if test="${(itensFatura ne null && not empty itensFatura)}">
							<div class="tab-pane fade" id="leituraConsumoFatura" role="tabpanel"
								aria-labelledby="leituraConsumoFatura-tab">
								<jsp:include page="/jsp/faturamento/fatura/abaLeituraConsumoFatura.jsp"/>
							</div>
						</c:if>
						<c:if
							test="${listaDadosGerais ne null && not empty listaDadosGerais}">
							<div class="tab-pane fade" id="debitosCreditosFatura" role="tabpanel"
								aria-labelledby="debitosCreditosFatura-tab">
								<jsp:include page="/jsp/faturamento/fatura/abaDebitosCreditosFatura.jsp"/>
							</div>
						</c:if>
						<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes && false}">
							<div class="tab-pane fade" id="dadosFaturasPendentes" role="tabpanel"
								aria-labelledby="dadosFaturasPendentes-tab">
								<jsp:include page="/jsp/faturamento/fatura/abaFaturasPendentes.jsp"/>
							</div>
						</c:if>						
					</div>
				</div>
			</div>
	
	<div class="card-footer">
		<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
					type="button" onclick="cancelar();">
					<i class="fa fa-times"></i> Cancelar
				</button>
				<button id="buttonSalvar"
					class="btn btn-sm btn-success float-right ml-1 mt-1"
					type="submit" name="buttonIncluir" id="botaoIncluir">
					<i class="fa fa-save"></i> Avan�ar
				</button>
			</div>
		</div>
	</div>
	
</form:form>

	</div>



	<!-- div class="body" -->
</div> <!-- div class="card" -->

