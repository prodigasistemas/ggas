<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>

	function init() {
		<c:forEach items="${faturaForm.chavesFaturasPendentes}" var="chaveFaturaPendente">
			if(document.getElementById('chaveFaturaPendente'+'<c:out value="${chaveFaturaPendente}"/>') != undefined){
				document.getElementById('chaveFaturaPendente'+'<c:out value="${chaveFaturaPendente}"/>').checked = true;	
			}	
		</c:forEach>
	}

	addLoadEvent(init);
	
	$(document).ready(function(){
		$('#faturasPendentes').dataTable( {
			"paging": false,
			"language": {
	            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		});
	});

</script>

<div class="card">
	<div class="card-body">
		<h5 class="card-title">Faturas Pendentes</h5>
		<div class="alert alert-primary" role="alert">
			<i class="fa fa-exclamation-circle"></i> As faturas abaixo n�o foram emitidas, pois n�o atingiram o valor m�nimo para gera��o do documento de cobran�a.
		</div>
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover" id="faturasPendentes" width="100%">
				<thead class="thead-ggas-bootstrap">
					<tr>
						<th scope="col" class="text-center"></th>
						<th scope="col" class="text-center">N�mero do Documento</th>
						<th scope="col" class="text-center">Data de Emiss�o</th>
						<th scope="col" class="text-center">Data de Vencimento</th>
						<th scope="col" class="text-center">Ciclo / Refer�ncia</th>
						<th scope="col" class="text-center">Valor Total (R$)</th>
						<th scope="col" class="text-center">Situa��o</th>
						<th scope="col" class="text-center">Pagamento</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listaFaturasPendentes}" var="fatura">
						<tr>
							<td>
								<div class="form-check">
									<input type="checkbox" class="form-check-input" name="chavesFaturasPendentes" id="chaveFaturaPendente${fatura.chavePrimaria}" value="${fatura.chavePrimaria}" />
								</div>
							</td>
							<td><c:out value="${fatura.chavePrimaria}"/></td>
							<td><fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy"/></td>
							<td><fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/></td>
							<td>
								<c:if test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
									<c:out value="${fatura.cicloReferenciaFormatado}"/>
								</c:if>
							</td>
							<td><fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/></td>
							<td><c:out value="${fatura.creditoDebitoSituacao.descricao}"/></td>
							<td><c:out value="${fatura.situacaoPagamento.descricao}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

