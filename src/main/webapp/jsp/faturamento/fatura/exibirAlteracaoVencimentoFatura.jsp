<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
	
<script type="text/javascript">

	$(document).ready(function(){
		iniciarDatatable('#fatura', {
			searching: false,
			info:false,
			paging: false
		});

		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
								    autoclose: true,
									format: 'dd/mm/yyyy',
									orientation: "bottom",
									language: 'pt-BR'
								});
			
		
		$('input[name$="dataVencimento"]').inputmask("99/99/9999",{placeholder:"_"});
		// Dialog			
		$("#pontoConsumoPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
	});

	function cancelar(){
		submeter('faturaForm','pesquisarFatura');
	}
	
	function alterarVencimentoFatura() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('faturaForm','alterarVencimentoFatura');
		}
	}

	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
	}

	function exibirDadosPontoConsumo(idPontoConsumo) {
		
		var descricao = '';
		var endereco = '';
		var cep = '';
		var complemento = '';
		
		if(idPontoConsumo != "" && parseInt(idPontoConsumo) > 0){
	       	AjaxService.obterPontoConsumoPorChave( idPontoConsumo, { 
	           	callback: function(pontoConsumo) {
					descricao = pontoConsumo['descricao'];
					endereco = pontoConsumo['endereco'];
					cep = pontoConsumo['cep'];
					complemento = pontoConsumo['complemento'];
	       	  }
	       	 , async: false}
	       	);

	       	if (descricao != null) {
			document.getElementById('descricaoPopup').innerHTML = descricao;
	       	}
			if (endereco != null) {
			document.getElementById('enderecoPopup').innerHTML = endereco;
			}
			if (cep != null) {
			document.getElementById('cepPopup').innerHTML = cep;
			}
			if (complemento != null) {
			document.getElementById('complementoPopup').innerHTML = complemento;
			}
	
			exibirJDialog("#pontoConsumoPopup");
		} else {
			alert("Cr�dito ou D�bito selecionado n�o possui ponto de consumo.");
		}
		
	}

	function manterCheckBoxDasTabelas(valor){	
		<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaFatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${chavePrimariaFatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${chavePrimariaFatura}"/>').checked = valor;
			}	
		</c:forEach>
	}

	function desabilitaDataVencimento(valor){
		if(valor.value == 'false'){
			document.getElementById("dataVencimento").disabled = true;
			document.getElementById("motivoAlteracao").disabled = true;
			$("#dataVencimento")
					.datepicker( "option", "disabled", true );
			document.getElementById("dataVencimento").value = '';
			document.getElementById("motivoAlteracao").value = '';
		}else{
			document.getElementById("dataVencimento").disabled = false;
			document.getElementById("motivoAlteracao").disabled = false;
			$("#dataVencimento")
					.datepicker( "option", "disabled", false );
		}
	}

	function init() {
		manterCheckBoxDasTabelas(true);
	}
	
	addLoadEvent(init);	

</script>

<div class="responsivo">
	<form:form method="post" action="alterarVencimentoFatura" name="faturaForm">
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
		<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
		<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
		<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${faturaForm.numeroDocumentoInicial}"/>">
		<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${faturaForm.numeroDocumentoFinal}"/>">
		<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${faturaForm.codigoCliente}"/>">
		<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
		<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
		<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
		<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
		<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
		<input name="numeroCiclo" type="hidden" id="numeroCiclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
		<input name="anoMesReferencia" type="hidden" id="anoMesReferencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
		<input name="numeroNotaFiscalInicial" type="hidden" id="numeroNotaFiscalInicial" value="<c:out value="${faturaForm.numeroNotaFiscalInicial}"/>">
		<input name="numeroNotaFiscalFinal" type="hidden" id="numeroNotaFiscalFinal" value="<c:out value="${faturaForm.numeroNotaFiscalFinal}"/>">
		<input name="tipoFatura" type="hidden" id="tipoFatura" value="<c:out value="${faturaForm.tipoFatura}"/>">
		<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
		<input name="indicadorComplementar" type="hidden" id="indicadorComplementar" value="<c:out value="${faturaForm.indicadorComplementar}"/>">
		<input name="tipoDocumento" type="hidden" id="chavePrimaria" value="1">
		<input name="idSituacaoPagamento" type="hidden" id="idSituacaoPagamento" value="<c:out value="${faturaForm.idSituacaoPagamento}"/>">
		<input name="idPontoConsumo" type="hidden"  id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
		<input name="indicadorPesquisaPontos" type="hidden" id="indicadorPesquisaPontos" value="${faturaForm.indicadorPesquisaPontos}">
		
		<token:token></token:token>
	
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Prorrogar Data de Vencimento da Fatura</h5>
			</div>
			<div class="card-body bg-light">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Selecione o <b>motivo</b>, a
					<b>nova data de vencimento</b>, se <b>incidem juros/multa</b> por
					atraso de pagamento da Fatura e clique em <b>Alterar</b>.
				</div>
				<div id="pontoConsumoPopup" title="Ponto de Consumo">
					<label class="rotulo">Descri��o:</label> <span
						class="itemDetalhamento itemDetalhamentoMedioPequeno"
						id="descricaoPopup"></span><br /> <label class="rotulo">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoMedioPequeno"
						id="enderecoPopup"></span><br /> <label class="rotulo">Cep:</label>
					<span class="itemDetalhamento" id="cepPopup"></span><br /> <label
						class="rotulo">Complemento:</label> <span
						class="itemDetalhamento itemDetalhamentoMedioPequeno"
						id="complementoPopup"></span><br />
					<br />
				</div>

				<div class="card">
					<div class="card-body bg-light">
						<div class="row mb-2">
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="motivoAlteracao">Motivo:<span
											class="text-danger">* </span></label>
										<div class="input-group input-group-sm">
											<input type="text" aria-label="motivoAlteracao"
												id="motivoAlteracao" class="form-control form-control-sm"
												name="motivoAlteracao" maxlength="200" size="60"
												value="${faturaForm.motivoAlteracao}">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="dataVencimento">Data de Vencimento: <span
											class="text-danger">* </span></label>
										<div class="input-group input-group-sm">
											<input type="text" aria-label="dataVencimento"
												id="dataVencimento"
												class="form-control form-control-sm bootstrapDP"
												name="dataVencimento" maxlength="10"
												value="${faturaForm.dataVencimento}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="col-md-6">
								<div class="col-md-12">
									<label for="incidemJurosMultaSim">Prorrogar data de
										vencimento: </label>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="incidemJurosMultaSim"
											name="incidemJurosMulta"
											onclick="desabilitaDataVencimento(this)"
											class="custom-control-input" value="true"
											<c:if test="${faturaForm.incidemJurosMulta eq true or empty faturaForm.incidemJurosMulta}">checked</c:if>>
										<label class="custom-control-label" for="incidemJurosMultaSim">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="incidemJurosMultaNao"
											name="incidemJurosMulta"
											onclick="desabilitaDataVencimento(this)"
											class="custom-control-input" value="false"
											<c:if test="${faturaForm.incidemJurosMulta eq false}">checked</c:if>>
										<label class="custom-control-label" for="incidemJurosMultaNao">N�o</label>
									</div>
								</div>
							</div>
						</div>

						</br>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover"
								name="listaFatura" id="fatura" width="100%" style="opacity: 0;">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center"></th>
										<th scope="col" class="text-center">N�mero do Documento</th>
										<th scope="col" class="text-center">Data de Emiss�o</th>
										<th scope="col" class="text-center">Data de Vencimento</th>
										<th scope="col" class="text-center">Ciclo/Refer�ncia</th>
										<th scope="col" class="text-center">Valor (R$)</th>
										<th scope="col" class="text-center">Situa��o</th>
										<th scope="col" class="text-center">Pagamento</th>
										<th scope="col" class="text-center">Revis�o</th>
										<th scope="col" class="text-center">Data de Vencimento da
											Revis�o</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaFatura}" var="fatura">
										<tr>
											<td>
												<div
													class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
													data-identificador-check="chk${fatura.chavePrimaria}">
													<input id="chaveFatura${fatura.chavePrimaria}"
														type="checkbox" name="chavesPrimarias"
														class="custom-control-input"
														value="${fatura.chavePrimaria}"> <label
														class="custom-control-label p-0"
														for="chaveFatura${fatura.chavePrimaria}"></label>
												</div>
											</td>
											<td class="text-center"><c:out
													value="${fatura.chavePrimaria}" /></td>
											<td class="text-center"><fmt:formatDate
													value="${fatura.dataEmissao}" pattern="dd/MM/yyyy" /></td>
											<td class="text-center"><fmt:formatDate
													value="${fatura.dataVencimento}" pattern="dd/MM/yyyy" /></td>
											<td class="text-center"><c:if
													test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
													<c:out value="${fatura.cicloReferenciaFormatado}" />
												</c:if></td>
											<td class="text-center"><fmt:formatNumber
													value="${fatura.valorTotal}" minFractionDigits="2" /></td>
											<td class="text-center"><c:out
													value="${fatura.creditoDebitoSituacao.descricao}" /></td>
											<td class="text-center"><c:out
													value="${fatura.situacaoPagamento.descricao}" /></td>
											<td class="text-center"><c:out
													value="${fatura.situacaoPagamento.descricao}" /> <span
												title='<c:out value="${fatura.motivoRevisao.descricao}"/>'>
													<c:out value="${fatura.motivoRevisao.chavePrimaria}" />
											</span></td>
											<td class="text-center"><c:if
													test="${fatura.dataRevisao ne null}">
													<fmt:formatDate value="${fatura.dataRevisao}"
														pattern="dd/MM/yyyy" />
												</c:if></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-12">
						<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
							type="button" onclick="cancelar();">
							<i class="fa fa-times"></i> Cancelar
						</button>
						<button id="buttonSalvar" value="Salvar"
							onclick="alterarVencimentoFatura();"
							class="btn btn-sm btn-success float-right ml-1 mt-1"
							type="button" data-toggle="modal" data-target="#atencao">
							<i class="fas fa-save"></i> Alterar
						</button>
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>
