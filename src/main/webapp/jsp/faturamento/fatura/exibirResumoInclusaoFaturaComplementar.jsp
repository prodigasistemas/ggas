<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Resumo da Fatura<a class="linkHelp" href="<help:help>/resumodefatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	
<script type="text/javascript">

	$(document).ready(function(){
		// Dialog - configura��es
		$("#incluirFaturaLeituraPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
		//Faz a rolagem de tela at� a parte superior
		$.scrollTo($("#mainContainer"),0);
	});

	<c:if test="${listaDadosInclusaoFatura ne null}">
		<c:set var="count" value="1"/>
		<c:forEach items="${listaDadosInclusaoFatura}">
			animatedcollapse.addDiv('conteinerDadosFatura${count}', 'fade=0,speed=400,hide=1');
			<c:set var="count" value="${count+1}"/>
		</c:forEach>
	</c:if>

	function exibirFatura(elem, valor){
		if(elem.checked){
			animatedcollapse.show('conteinerDadosFatura' + valor);
		}else{
			animatedcollapse.hide('conteinerDadosFatura' + valor);
		}
	}
	
	function voltar() {
		submeter('faturaForm', 'exibirResumoInclusaoFaturaComplementar');
	}

	function salvar() {				
		submeter('faturaForm', 'incluirFaturaComplementar');
	}
	
	
	function exibirDadosLeituraAnterior(leituraAnterior, dataLeituraAnterior, anormalidadeAnterior, volumeAnterior, consumoFaturado, diasConsumo) {
		$("#leituraAnteriorPopup").html(leituraAnterior);
		$("#dataLeituraAnteriorPopup").html(dataLeituraAnterior);
		$("#anormalidadeAnteriorPopup").html(anormalidadeAnterior);
		$("#volumeAnteriorPopup").html(volumeAnterior);
		$("#consumoFaturadoPopup").html(consumoFaturado);
		$("#diasConsumoPopup").html(diasConsumo);
		exibirJDialog("#incluirFaturaLeituraPopup");
	}
	
	<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
		animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=1');
	</c:if>
	<c:if test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
		animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=1');
	</c:if>
	<c:if test="${listaPontoConsumo ne null && (faturaForm.idCliente eq null || faturaForm.idCliente eq 0)}">
		animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=1');	
	</c:if>
	
</script>

<form:form method="post" action="incluirFaturaComplementar">
	<input name="acao" type="hidden" id="acao" value="incluirFaturaComplementar">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idCliente" type="hidden" id="idCliente" value="${faturaForm.idCliente}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	<input name="idContratoEncerrarRescindir" type="hidden" id="idContratoEncerrarRescindir" value="${faturaForm.idContratoEncerrarRescindir}">	
	<input name="listaIdsPontoConsumoRemovidosAgrupados" type="hidden" id="listaIdsPontoConsumoRemovidosAgrupados" value="${faturaForm.listaIdsPontoConsumoRemovidosAgrupados}">
	
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${idImovel}"/>">
	
	<input name="referencia" type="hidden" id="referencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
	<input name="ciclo" type="hidden" id="ciclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
	
	
	<div id="incluirFaturaLeituraPopup" title="Leitura">
		<label class="rotulo">Leitura Anterior:</label>
		<span class="itemDetalhamento" id="leituraAnteriorPopup"></span><br />
		<label class="rotulo">Data da Leitura Anterior:</label>
		<span class="itemDetalhamento" id="dataLeituraAnteriorPopup"></span><br />
		<label class="rotulo">Anormalidade da Leitura:</label>
		<span class="itemDetalhamento" id="anormalidadeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Volume Anterior Apurado:</label>
		<span class="itemDetalhamento" id="volumeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Consumo Faturado:</label>
		<span class="itemDetalhamento" id="consumoFaturadoPopup"></span><br /><br />
		<label class="rotulo">Dias de Consumo:</label>
		<span class="itemDetalhamento" id="diasConsumoPopup"></span><br /><br />
	</div>
	
	<c:forEach items="${faturaForm.chavesCreditoDebito}" var="chaveCreditoDebito">
		<input name="chavesCreditoDebito" type="hidden" id="chavesCreditoDebito" value="${chaveCreditoDebito}">
	</c:forEach>
	
		<fieldset id="resumoFatura" class="conteinerPesquisarIncluirAlterar">
			<c:if test="${faturaForm.map.idCliente ne null && faturaForm.map.idCliente > 0}">
				<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
					<fieldset class="coluna detalhamentoColunaLarga">
						<label class="rotulo">Cliente:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${faturaForm.map.nomeCompletoCliente}"/></span><br />
						<label class="rotulo">CPF/CNPJ:</label>
						<span class="itemDetalhamento"><c:out value="${faturaForm.numeroDocumentoFormatado}"/></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${faturaForm.enderecoFormatadoCliente}"/></span><br />
						<label class="rotulo">E-mail:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${faturaForm.emailCliente}"/></span><br />
					</fieldset>
				</fieldset><br/>
			</c:if>
			
			<c:if test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
				<a id="linkDadosImovel" class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosImovel" class="conteinerDados conteinerBloco">
					<fieldset class="coluna detalhamentoColunaLarga">
						<label class="rotulo">Descri��o:</label>
						<span class="itemDetalhamento"><c:out value="${faturaForm.nomeFantasiaImovel}"/></span><br />
						<label class="rotulo">Matr�cula:</label>
						<span class="itemDetalhamento"><c:out value="${faturaForm.matriculaImovel}"/></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">Cidade:</label>
						<span class="itemDetalhamento"><c:out value="${faturaForm.cidadeImovel}"/></span><br />
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento"><c:out value="${faturaForm.enderecoImovel}"/></span><br />
					</fieldset>
				</fieldset><br/>
			</c:if>
			
			<c:if test="${listaPontoConsumo ne null && (faturaForm.idCliente eq null || faturaForm.idCliente eq 0)}">
				<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosPontoConsumo" class="conteinerBloco">
					<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
					   	<display:column title="Im�vel - Ponto Consumo" style="width: 330px">
					   		<input type="hidden" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"/>
					   		
					   		<c:choose>
					   			 <c:when test="${nomeImovel ne null}">
					   			 	<c:out value="${nomeImovel}"/> - <c:out value="${pontoConsumo.descricao}"/>
					   			 </c:when>
					   			 <c:otherwise>
									<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>							
					   			 </c:otherwise>
					   		</c:choose>
					   	</display:column>
					   	<display:column property="segmento.descricao" title="Segmento"/>    
					   	<display:column property="situacaoConsumo.descricao" title="Situa��o"/>
					   	<display:column property="instalacaoMedidor.medidor.numeroSerie" title="N� Medidor"/>
				    </display:table>	
				</fieldset>
			</c:if>
			
			<br class="quebraLinha" />
		    
		    
			<br class="quebraLinha" />
			
			<c:if test="${listaDadosLeituraConsumoFatura ne null && not empty(listaDadosLeituraConsumoFatura)}">
			<fieldset class="conteinerBloco">
				<legend>Leitura Consumo:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" name="${listaDadosLeituraConsumoFatura}" sort="list" id="leituraConsumo" requestURI="#">
				   	<display:column title="Leitura Atual">
				   		<fmt:formatNumber value='${leituraConsumo.leituraAtual}' maxFractionDigits='0'/>
				   	</display:column>    

				   	<display:column title="Informada <br/>pelo Cliente">
				   		<c:choose>
				   			<c:when test="${leituraConsumo.informadoCliente}">
				   				Sim
				   			</c:when>
				   			<c:otherwise>
				   				N�o
				   			</c:otherwise>
				   		</c:choose>
				   	</display:column>    
				   
				   	<display:column title="Data da <br/>Leitura Atual">
				   		<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' pattern='dd/MM/yyyy'/>
				   	</display:column>
				   	
				   	<display:column title="Leiturista">
				   		<c:out value="${leituraConsumo.leiturista.funcionario.nome}" />
				   	</display:column>
				   	
				   	<display:column title="Leitura<br/>Anterior">
				   		<a href='javascript:exibirDadosLeituraAnterior(
							"<fmt:formatNumber value="${leituraConsumo.leituraAnterior}" maxFractionDigits="0"/>",
							"<fmt:formatDate value="${leituraConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>",
							"<c:out value="${leituraConsumo.anormalidadeLeitura.descricao}"/>",
							"<fmt:formatNumber value="${leituraConsumo.volumeAnteriorApurado}" maxFractionDigits="0"/>",
							"<fmt:formatNumber value="${leituraConsumo.consumoFaturado}" maxFractionDigits="0"/>",
							"<c:out value="${leituraConsumo.diasConsumo}"/>");'>
							
							<img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/>
						</a>	
				   	</display:column>
			    </display:table>
			</fieldset>
		    </c:if>
		    
			<hr class="linhaSeparadoraDetalhamento" />
			
			<fieldset id="conteinerGeral" class="conteinerBloco">
				<legend>Geral:</legend>
				<fieldset class="coluna2">
					<label class="rotulo">M�s/Ano-Ciclo:</label>
					<span class="itemDetalhamento">
						<c:out value="${faturaForm.anoMesReferencia}" />
					</span><br class="quebraLinha" />
					<label class="rotulo">Data de Emiss�o:</label>
					<span class="itemDetalhamento"><c:out value='${faturaForm.dataEmissao}'/></span><br class="quebraLinha" />
				</fieldset>
				<fieldset class="colunaFinal2">
					<label class="rotulo">Motivo de Inclus�o:</label>
					<span class="itemDetalhamento"><c:out value="${faturaForm.descricaoMotivoInclusao}" /></span><br class="quebraLinha" />
					<c:if test="${faturaForm.numeroContrato ne null && faturaForm.numeroContrato ne ''}">
						<label class="rotulo">Contrato:</label>
						<span class="itemDetalhamento"><c:out value="${faturaForm.numeroContrato}" /></span>
					</c:if>
				</fieldset>
			</fieldset>
			
			<c:if test="${listaDadosGerais ne null}">
				<c:set var="i" value="1" />
				<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
					<hr class="linhaSeparadoraDetalhamento" />
					<fieldset class="conteinerBloco">
						<legend>Fatura <c:out value="${i}"/></legend>
						<label class="rotulo">Data de Vencimento:</label>
						<span class="itemDetalhamento"><fmt:formatDate value="${dadosGeraisItensFatura.dataVencimento}" pattern="dd/MM/yyyy"/></span>
						<label class="rotulo rotuloHorizontal">Tipo da Nota Fiscal:</label>
						<span class="itemDetalhamento">
							<c:choose>
								<c:when test="${dadosGeraisItensFatura.indicadorProduto}"><c:out value="Produto"/></c:when>
								<c:otherwise><c:out value="Servi�o"/></c:otherwise>
							</c:choose>
						</span>
						
						<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" list="${dadosGeraisItensFatura.listaDadosItensFaturaExibicao}" sort="list" id="item" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
						   	<display:column title="Descri��o" style="width: 300px">
						   		<c:choose>
							   		<c:when test="${item.descricaoDesconto ne null}">
							   		    <c:out value="${item.descricaoDesconto}"/>
							   		</c:when>	
							   		<c:when test="${item.creditoDebitoARealizar ne null && item.creditoDebitoARealizar.creditoDebitoNegociado ne null 
							   						&& item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica ne null}">
							   			<c:out value="${item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao}"/>
							   			(<c:out value="${item.creditoDebitoARealizar.numeroPrestacao}"/>/<c:out value="${item.creditoDebitoARealizar.creditoDebitoNegociado.quantidadeTotalPrestacoes}"/>)
							   		</c:when>		   		
							   		<c:when test="${item.faturaItem ne null && item.faturaItem.rubrica ne null && item.pontoConsumo ne null 
							   						&& item.pontoConsumo.instalacaoMedidor ne null && item.pontoConsumo.instalacaoMedidor.medidor ne null}">
							   			<c:out value="${item.faturaItem.rubrica.descricaoImpressao}"/>
							   		</c:when>	
							   		<c:otherwise>
							   			<c:out value="${item.rubrica.descricaoImpressao}"/>
							   		</c:otherwise>
						   		</c:choose>	
						   	</display:column>
						   	<display:column title="Tipo" style="width: 120px">
						   		<c:choose>
						   			<c:when test="${item.indicadorCredito eq true}">
				   						<c:out value="Cr�dito"></c:out>
				   					</c:when>
				   					<c:otherwise>
					   					<c:out value="D�bito"></c:out>
				   					</c:otherwise>
						   		</c:choose>
						   	</display:column>
						   	
						   	<display:column title="Data In�cio" style="width: 80px">
								<fmt:formatDate value="${item.dataInicial}"></fmt:formatDate>
						   	</display:column>
						   	<display:column title="Data Final" style="width: 80px">
								<fmt:formatDate value="${item.dataFinal}"></fmt:formatDate>
						   	</display:column>						   	
						   	
						   	<display:column title="Quantidade" style="width: 80px">
								<fmt:formatNumber value="${item.consumo}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
						   	</display:column>
						   	
						   	<display:column title="Unidade" style="width: 60px">
						   		<c:choose>
						   			<c:when test="${item.itemFatura ne null}">
				   						m<span class="expoente">3</span>
				   					</c:when>
						   		</c:choose>
						   	</display:column>
						   	
							<display:column title="Valor Unit�rio (R$)" style="width: 110px; text-align: right; padding-right: 5px">
								<c:choose>
									<c:when test="${item.indicadorCredito eq true}">
										-<fmt:formatNumber value="${item.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
									</c:when>
					   				<c:otherwise>
					   					<fmt:formatNumber value="${item.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
					   				</c:otherwise>
					   			</c:choose>
						   	</display:column>
						   	
						   	<display:column title="Valor (R$)" style="width: 110px; text-align: right; padding-right: 5px">
								<c:choose>
							   		<c:when test="${item.valorTotal ne null}">
								   		<c:choose>
								   			<c:when test="${item.indicadorCredito eq true}">
						   						-<fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
						   					</c:when>
						   					<c:otherwise>
							   					<fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
						   					</c:otherwise>
								   		</c:choose>
									</c:when>	
							   		<c:otherwise>
							   			<c:choose>
							   				<c:when test="${item.indicadorCredito eq true}">
						   						-<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
						   					</c:when>
						   					<c:otherwise>
							   					<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
						   					</c:otherwise>
						   				</c:choose>
							   		</c:otherwise>
							   	</c:choose>
						   	</display:column>
						   	
						   	<display:column title="Valor Cr�dito Dispon�vel (R$)" style="width: 110px; text-align: right; padding-right: 5px">
								<c:choose>
									<c:when test="${item.indicadorCredito eq true}">
										-<fmt:formatNumber value="${item.valorTotalDisponivel}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
									</c:when>
					   				<c:otherwise>
					   					<fmt:formatNumber value="${item.valorTotalDisponivel}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
					   				</c:otherwise>
					   			</c:choose>
						   	</display:column>
						</display:table> 
						
						<c:if test="${dadosGeraisItensFatura.listaDadosTributoVO ne null && not empty dadosGeraisItensFatura.listaDadosTributoVO}">
							<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" list="${dadosGeraisItensFatura.listaDadosTributoVO}" sort="list" id="tributoVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
							   	<display:column property="tributoAliquota.tributo.descricao" title="Tributo" style="width: 25%"/>
							   	<display:column title="Al�quota" style="width: 25%">
							   		<fmt:formatNumber value="${tributoVO.tributoAliquota.percentual}" minFractionDigits="2" maxFractionDigits="2"/>%
							   	</display:column>
							   	<display:column title="Base de C�lculo (R$)" style="width: 25%; text-align: right; padding-right: 5px">
							   		<fmt:formatNumber value="${tributoVO.baseCalculo}" minFractionDigits="2" maxFractionDigits="2"/>
							   	</display:column>
							   	<display:column title="Valor (R$)" style="width: 25%; text-align: right; padding-right: 5px">
							   		<fmt:formatNumber value="${tributoVO.valor}" minFractionDigits="2" maxFractionDigits="2"/>
							   	</display:column>
						   	</display:table>
						</c:if>
					</fieldset>
					<c:set var="i" value="${i+1}" />
					
					<label class="rotulo">Valor total da fatura:</label>
					<span class="itemDetalhamento">R$ <fmt:formatNumber value='${dadosGeraisItensFatura.valorTotalFatura}' minFractionDigits="2" maxFractionDigits="2"/></span><br class="quebraLinha" />
				</c:forEach>
			</c:if>
			
			<c:if test="${listaFaturasPendentes ne null && not empty(listaFaturasPendentes)}">
				<hr class="linhaSeparadoraDetalhamento" />
				
				<label class="rotulo">Incluir faturas n�o cobradas?</label>
				<input class="campoRadio" type="radio" name="indicadorFaturasPendentes" value="true" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'true' || faturaForm.indicadorFaturasPendentes eq ''}">checked="checked" </c:if>>
				<label class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" name="indicadorFaturasPendentes" value="false" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'false'}">checked="checked" </c:if>>
				<label class="rotuloRadio">N�o</label>
				
				<fieldset class="conteinerBloco">
					<legend>Faturas N�o Cobradas</legend>
					<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" name="listaFaturasPendentes" sort="list" id="fatura" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
						<c:set var="i" value="${i+1}" />					
						<display:column title="N�mero do Documento" >
							<input type="hidden" name="chavesFaturasPendentes" value="${fatura.chavePrimaria}" />
							<c:out value="${fatura.chavePrimaria}"/>
						</display:column>    
						<display:column title="Data de<br />Emiss�o" style="width: 85px">
							<fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy"/>
						</display:column>
						<display:column title="Data de<br />Vencimento" style="width: 85px">
							<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
						</display:column>    
						<display:column title="Ciclo /<br />Refer�ncia" style="width: 85px">
							<c:if test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
								<c:out value="${fatura.cicloReferenciaFormatado}"/>
							</c:if>
						</display:column>
						<display:column title="Valor Total (R$)" style="width: 105px; text-align: right; padding-right: 5px" headerClass="tituloTabelaEsq">
							<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/>
						</display:column>
						<display:column title="Situa��o" style="width: 95px">
			            	<c:out value="${fatura.creditoDebitoSituacao.descricao}"/>
						</display:column>
						<display:column title="Pagamento" style="width: 115px">
			            	<c:out value="${fatura.situacaoPagamento.descricao}"/>
						</display:column>
				    </display:table>
				</fieldset>
			</c:if>
			
			
	
		   <!--   
		 <c:if test="${listaCreditoDebito ne null && not empty(listaCreditoDebito)}">
		 <hr class="linhaSeparadoraDetalhamento" />
		    <fieldset class="conteinerBloco">
				<legend>Cr�dito e D�bito:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" sort="list" id="creditoDebito" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
					
				<display:column title="Rubrica" style="width: 330px">				   		
					<c:out value="${creditoDebito.creditoDebitoNegociado.rubrica.descricao}"/> 
					(<c:out value="${creditoDebito.numeroPrestacao}"/>/<c:out value="${creditoDebito.creditoDebitoNegociado.quantidadeTotalPrestacoes}"/>)
					<c:if test="${creditoDebito.creditoDebitoNegociado.descricao ne null && creditoDebito.creditoDebitoNegociado.descricao ne ''}">
						<c:out value="${creditoDebito.creditoDebitoNegociado.descricao}" />
					</c:if>
			   	</display:column>	
				
				<display:column title="Tipo" >
			   		<c:choose>
						<c:when test="${creditoDebito.creditoDebitoNegociado.creditoOrigem eq null}">D�bito</c:when>
						<c:otherwise>Cr�dito</c:otherwise>
					</c:choose>
			   	</display:column>
					
			   	<display:column title="Quantidade" >
			   		<fmt:formatNumber value="${creditoDebito.quantidade}" minFractionDigits="2" />
			   	</display:column>
			   	
			   	<display:column title="Valor Unit�rio (R$)">
					<fmt:formatNumber value="${creditoDebito.valorUnitario}" minFractionDigits="2" maxFractionDigits="2"/>
			   	</display:column>
			   	
			   <display:column title="Valor Total (R$)" >
			   		<input type="hidden" id="creditoDebito${creditoDebito.chavePrimaria}" value="<fmt:formatNumber value='${creditoDebito.saldoCredito}' maxFractionDigits='2' minFractionDigits='2'/>"></input>
			   		<fmt:formatNumber value="${creditoDebito.saldoCredito}" minFractionDigits="2" maxFractionDigits="2"/>
			   	</display:column>
			   	
				</display:table>
			</fieldset>
		    </c:if>-->
			
			
		</fieldset>	
		
		<fieldset class="conteinerBotoes"> 
		    <input name="Button" class="bottonRightCol2 botaoVoltar" value="<< Voltar" type="button" onclick="voltar();">
		    <input name="Button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="button" onclick="salvar();">
	</fieldset>
</form:form>