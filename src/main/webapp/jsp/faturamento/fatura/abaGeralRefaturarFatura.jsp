<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<script>

	$(document).ready(function(){
		/*
		$("input[name=dataVencimento]").datepicker({
			changeYear: true, 
			//maxDate: '+0d', 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});
		*/
		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
									autoclose: true,
									format: 'dd/mm/yyyy',
									language: 'pt-BR'
								});
		iniciarDatatable('#itemFatura', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});
		
		/* $('#itemFatura').dataTable( {
			"paging": true,
			"language": {
	            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		} ); */
	});
</script>
<div class="card">
	<div class="card-body">
		<h5 class="mt-2 mb-3">Itens da Fatura</h5>
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover" id="itemFatura" width="100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" width="50%" class="text-center">Descri��o</th>
							<th scope="col" width="50%" class="text-center">Data</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${itensFatura}" var="itemFaturamento">
							<tr>
								<td class="text-center"><c:out value="${itemFaturamento.descricao}" /></td>
								<td><input class="form-control bootstrapDP" type="text" name="dataVencimento" id="dataVencimento_${itemFaturamento.dataVencimento}"
									value="<fmt:formatDate value='${itemFaturamento.dataVencimento}' pattern='dd/MM/yyyy'/>" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
	</div>
</div>

