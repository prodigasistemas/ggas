<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<div class="card">
	<div class="card-header">
		<h5 class="card-title">Incluir Fatura<a class="linkHelp" href="<help:help>/inclusodefatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h5>
	</div>
	<div class="card-body">
		<div class="alert alert-primary" role="alert">
			<i class="fa fa-info-circle" aria-hidden="true"></i> Para inserir uma Fatura clique em <span class="destaqueOrientacaoInicial">Avan�ar</span>.
		</div>

	
	
<script type="text/javascript">

$(document).ready(function(){
	//identificarMotoristasVeiculos(document.faturaForm.idTransportadora);
	
});

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('pontoConsumo', 'fade=0,speed=400,hide=0');

	function consistir() {
		// Setando a data dos campos de data de leitura nos campos hidden, caso o usu�rio selecione a data pelo calend�rio.
		var datasLeituraAtual = $("input[name=dataLeituraAtual]");
		$("input[name=dataLeitura]").each(function(indice, elem){
			datasLeituraAtual[indice].value = elem.value;
		});
		
		submeter('faturaForm','consistirLeituraFaturamento');
	}

	function cancelar() {
		var idContratoEncerrarRescindir = document.getElementById('idContratoEncerrarRescindir').value;
		submeter('faturaForm','pesquisarFatura?chavePrimariaContrato='+idContratoEncerrarRescindir);
	}
	
	
	function identificarMotoristasVeiculos(component){
		
		var idTransportadora = component.value;
		
 		var veiculoSelect = document.getElementById('idVeiculo');
 		veiculoSelect.disabled = false;
 		veiculoSelect.length = 0;
 		veiculoSelect.options[veiculoSelect.length] = new Option('Selecione', '-1');
 		
 		
		var motoristaSelect = document.getElementById('idMotorista');
		
// 		$("select[value=-1]").removeAttr("disabled");
		
		motoristaSelect.disabled = false;
		motoristaSelect.length = 0;
		motoristaSelect.options[motoristaSelect.length] = new Option('Selecione', '-1');
		
	   	AjaxService.obterListaVeiculosPorTransportadora( idTransportadora, {
	       	callback:function(veiculos) {  

	       		for(key in veiculos){
	       			var novaOpcao = new Option(veiculos[key], key);
	       			veiculoSelect.options[veiculoSelect.length] = novaOpcao;
	       			
	       		}
	     		
	       			
	 	 
	       		
	    	}, async:false}
	    );	
	   	
	   	AjaxService.obterListaMotoristasPorTransportadora( idTransportadora, {
	       	callback:function(motoristas) { 
	       		for(key in motoristas){
	       			var novaOpcao = new Option(motoristas[key], key);
	       			motoristaSelect.options[motoristaSelect.length] = novaOpcao;
	       		}
	       		
	    	}, async:false}
	    );	
	}
	
</script>

<form:form method="post" action="exibirResumoInclusaoFaturaAvulso" name="faturaForm">
	<input name="acao" type="hidden" id="acao" value="exibirResumoInclusaoFaturaAvulso">
	<input name="indexLista" type="hidden" id="indexLista" value="-1">
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="tela" type="hidden" id="tela" value="exibirInclusaoFaturaAvulso">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}" />
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="idContratoEncerrarRescindir" type="hidden" id="idContratoEncerrarRescindir" value="${faturaForm.idContratoEncerrarRescindir}">
	<input name="listaIdsPontoConsumoRemovidosAgrupados" type="hidden" id="listaIdsPontoConsumoRemovidosAgrupados" value="${listaIdsPontoConsumoRemovidosAgrupados}">
	<input name="anoMesReferencia"	type="hidden" id="anoMesReferencia"  value="${anoMesReferencia}"> 
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
	
	<fieldset id="incluirFatura" class="conteinerPesquisarIncluirAlterar">
		<input type="hidden" name="idCliente" id="idCliente" value="${faturaForm.idCliente}">
		<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
			<div class="container-fluid">
				
				<div class="row">
					<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
				</div>
				<div id="dadosCliente">
					<div class="row">
						<div class="col-5">
							<label class="rotulo">Cliente:</label>
							<input name="nomeCompletoCliente" type="hidden" value="${cliente.nome}">
							<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
						</div>
						<div class="col">
							<label class="rotulo">CPF/CNPJ:</label>
							<input name="documentoFormatado" type="hidden" value="${cliente.numeroDocumentoFormatado}">
							<span class="itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
						</div>
					</div>
					<div class="row">
						<div class="col-5">
							<label class="rotulo">Endere�o:</label>
							<input name="enderecoFormatadoCliente" type="hidden" value="${cliente.enderecoPrincipal.enderecoFormatado}">
							<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
						</div>
						<div class="col">
							<label class="rotulo">E-mail:</label>
							<input name="emailCliente" type="hidden" value="${cliente.emailPrincipal}">
							<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
						</div>
					</div>
				</div>
			</div>
			
		</c:if>
		
		<c:if test="${listaPontoConsumo ne null && not empty listaPontoConsumo}">
			<div class="container-fluid">
				<div class="row">
					<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[pontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				</div>
				<div class="row" id="pontoConsumo">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" id="pontoConsumo">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th scope="col" class="text-center">C�digo</th>
									<th scope="col" class="text-center">Descri��o</th>
									<th scope="col" class="text-center">Endere�o</th>
									<th scope="col" class="text-center">CEP</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
									<tr>
										<td><c:out value="${pontoConsumo.codigoPontoConsumo}"/></td>
										<td><c:out value="${pontoConsumo.descricao}"/></td>
										<td><c:out value="${pontoConsumo.enderecoFormatado}"/></td>
										<td><c:out value="${pontoConsumo.cep.cep}"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</c:if>

		<div class="container-fluid">
			<div class="form-row">
				<div class="form-group row col-4">
					<label for="idMotivoInclusao" class="col-5 text-right rotulo campoObrigatorio"><span
						class="campoObrigatorioSimbolo">* </span>Motivo de Inclus�o:</label> 
					<select name="idMotivoInclusao" id="idMotivoInclusao"
							class="form-control col-6" >
						<option value="-1">Selecione</option>
						<c:forEach items="${listaMotivoInclusao}" var="motivoInclusao">
							<option value="<c:out value="${motivoInclusao.chavePrimaria}"/>"
								<c:if test="${faturaForm.idMotivoInclusao == motivoInclusao.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${motivoInclusao.descricao}" />
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-group row col-4">
					<label for="idTransportadora" class="col-5 text-right rotulo campoObrigatorio"><span
						class="campoObrigatorioSimbolo2">* </span>Transportadora:</label> 
						<select name="idTransportadora" id="idTransportadora"
							class="form-control col-6"
						onchange="identificarMotoristasVeiculos(this);">
						<option value="-1">Selecione</option>
						<c:forEach items="${listaTransportadora}" var="transportadora">
							<option value="<c:out value="${transportadora.chavePrimaria}"/>"
								<c:if test="${faturaForm.idTransportadora == transportadora.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${transportadora.cliente.nome}" />
							</option>
						</c:forEach>
					</select>		
				</div>
			</div>
			<div class="form-row">
				<div class="form-group row col-4">
					<label class="col-5 text-right rotulo campoObrigatorio" id="rotuloNome" for="volumeApurado"><span
							class="campoObrigatorioSimbolo">* </span><span>Volume do G�s:</span></label>
					<input class="form-control col-6" type="text" id="volumeApurado" name="volumeApurado"
						onkeypress="return formatarCampoInteiro(event);" maxlength="4" size="5"
						value="${faturaForm.volumeApurado[0]}"	/>
				</div>
				<div class="form-group row col-4">
					<label for="idVeiculo" class="col-5 text-right  rotulo campoObrigatorio "><span class="campoObrigatorioSimbolo2">* </span>Veiculo:</label>
					<select name="idVeiculo" id="idVeiculo" class="form-control col-6">
						<option value="-1">Selecione</option>
					</select>
				</div>
			</div>
			
			<div class="form-row">
				<div class="form-group row col-4">
					<label for="tarifaDoGas" class="col-5 text-right rotulo" id="rotuloNome" for="nome">
						Tarifa do G�s:
					</label> 
					<input id="tarifaDoGas" class="form-control col-6" name="tarifaAplicada" type="text"
							value="${tarifaAplicada}" disabled="disabled" />
				</div>
				<div class="form-group row col-4">
					<label for="idMotorista" class="col-5 text-right rotulo campoObrigatorio "><span
						class="campoObrigatorioSimbolo2">* </span>Motorista:</label> 
						<select name="idMotorista" id="idMotorista" class="form-control col-6" >
						<option value="-1">Selecione</option>
					</select>
				</div>
			</div>
		</div>

		<ul class="nav nav-tabs" id="myTab" role="tablist">
			<li class="nav-item">
				<a href="#debitosCreditosFatura" class="nav-link active" 
					id="debitosCreditosFatura-tab" data-toggle="tab" role="tab" aria-controls="debitosCreditosFatura"
					aria-selected="true">D�bitos e Cr�ditos</a>
			</li>
			<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes}">
				<li class="nav-item">
					<a href="#dadosFaturasPendentes" class="nav-link active" 
						id="dadosFaturasPendentes-tab" data-toggle="tab" role="tab" aria-controls="dadosFaturasPendentes"
						aria-selected="true">Faturas N�o Cobradas</a>
				</li>
			</c:if>
		</ul>
		<div class="tab-content" id="myTabContent">
			<div class="tab-pane fade show active" id="debitosCreditosFatura" role="tabpanel" aria-labelledby="debitosCreditosFatura-tab">
				<jsp:include page="/jsp/faturamento/fatura/abaDebitosCreditosFatura.jsp"/>
			</div>
			<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes}">	
				<div class="tab-pane fade" id="dadosFaturasPendentes" role="tabpanel" aria-labelledby="dadosFaturasPendentes-tab">
					<jsp:include page="/jsp/faturamento/fatura/abaFaturasPendentes.jsp"/>
				</div>
			</c:if>
		</div>
		<p><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o da Fatura.</p>
	
	
	<div class="btn-group" role="group" aria-label="Button group">
		<button type="button" class="btn btn-primary" onclick="javascript:cancelar();">Cancelar</button>
		<button type="submit" class="btn btn-primary">Avan�ar <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
	</div>

</form:form>
	
	</div>
</div>
