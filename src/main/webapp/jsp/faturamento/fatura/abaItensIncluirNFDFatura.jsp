<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
	
	
<script>

	$(document).ready(function(){


		iniciarDatatable('#listaDadosGeraisItens', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});
		
		$("input[name=dataVencimento]").datepicker({
			changeYear: true, 
			//maxDate: '+0d', 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});

		$('.checkItem').change(function () {
			var habilitado = $(this).is(":checked");
			$(this).closest('tr').find('.qtdItem').prop('disabled', !habilitado);
			$(this).closest('tr').find('.vlrUnitario').prop('disabled', !habilitado);
		});

		function calcularTotal() {
			var totalGeral = 0;
			var nf = Intl.NumberFormat();
			$('.checkItem:checked').each(function(){
				var linha = $(this).closest('tr');
				var qtd = Number(linha.find('.qtdItem').val().replace(',', '.'));
				var vlr = Number(linha.find('.vlrUnitario').val().replace(',', '.'));
				var totalItem = vlr * qtd;

				totalItem = Number(totalItem.toFixed(2));
				totalGeral += totalItem;
				linha.find('.lblTotalItem').html(nf.format(totalItem.toFixed(2)));
			});
			$('#totalFatura').html('R$ ' + nf.format(totalGeral.toFixed(2)));
			atualizarTributos(totalGeral);
		}

		function atualizarTributos(totalGeral) {
			$('.aliquotaTributo').each(function(){
				var linha = $(this).closest('tr');
				var aliquota = Number($(this).html().replace('%', '').replace(',', '.'));
				var totalItem = totalGeral * aliquota / 100;
				totalItem = Number(totalItem.toFixed(2));
				linha.find('.valorBaseTributo').html(totalGeral.toFixed(2));
				linha.find('.totalTributo').html(totalItem.toFixed(2));
			});
		}
		$('.atualizavel').change(function () {
			calcularTotal();
		});
		calcularTotal();


	});
</script>

		<c:if test="${listaDadosGerais ne null}">
			<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover"
					name="listaDadosGeraisItens" id="listaDadosGeraisItens" width="100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" class="text-center">Devolver</th>
							<th scope="col" class="text-center">Descri��o</th>
							<th scope="col" class="text-center">Tipo</th>
							<th scope="col" class="text-center">Quantidade</th>	
							<th scope="col" class="text-center">Unidade</th>												
							<th scope="col" class="text-center">Valor Unit�rio (R$)</th>
							<th scope="col" class="text-center">Valor (R$)</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${dadosGeraisItensFatura.listaDadosItensFaturaExibicao}"
							var="item">
							<tr>
								<td class="text-center">
									<input type="checkbox" class="checkItem atualizavel" checked>
								</td>
								<td class="text-center">
							   		<c:choose>
								   		<c:when test="${item.descricaoDesconto ne null}">
								   			<c:out value="${item.descricaoDesconto}"/>
								   		</c:when>	
								   		<c:when test="${item.creditoDebitoARealizar ne null && item.creditoDebitoARealizar.creditoDebitoNegociado ne null 
								   						&& item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica ne null}">
								   			<c:out value="${item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao}"/>
								   		</c:when>		   		
								   		<c:when test="${item.faturaItem ne null && item.faturaItem.rubrica ne null && item.pontoConsumo ne null 
								   						&& item.pontoConsumo.instalacaoMedidor ne null && item.pontoConsumo.instalacaoMedidor.medidor ne null}">
								   			<c:out value="${item.faturaItem.rubrica.descricaoImpressao}"/>
								   		</c:when>	
								   		<c:otherwise>
								   			<c:out value="${item.rubrica.descricaoImpressao}"/>
								   		</c:otherwise>
							   		</c:choose>									
								</td>
								<td class="text-center">
							   		<c:choose>
							   			<c:when test="${item.indicadorCredito eq true}">
					   						<c:out value="Cr�dito"></c:out>
					   					</c:when>
					   					<c:otherwise>
						   					<c:out value="D�bito"></c:out>
					   					</c:otherwise>
							   		</c:choose>
								</td>
								<td class="text-center">
									<fmt:formatNumber value="${item.faturaItem.quantidade}" minFractionDigits="4" maxFractionDigits="4" var="qtdItem" />
									<input name="consumoItem" class="qtdItem atualizavel" type="number" style="text-align:right;" value="${item.faturaItem.quantidade}" />
								</td>
								<td class="text-center">
							   		<c:choose>
							   			<c:when test="${item.itemFatura ne null}">
					   						m<span class="expoente">3</span>
					   					</c:when>
							   		</c:choose>
								</td>
								<td class="text-center">
									<fmt:formatNumber value="${item.faturaItem.valorUnitario}" minFractionDigits="4" maxFractionDigits="4" var="unit" />
									<c:choose>
										<c:when test="${item.indicadorCredito eq true}">
											<input name="vlrUnitarioItem" class="vlrUnitario atualizavel" style="text-align:right;" type="number" value="${item.faturaItem.valorUnitario}"/>
										</c:when>
						   				<c:otherwise>
											<input name="vlrUnitarioItem" class="vlrUnitario atualizavel" style="text-align:right;" type="number" value="${item.faturaItem.valorUnitario}"/>
						   				</c:otherwise>
						   			</c:choose>
								</td>
								<td class="text-center">
									<label class="lblTotalItem"></label>
								</td>																							
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>


				<label class="rotulo">Valor total da fatura:</label>
				<span id="totalFatura" class="itemDetalhamento">R$ </span><br class="quebraLinha" />
				</c:forEach>
		</c:if>


