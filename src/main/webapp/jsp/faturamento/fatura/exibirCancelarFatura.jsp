<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">

	function cancelarFatura(){

		if(confirm("Deseja Cancelar a(s) Fatura(s) ?")) {
			submeter('faturaForm','cancelarFatura');
		}
	}

	function cancelar(){
		submeter('faturaForm','pesquisarFatura');
	}
	
	$(document).ready(function(){

		iniciarDatatable('#fatura', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});
	});

</script>

<div class="card">
	<div class="card-header">
		<h5 class="card-title">Cancelar Fatura<a class="linkHelp" href="<help:help>/cancelamentodefatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h5>
	</div>
	<div class="card-body">
		<div class="alert alert-primary fade show" role="alert">
			<i class="fa fa-exclamation-circle"></i> Selecione o Motivo do cancelamento da Fatura.
		</div>


<form:form method="post" styleId="formPesquisarFatura" action="pesquisarFatura" name="faturaForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarFatura">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${faturaForm.chavePrimaria}">
	
	<input name="tipoDocumento" type="hidden" id="chavePrimaria" value="1">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${faturaForm.numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${faturaForm.numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${faturaForm.codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
	<input name="numeroCiclo" type="hidden" id="numeroCiclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
	<input name="anoMesReferencia" type="hidden" id="anoMesReferencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
	<input name="numeroNotaFiscalInicial" type="hidden" id="numeroNotaFiscalInicial" value="<c:out value="${faturaForm.numeroNotaFiscalInicial}"/>">
	<input name="numeroNotaFiscalFinal" type="hidden" id="numeroNotaFiscalFinal" value="<c:out value="${faturaForm.numeroNotaFiscalFinal}"/>">
	<input name="tipoFatura" type="hidden" id="tipoFatura" value="<c:out value="${faturaForm.tipoFatura}"/>">
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="indicadorComplementar" type="hidden" id="indicadorComplementar" value="<c:out value="${faturaForm.indicadorComplementar}"/>">
	<input name="idSituacaoPagamento" type="hidden" id="idSituacaoPagamento" value="<c:out value="${faturaForm.idSituacaoPagamento}"/>">
	<input name="indicadorPesquisaPontos" type="hidden" id="indicadorPesquisaPontos" value="${faturaForm.indicadorPesquisaPontos}">
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-4">
				<fieldset class="form-group">
					<div class="form-group row">
						<label class="col-3 rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Motivo:</label>
						<select name="idMotivoCancelamento" id="idMotivoCancelamento" class="col-9 form-control" tabindex="1">
				   			<option value="-1">Selecione</option>				
							<c:forEach items="${listaMotivoCancelamento}" var="motivoCancelamento">
								<option value="<c:out value="${motivoCancelamento.chavePrimaria}"/>" <c:if test="${creditoDebitoForm.idMotivoCancelamento == motivoCancelamento.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${motivoCancelamento.descricao}"/>
								</option>		
							</c:forEach>
						</select>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<hr>
	
	<c:if test="${listaFaturaCancelar ne null}">
		<div class="alert alert-primary fade show" role="alert">
			<i class="fa fa-exclamation-circle"></i> As faturas exibidas na lista abaixo ser�o canceladas.
		</div>
		
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover" id="fatura" width="100%">
				<thead class="thead-ggas-bootstrap">
					<tr>
						<th scope="col" class="text-center"></th>
						<th scope="col" class="text-center">N�mero do Documento</th>
						<th scope="col" class="text-center">Data de Emiss�o</th>
						<th scope="col" class="text-center">Data de Vencimento</th>
						<th scope="col" class="text-center">Ciclo / Refer�ncia</th>
						<th scope="col" class="text-center">Valor (R$)</th>
						<th scope="col" class="text-center">Situa��o</th>
						<th scope="col" class="text-center">Pagamento</th>
						<th scope="col" class="text-center">Revis�o</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listaFaturaCancelar}" var="fatura">
						<tr>
							<td class="text-center">
								<input type="hidden" id="chaveFatura${fatura.chavePrimaria}" name="chavesPrimarias" value="${fatura.chavePrimaria}">
								<c:out value="${fatura.chavePrimaria}"/>
							</td>
							<td class="text-center"><fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy"/></td>
							<td class="text-center"><fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/></td>
							<td class="text-center">
								<c:if test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
									<c:out value="${fatura.cicloReferenciaFormatado}"/>
								</c:if>
							</td>
							<td class="text-center"><fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/></td>
							<td class="text-center"><c:out value="${fatura.creditoDebitoSituacao.descricao}"/></td>
							<td class="text-center"> <c:out value="${fatura.situacaoPagamento.descricao}"/></td>
							<td class="text-center"><c:out value="${fatura.motivoRevisao.chavePrimaria}"/></td>
							<td class="text-center">
								<c:if test="${fatura.dataRevisao ne null}">
									<fmt:formatDate value="${fatura.dataRevisao}" pattern="dd/MM/yyyy"/>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:if>
</form:form>


	</div> <!-- div class="body" -->

	<div class="card-footer">
		<div class="row">
			<div class="col-sm-12">
				<c:if test="${listaFaturaCancelar ne null}">
					<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
						name="buttonCancelar" type="button" onclick="cancelar();">
						<i class="fa fa-times"></i> Cancelar
					</button>
				</c:if>
				<button name="buttonIncluir" id="botaoIncluir" value="Salvar"
					onclick="cancelarFatura();"
					class="btn btn-sm btn-success float-right ml-1 mt-1" type="button"
					data-toggle="modal" data-target="#atencao">
					<i class="fas fa-save"></i> Confirmar
				</button>
			</div>
		</div>
	</div>
</div> <!-- div class="card" -->