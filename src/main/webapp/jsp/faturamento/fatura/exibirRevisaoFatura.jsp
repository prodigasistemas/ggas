<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>




<div class="card">
	<div class="card-header">
		<h5 class="card-title">
			<c:out value="${tituloTela}" />
			<c:choose>
				<c:when test="${faturaForm.indicadorRevisao}">
					<a class="linkHelp"
						href="<help:help>/retirandoumafaturadereviso.htm</help:help>"
						target="right" onclick="exibirJDialog('#janelaHelp');"></a>
				</c:when>
				<c:otherwise>
					<a class="linkHelp"
						href="<help:help>/colocandoumafaturaemreviso.htm</help:help>"
						target="right" onclick="exibirJDialog('#janelaHelp');"></a>
				</c:otherwise>
			</c:choose>
		</h5>
	</div>
	<div class="card-body">
		<div class="alert alert-primary fade show" role="alert">
			
			<c:choose>
				<c:when test="${faturaForm.indicadorRevisao}">
					<i class="fa fa-exclamation-circle"></i> Selecione as faturas a serem
						retiradas de revis�o
				</c:when>
				<c:otherwise>
					<i class="fa fa-exclamation-circle"></i> Selecione o motivo e as Faturas a
						serem colocadas em revis�o
				</c:otherwise>
			</c:choose>
		</div>



		<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});	


		iniciarDatatable('#fatura', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});

		 // Dialog			
		$("#pontoConsumoPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});

		$("select#idMotivoRevisao").change(function() {
			motivoRevisao = $(this).val();
			if(motivoRevisao != -1){
				$("input#botaoIncluir").removeAttr("disabled");
			} else {
				$("input#botaoIncluir").attr("disabled","disabled");
			}
		});
	});


	function cancelar(){
		submeter('faturaForm','pesquisarFatura');
	}
	
	function revisarFatura() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('faturaForm','revisarFatura');
		}
	}

	function init() {		
		if (<c:out value="${faturaForm.indicadorRevisao}"/>) {
			document.getElementById('botaoIncluir').value = 'Retirar';			
		} else {
			document.getElementById('botaoIncluir').value = 'Revisar';
		}
		
		if ($("select#idMotivoRevisao").length > 0){
			$("input#botaoIncluir").attr("disabled","disabled");
		}
		
		manterCheckBoxDasTabelas(true);
	}
	
	addLoadEvent(init);

	


	function manterCheckBoxDasTabelas(valor){	
		<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaFatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${chavePrimariaFatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${chavePrimariaFatura}"/>').checked = valor;
			}	
		</c:forEach>
	}
</script>

<form:form method="post" action="revisarFatura" name="faturaForm">
	<input name="tituloTela" type="hidden" id="tituloTela" value="<c:out value="${tituloTela}"/>">
	<input name="tipoDocumento" type="hidden" id="chavePrimaria" value="1">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${faturaForm.numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${faturaForm.numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${faturaForm.codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
	<input name="numeroCiclo" type="hidden" id="numeroCiclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
	<input name="anoMesReferencia" type="hidden" id="anoMesReferencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
	<input name="numeroNotaFiscalInicial" type="hidden" id="numeroNotaFiscalInicial" value="<c:out value="${faturaForm.numeroNotaFiscalInicial}"/>">
	<input name="numeroNotaFiscalFinal" type="hidden" id="numeroNotaFiscalFinal" value="<c:out value="${faturaForm.numeroNotaFiscalFinal}"/>">
	<input name="tipoFatura" type="hidden" id="tipoFatura" value="<c:out value="${faturaForm.tipoFatura}"/>">
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="indicadorComplementar" type="hidden" id="indicadorComplementar" value="<c:out value="${faturaForm.indicadorComplementar}"/>">
	<input name="tipoDocumento" type="hidden" id="chavePrimaria" value="1">
	<input name="idSituacaoPagamento" type="hidden" id="idSituacaoPagamento" value="<c:out value="${faturaForm.idSituacaoPagamento}"/>">
	<input name="idPontoConsumo" type="hidden"  id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	<input name="indicadorPesquisaPontos" type="hidden" id="indicadorPesquisaPontos" value="${faturaForm.indicadorPesquisaPontos}">	
		
	<div id="pontoConsumoPopup" title="Ponto de Consumo">
		<label class="rotulo">Descri��o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="descricaoPopup"></span><br />
		<label class="rotulo">Endere�o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="enderecoPopup"></span><br />
		<label class="rotulo">Cep:</label>
		<span class="itemDetalhamento" id="cepPopup"></span><br />
		<label class="rotulo">Complemento:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="complementoPopup"></span><br /><br />
	</div>

			<c:if test="${faturaForm.indicadorRevisao eq 'false'}">

				<div class="container-fluid">
					<div class="row">
						<div class="col-4">
							<fieldset id="pesquisarBanco" class="form-group">
								<div class="form-group row">
									<label class="col-3 rotulo campoObrigatorio" for="idMotivoRevisao">
										<span class="campoObrigatorioSimbolo">* </span>
										Motivo:
									</label>
									<select name="idMotivoRevisao" id="idMotivoRevisao" class="col-9 form-control">
										<option value="-1">Selecione</option>
										<c:forEach items="${listaMotivosRevisao}" var="motivo">
											<option value="<c:out value="${motivo.chavePrimaria}"/>">
												<c:out value="${motivo.descricao}" />
											</option>
										</c:forEach>
									</select>
								</div>
								<div class="form-group row">
									<label class="col-3 rotulo">Respons�vel:</label>
									<select name="idFuncionario" id="idFuncionario" class="col-9 form-control">
										<option value="-1">Selecione</option>
										<c:forEach items="${listaFuncionario}" var="funcionario">
											<option value="<c:out value="${funcionario.chavePrimaria}"/>">
												<c:out value="${funcionario.nome}" />
											</option>
										</c:forEach>
									</select>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
				<hr class="linhaSeparadoraDetalhamento" />
			</c:if>


			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover" id="fatura" width="100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" class="text-center"></th>
							<th scope="col" class="text-center">N�mero do Documento</th>
							<th scope="col" class="text-center">Data deEmiss�o</th>
							<th scope="col" class="text-center">Data de Vencimento</th>
							<th scope="col" class="text-center">Ciclo / Refer�ncia</th>
							<th scope="col" class="text-center">Valor (R$)</th>
							<th scope="col" class="text-center">Situa��o</th>
							<th scope="col" class="text-center">Pagamento</th>
							<th scope="col" class="text-center">Revis�o</th>
							<th scope="col" class="text-center">Data de Vencimento da Revis�o</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listaFatura}" var="fatura">
							<tr>
								<td><input type="checkbox" id="chaveFatura${fatura.chavePrimaria}" name="chavesPrimarias" value="${fatura.chavePrimaria}"></td>
								<td><c:out value="${fatura.chavePrimaria}"/></td>
								<td><fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy"/></td>
								<td><fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/></td>
								<td>
									<c:if test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
										<c:out value="${fatura.cicloReferenciaFormatado}" />
									</c:if>
								</td>
								<td><fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/></td>
								<td><c:out value="${fatura.creditoDebitoSituacao.descricao}"/></td>
								<td><c:out value="${fatura.situacaoPagamento.descricao}"/></td>
								<td>
									<span title='<c:out value="${fatura.motivoRevisao.descricao}"/>' >
						            	<c:out value="${fatura.motivoRevisao.chavePrimaria}"/>
									</span>
								</td>
								<td>
									<c:if test="${fatura.dataRevisao ne null}">
										<fmt:formatDate value="${fatura.dataRevisao}" pattern="dd/MM/yyyy"/>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
</form:form>

	</div> <!-- div class="body" -->

	<div class="card-footer">
		<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
					name="buttonCancelar"
					type="button" onclick="cancelar();">
					<i class="fa fa-times"></i> Cancelar
				</button>
				<button id="botaoIncluir" value="Salvar"
					name="buttonIncluir"
					onclick="revisarFatura();"
					class="btn btn-sm btn-success float-right ml-1 mt-1" type="button"
					data-toggle="modal" data-target="#atencao">
					<i class="fas fa-save"></i> Revistar
				</button>
			</div>
		</div>
	</div>
</div> <!-- div class="card" -->
