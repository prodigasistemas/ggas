<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<script>

iniciarDatatable('#listaDadosTributoVO', {
	searching: false,
	info:false,
	paging: false,
	ordering: false
});


	$(document).ready(function(){
		$("input[name=dataVencimento]").datepicker({
			changeYear: true, 
			//maxDate: '+0d', 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});
	});
</script>
<style>
    .aliquotaTributo {
        width: 100% !important;
    }
</style>
<c:if test="${listaDadosGerais ne null}">

	<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
		<c:if
			test="${dadosGeraisItensFatura.listaDadosTributoVO ne null && not empty dadosGeraisItensFatura.listaDadosTributoVO}">
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover"
					name="listaDadosTributoVO" id="tributoVO" width="100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" class="text-center">Tributo</th>
							<th scope="col" class="text-center">Al�quota</th>
							<th scope="col" class="text-center">Base de C�lculo (R$)</th>
							<th scope="col" class="text-center">Valor (R$)</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${dadosGeraisItensFatura.listaDadosTributoVO}"
							var="tributoVO">
							<tr>
								<td class="text-center"><c:out
										value="${tributoVO.tributoAliquota.tributo.descricao}" /></td>
								<td class="text-center"><fmt:formatNumber
										value="${tributoVO.tributoAliquota.valorAliquota}"
										minFractionDigits="2" maxFractionDigits="2" />%</td>
								<td class="text-center"><fmt:formatNumber
										value="${tributoVO.baseCalculo}" minFractionDigits="2"
										maxFractionDigits="2" /></td>
								<td class="text-center"><fmt:formatNumber
										value="${tributoVO.valor}" minFractionDigits="2"
										maxFractionDigits="2" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</c:if>

	</c:forEach>
</c:if>

