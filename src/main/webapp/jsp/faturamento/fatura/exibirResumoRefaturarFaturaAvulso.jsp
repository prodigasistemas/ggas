<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Resumo da Fatura<a class="linkHelp" href="<help:help>/resumodefatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	
<script type="text/javascript">

	$(document).ready(function(){
		// Dialog - configura��es
		$("#incluirFaturaLeituraPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
	});

	<c:if test="${listaDadosInclusaoFatura ne null}">
		<c:set var="count" value="1"/>
		<c:forEach items="${listaDadosInclusaoFatura}">
			animatedcollapse.addDiv('conteinerDadosFatura${count}', 'fade=0,speed=400,hide=1');
			<c:set var="count" value="${count+1}"/>
		</c:forEach>
	</c:if>

	function exibirFatura(elem, valor){
		if(elem.checked){
			animatedcollapse.show('conteinerDadosFatura' + valor);
		}else{
			animatedcollapse.hide('conteinerDadosFatura' + valor);
		}
	}
	
	function voltar() {
		submeter('faturaForm', 'exibirRefaturarAvulso');
	}

	function exibirDadosLeituraAnterior(leituraAnterior, dataLeituraAnterior, anormalidadeAnterior, volumeAnterior, consumoFaturado, diasConsumo) {
		$("#leituraAnteriorPopup").html(leituraAnterior);
		$("#dataLeituraAnteriorPopup").html(dataLeituraAnterior);
		$("#anormalidadeAnteriorPopup").html(anormalidadeAnterior);
		$("#volumeAnteriorPopup").html(anormalidadeAnterior);
		$("#consumoFaturadoPopup").html(anormalidadeAnterior);
		$("#diasConsumoPopup").html(anormalidadeAnterior);
		exibirJDialog("#incluirFaturaLeituraPopup");
	}

</script>

<form:form method="post" action="refaturarFatura">
	<input name="acao" type="hidden" id="acao" value="refaturarFatura">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idCliente" type="hidden" id="idCliente" value="${faturaForm.idCliente}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	<input name="idMotivoRefaturamento" type="hidden" id="idMotivoRefaturamento" value="${faturaForm.idMotivoRefaturamento}">
	<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaLista">
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias${chavePrimariaLista}" value="${chavePrimariaLista}">
	</c:forEach>
	
	<div id="incluirFaturaLeituraPopup" title="Leitura">
		<label class="rotulo">Leitura Anterior:</label>
		<span class="itemDetalhamento" id="leituraAnteriorPopup"></span><br />
		<label class="rotulo">Data da Leitura Anterior:</label>
		<span class="itemDetalhamento" id="dataLeituraAnteriorPopup"></span><br />
		<label class="rotulo">Anormalidade da Leitura:</label>
		<span class="itemDetalhamento" id="anormalidadeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Volume Anterior Apurado:</label>
		<span class="itemDetalhamento" id="volumeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Consumo Faturado:</label>
		<span class="itemDetalhamento" id="consumoFaturadoPopup"></span><br /><br />
		<label class="rotulo">Dias de Consumo:</label>
		<span class="itemDetalhamento" id="diasConsumoPopup"></span><br /><br />
	</div>
	
		<input name="chavesFaturasPendentes" type="hidden" id="chavesFaturasPendentes" value="${chaveFatura}">
	<c:forEach items="${faturaForm.chavesFaturasPendentes}" var="chaveFatura">
	</c:forEach>
	<c:forEach items="${faturaForm.chavesCreditoDebito}" var="chaveCreditoDebito">
		<input name="chavesCreditoDebito" type="hidden" id="chavesCreditoDebito" value="${chaveCreditoDebito}">
	</c:forEach>
	
		<fieldset id="resumoFatura" class="conteinerPesquisarIncluirAlterar">
			<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
				<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
					<fieldset class="coluna detalhamentoColunaLarga">
						<label class="rotulo">Cliente:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
						<label class="rotulo">CPF/CNPJ:</label>
						<span class="itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
						<label class="rotulo">E-mail:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
					</fieldset>
				</fieldset><br/>
			</c:if>
			<c:if test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
				<a id="linkDadosImovel" class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosImovel" class="conteinerDados conteinerBloco">
					<fieldset class="coluna detalhamentoColunaLarga">
						<label class="rotulo">Descri��o:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.nome}"/></span><br />
						<label class="rotulo">Matr�cula:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.chavePrimaria}"/></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">Cidade:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.nomeMunicipio}"/></span><br />
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.modalidadeMedicaoImovel.codigo}"/></span><br />
					</fieldset>
				</fieldset><br/>
			</c:if>
			<c:if test="${listaPontoConsumo ne null && (faturaForm.idCliente eq null || faturaForm.idCliente eq 0)}">
				<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosPontoConsumo" class="conteinerBloco">
					<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
					   	<display:column title="Im�vel - Ponto Consumo" style="width: 330px">
					   		<input type="hidden" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"/>
					   		
					   		<c:choose>
					   			 <c:when test="${nomeImovel ne null}">
					   			 	<c:out value="${nomeImovel}"/> - <c:out value="${pontoConsumo.descricao}"/>
					   			 </c:when>
					   			 <c:otherwise>
									<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>							
					   			 </c:otherwise>
					   		</c:choose>
					   	</display:column>
					   	<display:column property="segmento.descricao" title="Segmento"/>    
					   	<display:column property="situacaoConsumo.descricao" title="Situa��o"/>
					   	<display:column property="instalacaoMedidor.medidor.numeroSerie" title="N� Medidor"/>
				    </display:table>	
				</fieldset>
			</c:if>
			<br class="quebraLinha" />
		    <c:if test="${listaHistoricoConsumo ne null && not empty(listaHistoricoConsumo)}">
		    <fieldset class="conteinerBloco">
				<legend>Consumo:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="${listaHistoricoConsumo}" sort="list" id="consumo" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
					<display:column title="Medidor" style="width: 75px">
						<c:out value="${consumo.historicoAtual.historicoInstalacaoMedidor.medidor.numeroSerie}"/>
					</display:column>

					<display:column title="Data" style="width: 75px">
						<fmt:formatDate value="${consumo.historicoAtual.dataLeituraInformada}" pattern="dd/MM/yyyy" />
					</display:column>
					
					<display:column title="Leitura">
						<fmt:formatNumber value="${consumo.historicoAtual.numeroLeituraInformada}" maxFractionDigits="0" />
					</display:column>
					
					<display:column title="Consumo<br/>Apurado" style="width: 95px">
						<fmt:formatNumber value="${consumo.consumoApurado}" minFractionDigits="4" maxFractionDigits="4" />
					</display:column>
					
					<display:column title="Consumo<br/>Di�rio" style="width: 95px">
						<fmt:formatNumber value="${consumo.consumoDiario}" minFractionDigits="4" maxFractionDigits="4" />
					</display:column>
					
					<display:column title="Cr�dito<br/>Consumo" style="width: 95px">
						<fmt:formatNumber value="${consumo.consumoCredito}" minFractionDigits="4" maxFractionDigits="4" />
					</display:column>
					
					<display:column title="Fator PTZ" style="width: 60px"> 
						<fmt:formatNumber value="${consumo.fatorPTZ}" minFractionDigits="4" maxFractionDigits="4" />
					</display:column>
					
					<display:column title="Fator PCS" style="width: 60px">
						<fmt:formatNumber value="${consumo.fatorPCS}" minFractionDigits="4" maxFractionDigits="4" />
					</display:column>
					
					<display:column title="Fator<br/>Corre��o" style="width: 60px">
						<fmt:formatNumber value="${consumo.fatorCorrecao}" minFractionDigits="4" maxFractionDigits="4" />
					</display:column>
					
					<display:column title="Dias de<br/>Consumo" style="width: 60px">
						<c:out value="${consumo.diasConsumo}"></c:out>
					</display:column>
					
					<display:column title="Tipo de <br/>Consumo" style="width: 90px">
						<c:out value="${consumo.tipoConsumo.descricao}"></c:out>
					</display:column>
				</display:table>
			</fieldset>
		    </c:if>
		    
			<br class="quebraLinha" />
			
			<c:if test="${listaDadosLeituraConsumoFatura ne null && not empty(listaDadosLeituraConsumoFatura)}">
			<fieldset class="conteinerBloco">
				<legend>Leitura Consumo:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" name="${listaDadosLeituraConsumoFatura}" sort="list" id="leituraConsumo" requestURI="#">
				   	<display:column title="Leitura Atual">
				   		<fmt:formatNumber value='${leituraConsumo.leituraAtual}' maxFractionDigits='0'/>
				   	</display:column>    

				   	<display:column title="Informada <br/>pelo Cliente">

				   	</display:column>    
				   
				   	<display:column title="Data da /<br/>Leitura Atual">
				   		<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' pattern='dd/MM/yyyy'/>
				   	</display:column>
				   	
				   	<display:column title="Leiturista">
				   		<c:out value="${leituraConsumo.leiturista.funcionario.nome}" />
				   	</display:column>
				   	
				   	<display:column title="Consumo<br/>Apurado(m3)">
				   		<fmt:formatNumber value='${leituraConsumo.consumoApurado}' maxFractionDigits='0'/>
				   	</display:column>
				   	
				   	<display:column title="Leitura<br/>Anterior">
				   		<a href='javascript:exibirDadosLeituraAnterior(
							"<fmt:formatNumber value="${leituraConsumo.leituraAnterior}" maxFractionDigits="0"/>",
							"<fmt:formatDate value="${leituraConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>",
							"<c:out value="${leituraConsumo.anormalidadeLeitura.descricao}"/>",
							"<fmt:formatNumber value="${leituraConsumo.volumeAnteriorApurado}" maxFractionDigits="0"/>",
							"<fmt:formatNumber value="${leituraConsumo.consumoFaturado}" maxFractionDigits="0"/>",
							"<c:out value="${leituraConsumo.diasConsumo}"/>");'>
							
							<img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/>
						</a>	
				   	</display:column>
			    </display:table>
			</fieldset>
		    </c:if>
		    
			<hr class="linhaSeparadoraDetalhamento" />
						
			<fieldset id="conteinerGeral" class="conteinerBloco">
				<legend>Geral:</legend>
				<fieldset class="coluna2">
					<label class="rotulo">M�s/Ano-Ciclo:</label>
					<span class="itemDetalhamento">
						<c:out value="${faturaForm.anoMesReferencia}" /><c:if test="${faturaForm.ciclo ne ''}">-<c:out value="${faturaForm.ciclo}" /></c:if>
					</span><br class="quebraLinha" />
					<label class="rotulo">Data de Emiss�o:</label>
					<span class="itemDetalhamento"><c:out value='${faturaForm.dataEmissao}'/></span><br class="quebraLinha" />
				</fieldset>
				<fieldset class="colunaFinal2">
					<label class="rotulo">Motivo de Inclus�o:</label>
					<span class="itemDetalhamento"><c:out value="${faturaForm.descricaoMotivoInclusao}" /></span><br class="quebraLinha" />
					<c:if test="${faturaForm.numeroContrato ne null && faturaForm.numeroContrato ne ''}">
						<label class="rotulo">Contrato:</label>
						<span class="itemDetalhamento"><c:out value="${faturaForm.numeroContrato}" /></span>
					</c:if>
				</fieldset>
			</fieldset>
			
			<c:if test="${listaDadosGerais ne null}">
				<c:set var="i" value="1" />
				<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
					<hr class="linhaSeparadoraDetalhamento" />
					<fieldset class="conteinerBloco">
						<legend>Fatura <c:out value="${i}"/></legend>
						<label class="rotulo">Data de Vencimento:</label>
						<span class="itemDetalhamento"><fmt:formatDate value="${dadosGeraisItensFatura.dataVencimento}" pattern="dd/MM/yyyy"/></span>
						<label class="rotulo rotuloHorizontal">Tipo da Nota Fiscal:</label>
						<span class="itemDetalhamento">
							<c:choose>
								<c:when test="${dadosGeraisItensFatura.indicadorProduto}"><c:out value="Produto"/></c:when>
								<c:otherwise><c:out value="Servi�o"/></c:otherwise>
							</c:choose>
						</span>
						
						<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" list="${dadosGeraisItensFatura.listaDadosItensFaturaExibicao}" sort="list" id="item" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
						  <display:column title="Descri��o" style="width: 330px">	

						   		<c:choose>
							   		<c:when test="${item.descricaoDesconto ne null}">
							   			<c:out value="${item.descricaoDesconto}"/>
							   		</c:when>	
							   		<c:when test="${item.creditoDebitoARealizar ne null && item.creditoDebitoARealizar.creditoDebitoNegociado ne null 
							   						&& item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica ne null}">
							   			<c:out value="${item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao}"/>
							   		</c:when>		   		
							   		<c:when test="${item.faturaItem ne null && item.faturaItem.rubrica ne null && item.pontoConsumo ne null 
							   						&& item.pontoConsumo.instalacaoMedidor ne null && item.pontoConsumo.instalacaoMedidor.medidor ne null}">
							   			<c:out value="${item.faturaItem.rubrica.descricaoImpressao} - ${item.pontoConsumo.instalacaoMedidor.medidor.numeroSerie}"/>
							   		</c:when>	
							   		<c:otherwise>
							   			<c:out value="${item.rubrica.descricaoImpressao}"/>
							   		</c:otherwise>
						   		</c:choose>	
						   	</display:column>

						   	<display:column title="Tipo" style="width: 100px">

						   		<c:choose>
						   			<c:when test="${item.indicadorCredito eq true}">
				   						<c:out value="Cr�dito"></c:out>
				   					</c:when>
				   					<c:otherwise>
					   					<c:out value="D�bito"></c:out>
				   					</c:otherwise>
						   		</c:choose>
						   	</display:column>

						   	<display:column title="Data In�cio" style="width: 80px">
								<fmt:formatDate value="${item.dataInicial}"></fmt:formatDate>
						   	</display:column>
						   	<display:column title="Data Final" style="width: 80px">
								<fmt:formatDate value="${item.dataFinal}"></fmt:formatDate>
						   	</display:column>
						   	<display:column title="Quant. Dias" style="width: 50px">
								<fmt:formatNumber value="${item.quantidadeDias}" maxFractionDigits="0"></fmt:formatNumber>
						   	</display:column>
						   	
						   	<display:column title="Valor (R$)" style="width: 200px; text-align: right; padding-right: 5px">
								<c:choose>
							   		<c:when test="${item.valorTotal ne null}">
										<fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
									</c:when>	
							   		<c:otherwise>
							   			<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
							   		</c:otherwise>	
							   	</c:choose>										
						   	</display:column>
						</display:table> 
						
						<c:if test="${dadosGeraisItensFatura.listaDadosTributoVO ne null && not empty dadosGeraisItensFatura.listaDadosTributoVO}">
							<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" list="${dadosGeraisItensFatura.listaDadosTributoVO}" sort="list" id="tributoVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" requestURI="#">
							   	<display:column property="tributoAliquota.tributo.descricao" title="Tributo"/>
							   	<display:column title="Al�quota">
							   		<fmt:formatNumber value="${tributoVO.tributoAliquota.percentual}" minFractionDigits="2" maxFractionDigits="2"/>
							   		<span class="rotuloInformativo"><c:out value="%"></c:out></span>
							   	</display:column>
							   	<display:column title="Base de C�lculo">
							   		<fmt:formatNumber value="${tributoVO.baseCalculo}" minFractionDigits="2" maxFractionDigits="2"/>
							   	</display:column>
							   	<display:column title="Valor">
							   		<fmt:formatNumber value="${tributoVO.valor}" minFractionDigits="2" maxFractionDigits="2"/>
							   	</display:column>
					   		</display:table>
						</c:if>
					</fieldset>
					<c:set var="i" value="${i+1}" />
					
					<label class="rotulo">Valor total da fatura:</label>
					<span class="itemDetalhamento"><fmt:formatNumber value='${dadosGeraisItensFatura.valorTotalFatura}' minFractionDigits="2" maxFractionDigits="2"/></span><br class="quebraLinha" />
				</c:forEach>
			</c:if>
			
			<c:if test="${listaFaturasPendentes ne null && not empty(listaFaturasPendentes)}">
				<hr class="linhaSeparadoraDetalhamento" />
				
				<label class="rotulo">Incluir faturas n�o cobradas?</label>
				<input class="campoRadio" type="radio" name="indicadorFaturasPendentes" value="true" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'true'}">checked="checked" </c:if>>
				<label class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" name="indicadorFaturasPendentes" value="false" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'false' || faturaForm.indicadorFaturasPendentes eq ''}">checked="checked" </c:if>>
				<label class="rotuloRadio">N�o</label><br class="quebraLinha" /><br class="quebra2Linhas"/>
				
				<fieldset>
					<legend>Faturas N�o Cobradas</legend>
					<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableDetalhamento" name="listaFaturasPendentes" sort="list" id="fatura" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
						<c:set var="i" value="${i+1}" />					
						<display:column title="N�mero do Documento" >
							<input type="hidden" name="chavesFaturasPendentes" value="${fatura.chavePrimaria}" />
							<c:out value="${fatura.chavePrimaria}"/>
						</display:column>    
						<display:column title="Data de<br />Emiss�o" style="width: 85px">
							<fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy"/>
						</display:column>
						<display:column title="Data de<br />Vencimento" style="width: 85px">
							<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
						</display:column>    
						<display:column title="Ciclo /<br />Refer�ncia" style="width: 85px">
							<c:if test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
								<c:out value="${fatura.cicloReferenciaFormatado}"/>
							</c:if>
						</display:column>
						<display:column title="Valor Total (R$)" style="width: 105px; text-align: right" headerClass="tituloTabelaEsq">
							<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/>
						</display:column>
						<display:column title="Situa��o" style="width: 95px">
			            	<c:out value="${fatura.creditoDebitoSituacao.descricao}"/>
						</display:column>
						<display:column title="Pagamento" style="width: 115px">
			            	<c:out value="${fatura.situacaoPagamento.descricao}"/>
						</display:column>
				    </display:table>
				</fieldset>
			</c:if>
		</fieldset>	
		
		<fieldset class="conteinerBotoes"> 
		    <input name="Button" class="bottonRightCol2" value="<< Voltar" type="button" onclick="voltar();">
		    <input name="Button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit">
	</fieldset>
</form:form>