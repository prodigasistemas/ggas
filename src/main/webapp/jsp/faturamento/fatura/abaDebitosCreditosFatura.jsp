<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>
<<<<<<< HEAD
=======
	$(document).ready(function(){
	/*	$('#creditoDebito').dataTable( {
			"paging": true,
			"language": {
		        "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		});*/
	});

>>>>>>> 7d71dc7b21... Ajustes nas telas de faturas

	function init() {
		<c:forEach items="${fatura.chavesCreditoDebito}" var="chaveCreditoDebito">
			if(document.getElementById('chaveCreditoDebito'+'<c:out value="${chaveCreditoDebito}"/>') != undefined){
				document.getElementById('chaveCreditoDebito'+'<c:out value="${chaveCreditoDebito}"/>').checked = true;	
			}	
		</c:forEach>

		<c:forEach items="${fatura.chavesPrimarias}" var="chave">
			var inputCredito = document.getElementById('chaveCredito'+'<c:out value="${chaveCreditoDebito}"/>');
			if(inputCredito != undefined){
				inputCredito.checked = true;
				var inputValorConciliado = document.getElementById('creditoDebito'+'<c:out value="${chaveCreditoDebito}"/>')
				if (inputValorConciliado != undefined) {
					atualizarValorDebitoCredito(inputCredito, inputValorConciliado.value);
				}
			}	
		</c:forEach>

		<c:forEach items="${fatura.chavesCreditoDebito}" var="chave">
			var checkCreditoDebito = document.getElementById('chaveCreditoDebito'+'<c:out value="${chave}"/>');
			var valorCreditoDebito = $('#creditoDebito'+'<c:out value="${chave}"/>').val();
			var tipoCreditoDebito = $('#tipo'+'<c:out value="${chave}"/>').val();
			atualizarValorTotal(checkCreditoDebito,valorCreditoDebito,tipoCreditoDebito);
		</c:forEach>
	}

	function atualizarValorTotal(obj, paramValor, paramTipo) {
		var inputValorDebitos = document.getElementById('valorDebitos');
		var inputValorCreditos = document.getElementById("valorCreditos");
		var tipoCredito = 'credito';

		if (inputValorDebitos.value == "") {
			inputValorDebitos.value = 0;
		}
		if (inputValorCreditos.value == "") {
			inputValorCreditos.value = 0;
		}
		
		var valorFloat = converterStringParaFloat(paramValor);
		var valorDebitos = parseFloat(inputValorDebitos.value);
		var valorCreditos = parseFloat(inputValorCreditos.value);
		var valorTotal = 0;
		if (obj.checked) {
			if(tipoCredito == paramTipo){
				// Se selecionou um cr�dito.
				valorCreditos = valorCreditos + valorFloat;
				inputValorCreditos.value = valorCreditos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
			} else {
				// Se selecionou um d�bito.
				valorDebitos = valorDebitos + valorFloat;
				inputValorDebitos.value = valorDebitos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
			}
		} else {
			if(tipoCredito == paramTipo){
				// Se desmarcou um cr�dito.
				valorCreditos = valorCreditos - valorFloat;
				inputValorCreditos.value = valorCreditos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
			} else {
				// Se desmarcou um d�bito.
				valorDebitos = valorDebitos - valorFloat;
				inputValorDebitos.value = valorDebitos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
			}
		}
		var valorFormatado = parseFloat(valorTotal).toFixed(2).toString().replace(".",",");
		$('#valorTotal').html('R$ ' + valorFormatado);
	}
	
	addLoadEvent(init);

</script>

<div class="card">
	<div class="card-body">
		<h5 class="card-title">D�bitos/Cr�ditos Fatura:</h5>
		<a class="linkHelp" href="<help:help>/abadbitosecrditoscadastrodafatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>	
		
		<c:if test="${listaCreditoDebito ne null}">	
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover" id="creditoDebito" width="100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" class="text-center"></th>
							<th scope="col" class="text-center">Rubrica</th>
							<th scope="col" class="text-center">Tipo</th>
							<th scope="col" class="text-center">Quantidade</th>
							<th scope="col" class="text-center">Valor Unit�rio (R$)</th>
							<c:if test="${isRefaturar ne null && isRefaturar}" >
								<th scope="col" class="text-center">Valor Lan�ado (R$)</th>
							</c:if>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listaCreditoDebito}" var="creditoDebito">
							<tr>
								<td>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" name="chavesCreditoDebito" id="chaveCreditoDebito${creditoDebito.chavePrimaria}" value="${creditoDebito.chavePrimaria}"
											<c:choose>
												<c:when test="${creditoDebito.creditoDebitoNegociado.creditoOrigem eq null}">
													onclick="atualizarValorTotal(this, '<fmt:formatNumber value="${creditoDebito.valor}" minFractionDigits="2" maxFractionDigits="2"/>', '<c:out value="debito" />');"
												</c:when>
												<c:otherwise>
													onclick="atualizarValorTotal(this, '<fmt:formatNumber value="${creditoDebito.saldoCredito}" minFractionDigits="2" maxFractionDigits="2"/>', '<c:out value="credito" />');"
												</c:otherwise>
											</c:choose>
											<c:if test='${isRefaturar && creditoDebito.isIncluidoNaFatura}'>
												checked="checked"
											</c:if>
										/>
										<c:choose>
											<c:when test="${creditoDebito.creditoDebitoNegociado.creditoOrigem eq null}">
												<input type="hidden" id="tipo${creditoDebito.chavePrimaria}" value="debito"></input>
											</c:when>
											<c:otherwise>
												<input type="hidden" id="tipo${creditoDebito.chavePrimaria}" value="credito"></input>
											</c:otherwise>
										</c:choose>
									</div>
								</td>
								<td>
									<c:out value="${creditoDebito.creditoDebitoNegociado.rubrica.descricao}"/> 
									(<c:out value="${creditoDebito.numeroPrestacao}"/>/<c:out value="${creditoDebito.creditoDebitoNegociado.quantidadeTotalPrestacoes}"/>)
									<c:if test="${creditoDebito.creditoDebitoNegociado.descricao ne null && creditoDebito.creditoDebitoNegociado.descricao ne ''}">
										<c:out value="${creditoDebito.creditoDebitoNegociado.descricao}" />
									</c:if>
								</td>
								<td>
									<c:choose>
										<c:when test="${creditoDebito.creditoDebitoNegociado.creditoOrigem eq null}">D�bito</c:when>
										<c:otherwise>Cr�dito</c:otherwise>
									</c:choose>
								</td>
								<td><fmt:formatNumber value="${creditoDebito.quantidade}" minFractionDigits="2" /></td>
								<td>
									<input type="hidden" id="creditoDebito${creditoDebito.chavePrimaria}" value="<fmt:formatNumber value='${creditoDebito.saldoCredito}' maxFractionDigits='2' minFractionDigits='2'/>"></input>
									<fmt:formatNumber value="${creditoDebito.saldoCredito}" minFractionDigits="2" maxFractionDigits="2"/>
								</td>
								<td>
									<c:if test="${isRefaturar ne null && isRefaturar}" >
										<fmt:formatNumber value="${creditoDebito.valorLancado}" minFractionDigits="2" maxFractionDigits="2"/>
									</c:if>

								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</c:if>
	</div>
	<input type="hidden" id="valorDebitos" value="${valorDebitos}">
	<input type="hidden" id="valorCreditos" value="${valorCredito}">
	<label class="rotulo rotuloHorizontal">Valor Total:</label>
	<span id="valorTotal" class="itemDetalhamento">0.00</span><br class="quebraLinha" />
</div>


	

	
