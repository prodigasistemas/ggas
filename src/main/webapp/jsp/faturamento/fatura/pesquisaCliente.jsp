<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>	
	function selecionarCliente(idSelecionado){

		
		var idCliente = document.getElementById("<c:out value='${param.idCampoIdCliente}' default=""/>");
		var nomeCompletoCliente = document.getElementById("<c:out value='${param.idCampoNomeCliente}' default=""/>");
		var documentoFormatado = document.getElementById("<c:out value='${param.idCampoDocumentoFormatado}' default=""/>");
		var emailCliente = document.getElementById("<c:out value='${param.idCampoEmail}' default=""/>");
		var enderecoFormatado = document.getElementById("<c:out value='${param.idCampoEnderecoFormatado}' default=""/>");
		var tipoCliente = 	document.getElementById("<c:out value='${param.idTipoCliente}' default=""/>");
		var telaVisualizarPontoConsumo = document.getElementById("<c:out value='${param.telaVisualizarPontoConsumo}' default=""/>");
		
		  if( document.getElementById("exibirGridPontoConsumo")){
			document.getElementById("exibirGridPontoConsumo").value = "";
		  }
		if(idSelecionado != '') {		
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];
		               	if(cliente["cnpj"] != undefined ){
		               		documentoFormatado.value = cliente["cnpj"];
		               	} else {
			               	if(cliente["cpf"] != undefined ){
			               		documentoFormatado.value = cliente["cpf"];
			               	} else {
			               		documentoFormatado.value = "";
			               	}
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
		               	tipoCliente.value = cliente["tipoCliente"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	documentoFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
        	tipoCliente.value= "";
       	}
		document.getElementById("idCliente").value = idCliente.value;
        document.getElementById("nomeCliente").value = nomeCompletoCliente.value;
        document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
        document.getElementById("emailCliente").value = emailCliente.value;
        document.getElementById("enderecoFormatadoClienteTexto").value = enderecoFormatado.value;

		if (tipoCliente.value == 'PESSOA FISICA') {
			document.getElementById("indicadorCPF").checked = true;
		} else {
			document.getElementById("indicadorCNPJ").checked = true;
		}

        if( document.getElementById("pontoConsumo")){
        	teste = document.getElementById("teste")

        	if(teste!=null){
        		teste.hidden = true;
            }
        	
        	linha = document.getElementById("linha");

        	if(linha!=null){
        		linha.hidden = true;
            }

        	exibirGridPontoConsumo = document.getElementById("exibirGridPontoConsumo");

        	if(exibirGridPontoConsumo!=null){
        		exibirGridPontoConsumo.value = "nao";
            }
        }
		//Verifica se a fun��o foi passada por par�metro e se ela � realmente v�lida antes de execut�-la.
        if (document.getElementById("funcaoParametro")!= null && document.getElementById("funcaoParametro").value != "") {
			var funcao = document.getElementById("funcaoParametro").value;
			if (funcaoExiste(funcao)) {
				funcao = funcao + '(\'' + idCliente.value + '\')';
				eval(funcao);
			}
        }

        if(telaVisualizarPontoConsumo != null && telaVisualizarPontoConsumo != "" && telaVisualizarPontoConsumo) {
        	pesquisarPontoConsumo();
        }
	}
	
	function exibirPopupPesquisaCliente() {	

		var restricaoTipoPessoa = '';

		<c:choose>
			<c:when test="${param.consultarPessoaFisica && not param.consultarPessoaJuridica}">
				restricaoTipoPessoa = '&pessoaFisica=true';
			</c:when>
			<c:when test="${not param.consultarPessoaFisica && param.consultarPessoaJuridica}">
				restricaoTipoPessoa = '&pessoaJuridica=true';
			</c:when>
		</c:choose>

		funcionalidade = "<c:out value='${param.funcionalidade}'/>";
		if(funcionalidade!=""){
			funcionalidade = "&funcionalidade=" + funcionalidade;
		}
		popup = window.open('exibirPesquisaClientePopup?' + funcionalidade + restricaoTipoPessoa,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

	}

	function limparFormularioDadosCliente(){
		document.getElementById('<c:out value='${param.idCampoIdCliente}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoNomeCliente}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoDocumentoFormatado}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoEnderecoFormatado}' default=""/>').value = "";
        document.getElementById('<c:out value='${param.idCampoEmail}' default=""/>').value = "";
        document.getElementById('nomeCliente').value = "";
        document.getElementById('documentoFormatadoTexto').value = "";
        document.getElementById('emailCliente').value = "";
        document.getElementById('enderecoFormatadoClienteTexto').value = "";
        document.getElementById('enderecoFormatadoClienteTexto').value = "";

		document.getElementById('indicadorCPF').checked = false;
		document.getElementById('indicadorCNPJ').checked = false;
        
        
	}

	function desabilitarPesquisaCliente(){
		document.getElementById('botaoPesquisarCliente').disabled = true;
	}
	
	function habilitarPesquisaCliente(){
		document.getElementById('botaoPesquisarCliente').disabled = false;
	}

</script>

	<div class="card">
		<div class="card-header">
			<h5>Pesquisar Pessoa</h5>
		</div>
		
		<div class="alert alert-primary" role="alert">
			<p><i class="fa fa-info-circle" aria-hidden="true"></i> Clique em <span class="destaqueOrientacaoInicial">Pesquisar Pessoa </span> para selecionar um Cliente.</p>
		</div>
	
		<div class="card-body">
			<input name="<c:out value='${param.idCampoIdCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoIdCliente}' default=""/>" value="${param.idCliente}">
			<input name="<c:out value='${param.idCampoNomeCliente}' default=""/>" type="hidden" id="<c:out value='${param.idCampoNomeCliente}' default=""/>" value="${param.nomeCliente}">
			<input name="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoDocumentoFormatado}' default=""/>" value="${param.documentoFormatadoCliente}">
			<input name="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEnderecoFormatado}' default=""/>" value="${param.enderecoFormatadoCliente}">
			<input name="<c:out value='${param.idCampoEmail}' default=""/>" type="hidden" id="<c:out value='${param.idCampoEmail}' default=""/>" value="${param.emailCliente}">
			<input name="<c:out value='${param.idTipoCliente}' default=""/>" type="hidden" id="<c:out value='${param.idTipoCliente}' default=""/>" value="${param.tipoCliente}">
			<input name="<c:out value='${param.telaVisualizarPontoConsumo}' default=""/>" type=hidden id="<c:out value='${param.telaVisualizarPontoConsumo}' default=""/>" value="${param.telaVisualizarPontoConsumo}">
			
			<input name="funcaoParametro" type="hidden" id="funcaoParametro" value="${param.funcaoParametro}">
			
			<div class="form-row">
				<div class="col-md-12">
			   		<label for="nomeClienteTexto" class="text-right rotulo" >Pessoa:</label>
					<input type="text" class="form-control" id="nomeCliente" name="nomeClienteTexto"  maxlength="50" value="${param.nomeCliente}" disabled>
			    </div>
			</div>
			
			<div class="form-row">
				<div class="col-md-12">
					<label for="enderecoFormatadoClienteTexto" class="text-right rotulo" >Endere�o: </label>
			      	<input type="text" class="form-control" id="enderecoFormatadoClienteTexto"
			      		name="enderecoFormatadoClienteTexto" value="${param.enderecoFormatadoCliente}" disabled>
			    </div>
			</div>
			
			<div class="form-row">
				<div class="col-md-12">
					<label for="documentoFormatadoTexto" class="text-right rotulo">CPF/CNPJ:</label>
				    <input type="text" class="form-control" id="documentoFormatadoTexto"
				      		name="documentoFormatadoTexto" maxlength="18" size="18" disabled
				            value="${param.documentoFormatadoCliente}">
				 </div>
			</div>
			
			
			<div class="form-row">
				<div class="col-md-12">
					<label for="emailClienteTex" class="text-right rotulo">E-mail:</label>
				      	<input type="text" class="form-control" id="emailCliente" maxlength="80" size="40" 
				      	name="emailClienteTexto" disabled value="${param.emailCliente}">
			    </div>
			</div>


		<div class="form-row">
			<label class="text-right rotulo"
				id="rotuloindicadorCondominioAmbosTexto"
				for="indicadorTipoCliente">Tipo Cliente:</label>
			<div class="col-md-12">
				<div class="custom-control custom-radio custom-control-inline">
					<input class="custom-control-input" type="radio"
						id="indicadorCPF"
						name="indicadorTipoCliente" value="true"
						disabled="disabled"
						<c:if test="${param.tipoCliente == 'PESSOA FISICA'}">checked="checked"</c:if>>
					<label class="custom-control-label"
						for="indicadorCPF">F�sica</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input class="custom-control-input" type="radio"
						id="indicadorCNPJ"
						name="indicadorTipoCliente" value="false"
						disabled="disabled"
						<c:if test="${param.tipoCliente == 'PESSOA JURIDICA'}">checked="checked"</c:if>>
					<label class="custom-control-label"
						for="indicadorCNPJ">Jur�dica</label>
				</div>
			</div>
		</div>


		<div class="row mt-3">
				<div class="col align-self-center text-center">
					<button type="button" id="botaoPesquisaCliente" class="btn btn-primary " onclick="exibirPopupPesquisaCliente(); exibirIndicador();">Pesquisar Pessoa</button>
				</div>
			</div>
										
		</div>
</div>
