<%--
  ~ Copyright (C) <2019> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2019 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<div class="card">
	<div class="card-header">
		<h5 class="card-title">Pesquisar Fatura<a class="linkHelp" href="<help:help>/consultadasfaturas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h5>
	</div>
	<div class="card-body">
		<div class="alert alert-primary" role="alert">
			<p><i class="fa fa-info-circle" aria-hidden="true"></i> Selecione pelo menos um cliente clicando em 
				<span class="destaqueOrientacaoInicial">Pesquisar Cliente</span>, 
				e/ou im�vel clicando em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span>,
				e/ou algum dos campos abaixo e clique no bot�o <span class="destaqueOrientacaoInicial">Pesquisar</span>.
				Caso queira selecionar um Ponto de Consumo espec�fico,
				selecione um Im�vel e clique em <span class="destaqueOrientacaoInicial">Ponto de Consumo</span>.</p>
		</div>


<script type="text/javascript">

	$(document).ready(function(){
		 // Dialog
		$("#popupGerarBoleto").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});

		$("#anoMesReferencia").inputmask("9999/99",{placeholder:"_"}); 

		$("#botaoPesquisaCliente").css("background-color", "");
		$("#botaoPesquisaCliente").css("border", "");
		 
		$("input#indicadorRevisaoNao,input#indicadorRevisaoSim,input#idSituacao,input#idSituacaoPagamento,input#dataEmissaoInicial,input#dataEmissaoFinal,input#numeroDocumentoInicial,"+
				"input#numeroDocumentoFinal,input#numeroNotaFiscalInicial,input#numeroNotaFiscalFinal,input#codigoCliente,input#tipoFatura,"+
				"input#numeroCiclo,input#anoMesReferencia").blur(function() {
			habilitarBotaoPesquisar();
		});

		
		$("select#idSituacao,select#idSituacaoPagamento,select#idGrupo,select#idRota,select#tipoFatura").change(function() {
			habilitarBotaoPesquisar();
		});

		$("input#indicadorRevisaoNao,input#indicadorRevisaoSim").click(function() {
			habilitarBotaoPesquisar();
		});		
		
		carregarRotas(document.getElementById('idGrupo'));
		carregarDialogGerarBoleto();
		corrigirPosicaoDatepicker();
		
		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});

		$('input[name$="dataEmissaoInicial"]').inputmask("99/99/9999",{placeholder:"_"});
		$('input[name$="dataEmissaoFinal"]').inputmask("99/99/9999",{placeholder:"_"});
		
		<c:if test="${listaFaturaVO ne null}">

		iniciarDatatable('#faturaVOItemLista', {
			targets: 0,
			orderable: false
		});
		</c:if>
		
	});
	
	function pesquisar(idPontoConsumo) {
		document.getElementById('idPontoConsumo').value = idPontoConsumo;
		pesquisaFatura();
	}
	 
	function pesquisaFatura() {

		this.copiarValores(document.forms[0].numeroDocumentoInicial, document.forms[0].numeroDocumentoFinal);	
		this.copiarValores(document.forms[0].numeroNotaFiscalInicial, document.forms[0].numeroNotaFiscalFinal);
		
		submeter('faturaForm','pesquisarFatura?indicadorPesquisaPontos=false');
	}

	function copiarValores(campo1, campo2){
		var valor1 = trim(campo1.value);
		var valor2 = trim(campo2.value);	
				
		if((valor1 != "") && (valor2 == "")){
			campo2.value = valor1;
		}else if((valor2 != "") && (valor1 == "")){
			campo1.value = valor2
		}
		
	}
	
	function incluirFaturaAvulso(){
		var chaves = document.getElementsByName("chavesPontoConsumo");
		var idCliente = document.getElementById("idCliente").value;
		var idImovel = document.getElementById("idImovel").value;
		var chavesPonto = 0;
		if (chaves != undefined) {
			for (var i = 0; i < chaves.length; i++) {
				if (chaves[i].checked) {
					incluirCliente = false;
					chavesPonto++;
				}
			}
		}
		if ((idCliente != null && idCliente != "" && idCliente > 0 && chavesPonto == 0) ||
				(idImovel != null && idImovel != "" && idImovel > 0 && chavesPonto == 0) ) {
		submeter('faturaForm','exibirInclusaoFaturaAvulso');
		}else {
			var selecao = verificarSelecaoApenasUmGeral('chavesPontoConsumo');
			if (selecao == true) {
				var idPontoConsumo = obterValorUnicoCheckboxSelecionadoGeral('chavesPontoConsumo');
				$("#idPontoConsumo").val(idPontoConsumo);
				submeter('faturaForm','exibirInclusaoFaturaAvulso');
			} else {
				submeter('faturaForm','exibirInclusaoFaturaAvulso');
			}
		}
	}
	
	function incluir() {
		var chaves = document.getElementsByName("chavesPontoConsumo");
		var idCliente = document.getElementById("idCliente").value;
		var idImovel = document.getElementById("idImovel").value;
		var chavesPonto = 0;
		if (chaves != undefined) {
			for (var i = 0; i < chaves.length; i++) {
				if (chaves[i].checked) {
					incluirCliente = false;
					chavesPonto++;
				}
			}
		}
		if ((idCliente != null && idCliente != "" && idCliente > 0 && chavesPonto == 0) ||
				(idImovel != null && idImovel != "" && idImovel > 0 && chavesPonto == 0) ) {
		submeter('faturaForm','exibirInclusaoFatura');
		} else {
			var selecao = verificarSelecaoApenasUmGeral('chavesPontoConsumo');
			if (selecao == true) {
				var idPontoConsumo = obterValorUnicoCheckboxSelecionadoGeral('chavesPontoConsumo');
				$("#idPontoConsumo").val(idPontoConsumo);
				submeter('faturaForm','exibirInclusaoFatura');
			} else {
				submeter('faturaForm','exibirInclusaoFatura');
			}
		}
	}

	function cancelarFatura(){

		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('faturaForm','exibirCancelarFatura');
		}
	}
	
	function excluirFatura(){

		var selecao = verificarSelecao();
		if (selecao == true) {
			if(confirm("Deseja Excluir Definitivamente a(s) Fatura(s) ?")) {
				submeter('faturaForm','excluirFatura');
			}
		}
	}
	
			
	function refaturar() {
		var arrayValorFatura = document.getElementsByName('faturaAvulso');
		var isAvulso = 0;
		var formulario = document.forms[0];
		var flag = 0;
		if (formulario != undefined && formulario.chavesPrimarias != undefined) {
			var total = document.getElementsByName('chavesPrimarias').length;
			if (total != undefined) {
				for ( var i = 0; i < total; i++) {
					if (document.getElementsByName('chavesPrimarias')[i].checked == true) {
						flag++;
						isAvulso = arrayValorFatura[i].value;
					}
				}
			}
		}

		var selecao = verificarSelecao();
		if (selecao == true) {
			if (flag > 1) {
				alert('N�o � poss�vel refaturar duas ou mais faturas simultaneamente.');
			} else if (isAvulso == 'false' || isAvulso == "") {

				submeter('faturaForm',
						'exibirRefaturar');
			} else if (isAvulso == 'true') {

				submeter('faturaForm',
						'exibirRefaturarAvulso');
			}
		}
	}

	function incluirNFD() {
		var formulario = document.forms[0];
		var flag = 0;
		if (formulario != undefined && formulario.chavesPrimarias != undefined) {
			var total = formulario.chavesPrimarias.length;
			if (total != undefined) {
				for ( var i = 0; i < total; i++) {
					if (formulario.chavesPrimarias[i].checked == true) {
						flag++;
					}
				}
			}
		}

		var selecao = verificarSelecao();
		if (selecao == true) {
			if (flag > 1) {
				alert('N�o � poss?vel incluir NFD para duas ou mais faturas simultaneamente.');
			} else {
				submeter('faturaForm',
						'exibirIncluirNFDFatura');
			}
		}
	}
	
	function incluirComplementar() {
		var formulario = document.forms[0];
		var flag = 0;
		if (formulario != undefined && document.getElementsByName('chavesPrimarias') != undefined) {

			var total = document.getElementsByName('chavesPrimarias').length;
			if (total != undefined) {
				for ( var i = 0; i < total; i++) {

					if (document.getElementsByName('chavesPrimarias')[i].checked == true) {
						flag++;
					}
				}
			}
		}

		if (flag > 1) {
			alert('Selecione apenas uma fatura para incluir uma fatura complementar.');
		} else {
			submeter('faturaForm', 'exibirIncluirFaturaComplementar');
		}
	}	

	function mudarStatusRota() {

		if (idGrupo.value != -1) {
			document.getElementById("idRota").disabled = false;
		} else {
			document.getElementById("idRota").disabled = true;
		}
	}
	
	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovel').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('idPontoConsumo').value = "";
		document.getElementById('idSituacao').value = "";
		document.getElementById('idSituacaoPagamento').value = "";
		document.getElementById('tipoDocumento').value = "";
		document.getElementById('tipoFatura').value = "";
		
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		// document.getElementById('situacaoContrato').value = "-1";
		
	}

	function limparCampoPesquisa(ele) {
		limparFormularios2(ele);
		limparCamposPesquisa();
		desativarBotoes();
	}

	function exibirPopupPesquisaImovel() {
		popup = window
				.open(
						'exibirPesquisaImovelCompletoPopup?postBack=true',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");

		if (idSelecionado != '') {
			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {
										idImovel.value = imovel["chavePrimaria"];
										matriculaImovel.value = imovel["chavePrimaria"];
										nomeFantasia.value = imovel["nomeFantasia"];
										numeroImovel.value = imovel["numeroImovel"];
										cidadeImovel.value = imovel["cidadeImovel"];
										indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
										enderecoImovel.value = imovel["enderecoImovel"];
									}
								},
								async : false
							}

					);
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
			enderecoImovel.value = "";
		}

		document.getElementById("nomeImovel").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if (indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

		ativarBotaoPesquisar();
		document.getElementById("botaoPontoConsumo").disabled = false;
		document.getElementById("botaoIncluir").disabled = false;
		document.getElementById("botaoIncluirAvulso").disabled = false;
		document.getElementById("botaoIncluirComplementar").disabled = false;

	}

	function carregarRotas(elem) {
		var codGrupoFaturamento = elem.value;
		
		carregarRotasPorGrupo(codGrupoFaturamento);
		mudarStatusRota();
	}

	function carregarRotasPorGrupo(codGrupoFaturamento) {
		var selectRotas = document.getElementById("idRota");
		var idRota = "${faturaForm.idRota}";

		selectRotas.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectRotas.options[selectRotas.length] = novaOpcao;
		AjaxService.listarRotaPorGrupoFaturamento(codGrupoFaturamento,
				function(rotas) {
					for (key in rotas) {
						var novaOpcao = new Option(rotas[key], key);
						if (key == idRota) {
							novaOpcao.selected = true;
						}
						selectRotas.options[selectRotas.length] = novaOpcao;
					}
					ordernarSelect(selectRotas)
				});
	}

	function ativarBotaoPesquisar() {
		document.getElementById("botaoPesquisar").disabled = false;
	}

	function ativarBotoes() {
		ativarBotaoPesquisar();
		document.getElementById("botaoIncluir").disabled = false;
		document.getElementById("botaoIncluirAvulso").disabled = false;
		document.getElementById("botaoIncluirComplementar").disabled = false;
		document.getElementById("botaoPontoConsumo").disabled = false;
	}

	function detalharFatura(idFatura, idDocumentoFiscal) {
		document.getElementById('chavePrimaria').value = idFatura;
		document.getElementById('idDocumentoFiscal').value = idDocumentoFiscal;
		submeter('faturaForm',
				'exibirDetalhamentoFatura');
	}

	function alterarVencimento() {

		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('faturaForm',
					'exibirAlteracaoVencimentoFatura');
		}
	}

	function revisar() {

		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('faturaForm',
					'exibirRevisaoFatura');
		}
	}

	function manterCheckBoxDasTabelas(valor) {
		<c:forEach items="${faturaForm.chavesPrimarias}" var="chaveFatura">
		if (document.getElementById('chaveFatura'
				+ '<c:out value="${chaveFatura}"/>') != undefined) {
			document.getElementById('chaveFatura'
					+ '<c:out value="${chaveFatura}"/>').checked = valor;
		}
		</c:forEach>
		<c:forEach items="${faturaForm.chavesPontoConsumo}" var="chavePonto">
		if (document.getElementById('chavePontoConsumo'
				+ '<c:out value="${chavePonto}"/>') != undefined) {
			document.getElementById('chavePontoConsumo'
					+ '<c:out value="${chavePonto}"/>').checked = valor;
		}
		</c:forEach>
	}

	function gerarECartas(idFatura, idDocumentoFiscal) {
		document.getElementById('chavePrimaria').value = idFatura;
		document.getElementById('idDocumentoFiscal').value = idDocumentoFiscal;
		submeter('faturaForm', 'gerarECartas');
	}

	function imprimirSegundaVia(idFatura, idDocumentoFiscal) {
        document.getElementById('chavePrimaria').value = idFatura;
        document.getElementById('idDocumentoFiscal').value = idDocumentoFiscal;
        submeter('faturaForm',
            'imprimirSegundaViaFaturaTelaFatura');
    }
	
	function enviarEmailFatura(idFatura, idDocumentoFiscal) {
		document.getElementById('chavePrimaria').value = idFatura;
		document.getElementById('idDocumentoFiscal').value = idDocumentoFiscal;
		submeter('faturaForm',
				'enviarEmailFatura');
	}

	function situacaoInvalida(idSituacao) {
		// Cancelado e pendente
		var situacoesInvalidas = [5,6,9];
		return situacoesInvalidas.includes(idSituacao);
	}

	function exibirMensagemErro(mensagem, scrollToMsg) {
		var $mensagemSpring = $('.divErrors');
		$mensagemSpring.html(
				$("<div>", {class : 'failure notification hideit'}).append(
						$("<p>").append(
								$("<strong>").html("Erro: "), mensagem
						)
				)
		).show().click(function(){
			$mensagemSpring.hide()
		});
		scrollToMsg ? document.getElementsByClassName("divErrors")[0].scrollIntoView() : null;
	}

    function imprimirDanfe(idFatura, idDocumentoFiscal, idSituacao) {
			document.getElementById('chavePrimaria').value = idFatura;
			document.getElementById('idDocumentoFiscal').value = idDocumentoFiscal;
			submeter('faturaForm',
					'imprimirDanfe');
    }

    function enviarEmailDanfe(idFatura, idDocumentoFiscal, idSituacao) {
			document.getElementById('chavePrimaria').value = idFatura;
			document.getElementById('idDocumentoFiscal').value = idDocumentoFiscal;
			submeter('faturaForm',
					'enviarEmailDanfe');
    }

    function gerarECartasEmLote() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('faturaForm', 'gerarECartasEmLote');
		}
	}
	
	function enviarEmailFaturaEmLote(){

		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('faturaForm', 'enviarEmailFaturaEmLote');
		}
	}

	function init() {
		manterCheckBoxDasTabelas(true);

		mudarStatusRota();

		var idCliente = "${faturaForm.idCliente}";
		var idImovel = "${faturaForm.idImovel}";
		if ((idCliente != undefined && idCliente > 0)) {
			ativarBotoes()
		} else if (idImovel != undefined && idImovel > 0) {
			document.getElementById("botaoPesquisar").disabled = false;
			document.getElementById("botaoPontoConsumo").disabled = false;
		}

		manterCheckBoxDasTabelas(true);
		gerarRelatorio();

		var mensagensEstado = $(".mensagens").css("display");
		if (mensagensEstado == "none") {
			<c:if test="${listaPontoConsumo ne null && listaFatura ne null || listaFatura ne null}">
			$.scrollTo($("#conteinerResultadosPesquisaFatura"), 1000);
			</c:if>
			<c:if test="${listaPontoConsumo ne null && listaFatura eq null}">
			$.scrollTo($("#pontoConsumo"), 800);
			</c:if>
		}

		habilitarBotaoPesquisar();

	}

	addLoadEvent(init);

	function gerarRelatorio() {
		<c:if test="${not empty gerarRelatorioEmissaoFatura && gerarRelatorioEmissaoFatura == true}">
		location.href = '<c:url value="/gerarRelatorioEmissaoFatura"/>';
		</c:if>
	}

	function pesquisarPontoConsumo() {
		submeter(
				'faturaForm',
				'pesquisarPontoConsumoFatura?indicadorPesquisaPontos=true');
	}

	function desativarBotoes() {
		document.getElementById("botaoPesquisar").disabled = true;
		document.getElementById("botaoIncluir").disabled = true;
		document.getElementById("botaoIncluirAvulso").disabled = true;
		document.getElementById("botaoIncluirComplementar").disabled = true;
		document.getElementById("botaoPontoConsumo").disabled = true;
		document.getElementById("idRota").value = "-1";
		document.getElementById("idRota").disabled = true;
	}

	function habilitarInclusao() {
		var chavesPontoConsumo = document
				.getElementsByName('chavesPontoConsumo');
		var count = 0;

		if (chavesPontoConsumo != undefined && chavesPontoConsumo.length > 0) {
			for ( var i = 0; i < chavesPontoConsumo.length; i++) {
				if (chavesPontoConsumo[i].checked) {
					count = count + 1;
				}
			}
		}

		var idCliente = $("#idCliente").val();
		if (count > 0 || (idCliente != undefined && idCliente > 0)) {
			$("#botaoIncluir").attr("disabled", false);
			$("#botaoIncluirAvulso").attr("disabled", false);
			$("#botaoIncluirComplementar").attr("disabled", false);
		} else {
			$("#botaoIncluir").attr("disabled", true);
			$("#botaoIncluirAvulso").attr("disabled", true);
			$("#botaoIncluirComplementar").attr("disabled", true);
		}
	}

	function habilitarBotaoPesquisar() {
		var idCliente = document.getElementById("idCliente").value;
		var idImovel = document.getElementById("idImovel").value;
		var dataEmissaoInicial = document.getElementById("dataEmissaoInicial").value;
		var dataEmissaoFinal = document.getElementById("dataEmissaoFinal").value;
		var numeroDocumentoInicial = document
				.getElementById("numeroDocumentoInicial").value;
		var numeroDocumentoFinal = document
				.getElementById("numeroDocumentoFinal").value;
		var numeroNotaFiscalInicial = document
				.getElementById("numeroNotaFiscalInicial").value;
		var numeroNotaFiscalFinal = document
				.getElementById("numeroNotaFiscalFinal").value;
		var codigoCliente = document.getElementById("codigoCliente").value;
		var idSituacao = document.getElementById("idSituacao").value;
		var idSituacaoPagamento = document.getElementById("idSituacaoPagamento").value;
		var tipoFatura = document.getElementById("tipoFatura").value;
		var idGrupo = document.getElementById("idGrupo").value;
		var idRota = document.getElementById("idRota").value;
		var indicadorRevisaoSim = document
				.getElementById("indicadorRevisaoSim").checked;
		var indicadorRevisaoNao = document
				.getElementById("indicadorRevisaoNao").checked;		
		var anoMesReferencia = document.getElementById("anoMesReferencia").value;
		var numeroCiclo = document.getElementById("numeroCiclo").value;

		if ((idCliente != "" && idCliente != "0")
				|| (idImovel != "" && idImovel != "0")
				|| (dataEmissaoInicial != "") || (dataEmissaoFinal != "")
				|| (numeroDocumentoInicial != "")
				|| (numeroDocumentoFinal != "")
				|| (numeroNotaFiscalInicial != "")
				|| (numeroNotaFiscalFinal != "") || (codigoCliente != "")
				|| (idSituacao != "-1") || (idSituacaoPagamento != "-1") || (idGrupo != "-1") || (tipoFatura != "-1")
				|| (idRota != "-1") || (indicadorRevisaoSim == true)
				|| (indicadorRevisaoNao == true)
				|| (numeroCiclo != "-1") || (anoMesReferencia != "")

		) {
			$("button#botaoPesquisar").removeAttr("disabled");
		} else {
			$("button#botaoPesquisar").attr("disabled", "disabled");
			$("button#botaoIncluir").attr("disabled", "disabled");
			$("button#botaoIncluirAvulso").attr("disabled", "disabled");
			$("button#botaoIncluirComplementar").attr("disabled", "disabled");
		}

	}

	function limparFormularios2(ele) {
		var tags = ele.getElementsByTagName('input');
		for ( var i = 0; i < tags.length; i++) {
			switch (tags[i].type) {
			case 'password':
				tags[i].value = '';
				break;
			case 'text':
				tags[i].value = '';
				break;
			case 'checkbox':
				tags[i].checked = false;
				break;
			case 'radio':
				tags[i].checked = false;
				break;
			}
		}
		tags = ele.getElementsByTagName('select');
		for (i = 0; i < tags.length; i++) {
			if (tags[i].type == 'select-one') {
				tags[i].selectedIndex = 0;
			} else {
				for (j = 0; j < tags[i].options.length; j++) {
					tags[i].options[j].selected = -1;
				}
			}
		}
		tags = ele.getElementsByTagName('textarea');
		for (i = 0; i < tags.length; i++) {
			tags[i].value = '';
		}

	}
	
</script>

<form:form method="post" styleId="formPesquisarFatura" action="pesquisarFatura" name="faturaForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarFatura">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${faturaForm.chavePrimaria}">
	<input name="idDocumentoFiscal" type="hidden" id="idDocumentoFiscal" value="${faturaForm.idDocumentoFiscal}"/>
	<input name="tipoDocumento" type="hidden" id="tipoDocumento" value="${1}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	<input name="indicadorPesquisaPontos" type="hidden" id="indicadorPesquisaPontos" value="${faturaForm.indicadorPesquisaPontos}">
	<input name="novaDataVencimento" type="hidden" id="novaDataVencimento" >
	
	<div id="dialogGerarBoleto" title="Gerar Boleto" style="display: none" >
		<input name="novaDataVencimento" type="hidden" id="novaDataVencimento" >
		<%--<span> N�o ser� poss�vel alterar a data de faturas n�o vencidas.</span>--%>
		<table class="dataTableGGAS dataTableDialog dataTableCabecalho2Linhas">
			<thead>
				<tr>
					<th>Numero da Fatura</th>
					<th style="width: 230px">Ponto Consumo</th>
					<th style="width: 100px">Data de Emissao</th>
					<th style="width: 100px">Data de Vencimento</th>
					<th style="width: 100px">Valor Documento Cobranca</th>
					<th style="width: 100px">Valor Recebimento</th>
					<th style="width: 100px">Valor Conciliado</th>
					<th style="width: 100px">Saldo</th>
					<th style="width: 105px">Nova Data Vencimento</th>
					<th style="width: 60px">Imprimir Boleto</th>
				</tr>
			</thead>
			<tbody id="corpoListaFaturasPendentes"></tbody><span style="color: red"> N�o ser� poss�vel alterar a data de vencimento das faturas n�o vencidas.</span>
		</table>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoFechar" class="bottonRightCol" 
			value="Fechar" type="button" onclick="closeDialogGerarBoleto()"
			style="float:right">
	</div>
	
	<div id="popupGerarBoleto" title="Gerar Boleto">
		<label class="rotulo">Descri��o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="descricaoPopup"></span><br />
		<label class="rotulo">Endere�o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="enderecoPopup"></span><br />
		<label class="rotulo">Cep:</label>
		<span class="itemDetalhamento" id="cepPopup"></span><br />
		<label class="rotulo">Complemento:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="complementoPopup"></span><br /><br />
	</div>
	
	<fieldset id="pesquisarFatura" class="conteinerPesquisarIncluirAlterar">
		<div class="container-fluid">
			<div class="row">
			
				<div class="col-md-6">
							<jsp:include page="/jsp/faturamento/fatura/pesquisaCliente.jsp">
								<jsp:param name="idCampoIdCliente" value="idCliente"/>
								<jsp:param name="idCliente" value="${faturaForm.idCliente}"/>
								<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
								<jsp:param name="nomeCliente" value="${cliente.nome}"/>
								<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
								<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
								<jsp:param name="idCampoEmail" value="emailCliente"/>
								<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
								<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
								<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>
								<jsp:param name="tipoCliente" value="${cliente.tipoCliente.descricao}"/>
								<jsp:param name="idTipoCliente" value="tipoCliente"/>
								<jsp:param name="possuiRadio" value="true"/>
								<jsp:param name="funcaoParametro" value="ativarBotoes"/>
							</jsp:include>
				</div>
				
				<div class ="col-md-6">
				
					<div class="card">
						<div class="card-header">
							<h5>Pesquisar Im�vel</h5>
						</div>
						<div class="alert alert-primary" role="alert">
							<p><i class="fa fa-info-circle" aria-hidden="true"></i> Clique em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span> para selecionar um Im�vel.</p>
						</div>
						<div class="card-body">
							<input name="idImovel" type="hidden" id="idImovel" value="${faturaForm.idImovel}">
							<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nome}">
							<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.chavePrimaria}">
							<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
							<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.quadraFace.endereco.cep.nomeMunicipio}">
							<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">
							<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${imovel.modalidadeMedicaoImovel.codigo}">
												
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloNomeFantasia" for="nomeImovel">Descri��o:</label>
									<input class="form-control campoDesabilitado" type="text" id="nomeImovel" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
									<input class="form-control campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
									<input class="form-control campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
									<input class="form-control campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.nomeMunicipio}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<label class="text-right rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioImovelTexto">O im�vel � condom�nio:</label>
								<div class="col-md-12">
									<div class="custom-control custom-radio custom-control-inline">
										<input class="custom-control-input" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="indicadorCondominioImovelTexto1">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input class="custom-control-input" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="indicadorCondominioImovelTexto2">N�o</label>
									</div>
								</div>
							</div>
							
							<div class="row mt-3">
								<div class="col align-self-center text-center">
									<button type="button" class="btn btn-primary " onclick="exibirPopupPesquisaImovel();">Pesquisar Im�vel</button>
								</div>
							</div>
														
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
		
		</br>
		<div class="card">
			<div class="card-body bg-light">
				<div class="row">
					<div class="col-md-6">
						<div class="form-row">
							<div class="col-md-12">
								<label for="idSituacao">Situa��o:</label>
								<select name="idSituacao" id="idSituacao" class="form-control">
									<option value="-1">Selecione</option>				
									<c:forEach items="${listaSituacao}" var="situacao">
										<option value="<c:out value="${situacao.chavePrimaria}"/>" <c:if test="${faturaForm.idSituacao == situacao.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${situacao.descricao}"/>
										</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-row">
							<div class="col-md-12">
								<label for="idSituacaoPagamento">Situa��o Pagamento:</label>
								<select name="idSituacaoPagamento" id="idSituacaoPagamento" class="form-control">
									<option value="-1">Selecione</option>				
									<c:forEach items="${listaSituacaoPagamento}" var="situacaoPagamentoFiltro">
										<option value="<c:out value="${situacaoPagamentoFiltro.chavePrimaria}"/>" <c:if test="${faturaForm.idSituacaoPagamento == situacaoPagamentoFiltro.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${situacaoPagamentoFiltro.descricao}"/>
										</option>
									</c:forEach>
								</select>
							</div>
						</div>



						<div class="form-row">
							<div class="col-md-12">
								<label class="campoObrigatorio" for="dataEmissaoInicial">Per�odo
									de emiss�o:</label>
								<div class="input-group input-group-sm">
									<input class="form-control form-control-sm bootstrapDP" type="text"
										id="dataEmissaoInicial" name="dataEmissaoInicial"
										maxlength="10" value="${faturaForm.dataEmissaoInicial}">
									<div class="input-group-prepend">
										<span class="input-group-text">at�</span>
									</div>
									<c:choose>
										<c:when test="${dtFinal eq null}">
											<input class="form-control form-control-sm bootstrapDP" type="text"
												id="dataEmissaoFinal" name="dataEmissaoFinal"
												maxlength="10" value="${faturaForm.dataEmissaoFinal}">
											<br />
										</c:when>
										<c:otherwise>
											<input class="form-control form-control-sm bootstrapDP" type="text"
												id="dataEmissaoFinal" name="dataEmissaoFinal"
												maxlength="10" value="${dtFinal}">
											<br />
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>


						<div class="form-row">
							<div class="col-md-12">
								<label for="numeroDocumentoInicio">N�mero da fatura:</label>
								<div class="input-group input-group-sm">
									<input class="form-control campoTexto campoHorizontal" type="text" id="numeroDocumentoInicial" name="numeroDocumentoInicial" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${faturaForm.numeroDocumentoInicial}">
										<div class="input-group-prepend">
											<span class="input-group-text">at�</span>
										</div>
									<input class="form-control campoTexto campoHorizontal" type="text" id="numeroDocumentoFinal" name="numeroDocumentoFinal" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${faturaForm.numeroDocumentoFinal}"><br class="quebraLinha2" />
								</div>
							</div>
						</div>
						
						
						<div class="form-row">
							<div class="col-md-12">
								<label for="numeroDocumentoInicio">N�mero da nota fiscal:</label>
								<div class="input-group input-group-sm">
									<input class="form-control campoTexto campoHorizontal" type="text" id="numeroNotaFiscalInicial" name="numeroNotaFiscalInicial" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${faturaForm.numeroNotaFiscalInicial}">
										<div class="input-group-prepend">
											<span class="input-group-text">at�</span>
										</div>
									<input class="form-control campoTexto campoHorizontal" type="text" id="numeroNotaFiscalFinal" name="numeroNotaFiscalFinal" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,10);" value="${faturaForm.numeroNotaFiscalFinal}"><br class="quebraLinha2" />				
								</div>
							</div>
						</div>
						
						<div class="form-row">
							<div class="col-md-12">
								<label for="codigoCliente">C�digo do Cliente:</label>
								<input class="form-control campoTexto" type="text" id="codigoCliente" name="codigoCliente" maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event,9);" value="${faturaForm.codigoCliente}"><br />
							</div>
						</div>
						
						
						<div class="form-row">
							<div class="col-md-12">
								<label for="tipoFatura">Tipo:</label>
								<select name="tipoFatura" id="tipoFatura" class="form-control">
									<option value="-1">Selecione</option>
									<option value="0" <c:if test="${faturaForm.tipoFatura == 0}">selected=</c:if>>Normal</option>
									<option value="1" <c:if test="${faturaForm.tipoFatura == 1}">selected=</c:if>>Devolu��o</option>
									<option value="2" <c:if test="${faturaForm.tipoFatura == 2}">selected=</c:if>>Complemento</option>
								</select>
							</div>
						</div>	
					</div>
					
					
					<div class="col-md-6">
						<div class="form-row">
							<div class="col-md-12">
								<label id="rotuloAnoMesReferencia" for="anoMesReferenciaLido">Ano / M�s de refer�ncia:</label>
								<input class="form-control campoTexto campoHorizontal" type="text" id="anoMesReferencia" name="anoMesReferencia"						
										value="<c:out value="${faturaForm.anoMesReferencia}"/>">
							</div>
						</div>
						
						
						<div class="form-row">
							<div class="col-md-12">
								<label id="rotuloCiclo" for="ciclo">Ciclo:</label>
								<select name="numeroCiclo" id="numeroCiclo" class="form-control campo2Linhas campoHorizontal">
									<option value="-1">Selecione</option>
										<c:forEach items="${listaCiclo}" var="ciclo">
											<option value="<c:out value="${ciclo}"/>"
												<c:if test="${faturaForm.numeroCiclo == ciclo}">selected="selected"</c:if>>
												<c:out value="${ciclo}" />
											</option>
										</c:forEach>
								</select>
							</div>
						</div>	
						
						
						<div class="form-row">
							<div class="col-md-12">
								<label for="idRubrica">Grupo:</label>
								<select name="idGrupo" id="idGrupo" class="form-control" onchange="carregarRotas(this)">
									<option value="-1">Selecione</option>				
									<c:forEach items="${listaGruposFaturamento}" var="grupo">
										<option value="<c:out value="${grupo.chavePrimaria}"/>" <c:if test="${faturaForm.idGrupo == grupo.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${grupo.descricao}"/>
										</option>
									</c:forEach>
								</select>
							</div>
						</div>	
						
						
						<div class="form-row">
							<div class="col-md-12">
								<label for="idRota">Rota:</label>
								<select name="idRota" id="idRota" class="form-control" >
									<option value="-1">Selecione</option>				
									<c:forEach items="${listaRota}" var="rota">
										<option value="<c:out value="${rota.chavePrimaria}"/>" <c:if test="${faturaForm.idRubrica == rota.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${rota.descricao}"/>
										</option>		
									</c:forEach>
								</select>
							</div>
						</div>	
						
						<div class="form-row">
							<label for="habilitado" class="col-md-12">Revis�o:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="indicadorRevisaoSim" name="indicadorRevisao"
										class="custom-control-input" value="true"
										<c:if test="${faturaForm.indicadorRevisao ne null and faturaForm.indicadorRevisao eq true}">checked="checked"</c:if>>
									<label class="custom-control-label" for="indicadorRevisaoSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="indicadorRevisaoNao" name="indicadorRevisao"
										class="custom-control-input" value="false"
										<c:if test="${faturaForm.indicadorRevisao ne null and faturaForm.indicadorRevisao eq false}">checked="checked"</c:if>>
									<label class="custom-control-label" for="indicadorRevisaoNao">N�o</label>
								</div>
							</div>
						</div>
						
						
						<div class="form-row">
							<label for="habilitado" class="col-md-12">Complementar:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="indicadorComplementarSim" name="indicadorComplementar"
										class="custom-control-input" value="true"
										<c:if test="${faturaForm.indicadorRevisao ne null and faturaForm.indicadorComplementar eq true}">checked="checked"</c:if>>
									<label class="custom-control-label" for="indicadorComplementarSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="indicadorComplementarNao" name="indicadorComplementar"
										class="custom-control-input" value="false"
										<c:if test="${faturaForm.indicadorRevisao ne null and faturaForm.indicadorComplementar eq false}">checked="checked"</c:if>>
									<label class="custom-control-label" for="indicadorComplementarNao">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="row mt-3">
				
			<div class="col align-self-end text-right">
			
				<button class="btn btn-primary btn-sm" id="botaoPontoConsumo"
						type="button" onclick="pesquisarPontoConsumo();" disabled="disabled">
						Pontos de Consumo
				</button>
				<button class="btn btn-primary btn-sm" id="botaoPesquisar"
					type="button" onclick="pesquisaFatura();">
					<i class="fa fa-search"></i> Pesquisar
				</button>
				<button class="btn btn-secondary btn-sm" id="botaoLimpar"
					type="button" onclick="limparCampoPesquisa(this.form);">
					<i class="fa fa-times"></i> Limpar
				</button>
				

								
			</div>
		</div>

	
		<c:if test="${listaPontoConsumo ne null}">
			<hr class="linhaSeparadora1" />
			<fieldset class="conteinerBloco">
				<div class="alert alert-primary" role="alert">
					<p class="orientacaoInicial">Selecione um Ponto de Consumo na lista abaixo para exibir as Faturas. Para incluir uma fatura associada ao Ponto de Consumo marque a caixa de sele��o � esquerda do Ponto de Consumo e clique em <span class="destaqueOrientacaoInicial">Incluir</span>.</p>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="pontoConsumo">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center"></th>
								<th scope="col" class="text-center">Ponto de Consumo</th>
								<th scope="col" class="text-center">C�digo do Ponto de Consumo</th>
								<th scope="col" class="text-center">Segmento</th>
								<th scope="col" class="text-center">Situa��o</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
								<tr>
									<td class="text-center">
										<input type="radio" class="form-check-input" name="chavesPontoConsumo" id="chavePontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}" onclick="habilitarInclusao();"/>
									</td>
									<td class="text-center">
										<a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.descricao} - ${pontoConsumo.imovel.nome}"/>
							            </a>
									</td>
									<td class="text-center">
										<a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.codigoPontoConsumo}"/>
							            </a>
									</td>
									<td class="text-center">
										<a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.segmento.descricao}"/>
							            </a>
									</td>
									<td class="text-center">
										<a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
							            </a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</fieldset>
		
		</c:if>

		<c:if test="${listaFaturaVO ne null}">
			<hr class="linhaSeparadora1" />
			<fieldset id="conteinerResultadosPesquisaFatura" class="conteinerBloco">
				<h5>Faturas</h5>
				
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="faturaVOItemLista">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">
									<div
										class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
										<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
											class="custom-control-input"> <label
											class="custom-control-label p-0" for="checkAllAuto"></label>
									</div>
								</th>
								<th scope="col" class="text-center">Fatura</th>
								<th scope="col" class="text-center">Nota Fiscal</th>
								<th scope="col" class="text-center">Ponto de Consumo</th>
								<th scope="col" class="text-center">Data de<br />Emiss�o</th>
								<th scope="col" class="text-center">Data de<br />Vencimento</th>
								<th scope="col" class="text-center">Ciclo /<br />Refer�ncia</th>
								<th scope="col" class="text-center">Per�odo de Medi��o</th>
								<th scope="col" class="text-center">Valor (R$)</th>
								<th scope="col" class="text-center">Situa��o</th>
								<th scope="col" class="text-center">Pagamento</th>
								<th scope="col" class="text-center">Revis�o</th>
								<th scope="col" class="text-center">Vencimento<br/> da Revis�o</th>
								<th scope="col" class="text-center">2� Via</th>
								<th scope="col" class="text-center">DANFE</th>
								<c:if test="${participaECartas eq '1'}">
								<th scope="col" class="text-center">e-Cartas</th>
								</c:if>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaFaturaVO}" var="faturaVOItemLista">
								<tr>
									<td class="text-center">
										<div
											class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
											data-identificador-check="chk${faturaVOItemLista.fatura.chavePrimaria}">
											<input id="chk${faturaVOItemLista.fatura.chavePrimaria}"
												type="checkbox" name="chavesPrimarias"
												class="custom-control-input"
												value="${faturaVOItemLista.fatura.chavePrimaria}"> <label
												class="custom-control-label p-0"
												for="chk${faturaVOItemLista.fatura.chavePrimaria}"></label>
										</div>
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<c:out value="${faturaVOItemLista.fatura.chavePrimaria}"/>
											<c:if test="${faturaVOItemLista.fatura.faturaPrincipal ne null}">(Complementar)</c:if>
										</a>
										<input type="hidden" name="faturaAvulso" id="faturaAvulso" value="${faturaVOItemLista.fatura.faturaAvulso}" >
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<c:out value="${faturaVOItemLista.documentoFiscal.numero}"/>
											<c:if test="${faturaVOItemLista.documentoFiscal.tipoOperacao ne null && faturaVOItemLista.documentoFiscal.tipoOperacao.chavePrimaria==50}">
												(<c:out value="${faturaVOItemLista.documentoFiscal.tipoOperacao.descricaoAbreviada}"/>)
											</c:if>							
										</a>
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<c:choose>
												<c:when test="${faturaVOItemLista.fatura.historicoMedicao ne null}">
													<c:out value="${faturaVOItemLista.fatura.pontoConsumo.descricao}"/>
												</c:when>
												<c:otherwise>
													<c:out value="${faturaVOItemLista.fatura.cliente.nome}"/>
												</c:otherwise>
											</c:choose> 	
										</a>
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<c:choose>
												<c:when test="${faturaVOItemLista.documentoFiscal.tipoOperacao ne null && faturaVOItemLista.documentoFiscal.tipoOperacao.chavePrimaria==50}">
													<fmt:formatDate value="${faturaVOItemLista.documentoFiscal.dataEmissao}" pattern="dd/MM/yyyy"/>	
												</c:when>
												<c:otherwise>
													<fmt:formatDate value="${faturaVOItemLista.fatura.dataEmissao}" pattern="dd/MM/yyyy"/>
												</c:otherwise>
											</c:choose>
										</a>
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<fmt:formatDate value="${faturaVOItemLista.fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
										</a>
													</td>
													<td class="text-center">
													<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<c:if test="${faturaVOItemLista.fatura.anoMesReferencia ne null && faturaVOItemLista.fatura.numeroCiclo ne null}">
												<c:out value="${faturaVOItemLista.fatura.cicloReferenciaFormatado}"/>
											</c:if>
										</a>
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<fmt:formatDate value="${faturaVOItemLista.dataInicioMedicao}" pattern="dd/MM"/> a <fmt:formatDate value="${faturaVOItemLista.dataFinalMedicao}" pattern="dd/MM"/>
										</a>
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
											<fmt:formatNumber value="${faturaVOItemLista.fatura.valorTotal}" />
										</a>
									</td>
									<td class="text-center">
										<a href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
											<c:out value="${faturaVOItemLista.fatura.creditoDebitoSituacao.descricao}"/>
										</a>
									</td>
									<td class="text-center">
										<c:if test="${faturaVOItemLista.fatura.creditoDebitoSituacao.valido}">
											<a id="situacao_pagamento_${faturaVOItemLista.fatura.chavePrimaria}" 
												href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out 
												value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
												<c:out value="${faturaVOItemLista.fatura.situacaoPagamento.descricao}"/>
											</a>
										</c:if>
									</td>
									<td class="text-center">
										<a title='' href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'><span class="linkInvisivel">&nbsp;</span>
											<c:out value="${faturaVOItemLista.quantidadeRevisao}"/>
										</a>
									</td>
									<td class="text-center">
										<c:if test="${faturaVOItemLista.fatura.dataRevisao ne null}">
											<a title='MOTIVO REVIS�O: <c:out value="${faturaVOItemLista.fatura.motivoRevisao.descricao}"/>&#13;RESPONS�VEL: <c:out value='${faturaVOItemLista.fatura.responsavelRevisao.nome}'/>' href='javascript:detalharFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, "<c:out value="${faturaVOItemLista.documentoFiscal.chavePrimaria}"/>");'><span class="linkInvisivel">&nbsp;</span>
											<fmt:formatDate value="${faturaVOItemLista.fatura.dataRevisao}" pattern="dd/MM/yyyy"/>
											</a>
										</c:if>
									</td>
									<td class="text-center">
										<c:if test="${faturaVOItemLista.fatura ne null}">
											<a href='javascript:imprimirSegundaVia(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'>
												<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via" title="Imprimir 2� Via" border="0" style="margin-top: 5px;" >
											</a>
											<a href='javascript:enviarEmailFatura(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value='${faturaVOItemLista.documentoFiscal.chavePrimaria}'/>);'>
												<img src="<c:url value="/imagens/icon_email.gif" />" alt="Enviar Fatura por Email" title="Enviar Fatura por Email" border="0" style="margin-top: 5px;" >
											</a>
										</c:if>
									</td>
									<td class="text-center">
										<c:if test="${faturaVOItemLista.fatura ne null}">
											<a href='javascript:imprimirDanfe(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>,
												<c:out value="${faturaVOItemLista.documentoFiscal.chavePrimaria}"/>, <c:out value="${faturaVOItemLista.fatura.creditoDebitoSituacao.chavePrimaria}"/>);'>
												<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via" title="Imprimir DANFE" border="0" style="margin-top: 5px;" >
											</a>
											<a href='javascript:enviarEmailDanfe(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>,
												<c:out value="${faturaVOItemLista.documentoFiscal.chavePrimaria}"/>, <c:out value="${faturaVOItemLista.fatura.creditoDebitoSituacao.chavePrimaria}"/>);'>
												<img src="<c:url value="/imagens/icon_email.gif" />" alt="Enviar DANFE por Email" title="Enviar DANFE por Email" border="0" style="margin-top: 5px;" >
											</a>
										</c:if>
									</td>
									<c:if test="${participaECartas eq '1'}">
										<td class="text-center">
										<c:if test="${faturaVOItemLista.fatura ne null}">
											<a href='javascript:gerarECartas(<c:out value='${faturaVOItemLista.fatura.chavePrimaria}'/>, <c:out value="${faturaVOItemLista.documentoFiscal.chavePrimaria}"/>);'>
												<img width="16" src="<c:url value="/imagens/icone_zip.png" />" alt="Gerar e-Cartas" title="Gerar e-Cartas" border="0" style="margin-top: 5px;" >
												</a>
											</c:if>
										</td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
		    </fieldset>
		</c:if>
	</fieldset>
	
	</br>
	
	
		<div class="row mt-3">
			<div class="col align-self-end text-left">
				<vacess:vacess param="exibirInclusaoFatura">
					<button class="btn btn-primary" name="buttonIncluir" id="botaoIncluir"
							type="button" onclick="incluir()">
							Incluir
					</button>
					<button class="btn btn-primary" name="buttonIncluirAvulso" id="botaoIncluirAvulso"
						type="button" onclick="incluirFaturaAvulso()" >
						Incluir Fatura Avulsa
					</button>
					<button class="btn btn-primary" name="buttonIncluirComplementar" id="botaoIncluirComplementar"
						type="button" onclick="incluirComplementar()">
						Incluir Fatura Complementar
					</button>
				</vacess:vacess>
			</div>
		</div>	
		</br>
	
	
		
	<div class="form-group">
		<c:if test="${listaFaturaVO ne null}">
		<div class="row mt-3">
			<div class="col align-self-end text-left">
				<vacess:vacess param="exibirAlteracaoVencimentoFatura">
					<button class="btn btn-primary" name="buttonCancelar"
							type="button" onclick="alterarVencimento()">
							Prorrogar Data Vencimento
					</button>
				</vacess:vacess>
				
				<vacess:vacess param="exibirRevisaoFatura">
					<button class="btn btn-primary" name="buttonCancelar"
							type="button" onclick="revisar()">
							Revisar
					</button>
				</vacess:vacess>

				<vacess:vacess param="exibirCancelarFatura">
					<button class="btn btn-primary" name="buttonCancelar"
							type="button" onclick="cancelarFatura()">
							Cancelar Fatura
					</button>
				</vacess:vacess>					
				
				<button class="btn btn-primary" name="botaoEnviar"
					type="button" onclick="enviarEmailFaturaEmLote()" >
					Enviar Fatura Em Lote
				</button>
				
				<vacess:vacess param="exibirRefaturar">
					<button class="btn btn-primary" name="buttonCancelar"
							type="button" onclick="refaturar()">
							Refaturar
					</button>
				</vacess:vacess>
				
				<vacess:vacess param="incluirNFDFatura">
					<button class="btn btn-primary" name="buttonCancelar"
							type="button" onclick="incluirNFD()">
							Incluir NFD
					</button>
				</vacess:vacess>
				
            <c:if test="${indicadorProducao eq '0'}">
				<vacess:vacess param="excluirFatura">
					<button class="btn btn-primary" name="buttonCancelar"
							type="button" onclick="excluirFatura()">
							Excluir Fatura
					</button>
				</vacess:vacess>            
            </c:if>	
            
				<button class="btn btn-primary" name="buttonGerarFaturaPendente"
					type="button" onclick="exibirDialogGerarBoletoFatura()" >
					Gerar Boleto
				</button>
				
            <c:if test="${participaECartas eq '1'}">				
				<button class="btn btn-primary" name="botaoEnviar"
					type="button" onclick="exibirDialogGerarBoletoFatura()" >
					Gerar e-Cartas
				</button>
			</c:if>								            															
			</div>
		</div>	
		</c:if>
	</div>

</form:form>

	</div>
</div>