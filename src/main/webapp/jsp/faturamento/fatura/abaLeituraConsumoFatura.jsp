<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script>

	$(document).ready(function(){
/* 		$("input[name=dataLeitura]").datepicker({
			changeYear: true, 
			maxDate: '+0d', 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		}); */

		// Dialog - configura��es
		$("#incluirFaturaLeituraPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
		/* $('#listaLeituraConsumoFatura').dataTable( {
			"paging": false,
			"language": {
	            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		}); */
		
		
	});

	function exibirDadosLeituraAnterior(leituraAnterior, dataLeituraAnterior, anormalidadeAnterior, volumeAnterior, consumoFaturado, diasConsumo) {
		$("#leituraAnteriorPopup").html(leituraAnterior);
		$("#dataLeituraAnteriorPopup").html(dataLeituraAnterior);
		$("#anormalidadeAnteriorPopup").html(anormalidadeAnterior);
		$("#volumeAnteriorPopup").html(volumeAnterior);
		$("#consumoFaturadoPopup").html(consumoFaturado);
		$("#diasConsumoPopup").html(diasConsumo);
		exibirJDialog("#incluirFaturaLeituraPopup");
	}

	function habilitarLeiturista(valor, idPontoConsumo) {
		if (valor) {
			$("#leiturista"+idPontoConsumo).val("-1");
			$("#leiturista"+idPontoConsumo).attr("disabled",true);
			$("#informadoCliente"+idPontoConsumo).val("true");
		} else {
			$("#leiturista"+idPontoConsumo).attr("disabled",false);
			$("#informadoCliente"+idPontoConsumo).val("false");
		}
	}

	function carregarHistoricoConsumo(idPontoConsumo) {
		$("#idPontoConsumo").val(idPontoConsumo);
		submeter("faturaForm","carregarHistoricoConsumoFatura");
	}

	function setarLeiturista(idLeiturista, idPontoConsumo) {
		$("#idLeiturista"+idPontoConsumo).val(idLeiturista);
	}

	function init() {
		var medicaoDiaria = "${faturaForm.medicaoDiaria}";

		if (medicaoDiaria != undefined && medicaoDiaria == "true") {
			$(".campoData:disabled + img").each(function(){
				$(this).css("display","none");
			});
		}

		<c:if test="${listaHistoricoConsumo ne null}">
			$.scrollTo($('#conteinerConsumo'),800);
		</c:if>
	}
	
	addLoadEvent(init);

</script>

<div id="incluirFaturaLeituraPopup" title="Leitura">
	<label class="rotulo">Leitura Anterior:</label>
	<span class="itemDetalhamento" id="leituraAnteriorPopup"></span><br />
	<label class="rotulo">Data da Leitura Anterior:</label>
	<span class="itemDetalhamento" id="dataLeituraAnteriorPopup"></span><br />
	<label class="rotulo">Anormalidade da Leitura:</label>
	<span class="itemDetalhamento" id="anormalidadeAnteriorPopup"></span><br /><br />
	<label class="rotulo">Volume Anterior Apurado:</label>
	<span class="itemDetalhamento" id="volumeAnteriorPopup"></span><br /><br />
	<label class="rotulo">Consumo Faturado:</label>
	<span class="itemDetalhamento" id="consumoFaturadoPopup"></span><br /><br />
	<label class="rotulo">Dias de Consumo:</label>
	<span class="itemDetalhamento" id="diasConsumoPopup"></span><br /><br />
</div>

<div class="card" id="conteinerLeitura">
	<div class="card-body">
		<h5 class="card-title">Leitura:</h5>
		<div class="alert alert-primary" role="alert">
			<i class="fa fa-exclamation-circle"></i> Clique em um dos Pontos de Consumo abaixo (coluna Im�vel - Ponto Consumo) para exibir os dados de Consumo.
		</div>
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover" id="listaLeituraConsumoFatura" width="100%">
				<thead class="thead-ggas-bootstrap">
					<tr>
						<th scope="col" class="text-center">Im�vel - Ponto Consumo</th>
						<th scope="col" class="text-center"><span class='campoObrigatorioSimbolo'>* </span>Leitura Atual</th>
						<th scope="col" class="text-center">Informada p/ Cliente</th>
						<th scope="col" class="text-center"><span class='campoObrigatorioSimbolo'>* </span>Data da<br/>Leitura Atual</th>
						<th scope="col" class="text-center">Leiturista</th>
						<th scope="col" class="text-center">Consumo<br/>Apurado (m<span class='expoente'>3</span>)</th>
						<th scope="col" class="text-center">Leitura<br/>Anterior</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listaLeituraConsumoFatura}" var="leituraConsumo">
						<tr>
							<td class="text-center">
								<input type="hidden" name="chavesPontoConsumo" id="idPontoConsumo${leituraConsumo.idPontoConsumo}" value="${leituraConsumo.idPontoConsumo}"/>
								<a style="word-wrap: break-word; width: 180px; text-align: left; overflow: hidden" href='javascript:carregarHistoricoConsumo(<c:out value="${leituraConsumo.idPontoConsumo}"/>);'>
									<c:out value="${leituraConsumo.descricaoPontoConsumo}"/>
								</a>
							</td>
							<td class="text-center">
							<input name="leituraAtual" id="leituraAtual${leituraConsumo.idPontoConsumo}" type="hidden" value="<fmt:formatNumber value='${leituraConsumo.leituraAtual}' minFractionDigits='3'/>"/>
					   		<input class="form-control campoValor" type="text" size="14" maxlength="<c:out value="${leituraConsumo.qtdDigitosMedidor}"/>" onkeypress="return formatarCampoInteiro(event);" 
					   			onblur="$('#leituraAtual'+${leituraConsumo.idPontoConsumo}).val(this.value);" 
					   			value="<fmt:formatNumber value='${leituraConsumo.leituraAtual}' maxFractionDigits='0'/>" 
					   			<c:if test="${faturaForm.medicaoDiaria eq true}">disabled="disabled"</c:if>/>
							</td>
							<td class=text-center">
								<input name="indicadorLeituraCliente" id="informadoCliente${leituraConsumo.idPontoConsumo}" type="hidden" value="<c:choose><c:when test='${leituraConsumo.informadoCliente}'>true</c:when><c:otherwise>false</c:otherwise></c:choose>"/>
						   		<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
							   		<input name="informadoCliente" type="checkbox" class="form-check-input"  <c:if test="${leituraConsumo.informadoCliente}">checked="checked"</c:if> 
							   			onclick="habilitarLeiturista(this.checked,'<c:out value="${leituraConsumo.idPontoConsumo}"/>');" 
							   			<c:if test="${faturaForm.medicaoDiaria eq true}">disabled="disabled"</c:if> />
						   		</div>
							</td>
							<td class="text-center">
								<input name="dataLeituraAtual" id="dataLeituraAtual${leituraConsumo.idPontoConsumo}" type="hidden" value="<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' pattern='dd/MM/yyyy'/>" />
						   		<input class="form-control bootstrapDP" name="dataLeitura" type="text" size="8" maxlength="10" value="<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' 
						   			pattern='dd/MM/yyyy'/>" <c:if test="${faturaForm.medicaoDiaria eq true}">disabled="disabled"</c:if> 
						   			onblur="$('#dataLeituraAtual'+${leituraConsumo.idPontoConsumo}).val(this.value);" />
							</td>
							<td class="text-center">
								<input id="idLeiturista${leituraConsumo.idPontoConsumo}" name="idLeiturista" type="hidden" value="${leituraConsumo.leiturista.chavePrimaria}"/>
						   		<select id="leiturista${leituraConsumo.idPontoConsumo}" class="form-control" <c:if test="${leituraConsumo.informadoCliente || faturaForm.medicaoDiaria eq true}">disabled="disabled"</c:if> onchange="setarLeiturista(this.value,'${leituraConsumo.idPontoConsumo}');">
							    	<option value="-1">Selecione</option>				
									<c:forEach items="${listaLeiturista}" var="leiturista">
										<option value="<c:out value='${leiturista.chavePrimaria}'/>" <c:if test="${leituraConsumo.leiturista.chavePrimaria == leiturista.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${leiturista.funcionario.nome}"/>
										</option>		
									</c:forEach>
								</select>
							</td>
							<td class="text-center">
								<div class="container">
								<input name="consumoApurado"  id="consumoApurado${leituraConsumo.idPontoConsumo}" type="hidden" value="<fmt:formatNumber value='${leituraConsumo.consumoApurado}'/>" disabled/>
						   		<input name="consumoApuradoAnterior" id="consumoApuradoAnterior${leituraConsumo.idPontoConsumo}" type="hidden" value="<fmt:formatNumber value='${leituraConsumo.consumoApurado}' />" />
						   		<input class="form-control campoValor" type="text" size="12" maxlength="14" onkeypress="return formatarCampoDecimalPositivo(event, this, 9, 9)" 
						   			onblur="aplicarMascaraNumeroDecimal(this, 9); $('#consumoApurado'+${leituraConsumo.idPontoConsumo}).val(this.value);" value="<fmt:formatNumber value='${leituraConsumo.consumoApurado}' 
						   			/>" <c:if test="${faturaForm.medicaoDiaria eq true}">disabled="disabled"</c:if> />
								</div>
							</td>
							<td class="text-center">
								<a href='javascript:exibirDadosLeituraAnterior(
									"<fmt:formatNumber value="${leituraConsumo.leituraAnterior}" maxFractionDigits="0"/>",
									"<fmt:formatDate value="${leituraConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>",
									"<c:out value="${leituraConsumo.anormalidadeLeitura.descricao}"/>",
									"<fmt:formatNumber value="${leituraConsumo.volumeAnteriorApurado}" minFractionDigits="4"/>",
									"<fmt:formatNumber value="${leituraConsumo.consumoFaturado}" minFractionDigits="4"/>",
									"<c:out value="${leituraConsumo.diasConsumo}"/>");'>
									
									<img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/>
								</a>
							</td>
						</tr>
					</c:forEach>
						<c:set var="i" value="${i+1}" />
				</tbody>
			</table>
		</div>
		
		<button type="button" name="Button" class="btn btn-primary bottonRightCol2 botaoGrande1" onclick="consistir();">Consistir</button>
	</div>
</div>
	
<c:if test="${listaHistoricoConsumo ne null}">
	<hr class="linhaSeparadora2" />
	<fieldset id="conteinerConsumo">
		<legend>Consumo:</legend>

		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover" id="">
				<thead class="thead-ggas-bootstrap">
					<tr>
						<th scope="col" class="text-center">Data</th>
						<th scope="col" class="text-center">Leitura</th>
						<th scope="col" class="text-center">Consumo Apurado</th>
						<th scope="col" class="text-center">Consumo Di�rio</th>
						<th scope="col" class="text-center">Cr�dito Consumo</th>
						<th scope="col" class="text-center">Fator PTZ</th>
						<th scope="col" class="text-center">Fator PCS</th>
						<th scope="col" class="text-center">Fator Corre��o</th>
						<th scope="col" class="text-center">Dias de Consumo</th>
						<th scope="col" class="text-center">Tipo de Consumo</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listaHistoricoConsumo}" var="consumo">
						<tr>
							<td><fmt:formatDate value="${consumo.historicoAtual.dataLeituraInformada}" pattern="dd/MM/yyyy" /></td>
							<td><fmt:formatNumber value="${consumo.historicoAtual.numeroLeituraInformada}" maxFractionDigits="0" /></td>
							<td><fmt:formatNumber value="${consumo.consumoApurado}" /></td>
							<td><fmt:formatNumber value="${consumo.consumoDiario}" minFractionDigits="4" maxFractionDigits="4" /></td>
							<td><fmt:formatNumber value="${consumo.consumoCredito}" minFractionDigits="4" maxFractionDigits="4" /></td>
							<td><fmt:formatNumber value="${consumo.fatorPTZ}" minFractionDigits="4" maxFractionDigits="4" /></td>
							<td><fmt:formatNumber value="${consumo.fatorPCS}" minFractionDigits="4" maxFractionDigits="4" /></td>
							<td><fmt:formatNumber value="${consumo.fatorCorrecao}" minFractionDigits="4" maxFractionDigits="4" /></td>
							<td><fmt:formatNumber value="${consumo.fatorCorrecao}" minFractionDigits="4" maxFractionDigits="4" /></td>
							<td><c:out value="${consumo.tipoConsumo.descricao}"></c:out></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</fieldset>
</c:if>