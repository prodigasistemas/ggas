<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
	
<script type="text/javascript">

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=0');

	function cancelar() {
		submeter('faturaForm','pesquisarFatura');
	}

	function incluir() {
		submeter('faturaForm','incluirFaturaComplementar');
	}
	
	function verificaSelecaoMotivo() { 
		if (document.getElementById('idMotivoComplemento').options[document.getElementById("idMotivoComplemento").selectedIndex].text == 'Complemento de Volume') {
			document.getElementById('valorFatura').value = '';
			$("#valorFatura").css("background-color", "#EEEEE0");
			document.getElementById('valorFatura').disabled = true;
			document.getElementById('volumeApurado').disabled = false;
		} else {
			document.getElementById('valorFatura').disabled = false;
			$("#valorFatura").css("background-color", "#FFFFFF");
			document.getElementById('volumeApurado').value = '';
			document.getElementById('volumeApurado').disabled = true;
		}
		
	}
	
	function init() {
		document.getElementById('valorFatura').disabled = true;
		document.getElementById('volumeApurado').disabled = true;
	}
	
	addLoadEvent(init);
	
</script>

<div class="responsivo">
	<form:form method="post" action="incluirFaturaComplementar" name="faturaForm">
		<input name="acao" type="hidden" id="acao" value="incluirFaturaComplementar">
		<input name="indexLista" type="hidden" id="indexLista" value="-1">
		<input name="postBack" type="hidden" id="postBack" value="false">
		<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
		<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}" />
		<input name="idFatura" type="hidden" id="idFatura" value="${faturaForm.idFatura}" />
		<input name="idFatura" type="hidden" id="idFatura" value="${faturaForm.chavePrimaria}" />
		<input name="numeroCiclo" type="hidden" id="ciclo" value="${faturaForm.numeroCiclo}" />
		<input name="anoMesReferencia" type="hidden" id="referencia" value="${faturaForm.anoMesReferencia}" />
		<input name="indicadorRefuramento" type="hidden" id="indicadorRefuramento" value="${indicadorRefuramento}"/>
		<input name="tipoDocumento" type="hidden" id="tipoDocumento" value="${1}">
		<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaLista">
			<input name="chavesPrimarias" type="hidden" id="chavesPrimarias${chavePrimariaLista}" value="${chavePrimariaLista}">
		</c:forEach>
		
		<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
		<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${dataEmissaoInicial}"/>">
		<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${dataEmissaoFinal}"/>">
		<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${numeroDocumentoInicial}"/>">
		<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${numeroDocumentoFinal}"/>">
		<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${codigoCliente}"/>">
		<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
		<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
		<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${indicadorRevisao}"/>">
		<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
		<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">


		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Incluir Fatura de Complemento</h5>
			</div>
			<div class="card-body bg-light">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos abaixo para realizar a inclus�o.
				</div>

				<div class="card">
					<div class="card-body bg-light">
						<div class="row">
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="idMotivoComplemento">Motivo:<span
											class="text-danger">*</span></label> <select
											name="idMotivoComplemento" id="idMotivoComplemento"
											class="form-control form-control-sm" onChange="verificaSelecaoMotivo();">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaMotivoComplemento}"
												var="motivoComplemento">
												<option
													value="<c:out value="${motivoComplemento.chavePrimaria}"/>"
													<c:if test="${faturaForm.idMotivoComplemento == motivoComplemento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${motivoComplemento.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-10">
										<label for="volumeApurado">Volume: <span
											class="text-danger">* </span></label>
										<div class="input-group input-group-sm">
											<input type="text" aria-label="volumeApurado"
												id="volumeApurado"
												class="form-control form-control-sm"
												name="volumeApurado" maxlength="13" size="13"
												onblur="aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(this,4);"
												onkeypress="return formatarCampoDecimal(event,this,8,4);"
												value="${faturaForm.volumeApurado[0]}"> 
										</div>
									</div>
								</div>
								
								<div class="form-row">
									<div class="col-md-10">
										<label for="valorFatura">Valor: <span
											class="text-danger">* </span></label>
										<div class="input-group input-group-sm">
											<input type="text" aria-label="valorFatura"
												id="valorFatura"
												class="form-control form-control-sm"
												name="valorFatura" maxlength="16"
												value="${faturaForm.valor}"
												onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 2);"
												onblur="aplicarMascaraNumeroDecimal(this, 2);"
												onchange="aplicarMascaraNumeroDecimal(this, 2);">
										</div>
									</div>
								</div>								

							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="observacoesComplemento"
											id="rotuloObservacaoVigencia">Observa��es:</label>
										<textarea class="form-control" id="observacoesComplemento"
											rows="4" cols="50" name="observacoesComplemento"><c:out
												value="${faturaForm.observacoesComplemento}" /></textarea>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>

				
				<hr>

   			 	<div class="card mt-3">
                    <div class="card-header">
	   			 		<ul class="nav nav-tabs card-header-tabs" id="notaTabs" role="selecaoTabs">
	   			 			<c:if test="${(listaDadosGerais ne null && not empty listaDadosGerais)}">
		  						<li class="nav-item">
		   						 	<a class="nav-link active" id="itens-tab" data-toggle="tab" href="#itensFatura" role="tab" 
		   						 	aria-controls="itensFatura"><i class="fa fa-coins"></i><strong> Itens</strong> <span class="text-danger">*</span></a>
		  						</li>
		  					</c:if>
							<c:if test="${(dadosResumoFatura ne null && not empty dadosResumoFatura)}">	  					
		 						<li class="nav-item">
		    						<a class="nav-link" id="geral-tab" data-toggle="tab" href="#geralFatura" role="tab"
		    						 aria-controls="geralFatura"><i class="fas fa-wallet"></i><strong> Geral</strong> <span class="text-danger">*</span></a>
		  						</li>
	  						</c:if>
							<c:if test="${(listaDadosGerais ne null && not empty listaDadosGerais)}">  						
		  						<li class="nav-item">
		   							 <a class="nav-link" id="tributos-tab" data-toggle="tab" href="#tributosFatura" role="tab"
		   							  aria-controls="tributosFatura"><i class="fas fa-money-bill-wave"></i><strong> Tributos </strong><span class="text-danger">*</span></a>
		  						</li>
	  					    </c:if>
						</ul>
					</div>
					
					<div class="card-body">
						<div class="tab-content" id="notaFiscalSaida">
		  					<c:if test="${(listaDadosGerais ne null && not empty listaDadosGerais)}">
		  						<div class="tab-pane fade active show" id="itensFatura" role="tabpanel" aria-labelledby="itens-tab">
		  							<jsp:include page="/jsp/faturamento/fatura/abaItensIncluirNFDFatura.jsp" />
		  						</div>
		  					</c:if>
							<c:if test="${(dadosResumoFatura ne null && not empty dadosResumoFatura)}">
		  						<div class="tab-pane fade" id="geralFatura" role="tabpanel" aria-labelledby="geral-tab">
		  							<jsp:include page="/jsp/faturamento/fatura/abaGeralIncluirNFDFatura.jsp" />
		  						</div>
		  					</c:if>		  					
							<c:if test="${listaDadosGerais ne null && not empty listaDadosGerais}">		  					
		  						<div class="tab-pane fade" id="tributosFatura" role="tabpanel" aria-labelledby="tributos-tab">
		  							<jsp:include page="/jsp/faturamento/fatura/abaTributosIncluirNFDFatura.jsp" />
		  						</div>
		  					</c:if>
						</div>
   			 		</div>
   			 	</div>			

				</div>
				
				<div class="card-footer">
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
								type="button" onclick="cancelar();">
								<i class="fa fa-times"></i> Cancelar
							</button>
                  		<button id="buttonSalvar" class="btn btn-sm btn-success float-right ml-1 mt-1"  type="submit">
                       		<i class="fa fa-save"></i> Salvar               		
                       	</button>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</form:form>
</div>
