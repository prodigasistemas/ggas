<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">
	Refaturar Fatura Avulso
	<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
		<a class="linkHelp" href="<help:help>/refaturandoafaturadeumcliente.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
	<c:if test="${listaPontoConsumo ne null}">
		<a class="linkHelp" href="<help:help>/refaturandoafaturadeumimvel.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</c:if>
</h1>
<p class="orientacaoInicial">Para refaturar uma Fatura clique em <span class="destaqueOrientacaoInicial">Avan�ar</span>.</p>
	
	
<script type="text/javascript">

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=0');

	function cancelar() {
		submeter('faturaForm','pesquisarFatura');
	}

	function avancar(){
		submeter('faturaForm','exibirResumoRefaturarFaturaAvulso');
	}
	function consistir() {
		// Setando a data dos campos de data de leitura nos campos hidden, caso o usu�rio selecione a data pelo calend�rio.
		var datasLeituraAtual = $("input[name=dataLeituraAtual]");
		$("input[name=dataLeitura]").each(function(indice, elem){
			datasLeituraAtual[indice].value = elem.value;
		});		
		submeter('faturaForm','consistirLeituraRefaturar');
	}

</script>

<form:form method="post" action="exibirResumoRefaturarFaturaAvulso" name="faturaForm">
	<input name="acao" type="hidden" id="acao" value="exibirResumoRefaturarFatura">
	<input name="indexLista" type="hidden" id="indexLista" value="-1">
	<input name="tela" type="hidden" id="tela" value="exibirRefaturarFaturaAvulso">
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}" />
	<input name="ciclo" type="hidden" id="ciclo" value="${faturaForm.numeroCiclo}" />
	<input name="referencia" type="hidden" id="referencia" value="${faturaForm.anoMesReferencia}" />
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="indicadorRefuramento" type="hidden" id="indicadorRefuramento" value="${faturaForm.indicadorRefuramento}"/>
	<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaLista">
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias${chavePrimariaLista}" value="${chavePrimariaLista}">
	</c:forEach>
	
	<fieldset id="incluirFatura" class="conteinerPesquisarIncluirAlterar">
		<input type="hidden" name="idCliente" id="idCliente" value="${faturaForm.idCliente}">
		<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
			<a id="linkDadosCliente" class="linkPesquisaAvancada linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Cliente:</label>
					<input name="nomeCompletoCliente" type="hidden" value="${cliente.nome}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
					<label class="rotulo">CPF/CNPJ:</label>
					<input name="documentoFormatado" type="hidden" value="${cliente.numeroDocumentoFormatado}">
					<span class="itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Endere�o:</label>
					<input name="enderecoFormatadoCliente" type="hidden" value="${cliente.enderecoPrincipal.enderecoFormatado}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
					<label class="rotulo">E-mail:</label>
					<input name="emailCliente" type="hidden" value="${cliente.emailPrincipal}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
				</fieldset>
			</fieldset>
			<label class="rotulo rotuloHorizontal">M�s/Ano:</label>
			<span class="itemDetalhamento"><c:out value="${faturaForm.anoMesReferencia}"/></span><br class="quebra2Linhas"/>
		</c:if>
			
		<c:if test="${listaPontoConsumo ne null}">
			<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			<display:table class="dataTableGGAS" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.web.faturamento.simulacaocalculo.decorator.PontoConsumoSimularCalculoDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao">
		        <display:column sortable="false" title="Descricao" style="width: 100px">
		    	     <c:out value="${pontoConsumo.descricao}"/>
				</display:column>			
				<display:column sortable="false" title="Endere�o" style="width: 100px">
		    	     <c:out value="${pontoConsumo.enderecoFormatado}"/>
				</display:column>	
				<display:column sortable="false" title="CEP" style="width: 100px">
		    	     <c:out value="${pontoConsumo.cep.cep}"/>
				</display:column>	
		    	 <display:column sortable="false" title="Complemento" style="width: 100px">
		    	     <c:out value="${pontoConsumo.descricaoComplemento}"/>
				</display:column>	
			</display:table>
			
			<label class="rotulo">M�s/Ano-Ciclo:</label>
			<span class="itemDetalhamento valorMesAnoCiclo"><c:out value="${faturaForm.anoMesReferencia}"/>-<c:out value="${faturaForm.numeroCiclo}"/></span>
		</c:if>
		
		<label class="rotulo rotuloHorizontal campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Motivo de Refaturamento:</label>
		<select name="idMotivoRefaturamento" id="idMotivoRefaturamento" class="campoSelect">
	    	<option value="-1">Selecione</option>				
			<c:forEach items="${listaMotivoRefaturar}" var="motivoRefaturar">
				<option value="<c:out value='${motivoRefaturar.chavePrimaria}'/>" <c:if test="${faturaForm.idMotivoRefaturamento == motivoRefaturar.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${motivoRefaturar.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		
			<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span class="campoObrigatorioSimbolo">* </span><span>Volume do G�s:</span></label>
		<input class="campoTexto" type="text" id="volumeApurado" name="volumeApurado" onkeypress="return formatarCampoInteiro(event);" maxlength="4" size="5"  value="${faturaForm.volumeApurado[0]}"><br />
	</fieldset>	
	
	<fieldset id="tabs" style="display: none">
		<ul>
			<c:if test="${(itensFatura ne null && not empty itensFatura)}">
				<li><a href="#geralFatura">Geral</a></li>
			</c:if>
<%-- 			<c:if test="${(itensFatura ne null && not empty itensFatura)}"> --%>
<!-- 				<li><a href="#leituraConsumoFatura">Leitura e Consumo</a></li> -->
<%-- 			</c:if> --%>
			<c:if test="${listaCreditoDebito ne null && not empty listaCreditoDebito}">
			<li><a href="#debitosCreditosFatura">D�bitos e Cr�ditos</a></li>
			</c:if>
			<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes}">
				<li><a href="#dadosFaturasPendentes">Faturas N�o Cobradas</a></li>
			</c:if>
		</ul>
		
		<c:if test="${(itensFatura ne null && not empty itensFatura)}">
			<fieldset id="geralFatura">
				<jsp:include page="/jsp/faturamento/fatura/abaGeralRefaturarFatura.jsp"/>
			</fieldset>
		</c:if>
		
<%-- 		<c:if test="${(itensFatura ne null && not empty itensFatura)}"> --%>
<!-- 			<fieldset id="leituraConsumoFatura"> -->
<%-- 				<jsp:include page="/jsp/faturamento/fatura/abaLeituraConsumoFatura.jsp"/> --%>
<!-- 			</fieldset> -->
<%-- 		</c:if> --%>
		
		<c:if test="${listaCreditoDebito ne null && not empty listaCreditoDebito}">
		<fieldset id="debitosCreditosFatura">
			<jsp:include page="/jsp/faturamento/fatura/abaDebitosCreditosFatura.jsp"/>
		</fieldset>		
		</c:if>
		
		<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes && false}">	
			<fieldset id="dadosFaturasPendentes">
				<jsp:include page="/jsp/faturamento/fatura/abaFaturasPendentes.jsp"/>
			</fieldset>
		</c:if>		
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
	    <input name="Button" class="bottonRightCol2 botaoGrande1" value="Avan�ar"  type="button" onclick="avancar();">
	</fieldset>
</form:form>
