<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<div class="card">
	<div class="card-header">
		<h5 class="tituloInterno">Resumo da Fatura<a class="linkHelp" href="<help:help>/resumodefatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h5>
	</div>
	<div class="card-body">
	
	
<script type="text/javascript">

	$(document).ready(function(){
		// Dialog - configura��es
		$("#incluirFaturaLeituraPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
		//Faz a rolagem de tela at� a parte superior
		$.scrollTo($("#mainContainer"),0);
	});

	<c:if test="${listaDadosInclusaoFatura ne null}">
		<c:set var="count" value="1"/>
		<c:forEach items="${listaDadosInclusaoFatura}">
			animatedcollapse.addDiv('conteinerDadosFatura${count}', 'fade=0,speed=400,hide=1');
			<c:set var="count" value="${count+1}"/>
		</c:forEach>
	</c:if>

	function exibirFatura(elem, valor){
		if(elem.checked){
			animatedcollapse.show('conteinerDadosFatura' + valor);
		}else{
			animatedcollapse.hide('conteinerDadosFatura' + valor);
		}
	}
	
	function voltar() {
		submeter('faturaForm', 'exibirInclusaoFaturaAvulso');
	}

	function salvarFaturaEncerramento() {				
		submeter('faturaForm', 'incluirFaturaEncerramento');
	}
	
	
	function salvar() {				
		submeter('faturaForm', 'incluirFaturaAvulso');
	}
	
	
	function exibirDadosLeituraAnterior(leituraAnterior, dataLeituraAnterior, anormalidadeAnterior, volumeAnterior, consumoFaturado, diasConsumo) {
		$("#leituraAnteriorPopup").html(leituraAnterior);
		$("#dataLeituraAnteriorPopup").html(dataLeituraAnterior);
		$("#anormalidadeAnteriorPopup").html(anormalidadeAnterior);
		$("#volumeAnteriorPopup").html(volumeAnterior);
		$("#consumoFaturadoPopup").html(consumoFaturado);
		$("#diasConsumoPopup").html(diasConsumo);
		exibirJDialog("#incluirFaturaLeituraPopup");
	}
	
	<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
		animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=1');
	</c:if>
	<c:if test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
		animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=1');
	</c:if>
	<c:if test="${listaPontoConsumo ne null && (faturaForm.idCliente eq null || faturaForm.idCliente eq 0)}">
		animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=1');	
	</c:if>
	
</script>

<form:form method="post" action="incluirFaturaAvulso" name="faturaForm">
	<token:token></token:token>
	<input name="acao" type="hidden" id="acao" value="incluirFatura">
	<input name="tela" type="hidden" id="tela" value="exibirResumoInclusaoFaturaAvulso">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idCliente" type="hidden" id="idCliente" value="${faturaForm.idCliente}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	<input name="idContratoEncerrarRescindir" type="hidden" id="idContratoEncerrarRescindir" value="${faturaForm.idContratoEncerrarRescindir}">	
	<input name="listaIdsPontoConsumoRemovidosAgrupados" type="hidden" id="listaIdsPontoConsumoRemovidosAgrupados" value="${listaIdsPontoConsumoRemovidosAgrupados}">
	
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${faturaForm.codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
	
	<input name="anoMesReferencia" type="hidden" id="referencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
	<input name="numeroCiclo" type="hidden" id="ciclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
	
	
	<div id="incluirFaturaLeituraPopup" title="Leitura">
		<label class="rotulo">Leitura Anterior:</label>
		<span class="itemDetalhamento" id="leituraAnteriorPopup"></span><br />
		<label class="rotulo">Data da Leitura Anterior:</label>
		<span class="itemDetalhamento" id="dataLeituraAnteriorPopup"></span><br />
		<label class="rotulo">Anormalidade da Leitura:</label>
		<span class="itemDetalhamento" id="anormalidadeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Volume Anterior Apurado:</label>
		<span class="itemDetalhamento" id="volumeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Consumo Faturado:</label>
		<span class="itemDetalhamento" id="consumoFaturadoPopup"></span><br /><br />
		<label class="rotulo">Dias de Consumo:</label>
		<span class="itemDetalhamento" id="diasConsumoPopup"></span><br /><br />
	</div>
	
	<c:forEach items="${faturaForm.chavesCreditoDebito}" var="chaveCreditoDebito">
		<input name="chavesCreditoDebito" type="hidden" id="chavesCreditoDebito" value="${chaveCreditoDebito}">
	</c:forEach>
	
		<fieldset id="resumoFatura" class="conteinerPesquisarIncluirAlterar">
			<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
				<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
					<div class="form-row">
						<div class="col-8">
							<div class="row">
								<label class="col-2 text-right rotulo">Cliente:</label>
								<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span>
								<label class="col-2 text-right rotulo">Endere�o:</label>
								<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span>
							</div>	
						</div>
					</div>
					
					<div class="form-row">
						<div class="col-8">
							<div class="row">
								<label class="col-2 text-right rotulo">CPF/CNPJ:</label>
								<span class="col itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span>
								<label class="col-2 text-right rotulo">E-mail:</label>
								<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span>
							</div>	
						</div>
					</div>
				</fieldset><br/>
			</c:if>
			
			<c:if test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
				<a id="linkDadosImovel" class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosImovel" class="conteinerDados conteinerBloco">

					<div class="form-row">
						<div class="col-8">
							<div class="row">
								<label class="col-2 text-right rotulo">Descri��o:</label>
								<span class="col itemDetalhamento"><c:out value="${imovel.nome}"/></span><br />
								
								<label class="col-2 text-right rotulo">Cidade:</label>
								<span class="col itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.nomeMunicipio}"/></span><br />
							</div>	
						</div>
					</div>
					
					<div class="form-row">
						<div class="col-8">
							<div class="row">
								<label class="col-2 text-right rotulo">Matr�cula:</label>
								<span class="col itemDetalhamento"><c:out value="${imovel.chavePrimaria}"/></span><br />
								
								<label class="col-2 text-right rotulo">Endere�o:</label>
								<span class="col itemDetalhamento"><c:out value="${imovel.modalidadeMedicaoImovel.codigo}"/></span><br />
							</div>	
						</div>
					</div>

				</fieldset><br/>
			</c:if>
			
			<c:if test="${listaPontoConsumo ne null}">
				<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosPontoConsumo" class="conteinerBloco">
					
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" id="pontoConsumo">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th scope="col" class="text-center">Im�vel - Ponto Consumo</th>
									<th scope="col" class="text-center">Segmento</th>
									<th scope="col" class="text-center">Situa��o</th>
									<th scope="col" class="text-center">N� Medidor</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
									<tr>
										<td class="text-center">
											<input type="hidden" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"/>
					   		
									   		<c:choose>
									   			 <c:when test="${nomeImovel ne null}">
									   			 	<c:out value="${nomeImovel}"/> - <c:out value="${pontoConsumo.descricao}"/>
									   			 </c:when>
									   			 <c:otherwise>
													<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>							
									   			 </c:otherwise>
									   		</c:choose>
										</td>
										<td class="text-center"><c:out value="${segmento.descricao}"/></td>
										<td class="text-center"><c:out value="${situacaoConsumo.descricao}"/></td>
										<td class="text-center"><c:out value="${instalacaoMedidor.medidor.numeroSerie}"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				
				</fieldset>
			</c:if>
			
			<br class="quebraLinha" />
		    
		    
			<br class="quebraLinha" />
			
			<c:if test="${listaDadosLeituraConsumoFatura ne null && not empty(listaDadosLeituraConsumoFatura)}">
			<fieldset class="conteinerBloco">
				<legend class="h5">Leitura Consumo:</legend>
				
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="leituraConsumo">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">Leitura Atual</th>
								<th scope="col" class="text-center">Informada <br/>pelo Cliente</th>
								<th scope="col" class="text-center">Data da <br/>Leitura Atual</th>
								<th scope="col" class="text-center">Leiturista</th>
								<th scope="col" class="text-center">Leitura<br/>Anterior</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaDadosLeituraConsumoFatura}" var="leituraConsumo">
								<tr>
									<td class="text-center">
										<fmt:formatNumber value='${leituraConsumo.leituraAtual}' maxFractionDigits='0'/>
									</td>
									<td class="text-center">
										<c:choose>
								   			<c:when test="${leituraConsumo.informadoCliente}">
								   				Sim
								   			</c:when>
								   			<c:otherwise>
								   				N�o
								   			</c:otherwise>
								   		</c:choose>
									</td>
									<td class="text-center">
										<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' pattern='dd/MM/yyyy'/>
									</td>
									<td class="text-center">
										<c:out value="${leituraConsumo.leiturista.funcionario.nome}" />
									</td>
									<td class="text-center">
										<a href='javascript:exibirDadosLeituraAnterior(
											"<fmt:formatNumber value="${leituraConsumo.leituraAnterior}" maxFractionDigits="0"/>",
											"<fmt:formatDate value="${leituraConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>",
											"<c:out value="${leituraConsumo.anormalidadeLeitura.descricao}"/>",
											"<fmt:formatNumber value="${leituraConsumo.volumeAnteriorApurado}" maxFractionDigits="0"/>",
											"<fmt:formatNumber value="${leituraConsumo.consumoFaturado}" maxFractionDigits="0"/>",
											"<c:out value="${leituraConsumo.diasConsumo}"/>");'>
											
											<img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/>
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</fieldset>
		    </c:if>
		    
			<hr class="linhaSeparadoraDetalhamento" />
			
			<fieldset id="conteinerGeral" class="conteinerBloco">
				<legend class="h5">Geral:</legend>
				
				<div class="form-row">
					<div class="col-8">
						<div class="row">
							<label class="col-2 text-right rotulo">M�s/Ano-Ciclo:</label>
							<span class="col itemDetalhamento">
								<c:out value="${faturaForm.anoMesFormatado}" />
							</span><br class="quebraLinha" />
							
							<label class="col-2 text-right rotulo">Motivo de Inclus�o:</label>
							<span class="col itemDetalhamento"><c:out value="${descricaoMotivoInclusao}" /></span><br class="quebraLinha" />
						</div>	
					</div>
				</div>
				
				<div class="form-row">
					<div class="col-8">
						<div class="row">
							<label class="col-2 text-right rotulo">Data de Emiss�o:</label>
							<span class="col itemDetalhamento"><c:out value='${dataEmissao}'/></span><br class="quebraLinha" />
							
							<c:if test="${numeroContrato ne null && numeroContrato ne ''}">
								<label class="col-2 text-right rotulo">Contrato:</label>
								<span class="col itemDetalhamento"><c:out value="${numeroContrato}" /></span>
							</c:if>
						</div>	
					</div>
				</div>
				
			</fieldset>
			
			<c:if test="${listaDadosGerais ne null}">
				<c:set var="i" value="1" />
				<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
					<hr class="linhaSeparadoraDetalhamento" />
					<fieldset class="conteinerBloco">
						<legend class="h5">Fatura <c:out value="${i}"/></legend>
						
						<div class="form-row">
							<div class="col-8">
								<div class="row">
									<label class="col-2 text-right rotulo">Data de Vencimento:</label>
									<span class="col itemDetalhamento"><fmt:formatDate value="${dadosGeraisItensFatura.dataVencimento}" pattern="dd/MM/yyyy"/></span>
									<label class="col-2 text-right rotulo rotuloHorizontal">Tipo da Nota Fiscal:</label>
									<span class="col itemDetalhamento">
										<c:choose>
											<c:when test="${dadosGeraisItensFatura.indicadorProduto}"><c:out value="Produto"/></c:when>
											<c:otherwise><c:out value="Servi�o"/></c:otherwise>
										</c:choose>
									</span>
								</div>	
							</div>
						</div>
						
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="item">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center">Descri��o</th>
										<th scope="col" class="text-center">Tipo</th>
										<th scope="col" class="text-center">Data In�cio</th>
										<th scope="col" class="text-center">Data Final</th>
										<th scope="col" class="text-center">Quantidade</th>
										<th scope="col" class="text-center">Unidade</th>
										<th scope="col" class="text-center">Valor Unit�rio (R$)</th>
										<th scope="col" class="text-center">Valor (R$)</th>
										<th scope="col" class="text-center">Valor Cr�dito Dispon�vel (R$)</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${dadosGeraisItensFatura.listaDadosItensFaturaExibicao}" var="item">
										<tr>
											<td class="text-center">
												<c:choose>
											   		<c:when test="${item.descricaoDesconto ne null}">
											   		    <c:out value="${item.descricaoDesconto}"/>
											   		</c:when>	
											   		<c:when test="${item.creditoDebitoARealizar ne null && item.creditoDebitoARealizar.creditoDebitoNegociado ne null 
											   						&& item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica ne null}">
											   			<c:out value="${item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao}"/>
											   			(<c:out value="${item.creditoDebitoARealizar.numeroPrestacao}"/>/<c:out value="${item.creditoDebitoARealizar.creditoDebitoNegociado.quantidadeTotalPrestacoes}"/>)
											   		</c:when>		   		
											   		<c:when test="${item.faturaItem ne null && item.faturaItem.rubrica ne null && item.pontoConsumo ne null 
											   						&& item.pontoConsumo.instalacaoMedidor ne null && item.pontoConsumo.instalacaoMedidor.medidor ne null}">
											   			<c:out value="${item.faturaItem.rubrica.descricaoImpressao}"/>
											   		</c:when>	
											   		<c:otherwise>
											   			<c:out value="${item.rubrica.descricaoImpressao}"/>
											   		</c:otherwise>
										   		</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
										   			<c:when test="${item.indicadorCredito eq true}">
								   						<c:out value="Cr�dito"></c:out>
								   					</c:when>
								   					<c:otherwise>
									   					<c:out value="D�bito"></c:out>
								   					</c:otherwise>
										   		</c:choose>
											</td>
											<td class="text-center">
												<fmt:formatDate value="${item.dataInicial}"></fmt:formatDate>
											</td>
											<td class="text-center">
												<fmt:formatDate value="${item.dataFinal}"></fmt:formatDate>
											</td>
											<td class="text-center">
												<fmt:formatNumber value="${item.consumo}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
											</td>
											<td class="text-center">
												<c:choose>
										   			<c:when test="${item.itemFatura ne null}">
								   						m<span class="expoente">3</span>
								   					</c:when>
										   		</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
													<c:when test="${item.indicadorCredito eq true}">
														-<fmt:formatNumber value="${item.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
													</c:when>
									   				<c:otherwise>
									   					<fmt:formatNumber value="${item.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
									   				</c:otherwise>
									   			</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
											   		<c:when test="${item.valorTotal ne null}">
												   		<c:choose>
												   			<c:when test="${item.indicadorCredito eq true}">
										   						-<fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:when>
										   					<c:otherwise>
											   					<fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:otherwise>
												   		</c:choose>
													</c:when>	
											   		<c:otherwise>
											   			<c:choose>
											   				<c:when test="${item.indicadorCredito eq true}">
										   						-<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:when>
										   					<c:otherwise>
											   					<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:otherwise>
										   				</c:choose>
											   		</c:otherwise>
											   	</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
													<c:when test="${item.indicadorCredito eq true}">
														-<fmt:formatNumber value="${item.valorTotalDisponivel}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
													</c:when>
									   				<c:otherwise>
									   					<fmt:formatNumber value="${item.valorTotalDisponivel}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
									   				</c:otherwise>
									   			</c:choose>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						
						<c:if test="${dadosGeraisItensFatura.listaDadosTributoVO ne null && not empty dadosGeraisItensFatura.listaDadosTributoVO}">
							
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover" id="tributoVO">
									<thead class="thead-ggas-bootstrap">
										<tr>
											<th scope="col" class="text-center">Tributo</th>
											<th scope="col" class="text-center">Al�quota</th>
											<th scope="col" class="text-center">Base de C�lculo (R$)</th>
											<th scope="col" class="text-center">Valor (R$)</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${dadosGeraisItensFatura.listaDadosTributoVO}" var="tributoVO">
											<tr>
												<td class="text-center"><c:out value="${tributoAliquota.tributo.descricao}"/></td>
												<td class="text-center"><fmt:formatNumber value="${tributoVO.percentual}" minFractionDigits="2" maxFractionDigits="2"/>%</td>
												<td class="text-center"><fmt:formatNumber value="${tributoVO.baseCalculo}" minFractionDigits="2" maxFractionDigits="2"/></td>
												<td class="text-center"><fmt:formatNumber value="${tributoVO.valor}" minFractionDigits="2" maxFractionDigits="2"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:if>
					</fieldset>
					<c:set var="i" value="${i+1}" />
					
					<label class="rotulo">Valor total da fatura:</label>
					<span class="itemDetalhamento">R$ <fmt:formatNumber value='${dadosGeraisItensFatura.valorTotalFatura}' minFractionDigits="2" maxFractionDigits="2"/></span><br class="quebraLinha" />
				</c:forEach>
			</c:if>
			
			<c:if test="${listaFaturasPendentes ne null && not empty(listaFaturasPendentes)}">
				<hr class="linhaSeparadoraDetalhamento" />
				
				<div class="form-row">
					<div class="col-8">
						<div class="row">
							<label class="col-2 text-right rotulo">Incluir faturas n�o cobradas?</label>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="indicadorFaturasPendentes" value="true" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'true' || faturaForm.indicadorFaturasPendentes eq ''}">checked="checked" </c:if>>
								<label class="form-check-label rotuloRadio">Sim</label>
								<input class="form-check-input" type="radio" name="indicadorFaturasPendentes" value="false" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'false'}">checked="checked" </c:if>>
								<label class="form-check-label rotuloRadio">N�o</label>
							</div>
						</div>	
					</div>
				</div>
								
				
				<fieldset class="conteinerBloco">
					<legend class="h5">Faturas N�o Cobradas</legend>
					
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" id="fatura">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th scope="col" class="text-center">N�mero do Documento</th>
									<th scope="col" class="text-center">Data de<br />Emiss�o</th>
									<th scope="col" class="text-center">Data de<br />Vencimento</th>
									<th scope="col" class="text-center">Ciclo /<br />Refer�ncia</th>
									<th scope="col" class="text-center">Valor Total (R$)</th>
									<th scope="col" class="text-center">Situa��o</th>
									<th scope="col" class="text-center">Pagamento</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaFaturasPendentes}" var="fatura">
									<tr>
										<td class="text-center">
											<input type="hidden" name="chavesFaturasPendentes" value="${fatura.chavePrimaria}" />
											<c:out value="${fatura.chavePrimaria}"/>
										</td>
										<td class="text-center">
											<fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy"/>
										</td>
										<td class="text-center">
											<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
										</td>
										<td class="text-center">
										<c:if test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
											<c:out value="${fatura.cicloReferenciaFormatado}"/>
										</c:if>
										</td>
										<td class="text-center">
											<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/>
										</td>
										<td class="text-center">
											<c:out value="${fatura.creditoDebitoSituacao.descricao}"/>
										</td>
										<td class="text-center">
											<c:out value="${fatura.situacaoPagamento.descricao}"/>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</c:if>
			
			
	
		   <!--   
		 <c:if test="${listaCreditoDebito ne null && not empty(listaCreditoDebito)}">
		 <hr class="linhaSeparadoraDetalhamento" />
		    <fieldset class="conteinerBloco">
				<legend>Cr�dito e D�bito:</legend>
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" sort="list" id="creditoDebito" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
					
				<display:column title="Rubrica" style="width: 330px">				   		
					<c:out value="${creditoDebito.creditoDebitoNegociado.rubrica.descricao}"/> 
					(<c:out value="${creditoDebito.numeroPrestacao}"/>/<c:out value="${creditoDebito.creditoDebitoNegociado.quantidadeTotalPrestacoes}"/>)
					<c:if test="${creditoDebito.creditoDebitoNegociado.descricao ne null && creditoDebito.creditoDebitoNegociado.descricao ne ''}">
						<c:out value="${creditoDebito.creditoDebitoNegociado.descricao}" />
					</c:if>
			   	</display:column>	
				
				<display:column title="Tipo" >
			   		<c:choose>
						<c:when test="${creditoDebito.creditoDebitoNegociado.creditoOrigem eq null}">D�bito</c:when>
						<c:otherwise>Cr�dito</c:otherwise>
					</c:choose>
			   	</display:column>
					
			   	<display:column title="Quantidade" >
			   		<fmt:formatNumber value="${creditoDebito.quantidade}" minFractionDigits="2" />
			   	</display:column>
			   	
			   	<display:column title="Valor Unit�rio (R$)">
					<fmt:formatNumber value="${creditoDebito.valorUnitario}" minFractionDigits="2" maxFractionDigits="2"/>
			   	</display:column>
			   	
			   <display:column title="Valor Total (R$)" >
			   		<input type="hidden" id="creditoDebito${creditoDebito.chavePrimaria}" value="<fmt:formatNumber value='${creditoDebito.saldoCredito}' maxFractionDigits='2' minFractionDigits='2'/>"></input>
			   		<fmt:formatNumber value="${creditoDebito.saldoCredito}" minFractionDigits="2" maxFractionDigits="2"/>
			   	</display:column>
			   	
				</display:table>
			</fieldset>
		    </c:if>-->
			
			
		</fieldset>	
		
		
		<div class="btn-group" role="group" aria-label="Button group">
			<button type="button" class="btn btn-primary" onclick="voltar();"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Voltar</button>
			<c:if test="${empty faturaForm.idContratoEncerrarRescindir}">
				<button type="button" class="btn btn-primary" onclick="salvar();">Salvar</button>
		    </c:if>
			<c:if test="${not empty faturaForm.idContratoEncerrarRescindir}">
				<button type="button" class="btn btn-primary" onclick="salvarFaturaEncerramento();">Salvar Fatura Encerramento</button>
		    </c:if>
		</div>		
		    
</form:form>

	</div>
</div>
