<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Detalhar Controle de Impress�o<a class="linkHelp" href="<help:help>/consultadasfaturas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Hist�rico dos usu�rios que emitiram fatura</p>


<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
	});
	
	
	function voltar(){
		submeter("faturaForm", "pesquisarControleFatura");
	}
	
</script>		

<form:form method="post" styleId="formPesquisarFatura" action="exibirDetalharFaturaControleImpressao" name="faturaForm">
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna" style="border: none" id="pesquisaMedicaoCol1">
			
			<label>Grupo Faturamento:</label>


		</fieldset>		
			
		<!--							
		<fieldset class="colunaFinal" id="pesquisaMedicaoCol2">
			 <label for="indicadorEmissao" class="rotulo">Indicador de Emiss�o:</label>

			<input type="radio" <c:if test="${faturaForm.map.indicadorEmissao == 'true'}">checked="checked"</c:if> value="true" id="indicadorEmissaoSim" name="indicadorEmissao" class="campoRadio">
			<label class="rotuloRadio">Sim</label>

			<input type="radio" <c:if test="${faturaForm.map.indicadorEmissao == 'false'}">checked="checked"</c:if> value="false" id="indicadorEmissaoNao" name="indicadorEmissao" class="campoRadio">
			<label class="rotuloRadio">N�o</label>
			<br/>																		
			
			<label for="dataEmissaoInicial" id="rotuloIntervaloDataEmissao" class="rotulo">Intervalo de Data de Emiss�o:</label>
			<input class="campoData campoHorizontal" type="text" id="dataEmissaoInicial" name="dataEmissaoInicial" maxlength="10" value="${faturaForm.map.dataEmissaoInicial}"/>
			<span> a </span>
			<input class="campoData campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10" value="${faturaForm.map.dataEmissaoFinal}"/>			
			
		</fieldset>
		-->
		
		<br/><br/>
		
		<c:if test="${listaFaturaControleImpressao ne null}">		
			<fieldset class="conteinerBloco">
				<display:table class="dataTableGGAS dataTableDetalhamento" name="${listaFaturaControleImpressao}" sort="list" id="faturaControleImpressao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarControleFatura">

					<display:column title="Grupo Faturamento">
						 <c:out
								value='${faturaControleImpressao.chavePrimaria}' /> 

					</display:column>

					<display:column title="Rota">
						 <c:out
								value='${faturaControleImpressao.chavePrimaria}' /> 
				   	</display:column>    
				   
				   	<display:column title="Impress�es Realizadas">
				   		<c:out	value='${faturaControleImpressao.seqImpressao}' />
				   		
				   	</display:column>
				   	
				   	<display:column title="Data de Emiss�o">
				   		<fmt:formatDate value="${faturaControleImpressao.dtImpressao}" pattern="dd/MM/yyyy" />
				   	</display:column>
				   	<display:column title="Ano/M�s Refer�ncia">
				   		<c:out	value='${faturaControleImpressao.anoMesReferencia}' />
				   		
				   	</display:column>
				   	
				   	<display:column title="Ciclo">
				   		<c:out	value='${faturaControleImpressao.ciclo}' />
				   		
				   	</display:column>
				   	<display:column title="Usu�rio">
				   		<c:out	value='${faturaControleImpressao.usuario.login}' />
				   		
				   	</display:column>
				   	
				   	
			    </display:table>
			    
			    <fieldset class="containerBotoes">
			    	<input name="buttonIncluir" value="Voltar" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="voltar();" type="button">
			    	<!-- <vacess:vacess param="geraFaturaControleImpressao"> -->
						
					<!-- </vacess:vacess> -->
			    </fieldset>
			</fieldset>						
		</c:if>
	</fieldset>
	

	
</form:form>
