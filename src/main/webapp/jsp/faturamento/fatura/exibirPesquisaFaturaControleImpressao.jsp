<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Controle de Impress�o<a class="linkHelp" href="<help:help>/consultadasfaturas.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os filtros abaixo e clique em <b>Pesquisar</b></p>


<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		$("#exportarRelatorio").dialog({autoOpen: false, width: 245, modal: true, minHeight: 100, maxHeight: 400, resizable: false});
	});
	
	function exibirExportarRelatorio(){
		exibirJDialog("#exportarRelatorio");
		gerar();
	}
	
	function gerar(){
		$(".mensagens").hide();
		submeter('0', 'gerarRelatorioControleImpressao');
	}
	
	function limparFormulario(){
		limparFormularios(document.faturaForm);
		document.forms[0].idGrupoFaturamento.selectedIndex = 0;
		document.forms[0].idRota.selectedIndex = 0;
		document.forms[0].indicadorEmissaoSim.checked = true;
		document.forms[0].dataEmissaoInicial.value = "";
		document.forms[0].dataEmissaoFinal.value = "";
		document.forms[0].dataGeracaoInicial.value = "";
		document.forms[0].dataGeracaoFinal.value = "";
		
	}
	
	function geraFaturaControleImpressao(){
		submeter("faturaForm", "gerarRelatorioControleImpressao");
	}
	
	function exibirPopupPesquisaCliente() {
		popup = window.open('exibirPesquisaClientePopup','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes ,modal=yes');
	}
	
	function selecionarCliente(idSelecionado){
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document.getElementById("nomeCompletoCliente");
		var documentoFormatado = document.getElementById("documentoFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document.getElementById("enderecoFormatadoCliente");		
		
		if(idSelecionado != '') {				
			AjaxService.obterClientePorChave( idSelecionado, {
	           	callback: function(cliente) {	           		
	           		if(cliente != null){  	           			        		      		         		
		               	idCliente.value = cliente["chavePrimaria"];
		               	nomeCompletoCliente.value = cliente["nome"];		               	
		               	if(cliente["cnpj"] != undefined ){
		               		documentoFormatado.value = cliente["cnpj"];
		               	} else {
			               	if(cliente["cpf"] != undefined ){
			               		documentoFormatado.value = cliente["cpf"];
			               	} else {
			               		documentoFormatado.value = "";
			               	}
		               	}
		               	emailCliente.value = cliente["email"];
		               	enderecoFormatado.value = cliente["enderecoFormatado"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idCliente.value = "";
        	nomeCompletoCliente.value = "";
        	documentoFormatado.value = "";
        	emailCliente.value = "";
        	enderecoFormatado.value = "";
       	}
	
		document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
		document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
		document.getElementById("emailClienteTexto").value = emailCliente.value;
		document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
	}
	
	
	function detalharControleFaturaImpressao(chave){
		document.getElementById('chavePrimaria').value = chave;
		submeter("faturaForm", "exibirDetalharFaturaControleImpressao");
	}
	
</script>		

<form:form method="post" styleId="formPesquisarFatura" action="pesquisarControleFatura" name="faturaForm">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna" id="pesquisaMedicaoCol1">
						
			<fieldset id="pesquisarCliente" class="colunaEsq">
				<legend>Pesquisar Cliente</legend>
				<div class="pesquisarClienteFundo">
					<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span> para selecionar um Cliente.</p>
					<input name="idCliente" type="hidden" id="idCliente" value="${imovelForm.map.idCliente}">
					<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${imovelForm.map.nomeCompletoCliente}">
					<input name="documentoFormatado" type="hidden" id="documentoFormatado" value="${imovelForm.map.documentoFormatado}">
					<input name="enderecoFormatadoCliente" type="hidden" id="enderecoFormatadoCliente" value="${imovelForm.map.enderecoFormatadoCliente}">
					<input name="emailCliente" type="hidden" id="emailCliente" value="${imovelForm.map.emailCliente}">
					
					<input name="Button" id="botaoPesquisarCliente" class="bottonRightCol2" title="Pesquisar cliente"  value="Pesquisar cliente" onclick="exibirPopupPesquisaCliente();" type="button"><br >
					<label class="rotulo" id="rotuloCliente" for="nomeClienteTexto">Cliente:</label>
					<input class="campoDesabilitado" type="text" id="nomeClienteTexto" name="nomeClienteTexto"  maxlength="50" size="50" disabled="disabled" value="${imovelForm.map.nomeCompletoCliente}"><br />
					<label class="rotulo" id="rotuloCnpjTexto" for="documentoFormatadoTexto">CPF/CNPJ:</label>
					<input class="campoDesabilitado" type="text" id="documentoFormatadoTexto" name="documentoFormatadoTexto"  maxlength="18" size="18" disabled="disabled" value="${imovelForm.map.documentoFormatado}"><br />	
					<label class="rotulo" id="rotuloEnderecoTexto" for="enderecoFormatadoTexto">Endere�o:</label>
					<textarea class="campoDesabilitado" id="enderecoFormatadoTexto" name="enderecoFormatadoTexto" rows="2" cols="37" disabled="disabled">${param.enderecoFormatadoCliente}</textarea><br />
					<label class="rotulo" id="rotuloEmailClienteTexto" for="emailClienteTexto">E-mail:</label>
					<input class="campoDesabilitado" type="text" id="emailClienteTexto" name="emailClienteTexto"  maxlength="80" size="40" disabled="disabled" value="${imovelForm.map.emailCliente}"><br />
				</div>
			</fieldset>
						
		</fieldset>		
			
								
		<fieldset class="colunaFinal" id="pesquisaMedicaoCol2">
		
		<label for="idGrupoFaturamento" id="rotuloGrupoFaturamento" class="rotulo rotulo2Linhas">Grupo de Faturamento:</label>

			<select name="idGrupoFaturamento" id="idGrupoFaturamento" class="campoSelect">
		    	<option value="-1">Selecione</option>				
				<c:forEach items="${listaGrupoFaturamento}" var="grupoFaturamento">
					<option value="<c:out value="${grupoFaturamento.chavePrimaria}"/>" <c:if test="${faturaImpressaoVO.idGrupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${grupoFaturamento.descricao}"/>
					</option>		
				</c:forEach>
			</select>			

			<br>
			<label for="idRota" id="rotuloRota" class="rotulo">Rota:</label>

			<select name="idRota" id="idRota" class="campoSelect">
		    	<option value="-1">Selecione</option>				
				<c:forEach items="${listaRota}" var="rota">
					<option value="<c:out value="${rota.chavePrimaria}"/>" <c:if test="${faturaImpressaoVO.idRota == rota.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${rota.numeroRota}"/>
					</option>		
				</c:forEach>
			</select><br><br>	
			
			<label class="rotulo" id="rotuloIntervaloDataGeracao" for="intervaloDataGeracao" >Data de Gera��o:</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataGeracaoInicial" name="dataGeracaoInicial" maxlength="10" value="${faturaImpressaoVO.dataGeracaoInicial}">
			<label class="rotuloEntreCampos" id="rotuloDataGeracao">a</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataGeracaoFinal" name="dataGeracaoFinal" maxlength="10" value="${faturaImpressaoVO.dataGeracaoFinal}">
						
			<label class="rotulo" id="rotuloIntervaloDataEmissao" for="intervaloDataEmissao" >Data de Emiss�o:</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataEmissaoInicial" name="dataEmissaoInicial" maxlength="10" value="${faturaImpressaoVO.dataEmissaoInicial}">
			<label class="rotuloEntreCampos" id="rotuloEntreCamposDataEmissao">a</label>
			<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataEmissaoFinal" name="dataEmissaoFinal" maxlength="10" value="${faturaImpressaoVO.dataEmissaoFinal}">
			
			
			<label for="indicadorEmissao" class="rotulo rotulo2Linhas">Indicador de Emiss�o:</label>
			<input class="campoRadio" type="radio" name="indicadorEmissao" id="indicadorEmissaoSim" value="true" <c:if test="${faturaImpressaoVO.indicadorEmissao eq 'true' or empty faturaImpressaoVO.indicadorEmissao }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorEmissao" id="indicadorEmissaoNao" value="false" <c:if test="${faturaImpressaoVO.indicadorEmissao eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
		</fieldset>
		
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
		
			<input type="submit" value="Pesquisar" id="botaoPesquisar" class="bottonRightCol2" name="Button">
					
			<input type="button" onclick="limparFormulario();" value="Limpar" id="Limpar" class="bottonRightCol bottonRightColUltimo" name="Button">						
		</fieldset>	
		
		<br/><br/>
		
		<c:if test="${listaFaturaControleImpressao ne null}">		
			<fieldset class="conteinerBloco">
				<display:table class="dataTableGGAS dataTableDetalhamento" name="${listaFaturaControleImpressao}" sort="list" id="faturaControleImpressao" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarControleFatura">
					<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
		      			<input type="checkbox" name="chavesPrimarias" value="${faturaControleImpressao.chavePrimaria}">
		     			</display:column>
				   		     	 
				   	<display:column title="Grupo Faturamento">
						<a
							href='javascript:detalharControleFaturaImpressao(<c:out value='${faturaControleImpressao.chavePrimaria}'/>);'><span
							class="linkInvisivel"></span> <c:out
								value='${faturaControleImpressao.grupoFaturamento.descricao}' /> </a>

				   	</display:column>    

				   	<display:column title="Rota">
						<a
							href='javascript:detalharControleFaturaImpressao(<c:out value='${faturaControleImpressao.chavePrimaria}'/>);'><span
							class="linkInvisivel"></span> <c:out
								value='${faturaControleImpressao.rota.numeroRota}' /> </a>
				   	</display:column>    
				   
				   	<display:column title="Impress�es Realizadas">
				   		<a
							href='javascript:detalharControleFaturaImpressao(<c:out value='${faturaControleImpressao.chavePrimaria}'/>);'><span
							class="linkInvisivel"></span> <c:out
								value='${faturaControleImpressao.seqImpressao}' /> </a>
				   		
				   	</display:column>
				   	
				   	<display:column title="Data de Emiss�o">
				   		<fmt:formatDate value="${faturaControleImpressao.dtImpressao}" pattern="dd/MM/yyyy" />
				   	</display:column>
			    </display:table>
			    
			    
			    <fieldset class="containerBotoes">
			    	<input name="buttonIncluir" value="Iniciar Processo" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="geraFaturaControleImpressao();" type="button">
			    	<!-- <vacess:vacess param="geraFaturaControleImpressao"> -->
						
					<!-- </vacess:vacess> -->
			    </fieldset>
			</fieldset>						
		</c:if>
	</fieldset>
	

	
</form:form>
