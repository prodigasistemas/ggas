<!--
 Copyright (C) <2011> GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

 Este arquivo ? parte do GGAS, um sistema de gest?o comercial de Servi?os de Distribui??o de G?s

 Este programa ? um software livre; voc? pode redistribu?-lo e/ou
 modific?-lo sob os termos de Licen?a P?blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers?o 2 da Licen?a.

 O GGAS ? distribu?do na expectativa de ser ?til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl?cita de
 COMERCIALIZA??O ou de ADEQUA??O A QUALQUER PROP?SITO EM PARTICULAR.
 Consulte a Licen?a P?blica Geral GNU para obter mais detalhes.

 Voc? deve ter recebido uma c?pia da Licen?a P?blica Geral GNU
 junto com este programa; se n?o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest?o Comercial (Billing) de Servi?os de Distribui??o de G?s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<div class="card">
	<div class="card-header">
		<h5 class="card-title">Incluir Fatura</h5>
		<a class="linkHelp" href="<help:help>/inclusodefatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	</div>
	<div class="card-body">
		<div class="alert alert-primary" role="alert">
			<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Para inserir uma Fatura clique em <span class="destaqueOrientacaoInicial">Avan�ar</span>.
		</div>
	
	
<script type="text/javascript">

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('pontoConsumo', 'fade=0,speed=400,hide=0');

	function consistir() {
		// Setando a data dos campos de data de leitura nos campos hidden, caso o usu?rio selecione a data pelo calend?rio.
		var datasLeituraAtual = $("input[name=dataLeituraAtual]");
		$("input[name=dataLeitura]").each(function(indice, elem){
			datasLeituraAtual[indice].value = elem.value;
		});
		
		submeter('faturaForm','consistirLeituraFaturamento');
	}

	function cancelar() {
		submeter('faturaForm','exibirPesquisa?idContratoEncerrarRescindir='+document.forms[0].idContratoEncerrarRescindir.value);
	}
	
	 $(document).ready(function(){
		/*$('#pontoConsumo').dataTable( {
			"paging": false,
			"language": {
	            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		});*/
			var datepicker = $.fn.datepicker.noConflict();
			$.fn.bootstrapDP = datepicker;  
			$('.bootstrapDP').bootstrapDP({
										autoclose: true,
										format: 'dd/mm/yyyy',
										language: 'pt-BR'
									});
	}); 
</script>

<form:form method="post" action="exibirResumoInclusaoFatura" name="faturaForm">
	<input name="acao" type="hidden" id="acao" value="exibirResumoInclusaoFatura">
	<input name="indexLista" type="hidden" id="indexLista" value="-1">
	<input name="postBack" type="hidden" id="postBack" value="false">
	<input name="tela" type="hidden" id="tela" value="exibirInclusaoFatura">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}" />
	<input name="numeroCiclo" type="hidden" id="ciclo" value="${faturaForm.numeroCiclo}" />
	<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
	<input name="idContratoEncerrarRescindir" type=hidden id="idContratoEncerrarRescindir" value="${faturaForm.idContratoEncerrarRescindir}">
	<input name="listaIdsPontoConsumoRemovidosAgrupados" type="hidden" id="listaIdsPontoConsumoRemovidosAgrupados" value="${listaIdsPontoConsumoRemovidosAgrupados}">
	
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${faturaForm.codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
	
	<fieldset id="incluirFatura" class="conteinerPesquisarIncluirAlterar">
		<input type="hidden" name="idCliente" id="idCliente" value="${faturaForm.idCliente}">
		<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
			<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<div class="row">
					<div class="col-3">
						<div class="row">
							<label class="col-3 text-right rotulo">Cliente:</label>
							<input name="nomeCompletoCliente" type="hidden" value="${cliente.nome}" />
							<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span>
						</div>
						<div class="row">
							<label class="col-3 text-right rotulo">CPF/CNPJ:</label>
							<input name="documentoFormatado" type="hidden" value="${cliente.numeroDocumentoFormatado}" />
							<span class="col itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
						</div>
					</div>
					<div class="col">
						<div class="row">
							<label class="col-1 text-right rotulo">Endere�o:</label>
							<input name="enderecoFormatadoCliente" type="hidden" value="${cliente.enderecoPrincipal.enderecoFormatado}" />
							<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span>
						</div>
						<div class="row">
							<label class="col-1 text-right rotulo">E-mail:</label>
							<input name="emailCliente" type="hidden" value="${cliente.emailPrincipal}" />
							<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
						</div>
					</div>
				</div>
			</fieldset>
		</c:if>
		
		<c:if test="${listaPontoConsumo ne null && not empty listaPontoConsumo}">
			<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[tabelaPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			
			<div class="table-responsive" id="tabelaPontoConsumo">
				<table class="table table-bordered table-striped table-hover" id="pontoConsumo">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" class="text-center">C�digo</th>
							<th scope="col" class="text-center">Descri��o</th>
							<th scope="col" class="text-center">Endere�o</th>
							<th scope="col" class="text-center">CEP</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
							<tr>
								<td><c:out value="${pontoConsumo.codigoPontoConsumo}"/></td>
								<td><c:out value="${pontoConsumo.descricao}"/></td>
								<td><c:out value="${pontoConsumo.enderecoFormatado}"/></td>
								<td><c:out value="${pontoConsumo.cep.cep}"/></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			
		</c:if>
	
<!--		<c:choose>
			<c:when test="${listaPontoConsumo ne null && not empty listaPontoConsumo}">
				<label class="rotulo">M?s/Ano-Ciclo:</label>
				<span class="itemDetalhamento valorMesAnoCiclo"><c:out value="${faturaForm.anoMesReferencia}"/>-<c:out value="${faturaForm.numeroCiclo}"/></span>
			</c:when>
			<c:otherwise>
				<label class="rotulo">M?s/Ano:</label>
				<span class="itemDetalhamento valorMesAnoCiclo"><c:out value="${faturaForm.anoMesReferencia}"/></span>
			</c:otherwise>
		</c:choose>
-->
			<div class="row form-group">
				<c:choose>
					<c:when test="${listaPontoConsumo ne null && not empty listaPontoConsumo}">
						<label class="col-2 text-right rotulo">M�s/Ano-Ciclo:</label>
					</c:when>
					<c:otherwise>
						<label class="col-2 text-right rotulo">M�s/Ano:</label>
					</c:otherwise>
				</c:choose>
		
				<select name="idAnoMesReferencia" id="idAnoMesReferencia" class="form-control campoSelect">
					<c:forEach items="${anoMesCicloLista}" var="anoMesCiclo">
						<option value="<c:out value="${anoMesCiclo.anoMes}"/>" <c:if test="${faturaForm.idAnoMesReferencia == anoMesCiclo.anoMes}">selected="selected"</c:if>>
							<c:out value="${anoMesCiclo.anoMesFormatado}"/>
						</option>
					</c:forEach>
				</select>
			</div>
			
			<div class="row form-group">
				<label class="col-2 text-right rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Motivo de Inclus�o:</label>
				<select name="idMotivoInclusao" id="idMotivoInclusao" class="form-control campoSelect campoHorizontal">
								<option value="-1">Selecione</option>
					<c:forEach items="${listaMotivoInclusao}" var="motivoInclusao">
						<option value="<c:out value="${motivoInclusao.chavePrimaria}"/>" <c:if test="${faturaForm.idMotivoInclusao == motivoInclusao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${motivoInclusao.descricao}"/>
						</option>
					</c:forEach>
				</select>
			</div>
		</fieldset>
		
		<fieldset id="tabs_" >
			<ul class="nav nav-tabs">
				<c:if test="${itensFatura ne null && not empty itensFatura}">
					<c:if test="${faturaForm.idPontoConsumo ne null && faturaForm.idPontoConsumo > 0}">
						<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#geralFatura">Geral</a></li>
					</c:if>
					<c:if test="${faturaForm.idPontoConsumo ne null && faturaForm.idPontoConsumo > 0}">
						<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#leituraConsumoFatura">Leitura e Consumo</a></li>
					</c:if>
				</c:if>
				<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#debitosCreditosFatura">D�bitos e Cr�ditos</a></li>
				<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes}">
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#dadosFaturasPendentes">Faturas N�o Cobradas</a></li>
				</c:if>
			</ul>
			
			<div class="tab-content">
				<c:if test="${itensFatura ne null && not empty itensFatura}">
					<c:if test="${faturaForm.idPontoConsumo ne null && faturaForm.idPontoConsumo > 0}">
						<fieldset class="tab-pane fade show active" role="tabpanel" id="geralFatura">
							<jsp:include page="/jsp/faturamento/fatura/abaGeralFatura.jsp"/>
						</fieldset>
					</c:if>
				
					<c:if test="${faturaForm.idPontoConsumo ne null && faturaForm.idPontoConsumo > 0}">
						<fieldset class="tab-pane fade" role="tabpanel" id="leituraConsumoFatura">
							<jsp:include page="/jsp/faturamento/fatura/abaLeituraConsumoFatura.jsp"/>
						</fieldset>
					</c:if>
				</c:if>
				
				<fieldset class="tab-pane fade" role="tabpanel" id="debitosCreditosFatura">
					<jsp:include page="/jsp/faturamento/fatura/abaDebitosCreditosFatura.jsp"/>
				</fieldset>
				
				<c:if test="${listaFaturasPendentes ne null && not empty listaFaturasPendentes}">
					<fieldset class="tab-pane fade" role="tabpanel" id="dadosFaturasPendentes">
						<jsp:include page="/jsp/faturamento/fatura/abaFaturasPendentes.jsp"/>
					</fieldset>
				</c:if>
			</div>
			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o da Fatura.</p>
		</fieldset>
	
	<div class="btn-group" role="group" aria-label="Button group">
		<button type="button" class="btn btn-primary" onclick="javascript:cancelar();">Cancelar</button>
		<vacess:vacess param="exibirResumoInclusaoFatura">	
			<button type="submit" class="btn btn-primary">Avan�ar <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
		</vacess:vacess>
	</div>
</form:form>

	</div>

</div>