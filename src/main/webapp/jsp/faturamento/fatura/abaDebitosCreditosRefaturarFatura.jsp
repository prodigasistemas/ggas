<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script>

	function init() {
		<c:forEach items="${faturaForm.map.chavesCreditoDebito}" var="chaveCreditoDebito">
			if(document.getElementById('chaveCreditoDebito'+'<c:out value="${chaveCreditoDebito}"/>') != undefined){
				document.getElementById('chaveCreditoDebito'+'<c:out value="${chaveCreditoDebito}"/>').checked = true;	
			}	
		</c:forEach>

		<c:forEach items="${faturaForm.map.chavesPrimarias}" var="chave">
			var inputCredito = document.getElementById('chaveCredito'+'<c:out value="${chaveCreditoDebito}"/>');
			if(inputCredito != undefined){
				inputCredito.checked = true;
				var inputValorConciliado = document.getElementById('creditoDebito'+'<c:out value="${chaveCreditoDebito}"/>')
				if (inputValorConciliado != undefined) {
					atualizarValorDebitoCredito(inputCredito, inputValorConciliado.value);
				}
			}	
		</c:forEach>

	}

	function atualizarValorDebitoCredito(obj, paramValor) {
		paramValor = paramValor.replace(",",".");
		atualizarValorDebitos(paramValor, obj.checked);
	}

	function atualizarValorDebitos(valor, checked) {
		var inputValorCreditoDebito = document.getElementById('valorCreditoDebito');
		if (inputValorCreditoDebito.value == "") {
			inputValorCreditoDebito.value = 0;
		}
		var valorDebitos = parseFloat(inputValorCreditoDebito.value);
		if (checked) {
			valorDebitos = valorDebitos + parseFloat(valor);
		} else {
			valorDebitos = valorDebitos - parseFloat(valor);
		}
		inputValorCreditoDebito.value = valorDebitos.toFixed(2);
		document.getElementById('saldoDebitosCreditos').innerHTML = 'R$ ' + parseFloat(inputValorCreditoDebito.value).toFixed(2);

	}

	addLoadEvent(init);

</script>

<fieldset>
	<a class="linkHelp" href="<help:help>/refaturamentoabadbitosecrditos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
	<c:if test="${listaCreditoDebito ne null}">			
		<fieldset id="dadosCreditoDebito">
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaCreditoDebito" sort="list" id="creditoDebito" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
				<c:set var="i" value="${i+1}" />					
				<display:column>
					<input type="checkbox" name="chavesCreditoDebito" id="chaveCreditoDebito${creditoDebito.chavePrimaria}" value="${creditoDebito.chavePrimaria}" onclick="atualizarValorDebitoCredito(this, '<c:out value="${creditoDebito.valor}"/>');" />
				</display:column>
			   	<display:column title="Rubrica" style="width: 330px">				   		
					<c:out value="${creditoDebito.creditoDebitoNegociado.rubrica.descricao}"/> 
					(<c:out value="${creditoDebito.numeroPrestacao}"/>/<c:out value="${creditoDebito.creditoDebitoNegociado.quantidadeTotalPrestacoes}"/>)
					<c:if test="${creditoDebito.creditoDebitoNegociado.descricao ne null && creditoDebito.creditoDebitoNegociado.descricao ne ''}">
						<c:out value="${creditoDebito.creditoDebitoNegociado.descricao}" />
					</c:if>
			   	</display:column>
			   	<display:column title="Tipo" >
			   		<c:choose>
						<c:when test="${creditoDebito.creditoDebitoNegociado.creditoOrigem eq null}">D�bito</c:when>
						<c:otherwise>Cr�dito</c:otherwise>
					</c:choose>
			   	</display:column>
			   	<display:column title="Quantidade" >
			   		<fmt:formatNumber value="${creditoDebito.quantidade}" minFractionDigits="2" />
			   	</display:column>
			   	<display:column title="Valor Unit�rio (R$)">
			   		<fmt:formatNumber value="${creditoDebito.valorUnitario}" minFractionDigits="2" />
			   	</display:column>
			   	<display:column title="Valor Total (R$)" >
			   		<input type="hidden" id="creditoDebito${creditoDebito.chavePrimaria}" value="<fmt:formatNumber value='${creditoDebito.valor}' maxFractionDigits='2' minFractionDigits='2'/>"></input>
			   		<fmt:formatNumber value="${creditoDebito.valor}" minFractionDigits="2" />
			   	</display:column>
		    </display:table>	
		</fieldset>			
	</c:if>
	
	<input type="hidden" id="valorCreditoDebito">
	<label class="rotulo rotuloHorizontal">Saldo:</label>
	<span id="saldoDebitosCreditos" class="itemDetalhamento">0.00</span><br class="quebraLinha" />
	
</fieldset>
