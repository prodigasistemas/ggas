<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<div class="card">
	<div class="card-header">
		<h5 class="card-title">Resumo da Fatura<a class="linkHelp" href="<help:help>/resumodefatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h5>
	</div>
	<div class="card-body">
	
<script type="text/javascript">

	$(document).ready(function(){
		// Dialog - configura��es
		$("#incluirFaturaLeituraPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
		//Faz a rolagem de tela at� a parte superior
		$.scrollTo($("#mainContainer"),0);
	});

	<c:if test="${listaDadosInclusaoFatura ne null}">
		<c:set var="count" value="1"/>
		<c:forEach items="${listaDadosInclusaoFatura}">
			animatedcollapse.addDiv('conteinerDadosFatura${count}', 'fade=0,speed=400,hide=1');
			<c:set var="count" value="${count+1}"/>
		</c:forEach>
	</c:if>

	function exibirFatura(elem, valor){
		if(elem.checked){
			animatedcollapse.show('conteinerDadosFatura' + valor);
		}else{
			animatedcollapse.hide('conteinerDadosFatura' + valor);
		}
	}
	
	function voltar() {
		submeter('faturaForm', 'exibirInclusaoFatura');
	}

	function salvarFaturaEncerramento() {				
		submeter('faturaForm', 'incluirFaturaEncerramento');
	}
	
	function exibirDadosLeituraAnterior(leituraAnterior, dataLeituraAnterior, anormalidadeAnterior, volumeAnterior, consumoFaturado, diasConsumo) {
		$("#leituraAnteriorPopup").html(leituraAnterior);
		$("#dataLeituraAnteriorPopup").html(dataLeituraAnterior);
		$("#anormalidadeAnteriorPopup").html(anormalidadeAnterior);
		$("#volumeAnteriorPopup").html(volumeAnterior);
		$("#consumoFaturadoPopup").html(consumoFaturado);
		$("#diasConsumoPopup").html(diasConsumo);
		exibirJDialog("#incluirFaturaLeituraPopup");
	}
	
	<c:if test="${fatura.idCliente ne null && faturaForm.idCliente > 0}">
		animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=1');
	</c:if>
	<c:if test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
		animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=1');
	</c:if>
	<c:if test="${listaPontoConsumo ne null && (faturaForm.idCliente eq null || faturaForm.idCliente eq 0)}">
		animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=1');	
	</c:if>
	
</script>

<form:form method="post" action="incluirFatura" name="faturaForm">
	<token:token></token:token>
	<input name="acao" type="hidden" id="acao" value="incluirFatura">
	<input name="tela" type="hidden" id="tela" value="exibirResumoInclusaoFatura">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idCliente" type="hidden" id="idCliente" value="${faturaForm.idCliente}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
	<input name="idContratoEncerrarRescindir" type="hidden" id="idContratoEncerrarRescindir" value="${faturaForm.idContratoEncerrarRescindir}">	
	<input name="listaIdsPontoConsumoRemovidosAgrupados" type="hidden" id="listaIdsPontoConsumoRemovidosAgrupados" value="${listaIdsPontoConsumoRemovidosAgrupados}">
	
	<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${idSituacao}"/>">
	<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${dataEmissaoInicial}"/>">
	<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${dataEmissaoFinal}"/>">
	<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${numeroDocumentoInicial}"/>">
	<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${numeroDocumentoFinal}"/>">
	<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${codigoCliente}"/>">
	<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${idGrupo}"/>">
	<input name="idRota" type="hidden" id="idRota" value="<c:out value="${idRota}"/>">	
	<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${indicadorRevisao}"/>">
	<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaFrom.idCliente}"/>">
	<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaFrom.idImovel}"/>">
	<input name="anoMesReferencia" type="hidden" id="anoMesReferencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
	<input name="anoMesFormatado" type="hidden" id="anoMesFormatado" value="<c:out value="${faturaForm.anoMesFormatado}"/>">
	<input name="numeroCiiclo" type="hidden" id="ciclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
	
	
	<div id="incluirFaturaLeituraPopup" title="Leitura">
		<label class="rotulo">Leitura Anterior:</label>
		<span class="itemDetalhamento" id="leituraAnteriorPopup"></span><br />
		<label class="rotulo">Data da Leitura Anterior:</label>
		<span class="itemDetalhamento" id="dataLeituraAnteriorPopup"></span><br />
		<label class="rotulo">Anormalidade da Leitura:</label>
		<span class="itemDetalhamento" id="anormalidadeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Volume Anterior Apurado:</label>
		<span class="itemDetalhamento" id="volumeAnteriorPopup"></span><br /><br />
		<label class="rotulo">Consumo Faturado:</label>
		<span class="itemDetalhamento" id="consumoFaturadoPopup"></span><br /><br />
		<label class="rotulo">Dias de Consumo:</label>
		<span class="itemDetalhamento" id="diasConsumoPopup"></span><br /><br />
	</div>
	
	<c:forEach items="${faturaForm.chavesCreditoDebito}" var="chaveCreditoDebito">
		<input name="chavesCreditoDebito" type="hidden" id="chavesCreditoDebito" value="${chaveCreditoDebito}">
	</c:forEach>
	
		<fieldset id="resumoFatura" class="conteinerPesquisarIncluirAlterar">
			<c:if test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
				<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
					<div class="row">
						<div class="col-1">
							<fieldset class="coluna detalhamentoColunaLarga">
								<div class="row">
									<label class="col-2 text-right rotulo">Cliente:</label>
									<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
								</div>
								<div class="row">
									<label class="col-2 text-right rotulo">CPF/CNPJ:</label>
									<span class="col itemDetalhamento"><c:out value="${cliente.numeroDocumentoFormatado}"/></span><br />
								</div>
							</fieldset>
						</div>
						<div class="col">
							<fieldset class="colunaFinal">
								<div class="row">
									<label class="col-2 text-right rotulo">Endere�o:</label>
									<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
								</div>
								<div class="row">
									<label class="col-2 text-right rotulo">E-mail:</label>
									<span class="col itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
								</div>
							</fieldset>
						</div>
					</div>
				</fieldset>
			</c:if>
			
			<c:if test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
				<a id="linkDadosImovel" class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosImovel" class="conteinerDados conteinerBloco">
					<div class="row">
						<div class="col-1">
							<fieldset class="coluna detalhamentoColunaLarga">
								<div class="row">
									<label class="col-2 text-right rotulo">Descri��o:</label>
									<span class="col itemDetalhamento"><c:out value="${imovel.nome}"/></span><br />
								</div>
								<div class="row">
									<label class="col-2 text-right rotulo">Matr�cula:</label>
									<span class="col itemDetalhamento"><c:out value="${imovel.chavePrimaria}"/></span><br />
								</div>
							</fieldset>
						</div>
						<div class="col">
							<fieldset class="colunaFinal">
								<div class="row">
									<label class="col-2 text-right rotulo">Cidade:</label>
									<span class="col itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.nomeMunicipio}"/></span><br />
								</div>
								<div class="row">
									<label class="col-2 text-right rotulo">Endere�o:</label>
									<span class="col itemDetalhamento"><c:out value="${imovel.modalidadeMedicaoImovel.codigo}"/></span><br />
								</div>
							</fieldset>
						</div>
					</div>
				</fieldset><br/>
			</c:if>
			
			<c:if test="${listaPontoConsumo ne null && (faturaForm.idCliente eq null || faturaForm.idCliente eq 0)}">
				<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosPontoConsumo" class="conteinerBloco">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" id="pontoConsumo">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th scope="col" class="text-center">Im�vel - Ponto Consumo</th>
									<th>Segmento</th>
									<th>Situa��o</th>
									<th>N� Medidor</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
									<tr>
										<td>
											<input type="hidden" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}"/>
									   		<c:choose>
									   			 <c:when test="${nomeImovel ne null}">
									   			 	<c:out value="${nomeImovel}"/> - <c:out value="${pontoConsumo.descricao}"/>
									   			 </c:when>
									   			 <c:otherwise>
													<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>							
									   			 </c:otherwise>
									   		</c:choose>
					   					</td>
					   					<td><c:out value="${pontoConsumo.segmento.descricao}"/></td>
					   					<td><c:out value="${pontoConsumo.situacaoConsumo.descricao}"/></td>
					   					<td><c:out value="${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					
				</fieldset>
			</c:if>
			
			<br class="quebraLinha" />
		    
		    <c:if test="${listaHistoricoConsumo ne null && not empty(listaHistoricoConsumo)}">
		    <fieldset class="conteinerBloco">
				<h6>Consumo:</h6>
				
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="consumo">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">Medidor</th>
								<th scope="col" class="text-center">Consumo<br/>Apurado (m<span class='expoente'>3</span>)</th>
								<th scope="col" class="text-center">Consumo<br/>Di�rio (m<span class='expoente'>3</span>)</th>
								<th scope="col" class="text-center">Cr�dito<br/>Consumo (m<span class='expoente'>3</span>)</th>
								<th scope="col" class="text-center">Fator PTZ</th>
								<th scope="col" class="text-center">Fator PCS</th>
								<th scope="col" class="text-center">Fator<br/>Corre��o</th>
								<th scope="col" class="text-center">Dias de<br/>Consumo</th>
								<th scope="col" class="text-center">Tipo de <br/>Consumo</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaHistoricoConsumo}" var="consumo">
								<tr>
									<td class="text-center"><c:out value="${consumo.historicoAtual.historicoInstalacaoMedidor.medidor.numeroSerie}"/></td>
									<td class="text-center"><fmt:formatNumber value="${consumo.consumoApurado}" minFractionDigits="4" maxFractionDigits="4" /></td>
									<td class="text-center"><fmt:formatNumber value="${consumo.consumoDiario}" minFractionDigits="4" maxFractionDigits="4" /></td>
									<td class="text-center"><fmt:formatNumber value="${consumo.consumoCredito}" minFractionDigits="4" maxFractionDigits="4" /></td>
									<td class="text-center"><fmt:formatNumber value="${consumo.fatorPTZ}" minFractionDigits="4" maxFractionDigits="4" /></td>
									<td class="text-center"><fmt:formatNumber value="${consumo.fatorPCS}" minFractionDigits="4" maxFractionDigits="4" /></td>
									<td class="text-center"><fmt:formatNumber value="${consumo.fatorCorrecao}" minFractionDigits="4" maxFractionDigits="4" /></td>
									<td class="text-center"><c:out value="${consumo.diasConsumo}"></c:out></td>
									<td class="text-center"><c:out value="${consumo.tipoConsumo.descricao}"></c:out></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
		    </c:if>
		    
			<br class="quebraLinha" />
			
			<c:if test="${listaDadosLeituraConsumoFatura ne null && not empty(listaDadosLeituraConsumoFatura)}">
			<fieldset class="conteinerBloco">
				<h6>Leitura Consumo:</h6>
				
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="leituraConsumo">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">Leitura Atual</th>
								<th scope="col" class="text-center">Informada <br/>pelo Cliente</th>
								<th scope="col" class="text-center">Data da <br/>Leitura Atual</th>
								<th scope="col" class="text-center">Leiturista</th>
								<th scope="col" class="text-center">Leitura<br/>Anterior</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaDadosLeituraConsumoFatura}" var="leituraConsumo">
								<tr>
									<td class="text-center"><fmt:formatNumber value='${leituraConsumo.leituraAtual}' maxFractionDigits='0'/></td>
									<td class="text-center">
										<c:choose>
											<c:when test="${leituraConsumo.informadoCliente}">
												Sim
											</c:when>
											<c:otherwise>
												N�o
											</c:otherwise>
										</c:choose>
									</td>
									<td class="text-center"><fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' pattern='dd/MM/yyyy'/></td>
									<td class="text-center"><c:out value="${leituraConsumo.leiturista.funcionario.nome}" /></td>
									<td class="text-center">
										<a href='javascript:exibirDadosLeituraAnterior(
											"<fmt:formatNumber value="${leituraConsumo.leituraAnterior}" maxFractionDigits="0"/>",
											"<fmt:formatDate value="${leituraConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>",
											"<c:out value="${leituraConsumo.anormalidadeLeitura.descricao}"/>",
											"<fmt:formatNumber value="${leituraConsumo.volumeAnteriorApurado}" maxFractionDigits="0"/>",
											"<fmt:formatNumber value="${leituraConsumo.consumoFaturado}" maxFractionDigits="0"/>",
											"<c:out value="${leituraConsumo.diasConsumo}"/>");'>
											
											<img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/>
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
		    </c:if>
		    
			<hr class="linhaSeparadoraDetalhamento" />
			
			<fieldset id="conteinerGeral" class="conteinerDados conteinerBloco">
				<h6>Geral:</h6>
				<div class="row">
					<div class="col-2">
						<fieldset  class="coluna detalhamentoColunaLarga">
							<div class="row">
								<label class="col-4 text-right rotulo">M�s/Ano-Ciclo:</label>
								<span class=" col itemDetalhamento">
									<c:out value="${faturaForm.anoMesReferencia}" />
								</span>
							</div>
							<div class="row">
								<label class="col-4 text-right rotulo">Data de Emiss�o:</label>
								<span class="col itemDetalhamento"><c:out value='${dataEmissao}'/></span>
							</div>
						</fieldset>
					</div>
					<div class="col">
						<fieldset  class="colunaFinal">
							<div class="row">
								<label class="col-2 text-right rotulo">Motivo de Inclus�o:</label>
								<span class="col itemDetalhamento"><c:out value="${descricaoMotivoInclusao}" /></span>
							</div>
							<c:if test="${numeroContrato ne null && numeroContrato ne ''}">
								<div class="row">
									<label class="col-2 text-right rotulo">Contrato:</label>
									<span class="col itemDetalhamento"><c:out value="${numeroContrato}" /></span>
								</div>
							</c:if>
						</fieldset>
					</div>
				</div>
			</fieldset>
			
			<c:if test="${listaDadosGerais ne null}">
				<c:set var="i" value="1" />
				<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
					<hr class="linhaSeparadoraDetalhamento" />
					<fieldset class="conteinerDados conteinerBloco">
						<h4><span class="badge badge-info">Fatura <span class="badge badge-light"><c:out value="${i}"/></span></span></h4>
						<div class="row">
							<div class="col-4">
								<div class="row">
									<label class="col-4 text-right rotulo">Data de Vencimento:</label>
									<span class="col-6 itemDetalhamento"><fmt:formatDate value="${dadosGeraisItensFatura.dataVencimento}" pattern="dd/MM/yyyy"/></span>
								</div>
								<div class="row">
									<label class="col-4 text-right rotulo">Tipo da Nota Fiscal:</label>
									<span class="col-6 itemDetalhamento">
										<c:choose>
											<c:when test="${dadosGeraisItensFatura.indicadorProduto}"><c:out value="Produto"/></c:when>
											<c:otherwise><c:out value="Servi�o"/></c:otherwise>
										</c:choose>
									</span>
								</div>
							</div>
						</div>
						
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="item">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center">Descri��o</th>
										<th scope="col" class="text-center">Tipo</th>
										<th scope="col" class="text-center">Data In�cio</th>
										<th scope="col" class="text-center">Data Final</th>
										<th scope="col" class="text-center">Quantidade</th>
										<th scope="col" class="text-center">Unidade</th>
										<th scope="col" class="text-center">Valor Unit�rio (R$)</th>
										<th scope="col" class="text-center">Valor (R$)</th>
										<th scope="col" class="text-center">Valor Cr�dito Dispon�vel (R$)</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${dadosGeraisItensFatura.listaDadosItensFatura}" var="item">
										<tr>
											<td class="text-center">
												<c:choose>
											   		<c:when test="${item.descricaoDesconto ne null}">
											   		    <c:out value="${item.descricaoDesconto}"/>
											   		</c:when>	
										   		</c:choose>	
											</td>
											<td class="text-center">
												<c:choose>
										   			<c:when test="${item.indicadorCredito eq true}">
								   						<c:out value="Cr�dito"></c:out>
								   					</c:when>
								   					<c:otherwise>
									   					<c:out value="D�bito"></c:out>
								   					</c:otherwise>
										   		</c:choose>
											</td>
											<td class="text-center">
												<fmt:formatDate value="${item.dataInicial}"></fmt:formatDate>
											</td>
											<td class="text-center">
												<fmt:formatDate value="${item.dataFinal}"></fmt:formatDate>
											</td>
											<td class="text-center">
												<fmt:formatNumber value="${item.consumo}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
											</td>
											<td class="text-center">
												<c:choose>
										   			<c:when test="${item.itemFatura ne null}">
								   						m<span class="expoente">3</span>
								   					</c:when>
										   		</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
													<c:when test="${item.indicadorCredito eq true}">
														-<fmt:formatNumber value="${item.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
													</c:when>
									   				<c:otherwise>
									   					<fmt:formatNumber value="${item.valorUnitario}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
									   				</c:otherwise>
									   			</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
											   		<c:when test="${item.valorTotal ne null}">
												   		<c:choose>
												   			<c:when test="${item.indicadorCredito eq true}">
										   						-<fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:when>
										   					<c:otherwise>
											   					<fmt:formatNumber value="${item.valorTotal}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:otherwise>
												   		</c:choose>
													</c:when>	
											   		<c:otherwise>
											   			<c:choose>
											   				<c:when test="${item.indicadorCredito eq true}">
										   						-<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:when>
										   					<c:otherwise>
											   					<fmt:formatNumber value="${item.valor}" minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
										   					</c:otherwise>
										   				</c:choose>
											   		</c:otherwise>
											   	</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
													<c:when test="${item.indicadorCredito eq true}">
														-<fmt:formatNumber value="${item.valorTotalDisponivel}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
													</c:when>
									   				<c:otherwise>
									   					<fmt:formatNumber value="${item.valorTotalDisponivel}" minFractionDigits="4" maxFractionDigits="4"></fmt:formatNumber>
									   				</c:otherwise>
									   			</c:choose>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
										
						<c:if test="${dadosGeraisItensFatura.listaDadosTributoVO ne null && not empty dadosGeraisItensFatura.listaDadosTributoVO}">
						
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover" id="tributoVO">
										<thead class="thead-ggas-bootstrap">
											<tr>
												<th scope="col" class="text-center">Tributo</th>
												<th scope="col" class="text-center">Al�quota</th>
												<th scope="col" class="text-center">Base de C�lculo (R$)</th>
												<th scope="col" class="text-center">Valor (R$)</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${dadosGeraisItensFatura.listaDadosTributoVO}" var="tributoVO">
												<tr>
													<td class="text-center"><c:out value="${tributoVO.tributoAliquota.tributo.descricao}"></c:out></td>
													<td class="text-center"><fmt:formatNumber value="${tributoVO.tributoAliquota.valorAliquota * 100}" minFractionDigits="2" maxFractionDigits="2"/>%</td>
													<td class="text-center"><fmt:formatNumber value="${tributoVO.baseCalculo}" minFractionDigits="2" maxFractionDigits="2"/></td>
													<td class="text-center"><fmt:formatNumber value="${tributoVO.valor}" minFractionDigits="2" maxFractionDigits="2"/></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
						</c:if>
					</fieldset>
					<c:set var="i" value="${i+1}" />
					
					
					<div class="container-fluid">
						<label class="rotulo">Valor total da fatura:</label>
						<span class="itemDetalhamento">R$ <fmt:formatNumber value='${dadosGeraisItensFatura.valorTotalFatura}' minFractionDigits="2" maxFractionDigits="2"/></span><br class="quebraLinha" />
					</div>
				</c:forEach>
			</c:if>
			
			<c:if test="${listaFaturasPendentes ne null && not empty(listaFaturasPendentes)}">
				<hr class="linhaSeparadoraDetalhamento" />
				
				<label class="rotulo">Incluir faturas n�o cobradas?</label>
				<input class="campoRadio" type="radio" name="indicadorFaturasPendentes" value="true" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'true' || faturaForm.indicadorFaturasPendentes eq ''}">checked="checked" </c:if>>
				<label class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" name="indicadorFaturasPendentes" value="false" <c:if test="${faturaForm.indicadorFaturasPendentes eq 'false'}">checked="checked" </c:if>>
				<label class="rotuloRadio">N�o</label>
				
				<fieldset class="conteinerBloco">
					<h6>Faturas N�o Cobradas:</h6>
					
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" id="fatura">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th scope="col" class="text-center">N�mero do Documento</th>
									<th scope="col" class="text-center">Data de<br />Emiss�o</th>
									<th scope="col" class="text-center">Data de<br />Vencimento</th>
									<th scope="col" class="text-center">Ciclo /<br />Refer�ncia</th>
									<th scope="col" class="text-center">Valor Total (R$)</th>
									<th scope="col" class="text-center">Situa��o</th>
									<th scope="col" class="text-center">Pagamento</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaFaturasPendentes}" var="fatura">
									<tr>
										<td>
											<input type="hidden" name="chavesFaturasPendentes" value="${fatura.chavePrimaria}" />
											<c:out value="${fatura.chavePrimaria}"/>
										</td>
										<td>
											<fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy"/>
										</td>
										<td>
											<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
										</td>
										<td>
											<c:if test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
												<c:out value="${fatura.cicloReferenciaFormatado}"/>
											</c:if>
										</td>
										<td>
											<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/>
										</td>
										<td>
											<c:out value="${fatura.creditoDebitoSituacao.descricao}"/>
										</td>
										<td>
											<c:out value="${fatura.situacaoPagamento.descricao}"/>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</c:if>
			
		</fieldset>	
		
		<fieldset class="conteinerBotoes">
			<div class="btn-group" role="group" aria-label="">
				<button type="button" class="btn btn-primary" onclick="voltar();"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Voltar</button>
				<c:if test="${empty faturaForm.idContratoEncerrarRescindir}">
					<button type="submit" class="btn btn-primary">Salvar</button>
				</c:if>
				<c:if test="${not empty faturaForm.idContratoEncerrarRescindir}">
					<button type="button" class="btn btn-primary" onclick="salvarFaturaEncerramento();">Salvar Fatura Encerramento</button>
				</c:if>
			</div> 
		</fieldset>
	
</form:form>

	</div>
</div>
