<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<script>

	$(document).ready(function(){
		$("input[name=dataLeitura]").datepicker({
			changeYear: true, 
			maxDate: '+0d', 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy'
		});

		// Dialog - configura��es
		$("#refaturarFaturaLeituraPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
	});

	function exibirDadosLeituraAnterior(leituraAnterior, dataLeituraAnterior, anormalidadeAnterior, volumeAnterior, consumoFaturado, diasConsumo) {
		$("#leituraAnteriorPopup").html(leituraAnterior);
		$("#dataLeituraAnteriorPopup").html(dataLeituraAnterior);
		$("#anormalidadeAnteriorPopup").html(anormalidadeAnterior);
		$("#volumeAnteriorPopup").html(anormalidadeAnterior);
		$("#consumoFaturadoPopup").html(anormalidadeAnterior);
		$("#diasConsumoPopup").html(anormalidadeAnterior);
		exibirJDialog("#refaturarFaturaLeituraPopup");
	}

	function consistir() {
		// Setando a data dos campos de data de leitura nos campos hidden, caso o usu�rio selecione a data pelo calend�rio.
		var datasLeituraAtual = $("input[name=dataLeituraAtual]");
		$("input[name=dataLeitura]").each(function(indice, elem){
			datasLeituraAtual[indice].value = elem.value;
		});
		
		submeter('faturaForm','consistirLeituraFaturamento');
	}

	function habilitarLeiturista(valor, idPontoConsumo) {
		if (valor) {
			$("#leiturista"+idPontoConsumo).val("-1");
			$("#leiturista"+idPontoConsumo).attr("disabled",true);
			$("#informadoCliente"+idPontoConsumo).val("true");
		} else {
			$("#leiturista"+idPontoConsumo).attr("disabled",false);
			$("#informadoCliente"+idPontoConsumo).val("false");
		}
	}

	function carregarHistoricoConsumo(idPontoConsumo) {
		$("#idPontoConsumo").val(idPontoConsumo);
		submeter("faturaForm","carregarHistoricoConsumoFatura");
	}

	function setarLeiturista(idLeiturista, idPontoConsumo) {
		$("#idLeiturista"+idPontoConsumo).val(idLeiturista);
	}

	function init() {
		var medicaoDiaria = "${faturaForm.medicaoDiaria}";

		if (medicaoDiaria != undefined && medicaoDiaria == "true") {
			$(".campoData:disabled + img").each(function(){
				$(this).css("display","none");
			});
		}

		<c:if test="${listaHistoricoConsumo ne null}">
			$.scrollTo($('#conteinerConsumo'),800);
		</c:if>
	}
	
	addLoadEvent(init);

</script>

<div id="refaturarFaturaLeituraPopup" title="Leitura">
	<label class="rotulo">Leitura Anterior:</label>
	<span class="itemDetalhamento" id="leituraAnteriorPopup"></span><br />
	<label class="rotulo">Data da Leitura Anterior:</label>
	<span class="itemDetalhamento" id="dataLeituraAnteriorPopup"></span><br />
	<label class="rotulo">Anormalidade da Leitura:</label>
	<span class="itemDetalhamento" id="anormalidadeAnteriorPopup"></span><br /><br />
	<label class="rotulo">Volume Anterior Apurado:</label>
	<span class="itemDetalhamento" id="volumeAnteriorPopup"></span><br /><br />
	<label class="rotulo">Consumo Faturado:</label>
	<span class="itemDetalhamento" id="consumoFaturadoPopup"></span><br /><br />
	<label class="rotulo">Dias de Consumo:</label>
	<span class="itemDetalhamento" id="diasConsumoPopup"></span><br /><br />
</div>

<fieldset id="conteinerLeitura">
	<legend>Leitura:</legend>
	<a class="linkHelp" href="<help:help>/abaleituraeconsumocadastrodafatura.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a>
	<p class="orientacaoInicial">Clique em um dos pontos de consumo abaixo para exibir os dados de Consumo.</p>
	<c:set var="i" value="0" />
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaLeituraConsumoFatura" sort="list" id="leituraConsumo" requestURI="#">
	   	<display:column title="Im�vel - Ponto Consumo" style="width: 130px">
	   		<input type="hidden" name="chavesPontoConsumo" id="idPontoConsumo${leituraConsumo.idPontoConsumo}" value="${leituraConsumo.idPontoConsumo}"/>
	   		<input type="hidden" name="chavesHistoricoMedicao" id="idMedicaoHistorico${leituraConsumo.idHistoricoMedicao}" value="${leituraConsumo.idHistoricoMedicao}"/>
			
			<a href='javascript:carregarHistoricoConsumo(<c:out value="${leituraConsumo.idPontoConsumo}"/>);'>
				<c:out value="${leituraConsumo.descricaoPontoConsumo}"/>
			</a>	
	   	</display:column>
	   	
	   	<display:column title="Leitura Atual">
	   		<input name="leituraAtual" id="leituraAtual${leituraConsumo.idPontoConsumo}" type="hidden" value="<fmt:formatNumber value='${leituraConsumo.leituraAtual}' minFractionDigits='3'/>"/>
	   		<input class="campoValor" type="text" size="14" maxlength="<c:out value="${leituraConsumo.qtdDigitosMedidor}"/>" onkeypress="return formatarCampoInteiro(event);" 
	   			onblur="$('#leituraAtual'+${leituraConsumo.idPontoConsumo}).val(this.value);" 
	   			value="<fmt:formatNumber value='${leituraConsumo.leituraAtual}' maxFractionDigits='0'/>" 
	   			<c:if test="${faturaForm.medicaoDiaria eq 'true'}">disabled="disabled"</c:if>/>
	   	</display:column>    

	   	<display:column title="Informada <br/>pelo Cliente" style="text-align: center">
	   		<input name="indicadorLeituraCliente" id="informadoCliente${leituraConsumo.idPontoConsumo}" type="hidden" value="<c:choose><c:when test='${leituraConsumo.informadoCliente}'>true</c:when><c:otherwise>false</c:otherwise></c:choose>"/>
	   		<input name="informadoCliente" type="checkbox" <c:if test="${leituraConsumo.informadoCliente}">checked="checked"</c:if> 
	   			onclick="habilitarLeiturista(this.checked,'<c:out value="${leituraConsumo.idPontoConsumo}"/>');" 
	   			<c:if test="${faturaForm.medicaoDiaria eq 'true'}">disabled="disabled"</c:if> />
	   	</display:column>    
	   
	   	<display:column title="Data da /<br/>Leitura Atual">
	   		<input name="dataLeituraAtual" id="dataLeituraAtual${leituraConsumo.idPontoConsumo}" type="hidden" value="<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' pattern='dd/MM/yyyy'/>" />
	   		<input class="campoData" name="dataLeitura" type="text" size="8" maxlength="10" value="<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}' 
	   			pattern='dd/MM/yyyy'/>" <c:if test="${faturaForm.medicaoDiaria eq 'true'}">disabled="disabled"</c:if> 
	   			onblur="$('#dataLeituraAtual'+${leituraConsumo.idPontoConsumo}).val(this.value);" />
	   	</display:column>
	   	
	   	<display:column title="Leiturista">
	   		<input id="idLeiturista${leituraConsumo.idPontoConsumo}" name="idLeiturista" type="hidden" value="${leituraConsumo.leiturista.chavePrimaria}"/>
	   		<select id="leiturista${leituraConsumo.idPontoConsumo}" class="campoSelect" <c:if test="${leituraConsumo.informadoCliente || faturaForm.medicaoDiaria eq 'true'}">disabled="disabled"</c:if> onchange="setarLeiturista(this.value,'${leituraConsumo.idPontoConsumo}');">
		    	<option value="-1">Selecione</option>				
				<c:forEach items="${listaLeiturista}" var="leiturista">
					<option value="<c:out value='${leiturista.chavePrimaria}'/>" <c:if test="${leituraConsumo.leiturista.chavePrimaria == leiturista.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${leiturista.funcionario.nome}"/>
					</option>		
				</c:forEach>
			</select>
	   	</display:column>
	   	
	   	<display:column title="Consumo<br/>Apurado (m<span class='expoente'>3</span>)">
	   		<input name="consumoApurado" id="consumoApurado${leituraConsumo.idPontoConsumo}" type="hidden" value="<fmt:formatNumber value='${leituraConsumo.consumoApurado}' minFractionDigits='3'/>" />
	   		<input class="campoValor" type="text" size="12" maxlength="14" onkeypress="return formatarCampoDecimalPositivo(event, this, 9, 4)" 
	   			onblur="aplicarMascaraNumeroDecimal(this, 4); $('#consumoApurado'+${leituraConsumo.idPontoConsumo}).val(this.value);" value="<fmt:formatNumber value='${leituraConsumo.consumoApurado}' 
	   			minFractionDigits='3'/>" <c:if test="${faturaForm.medicaoDiaria eq 'true'}">disabled="disabled"</c:if>  />
	   	</display:column>
	   	
	   	<display:column title="Leitura<br/>Anterior">
	   		<a href='javascript:exibirDadosLeituraAnterior(
				"<fmt:formatNumber value="${leituraConsumo.leituraAnterior}" maxFractionDigits="0"/>",
				"<fmt:formatDate value="${leituraConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>",
				"<c:out value="${leituraConsumo.anormalidadeLeitura.descricao}"/>",
				"<fmt:formatNumber value="${leituraConsumo.volumeAnteriorApurado}" minFractionDigits="4"/>",
				"<fmt:formatNumber value="${leituraConsumo.consumoFaturado}" minFractionDigits="4"/>",
				"<c:out value="${leituraConsumo.diasConsumo}"/>");'>
				
				<img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/>
			</a>	
	   	</display:column>
	   	
	   	<c:set var="i" value="${i+1}" />
    </display:table>
	<input name="Button" class="bottonRightCol2 botaoGrande1" value="Consistir"  type="button" onclick="consistir();">
</fieldset>
	
<c:if test="${listaHistoricoConsumo ne null}">
	<hr class="linhaSeparadora2" />
	<fieldset id="conteinerConsumo">
		<legend>Consumo:</legend>
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaHistoricoConsumo" sort="list" id="consumo" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			<display:column title="Data" style="width: 75px">
				<fmt:formatDate value="${consumo.historicoAtual.dataLeituraInformada}" pattern="dd/MM/yyyy" />
			</display:column>
			
			<display:column title="Leitura">
				<fmt:formatNumber value="${consumo.historicoAtual.numeroLeituraInformada}" maxFractionDigits="0" />
			</display:column>
			
			<display:column title="Consumo<br/>Apurado" style="width: 95px">
				<fmt:formatNumber value="${consumo.consumoApurado}" minFractionDigits="4" maxFractionDigits="4" />
			</display:column>
			
			<display:column title="Consumo<br/>Di�rio" style="width: 95px">
				<fmt:formatNumber value="${consumo.consumoDiario}" minFractionDigits="4" maxFractionDigits="4" />
			</display:column>
			
			<display:column title="Cr�dito<br/>Consumo" style="width: 95px">
				<fmt:formatNumber value="${consumo.consumoCredito}" minFractionDigits="4" maxFractionDigits="4" />
			</display:column>
			
			<display:column title="Fator PTZ" style="width: 60px">
				<fmt:formatNumber value="${consumo.fatorPTZ}" minFractionDigits="4" maxFractionDigits="4" />
			</display:column>
			
			<display:column title="Fator PCS" style="width: 60px">
				<fmt:formatNumber value="${consumo.fatorPCS}" minFractionDigits="4" maxFractionDigits="4" />
			</display:column>
			
			<display:column title="Fator<br/>Corre��o" style="width: 60px">
				<fmt:formatNumber value="${consumo.fatorCorrecao}" minFractionDigits="4" maxFractionDigits="4" />
			</display:column>
			
			<display:column title="Dias de<br/>Consumo" style="width: 60px">
				<c:out value="${consumo.diasConsumo}"></c:out>
			</display:column>
			
			<display:column title="Tipo de <br/>Consumo" style="width: 90px">
				<c:out value="${consumo.tipoConsumo.descricao}"></c:out>
			</display:column>
		</display:table>
	</fieldset>
</c:if>