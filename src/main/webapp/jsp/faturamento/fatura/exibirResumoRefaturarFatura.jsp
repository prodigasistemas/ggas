<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
	
<script type="text/javascript">

	$(document).ready(function(){
		// Dialog - configura��es
		$("#incluirFaturaLeituraPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});

		iniciarDatatable('#pontoConsumo', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});
		
		iniciarDatatable('#consumo', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});

		iniciarDatatable('#leituraConsumo', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});

		iniciarDatatable('#item', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});

		iniciarDatatable('#tributoVO', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});

		iniciarDatatable('#fatura', {
			searching: false,
			info:false,
			paging: false,
			ordering: false
		});
	});

	<c:if test="${listaDadosInclusaoFatura ne null}">
		<c:set var="count" value="1"/>
		<c:forEach items="${listaDadosInclusaoFatura}">
			animatedcollapse.addDiv('conteinerDadosFatura${count}', 'fade=0,speed=400,hide=1');
			<c:set var="count" value="${count+1}"/>
		</c:forEach>
	</c:if>

	function exibirFatura(elem, valor){
		if(elem.checked){
			animatedcollapse.show('conteinerDadosFatura' + valor);
		}else{
			animatedcollapse.hide('conteinerDadosFatura' + valor);
		}
	}
	
	function voltar() {
		submeter('faturaForm', 'exibirRefaturar');
	}

	function exibirDadosLeituraAnterior(leituraAnterior, dataLeituraAnterior, anormalidadeAnterior, volumeAnterior, consumoFaturado, diasConsumo) {
		$("#leituraAnteriorPopup").html(leituraAnterior);
		$("#dataLeituraAnteriorPopup").html(dataLeituraAnterior);
		$("#anormalidadeAnteriorPopup").html(anormalidadeAnterior);
		$("#volumeAnteriorPopup").html(anormalidadeAnterior);
		$("#consumoFaturadoPopup").html(anormalidadeAnterior);
		$("#diasConsumoPopup").html(anormalidadeAnterior);
		exibirJDialog("#incluirFaturaLeituraPopup");
	}


</script>


<div class="responsivo">

	<div class="card">
		<div class="card-header">
			<h5 class="card-title">
				Resumo da Fatura
			</h5>
		</div>
	<div class="card-body">	



		<form:form method="post" action="refaturarFatura" name="faturaForm">
			
		<input name="acao" type="hidden" id="acao" value="refaturarFatura">
		<input name="postBack" type="hidden" id="postBack" value="false">
		<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}" />
		<input name="idFatura" type="hidden" id="idFatura" value="${faturaForm.idFatura}" />
		<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
		<input name="indicadorRefuramento" type="hidden" id="indicadorRefuramento" value="${indicadorRefuramento}"/>
		<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaLista">
			<input name="chavesPrimarias" type="hidden" id="chavesPrimarias${chavePrimariaLista}" value="${chavePrimariaLista}">
		</c:forEach>
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
		<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
		<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
		<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${faturaForm.numeroDocumentoInicial}"/>">
		<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${faturaForm.numeroDocumentoFinal}"/>">
		<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${faturaForm.codigoCliente}"/>">
		<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
		<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
		<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
		<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
		<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">
		<input name="numeroCiclo" type="hidden" id="numeroCiclo" value="<c:out value="${faturaForm.numeroCiclo}"/>">
		<input name="anoMesReferencia" type="hidden" id="anoMesReferencia" value="<c:out value="${faturaForm.anoMesReferencia}"/>">
		<input name="numeroNotaFiscalInicial" type="hidden" id="numeroNotaFiscalInicial" value="<c:out value="${faturaForm.numeroNotaFiscalInicial}"/>">
		<input name="numeroNotaFiscalFinal" type="hidden" id="numeroNotaFiscalFinal" value="<c:out value="${faturaForm.numeroNotaFiscalFinal}"/>">
		<input name="tipoFatura" type="hidden" id="tipoFatura" value="<c:out value="${faturaForm.tipoFatura}"/>">
		<input name="indicadorRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.indicadorRevisao}"/>">
		<input name="indicadorComplementar" type="hidden" id="indicadorComplementar" value="<c:out value="${faturaForm.indicadorComplementar}"/>">
		<input name="tipoDocumento" type="hidden" id="chavePrimaria" value="1">
		<input name="idSituacaoPagamento" type="hidden" id="idSituacaoPagamento" value="<c:out value="${faturaForm.idSituacaoPagamento}"/>">
		<input name="idPontoConsumo" type="hidden"  id="idPontoConsumo" value="${faturaForm.idPontoConsumo}">
		<input name="indicadorPesquisaPontos" type="hidden" id="indicadorPesquisaPontos" value="${faturaForm.indicadorPesquisaPontos}">
		<input type="hidden" name="descricaoMotivoInclusao" id="descricaoMotivoInclusao"/>			
			
			<div id="incluirFaturaLeituraPopup" title="Leitura">
				<label class="rotulo">Leitura Anterior:</label>
				<span class="itemDetalhamento" id="leituraAnteriorPopup"></span><br />
				<label class="rotulo">Data da Leitura Anterior:</label>
				<span class="itemDetalhamento" id="dataLeituraAnteriorPopup"></span><br />
				<label class="rotulo">Anormalidade da Leitura:</label>
				<span class="itemDetalhamento" id="anormalidadeAnteriorPopup"></span><br /><br />
				<label class="rotulo">Volume Anterior Apurado:</label>
				<span class="itemDetalhamento" id="volumeAnteriorPopup"></span><br /><br />
				<label class="rotulo">Consumo Faturado:</label>
				<span class="itemDetalhamento" id="consumoFaturadoPopup"></span><br /><br />
				<label class="rotulo">Dias de Consumo:</label>
				<span class="itemDetalhamento" id="diasConsumoPopup"></span><br /><br />
			</div>
			
			<c:forEach items="${faturaForm.chavesFaturasPendentes}" var="chaveFatura">
				<input name="chavesFaturasPendentes" type="hidden" id="chavesFaturasPendentes" value="${chaveFatura}">
			</c:forEach>
			<c:forEach items="${faturaForm.chavesCreditoDebito}" var="chaveCreditoDebito">
				<input name="chavesCreditoDebito" type="hidden" id="chavesCreditoDebito" value="${chaveCreditoDebito}">
			</c:forEach>
			
					<c:if
						test="${faturaForm.idCliente ne null && faturaForm.idCliente > 0}">
						<div class="row">
							<h5 class="col-md-12 mt-2">Dados Cliente</h5>
							<div class="col-md-6">
								<ul class="list-group">
									<li class="list-group-item">Cliente: <span
										class="font-weight-bold"> <c:out
												value='${cliente.nome}' /></span>
									</li>
									<li class="list-group-item">CPF/CNPJ: <span
										class="font-weight-bold"> <c:out
												value='${cliente.numeroDocumentoFormatado}' /></span>
									</li>
								</ul>
							</div>

							<div class="col-md-6">
								<ul class="list-group">
									<li class="list-group-item">Endere�o: <span
										class="font-weight-bold"> <c:out
												value='${cliente.enderecoPrincipal.enderecoFormatado}' /></span>
									</li>
									<li class="list-group-item">CPF/CNPJ: <span
										class="font-weight-bold"> <c:out
												value='${cliente.emailPrincipal}' /></span>
									</li>
								</ul>
							</div>
						</div>
						<hr>
						<br />
					</c:if>
					
					
					<c:if
						test="${faturaForm.idImovel ne null && faturaForm.idImovel > 0}">
						<div class="row">
							<h5 class="col-md-12 mt-2">Dados Im�vel</h5>
							<div class="col-md-6">
								<ul class="list-group">
									<li class="list-group-item">Descri��o: <span
										class="font-weight-bold"> <c:out value='${imovel.nome}' /></span>
									</li>
									<li class="list-group-item">Matr�cula: <span
										class="font-weight-bold"> <c:out
												value='${imovel.chavePrimaria}' /></span>
									</li>
								</ul>
							</div>

							<div class="col-md-6">
								<ul class="list-group">
									<li class="list-group-item">Cidade: <span
										class="font-weight-bold"> <c:out
												value='${imovel.quadraFace.endereco.cep.nomeMunicipio}' /></span>
									</li>
									<li class="list-group-item">Endere�o: <span
										class="font-weight-bold"> <c:out
												value='${imovel.modalidadeMedicaoImovel.codigo}' /></span>
									</li>
								</ul>
							</div>
						</div>
						<hr>
						<br />
					</c:if>
					
					
					<c:if test="${listaPontoConsumo ne null && (faturaForm.idCliente eq null || faturaForm.idCliente eq 0)}">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover"
								name="listaPontoConsumo" id="pontoConsumo" width="100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center">Im�vel - Ponto Consumo</th>
										<th scope="col" class="text-center">Segmento</th>
										<th scope="col" class="text-center">Situa��o</th>
										<th scope="col" class="text-center">N� Medidor</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
										<tr>
											<td class="text-center"><input type="hidden"
												name="idPontoConsumo"
												id="idPontoConsumo${pontoConsumo.chavePrimaria}"
												value="${pontoConsumo.chavePrimaria}" /> <c:choose>
													<c:when test="${nomeImovel ne null}">
														<c:out value="${nomeImovel}" /> - <c:out
															value="${pontoConsumo.descricao}" />
													</c:when>

													<c:otherwise>
														<c:choose>
															<c:when
																test="${pontoConsumo.imovel.nome ne null && fn:trim(pontoConsumo.imovel.nome) ne ''}">
																<c:out value="${pontoConsumo.imovel.nome}" /> - <c:out
																	value="${pontoConsumo.descricao}" />
															</c:when>
															<c:otherwise>
																<c:out value="${pontoConsumo.descricao}" />
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose></td>
												<td class="text-center">
													<c:out value="${pontoConsumo.segmento.descricao}" />
												</td>												
												<td class="text-center">
													<c:out value="${pontoConsumo.situacaoConsumo.descricao}" />
												</td>
												<td class="text-center">
													<c:if
														test="${pontoConsumo.instalacaoMedidor ne null && pontoConsumo.instalacaoMedidor.medidor ne null}">
														<c:out
															value="${pontoConsumo.instalacaoMedidor.medidor.numeroSerie}" />
													</c:if>
												</td>
											</tr>									
									</c:forEach>
								</tbody>
							</table>
						</div>
					</c:if>

					<c:if
						test="${listaHistoricoConsumo ne null && not empty(listaHistoricoConsumo)}">
						<h5 class="mt-2 mb-3">Consumo:</h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover"
								name="listaHistoricoConsumo" id="consumo" width="100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center">Medidor</th>
																				<th scope="col" class="text-center">Data</th>
																				<th scope="col" class="text-center">Leitura</th>
										<th scope="col" class="text-center">Consumo Apurado</th>
										<th scope="col" class="text-center">Consumo Di�rio</th>
										<th scope="col" class="text-center">Cr�dito Consumo</th>
										<th scope="col" class="text-center">Fator PTZ</th>
										<th scope="col" class="text-center">Fator PCS</th>	
										<th scope="col" class="text-center">Fator Corre��o</th>	
										<th scope="col" class="text-center">Dias de Consumo</th>	
										<th scope="col" class="text-center">Tipo de Consumo</th>																																																		
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaHistoricoConsumo}" var="consumo">
										<tr>
											<td class="text-center">
												<c:out
													value="${consumo.historicoAtual.historicoInstalacaoMedidor.medidor.numeroSerie}" />	
											</td>	
											<td class="text-center">
												<fmt:formatDate value="${consumo.historicoAtual.dataLeituraInformada}" pattern="dd/MM/yyyy" />	
											</td>
											<td class="text-center">
												<fmt:formatNumber value="${consumo.historicoAtual.numeroLeituraInformada}" maxFractionDigits="0" />	
											</td>											
											<td class="text-center">
												<fmt:formatNumber value="${consumo.consumoApurado}"
													minFractionDigits="4" maxFractionDigits="4" />			
											</td>
											<td class="text-center">
												<fmt:formatNumber value="${consumo.consumoDiario}"
													minFractionDigits="4" maxFractionDigits="4" />			
											</td>	
											<td class="text-center">
												<fmt:formatNumber value="${consumo.consumoCredito}"
													minFractionDigits="4" maxFractionDigits="4" />			
											</td>		
											<td class="text-center">
												<fmt:formatNumber value="${consumo.fatorPTZ}"
													minFractionDigits="4" maxFractionDigits="4" />			
											</td>			
											<td class="text-center">
												<fmt:formatNumber value="${consumo.fatorPCS}"
													minFractionDigits="4" maxFractionDigits="4" />			
											</td>		
											<td class="text-center">
												<fmt:formatNumber value="${consumo.fatorCorrecao}"
													minFractionDigits="4" maxFractionDigits="4" />			
											</td>
											<td class="text-center">
												<c:out
													value="${consumo.diasConsumo}" />	
											</td>	
											<td class="text-center">
												<c:out
													value="${consumo.tipoConsumo.descricao}" />	
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<br class="quebraLinha" />
					</c:if>
					
					<c:if
						test="${listaDadosLeituraConsumoFatura ne null && not empty(listaDadosLeituraConsumoFatura)}">
						<h5 class="mt-2 mb-3">Leitura e Consumo</h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover"
								name="listaDadosLeituraConsumoFatura" id="leituraConsumo" width="100%" style="opacity: 0;">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center">Leitura Atual</th>
										<th scope="col" class="text-center">Informada pelo CLiente</th>
										<th scope="col" class="text-center">Data da Leitura Atual</th>
										<th scope="col" class="text-center">Leiturista</th>
										<th scope="col" class="text-center">Consumo Apurado (m3)</th>
										<th scope="col" class="text-center">Leitura Anterior</th>																																																	
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaDadosLeituraConsumoFatura}" var="leituraConsumo">
										<tr>
											<td class="text-center">
												<fmt:formatNumber value="${leituraConsumo.leituraAtual}"
												maxFractionDigits="0" />			
											</td>
											<td class="text-center">
												<c:choose>
													<c:when test="${leituraConsumo.informadoCliente}">
									   					Sim
									   				</c:when>
													<c:otherwise>
									   					N�o
									   				</c:otherwise>
												</c:choose>
											</td>
											<td class="text-center">
												<fmt:formatDate value='${leituraConsumo.dataLeituraAtual}'
													pattern='dd/MM/yyyy' />											
											</td>
											<td class="text-center">
												<c:out
													value="${leituraConsumo.leiturista.funcionario.nome}" />	
											</td>
											<td class="text-center">
												<fmt:formatNumber value='${leituraConsumo.consumoApurado}' maxFractionDigits='0'/>
											</td>
											<td class="text-center">
												<a
													href='javascript:exibirDadosLeituraAnterior(
												"<fmt:formatNumber value="${leituraConsumo.leituraAnterior}" maxFractionDigits="0"/>",
												"<fmt:formatDate value="${leituraConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>",
												"<c:out value="${leituraConsumo.anormalidadeLeitura.descricao}"/>",
												"<fmt:formatNumber value="${leituraConsumo.volumeAnteriorApurado}" maxIntegerDigits="14" maxFractionDigits="4"/>",
												"<fmt:formatNumber value="${leituraConsumo.consumoFaturado}" maxFractionDigits="0"/>",
												"<c:out value="${leituraConsumo.diasConsumo}"/>");'>
				
													<img border="0"
													src="<c:url value="/imagens/icone_exibir_detalhes.png"/>" />
												</a>											
											</td>																																
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</c:if>
				    
					<hr class="linhaSeparadoraDetalhamento" />
								
					<div class="row">
						<h5 class="col-md-12 mt-2">Geral</h5>
						<div class="col-md-6">
							<ul class="list-group">
								<li class="list-group-item">M�s/Ano Ciclo: <span
									class="font-weight-bold"> 
										<c:out value="${faturaForm.anoMesReferencia}" /><c:if test="${faturaForm.numeroCiclo ne ''}">-<c:out value="${faturaForm.numeroCiclo}" /></c:if>
									</span>
								</li>
								<li class="list-group-item">Data de Emiss�o: <span
									class="font-weight-bold"> <c:out
										value='${dataEmissao}' /></span>
								</li>
							</ul>
						</div>
						
						<div class="col-md-6">
							<ul class="list-group">
								<li class="list-group-item">Motivo Inclus�o: <span
									class="font-weight-bold"> 
										<c:out value="${descricaoMotivoInclusao}" /></span>
								</li>
								<li class="list-group-item">Contrato: <span
									class="font-weight-bold"> <c:out
										value='${numeroContrato}' /></span>
								</li>
							</ul>
						</div>
					</div>
					
					
					
					
										
					
					<c:if test="${listaDadosGerais ne null}">
						<c:set var="i" value="1" />
						<c:forEach items="${listaDadosGerais}" var="dadosGeraisItensFatura">
							<hr class="linhaSeparadoraDetalhamento" />
							
							<div class="row">
								<h5 class="col-md-12 mt-2">Fatura <c:out value="${i}"/></h5>
								<div class="col-md-6">
									<ul class="list-group">
										<li class="list-group-item">Data de Vencimento: <span
											class="font-weight-bold"><fmt:formatDate
										value="${dadosGeraisItensFatura.dataVencimento}"
										pattern="dd/MM/yyyy" /></span>
										</li>
										<li class="list-group-item">Tipo da Nota Fiscal: <span
											class="font-weight-bold"> 
												<c:choose>
													<c:when test="${dadosGeraisItensFatura.indicadorProduto}">
														<c:out value="Produto"/></c:when>
													<c:otherwise>
														<c:out value="Servi�o"/></c:otherwise>
													</c:choose>
											</span>
										</li>									
									</ul>
								</div>
								
								<div class="col-md-6">
									<ul class="list-group">
										<li class="list-group-item">Valor total da fatura: <span
											class="font-weight-bold"> 
											R$ <fmt:formatNumber
												value='${dadosGeraisItensFatura.valorTotalFatura}'
												minFractionDigits="2" maxFractionDigits="2" />
										</span>
										</li>	
									</ul>
								</div>
							</div>
							<br/>													

									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover"
											name="listaDadosItensFaturaExibicao" id="item" width="100%"
											style="opacity: 0;">
											<thead class="thead-ggas-bootstrap">
												<tr>
													<th scope="col" class="text-center">Descri��o</th>
													<th scope="col" class="text-center">Tipo</th>
													<th scope="col" class="text-center">Data In�cio</th>
													<th scope="col" class="text-center">Data Final</th>
													<th scope="col" class="text-center">Quantidade Dias</th>
													<th scope="col" class="text-center">Valor (R$)</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach
													items="${dadosGeraisItensFatura.listaDadosItensFaturaExibicao}"
													var="item">
													<tr>
														<td class="text-center"><c:choose>
																<c:when test="${item.descricaoDesconto ne null}">
																	<c:out value="${item.descricaoDesconto}" />
																</c:when>
																<c:when
																	test="${item.creditoDebitoARealizar ne null && item.creditoDebitoARealizar.creditoDebitoNegociado ne null 
													   						&& item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica ne null && item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao ne null}">
																	<c:out
																		value="${item.creditoDebitoARealizar.creditoDebitoNegociado.rubrica.descricaoImpressao}" />
																</c:when>
																<c:when
																	test="${item.faturaItem ne null && item.faturaItem.rubrica ne null && item.pontoConsumo ne null 
													   						&& item.pontoConsumo.instalacaoMedidor ne null && item.pontoConsumo.instalacaoMedidor.medidor ne null}">
																	<c:out
																		value="${item.faturaItem.rubrica.descricaoImpressao}" />
																</c:when>
																<c:otherwise>
																	<c:out value="${item.rubrica.descricaoImpressao}" />
																</c:otherwise>
															</c:choose></td>
														<td class="text-center"><c:choose>
																<c:when test="${item.indicadorCredito eq true}">
																	<c:out value="Cr�dito"></c:out>
																</c:when>
																<c:otherwise>
																	<c:out value="D�bito"></c:out>
																</c:otherwise>
															</c:choose></td>
														<td class="text-center"><fmt:formatDate
																value="${item.dataInicial}"></fmt:formatDate></td>
														<td class="text-center"><fmt:formatDate
																value="${item.dataFinal}"></fmt:formatDate></td>
														<td class="text-center"><fmt:formatNumber 
															value="${item.quantidadeDias}" maxFractionDigits="0"></fmt:formatNumber>
														</td>
														<td class="text-center"><c:choose>
																<c:when test="${item.valorTotal ne null}">
																	<c:choose>
																		<c:when test="${item.indicadorCredito eq true}">
											   						-<fmt:formatNumber value="${item.valorTotal}"
																				minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																		</c:when>
																		<c:otherwise>
																			<fmt:formatNumber value="${item.valorTotal}"
																				minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																		</c:otherwise>
																	</c:choose>
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${item.indicadorCredito eq true}">
											   						-<fmt:formatNumber value="${item.valor}"
																				minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																		</c:when>
																		<c:otherwise>
																			<fmt:formatNumber value="${item.valor}"
																				minFractionDigits="2" maxFractionDigits="2"></fmt:formatNumber>
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>

									<c:if
										test="${dadosGeraisItensFatura.listaDadosTributoVO ne null && not empty dadosGeraisItensFatura.listaDadosTributoVO}">
										<hr>
										<div class="table-responsive">
											<h5 class="col-md-12 mt-2">Tributos</h5>
											<table class="table table-bordered table-striped table-hover"
												name="listaDadosTributoVO" id="tributoVO" width="100%">
												<thead class="thead-ggas-bootstrap">
													<tr>
														<th scope="col" class="text-center">Tributo</th>
														<th scope="col" class="text-center">Al�quota</th>
														<th scope="col" class="text-center">Base de C�lculo
															(R$)</th>
														<th scope="col" class="text-center">Valor (R$)</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach
														items="${dadosGeraisItensFatura.listaDadosTributoVO}"
														var="tributoVO">
														<tr>
															<td class="text-center"><c:out value="${tributoVO.tributoAliquota.tributo.descricao}" /></td>
															<td class="text-center"><fmt:formatNumber
																value="${tributoVO.tributoAliquota.valorAliquota}"
																minFractionDigits="2" maxFractionDigits="2" />%</td>
															<td class="text-center">
																<fmt:formatNumber value="${tributoVO.baseCalculo}"
																	minFractionDigits="2" maxFractionDigits="2" />															
															</td>
															<td class="text-center">
															<fmt:formatNumber value="${tributoVO.valor}"
																minFractionDigits="2" maxFractionDigits="2" />															
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</c:if>
							
							<br class="quebraLinha" />
						<c:set var="i" value="${i+1}" />
						</c:forEach>
					</c:if>
					
				<c:if test="${listaFaturasPendentes ne null && not empty(listaFaturasPendentes)}">
					<hr class="linhaSeparadoraDetalhamento" />
					<div class="card">
						<div class="card-body bg-light">
							<div class="row">
								<div class="col-md-6">
									<div class="form-row">
										<label for="indicadorFaturasPendentes">Incluir faturas
											n�o cobradas? </label>
										<div class="col-md-12">
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="indicadorFaturasPendentesSim"
													name="indicadorFaturasPendentes"
													class="custom-control-input" value="true"
													<c:if test="${faturaForm.indicadorFaturasPendentes eq 'true'}">checked="checked" </c:if>>
												<label class="custom-control-label"
													for="indicadorFaturasPendentesSim">Sim</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="indicadorFaturasPendentesNao"
													name="indicadorFaturasPendentesSim"
													class="custom-control-input" value="false"
													<c:if test="${faturaForm.indicadorFaturasPendentes eq 'false' || faturaForm.indicadorFaturasPendentes eq ''}">checked="checked" </c:if>>
												<label class="custom-control-label"
													for="indicadorFaturasPendentesNao">N�o</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							</br>
							<div class="table-responsive">
								<h5 class="col-md-12 mt-2">Faturas N�o Cobradas</h5>
								<table class="table table-bordered table-striped table-hover"
									name="listaFaturasPendentes" id="fatura" width="100%">
									<thead class="thead-ggas-bootstrap">
										<tr>
											<th scope="col" class="text-center">N�mero do Documento</th>
											<th scope="col" class="text-center">Data de Emiss�o</th>
											<th scope="col" class="text-center">Data de Vencimento</th>
											<th scope="col" class="text-center">Ciclo/Refer�ncia</th>
											<th scope="col" class="text-center">Valor Total (R$)</th>
											<th scope="col" class="text-center">Situa��o</th>
											<th scope="col" class="text-center">Pagamento</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach
											items="${dadosGeraisItensFatura.listaDadosTributoVO}"
											var="tributoVO">
											<tr>
												<td class="text-center"><input type="hidden"
													name="chavesFaturasPendentes"
													value="${fatura.chavePrimaria}" /> <c:out
														value="${fatura.chavePrimaria}" /></td>
												<td class="text-center"><fmt:formatDate
														value="${fatura.dataEmissao}" pattern="dd/MM/yyyy" /></td>
												<td class="text-center"><fmt:formatDate
														value="${fatura.dataVencimento}" pattern="dd/MM/yyyy" /></td>
												<td class="text-center"><c:if
														test="${fatura.anoMesReferencia ne null && fatura.numeroCiclo ne null}">
														<c:out value="${fatura.cicloReferenciaFormatado}" />
													</c:if></td>
												<td class="text-center"><fmt:formatNumber
														value="${fatura.valorTotal}" minFractionDigits="2" /></td>
												<td class="text-center"><c:out
														value="${fatura.creditoDebitoSituacao.descricao}" /></td>
												<td class="text-center"><c:out
														value="${fatura.situacaoPagamento.descricao}" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</c:if>



				<div class="card-footer">
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
								type="button" onclick="voltar();">
								<i class="fa fa-times"></i> Voltar
							</button>
							<button id="buttonSalvar"
								class="btn btn-sm btn-success float-right ml-1 mt-1"
								type="submit" name="buttonIncluir" id="botaoIncluir">
								<i class="fa fa-save"></i> Salvar
							</button>
						</div>
					</div>
				</div>
				
		</form:form>
		</div>
	</div>
</div>