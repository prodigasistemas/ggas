<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>




<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">

$(document).ready(function(){

	$('#tabs').removeAttr("style");

	
	var datepicker = $.fn.datepicker.noConflict();
	$.fn.bootstrapDP = datepicker;  
	
	$('.bootstrapDP').bootstrapDP({
	    autoclose: true,
		format: 'dd/mm/yyyy',
		language: 'pt-BR'
	});

	$('input[name="emissaoCliente"]').change(function () {
		atualizarCamposEmissaoCliente();
	});

	var divNumSerie;
	function atualizarCamposEmissaoCliente() {
		var emissaoCliente = $('input[name="emissaoCliente"]:checked').val();

		
		if (emissaoCliente == 0) {
			$('#numeroSerieDevolucao').val('');
			$('#numeroNotaDevolucao').val('');
			$('#numeroSerieDevolucao').prop('disabled', true);
			$('#numeroNotaDevolucao').prop('disabled', true);
			$('#numeroNotaDevolucao').addClass('disabled');
			$('#numeroSerieDevolucao').addClass('disabled');
			$('#divNumSerie').fadeOut(400, function(){ divNumSerie = $(this).detach(); });
		} else {
			divNumSerie.appendTo('#divNumSerieContainer').fadeIn(800);
			divNumSerie = null;
			$('#numeroSerieDevolucao').prop('disabled', false);
			$('#numeroNotaDevolucao').prop('disabled', false);
			$('#numeroNotaDevolucao').removeClass('disabled');
			$('#numeroSerieDevolucao').removeClass('disabled');
		}
	}

	atualizarCamposEmissaoCliente();
});

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=0');

	function cancelar() {
		submeter('faturaForm','pesquisarFatura');
	}

	function exibirMensagemErro(mensagem) {
		var $mensagemSpring = $('.mensagensSpring');
		$mensagemSpring.html(
				$("<div>", {class : 'failure notification hideit'}).append(
						$("<p>").append(
								$("<strong>").html("Erro: "), mensagem
						)
				)
		).show().click(function(){
			$mensagemSpring.hide()
		});
		document.getElementsByClassName("mensagensSpring")[0].scrollIntoView();
	}

	function itensValidos() {
		var valido = true;
		$('.checkItem:checked').each(function(){
			var linha = $(this).closest('tr');
			var qtd = Number(linha.find('.qtdItem').val().replace(',', '.'));
			var vlr = Number(linha.find('.vlrUnitario').val().replace(',', '.'));
			if (qtd <= 0) {
				exibirMensagemErro("O campo 'Quantidade' na tabela de itens deve ser maior que zero nos itens marcados para devolu��o.");
				valido = false;
				return false;
			}
			if (vlr <= 0) {
				exibirMensagemErro("O campo 'Valor Unit�rio' na tabela de itens deve ser maior que zero nos itens marcados para devolu��o.");
				valido = false;
				return false;
			}
		});
		if (valido) {
			if ($('.checkItem:checked').size() > 0 && valido){
				return true;
			} else {
				exibirMensagemErro("Selecione ao menos um item para devolu��o.");
				return false;
			}
		}
		return false;
	}

	function incluir() {
		if (itensValidos()) {
			$('.checkItem:checkbox:enabled').not(':checked').closest('tr').find('.qtdItem').prop('disabled', true);
			$('.checkItem:checkbox:enabled').not(':checked').closest('tr').find('.vlrUnitario').prop('disabled', true);
			submeter('faturaForm','incluirNFDFatura');
		}
	}
	
	function setarObrigatorio() {
		var indicadorNormal = document.getElementById("indicadorTipoNotaNormal").checked;
		
		if (indicadorNormal){
			var normal = true;
		}else{
			var normal = false;
		}
		
        var tagSpan = document.getElementById("chaveObrigatorio");
        if (tagSpan != undefined) {
        	$('#chaveObrigatorio').hide();
        }   
        var tagSpan2 = document.getElementById("protocoloObrigatorio");
        if (tagSpan2 != undefined) {
        	$('#protocoloObrigatorio').hide();
        }   
		if (normal){
        	$('#chaveObrigatorio').hide();
        	$('#protocoloObrigatorio').hide();
			document.getElementById("chaveAcessoDevolucao").value = '';
			document.getElementById("numeroProtocoloDevolucao").value = '';
		} else {
        	$('#chaveObrigatorio').hide();
        	$('#protocoloObrigatorio').hide();
        	$("#chaveObrigatorio").css("display", "block");
        	$("#protocoloObrigatorio").css("display", "block");
		}
        
	}

</script>
<style>
	.disabled {
		background-color: #ebebe4 !important;
	}
</style>


<div class="responsivo">
	<form:form method="post" action="exibirIncluirNFDFatura" name="faturaForm">
		<input name="acao" type="hidden" id="acao" value="incluirNFDFatura">
		<input name="indexLista" type="hidden" id="indexLista" value="-1">
		<input name="postBack" type="hidden" id="postBack" value="false">
		<input name="abaId" type="hidden" id="abaId" value="${abaId}"/>
		<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${faturaForm.idPontoConsumo}" />
		<input name="idFatura" type="hidden" id="idFatura" value="${faturaForm.idFatura}" />
		<input name="numeroCiclo" type="hidden" id="ciclo" value="${faturaForm.numeroCiclo}" />
		<input name="anoMesReferencia" type="hidden" id="referencia" value="${faturaForm.anoMesReferencia}" />
		<input name="indicadorRefuramento" type="hidden" id="indicadorRefuramento" value="${indicadorRefuramento}"/>
		<c:forEach items="${faturaForm.chavesPrimarias}" var="chavePrimariaLista">
			<input name="chavesPrimarias" type="hidden" id="chavesPrimarias${chavePrimariaLista}" value="${chavePrimariaLista}">
		</c:forEach>
		<input name="idSituacao" type="hidden" id="idSituacao" value="<c:out value="${faturaForm.idSituacao}"/>">
		<input name="dataEmissaoInicial" type="hidden" id="dataEmissaoInicial" value="<c:out value="${faturaForm.dataEmissaoInicial}"/>">
		<input name="dataEmissaoFinal" type="hidden" id="dataEmissaoFinal" value="<c:out value="${faturaForm.dataEmissaoFinal}"/>">
		<input name="numeroDocumentoInicial" type="hidden" id="numeroDocumentoInicial" value="<c:out value="${faturaForm.numeroDocumentoInicial}"/>">
		<input name="numeroDocumentoFinal" type="hidden" id="numeroDocumentoFinal" value="<c:out value="${faturaForm.numeroDocumentoFinal}"/>">
		<input name="codigoCliente" type="hidden" id="codigoCliente" value="<c:out value="${codigoCliente}"/>">
		<input name="idGrupo" type="hidden" id="idGrupo" value="<c:out value="${faturaForm.idGrupo}"/>">
		<input name="idRota" type="hidden" id="idRota" value="<c:out value="${faturaForm.idRota}"/>">	
		<input name="existeMotivoRevisao" type="hidden" id="indicadorRevisao" value="<c:out value="${faturaForm.existeMotivoRevisao}"/>">
		<input name="idCliente" type="hidden" id="idCliente" value="<c:out value="${faturaForm.idCliente}"/>">
		<input name="idImovel" type="hidden" id="idImovel" value="<c:out value="${faturaForm.idImovel}"/>">


		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Incluir Nota Fiscal de Devolu��o</h5>
			</div>
			<div class="card-body bg-light">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos abaixo para realizar a inclus�o.
				</div>

				<div class="card">
					<div class="card-body bg-light">
						<div class="row">
							<div class="col-md-6">
								<div class="form-row">
									<label for="emissaoCliente">Nota emitida pelo cliente:<span
										class="text-danger">*</span></label>
									<div class="col-md-12">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="emissaoClienteSim"
												name="emissaoCliente" class="custom-control-input"
												value="1"
												<c:if test="${empty faturaForm.emissaoCliente or faturaForm.emissaoCliente == 1}">checked</c:if>>
											<label class="custom-control-label" for="emissaoClienteSim">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="emissaoClienteNao"
												name="emissaoCliente" class="custom-control-input"
												value="0"
												<c:if test="${faturaForm.emissaoCliente == 0}">checked</c:if>>
											<label class="custom-control-label" for="emissaoClienteNao">N�o</label>
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-10">
										<label for="idMotivoCancelamento">Motivo:<span
											class="text-danger">*</span></label> <select
											name="idMotivoCancelamento" id="idMotivoCancelamento"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaMotivoCancelamento}"
												var="motivoCancelamento">
												<option
													value="<c:out value="${motivoCancelamento.chavePrimaria}"/>"
													<c:if test="${faturaForm.idMotivoCancelamento == motivoCancelamento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${motivoCancelamento.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div id="divNumSerieContainer">
									<div id="divNumSerie">
										<div class="form-row">
											<div class="col-md-10">
												<label for="numeroSerieDevolucao">S�rie:<span
													class="text-danger">*</span></label> <input type="text"
													id="numeroSerieDevolucao" name="numeroSerie"
													class="form-control form-control-sm" maxlength="10"
													size="12"
													onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
													value="${faturaForm.numeroSerie}">
											</div>
										</div>

										<div class="form-row">
											<div class="col-md-10">
												<label for="numeroNotaDevolucao">N�mero:<span
													class="text-danger">*</span></label> <input type="text"
													id="numeroNotaDevolucao" name="numeroNotaDevolucao"
													class="form-control form-control-sm" maxlength="9"
													size="12" onkeypress="return formatarCampoInteiro(event);"
													value="${faturaForm.numeroNotaDevolucao}">
											</div>
										</div>

									</div>
								</div>

								<div class="form-row">
									<div class="col-md-10">
										<label for="dataDevolucao">Data de emiss�o: <span
											class="text-danger">* </span></label>
										<div class="input-group input-group-sm">
											<input type="text" aria-label="dataDevolucao"
												id="dataDevolucao"
												class="form-control form-control-sm bootstrapDP"
												name="dataDevolucao" maxlength="10"
												value="${faturaForm.dataDevolucao}">
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col-md-10">
										<label for="faturaOrigem">Fatura de Origem:</label> <input
											type="text" id="faturaOrigem" name="faturaOrigem"
											class="form-control form-control-sm" size="12"
											value="${dadosResumoFatura.getIdFatura()}" disabled>
									</div>
								</div>

							</div>

							<div class="col-md-6">
								<div class="form-row">
									<label for="indicadorTipoNotaNormalEletronica">Tipo de nota:<span
										class="text-danger">*</span></label>
									<div class="col-md-12">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="indicadorTipoNotaNormal"
												name="indicadorTipoNotaNormalEletronica"
												class="custom-control-input" value="0"
												<c:if test="${faturaForm.indicadorTipoNotaNormalEletronica == 0}">checked</c:if>
												onclick="setarObrigatorio();"> <label
												class="custom-control-label" for="indicadorTipoNotaNormal">Normal</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="indicadorTipoNotaEletronica"
												name="indicadorTipoNotaNormalEletronica"
												class="custom-control-input" value="1"
												<c:if test="${faturaForm.indicadorTipoNotaNormalEletronica == 1}">checked</c:if>
												onclick="setarObrigatorio();"> <label
												class="custom-control-label"
												for="indicadorTipoNotaEletronica">Eletr�nica</label>
										</div>
									</div>
								</div>
								
								<span id="chaveObrigatorio">
									<div class="form-row">
										<div class="col-md-10">
											<label for="numeroNotaDevolucao">Chave de Acesso:<span
												class="text-danger">*</span></label> <input type="text"
												id="chaveAcessoDevolucao" name="chaveAcesso"
												class="form-control form-control-sm" maxlength="44" size="30"
												onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
												value="${faturaForm.chaveAcesso}">
										</div>
									</div>
								</span>

								<span id="protocoloObrigatorio">
									<div class="form-row">
										<div class="col-md-10">
											<label for="numeroNotaDevolucao">N�mero do protocolo:<span
												class="text-danger">*</span></label> <input type="text"
												id="numeroProtocoloDevolucao" name="numeroProtocoloDevolucao"
												class="form-control form-control-sm" maxlength="100"
												size="30"
												onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
												value="${faturaForm.numeroProtocoloDevolucao}">
										</div>
									</div>
								</span>

								<div class="form-row">
									<div class="col-md-10">
										<label for="observacoesDevolucao2"
											id="rotuloObservacaoVigencia">Observa��es:</label>
										<textarea class="form-control" id="observacoesDevolucao2"
											rows="4" cols="50" name="observacoesDevolucao"><c:out
												value="${faturaForm.observacoesDevolucao}" /></textarea>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>


				<br />
				
   			 	<div class="card mt-3">
                    <div class="card-header">
	   			 		<ul class="nav nav-tabs card-header-tabs" id="tabs" role="selecaoTabs">
	   			 			<c:if test="${(listaDadosGerais ne null && not empty listaDadosGerais)}">
		  						<li class="nav-item">
		   						 	<a class="nav-link active" id="itens-tab" data-toggle="tab" href="#itensFatura" role="tab" 
		   						 	aria-controls="itensFatura"><i class="fa fa-coins"></i><strong> Itens</strong> <span class="text-danger">*</span></a>
		  						</li>
		  					</c:if>
							<c:if test="${(dadosResumoFatura ne null && not empty dadosResumoFatura)}">	  					
		 						<li class="nav-item">
		    						<a class="nav-link" id="geral-tab" data-toggle="tab" href="#geralFatura" role="tab"
		    						 aria-controls="geralFatura"><i class="fas fa-wallet"></i><strong> Geral</strong> <span class="text-danger">*</span></a>
		  						</li>
	  						</c:if>
							<c:if test="${(listaDadosGerais ne null && not empty listaDadosGerais)}">  						
		  						<li class="nav-item">
		   							 <a class="nav-link" id="tributos-tab" data-toggle="tab" href="#tributosFatura" role="tab"
		   							  aria-controls="tributosFatura"><i class="fas fa-money-bill-wave"></i><strong> Tributos </strong><span class="text-danger">*</span></a>
		  						</li>
	  					    </c:if>
						</ul>
					</div>
					
					<div class="card-body">
						<div class="tab-content" id="notaFiscalSaida">
		  					<c:if test="${(listaDadosGerais ne null && not empty listaDadosGerais)}">
		  						<div class="tab-pane fade active show" id="itensFatura" role="tabpanel" aria-labelledby="itens-tab">
		  							<jsp:include page="/jsp/faturamento/fatura/abaItensIncluirNFDFatura.jsp" />
		  						</div>
		  					</c:if>
							<c:if test="${(dadosResumoFatura ne null && not empty dadosResumoFatura)}">
		  						<div class="tab-pane fade" id="geralFatura" role="tabpanel" aria-labelledby="geral-tab">
		  							<jsp:include page="/jsp/faturamento/fatura/abaGeralIncluirNFDFatura.jsp" />
		  						</div>
		  					</c:if>		  					
							<c:if test="${listaDadosGerais ne null && not empty listaDadosGerais}">		  					
		  						<div class="tab-pane fade" id="tributosFatura" role="tabpanel" aria-labelledby="tributos-tab">
		  							<jsp:include page="/jsp/faturamento/fatura/abaTributosIncluirNFDFatura.jsp" />
		  						</div>
		  					</c:if>
						</div>
   			 		</div>
   			 	</div>			

				</div>
				
				<div class="card-footer">
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
								type="button" onclick="cancelar();">
								<i class="fa fa-times"></i> Cancelar
							</button>
                  		<button id="buttonSalvar" class="btn btn-sm btn-success float-right ml-1 mt-1"  type="button" onclick="incluir();">
                       		<i class="fa fa-save"></i> Salvar               		
                       	</button>
						</div>
					</div>
				</div>			
				<script language="javascript">setarObrigatorio();</script>
			</div>
				
	</form:form>
</div>