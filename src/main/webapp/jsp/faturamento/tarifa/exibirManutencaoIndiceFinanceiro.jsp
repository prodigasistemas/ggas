<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">�ndice Financeiro<a class="linkHelp" href="<help:help>/mantendondicefinanceiro.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para consultar �ndices Financeiros passados ou futuros, selecione o per�odo desejado e clique em <span class="destaqueOrientacaoInicial">Exibir</span>.
Para registrar um novo �ndice Financeiro, preencha os dados e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.</p>

<script>

	// Flag para saber se o campo dataFim j� foi setado com a data somada.
	var dataFimSetada = false;

	$(document).ready(function(){

		// Datepicker
	
		$("#dataFim").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy', 
			maxDate: '+0d',
			beforeShow: function() {
				setarDataFim(document.getElementById('dataInicio').value);
			},
			onSelect: function() {
				dataFimSetada = true;
			}
		});

		$("#dataInicio").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy', 
			maxDate: '+0d',
			onSelect: function() {
				setarFlagFalse();
				validarValorCampoDataFim();
			}
		});
		
		$('input[name=valorIndice]').each(function() {
		    var input = $(this),
		    text = input.val().replace(".", ",");
		    input.val(text);
		});
		
		
		$('input[name=valorIndice]').autoNumeric({
			vMax: '999999999.999999',
			aDec: ',',
			aSep: '.',
			vMin: '<c:out value="${valorMinimo}"/>'
		}).trigger('focusout'); //trigger focusout para formatar logo ao carregar os valores

		
	});

	function validarValorCampoDataFim() {
		var dataMaxima = obterDataMaxima(document.getElementById('dataInicio').value);
		if (dataMaxima != null) {
			var valorDataFim = document.getElementById("dataFim").value;
	        if (valorDataFim != null && valorDataFim != "") {
	        	arrayData = valorDataFim.split("/");
	    		dia = arrayData[0];
	    		mes = arrayData[1]-1;
	    		ano = arrayData[2];
	    		if (dia[0] == "0") {
	    			dia = dia[1];
	    		}
	    		var dataCampoFim = new Date(ano,mes,dia);
				if (dataMaxima != null && dataCampoFim > dataMaxima) {
					dia = dataMaxima.getDate();
					if (dia < 10) {
						dia = "0"+dia;
					}
					mes = dataMaxima.getMonth() + 1;
					if (mes < 10) {
						mes = "0"+mes;
					}
					ano = dataMaxima.getFullYear();
					document.getElementById("dataFim").value = dia+"/"+mes+"/"+ano;
				}
		    }
		}
	}
	
	//abilita campos para serem enviados ao servidor
	function abilitarValorIndiceAntesSalvar(){		
		$(".campoTexto").removeAttr("disabled");
	}
	
	function setarFlagFalse() {
		var dataInicio = document.getElementById('dataInicio').value;
		AjaxService.isDataValida( dataInicio, {
        	callback: function(retorno) {            		      		         		
				if (retorno) {
					dataFimSetada = false;
				}
            }, async:false}
        );
	}

	function setarDataFim(dataInicio) {
		if (dataFimSetada) {
			return false;
		} else {
			// Sempre seta a data m�nima do campo dataFim.
			var arrayData = dataInicio.split("/");
			var dia = arrayData[0];
			var mes = arrayData[1]-1;
			var ano = arrayData[2];
			if (dia[0] == "0") {
				dia = dia[1];
			}
			
			$("#dataFim").datepicker("option", "minDate", new Date(ano,mes,dia));
			
			var dataMaximaFim = obterDataMaxima(dataInicio);
			if (dataMaximaFim != null) {
				$("#dataFim").datepicker("option", "maxDate", dataMaximaFim);
			}
		}
	}

	// A dataFim est� sendo setada no beforeShow do datepicker do campo dataFim.
	function obterDataMaxima(data) {
		var dataValida = false;
		AjaxService.isDataValida( data, {
        	callback: function(retorno) {            		      		         		
				if (retorno) {
					dataValida = true;
				}
            }, async:false}
        );

		if (dataValida) {
			var dataAtual = $.datepicker.formatDate('dd/mm/yy', new Date());

			// Verificando intervalo da data selecionada e a data atual.
			var intervaloDatas = false;
			AjaxService.intervaloDatas( data, dataAtual, {
	        	callback: function(intervalo) {            		      		         		
					if (intervalo > 90) {
						intervaloDatas = true;
					}
	            }, async:false}
	        );

			var dataMaxima = null;
			// Caso o intervalo da data selecionada e a data atual seja maior que 90, a data m�xima � (data selecionada + 90), 
			// se n�o a data m�xima � a data atual
			if (intervaloDatas) {
	        	AjaxService.adicionarDiasData( data, 90, {
	            	callback: function(dataSomada) {
	                	var arrayData = dataSomada.split("/");
	            		var dia = arrayData[0];
	            		var mes = arrayData[1]-1;
	            		var ano = arrayData[2];
	            		if (dia[0] == "0") {
	            			dia = dia[1];
	            		}
	            		dataMaxima = new Date(ano,mes,dia);
	                }, async:false}
	            );
	        } else {
	        	dataMaxima = new Date();
		    }

		    return dataMaxima;
		}
		
	}
	
	function exibirIndicesFinanceiros() {
		submeter('indiceFinanceiroForm', 'exibirIndiceFinanceiro');
	}

	function init() {
		var dataInicio = "${indiceFinanceiroFormVO.dataInicio}";
		if (dataInicio != null && dataInicio != "") {
			setarDataFim(dataInicio);
		}
	}
	
	addLoadEvent(init);

	function limparCampos() {
		location.href = "<c:url value='/exibirManutencaoIndiceFinanceiro'/>";
	}

	function carregarTipoValor(idIndiceFinanceiro) {
		submeter("indiceFinanceiroForm","exibirManutencaoIndiceFinanceiro");
	}

	function setarValorHidden(valor, data) {
		document.getElementById('valorNominal'+data).value = valor;
	}

</script>

<form method="post" id="indiceFinanceiroForm" name="indiceFinanceiroForm" onsubmit="abilitarValorIndiceAntesSalvar()" action="atualizarIndiceFinanceiro">
	<input name="acao" type="hidden" id="acao" value="atualizarIndiceFinanceiro"/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idIndice" type="hidden" id="idIndice" value="${idIndiceFinanceiro}">
	<token:token></token:token>
	
	<fieldset id="dadosIndiceFinanceiro" class="conteinerPesquisarIncluirAlterar">		
		<fieldset class="coluna">
			<label class="rotulo campoObrigatorio" for="ano"><span class="campoObrigatorioSimbolo">* </span>�ndice Financeiro:</label>
			<select tabindex="2" name="idIndiceFinanceiro" id="idIndiceFinanceiro" class="campoSelect" onchange="carregarTipoValor(this.value);">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaIndiceFinanceiro}" var="indiceFinanceiro">
		    		<option value="<c:out value="${indiceFinanceiro.chavePrimaria}"/>" <c:if test="${idIndiceFinanceiro == indiceFinanceiro.chavePrimaria}">selected="selected"</c:if> >
		    			<c:out value="${indiceFinanceiro.descricao}"/>
		    		</option>
		    	</c:forEach>
		    </select><br />
		    
			<label class="rotulo" for="idTipoValor">Tipo:</label>
			<input type="hidden" id="descricaoTipoValor" name="descricaoTipoValor" value="${indiceFinanceiroForm.map.descricaoTipoValor}"/>
		    <span id="idTipoValor" class="itemDetalhamento">
		    	<c:if test="${indicadorPercentual eq 'true'}">
		    		<c:out value="Percentual (%)"/>
		    	</c:if>
		    	<c:if test="${indicadorPercentual eq 'false'}">
		    		<c:out value="Valor (R$)"/>
		    	</c:if>
		    </span>
		    
		    <label class="rotulo" for="descricaoPeriodicidade">Periodicidade:</label>
		    <input type="hidden" id="periodicidade" name="descricaoPeriodicidade" value="${indiceFinanceiroForm.map.descricaoPeriodicidade}"/>
		    <span id="descricaoPeriodicidade" class="itemDetalhamento">
		    	<c:if test="${indicadorMensal eq 'true'}">
		    		<c:out value="Mensal"/>
		    	</c:if>
		    	<c:if test="${indicadorMensal eq 'false'}">
		    		<c:out value="Di�ria"/>
		    	</c:if>
		    </span>
	    </fieldset>
		
		<fieldset class="colunaFinal">
		
			<c:if test="${indicadorMensal eq 'true'}">
				<fieldset id="dadosIndiceMensal">
					<label class="rotulo campoObrigatorio" for="mesInicio"><span class="campoObrigatorioSimbolo">* </span>In�cio (m�s/ano):</label>
					<select tabindex="3" name="mesInicio" id="mesInicio" class="campoSelect campoHorizontal">
				    	<option value="-1">Selecione</option>
				    	<c:forEach items="${listaMeses}" var="mesAno">
				    		<option value="<c:out value="${mesAno.key}"/>" <c:if test="${indiceFinanceiroFormVO.mesInicio == mesAno.key}">selected="selected"</c:if>>
				    			<c:out value="${mesAno.value}"/>
				    		</option>
				    	</c:forEach>
				    </select>
					<select tabindex="3" name="anoInicio" id="anoInicio" class="campoSelect">
				    	<option value="-1">Selecione</option>
				    	<c:forEach items="${listaAnos}" var="mesAno">
				    		<option value="<c:out value="${mesAno}"/>" <c:if test="${indiceFinanceiroFormVO.anoInicio == mesAno}">selected="selected"</c:if>>
				    			<c:out value="${mesAno}"/>
				    		</option>
				    	</c:forEach>
				    </select><br />
				
					<label class="rotulo campoObrigatorio" for="mesFim"><span class="campoObrigatorioSimbolo">* </span>Fim (m�s/ano):</label>
					<select tabindex="4" name="mesFim" id="mesFim" class="campoSelect campoHorizontal">
				    	<option value="-1">Selecione</option>
				    	<c:forEach items="${listaMeses}" var="mesAno">
				    		<option value="<c:out value="${mesAno.key}"/>" <c:if test="${indiceFinanceiroFormVO.mesFim == mesAno.key}">selected="selected"</c:if>>
				    			<c:out value="${mesAno.value}"/>
				    		</option>
				    	</c:forEach>
				    </select>
					<select tabindex="4" name="anoFim" id="anoFim" class="campoSelect">
				    	<option value="-1">Selecione</option>
				    	<c:forEach items="${listaAnos}" var="mesAno">
				    		<option value="<c:out value="${mesAno}"/>" <c:if test="${indiceFinanceiroFormVO.anoFim == mesAno}">selected="selected"</c:if>>
				    			<c:out value="${mesAno}"/>
				    		</option>
				    	</c:forEach>
				    </select>
				</fieldset>
			</c:if>
			
			<c:if test="${indicadorMensal eq 'false'}">
				<fieldset id="dadosIndiceDiario">
					<label class="rotulo campoObrigatorio" for="dataInicio"><span class="campoObrigatorioSimbolo">* </span>In�cio:</label>
					<input class="campoData campoHorizontal" type="text" name="dataInicio" id="dataInicio" size="8" value="${indiceFinanceiroFormVO.dataInicio}" onblur="setarFlagFalse(); validarValorCampoDataFim();">
					<br />
					<label class="rotulo campoObrigatorio" for="dataFim"><span class="campoObrigatorioSimbolo">* </span>Fim:</label>
					<input class="campoData campoHorizontal" type="text" name="dataFim" id="dataFim" size="8" value="${indiceFinanceiroFormVO.dataFim}">
				</fieldset>
			</c:if>
		</fieldset>
		
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<vacess:vacess param="exibirIndiceFinanceiro">
				<input name="Button" class="bottonRightCol2 botaoExibir" value="Exibir" type="button" onclick="exibirIndicesFinanceiros();">
			</vacess:vacess>
				<input name="Button" id="botaoLimpar" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCampos();" >
		</fieldset>
	</fieldset>
	    
	<c:if test="${listaIndiceFinanceiroCarregado ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<fieldset class="conteinerPesquisarIncluirAlterar">
			<c:if test="${indicadorMensal eq 'true'}">
				<c:set var="i" value="0" />
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas"
					name="listaIndiceFinanceiroCarregado" id="indiceFinanceiro">
					<display:column title="Ano">
						<fmt:formatDate value='${indiceFinanceiro.dataReferencia}'
							pattern='yyyy' />
					</display:column>
					<display:column title="M�s">
						<input type="hidden" name="mesIndiceFinanceiro"
							value="<fmt:formatDate value='${indiceFinanceiro.dataReferencia}' pattern='dd/MM/yyyy'/>" />
						<c:out value="${meses[i]}"></c:out>
					</display:column>
					<display:column title="Valor Nominal">
						<input type="hidden" name="indicadorUtilizado"
							value="${indiceFinanceiro.indicadorUtilizado}" />
						<input type="text"
							<c:if test="${indiceFinanceiro.indicadorUtilizado eq true}">DISABLED</c:if>
							class="campoTexto <c:if test="${indiceFinanceiro.indicadorUtilizado eq true}">campoDesabilitado</c:if>"
							name="valorIndice" id="valorIndice"
							value="${indiceFinanceiro.valorNominal}" />
					</display:column>
					<c:set var="i" value="${i+1}" />
				</display:table>
			</c:if>
			<c:if test="${indicadorMensal  eq 'false'}">
				<table id="indiceFinanceiro"
					class="dataTableGGAS dataTableCabecalho2Linhas">
					<thead>
						<tr>
							<th>Data</th>
							<th>Valor Nominal</th>
							<c:if test="${fn:length(listaIndiceFinanceiroCarregado) > 1}">
								<th>Data</th>
								<th>Valor Nominal</th>
							</c:if>
							<c:if test="${fn:length(listaIndiceFinanceiroCarregado) > 2}">
								<th>Data</th>
								<th>Valor Nominal</th>
							</c:if>
							<c:if test="${fn:length(listaIndiceFinanceiroCarregado) > 3}">
								<th>Data</th>
								<th>Valor Nominal</th>
							</c:if>
						</tr>
					</thead>
					<tbody>
						<c:set var="count" value="0" />
						<c:set var="cssClass" value="odd" />
						<tr class="${cssClass}">
							<c:forEach items="${listaIndiceFinanceiroCarregado}"
								var="indiceFinanceiro">
								<td><input type="hidden" name="dataIndiceFinanceiro"
									value="<fmt:formatDate value='${indiceFinanceiro.dataReferencia}' pattern='dd/MM/yyyy'/>" />
									<fmt:formatDate value="${indiceFinanceiro.dataReferencia}"
										pattern="dd/MM/yyyy" /></td>
								<td><input type="hidden" name="indicadorUtilizado"
									value="${indiceFinanceiro.indicadorUtilizado}" /> <input
									type="text"
									class="campoTexto <c:if test="${indiceFinanceiro.indicadorUtilizado == 'true'}">campoDesabilitado</c:if>"
									name="valorIndice" value="${indiceFinanceiro.valorNominal}"
									<c:if test="${indiceFinanceiro.indicadorUtilizado == 'true'}">disabled="disabled"</c:if> />
								</td>
								<c:choose>
									<c:when test="${count % 4  == 3}">
										<c:choose>
											<c:when test="${cssClass == 'odd'}">
												<c:set var="cssClass" value="even" />
											</c:when>
											<c:otherwise>
												<c:set var="cssClass" value="odd" />
											</c:otherwise>
										</c:choose>
										<c:if
											test="${count < fn:length(listaIndiceFinanceiroCarregado)-1}">
						</tr>
						<tr class="${cssClass}">
							</c:if>
							</c:when>
							</c:choose>

							<c:set var="count" value="${count+1}" />
							</c:forEach>

							<c:if test="${count >= 4 && count % 4 != 0}">
								<c:forEach var="i" begin="0" end="${3 - (count % 4)}">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</c:forEach>
						</tr>
						</c:if>
					</tbody>
				</table>
			</c:if>
		</fieldset>
		<fieldset class="conteinerBotoes">
			<vacess:vacess param="atualizarIndiceFinanceiro">
				<input name="Button" id="botaoSalvar" class="bottonRightCol2 botaoGrande1" value="Salvar" type="submit">
			</vacess:vacess>
		</fieldset>
	</c:if>
</form>

