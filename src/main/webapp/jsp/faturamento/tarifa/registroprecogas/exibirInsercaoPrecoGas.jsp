<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Incluir Registro de Pre�o do G�s<a class="linkHelp" href="<help:help>/inclusoalteraoderegistrodepreodogs.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="inserirPrecoGas" name="precoGasForm">

	<script language="javascript">

		$(document).ready(function(){

			if(${empty precoGas.indicadorComplementar}) {
				document.forms[0].indicadorComplementar1.checked = false;
				document.forms[0].indicadorComplementar2.checked = true;				
			} else {
				listarPrecoGasComplementar();
			}			

			$("input#indicadorComplementar1").click(function(){
				animatedcollapse.show('complementar');
				animatedcollapse.hide('naoComplementar');
			});
			
			$("input#indicadorComplementar2").click(function(){
				animatedcollapse.hide('complementar');
				animatedcollapse.show('naoComplementar');
			});			

		    $("#dataEmissaoNotaFiscal,.dataEmissaoNotaFiscal").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', maxDate: '0d'});

		    $("#dataFornecimentoInicio,.dataFornecimentoInicio").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, 
					buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', maxDate: '0d',	onSelect: function() {listarPrecoGasComplementar();}});
		    
		    $("#dataFornecimentoFim,.dataFornecimentoFim").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', 
			    	buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', maxDate: '0d',	onSelect: function() {listarPrecoGasComplementar();}});

		    $("#dataVencimento,.dataVencimento").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		    
		});
			
		function incluirPrecoGasItem(form, actionPrecoGAsItem) {
			submeter('precoGasForm',  'adicionarPrecoGasItem');
		}

		function removerPrecoGasItem(indice) {
			document.forms[0].indexLista.value = indice;
			submeter('precoGasForm', 'excluirPrecoGasItem?operacaoLista=true');		
		}		
				
		function incluirPrecoGas() {
			submeter('precoGasForm',  'inserirPrecoGas');
		}

		function cancelar() {
			location.href = '<c:url value="/exibirPesquisaPrecoGas"/>';
		}	

		function limpar(ele) {
			
			limparFormularios(ele);

		  	document.getElementById("indicadorComplementar2").checked = true;
		  	
		  	limparItemFatura();
		  	init();
		
		}	


		function listarPrecoGasComplementar() {
			var selectPrecoGas = document.getElementById("idPrecoGasComplementar");
			selectPrecoGas.length=0;
		  	var novaOpcao = new Option("Selecione","-1");
		  	selectPrecoGas.options[selectPrecoGas.length] = novaOpcao;

		  	var idContratoCompra = document.getElementById('idContratoCompra').value;
		  	var dataInicioFornecimento = document.getElementById('dataFornecimentoInicio').value;		  	
		  	var dataFimFornecimento = document.getElementById('dataFornecimentoFim').value;
		  	var indicadorComplementar = document.getElementById('indicadorComplementar1').checked;

		  	if ( (dataInicioFornecimento != '' && dataInicioFornecimento.indexOf("_") == -1) &&
		  		 (dataFimFornecimento != '' && dataFimFornecimento.indexOf("_") == -1) && 
		  		 (idContratoCompra  != '' && idContratoCompra > -1) && 
		  		 (indicadorComplementar == true)) {

				  AjaxService.consultarPrecoGasComplementar(idContratoCompra, dataInicioFornecimento, dataFimFornecimento, null, {
						callback: function(faixas) {            		      		         		
			            	for (key in faixas){
			                	var novaOpcao = new Option(faixas[key], key);
			                	selectPrecoGas.options[selectPrecoGas.length] = novaOpcao;                    	
			                }
			            }, async:false});
		  	}
		}

		function listarVolumeItemFaturaPrecoGasComplementar() {
			var idPrecoGasComplementar = document.getElementById("idPrecoGasComplementar").value;
			var idItemFatura = document.getElementById("idItemFatura").value;
		  	var inputVolume = document.getElementById('volume');
		  	var inputEstadoVolume = document.getElementById("volumeEstado");

	  		$("#volume").removeClass("campoDesabilitado").removeAttr("readonly");
	  		inputEstadoVolume.value = 'A';
	  		inputVolume.value = '';
		  	
		  	if ((idPrecoGasComplementar != '' && idPrecoGasComplementar != '-1') && ((idItemFatura != '' && idItemFatura != '-1'))) {
				  AjaxService.consultarItemPrecoGasComplementar(idPrecoGasComplementar, idItemFatura,  {
						callback: function(faixas) {            		      		         		
			            	for (key in faixas){
			            		$("#volume").addClass("campoDesabilitado").attr("readonly","readonly");
			            		inputEstadoVolume.value = 'D';
			    		  		inputVolume.value = faixas[key].replace(',', '').replace('.', ',') ;
			    		  		inputVolume.onblur();
			                }
			            }, async:false});
		  	} 
		}	

		function limparItemFatura() {
			var inputItemFatura = document.getElementById("idItemFatura");
		  	var inputVolume = document.getElementById('volume');
		  	var inputValor = document.getElementById('valor');
		  	var inputDataVencimento = document.getElementById('dataVencimento');
		  	var inputEstadoVolume = document.getElementById("volumeEstado");

		  	inputItemFatura.value = -1;

		  	inputVolume.value = '';
		  	$("#volume").removeClass("campoDesabilitado").removeAttr("readonly");
		  	inputEstadoVolume.value = 'A';
		  			  			  	
		  	inputValor.value = '';
		  	inputDataVencimento.value = '';
		}				

		function init () {
			if(${precoGas.indicadorComplementar eq 'true'}) {
				animatedcollapse.show('complementar');
				animatedcollapse.hide('naoComplementar');
			}
			else {
				animatedcollapse.hide('complementar');
				animatedcollapse.show('naoComplementar');
			}

			<c:if test="${precoGas.precoGas.chavePrimaria > -1 && !empty precoGas.precoGas.chavePrimaria}">
				document.forms[0].idPrecoGasComplementar.value = ${precoGas.precoGas.chavePrimaria};
			</c:if>	

			if(${volumeEstado eq 'D'}) {
				$("#volume").addClass("campoDesabilitado").attr("disabled","");
			} else {
		  		$("#volume").removeClass("campoDesabilitado").removeAttr("disabled");
			}	
		}
		
		addLoadEvent(init);
		
		animatedcollapse.addDiv('complementar','fade=0,speed=400,persist=1,hide=0');
		animatedcollapse.addDiv('naoComplementar','fade=0,speed=400,persist=1,hide=1');		
		
	</script>
	<input name="retornoTela" type="hidden" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">
	<input name="volumeEstado" type="hidden" id="volumeEstado" value="${volumeEstado}">
	
	<fieldset id="conteinerPrecoGas" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">	
			<label class="rotulo campoObrigatorio" for="numeroNotaFiscal"><span class="campoObrigatorioSimbolo">* </span>N� da Nota Fiscal:</label>
			<input class="campoTexto" type="text" name="numeroNotaFiscal" id="numeroNotaFiscal" size="10" maxlength="9" value="${precoGas.numeroNotaFiscal}" onkeypress="return formatarCampoInteiro(event)"><br />
			
			<label class="rotulo campoObrigatorio" for="contratoCompra"><span class="campoObrigatorioSimbolo">* </span>Contrato Compra:</label>
			<select name="contratoCompra" id="idContratoCompra" class="campoSelect" onchange="listarPrecoGasComplementar();" >
				<option value="-1">Selecione</option>
				<c:forEach items="${listaContratoCompra}" var="contratoCompra">
					<option value="<c:out value="${contratoCompra.chavePrimaria}"/>" <c:if test="${precoGas.contratoCompra.chavePrimaria == contratoCompra.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${contratoCompra.descricao}"/>
					</option>		
				</c:forEach>
			</select><br />			
			
			<label class="rotulo campoObrigatorio" for="dataFornecimentoInicio"><span class="campoObrigatorioSimbolo">* </span>In�cio do Fornecimento:</label> 
			<input class="campoData" type="text" id="dataFornecimentoInicio"  onblur="listarPrecoGasComplementar();"  name="dataFornecimentoInicio" maxlength="10" value="${dataFornecimentoInicio}"><br />
			
			<label class="rotulo campoObrigatorio" for="dataFornecimentoFim"><span class="campoObrigatorioSimbolo">* </span>Fim do Fornecimento:</label>
			<input class="campoData" type="text" id="dataFornecimentoFim" onblur="listarPrecoGasComplementar();" name="dataFornecimentoFim" maxlength="10" value="${dataFornecimentoFim}"><br />

			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Complementar:</label>
		    <input class="campoRadio" type="radio" name="indicadorComplementar" onchange="listarPrecoGasComplementar();" id="indicadorComplementar1" value="true" <c:if test="${precoGas.indicadorComplementar eq 'true'}">checked</c:if>><label class="rotuloRadio" for="indicadorComplementar1">Sim</label>
		   	<input class="campoRadio" type="radio" name="indicadorComplementar" onchange="listarPrecoGasComplementar();" id="indicadorComplementar2" value="false" <c:if test="${precoGas.indicadorComplementar eq 'false'}">checked</c:if>><label class="rotuloRadio" for="indicadorComplementar2">N�o</label>
		   	<br class="quebraRadio" />


			<div id="naoComplementar" style="display: none;">
			</div>
			
			<div id="complementar" style="display: none;">
				<label class="rotulo rotulo2Linhas campoObrigatorio" for="precoGasComplementar"><span class="campoObrigatorioSimbolo">* </span>N� da Nota Fiscal a Complementar:</label>
				<select name="precoGasComplementar" id="idPrecoGasComplementar" class="campoSelect campo2Linhas" onchange="limparItemFatura();">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaPrecoGas}" var="itemPrecoGas">
						<option value="<c:out value="${itemPrecoGas.chavePrimaria}"/>">
							<c:out value="${itemPrecoGas.numeroNotaFiscal}"/>
						</option>		
					</c:forEach>
				</select>
			</div>
			
			<label class="rotulo rotulo2Linhas campoObrigatorio" for="dataEmissaoNotaFiscal"><span class="campoObrigatorioSimbolo">* </span>Data de Emiss�o da Nota Fiscal:</label> 
			<input class="campoData campo2Linhas" type="text" id="dataEmissaoNotaFiscal" name="dataEmissaoNotaFiscal" maxlength="10" value="${dataEmissaoNotaFiscal}">
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo rotuloVertical" for="observacao">Observa��o:</label>
			<textarea name="observacao" id="observacao" class="campoVertical" rows="5"
			onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
			 onkeypress="return formatarCampoTextoLivreComLimite(event,this,200);">${precoGas.observacao}</textarea>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset class="conteinerBloco">
			<label class="rotulo rotulo2Linhas campoObrigatorio" for="itemFatura"><span class="campoObrigatorioSimbolo">* </span>Item de Fatura:</label>
			<select name="itemFatura" id="idItemFatura" class="campoSelect campoHorizontal" onchange="listarVolumeItemFaturaPrecoGasComplementar();">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaItemFatura}" var="itemFatura">
					<option value="<c:out value="${itemFatura.chavePrimaria}"/>" <c:if test="${precoGasItem.itemFatura.chavePrimaria == itemFatura.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${itemFatura.descricao}"/>
					</option>		
				</c:forEach>
			</select>
			
			<label class="rotulo rotulo2Linhas rotuloHorizontal campoObrigatorio" for="dataVencimento"><span class="campoObrigatorioSimbolo">* </span>Data de Vencimento:</label>
			<input class="campoData campoHorizontal" type="text" id="dataVencimento" name="dataVencimento" maxlength="10" value="${dataVencimento}">

			<label class="rotulo rotuloHorizontal campoObrigatorio" id="labelVolume" for="volume"><span class="campoObrigatorioSimbolo">* </span>Volume:</label>
			<input class="campoTexto campoHorizontal " type="text" id="volume" name="volume" maxlength="15" size="14" onblur="aplicarMascaraNumeroDecimal(this,4);" onkeypress="return formatarCampoDecimalPositivo(event,this,9,4);" value="${precoGasItem.volume}">

			<label class="rotulo rotulo2Linhas rotuloHorizontal campoObrigatorio" id="labelValor" for="valor"><span class="campoObrigatorioSimbolo">* </span>Valor Unit�rio com Tributos:</label>
			<input class="campoTexto campoHorizontal campoValorReal" type="text" id="valor" name="valor" maxlength="10" size="12" onblur="aplicarMascaraNumeroDecimal(this,4);" onkeypress="return formatarCampoDecimal(event,this,5,4);" value="${precoGasItem.valor}">

			<input id="botaoAdicionar" name="buttonAdicionar" value="Adicionar" class="bottonRightCol2 botaoGrande1 botaoAdicionar" onclick="incluirPrecoGasItem()" type="button">
		</fieldset>
		
		<c:set var="i" value="0" />
		<display:table class="dataTableGGAS" name="sessionScope.listaPrecoGasItem" sort="list" id="listaPrecoGasItem" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			<display:column sortable="false" title="Item de Fatura" style="width: 24%">
				<c:out value="${listaPrecoGasItem.itemFatura.descricao}"/> 
			</display:column>
			
			<display:column sortable="false" title="Volume (m<span class='expoente'>3</span>)" style="width: 21%">
				<fmt:formatNumber value="${listaPrecoGasItem.volume}" minFractionDigits="4" maxFractionDigits="4" type="number"/>
			</display:column>			
			
			<display:column sortable="false" title="Valor (R$)" style="width: 21%">
				<fmt:formatNumber value="${listaPrecoGasItem.valor}" minFractionDigits="4" maxFractionDigits="4" type="number"/>
			</display:column>
			
			<display:column sortable="false" title="Pre�o Total (R$)" style="width: 21%">
				<fmt:formatNumber value="${listaPrecoGasItem.valor * listaPrecoGasItem.volume}" minFractionDigits="4" maxFractionDigits="4" type="number"/>
			</display:column>			
			
			<display:column  sortable="false" title="Data de Vencimento" style="width: 17%">
				<fmt:formatDate value="${listaPrecoGasItem.dataVencimento}" pattern="dd/MM/yyyy" />
			</display:column>		
			
			<display:column style="width: 4%"> 
				<a onclick="return confirm('Deseja excluir o Item?');" href="javascript:removerPrecoGasItem(<c:out value="${i}"/>);"><img title="Exluir Tronco" alt="Exluir Tronco" src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a> 
			</display:column>
			
			<c:set var="i" value="${i+1}" />	
		</display:table>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
	    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limpar(this.form);">
	    <vacess:vacess param="exibirInsercaoPrecoGas">
	    	<input id="botaoSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar"  type="button" onclick="incluirPrecoGas();">
	    </vacess:vacess>
	</fieldset>	
	<token:token></token:token>
</form:form>

	