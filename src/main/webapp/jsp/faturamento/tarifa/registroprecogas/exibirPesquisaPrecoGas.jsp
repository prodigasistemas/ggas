<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Registro de Pre�o do G�s<a class="linkHelp" href="<help:help>/consultadoregistrodepreodogs.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar Pre�o do G�s clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>. Para inserir Pre�o do G�s clique em <span class="destaqueOrientacaoInicial">Incluir.</span></p>

<form:form method="post" action="pesquisarPrecoGas" name="precoGasForm">

	<script language="javascript">

		$(document).ready(function(){
			$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
			
		});
			

		function limparFormulario() {
			document.precoGasForm.numeroNotaFiscal.value = "";
			document.precoGasForm.dataInicioEmissao.value = "";
			document.precoGasForm.dataFimEmissao.value = "";
			document.precoGasForm.idContratoCompra.value = "-1";
						
		}

		function incluirPrecoGas() {
			location.href = '<c:url value="/exibirInsercaoPrecoGas"/>';
		}

			
		
		function detalharPrecoGas(chave){
			document.precoGasForm.chavePrimaria.value = chave;
			submeter("precoGasForm", "exibirDetalhamentoPrecoGas");
		}

		function alterarPrecoGas(chave){

			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("precoGasForm", "exibirAlteracaoPrecoGas");
		    }
			
		}

		function excluirPrecoGas(){  
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('precoGasForm', 'excluirPrecoGas');
				}
			}
		}	

		function alterarPrecoGas(chave){

			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("precoGasForm", "exibirAlteracaoPrecoGas");
		    }
			
		}
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarPrecoGas">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
		
	<fieldset id="pesquisarPrecoGas" class="conteinerPesquisarIncluirAlterar">
		<label class="rotulo rotulo2Linhas" for="contratoCompra">Contrato Compra:</label>
		<select name="contratoCompra" id="idContratoCompra" class="campoSelect campo2Linhas campoHorizontal">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaContratoCompra}" var="contratoCompra">
				<option value="<c:out value="${contratoCompra.chavePrimaria}"/>" <c:if test="${precoGas.contratoCompra.chavePrimaria == contratoCompra.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${contratoCompra.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<label class="rotulo rotulo2Linhas rotuloHorizontal" for="numeroNotaFiscal" >N� da Nota Fiscal:</label>
		<input class="campoTexto campo2Linhas campoHorizontal" id="numeroNotaFiscal" type="text" name="numeroNotaFiscal"  size="10" maxlength="9"  value="${precoGas.numeroNotaFiscal}" onkeypress="return formatarCampoInteiro(event)">				
		<label class="rotulo rotulo2Linhas rotuloHorizontal" for="dataInicioEmissao">Per�odo de Emiss�o:</label>
	   	<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataInicioEmissao" name="dataInicioEmissao" maxlength="10" value="${dataInicioEmissao}" >
		<label class="rotuloEntreCampos campo2Linhas" for="dataFimEmissao">a</label>
	   	<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataFimEmissao" name="dataFimEmissao" maxlength="10" value="${dataFimEmissao}" >
				
	    <fieldset class="conteinerBotoesPesquisarDir">
	    	<vacess:vacess param="exibirPesquisaPrecoGas">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>	
	</fieldset>
	
	<c:if test="${listaPrecoGas ne null}">	
		<hr class="linhaSeparadoraPesquisa" />
			
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPrecoGas" sort="list" id="precoGasPesquisar" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPrecoGas">
			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
				<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${precoGasPesquisar.chavePrimaria}">
			</display:column>
			<display:column sortable="true" sortProperty="numeroNotaFiscal" title="N� da Nota Fiscal" headerClass="tituloTabelaEsq" style="text-align: left">
				<a href='javascript:detalharPrecoGas(<c:out value='${precoGasPesquisar.chavePrimaria}'/>);'>
					<c:out value="${precoGasPesquisar.numeroNotaFiscal}"/>
				</a>
			</display:column>
			<display:column sortable="true" sortProperty="dataEmissaoNotaFiscal" title="Data de<br />Emiss�o" style="width: 100px">
				<a href="javascript:detalharPrecoGas(<c:out value='${precoGasPesquisar.chavePrimaria}'/>);">
					<fmt:formatDate value="${precoGasPesquisar.dataEmissaoNotaFiscal}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			<display:column sortable="true" sortProperty="dataFornecimentoInicio" title="In�cio do<br />Fornecimento" style="width: 100px">
				<a href="javascript:detalharPrecoGas(<c:out value='${precoGasPesquisar.chavePrimaria}'/>);">
					<fmt:formatDate value="${precoGasPesquisar.dataFornecimentoInicio}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			<display:column sortable="true" sortProperty="dataFornecimentoFim" title="Fim do<br />Fornecimento" style="width: 100px">
				<a href="javascript:detalharPrecoGas(<c:out value='${precoGasPesquisar.chavePrimaria}'/>);">
					<fmt:formatDate value="${precoGasPesquisar.dataFornecimentoFim}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
		</display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaPrecoGas}">
			<vacess:vacess param="excluirPrecoGas">
				<input id="botaoRemover" name="buttonExcluir" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="excluirPrecoGas()" type="button">
			</vacess:vacess>
			<vacess:vacess param="exibirAlteracaoPrecoGas">
				<input id="botaoAlterar" name="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarPrecoGas(<c:out value='${precoGasPesquisar.chavePrimaria}'/>);" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInsercaoPrecoGas">
			<input id="botaoIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluirPrecoGas();" type="button">
		</vacess:vacess>
	</fieldset>
</form:form>

	