<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<h1 class="tituloInterno">Detalhar Registro de Pre�o do G�s<a class="linkHelp" href="<help:help>/detalharumregistrodepreodogs.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form:form method="post" action="exibirDetalhamentoPrecoGas" name="precoGasForm">

	<script language="javascript">
	
		function alterarPrecoGas(chave) {
			document.precoGasForm.chavePrimaria.value = chave;
			submeter("precoGasForm", "exibirAlteracaoPrecoGas");
		}

		function voltar() {
			submeter("precoGasForm", "voltaPrecoGas");
		}	
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="exibirDetalhamentoPrecoGas">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="indexLista" type="hidden" id="indexLista" value="${clienteForm.map.indexLista}">
	
	<fieldset id="detalharPrecoGas" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna detalhamentoColunaLarga ">
			<label class="rotulo" >N� da Nota Fiscal:</label> 
			<span class="itemDetalhamento"><c:out value="${precoGas.numeroNotaFiscal}"/></span><br />
			
			<label class="rotulo" >Contrato Compra:</label> 
			<span class="itemDetalhamento"><c:out value="${precoGas.contratoCompra.descricao}"/></span><br />
			
			<label class="rotulo" >In�cio do Fornecimento:</label>
			<span class="itemDetalhamento">
			<fmt:formatDate value="${precoGas.dataFornecimentoFim}" pattern="dd/MM/yyyy"/></span><br />
			
			<label class="rotulo" >Fim do Fornecimento:</label>
			<span class="itemDetalhamento">
			<fmt:formatDate value="${precoGas.dataFornecimentoFim}" pattern="dd/MM/yyyy"/></span><br />
					
			<label class="rotulo" >Complementar:</label> 
			<span class="itemDetalhamento">
			<c:if test="${precoGas.indicadorComplementar eq 'true'}"><c:out value="Sim"/></c:if>
			<c:if test="${precoGas.indicadorComplementar eq 'false'}"><c:out value="N�o"/></c:if>
			</span><br />
			
			<c:if test="${precoGas.indicadorComplementar eq 'true'}">
				<label class="rotulo rotulo2Linhas" for="valorReferencia">N� da Nota Fiscal a Complementar:</label>
				<span class="itemDetalhamento itemDetalhamento2Linhas">
					<c:out value="${precoGas.numeroNotaFiscalComplementar}"/></span><br />				
			</c:if>			
			
			<label class="rotulo rotulo2Linhas" >Data de Emiss�o da <br />Nota Fiscal:</label> 
			<span class="itemDetalhamento itemDetalhamento2Linhas">
			<fmt:formatDate value="${precoGas.dataEmissaoNotaFiscal}" pattern="dd/MM/yyyy"/></span>
		</fieldset>
		
		<fieldset class="colunaFinal">
			<label class="rotulo" >Observa��o:</label> 
			<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${precoGas.observacao}"/></span>
		</fieldset>
		
		<c:set var="i" value="0" />
		<hr class="linhaSeparadora1" />
		<display:table class="dataTableGGAS" name="sessionScope.listaPrecoGasItem" sort="list" id="listaPrecoGasItem" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			<display:column sortable="false" title="Item de Fatura" style="width: 24%">
				<c:out value="${listaPrecoGasItem.itemFatura.descricao}"/> 
			</display:column>
			
			<display:column sortable="false" title="Volume (m<span class='expoente'>3</span>)" style="width: 21%">
				<fmt:formatNumber value="${listaPrecoGasItem.volume}" minFractionDigits="4"  maxFractionDigits="4" type="number"/>
			</display:column>			
			
			<display:column sortable="false" title="Valor (R$)" style="width: 21%">
				<fmt:formatNumber value="${listaPrecoGasItem.valor}" maxFractionDigits="4" minFractionDigits="4"  type="number"/>
			</display:column>
			
			<display:column sortable="false" title="Pre�o Total (R$)" style="width: 21%">
				<fmt:formatNumber value="${listaPrecoGasItem.valor * listaPrecoGasItem.volume}" minFractionDigits="4" maxFractionDigits="4" type="number"/>
			</display:column>			
			
			<display:column  sortable="false" title="Data de Vencimento" style="width: 17%">
				<fmt:formatDate value="${listaPrecoGasItem.dataVencimento}" pattern="dd/MM/yyyy" />
			</display:column>
			
			<c:set var="i" value="${i+1}" />
		</display:table>
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="button" value="Alterar" class="bottonRightCol2 botaoGrande1" onclick="alterarPrecoGas(<c:out value='${precoGas.chavePrimaria}'/>);" type="button">
		<input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
	</fieldset>
</form:form>

	