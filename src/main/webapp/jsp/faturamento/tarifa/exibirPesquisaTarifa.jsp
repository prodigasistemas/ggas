<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token"%>


<script>

	$(document).ready(function(){
		   iniciarDatatable('#tarifaVigenciaPesquisar', {"order" : [[2, 'asc']]});
		
		   $(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		
		  		    
			var possuiAlcadaAutorizacao = "<c:out value="${possuiAlcadaAutorizacao}" />"						
			if(possuiAlcadaAutorizacao == "true"){				
				$("#buttonAutorizarTarifaVigencia").prop("disabled", false);
			}
			
			$("#exportarRelatorio").dialog(
					{autoOpen: false, width: 245, modal: true, 
						minHeight: 240, maxHeight: 200, resizable: false});
			
			corrigirPosicaoDatepicker();
			
		});		
		
		function escolherVigencia(){	
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				var chavePrimariaTarifa = obterValorUnicoCheckboxSelecionado();				
				$("#chavePrimaria").val(chavePrimariaTarifa);
				exibirModalTarifaVigencia(chavePrimariaTarifa);
		    }			
		}
		
		function exibirModalTarifaVigencia(chaveTarifa){
			var tarifasVigencia = document.getElementById("tarifasVigencia");
			tarifasVigencia.length = 0;			
			var novaOpcao = new Option("Selecione", "");
			tarifasVigencia.options[tarifasVigencia.length] = novaOpcao;
			AjaxService.listarTarifaVigenciaPorTarifaNaoAutorizada(chaveTarifa, 
			     	function(dados) {
						for (key in dados) {
							var item = dados[key];
							var novaOpcao = new Option(item[1], item[0]);							
							tarifasVigencia.options[tarifasVigencia.length] = novaOpcao;
						}
			        }
			     );
			$( "#modalAutorizacaoVigencia" ).modal('show');
		}
		
		function autorizarTarifaVigencia(statusAutorizacaoVigencia){
			var tarifasVigencia = document.getElementById('tarifasVigencia').value;
			$("#chavePrimariaVigencia").val(tarifasVigencia);			
			$("#statusAutorizacaoVigencia").val(statusAutorizacaoVigencia);
			
			submeter('tarifaForm', 'autorizarTarifaVigencia');
		}
		
		
		function limparFormulario() {
			document.tarifaForm.descricao.value = "";
			document.tarifaForm.descricaoAbreviada.value = "";
			document.tarifaForm.dataVigencia.value = "";
			document.tarifaForm.habilitado[0].checked = true;
			document.tarifaForm.idSegmento.value = "-1";
			document.tarifaForm.idItemFatura.value = "-1";
			document.tarifaForm.tipoContrato.value = "-1";
			document.tarifaForm.status[3].checked = true;
		}
		
		function incluirTarifa() {
			submeter("0", "exibirInsercaoTarifa");
		}
		

		function detalharTarifa(chave){
			$("#chavePrimaria").val(chave);
			submeter("tarifaForm", "exibirDetalhamentoTarifa");
		}

		function alterarTarifa(chave){

			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("0", "exibirAlteracaoTarifa");
		    }
			
		}
		
		function excluirTarifa(){  
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('0', 'excluirTarifa');
				}
			}
		}
		
		function exibirExportarRelatorio() {
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				var idTarifaSelecionada = obterValorUnicoCheckboxSelecionado();
				document.forms[0].chavePrimaria.value = idTarifaSelecionada;
				carregarVigencias(idTarifaSelecionada);
				exibirJDialog("#exportarRelatorio");
		    }
		}
		
		function gerar() {
			submeter('0', 'exportarRelatorioTarifa');
		}
		
		function exibirPesquisaTarifaSubmit() {
			submeter('0', 'pesquisarTarifa');
		}

</script>


<div class="bootstrap">
	<form:form method="post" action="exibirPesquisaTarifa" id="tarifaForm" name="tarifaForm">
		<div class="card">
			<div class="card-header">
            	<h5 class="card-title mb-0">Pesquisar Tarifa</h5>
        	</div>
        	<div class="card-body">
        		<input name="acao" type="hidden" id="acao" value="pesquisarTarifa">
				<input name="chavePrimaria" type="hidden" id="chavePrimaria">
				<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true"> 
				<input name="chavePrimariaVigencia" type="hidden" id="chavePrimariaVigencia" /> 
				<input name="statusAutorizacaoVigencia" type="hidden" id="statusAutorizacaoVigencia" />
				<token:token></token:token>
        		
        		<jsp:include page="/jsp/relatorio/opcoesGeraisRelatorio.jsp">
					<jsp:param name="exibeOpcoesTarifa" value="true" />
					<jsp:param name="ocultarCheckFiltros" value="true" />
				</jsp:include>
				
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i>
         			Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em
        			<strong>Pesquisar</strong>, ou clique apenas em <strong>Pesquisar</strong> para exibir todos. Para incluir um novo
        			 registro clique em <strong>Incluir</strong>.
				</div>
				
				<div class="row">
					<div class="col-md-6">
					
						<div class="form-row">
							<div class="col-md-10">
								<label for="descricao">Descri��o:</label>
								 <input class="form-control form-control-sm" type="text"  id="descricaoModelo" name="descricao"
                                    maxlength="30" size="35" value="${tarifa.descricao}"
									onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="col-md-10">
								<label for="descricaoAbreviada">Descri��o Abreviada:</label>
								 <input class="form-control form-control-sm" type="text"  id="descricaoAbreviada" name="descricaoAbreviada"
                                    maxlength="7" size="10" value="${tarifa.descricaoAbreviada}"
									onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="col-md-6">
        						<label for="dataVigencia">Vig�ncia:</label>
        							<div class="input-group input-group-sm">
                   						<input type="text" class="form-control form-control-sm campoData"
                     					id="dataVigencia" name="dataVigencia" value="${dataVigencia}">
               						</div>
        					</div>
						</div>
						
						<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idFatura">Item da Fatura: </label>
             				<select name="itemFatura" id="idItemFatura" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaItemFatura}" var="itemFatura">
									<option value="<c:out value="${itemFatura.chavePrimaria}"/>"
									<c:if test="${tarifa.itemFatura.chavePrimaria == itemFatura.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${itemFatura.descricao}" />
									</option>
								</c:forEach>
           					</select>
           					</div>
        		 		</div>
						
					</div>
					<div class="col-md-6">
					
						<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idSegmento">Segmento: </label>
             				<select name="segmento" id="idSegmento" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaSegmento}" var="segmento">
									<option value="<c:out value="${segmento.chavePrimaria}"/>"
										<c:if test="${tarifa.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${segmento.descricao}" />
									</option>
								</c:forEach>
           					</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
             				<div class="col-md-10">
    		 				<label  for="tipoContrato">Tipo de Contrato: </label>
             				<select name="tipoContrato" id="tipoContrato" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
								<c:forEach items="${listaTipoContrato}" var="tipoContrato">
									<option value="<c:out value="${tipoContrato.chavePrimaria}"/>"
										<c:if test="${tarifa.tipoContrato.chavePrimaria eq tipoContrato.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${tipoContrato.descricao}" />
									</option>
								</c:forEach>
           					</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row mt-1">
						<label for="habilitado">Indicador de Uso:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoAtivo" value="true" name="habilitado" class="custom-control-input" 
									   <c:if test="${habilitado eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="habilitadoAtivo">Ativo</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitadoInativo" value="false" name="habilitado" class="custom-control-input" 
									   <c:if test="${habilitado eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="habilitadoInativo">Inativo</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="habilitado" value="" name="habilitado" class="custom-control-input" 
									   <c:if test="${empty habilitado}">checked</c:if>>
									<label class="custom-control-label" for="habilitado">Todos</label>
								</div>
							</div>
						</div>
        		 		
        		 		<div class="form-row mt-1">
						<label for="status">Al�ada:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="status" id="statusAutorizado" class="custom-control-input" value="${statusAutorizado}"
									   <c:if test="${status eq statusAutorizado}">checked</c:if>>
									<label class="custom-control-label" for="statusAutorizado">Autorizado</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="status" id="statusPendente" class="custom-control-input" value="${statusPendente}"
									   <c:if test="${status eq statusPendente}">checked</c:if>>
									<label class="custom-control-label" for="statusPendente">Pendente</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="status" id="statusNaoAutorizado" class="custom-control-input" value="${statusNaoAutorizado}" 
									   <c:if test="${status eq statusNaoAutorizado}">checked</c:if>>
									<label class="custom-control-label" for="statusNaoAutorizado">N�o Autorizado</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="status" id="status" class="custom-control-input" value="0" 
									   <c:if test="${status eq null or status eq 0}">checked</c:if>>
									<label class="custom-control-label" for="status">Todos</label>
								</div>
							</div>
						</div>
        		 		
					</div>
				</div> <!-- linha da pesquisa -->
				
				<div class="row mt-3">
                    <div class="col align-self-end text-right">
                    	<vacess:vacess param="exibirPesquisaTarifa">
                        	<button class="btn btn-primary btn-sm" id="botaoPesquisar" type="button"
                        		onclick="exibirPesquisaTarifaSubmit();">
                            	<i class="fa fa-search"></i> Pesquisar
                        	</button>
                        </vacess:vacess>
                        <button class="btn btn-secondary btn-sm" id="botaoLimpar" type="button"
                                onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                    </div>
                </div><!-- linha da bot�es -->
                
                <c:if test="${listaTarifa ne null}">
                <hr/>
                
                <div class="table-responsive">
                	 <table class="table table-bordered table-striped table-hover" id="tarifaVigenciaPesquisar" style="width:100%">
                	 	 <thead class="thead-ggas-bootstrap">
                	 	 	<tr>
                 	 			<th class="text-center" data-orderable="false">
                     				<input type='checkbox' name='checkAllAuto' id='checkAllAuto' disabled/>
                    			 </th>
                    			<th scope="col" class="text-center">Vigente</th>
                    			<th scope="col" class="text-center">Descri��o</th>
                    			<th scope="col" class="text-center">Descri��o Abreviada</th>
                    			<th scope="col" class="text-center">Segmento</th>
                    			<th scope="col" class="text-center">In�cio da Vig�ncia</th>
                    			<th scope="col" class="text-center">Fim da Vig�ncia</th>
                  			</tr>
                	 	 </thead>
                	 	 <tbody>
                	 	 	<c:forEach items="${listaTarifa}" var="tarifaVigenciaPesquisar">
                	 	 		<tr>
                	 	 			<td class="text-center">
               				 			<input type="checkbox" name="chavesPrimarias" value="${tarifaVigenciaPesquisar.chavePrimaria}">
               						</td>
               						<td class="text-center">
               							<c:choose>
										<c:when test="${tarifaVigenciaPesquisar.vigente == true}">
											<i class="fas fa-check-circle text-success" title="Ativo"></i>
										</c:when>
										<c:otherwise>
											<i class="fas fa-ban text-danger" title="Inativo"></i>
										</c:otherwise>
										</c:choose>
               						</td>  
               						<td class="text-center">
               							<a href='javascript:detalharTarifa(<c:out value='${tarifaVigenciaPesquisar.chavePrimaria}'/>);'>
               							<c:out value="${tarifaVigenciaPesquisar.descricaoTarifa}" /> </a>
               						</td>
               						<td class="text-center">
               							<a href='javascript:detalharTarifa(<c:out value='${tarifaVigenciaPesquisar.chavePrimaria}'/>);'>
               							<c:out value="${tarifaVigenciaPesquisar.descricaoAbreviadaTarifa}" /> </a>
               						</td>
               						<td class="text-center">
               							<a href='javascript:detalharTarifa(<c:out value='${tarifaVigenciaPesquisar.chavePrimaria}'/>);'>
               							<c:out value="${tarifaVigenciaPesquisar.descricaoSegmento}" /> </a>
               						</td> 
               						<td class="text-center">
               							<a href='javascript:detalharTarifa(<c:out value='${tarifaVigenciaPesquisar.chavePrimaria}'/>);'>
               							<c:out value="${tarifaVigenciaPesquisar.dataInicioVigencia}" /> </a>
               						</td> 
               						<td class="text-center">
               							<a href='javascript:detalharTarifa(<c:out value='${tarifaVigenciaPesquisar.chavePrimaria}'/>);'>
               							<c:out value="${tarifaVigenciaPesquisar.dataFimVigencia}" /> </a>
               						</td>      
                	 	 		</tr>
                	 	 	</c:forEach>
                	 	 </tbody>
                	 </table>
                </div>
                </c:if>
        		
        	</div><!-- fim do card-body -->
        	<div class="card-footer">
        		<div class="row">
        			<div class="col-sm-12">
        				<c:if test="${not empty listaTarifa}">
        					<vacess:vacess param="exibirAlteracaoTarifa">
        						<button id="buttonAlterar" type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-left" 
        							onclick="alterarTarifa(<c:out value='${tarifaVigencia.chavePrimaria}'/>);">
        							<i class="fas fa-pencil-alt"></i> Alterar
    							</button>
        					</vacess:vacess>
        				
        					<vacess:vacess param="excluirTarifa">
        						<button id="botaoRemover" type="button" class="btn btn-danger btn-sm ml-1 mt-1 float-left" onclick="excluirTarifa();">
        						<i class="far fa-trash-alt"></i> Remover </button>
        					</vacess:vacess>
        				
        					<vacess:vacess param="autorizarTarifaVigencia">
        						<button id="buttonAutorizarTarifaVigencia"  type="button" class="btn btn-secondary btn-sm ml-1 mt-1 float-left"
            						onclick="escolherVigencia();">
        							<i class="fas fa-users"></i> Autorizar Vig�ncia da Tarifa
    							</button>
        					</vacess:vacess>
        				
        					<vacess:vacess param="exibirRelatorioTarifa">
        						<button id="buttonAutorizarTarifaVigencia"  type="button" class="btn btn-info btn-sm ml-1 mt-1 float-left"
            						onclick="exibirExportarRelatorio();">
        							<i class="fas fa-file"></i> Relat�rio
    							</button>
        					</vacess:vacess>
        				</c:if>
        				<vacess:vacess param="exibirInsercaoTarifa">
							<button id="buttonIncluir" type="button" class="btn btn-sm btn-primary float-right ml-1 mt-1" onclick="incluirTarifa();">
                       			<i class="fa fa-plus"></i> Incluir
                   			</button>
						</vacess:vacess>
        			</div>
        		</div>
			</div>
		</div><!-- fim do card -->
	</form:form>
	
	
	
	<!-- Modal -->
<div class="modal fade" id="modalAutorizacaoVigencia" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="autorizarVigencia">Autorizar Vig�ncia da Tarifa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="form-row">
             <div class="col-md-10">
    		 	<label>Vig�ncias:</label>
             	<select id="tarifasVigencia" name="tarifasVigencia"
						onChange="verificarPermissaoAutorizacao(this);" class="form-control form-control-sm"> 
             		<option value="-1">Selecione</option>
             		<c:forEach items="${listaTarifasVigencia}" var="tarifaVigencia">
						<option value="<c:out value="${tarifaVigencia.chavePrimaria}"/>"
							title="<fmt:formatDate value="${tarifaVigencia.dataVigencia}" type="both" pattern="dd/MM/yyyy" />"><fmt:formatDate
							value="${tarifaVigencia.dataVigencia}" type="both"
							pattern="dd/MM/yyyy" /></option>
					</c:forEach>
           		</select>
           	</div>
        </div>
        
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" id="buttonAutorizarVigencia" value="Autorizar"
      		onclick="autorizarTarifaVigencia(${statusAutorizado});">Autorizar</button>
      		
        <button type="button" class="btn btn-primary" id="buttonNaoAutorizarVigencia" value="N�o Autorizar"
        	onclick="autorizarTarifaVigencia(${statusNaoAutorizado});">N�o Autorizar</button>
        	
      </div>
    </div>
  </div>
</div>

</div>





