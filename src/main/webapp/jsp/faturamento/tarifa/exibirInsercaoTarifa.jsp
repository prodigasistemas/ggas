<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>





<script>

	$(document).ready(function(){
		
		 $("#dadosFaixa, #descontoFaixa").DataTable( {
	    	 "responsive": true,
	    	 "paging":   false,
	         "ordering": false,
	         "info": false,
	         "searching": false,
	    	 "language": {
	             "zeroRecords": "Nenhum registro foi encontrado"
	    	 }
	    } );
		
		AjaxService.isTipoSubstitutoPetrobras(function(isPetrobras) {
			if (isPetrobras == true) {
				animatedcollapse.show('conteinerICMSSubstituido');	
				$('#linhaICMSSubstituido').show();
			} else {
				animatedcollapse.hide('conteinerICMSSubstituido');
				$('#linhaICMSSubstituido').hide();
			}
		});
		
		$("#informarMsgIcmsSubstituidoSim").click(habilitaIcmsSubstituto);
		$("#informarMsgIcmsSubstituidoNao").click(desabilitaIcmsSubstituto);
		
	    $("#dataVigencia,.dataTarifaVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});   
	    $("#dataDescontoVigenciaDe,.dataTarifaVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	    $("#dataDescontoVigenciaPara,.dataTarifaVigencia").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		

		//Remove a Faixa de Tarifa
		$("#tarifaFaixas .tabelaFaixas tr td .botaoRemoverFaixas").click(function() {
			var linha = $(this).parents("#tarifaFaixas .tabelaFaixas tr");
				$(linha).remove();
				faixasTarifaClasses();
		});
		
		var qtdCasasDecimal = '${tarifa.quantidadeCasaDecimal}';
		if (qtdCasasDecimal == undefined || qtdCasasDecimal == null || qtdCasasDecimal <= 0) {
			qtdCasasDecimal = 2;
		}
		
		$("input[name=faixaFinal]").each(function(){
			aplicarMascaraNumeroInteiro(this);
		});
		
		$("input[name=faixaInicial]").each(function(){
			aplicarMascaraNumeroInteiro(this);
		});
		
		verificarDadosTarifaFaixas();
		verificarDadosTarifaFaixasDesconto();
		
		corrigirPosicaoDatepicker();
		
	});

	animatedcollapse.addDiv('conteinerICMSSubstituido', 'fade=0,speed=400,hide=1');
	
	function desabilitaIcmsSubstituto(){
		$("#baseCalculoIcmsSubstituido,#icmsSubstituidoMetroCubico,#observacaoVigenciaIcmsSubstituto,#rotuloObservacaoVigencia,#rotuloIcmsSubstituidoMetroCubico,#rotuloBaseCalculoIcms").addClass("rotuloDesabilitado");
		document.getElementById("baseCalculoIcmsSubstituido").disabled = true;
		document.getElementById("icmsSubstituidoMetroCubico").disabled = true;
		document.getElementById("observacaoVigenciaIcmsSubstituto").disabled = true;
		
	}
	
	function habilitaIcmsSubstituto(){
		$("#baseCalculoIcmsSubstituido,#icmsSubstituidoMetroCubico,#observacaoVigenciaIcmsSubstituto,#rotuloObservacaoVigencia,#rotuloIcmsSubstituidoMetroCubico,#rotuloBaseCalculoIcms").removeClass("rotuloDesabilitado")
		document.getElementById("baseCalculoIcmsSubstituido").disabled = false;
		document.getElementById("icmsSubstituidoMetroCubico").disabled = false;
		document.getElementById("observacaoVigenciaIcmsSubstituto").disabled = false;
	}
	
	function adicionarFaixaTarifa(linha, chave){
		var tributosAssociados = document.getElementById('tributosAssociados');	
		if (tributosAssociados != undefined) {
			for (i=0; i<tributosAssociados.length; i++){
				tributosAssociados.options[i].selected = true;
			}
		}
		document.getElementById('faixaFimAdicao').value = chave;
		definirCamposEscondidos();
		submeter('tarifaForm','adicionarFaixaTarifa#blocoVigenciaTarifa');
	}
	
	function removerFaixaTarifa(chave){
		var tributosAssociados = document.getElementById('tributosAssociados');	
		if (tributosAssociados != undefined) {
			for (i=0; i<tributosAssociados.length; i++){
				tributosAssociados.options[i].selected = true;
			}
		}
		document.getElementById('faixaInicialExclusao').value = chave; 
		submeter('tarifaForm','removerFaixaTarifa#blocoVigenciaTarifa')
	}
	
	function aplicarFaixasTarifa(){
		var tributosAssociados = document.getElementById('tributosAssociados');	
		if (tributosAssociados != undefined) {
			for (i=0; i<tributosAssociados.length; i++){
				tributosAssociados.options[i].selected = true;
			}
		}
		definirCamposEscondidos();
		submeter('tarifaForm','aplicarFaixasTarifa');
	}

	//Adiciona as classes even e odd para alternar as cores das linhas da tabela de Faixas de Tarifa
	function faixasTarifaClasses(){
	   	$("#tarifaFaixas .tabelaFaixas tr:nth-child(odd)").addClass("odd").removeClass("even");
	   	$("#tarifaFaixas .tabelaFaixas tr:nth-child(even)").addClass("even").removeClass("odd");
	}

	function limparFormulario() {
		
		location.href = '<c:url value="/exibirInsercaoTarifa"/>'; 
		
	}

	function associarTributos(){
		desabilitarBotoesPontoConsumo();
		atualizarContagemPontosAssociados();
		var lista = document.getElementById('idPontosConsumoAssociados');		
		for (i=0; i<lista.length; i++){
			lista.options[i].selected = true;
		}
		var valorMaximo = document.getElementById('numeroMaximoPontosConsumo').value;		
		if(valorMaximo != '' && lista.length > valorMaximo){
			alert('<fmt:message key="ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO"/>' );
		}
	}
	
	function voltar() {
		location.href = '<c:url value="/exibirPesquisaTarifa"/>';
	}
		
	function salvar() {
		var tributosAssociados = document.getElementById('tributosAssociados');	
		if (tributosAssociados != undefined) {
			for (i=0; i<tributosAssociados.length; i++){
				tributosAssociados.options[i].selected = true;
			}
		}
		definirCamposEscondidos();
		var chavePrimariaTarifa = $('form[name="tarifaForm"] :input[name="chavePrimaria"]').val();
		var chavePrimariaVigencia = $('form[name="tarifaForm"] :input[name="chavePrimariaVigencia"]').val();
		var dataSelecionada = $('#dataVigencia').val();
		
		if(chavePrimariaTarifa && chavePrimariaVigencia) {
			AjaxService.montarListaTarifaVigenciaDesconto(chavePrimariaTarifa, chavePrimariaVigencia, dataSelecionada,
	          	   {callback: function(colecaoDescontosAfetados) {
		          		   if(colecaoDescontosAfetados.length > 0) {
			          			 var confirmacao = confirm("A submiss�o de uma vig�ncia com esta data de in�cio vai sobrepor o per�odo de outra vig�ncia.\n"
				          				   + "Caso prossiga, o GGAS vai inativar os seguintes descontos cadastrados previamente:\n"
				          				   + colecaoDescontosAfetados.join("\n"));   
			          			 if(confirmacao == true) {
			          				submeter('tarifaForm', 'inserirTarifa');
			          			 }
				           } else {
				        	   submeter('tarifaForm', 'inserirTarifa');
				           }
		           }}
		    );		
		} else {
			submeter('tarifaForm', 'inserirTarifa');
		}
	}

	function clonarFaixa(){
		var tributosAssociados = document.getElementById('tributosAssociados');	
		if (tributosAssociados != undefined) {
			for (i=0; i<tributosAssociados.length; i++){
				tributosAssociados.options[i].selected = true;
			}
		}
		submeter('tarifaForm','clonarFaixaTarifa')
	}

	 function selecionarTributosAssociados(){

		 var tributosAssociados = document.getElementById('tributosAssociados');	
		 	//alert(tributosAssociados.length);	
			if (tributosAssociados != undefined) {
				for (i=0; i<tributosAssociados.length; i++){
					tributosAssociados.options[i].selected = true;
				}
			}		 		
	 }

	 function carregarVigenciasFaixas(elem){
		var codTarifa = elem.value;
		carregarVigencias(codTarifa)
	}

	function carregarVigencias(codTarifa){
		
      	var selectVigencia = document.getElementById("idVigenciasTarifa");
      	selectVigencia.length=0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectVigencia.options[selectVigencia.length] = novaOpcao;
      	if (codTarifa != "-1") {
      		selectVigencia.disabled = false;      		
        	AjaxService.obterVigencia( codTarifa, {
        			callback: function(tarifaVigencia) {            		      		         		
                	for (key in tarifaVigencia){
                		var item = tarifaVigencia[key];
                    	var novaOpcao = new Option(item[1], item[0]);
                    	selectVigencia.options[selectVigencia.length] = novaOpcao;
                    }
                }, async:false}            
            );

        	var idVigenciasTarifa = "${idVigenciasTarifa}";
        	if(idVigenciasTarifa != undefined && idVigenciasTarifa != ''){
        		selectVigencia.value = idVigenciasTarifa;
        	}
      	} else {
      		selectVigencia.disabled = true;      	
      	}
	}
		 
	function setarValorIdentificador(valor){
		document.getElementById('identificadorSelecionado').value = valor;
		document.getElementById('ultimaFaixa').value = valor;
	}

	function getQtdCasasDecimais(){
		var qtDecimais = document.getElementById('qtdCasasDecimais').value ;

		if ( qtDecimais != "" | qtDecimais != 0 ){
			return qtDecimais;
		}else{
			return 4;
		}
	}

	function getQtdCasasDecimaisDesconto() {
		var idTipoPercentual = "${idTipoPercentual}";
		var idTipoDesconto = document.getElementById("idTipoDesconto").value;
		if (idTipoPercentual != undefined && idTipoDesconto == idTipoPercentual) {
			return 4;
		} else {
			return getQtdCasasDecimais();
		}
	}
	
	function carregarRamosAtividade(elem) {	
		
		var idSegmento = elem.value;
	  	var selectRamoAtividade = document.getElementById("idRamoAtividade");

	  	selectRamoAtividade.length=0;
	  	var novaOpcao = new Option("Selecione","-1");
	    selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
	  	  
	  	if(idSegmento != '-1'){
	       	AjaxService.listarRamoAtividadePorSegmento( idSegmento, {
	           	callback:function(ramosAtividade) {            		      		         		
	           		for (ramo in ramosAtividade){
	               		for (key in ramosAtividade[ramo]){
		                	var novaOpcao = new Option(ramosAtividade[ramo][key], key);
		            		selectRamoAtividade.options[selectRamoAtividade.length] = novaOpcao;
	               		}
	            	}
	            	selectRamoAtividade.disabled = false;
	        	}, async:false}
	        );
	    } else {	    	
	    	selectRamoAtividade.disabled = true;
	    }
			
	}

	function init() {

		faixasTarifaClasses();
		var idCloneFaixa = '<c:out value="${tarifaFormVO.idCloneFaixa}"/>';
		if(idCloneFaixa != undefined && idCloneFaixa != ""){
			carregarVigencias(idCloneFaixa);
			var idVigenciasTarifa = '<c:out value="${tarifaFormVO.idVigenciasTarifa}"/>';
			if(idVigenciasTarifa != undefined && idVigenciasTarifa != ''){
				document.getElementById('idVigenciasTarifa').value = idVigenciasTarifa;
			}
		}

		var idSelecionado = "${tarifaForm.map.identificadorSelecionado}";		
		var idCompletoSelect = "identificadores" + idSelecionado;
		if((idCompletoSelect != undefined) && (idCompletoSelect != '') && (idSelecionado != undefined) && (idSelecionado != '')){
		}
	}
	
	addLoadEvent(init);	

	function limparDesconto() {
		$("input[name=descontoFixoSemImposto]").val("");
		$("input[name=descontoVariavelSemImposto]").val("");
	}

</script>


<div class="bootstrap">
	<form:form method="post" action="exibirInsercaoTarifa" name="tarifaForm" id="tarifaForm">
		
		<input name="acaoAlterarTarifa" type="hidden" id="acao" value="false">
		<input name="postBack" type="hidden" id="postBack" value="true">
		<input name="faixaInicialExclusao" type="hidden" id="faixaInicialExclusao">
		<input name="faixaFimAdicao" type="hidden" id="faixaFimAdicao">
		<input name="ultimaFaixa" type="hidden" id="ultimaFaixa">
		<input name="identificadorSelecionado" type="hidden" id="identificadorSelecionado" value="${tarifaForm.map.identificadorSelecionado}">
		<token:token></token:token>
	
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Incluir Tarifa</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         			Informe os dados abaixo e clique em <strong>Salvar</strong> para finalizar.
   			 	</div>
   			 	<div class="row">
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
							<div class="col-md-10">
								<label for="descricao">Descri��o: <span class="text-danger">*</span></label>
								 <input class="form-control form-control-sm" id="descricao" type="text" name="descricao"
                                    maxlength="30" size="35" value="${tarifa.descricao}"
									onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="col-md-10">
								<label for="descricaoAbreviada">Descri��o Abreviada: <span class="text-danger">*</span></label>
								 <input class="form-control form-control-sm" type="text"  id="descricaoAbreviada"  name="descricaoAbreviada"
                                    maxlength="7" size="10" value="${tarifa.descricaoAbreviada}"
									onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
							</div>
						</div>
						
						<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="tipoContrato">Tipo de Tarifa: <span class="text-danger">*</span></label>
             				<select name="tipoContrato" id="tipoContrato" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaTipoContrato}" var="tipoContrato">
									<option value="<c:out value="${tipoContrato.chavePrimaria}"/>" <c:if test="${tarifa.tipoContrato.chavePrimaria eq tipoContrato.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoContrato.descricao}"/>
									</option>	
								</c:forEach>	
           					</select>
           					</div>
        		 		</div>
						
						<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idFatura">Item da Fatura: <span class="text-danger">*</span></label>
             				<select name="itemFatura" id="idItemFatura" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaItemFatura}" var="itemFatura">
									<option value="<c:out value="${itemFatura.chavePrimaria}"/>"
									<c:if test="${tarifa.itemFatura.chavePrimaria == itemFatura.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${itemFatura.descricao}" />
									</option>
								</c:forEach>
           					</select>
           					</div>
        		 		</div>

   			 		</div>
   			 		
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idSegmento">Segmento: <span class="text-danger">*</span></label>
             				<select name="segmento" id="idSegmento" class="form-control form-control-sm" onchange="carregarRamosAtividade(this);"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaSegmento}" var="segmento">
									<option value="<c:out value="${segmento.chavePrimaria}"/>"
										<c:if test="${tarifa.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
											<c:out value="${segmento.descricao}" />
									</option>
								</c:forEach>
           					</select>
           					</div>
        		 		</div>
        		 		
        		 		<div class="form-row">
             				<div class="col-md-10">
    		 				<label  for="idRamoAtividade">Ramo de Atividade: </label>
             				<select name= "ramoAtividade" id="idRamoAtividade" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaRamoAtividade}" var="ramoAtividade">
									<option value="<c:out value="${ramoAtividade.chavePrimaria}"/>" <c:if test="${tarifa.ramoAtividade.chavePrimaria == ramoAtividade.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${ramoAtividade.descricao}"/>
									</option>		
			    				</c:forEach>	
           					</select>
           					</div>
   			 			</div>
   			 			
   			 			<div class="form-row">
							<div class="col-md-10">
								<label for="qtdCasasDecimais">Quantidade de casas decimais da tarifa: <span class="text-danger">*</span></label>
								 <input class="form-control form-control-sm" type="text" id="qtdCasasDecimais" name="quantidadeCasaDecimal"
                                    maxlength="1" size="3" value="${tarifa.quantidadeCasaDecimal}" onkeypress="return formatarCampoInteiro(event)"/>
							</div>
						</div>
        		 		<p><span class="text-danger">* </span>Campos Obrigat�rios para Inclus�o de Tarifa.</p>
   			 		</div>
   			 	</div><!-- primeira linha -->
   			 	
   			 	<hr/>
   			 	
   			 	<div class="row">
   			 		<h5 class="col-md-12">Vig�ncia</h5>
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
							<div class="col-md-6">
        						<label for="dataVigencia">In�cio da Vig�ncia: <span class="text-danger">*</span></label>
        							<div class="input-group input-group-sm">
                   						<input type="text" class="form-control form-control-sm campoData"
                     					id="dataVigencia" name="dataVigencia" value="<fmt:formatDate value="${tarifaVigencia.dataVigencia}" pattern="dd/MM/yyyy" />">
               						</div>
        					</div>
						</div>
						
						<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idTipoCalculo">Tipo de C�lculo: <span class="text-danger">*</span></label>
             				<select name="tipoCalculo" id="idTipoCalculo" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaTipoCalculo}" var="tipoCalculo">
									<option value="<c:out value="${tipoCalculo.chavePrimaria}"/>" <c:if test="${tarifaVigencia.tipoCalculo.chavePrimaria == tipoCalculo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${tipoCalculo.descricao}"/>
									</option>		
			    				</c:forEach>	
           					</select>
           					</div>
   			 			</div>
						
   			 		</div>
   			 		
   			 		
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idBaseApuracao">Volume Base de C�lculo: <span class="text-danger">*</span></label>
             				<select name="baseApuracao" id="idBaseApuracao" class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaBaseCalculo}" var="baseCalculo">
									<option value="<c:out value="${baseCalculo.chavePrimaria}"/>" <c:if test="${tarifaVigencia.baseApuracao.chavePrimaria == baseCalculo.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${baseCalculo.descricao}"/>
									</option>		
			    				</c:forEach>
           					</select>
           					</div>
   			 			</div>
   			 			
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idUnidadeMonetaria">Unidade Monet�ria: <span class="text-danger">*</span></label>
             				<select name="unidadeMonetaria" id="idUnidadeMonetaria"  class="form-control form-control-sm"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaUnidadeMonetaria}" var="unidadeMonetaria">
									<option value="<c:out value="${unidadeMonetaria.chavePrimaria}"/>" <c:if test="${tarifaVigencia.unidadeMonetaria.chavePrimaria == unidadeMonetaria.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${unidadeMonetaria.descricao}"/>
									</option>		
			   					 </c:forEach>
           					</select>
           					</div>
   			 			</div>
   			 			<p><span class="text-danger">* </span>Campos Obrigat�rios para Cadastrar a Vig�ncia.</p>
   			 		
   			 		</div>
   			 	</div><!-- segunda linha -->
   			 	
   			 	<hr/>
   			 	
   			 	<div class="row">
   			 		<h5 class="col-md-12">Tributos</h5>
   			 		
   			 		<div class="col-md-6">
   			 			<div class="form-row">
   			 			
   			 				 <div class="col-md-5">
             					<label for="tributosDisponiveis">Dispon�veis:</label>
								<select multiple id="tributosDisponiveis" class="form-control"
									onDblClick="moveSelectedOptionsEspecial(document.forms[0].tributosDisponiveis,document.forms[0].tributosAssociados,true);associarTributos();">
								<c:forEach items="${listaTributos}" var="tributo">
									<option value="<c:out value="${tributo.chavePrimaria}"/>" title="<c:out value="${tributo.descricao}"/>"> <c:out value="${tributo.descricao}"/></option>		
								</c:forEach>
								</select>
           		  			</div>
           		  			
           		  			<div class="col-md-2 text-center align-self-center">
           		  			
           		  				<div class="btn-group" role="group" >
                   					<button name="left" id="botaoEsquerda" class="btn btn-sm btn-info" type="button" 
           		  					onClick="moveSelectedOptionsEspecial(document.forms[0].tributosAssociados,document.forms[0].tributosDisponiveis,true);">
                       				<i class="fas fa-angle-left"></i>
                   					</button>
                   					
                   					<button name="right" id="botaoDireita" class="btn btn-sm btn-info" type="button"
           		  					onClick="moveSelectedOptionsEspecial(document.forms[0].tributosDisponiveis,document.forms[0].tributosAssociados,true);">
                       				<i class="fas fa-angle-right"></i>
                   					</button>
                   				</div>
                   				
                   				<div class="btn-group" role="group" >
                   					<button name="left" id="botaoEsquerdaTodos" class="btn btn-sm btn-info mt-1" type="button" title="Todos"
           		  					onClick="moveAllOptionsEspecial(document.forms[0].tributosAssociados,document.forms[0].tributosDisponiveis,true);">
                       				<i class="fas fa-angle-double-left"></i> 
                       				</button>
                       					
                       				<button name="right" id="botaoDireitaTodos" class="btn btn-sm btn-info mt-1" type="button" title="Todos"
           		  					onClick="moveAllOptionsEspecial(document.forms[0].tributosDisponiveis,document.forms[0].tributosAssociados,true);">
                       				<i class="fas fa-angle-double-right"></i> 
                   					</button>
                   			
                   				</div>
           		  			</div>
           		  			
           		  			 <div class="col-md-5">
             					<label for="tributosAssociados">Selecionados:<span class="itemContador" id="quantidadePontosAssociados"></span> </label>
								<select multiple id="tributosAssociados" name="tributosAssociados" class="form-control"
									onDblClick="moveSelectedOptionsEspecial(document.forms[0].tributosAssociados,document.forms[0].tributosDisponiveis,true);associarTributos();">
								<c:forEach items="${listaTributosAssociados}" var="tributosAssociados">
									<option value="<c:out value="${tributosAssociados.chavePrimaria}"/>" title="<c:out value="${tributosAssociados.descricao}"/>"><c:out value="${tributosAssociados.descricao}"/></option>
								</c:forEach>
								</select>
           		  			</div>
   			 			
   			 			</div>
   			 		
   			 		</div>
   			 		
   			 		<div class="col-md-6">
   			 			<div class="form-row">
   			 			<div class="col-md-10">
    						<label for="observacaoVigencia">Observa��o: <span class="text-danger">*</span></label>
   						 	<textarea class="form-control" id="observacaoVigencia" rows="3" 
   						 	onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
							name="comentario" onkeypress="return formatarCampoTextoLivreComLimite(event,this,199);"><c:out value="${tarifaVigencia.comentario}"/></textarea>
  						</div>
   			 			</div>
   			 		</div>
   			 	</div><!-- fim da terceira linha -->
   			 	<hr id="linhaICMSSubstituido"/>
   			 	
   			 	<div class="row" id="conteinerICMSSubstituido">
   			 		<h5 class="col-md-12">ICMS Substituto</h5>
   			 		<div class="col-md-6">
   			 			<div class="form-row mt-1">
							<label class="col-md-12" for="idInformarMensagemIcms">Informar mensagem complementar para ICMS substituto?</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="informarMsgIcmsSubstituido" id="informarMsgIcmsSubstituidoSim" checked="checked" class="custom-control-input" value="true"
									   <c:if test="${tarifaVigencia.informarMsgIcmsSubstituido eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="informarMsgIcmsSubstituidoSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="informarMsgIcmsSubstituido" id="informarMsgIcmsSubstituidoNao" class="custom-control-input" value="false"
									   <c:if test="${tarifaVigencia.informarMsgIcmsSubstituido eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="informarMsgIcmsSubstituidoNao">N�o</label>
								</div>
							</div>
						</div>
   			 		
   			 			<div class="form-row">
							<div class="col-md-10">
								<label for="idIBaseCalculoIcms" id="rotuloBaseCalculoIcms">Base C�lculo do ICMS Subs. por Metro C�bico:</label>
								 <input class="form-control form-control-sm" type="text" id="baseCalculoIcmsSubstituido" name="vigenciaBaseCalculoIcms"
                                    <c:if test="${tarifaVigencia.informarMsgIcmsSubstituido eq 'false'}">disabled="disabled"</c:if> value="${tarifaFormVO.vigenciaBaseCalculoIcms}"
                                    onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 4);"/>
							</div>
						</div>
   			 		
   			 			<div class="form-row">
							<div class="col-md-10">
								<label id="rotuloIcmsSubstituidoMetroCubico" for="idInformarMensagemIcms">ICMS Substitu�do por Metro C�bico:</label>
								 <input class="form-control form-control-sm" type="text" id="icmsSubstituidoMetroCubico" name="vigenciaIcmsSubstituidoMetroCubico"
                                    <c:if test="${tarifaVigencia.informarMsgIcmsSubstituido eq 'false'}">disabled="disabled"</c:if> value="${tarifaFormVO.vigenciaIcmsSubstituidoMetroCubico}"
                                    onkeypress="return formatarCampoDecimalPositivo(event, this, 11, 4);"/>
							</div>
						</div>
						
   			 		</div>
   			 		<div class="col-md-6">
   			 			<div class="col-md-10">
   			 			<div class="form-row">
    						<label for="mensagemIcmsSubstituto" id="rotuloObservacaoVigencia">Mensagem ICMS Substitu�do:</label>
   						 	<textarea class="form-control" id="observacaoVigenciaIcmsSubstituto" <c:if test="${tarifaVigencia.informarMsgIcmsSubstituido eq 'false'}">disabled="disabled"</c:if> 
							onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" rows="3"
							name="mensagemIcmsSubstituto" onkeypress="return formatarCampoTextoLivreComLimite(event,this,199);"><c:out value="${tarifaVigencia.mensagemIcmsSubstituto}"/></textarea>
  						</div>
   			 			</div>
   			 		</div>
   			 		
   			 	</div><!-- fim da quarta linha -->
   			 	
   			 	<hr/>
   			 	<a name="blocoVigenciaTarifa"></a>
   			 	<div class="row">
   			 		<h5 class="col-md-12">Faixas de Tarifa</h5>
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idCloneFaixa">Clonar faixas de:</label>
             				<select name="idCloneFaixa" id="idCloneFaixa" class="form-control form-control-sm" onchange="carregarVigenciasFaixas(this);"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaCloneFaixa}" var="objCloneFaixa">
									<option value="<c:out value="${objCloneFaixa.chavePrimaria}"/>" <c:if test="${tarifaFormVO.idCloneFaixa == objCloneFaixa.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${objCloneFaixa.descricao}"/>
									</option>		
				    			</c:forEach>
           					</select>
           					</div>
   			 			</div>
   			 			
   			 			<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idVigenciasTarifa">Vig�ncia(s):</label>
             				<select name="idVigenciasTarifa" id="idVigenciasTarifa" class="form-control form-control-sm" <c:if test="${empty listaVigenciaTarifa}">disabled="disabled"</c:if>> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaVigenciasTarifa}" var="objVigenciasTarifa">
									<option value="<c:out value="${objVigenciasTarifa.chavePrimaria}"/>" <c:if test="${tarifaFormVO.idVigenciasTarifa == objVigenciasTarifa.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${objVigenciasTarifa.descricao}"/>
									</option>
				    			</c:forEach>
           					</select>
           					</div>
   			 			</div>
   			 			<div class="form-row  mt-3 ">
   			 			 	<div class="col-md-10 align-self-end text-center">
   			 					<button class="btn btn-primary btn-sm" name="btnCloneOK" type="button"
                        		onclick="clonarFaixa();"><i class="fas fa-clone"></i> Confirmar </button>
                        	</div>
                        </div>
   			 		</div>
   			 		
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
							<div class="col-md-10">
								<label for="replicante_valor_compra">Valor de Compra (R$):</label>
								 <input class="form-control form-control-sm" id="replicante_valor_compra" type="text" name="replicante_valor_compra" 
								 onkeypress="return formatarCampoDecimalPositivo(event, this, 7, getQtdCasasDecimais());">
							</div>
						</div>
						
						<div class="form-row">
							<div class="col-md-10">
								<label for="replicante_margem_valor_agregado">Margem de Valor Agregado (R$):</label>
								 <input class="form-control form-control-sm" id="replicante_margem_valor_agregado" type="text" name="replicante_margem_valor_agregado"
								 onkeypress="return formatarCampoDecimalPositivo(event, this, 7, getQtdCasasDecimais());">
							</div>
						</div>
   			 		
   			 			<div class="form-row mt-3 mb-2">
   			 			 	<div class="col-md-10 align-self-end text-center">
   			 					<button class="btn btn-primary btn-sm" name="btnCloneOK" type="button"
                        		onclick="replicarValoresVcMva();"><i class="fas fa-clone"></i> Replicar </button>
                        	</div>
                        </div>
   			 		</div>
   			 	</div><!-- fim da quinta linha -->
   			 	
   			 	
   			 	
   			 	<!-- tabela 1 -->
   			 	<div id="tarifaFaixas">
   			 		<div class="table-responsive">
   			 			<table class="table table-bordered table-striped table-hover tabelaFaixas" id="dadosFaixa" style="width:100%">
   			 			<thead class="thead-ggas-bootstrap">
                	 	 	<tr>
                    			<th scope="col" class="text-center">Faixa Inicial (m�)</th>
                    			<th scope="col" class="text-center">Faixa Final (m�)</th>
                    			<th scope="col" class="text-center">Valor Fixo <br>sem Imposto (R$)</th>
                    			<th scope="col" class="text-center">Valor de <br> Compra (R$)</th>
                    			<th scope="col" class="text-center">Margem de Valor <br> Agregado (R$)</th>
                    			<th scope="col" class="text-center">Valor Vari�vel<br> sem Imposto (R$)</th>
                    			<th scope="col" class="text-center">A��es</th>
                  			</tr>
                	 	 </thead>
                	 	 <tbody>
                	 	 <c:set var="i" value="0" />
                	 	 <c:forEach items="${sessionScope.listaDadosFaixa}" var="dadosFaixa">
                	 	 	<tr>
                	 	 		<td class="text-center">
                	 	 			<input type="text" class="form-control form-control-sm faixaInicial" name="faixaInicial" disabled="disabled" value="${dadosFaixa.faixaInicial}" onblur="aplicarMascaraNumeroInteiro(this);" onkeypress="return formatarCampoDecimalPositivo(event,this,9,2);">
									<input type="hidden" name="faixaInicialEscondido" value="<c:out value="${dadosFaixa.faixaInicial}"/>">
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<input type="text" class="form-control form-control-sm faixaFinal" 
										id="primeiraFI_${i}" name="faixaFinal" value="${dadosFaixa.faixaFinal}" 
										onblur="aplicarMascaraNumeroInteiro(this);" 
										onkeypress="return formatarCampoInteiro(event,17);">
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<input type="text" id="valorFixoSemImposto_${i}" size="6"
										class="form-control form-control-sm valorFixoSemImposto valorConteinerTarifa"
										name="valorFixoSemImposto" value="${dadosFaixa.valorFixoSemImposto}" 
										onblur="aplicarMascaraNumeroDecimal(this,getQtdCasasDecimais());" 
										onkeypress="return formatarCampoDecimalPositivo(event,this,7,getQtdCasasDecimais());">
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<input type="text" id="valor_compra_parcela_vvsi_${i}" 
										class="form-control form-control-sm valorFixoSemImposto valorConteinerTarifa" 
										name="valorCompra" value="${dadosFaixa.valorCompra}" size="6"
										onblur="aplicarMascaraNumeroDecimalCalcularVvsi(this, getQtdCasasDecimais(), ${i});" 
										onkeypress="return formatarCampoDecimalPositivo(event, this, 7, getQtdCasasDecimais());">
									<input type="hidden" id="valor_compra_parcela_vvsi_escondido_${i}" 
										name="valorCompraEscondido" value="<c:out value="${dadosFaixa.valorCompra}"/>">
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<input type="text" id="margem_valor_agregado_parcela_vvsi_${i}" 
										class="form-control form-control-sm valorFixoSemImposto valorConteinerTarifa" 
										name="margemValorAgregado" value="${dadosFaixa.margemValorAgregado}" size="6"
										onblur="aplicarMascaraNumeroDecimalCalcularVvsi(this, getQtdCasasDecimais(), ${i});" 
										onkeypress="return formatarCampoDecimalPositivo(event, this, 7, getQtdCasasDecimais());">
									<input type="hidden" id="margem_valor_agregado_parcela_vvsi_escondido_${i}"
										name="margemValorAgregadoEscondido" value="<c:out value="${dadosFaixa.margemValorAgregado}"/>">
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<input type="text" id="valor_variavel_sem_imposto_${i}" 
										class="form-control form-control-sm valorVariavelSemImposto valorConteinerTarifa" 
										name="valorVariavelSemImposto" value="${dadosFaixa.valorVariavelSemImposto}" size="6"
										onblur="aplicarMascaraNumeroDecimalVvsiFixo(this, getQtdCasasDecimais(), ${i});" 
										onkeypress="return formatarCampoDecimalPositivo(event,this,7,getQtdCasasDecimais());">
									<input type="hidden" id="valor_variavel_sem_imposto_escondido_${i}"
										name="valorVariavelSemImpostoEscondido" value="<c:out value="${dadosFaixa.valorVariavelSemImposto}"/>">
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<c:choose>
		        						<c:when test="${fn:length(sessionScope.listaDadosFaixa) > 1 && i > 0}">
		        							<button type="button" onClick='adicionarFaixaTarifa(${i}, <c:out value="${dadosFaixa.faixaInicial}"/>);' 
		        							title="Adicionar" class="btn btn-primary btn-sm botaoAdicionarFaixas botaoAdicionar"><i class="fas fa-plus-circle"></i></button>
		        						
		        							<button type="button" onClick="removerFaixaTarifa('<c:out value='${dadosFaixa.faixaInicial}'/>')" 
		        							title="Remover" class="btn btn-danger btn-sm botaoRemoverFaixas botaoRemover"><i class="fas fa-times-circle"></i></button>
		        						</c:when>
		        						<c:otherwise>
		        							<button type="button" class="btn btn-primary btn-sm botaoAdicionarFaixas botaoAdicionar" 
			        						onClick="adicionarFaixaTarifa(${i});" title="Adicionar"><i class="fas fa-plus-circle"></i></button>
		        						</c:otherwise>
		        					</c:choose>
                	 	 		</td>
                	 		</tr>
                	 		<c:set var="i" value="${i+1}" />	
                	 		</c:forEach>
                	 	 </tbody>
   			 			</table>
   			 		</div>
   			 		
   			 		 <div class="col mt-2 align-self-end text-right">
                        <button class="btn btn-primary btn-sm botaoAplicar" id="Descontos" type="button"
                        	onClick="aplicarFaixasTarifa();">
                           	<i class="fas fa-money-bill"></i> Descontos
                        </button>
                    </div>
   			 		
   			 	</div>
   			 	<!-- fim da table 1 -->
   			 	
   			 	<c:if test="${sessionScope.listaDescontosFaixa ne null}">
   			 	<hr/>
   			 	
   			 	<div class="row mb-2" id="tarifaDescontos">
   			 		
   			 		<h5 class="col-md-12">Descontos</h5>
   			 		
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
							<div class="col-md-10">
								<label for="dataDescontoVigenciaDe">Per�odo de Vig�ncia: <span> *</span></label>
								<div class="input-group input-group-sm">

									<input type="text" aria-label="mesAnoFaturamentoInicial" id="dataDescontoVigenciaDe"
										   class="form-control form-control-sm campoData" name="inicioVigencia"
										   value="<fmt:formatDate value="${tarifaVigenciaDesconto.inicioVigencia}" pattern="dd/MM/yyyy" />">
									<div class="input-group-prepend">
										<span class="input-group-text">at�</span>
									</div>
									<input type="text" aria-label="mesAnoFaturamentoFinal" id="dataDescontoVigenciaPara"
										   class="form-control form-control-sm campoData" name="fimVigencia"
										   value="<fmt:formatDate value="${tarifaVigenciaDesconto.fimVigencia}" pattern="dd/MM/yyyy" />">
								</div>
							</div>
						</div>
						
						<div class="form-row">
             				<div class="col-md-10">
    		 				<label for="idTipoDesconto">Tipo de Desconto: <span>*</span></label>
             				<select id="idTipoDesconto" name="idTipoDesconto" class="form-control form-control-sm" onchange="limparDesconto();"> 
                  				<option value="-1">Selecione</option>
                  				<c:forEach items="${listaTipoValor}" var="tipoValorVariavel">
									<option value="<c:out value="${tipoValorVariavel.chavePrimaria}"/>" <c:if test="${tarifaFormVO.idTipoDesconto == tipoValorVariavel.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${tipoValorVariavel.descricao}"/>
									</option>		
								</c:forEach>
           					</select>
           					</div>
        		 		</div>
						
						<div class="form-row mt-1 mb-1">
							<label class="col-md-12">Discriminar desconto na Nota da Fatura?</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" name="impressaoDesconto" id="discriminarNotaFaturaSim" value="true" 
									<c:if test="${tarifaVigenciaDesconto.impressaoDesconto eq 'true' or (empty tarifaVigenciaDesconto.impressaoDesconto)}">checked</c:if>>
									<label class="custom-control-label" for="discriminarNotaFaturaSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" name="impressaoDesconto" id="discriminarNotaFaturaNao" value="false" 
									<c:if test="${tarifaVigenciaDesconto.impressaoDesconto eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="discriminarNotaFaturaSim">N�o</label>
								</div>
							</div>
						</div>
						
   			 		</div>
   			 		
   			 		<div class="col-md-6">
   			 		
   			 			<div class="form-row">
   			 				<div class="col-md-10">
    							<label for="descricaoDescontoVigencia">Descri��o: <span>*</span></label>
   						 		<textarea class="form-control" id="descricaoDescontoVigencia" rows="3" 
								onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 
								'formatarCampoNome(event)');" name="descricaoDescontoVigencia" onkeypress="return formatarCampoTextoLivreComLimite(event,this,255);">
								<c:out value="${tarifaVigenciaDesconto.descricaoDescontoVigencia}"/>
								</textarea>
  							</div>
   			 			</div>
   			 			
   			 				<div class="form-row mt-1 ">
							<label id="rotuloImprimirNotaFatura" for="imprimirNotaFatura">Imprimir descri��o na Nota / Fatura?</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" name="impressaoDescricao" id="imprimirNotaFaturaSim" value="true" 
									<c:if test="${tarifaVigenciaDesconto.impressaoDescricao eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="imprimirNotaFatura">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" name="impressaoDescricao" id="imprimirNotaFaturaNao" value="false" 
									<c:if test="${tarifaVigenciaDesconto.impressaoDescricao eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="imprimirNotaFaturaNao">N�o</label>
								</div>
							</div>
						</div>
   			 			
   			 		
   			 		</div>
   			 		
   			 	</div><!-- row dos descontos -->
   			 	
   			 	<div id="faixaTarifaDescontos">
   			 	
   			 		<div class="table-responsive">
   			 			<table class="table table-bordered table-striped table-hover" id="descontoFaixa" style="width:100%">
   			 			<thead class="thead-ggas-bootstrap">
                	 	 	<tr>
                    			<th scope="col" class="text-center">Faixa Inicial<br>- Final (m�)</th>
                    			<th scope="col" class="text-center">Valor Fixo<br>sem Imposto (R$)</th>
                    			<th scope="col" class="text-center">Desconto para Valor<br>Fixo s\ Imposto</th>
                    			<th scope="col" class="text-center">Valor Fixo da Tarifa<br>Aplicado o Desconto (R$)</th>
                    			<th scope="col" class="text-center">Valor Vari�vel<br>sem Imposto (R$)</th>
                    			<th scope="col" class="text-center">Desconto para Valor<br>Vari�vel s/ Imposto</th>
                    			<th scope="col" class="text-center">Valor Vari�vel da Tarifa<br>Aplicado o Desconto (R$)</th>
                  			</tr>
                	 	 </thead>
                	 	 <tbody>
                	 	 <c:set var="j" value="0" />
                	 	 <c:forEach items="${sessionScope.listaDescontosFaixa}" var="descontoFaixa">
                	 	 	<tr>
                	 	 		<td class="text-center">
                	 	 			<c:out value="${descontoFaixa.faixaInicial}"/> - <c:out value="${descontoFaixa.faixaFinal}"/>
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<span id="valor_fixo_sem_imposto_tabela_desconto_${j}" >
										<c:out value="${descontoFaixa.valorFixoSemImposto}"/>
		        					</span>
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<c:if test="${descontoFaixa.valorFixoSemImpostoPreenchido}">
										<input type="text" class="form-control form-control-sm valorConteinerTarifa" id="desconto_fixo_sem_imposto_${j}"
											name="descontoFixoSemImposto" value="${descontoFaixa.descontoFixoSemImposto}" 
											onblur="aplicarMascaraNumeroDecimalCalcularVtad(this, getQtdCasasDecimaisDesconto(), 
												'valor_fixo_sem_imposto_tabela_desconto_${j}', 'desconto_fixo_sem_imposto_${j}', 'valor_fixo_tarifa_aplicados_descontos_${j}',
												'idTipoDesconto');" onkeypress="return formatarCampoDecimalPositivo(event, this, 7, getQtdCasasDecimaisDesconto());">
									</c:if>
									<c:if test="${!descontoFaixa.valorFixoSemImpostoPreenchido}">
										<input type="text" class=" form-control form-control-sm valorConteinerTarifa" 
											name="descontoFixoSemImposto" value="${descontoFaixa.descontoFixoSemImposto}" readonly>
										</c:if>
                	 	 			</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<input type="text" class="form-control form-control-sm valorConteinerTarifa" 
									id="valor_fixo_tarifa_aplicados_descontos_${j}" readonly/>
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<span id="valor_variavel_sem_imposto_tabela_desconto_${j}" >
		        						<c:out value="${descontoFaixa.valorVariavelSemImposto}"/>
		        					</span>
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<c:if test="${descontoFaixa.valorVariavelSemImpostoPreenchido}">
										<input type="text" class="form-control form-control-sm valorConteinerTarifa" 
										id="desconto_variavel_sem_imposto_${j}"  name="descontoVariavelSemImposto" 
										value="${descontoFaixa.descontoVariavelSemImposto}" onblur="aplicarMascaraNumeroDecimalCalcularVtad(
											this, getQtdCasasDecimaisDesconto(), 'valor_variavel_sem_imposto_tabela_desconto_${j}', 'desconto_variavel_sem_imposto_${j}', 
											'valor_variavel_tarifa_aplicados_descontos_${j}', 'idTipoDesconto');" 
										onkeypress="return formatarCampoDecimalPositivo(event, this, 7, getQtdCasasDecimaisDesconto());">
									</c:if>
									<c:if test="${!descontoFaixa.valorVariavelSemImpostoPreenchido}">
									<input type="text" class="form-control form-control-sm valorConteinerTarifa" name="descontoVariavelSemImposto" 
										value="${descontoFaixa.descontoVariavelSemImposto}" readonly/>
									</c:if>
                	 	 		</td>
                	 	 		
                	 	 		<td class="text-center">
                	 	 			<input type="text" class="form-control form-control-sm valorConteinerTarifa" 
										id="valor_variavel_tarifa_aplicados_descontos_${j}" readonly />
                	 	 		</td>
                	 		</tr>
                	 		<c:set var="j" value="${j+1}" />	
                	 		</c:forEach>
                	 	 </tbody>
   			 			</table>
   			 	
   			 		</div>
   			 	
   			 	</div>
   			 	
   			 	</c:if>
   			 	
			</div><!-- fim do card-body -->
			
			
			<div class="card-footer">
        		<div class="row">
        			<div class="col-sm-12">
        				  <button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="voltar();">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                        <button class="btn btn-secondary btn-sm float-left ml-1 mt-1" type="button" onclick="limparFormulario();">
                            <i class="far fa-trash-alt"></i> Limpar
                        </button>
                        <vacess:vacess param="exibirInsercaoTarifa">
                  		<button id="buttonSalvar" class="btn btn-sm btn-success float-right ml-1 mt-1"  type="button" onclick="salvar();">
                       		<i class="fa fa-save"></i> Salvar               		
                       	</button>
             		 	</vacess:vacess> 
        			</div>
        		</div>
        	</div>
		</div><!-- fim do card -->
	<div style="display: none;" id="erroMsg" title="Aten��o">$(mensagemErro).text()</div>
	</form:form>
</div>


















		    
		  