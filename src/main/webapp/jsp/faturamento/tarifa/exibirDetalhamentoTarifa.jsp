<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

	$(document).ready(function(){
		$("#tarifaFaixaDesconto").DataTable( {
	    	 "responsive": true,
	    	 "paging":   false,
	         "ordering": false,
	         "info": false,
	         "searching": false,
	    	 "language": {
	             "zeroRecords": "Nenhum registro foi encontrado"
	    	 }
	    } );
		
		$("#tarifasVigenciaId").change();
		
	});

	
	function voltar(){	
		submeter("tarifaForm", "pesquisarTarifa");
	}

	function alterar(chave){
		document.tarifaForm.chavePrimaria.value = chave;
		submeter('tarifaForm', 'exibirAlteracaoTarifa');
	}
	
	function exibirDetalhamentoTarifaFaixaDesconto(){
		submeter('tarifaForm','exibirDetalhamentoTarifaFaixaDesconto');
	}

	function exibirDadosTarifaVigencia(oList){
		var idSelecionado = oList.options[oList.selectedIndex].value;
	
		$('span#descricaoVigenciaTipoCalculo').html('');
		$('span#descricaoVigenciaVolumeBaseCalculo').html('');
		$('span#descricaoVigenciaUnidadeMonetaria').html('');
		$('span#observacaoVigencia').html('');
		$('span#statusVigencia').html('');
		$('span#habilitado').html('');
		$('span#baseCalculoIcms').html('');
		$('span#icmsSubstituido').html('');
		$('span#mensagemIcmsSubstituido').html('');
		$('span#tributos').html('');
		

		if(idSelecionado != '' && idSelecionado != '-1') {
			$.get("obterTarifaVigencia?chavePrimaria=" + idSelecionado).
					done(function(tarifaVigencia) {
						$('span#descricaoVigenciaTipoCalculo').html(tarifaVigencia['descricaoVigenciaTipoCalculo']);
						$('span#descricaoVigenciaVolumeBaseCalculo').html(tarifaVigencia['descricaoVigenciaVolumeBaseCalculo']);
						$('span#descricaoVigenciaUnidadeMonetaria').html(tarifaVigencia['descricaoVigenciaUnidadeMonetaria']);
						$('span#observacaoVigencia').html(tarifaVigencia['observacaoVigencia']);
						$('span#statusVigencia').html(tarifaVigencia['status']);
						$('span#habilitado').html(tarifaVigencia['habilitado']);
						$('span#baseCalculoIcms').html(tarifaVigencia['baseCalculoIcms']);
						$('span#icmsSubstituido').html(tarifaVigencia['icmsSubstituido']);
						$('span#mensagemIcmsSubstituido').html(tarifaVigencia['mensagemIcmsSubstituto']);
						$('span#tributos').html(tarifaVigencia['tributos']);
					});      
        } 
	}

	function atualizarSelectTarifaVigenciaDesconto(oList){

		var selTarifaVigenciaDesconto = document.getElementById('tarifasVigenciaDescontoId');
		selTarifaVigenciaDesconto.length = 0;

		var oObservacao = document.getElementById('descricaoVigenciaDesconto');
		oObservacao.innerHTML = '';

		var novaOpcao = new Option("Selecione","-1");
		selTarifaVigenciaDesconto.options[selTarifaVigenciaDesconto.length] = novaOpcao;		

	    var tarifasVigencia = document.getElementById('tarifasVigenciaId');
		
	 	if (tarifasVigencia.options[tarifasVigencia.selectedIndex].value != "-1") {
			selTarifaVigenciaDesconto.disabled = false;
			
			$.get("obterTarifaVigenciaDescontoPorTarifaVigencia?chavePrimariaTarifaVigencia=" + oList)
						.done(function(listaTarifaVigenciaDesconto) {
							for (key in listaTarifaVigenciaDesconto){
								var novaOpcao = new Option(listaTarifaVigenciaDesconto[key], key);
								selTarifaVigenciaDesconto.options[selTarifaVigenciaDesconto.length] = novaOpcao;
			                }
							
							var valor = '${tarifasVigenciaDescontoId}';
							var listaDescontos = document.getElementById('tarifasVigenciaDescontoId');
							listaDescontos.value = valor;
							$("#tarifasVigenciaDescontoId").change();
						});
			
			
			
		}
	}

	function exibirDadosTarifaVigenciaDesconto(oList){
		var idSelecionado = oList.value;

		$('span#descricaoVigenciaDesconto').html('');
		
		if(idSelecionado != '' && idSelecionado != '-1') {	
			$.get("obterTarifaVigenciaDesconto?chavePrimaria="+idSelecionado)
					.done(function(tarifaVigenciaDesconto) {
						if(tarifaVigenciaDesconto != null){  
		           			$('span#descricaoVigenciaDesconto').html(tarifaVigenciaDesconto['descricaoVigenciaDesconto']);
		               	}
					});	        
	    }  
	}

	

	function verificarPermissaoAutorizacao(oList) {	

		var idUsuario = document.getElementById('idUsuario').value;
		var idTarifaVigencia = oList.options[oList.selectedIndex].value;

	     if (idUsuario != null && idUsuario != "0" 
		     && idTarifaVigencia != null && idTarifaVigencia != "-1") {
	    	 $.get("possuiAlcadaAutorizacaoTarifaVigencia?idUsuario="+idUsuario+"&idTarifaVigencia="+idTarifaVigencia)
		 	 		.done(function(possuiAlcadaAutorizacao) {
		 	 			exibirDadosTarifaVigencia(oList);
						atualizarSelectTarifaVigenciaDesconto(oList.value);
		 	 		});
		 } 
	}
	
</script>

<div class="bootstrap">
	<form:form method="post" action="exibirDetalhamentoTarifa" name="tarifaForm" id="tarifaForm">
		<input name="postBack" type="hidden" id="postBack" value="false">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tarifa.chavePrimaria}"/>
		<input name="chavePrimariaVigencia" type="hidden" id="chavePrimariaVigencia" value="${chavePrimariaVigencia}"/>
		<input name="idUsuario" type="hidden" id="idUsuario" value="${idUsuario}"/>
		<input name="versao" type="hidden" id="versao" value="${tarifa.versao}">

		<input name="status" type="hidden" id="filtroPesquisaStatus" value="${tarifaFormVO.status}" />
		<input name="dataVigencia" type="hidden" id="filtroPesquisaDataVigencia" value="${tarifaFormVO.dataVigencia }" />
		<input name="isAutorizacao" type="hidden" id="filtroPesquisaIsAutorizacao" value="${tarifaFormVO.habilitado }" />
		<input name="habilitado" type="hidden" id="filtroPesquisaIndicadorUso" value="${tarifaFormVO.habilitado }" />
		<input name="segmento" type="hidden" id="filtroPesquisaSegmento" value="${filtroPesquisaTarifa.segmento.chavePrimaria }" />
		<input name="ramoAtividade" type="hidden" id="filtroPesquisaRamoAtividade" value="${filtroPesquisaTarifa.ramoAtividade.chavePrimaria }"/>
		<input name="itemFatura" type="hidden" id="filtroPesquisaItemFatura" value="${filtroPesquisaTarifa.itemFatura.chavePrimaria }"/>
		<input name="tipoContrato" type="hidden" id="filtroPesquisaTipoContrato" value="${filtroPesquisaTarifa.tipoContrato.chavePrimaria }" />
		<input name="descricao" type="hidden" id="filtroPesquisaDescricao" value="${filtroPesquisaTarifa.descricao }" />
		<input name="descricaoAbreviada" type="hidden" id="filtroPesquisaDescricaoAbreviada" value="${filtroPesquisaTarifa.descricaoAbreviada }" />
		
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">
					Detalhar Tarifa
				</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
         			<i class="fa fa-question-circle"></i>
         			Para modificar as informa��es desta tarifa clique em <strong>Alterar</strong>.
   			 	</div>
				<div class="row" id="conteinerDetalhamentoTarifa">
					<h5 class="col-md-12 mt-2">Dados Gerais</h5>
					<div class="col-md-6">
						<ul class="list-group">
							<li class="list-group-item">
   			 					Descri��o: <span class="font-weight-bold">
								<c:out value="${tarifa.descricao}"/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Descri��o Abreviada: <span class="font-weight-bold">
								<c:out value="${tarifa.descricaoAbreviada}"/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Tipo de Contrato: <span class="font-weight-bold">
								<c:out value="${tarifa.tipoContrato.descricao}"/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Item da Fatura: 
								<c:forEach items="${listaItemFatura}" var="itemFatura">
								<c:if test="${tarifa.itemFatura.chavePrimaria == itemFatura.chavePrimaria}">
									<span class="font-weight-bold"><c:out value="${itemFatura.descricao}"/></span>
								</c:if>
		    					</c:forEach>	
   			 				</li>
						</ul>
						
					</div>
					<div class="col-md-6">
						<ul class="list-group">
							<li class="list-group-item">
   			 					Segmento: <span class="font-weight-bold">
								<c:out value="${tarifa.segmento.descricao}"/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Ramo Atividade: <span class="font-weight-bold">
								<c:out value="${tarifa.ramoAtividade.descricao}"/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Quantidade de casas decimais da Tarifa: <span class="font-weight-bold">
								<c:out value="${tarifa.quantidadeCasaDecimal.toString()}"/></span>
   			 				</li>
						</ul>
					</div>
					
				</div><!-- fim da primeira linha -->
				
				<hr/>
				
				
			 <div class="alert alert-primary col-md-12 fade show" role="alert">
         		<i class="fa fa-question-circle"></i>
         	    Selecionar a Vig�ncia e o Desconto desejados e Clique em <strong>Exibir</strong> para visualizar a tabela de Faixas e Descontos.
   			 </div>
   			 <div class="row mt-2" id="dadosVigenciasDescontos">
				<h5 class="col-md-12 mt-2 mb-3">Dados de Vig�ncia e Descontos</h5>
   	       			<div class="col-md-5">
   	       			
   	       				<div class="form-row mb-2">
             		 		<label for="tarifasVig" class="col-sm-3 mb-1">Vig�ncia:</label>
             		 		 <div class="col-sm-8">
                             <select id="tarifasVigenciaId" name="tarifasVigencia" class="form-control form-control-sm" onChange="verificarPermissaoAutorizacao(this);"> 
                  				<option value="">Selecione</option>
								<c:forEach items="${listaTarifasVigencia}" var="tarifaVigencia">
								<option value="<c:out value="${tarifaVigencia.chavePrimaria}"/>" 
									title="<fmt:formatDate value="${tarifaVigencia.dataVigencia}" type="both" pattern="dd/MM/yyyy" />" 
									<c:if test="${tarifaVigencia.chavePrimaria == tarifasVigencia}">selected="selected"</c:if>>
									<fmt:formatDate value="${tarifaVigencia.dataVigencia}" type="both" pattern="dd/MM/yyyy" />
								</option>
								</c:forEach>
           					</select>
           					</div>
             		 	</div>
   	       		
           			</div>
           			
           			<div class="col-md-5">
           				<div class="form-row mb-2">
             		 		<label for="tarifasVigenciaDescontoId" class="col-sm-3 mb-1">Descontos:</label>
             		 		 <div class="col-sm-8">
                             <select name="tarifasVigenciaDesconto" id="tarifasVigenciaDescontoId" class="form-control form-control-sm" onchange="exibirDadosTarifaVigenciaDesconto(this);"> 
                  					<option value="">Selecione</option>
									<c:forEach items="${listaTarifasVigenciaDesconto}" var="tarifaVigenciaDesconto">
										<option value="<c:out value="${tarifaVigenciaDesconto.chavePrimaria}"/>" <c:if test="${tarifaVigenciaDesconto.chavePrimaria == tarifasVigenciaDesconto}">selected="selected"</c:if>>
										De <fmt:formatDate value="${tarifaVigenciaDesconto.inicioVigencia}" type="both" pattern="dd/MM/yyyy" /> a
										<fmt:formatDate value="${tarifaVigenciaDesconto.fimVigencia}" type="both" pattern="dd/MM/yyyy" />
										</option>	
			    					</c:forEach>	
           					</select>
           					</div>
             		 	</div>
           			</div>
        		 	<div class="col-md-2">
           				<button class="btn btn-primary btn-sm" name="exibir" type="button"
                       	onclick="exibirDetalhamentoTarifaFaixaDesconto();"><i class="fas fa-eye"></i> Exibir </button>
   			 		</div>	
   			 		
				</div><!-- fim da segunda linha -->
				
				<div class="row mt-3">
					<div class="col-md-6">
						<ul class="list-group">
							<li class="list-group-item">
   			 					Tipo de C�lculo: <span class="font-weight-bold" id="descricaoVigenciaTipoCalculo"></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Volume Base para C�lculo: <span class="font-weight-bold" id="descricaoVigenciaVolumeBaseCalculo"></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Unidade Monet�ria: <span class="font-weight-bold" id="descricaoVigenciaUnidadeMonetaria"></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Observa��o: <span class="font-weight-bold" id="observacaoVigencia"></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Status da Vig�ncia: <span class="font-weight-bold" id="statusVigencia"></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Tributos: <span class="font-weight-bold" id="tributos"></span>
   			 				</li>
						</ul>
					</div>
					<div class="col-md-6">
						<ul class="list-group">
   			 				<li class="list-group-item">
   			 					Informar Mensagem: <span class="font-weight-bold" id="habilitado">
   			 					<c:choose>
									<c:when test="${tarifaVigencia.map.informarMsgIcmsSubstituido == 'true'}">Sim</c:when>
									<c:otherwise>N�o</c:otherwise>
								</c:choose>
   			 					</span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Base de C�lculo do ICMS Substitu�do por Metro C�bico: <span class="font-weight-bold" id="baseCalculoIcms">
   			 					<c:out value="${tarifaVigencia.map.baseCalculoIcmsSubstituido}"/>
   			 					</span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					ICMS Substitu�do por Metro C�bico: <span class="font-weight-bold" id="icmsSubstituido">
   			 					<c:out value="${tarifaVigencia.map.icmsSubstituidoMetroCubico}"/>
   			 					</span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Mensagem ICMS Substitu�do: <span class="font-weight-bold" id="icmsSubstituido">
   			 					<c:out value="${tarifaVigencia.map.icmsSubstituidoMetroCubico}"/></span>
   			 				</li>
   			 				<li class="list-group-item">
   			 					Observa��o(Descontos): <span class="font-weight-bold" id="descricaoVigenciaDesconto"></span>
   			 				</li>
						</ul>
					</div>
					
				
				</div><!-- fim da terceira linha -->
				<hr/>
				<h5 class="mt-2 mb-3">Faixas de Tarifas e Descontos</h5>
				<a name="blocoTabelaTarifaDesconto"></a>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover" id="tarifaFaixaDesconto" style="width:100%">
					<thead class="thead-ggas-bootstrap">
						<tr>
							<th scope="col" class="text-center">Faixa(m�)</th>
							<th scope="col" class="text-center">Valor Fixo</th>
							<th scope="col" class="text-center">Desconto Fixo</th>
							<th scope="col" class="text-center">L�quido s/ Imposto Fixo</th>
							<th scope="col" class="text-center">L�quido c/ Imposto Fixo</th>
							<th scope="col" class="text-center">Valor de Compra</th>
							<th scope="col" class="text-center">Margem de Valor Agregado</th>
							<th scope="col" class="text-center">Valor Vari�vel</th>
							<th scope="col" class="text-center">Desconto Vari�vel</th>
							<th scope="col" class="text-center">L�quido s/ Imposto Vari�vel</th>
							<th scope="col" class="text-center">L�quido c/ Imposto Vari�vel</th>
							<th scope="col" class="text-center">Total s/ Imposto</th>
							<th scope="col" class="text-center">Total c/ Imposto</th>
						</tr>
					</thead>
					
					<tbody>
					<c:forEach items="${listaTarifasFaixaDesconto}" var="tarifaFaixaDesconto">
						<tr>
							<td class="text-center">
								<c:out value="${tarifaFaixaDesconto.medidaInicio}"/> -
	        					<c:out value="${tarifaFaixaDesconto.medidaFim}"/> 
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorFixo}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorDescontoFixo}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorLiquidoSemImpostoFixo}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorLiquidoComImpostoFixo}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorCompra}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.margemValorAgregado}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorVariavel}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorDescontoVariavel}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorLiquidoSemImpostoVariavel}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorLiquidoComImpostoVariavel}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorTotalSemImposto}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
							
							<td class="text-center">
								<fmt:formatNumber value="${tarifaFaixaDesconto.valorTotalComImposto}" minFractionDigits="${tarifaFaixaDesconto.qtdeCasasDecimais}" />
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				</div><!-- fim da table responsive -->
			
			</div><!-- fim do card body -->
			
			<div class="card-footer">
        		<div class="row">
        			<div class="col-sm-12">
        				 <button class="btn btn-danger btn-sm float-left ml-1 mt-1" type="button" onclick="voltar();">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                       
                       <vacess:vacess param="exibirAlteracaoTarifa">
                  		<button id="buttonAlterar" class="btn btn-sm btn-primary float-right ml-1 mt-1" 
                  		 type="button" onclick="alterar(<c:out value='${tarifa.chavePrimaria}'/>);">
                       		<i class="fas fa-pencil-alt"></i> Alterar         		
                       	</button>
                       	</vacess:vacess>
        			</div>
        		</div>
        	</div>
		</div><!-- fim do card -->
	
	</form:form>
</div>


