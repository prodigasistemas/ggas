<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<h1 class="tituloInterno">Selecionar Ponto de Consumo<a class="linkHelp" href="<help:help>/associaesdopontodeconsumoemedidorcorretordevazopesquisadospontosdeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar uma Associa��o Tempor�ria de Tarifa ao Ponto de Consumo selecione um cliente clicando em <span class="destaqueOrientacaoInicial">Pesquisar Cliente</span>, ou um im�vel clicando em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span></p>


<script type="text/javascript">

	$(document).ready(function(){

		// Dialog			
		$("#cancelamentoLancamentoPopup").dialog({
			autoOpen: false,
			width: 310,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
	});
	

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
		}
		document.getElementById("botaoPesquisar").disabled = true;	
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

	    document.getElementById("botaoPesquisar").disabled = true;
		
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true&funcaoParametro=\'ativarBotaoPesquisar\'','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	enderecoImovel.value = imovel["enderecoImovel"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
            enderecoImovel.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

		ativarBotaoPesquisar();
	}

	function init() {

			pesquisarImovel(false);
			pesquisarCliente(false);
			ativarBotaoPesquisar();

		<c:choose>

			<c:when test="${indicadorPesquisa == 'indicadorPesquisaCliente'}">
				pesquisarCliente(true);
				pesquisarImovel(false);
				ativarBotaoPesquisar();
			</c:when>

			<c:when test="${indicadorPesquisa == 'indicadorPesquisaImovel'}">
				pesquisarImovel(true);
				pesquisarCliente(false);
				ativarBotaoPesquisar();
			</c:when>			

			<c:when test="${limparDadosClienteImovel}">
				limparCamposPesquisa();
			</c:when>
							
		</c:choose>	

	}	

	function ativarBotaoPesquisar() {
		document.getElementById("botaoPesquisar").disabled = false;
	}

	function exibirInclusaoAssocTempTarifaPontoConsumo(idPontoConsumo){
		document.getElementById('idPontoConsumo').value = idPontoConsumo;
		submeter('tarifaForm', 'exibirIncluisaoAssocTemporariaTarifaPontoConsumoPasso1');
	}

	function pesquisarPontoConsumo(){
		submeter('tarifaForm', 'pesquisarPontosConsumo');
	}
 	
	addLoadEvent(init);
</script>

<form method="post" name="tarifaForm" id="tarifaForm"  action="pesquisarPontosConsumo">
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tarifa.chavePrimaria}">
	<input name="chavePrimariaPontoConsumo" type="hidden" id="idPontoConsumo"/>
	<input name="chavePrimariaCliente" type="hidden" id="idCliente" value="${cliente.chavePrimaria}"/>
	<input name="chavePrimariaImovel" type="hidden" id="idImovel" value="${tarifa.imovel.chavePrimaria}"/>
	
	
	<div id="cancelamentoLancamentoPopup" title="Cancelar Lan�amento">
		<label class="rotulo">Selecione o motivo do cancelamento do lan�amento:</label><br/>
		
		<select name="motivoCancelamento" id="motivoCancelamento" class="campoSelect">
	    	<option value="-1">Selecione</option>				
			<c:forEach items="${listaMotivoCancelamento}" var="motivoCancelamento">
				<option value="<c:out value="${motivoCancelamento.chavePrimaria}"/>" <c:if test="${tarifa.motivoCancelamento.chavePrimaria == motivoCancelamento.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${motivoCancelamento.descricao}"/>
				</option>		
			</c:forEach>
		</select><br class="quebraLinha"/>	
		
		<input type="button" class="bottonRightCol2" value="Ok" onclick="cancelarLancamento();">
	</div>
	
	<fieldset id="pesquisarPontoConsumoAssocTemp" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${indicadorPesquisa == 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="chavePrimariaCliente" value="${cliente.chavePrimaria}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${cliente.nome}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPesquisar"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${indicadorPesquisa == 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="chavePrimariaImovel" type="hidden" id="idImovel" value="${tarifaForm.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${tarifaForm.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${tarifaForm.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${tarifaForm.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${tarifaForm.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${tarifaForm.map.condominio}">
				<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${tarifaForm.map.enderecoImovel}">
				

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Nome Fantasia:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.nomeMunicipio}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio eq true}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio eq false}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset id="conteinerBotoesPesquisarDirPontoConsumoAssocTemp" class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2" type="submit" id="botaoPesquisar" value="Pesquisar" disabled="disabled" onclick="javascript:pesquisarPontoConsumo();">
			<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>
	
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" pagesize="15" requestURI="pesquisarPontosConsumo">
	        <display:column title="Ponto de Consumo"  headerClass="tituloTabelaEsq" class="conteudoTabelaEsq">
	            <a href="javascript:exibirInclusaoAssocTempTarifaPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="Ramo da Atividade" style="width: 300px">
	            <a href="javascript:exibirInclusaoAssocTempTarifaPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.ramoAtividade.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="Segmento" style="width: 150px">
	            <a href="javascript:exibirInclusaoAssocTempTarifaPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.segmento.descricao}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>

</form>
