<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Associar Temporariamente Tarifa ao Ponto de Consumo<a class="linkHelp" href="<help:help>/associaotemporriadetarifasaopontodeconsumo.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>


<script type="text/javascript">

	$(document).ready(function(){

		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', minDate: '+0d'});
				
	 	// Dialog			
		$("#cancelamentoLancamentoPopup").dialog({
			autoOpen: false,
			width: 310,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
		
		
	});

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,persist=0,hide=0');
	
	function carregarTarifa(elem ,idPontoConsumo) {
		
		var codItemFatura = elem.value
		carregarTarifaValor(codItemFatura, undefined, idPontoConsumo);
	}

	function carregarTarifaValor(codItemFatura, tarifaSelecionada, idPontoConsumo){

		var selTarifa = document.getElementById('tarifa');
		selTarifa.length = 0;

		var novaOpcao = new Option("Selecione","-1");
	    selTarifa.options[selTarifa.length] = novaOpcao;		

	    var itemFatura = document.getElementById('idItemFatura');
		
	 	if (itemFatura.options[itemFatura.selectedIndex].value != "-1") {

		 	selTarifa.disabled = false;      
			AjaxService.listarTarifaVigenciaItemFatura(codItemFatura, idPontoConsumo,
	         	function(listaTarifas) {          
	             	for (key in listaTarifas){
	                 	var novaOpcao = new Option(listaTarifas[key], key);
	                 	if (tarifaSelecionada != undefined && tarifaSelecionada == key) {
							novaOpcao.selected = true;
	                 	}
	                 	selTarifa.options[selTarifa.length] = novaOpcao;
	                 }
	             }
	         );
	   	} else {
	   		selTarifa.disabled = true;		
	   	}
	}


	function recarregarTarifa(codItemFatura, idPontoConsumo){

		var selTarifa = document.getElementById('tarifa');
		var idTarifa = "${idTarifa}";
		selTarifa.length = 0;

		var novaOpcao = new Option("Selecione","-1");
	    selTarifa.options[selTarifa.length] = novaOpcao;		

	    var itemFatura = document.getElementById('idItemFatura');
		
	 	if (itemFatura.options[itemFatura.selectedIndex].value != "-1") {

		 	selTarifa.disabled = false;      
			AjaxService.listarTarifaVigenciaItemFatura(codItemFatura, idPontoConsumo, {
            	callback:function(listaTarifas) {            		      		         		
	             	for (key in listaTarifas){
	                 	var novaOpcao = new Option(listaTarifas[key], key);
	                 	selTarifa.options[selTarifa.length] = novaOpcao;
	                 }
	             }, async:false}
	         );

			if (selTarifa.disabled == false) {
	            if (idTarifa != "-1") {
	            	selTarifa.value = idTarifa;
	            }
	        }
	         
	   	} else {
	   		selTarifa.disabled = true;		
	   	}
	}

	function alterarAssocTempTarifaPontoConsumo(idTarifaPontoConsumo,idPontoConsumo,indice,idItemFatura,tarifa,dataInicioVigencia,dataFimVigencia){

		if (indice != "") {

			document.forms[0].indexLista.value = indice;


			if (idItemFatura != "") {
				var tamanho = document.forms[0].idItemFatura.length;
				var opcao = undefined;
				for(var i = 0; i < tamanho; i++) {
					opcao = document.forms[0].idItemFatura.options[i];
					if (idItemFatura == opcao.value) {
						opcao.selected = true;
						break;
					}
				}
			}

			
			if (tarifa != "" && idPontoConsumo != undefined) {
				carregarTarifaValor(idItemFatura, tarifa, idPontoConsumo);
			}
			
 			
			if (dataInicioVigencia != "") {
				document.forms[0].dataInicioVigencia.value = dataInicioVigencia;
			} else {
				document.forms[0].dataInicioVigencia.value = "";
			}
			
			if (dataFimVigencia != "") {
				document.forms[0].dataFimVigencia.value = dataFimVigencia;
			} else {
				document.forms[0].dataFimVigencia.value = "";
			}
			
			var botaoAlterarAssocTempTarifaPontoConsumo = document.getElementById("buttonAlterar");
			var botaoLimparAssocTempTarifaPontoConsumo = document.getElementById("buttonLimpar");	
			var botaoAdicionarAssocTempTarifaPontoConsumo = document.getElementById("botaoAdicionar");
			var botarIncluirAssocTempTarifaPontoConsumo = document.getElementById('buttonIncluir');

			botaoAdicionarAssocTempTarifaPontoConsumo.disabled = true;
			botaoAlterarAssocTempTarifaPontoConsumo.disabled = false;	
			botaoLimparAssocTempTarifaPontoConsumo.disabled = false;
			botarIncluirAssocTempTarifaPontoConsumo.disabled = true;
		}
	}
	
	
	function init() {
		var itemFatura = document.getElementById('idItemFatura').value;
		var idPontoConsumo = document.getElementById('idPontoConsumo').value;
		recarregarTarifa(itemFatura, idPontoConsumo);


		//idItemFatura
		
	<c:choose>
		<c:when test="${indexLista > -1}">
			var botaoAlterarAssocTempTarifaPontoConsumo = document.getElementById("buttonAlterar");
			var botaoLimparAssocTempTarifaPontoConsumo = document.getElementById("buttonLimpar");	
			var botaoAdicionarAssocTempTarifaPontoConsumo = document.getElementById("botaoAdicionar");
			
			botaoAdicionarAssocTempTarifaPontoConsumo.disabled = true;
			botaoAlterarAssocTempTarifaPontoConsumo.disabled = false;	
			botaoLimparAssocTempTarifaPontoConsumo.disabled = false;

			<c:if test="${requestScope.paramAlteracao}">
				limpar();
			</c:if>
			 

			return true;				
		</c:when>

		<c:when test="${indexLista == -1}">
			var botaoAlterarAssocTempTarifaPontoConsumo = document.getElementById("buttonAlterar");
			var botaoLimparAssocTempTarifaPontoConsumo = document.getElementById("buttonLimpar");	
			var botaoAdicionarAssocTempTarifaPontoConsumo = document.getElementById("botaoAdicionar");
			
			
			botaoAdicionarAssocTempTarifaPontoConsumo.disabled = false;
			botaoAlterarAssocTempTarifaPontoConsumo.disabled = true;	
			botaoLimparAssocTempTarifaPontoConsumo.disabled = false;
	
			<c:if test="${sucessoManutencaoLista}">
				limpar();
			</c:if>
			
			return true;				
		</c:when>
	 
	
	</c:choose>		

	
	}

	function adicionarAssocTempTarifaPontoConsumo() {
		submeter("tarifaForm", "adicionarAssocTempTarifaPontoConsumo");
	}

	function removerAssocTempTarifaPontoConsumo(indice) {
		document.forms[0].indexLista.value = indice;
		submeter("tarifaForm", "excluirAssocTemporariaTarifaPontoConsumoItem");		
	}	

	function salvar(){
		submeter("tarifaForm", "InclusaoAssocTempTarifaPontoConsumoPasso1");
	}

	function limpar(){
		
		var selItemFatura = document.getElementById("idItemFatura");
		var selTarifa = document.getElementById("tarifa");
		var dataInicio = document.getElementById("dataInicioVigencia");
		var dataFim = document.getElementById("dataFimVigencia");
		var botaoAlterarAssocTempTarifaPontoConsumo = document.getElementById("buttonAlterar");
		var botaoLimparAssocTempTarifaPontoConsumo = document.getElementById("buttonLimpar");	
		var botaoAdicionarAssocTempTarifaPontoConsumo = document.getElementById("botaoAdicionar");
		
		botaoAdicionarAssocTempTarifaPontoConsumo.disabled = false;
		botaoAlterarAssocTempTarifaPontoConsumo.disabled = true;	
		botaoLimparAssocTempTarifaPontoConsumo.disabled = false;		

		selItemFatura.value = -1;
		selTarifa.value = -1;
		dataInicio.value = "";
		dataFim.value = "";
		document.forms[0].indexLista.value = "-1";
		selTarifa.disabled = true;
	}

	function cancelar(){
		submeter("tarifaForm", "exibirAssocTempTarifaPontoConsumo");
	}

	function alterar(){
		submeter("tarifaForm", "adicionarAssocTempTarifaPontoConsumo");
	}
	
	addLoadEvent(init);

</script>

<form method="post" id="tarifaForm" name="tarifaForm" action="exibirIncluisaoAssocTemporariaTarifaPontoConsumoPasso1">
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tarifa.chavePrimaria}">
	<input name="indexLista" type="hidden" id="indexLista" value="${indexLista}">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${pontoConsumo.chavePrimaria}">
	<input name="idCliente" type="hidden" id="idCliente" value="${cliente.chavePrimaria}">
	
	<input name="nomeCompletoCliente" type="hidden" id="nomeCompletoCliente" value="${cliente.nome}">
	<input name="documentoFormatado" type="hidden" id="documentoFormatado" value="${cliente.numeroCpfCnpj}">	
	<input name="enderecoFormatadoCliente" type="hidden" id="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}">
	<input name="emailCliente" type="hidden" id="emailCliente" value="${cliente.emailPrincipal}">
	 
	<input name="enderecoPontoConsumo" type="hidden" id="enderecoPontoConsumo" value="${pontoConsumo.enderecoFormatado}">
	<input name="descricaoPontoConsumo" type="hidden" id="descricaoPontoConsumo" value="${pontoConsumo.descricao}">	
	<input name="cepPontoConsumo" type="hidden" id="cepPontoConsumo" value="${pontoConsumo.cep.cep}">
	<input name="complementoPontoConsumo" type="hidden" id="complementoPontoConsumo" value="${pontoConsumo.descricaoComplemento}">
	
	<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${pontoConsumo.imovel.nome}"/>
	<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${pontoConsumo.imovel.chavePrimaria}"/>
	<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${pontoConsumo.imovel.quadraFace.endereco.cep.nomeMunicipio}"/>
	<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${pontoConsumo.enderecoFormatado}"/>
	
	 
	 
	<fieldset id="incluirAssociacaoTemporariaTarifaPontoConsumo" class="conteinerPesquisarIncluirAlterar">
		<c:if test="${cliente ne null && cliente.chavePrimaria > 0}">
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDadosDetalhe">
				<fieldset class="coluna">
					<label class="rotulo">Cliente:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.nome}"/></span><br />
					<label class="rotulo">CPF/CNPJ:</label>
					<span class="itemDetalhamento"><c:out value="${cliente.numeroCpfCnpj}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.enderecoPrincipal.enderecoFormatado}"/></span><br />
					<label class="rotulo">E-mail:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${cliente.emailPrincipal}"/></span><br />
				</fieldset>
			</fieldset>
		</c:if>
		
		<c:if test="${pontoConsumo.imovel ne null && pontoConsumo.imovel.chavePrimaria > 0 && cliente eq  null}">
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
				<fieldset class="coluna">
					<label class="rotulo">Descri��o:</label>
					<span class="itemDetalhamento"><c:out value="${pontoConsumo.imovel.nome}"/></span><br />
					<label class="rotulo">Matr�cula:</label>
					<span class="itemDetalhamento"><c:out value="${pontoConsumo.imovel.chavePrimaria}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Cidade:</label>
					<span class="itemDetalhamento"><c:out value="${pontoConsumo.imovel.quadraFace.endereco.cep.nomeMunicipio}"/></span><br />
					<label class="rotulo">Endere�o:</label>
					<span class="itemDetalhamento"><c:out value="${pontoConsumo.imovel.enderecoFormatado}"/></span><br />
				</fieldset>
			</fieldset>
		</c:if>
		
		<hr class="linhaSeparadora1" />
		
		<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo">Descricao:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.descricao}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.enderecoFormatado}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.cep.cep}"/></span><br />
				<label class="rotulo">Complemento:</label>
				<span class="itemDetalhamento"><c:out value="${pontoConsumo.descricaoComplemento}"/></span><br />
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadora1" />		

		<fieldset id="dadosAssociacao" class="conteinerBloco">
			<label class="rotulo campoObrigatorio" for="itemFatura"><span class="campoObrigatorioSimbolo">* </span>Item de Fatura:</label>
			<select name="idItemFatura" id="idItemFatura" class="campoSelect campoHorizontal" onchange="carregarTarifa(this,'<c:out value="${pontoConsumo.chavePrimaria}"/>');");>
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaItemFatura}" var="itemFatura">
					<option value="<c:out value="${itemFatura.chavePrimaria}"/>" <c:if test="${itemFatura.chavePrimaria == idItemFatura}">selected="selected"</c:if>>
						<c:out value="${itemFatura.descricao}"/>
					</option>		
				</c:forEach>
			</select>			
			<label class="rotulo rotuloHorizontal2 campoObrigatorio" for="tarifa"><span class="campoObrigatorioSimbolo">* </span>Tarifa:</label>
			<select name="idTarifa" id="tarifa" class="campoSelect campoHorizontal" >
		    	<option value="-1">Selecione</option>
				<c:forEach items="${listaTarifa}" var="tarifa">
					<option value="<c:out value="${tarifa.chavePrimaria}"/>" <c:if test="${tarifa.chavePrimaria == idTarifa}">selected="selected"</c:if>>
						<c:out value="${tarifa.descricao}"/>
					</option>		
				</c:forEach>
			</select>
			
			
			<label class="rotulo rotuloHorizontal2 campoObrigatorio" for="dataInicioVigencia"><span class="campoObrigatorioSimbolo">* </span>Data Inicial:</label>
			<input class="campoData campoHorizontal" id="dataInicioVigencia" name="dataInicioVigencia" type="text" size="8" value="${dataInicioVigencia}">
			<label class="rotulo rotuloHorizontal2 campoObrigatorio" for="dataFinalAssocTempTarifaPontoConsumo"><span class="campoObrigatorioSimbolo">* </span>Data Final:</label>
			<input class="campoData campoHorizontal" id="dataFimVigencia" name="dataFimVigencia" type="text" size="8" value="${dataFimVigencia}">
			
			<fieldset class="conteinerBotoesDirFixo2">
				<input name="buttonAlterar" id="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterar()" type="button" disabled="disabled">
				<input name="buttonAdicionar" value="Adicionar" class="bottonRightCol2" id="botaoAdicionar" onclick="adicionarAssocTempTarifaPontoConsumo();" type="button">
				<input name="buttonLimpar" id="buttonLimpar" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limpar();">
			</fieldset>
		</fieldset>
				
			<hr class="linhaSeparadora1" />
			<c:set var="i" value="0" />
			<display:table class="dataTableGGAS" name="listaAssocTempTarifaPontoConsumo" sort="list" id="itemAssocTarifaPontoConsumo" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
				<display:column title="Item de Fatura" style="width: 250px">
					<c:out value="${itemAssocTarifaPontoConsumo.tarifa.itemFatura.descricao}"/> 
				</display:column>    
				
				<display:column title="Tarifa" style="width: 250px">
					<c:out value="${itemAssocTarifaPontoConsumo.tarifa.descricao}"/> 
				</display:column>
				
				<display:column title="Data Inicio" style="width: 100px">
					<fmt:formatDate value="${itemAssocTarifaPontoConsumo.dataInicioVigencia}" pattern="dd/MM/yyyy"/>
				</display:column>
				
				<display:column title="Data Fim" style="width: 100px">
					<fmt:formatDate value="${itemAssocTarifaPontoConsumo.dataFimVigencia}" pattern="dd/MM/yyyy"/>	
				</display:column>
				
				<display:column style="text-align: center;"> 
					<a href="javascript:alterarAssocTempTarifaPontoConsumo('${itemAssocTarifaPontoConsumo.chavePrimaria}','${itemAssocTarifaPontoConsumo.pontoConsumo.chavePrimaria}','${i}','${itemAssocTarifaPontoConsumo.tarifa.itemFatura.chavePrimaria}','${itemAssocTarifaPontoConsumo.tarifa.chavePrimaria}','<fmt:formatDate value="${itemAssocTarifaPontoConsumo.dataInicioVigencia}" pattern="dd/MM/yyyy"/>','<fmt:formatDate value="${itemAssocTarifaPontoConsumo.dataFimVigencia}" pattern="dd/MM/yyyy"/>');"><img title="Alterar Associa��o Tempor�ria" alt="Alterar Associa��o Tempor�ria""  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a> 
				</display:column>
				
				<display:column style="width: 4%"> 
					<a onclick="return confirm('Deseja excluir o Item?');" href="javascript:removerAssocTempTarifaPontoConsumo(<c:out value="${i}"/>);"><img title="Excluir Associa��o Tempor�ria" alt="Excluir Associa��o Tempor�ria" src="<c:url value="/imagens/deletar_x.png"/>" border="0"></a> 
				</display:column>
					
				<c:set var="i" value="${i+1}" />
		    </display:table>	

	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonCancelar" value="Cancelar" class="bottonRightCol bottonLeftColUltimo" onclick="cancelar()" type="button">
		<input name="buttonIncluir" id="buttonIncluir" value="Salvar" class="bottonRightCol2 botaoGrande1 botaoSalvar" id="botaoSalvar" onclick="salvar();" type="button">
	</fieldset>
		<token:token></token:token>
</form>
