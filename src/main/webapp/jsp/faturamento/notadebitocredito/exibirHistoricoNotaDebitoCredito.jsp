<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<script>

	$(document).ready(function(){
		
		var isDisabledBotaoAditar = $("input[name='buttonAditar']").prop("disabled");
		var isDisabledBotaoAlterar = $("input[name='buttonAlterar']").prop("enabled");
						
// 		//Desabilita todos os campos input (todos), select e textarea.
// 		$(":input").attr("disabled","disabled");	
// 		$(":input#searchInput").removeAttr("disabled");
		
		//Remove o atributo disabled="disabled" para elementos HTML espec�ficos desta tela.
		$("input[type='hidden'],.conteinerBotoes .bottonRightCol,#chavesPrimariasPontoConsumoVO,#idModeloContrato,#QDCContrato,.ui-dialog button").removeAttr("disabled");
		$("input[name='buttonCancelar']").removeAttr("disabled");
		$("input[name='buttonAvancar']").removeAttr("enabled");
		$("input[name='buttonAlterar']").removeAttr("enabled");
		
		//Esconde elementos espec�ficos da tela.
		$("img.ui-datepicker-trigger,a.apagarItemPesquisa,span.campoObrigatorioSimbolo,input[type='radio'],label.rotuloRadio,#rotuloQDCData,#QDCData,#rotuloQDC,#QDCValor,label.rotuloInformativo,#botaoOKQDC,#botaoReplicarQDC,#botaoExcluirQDC").css("display","none");
		
		//Esconde os bot�es radio e exibe o label correspodente.
		$("input[type='radio']:checked + label.rotuloRadio").each(function(){
			$(this).css({"display":"block","font-size":"14px","padding-top":"5px"});
		});
		
		//Esconde os r�tulos de unidades de medida e tempo e <selects> cujos campos <input> estejam vazios.
		$(":input[value=''] + label.rotuloInformativo,:input[value=''] + select.campoHorizontal").each(function(){
			$(this).css("display","none");
		});

		$(".itemDetalhamento").css("margin-right","5px");
		$(".itemDetalhamento3Linhas").css("margin-right","0");
		
		//Adiciona a classe "campoDesabilitadoDetalhar" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
		$("input[type='text'],select").addClass("campoDesabilitadoDetalhar");
		$("input.searchInput,select").removeClass("campoDesabilitadoDetalhar").addClass("searchInput");
		$("input.campo2Linhas[type='text'],select.campo2Linhas").removeClass("campoDesabilitadoDetalhar").addClass("campoDesabilitado2Linhas");
		$(".campoValorReal.campoDesabilitadoDetalhar,.campoValorReal.campoDesabilitado2Linhas").addClass("campoValorRealDesabilitadoDetalhar");
		
		
		
		// Corrige a classe dos <span class="itemDetalhamento"...> cujos r�tulos t�m de 2 linhas
		$(".campoDesabilitado2Linhas  + span").removeClass("itemDetalhamento").addClass("itemDetalhamento2Linhas");
		
		// Corrige os <span> e <label> cujos r�tulos t�m de 3 linhas
		$("input#tempoAntecRevisaoGarantias + span").removeClass("itemDetalhamento2Linhas").addClass("itemDetalhamento3Linhas")
		$("input#periodicidadeReavGarantias + span").removeClass("itemDetalhamento2Linhas").addClass("itemDetalhamento3Linhas")		
		$(".rotuloInformativo3Linhas").css("margin-top","18px");
		
	});
	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaNotaDebitoCredito"/>';
	}

	function exibirProrrogacaoNota() {
		submeter('notaDebitoCreditoForm', 'exibirProrrogacaoNotaCreditoDebito');	
	}
	
</script>

<h1 class="tituloInterno">Hist�rico das Prorroga��es Nota D�bito Cr�dito<a class="linkHelp" href="<help:help>/detalhamentodoscontratos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form method="post"  id="formDetalharContrato" name="notaDebitoCreditoForm">
	<token:token></token:token>
	<input name="chaveNotaSelecionada" type="hidden" id="chaveNotaSelecionada">
	<input name="telaHistorico" type="hidden" id="telaHistorico" value="true">
	
		
	<fieldset id="conteinerDetalharCliente" class="conteinerBloco">
<!-- 		<h1><strong>Hist�rico D�bito/Cr�dito</strong></h1> -->
		<fieldset>
			<c:if test="${listaHistorico ne null}">
			<hr class="linhaSeparadoraPesquisa" />
			<display:table class="dataTableGGAS" name="listaHistorico"
			sort="list" id="historico"
			decorator="br.com.ggas.web.contratoadequacaoparceria.decorator.ContratoAdequacaoParceriaResultadoPesquisaDecorator"
			pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="exibirHistoricoNotaDebitoCredito">
			   <display:column title="N�mero Documento">
		            <c:out value="${historico.fatura.chavePrimaria}"/>
		        </display:column>
		        
		        <display:column title="Valor (R$)" sortProperty="notaDebitoCredito.valorTotal">
		           <fmt:formatNumber value="${historico.fatura.valorTotal}" maxFractionDigits="2" minFractionDigits="2" />
		        </display:column>
		     
		        <display:column  sortProperty="usuario" title="Usu�rio">
		            <c:out value="${historico.usuario.login}"/>
		        </display:column>
		     
		        <display:column  sortProperty="dataAnterior" title="Data Anterior" >
		           <fmt:formatDate value="${historico.dataAnterior}" pattern="dd/MM/yyyy"/>
 		        </display:column>
		     
		        <display:column title="Prorrogado Para" style="width: 154px" >
		        	<fmt:formatDate value="${historico.dataProrrogacao}" pattern="dd/MM/yyyy"/>
		        </display:column>
		     
		        <display:column title="Observa��o">
		          <c:out value="${historico.observacoes}"/>
		        </display:column>
		   
		    
		    </display:table>
		</c:if>
		</fieldset>	
		
		<fieldset class="conteinerBotoes">
			<input name="buttonVoltar"  class="bottonRightCol2 botaoGrande1" value="Voltar" type="button" onclick="exibirProrrogacaoNota();"/>	
		</fieldset>
	</fieldset>
			
</form> 
			
		