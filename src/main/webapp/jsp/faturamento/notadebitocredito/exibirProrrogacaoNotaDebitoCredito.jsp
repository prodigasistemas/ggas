<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<script>

	$(document).ready(function(){
		
		
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});	 	
		
	});
	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaNotaDebitoCredito"/>';
	}
	
	function prorrogar() {
		submeter('notaDebitoCreditoForm', 'prorrogarNotaCreditoDebito');
	}
	
	function visualizarHistorico(valor){
		if(valor != null){
			document.getElementById("chaveNotaSelecionada").value = valor;
			submeter('notaDebitoCreditoForm', 'exibirHistoricoNotaDebitoCredito');
		}
	}

</script>

<h1 class="tituloInterno">Prorroga��o Nota D�bito Cr�dito<a class="linkHelp" href="<help:help>/detalhamentodoscontratos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form method="post" id="formDetalharContrato" name="notaDebitoCreditoForm">
	<token:token></token:token>
	<input name="chaveNotaSelecionada" type="hidden" id="chaveNotaSelecionada">
	<input name="telaHistorico" type="hidden" id="telaHistorico" value="true">
	
	<fieldset id="conteinerDetalharCliente" class="conteinerBloco" >
		
		<fieldset>
			<c:if test="${listaNotaDebitVos ne null}">
		
			<display:table style="margin-left: 5px;" class="dataTableGGAS" name="listaNotaDebitVos"
			sort="list" id="notaDebitoCredito"
			decorator="br.com.ggas.web.contratoadequacaoparceria.decorator.ContratoAdequacaoParceriaResultadoPesquisaDecorator"
			pagesize="15"
			excludedParams="org.apache.struts.taglib.html.TOKEN acao"
			requestURI="pesquisarContratoAdequacaoParceria">
			 
			 	<display:column title="Cliente" style="width: 150px;">
		   			<c:out value="${notaDebitoCredito.nomeCliente}"/>
				</display:column>
				
			 	<display:column title="Ponto Consumo" >
		   			<c:out value="${notaDebitoCredito.pontoConsumo.descricao}"/>
				</display:column>
			 
			    <display:column title="Tipo Documento">
		            <c:out value="${notaDebitoCredito.tipoDocumento.descricao}"/>
		        </display:column>
		        
		        <display:column  sortProperty="chavePrimaria" title="N� Documento" >
		           <c:out value="${notaDebitoCredito.chavePrimaria}"/>
		        </display:column>
		     
		        <display:column title="Valor (R$)" sortProperty="notaDebitoCredito.valorTotal">
		           <fmt:formatNumber value="${notaDebitoCredito.valorTotal}" maxFractionDigits="2" minFractionDigits="2" />
		        </display:column>
		     
		     	<display:column title="Sit. Documento">
		          <c:out value="${notaDebitoCredito.creditoDebitoSituacao.descricao}"/>
		        </display:column>
		     
		        <display:column title="Sit. Pagamento" >
		         	<c:if test="${notaDebitoCredito.creditoDebitoSituacao.valido}">
		    	       	<c:out value="${notaDebitoCredito.situacaoPagamento.descricao}"/>
		            </c:if>
		        </display:column>
		        
		         <display:column  sortProperty="dataEmissao" title="Data Emiss�o">
		            <fmt:formatDate value="${notaDebitoCredito.dataEmissao}" pattern="dd/MM/yyyy"/>
		        </display:column>
		        
		         <display:column  sortProperty="dataVencimento" title="Data Venc." >
		           <fmt:formatDate value="${notaDebitoCredito.dataVencimento}" pattern="dd/MM/yyyy"/>
 		        </display:column>
		     	
				<display:column style="width: 25px"  title="Hist�rico" >
		        	 <a href="javascript:visualizarHistorico(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>
		        </display:column>	
		    </display:table>
		</c:if>
		</fieldset>	
	<br/>		
		<fieldset  style="width: 300px;" >
	
			<div class="coluna detalhamentoColunaLarga2" style="margin-left:0px; width: 458px;">
				<label class="rotulo" for="dataProrrogacao"><span class="campoObrigatorioSimbolo">* </span>Data Prorroga��o:</label>
				<input tabindex="5" class="campoData campoHorizontal" type="text" id="dataProrrogacao" name="dataProrrogacao" value="${creditoDebitoVO.dataProrrogacao} " />
				
				<label class="rotulo" for="observacaoNota" style="margin-left: 27px;">Observa��es:</label>
			    <textarea  id="observacoesNota"  maxlength="250"  name="observacoesNota" class="campoTexto" rows="3" cols="40">${creditoDebitoVO.observacoesNota}</textarea>
				
				</div>
		</fieldset>
		
		<fieldset class="conteinerBotoes">
			<input name="buttonSalvar"  class="bottonRightCol2 botaoGrande1" value="Salvar" type="button" onclick="prorrogar();"/>	
			<input name="buttonCancelar" class="bottonRightCol2 botaoAlterar" value="Cancelar" type="button" onclick="cancelar();"/>
		</fieldset>
	</fieldset>
			
</form> 
			
		