<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Cancelar Nota de Cr�dito / D�bito<a class="linkHelp" href="<help:help>/cancelandonotadecrditodbito.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Selecione o motivo para o cancelamento da nota de cr�dito / d�bito.</p>


<script type="text/javascript">

	$(document).ready(function(){

		 // Dialog			
		$("#pontoConsumoPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});

		$("select#idMotivoCancelamento").change(function() {
			motivoCancelamento = $(this).val();
			if(motivoCancelamento != -1){
				$("input#buttonCancelar").removeAttr("disabled");
			} else {
				$("input#buttonCancelar").attr("disabled","disabled");
			}
		});
	});

	function voltar(){
		submeter('notaDebitoCreditoForm','voltarParaPesquisarNotaCreditoDebito');
	}
	
	function cancelarNotas() {
		submeter('notaDebitoCreditoForm','cancelarNotaDebitoCredito');
	}

	function manterCheckboSelecionado() {
		<c:forEach items="${creditoDebitoVO.chavesNotasSelecionadas}" var="chaveNota">
			if(document.getElementById('chaveNota'+'<c:out value="${chaveNota}"/>') != undefined){
				document.getElementById('chaveNota'+'<c:out value="${chaveNota}"/>').checked = true;
			}	
		</c:forEach>
	}

	function exibirDadosPontoConsumo(idPontoConsumo) {
		
		var descricao = '';
		var endereco = '';
		var cep = '';
		var complemento = '';
		
		if(idPontoConsumo != "" && parseInt(idPontoConsumo) > 0){
	       	AjaxService.obterPontoConsumoPorChave( idPontoConsumo, { 
	           	callback: function(pontoConsumo) {
					descricao = pontoConsumo['descricao'];
					endereco = pontoConsumo['endereco'];
					cep = pontoConsumo['cep'];
					complemento = pontoConsumo['complemento'];
	       	  }
	       	 , async: false}
	       	);

	       	if (descricao != null) {
			document.getElementById('descricaoPopup').innerHTML = descricao;
	       	}
			if (endereco != null) {
			document.getElementById('enderecoPopup').innerHTML = endereco;
			}
			if (cep != null) {
			document.getElementById('cepPopup').innerHTML = cep;
			}
			if (complemento != null) {
			document.getElementById('complementoPopup').innerHTML = complemento;
			}
	
			exibirJDialog("#pontoConsumoPopup");
		} else {
			alert("Cr�dito ou D�bito selecionado n�o possui ponto de consumo.");
		}
		
	}

	function init() {		

		var idMotivoCancelamento = '${creditoDebitoVO.idMotivoCancelamento}';
		if (idMotivoCancelamento != null && idMotivoCancelamento > 0) {
			$("#buttonCancelar").attr('disabled', false);
		}
		
		manterCheckboSelecionado();
	}
	
	addLoadEvent(init);

</script>

<form method="post" action="cancelarNotaDebitoCredito" name="notaDebitoCreditoForm" id="notaDebitoCreditoForm">
	<token:token></token:token>
	
	<c:forEach items="${creditoDebitoVO.chavesPrimarias}" var="chave">
		<input type="hidden" name="chavesPrimarias" id="chavesPrimarias${chave}" value="${chave}"/>
	</c:forEach>
	
	<div id="pontoConsumoPopup" title="Ponto de Consumo">
		<label class="rotulo">Descri��o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="descricaoPopup"></span><br />
		<label class="rotulo">Endere�o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="enderecoPopup"></span><br />
		<label class="rotulo">Cep:</label>
		<span class="itemDetalhamento" id="cepPopup"></span><br />
		<label class="rotulo">Complemento:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno" id="complementoPopup"></span><br /><br />
	</div>

	<fieldset id="dadosCancelamentoNotaDebitoCredito" class="conteinerPesquisarIncluirAlterar">		
		<label class="rotulo campoObrigatorio" for="idMotivoCancelamento"><span class="campoObrigatorioSimbolo">* </span>Motivo do Cancelamento:</label>
			<select tabindex="1" name="idMotivoCancelamento" id="idMotivoCancelamento" class="campoSelect" >
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMotivoCancelamento}" var="motivoCancelamento">
					<option value="<c:out value="${motivoCancelamento.chavePrimaria}"/>" <c:if test="${creditoDebitoVO.idMotivoCancelamento == motivoCancelamento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${motivoCancelamento.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br/>
			
			<label class="rotulo" for="descricaoCancelamento">Complemento:</label>
		    <textarea tabindex="2" id="descricaoCancelamento" name="descricaoCancelamento" class="campoTexto" rows="3" cols="40" onkeypress="return formatarCampoTextoComLimite(event,this,50);">${creditoDebitoVO.descricaoCancelamento}</textarea>
			
			<hr class="linhaSeparadoraDetalhamento" />		
	</fieldset>
		
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaNotaDebitoCredito" sort="list" id="notaDebitoCredito" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	        <display:column style="width: 25px" title="<input tabindex='3' type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" tabindex="4" id="chaveNota${notaDebitoCredito.chavePrimaria}" name="chavesNotasSelecionadas" value="${notaDebitoCredito.chavePrimaria}" >
	     	</display:column>
	        <display:column title="N�mero do<br />Documento">
            	<c:out value="${notaDebitoCredito.chavePrimaria}"/>
	        </display:column>
	        <display:column title="Valor (R$)" style="width: 200px; text-align: right; padding-right: 10px">
            	<fmt:formatNumber value="${notaDebitoCredito.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
	        </display:column>
	        <display:column title="Data de<br/> Emiss�o" style="width: 80px">
            	<fmt:formatDate value="${notaDebitoCredito.dataEmissao}" pattern="dd/MM/yyyy"/>
	        </display:column>
	        <display:column title="Vencimento" style="width: 80px">
            	<fmt:formatDate value="${notaDebitoCredito.dataVencimento}" pattern="dd/MM/yyyy"/>
           	</display:column>
	        <display:column title="Tipo de<br/> Documento" style="width: 220px">
            	<c:out value="${notaDebitoCredito.tipoDocumento.descricao}"/>
	        </display:column>
	   		<display:column title="Ponto de<br/> Consumo" style="width: 70px">
				<a href="javascript:exibirDadosPontoConsumo(<c:out value='${notaDebitoCredito.pontoConsumo.chavePrimaria}'/>);"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
			</display:column>	
	    </display:table>
	
	<fieldset class="conteinerBotoes">
		<input tabindex="5" name="buttonVoltar" value="Voltar" class="bottonRightCol" onclick="voltar();" type="button">
		<input tabindex="6" name="buttonCancelar" value="Cancelar Notas" class="bottonRightCol2 botaoGrande1 bottonLeftColUltimo botaoCancelarNotas" id="buttonCancelar" onclick="cancelarNotas();" type="button" disabled="disabled">
	</fieldset>			
</form>
