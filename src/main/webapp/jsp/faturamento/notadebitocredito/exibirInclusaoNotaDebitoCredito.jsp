<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<h1 class="tituloInterno">Incluir Nota de Cr�dito / D�bito<a class="linkHelp" href="<help:help>/inclusodenotasdecrditodbito.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script> 

	$(document).ready(function(){
		// Comportamento e m�scara do campo de data Vencimento.
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', minDate: '+0d'});

		if(document.getElementById('indicadorDebitoSim').checked){
			$("#conteinerVencimentoNota").slideDown();
			selecionarCreditoDebito("debito");
		}
		
		if (document.getElementById('indicadorCreditoPenalidade').checked){
			selecionarCreditoDebito("creditoPenalidade");
			$("#indicadorCreditoPenalidade").attr("disabled", false);
			$("#quantidade").addClass("campoDesabilitado");
			$("#valorTotal").addClass("campoDesabilitado");
		}
		
		// Exibe e esconde o conteiner do campo Vencimento.
		$("input#tipoNotaDebito").click(function () {
			$("#conteinerVencimentoNota").slideDown();
		});		
		$("input#tipoNotaCredito").click(function () { 
			$("#conteinerVencimentoNota").slideUp();
		})
	});
	
	// Esconde o conteiner do campo Vencimento quando carrega a tela.
	animatedcollapse.addDiv('conteinerVencimentoNota', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,hide=0');

	function limparFormulario(){
		$("input[name=idPontoConsumo]").attr('checked', false);
		document.getElementById('indicadorDebitoSim').checked = false;
		document.getElementById('indicadorDebitoNao').checked = false;
		document.getElementById('quantidade').value = "";
		document.getElementById('valorTotal').value = "";
		document.getElementById('valor').value = "";
		document.getElementById('observacaoNota').value = "";
		document.getElementById('dataVencimento').value = "";
		var selectRubrica = document.getElementById('idRubrica');
		selectRubrica.length = 0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectRubrica.options[selectRubrica.length] = novaOpcao;
		$("#conteinerVencimentoNota").slideUp();
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaNotaDebitoCredito"/>';
	}
	
	function validarTextArea(e, elem, limiteCaracteres){
		var estouroLimite = false;
		var reDigits = /[A-Za-z�-��-�0-9\s\-\b\.\,\\\!\@\#\$\%\&\*\(\)\{\}\[\]\;\<\>\/\+\-\=\_]+$/;		
		var whichCode = (window.event) ? e.keyCode : e.which;
		key = String.fromCharCode(whichCode);
		
		if(elem.value.length == limiteCaracteres && validarPadrao(/[A-Za-z�-��-�0-9\s\-\b\.\,\\\!\@\#\$\%\&\*\(\)\{\}\[\]\;\<\>\/\+\-\=\_]+$/,key)){
			estouroLimite = true;		
		}
			
	    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroLimite;	
	}
	
	function exibirPenalidadesPontoConsumo(chavePontoConsumo){
// 		document.forms[0].chavePrimariaContratoMigrar = component; 
		submeter('notaDebitoCreditoForm', 'exibirPenalidadesPontoConsumo');
	}

	function selecionarCreditoDebito(tipo) {

		$("#valorTotal").val("");
// 		$("#valorTotal").attr("disabled", false);
		
		var selectRubrica = document.getElementById('idRubrica');
		var idRubrica = '${creditoDebitoVO.idRubrica}';

		selectRubrica.length = 0;
      	var novaOpcao = new Option("Selecione","-1");
      	selectRubrica.options[selectRubrica.length] = novaOpcao;
			
		if (tipo != null && tipo == "debito") {
			$("#conteinerVencimentoNota").slideDown();
			$("#quantidade").attr("disabled", false);
			$("#valorTotal").attr("disabled", false);

			AjaxService.listarRubricasPorTipoCreditoDebito(true, { 
	           	callback: function(listaRubrica) {
	           		for (key in listaRubrica) {
		           		var novaRubrica = new Option(listaRubrica[key], key);
						if (idRubrica != null && idRubrica == key) {
							novaRubrica.selected = true;
			           	}
		           		selectRubrica.options[selectRubrica.length] = novaRubrica;
					}
				}, async: false }
	       	);
		} else if (tipo != null && tipo == "credito" || tipo == "creditoPenalidade") {
			$('#conteinerVencimentoNota').slideUp();
			
			document.getElementById('dataVencimento').value = "";

			AjaxService.listarRubricasPorTipoCreditoDebito(false, {
	           	callback: function(listaRubrica) {
	           		for (key in listaRubrica) {
		           		var novaRubrica = new Option(listaRubrica[key], key);
		           		if (idRubrica != null && idRubrica == key) {
							novaRubrica.selected = true;
			           	}
		           		selectRubrica.options[selectRubrica.length] = novaRubrica;
					}
				}, async:false }
	       	);
		}
		if (tipo == "creditoPenalidade"){
// 			$("#idProposta").removeClass("campoDesabilitado");
			$("#quantidade").addClass("campoDesabilitado");
			$("#valorTotal").addClass("campoDesabilitado");
// 			$("#quantidade").attr("disabled", true);
// 			$("#valorTotal").attr("disabled", true);
			
		}
		if (tipo == "credito"){
			$("#quantidade").removeClass("campoDesabilitado");
			$("#valorTotal").removeClass("campoDesabilitado");
		}
		
		
	}
	
	function habilitarCreditoPenalidade(){
		$("#indicadorCreditoPenalidade").attr("disabled", false);
	}

	function exibirNotas() {
		atualizarValorTotalizado();
		
		if (!document.getElementById('valorTotal').disabled) {
			document.getElementById('valor').value = document.getElementById('valorTotal').value;
		}
		submeter('notaDebitoCreditoForm', 'exibirNotasDebitoCredito');
	}

	function carregarCampos(elem) {
		var valorTotal = document.getElementById('valorTotal');
		var valorHidden = document.getElementById('valor');

		var indicadorAlteracaoValor;
		var indicadorPermiteDescricao;
		
		if (elem.value != -1) {
			AjaxService.obterValorRubrica(elem.value, {
	           	callback: function(rubrica) {
					var valorReferencia = rubrica['valor'];
					if(valorReferencia != undefined && valorReferencia != ""){		           		
	           			valorTotal.value = valorReferencia;
	           			aplicarMascaraNumeroDecimal(valorTotal, 4);
	           		}else{		           		
	           			valorTotal.value = "";
	           		}
					
					indicadorAlteracaoValor = rubrica['indicadorAlteracaoValor'];        				

        			if(indicadorAlteracaoValor == 'true') {
        				valorTotal.disabled = false;
        	        } else {     
        	        	valorHidden.value = rubrica['valor'];   	
        	        	valorTotal.disabled = true;
        	        }

        			atualizarValorTotalizado();

        	        indicadorPermiteDescricao = rubrica['indicadorDescricaoComplementar'];
        	        if (indicadorPermiteDescricao != null && indicadorPermiteDescricao == 'true') {
        	        	$('#observacaoNota').attr('disabled', false);
            	    } else if (indicadorPermiteDescricao != null && indicadorPermiteDescricao == 'false') {
            	    	$('#observacaoNota').attr('disabled', true);
                	}
	           		
	            }, async:false}
	        );
		} else {
			valorTotal.disabled = false;
			valorTotal.value = "";
			valorHidden.value = "";
		}
		
	}

	function init() {
		var indicadorCreditoDebito = "${creditoDebitoVO.indicadorCreditoDebito}";
		if (indicadorCreditoDebito != null && indicadorCreditoDebito == "debito") {
			selecionarCreditoDebito("debito");

			<c:forEach items="${creditoDebitoVO.chavesPrimarias}" var="chaveNota">
				var inputCredito = document.getElementById('chaveCredito'+'<c:out value="${chaveNota}"/>');
				if(inputCredito != undefined){
					inputCredito.checked = true;
					var inputValorConciliado = document.getElementById('creditoConciliado'+'<c:out value="${chaveNota}"/>')
					if (inputValorConciliado != undefined) {
						atualizarValorNotaDebitoCredito(inputCredito, inputValorConciliado.value);
					}
				}	
			</c:forEach>
		} else if (indicadorCreditoDebito != null && indicadorCreditoDebito == "credito") {
			selecionarCreditoDebito("credito");

			<c:forEach items="${creditoDebitoVO.chavesPrimarias}" var="chaveNota">
				var inputDebito = document.getElementById('chaveDebito'+'<c:out value="${chaveNota}"/>');
				if(inputDebito != undefined){
					inputDebito.checked = true;
					var inputValorConciliado = document.getElementById('debitoConciliado'+'<c:out value="${chaveNota}"/>')
					if (inputValorConciliado != undefined) {
						atualizarValorNotaDebitoCredito(inputDebito, inputValorConciliado.value);
					}
				}	
			</c:forEach>
		}

		var idRubrica = document.getElementById('idRubrica');
		if (idRubrica != null) {
			carregarCampos(idRubrica);
		}

		var valorTotal = "${creditoDebitoVO.valorTotal}";
		if (valorTotal != null) {
			document.getElementById('valorTotal').value = valorTotal;
		}

		atualizarValorTotalizado();
	}
	
	addLoadEvent(init);
	
	function atualizarValorDebitos(valor, checked) {
		var inputValorCreditoDebito = document.getElementById('valorCreditoDebito');
		if (inputValorCreditoDebito.value == "") {
			inputValorCreditoDebito.value = 0;
		}
		var valorDebitos = parseFloat(inputValorCreditoDebito.value);
		if (checked) {
			valorDebitos = valorDebitos + parseFloat(valor);
		} else {
			valorDebitos = valorDebitos - parseFloat(valor);
		}
		inputValorCreditoDebito.value = valorDebitos.toFixed(2);
		document.getElementById('creditoDebito').innerHTML = 'R$ ' + parseFloat(inputValorCreditoDebito.value).toFixed(2);

	}

	function atualizarValorNotaDebitoCredito(obj, paramValor) {
		paramValor = paramValor.replace(",",".");
		atualizarValorDebitos(paramValor, obj.checked);
	}

	function incluir() {
		
		
		atualizarValorTotalizado();
		
		if (!document.getElementById('valorTotal').disabled) {
			document.getElementById('valor').value = document.getElementById('valorTotal').value;
		}
		
		if(document.getElementById('indicadorDebitoSim').checked == true && document.getElementById('idRubrica').value!=null && document.getElementById('idRubrica').value!="-1" && document.getElementById('dataVencimento').value!=null && document.getElementById('dataVencimento').value!="" && document.getElementById('valorTotal').value!=null && document.getElementById('quantidade').value!=null && document.getElementById('quantidade').value!="") {
			AjaxService.listarRubricasPorTipoCreditoDebito(true, { 
	           	callback: function(mapa) {
	           		if(Object.keys(mapa).length > 0) {
	           			var retorno = confirm('<fmt:message key="PERGUNTA_ALIQUOTA_NAO_CADASTRADA"/>');
	        			if(retorno == true) {
	        				submeter('notaDebitoCreditoForm','incluirNotaDebitoCredito');
	        			}
		           	}
				}, async: false }
	       	);
		} else {
			submeter('notaDebitoCreditoForm','incluirNotaDebitoCredito');
			}
		
	}
	
function atualizarValorTotal(obj, paramValorQPNR) {
		

	
		var valorFloatQPNR = converterStringParaFloat(paramValorQPNR);
		
		var inputValorCreditosQPNR = document.getElementById("saldoQPNR");
		
		if (inputValorCreditosQPNR.value == "") {
			inputValorCreditosQPNR.value = 0;
		}
		
		var valorCreditosQPNR = parseFloat(inputValorCreditosQPNR.value);
		var valorTotalQPNR = 0;
		if (obj.checked) {
			
			valorCreditosQPNR = valorCreditosQPNR + valorFloatQPNR;
			valorTotalQPNR = valorCreditosQPNR;
			atualizarValores(valorTotalQPNR);
			
			
		}else{
			valorCreditosQPNR = valorCreditosQPNR - valorFloatQPNR;
			valorTotalQPNR = valorCreditosQPNR;
			atualizarValores(valorTotalQPNR);
		}
		
	}
	
function atualizarValores(valorTotalQPNR){
	

	document.getElementById('saldoQPNR').value = parseFloat(valorTotalQPNR).toFixed(4);
	
}

	function atualizarValorTotalizado() {
		
		var quantidade = $("#quantidade").val();
		var valorTotal = $("#valorTotal").val();
		

		if (quantidade != "" && valorTotal != "") {
			//valorTotal = converterStringParaFloat(valorTotal).toFixed(2);

			//var valorTotalizado = valorTotal*quantidade;
			     //alert(valorTotal + " X " + quantidade + " = " + valorTotalizado);
			//valorTotalizado = valorTotalizado.toString().replace(".",",");
			//valorTotalizado = formatarValorDecimalVariavel(valorTotalizado,2);
			
			AjaxService.obterMultiplicacaoValores(quantidade, valorTotal, 
		           	function(listaValorRetorno) {
		           		for (key in listaValorRetorno) {
							valorRetorno = listaValorRetorno[key];

							var valorInteiroRetorno = valorRetorno;
							valorInteiroRetorno = Math.floor((valorInteiroRetorno*10000+0.5)/10000) + '';
							if (valorInteiroRetorno.length > 12) {
								alert('O Valor Totalizado � superior ao suportado.');
								document.getElementById('botaoIncluir').disabled = true;
								$("#valorTotalizado").html("R$ 0,00");
								return false;
							} else {
								var valorTotalizado = formataMoeda(valorRetorno);
								$("#valorTotalizado").html("R$ " + valorTotalizado);
								
								
								document.getElementById("botaoIncluir").disabled = false;
								
							}
 						}
					}
	        	);
		} else {
			$("#valorTotalizado").html("R$ 0,00");
		}
	}

	function formataMoeda(num) {
		  
		   x = 0;

		   if(num<0) {
		      num = Math.abs(num);
		      x = 1;
		   }   

		   if(isNaN(num)) {
			   num = "0";
		   }

		   cents = Math.floor((num*10000+0.5)%10000);

		   num = Math.floor((num*10000+0.5)/10000).toString();
		
		   if(cents < 10) {
			   cents = "0" + cents;
		   } else if(cents < 100) {
			   cents = "" + cents;
		   } else if(cents < 1000) {
			   cents = "" + cents;
		   }
		   
	       for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	         num = num.substring(0,num.length-(4*i+3))+'.'+num.substring(num.length-(4*i+3));   
			
        ret = num + ',' + cents.toString().substring(0,2);   

        if (x == 1) 
            ret = ' - ' + ret;

        return ret;
	}

	function calcularVolume(){
		submeter('notaDebitoCreditoForm', 'calcularValorVolumePenalidade');
	}
</script>

<form method="post" action="incluirNotaDebitoCredito" id="notaDebitoCreditoForm" name="notaDebitoCreditoForm">
	<token:token></token:token>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input type="hidden" id="idImovel" name="idImovel" value="${creditoDebitoVO.idImovel}" />
	<input type="hidden" id="idCliente" name="idCliente" value="${creditoDebitoVO.idCliente}" />
	<input type="hidden" name="indicadorPesquisa" value="${creditoDebitoVO.indicadorPesquisa}" />
	<input type="hidden" id="integracaoBaixa" value="${sessionScope.integracaoBaixa}" />

	<fieldset id="incluirNotaDebitoCredito" class="conteinerPesquisarIncluirAlterar conteinerNotaDebitoCredito">
		<c:if test="${creditoDebitoVO.idCliente ne null && creditoDebitoVO.idCliente > 0}">
			<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Nome:</label>
					<input type="hidden" name="nomeCompletoCliente" value="${creditoDebitoVO.nomeCompletoCliente}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.nomeCompletoCliente}"/></span><br />
					<label class="rotulo">CPF/CNPJ:</label>
					<input type="hidden" name="documentoFormatado" value="${creditoDebitoVO.documentoFormatado}">
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.documentoFormatado}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Endere�o:</label>
					<input type="hidden" name="enderecoFormatadoCliente" value="${creditoDebitoVO.enderecoFormatadoCliente}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoFormatadoCliente}"/></span><br />
					<label class="rotulo">E-mail:</label>
					<input type="hidden" name="emailCliente" value="${creditoDebitoVO.emailCliente}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.emailCliente}"/></span><br />
				</fieldset>
			</fieldset>
		</c:if>
		
		<c:if test="${creditoDebitoVO.idImovel ne null && creditoDebitoVO.idImovel > 0}">
			<a id="linkDadosImovel" class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosImovel" class="conteinerDados conteinerBloco">
				<fieldset class="coluna">
					<label class="rotulo">Descri��o:</label>
					<input type="hidden" name="nomeFantasiaImovel" value="${creditoDebitoVO.nomeFantasiaImovel}">
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.nomeFantasiaImovel}"/></span><br />
					<label class="rotulo">Matr�cula:</label>
					<input type="hidden" name="matriculaImovel" value="${creditoDebitoVO.matriculaImovel}">
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.matriculaImovel}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Cidade:</label>
					<input type="hidden" name="cidadeImovel" value="${creditoDebitoVO.cidadeImovel}">
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.cidadeImovel}"/></span><br />
					<label class="rotulo">Endere�o:</label>
					<input type="hidden" name="enderecoImovel" value="${creditoDebitoVO.enderecoImovel}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoImovel}"/></span><br />
				</fieldset>
			</fieldset><br/>
		</c:if>

		<c:if test="${listaPontoConsumo ne null}">
			<a id="linkDadosPontoConsumo" class="linkExibirDetalhesSeguinte" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Im�veis e Pontos de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosPontoConsumo" class="conteinerBloco">
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
					<display:column style="text-align: center; width: 25px" sortable="false">
				   		<input type="radio" name="idPontoConsumo" id="idPontoConsumo${pontoConsumo.chavePrimaria}" value="${pontoConsumo.chavePrimaria}" <c:if test="${creditoDebitoVO.idPontoConsumo == pontoConsumo.chavePrimaria}">checked="checked"</c:if> onclick="habilitarCreditoPenalidade();"/>
				   	</display:column>
				   	<display:column title="Im�vel - Ponto Consumo" headerClass="tituloTabelaEsq" style="text-align: left; padding-left: 10px">
						<c:out value="${pontoConsumo.imovel.nome}"/> - <c:out value="${pontoConsumo.descricao}"/>							
				   	</display:column>
				   	<display:column property="segmento.descricao" title="Segmento" style="width: 170px"/>    
				   	<display:column property="situacaoConsumo.descricao" title="Situa��o" style="width: 250px"/>
				   	<display:column title="C�digo do Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: Center; padding-left: 10px">
						<c:out value="${pontoConsumo.codigoLegado}"/>							
				   	</display:column>    
			    </display:table>	
			</fieldset>
		</c:if>
		
		<hr class="linhaSeparadoraDetalhamento" />
		
		<fieldset id="dadosNotaCreditoDebito" class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Dados da Nota</legend>
			<fieldset class="coluna">
				<fieldset>
					<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Tipo de Nota:</label>
					
					<input class="campoRadio" type="radio" id="indicadorDebitoSim" name="indicadorCreditoDebito" value="debito" <c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'debito'}">checked="checked" </c:if> onclick="selecionarCreditoDebito(this.value);">
					<label class="rotuloRadio" for="indicadorDebitoSim">D�bito</label>
					<input class="campoRadio" type="radio" id="indicadorDebitoNao" name="indicadorCreditoDebito" value="credito" <c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'credito'}">checked="checked" </c:if> onclick="selecionarCreditoDebito(this.value);">
					<label class="rotuloRadio" for="indicadorDebitoNao">Cr�dito</label>
						 
					<input class="campoRadio" type="radio" id="indicadorCreditoPenalidade" disabled="disabled" name="indicadorCreditoDebito" value="creditoPenalidade" <c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'creditoPenalidade'}">checked="checked" </c:if> onclick="selecionarCreditoDebito(this.value);exibirPenalidadesPontoConsumo(${pontoConsumo.chavePrimaria});">
					<label class="rotuloRadio" for="indicadorDebitoNao">Cr�dito Penalidade</label>	
				    <fieldset id="conteinerVencimentoNota">
						<label class="rotulo campoObrigatorio" for="dataVencimento"><span class="campoObrigatorioSimbolo">* </span>Vencimento:</label>
						<input class="campoData" type="text" id="dataVencimento" name="dataVencimento" maxlength="10" value="${creditoDebitoVO.dataVencimento}">
				    </fieldset>
			    </fieldset>
			    
				<label class="rotulo campoObrigatorio" for="descricaoAbreviada"><span class="campoObrigatorioSimbolo">* </span>Rubrica:</label>
				<select name="idRubrica" id="idRubrica" class="campoSelect" onchange="carregarCampos(this);">
			    	<option value="-1">Selecione</option>
			    	<c:forEach items="${listaRubrica}" var="rubrica">
						<option value="<c:out value="${rubrica.chavePrimaria}"/>" <c:if test="${creditoDebitoVO.idRubrica == rubrica.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${rubrica.descricao}"/>
						</option>		
				    </c:forEach>				
			    </select><br />
			    
				<label class="rotulo campoObrigatorio" for="descricaoAcao"><span class="campoObrigatorioSimbolo">* </span>Quantidade:</label>
				<input class="campoTexto" type="text" id="quantidade" name="quantidade" maxlength="20" size="22"  onkeypress="return formatarCampoDecimalPositivo(event, this, 15, 4);" value="${creditoDebitoVO.quantidade}" onchange="atualizarValorTotalizado();"><br />
				
				<label class="rotulo campoObrigatorio" for="valorTotal"><span class="campoObrigatorioSimbolo">* </span>Valor Unit�rio:</label>
				<input type="hidden" name="valor" id="valor" value="${creditoDebitoVO.valor}"/>
				<input class="campoTexto " type="text" id="valorTotal" name="valorTotal" maxlength="19" size="20" onkeypress="return formatarCampoDecimalPositivo(event,this,14,2)" onblur="aplicarMascaraNumeroDecimal(this,2);" onchange="atualizarValorTotalizado();" value="${creditoDebitoVO.valorTotal}"><br/>
			</fieldset>
			
			<fieldset class="colunaFinal">
			
				<label class="rotulo">Valor:</label>
				<span id="valorTotalizado" class="itemDetalhamento">R$</span><br class="quebraLinha"/>
			
			    <label class="rotulo" for="observacaoNota">Descri��o:</label>
			    <textarea id="observacaoNota" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" name="observacaoNota" class="campoTexto" rows="3" cols="40" onkeypress="return validarTextArea(event,this,${tamanhoDescricao});" disabled="disabled">${creditoDebitoVO.observacaoNota}</textarea>
			</fieldset>
			
			<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Nota de D�bito/Cr�dito.</p>
			
			<fieldset class="conteinerBloco">
			<c:if test="${listaApuracaoPenalidade ne null}">
			<hr class="linhaSeparadoraDetalhamento" />
			<fieldset class="conteinerBloco">
			
			
			<display:table class="dataTableGGAS" name="sessionScope.listaApuracaoPenalidade" sort="list" id="apuracaoPenalidade" excludedParams="" requestURI="#" >
			
			<c:set var="selecionado" value="false" />
			<display:column headerClass="headerCheckbox"
					style="text-align: center; width: 25px" sortable="false"
					>
					<c:forEach items="${creditoDebitoVO.idsApuracaoPenalidade}"
						var="idApuracaoPenalidade" >
						<c:if test="${idApuracaoPenalidade eq apuracaoPenalidade.chavePrimaria }">
							<c:set var="selecionado" value="true" />
						</c:if>
					</c:forEach>
					
					<input type="hidden" value="${apuracaoPenalidade.qtdaPagaNaoRetirada}" name="valorQPNR" id="valorQPNR">
					<input type="hidden" name="idsApuracaoPenalidade" id="idsApuracaoPenalidade" 
						value="${apuracaoPenalidade.chavePrimaria}"
						onclick="atualizarValorTotal(this, '<c:out value="${apuracaoPenalidade.qtdaPagaNaoRetirada}"/>');"  
						<c:if test="${selecionado eq true}">
				 			checked
						</c:if>/>
					</display:column>	
						
			     	<display:column title="Volume Pendente de NC (m�)" style="width: 150px">
						<c:out value="${apuracaoPenalidade.qtdaPagaNaoRetirada}"/>							
				   	</display:column>
			     	<display:column title="Penalidade Periodicidade" style="width: 150px">
						<c:out value="${apuracaoPenalidade.periodicidadePenalidade.descricao}"/>							
				   	</display:column>
			     	<display:column style="width: 150px" title="Data In�cio Apura��o">
					   	<fmt:formatDate value="${apuracaoPenalidade.dataInicioApuracao}" pattern="dd/MM/yyyy"/>
			   		</display:column>
			   		<display:column style="width: 150px" title="Data Fim Apura��o">
					   	<fmt:formatDate value="${apuracaoPenalidade.dataFimApuracao}" pattern="dd/MM/yyyy"/>
			   		</display:column>
			   		<display:column title="Volume Recuper�vel (m�)" style="width: 150px">
								<input  type="text" id= "volumeRecuperacao" name="volumeRecuperacao" <c:out value="${creditoDebitoVO.volumeRecuperacao}"/> />			
				   	</display:column>
			</display:table>
			
			<label class="rotulo">Total do Volume Recuper�vel:</label>
				<input class="campoTexto campoDesabilitado" type="text" id="saldoQPNR" readonly="readonly" name="saldoQPNR"  value="${creditoDebitoVO.saldoQPNR}" ><br />
				
			</fieldset>
			<input name="Button" class="bottonRightCol" value="Calcular" type="button" onclick="calcularVolume();">
			</c:if>
		</fieldset>
			<fieldset class="conteinerBotoesPesquisarDirFixo">
				<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
			</fieldset>
		</fieldset>
		
		<fieldset class="conteinerBloco">
			<c:if test="${listaNotaCredito ne null}">
			<hr class="linhaSeparadoraDetalhamento" />
			<fieldset class="conteinerBloco">
				<legend class="conteinerBlocoTitulo">Notas de Cr�dito:</legend>
				<display:table class="dataTableGGAS" name="listaNotaCredito" sort="list" id="notaCredito" excludedParams="" requestURI="#" >
					<display:column style="width: 25px"> 
			      		<input type="checkbox" name="chavesPrimarias" id="chaveCredito${notaCredito.chavePrimaria}" value="${notaCredito.chavePrimaria}" onclick="atualizarValorNotaDebitoCredito(this, '<c:out value="${notaCredito.valorConciliado}"/>');">
			     	</display:column>
					<display:column property="chavePrimaria" title="N� do Documento" />
			   		<display:column style="width: 150px; text-align: right" title="Valor (R$)" >
			   			<fmt:formatNumber value="${notaCredito.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
			   		</display:column>
			   		<display:column style="width: 150px; text-align: right" title="Saldo (R$)" >
			   			<input type="hidden" id="creditoConciliado${notaCredito.chavePrimaria}" value="<fmt:formatNumber value='${notaCredito.valorConciliado}' maxFractionDigits='2' minFractionDigits='2'/>"></input>
			   			<fmt:formatNumber value="${notaCredito.valorConciliado}" maxFractionDigits="2" minFractionDigits="2"/>
			   		</display:column>
			   		<display:column style="width: 150px" title="Data de Emiss�o">
					   	<fmt:formatDate value="${notaCredito.dataEmissao}" pattern="dd/MM/yyyy"/>
			   		</display:column>
			   	</display:table>
			   	<input type="hidden" id="valorCreditoDebito">
			   	<label class="rotulo">Valor total dos cr�ditos:</label>
				<span id="creditoDebito" class="itemDetalhamento">R$ <c:out value="000.00"/></span><br />
			</fieldset>
			</c:if>
			<c:if test="${listaNotaDebito ne null}">
			<hr class="linhaSeparadoraDetalhamento" />
				<fieldset class="conteinerBloco">
					<legend class="conteinerBlocoTitulo">Documentos:</legend>
					<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaNotaDebito" sort="list" id="notaDebito" excludedParams="" requestURI="#" >
						<display:column style="width: 25px" > 
				      		<input type="checkbox" name="chavesPrimarias" id="chaveDebito${notaDebito.chavePrimaria}" value="${notaDebito.chavePrimaria}" onclick="atualizarValorNotaDebitoCredito(this, '<c:out value="${notaDebito.valorConciliado}"/>');">
				     	</display:column>
						<display:column property="chavePrimaria" title="N� do<br />Documento" />
				   		<display:column style="width: 150px; text-align: right" title="Valor (R$)">
						   	<fmt:formatNumber value="${notaDebito.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
				   		</display:column>
				   		<display:column style="width: 150px; text-align: right" title="Saldo (R$)">
						   	<input type="hidden" id="debitoConciliado${notaDebito.chavePrimaria}" value="<fmt:formatNumber value='${notaDebito.valorConciliado}' maxFractionDigits='2' minFractionDigits='2'/>"></input>
						   	<fmt:formatNumber value="${notaDebito.valorConciliado}" maxFractionDigits="2" minFractionDigits="2"/>
				   		</display:column>
				   		<display:column style="width: 150px" title="Data de<br/>Emiss�o">
						   	<fmt:formatDate value="${notaDebito.dataEmissao}" pattern="dd/MM/yyyy"/>
				   		</display:column>
				   		<display:column style="width: 150px" title="Vencimento">
						   	<fmt:formatDate value="${notaDebito.dataVencimento}" pattern="dd/MM/yyyy"/>
				   		</display:column>
				   	</display:table>
				   	<input type="hidden" id="valorCreditoDebito">
				   	<label class="rotulo">Valor total dos d�bitos:</label>
					<span id="creditoDebito" class="itemDetalhamento">R$ <c:out value="000.00"/></span><br />
				</fieldset>
			</c:if>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
	    <vacess:vacess param="incluirNotaDebitoCredito">
	    	<input name="Button" class="bottonRightCol2 botaoGrande1 botaoIncluir" id="botaoIncluir" value="Salvar" type="button" onclick="incluir();">
	    </vacess:vacess>
	</fieldset>

</form> 