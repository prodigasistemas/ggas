<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Detalhar Nota de Cr�dito / D�bito<a class="linkHelp" href="<help:help>/detalhamentodenotadecrditodbito.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form method="post" action="exibirDetalhamentoNotaDebitoCredito" name="notaDebitoCreditoForm"> 

<script>

	function voltar() {
		submeter('notaDebitoCreditoForm','voltarParaPesquisarNotaCreditoDebito');
	}

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=0');
	
</script>
<token:token></token:token>
<input type="hidden" name="acao" id="acao" value="exibirDetalhamentoNotaDebitoCredito">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${creditoDebitoVO.chavePrimaria}"/>
<input type="hidden" name="indicadorPesquisa" value="${creditoDebitoVO.indicadorPesquisa}" />


<fieldset id="detalharConciliarNotaDebitoCredito" class="detalhamento conteinerNotaDebitoCredito">
	
	<input type="hidden" id="idCliente" name="idCliente" value="${creditoDebitoVO.idCliente }" />
	<c:if test="${creditoDebitoVO.idCliente ne null && creditoDebitoVO.idCliente > 0}">
		<a class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo">Nome:</label>
				<input type="hidden" name="nomeCompletoCliente" value="${creditoDebitoVO.nomeCompletoCliente}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.nomeCompletoCliente}"/></span><br />
				<label class="rotulo">CPF/CNPJ:</label>
				<input type="hidden" name="documentoFormatado" value="${creditoDebitoVO.documentoFormatado}">
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.documentoFormatado}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">Endere�o:</label>
				<input type="hidden" name="enderecoFormatadoCliente" value="${creditoDebitoVO.enderecoFormatadoCliente}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoFormatadoCliente}"/></span><br />
				<label class="rotulo">E-mail:</label>
				<input type="hidden" name="emailCliente" value="${creditoDebitoVO.emailCliente}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.emailCliente}"/></span><br />
			</fieldset>
		</fieldset>
	</c:if>
	
	<input type="hidden" id="idImovel" name="idImovel" value="${creditoDebitoVO.idImovel}" />
	<c:if test="${creditoDebitoVO.idImovel ne null && creditoDebitoVO.idImovel > 0}">
		<a id="linkDadosImovel" class="linkPesquisaAvancada linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosImovel" class="conteinerDados conteinerBloco">
			<fieldset class="coluna">
				<label class="rotulo">Descri��o:</label>
				<input type="hidden" name="nomeFantasiaImovel" value="${creditoDebitoVO.nomeFantasiaImovel}">
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.nomeFantasiaImovel}"/></span><br />
				<label class="rotulo">Matr�cula:</label>
				<input type="hidden" name="matriculaImovel" value="${creditoDebitoVO.matriculaImovel}">
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.matriculaImovel}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">Cidade:</label>
				<input type="hidden" name="cidadeImovel" value="${creditoDebitoVO.cidadeImovel}">
				<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.cidadeImovel}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<input type="hidden" name="enderecoImovel" value="${creditoDebitoVO.enderecoImovel}">
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoImovel}"/></span><br />
			</fieldset>
		</fieldset><br/>
	</c:if>

	<input type="hidden" name="idPontoConsumo" value="${creditoDebitoVO.idPontoConsumo }" />
	<c:if test="${creditoDebitoVO.idPontoConsumo ne null && creditoDebitoVO.idPontoConsumo > 0}">
		<a id="linkDadosPontoConsumo" class="linkExibirDetalhesSeguinte" href="#" rel="toggle[dadosPontoConsumo]"  data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
			<fieldset class="coluna detalhamentoColunaLarga">
				<label class="rotulo">Descri��o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.descricaoPontoConsumo}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoPontoConsumo}"/></span>
				<label class="rotulo rotulo3Linhas" style="white-space: normal;" for="spanPontoLegado">C&oacute;digo do Ponto de <br/>Consumo:</label>
				<span class="itemDetalhamento3Linhas" id="spanPontoLegado"><c:out value="${creditoDebitoVO.pontoConsumoLegado}"/></span>
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.cepPontoConsumo}"/></span><br />
				<label class="rotulo">Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.complementoPontoConsumo}"/></span><br />
			</fieldset>
		</fieldset>
	</c:if>
		
	<hr class="linhaSeparadoraDetalhamento" />
	
	<fieldset id="dadosNotaCreditoDebito" class="conteinerBloco">
		<fieldset class="coluna">
			<label class="rotulo">Tipo de Nota:</label>
			<span class="itemDetalhamento">
				<c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'debito'}"><c:out value="D�bito"/></c:if>
				<c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'credito'}"><c:out value="Cr�dito"/></c:if>
			</span><br />
			
			<label class="rotulo">Vencimento:</label>
			<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.dataVencimento}"/></span>
			
			<label class="rotulo">Rubrica:</label>
			<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.descricaoRubrica}"/></span><br />
			
			<label class="rotulo">Quantidade:</label>
			<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.quantidade}"/></span>

			<label class="rotulo">Valor Unit�rio:</label>
			<span class="itemDetalhamento">R$ <c:out value="${creditoDebitoVO.valor}"/></span>
		</fieldset>
	
		<fieldset class="colunaFinal">
			<label class="rotulo">Valor:</label>
			<span class="itemDetalhamento">R$ <c:out value="${creditoDebitoVO.valorTotal}"/></span><br />

			<label class="rotulo">Saldo:</label>
			<span class="itemDetalhamento">R$ <c:out value="${creditoDebitoVO.saldo}"/></span><br />
		    
			<label class="rotulo">Observa��o:</label>
			<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${creditoDebitoVO.observacaoNota}"/></span><br />
			
			<label class="rotulo">N� do Documento:</label>
			<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.chavePrimaria}"/></span><br />
		    
			<label class="rotulo">Data de Emiss�o:</label>
			<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.dataEmissao}"/></span>
			
			<label class="rotulo">Situa��o:</label>
			<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.situacao}"/></span>
			
			<c:if test="${creditoDebitoVO.descricaoCancelamento ne null && creditoDebitoVO.descricaoCancelamento ne ''}">
				<br/><label class="rotulo rotulo2Linhas">Motivo do Cancelamento:</label>
				<span class="itemDetalhamento itemDetalhamento2Linhas itemDetalhamentoMedio"><c:out value="${creditoDebitoVO.descricaoCancelamento}"/></span>
			</c:if>
		</fieldset>
	</fieldset>
	
	<c:if test="${listaDetalhamentoParcelamento ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Detalhamento do Parcelamento:</legend>
		<div class="conteinerTabela">
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaDetalhamentoParcelamento" sort="list" id="parcelamento" pagesize="15" excludedParams="" requestURI="#" >
	   		<display:column style="width: 70px" sortable="true" title="Descri��o">
	   			<c:out value="${parcelamento.descricao}"></c:out>
	   		</display:column>
	   		<display:column style="width: 150px" sortable="tue" title="Valor (R$)" >
	   		<fmt:formatNumber value="${parcelamento.valor}" minFractionDigits="2" type="currency"/>
	   		</display:column>
	   	</display:table>
	   	</div>
	</fieldset>
	</c:if>
	
	<c:if test="${listaParcelamentos ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Parcelamento:</legend>
		<div class="conteinerTabela">
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaParcelamentos" sort="list" id="parcela" pagesize="15" excludedParams="" requestURI="#" >
	   		<display:column style="width: 70px" sortable="true" title="Parcela">
	   			<c:out value="${parcela.numeroParcela}"></c:out>
	   		</display:column>
	   		<c:if test="${exibirNumeroDocumento eq 'true'}">
		   		<display:column style="width: 70px" sortable="true" title="N� Documento">
		   			<c:out value="${parcela.numeroDocumento}"></c:out>
		   		</display:column>
	   		</c:if>
	   		<display:column style="width: 150px" sortable="tue" title="Valor (R$)" >
	   			<c:out value="${parcela.valor}"></c:out>
	   		</display:column>
	   		
	   		<c:if test="${mostrarColunaDataVencimento eq true}">
		   		<display:column style="width: 150px" sortable="true" title="Data Vencimento">	   
					<c:out value="${parcela.dataVencimento}"></c:out>
				</display:column>
			</c:if>
						
	   	</display:table>
	   	</div>
	</fieldset>
	</c:if>

	
	
	<c:if test="${listaNotaCredito ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Cr�ditos Conciliados:</legend>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaNotaCredito" sort="list" id="notaCreditoConciliado" excludedParams="" requestURI="#" >
				<display:column style="width: 90px" property="credito.faturaAtual.chavePrimaria" title="N� do<br />Documento" />
				<display:column style="width: 150px" title="Rubrica" >
					<c:forEach items="${notaCreditoConciliado.credito.faturaAtual.listaFaturaItem}" var="faturaItem">
						<c:out value="${faturaItem.rubrica.descricao}"/>
					</c:forEach>
				</display:column>
				<display:column style="width: 150px" title="Quantidade" >
					<c:forEach items="${notaCreditoConciliado.credito.faturaAtual.listaFaturaItem}" var="faturaItem">
						<c:out value="${faturaItem.quantidade}"/>
					</c:forEach>
				</display:column>
		   		<display:column style="width: 80px" title="Valor (R$)" >
		   			<fmt:formatNumber value="${notaCreditoConciliado.credito.faturaAtual.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
		   		<display:column style="width: 80px" title="Valor Conciliado (R$)" >
		   			<fmt:formatNumber value="${notaCreditoConciliado.valorConciliacao}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
		   		<display:column style="width: 80px" property="credito.faturaAtual.observacaoNota" title="Observa��o" />
		   	</display:table>
		</fieldset>
	</c:if>
		
	<c:if test="${listaNotaDebito ne null}">
		<hr class="linhaSeparadoraDetalhamento" />
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">D�bitos Conciliados:</legend>
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaNotaDebito" sort="list" id="notaDebitoConciliado" excludedParams="" requestURI="#" >
		   		<display:column style="width: 90px" property="debito.faturaAtual.chavePrimaria" title="N� do<br />Documento" />
				<display:column style="width: 150px" title="Rubrica" >
					<c:forEach items="${notaDebitoConciliado.debito.faturaAtual.listaFaturaItem}" var="faturaItem">
						<c:out value="${faturaItem.rubrica.descricao}"/>
					</c:forEach>
				</display:column>
				<display:column style="width: 150px" title="Quantidade" >
					<c:forEach items="${notaDebitoConciliado.debito.faturaAtual.listaFaturaItem}" var="faturaItem">
						<c:out value="${faturaItem.quantidade}"/>
					</c:forEach>
				</display:column>
		   		<display:column style="width: 80px" title="Valor (R$)" >
		   			<fmt:formatNumber value="${notaDebitoConciliado.debito.faturaAtual.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
		   		<display:column style="width: 80px" title="Valor Conciliado (R$)" >
		   			<fmt:formatNumber value="${notaDebitoConciliado.valorConciliacao}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
		   		<display:column style="width: 80px" property="debito.faturaAtual.observacaoNota" title="Observa��o" />
		   	</display:table>
		</fieldset>
	</c:if>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">   
</fieldset>

</form> 