<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Pesquisar Nota de Cr�dito / D�bito<a class="linkHelp" href="<help:help>/consultadenotadecrditodbito.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.<br />
Para incluir uma nova nota de cr�dito / d�bito, informe um cliente ou um im�vel e clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>

<script>

	
	$(document).ready(function(){

		$(".campoData").datepicker({
			changeYear: true, 
			showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy', 
			onSelect: function() {
					$('form[name="notaDebitoCreditoForm"]').change();
				}
		});
		
		// Dialog - configura��es
		$("#cancelamentoNotaPopup").dialog({
			autoOpen: false,			
			width: 370,
			modal: true,
			minHeight: 90, 
			resizable: false
		});

		$("#pontoConsumoPopupLista").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			minHeight: 90, 
			resizable: false
		});
					
		
	    //-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		carregarDialogGerarBoleto();
		corrigirPosicaoDatepicker();

		$('form[name="notaDebitoCreditoForm"]').on('change keyup paste', function(){
			habilitaDesabilitaBotaoPesquisar();
			habilitaDesabilitaBotaoIncluir();
			habilitaDesabilitaBotaoPontosDeConsumo();
		});
		$('form[name="notaDebitoCreditoForm"]').change();
	});
	
	function habilitaDesabilitaBotaoIncluir() {
		var clienteSelecionado = possuiId('#idCliente');
		var imovelSelecionado = possuiId('#idImovel');
		var clienteOuImovelSelecionado = clienteSelecionado || imovelSelecionado;
		$('#botaoIncluir').prop('disabled', !clienteOuImovelSelecionado);
		return clienteOuImovelSelecionado;
	}
	
	function habilitaDesabilitaBotaoPontosDeConsumo(){
		var imovelSelecionado = possuiId('#idImovel');
		$('#botaoPontosConsumo').prop('disabled', !imovelSelecionado);
		return imovelSelecionado;
	}
	
	function habilitaDesabilitaBotaoPesquisar(){
		var regexExisteValor = '=\\w';
		var inputs = $('form[name="notaDebitoCreditoForm"] :input\
				[name="nomeCompletoCliente"],\
				[name="documentoFormatado"],\
				[name="enderecoFormatadoCliente"],\
				[name="emailCliente"],\
				[name="nomeFantasiaImovel"],\
				[name="matriculaImovel"],\
				[name="numeroImovel"],\
				[name="cidadeImovel"],\
				[name="indicadorCreditoDebito"],\
				[name="dataVencimentoInicial"],\
				[name="dataVencimentoFinal"],\
				[name="idSituacao"],\
				[name="numeroDocumento"]');
		var data = inputs.serialize();
		var matchValor = data.match(regexExisteValor);
		var filtroFoiDefinido = Boolean(matchValor);
		$("#botaoPesquisar").prop('disabled', !filtroFoiDefinido);
		return filtroFoiDefinido;
	}
	
	function possuiId(elem){
		var valor = $(elem).val();
		var valorInt = parseInt(valor);
		var possuiId = Boolean(valorInt);
		return possuiId;
	}
	
	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};	
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
		}	
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;
	}

	function limparFormulario() {
		$('form[name="notaDebitoCreditoForm"] :input')
			.not(':button, :submit, :reset, :hidden')
			.removeAttr('checked')
			.removeAttr('selected')
			.not(':checkbox, :radio, select')
			.val('');
		$('form[name="notaDebitoCreditoForm"] select').val(-1);
		limparCamposPesquisa();	
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=700,width=750,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	$('form[name="notaDebitoCreditoForm"]').change();
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}

 		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

	}

	function pesquisar(idPontoConsumo) {
		document.getElementById('idPontoConsumo').value = idPontoConsumo;
		pesquisarNota();
	}

	function pesquisarNota() {
		submeter('notaDebitoCreditoForm', 'pesquisarNotaDebitoCredito');
	}

	function pesquisarPontos() {
		submeter('notaDebitoCreditoForm', 'pesquisarPontoConsumoNotaDebitoCredito');
	}

	function incluir() {
		submeter('notaDebitoCreditoForm', 'exibirInclusaoNotaDebitoCredito');
	}

	function conciliarNotaDebitoCredito(){			
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			submeter("notaDebitoCreditoForm", "exibirConciliarNotaDebitoCredito");
	    }
	}
	
	function exibirProrrogacaoNota() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('notaDebitoCreditoForm', 'exibirProrrogacaoNotaCreditoDebito');
	    }
		
	}
	
	function cancelarNotas() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			submeter('notaDebitoCreditoForm', 'exibirCancelamentoNotaDebitoCredito');
	    }
	}

	function cancelarNotaDebitoCredito() {
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		var motivoCancelamento = document.getElementById('motivoCancelamento');
		document.getElementById('idMotivoCancelamento').value = motivoCancelamento.value;
		submeter('notaDebitoCreditoForm','cancelarNotaDebitoCredito');
		
	}

	function detalharNotaDebitoCredito(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("notaDebitoCreditoForm", "exibirDetalhamentoNotaDebitoCredito");
	}

	function init() {
		<c:choose>
			<c:when test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				$("indicadorPesquisaCliente").attr('checked', true);
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				$("indicadorPesquisaImovel").attr('checked', true);
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	

		var indicadorCreditoDebito = "${creditoDebitoVO.indicadorCreditoDebito}";
		var dataVencimentoInicial = "${creditoDebitoVO.dataVencimentoInicial}";
		var dataVencimentoFinal = "${creditoDebitoVO.dataVencimentoFinal}";
		if ((indicadorCreditoDebito != null && indicadorCreditoDebito != "") || (dataVencimentoInicial != null && dataVencimentoInicial != "") || (dataVencimentoFinal != null && dataVencimentoFinal != "")) {
			document.getElementById("botaoPesquisar").disabled = false;
		}
		manterCheckboSelecionado();

		<c:if test="${not empty gerarRelatorioNotaDebitoCredito && gerarRelatorioNotaDebitoCredito == 'true'}">
		efetuarDownload();
		</c:if>
		
	}	


	function efetuarDownload() {
		var iframe = document.getElementById("download");
		if (iframe != undefined) {
			iframe.src = '<c:url value="/gerarRelatorioNotaDebitoCredito"/>';
		}
	}
	
	addLoadEvent(init);

	function exibirDadosPontoConsumo(idFatura) {
		
		var descricao = '';
		var endereco = '';
		var cep = '';
   		var segmento = '';
   		var matriculaImovel = '';
   		var exibe = false;
   			
		if(idFatura != "" && parseInt(idFatura) > 0){
	       	AjaxService.obterListaPontoConsumoPorFatura( idFatura, { 
	           	callback: function(listaPontos) {
	       		answer = listaPontos;
	       		$('#pontoConsumo tr:not(:first)').remove();
	       		var param = '';
           		var div = 2;
           		var start = 2;
           		var cont = 0;
           		$('#corpoPontoConsumo').html('');
           		           		
	       		for (key in listaPontos) {
	       			var pontoConsumo = listaPontos[cont];
	       			var inner = '';
	       			
	       			if(start % div == 0){
						param = "odd";
		            }else{
						param = "even";
			        }
		
					descricao = pontoConsumo['descricao'];
					endereco = pontoConsumo['endereco'];
					cep = pontoConsumo['cep'];
					segmento = pontoConsumo['segmento'];
					matriculaImovel = pontoConsumo['matriculaImovel'];

		       		inner = inner + '<tr class='+param+'><td>'+matriculaImovel+'</td>';
		       		inner = inner + '<td>'+descricao+'</td>';
		       		inner = inner + '<td>'+endereco+'</td>';
		       		inner = inner + '<td>'+segmento+'</td>';
		       		inner = inner + '<td>'+cep+'</td></tr>';

		           	$("#corpoPontoConsumo").append(inner);
		           	start = start + 1;
					cont++;
					exibe = true;
	       		}
	       	  }
	       	 , async: false}
	       	);
			if (exibe){
				$("#pontoConsumoPopupLista").dialog('open');
			}else{
				alert("Cr�dito ou D�bito selecionado n�o possui ponto de consumo.");
	       	}
			$("#pontoConsumoPopup").css({position:"relative"}).end().dialog('open');
		} else {
			alert("Cr�dito ou D�bito selecionado n�o possui ponto de consumo.");
		}
		
	}

	function closePopup(){
		$("#pontoConsumoPopupLista").dialog('close');
	}

	function manterCheckboSelecionado() {
		<c:forEach items="${creditoDebitoVO.chavesPrimarias}" var="chaveNota">
			if(document.getElementById('chavePrimaria'+'<c:out value="${chaveNota}"/>') != undefined){
				document.getElementById('chavePrimaria'+'<c:out value="${chaveNota}"/>').checked = true;
			}	
		</c:forEach>
	}

	function imprimirSegundaVia(idNota) {
		document.getElementById('chavePrimaria').value = idNota;
		submeter('0', 'imprimirSegundaVia');
	}

</script>

<form method="post" action="pesquisarNotaDebitoCredito" id="notaDebitoCreditoForm" name="notaDebitoCreditoForm">	
	<token:token></token:token>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${creditoDebitoVO.idPontoConsumo}">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idMotivoCancelamento" type="hidden" id="idMotivoCancelamento">
	<input name="chaveNotaSelecionada" type="hidden" id="chaveNotaSelecionada">
	<input name="telaHistorico" type="hidden" id="telaHistorico" value="false">
	<input name="novaDataVencimento" type="hidden" id="novaDataVencimento" >
	
	<div id="dialogGerarBoleto" title="Gerar Boleto" style="display: none" >
		<input name="novaDataVencimento" type="hidden" id="novaDataVencimento" >
		<table class="dataTableGGAS dataTableDialog dataTableCabecalho2Linhas">
			<thead>
				<tr>
					<th>Numero do Documento</th>
					<th style="width: 230px">Ponto Consumo</th>
					<th style="width: 100px">Data de Emissao</th>
					<th style="width: 100px">Data de Vencimento</th>
					<th style="width: 100px">Valor Documento Cobranca</th>
					<th style="width: 100px">Valor Recebimento</th>
					<th style="width: 100px">Saldo</th>
					<th style="width: 105px">Nova Data Vencimento</th>
					<th style="width: 60px">Imprimir Boleto</th>
				</tr>
			</thead>
			<tbody id="corpoListaFaturasPendentes"></tbody>
		</table>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoFechar" class="bottonRightCol" 
			value="Fechar" type="button" onclick="closeDialogGerarBoleto()"
			style="float:right">
	</div>            
	
	<div id="pontoConsumoPopupLista" title="Ponto de Consumo" >
		<table class="dataTableGGAS dataTableDialog">
			<thead>
				<tr>
					<th>Matr�cula do Im�vel</th>
					<th style="width: 230px">Descricao</th>
					<th style="width: 230px">Endere�o</th>
					<th style="width: 140px">Segmento</th>
					<th style="width: 75px">CEP</th>
				</tr>
			</thead>
			<tbody id="corpoPontoConsumo"></tbody>
		</table>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol" value="Fechar" type="button" onclick="closePopup()">
	</div>
	
	<fieldset id="pesquisarNotaDebitoCredito" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input tabindex="1" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${creditoDebitoVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${creditoDebitoVO.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${creditoDebitoVO.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${creditoDebitoVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${creditoDebitoVO.enderecoFormatadoCliente}"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>
		</fieldset>
	
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input tabindex="2" id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${creditoDebitoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${creditoDebitoVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${creditoDebitoVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${creditoDebitoVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${creditoDebitoVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${creditoDebitoVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${creditoDebitoVO.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${creditoDebitoVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${creditoDebitoVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${creditoDebitoVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${creditoDebitoVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${creditoDebitoVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${creditoDebitoVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset id="conteinerPesquisarNotaDebitoCreditoInferior">
			<label class="rotulo">Tipo de Lan�amento:</label>
			<input tabindex="3" class="campoRadio" type="radio" id="indicadorDebitoSim" name="indicadorCreditoDebito" value="debito" <c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'debito'}">checked="checked" </c:if> onclick="document.getElementById('botaoPesquisar').disabled = false;">
			<label class="rotuloRadio" for="indicadorDebitoSim">D�bito</label>
			<input tabindex="4" class="campoRadio" type="radio" id="indicadorDebitoNao" name="indicadorCreditoDebito" value="credito" <c:if test="${creditoDebitoVO.indicadorCreditoDebito eq 'credito'}">checked="checked" </c:if> onclick="document.getElementById('botaoPesquisar').disabled = false;">
			<label class="rotuloRadio" for="indicadorDebitoNao">Cr�dito</label><br />
		
			<label class="rotulo" >Intervalo de Vencimento:</label>
			<input tabindex="5" class="campoData campoHorizontal" type="text" id="dataVencimentoInicial" name="dataVencimentoInicial" value="${creditoDebitoVO.dataVencimentoInicial}">
			<label class="rotuloEntreCampos">a</label>
			<input tabindex="6" class="campoData campoHorizontal" type="text" id="dataVencimentoFinal" name="dataVencimentoFinal" value="${creditoDebitoVO.dataVencimentoFinal}"><br/><br class="quebraLinha"/>
			
			<label class="rotulo">Situa��o do Documento:</label>
			<select name="idSituacao" id="idSituacao" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaSituacaoNota}" var="situacao">
					<option value="<c:out value="${situacao.chavePrimaria}"/>" <c:if test="${creditoDebitoVO.idSituacao == situacao.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${situacao.descricao}"/>
					</option>		
			    </c:forEach>				
		    </select><br />
			<label class="rotulo" id="rotuloNumeroDocumentoTexto" for="numeroDocumentoTexto">Numero do documento:</label>
		    <input class="campoTexto" type="text" id="numeroDocumento" name="numeroDocumento"  maxlength="50" size="37" value="${creditoDebitoVO.numeroDocumento}" onkeypress="return formatarCampoInteiro(event);"><br />
		</fieldset>
		
	    <fieldset id="conteinerBotoesPesquisaNotaDebitoCredito" class="conteinerBotoesPesquisarDirFixo">
	    	<input tabindex="7" id="botaoPontosConsumo" name="Button" class="bottonRightCol2 botaoPontosDeConsumo" value="Pontos de Consumo" type="button" onclick="pesquisarPontos();"/>	
	    	<input tabindex="8" id="botaoPesquisar" name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisarNota();"/>	
			<input tabindex="9" name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();" />		
		</fieldset>
	</fieldset>	
	
	<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarNotaDebitoCredito">
	        <display:column title="Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.codigoPontoConsumo}"/> - <c:out value="${pontoConsumo.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="Segmento" style="width: 170px">
	            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.segmento.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="Situa��o" style="width: 250px">
	            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="C�digo do Ponto de Consumo" style="width: 250px">
	            <a href="javascript:pesquisar(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${pontoConsumo.codigoLegado}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<c:if test="${listaNotaDebitoCredito ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaNotaDebitoCredito" id="notaDebitoCredito" partialList="true" sort="external" pagesize="15" size="${pageSize}" decorator="br.com.ggas.web.faturamento.notadebitocredito.decorator.NotaDebitoCreditoResultadoPesquisaDecorator" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarNotaDebitoCredito">	
			
			<display:column property="chavePrimariaComLock" style="width: 25px" media="html" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"/>
	        
	        <display:column sortable="true" sortProperty="tipoDocumento.descricao" title="Tipo de<br/> Documento" style="width: 96px">
	            <a id="tipo_documento_${notaDebitoCredito.chavePrimaria}" href="javascript:detalharNotaDebitoCredito(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${notaDebitoCredito.tipoDocumento.descricao}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="chavePrimaria" title="N�mero do<br />Documento" style="width: 73px">
	            <a href="javascript:detalharNotaDebitoCredito(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${notaDebitoCredito.chavePrimaria}"/>
	            </a>
	        </display:column>
	        <display:column title="Valor (R$)" sortProperty="valorTotal" style="width: 113px; text-align: right" sortable="true">
	            <a href="javascript:detalharNotaDebitoCredito(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<fmt:formatNumber value="${notaDebitoCredito.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="dataEmissao" title="Data de<br/>Emiss�o" style="width: 72px">
	            <a href="javascript:detalharNotaDebitoCredito(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<fmt:formatDate value="${notaDebitoCredito.dataEmissao}" pattern="dd/MM/yyyy"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="dataVencimento" title="Data de<br/>Vencimento" style="width: 72px">
	           <a href="javascript:detalharNotaDebitoCredito(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:if test="${notaDebitoCredito.dataVencimento ne null}">
	            		<fmt:formatDate value="${notaDebitoCredito.dataVencimento}" pattern="dd/MM/yyyy"/>
	            	</c:if>
	            </a>
	        </display:column>
	         <display:column title="Situa��o de<br/>Pagamento" sortProperty="situacaoPagamento.descricao" style="width: 154px" sortable="true">
	         	<c:if test="${notaDebitoCredito.creditoDebitoSituacao.valido}">
	           <a id="situacao_pagamento_${notaDebitoCredito.chavePrimaria}"
	           		href="javascript:detalharNotaDebitoCredito(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);">
	           		<span class="linkInvisivel"></span>
	            	<c:out value="${notaDebitoCredito.situacaoPagamento.descricao}"/>
	            </a>
	            </c:if>
	        </display:column>
	        <display:column title="Situa��o do<br/>Documento" sortProperty="creditoDebitoSituacao.descricao" sortable="true">
	           <a href="javascript:detalharNotaDebitoCredito(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${notaDebitoCredito.creditoDebitoSituacao.descricao}"/>
	            </a>
	        </display:column>
	        <display:column title="2� Via" style="width: 40px" sortable="false">
	        	<c:if test="${notaDebitoCredito.situacaoPagamento.chavePrimaria eq idSituacaoPendente || notaDebitoCredito.situacaoPagamento.chavePrimaria eq idSituacaoParcial}">
					<a href='javascript:imprimirSegundaVia(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);'>
			    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via" title="Imprimir 2� Via" border="0" style="margin-top: 5px;" >
			    	</a>
		    	</c:if>					
			</display:column>
	   		<display:column title="Ponto de<br/>Consumo" style="width: 60px" sortable="false">
	   			<c:if test="${notaDebitoCredito.indicadorPontoConsumo}">
					<a href="javascript:exibirDadosPontoConsumo(<c:out value='${notaDebitoCredito.chavePrimaria}'/>);"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>
				</c:if>
			</display:column>	
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaNotaDebitoCredito}">
   			<input tabindex="10" name="buttonAlterar" value="Conciliar" class="bottonRightCol2 botaoConciliar" onclick="conciliarNotaDebitoCredito();" type="button">
			<input tabindex="11" name="buttonRemover" value="Cancelar Nota" id="cancelarNota" class="bottonRightCol2 bottonLeftColUltimo botaoCancelarNotas" onclick="cancelarNotas();" type="button">
			<input tabindex="11" name="prorrogarNota" value="Prorrogar" id="prorrogarNota" class="bottonRightCol2 bottonLeftColUltimo botaoCancelarNotas" onclick="exibirProrrogacaoNota();" type="button">
			<input tabindex="12" name="buttonGerarBoleto" value="Gerar Boleto" id="buttonGerarBoleto" class="bottonRightCol2 bottonLeftColUltimo" type="button" onclick="exibirDialogGerarBoletoNotaDebito()" />
		</c:if>
		<input tabindex="13" id="botaoIncluir" name="botaoIncluir" class="bottonRightCol2 botaoGrande1" value="Incluir" type="button" disabled="disabled" onclick="incluir();">
	</fieldset>
	    
</form>


<c:if test="${not empty gerarRelatorioNotaDebitoCredito && gerarRelatorioNotaDebitoCredito == 'true'}">
	<iframe id="download" src ="" width="0" height="0"></iframe> 
</c:if>