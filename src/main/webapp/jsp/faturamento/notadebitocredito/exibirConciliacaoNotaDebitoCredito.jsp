<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Conciliar Nota de Cr�dito / D�bito<a class="linkHelp" href="<help:help>/conciliandonotadecrditodbito.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form method="post" action="conciliarNotaDebitoCredito" id="notaDebitoCreditoForm" name="notaDebitoCreditoForm"> 

	<script>
	
		function cancelar() {
			submeter('notaDebitoCreditoForm','pesquisarNotaDebitoCredito?unlock=true');
		}

		function atualizarValorDebitos(valor, checked, input, detalhe) {
			var inputValorCreditoDebito = document.getElementById(input);
			if (inputValorCreditoDebito.value == "") {
				inputValorCreditoDebito.value = 0;
			}
			var valorDebitos = parseFloat(inputValorCreditoDebito.value);
			if (checked) {
				valorDebitos = valorDebitos + parseFloat(valor);
			} else {
				valorDebitos = valorDebitos - parseFloat(valor);
			}
			inputValorCreditoDebito.value = valorDebitos.toFixed(2);

			var valorFormatado = inputValorCreditoDebito.value;
			valorFormatado = valorFormatado.replace(".",",");
			valorFormatado = formatarValorDecimalVariavel(valorFormatado, 2);
			document.getElementById(detalhe).innerHTML = 'R$ ' + valorFormatado;
		}

		function atualizarValorNotaDebitoCredito(obj, paramValor, indicadorDebito) {
			paramValor = paramValor.replace(",",".");
			if (indicadorDebito) {
				atualizarValorDebitos(paramValor, obj.checked, "valorDebitos", "detalheDebito");
			} else {
				atualizarValorDebitos(paramValor, obj.checked, "valorCreditos", "detalheCredito");
			}

			var valorDebitos = $("#valorDebitos").val();
			var valorCreditos = $("#valorCreditos").val();
			if (valorDebitos == "") {
				valorDebitos = 0;
			}
			if (valorCreditos == "") {
				valorCreditos = 0;
			}
			var somaValores = valorCreditos - valorDebitos;
			var valores = somaValores.toString();
			valores = valores.replace(".",",");
			var valorNegativo = "";
			if (valores != null && valores[0] == "-") {
				valorNegativo = "-";
				valores = valores.substr(1,valores.length-1);
			}
			valores = formatarValorDecimalVariavel(valores, 2);
			valorNegativo = valorNegativo + valores;
			
			document.getElementById("somaValores").innerHTML = 'R$ ' + valorNegativo;
			if (somaValores < 0) {
				$("#somaValores").removeClass("itemDetalhamentoPositivo");
				$("#somaValores").addClass("itemDetalhamentoNegativo");
			} else if (somaValores > 0) {
				$("#somaValores").removeClass("itemDetalhamentoNegativo");
				$("#somaValores").addClass("itemDetalhamentoPositivo");
			} else {
				$("#somaValores").removeClass("itemDetalhamentoPositivo");
				$("#somaValores").removeClass("itemDetalhamentoNegativo");
			}
			
		}

		animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,hide=0');
		animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,hide=0');

		function init() {
			<c:forEach items="${creditoDebitoVO.chavesPrimariasCreditos}" var="chaveCreditoSelecionada">
				var inputCredito = document.getElementById('chaveCredito'+'<c:out value="${chaveCreditoSelecionada}"/>');
				if(inputCredito != undefined){
					inputCredito.checked = true;
					var inputValorConciliado = document.getElementById('creditoConciliado'+'<c:out value="${chaveCreditoSelecionada}"/>')
					if (inputValorConciliado != undefined) {
						atualizarValorNotaDebitoCredito(inputCredito, inputValorConciliado.value, false);
					}
				}	
			</c:forEach>

			<c:forEach items="${creditoDebitoVO.chavesPrimariasDebitos}" var="chaveDebitoSelecionada">
				var inputDebito = document.getElementById('chaveDebito'+'<c:out value="${chaveDebitoSelecionada}"/>');
				if(inputDebito != undefined){
					inputDebito.checked = true
					var inputValorConciliado = document.getElementById('debitoConciliado'+'<c:out value="${chaveDebitoSelecionada}"/>')
					if (inputValorConciliado != undefined) {
						atualizarValorNotaDebitoCredito(inputDebito, inputValorConciliado.value, true);
					}
				}	
			</c:forEach>
		}
		
		addLoadEvent(init);
		
	</script>
	<token:token></token:token>
	<input type="hidden" name="acao" id="acao" value="conciliarNotaDebitoCredito">
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${creditoDebitoVO.chavePrimaria}"/>
	<input type="hidden" name="indicadorPesquisa" value="${creditoDebitoVO.indicadorPesquisa}" />
	<c:forEach items="${creditoDebitoVO.chavesPrimarias}" var="chave">
		<input type="hidden" name="chavesPrimarias" id="chavesPrimarias${chave}" value="${chave}"/>
	</c:forEach>
	
	<fieldset id="detalharConciliarNotaDebitoCredito" class="detalhamento conteinerNotaDebitoCredito">
	
		<c:if test="${creditoDebitoVO.idCliente ne null && creditoDebitoVO.idCliente > 0}">
			<input type="hidden" id="idCliente" name="idCliente" value="${creditoDebitoVO.idCliente}" />
			<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Nome:</label>
					<input type="hidden" name="nomeCompletoCliente" value="${creditoDebitoVO.nomeCompletoCliente}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.nomeCompletoCliente}"/></span><br />
					<label class="rotulo">CPF/CNPJ:</label>
					<input type="hidden" name="documentoFormatado" value="${creditoDebitoVO.documentoFormatado}">
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.documentoFormatado}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Endere�o:</label>
					<input type="hidden" name="enderecoFormatadoCliente" value="${creditoDebitoVO.enderecoFormatadoCliente}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoFormatadoCliente}"/></span><br />
					<label class="rotulo">E-mail:</label>
					<input type="hidden" name="emailCliente" value="${creditoDebitoVO.emailCliente}">
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.emailCliente}"/></span><br />
				</fieldset>
			</fieldset>
			<hr class="linhaSeparadoraDetalhamento" />
		</c:if>
	
		<c:if test="${creditoDebitoVO.idPontoConsumo ne null && creditoDebitoVO.idPontoConsumo > 0}">
			<input type="hidden" id="idPontoConsumo" name="idPontoConsumo" value="${creditoDebitoVO.idPontoConsumo}" />
			<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
				<fieldset class="coluna detalhamentoColunaLarga">
					<label class="rotulo">Descricao:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.descricaoPontoConsumo}"/></span><br />
					<label class="rotulo">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.enderecoPontoConsumo}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">CEP:</label>
					<span class="itemDetalhamento"><c:out value="${creditoDebitoVO.cepPontoConsumo}"/></span><br />
					<label class="rotulo">Complemento:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${creditoDebitoVO.complementoPontoConsumo}"/></span><br />
				</fieldset>
			</fieldset>
			<hr class="linhaSeparadoraDetalhamento" />
		</c:if>
			
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Cr�ditos:</legend>
			<display:table class="dataTableGGAS" name="listaNotasCredito" sort="list" id="notaCredito" excludedParams="" requestURI="#" >
				<display:column style="width: 25px"> 
		      		<input type="checkbox" name="chavesPrimariasCreditos" id="chaveCredito${notaCredito.chavePrimaria}" value="${notaCredito.chavePrimaria}" onclick="atualizarValorNotaDebitoCredito(this, '<c:out value="${notaCredito.valorConciliado}"/>', false);">
		     	</display:column>
				<display:column property="chavePrimaria" title="N� do Cr�dito" />
				<display:column style="width: 200px; text-align: right" title="Valor (R$)" >
		   			<fmt:formatNumber value="${notaCredito.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
				<display:column style="width: 200px; text-align: right" title="Saldo (R$)" >
					<input type="hidden" id="creditoConciliado${notaCredito.chavePrimaria}" value="<c:out value='${notaCredito.valorConciliado}'/>"></input>
		   			<fmt:formatNumber value="${notaCredito.valorConciliado}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
		   		<display:column style="width: 200px" title="Data de Emiss�o" >
		   			<fmt:formatDate value="${notaCredito.dataEmissao}" pattern="dd/MM/yyyy"/>
		   		</display:column>
		   	</display:table>
			<input type="hidden" id="valorCreditos" >
		   	<label class="rotulo">Valor dos cr�ditos selecionados:</label>
			<span id="detalheCredito" class="itemDetalhamento">R$ <c:out value="0,00"/></span>
		</fieldset>
		
		<hr class="linhaSeparadora" />
		
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">D�bitos:</legend>
			<display:table class="dataTableGGAS" name="listaNotasDebito" sort="list" id="notaDebito" excludedParams="" requestURI="#" >
				<display:column style="width: 25px"> 
		      		<input type="checkbox" name="chavesPrimariasDebitos" id="chaveDebito${notaDebito.chavePrimaria}" value="${notaDebito.chavePrimaria}" onclick="atualizarValorNotaDebitoCredito(this, '<c:out value="${notaDebito.valorConciliado}"/>', true);">
		     	</display:column>
				<display:column property="chavePrimaria" title="N� do Documento" />
		   		<display:column style="width: 200px; text-align: right" title="Valor (R$)" >
		   			<fmt:formatNumber value="${notaDebito.valorTotal}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
		   		<display:column style="width: 200px; text-align: right" title="Saldo (R$)" >
		   			<input type="hidden" id="debitoConciliado${notaDebito.chavePrimaria}" value="<c:out value='${notaDebito.valorConciliado}'/>"></input>
		   			<fmt:formatNumber value="${notaDebito.valorConciliado}" maxFractionDigits="2" minFractionDigits="2"/>
		   		</display:column>
		   		<display:column style="width: 200px" title="Data de Emiss�o" >
		   			<fmt:formatDate value="${notaDebito.dataEmissao}" pattern="dd/MM/yyyy"/>
		   		</display:column>
		   		<display:column style="width: 200px" title="Vencimento" >
		   			<fmt:formatDate value="${notaDebito.dataVencimento}" pattern="dd/MM/yyyy"/>
		   		</display:column>
		   	</display:table>
			<input type="hidden" id="valorDebitos" >
		   	<label class="rotulo">Valor dos d�bitos selecionados:</label>
			<span id="detalheDebito" class="itemDetalhamento">R$ <c:out value="0,00"/></span><br />

		</fieldset>
		
		<label class="rotulo">Valor Total:</label>
		<span id="somaValores" class="itemDetalhamento">R$ <c:out value="0,00"/></span>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
		<input name="Button" class="bottonRightCol2 botaoGrande1 botaoConciliar" value="Conciliar" type="submit" >
	</fieldset>

</form> 