<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
function incluirEquipamento() {
	var chavePrimaria = document.forms[0].chavePrimaria.value;
	var chaveEquipamento = document.forms[0].equipamento.value;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "incluirEquipamento?equipamento="+chaveEquipamento+"&chavePrimaria="+chavePrimaria+"&"+noCache;
	
	if(chaveEquipamento != "-1") {
	  carregarFragmento("gridEquipamentos",url);
	} else {
		alert('Equipamento n�o selecionado');
	}
}

function removerEquipamento(chaveEquipamento) {
	var noCache = "noCache=" + new Date().getTime();
	$("#gridEquipamentos").load("removerEquipamento?chavePrimariaEquipamento="+chaveEquipamento+"&"+noCache);
}
</script>
	<fieldset class="conteinerBloco">
			<fieldset id="servicoTipoEquipamentoInclusao">
				<legend class="conteinerBlocoTitulo">Equipamento Especial Necess�rio</legend>
				<fieldset id="equipamentosServico" class="conteinerDados">
	<label class="rotulo rotuloHorizontal campoObrigatorio" for="material"><span class="campoObrigatorioSimbolo2"> * </span>Descri��o do Equipamentos:</label>
	<select class="campoSelect" name="equipamento" id="equipamento">
    	<option value="-1">Selecione</option>
		<c:forEach items="${listaEquipamentosCadastro}" var="equipamento">
			<option value="<c:out value="${equipamento.chavePrimaria}"/>" >
				<c:out value="${equipamento.descricao}"/>
			</option>
	    </c:forEach>
    </select>

	<fieldset class="conteinerBotoes"> 
		<input class="bottonRightCol bottonLeftColUltimo" id="botaoIncluirEquipamento" name="botaoIncluirEquipamento" value="Adicionar" type="button" onclick="incluirEquipamento();"	>
	</fieldset>
	
	<c:set var="i" value="0" />
	<display:table class="dataTableGGAS" name="sessionScope.listaEquipamentos" sort="list" id="servicoTipoEquipamento" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		<display:column property="equipamento.descricao" sortable="false" title="Equipamento Especial Necess�rio" />
	 		
		
		<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
			<a onclick="return confirm('Deseja excluir o equipamento?');" href="javascript:removerEquipamento('${servicoTipoEquipamento.equipamento.chavePrimaria}');">
				<img title="Excluir equipamento" alt="Excluir equipamento"  src="<c:url value="/imagens/deletar_x.png"/>">
			</a> 
		</display:column>
		<c:set var="i" value="${i+1}" />
	</display:table>
	</fieldset>
	</fieldset>
</fieldset>