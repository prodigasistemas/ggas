<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<input type="hidden" name="servicoTipo.indicadorAtualizacaoCadastral" value="${servicoTipo.indicadorAtualizacaoCadastral}" />
<input type="hidden" name="servicoTipo.telaAtualizacao.descricao" value="${servicoTipo.telaAtualizacao.descricao}" />
<input type="hidden" name="descricaoTelaAtualizacaoPontoConsumo" value="${descricaoTelaAtualizacaoPontoConsumo}" />
<input type="hidden" name="servicoTipo.indicadorOperacaoMedidor" value="${servicoTipo.indicadorOperacaoMedidor}" />

<form:form action="pesquisarServicoTipo" onsubmit="return false;" id="servicoTipoForm" name="servicoTipoForm" method="post" modelAttribute="ServicoTipo" >
    <div class="card shadow-sm">
        <div class="card-header"><h5 class="card-title mb-0">Pesquisar Tipo de Servi�o</h5></div>
        <div class="card-body">
            <div class="alert alert-primary fade show">
                <i class="fa fa-question-circle"></i>
                Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <b>Pesquisar</b>, ou clique apenas em
                <b>Pesquisar</b> para exibir todos. Para incluir uma novo registro clique em <b>Incluir</b>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body bg-light">
                            <h5>Caracter�sticas</h5>
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-md-12">

                                    <label for="descricao">Descri��o</label>
                                    <input class="form-control form-control-sm" type="text" name="descricao" id="descricao"
                                           maxlength="200" size="20" value="${servicoTipo.descricao}"
                                           onkeyup="letraMaiuscula(this);"/>
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="servicoTipoPrioridade">Prioridade</label>
                                    <select id="servicoTipoPrioridade" class="form-control form-control-sm" name="servicoTipoPrioridade">
                                        <option value="-1">Selecione</option>
                                        <c:forEach items="${listaPrioridade}" var="prioridade">
                                            <option value="<c:out value="${prioridade.chavePrimaria}"/>" <c:if test="${servicoTipo.servicoTipoPrioridade.chavePrimaria == prioridade.chavePrimaria}">selected="selected"</c:if>>
                                                <c:out value="${prioridade.descricao}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="documentoImpressaoLayout">Layout do Documento</label>
                                    <select id="documentoImpressaoLayout" class="form-control form-control-sm" name="documentoImpressaoLayout">
                                        <option value="-1">Selecione</option>
                                        <c:forEach items="${listaDocumentos}" var="documentoImpressaoLayout">
                                            <option value="<c:out value="${documentoImpressaoLayout.chavePrimaria}"/>" <c:if test="${servicoTipo.documentoImpressaoLayout.chavePrimaria == documentoImpressaoLayout.chavePrimaria}">selected="selected"</c:if>>
                                                <c:out value="${documentoImpressaoLayout.descricao}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>Tempo M�dio para execu��o do Servi�o (minutos)</label>
                                    <input class="form-control form-control-sm" type="number" name="quantidadeTempoMedio" id="quantidadeTempoMedio" maxlength="10"
                                           min="10" size="10" value="${servicoTipo.quantidadeTempoMedio}" onkeypress="return formatarCampoInteiro(event, this);">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>Prazo para execu��o do Servi�o (horas)</label>
                                    <input class="form-control form-control-sm" type="number" name="quantidadePrazoExecucao" id="quantidadePrazoExecucao"
                                           min="0" maxlength="10" size="10" value="${servicoTipo.quantidadePrazoExecucao}" onkeypress="return formatarCampoInteiro(event, this);">
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorServicoRegulamento">Servi�o Regulamentado</label>
                                    <select id="indicadorServicoRegulamento" name="indicadorServicoRegulamento" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorServicoRegulamento eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorServicoRegulamento eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorServicoRegulamento eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">

                                    <label for="indicadorAgendamento">Pode ser agendado</label>
                                    <select id="indicadorAgendamento" name="indicadorAgendamento" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorAgendamento eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorAgendamento eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorAgendamento eq 'false'}">selected</c:if>>N�o</option>
                                    </select>

                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">

                                    <label for="indicadorVeiculo">Necessita de ve�culo</label>
                                    <select id="indicadorVeiculo" name="indicadorVeiculo" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorVeiculo eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorVeiculo eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorVeiculo eq 'false'}">selected</c:if>>N�o</option>
                                    </select>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorPesquisaSatisfacao">Gerar Pesquisa de Satisfa��o Autom�tica</label>
                                    <select id="indicadorPesquisaSatisfacao" name="indicadorPesquisaSatisfacao" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorPesquisaSatisfacao eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorPesquisaSatisfacao eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorPesquisaSatisfacao eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorGeraLote">Permite gerar autoriza��o de servi�o em lote</label>
                                    <select id="indicadorGeraLote" name="indicadorGeraLote" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorGeraLote eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorGeraLote eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorGeraLote eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorEncerramentoAuto">Encerramento autom�tico ao executar</label>
                                    <select id="indicadorEncerramentoAuto" name="indicadorEncerramentoAuto" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorEncerramentoAuto eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorEncerramentoAuto eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorEncerramentoAuto eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorEquipamento">Equipamento especial</label>
                                    <select id="indicadorEquipamento" name="indicadorEquipamento" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorEquipamento eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorEquipamento eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorEquipamento eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-md-12">

                                    <label for="indicadorEquipamento">Exige Atualiza��o Cadastral</label>
                                    <select id="indicadorAtualizacaoCadastral" name="indicadorAtualizacaoCadastral" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorAtualizacaoCadastral eq null}">checked</c:if>>Todos</option>
                                        <option value="I" <c:if test="${servicoTipo.indicadorAtualizacaoCadastral eq 'I'}">selected</c:if>>Imediata</option>
                                        <option value="P" <c:if test="${servicoTipo.indicadorAtualizacaoCadastral eq 'P'}">selected</c:if>>Posterior</option>
                                        <option value="N" <c:if test="${servicoTipo.indicadorAtualizacaoCadastral eq 'N'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="telaAtualizacao">Tela para Atualiza��o</label>
                                    <select id="telaAtualizacao" class="form-control form-control-sm" disabled
                                            name="telaAtualizacao">
                                        <c:forEach items="${listaTelaAtualizacao}" var="tela">
                                            <option value="<c:out value="${tela.chavePrimaria}"/>" <c:if test="${servicoTipo.telaAtualizacao.chavePrimaria == tela.chavePrimaria}">selected="selected"</c:if>>
                                                <c:out value="${tela.descricao}"/>
                                            </option>
                                        </c:forEach>
                                    </select>

                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorTipoAssociacaoPontoConsumo">Tipo de Associa��o</label>
                                    <select id="indicadorTipoAssociacaoPontoConsumo" class="form-control form-control-sm" disabled
                                            name="indicadorTipoAssociacaoPontoConsumo">
                                        <c:forEach items="${listaTipoAssociacao}" var="indicador">
                                            <option value="<c:out value="${indicador.key}"/>"
                                                    <c:if test="${servicoTipo.indicadorTipoAssociacaoPontoConsumo eq indicador.key}">selected</c:if>
                                                    >
                                                <c:out value="${indicador.value}"/>
                                            </option>
                                        </c:forEach>
                                    </select>

                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>A��o</label>
                                    <div id="divOperacaoMedidor">
                                        <jsp:include page="/jsp/atendimento/servicotipo/bootstrap/selectOperacaoMedidorBootstrap.jsp"></jsp:include>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorUso">Indicador de Uso</label>
                                    <select id="indicadorUso" class="form-control form-control-sm"
                                            name="habilitado">
                                        <option value="" <c:if test="${servicoTipo.habilitado eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.habilitado eq 'true'}">selected</c:if>>Ativo</option>
                                        <option value="false" <c:if test="${servicoTipo.habilitado eq 'false'}">selected</c:if>>Inativo</option>
                                    </select>
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-12">
                                    <label>N�mero m�ximo de execu��es (por ponto de consumo)</label>
                                    <input class="form-control form-control-sm" type="number" name="numeroMaximoExecucacao" id="numeroMaximoExecucacao" maxlength="2"
                                           min="0" size="2" max="99" value="${servicoTipo.numeroMaximoExecucoes}" onkeypress="return formatarCampoInteiro(event, this);">
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>Prazo de garantia (em dias)</label>
                                    <input class="form-control form-control-sm" type="number" name="prazoGarantia" id="prazoGarantia" maxlength="3"
                                           min="0" size="3" max="365" value="${servicoTipo.prazoGarantia}" onkeypress="return formatarCampoInteiro(event, this);">
                                </div>
                            </div>

							<div class="row">
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>Metade do tempo de Execu��o quando Segmento Residencial - Individual</label>
                                    <select id="indicadorMetadeTempoExecucao" class="form-control form-control-sm"
                                            name="indicadorMetadeTempoExecucao">
                                        <option value="" <c:if test="${servicoTipo.indicadorMetadeTempoExecucao eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorMetadeTempoExecucao eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorMetadeTempoExecucao eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>Servi�o insere informa��es de novo medidor</label>
                                    <select id="indicadorNovoMedidor" class="form-control form-control-sm"
                                            name="indicadorNovoMedidor">
                                        <option value="" <c:if test="${servicoTipo.indicadorNovoMedidor eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorNovoMedidor eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorNovoMedidor eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
							
								<div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>Servi�o pode ser executado no mobile</label>
                                    <select id="indicadorServicoMobile" class="form-control form-control-sm"
                                            name="indicadorServicoMobile">
                                        <option value="" <c:if test="${servicoTipo.indicadorServicoMobile eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorServicoMobile eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorServicoMobile eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                
								<div class="col-xl-3 col-lg-6 col-md-12">
                                    <label>Servi�o possui assinatura obrigat�ria</label>
                                    <select id="indicadorAssinatura" class="form-control form-control-sm"
                                            name="indicadorAssinatura">
                                        <option value="" <c:if test="${servicoTipo.indicadorAssinatura eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorAssinatura eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorAssinatura eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>                                
							</div>
							
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card mt-3">
                        <div class="card-body bg-light">
                            <h5>Campos Obrigat�rios</h5>
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorClienteObrigatorio">Cliente</label>
                                    <select id="indicadorClienteObrigatorio" name="indicadorClienteObrigatorio" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorClienteObrigatorio eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorClienteObrigatorio eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorClienteObrigatorio eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorImovelObrigatorio">Im�vel</label>
                                    <select id="indicadorImovelObrigatorio" name="indicadorImovelObrigatorio" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorImovelObrigatorio eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorImovelObrigatorio eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorImovelObrigatorio eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorContratoObrigatorio">Contrato</label>
                                    <select id="indicadorContratoObrigatorio" name="indicadorContratoObrigatorio" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorContratoObrigatorio eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorContratoObrigatorio eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorContratoObrigatorio eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-12">
                                    <label for="indicadorPontoConsumoObrigatorio">Ponto de Consumo</label>
                                    <select id="indicadorPontoConsumoObrigatorio" name="indicadorPontoConsumoObrigatorio" class="form-control form-control-sm">
                                        <option value="" <c:if test="${servicoTipo.indicadorPontoConsumoObrigatorio eq null}">checked</c:if>>Todos</option>
                                        <option value="true" <c:if test="${servicoTipo.indicadorPontoConsumoObrigatorio eq 'true'}">selected</c:if>>Sim</option>
                                        <option value="false" <c:if test="${servicoTipo.indicadorPontoConsumoObrigatorio eq 'false'}">selected</c:if>>N�o</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input name="chavePrimaria" type="hidden" id="chavePrimaria" >
            <input name="chavesPrimarias" type="hidden">
            <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
            <input type="hidden" name="descricaoTelaAtualizacaoPontoConsumo" id="descricaoTelaAtualizacaoPontoConsumo" value="${descricaoTelaAtualizacaoPontoConsumo}">

            <div class="row mt-3">
                <div class="col-sm-12">

                    <button id="limparFormulario" type="button" class="btn btn-secondary btn-sm float-right">
                        <i class="fa fa-times"></i> Limpar
                    </button>

                    <button id="botaoPesquisar" type="submit" class="btn btn-primary btn-sm float-right mr-1">
                        <i class="fa fa-search"></i> Pesquisar
                    </button>
                </div>
            </div>

            <c:if test="${listaServicoTipo ne null}">

                <div class="text-center loading pt-3">
                    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
                </div>

                <div class="table-responsive pt-3">
                    <table class="table table-bordered table-striped table-hover" id="table-servicos" width="100%"
                           style="opacity: 0;">
                        <thead class="thead-ggas-bootstrap">
                           <th scope="col" class="text-center">
	                            <div class="custom-control custom-checkbox custom-control-inline ">
									<input id="checkAllAuto" type="checkbox"  name="checkAllAuto"
										   class="custom-control-input">
									<label class="custom-control-label p-0" for="checkAllAuto"></label>
								</div>
							</th>
                            <th scope="col" class="text-center">Ativo</th>
                            <th scope="col" class="text-center">Descri��o</th>
                            <th scope="col" class="text-center">Prioridade</th>
                            <th scope="col" class="text-center">Servi�o Regulamentado</th>
                        </thead>
                        <tbody>
                        <c:forEach items="${listaServicoTipo}" var="servicoTipo">
                            <tr>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
                                        <input id="chk${servicoTipo.chavePrimaria}" type="checkbox"  name="chavesPrimarias"
                                               class="custom-control-input"
                                               value="${servicoTipo.chavePrimaria}">
                                        <label class="custom-control-label p-0" for="chk${servicoTipo.chavePrimaria}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <c:choose>
                                        <c:when test="${servicoTipo.habilitado == true}">
                                            <i title="Ativo" class="fa fa-check-circle text-success fa-lg"></i>
                                        </c:when>
                                        <c:otherwise>
                                            <i title="Inativo" class="fa fa-times-circle text-danger fa-lg"></i>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="text-center">
                                    <a name="linkTabelaServicoTipo" href="javascript:void(0)" data-chavePrimaria="${servicoTipo.chavePrimaria}">
                                        <span class="linkInvisivel"></span>
                                        <c:out value="${servicoTipo.descricao}"/>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <a name="linkTabelaServicoTipo" href="javascript:void(0)" data-chavePrimaria="${servicoTipo.chavePrimaria}"><span class="linkInvisivel"></span>
                                        <c:out value="${servicoTipo.servicoTipoPrioridade.descricao}"/>
                                    </a>

                                </td>
                                <td class="text-center">
                                    <a name="linkTabelaServicoTipo" href="javascript:void(0)" data-chavePrimaria="${servicoTipo.chavePrimaria}"><span class="linkInvisivel"></span>
                                        <c:choose>
                                            <c:when test="${servicoTipo.indicadorServicoRegulamento == 'true'}">
                                                <c:out value="Sim"/><br />
                                            </c:when>
                                            <c:when test="${servicoTipo.indicadorServicoRegulamento == 'false'}">
                                                <c:out value="N�o"/><br />
                                            </c:when>
                                        </c:choose>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-sm-12">
                    <vacess:vacess param="exibirInclusaoServicoTipo">
                        <button id="buttonIncluir" type="button" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-plus"></i> Incluir
                        </button>
                    </vacess:vacess>
                    <c:if test="${not empty listaServicoTipo}">
                        <vacess:vacess param="exibirAlteracaoServicoTipo">
                            <button id="buttonAlterar" type="button" class="btn btn-primary btn-sm float-right mr-1">
                                <i class="fa fa-edit"></i> Alterar
                            </button>
                        </vacess:vacess>
                        <vacess:vacess param="removerServicoTipo">
                            <button id="buttonRemover" type="button" class="btn btn-danger btn-sm float-right mr-1">
                                <i class="fa fa-trash"></i> Remover
                            </button>
                        </vacess:vacess>
                    </c:if>

                </div>
            </div>
        </div>
    </div>
</form:form>
<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/autorizacaoServico/tipoServico/pesquisaTipoServico/index.js"></script>
