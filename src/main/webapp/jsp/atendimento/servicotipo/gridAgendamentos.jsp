<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
$(document).ready(function(){
	var botaoAlterarTurno = document.getElementById("botaoAlterarTurno");
	botaoAlterarTurno.disabled = true;
});
function incluirTurno() {
	alert("incluirTurno");
	var chavePrimaria = document.forms[0].chavePrimaria.value;
	var chaveEquipe = document.forms[0].equipe.value;
	alert(chaveEquipe);
	var chaveTurno = document.forms[0].turno.value;
	var quantidadeAgendamentosTurno = document.forms[0].quantidadeAgendamentosTurno.value;
	var indexList = -1;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "incluirTurno?equipe="+chaveEquipe+"&turno="+chaveTurno+"&quantidadeAgendamentosTurno="+quantidadeAgendamentosTurno+"&chavePrimaria="+chavePrimaria+"&indexList="+indexList;
	alert(url);
	if(chaveTurno != "-1" && quantidadeAgendamentosTurno > 0) {
		carregarFragmento("gridAgendamentos",url);
	} else {
		alert('Os campos Turno e Quantidade s�o de preenchimento obrigat�rio');
	}
}

function alterarTurno() {
	alert("alterarTurno");
	var chavePrimaria = document.forms[0].chavePrimaria.value;
	var chaveEquipe = document.forms[0].equipe.value;
	var chaveTurno = document.forms[0].turno.value;
	var quantidadeAgendamentosTurno = document.forms[0].quantidadeAgendamentosTurno.value;
	var indexList = document.forms[0].indexList.value;
	var noCache = "noCache=" + new Date().getTime();
	
	var url="incluirTurno?equipe="+chaveEquipe+"&turno="+chaveTurno+"&quantidadeAgendamentosTurno="+quantidadeAgendamentosTurno+"&chavePrimaria="+chavePrimaria+"&indexList="+indexList+"&"+noCache;
	
	if(chaveTurno != "-1" && quantidadeAgendamentosTurno > 0) {
		carregarFragmento("gridAgendamentos",url);
	} else {
		alert('Os campos Turno e Quantidade s�o de preenchimento obrigat�rio');
	} 
}

function limparTurno() {
	document.forms[0].equipe.value = -1;
	document.forms[0].turno.value = -1;
	document.forms[0].quantidadeAgendamentosTurno.value = "";
}	

function exibirAlteracaoTurno(indice, chaveEquipe, idTurno, quantidade) {

	document.forms[0].indexList.value = indice;
	document.forms[0].equipe.value = chaveEquipe;
	document.forms[0].turno.value = idTurno;
	document.forms[0].quantidadeAgendamentosTurno.value = quantidade;
	
	var botaoAlterarTurno = document.getElementById("botaoAlterarTurno");
	var botaoLimparTurno = document.getElementById("botaoLimparTurno");	
	var botaoIncluirTurno = document.getElementById("botaoIncluirTurno");	
	botaoIncluirTurno.disabled = true;
	botaoLimparTurno.disabled = false;
	botaoAlterarTurno.disabled = false;
}

function removerTurno(chavePrimariaTurno) {
	var noCache = "noCache=" + new Date().getTime();
	$("#gridAgendamentos").load("removerTurno?chavePrimariaTurno="+chavePrimariaTurno+"&"+noCache);
}
</script>

<fieldset class="conteinerBloco">
			<fieldset id="servicoTipoEquipamentoInclusao">
				<legend class="conteinerBlocoTitulo">Quantidade de Agendamentos por Turno</legend>
				<fieldset id="agendamentosTurnos" class="conteinerDados">
	<label class="rotulo rotuloHorizontal" for="equipe"><span class="campoObrigatorioSimbolo2"> * </span>Equipe:</label>
		<select class="campoSelect campoHorizontal" name="equipe" id="equipe">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaEquipe}" var="equipe">
				<option value="<c:out value="${equipe.chavePrimaria}"/>" >
					<c:out value="${equipe.nome}"/>
				</option>
           </c:forEach>
		</select>
	<label class="rotulo rotuloHorizontal" for="turno"><span class="campoObrigatorioSimbolo2"> * </span>Turno:</label>
	<select class="campoSelect campoHorizontal" name="turno" id="turno">
    	<option value="-1">Selecione</option>
		<c:forEach items="${listaTurnosCadastro}" var="turno">
			<option value="<c:out value="${turno.chavePrimaria}"/>" >
				<c:out value="${turno.descricao}"/>
			</option>
	    </c:forEach>
    </select>
    <label class="rotulo rotuloHorizontal" id="rotuloQuantidadeAgendamento" for="quantidadeAgendamentosTurno">&nbsp;&nbsp;<span class="campoObrigatorioSimbolo2">    * </span>Quantidade:</label>
	<input class="campoTexto campoHorizontal" id="quantidadeAgendamentosTurno" name="quantidadeAgendamentosTurno" type="text" size="10" maxlength="5" onkeypress="return formatarCampoInteiro(event, this);">

	<fieldset class="conteinerBotoes"> 
		<input class="bottonRightCol" id="botaoLimparTurno" name="botaoLimparTurnos" value="Limpar" type="button" onclick="limparTurno();">
		<input class="bottonRightCol bottonLeftColUltimo" id="botaoIncluirTurno" name="botaoIncluirTurno" value="Adicionar" type="button" onclick="incluirTurno();"	>
	   	<input class="bottonRightCol bottonLeftColUltimo" id="botaoAlterarTurno" name="botaoAlterarTurno" value="Alterar" type="button" onclick="alterarTurno();">
	</fieldset>
	
	<c:set var="i" value="0" />
	<display:table class="dataTableGGAS" name="sessionScope.listaAgendamentos" sort="list" id="servicoTipoAgendamentoTurno" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		<display:column property="equipe.nome" sortable="false" title="Equipe" />
		<display:column property="turno.descricao" sortable="false" title="Quantidade de agendamentos por turnos" />
		<display:column property="quantidadeAgendamentosTurno" sortable="false" title="Quantidade"  style="width: 130px"/>
	 		
		<display:column style="text-align: center; width: 25px">
			<a href="javascript:exibirAlteracaoTurno('${i}','${servicoTipoAgendamentoTurno.equipe.chavePrimaria}','${servicoTipoAgendamentoTurno.turno.chavePrimaria}','${servicoTipoAgendamentoTurno.quantidadeAgendamentosTurno}');">
				<img title="Alterar Turno" alt="Alterar Turno"  src="<c:url value="/imagens/16x_editar.gif"/>">
			</a> 
		</display:column>
		
		<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
			<a onclick="return confirm('Deseja excluir o agendametno?');" href="javascript:removerTurno('${servicoTipoAgendamentoTurno.turno.chavePrimaria}');">
				<img title="Excluir agendamento" alt="Excluir agendamento"  src="<c:url value="/imagens/deletar_x.png"/>">
			</a> 
		</display:column>
		<c:set var="i" value="${i+1}" />
	</display:table>
	</fieldset>
	</fieldset>
</fieldset>