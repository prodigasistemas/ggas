<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<hr />
<div class="form-row">
    <div class="col-md-12">
        <label for="equipamento">Equipamento especial necess�rio: <span class="text-danger">*</span></label>
        <select class="form-control form-control-sm" name="equipamento" id="equipamento">
            <option value="-1">Selecione</option>
            <c:forEach items="${listaEquipamentosCadastro}" var="equipamento">
                <option value="<c:out value="${equipamento.chavePrimaria}"/>">
                    <c:out value="${equipamento.descricao}"/>
                </option>
            </c:forEach>
        </select>
        <div class="invalid-feedback" id="equipamentoFeedback">
            Por favor informe o equipamento
        </div>
    </div>

    <div class="col-md-12 mt-1">
        <button class="btn btn-primary btn-sm mb-1 mr-1"
                id="botaoIncluirEquipamento" name="botaoIncluirEquipamento"
                type="button">
            <i class="fa fa-plus-circle"></i> Adicionar
        </button>
    </div>
</div>


<div class="text-center loading">
    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive mt-1">
    <c:set var="indexListaChamadoContato" value="0"/>
    <table class="table table-bordered table-striped table-hover" id="datatable-equipamento-especial" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th width="80%" scope="col" class="text-center">Equipamento necess�rio</th>
            <th width="15%" scope="col" class="text-center">A��o</th>
        </tr>
        </thead>
        <tbody>
        <c:set var="i" value="0"/>
        <c:forEach var="servicoTipoEquipamento" items="${sessionScope.listaEquipamentos}">
            <tr>
                <td>${servicoTipoEquipamento.equipamento.descricao}</td>
                <td class="text-center">
                    <button type="button" class="acaoRemoverEquipamento btn btn-danger btn-sm mr-1" data-id="${servicoTipoEquipamento.equipamento.chavePrimaria}">
                        <i class="fa fa-trash"></i> Excluir
                    </button>
                </td>
            </tr>
            <c:set var="i" value="${i+1}"/>
        </c:forEach>
        </tbody>
    </table>
</div>


<div id="modal-confirmar-exclusao-equipamento-especial" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmar exclus�o</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id-equipamento-especial-excluir" />
                <p>Deseja realmente confirmar exclus�o do equipamento especial?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">N�o</button>
                <button id="confirmar-exclusao-material-necessario" type="button" class="btn btn-primary">Sim</button>
            </div>
        </div>
    </div>
</div>
