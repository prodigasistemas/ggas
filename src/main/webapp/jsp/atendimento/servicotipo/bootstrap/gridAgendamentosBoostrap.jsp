<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
    $(document).ready(function () {
        var botaoAlterarTurno = document.getElementById("botaoAlterarTurno");
        botaoAlterarTurno.disabled = true;
    });
</script>


<div class="form-row">
	<div class="col-md-6">
		<label for="equipe">Equipe:<span class="text-danger">*</span></label>
		<select class="form-control form-control-sm" name="equipe" id="equipe">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaEquipes}" var="equipe">
				<option value="<c:out value="${equipe.chavePrimaria}"/>">
					<c:out value="${equipe.nome}"/>
				</option>
			</c:forEach>
		</select>
        <div class="invalid-feedback" id="equipeFeedback">
            Por favor informe a equipe
        </div>		
    </div>
    <div class="col-md-3">
        <label for="turno">Turno: <span class="text-danger">*</span></label>
        <select class="form-control form-control-sm" name="turno" id="turno">
            <option value="-1">Selecione</option>
            <c:forEach items="${listaTurnosCadastro}" var="turno">
                <option value="<c:out value="${turno.chavePrimaria}"/>">
                    <c:out value="${turno.descricao}"/>
                </option>
            </c:forEach>
        </select>
        <div class="invalid-feedback" id="turnoFeedback">
            Por favor informe o turno
        </div>
    </div>
    <div class="col-md-3">
        <label id="rotuloQuantidadeAgendamento" for="quantidadeAgendamentosTurno">Quantidade: <span class="text-danger">*</span></label>
        <input class="form-control form-control-sm input-sem-spinner" id="quantidadeAgendamentosTurno" name="quantidadeAgendamentosTurno" type="number"
               size="10" maxlength="5" onkeypress="return formatarCampoInteiro(event, this);"
               onpaste="return formatarCampoInteiro(event, this);">
        <div class="invalid-feedback" id="quantidadeFeedback">
            Por favor informe a quantidade
        </div>
    </div>

    <div class="col-md-12 mt-1">
        <button class="btn btn-danger btn-sm mb-1 mr-1" id="botaoLimparTurno" name="botaoLimparTurnos" type="button">
            <i class="fa fa-times-circle"></i> Limpar
        </button>
        <button class="btn btn-primary btn-sm mb-1 mr-1" id="botaoIncluirTurno" name="botaoIncluirTurno" type="button">
            <i class="fa fa-plus-circle"></i> Adicionar
        </button>
        <button class="btn btn-primary btn-sm mb-1 mr-1" id="botaoAlterarTurno" name="botaoAlterarTurno" type="button">
            <i class="fa fa-edit"></i> Alterar
        </button>
    </div>
</div>

<div class="table-responsive mt-1">
    <table class="table table-bordered table-striped table-hover" id="servicoTipoAgendamentoTurno" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
        	<th scope="col" class="text-center">Equipe</th>
            <th scope="col" class="text-center">Quantidade de agendamentos por turnos</th>
            <th scope="col" class="text-center">Quantidade</th>
            <th scope="col" class="text-center">A��es</th>
        </tr>
        </thead>
        <tbody>
        <c:set var="i" value="0"/>
        <c:forEach var="servicoTipoAgendamentoTurno" items="${sessionScope.listaAgendamentos}">
            <tr>
                <td width="40%">${servicoTipoAgendamentoTurno.equipe.nome}</td>
                <td width="20%">${servicoTipoAgendamentoTurno.turno.descricao}</td>
                <td width="20%" class="text-right">${servicoTipoAgendamentoTurno.quantidadeAgendamentosTurno}</td>
                <td width="20%" class="text-center">
                    <button type="button" class="acaoAlterarTurnoAgendamento btn btn-primary btn-sm mr-1 mt-1" data-indice="${i}" data-idequipe="${servicoTipoAgendamentoTurno.equipe.chavePrimaria}" data-idturno="${servicoTipoAgendamentoTurno.turno.chavePrimaria}" data-quantidade="${servicoTipoAgendamentoTurno.quantidadeAgendamentosTurno}">
                        <i class="fa fa-edit"></i> Alterar
                    </button>

                    <button type="button" class="acaoRemoverTurnoAgendamento btn btn-danger btn-sm mt-1" data-idservicoagendamento="${servicoTipoAgendamentoTurno.chavePrimaria}" data-idturno="${servicoTipoAgendamentoTurno.turno.chavePrimaria}">
                        <i class="fa fa-trash"></i> Excluir
                    </button>
                </td>
            </tr>
            <c:set var="i" value="${i+1}"/>
        </c:forEach>
        </tbody>
    </table>
</div>

