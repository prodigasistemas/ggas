<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
$(document).ready(function(){
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	botaoAlterarMaterial.disabled = true;
});

function incluirMaterial() {
	var chavePrimaria = document.forms[0].chavePrimaria.value;
	var chaveMaterial = document.forms[0].material.value;
	var quantidadeMaterial = document.forms[0].quantidadeMaterial.value;
	var indexList = -1;
	var noCache = "noCache=" + new Date().getTime();
	
	
	var url = "incluirMaterialServicoTipo?material="+chaveMaterial+"&quantidadeMaterial="+quantidadeMaterial+"&chavePrimaria="+chavePrimaria+"&indexList="+indexList+"&"+noCache;
	
	
	if(chaveMaterial!="-1" && quantidadeMaterial > 0) {
	  carregarFragmento('gridMateriais',url);
	} else if(chaveMaterial == "-1" || quantidadeMaterial == ""){
		alert('Os campos Descri��o do Material e Quantidade s�o de preenchimento obrigat�rio');
	} else if(quantidadeMaterial == 0){
		alert('O campo Quantidade deve ser maior que zero.');
	}
}

function alterarMaterial() {
	var chavePrimaria = document.forms[0].chavePrimaria.value;
	var chaveMaterial = document.forms[0].material.value;
	var quantidadeMaterial = document.forms[0].quantidadeMaterial.value;
	var indexList = document.forms[0].indexList.value;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "incluirMaterialServicoTipo?material="+chaveMaterial+"&quantidadeMaterial="+quantidadeMaterial+"&chavePrimaria="+chavePrimaria+"&indexList="+indexList+"&"+noCache;
	
	if(chaveMaterial!="-1" && chaveMaterial != "" && quantidadeMaterial > 0) {
		carregarFragmento('gridMateriais',url);
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	botaoAlterarMaterial.disabled = true;
	} else if(chaveMaterial == "-1" || quantidadeMaterial == ""){
		alert('Os campos Descri��o do Material e Quantidade s�o de preenchimento obrigat�rio');
	} else if(quantidadeMaterial == 0){
		alert('O campo Quantidade deve ser maior que zero.');
	}
}

function limparMaterial() {
	document.forms[0].material.value = -1;
	document.forms[0].quantidadeMaterial.value = "";
}

function exibirAlteracaoMaterialServicoTipo(indice,idMaterial,quantidade) {
	document.forms[0].indexList.value = indice;
	document.forms[0].material.value = idMaterial;
	document.forms[0].quantidadeMaterial.value = quantidade;
	
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	var botaoLimparMaterial = document.getElementById("botaoLimparMaterial");	
	var botaoIncluirMaterial = document.getElementById("botaoIncluirMaterial");	
	botaoIncluirMaterial.disabled = true;
	botaoLimparMaterial.disabled = false;
	botaoAlterarMaterial.disabled = false;
}

function removerMaterialServicoTipo(chavePrimariaMaterial) {
	var noCache = "noCache=" + new Date().getTime();
	$("#gridMateriais").load("removerMaterialServicoTipo?chavePrimariaMaterial="+chavePrimariaMaterial+"&"+noCache);
}

</script>
<fieldset class="conteinerBloco">
			<fieldset id="servicoTipoEquipamentoInclusao">
				<legend class="conteinerBlocoTitulo">Materiais Necess�rios</legend>
				<fieldset id="materiaisServico" class="conteinerDados">
		
	<label class="rotulo campoObrigatorio rotuloHorizontal" for="material"><span class="campoObrigatorioSimbolo2"> * </span>Descri��o do Material:</label>
	<select class="campoSelect campoHorizontal" name="material" id="material">
    	<option value="-1">Selecione</option>
		<c:forEach items="${listaMateriaisCadastro}" var="material">
			<option value="<c:out value="${material.chavePrimaria}"/>" >
				<c:out value="${material.descricao}"/>
			</option>
	    </c:forEach>
    </select>
    <label class="rotulo campoObrigatorio rotuloHorizontal" id="rotuloQuantidadeMaterial" for="quantidadeMaterial">&nbsp;&nbsp; <span class="campoObrigatorioSimbolo2"> * </span>Quantidade:</label>
	<input class="campoTexto campoHorizontal" id="quantidadeMaterial" name="quantidadeMaterial" type="text" size="10" maxlength="5" onkeypress="return formatarCampoInteiro(event, this);">

	<fieldset class="conteinerBotoes"> 
		<input class="bottonRightCol" id="botaoLimparMaterial" name="botaoLimparMaterial" value="Limpar" type="button" onclick="limparMaterial();">
		<input class="bottonRightCol bottonLeftColUltimo" id="botaoIncluirMaterial" name="botaoIncluirMaterial" value="Adicionar" type="button" onclick="incluirMaterial();">
	   	<input class="bottonRightCol bottonLeftColUltimo" id="botaoAlterarMaterial" name="botaoAlterarMaterial" value="Alterar" type="button" onclick="alterarMaterial();">
	</fieldset>
	
	<c:set var="i" value="0" />
	<display:table class="dataTableGGAS" name="sessionScope.listaMateriais" sort="list" id="servicoTipoMaterial" pagesize="5" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		<display:column property="material.descricao" sortable="false" title="Materiais Necess�rios" />
		<display:column property="material.unidadeMedida.descricao" sortable="false" title="Unidade"  style="width: 100px"/>
		<display:column property="quantidadeMaterial" sortable="false" title="Quantidade"  style="width: 100px"/>
	 		
		<display:column style="text-align: center; width: 25px">
			<a href="javascript:exibirAlteracaoMaterialServicoTipo('${i}','${servicoTipoMaterial.material.chavePrimaria}','${servicoTipoMaterial.quantidadeMaterial}');">
				<img title="Alterar Material" alt="Alterar Material"  src="<c:url value="/imagens/16x_editar.gif"/>">
			</a> 
		</display:column>
		
		<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
			<a onclick="return confirm('Deseja excluir o material?');" href="javascript:removerMaterialServicoTipo('${servicoTipoMaterial.material.chavePrimaria}');">
				<img title="Excluir Material" alt="Excluir Material"  src="<c:url value="/imagens/deletar_x.png"/>">
			</a> 
		</display:column>
		<c:set var="i" value="${i+1}" />
	</display:table>
	</fieldset>
	</fieldset>
</fieldset>