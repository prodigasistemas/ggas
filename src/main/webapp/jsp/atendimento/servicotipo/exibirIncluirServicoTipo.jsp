<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">


    $(document).ready(function () {
        <c:choose>
            <c:when test="${servicoTipo.indicadorEquipamento ne true}">
                $("#gridEquipamentos").hide();
            </c:when>
            <c:otherwise>
                $("#gridEquipamentos").show();
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${servicoTipo.indicadorAtualizacaoCadastral == 'N'}">
                document.forms[0].telaAtualizacao.value = -1;
                $("#divTelaAtualizacao").hide();
                $('#divAssociacaoMedicao').hide();
            </c:when>
            <c:otherwise>
                $("#divTelaAtualizacao").show();
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${servicoTipo.telaAtualizacao.descricao != descricaoTelaAtualizacaoPontoConsumo}">
                esconderDadosOperacaoMedidor();
            </c:when>
            <c:otherwise>
                exibirDadosOperacaoMedidor();
                obterOperacoes();
            </c:otherwise>
        </c:choose>
        
        <c:choose>
	        <c:when test="${servicoTipo.indicadorProtocolo eq false}">
	        	esconderInformacoesProtocolo();
	        </c:when>
	        <c:otherwise>
	       	 	exibirInformacoesProtocolo();
	        </c:otherwise>
    	</c:choose>
    	
        <c:choose>
        <c:when test="${servicoTipo.indicadorAssinatura eq true}">
        	exibirInformacoesAssinatura();
        </c:when>
        <c:otherwise>
        	esconderInformacoesAssinatura();
        </c:otherwise>
	</c:choose>    	
    	
    	

        <c:choose>
	        <c:when test="${servicoTipo.indicadorAviso eq false}">
	        	esconderInformacoesAviso();
	        </c:when>
	        <c:otherwise>
	       	 	exibirInformacoesAviso();
	        </c:otherwise>
		</c:choose>  
    	
        <c:choose>
	        <c:when test="${servicoTipo.indicadorProtocoloObrigatorio eq false}">
	        	esconderPrazoRetornoProtocolo();
	        </c:when>
	        <c:otherwise>
	        	exibirPrazoRetornoProtocolo();
	        </c:otherwise>
		</c:choose>     	
    	

        $("#indicadorPontoConsumoObrigatorio").click(function () {
            $("#indicadorImovelObrigatorio").attr("checked", true)
        });


        $("#indicadorImovelObrigatorioFalse").click(function () {
            $("#indicadorPontoConsumoObrigatorioFalse").attr("checked", true)
        });

        $("#valorCobranca").hide();
        $("[name='indicadorProximaFatura']").attr("disabled", "disabled");
        if ($("input[name=indicadorCobranca]:checked").val() === 'true') {
            exibirCobranca();
        } else {
            ocultarCobranca();
        }

        if (document.forms["servicoTipoForm"].indicadorFormularioObrigatorio[1].checked) {
            desabilitarFormulario();
        } else {
            habilitarFormulario();
        }

        desabilitarAcaoSpinnerInputNumber();
        
        var isChecked = document.getElementById("indicadorEmailPersonalizado").checked;
        mostrarListaEmail(isChecked);

    });


    function habilitarFatura() {
        $('#valorServico').removeAttr("disabled");
        $('#valorServico').css('background', '#FFF');
        $('#valorServico').focus();
    }

    function desabilitarFatura() {
        $('#valorServico').attr("disabled", "disabled");
        $('#valorServico').css('background', '#EBEBE4');
    }


    function incluir() {
        var indicadorOperacaoMedidor = document.forms[0].indicadorOperacaoMedidor.value;
        if (indicadorOperacaoMedidor == '-1') {
            document.forms[0].indicadorOperacaoMedidor.value = null;
        }
        if ($("#servicoTipoForm")[0].checkValidity()) {
            submeter('servicoTipoForm', 'incluirServicoTipo');
        }
    }

    function alterar() {
        if ($("#servicoTipoForm")[0].checkValidity()) {
            submeter('servicoTipoForm', 'alterarServicoTipo');
        }
    }

    function cancelar() {
        submeter('servicoTipoForm', 'exibirPesquisaServicoTipo');
    }

    function exibirDadosEquipamento() {
        $('#gridEquipamentos').slideDown('slow');
    }

    function esconderDadosEquipamento() {

        var chaveEquipamento = 0;
        var noCache = "noCache=" + new Date().getTime();
        $("#gridEquipamentos").load("removerTodosEquipamento?chavePrimariaEquipamento=" + chaveEquipamento + "&" + noCache);

        $('#gridEquipamentos').slideUp('slow');

    }

    function limparFormulario() {

        desabilitarFormulario();

        document.forms[0].documentoImpressaoLayout.value = -1;
        document.forms[0].servicoTipoPrioridade.value = -1;
        document.forms[0].descricao.value = "";
        document.forms[0].orientacao.value = "";
        document.forms[0].indicadorEquipamento.checked = false;
        document.forms[0].quantidadeTempoMedio.value = "";
        document.forms[0].quantidadePrazoExecucao.value = "";
        document.forms[0].material.value = -1;
        document.forms[0].quantidadeMaterial.value = "";
        document.forms[0].turno.value = -1;
        document.forms[0].quantidadeAgendamentosTurno.value = "";

        $("#formulario").val('-1');
        $("#indicadorFormularioObrigatorioFalse").attr('checked', true);
        $("#indicadorClienteObrigatorioFalse").attr('checked', true);
        $("#indicadorImovelObrigatorioFalse").attr('checked', true);
        $("#indicadorContratoObrigatorioFalse").attr('checked', true);
        $("#indicadorPontoConsumoObrigatorioFalse").attr('checked', true);

        $("#indicadorEmailCadastrarFalse").attr('checked', true);
        $("#indicadorEmailEmExecucaoFalse").attr('checked', true);
        $("#indicadorEmailExecucaoFalse").attr('checked', true);
        $("#indicadorEmailEncerrarFalse").attr('checked', true);
        $("#indicadorEmailAlterarFalse").attr('checked', true);

        $("#indicadorServicoRegulamentoFalse").attr('checked', true);
        $("#indicadorAgendamento").attr('checked', true);
        $("#indicadorCobrancaFalse").attr('checked', true);
        $("#indicadorEquipamentoFalse").attr('checked', true);
        $("#indicadorVeiculoFalse").attr('checked', true);
        $("#indicadorPesquisaSatisfacao").attr('checked', true);
        $("#indicadorGeraLoteFalse").attr('checked', true);
        $("#indicadorEncerramentoAuto").attr('checked', true);
        $("#indicadorMetadeTempoExecucao").attr('checked', false);
        $("#indicadorAtualizacaoCadastralFalse").attr('checked', true);
        $('#divTelaAtualizacao').hide();

    }

    function exibirDadosTelaAtualizacao() {

// 	var cliente = document.getElementById("indicadorClienteObrigatorio").checked;
// 	var contrato = document.getElementById("indicadorContratoObrigatorio").checked;

// 	AjaxService.obterListaAtualizacaoCadastral( cliente+";"+contrato,
// 		function(pontos) {
// 			var selectOperacoes = document.getElementById("telaAtualizacao");
// 			removeChildrenFromNode(selectOperacoes);
// 			for (key in pontos){
// 				var id = key;
// 				var novaOpcao = new Option(pontos[key], key);
// 				novaOpcao.label = pontos[key];
// 				selectOperacoes.appendChild(novaOpcao);
//         	}
// 			ordenar();
// 			selectOperacoes.insertBefore(new Option("Selecione", "-1"), selectOperacoes.firstChild );
// 			$("#telaAtualizacao").get(0).selectedIndex = 0;
//     	}
//     );

        $('#divTelaAtualizacao').show();
    }

    function ordenar() {
        $("#telaAtualizacao").html($('#telaAtualizacao option').sort(function (x, y) {
            return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
        }));
    }

    function removeChildrenFromNode(node) {
        if (node.hasChildNodes()) {
            while (node.childNodes.length >= 1) {
                node.removeChild(node.firstChild);
            }
        }
    }

    function esconderDadosTelaAtualizacao() {
        document.forms[0].telaAtualizacao.value = -1;
        $('#divTelaAtualizacao').hide();
        esconderDadosOperacaoMedidor();
    }

    function exibirDadosOperacaoMedidor() {
        $('#divAssociacaoMedicao').show();
    }

    function esconderDadosOperacaoMedidor() {
        document.forms[0].indicadorTipoAssociacaoPontoConsumo1.checked = false;
        document.forms[0].indicadorTipoAssociacaoPontoConsumo2.checked = false;
        document.forms[0].indicadorOperacaoMedidor.value = -1;
        $('#divAssociacaoMedicao').hide();
    }

    function manipularExibicaoDivOperacaoMedidor() {
        var descricaoTelaAtualizacao = $("#telaAtualizacao option:selected").text();
        var descricao = "<c:out value="${ descricaoTelaAtualizacaoPontoConsumo }" />";
        if (descricaoTelaAtualizacao.trim() == descricao) {
            exibirDadosOperacaoMedidor();
        } else {
            esconderDadosOperacaoMedidor();
        }
    }

    function obterOperacoes() {
        var noCache = "noCache=" + new Date().getTime();
        var codigoAssociacao = getCheckedValue(document.servicoTipoForm.indicadorTipoAssociacaoPontoConsumo);
        var chaveOperacao = "<c:out value="${ servicoTipo.indicadorOperacaoMedidor }" />";
        var chaveMedidorMotivoOperacao = "<c:out value="${ servicoTipo.motivoOperacaoMedidor.chavePrimaria }" />";
        if (codigoAssociacao != "") {
            $("#divOperacaoMedidor").load("obterOperacoes?tipoAssociacao=" + codigoAssociacao + "&chaveOperacao=" + chaveOperacao + "&chaveMedidorMotivoOperacao=" + chaveMedidorMotivoOperacao + "&" + noCache);
        }
    }

    function selecaoImovel() {
        var indicadorPontoConsumo = document.getElementById('indicadorPontoConsumoObrigatorio').value;
        if (indicadorPontoConsumo == "true") {
            document.getElementById('indicadorImovelObrigatorio').value = true;
        }
    }


    function ocultarCobranca() {
        $("#valorCobranca").hide();
        $("#indicadorProximaFaturaSim").attr("disabled", "disabled");
        $("#indicadorProximaFaturaNao").attr("disabled", "disabled");
    }


    function exibirCobranca() {
        $("#valorCobranca").show();
        $("#indicadorProximaFaturaSim").removeAttr("disabled");
        $("#indicadorProximaFaturaNao").removeAttr("disabled");
        $("#indicadorProximaFaturaSim").attr("checked", true)
    }

    function habilitarFormulario() {
        document.getElementById("formulario").disabled = false;
        document.getElementById('spanFormulario').innerHTML = '*';
    }

    function desabilitarFormulario() {
        document.getElementById("formulario").disabled = true;
        document.getElementById("formulario").value = -1;
        document.getElementById('spanFormulario').innerHTML = '';
    }
    
    function esconderInformacoesProtocolo() {
        $('#divInformacoesProtocolo').hide();
        $("#indicadorProtocoloObrigatorioFalse").attr('checked', true);
        $("#indicadorTipoProtocoloFalse").attr('checked', true);
        $("#tempoEmissaoAS").val("0");
        $("#tempoRetornoProtocolo").val("0");
    }

    function exibirInformacoesProtocolo() {
        $('#divInformacoesProtocolo').show();
    }
    
    function esconderPrazoRetornoProtocolo() {
        $('#divTempoRetornoProtocolo').hide();
        $("#tempoRetornoProtocolo").val("0");
    }

    function exibirPrazoRetornoProtocolo() {
        $('#divTempoRetornoProtocolo').show();
    }


    function esconderInformacoesAviso() {
        $('#divInformacoesDoAviso').hide();
        $("#documentoImpressaoAviso").val("-1").change();
    }

    function exibirInformacoesAviso() {
        $('#divInformacoesDoAviso').show();
    }
    
    function esconderInformacoesAssinatura() {
        $('#divIndicadorAssinatura').hide();
        $("#indicadorAssinaturaFalse").attr('checked', true);
    }

    function exibirInformacoesAssinatura() {
        $('#divIndicadorAssinatura').show();
    }

    function mostrarListaEmail(isVisivel) {
        var inputDiv = document.getElementById("emailPersonalizadoDiv");
        inputDiv.style.display = isVisivel ? "block" : "none";
    }
    
</script>

<div class="bootstrap">

    <form:form method="post" modelAttribute="ServicoTipo" action="incluirServicoTipo" id="servicoTipoForm" name="servicoTipoForm">

        <input type="hidden" name="chavesPrimarias" id="chavesPrimarias" value="${servicoTipo.chavePrimaria}">
        <input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${servicoTipo.chavePrimaria}">
        <input type="hidden" name="chavePrimariaMaterial" id="chavePrimariaMaterial">
        <input type="hidden" name="chavePrimariaEquipamento" id="chavePrimariaEquipamento">
        <input type="hidden" name="chavePrimariaTurno" id="chavePrimariaTurno">
        <input type="hidden" name="indexList" id="indexList">
        <input type="hidden" name="descricaoTelaAtualizacaoPontoConsumo" id="descricaoTelaAtualizacaoPontoConsumo"
               value="${descricaoTelaAtualizacaoPontoConsumo}">


        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">
                    <c:choose>
                        <c:when test="${fluxoAlteracao eq true}">Alterar Tipo de Servi�o</c:when>
                        <c:otherwise>Incluir Tipo de Servi�o</c:otherwise>
                    </c:choose>
                </h5>
            </div>

            <div class="card-body">
                <div class="alert alert-primary" role="alert">
                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                    <div class="d-inline">
                        Informe os dados abaixo e clique em <strong>Salvar</strong> para finalizar.
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-solicitante" data-toggle="tab"
                                   href="#contentTabSolicitante" role="tab" aria-controls="contentTabSolicitante"
                                   aria-selected="true"><i class="fa fa-info-circle"></i> Caracter�sticas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-campo-obrigatorio" data-toggle="tab"
                                   href="#contentTabCampoObrigatorio" role="tab" aria-controls="contentTabCampoObrigatorio"
                                   aria-selected="false"><i class="fa fa-font"></i>* Campos Obrigat�rios</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-envio-email" data-toggle="tab"
                                   href="#contentTabEnvioEmail" role="tab" aria-controls="contentTabEnvioEmail"
                                   aria-selected="false"><i class="fa fa-at"></i> Opera��o de Envio de E-mail</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-material-necessario" data-toggle="tab"
                                   href="#contentTabMaterialNecessario" role="tab" aria-controls="contentTabMaterialNecessario"
                                   aria-selected="false"><i class="fa fa-archive"></i> Materiais Necess�rios</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-quantidade-agendamento" data-toggle="tab"
                                   href="#contentTabQuantidadeAgendamento" role="tab" aria-controls="contentTabQuantidadeAgendamento"
                                   aria-selected="false"><i class="fa fa-calendar-alt"></i> Quantidade de Agendamentos por Turno</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-conteudo-email" data-toggle="tab"
                                   href="#contentTabConteudoEmail" role="tab" aria-controls="contentTabConteudoEmail"
                                   aria-selected="false"><i class="fa fa-at"></i>Layout Email</a>
                            </li>                            
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="content-incluir-chamado">
                            <div class="tab-pane fade show active" id="contentTabSolicitante" role="tabpanel"
                                 aria-labelledby="contentTabSolicitante">
                                <div class="row">
                                    <div class="col-md-12" id="incAltServicoTipoCol1"> <!-- Talvez id seja desnecess�rio... -->
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label for="descricao">Descri��o: <span class="text-danger">*</span></label>
                                                <input class="form-control form-control-sm" type="text" name="descricao"
                                                       id="descricao" maxlength="200" size="20"
                                                       value="${servicoTipo.descricao}"/>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-4">
                                                <label for="servicoTipoPrioridade">Prioridade: <span class="text-danger">*</span></label>
                                                <select id="servicoTipoPrioridade" class="form-control form-control-sm"
                                                        name="servicoTipoPrioridade">
                                                    <option value="-1">Selecione</option>
                                                    <c:forEach items="${listaPrioridade}" var="servicoTipoPrioridade">
                                                        <option value="<c:out value="${servicoTipoPrioridade.chavePrimaria}"/>"
                                                                <c:if test="${servicoTipo.servicoTipoPrioridade.chavePrimaria == servicoTipoPrioridade.chavePrimaria}">selected="selected"</c:if>>
                                                            <c:out value="${servicoTipoPrioridade.descricao}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label for="documentoImpressaoLayout">Layout do Documento: <span
                                                        class="text-danger">*</span></label>
                                                <select id="documentoImpressaoLayout" class="form-control form-control-sm"
                                                        name="documentoImpressaoLayout">
                                                    <option value="-1">Selecione</option>
                                                    <c:forEach items="${listaDocumentos}" var="documentoImpressaoLayout">
                                                        <option value="<c:out value="${documentoImpressaoLayout.chavePrimaria}"/>"
                                                                <c:if test="${servicoTipo.documentoImpressaoLayout.chavePrimaria == documentoImpressaoLayout.chavePrimaria}">selected="selected"</c:if>>
                                                            <c:out value="${documentoImpressaoLayout.descricao}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="col-md-4">

                                                <label for="formulario">Formul�rio: <span id="spanFormulario"
                                                                                          class="text-danger"></span></label>
                                                <select id="formulario" class="form-control form-control-sm" name="formulario">
                                                    <option value="-1">Selecione</option>
                                                    <c:forEach items="${listaFormulario}" var="formulario">
                                                        <option value="<c:out value="${formulario.chavePrimaria}"/>"
                                                                <c:if test="${servicoTipo.formulario.chavePrimaria == formulario.chavePrimaria}">selected="selected"</c:if>>
                                                            <c:out value="${formulario.nomeQuestionario}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <label id="rotuloQuantidadeTempoMedio" for="quantidadeTempoMedio">
                                                    Tempo m�dio para execu��o do servi�o (em minutos):<span
                                                        class="text-danger">*</span></label>
                                                <input class="form-control form-control-sm input-sem-spinner" type="number" name="quantidadeTempoMedio"
                                                       id="quantidadeTempoMedio" min="0"
                                                       maxlength="9" size="10" value="${servicoTipo.quantidadeTempoMedio}"
                                                       onkeypress="return formatarCampoInteiro(event, this);"
                                                       onpaste="return formatarCampoInteiro(event, this);"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label id="rotuloQuantidadePrazoExecucao" for="quantidadePrazoExecucao">
                                                    Prazo para execu��o do servi�o (em horas):
                                                </label>
                                                <input class="form-control form-control-sm input-sem-spinner" type="number" name="quantidadePrazoExecucao"
                                                       id="quantidadePrazoExecucao" maxlength="9" size="10"
                                                       value="${servicoTipo.quantidadePrazoExecucao}" min="0"
                                                       onkeypress="return formatarCampoInteiro(event, this);"
                                                       onpaste="return formatarCampoInteiro(event, this);"/>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <label for="numeroMaximoExecucoes">
                                                    N�mero m�ximo de execu��es (por ponto de consumo):</label>
                                                <input class="form-control form-control-sm input-sem-spinner" type="number" name="numeroMaximoExecucoes"
                                                       id="numeroMaximoExecucoes" min="0" max="99"
                                                       maxlength="9" size="10" value="${servicoTipo.numeroMaximoExecucoes}"
                                                       onkeypress="return formatarCampoInteiro(event, this);"
                                                       onpaste="return formatarCampoInteiro(event, this);"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="prazoGarantia">
                                                    Prazo de garantia (em dias):
                                                </label>
                                                <input class="form-control form-control-sm input-sem-spinner" type="number" name="prazoGarantia"
                                                       id="prazoGarantia" maxlength="9" size="10"
                                                       value="${servicoTipo.prazoGarantia}" min="0" max="365"
                                                       onkeypress="return formatarCampoInteiro(event, this);"
                                                       onpaste="return formatarCampoInteiro(event, this);"/>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-row">
				                            <div class="col-md-6">
				                                <label for="equipe">Equipe Priorit�ria: <span id="spanEquipe" class="text-danger">*</span></label>
		                                        <select name="equipePrioritaria" id="equipePrioritaria" class="form-control form-control-sm">
		                                            <option value="-1">Selecione</option>
		                                            <c:forEach items="${listaEquipes}" var="equipe">
		                                                <option value="<c:out value="${equipe.chavePrimaria}"/>"
		                                                        <c:if test="${servicoTipo.equipePrioritaria.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
		                                                    <c:out value="${equipe.nome}"/>
		                                                </option>
		                                            </c:forEach>
		                                        </select>
				                            </div>
				                        </div>
                                        
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Servi�o regulamentado?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input"
                                                                   type="radio" name="indicadorServicoRegulamento"
                                                                   id="indicadorServicoRegulamento" value="true"
                                                                   <c:if test="${servicoTipo.indicadorServicoRegulamento eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorServicoRegulamento">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorServicoRegulamento"
                                                                   id="indicadorServicoRegulamentoFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorServicoRegulamento eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorServicoRegulamentoFalse">N�o</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Pode ser agendado?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorAgendamento"
                                                                   id="indicadorAgendamento" value="true"
                                                                   <c:if test="${servicoTipo.indicadorAgendamento eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorAgendamento">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorAgendamento"
                                                                   id="indicadorAgendamentoFalse" value="false"
                                                                   <c:if test="${servicoTipo.indicadorAgendamento eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorAgendamentoFalse">N�o</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>O servi�o ser� cobrado?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorCobranca"
                                                                   id="indicadorCobranca"
                                                                   onclick="exibirCobranca()"
                                                                   value="true"
                                                                   <c:if test="${servicoTipo.indicadorCobranca eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorCobranca">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorCobranca"
                                                                   onclick="ocultarCobranca();" id="indicadorCobrancaFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorCobranca eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorCobrancaFalse">N�o</label>
                                                        </div>
                                                    </div>

                                                    <div id="valorCobranca">
                                                        <div class="col-md-12">
                                                            <label class="mb-0">Cobrar servi�o na pr�xima fatura?</label>
                                                        </div>
                                                        <div class="col-md-12 margin-top-menor">
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input class="custom-control-input" type="radio" name="indicadorProximaFatura"
                                                                       id="indicadorProximaFaturaSim" value="true"
                                                                       <c:if test="${servicoTipo.indicadorProximaFatura eq 'true'}">checked</c:if>>
                                                                <label class="custom-control-label"
                                                                       for="indicadorProximaFaturaSim">Sim</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input class="custom-control-input" type="radio" name="indicadorProximaFatura"
                                                                       id="indicadorProximaFaturaNao" value="false"
                                                                       <c:if test="${servicoTipo.indicadorProximaFatura eq 'false'}">checked</c:if>>
                                                                <label class="custom-control-label"
                                                                       for="indicadorProximaFaturaNao">N�o</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Equipamento especial?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">

                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorEquipamento"
                                                                   id="indicadorEquipamento" value="true"
                                                                   onclick="exibirDadosEquipamento();"
                                                                   <c:if test="${servicoTipo.indicadorEquipamento eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorEquipamento">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorEquipamento"
                                                                   id="indicadorEquipamentoFalse" value="false"
                                                                   onclick="esconderDadosEquipamento();"
                                                                   <c:if test="${servicoTipo.indicadorEquipamento eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorEquipamentoFalse">N�o</label>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                        <label>Emite Notifica��o?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorNotificacao"
                                                                   id="indicadorNotificacao" value="true"
                                                                   <c:if test="${servicoTipo.indicadorNotificacao eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorNotificacao">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorNotificacao"
                                                                   id="indicadorNotificacaoFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorNotificacao eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorNotificacaoFalse">N�o</label>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class="col-md-12">
                                                        <label>Possui integra��o com outro sistema?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorIntegracao"
                                                                   id="indicadorIntegracao" value="true"
                                                                   <c:if test="${servicoTipo.indicadorIntegracao eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorIntegracao">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorIntegracao"
                                                                   id="indicadorIntegracaoFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorIntegracao eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorIntegracaoFalse">N�o</label>
                                                        </div>
                                                    </div>                                                       
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Necessita de ve�culo?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorVeiculo"
                                                                   id="indicadorVeiculo" value="true"
                                                                   <c:if test="${servicoTipo.indicadorVeiculo eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorVeiculo">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorVeiculo"
                                                                   id="indicadorVeiculoFalse" value="false"
                                                                   <c:if test="${servicoTipo.indicadorVeiculo eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorVeiculoFalse">N�o</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Gerar pesquisa de satisfa��o autom�tica?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorPesquisaSatisfacao"
                                                                   id="indicadorPesquisaSatisfacao" value="true"
                                                                   <c:if test="${servicoTipo.indicadorPesquisaSatisfacao eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorPesquisaSatisfacao">Sim</label>
                                                        </div>

                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorPesquisaSatisfacao"
                                                                   id="indicadorPesquisaSatisfacaoFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorPesquisaSatisfacao eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorPesquisaSatisfacaoFalse">N�o</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Permite gerar autoriza��o de servi�o em lote?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorGeraLote"
                                                                   id="indicadorGeraLote" value="true"
                                                                   <c:if test="${servicoTipo.indicadorGeraLote eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorGeraLote">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorGeraLote"
                                                                   id="indicadorGeraLoteFalse" value="false"
                                                                   <c:if test="${servicoTipo.indicadorGeraLote eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorGeraLoteFalse">N�o</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Encerramento autom�tico ao executar?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorEncerramentoAuto"
                                                                   id="indicadorEncerramentoAuto" value="true"
                                                                   <c:if test="${servicoTipo.indicadorEncerramentoAuto eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorEncerramentoAuto">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorEncerramentoAuto"
                                                                   id="indicadorEncerramentoAutoFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorEncerramentoAuto eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorEncerramentoAutoFalse">N�o</label>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                        <label>Metade do tempo de Execu��o quando Segmento Residencial - Individual?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorMetadeTempoExecucao"
                                                                   id="indicadorMetadeTempoExecucao" value="true"
                                                                   <c:if test="${servicoTipo.indicadorMetadeTempoExecucao eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorMetadeTempoExecucao">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorMetadeTempoExecucao"
                                                                   id="indicadorMetadeTempoExecucaoFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorMetadeTempoExecucao eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorMetadeTempoExecucaoFalse">N�o</label>
                                                        </div>
                                                    </div>                                                    
                                                    
                                                    <div class="col-md-12">
                                                        <label>Servi�o insere informa��es de novo medidor?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorNovoMedidor"
                                                                   id="indicadorNovoMedidor" value="true"
                                                                   <c:if test="${servicoTipo.indicadorNovoMedidor eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorNovoMedidor">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorNovoMedidor"
                                                                   id="indicadorNovoMedidorFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorNovoMedidor eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorNovoMedidorFalse">N�o</label>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                        <label>Servi�o pode ser executado no mobile?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorServicoMobile"
                                                                   onclick="exibirInformacoesAssinatura();"
                                                                   id="indicadorServicoMobile" value="true"
                                                                   <c:if test="${servicoTipo.indicadorServicoMobile eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorServicoMobile">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorServicoMobile"
                                                                   onclick="esconderInformacoesAssinatura();"
                                                                   id="indicadorServicoMobileFalse"
                                                                   value="false"
                                                                   <c:if test="${servicoTipo.indicadorServicoMobile eq 'false' or servicoTipo.indicadorServicoMobile eq null}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorServicoMobileFalse">N�o</label>
                                                        </div>
                                                    </div>
                                                    
                                                    <div id="divIndicadorAssinatura">
	                                                    <div class="col-md-12">
	                                                        <label>Servi�o possui assinatura obrigat�ria?</label>
	                                                    </div>
	                                                    <div class="col-md-12 margin-top-menor">
	                                                        <div class="custom-control custom-radio custom-control-inline">
	                                                            <input class="custom-control-input" type="radio"
	                                                                   name="indicadorAssinatura"
	                                                                   id="indicadorAssinaturaTrue" value="true"
	                                                                   <c:if test="${servicoTipo.indicadorAssinatura eq 'true'}">checked</c:if>>
	                                                            <label class="custom-control-label"
	                                                                   for="indicadorAssinaturaTrue">Sim</label>
	                                                        </div>
	                                                        <div class="custom-control custom-radio custom-control-inline">
	                                                            <input class="custom-control-input" type="radio"
	                                                                   name="indicadorAssinatura"
	                                                                   id="indicadorAssinaturaFalse"
	                                                                   value="false"
	                                                                   <c:if test="${servicoTipo.indicadorAssinatura eq 'false' or servicoTipo.indicadorAssinatura eq null}">checked</c:if>>
	                                                            <label class="custom-control-label"
	                                                                   for="indicadorAssinaturaFalse">N�o</label>
	                                                        </div>
	                                                    </div>
                                                    </div>                                                    
                                                    
                                                    <c:if test="${fluxoAlteracao}">
                                                        <div class="col-md-12">
                                                            <label>Indicador de Uso:</label>
                                                        </div>
                                                        <div class="col-md-12 margin-top-menor">
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input class="custom-control-input" type="radio"
                                                                       name="habilitado"
                                                                       id="habilitadoSim" value="true"
                                                                       <c:if test="${servicoTipo.habilitado eq 'true'}">checked</c:if>>
                                                                <label class="custom-control-label"
                                                                       for="habilitadoSim">Sim</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input class="custom-control-input" type="radio"
                                                                       name="habilitado"
                                                                       id="habilitadoNao"
                                                                       value="false"
                                                                       <c:if test="${servicoTipo.habilitado eq 'false'}">checked</c:if>>
                                                                <label class="custom-control-label"
                                                                       for="habilitadoNao">N�o</label>
                                                            </div>
                                                        </div>
                                                    </c:if>

                                                </div>
                                            </div>
                                        </div>

										<div class="form-row">
											<div class="col-md-12">
												<label>Emite Protocolo?</label>
											</div>
											<div class="col-md-12 margin-top-menor">
												<div
													class="custom-control custom-radio custom-control-inline">
													<input class="custom-control-input" type="radio"
														name="indicadorProtocolo" id="indicadorProtocolo"
														value="true" onclick="exibirInformacoesProtocolo();"
														<c:if test="${servicoTipo.indicadorProtocolo eq 'true'}">checked</c:if>>
													<label class="custom-control-label"
														for="indicadorProtocolo">Sim</label>
												</div>
												<div
													class="custom-control custom-radio custom-control-inline">
													<input class="custom-control-input" type="radio"
														name="indicadorProtocolo" id="indicadorProtocoloFalse"
														value="false" onclick="esconderInformacoesProtocolo();"
														<c:if test="${servicoTipo.indicadorProtocolo eq 'false'}">checked</c:if>>
													<label class="custom-control-label"
														for="indicadorProtocoloFalse">N�o</label>
												</div>
											</div>
										</div>

										<div class="form-row" id="divInformacoesProtocolo">
											<div class="col-md-12">
												<div class="row">
													<div class="col-md-12">
														<label>Retorno do Protocolo Obrigat�rio?</label>
													</div>
													<div class="col-md-12 margin-top-menor">

														<div
															class="custom-control custom-radio custom-control-inline">
															<input class="custom-control-input" type="radio"
																name="indicadorProtocoloObrigatorio"
																id="indicadorProtocoloObrigatorio" value="true" onclick="exibirPrazoRetornoProtocolo();"
																<c:if test="${servicoTipo.indicadorProtocoloObrigatorio eq 'true'}">checked</c:if>>
															<label class="custom-control-label"
																for="indicadorProtocoloObrigatorio">Sim</label>
														</div>
														<div
															class="custom-control custom-radio custom-control-inline">
															<input class="custom-control-input" type="radio"
																name="indicadorProtocoloObrigatorio"
																id="indicadorProtocoloObrigatorioFalse" value="false" onclick="esconderPrazoRetornoProtocolo();"
																<c:if test="${servicoTipo.indicadorProtocoloObrigatorio eq 'false'}">checked</c:if>>
															<label class="custom-control-label"
																for="indicadorProtocoloObrigatorioFalse">N�o</label>
														</div>
													</div>
													
		                                            <div id="divTempoRetornoProtocolo">
		                                            	<div class="col-md-12">
			                                                <label id="rotuloTempoRetornoProtocolo" for="tempoRetornoProtocolo">
			                                                    Prazo para retorno do protocolo (dias):
			                                                </label>
		                                                </div>
		                                                
		                                                <div class="col-md-6">
			                                                <input class="form-control form-control-sm input-sem-spinner" type="number" name="tempoRetornoProtocolo"
			                                                       id="tempoRetornoProtocolo" maxlength="9" size="500"
			                                                       value="${servicoTipo.tempoRetornoProtocolo}" min="0"
			                                                       onkeypress="return formatarCampoInteiro(event, this);"
			                                                       onpaste="return formatarCampoInteiro(event, this);"/>
		                                                </div>
		                                            </div>		
		                                            
		                                            	<div class="col-md-12">
			                                                <label id="rotuloQuantidadePrazoExecucao" for="quantidadePrazoExecucao">
			                                                    Prazo para emiss�o da Autoriza��o de Servi�o ap�s conclus�o do protocolo (dias):
			                                                </label>
			                                             </div>
			                                             
			                                            <div class="col-md-6">
			                                                <input class="form-control form-control-sm input-sem-spinner" type="number" name="tempoEmissaoAS"
			                                                       id="tempoEmissaoAS" maxlength="9" size="500"
			                                                       value="${servicoTipo.tempoEmissaoAS}" min="0"
			                                                       onkeypress="return formatarCampoInteiro(event, this);"
			                                                       onpaste="return formatarCampoInteiro(event, this);"/>
		                                            	</div>			                                            											

													<div class="col-md-12">
														<label>Tipo do Retorno do Protocolo</label>
													</div>
													<div class="col-md-12 margin-top-menor">

														<div
															class="custom-control custom-radio custom-control-inline">
															<input class="custom-control-input" type="radio"
																name="indicadorTipoProtocolo"
																id="indicadorTipoProtocolo" value="true"
																<c:if test="${servicoTipo.indicadorTipoProtocolo eq 'true'}">checked</c:if>>
															<label class="custom-control-label"
																for="indicadorTipoProtocolo">Individual</label>
														</div>
														<div
															class="custom-control custom-radio custom-control-inline">
															<input class="custom-control-input" type="radio"
																name="indicadorTipoProtocolo"
																id="indicadorTipoProtocoloFalse" value="false"
																<c:if test="${servicoTipo.indicadorTipoProtocolo eq 'false'}">checked</c:if>>
															<label class="custom-control-label"
																for="indicadorTipoProtocoloFalse">Coletivo</label>
														</div>
													</div>
												</div>
											</div>

										</div>
										
										<div class="form-row">
											<div class="col-md-12">
												<label>Emite comunica��o ao cliente antes da Execu��o da Autoriza��o de Servi�o?</label>
											</div>
											<div class="col-md-12 margin-top-menor">
												<div
													class="custom-control custom-radio custom-control-inline">
													<input class="custom-control-input" type="radio"
														name="indicadorAviso" id="indicadorAviso"
														value="true" onclick="exibirInformacoesAviso();"
														<c:if test="${servicoTipo.indicadorAviso eq 'true'}">checked</c:if>>
													<label class="custom-control-label"
														for="indicadorAviso">Sim</label>
												</div>
												<div
													class="custom-control custom-radio custom-control-inline">
													<input class="custom-control-input" type="radio"
														name="indicadorAviso" id="indicadorAvisoFalse"
														value="false" onclick="esconderInformacoesAviso();"
														<c:if test="${servicoTipo.indicadorAviso eq 'false'}">checked</c:if>>
													<label class="custom-control-label"
														for="indicadorAvisoFalse">N�o</label>
												</div>
											</div>
										</div>										
										<div id="divInformacoesDoAviso">
											<div class="col-md-6">
	                                                <label for="documentoImpressaoAviso">Layout da Comunica��o ao Cliente: <span
	                                                        class="text-danger">*</span></label>
	                                                <select id="documentoImpressaoAviso" class="form-control form-control-sm"
	                                                        name="documentoImpressaoAviso">
	                                                    <option value="-1">Selecione</option>
	                                                    <c:forEach items="${listaProtocolo}" var="documentoImpressaoAviso">
	                                                        <option value="<c:out value="${documentoImpressaoAviso.chavePrimaria}"/>"
	                                                                <c:if test="${servicoTipo.documentoImpressaoAviso.chavePrimaria == documentoImpressaoAviso.chavePrimaria}">selected="selected"</c:if>>
	                                                            <c:out value="${documentoImpressaoAviso.descricao}"/>
	                                                        </option>
	                                                    </c:forEach>
	                                                </select>
	                                         </div>		
										</div>

										<div class="form-row">

                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Exige atualiza��o cadastral?</label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorAtualizacaoCadastral"
                                                                   id="indicadorAtualizacaoCadastral" value="I"
                                                                   onclick="exibirDadosTelaAtualizacao();"
                                                                   <c:if test="${servicoTipo.indicadorAtualizacaoCadastral eq 'I'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorAtualizacaoCadastral">Imediata</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorAtualizacaoCadastral"
                                                                   id="indicadorAtualizacaoCadastralPosterior" value="P"
                                                                   onclick="exibirDadosTelaAtualizacao();"
                                                                   <c:if test="${servicoTipo.indicadorAtualizacaoCadastral eq 'P'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorAtualizacaoCadastralPosterior">Posterior</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio"
                                                                   name="indicadorAtualizacaoCadastral"
                                                                   id="indicadorAtualizacaoCadastralFalse"
                                                                   value="N" onclick="esconderDadosTelaAtualizacao();"
                                                                   <c:if test="${servicoTipo.indicadorAtualizacaoCadastral eq 'N'}">checked</c:if>>
                                                            <label class="custom-control-label"
                                                                   for="indicadorAtualizacaoCadastralFalse">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row" id="divTelaAtualizacao">
                                            <div class="col-md-12">
                                                <label for="telaAtualizacao">Tela para atualiza��o: <span
                                                        class="text-danger">*</span></label>
                                                <select id="telaAtualizacao" class="form-control form-control-sm" name="telaAtualizacao"
                                                        onchange="manipularExibicaoDivOperacaoMedidor();">
                                                    <option value="-1">Selecione</option>
                                                    <c:forEach items="${listaTelaAtualizacao}" var="tela">
                                                        <option value="<c:out value="${tela.chavePrimaria}"/>"
                                                                <c:if test="${servicoTipo.telaAtualizacao.chavePrimaria == tela.chavePrimaria}">selected="selected"</c:if>>
                                                            <c:out value="${tela.descricao}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-row" id="divAssociacaoMedicao">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Tipo de Associa��o: <span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="col-md-12 margin-top-menor">
                                                        <c:forEach items="${listaTipoAssociacao}" var="indicadorTipoAssociacaoPontoConsumo">
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input class="custom-control-input" type="radio"
                                                                       id="indicadorTipoAssociacaoPontoConsumo<c:out value="${indicadorTipoAssociacaoPontoConsumo.key}"/>"
                                                                       name="indicadorTipoAssociacaoPontoConsumo"
                                                                       value="<c:out value="${indicadorTipoAssociacaoPontoConsumo.key}"/>"
                                                                       <c:if test="${servicoTipo.indicadorTipoAssociacaoPontoConsumo eq indicadorTipoAssociacaoPontoConsumo.key}">checked="checked"</c:if>
                                                                       onclick="obterOperacoes();"/>
                                                                <label class="custom-control-label"
                                                                       for="indicadorTipoAssociacaoPontoConsumo<c:out value="${indicadorTipoAssociacaoPontoConsumo.key}"/>">
                                                                    <c:out value="${indicadorTipoAssociacaoPontoConsumo.value}"/>
                                                                </label>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div id="divOperacaoMedidor">
                                                    <jsp:include
                                                            page="/jsp/atendimento/servicotipo/bootstrap/selectOperacaoMedidorBootstrap.jsp"></jsp:include>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label for="orientacao">Observa��o/Orienta��o: </label>
                                                <textarea class="form-control form-control-sm" id="orientacao" name="orientacao" cols="40"
                                                          rows="7" maxlength="800"
                                                          onblur="this.value = removerEspacoInicialFinal(this.value);"
                                                          onkeypress="return formatarCampoTextoLivreComLimite(event,this,800);"
                                                          onpaste="return formatarCampoTextoLivreComLimite(event,this,800);">${servicoTipo.orientacao}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-12" id="gridEquipamentos">
                                                <jsp:include
                                                        page="/jsp/atendimento/servicotipo/bootstrap/gridEquipamentosBootstrap.jsp"></jsp:include>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabCampoObrigatorio" role="contentTabCampoObrigatorio"
                                 aria-labelledby="contentTabCliente">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mb-0">O campo <strong>cliente</strong> � obrigat�rio?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorClienteObrigatorio"
                                                           id="indicadorClienteObrigatorio" value="true"
                                                           <c:if test="${servicoTipo.indicadorClienteObrigatorio eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorClienteObrigatorio">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorClienteObrigatorio"
                                                           id="indicadorClienteObrigatorioFalse" value="false"
                                                           <c:if test="${servicoTipo.indicadorClienteObrigatorio eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorClienteObrigatorioFalse">N�o</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="mb-0">O campo <strong>im�vel</strong> � obrigat�rio?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorImovelObrigatorio"
                                                           id="indicadorImovelObrigatorio"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorImovelObrigatorio eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorImovelObrigatorio">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorImovelObrigatorio"
                                                           id="indicadorImovelObrigatorioFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorImovelObrigatorio eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorImovelObrigatorioFalse">N�o</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="mb-0">O campo <strong>ponto de consumo</strong> � obrigat�rio?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorPontoConsumoObrigatorio"
                                                           id="indicadorPontoConsumoObrigatorio"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorPontoConsumoObrigatorio eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorPontoConsumoObrigatorio">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorPontoConsumoObrigatorio"
                                                           id="indicadorPontoConsumoObrigatorioFalse" value="false"
                                                           <c:if test="${servicoTipo.indicadorPontoConsumoObrigatorio eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorPontoConsumoObrigatorioFalse">N�o</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mb-0">O campo <strong>contrato</strong> � obrigat�rio?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorContratoObrigatorio"
                                                           id="indicadorContratoObrigatorio"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorContratoObrigatorio eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorContratoObrigatorio">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorContratoObrigatorio"
                                                           id="indicadorContratoObrigatorioFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorContratoObrigatorio eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorContratoObrigatorioFalse">N�o</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="mb-0">O campo <strong>formul�rio</strong> � obrigat�rio?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorFormularioObrigatorio"
                                                           id="indicadorFormularioObrigatorioTrue"
                                                           value="true" onclick="habilitarFormulario();"
                                                           <c:if test="${servicoTipo.indicadorFormularioObrigatorio eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorFormularioObrigatorioTrue">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorFormularioObrigatorio"
                                                           id="indicadorFormularioObrigatorioFalse" value="false"
                                                           onclick="desabilitarFormulario();"
                                                           <c:if test="${servicoTipo.indicadorFormularioObrigatorio eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorFormularioObrigatorioFalse">N�o</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabEnvioEmail" role="contentTabEnvioEmail"
                                 aria-labelledby="contentTabEnvioEmail">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail ao cadastrar servi�o?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailCadastrar"
                                                           id="indicadorEmailCadastrar"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorEmailCadastrar eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailCadastrar">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailCadastrar"
                                                           id="indicadorEmailCadastrarFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorEmailCadastrar eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailCadastrarFalse">N�o</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail ao colocar servi�o para execu��o?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailEmExecucao"
                                                           id="indicadorEmailEmExecucao" value="true"
                                                           <c:if test="${servicoTipo.indicadorEmailEmExecucao eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailEmExecucao">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailEmExecucao"
                                                           id="indicadorEmailEmExecucaoFalse" value="false"
                                                           <c:if test="${servicoTipo.indicadorEmailEmExecucao eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailEmExecucaoFalse">N�o</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail ao executar servi�o?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailExecucao"
                                                           id="indicadorEmailExecucao"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorEmailExecucao eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailExecucao">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailExecucao"
                                                           id="indicadorEmailExecucaoFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorEmailExecucao eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailExecucaoFalse">N�o</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail ao cliente ao encerrar o servi�o?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailCliente"
                                                           id="indicadorEmailCliente"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorEmailCliente eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailCliente">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailCliente"
                                                           id="indicadorEmailClienteFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorEmailCliente eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailClienteFalse">N�o</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail ao encerrar servi�o?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailEncerrar"
                                                           id="indicadorEmailEncerrar"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorEmailEncerrar eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailEncerrar">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailEncerrar"
                                                           id="indicadorEmailEncerrarFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorEmailEncerrar eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailEncerrarFalse">N�o</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail ao alterar servi�o?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailAlterar"
                                                           id="indicadorEmailAlterar"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorEmailAlterar eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailAlterar">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailAlterar"
                                                           id="indicadorEmailAlterarFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorEmailAlterar eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailAlterarFalse">N�o</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail ao agendar o servi�o?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailAgendar"
                                                           id="indicadorEmailAgendar"
                                                           value="true"
                                                           <c:if test="${servicoTipo.indicadorEmailAgendar eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailAgendar">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailAgendar"
                                                           id="indicadorEmailAgendarFalse"
                                                           value="false"
                                                           <c:if test="${servicoTipo.indicadorEmailAgendar eq 'false'}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailAgendarFalse">N�o</label>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <label class="mb-0">Enviar e-mail personalizado ao encerrar?</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailPersonalizado"
                                                           id="indicadorEmailPersonalizado" value="true"
                                                           onclick="mostrarListaEmail(true)"
                                                           <c:if test="${servicoTipo.indicadorEmailPersonalizado eq 'true'}">checked</c:if>>
                                                    <label class="custom-control-label" for="indicadorEmailPersonalizado">Sim</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="indicadorEmailPersonalizado"
                                                           id="indicadorEmailPersonalizadoFalse" value="false"
                                                           onclick="mostrarListaEmail(false)"
                                                           <c:if test="${servicoTipo.indicadorEmailPersonalizado eq 'false' || servicoTipo.indicadorEmailPersonalizado eq null}">checked</c:if>>
                                                    <label class="custom-control-label"
                                                           for="indicadorEmailPersonalizadoFalse">N�o</label>
                                                </div>
                                            </div>
                                            <div id="emailPersonalizadoDiv" class="col-md-12 custom-control custom-radio custom-control-inline" style="display: none; margin-top: 10px;">
										<input type="text" class="form-control" id="listaEmailPersonalizado" name="listaEmailPersonalizado" 
										       placeholder="Digite os e-mails separados por ponto e v�rgula (;)" 
										       value="<c:out value='${servicoTipo.listaEmailPersonalizado}'/>">
										</br>
										<input type="text" class="form-control" id="assuntoEmail" name="assuntoEmail" 
											       placeholder="Assunto do E-mail" 
											       value="<c:out value='${servicoTipo.assuntoEmail}'/>">										       
											</div>
                                                                                                                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabMaterialNecessario" role="contentTabMaterialNecessario"
                                 aria-labelledby="contentTabMaterialNecessario">
                                <div class="row">
                                    <div class="col-md-12" id="gridMateriais">
                                        <jsp:include page="/jsp/atendimento/servicotipo/bootstrap/gridMateriaisBootstrap.jsp"></jsp:include>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabQuantidadeAgendamento" role="contentTabQuantidadeAgendamento"
                                 aria-labelledby="contentTabQuantidadeAgendamento">
                                <div id="gridAgendamentos">
                                    <jsp:include page="/jsp/atendimento/servicotipo/bootstrap/gridAgendamentosBoostrap.jsp"></jsp:include>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabConteudoEmail" role="contentTabConteudoEmail"
                                 aria-labelledby="contentTabConteudoEmail">
                                <div id="gridConteudoEmail">
                                    <jsp:include page="/jsp/atendimento/servicotipo/bootstrap/gridConteudoEmailBootstrap.jsp"></jsp:include>
                                </div>
                            </div>                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="form-row mt-2">
                    <div class="col-md-12">
                        <div class="row justify-content-between">

                            <div class="col-md-6 col-sm-12 mt-1">
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDropInclusaoCancelar" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-times"></i> A��es (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDropInclusaoCancelar">
                                        <input name="Button" class="dropdown-item" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                               onclick="limparFormulario();">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                <c:choose>
                                    <c:when test="${fluxoAlteracao}">
                                        <vacess:vacess param="alterarServicoTipo">
                                            <button id="buttonSalvar" name="button"
                                                    class="btn btn-primary btn-sm mb-1 mr-1"
                                                    type="submit" onclick="alterar();">
                                                <i class="fa fa-save"></i> Salvar
                                            </button>
                                        </vacess:vacess>
                                    </c:when>
                                    <c:otherwise>
                                        <vacess:vacess param="incluirServicoTipo">
                                            <button id="buttonSalvar" name="button"
                                                    class="btn btn-primary btn-sm mb-1 mr-1"
                                                    type="submit" onclick="incluir();">
                                                <i class="fa fa-save"></i> Salvar
                                            </button>
                                        </vacess:vacess>
                                    </c:otherwise>
                                </c:choose>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form:form>

</div>

<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/autorizacaoServico/tipoServico/incluirTipoServico/index.js"
        type="application/javascript"></script>
