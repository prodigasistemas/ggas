<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

function voltar(){
	submeter('servicoTipoForm','pesquisarServicoTipo');
}

function alterar(){
	submeter('servicoTipoForm','exibirAlteracaoServicoTipo');
}

</script>

<form method="post" action="servicoTipo/alterarServicoTipo" id="servicoTipoForm" >
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${servicoTipo.chavePrimaria }">
	<input type="hidden" name="chavesPrimarias" id="chavesPrimarias" value="${servicoTipo.chavePrimaria }">
	<input type="hidden" name="habilitado" id="habilitado" value="${servicoTipo.habilitado }">

    <div class="card">
        <div class="card-header">
            <h5>Detalhar Tipo de Servi�o</h5>
        </div>

        <div class="card-body">
            <div class="alert alert-primary" role="alert">
                <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                <div class="d-inline">
                    Para modificar as informa��es deste registro clique em <b>Alterar</b>.
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="tab-solicitante" data-toggle="tab"
                               href="#contentTabSolicitante" role="tab" aria-controls="contentTabSolicitante"
                               aria-selected="true"><i class="fa fa-info-circle"></i> Caracter�sticas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab-campo-obrigatorio" data-toggle="tab"
                               href="#contentTabCampoObrigatorio" role="tab" aria-controls="contentTabCampoObrigatorio"
                               aria-selected="false"><i class="fa fa-font"></i>* Campos Obrigat�rios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab-envio-email" data-toggle="tab"
                               href="#contentTabEnvioEmail" role="tab" aria-controls="contentTabEnvioEmail"
                               aria-selected="false"><i class="fa fa-at"></i> Opera��o de Envio de E-mail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab-material-necessario" data-toggle="tab"
                               href="#contentTabMaterialNecessario" role="tab" aria-controls="contentTabMaterialNecessario"
                               aria-selected="false"><i class="fa fa-archive"></i> Materiais Necess�rios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab-quantidade-agendamento" data-toggle="tab"
                               href="#contentTabQuantidadeAgendamento" role="tab" aria-controls="contentTabQuantidadeAgendamento"
                               aria-selected="false"><i class="fa fa-calendar-alt"></i> Quantidade de Agendamentos por Turno</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="content-incluir-chamado">
                        <div class="tab-pane fade show active" id="contentTabSolicitante" role="tabpanel"
                             aria-labelledby="contentTabSolicitante">
                            <div class="row">
                                <div class="col-md-12" id="incAltServicoTipoCol1"> <!-- Talvez id seja desnecess�rio... -->
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label><b>Descri��o:</b></label>
                                            <label>${servicoTipo.descricao}</label>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-4">
                                            <label><b>Prioridade:</b></label>
                                            <label>${servicoTipo.servicoTipoPrioridade.descricao}</label>
                                        </div>

                                        <div class="col-md-4">
                                            <label><b>Layout do documento:</b></label>
                                            <label>${servicoTipo.documentoImpressaoLayout.descricao}</label>
                                        </div>
                                        <div class="col-md-4">

                                            <label><b>Formul�rio:</b></label>
                                            <label>${servicoTipo.formulario.nomeQuestionario}</label>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>Tempo m�dio para execu��o do servi�o (em minutos):</b></label>
                                            <label>${servicoTipo.quantidadeTempoMedio}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><b>Prazo para execu��o do servi�o (em horas):</b></label>
                                            <label>${servicoTipo.quantidadePrazoExecucao}</label>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>N�mero m�ximo de execu��es (por ponto de consumo):</b></label>
                                            <label>
                                            <c:choose>
                                                <c:when test="${servicoTipo.numeroMaximoExecucoes ne null}">${servicoTipo.numeroMaximoExecucoes}</c:when>
                                                <c:otherwise>ilimitado</c:otherwise>
                                            </c:choose>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><b>Prazo de garantia (em dias):</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.prazoGarantia ne null}">${servicoTipo.prazoGarantia}</c:when>
                                                    <c:otherwise>Sem garantia</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>

									<div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>Equipe Priorit�ria:</b></label>
                                            <label>${servicoTipo.equipePrioritaria.nome}</label>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="col-md-4">
                                            <label><b>Servi�o regulamentado?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorServicoRegulamento eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-4">
                                            <label><b>Pode ser agendado?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorAgendamento eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-4">
                                            <label><b>O servi�o ser� cobrado?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorCobranca eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-4">
                                            <label><b>Cobrar servi�o na pr�xima fatura?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorProximaFatura eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-4">
                                            <label><b>Equipamento especial?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEquipamento eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-4">
                                            <label><b>Necessita de ve�culo?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorVeiculo eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-4">
                                            <label><b>Gerar pesquisa de satisfa��o autom�tica?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorPesquisaSatisfacao eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-6">
                                            <label><b>Permite gerar autoriza��o de servi�o em lote?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorGeraLote eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-6">
                                            <label><b>Encerramento autom�tico ao executar?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEncerramentoAuto eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><b>Gera Protocolo?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorProtocolo eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>
                                        
                                        
                                        <div class="col-md-6">
                                        	<label><b>Emite Comunica��o ao Cliente antes da Execu��o da Autoriza��o de Servi�o?</b></label>
                                        		<label>
		                                        	<c:choose>
		                                            	<c:when test="${servicoTipo.indicadorAviso eq 'true'}">Sim</c:when>
		                                           		<c:otherwise>N�o</c:otherwise>
		                                        	</c:choose>
	                                        	</label>
                                        </div>
                                        <c:if test="${servicoTipo.indicadorAviso eq 'true'}" >
	                                         <div class="col-md-6">
	                                            <label><b>Layout da Comunica��o ao Cliente</b></label>
												<label>${servicoTipo.documentoImpressaoAviso.descricao}</label>
	                                        </div>
                                        </c:if>
                                        
                                        <c:if test="${servicoTipo.indicadorProtocolo eq 'true'}" >
	                                        <div class="col-md-6">
	                                            <label><b>Retorno do Protocolo � obrigat�rio?</b></label>
	                                            <label>
	                                                <c:choose>
	                                                    <c:when test="${servicoTipo.indicadorProtocoloObrigatorio eq 'true'}">Sim</c:when>
	                                                    <c:otherwise>N�o</c:otherwise>
	                                                </c:choose>
	                                            </label>
	                                        </div>
	                                        
	                                        <c:if test="${servicoTipo.indicadorProtocoloObrigatorio eq 'true' }">
			                                    <div class="col-md-6">
		                                            <label><b>Prazo m�ximo em dias para aguardar o retorno (dias):</b></label>
													<label>${servicoTipo.tempoRetornoProtocolo}</label>
		                                        </div>
	                                        </c:if>
	                                        
	                                        <div class="col-md-6">
	                                            <label><b>Prazo para emiss�o da Autoriza��o de Servi�o, ap�s a conclus�o do Protocolo (dias):</b></label>
												<label>${servicoTipo.tempoEmissaoAS}</label>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <label><b>Tipo de Protocolo?</b></label>
	                                            <label>
	                                                <c:choose>
	                                                    <c:when test="${servicoTipo.indicadorTipoProtocolo eq 'true'}">Individual</c:when>
	                                                    <c:otherwise>Coletivo</c:otherwise>
	                                                </c:choose>
	                                            </label>
	                                        </div>
                                        </c:if>
                                        
                                        <div class="col-md-6">
                                            <label><b>Gera Notifica��o?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorNotificacao eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><b>Possui integra��o com outro sistema?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorIntegracao eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div> 
                                        
                                        <div class="col-md-6">
                                            <label><b>Metade do tempo de Execu��o quando Segmento Residencial - Individual?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorMetadeTempoExecucao eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>    
                                        
                                        <div class="col-md-6">
                                            <label><b>Servi�o insere informa��es de novo medidor?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorNovoMedidor eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>    
                                              
                                        <div class="col-md-6">
                                            <label><b>Servi�o pode ser executado no mobile?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorServicoMobile eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <label><b>Servi�o possui assinatura obrigat�ria?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorAssinatura eq 'true'}">Sim</c:when>
                                                    <c:otherwise>N�o</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>                                        
                                                                                                                                                                                                                                                                               
                                    </div>
                                    <div class="form-row">

                                        <div class="col-md-12">
                                            <label><b>Exige atualiza��o cadastral?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorAtualizacaoCadastral eq 'I'}">Imediata</c:when>
                                                    <c:when test="${servicoTipo.indicadorAtualizacaoCadastral eq 'P'}">Posterior</c:when>
                                                    <c:when test="${servicoTipo.indicadorAtualizacaoCadastral eq 'N'}">N�o</c:when>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-12">
                                            <label><b>Tela para atualiza��o:</b></label>
                                            <label>${servicoTipo.telaAtualizacao.descricao}</label>
                                        </div>
                                        <div class="col-md-12">
                                            <label><b>Tipo de Associa��o:</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorTipoAssociacaoPontoConsumo == '1'}">
                                                        Medidor
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorTipoAssociacaoPontoConsumo == '2'}">
                                                        Corretor de Vaz�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-12">
                                            <label><b>A��o:</b></label>
                                            <label>
                                                <c:forEach items="${listaOperacaoMedidor}" var="tipoOperacao">
                                                    <c:if test="${ servicoTipo.indicadorOperacaoMedidor == tipoOperacao.chavePrimaria }">
                                                        <c:out value="${tipoOperacao.descricao}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </label>
                                        </div>
                                        <div class="col-md-12">
                                            <label><b>Medidor Motivo Opera��o:</b></label>
                                            <label>
                                                <c:forEach items="${listaMedidorMotivoOperacao}" var="motivoOperacaoMedidor">
                                                    <c:if test="${ servicoTipo.motivoOperacaoMedidor.chavePrimaria == motivoOperacaoMedidor.chavePrimaria }">
                                                        <c:out value="${motivoOperacaoMedidor.descricao}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </label>
                                        </div>                                        
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label><b>Indicador de uso:</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.habilitado}">Ativo</c:when>
                                                    <c:otherwise>Inativo</c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label for="orientacao"><b>Observa��o/Orienta��o:</b></label>
                                            <textarea class="form-control form-control-sm" id="orientacao" name="orientacao" cols="40" disabled><c:out value="${servicoTipo.orientacao}"/></textarea>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12">

                                            <c:if test="${empty servicoTipo.listaEquipamentoEspecial == false }">
                                                <label><b>Equipamento Especial Necess�rio:</b></label>
                                                <div class="table-responsive mt-3">
                                                    <table class="table table-bordered table-striped table-hover w-100" id="datatable-equipamento-especial">
                                                        <thead class="thead-ggas-bootstrap">
                                                            <tr>
                                                                <th scope="col" class="text-center">Equipamento especial necess�rio</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <c:forEach var="servicoTipoEquipamento" items="${servicoTipo.listaEquipamentoEspecial}">
                                                            <tr>
                                                                <td>${servicoTipoEquipamento.equipamento.descricao}</td>
                                                            </tr>
                                                        </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contentTabCampoObrigatorio" role="contentTabCampoObrigatorio"
                             aria-labelledby="contentTabCliente">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>O campo cliente � obrigat�rio?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorClienteObrigatorio == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorClienteObrigatorio == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-6">
                                            <label><b>O campo im�vel � obrigat�rio?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorImovelObrigatorio == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorImovelObrigatorio == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>O campo contrato � obrigat�rio?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorContratoObrigatorio == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorContratoObrigatorio == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>

                                        <div class="col-md-6">
                                            <label><b>O campo ponto de consumo � obrigat�rio?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorPontoConsumoObrigatorio == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorPontoConsumoObrigatorio == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>O campo formul�rio � obrigat�rio?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorFormularioObrigatorio == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorFormularioObrigatorio == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contentTabEnvioEmail" role="contentTabEnvioEmail"
                             aria-labelledby="contentTabEnvioEmail">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>Enviar e-mail ao cadastrar servi�o?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEmailCadastrar == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorEmailCadastrar == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><b>Enviar e-mail ao colocar servi�o para execu��o?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEmailEmExecucao == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorEmailEmExecucao == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>Enviar e-mail ao executar servi�o?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEmailExecucao == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorEmailExecucao == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><b>Enviar e-mail ao encerrar servi�o?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEmailEncerrar == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorEmailEncerrar == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>Enviar e-mail ao alterar servi�o?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEmailAlterar == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorEmailAlterar == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label><b>Enviar e-mail ao agendar o servi�o?</b></label>
                                            <label>
                                                <c:choose>
                                                    <c:when test="${servicoTipo.indicadorEmailAgendar == 'true'}">
                                                        Sim
                                                    </c:when>
                                                    <c:when test="${servicoTipo.indicadorEmailAgendar == 'false'}">
                                                        N�o
                                                    </c:when>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contentTabMaterialNecessario" role="contentTabMaterialNecessario"
                             aria-labelledby="contentTabMaterialNecessario">
                            <div class="row">
                                <div class="col-md-12" id="gridMateriais">
                                    <c:choose>
                                        <c:when test="${empty servicoTipo.listaMateriais}">
                                            <h6 class="text-center">Nenhum material cadastrado.</h6>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="table-responsive mt-1">
                                                <table class="table table-bordered table-striped table-hover w-100" id="servicoTipoMaterial">
                                                    <thead class="thead-ggas-bootstrap">
                                                    <tr>
                                                        <th width="50%" scope="col" class="text-center">Materiais necess�rios</th>
                                                        <th width="25%" scope="col" class="text-center">Unidade</th>
                                                        <th width="25%" scope="col" class="text-center">Quantidade</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <c:forEach var="servicoTipoMaterial" items="${servicoTipo.listaMateriais}">
                                                        <tr>
                                                            <td>${servicoTipoMaterial.material.descricao}</td>
                                                            <td>${servicoTipoMaterial.material.unidadeMedida.descricao}</td>
                                                            <td>${servicoTipoMaterial.quantidadeMaterial}</td>
                                                        </tr>
                                                    </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contentTabQuantidadeAgendamento" role="contentTabQuantidadeAgendamento"
                             aria-labelledby="contentTabQuantidadeAgendamento">
                            <div id="gridAgendamentos">
                                <c:choose>
                                    <c:when test="${empty servicoTipo.listaAgendamentosTurno}">
                                        <h6 class="text-center">Nenhum agendamento de turno cadastrado.</h6>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="table-responsive mt-1">
                                            <table class="w-100 table table-bordered table-striped table-hover" id="servicoTipoAgendamentoTurno">
                                                <thead class="thead-ggas-bootstrap">
                                                <tr>
                                                	<th scope="col" class="text-center">Equipe</th>
                                                    <th scope="col" class="text-center">Quantidade de agendamentos por turnos</th>
                                                    <th scope="col" class="text-center">Quantidade</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="servicoTipoAgendamentoTurno" items="${servicoTipo.listaAgendamentosTurno}">
                                                    <tr>
                                                    	<td width="50%">${servicoTipoAgendamentoTurno.equipe.nome}</td>
                                                        <td width="30%">${servicoTipoAgendamentoTurno.turno.descricao}</td>
                                                        <td width="20%">${servicoTipoAgendamentoTurno.quantidadeAgendamentosTurno}</td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </c:otherwise>
                                </c:choose>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <div class="form-row mt-2">
                <div class="col-md-12">
                    <div class="row justify-content-between">

                        <div class="col w-50">
                            <button class="btn btn-secondary btn-sm" onclick="voltar();"><i class="fa fa-chevron-circle-left"></i> Voltar</button>
                        </div>

                        <div class="col w-50 mt-sm-1 align-items-end flex d-flex align-items-end flex-column">
                            <vacess:vacess param="exibirAlteracaoServicoTipo">
                                <button id="buttonSalvar" name="button"
                                        class="btn btn-info btn-sm"
                                        type="submit" onclick="alterar();">
                                    <i class="fa fa-edit"></i> Alterar
                                </button>
                            </vacess:vacess>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
