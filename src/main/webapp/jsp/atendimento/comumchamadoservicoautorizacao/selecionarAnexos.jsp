<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script language="javascript">

function visualizarAnexo(){
		
	var index = document.forms[0].indexLista.value;
	var url =  '<c:out value="${param['actionVisualizarAnexo']}"/>';

	var conector = "?";
	if(url.indexOf("?") >= 0){
		conector = "&";
	}
	
	popup = window.open(url+conector+"indexLista="+index ,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function recarregarLayout() {
	
	$("input.campoFile").filestyle({ 
		image: "<c:out value='${pageContext.request.contextPath}'/>/imagens/botaoProcurar.gif",
		imageheight: 35,
		imagewidth: 98,
		width: 191
	});
}

function limparAnexo() {
	document.forms[0].indexLista.value = -1;
	document.forms[0].descricaoAnexo.value = "";
	
	var botaoAlterarAnexo = document.getElementById("botaoAlterarAnexo");
	var botaoLimparAnexo = document.getElementById("botaoLimparAnexo");	
	var botaoIncluirAnexo = document.getElementById("botaoIncluirAnexo");	
	var botaoVisualizarAnexo = document.getElementById("botaoVisualizarAnexo");
	
	botaoAlterarAnexo.disabled = true;
	botaoLimparAnexo.disabled = false;
	botaoIncluirAnexo.disabled = false;
	botaoVisualizarAnexo.disabled = true;

	var url = '<c:out value="${param['actionLimparCampoArquivo']}"/>';
	
	$("#divCampoArquivo").load(url, function () {
			recarregarLayout();
		}
	);
}


function incluirAnexo(form, actionAdicionarAnexo) {
	var nomeForm = "<c:out value='${param["nomeForm"]}'/>"
	document.forms[nomeForm].indexLista.value = -1;
	carregarFragmentoPOST('gridAnexos', actionAdicionarAnexo,document.forms[nomeForm]);	
	limparAnexo();
}

function alterarAnexo(form, actionAdicionarAnexo) {
	
	var nomeForm = "<c:out value='${param["nomeForm"]}'/>"
	carregarFragmentoPOST('gridAnexos', actionAdicionarAnexo,document.forms[nomeForm]);	
	limparAnexo();
}
		
		
function exibirAlteracaoDoAnexo(indexLista,chavePrimariaAnexo,descricaoAnexo) {
	
	if (indexLista != "") {
		document.forms[0].indexLista.value = indexLista;
		
		if (descricaoAnexo != "") {
			document.forms[0].descricaoAnexo.value = descricaoAnexo;
		}else{
			document.forms[0].descricaoAnexo.value = "";
		}
		
		if (chavePrimariaAnexo != ""){
			document.forms[0].chavePrimariaAnexo.value = chavePrimariaAnexo;
		}
		
		var botaoAlterarAnexo = document.getElementById("botaoAlterarAnexo");
		var botaoLimparAnexo = document.getElementById("botaoLimparAnexo");	
		var botaoIncluirAnexo = document.getElementById("botaoIncluirAnexo");	
		var botaoVisualizarAnexo = document.getElementById("botaoVisualizarAnexo");
		
		botaoAlterarAnexo.disabled = false;
		botaoLimparAnexo.disabled = false;
		botaoIncluirAnexo.disabled = true;
		botaoVisualizarAnexo.disabled = false;
	}
}

function removerAnexo(indice) {
	var nomeForm = "<c:out value='${param["nomeForm"]}'/>"
	document.forms[nomeForm].indexLista.value = indice;
	var actionRemoverAnexo = '<c:out value="${param['actionRemoverAnexo']}" />';
	
	carregarFragmentoPOST('gridAnexos', actionRemoverAnexo,document.forms[nomeForm]);
}

</script>


<fieldset id="conteinerDocumentos">
	<legend id="legendDocumentos">Documentos</legend>	
		<div class="conteinerDados">
			
		<fieldset > 
			<input type="hidden" id="chavePrimariaAnexo" name="chavePrimariaAnexo" />
			<c:if test="${param['moduloSistema'] ne 'Contrato'}">
				<input type="hidden" id="indexLista" name="indexLista" />				
			</c:if>
			
			<label class="rotulo" id="rotuloDocumentoAnexoAbaIdentificacao" for="anexo"><span class="campoObrigatorioSimbolo2">* </span>Incluir Documento:</label>
			
			<div id="divCampoArquivo"> 
				<jsp:include page="/jsp/atendimento/comumchamadoservicoautorizacao/campoArquivo.jsp"></jsp:include>		
			</div>
			
			<input name="botaoVisualizarAnexo" disabled="disabled" id="botaoVisualizarAnexo" class="bottonRightCol2 carregarPosicionamento2" title="Visualizar"  value="Visualizar" onclick="visualizarAnexo();" type="button">
			
			<label class="rotulo" id="rotuloDescricaoDocumento" for="descricaoDocumento"><span class="campoObrigatorioSimbolo2">* </span>Descri��o Documento:</label>
			<input class="campoTexto" type="text" id="descricaoAnexo" name="descricaoAnexo" maxlength="50" ><br />
			
		</fieldset>
			
		<fieldset class="conteinerBotoesAba"> 
			<input class="bottonRightCol2" name="botaoLimparAnexo" id="botaoLimparAnexo" value="Limpar" type="button" onclick="limparAnexo();">
			<input class="bottonRightCol2" name="botaoIncluirAnexo" id="botaoIncluirAnexo" value="Adicionar Anexo" type="button"  onclick="incluirAnexo(this.form,'<c:out value="${param['actionAdicionarAnexo']}"/>');">
		   	<input class="bottonRightCol2" disabled="disabled" name="botaoAlterarAnexo" id="botaoAlterarAnexo" value="Alterar Anexo" type="button" onclick="alterarAnexo(this.form,'<c:out value="${param['actionAdicionarAnexo']}"/>');">
		</fieldset>
		
		<div id="gridAnexos"> 
			<jsp:include page="/jsp/atendimento/comumchamadoservicoautorizacao/gridAnexos.jsp"></jsp:include>		
		</div>	

	</div>
</fieldset>
		



