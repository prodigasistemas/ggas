<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<input class="form-control form-control-sm"
       type="file" id="arquivoAnexo" name="arquivoAnexo" title="Procurar" style="padding: 0.09rem 0.5rem; display: none;"/>
<button type="button" class="btn btn-default btn-block btn-sm" id="fileSelect"><i class="fa fa-upload"></i> Clique para escolher arquivo...</button>

<script>

    $(document).ready(function () {

        $('#fileSelect').click(function () {
            $('#arquivoAnexo').attr('disabled', false);
            $('#arquivoAnexo').click();
        });

        $('#arquivoAnexo').change(function (event) {

            if (this.files[0].size > $('#tamanhoMaximoArquivo').val()) {
                $('#botaoIncluirAnexo').attr('disabled', true);
                $('#botaoAlterarAnexo').attr('disabled', true);
                $('#documentoAnexoFeedback').html('O tamanho do arquivo (' + parseFloat(this.files[0].size / 1024).toFixed(2) + ' kb) excede o limite (' + parseFloat($('#tamanhoMaximoArquivo').val() / 1024).toFixed(2) + ' kb)');
                $('#documentoAnexoFeedback').show();
                $('#documentoAnexoFeedbackValid').hide();
            } else {
                $('#documentoAnexoFeedback').html('Por favor informe o documento');
                $('#documentoAnexoFeedback').hide();
                $('#documentoAnexoFeedbackValid').show();
            }

        });
    });
</script>
