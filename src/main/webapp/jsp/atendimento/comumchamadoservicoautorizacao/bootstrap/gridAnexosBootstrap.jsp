<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>	

<script>
	$(document).ready(function() {
		//Necess�rio para mudar aba do m�dulo do contrato
		if( typeof mudarAba == 'function'){
			mudarAba(<c:out value="${abaAtual}"/>); 
		}
	});
</script>


<div class="text-center loading">
    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<c:set var="x" value="0" />
<div class="table-responsive">

    <table class="table table-bordered table-striped table-hover" id="table-grid-anexo" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th scope="col" class="text-center">Descri��o</th>
            <th scope="col" class="text-center">Data da inclus�o</th>
            <th scope="col" class="text-center">A��es</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${listaAnexos}" var="anexo">
            <tr>
                <td>
                    ${anexo.descricaoAnexo}
                </td>
                <td>
                    <fmt:formatDate pattern="dd/MM/yyyy" value="${anexo.ultimaAlteracao}" var="formattedStatusDate" />
                    <c:out value="${formattedStatusDate}"/>

                    <fmt:formatDate pattern="HH:mm" value="${anexo.ultimaAlteracao}" var="formattedStatusHour" />
                    <c:out value="${formattedStatusHour}"/>
                </td>
                <td>
                    <a href="javascript:exibirAlteracaoDoAnexo('${x}','${anexo.chavePrimaria}','${anexo.descricaoAnexo}');"><img title="Alterar Anexo" alt="Alterar Anexo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a>

                    <a onclick="return confirm('Deseja excluir o Anexo?');" href="javascript:removerAnexo(<c:out value="${x}"/>);"><img title="Excluir Anexo" alt="Excluir Anexo"  src="<c:url value="/imagens/deletar_x.png"/>" border="0" /></a>
                </td>
            </tr>

            <c:set var="x" value="${x+1}" />
        </c:forEach>
        </tbody>
    </table>
</div>
