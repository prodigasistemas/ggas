<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<input type="hidden" id="chavePrimariaAnexo" name="chavePrimariaAnexo"/>
<c:if test="${param['moduloSistema'] ne 'Contrato'}">
    <input type="hidden" id="indexLista" name="indexLista"/>
</c:if>
<input type="hidden" id="tamanhoMaximoArquivo" value="${TAMANHO_MAXIMO_ARQUIVO}"/>

<div class="row">
    <div class="col-md-6">
        <div class="form-row">
            <div class="col-md-12">
                <label for="botaoVisualizarAnexo">Incluir documento: <span class="text-danger">*</span></label>

                <div class="input-group">
                    <div id="divCampoArquivo" style="width: 300px;">
                        <jsp:include page="/jsp/atendimento/comumchamadoservicoautorizacao/bootstrap/campoArquivoBootstrap.jsp"></jsp:include>
                    </div>
                    <div class="input-group-append">
                        <button type="button" class="btn btn-primary btn-sm" name="botaoVisualizarAnexo" disabled="disabled"
                                style="height: 31px;"
                                id="botaoVisualizarAnexo"
                                onclick="visualizarAnexo();">
                            Visualizar
                        </button>
                    </div>
                </div>
                <div class="invalid-feedback" id="documentoAnexoFeedback">
                    Por favor informe o documento
                </div>
                <div class="text-success" id="documentoAnexoFeedbackValid" style="display: none;">
                    Arquivo selecionado com sucesso
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-12">
                <label for="descricaoAnexo">Descri��o do documento: <span class="text-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" id="descricaoAnexo" name="descricaoAnexo"
                       maxlength="50">
                <div class="invalid-feedback" id="descricaoAnexoFeedback">
                    Por favor informe o descri��o do documento
                </div>
                <div class="invalid-feedback" id="descricaoAnexoDuplicado">
                    J� existe um anexo com a mesma descri��o
                </div>
            </div>
        </div>

        <div class="form-group row mt-3">
            <div class="col-md-12 col-md-offset-3">
                <button type="button" class="btn btn-danger btn-sm mt-1" name="botaoLimparAnexo" id="botaoLimparAnexo"
                        onclick="limparAnexo();">
                    <i class="fa fa-times-circle"></i> Limpar
                </button>
                <button type="button" class="btn btn-primary btn-sm mt-1" name="botaoIncluirAnexo" id="botaoIncluirAnexo"
                        onclick="incluirAnexo(this.form,'<c:out value="${param['actionAdicionarAnexo']}"/>');">
                    <i class="fa fa-plus-circle"></i> Adicionar anexo
                </button>
                <button disabled="disabled" type="button" class="btn btn-primary btn-sm mt-1" name="botaoAlterarAnexo" id="botaoAlterarAnexo"
                        onclick="alterarAnexo(this.form,'<c:out value="${param['actionAdicionarAnexo']}"/>');">
                    <i class="fa fa-edit"></i> Alterar anexo
                </button>
            </div>
        </div>

    </div>


    <div class="col-md-6" id="gridAnexos">
        <jsp:include page="/jsp/atendimento/comumchamadoservicoautorizacao/bootstrap/gridAnexosBootstrap.jsp"></jsp:include>
    </div>
</div>


<script language="javascript">

    function visualizarAnexo() {

        var index = document.forms[0].indexLista.value;
        var url = '<c:out value="${param['actionVisualizarAnexo']}"/>';

        var conector = "?";
        if (url.indexOf("?") >= 0) {
            conector = "&";
        }

        popup = window.open(url + conector + "indexLista=" + index, 'popup', 'height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
    }

    function limparAnexo() {
        document.forms[0].indexLista.value = -1;
        document.forms[0].descricaoAnexo.value = "";

        $('#botaoAlterarAnexo').attr('disabled', true);
        $('#botaoLimparAnexo').attr('disabled', false);
        $('#botaoIncluirAnexo').attr('disabled', false);
        $('#botaoVisualizarAnexo').attr('disabled', true);


        $('#documentoAnexoFeedback').hide();
        $('#documentoAnexoFeedbackValid').hide();
        $('#descricaoAnexoFeedback').hide();
        $('#descricaoAnexoDuplicado').hide();

        var url = '<c:out value="${param['actionLimparCampoArquivo']}"/>';

        $("#divCampoArquivo").load(url, function () {
            }
        );
    }


    function incluirAnexo(form, actionAdicionarAnexo) {
        if (camposValidos()) {
            var nomeForm = "<c:out value='${param["nomeForm"]}'/>"
            document.forms[nomeForm].indexLista.value = -1;
            carregarFragmentoPOST('gridAnexos', actionAdicionarAnexo, document.forms[nomeForm], true);
            limparAnexo();
            iniciarDatatable('#table-grid-anexo');
            $("#documentoAnexoFeedback").hide();
            $("#documentoAnexoFeedbackValid").hide();
            $("#descricaoAnexoDuplicado").hide();
        }
    }

    function alterarAnexo(form, actionAdicionarAnexo) {

        if (camposValidos()) {
            var nomeForm = "<c:out value='${param["nomeForm"]}'/>"
            carregarFragmentoPOST('gridAnexos', actionAdicionarAnexo, document.forms[nomeForm]);
            limparAnexo();
            iniciarDatatable('#table-grid-anexo');
            $("#documentoAnexoFeedback").hide();
            $("#documentoAnexoFeedbackValid").hide();
            $("#descricaoAnexoDuplicado").hide();
        }
    }


    function exibirAlteracaoDoAnexo(indexLista, chavePrimariaAnexo, descricaoAnexo) {

        if (indexLista != "") {
            document.forms[0].indexLista.value = indexLista;

            if (descricaoAnexo != "") {
                document.forms[0].descricaoAnexo.value = descricaoAnexo;
            } else {
                document.forms[0].descricaoAnexo.value = "";
            }

            if (chavePrimariaAnexo != "") {
                document.forms[0].chavePrimariaAnexo.value = chavePrimariaAnexo;
            }

            $('#botaoAlterarAnexo').attr('disabled', false);
            $('#botaoLimparAnexo').attr('disabled', false);
            $('#botaoIncluirAnexo').attr('disabled', true);
            $('#botaoVisualizarAnexo').attr('disabled', false);
            $("#documentoAnexoFeedback").hide();
            $("#documentoAnexoFeedbackValid").hide();
            $("#descricaoAnexoDuplicado").hide();
        }
    }

    function removerAnexo(indice) {
        var nomeForm = "<c:out value='${param["nomeForm"]}'/>"
        document.forms[nomeForm].indexLista.value = indice;
        var actionRemoverAnexo = '<c:out value="${param['actionRemoverAnexo']}" />';

        carregarFragmentoPOST('gridAnexos', actionRemoverAnexo, document.forms[nomeForm]);
        iniciarDatatable('#table-grid-anexo');
    }

    function camposValidos() {
        var valido = true;
        if ($('#arquivoAnexo').val() == '') {
            $('#documentoAnexoFeedback').html('Por favor informe o documento');
            $('#documentoAnexoFeedback').show();
            valido = false;
        } else {
            $('#descricaoAnexoFeedback').hide();
        }
        if ($('#descricaoAnexo').val() == '') {
            $('#descricaoAnexoFeedback').show();
            valido = false;
        } else {
            $('#descricaoAnexoFeedback').hide();
        }

        var descricoesJaAdicionadas = $("#table-grid-anexo").DataTable().data().toArray()
            .filter(function(v, index) {
                return index != document.forms[0].indexLista.value;
            }).map(function(data) {
                return data[0];
            });

        if (descricoesJaAdicionadas.includes($("#descricaoAnexo").val())) {
            $("#descricaoAnexoDuplicado").show();
            valido = false;
        } else {
            $("#descricaoAnexoDuplicado").hide();
        }
        return valido;
    }

</script>
