<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>	

<script>
	$(document).ready(function() {
		//Necess�rio para mudar aba do m�dulo do contrato
		if( typeof mudarAba == 'function'){
			mudarAba(<c:out value="${abaAtual}"/>); 
		}
	});
</script>

	<c:set var="x" value="0" />
	
	<display:table class="dataTableGGAS dataTableAba" name="listaAnexos" sort="list" id="anexo" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		
		<display:column sortable="false" title="Descri��o" style="width: 80px" maxLength="50">${anexo.descricaoAnexo}</display:column>
			
		<display:column sortable="false" title="Data Inclus�o" style="width: 80px">
			<fmt:formatDate pattern="dd/MM/yyyy" value="${anexo.ultimaAlteracao}" var="formattedStatusDate" />
			<c:out value="${formattedStatusDate}"/>
			
			<fmt:formatDate pattern="HH:mm" value="${anexo.ultimaAlteracao}" var="formattedStatusHour" />
			<c:out value="${formattedStatusHour}"/>
		</display:column>
		
		<display:column class="colunaSemTitulo" style="text-align: center; width: 25px"> 
			<a href="javascript:exibirAlteracaoDoAnexo('${x}','${anexo.chavePrimaria}','${anexo.descricaoAnexo}');"><img title="Alterar Anexo" alt="Alterar Anexo"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a> 
		</display:column>
		
		<display:column style="text-align: center; width: 25px"> 
			<a onclick="return confirm('Deseja excluir o Anexo?');" href="javascript:removerAnexo(<c:out value="${x}"/>);"><img title="Excluir Anexo" alt="Excluir Anexo"  src="<c:url value="/imagens/deletar_x.png"/>" border="0" /></a>
		</display:column>	
		
		<c:set var="x" value="${x+1}" />
		
	</display:table>