<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery-weekdays.js"></script>

<link rel="stylesheet" media="all" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/jquery-weekdays.css"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js" integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js" integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type="text/javascript">

$(document).ready(function(){
	
    let horariosSalvos = JSON.parse(localStorage.getItem('horariosSalvos')) || {};
    
	var isGerarComunicacao = $("#isGerarComunicacao").val();
	
	if(!isGerarComunicacao) {
		localStorage.removeItem("horariosSalvos");
	} else {
	    let selecionados = $('#turno').val();
	    let container = $('#turno-horarios');
	
	    if (selecionados && selecionados.length > 0) {
	        selecionados.forEach(function (turno) {
	            let turnoTexto = $('#turno option[value="' + turno + '"]').text().trim();
	
	            let label = $('<label>').text('Hor�rio para ' + turnoTexto).addClass('d-block mt-2');
	            let input = $('<input>').attr({
	                type: 'time',
	                id: 'horario_' + turnoTexto,
	                name: 'horario_' + turnoTexto,
	                class: 'form-control form-control-sm',
	                value: horariosSalvos[turnoTexto] || ''
	            }).css('font-size', '1.2rem');
	
	            let div = $('<div>').addClass('form-group').append(label, input);
	            container.append(div);
	        });
	
	        $('#horaServico').prop('disabled', true);
	    }
		
	}
	
    
    

	iniciarDatatable('#pontoConsumo');
	
	var datepicker = $.fn.datepicker.noConflict();

	var date = new Date();
	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	
	$.fn.bootstrapDP = datepicker;  
	$('.bootstrapDP').bootstrapDP({
	    autoclose: true,
		format: 'dd/mm/yyyy',
		language: 'pt-BR',
		startDate: "today"
	});

	$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});



    $("#horaServico").inputmask("99:99", {placeholder: "_", greedy: false});

    $('#horaServico').timepicker({
    	  timeFormat: 'HH:mm',
    	  defaultTime: '08:00',
    	});
    
    
	$('#turno').select2({
		language: 'pt-BR',
		theme: 'bootstrap4',
        placeholder: "Selecione uma ou mais op��es",
        containerCssClass: ':all:',
        language: 'pt-BR',
        width: null

	});

	$('#turno').on('change', function () {
	    let selecionados = $(this).val();
	    let container = $('#turno-horarios');

	    let horariosSalvos = {};
	    container.find('input[type="time"]').each(function () {
	        let turno = $(this).attr('name').replace('horario_', '');
	        horariosSalvos[turno] = $(this).val();
	    });

	    container.empty();

	    if (selecionados && selecionados.length > 0) {
	        selecionados.forEach(function (turno) {
	            let turnoTexto = $('#turno option[value="' + turno + '"]').text().trim();

	            let label = $('<label>').text('Hor�rio para ' + turnoTexto).addClass('d-block mt-2');
	            let input = $('<input>').attr({
	                type: 'time',
	                id: 'horario_' + turnoTexto,
	                name: 'horario_' + turnoTexto,
	                class: 'form-control form-control-sm',
	                value: horariosSalvos[turnoTexto] || ''
	            }).css('font-size', '1.2rem');

	            let div = $('<div>').addClass('form-group').append(label, input);
	            container.append(div);
	        });

	        $('#horaServico').prop('disabled', true);
	    } else {
	        $('#horaServico').prop('disabled', false);
	    }
        localStorage.setItem('horariosSalvos', JSON.stringify(horariosSalvos));
	});

    $('#turno-horarios').on('change', 'input[type="time"]', function () {
        let turnoTexto = $(this).attr('name').replace('horario_', '');
        horariosSalvos[turnoTexto] = $(this).val();
        localStorage.setItem('horariosSalvos', JSON.stringify(horariosSalvos));
    });
    
});

function gerarLote() {
	$("#servicoTipo").removeAttr('disabled');
	submeter('comunicacaoLoteForm','gerarComunicacaoLote');
}

function roteirizar() {
	var diasSelecionados = [];
    var turnosSelecionados = $('#turno').val();
    var horariosPreenchidos = true;

	if (turnosSelecionados && turnosSelecionados.length > 0) {
		turnosSelecionados.forEach(function(turno) {
			var turnoTexto = $('#turno option[value="' + turno + '"]').text().trim();
			var horario = $('input[name="horario_' + turnoTexto + '"]').val();

			if (!horario) {
				horariosPreenchidos = false;
			}
		});

		if (!horariosPreenchidos) {
			alert("Preencha todos os hor�rios para os turnos selecionados.");
			return;
		}
	}

	if(validarDiasSelecionados()){
		$('#weekdays').selectedIndexes().map(function(nome, i) {
			diasSelecionados.push(i);
		}); 
		
		exibirIndicador();
		$("#servicoTipo").removeAttr('disabled');
		$("#diasSemana").val(diasSelecionados);
		submeter('comunicacaoLoteForm','roteirizarComunicacaoLote');
	}

}

$(function(){

	var dias = "<c:out value="${diasSemana}"/>";

	if(!dias) {
		dias = [1,2,3,4,5];
	} 
	
	$('#weekdays').weekdays({
		selectedIndexes: dias,
		days: ["Domingo", "Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado"]
	});
});

function validarDiasSelecionados() {
	if($('#weekdays').selectedIndexes().length > 0) {
		return true;
	}

	alert("Selecione pelo menos um dia da semana!");
	return false;
}

function alterarLote(){
	$("#servicoTipo").removeAttr('disabled');
	submeter('comunicacaoLoteForm','alterarLote');
}

function gerarPlanilhaExcel() {
	$("#servicoTipo").removeAttr('disabled');
	$("input[type=hidden][class=chavesPrimariasDinamicas][name=chavesPrimarias]").remove();

	var tabela = $('#pontoConsumo').DataTable(); 

	var parametros = tabela.$("td[id^='chk_']").map(function() {
		return this.id.split("chk_")[1];
	}).get();

	var form = $("#comunicacaoLoteForm");

	$.each(parametros, function(index, value) {
		if ($("#chk" + value).length == 0) {
			$(form).append(
				$('<input>').attr('type', 'hidden')
					.attr('class', 'chavesPrimariasDinamicas')
				    .attr('name', 'chavesPrimarias')
				    .val(value)
			);
		}
	});

	submeter('comunicacaoLoteForm', 'gerarPlanilhaExcel');
	$("#servicoTipo").removeAttr('enable');

}
</script>

<style>
    .select2-selection--multiple:before {
        visibility:hidden;
    }
</style>


<div class="bootstrap">
	<form:form action="gerarComunicacaoLote" id="comunicacaoLoteForm" name="comunicacaoLoteForm" method="post" modelAttribute="GeracaoComunicaoLoteVO">
		<input type="hidden" id="fluxoAlteracao" class="fluxoAlteracao" name="fluxoAlteracao"></input>
		<input type="hidden" id="isGerarComunicacao" value="${isGerarComunicacao}">
		
		<div class="card">
				<div class="card-header">
                <c:choose>
                    <c:when test="${ fluxoAlteracao eq true }"><h5 class="card-title mb-0">Alterar Comunica��o de Interrup��o Em Lote</h5></c:when>
                    <c:when test="${ fluxoAlteracao ne true }"><h5 class="card-title mb-0">Gerar Comunica��o de Interrup��o Em Lote</h5></c:when>
                </c:choose>				
				</div>
				<div class="card-body">
					<div class="alert alert-primary fade show" role="alert">
						<i class="fa fa-question-circle"></i> Preencha os campos para
						pesquisar os pontos de consumos, clique em <b>Roterizar</b>
						para roterizar os pontos de consumo e clique em <b>Gerar</b> para gerar a comunica��o em lote.
					</div>
	
					<hr>
	
					<div class="card">
						<div class="card-body bg-light">
						
							<div class="row mb-2">
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-12">
											<label for="servicoTipo">Tipo de Servi�o:<span class="text-danger">*</span></label> 
											<c:choose> 
												<c:when test="${ fluxoAlteracao ne true }">
													<select
														name="servicoTipo" id="servicoTipo"
														class="form-control form-control-sm" disabled>
														<option value="-1">Selecione</option>
														<c:forEach items="${listaServicoTipo}"
															var="servicoTipo">
															<option
																value="<c:out value="${servicoTipo.chavePrimaria}"/>"
																<c:if test="${filtroGeracaoComunicacaoLoteDTO.servicoTipo.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
																<c:out value="${servicoTipo.descricao}" />
															</option>
														</c:forEach>
													</select>
												</c:when>
												<c:when test="${ fluxoAlteracao eq true }">
													<select
														name="servicoTipo" id="servicoTipo"
														class="form-control form-control-sm" disabled>
														<option value="${servicoTipo.chavePrimaria}">${servicoTipo.descricao}</option>
													</select>
												</c:when>
											</c:choose>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Tempo de Execu��o Prevista do Servi�o:<span class="text-danger">*</span></label>
												<input class="form-control form-control-sm" type="text" 
												id="tempoExecucao" name="tempoExecucao" onkeypress="return formatarCampoInteiro(event);" maxlength="10" 
												<c:if test="${fluxoAlteracao ne true}">value = "${filtroGeracaoComunicacaoLoteDTO.servicoTipo.quantidadeTempoMedio}"</c:if>
												<c:if test="${fluxoAlteracao eq true}"> value = "${tempoExecucao}"</c:if>
												>
												

										</div>
		                            </div>
								</div>								
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Data Prevista Servi�o:<span class="text-danger">*</span></label>
												<input class="form-control form-control-sm campoData bootstrapDP" type="text" id="dataServico" name="dataServico" maxlength="10" value="${filtroGeracaoComunicacaoLoteDTO.dataServico}">
										</div>
		                            </div>
								</div>
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Hora Prevista de Sa�da:<span class="text-danger">*</span></label>
												<input class="form-control form-control-sm" type="text" id="horaServico" name="horaServico" maxlength="5" value="${filtroGeracaoComunicacaoLoteDTO.horaServico}">
										</div>
		                            </div>
								</div>
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
	                                        <label for="turno" class="float-none">Turno</label>
	                                        <select class="form-control form-control-sm" id="turno" name="turno" multiple>
	                                            <c:forEach var="turno" items="${listaTurno}">
	                                                <option value="${turno.chavePrimaria}"
	                                                <c:if test="${turnosSelecionados.contains(turno.chavePrimaria)}">selected="selected"</c:if>>
	                                                ${turno.descricao}
	                                                </option>
	                                            </c:forEach>
	                                        </select>
	                                        <div id="turno-horarios" class="mt-3"></div>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-row">
										<label for="habilitado" class="col-md-12">Gerar Autoriza��o de Servi�o:</label>
										<div class="col-md-10">
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="isGerarAutorizacaoServicoSim" name="isGerarAutorizacaoServico" class="custom-control-input" value="true"
													   <c:if test="${filtroGeracaoComunicacaoLoteDTO.isGerarAutorizacaoServico eq true or filtroGeracaoComunicacaoLoteDTO.isGerarAutorizacaoServico eq null}">checked="checked"</c:if>>
												<label class="custom-control-label" for="medicacaoIndividual">Sim</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="isGerarAutorizacaoServicoNao" name="isGerarAutorizacaoServico" class="custom-control-input" value="false"
													   <c:if test="${filtroGeracaoComunicacaoLoteDTO.isGerarAutorizacaoServico eq false}">checked="checked"</c:if>>
												<label class="custom-control-label" for="isGerarAutorizacaoServicoNao">N�o</label>
											</div>
										</div>
									</div>															
								</div>															
								
								<div class="col-md-12">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Dias para execu��o do Servi�o:<span class="text-danger">*</span></label>
		                                    <br/><br/>
		                                    <input type="hidden" name="diasSemana" id="diasSemana"/>
		                                    <div class="form-row">
		                                   		 <div class="col-md-12" id="weekdays"> </div>
		                                    </div>
										</div>
		                            </div>
								</div>									
								
							</div>
							
							<div class="row mb-2">
													
								
							</div>
							
							<br/>
							<div class="row mt-3">
								<div class="col align-self-end text-right">
									<button class="btn btn-primary btn-sm" id="botaoPesquisar"
											type="button" onclick="roteirizar();">
											<i class="fa fa-search"></i> Roteirizar Pontos de Consumo
									</button>
								</div>
							</div>
							
							<c:if test="${listaPontoConsumo ne null}">
								<hr class="linhaSeparadora1" />
								<fieldset class="conteinerBloco">
									<div class="alert alert-primary" role="alert">
										<p class="orientacaoInicial">O Ponto de Partida est� definido a partir da <b>ALG�S</b>. </p>
									</div>								
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover" id="pontoConsumo" style="width:100%">
											<thead class="thead-ggas-bootstrap">
												<tr>
													<th scope="col" class="text-center">Ponto de Consumo</th>
													<th scope="col" class="text-center">C�digo do Ponto de Consumo</th>
													<th scope="col" class="text-center">Segmento</th>
													<th scope="col" class="text-center">Dist�ncia entre Pontos (km)</th>
													<th scope="col" class="text-center">Tempo entre Pontos (minutos)</th>
													<th scope="col" class="text-center">Sequencial</th>
													<th scope="col" class="text-center">Equipe</th>	
													<th scope="col" class="text-center">Data Execu��o</th>
													<th scope="col" class="text-center">Previs�o de Chegada (horas)</th>
													<th scope="col" class="text-center">Tempo Execu��o (minutos)</th>	
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${listaPontoConsumo}" var="comunicacaoLoteVO">
													<tr>
														<td id="chk_${comunicacaoLoteVO.pontoConsumo.chavePrimaria}"class="text-center">
												        	<c:out value="${comunicacaoLoteVO.pontoConsumo.descricao} - ${comunicacaoLoteVO.pontoConsumo.imovel.nome}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${comunicacaoLoteVO.pontoConsumo.codigoPontoConsumo}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${comunicacaoLoteVO.pontoConsumo.segmento.descricao}"/>
														</td>
														<td class="text-center"><fmt:formatNumber type="number"
															maxFractionDigits="2"
															value="${comunicacaoLoteVO.distanciaEntrePontos}" />
														</td>
														<td class="text-center">
												        	<c:out value="${comunicacaoLoteVO.tempoDistancia}"/>
														</td>														
														<td class="text-center">
												        	<c:out value="${comunicacaoLoteVO.sequencial}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${comunicacaoLoteVO.servicoTipoAgendamentoTurno.equipe.nome}"/>
														</td>														
														<td class="text-center">
												        	<c:out value="${comunicacaoLoteVO.dataExecucao}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${fn:replace(fn:replace(comunicacaoLoteVO.periodoExecucao, 'horas', ''),'e', '-')}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${comunicacaoLoteVO.tempoExecucao}"/>
														</td>																																																																						
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
									
								</fieldset>
								
							
								<div class="row mt-3">
									<c:if test="${fluxoAlteracao ne true }">
										<div class="col align-self-end text-right">
											<button class="btn btn-primary" id="botaoPlanilha"
												type="button" onclick="gerarPlanilhaExcel();">
													Exportar Para Planilha Excel
											</button>										
											<button class="btn btn-primary btn-sm" id="botaoPesquisar"
													type="button" onclick="gerarLote();">
													<i class="fa fa-search"></i> Gerar Lote
											</button>
										</div>									
									</c:if>
									<c:if test="${fluxoAlteracao eq true }">
										<div class="col align-self-end text-right">
											<button class="btn btn-primary btn-sm" id="botaoPesquisar"
													type="button" onclick="alterarLote();">
													<i class="fa fa-search"></i> Alterar Lote
											</button>
										</div>									
									</c:if>
								</div>					
							</c:if>												
							
						</div>
					</div>
				</div>
			</div>
	 </form:form>
</div>