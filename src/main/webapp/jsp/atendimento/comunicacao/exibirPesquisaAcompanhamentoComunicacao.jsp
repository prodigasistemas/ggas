<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	var logRelatorioComunicacao = "<c:out value="${sessionScope.relatorioComunicacaoLote}"/>";
	if(logRelatorioComunicacao != ""){
		submeter('comunicacaoForm', 'exibirRelatorioComunicacaoLote');
	}	
	
	
	var logRelatorioASLote = "<c:out value="${sessionScope.relatorioAutorizacaoServicoLote}"/>";
	if(logRelatorioASLote != ""){
		submeter('comunicacaoForm', 'exibirRelatorioASLote');
	}
	
	var datepicker = $.fn.datepicker.noConflict();
	$.fn.bootstrapDP = datepicker;  
	$('.bootstrapDP').bootstrapDP({
	    autoclose: true,
		format: 'dd/mm/yyyy',
		language: 'pt-BR'
	});

	$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});

	iniciarDatatable('#loteComunicacao');
	iniciarDatatable('#itemLoteComunicacao');
});

function pesquisar() {
	submeter('comunicacaoForm','pesquisarAcompanhamentoComunicacao');
}

function limparFormulario(){		
	$('#cityGate option[value=-1]').attr('selected','selected');
	$('#segmento option[value=-1]').attr('selected','selected');
	$('#numeroMedidor').val('');
	$('#chaveCep').val('');
	$('#cepComunicacao').val('');
	carregarQuadras('');
	carregarTroncos($("#cityGate"));
}

function exibirDetalhamento(chave) {
    document.forms[0].chavePrimaria.value = chave;
    submeter('comunicacaoForm', 'exibirDetalhamentoServicoAutorizacao');
}

function limparFormulario(){		
	$('#servicoTipo option[value=-1]').attr('selected','selected');
	$('#descricaoPontoConsumo').val('');
	$('#dataGeracaoInicial').val('');
	$('#dataGeracaoFinal').val('');
	$('#autorizacaoTodos').attr('checked','checked');
	$('#protocoloTodos').attr('checked','checked');
	$('#loteCartasCanceladoNao').attr('checked','checked');
}

function imprimirSegundaVia(chaveLote) {
	$("#idLoteComunicacao").val(chaveLote);

	submeter("comunicacaoForm", 'imprimirSegundaViaComunicacao');
}

function exibirDetalhamentoLoteComunicacao(chaveLote) {
	$("#idLoteComunicacao").val(chaveLote);

	submeter("comunicacaoForm", 'exibirDetalhamentoLoteComunicacao');
}


function exibirMensagemRetornoProtocolo(chavePrimaria) {
    AjaxService.consultarInformacoesProtocolo(chavePrimaria, {
        callback: function (retorno) {
        $("#descricaoRetornoProtocolo").val(retorno[0]);
        $("#funcionario").val(retorno[1]);
        
        $('#observacaoRetornoPopup').modal('show');
        }, async: false
    });
}

function cancelarLoteComunicacao(){
	if ( verificarSelecaoApenasUm()) {
		
		if( confirm("Tem certeza que deseja realizar o cancelamento?") ){
			if( confirm("Deseja inserir o lote de cancelamento?") ){
				$("#isGerarLoteCancelamento").val(true);
			}
			
			document.forms["comunicacaoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("comunicacaoForm", "cancelarLoteComunicacao");
		}
	}

}


	function cancelarItemLoteComunicacao() {
		if (verificarSelecaoItemLote()) {
			if (confirm("Tem certeza que deseja realizar o cancelamento?")) {
				if (confirm("Deseja inserir o lote de cancelamento?")) {
					$("#isGerarLoteCancelamento").val(true);
				}

				submeter("comunicacaoForm", "cancelarItemLoteComunicacao");
			}
		}
	}

	function imprimirSegundaViaAS(chaveLote) {
		$("#idLoteComunicacao").val(chaveLote);

		submeter("comunicacaoForm", 'imprimirSegundaViaComunicacaoAS');
	}
	
	function alterarLoteComunicacao(){
		if(verificarSelecaoApenasUm()){
			document.forms["comunicacaoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter("comunicacaoForm","exibirAlteracaoLoteComunicacao");
		}
	}
	
	function gerarRelatorio() {
		submeter("comunicacaoForm","gerarRelatorioComunicacao");
		$('#tipoRelatorioPopup').modal('toggle');
	}
	
	
	function exibirDialogGerarRelatorio(tipoRelatorio) {
		
		if( verificarSelecao()) {
			$("#tipoRelatorio").val(tipoRelatorio);
			$('#tipoRelatorioPopup').modal('show');
		}
		
	}
	
	function exibirGerarAutorizacaoServicoComunicacao() {
		if(verificarSelecaoApenasUm()) {
			var tabela = $("#loteComunicacao").DataTable();
			
			var linhaSelecionada = tabela.$('input:checked').closest('tr');
			
			var colunaProtocolosRetornados = tabela.cell(linhaSelecionada, 4).data();
			
			var conteudoColuna = $(colunaProtocolosRetornados).attr('src');
			
			if (conteudoColuna && conteudoColuna.split('/').pop() === 'circle_green.png') {
				alert("Todos Protocolos j� foram retornados!");
			} else {
				var parametros = tabela.$('input:checked').serializeArray();
				$("#chavePrimaria").val(parametros[0].value);
				submeter("comunicacaoForm", "exibirGerarAutorizacaoServicoComunicacao");
			}
			
		}
	}
	
	
</script>


<div class="bootstrap">
	<form:form action="pesquisarAcompanhamentoComunicacao" id="comunicacaoForm" name="comunicacaoForm" method="post" modelAttribute="FiltroAcompanhamentoLoteDTO">
		<input type="hidden" name="chavePrimaria" id="chavePrimaria" value=""/>
		<input name="idLoteComunicacao" type="hidden" id="idLoteComunicacao" value="${numeroLote}">
		<input name="isGerarLoteCancelamento" type="hidden" id="isGerarLoteCancelamento" value="false">
		<input type="hidden" id="tipoGeracaoLote" name="tipoGeracaoLote" value="tab-automatica" />	
		<input type="hidden" id="fluxoAlteracao" name="fluxoAlteracao"/>
		<input type="hidden" id="tipoRelatorio" name="tipoRelatorio"/>
		<input type="hidden" id="chavePrimaria" name="chavePrimaria" />
		<input type="hidden" id="tipoGeracaoLote" name="tipoGeracaoLote" value="tab-automatica" />	

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Acompanhamento de Comunica��o de Interrup��o</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para
					pesquisar os pontos de consumos gerados, clique em <b>Pesquisar</b>
					para exibir os pontos de consumo gerados.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">
						<div class="row mb-2">
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="servicoTipo">Tipo de Servi�o:</label> <select
											name="servicoTipo" id="servicoTipo"
											class="form-control form-control-sm"">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaServicoTipo}"
												var="servicoTipo">
												<option
													value="<c:out value="${servicoTipo.chavePrimaria}"/>"
													<c:if test="${acompanhamento.servicoTipo.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${servicoTipo.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="descricaoPontoConsumo">Descri��o Ponto de Consumo:</label> 
										<input class="form-control form-control-sm" type="text"
										id="descricaoPontoConsumo" name="descricaoPontoConsumo"
										value="${acompanhamento.descricaoPontoConsumo}" maxlength="50"
										size="30"
										onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<label for="protocoloRetornado" class="col-md-12">Lotes com Itens com Protocolo Retornado:</label>
								<div class="col-md-10">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="protocoloSim" name="protocoloRetornado" class="custom-control-input" value="true"
											   <c:if test="${acompanhamento.protocoloRetornado eq 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="protocoloSim">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="protocoloNao" name="protocoloRetornado" class="custom-control-input" value="false"
											   <c:if test="${acompanhamento.protocoloRetornado eq 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="protocoloNao">N�o</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="protocoloTodos" name="protocoloRetornado" class="custom-control-input" value=""
											   <c:if test="${acompanhamento.protocoloRetornado eq null}">checked="checked"</c:if>>
										<label class="custom-control-label" for="protocoloTodos">Todos</label>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<label for="autorizacaoGerada" class="col-md-12">Lotes com Itens com Autoriza��o de Servi�o Gerada:</label>
								<div class="col-md-10">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="autorizacaoSim" name="autorizacaoGerada" class="custom-control-input" value="true"
											   <c:if test="${acompanhamento.autorizacaoGerada eq 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="autorizacaoSim">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="autorizacaoNao" name="autorizacaoGerada" class="custom-control-input" value="false"
											   <c:if test="${acompanhamento.autorizacaoGerada eq 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="autorizacaoNao">N�o</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="autorizacaoTodos" name="autorizacaoGerada" class="custom-control-input" value=""
											   <c:if test="${acompanhamento.autorizacaoGerada eq null}">checked="checked"</c:if>>
										<label class="custom-control-label" for="autorizacaoTodos">Todos</label>
									</div>
								</div>
							</div>							
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
	                                    <label>Per�odo Gera��o do Lote:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataGeracaoInicial" name="dataGeracaoInicial" maxlength="10" value="${acompanhamento.dataGeracaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataGeracaoFinal" name="dataGeracaoFinal" maxlength="10" value="${acompanhamento.dataGeracaoFinal}">
										</div>
									</div>
	                            </div>
							</div>

							<div class="col-md-6">
								<label for="loteCancelado" class="col-md-12">Lotes Cancelados:</label>
								<div class="col-md-10">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="loteCanceladoSim" name="loteCancelado" class="custom-control-input" value="false"
											   <c:if test="${acompanhamento.loteCancelado eq 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="loteCanceladoSim">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="loteCanceladoNao" name="loteCancelado" class="custom-control-input" value="true"
											   <c:if test="${acompanhamento.loteCancelado eq 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="loteCanceladoNao">N�o</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="loteCanceladoTodos" name="loteCancelado" class="custom-control-input" value=""
											   <c:if test="${acompanhamento.loteCancelado eq null}">checked="checked"</c:if>>
										<label class="custom-control-label" for="loteCanceladoTodos">Todos</label>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<label for="loteCancelado" class="col-md-12">Lotes com Cartas Canceladas:</label>
								<div class="col-md-10">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="itemLoteCanceladoSim" name="itemLoteCancelado" class="custom-control-input" value="false"
											   <c:if test="${acompanhamento.itemLoteCancelado eq 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="itemLoteCanceladoSim">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="itemLoteCanceladoNao" name="itemLoteCancelado" class="custom-control-input" value="true"
											   <c:if test="${acompanhamento.itemLoteCancelado eq 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="itemLoteCanceladoNao">N�o</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="itemLoteCanceladoTodos" name="itemLoteCancelado" class="custom-control-input" value=""
											   <c:if test="${acompanhamento.itemLoteCancelado eq null}">checked="checked"</c:if>>
										<label class="custom-control-label" for="itemLoteCanceladoTodos">Todos</label>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<label for="loteCancelado" class="col-md-12">Lotes Gerado para Cancelamento:</label>
								<div class="col-md-10">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="loteCartasCanceladoSim" name="lotesCartasCanceladas" class="custom-control-input" value="true"
											   <c:if test="${acompanhamento.lotesCartasCanceladas eq 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="loteCartasCanceladoSim">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="loteCartasCanceladoNao" name="lotesCartasCanceladas" class="custom-control-input" value="false"
											   <c:if test="${acompanhamento.lotesCartasCanceladas eq 'false' || acompanhamento.lotesCartasCanceladas eq null}">checked="checked"</c:if>>
										<label class="custom-control-label" for="loteCartasCanceladoNao">N�o</label>
									</div>
								</div>
							</div>																																																								
							
						</div>
						
						<br/>
						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoPesquisar"
										type="button" onclick="pesquisar();">
										<i class="fa fa-search"></i> Pesquisar
								</button>
								<button class="btn btn-secondary btn-sm" name="botaoLimpar"
									id="botaoLimpar" value="limparFormulario" type="button"
									onclick="limparFormulario();">
									<i class="far fa-trash-alt"></i> Limpar
								</button>
							</div>
						</div>						
						
					</div>
				</div>
				
				<c:if test="${listaLoteAcompanhamento ne null}">
					<hr class="linhaSeparadora1" />
					<fieldset class="conteinerBloco">
						<h5>Lotes de Comunica��o</h5>
						<div class="alert alert-primary" role="alert">
							<p class="orientacaoInicial">Para abrir os itens do lote basta clicar no lote. </p>
						</div>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="loteComunicacao" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th>
										</th>									
										<th scope="col" class="text-center">N�mero do Lote</th>
										<th scope="col" class="text-center">Tipo de Servi�o</th>
										<th scope="col" class="text-center">Data da Gera��o</th>
										<th scope="col" class="text-center">Itens possuem Protocolo Retornado?</th>
										<th scope="col" class="text-center">Itens possuem Autoriza��o de Servi�o?</th>
										<th scope="col" class="text-center">Itens possuem Autoriza��o de Servi�o de Protocolo?</th>
										<th scope="col" class="text-center">Lote Cancelado?</th>
										<th scope="col" class="text-center">Segunda via da Comunica��o</th>
										<th scope="col" class="text-center">Segunda via da Autoriza��o de Servi�o</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaLoteAcompanhamento}" var="loteComunicacao">
										<tr>
											<td>
												<c:if test="${loteComunicacao.servicoTipo ne null and loteComunicacao.habilitado ne false}">
													<div
														class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
														data-identificador-check="chk${loteComunicacao.chavePrimaria}">
														<input id="chk${loteComunicacao.chavePrimaria}" type="checkbox"
															name="chavesPrimarias" class="custom-control-input"
															value="${loteComunicacao.chavePrimaria}"> <label
															class="custom-control-label p-0"
															for="chk${loteComunicacao.chavePrimaria}"></label>
												  	</div>
												  </c:if>
											</td>										
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoLoteComunicacao(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
									        		<c:out value="${loteComunicacao.chavePrimaria}"/>
									        	</a>
											</td>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoLoteComunicacao(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
												<c:choose>
													<c:when test="${loteComunicacao.servicoTipo ne null}">
									        			<c:out value="${loteComunicacao.servicoTipo.descricao}"/>
									        		</c:when>
									        		<c:otherwise>
									        			Lote Gerado para Cancelamento
									        		</c:otherwise>
									        	</c:choose>
									        	</a>
											</td>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoLoteComunicacao(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
									        		<fmt:formatDate value="${loteComunicacao.dataGeracao}" pattern="dd/MM/yyyy"/>
									        	</a>
											</td>
											<td class="text-center">
									        	<c:choose>
									        		<c:when test="${loteComunicacao.getIsTodosProtocolosRetornados()}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
				               						</c:when>
				               						<c:when test="${loteComunicacao.getIsPeloMenosUmProtocoloRetornado()}">
				               							<img id="imagem" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                    				<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				                					</c:otherwise>
				            					</c:choose>
											</td>
											<td class="text-center">
									        	<c:choose>
									        		<c:when test="${loteComunicacao.getisExisteASParaTodosItens()}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
				               						</c:when>
				               						<c:when test="${loteComunicacao.getisExistePeloMenosUmaASParaItens()}">
				               							<img id="imagem" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                    				<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				                					</c:otherwise>
				            					</c:choose>
											</td>
											<td class="text-center">
									        	<c:choose>
									        		<c:when test="${loteComunicacao.getIsExisteTodasUmaAutorizacaoProtocolo()}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
				               						</c:when>
				               						<c:when test="${loteComunicacao.getIsPeloMenosUmaAutorizacaoProtocolo()}">
				               							<img id="imagem" src="<c:url value="/imagens/circle_yellow.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                    				<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				                					</c:otherwise>
				            					</c:choose>
											</td>											
											<td class="text-center">
									        	<c:choose>
									        		<c:when test="${loteComunicacao.habilitado eq false}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                    				<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				                					</c:otherwise>
				            					</c:choose>
											</td>																							
											<td class="text-center">
												<c:if test="${ loteComunicacao.habilitado eq true }">
												<a href='javascript:imprimirSegundaVia(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
													<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via Comunica��o" title="Imprimir 2� Via Comunica��o" border="0" style="margin-top: 5px;" >
												</a>
											</c:if>
											</td>
											<td class="text-center">
												<c:if test="${ loteComunicacao.habilitado eq true and loteComunicacao.getisExistePeloMenosUmaASParaItens()}">
												<a href='javascript:imprimirSegundaViaAS(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
													<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via AS" title="Imprimir 2� Via AS" border="0" style="margin-top: 5px;" >
												</a>
											</c:if>
											</td>																						
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						
						<div class="row mt-3">
						<!--  
							<div class="col align-self-start text-left">
								<button class="btn btn-primary btn-sm" id="botaoCancelar"
								        type="button" onclick="exibirAlterarLoteComunicacao();">
								            Alterar Lote
								</button>
							</div>
						-->								
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoGerarASProtocolo"
										type="button" onclick="exibirGerarAutorizacaoServicoComunicacao()">
											Gerar Autoriza��o de Servi�o de Protocolo
								</button>							
								<button class="btn btn-primary btn-sm" id="botaoRelatorioAnalitico"
										type="button" onclick="exibirDialogGerarRelatorio(true);">
											Relat�rio An�litico
								</button>
								<button class="btn btn-primary btn-sm" id="botaoRelatorioSintetico"
										type="button" onclick="exibirDialogGerarRelatorio(false);">
											Relat�rio Sint�tico
								</button>															
								<button class="btn btn-primary btn-sm" id="botaoCancelar"
										type="button" onclick="cancelarLoteComunicacao();">
											Cancelar Lote
								</button>
							</div>
						</div>						
						
					</fieldset>
				</c:if>				
				
				</br></br>
 				<c:if test="${listaItensLoteComunicacao ne null}">
					<hr class="linhaSeparadora1" />
					<fieldset class="conteinerBloco">
					<h5>Lote n�mero: ${numeroLote}</h5>
						<div class="alert alert-primary" role="alert">
							<p class="orientacaoInicial">Caso haja Autoriza��o de Servi�o Gerada, clique no icone verde para ser levado at� a mesma. </p>
						</div>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="itemLoteComunicacao" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th>
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
												<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
													class="custom-control-input"> <label
													class="custom-control-label p-0" for="checkAllAuto"></label>
											</div>
										</th>									
										<th scope="col" class="text-center">Ponto de Consumo</th>
										<th scope="col" class="text-center">C�digo do Ponto de Consumo</th>
										<th scope="col" class="text-center">Tipo de Servi�o</th>
										<th scope="col" class="text-center">Sequencial</th>
										<th scope="col" class="text-center">Data e Hora Planejada</th>
										<th scope="col" class="text-center">Data Retorno Protocolo</th>
										<th scope="col" class="text-center">Informa��es Retorno do Protocolo</th>
										<th scope="col" class="text-center">Possui Autoriza��o de Servi�o?</th>
										<th scope="col" class="text-center">Possui Autoriza��o de Servi�o de Protocolo?</th>
										<th scope="col" class="text-center">Carta Cancelada?</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaItensLoteComunicacao}" var="itemLoteComunicacao">
										<tr>
											<td>
												<c:if test="${ itemLoteComunicacao.habilitado eq true and (itemLoteComunicacao.servicoAutorizacao eq null or (itemLoteComunicacao.servicoAutorizacao.status.descricao == 'ABERTA' or itemLoteComunicacao.servicoAutorizacao.status.descricao == 'CANCELADA')) and itemLoteComunicacao.loteComunicacao.servicoTipo ne null }">
													<div
														class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
														data-identificador-check="chk${itemLoteComunicacao.chavePrimaria}">
														<input id="chkItem${itemLoteComunicacao.chavePrimaria}"
															type="checkbox" name="chavesPrimariasItem"
															class="custom-control-input"
															value="${itemLoteComunicacao.chavePrimaria}"> <label
															class="custom-control-label p-0"
															for="chkItem${itemLoteComunicacao.chavePrimaria}"></label>
													</div>
												</c:if>
											</td>										
											<td class="text-center">
									        	<c:out value="${itemLoteComunicacao.pontoConsumo.descricao}"/>
											</td>
											<td class="text-center">
									        	<c:out value="${itemLoteComunicacao.pontoConsumo.codigoPontoConsumo}"/>
											</td>
											<td class="text-center">
									        	<c:out value="${itemLoteComunicacao.loteComunicacao.servicoTipo.descricao}"/>
											</td>
											<td class="text-center">
									        	<c:out value="${itemLoteComunicacao.sequencialItem}"/>
											</td>
											<td class="text-center">
									        	<fmt:formatDate value="${itemLoteComunicacao.dataHoraExecucao}" pattern="dd/MM/yyyy HH:mm"/>
											</td>
											<td class="text-center">
												<c:if test="${itemLoteComunicacao.documentoNaoEntregue.indicadorRetornado}">
										        	<fmt:formatDate value="${itemLoteComunicacao.documentoNaoEntregue.dataSituacao}" pattern="dd/MM/yyyy"/>
												</c:if>
											</td>
											<td class="text-center">
												<c:if test="${itemLoteComunicacao.documentoNaoEntregue.indicadorRetornado or itemLoteComunicacao.documentoNaoEntregue.descricaoObservacaoRetorno ne null}">
													<a href='javascript:exibirMensagemRetornoProtocolo(<c:out value='"${itemLoteComunicacao.documentoNaoEntregue.chavePrimaria}"'/>);'>
														<img src="<c:url value="/imagens/icone_exibir_detalhes.png" />" alt="Exibir Observa��o Retorno" title="Exibir Observa��o Retorno" border="0" style="margin-top: 5px;" >
													</a>
												</c:if>
											</td>
											<td class="text-center">
												<c:choose>
									        		<c:when test="${itemLoteComunicacao.servicoAutorizacao eq null}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                					<a href="javascript:exibirDetalhamento(<c:out value='${itemLoteComunicacao.servicoAutorizacao.chavePrimaria}'/>);">
					                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
					                    				</a>
				                					</c:otherwise>
				            					</c:choose>
											</td>
											<td class="text-center">
												<c:choose>
									        		<c:when test="${itemLoteComunicacao.servicoAutorizacaoProtocolo eq null}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                					<a href="javascript:exibirDetalhamento(<c:out value='${itemLoteComunicacao.servicoAutorizacaoProtocolo.chavePrimaria}'/>);">
					                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
					                    				</a>
				                					</c:otherwise>
				            					</c:choose>
											</td>											
											<td class="text-center">
									        	<c:choose>
									        		<c:when test="${itemLoteComunicacao.habilitado eq false}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                    				<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				                					</c:otherwise>
				            					</c:choose>
											</td>																						
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						
						<div class="row mt-3">							
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoCancelar"
										type="button" onclick="cancelarItemLoteComunicacao();">
											Cancelar Item Lote
								</button>							
							</div>				
						</div>						
						
					</fieldset>
				</c:if>
				

				
			</div>
		</div>
		
		<!--  Modal Observa��o Retorno -->
		<div class="modal fade" id="observacaoRetornoPopup" tabindex="-1" role="dialog"
			aria-labelledby="observacaoRetornoPopup" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Informa��es Retorno</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
					    <div class="form-row">
					    	<div class="col-md-12">
					    		<label>Observa��o Retorno:</label>
						    	<textarea
						    		class="form-control form-control-sm" name="descricaoRetornoProtocolo"
								id="descricaoRetornoProtocolo" cols="60"
							    rows="6"
							    maxlength="800" disabled></textarea>
							</div>
						</div>
				 		<div class="form-row">
			            	<div class="col-md-12">
								<label>Usu�rio Respons�vel:</label>
								<input class="form-control form-control-sm" type="text"
									id="funcionario" name="funcionario" maxlength="50"
									disabled>
							</div>
						</div>													
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>


		<!--  Modal Tipo Relat�rio -->
		<div class="modal fade" id="tipoRelatorioPopup" tabindex="-1"
			role="dialog" aria-labelledby="tipoRelatorioPopup" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Gerar
							Relat�rio</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-row">
							<div class="col-md-6">
								<label for="autorizacaoGerada" class="col-md-12">Formato
									Impress�o:</label>
								<div class="col-md-10">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="formatoImpressaoPDF"
											name="tipoImpressao" class="custom-control-input"
											value="true" checked="checked">
										<label class="custom-control-label" for="formatoImpressaoPDF">PDF</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="formatoImpressaoXLS"
											name="tipoImpressao" class="custom-control-input"
											value="false">
										<label class="custom-control-label" for="formatoImpressaoXLS">XLS</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
					    <button type="button" class="btn btn-primary" onclick="gerarRelatorio();">Gerar</button>
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>

	</form:form>
</div>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/comunicacao/index.js"
        type="application/javascript"></script>