<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js" integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js" integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	var isPesquisaComunicacao = $("#isPesquisaComunicacao").val();
	
	if(!isPesquisaComunicacao) {
		localStorage.removeItem("condominiosSelecionados");
	}
	
    let condominiosSalvos = localStorage.getItem('condominiosSelecionados');
    
    if (condominiosSalvos) {
    	condominiosSalvos = JSON.parse(condominiosSalvos);

    	condominiosSalvos.forEach(function (item) {
            let opcao = new Option(item.text, item.id, true, true); 
            $('#condominio-select').append(opcao);
        });

        $('#condominio-select').trigger('change'); 
    }
    
	var logRelatorioComunicacao = "<c:out value="${sessionScope.relatorioComunicacaoLote}"/>";
	if(logRelatorioComunicacao != ""){
		submeter('comunicacaoForm', 'exibirRelatorioComunicacaoLote');
	}

	var datepicker = $.fn.datepicker.noConflict();
	$.fn.bootstrapDP = datepicker;  
	$('.bootstrapDP').bootstrapDP({
	    autoclose: true,
		format: 'dd/mm/yyyy',
		language: 'pt-BR'
	});

	$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});
	

	<c:if test="${listaPontoConsumo ne null}">
		iniciarDatatable('#pontoConsumo', {
			columnDefs: [
				{ orderable: false, targets: 0 }
			]
		});

 		$("#selecionarTodosPontos").click(function(){
	 		var tabela = $("#pontoConsumo").DataTable();

 			var linhas = tabela.cells().nodes();

 		    $(linhas).find(':checkbox').prop('checked', this.checked);
		});

		$("#pontoConsumo").on("page.dt", function(){
			$("#checkAllAuto").prop("checked", false); 
		});

		
	</c:if>

	<c:if test="${comunicacao.cityGate ne null}">
		carregarTroncos($("#cityGate"));
	</c:if>

	<c:if test="${comunicacao.cepComunicacao ne null || comunicacao.cepComunicacao ne ''}">
		carregarQuadras($("#cepComunicacao").val());
	</c:if>

	$('#pontoConsumo-select').select2({
		language: 'pt-BR',
		theme: 'bootstrap4',
		minimumInputLength: 4,
		delay: 3000,
		placeholder: "Digite o c�digo �nico ou descri��o do ponto de consumo.",
		ajax : {
			url : 'buscarPontosConsumo',
			dataType : 'json',
			contentType:"application/json; charset=utf-8",
			data : function (params) {
				return {
					q: params.term,
					servicoTipo: $("#servicoTipo").val(),
					page: params.page
				}
			}
		}
	}).live('change', function(){
		incluirPontoGrid(this.value)
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  var tab = $(e.target).attr("href")
		  if(tab == "#contentTabManual" && $("#servicoTipo").val() <= 0) {
			  $("#pontoConsumo-select").attr("disabled", "");
			  alert("Necess�rio selecionar o tipo de Servi�o para selecionar os pontos de consumo!");			  
		  }
	});

	$('#condominio-select').select2({
	    language: 'pt-BR',
	    theme: 'bootstrap4',
	    minimumInputLength: 4,
	    placeholder: "Digite o nome do condom�nio.",
	    width: 'auto',
	    dropdownAutoWidth: true,
	    ajax: {
	        url: 'buscarCondominio',
	        dataType: 'json',
	        contentType: "application/json; charset=utf-8",
	        delay: 300,
	        data: function (params) {
	            return {
	                q: params.term,
	                page: params.page
	            };
	        }
	    }
	});
	
	$('#condominio-select').on('change', function () {
	    let selecaoCondominios = $(this).select2('data').map(item => ({
	        id: item.id,
	        text: item.text
	    }));

	    localStorage.setItem('condominiosSelecionados', JSON.stringify(selecaoCondominios));
	});
	
});



function carregarTroncos(elem){

	var codCityGate = elem.value;
  	var selectTroncos = document.getElementById("tronco");

  	if(codCityGate == undefined) {
  		codCityGate = $("#cityGate").val();
  	}

  	selectTroncos.length=0;
	  	var novaOpcao = new Option("Selecione","-1");
	    selectTroncos.options[selectTroncos.length] = novaOpcao;

	    if (codCityGate != "-1") {
  		selectTroncos.disabled = false;      		
    	AjaxService.consultarTroncosPorCityGate( codCityGate, 
        	function(unidades) {            		      		         		
            	for (key in unidades){
                	var novaOpcao = new Option(unidades[key], key);
                    selectTroncos.options[selectTroncos.length] = novaOpcao;
                }
            	<c:if test="${comunicacao.tronco ne null}">
            		$('#tronco option[value='+${comunicacao.tronco.chavePrimaria}+']').attr('selected','selected');
            	</c:if>
            }
        );
  	} else {
		selectTroncos.disabled = true;      	
  	}
}

function carregarQuadras(valorCep) {

	var load = false;
  	var selectQuadras = document.getElementById("quadra");
	var cep = this.value;
  	if(cep == undefined){
		cep = valorCep.value;
		if(cep == undefined){
			cep = valorCep;
		}
	}
  	
  	selectQuadras.length=0;
  	var novaOpcao = new Option("Selecione","-1");
    selectQuadras.options[selectQuadras.length] = novaOpcao;
    selectQuadras.disabled = false;
  	$("#quadra").removeClass("campoDesabilitado");

    if((cep != undefined) && (trim(cep) != '') && (cep[8] != '_')){	
		AjaxService.obterTamanhoListaQuadraPorCep(cep, {
	       		 callback:function(quadras) {
	        		if(quadras == 1){
	        			selectQuadras.options[0].remove();
	        			load = true;
	        		}
	 			}, async:false}
			);
		AjaxService.consultarQuadrasPorCep( cep, {
                callback:function(quadras) {
                      for (key in quadras){
                            var novaOpcao = new Option(quadras[key], key);
                              selectQuadras.options[selectQuadras.length] = novaOpcao;
                          }

                  		<c:if test="${comunicacao.quadra ne null}">
            				$('#quadra option[value='+${comunicacao.quadra}+']').attr('selected','selected');
            			</c:if>

                      }, async:false}
                  );
   	} else {
   		$("#idQuadraImovel").addClass("campoDesabilitado");
   		selectQuadras.disabled = true;
   	}

	if(load){
		document.forms[0].idQuadraImovel.onchange();
	}
}


function pesquisar() {
	if(checarServicoTipoSelecionado()) {
		submeter('comunicacaoForm','pesquisarComunicacao');
	} else {
		alert("Obrigat�rio selecionar o tipo de servi�o");
	}
}



function limparFormulario(){		
	$('#cityGate option[value=-1]').attr('selected','selected');
	$('#segmento option[value=-1]').attr('selected','selected');
	$('#situacaoContrato option[value=-1]').attr('selected','selected');
	
	$('#numeroMedidor').val('');
	$('#chaveCep').val('');
	$('#cepComunicacao').val('');
	$('#servicoTipo option[value=-1]').attr('selected','selected');
	$('#dataAquisicaoInicial').val('');
	$('#dataAquisicaoFinal').val('');
	$('#dataInstalacaoInicial').val('');
	$('#dataInstalacaoFinal').val('');
	$('#dataInstalacaoFinal').val('');
	$('#segmentosSelecionados').val('');
	$('#medicaoTodos').prop('checked', true);
	
	carregarQuadras('');
	carregarTroncos($("#cityGate"));
}

function exibirGeracaoLote() {

	if(checarServicoTipoSelecionado()) {
		$("#tipoGeracaoLote").val($("a.active").attr('id'));

		if($("a.active").attr('id') == "tab-automatica"){
		
			var selecao = verificarSelecaoTodasPaginas();
			if (selecao == true) {
	
				$("input[type=hidden][class=chavesPrimariasDinamicas][name=chavesPrimarias]").remove();
				
	 			var tabela = $("#pontoConsumo").DataTable();
	
				var parametros = tabela.$('input:checked').serializeArray();
	
				var form = $("#comunicacaoForm");
	
			      $.each(parametros, function(){     
			          if($("#chk"+this.value).length == 0){
			             $(form).append(
			                $('<input>')
			                   .attr('type', 'hidden')
			                   .attr('class', 'chavesPrimariasDinamicas')
			                   .attr('name', 'chavesPrimarias')
			                   .val(this.value)
			             );
			          } 
			       }); 
			      
				var parametros = tabela.$('input:checked').serializeArray();

			      if(parametros.length <= 81) {
			    	  submeter('comunicacaoForm','exibirGerarComunicacaoLote');
			      } else {
						alert("Obrigat�rio inserir menos de 50 pontos de consumo!");
			      }
				
			}
		} else {
			if($("#table-ponto-inserido tr").length > 1) {
	 			var tabela = $("#table-ponto-inserido").DataTable();
				if(tabela.rows().count() <= 80) {
					submeter('comunicacaoForm','exibirGerarComunicacaoLote');
				} else {
					alert("Obrigat�rio inserir menos de 50 pontos de consumo!");
				}
			} else {
				alert("Obrigat�rio inserir pelo menos um ponto de consumo!");
			}
		}
	} else {
		alert("Obrigat�rio selecionar o tipo de servi�o");
	}
}

function checarServicoTipoSelecionado() {
	if ($("#servicoTipo").val() == -1) {
		return false;
	} else {
		return true;
	}
}

function verificarSelecaoTodasPaginas(){
		var tabela = $("#pontoConsumo").DataTable();

		var parametros = tabela.$('input:checked').serializeArray();

		if(parametros.length > 0) {
			return true;
		}

		alert("Selecione pelo menos um ponto de consumo para prosseguir com a gera��o");
		return false;
}


function incluirPontoGrid(idPontoConsumo) {

		$("#idPontoConsumo").val(idPontoConsumo);
		
		carregarFragmentoPOST('gridPontoConsumo', "incluirPontoGrid",
				document.forms["comunicacaoForm"], true);
		iniciarDatatable('#table-ponto-inserido');

		$('#pontoConsumo-select').empty();
}
	
function removerPontosGrid(indice, form) {
	document.forms["comunicacaoForm"].indexLista.value = indice;
	carregarFragmentoPOST('gridPontoConsumo', "removerPontosGrid",
			document.forms["comunicacaoForm"]);
	iniciarDatatable('#table-ponto-inserido');
}

function verificarSelecaoServicoTipo(){
	if ($("#servicoTipo").val() > 0){
		$("#pontoConsumo-select").removeAttr("disabled");
	} else {
		  $("#pontoConsumo-select").attr("disabled", "");
	}
}

function gerarPlanilhaExcel() {
		if(checarServicoTipoSelecionado()) {
		$("#tipoGeracaoLote").val($("a.active").attr('id'));

		if($("a.active").attr('id') == "tab-automatica"){
		
			var selecao = verificarSelecaoTodasPaginas();
			if (selecao == true) {
	
				$("input[type=hidden][class=chavesPrimariasDinamicas][name=chavesPrimarias]").remove();
				
	 			var tabela = $("#pontoConsumo").DataTable();
	
				var parametros = tabela.$('input:checked').serializeArray();
	
				var form = $("#comunicacaoForm");
	
			      $.each(parametros, function(){     
			          if($("#chk"+this.value).length == 0){
			             $(form).append(
			                $('<input>')
			                   .attr('type', 'hidden')
			                   .attr('class', 'chavesPrimariasDinamicas')
			                   .attr('name', 'chavesPrimarias')
			                   .val(this.value)
			             );
			          } 
			       }); 
	
				
				submeter('comunicacaoForm','gerarPlanilhaExcel');
			}
		} else {
			if($("#table-ponto-inserido tr").length > 1) {
				submeter('comunicacaoForm','gerarPlanilhaExcel');
			} else {
				alert("Obrigat�rio inserir pelo menos um ponto de consumo!");
			}
		}
	} else {
		alert("Obrigat�rio selecionar o tipo de servi�o");
	}

}

</script>


<div class="bootstrap">
	<form:form action="pesquisarComunicacao" id="comunicacaoForm" name="comunicacaoForm" method="post" modelAttribute="FiltroLoteComunicacaoDTO">
		<input type="hidden" id="idPontoConsumo" name="idPontoConsumo"/>
		<input type="hidden" id="indexLista" name="indexLista" />
		<input type="hidden" id="tipoGeracaoLote" name="tipoGeracaoLote" />	
		<input type="hidden" id="isPesquisaComunicacao" value="${isPesquisaComunicacao}">
			
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Controle de Comunica��o de Interrup��o</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para
					pesquisar os pontos de consumos, clique em <b>Pesquisar</b>
					para exibir os pontos de consumo e clique em <b>Gerar</b> para gerar a comunica��o em lote.
				</div>

				<hr>

				<div class="card">
					<div class="col-md-12">
						<div class="form-row">
							<div class="col-md-12">
								<label for="servicoTipo">Tipo de Servi�o:<span class="text-danger">*</span></label> <select
								onchange="verificarSelecaoServicoTipo();"
								name="servicoTipo" id="servicoTipo"
								class="form-control form-control-sm">
									<option value="-1">Selecione</option>
										<c:forEach items="${listaServicoTipo}"
										var="servicoTipo">
											<option
											value="<c:out value="${servicoTipo.chavePrimaria}"/>"
												<c:if test="${comunicacao.servicoTipo.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
												<c:out value="${servicoTipo.descricao}" />
											</option>
										</c:forEach>
									</select>
							</div>
						</div>
					</div>
								
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-automatica" data-toggle="tab"
                                   href="#contentTabAutomatica" role="tab" aria-controls="contentTabAutomatica"
                                   aria-selected="true"><i class="fa fa-info-circle"></i> Sele��o Autom�tica</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-manual" data-toggle="tab"
                                   href="#contentTabManual" role="tab" aria-controls="contentTabManual"
                                   aria-selected="false"><i class="fa fa-hotel"></i>Sele��o Manual</a>
                            </li>
                        </ul>
                    </div>				
					<div class="card-body bg-light">
					
                        <div class="tab-content" id="content-automatico">
                        <div class="tab-pane fade show active" id="contentTabAutomatica" role="tabpanel"
                        	aria-labelledby="contentTabAutomatica">
						<div class="row mb-2">
						
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="cityGate">City Gate:</label> <select
											name="cityGate" id="cityGate"
											class="form-control form-control-sm" onchange="carregarTroncos(this);">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaCityGate}"
												var="cityGate">
												<option
													value="<c:out value="${cityGate.chavePrimaria}"/>"
													<c:if test="${comunicacao.cityGate.chavePrimaria == cityGate.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${cityGate.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Tronco:</label> <select
											disabled="true" name="tronco" id="tronco"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listTronco}"
												var="tronco">
												<option
													value="<c:out value="${tronco.chavePrimaria}"/>"
													<c:if test="${comunicacao.tronco.chavePrimaria == tronco.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${tronco.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10"></br>
										<jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
										    <jsp:param name="cepObrigatorio" value="false"/>
										    <jsp:param name="idCampoCep" value="cepComunicacao"/>
										    <jsp:param name="numeroCep" value="${comunicacao.cepComunicacao}"/>
										    <jsp:param name="comunicacao" value="true"/>
										</jsp:include>
									</div>
								</div>
							</div>
							
							<div class="col-md-6" style="margin-top:20px;">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Quadra:</label> <select
											disabled="true" name="quadra" id="quadra"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaQuadra}"
												var="quadra">
												<option
													value="<c:out value="${quadra.chavePrimaria}"/>"
													<c:if test="${comunicacao.quadra.chavePrimaria == quadra.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${quadra.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>	
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Situa��o Ponto de Consumo:</label> <select
											name="situacaoConsumo" id="situacaoConsumo"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSituacaoPontoConsumo}"
												var="situacaoConsumo">
												<option
													value="<c:out value="${situacaoConsumo.chavePrimaria}"/>"
													<c:if test="${comunicacao.situacaoConsumo == situacaoConsumo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${situacaoConsumo.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="ramoAtividade">Ramo de Atividade:</label> <select
											name="ramoAtividade" id="ramoAtividade"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaRamoAtividade}"
												var="ramoAtividade">
												<option
													value="<c:out value="${ramoAtividade.chavePrimaria}"/>"
													<c:if test="${comunicacao.ramoAtividade.chavePrimaria == ramoAtividade.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${ramoAtividade.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>							
							
							<div class="col-md-6">
								<div class="form-row">
									<label for="habilitado" class="col-md-12">Tipo de Medi��o:</label>
									<div class="col-md-10">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="medicacaoIndividual" name="tipoMedicao" class="custom-control-input" value="true"
												   <c:if test="${comunicacao.tipoMedicao eq true}">checked="checked"</c:if>>
											<label class="custom-control-label" for="medicacaoIndividual">Individual</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="medicacaoColetiva" name="tipoMedicao" class="custom-control-input" value="false"
												   <c:if test="${comunicacao.tipoMedicao eq false}">checked="checked"</c:if>>
											<label class="custom-control-label" for="medicacaoColetiva">Coletivo</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="medicaoTodos" name="tipoMedicao" class="custom-control-input" value=""
												   <c:if test="${comunicacao.tipoMedicao eq null}">checked="checked"</c:if>>
											<label class="custom-control-label" for="medicaoTodos">Todos</label>
										</div>
									</div>
								</div>															
							</div>
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
	                                    <label>Per�odo de Aquisi��o do Medidor:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataAquisicaoInicial" name="dataAquisicaoInicial" maxlength="10" value="${comunicacao.dataAquisicaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataAquisicaoFinal" name="dataAquisicaoFinal" maxlength="10" value="${comunicacao.dataAquisicaoFinal}">
										</div>
									</div>
	                            </div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
	                                    <label>Per�odo de Instala��o do Medidor:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm campoData bootstrapDP" type="text" id="dataInstalacaoInicial" name="dataInstalacaoInicial" maxlength="10" value="${comunicacao.dataInstalacaoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm campoData bootstrapDP" type="text" id="dataInstalacaoFinal" name="dataInstalacaoFinal" maxlength="10" value="${comunicacao.dataInstalacaoFinal}">
										</div>
									</div>
	                            </div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">N�mero Medidor:</label> 
										<input class="form-control form-control-sm" type="text"
										id="numeroMedidor" name="numeroMedidor"
										value="${comunicacao.numeroMedidor}" maxlength="50"
										size="30"
										onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="modeloMedidor">Modelo do Medidor:</label> <select
											name="modeloMedidor" id="modeloMedidor"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaModeloMedidor}"
												var="modeloMedidor">
												<option
													value="<c:out value="${modeloMedidor.chavePrimaria}"/>"
													<c:if test="${comunicacao.modeloMedidor.chavePrimaria == modeloMedidor.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${modeloMedidor.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>															
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="situacaoMedidor">Situa��o do Medidor:</label> <select
											name="situacaoMedidor" id="situacaoMedidor"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSituacaoMedidor}"
												var="situacaoMedidor">
												<option
													value="<c:out value="${situacaoMedidor.chavePrimaria}"/>"
													<c:if test="${comunicacao.situacaoMedidor.chavePrimaria == situacaoMedidor.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${situacaoMedidor.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Segmento:</label> <select
											name="segmentosSelecionados" id="segmentosSelecionados"
											class="form-control form-control-sm" multiple="multiple">
											<c:forEach items="${listaSegmento}"
												var="segmento">
												<c:set var="selecionado" value="false"/>
												<c:forEach items="${comunicacao.segmentosSelecionados}" var="idSegmento">
													<c:if test="${idSegmento eq segmento.chavePrimaria}">
														<c:set var="selecionado" value="true" />
													</c:if>
												</c:forEach>												
												<option
													value="<c:out value="${segmento.chavePrimaria}"/>"
													<c:if test="${selecionado eq true}"> selected="selected"</c:if>>
													
													<c:out value="${segmento.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="situacaoContrato">Situa��o do Contrato:</label> <select
											name="situacaoContrato" id="situacaoContrato"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSituacaoContrato}"
												var="situacaoContrato">
												<option
													value="<c:out value="${situacaoContrato.chavePrimaria}"/>"
													<c:if test="${comunicacao.situacaoContrato == situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${situacaoContrato.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col align-self-start text-left">
										<label for="condominio-select">Condom�nio:</label>
										</br></br>
										<select name="condominios" id="condominio-select" multiple="multiple" class="form-control-sm form-control"></select>
									</div>
								</div>
							</div>
														
						</div>
						
						<br/>
						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoPesquisar"
										type="button" onclick="pesquisar();">
										<i class="fa fa-search"></i> Pesquisar
								</button>
								<button class="btn btn-secondary btn-sm" name="botaoLimpar"
									id="botaoLimpar" value="limparFormulario" type="button"
									onclick="limparFormulario();">
									<i class="far fa-trash-alt"></i> Limpar
								</button>
							</div>
						</div>						
						
				
				
				
				<c:if test="${listaPontoConsumo ne null}">
					<hr class="linhaSeparadora1" />
					<fieldset class="conteinerBloco">
						<div class="alert alert-primary" role="alert">
							<p class="orientacaoInicial">Selecione os pontos de consumo para gera��o em lote da comunica��o. </p>
						</div>
						<div class="table-responsive">
						
						<div
							class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
							<input id="selecionarTodosPontos" type="checkbox" name="selecionarTodosPontos"
							class="custom-control-input"> <label
							class="custom-control-label p-0" for="selecionarTodosPontos"> Selecionar todos os Pontos de Consumo</label>
						</div>
											</br></br>
							<table class="table table-bordered table-striped table-hover" id="pontoConsumo" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th>
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
												<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
													class="custom-control-input"> <label
													class="custom-control-label p-0" for="checkAllAuto"></label>
											</div>
										</th>
										<th scope="col" class="text-center">Ponto de Consumo</th>
										<th scope="col" class="text-center">C�digo do Ponto de Consumo</th>
										<th scope="col" class="text-center">Segmento</th>
										<th scope="col" class="text-center">Situa��o</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
										<tr>
											<td>
												<div
													class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
													data-identificador-check="chk${pontoConsumo.chavePrimaria}">
													<input id="chk${pontoConsumo.chavePrimaria}" type="checkbox"
														name="chavesPrimarias" class="custom-control-input"
														value="${pontoConsumo.chavePrimaria}"> <label
														class="custom-control-label p-0"
														for="chk${pontoConsumo.chavePrimaria}"></label>
												</div>
											</td>									
											<td class="text-center">
									        	<c:out value="${pontoConsumo.descricao} - ${pontoConsumo.imovel.nome}"/>
											</td>
											<td class="text-center">
									        	<c:out value="${pontoConsumo.codigoPontoConsumo}"/>
											</td>
											<td class="text-center">
									        	<c:out value="${pontoConsumo.segmento.descricao}"/>
											</td>
											<td class="text-center">
									        	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						
					</fieldset>
					
					<div class="row mt-3">
						<div class="col align-self-end text-right">
								<button class="btn btn-primary" id="botaoPlanilha"
									type="button" onclick="gerarPlanilhaExcel();">
										Exportar Para Planilha Excel
								</button>
																
								<button class="btn btn-primary" name="buttonGerar" id="buttonGerar"
										type="button" onclick="exibirGeracaoLote();">
										Gerar Comunica��o em Lote
								</button>
						</div>
					</div>					
				</c:if>
				</div>
				
                	<div class="tab-pane fade" id="contentTabManual" role="contentTabManual"
                    aria-labelledby="contentTabManual">
							<div class="form-row">
								<div class="col align-self-start text-left">
									<label for="pontoConsumo-select">Ponto de Consumo</label>
									</br></br>
									<select name="pontoConsumoId" id="pontoConsumo-select" class="form-control-sm form-control"></select>
								</div>
							</div>
							<div id="gridPontoConsumo">
								<jsp:include
									page="/jsp/atendimento/comunicacao/gridPontoConsumo.jsp"></jsp:include>
							</div>
							
							<div class="row mt-3">
								<div class="col align-self-end text-right">
										<button class="btn btn-primary" id="botaoPlanilhaManual"
												type="button" onclick="gerarPlanilhaExcel();">
												Exportar Para Planilha Excel
										</button>
																		
										<button class="btn btn-primary" name="buttonGerar" id="buttonGerarManual"
												type="button" onclick="exibirGeracaoLote();">
												Gerar Comunica��o em Lote
										</button>
								</div>
							</div>														
                    </div>
				
				
				</div>
				</div>
				

				
			</div>
		</div>
		</div>
	</form:form>
</div>

