<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Alterar Prioridade do Tipo de Servi�o<a href="<help:help>/localidadeinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form method="post" action="salvarAlteracaoServicoTipoPrioridade" id="servicoTipoPrioridadeForm">


<script>

function limparFormulario(){
	document.getElementById('descricao').value = "";
	document.getElementById('quantidadeHorasMaxima').value = "";
	document.forms['servicoTipoPrioridadeForm'].habilitado[0].checked = true;
}
	
function voltar() {
	location.href = '<c:url value="/exibirPesquisarServicoTipoPrioridade"/>';
}
		
function incluir() {         
    location.href = '<c:url value="/inserirServico"/>';
}
	
function alertInativacao() {
	var habilitado = document.getElementById("habilitado").checked;
	var habilitadoOriginal = document.getElementById("habilitadoOriginal").value;
	
	if(habilitado == false && habilitadoOriginal == "true"){
			if(confirm('Deseja inativar a Prioridade do Tipo de Servi�o?')){
				return true;  
			}else{
				return false;
			} 	
		}
	}
	

</script>


<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="localidadeCol1" class="coluna">
	

		<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${tipoPrioridade.chavePrimaria}"  />

		<label class="rotulo campoObrigatorio" id="rotuloDescricao" for="descricao" style="margin-left: 43px"	><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
		<input class="campoTexto" id="descricao" type="text" name="descricao" maxlength="40" size="40" value="${tipoPrioridade.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"></br>

		<label class="rotulo3Linhas campoObrigatorio" id="rotuloNomeEmpresa" for="nomeCompletoCliente" ><span class="campoObrigatorioSimbolo">* </span>Quantidade m�xima</br>de horas para a</br>execu��o do servi�o:</label>
		<input class="campoTexto" type="text" name="quantidadeHorasMaxima" id="quantidadeHorasMaxima" onkeypress="return formatarCampoInteiro(event,9)" value="${tipoPrioridade.quantidadeHorasMaxima}" maxlength="9" size="9" style="margin-top: 20px" /><br />
		
		
	</fieldset>
	
	<fieldset id="localidadeCol2" class="colunaFinal">
	<label class="rotulo" for="indicadorMulta">Dias:</label>
	<input class="campoRadio" type="radio" name="indicadorDiaCorridoUtil" id="indicadorCorrido" value="corrido" <c:if test="${dia eq 'corrido'}">checked</c:if>><label class="rotuloRadio" for="indicadorCorrido">Corrido</label>
	<input class="campoRadio" type="radio" name="indicadorDiaCorridoUtil" id="indicadorUtil" value="util" <c:if test="${dia eq 'util'}">checked</c:if>><label class="rotuloRadio" for="indicadorUtil">�til</label>
	</fieldset>
	
	<fieldset id="localidadeCol2" class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${tipoPrioridade.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${tipoPrioridade.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input type="hidden" id="habilitadoOriginal" value="${tipoPrioridade.habilitado}" >
		</fieldset>
	
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="voltar();">
 	<input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
 	<vacess:vacess param="alterarServicoTipoPrioridade">
		<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="submit" onclick="return alertInativacao()">
	</vacess:vacess>
</fieldset>

</form>   