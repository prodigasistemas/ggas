<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script>
	
function voltar(){
	location.href = '<c:url value="/exibirPesquisarServicoTipoPrioridade"/>';
}

function alterar(){
	submeter('servicoTipoPrioridadeForm','exibirAlteracaoServicoTipoPrioridade');
}
	
</script>

<h1 class="tituloInterno">Detalhar Prioridade do Tipo de Servi�o<a href="<help:help>/cadastrodalocalidadedetalhamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
<form method="post" action="exibirAlteracao"  id="servicoTipoPrioridadeForm">


<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${tipoPrioridade.chavePrimaria}">
<input name="descricao" type="hidden" id="descricao" value="${tipoPrioridade.descricao}">

<fieldset class="detalhamento">
	<fieldset >
		<label class="rotulo" for="descricao">Descri��o da Prioridade:</label>
		<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${tipoPrioridade.descricao}"/></span><br />	
		<label class="rotulo" style="margin-left: 19px">Quantidade m�xima</br>de horas para a</br>execu��o do servi�o:</label>
		<span class="itemDetalhamento" style="margin-top: 20px"><c:out value="${tipoPrioridade.quantidadeHorasMaxima}"/>h</span><br />	
		<label class="rotulo" style="margin-left: 110px">Dias:</label>
		<span class="itemDetalhamento"><c:out value="${dias}"/></span><br />	
		<label class="rotulo" style="margin-left: 39px">Indicador de Uso:</label>
		<span class="itemDetalhamento"><c:out value="${indicador}"/></span><br />
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
    <vacess:vacess param="exibirAlteracaoServicoTipoPrioridade">    
    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
    </vacess:vacess>
</fieldset>
</form>
