<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Pesquisar Prioridade do Tipo de Servi�o<a class="linkHelp" href="<help:help>/consultadosmedidores.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">



function limparFormulario(){
	document.getElementById('descricao').value = "";
	document.getElementById('quantidadeHorasMaxima').value = "";
	document.forms['servicoTipoPrioridadeForm'].habilitado[0].checked = true;
}


function detalharServicoTipoPrioridade(chave){
	document.forms["servicoTipoPrioridadeForm"].chavePrimaria.value = chave;
	submeter('servicoTipoPrioridadeForm','exibirDetalhamentoServicoTipoPrioridade');

}
	
function removerServicoTipoPrioridade(){
	
	var selecao = verificarSelecao();	
	
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('servicoTipoPrioridadeForm', 'removerServicoTipoPrioridade');
		}
    }
}

function incluir() {         
    location.href = '<c:url value="/exibirInclusaoServicoTipoPrioridade"/>';
}

function alterar() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms["servicoTipoPrioridadeForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('servicoTipoPrioridadeForm','exibirAlteracaoServicoTipoPrioridade');
    }
	
}


</script>


<form method="post" action="pesquisarServicoTipoPrioridade" id="servicoTipoPrioridadeForm" name="servicoTipoPrioridadeForm"> 
	
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
				<fieldset  class="coluna">
		<fieldset id="conteinerTipoMensagemVigencia">
			<label class="rotulo" id="rotuloNomeEmpresa" for="nomeCompletoCliente" >Descricao:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="40" size="40" value="${servicotipoprioridade.descricao}" onkeyup="letraMaiuscula(this);"/><br />
			<label class="rotulo rotulo2Linhas">Quantidade m�xima</br>de horas para a</br>execu��o do servi�o:</label>
		    <input class="campoTexto" type="text" style="margin-top: 20px" name="quantidadeHorasMaxima" id="quantidadeHorasMaxima" maxlength="9" size="9" value="${servicotipoprioridade.quantidadeHorasMaxima}" onblur="aplicarMascaraNumeroInteiro(this);" onkeypress="return formatarCampoInteiro(event,17);"/><br />
		</fieldset>
						
		</fieldset>
		
		<fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true' }">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${habilitado eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarServicoTipoPrioridade">		
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaTipoPrioridade ne null}">
	<hr class="linhaSeparadoraPesquisa" />
	<display:table class="dataTableGGAS" name="listaTipoPrioridade" sort="list" id="servicoTipo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarServicoTipoPrioridade">
		<display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
      		<input type="checkbox" name="chavesPrimarias" value="${servicoTipo.chavePrimaria}">
     	</display:column>
     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${servicoTipo.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
        <display:column sortable="true" sortProperty="descricao" title="Descri��o" style="auto"  maxLength="40">
            <a href="javascript:detalharServicoTipoPrioridade(<c:out value='${servicoTipo.chavePrimaria}'/>);"><span class="linkInvisivel"></span><c:out value="${servicoTipo.descricao}"/></a>
        </display:column>
		<display:column sortable="true" sortProperty="quantidadeHorasMaxima" title="Horas" style="auto">
			<a href="javascript:detalharServicoTipoPrioridade(<c:out value='${servicoTipo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${servicoTipo.quantidadeHorasMaxima}"/>
			</a>
		</display:column>
        <display:column sortable="true" sortProperty="indicadorDiaCorridoUtil.descricao" title="Dias" style="width: auto">
        	<a href="javascript:detalharServicoTipoPrioridade(<c:out value='${servicoTipo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${servicoTipo.indicadorDiaCorridoUtil.descricao}"/>
			</a>
        </display:column>
    </display:table>    
</c:if>
	
</form> 

	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaTipoPrioridade}">
  			<vacess:vacess param="exibirAlteracaoServicoTipoPrioridade">
  				<input id="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterar()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerServicoTipoPrioridade">
				<input id="buttonRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerServicoTipoPrioridade()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoServicoTipoPrioridade">
   			<input id="buttonIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>
	
	
		