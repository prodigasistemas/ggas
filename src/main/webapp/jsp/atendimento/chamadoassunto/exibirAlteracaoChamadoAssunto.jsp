<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>

</script>


<div class="bootstrap">

    <form:form method="post" modelAttribute="chamadoAssuntoForm" action="alterarChamadoAssunto" id="chamadoAssuntoForm" name="chamadoAssuntoForm">

		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${chamadoTipo.chavePrimaria}" > 
		<input type="hidden" name="indexList" id="indexList" >

        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Alterar Tipo de Chamado com Assunto</h5>
            </div>
            <div class="card-body">
                <div class="alert alert-primary" role="alert">
                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                    <div class="d-inline">
                        Altere os dados e clique em <strong>Salvar</strong> para alterar o Tipo de Chamado com Assunto. Para cancelar clique em <strong>Cancelar</strong>.<br />
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-md-12">
                            <label for="descricao" >Descri��o:<span class="text-danger">*</span></label>
                            <input class="form-control form-control-sm" type="text" name="descricao" id="descricao"
                                   maxlength="40" size="40" value="${chamadoTipo.descricao}"
                                   onkeyup="this.value=removerEspacoInicio(this.value),letraMaiuscula(this);"
                                   onkeypress="return campoSemHifen(event)"/>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12">
                            <label for="idSegmentoChamadoTipo">Segmento</label>
                            <select id="idSegmentoChamadoTipo" class="form-control form-control-sm" name="idSegmentoChamadoTipo">
                                <option value="-1">Selecione</option>
                                <c:forEach items="${listaSegmento}" var="segmento">
                                    <option value="<c:out value="${segmento.chavePrimaria}"/>"
                                            <c:if test="${chamadoTipo.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
                                        <c:out value="${segmento.descricao}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12">
                            <label for="categoriaChamado">Categoria</label>
                            <select id="categoriaChamado" class="form-control form-control-sm" name="categoriaChamado">
                                <option value="-1">Selecione</option>
                                <c:forEach items="${listaCategoria}" var="categoria">
                                    <option value="<c:out value="${categoria.name}"/>" <c:if test="${chamadoTipo.categoria.name == categoria.name}">selected="selected"</c:if>>
                                        <c:out value="${categoria.descricao}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Indicador de Uso:</label>
                                </div>
                                <div class="col-md-12">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input" type="radio" name="habilitado" id="habilitadoSim" value="true"
                                        <c:if test="${chamadoTipo.habilitado eq 'true'}"> checked</c:if>>
                                        <label class="custom-control-label" for="habilitadoSim">Ativo</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input" type="radio" name="habilitado" id="habilitadoNao" value="false"
                                               <c:if test="${chamadoTipo.habilitado eq 'false'}">checked</c:if>>
                                        <label class="custom-control-label" for="habilitadoNao">Inativo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Assunto</h5>
                        <ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-caracteristicas" data-toggle="tab"
                                   href="#contentTabCaracteristicas" role="tab" aria-controls="contentTabCaracteristicas"
                                   aria-selected="true"><i class="fa fa-info-circle"></i> Caracter�sticas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-campo-obrigatorio" data-toggle="tab"
                                   href="#contentTabCampoObrigatorio" role="tab" aria-controls="contentTabCampoObrigatorio"
                                   aria-selected="false"><i class="fa fa-font"></i> Obrigatoriedade dos campos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-envio-email" data-toggle="tab"
                                   href="#contentTabEnvioEmail" role="tab" aria-controls="contentTabEnvioEmail"
                                   aria-selected="false"><i class="fa fa-at"></i> Opera��o de Envio de E-mail</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-servicos" data-toggle="tab"
                                   href="#contentTabServicos" role="tab" aria-controls="contentTabServicos"
                                   aria-selected="false"><i class="fa fa-hammer"></i> Servi�os</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="content-alterar-chamado-assunto">
                            <div class="tab-pane fade show active" id="contentTabCaracteristicas" role="tabpanel"
                                 aria-labelledby="contentTabCaracteristicas">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="col-xl-8 col-lg-12 col-md-12">
                                                <label for="descricaoAssunto">Descri��o do Assunto: <span class="text-danger">*</span></label>
                                                <input type="text" id="descricaoAssunto" name="descricaoAssunto" class="form-control form-control-sm"
                                                       value="${assunto.descricao}" maxlength="40" size="26"
                                                       onkeyup="this.value=removerEspacoInicio(this.value),letraMaiuscula(this);"
                                                       onkeypress="return campoSemHifen(event)"/>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <label for="descricaoAssunto">Prazo previsto para atendimento em horas: <span class="text-danger">*</span></label>
                                                <input type="number" id="quantidadeHorasPrevistaAtendimento" name="quantidadeHorasPrevistaAtendimento" class="form-control form-control-sm"
                                                       value="${chamadoAssunto.quantidadeHorasPrevistaAtendimento}" maxlength="9" size="9"
                                                       onkeypress="return formatarCampoInteiro(event)"/>
                                            </div>
                                            <div class="col-xl-8 col-lg-12 col-md-12">
                                                <label for="nomeRelatorioRegulatorio">Nome do Relat�rio Regulat�rio: </label>
                                                <input type="text" id="nomeRelatorioRegulatorio" name="nomeRelatorioRegulatorio" class="form-control form-control-sm"
                                                       value="${assunto.nomeRelatorioRegulatorio}" maxlength="64" size="64"
                                                       onkeyup="this.value=removerEspacoInicio(this.value),letraMaiuscula(this);"
                                                       onkeypress="return campoSemHifen(event)"/>
                                            </div>                                              
                                            <div class="col-md-12">
                                            <label for="descricaoAssunto">Possui Indicador Regulat�rio? </label> 
                                            </div>                                            
                                            <div class="col-md-12">
                                                	<div class="custom-control custom-radio custom-control-inline">
                                                    	<input class="custom-control-input" onclick="carregarIndicadorRegulario()" type="radio" name="possuiIndicadorRegulatorio" id="possuiIndicadorRegulatorio1"
                                                                   value="true" <c:if test="${assunto.possuiIndicadorRegulario eq 'true'}">checked</c:if>>
                                                        <label class="custom-control-label" for="possuiIndicadorRegulatorio1">Sim</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input class="custom-control-input" onclick="carregarIndicadorRegulario()" type="radio" name="possuiIndicadorRegulatorio" id="possuiIndicadorRegulatorio2"
                                                                   value="false" <c:if test="${assunto.possuiIndicadorRegulario eq 'false'}">checked</c:if>>
                                                        <label class="custom-control-label" for="possuiIndicadorRegulatorio2">N�o</label>
                                                    </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12" id="divIndicadorRegulatorio">
                                                <label for="indicadorRegulatorioValores">Indicador Regulat�rio: </label>                                            
				                                <select class="form-control form-control-sm"  name="indicadorRegulatorioValores" id="indicadorRegulatorioValores" >
				                                    <option value="">Selecione</option>
				                                        <option value="<c:out value="${indicadorRegulatorioValores.chavePrimaria}"/>">
				                                            <c:out value="${indicadorRegulatorio.descricao}"/>
				                                       </option>
				                                </select>
			                                </div>                                             
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Gerar Autoriza��o de Servi�o Autom�tica?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorGeraServicoAutorizacao" id="indicadorGeraServicoAutorizacao1"
                                                                   value="true" <c:if test="${assunto.indicadorGeraServicoAutorizacao eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorGeraServicoAutorizacao1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorGeraServicoAutorizacao" id="indicadorGeraServicoAutorizacao2"
                                                                   value="false" <c:if test="${assunto.indicadorGeraServicoAutorizacao eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorGeraServicoAutorizacao2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Fechamento Autom�tico?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorFechamentoAutomatico" id="indicadorFechamentoAutomatico1" value="true"
                                                            <c:if test="${assunto.indicadorFechamentoAutomatico eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorFechamentoAutomatico1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorFechamentoAutomatico" id="indicadorFechamentoAutomatico2" value="false"
                                                                   <c:if test="${assunto.indicadorFechamentoAutomatico eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorFechamentoAutomatico2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Permite Tramita��o?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorTramitacao" id="indicadorTramitacao1" value="true"
                                                                   <c:if test="${assunto.indicadorTramitacao eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorTramitacao1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorTramitacao" id="indicadorTramitacao2" value="false"
                                                                   <c:if test="${assunto.indicadorTramitacao eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorTramitacao2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Verifica Restri��o de Servi�o?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorVerificaRestServ" id="indicadorVerificaRestServ1" value="true"
                                                            <c:if test="${assunto.indicadorVerificaRestServ eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorVerificaRestServ1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorVerificaRestServ" id="indicadorVerificaRestServ2" value="false"
                                                                   <c:if test="${assunto.indicadorVerificaRestServ eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorVerificaRestServ2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Encerrar automaticamente ao encerrar A.S relacionada(s)?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorEncerraAutoASRel" id="indicadorEncerraAutoASRel1" value="true"
                                                            <c:if test="${assunto.indicadorEncerraAutoASRel eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorEncerraAutoASRel1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorEncerraAutoASRel" id="indicadorEncerraAutoASRel2" value="false"
                                                                   <c:if test="${assunto.indicadorEncerraAutoASRel eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorEncerraAutoASRel2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Exige confirmar suspeita de vazamento?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorVerificarVazamento" id="indicadorVerificarVazamento1" value="true"
                                                            <c:if test="${assunto.indicadorVerificarVazamento eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorVerificarVazamento1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorVerificarVazamento" id="indicadorVerificarVazamento2" value="false"
                                                                   <c:if test="${assunto.indicadorVerificarVazamento eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorVerificarVazamento2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Informa��o sobre acionamento de gasista?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorAcionamentoGasista" id="indicadorAcionamentoGasistaSim" value="true"
                                                            <c:if test="${assunto.indicadorAcionamentoGasista eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorAcionamentoGasistaSim">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorAcionamentoGasista" id="indicadorAcionamentoGasistaNao" value="false"
                                                                   <c:if test="${assunto.indicadorAcionamentoGasista eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorAcionamentoGasistaNao">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Permite abrir chamado em lote?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input onclick="bloquearOperacoes()" class="custom-control-input" type="radio" name="permiteAbrirEmLote" id="permiteAbrirEmLoteSim" value="true"
                                                            <c:if test="${assunto.permiteAbrirEmLote eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="permiteAbrirEmLoteSim">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input onclick="desbloquearOperacoes()" class="custom-control-input" type="radio" name="permiteAbrirEmLote" id="permiteAbrirEmLoteNao" value="false"
                                                                   <c:if test="${assunto.permiteAbrirEmLote eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="permiteAbrirEmLoteNao">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Assunto com garantia?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorComGarantia" id="indicadorComGarantiaSim" value="true"
                                                            <c:if test="${assunto.indicadorComGarantia eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorComGarantiaSim">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorComGarantia" id="indicadorComGarantiaNao" value="false"
                                                                   <c:if test="${assunto.indicadorComGarantia eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorComGarantiaNao">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Outras Unidades podem visualizar esse chamado?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorUnidadesOrganizacionalVisualizadora"
                                                                   id="indicadorUnidadesOrganizacionalVisualizadoraSim" value="true" onclick="mudarEstadoUnidadesVisualizadoras(this)"
                                                            <c:if test="${assunto.indicadorUnidadesOrganizacionalVisualizadora eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorUnidadesOrganizacionalVisualizadoraSim">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorUnidadesOrganizacionalVisualizadora"
                                                                   id="indicadorUnidadesOrganizacionalVisualizadoraNao" value="false" onclick="mudarEstadoUnidadesVisualizadoras(this)"
                                                                   <c:if test="${assunto.indicadorUnidadesOrganizacionalVisualizadora eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorUnidadesOrganizacionalVisualizadoraNao">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Prazo Diferenciado?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorPrazoDiferenciado" id="indicadorPrazoDiferenciadoSim"
                                                                   value="true"  <c:if test="${assunto.indicadorPrazoDiferenciado eq 'true'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorPrazoDiferenciadoSim">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorPrazoDiferenciado" id="indicadorPrazoDiferenciadoNao"
                                                                   value="false"  <c:if test="${assunto.indicadorPrazoDiferenciado eq 'false'}"> checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorPrazoDiferenciadoNao">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Dias:</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorDiaCorridoUtil" id="indicadorDiaCorridoUtil1"
                                                                   value="corrido" <c:if test="${indicadorDiaCorridoUtil eq 'corrido'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorDiaCorridoUtil1">Corrido</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorDiaCorridoUtil" id="indicadorDiaCorridoUtil2"
                                                                   value="util" <c:if test="${indicadorDiaCorridoUtil eq 'util'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorDiaCorridoUtil2">�til</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Gera Cr�dito?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorCredito" id="indicadorCredito1"
                                                                   value="true" onclick="habilitarCredito();"<c:if test="${assunto.indicadorCredito eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorCredito1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorCredito" id="indicadorCredito2"
                                                                   value="false" onclick="desabilitarCredito();"<c:if test="${assunto.indicadorCredito eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorCredito2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Gera d�bito?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorDebito" id="indicadorDebito1"
                                                                   value="true" onclick="habilitarDebito();"<c:if test="${assunto.indicadorDebito eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorDebito1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorDebito" id="indicadorDebito2"
                                                                   value="false" onclick="desabilitarDebito();"<c:if test="${assunto.indicadorDebito eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorDebito2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Indicador de Uso:</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="habilitadoAssunto" id="indicadorHabilitadoTrue" value="true"
                                                                   <c:if test="${assunto.habilitado eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorHabilitadoTrue">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="habilitadoAssunto" id="indicadorHabilitadoFalse" value="false"
                                                                   <c:if test="${assunto.habilitado eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorHabilitadoFalse">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-8">
                                                <div class="form-row">
                                                    <div class="col-xl-6 col-lg-6 col-md-12">
                                                    <label for="rubricaCredito">Rubrica Cr�dito: <span id="spanRubricaCredito" style="display: none" class="text-danger">*</span></label>
                                                    <select class="form-control form-control-sm" name="rubricaCredito" id="rubricaCredito">
                                                        <option value="-1">Selecione</option>
                                                        <c:forEach items="${listaRubricaCredito}" var="credito">
                                                            <option value="<c:out value="${credito.chavePrimaria}"/>" <c:if test="${assunto.rubricaCredito.chavePrimaria == credito.chavePrimaria}">selected="selected"</c:if>>
                                                                <c:out value="${credito.descricao}"/>
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                    <div class="col-xl-6 col-lg-6 col-md-12">
                                                    <label for="rubricaDebito">Rubrica D�bito: <span id="spanRubricaDebito" style="display: none" class="text-danger">*</span></label>
                                                    <select class="form-control form-control-sm" name="rubricaDebito" id="rubricaDebito">
                                                        <option value="-1">Selecione</option>
                                                        <c:forEach items="${listaRubricaDebito}" var="debito">
                                                            <option value="<c:out value="${debito.chavePrimaria}"/>" <c:if test="${assunto.rubricaDebito.chavePrimaria == debito.chavePrimaria}">selected="selected"</c:if>>
                                                                <c:out value="${debito.descricao}"/>
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="col-xl-6 col-lg-6 col-md-12">
                                                    <label for="canalAtendimento">Canal Atendimento:</label>
                                                    <select class="form-control form-control-sm" name="canalAtendimento" id="canalAtendimento">
                                                        <option value="-1">Selecione</option>
                                                        <c:forEach items="${listaCanalAtendimento}" var="canal">
                                                            <option value="<c:out value="${canal.chavePrimaria}"/>" <c:if test="${assunto.canalAtendimento.chavePrimaria == canal.chavePrimaria}">selected="selected"</c:if>>
                                                                <c:out value="${canal.descricao}"/>
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                    <div class="col-xl-6 col-lg-6 col-md-12">
                                                    <label for="unidadeOrganizacional">Unidade Organizacional: <span class="text-danger">*</span></label>
                                                    <select class="form-control form-control-sm" name="unidadeOrganizacional" id="unidadeOrganizacional" onclick="carregarResponsavel(this.value);">
                                                        <option value="-1">Selecione</option>
                                                        <c:forEach items="${listaUnidadeOrganizacional}" var="unidade">
                                                            <option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${assunto.unidadeOrganizacional.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
                                                                <c:out value="${unidade.descricao}"/>
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="col-xl-6 col-lg-6 col-md-12">
                                                    <label for="questionario">Question�rio Satisfa��o:</label>
                                                    <select class="form-control form-control-sm" name="questionario" id="questionario">
                                                        <option value="-1">Selecione</option>
                                                        <c:forEach items="${listaQuestionario}" var="questionario">
                                                            <option value="<c:out value="${questionario.chavePrimaria}"/>" <c:if test="${assunto.questionario != null && assunto.questionario.chavePrimaria == questionario.chavePrimaria}">selected="selected"</c:if>>
                                                                <c:out value="${questionario.nomeQuestionario}"/>
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                    <div class="col-xl-6 col-lg-6 col-md-12">
                                                        <label>Respons�vel:</label>
                                                        <div id="divResponsavelAssunto">
                                                            <jsp:include page="/jsp/atendimento/chamadoassunto/selectResponsavelAssunto.jsp"></jsp:include>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divUnidadeOrganizacionalVisualizadoras" class="col-xl-4 col-lg-6 col-md-12">
                                                <label for="unidadeOrganizacional">Unidades que podem visualizar:</label>
                                                <select class="form-control form-control-sm" style="height: 83%;"
                                                        name="unidadeOrganizacionalVisualizadoras" id="unidadeOrganizacionalVisualizadoras" multiple="multiple">
                                                    <option value="-1">Selecione</option>
                                                    <c:forEach items="${listaUnidadeOrganizacional}" var="unidadeOrganizacional">
                                                        <option title="${unidadeOrganizacional.descricao}" value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>"
                                                                <c:forEach items="${assunto.listaUnidadeOrganizacionalVisualizadora}" var="unidadeVisualizadora">
                                                                    <c:if test="${unidadeVisualizadora.unidadeOrganizacional.chavePrimaria == unidadeOrganizacional.chavePrimaria}">
                                                                        selected="selected"
                                                                    </c:if>
                                                                </c:forEach>>
                                                            <c:out value="${unidadeOrganizacional.descricao}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabCampoObrigatorio" role="tabpanel"
                                 aria-labelledby="contentTabCampoObrigatorio">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Cliente � Obrigat�rio?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorClienteObrigatorio" id="indicadorClienteObrigatorio1" value="true"
                                                                   onclick="obterListaServicos();" <c:if test="${assunto.indicadorClienteObrigatorio eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorClienteObrigatorio1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorClienteObrigatorio" id="indicadorClienteObrigatorio2" value="false" onclick="obterListaServicos();"
                                                                   <c:if test="${assunto.indicadorClienteObrigatorio eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorClienteObrigatorio2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Im�vel � Obrigat�rio?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorImovelObrigatorio" id="indicadorImovelObrigatorio1" value="true"
                                                                   onclick="obterListaServicos();" <c:if test="${assunto.indicadorImovelObrigatorio eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorImovelObrigatorio1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorImovelObrigatorio" id="indicadorImovelObrigatorio2" value="false"  onclick="obterListaServicos();"
                                                                   <c:if test="${assunto.indicadorImovelObrigatorio eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorImovelObrigatorio2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>RG do solicitante � Obrigat�rio?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorRgSolicitanteObrigatorio" id="indicadorRgSolicitanteObrigatorio1" value="true"
                                                                   <c:if test="${assunto.indicadorRgSolicitanteObrigatorio eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorRgSolicitanteObrigatorio1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorRgSolicitanteObrigatorio" id="indicadorRgSolicitanteObrigatorio2" value="false"
                                                                   <c:if test="${assunto.indicadorRgSolicitanteObrigatorio eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorRgSolicitanteObrigatorio2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>CPF/CNPJ do solicitante � Obrigat�rio?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorCpfCnpjSolicitanteObrigatorio" id="indicadorCpfCnpjSolicitanteObrigatorio1" value="true"
                                                                   <c:if test="${assunto.indicadorCpfCnpjSolicitanteObrigatorio eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorCpfCnpjSolicitanteObrigatorio1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorCpfCnpjSolicitanteObrigatorio" id="indicadorCpfCnpjSolicitanteObrigatorio2" value="false"
                                                                   <c:if test="${assunto.indicadorCpfCnpjSolicitanteObrigatorio eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorCpfCnpjSolicitanteObrigatorio2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Nome do Solicitante Obrigat�rio?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorNomeSolicitanteObrigatorio" id="indicadorNomeSolicitanteObrigatorio1" value="true"
                                                                   <c:if test="${assunto.indicadorNomeSolicitanteObrigatorio eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorNomeSolicitanteObrigatorio1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorNomeSolicitanteObrigatorio" id="indicadorNomeSolicitanteObrigatorio2" value="false"
                                                                   <c:if test="${assunto.indicadorNomeSolicitanteObrigatorio eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorNomeSolicitanteObrigatorio2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Telefone do Solicitante Obrigat�rio?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorTelefoneSolicitanteObrigatorio" id="indicadorTelefoneSolicitanteObrigatorio1" value="true"
                                                                   <c:if test="${assunto.indicadorTelefoneSolicitanteObrigatorio eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorTelefoneSolicitanteObrigatorio1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorTelefoneSolicitanteObrigatorio" id="indicadorTelefoneSolicitanteObrigatorio2" value="false"
                                                                   <c:if test="${assunto.indicadorTelefoneSolicitanteObrigatorio eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorTelefoneSolicitanteObrigatorio2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-xl-4 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Email do solicitante � Obrigat�rio?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorEmailSolicitanteObrigatorio" id="indicadorEmailSolicitanteObrigatorio1" value="true"
                                                                   <c:if test="${assunto.indicadorEmailSolicitanteObrigatorio eq 'true'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorEmailSolicitanteObrigatorio1">Sim</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input" type="radio" name="indicadorEmailSolicitanteObrigatorio" id="indicadorEmailSolicitanteObrigatorio2" value="false"
                                                                   <c:if test="${assunto.indicadorEmailSolicitanteObrigatorio eq 'false'}">checked</c:if>>
                                                            <label class="custom-control-label" for="indicadorEmailSolicitanteObrigatorio2">N�o</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabEnvioEmail" role="tabpanel" aria-labelledby="contentTabEnvioEmail">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="col-xl-3 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Enviar Email ao cadastrar Chamado?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <c:forEach items="${listaOperacaoEmail}" var="operacao">
                                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                                <input class="custom-control-input" type="checkbox" name="enviarEmailCadastro" id="enviarEmailCadastro${operacao}"
                                                                       value="${operacao}" <c:if test="${assunto.enviarEmailCadastro.contains(operacao)}">checked=""</c:if> />
                                                                <label class="custom-control-label p-0" for="enviarEmailCadastro${operacao}">&nbsp;<fmt:message key="${operacao.chaveMensagem}"/></label>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Enviar Email ao tramitar Chamado?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <c:forEach items="${listaOperacaoEmail}" var="operacao">
                                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                                <input class="custom-control-input" type="checkbox" name="enviarEmailTramitar" id="enviarEmailTramitar${operacao}"
                                                                       value="${operacao}" <c:if test="${assunto.enviarEmailTramitar.contains(operacao)}">checked=""</c:if> />
                                                                <label class="custom-control-label p-0" for="enviarEmailTramitar${operacao}">&nbsp;<fmt:message key="${operacao.chaveMensagem}"/></label>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Enviar Email ao reiterar Chamado?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <c:forEach items="${listaOperacaoEmail}" var="operacao">
                                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                                <input class="custom-control-input" type="checkbox" name="enviarEmailReiterar" id="enviarEmailReiterar${operacao}"
                                                                       value="${operacao}" <c:if test="${assunto.enviarEmailReiterar.contains(operacao)}">checked=""</c:if> />
                                                                <label class="custom-control-label p-0" for="enviarEmailReiterar${operacao}">&nbsp;<fmt:message key="${operacao.chaveMensagem}"/></label>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Enviar Email ao encerrar Chamado?</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <c:forEach items="${listaOperacaoEmail}" var="operacao">
                                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                                <input class="custom-control-input" type="checkbox" name="enviarEmailEncerrar" id="enviarEmailEncerrar${operacao}"
                                                                       value="${operacao}" <c:if test="${assunto.enviarEmailEncerrar.contains(operacao)}">checked=""</c:if> />
                                                                <label class="custom-control-label p-0" for="enviarEmailEncerrar${operacao}">&nbsp;<fmt:message key="${operacao.chaveMensagem}"/></label>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-6 col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Prioridade no envio do protocolo com e-mails diferentes</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <c:forEach items="${listaOperacaoEmailPrioridade}" var="operacao">
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input class="custom-control-input" type="radio" name="enviarEmailPrioridadeProtocolo" id="enviarEmailPrioridadeProtocolo${operacao}"
                                                                       value="${operacao}" <c:if test="${assunto.enviarEmailPrioridadeProtocolo.contains(operacao)}">checked=""</c:if> />
                                                                <label class="custom-control-label p-0" for="enviarEmailPrioridadeProtocolo${operacao}">&nbsp;<fmt:message key="${operacao.chaveMensagem}"/></label>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label for="orientacaoAssunto">Observa��o/Orienta��o: </label>
                                                <textarea class="form-control form-control-sm" id="orientacaoAssunto" name="orientacaoAssunto" cols="40" rows="7" maxlength="800"
                                                          onblur="this.value = removerEspacoInicialFinal(this.value);" onkeypress="return formatarCampoTextoLivreComLimite(event,this,800);"
                                                          onpaste="return formatarCampoTextoLivreComLimite(event,this,800);">${assunto.orientacao}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabServicos" role="tabpanel" aria-labelledby="contentTabServicos">
                                <div id="gridComponentesServicoTipo">
                                    <jsp:include page="/jsp/atendimento/chamadoassunto/gridComponentesServicoTipoBootstrap.jsp"></jsp:include>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-row mt-2">
                            <div class="col-md-12">
                                <div class="row justify-content-between">
                                    <div class="col-md-12 col-sm-12 mt-1 text-md-right">
                                        <button class="btn btn-primary btn-sm mb-1 mr-1" id="botaoIncluirAssunto" name="botaoIncluirAssunto"
                                                value="Adicionar" type="button" onclick="carregarAssunto('incluir');">
                                            <i class="fa fa-plus-circle"></i> Adicionar
                                        </button>
                                        <button class="btn btn-primary btn-sm mb-1 mr-1" id="botaoAlterarAssunto" name="botaoAlterarAssunto"
                                                value="Alterar" type="button" onclick="carregarAssunto('alterar');" disabled>
                                            <i class="fa fa-edit"></i> Alterar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridComponentesAssunto" class="col-xl-12 col-md-12 col-sm-12">
                    <jsp:include page="/jsp/atendimento/chamadoassunto/gridComponentesAssunto.jsp"></jsp:include>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-row mt-2">
                    <div class="col-md-12">
                        <div class="row justify-content-between">
                            <div class="col-md-6 col-sm-12 mt-1">
                                <button name="button"
                                        class="btn btn-default btn-sm mb-1 mr-1"
                                        type="button"
                                        onclick="voltar()">
                                    <i class="fa fa-reply"></i> Cancelar
                                </button>
                                <button name="button"
                                        class="btn btn-danger btn-sm mb-1 mr-1"
                                        type="button"
                                        onclick="limparFormulario()">
                                    <i class="fa fa-times"></i> Limpar
                                </button>
                            </div>
                            <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                <vacess:vacess param="alterarChamadoTipo">
                                    <button name="button" class="btn btn-primary btn-sm mb-1 mr-1" type="button" onclick="alterar();">
                                        <i class="fa fa-save"></i> Salvar
                                    </button>
                                </vacess:vacess>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form:form>
</div>
<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/tipoChamadoComAssunto/alterarChamadoAssunto/exibirAlteracaoChamadoAssunto/index.js"></script>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/tipoChamadoComAssunto/exibirAlteracaoChamadoAssunto/index.js" type="application/javascript"></script>
