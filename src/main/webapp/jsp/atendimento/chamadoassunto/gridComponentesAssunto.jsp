<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

$(document).ready(function(){
});

function debugAs(assuntojson){	
 	console.log(assuntojson);
}

function desabilitarCredito() { 
    document.getElementById("rubricaCredito").disabled=true;
    document.getElementById("rubricaCredito").value=-1;
}

function desabilitarDebito() {
    document.getElementById("rubricaDebito").disabled=true;
    document.getElementById("rubricaDebito").value=-1;
}

function habilitarCredito() {	
	document.getElementById("rubricaCredito").disabled=false;
}

function habilitarDebito() {
	document.getElementById("rubricaDebito").disabled=false;
}

function removerAssunto(indexAssunto){
	var noCache = "noCache=" + new Date().getTime();
	$("#gridComponentesAssunto").load("removerAssunto?indexAssunto="+indexAssunto+"&"+noCache);
}

function exibirAlteracao(indexAssunto, chamadoAssuntoJSON){

	var assuntoJSON = JSON.parse(chamadoAssuntoJSON);
	
	document.getElementById('descricaoAssunto').value = assuntoJSON.descricao;
	if (assuntoJSON.orientacao != undefined) {
		document.getElementById('orientacaoAssunto').value = assuntoJSON.orientacao;
	}
	document.getElementById('quantidadeHorasPrevistaAtendimento').value = assuntoJSON.quantidadeHorasPrevistaAtendimento;		
	
	document.forms[0].indexList.value = indexAssunto;
	if(assuntoJSON.indicadorDiaCorridoUtil==29) {
		document.forms[0].indicadorDiaCorridoUtil[0].checked=true;
	} else {
		document.forms[0].indicadorDiaCorridoUtil[1].checked=true;
	}
	if(assuntoJSON.indicadorCredito){
		document.forms[0].indicadorCredito[0].checked=true;
		habilitarCredito();
	 }else{
		 document.forms[0].indicadorCredito[1].checked=true;
		 desabilitarCredito();
	 }
	if(assuntoJSON.rubricaCredito != ""){
		document.getElementById('rubricaCredito').value = assuntoJSON.rubricaCredito;
	}
	if(assuntoJSON.questionario != ""){
		document.getElementById('questionario').value = assuntoJSON.questionario;
	}
	if(assuntoJSON.indicadorDebito){
		document.forms[0].indicadorDebito[0].checked=true;
		habilitarDebito();
	 }else{
		 document.forms[0].indicadorDebito[1].checked=true;
		 desabilitarDebito();
	 }
	
	if(assuntoJSON.rubricaDebito != ""){
		document.getElementById('rubricaDebito').value = assuntoJSON.rubricaDebito;
	}

	document.getElementById('canalAtendimento').value = assuntoJSON.canalAtendimento;
	document.getElementById('unidadeOrganizacional').value = assuntoJSON.unidadeOrganizacional;
	document.getElementById('indicadorRegulatorioValores').value = assuntoJSON.indicadorRegulatorio;
	$("#usuarioResponsavel").prop("disabled", false);
	if(assuntoJSON.usuarioResponsavel == ""){
		assuntoJSON.usuarioResponsavel = "-1";
	}
	
	var noCache = "noCache=" + new Date().getTime();
    $("#divResponsavelAssunto").load("carregarResponsavelAssunto?chaveUnidadeOrganizacional="+assuntoJSON.unidadeOrganizacional+"&chaveUsuarioResponsavel="+assuntoJSON.usuarioResponsavel+"&"+noCache);
	
	
	if(assuntoJSON.indicadorClienteObrigatorio){
		document.forms[0].indicadorClienteObrigatorio[0].checked=true;
	 }else{
		 document.forms[0].indicadorClienteObrigatorio[1].checked=true;
	 }

    if(assuntoJSON.permiteAbrirEmLote){
        document.forms[0].permiteAbrirEmLote[0].checked=true;
        $("input[name=indicadorAcionamentoGasista]").prop("disabled", true);
        $("input[name=indicadorVerificarVazamento]").prop("disabled", true);
    }else{
        document.forms[0].permiteAbrirEmLote[1].checked=true;
    }

    if(assuntoJSON.indicadorComGarantia){
        document.forms[0].indicadorComGarantia[0].checked=true;
    }else{
        document.forms[0].indicadorComGarantia[1].checked=true;
    }

	
	if(assuntoJSON.indicadorImovelObrigatorio){
		document.forms[0].indicadorImovelObrigatorio[0].checked=true;
	 }else{
		 document.forms[0].indicadorImovelObrigatorio[1].checked=true;
	 }
	if(assuntoJSON.indicadorRgSolicitanteObrigatorio){
		document.forms[0].indicadorRgSolicitanteObrigatorio[0].checked=true;
	 }else{
		 document.forms[0].indicadorRgSolicitanteObrigatorio[1].checked=true;
	 }
	if(assuntoJSON.indicadorCpfCnpjSolicitanteObrigatorio){
		document.forms[0].indicadorCpfCnpjSolicitanteObrigatorio[0].checked=true;
	 }else{
		 document.forms[0].indicadorCpfCnpjSolicitanteObrigatorio[1].checked=true;
	 }
	if(assuntoJSON.indicadorTramitacao){
		document.forms[0].indicadorTramitacao[0].checked=true;
	 }else{
		 document.forms[0].indicadorTramitacao[1].checked=true;
	 }
	
	
	if(assuntoJSON.indicadorNomeSolicitanteObrigatorio){
		document.forms[0].indicadorNomeSolicitanteObrigatorio[0].checked=true;
	 }else{
		 document.forms[0].indicadorNomeSolicitanteObrigatorio[1].checked=true;
	 }
	
	if(assuntoJSON.indicadorTelefoneSolicitanteObrigatorio){
		document.forms[0].indicadorTelefoneSolicitanteObrigatorio[0].checked=true;
	 }else{
	    document.forms[0].indicadorTelefoneSolicitanteObrigatorio[1].checked=true;
	 }
	
	if(assuntoJSON.indicadorEmailSolicitanteObrigatorio){
		document.forms[0].indicadorEmailSolicitanteObrigatorio[0].checked=true;
	 }else{
	    document.forms[0].indicadorEmailSolicitanteObrigatorio[1].checked=true;
	 }
	
	
	if(assuntoJSON.indicadorGeraServicoAutorizacao){
		document.forms[0].indicadorGeraServicoAutorizacao[0].checked=true;
	 }else{
	    document.forms[0].indicadorGeraServicoAutorizacao[1].checked=true;
	 }

	if(assuntoJSON.indicadorFechamentoAutomatico){
		document.forms[0].indicadorFechamentoAutomatico[0].checked=true;
	 }else{
	    document.forms[0].indicadorFechamentoAutomatico[1].checked=true;
	 }
	
	if(assuntoJSON.indicadorPrazoDiferenciado){
		document.forms[0].indicadorPrazoDiferenciado[0].checked=true;
	 }else{
	    document.forms[0].indicadorPrazoDiferenciado[1].checked=true;
	 }
	
	if(assuntoJSON.indicadorVerificaRestServ){
		document.forms[0].indicadorVerificaRestServ[0].checked=true;
	 }else{
	    document.forms[0].indicadorVerificaRestServ[1].checked=true;
	 }
	
	if(assuntoJSON.indicadorEncerraAutoASRel){
		document.forms[0].indicadorEncerraAutoASRel[0].checked=true;
	 }else{
	    document.forms[0].indicadorEncerraAutoASRel[1].checked=true;
	 }
	
	if(assuntoJSON.indicadorVerificarVazamento){
		document.forms[0].indicadorVerificarVazamento[0].checked=true;
	 }else{
	    document.forms[0].indicadorVerificarVazamento[1].checked=true;
	 }

	if(assuntoJSON.indicadorAcionamentoGasista){
		document.forms[0].indicadorAcionamentoGasista[0].checked=true;
	 }else{
	    document.forms[0].indicadorAcionamentoGasista[1].checked=true;
	 }
	
	if(assuntoJSON.habilitado){
		document.forms[0].habilitadoAssunto[0].checked=true;
	 }else{
	    if(document.forms[0].habilitadoAssunto) {
         document.forms[0].habilitadoAssunto[1].checked=true;
        }
	 }
	
	if(assuntoJSON.indicadorRegulatorio){
		document.forms[0].possuiIndicadorRegulatorio[0].checked = true;
		carregarIndicadorRegulario();
		$("#indicadorRegulatorioValores").find("option:contains('" + assuntoJSON.indicadorRegulatorio + "')").prop("selected", true);
	}else{
		document.forms[0].possuiIndicadorRegulatorio[1].checked = true;
		carregarIndicadorRegulario();
	}
	
    $("[name='enviarEmailCadastro']").prop("checked", false);
    $("[name='enviarEmailEncerrar']").prop("checked", false);
    $("[name='enviarEmailReiterar']").prop("checked", false);
    $("[name='enviarEmailTramitar']").prop("checked", false);
    $("[name='enviarEmailPrioridadeProtocolo']").prop("checked",false);
	 assuntoJSON.enviarEmailCadastro.forEach(function(valor) {
         $("[name='enviarEmailCadastro'][value='" + valor + "']").prop("checked", true);
     });
	assuntoJSON.enviarEmailEncerrar.forEach(function(valor) {
         $("[name='enviarEmailEncerrar'][value='" + valor + "']").prop("checked", true);
     });
	assuntoJSON.enviarEmailReiterar.forEach(function(valor) {
         $("[name='enviarEmailReiterar'][value='" + valor + "']").prop("checked", true);
     });
    assuntoJSON.enviarEmailTramitar.forEach(function(valor) {
         $("[name='enviarEmailTramitar'][value='" + valor + "']").prop("checked", true);
     });
    assuntoJSON.enviarEmailPrioridadeProtocolo.forEach(function(valor) {
    	$("[name='enviarEmailPrioridadeProtocolo'][value='" + valor + "']").prop("checked", true);
    });
	
	var botaoAlterarAssunto = document.getElementById("botaoAlterarAssunto");
	botaoAlterarAssunto.disabled = false;

	if (assuntoJSON.indicadorUOVisualizadora){
		document.forms[0].indicadorUnidadesOrganizacionalVisualizadora[0].checked=true;
		$("#unidadeOrganizacionalVisualizadoras :selected").removeAttr("selected");
		$("#divUnidadeOrganizacionalVisualizadoras").show();
		
			var unidades = assuntoJSON.unidadesVisualizadoras;
			
			unidades.forEach(function(unidade){
				$("#unidadeOrganizacionalVisualizadoras option[value="+unidade.id+"]").attr("selected","selected")
			});
		
	}else{
		document.forms[0].indicadorUnidadesOrganizacionalVisualizadora[1].checked=true;
		$("#divUnidadeOrganizacionalVisualizadoras").hide();
	}
	
	if(assuntoJSON.nomeRelatorioRegulatorio != ""){
		document.getElementById('nomeRelatorioRegulatorio').value = assuntoJSON.nomeRelatorioRegulatorio;
	}
	
	var botaoIncluirAssunto = document.getElementById("botaoIncluirAssunto");
	botaoIncluirAssunto.disabled = true;
	var noCache = "noCache=" + new Date().getTime();
    var url = "carregarGridServicos?indexAssunto="+indexAssunto+"&"+noCache;
    carregarFragmento('gridComponentesServicoTipo',url);
	
	obterListaServicos();
}
	
</script>

<c:set var="indexAssunto" value="0" />

<div class="col-md-12">
    <div class="table-responsive mt-1">
        <table class="table table-bordered table-striped table-hover" id="tableAssunto" width="100%" >
            <thead class="thead-ggas-bootstrap">
                <tr>
                    <th width="80%" scope="col" class="text-center">Descri��o do Assunto</th>
                    <th width="10%" scope="col" class="text-center"></th>
                    <th width="10%" scope="col" class="text-center"></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="assunto" items="${listaAssunto}">
               <tr>
                  <td class="text-center">${assunto.descricao}</td>
                  <td class="text-center">
                     <a title="Alterar assunto" href='javascript:exibirAlteracao("${indexAssunto}", JSON.stringify(${assunto.chamadoAssuntoJSON}));'>
                        <i class="fa fa-edit"></i>
                     </a>
                  </td>
                  <td class="text-center">
                     <a title="Excluir assunto" onclick="return confirm('Deseja excluir o assunto?');" href="javascript:removerAssunto(<c:out value="${indexAssunto}"/>);">
                        <i class="fa fa-trash" style="color: #dc3545"></i>
                     </a>
                  </td>
               </tr>
               <c:set var="indexAssunto" value="${indexAssunto+1}" />
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

