<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
		src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<div class="bootstrap">
	<form:form action="pesquisarChamadoAssunto" id="chamadoAssuntoForm" name="chamadoAssuntoForm" method="post" modelAttribute="TipoChamadoVO">

		<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Pesquisar Tipo de Chamado com Assunto</h5>
			</div>
			<div class="card-body">

				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i>
					Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em
					<b>Pesquisar</b>, ou clique apenas em <b>Pesquisar</b> para exibir todos. Para incluir um novo
					registro clique em <b>Incluir</b>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body bg-light">
								<h5>Filtrar tipo de chamado</h5>
								<div class="row">
									<div class="col-xl-4 col-lg-6 col-md-12">
										<label for="descricao" >Descri��o:</label>
										<input class="form-control form-control-sm" type="text" name="descricao" id="descricao"
											   maxlength="40" size="40" value="${chamadoTipo.descricao}" onkeyup="letraMaiuscula(this);"
											   onblur="this.value = removerEspacoInicialFinal(this.value);"/>
									</div>
									<div class="col-xl-4 col-lg-6 col-md-12">
										<label for="idSegmentoChamadoTipo">Segmento</label>
										<select id="idSegmentoChamadoTipo" class="form-control form-control-sm" name="idSegmentoChamadoTipo">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSegmento}" var="segmento">
												<option value="<c:out value="${segmento.chavePrimaria}"/>"
														<c:if test="${idSegmentoChamadoTipo == segmento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${segmento.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-xl-4 col-lg-6 col-md-12">
										<label for="categoriaChamado">Categoria</label>
										<select id="categoriaChamado" class="form-control form-control-sm" name="categoriaChamado">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaCategoria}" var="categoria">
												<option value="<c:out value="${categoria.name}"/>"
														<c:if test="${chamadoTipo.categoria.name == categoria.name}">selected="selected"</c:if>>
													<c:out value="${categoria.descricao}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-4 col-lg-6 col-md-12">
										<label class="float-none">Situa��o</label><br>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="habilitado" id="situacaoAtivo" class="custom-control-input"
												   value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
											<label class="custom-control-label" for="situacaoAtivo">Ativo</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="habilitado" id="situacaoInativo" class="custom-control-input"
												   value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
											<label class="custom-control-label" for="situacaoInativo">Inativo</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="habilitado" id="situacaoTodos" class="custom-control-input"
												   value="" <c:if test="${habilitado eq ''}">checked</c:if>>
											<label class="custom-control-label" for="situacaoTodos">Todos</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mt-3">
					<div class="col-sm-12">
						<button id="limparFormulario" type="button" class="btn btn-secondary btn-sm float-right" onclick="limparCampos();">
							<i class="fa fa-times"></i> Limpar
						</button>
						<vacess:vacess param="pesquisarChamadoTipo">
							<button id="botaoPesquisar" type="submit" class="btn btn-primary btn-sm float-right mr-1">
								<i class="fa fa-search"></i> Pesquisar
							</button>
						</vacess:vacess>
					</div>
				</div>

				<c:if test="${listaChamadoAssunto ne null}">
					<hr/>

					<div class="text-center loading">
						<img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
					</div>

					<div class="table-responsive pt-3">
						<table class="table table-bordered table-striped table-hover" id="tableChamadoAssunto" width="100%" style="opacity: 0;">
							<thead class="thead-ggas-bootstrap">
								<th scope="col" class="text-center">
									<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
										<input id="checkAllAuto" type="checkbox"  name="checkAllAuto" class="custom-control-input">
										<label class="custom-control-label p-0" for="checkAllAuto"></label>
									</div>
								</th>
								<th scope="col" class="text-center">Ativo</th>
								<th scope="col" class="text-center">Descri��o</th>
								<th scope="col" class="text-center">Segmento</th>
								<th scope="col" class="text-center">Categoria</th>
							</thead>
							<tbody>
							<c:forEach items="${listaChamadoAssunto}" var="chamado">
								<tr>
									<td class="text-center">
										<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="chk${chamado.chavePrimaria}" type="checkbox"  name="chavesPrimarias"
												   class="custom-control-input" value="${chamado.chavePrimaria}">
											<label class="custom-control-label p-0" for="chk${chamado.chavePrimaria}"></label>
										</div>
									</td>
									<td class="text-center">
										<c:if test="${chamado.habilitado == true}">
											<i title="Ativo" class="fa fa-check-circle text-success fa-lg"></i>
										</c:if>
										<c:if test="${chamado.habilitado == false}">
											<i title="Inativo" class="fa fa-times-circle text-danger fa-lg"></i>
										</c:if>
									</td>
									<td class="text-center">
										<a href="javascript:exibirDetalhamento(<c:out value='${chamado.chavePrimaria}'/>);">
											<span class="linkInvisivel"></span>
											<c:out  value="${chamado.descricao}"/>
										</a>
									</td>
									<td class="text-center">
										<a href="javascript:exibirDetalhamento(<c:out value='${chamado.chavePrimaria}'/>);">
											<span class="linkInvisivel"></span>
											<c:out  value="${chamado.segmento.descricao}"/>
										</a>
									</td>
									<td class="text-center">
										<a href="javascript:exibirDetalhamento(<c:out value='${chamado.chavePrimaria}'/>);">
											<span class="linkInvisivel"></span>
											<c:out  value="${chamado.categoria.descricao}"/>
										</a>
									</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
			</div>

			<div class="card-footer">
				<div class="row">
					<div class="col-sm-12">
						<vacess:vacess param="exibirInclusaoChamadoTipo">
							<button id="botaoIncluir" type="button" class="btn btn-primary btn-sm float-right" onclick="incluir()">
								<i class="fa fa-plus"></i> Incluir
							</button>
						</vacess:vacess>
						<c:if test="${not empty listaChamadoAssunto}">
							<vacess:vacess param="exibirAlteracaoChamadoTipo">
								<button id="abrirAlteracao" type="button" class="btn btn-primary btn-sm float-right mr-1" onclick="alterar()">
									<i class="fa fa-edit"></i> Alterar
								</button>
							</vacess:vacess>
							<vacess:vacess param="removerChamadoTipo">
								<button id="buttonRemover" type="button" class="btn btn-danger btn-sm float-right mr-1" onclick="removerChamadoTipo()">
									<i class="fa fa-trash"></i> Remover
								</button>
							</vacess:vacess>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>

<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/tipoChamadoComAssunto/pesquisarChamadoAssunto/exibirPesquisaChamadoAssunto/index.js"></script>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/tipoChamadoComAssunto/exibirPesquisaChamadoAssunto/index.js"
		type="application/javascript"></script>
