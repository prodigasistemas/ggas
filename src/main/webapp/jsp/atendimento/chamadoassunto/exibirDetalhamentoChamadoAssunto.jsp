<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>


	function voltar() {
		location.href = '<c:url value="/exibirPesquisaChamadoAssunto"/>';
	}

	function alterar(){
		submeter('chamadoAssuntoForm','exibirAlteracaoChamadoAssunto');
	}

	function exibirAssunto(chave){
		var noCache = "noCache=" + new Date().getTime();
		var selectedTab = $('.nav-link.active').attr('aria-controls');
		$("#detalharChamadoAssunto").load("detalheAssunto?chavePrimaria="+chave+"&"+noCache+"&selectedTab="+selectedTab);
	}


</script>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<div class="bootstrap">
    <form:form action="exibirAlteracaoChamadoAssunto" id="chamadoAssuntoForm" name="chamadoAssuntoForm" method="post">
        <input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${chamadoTipo.chavePrimaria}" >
	    <div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Detalhar Chamado com Assunto</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary" role="alert">
					<div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
					<div class="d-inline">
						Para modificar as informa��es deste registro clique em <strong>Alterar</strong>.
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-xl-3 col-lg-6 col-md-12">
								<label for="descricao" >Descri��o:<span class="text-danger">*</span></label>
								<input disabled class="form-control form-control-sm" type="text" name="descricao" id="descricao" disabled
									   maxlength="40" size="40" value="${chamadoTipo.descricao}"/>
							</div>
							<div class="col-xl-3 col-lg-6 col-md-12">
								<label for="idSegmentoChamadoTipo">Segmento:</label>
								<input disabled class="form-control form-control-sm" type="text" name="descricao" id="idSegmentoChamadoTipo" disabled
									   maxlength="40" size="40" value="${chamadoTipo.segmento.descricao}"/>
							</div>
							<div class="col-xl-3 col-lg-6 col-md-12">
								<label for="categoriaChamado">Categoria:</label>
								<input disabled class="form-control form-control-sm" type="text" name="descricao" id="categoriaChamado" disabled
									   maxlength="40" size="40" value="${chamadoTipo.categoria.descricao}"/>
							</div>
							<div class="col-xl-3 col-lg-6 col-md-12">
								<div class="row">
									<div class="col-md-12">
										<label>Indicador de Uso:</label>
									</div>
									<div class="col-md-12">
										<div class="custom-control custom-radio custom-control-inline">
											<input class="custom-control-input" type="radio" name="habilitado" id="habilitadoSim" value="true" disabled
											<c:if test="${chamadoTipo.habilitado eq 'true'}"> checked</c:if>>
											<label class="custom-control-label" for="habilitadoSim">Ativo</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input class="custom-control-input" type="radio" name="habilitado" id="habilitadoNao" value="false" disabled
												   <c:if test="${chamadoTipo.habilitado eq 'false'}">checked</c:if>>
											<label class="custom-control-label" for="habilitadoNao">Inativo</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card mt-3">
					<div class="card-header">
						<h5 class="card-title mb-0">Assunto</h5>
						<ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="tab-caracteristicas" data-toggle="tab"
								   href="#contentTabCaracteristicas" role="tab" aria-controls="contentTabCaracteristicas"
								   aria-selected="true"><i class="fa fa-info-circle"></i> Caracter�sticas</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="tab-campo-obrigatorio" data-toggle="tab"
								   href="#contentTabCampoObrigatorio" role="tab" aria-controls="contentTabCampoObrigatorio"
								   aria-selected="false"><i class="fa fa-font"></i> Obrigatoriedade dos campos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="tab-envio-email" data-toggle="tab"
								   href="#contentTabEnvioEmail" role="tab" aria-controls="contentTabEnvioEmail"
								   aria-selected="false"><i class="fa fa-at"></i> Opera��o de Envio de E-mail</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="tab-servicos" data-toggle="tab"
								   href="#contentTabServicos" role="tab" aria-controls="contentTabServicos"
								   aria-selected="false"><i class="fa fa-hammer"></i> Servi�os</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div id="detalharChamadoAssunto">
							<jsp:include page="/jsp/atendimento/chamadoassunto/divDetalheAssunto.jsp"></jsp:include>
						</div>
					</div>
					<div class="card-footer">
					</div>
				</div>

				<div id="gridComponentesAssunto" class="row">

					<div class="col-md-12">
						<div class="table-responsive mt-1">
							<table class="table table-bordered table-striped table-hover" id="tableAssunto" width="100%" >
								<thead class="thead-ggas-bootstrap">
								<tr>
									<th width="2%" scope="col" class="text-center"></th>
									<th width="98%" scope="col" class="text-center">Descri��o do Assunto</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach var="assunto" items="${chamadoTipo.listaAssuntos}">
									<tr>
										<td>
                                            <input type="radio" name="assuntoSelecionado" onclick="javascript:exibirAssunto('${assunto.chavePrimaria}');">
                                        </td>
										<td class="text-left">${assunto.descricao}</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<div class="form-row mt-2">
					<div class="col-md-12">
						<div class="row justify-content-between">
							<div class="col-md-6 col-sm-12 mt-1">
								<button name="button"
										class="btn btn-default btn-sm mb-1 mr-1"
										type="button"
										onclick="voltar()">
									<i class="fa fa-reply"></i> Cancelar
								</button>
								<button name="button"
										class="btn btn-danger btn-sm mb-1 mr-1"
										type="button"
										onclick="limparFormulario()">
									<i class="fa fa-times"></i> Limpar
								</button>
							</div>
							<div class="col-md-6 col-sm-12 mt-1 text-md-right">
								<vacess:vacess param="exibirAlteracaoChamadoTipo">
									<button name="button" class="btn btn-primary btn-sm mb-1 mr-1" type="button" onclick="alterar();">
										<i class="fa fa-edit"></i> Alterar
									</button>
								</vacess:vacess>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
    </form:form>
</div>
