<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="form-row">
    <div class="col-xl-4 col-lg-6 col-md-12">
        <label for="servicoTipo">Tipo de Servi�o:</label>
        <div class="input-group">
            <select class="form-control form-control-sm" name="servicoTipo" id="servicoTipo">
                <option value="-1">Selecione</option>
                <c:forEach items="${listaServicoTipo}" var="servico">
                    <option value="<c:out value="${servico.chavePrimaria}"/>"<c:if test="${servicoTipo.chavePrimaria == servico.chavePrimaria}">selected="selected"</c:if> >
                        <c:out value="${servico.descricao}"/>
                    </option>
                </c:forEach>
            </select>
            <span class="input-group-btn" style="margin-left: 0.25rem !important;">
                <button class="btn btn-primary btn-sm mb-1 mr-1" id="botaoIncluirTurno" name="botaoIncluirTurno" type="button" onclick="adicionarServico('${servico.chavePrimaria}');">
                    <i class="fa fa-plus-circle"></i> Adicionar
                </button>
            </span>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-md-12">
        <div class="table-responsive mt-1">
            <table class="table table-bordered table-striped table-hover" id="tableServico" width="100%" >
                <thead class="thead-ggas-bootstrap">
                <tr>
                    <th width="70%" scope="col" class="text-left">Descri��o do Servi�o</th>
                    <th width="10%" scope="col" class="text-center">Ordem</th>
                    <th width="10%" scope="col" class="text-center"></th>
                    <th width="10%" scope="col" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                <c:set var="indexServicoTipo" value="0" />
                <c:forEach var="servico" items="${sessionScope.listaServicos}">
                    <tr>
                        <td class="text-left">${servico.servicoTipo.descricao}</td>
                        <td class="text-center">${servico.numeroOrdem}</td>
                        <td class="text-center">
                            <c:if test="${ indexServicoTipo > 0 }">
                                <a href="javascript:alterarOrdemListaServicos(<c:out value="${indexServicoTipo}"/>, 'subirPosicao')">
                                    <img title="Alterar Posi��o Servi�o" alt="Alterar Posi��o Servi�o" src=<c:url value="/imagens/setaCima.png"/>>
                                </a>
                            </c:if>
                            <c:if test="${ fn:length(listaServicos) - 1 !=  indexServicoTipo }">
                                <a href="javascript:alterarOrdemListaServicos(<c:out value="${indexServicoTipo}"/>, 'descerPosicao')">
                                    <img title="Alterar Posi��o Servi�o" alt="Alterar Posi��o Servi�o" src=<c:url value="/imagens/setaBaixo.png"/>>
                                </a>
                            </c:if>
                        </td>
                        <td class="text-center">
                            <a onclick="return confirm('Deseja excluir o servi�o?');" href="javascript:removerServicoTipo(<c:out value="${indexServicoTipo}"/>,
                            <c:out value="${fn:length(listaServicos)}"/>);">
                                <i class="fa fa-trash" style="color: #dc3545"></i>
                            </a>
                        </td>
                    </tr>
                    <c:set var="indexServicoTipo" value="${indexServicoTipo+1}" />
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>
</div>


