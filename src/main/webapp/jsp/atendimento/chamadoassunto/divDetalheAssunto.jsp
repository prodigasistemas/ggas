<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="tab-content">
	<div class="tab-pane fade <c:if test="${(empty selectedTab or selectedTab eq 'contentTabCaracteristicas')}">show active</c:if>" id="contentTabCaracteristicas" role="tabpanel"
		 aria-labelledby="contentTabCaracteristicas">
		<div class="row">
			<div class="col-md-12">
				<div class="form-row">
					<div class="col-xl-8 col-lg-12 col-md-12">
						<label for="descricaoAssunto">Descri��o do Assunto: <span class="text-danger">*</span></label>
						<input disabled type="text" id="descricaoAssunto" name="descricaoAssunto" class="form-control form-control-sm"
							   value="${chamadoAssunto.descricao}" maxlength="40" size="26" disabled/>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<label for="descricaoAssunto">Prazo previsto para atendimento em horas: <span class="text-danger">*</span></label>
						<input disabled type="number" id="quantidadeHorasPrevistaAtendimento" name="quantidadeHorasPrevistaAtendimento" class="form-control form-control-sm"
							   value="${chamadoAssunto.quantidadeHorasPrevistaAtendimento}" maxlength="9" size="9"/>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Gerar Autoriza��o de Servi�o Autom�tica?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorGeraServicoAutorizacao" id="indicadorGeraServicoAutorizacao1"
										   value="true" <c:if test="${chamadoAssunto.indicadorGeraServicoAutorizacao eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorGeraServicoAutorizacao1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorGeraServicoAutorizacao" id="indicadorGeraServicoAutorizacao2"
										   value="false" <c:if test="${chamadoAssunto.indicadorGeraServicoAutorizacao eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorGeraServicoAutorizacao2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Fechamento Autom�tico?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorFechamentoAutomatico" id="indicadorFechamentoAutomatico1" value="true"
									<c:if test="${chamadoAssunto.indicadorFechamentoAutomatico eq 'true'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorFechamentoAutomatico1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorFechamentoAutomatico" id="indicadorFechamentoAutomatico2" value="false"
										   <c:if test="${chamadoAssunto.indicadorFechamentoAutomatico eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorFechamentoAutomatico2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Permite Tramita��o?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorTramitacao" id="indicadorTramitacao1" value="true"
										   <c:if test="${chamadoAssunto.indicadorTramitacao eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorTramitacao1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorTramitacao" id="indicadorTramitacao2" value="false"
										   <c:if test="${chamadoAssunto.indicadorTramitacao eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorTramitacao2">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Verifica Restri��o de Servi�o?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorVerificaRestServ" id="indicadorVerificaRestServ1" value="true"
									<c:if test="${chamadoAssunto.indicadorVerificaRestServ eq 'true'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorVerificaRestServ1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorVerificaRestServ" id="indicadorVerificaRestServ2" value="false"
										   <c:if test="${chamadoAssunto.indicadorVerificaRestServ eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorVerificaRestServ2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Encerrar automaticamente ao encerrar A.S relacionada(s)?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorEncerraAutoASRel" id="indicadorEncerraAutoASRel1" value="true"
									<c:if test="${chamadoAssunto.indicadorEncerraAutoASRel eq 'true'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorEncerraAutoASRel1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorEncerraAutoASRel" id="indicadorEncerraAutoASRel2" value="false"
										   <c:if test="${chamadoAssunto.indicadorEncerraAutoASRel eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorEncerraAutoASRel2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Exige confirmar suspeita de vazamento?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorVerificarVazamento" id="indicadorVerificarVazamento1" value="true"
									<c:if test="${chamadoAssunto.indicadorVerificarVazamento eq 'true'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorVerificarVazamento1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorVerificarVazamento" id="indicadorVerificarVazamento2" value="false"
										   <c:if test="${chamadoAssunto.indicadorVerificarVazamento eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorVerificarVazamento2">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Informa��o sobre acionamento de gasista?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorAcionamentoGasista" id="indicadorAcionamentoGasistaSim" value="true"
									<c:if test="${chamadoAssunto.indicadorAcionamentoGasista eq 'true'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorAcionamentoGasistaSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorAcionamentoGasista" id="indicadorAcionamentoGasistaNao" value="false"
										   <c:if test="${chamadoAssunto.indicadorAcionamentoGasista eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorAcionamentoGasistaNao">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Permite abrir chamado em lote?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled onclick="bloquearOperacoes()" class="custom-control-input" type="radio" name="permiteAbrirEmLote" id="permiteAbrirEmLoteSim" value="true"
									<c:if test="${chamadoAssunto.permiteAbrirEmLote eq 'true'}"> checked</c:if>>
									<label class="custom-control-label">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled onclick="desbloquearOperacoes()" class="custom-control-input" type="radio" name="permiteAbrirEmLote" id="permiteAbrirEmLoteNao" value="false"
										   <c:if test="${chamadoAssunto.permiteAbrirEmLote eq 'false'}">checked</c:if>>
									<label class="custom-control-label">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Assunto com garantia?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorComGarantia" id="indicadorComGarantiaSim" value="true"
									<c:if test="${chamadoAssunto.indicadorComGarantia eq 'true'}"> checked</c:if>>
									<label class="custom-control-label">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorComGarantia" id="indicadorComGarantiaNao" value="false"
										   <c:if test="${chamadoAssunto.indicadorComGarantia eq 'false'}">checked</c:if>>
									<label class="custom-control-label">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Outras Unidades podem visualizar esse chamado?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorUnidadesOrganizacionalVisualizadora"
										   id="indicadorUnidadesOrganizacionalVisualizadoraSim" value="true" onclick="mudarEstadoUnidadesVisualizadoras(this)"
									<c:if test="${chamadoAssunto.indicadorUnidadesOrganizacionalVisualizadora eq 'true'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorUnidadesOrganizacionalVisualizadoraSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorUnidadesOrganizacionalVisualizadora"
										   id="indicadorUnidadesOrganizacionalVisualizadoraNao" value="false" onclick="mudarEstadoUnidadesVisualizadoras(this)"
										   <c:if test="${chamadoAssunto.indicadorUnidadesOrganizacionalVisualizadora eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorUnidadesOrganizacionalVisualizadoraNao">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Prazo Diferenciado?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorPrazoDiferenciado" id="indicadorPrazoDiferenciadoSim"
										   value="true"  <c:if test="${chamadoAssunto.indicadorPrazoDiferenciado eq 'true'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorPrazoDiferenciadoSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorPrazoDiferenciado" id="indicadorPrazoDiferenciadoNao"
										   value="false"  <c:if test="${chamadoAssunto.indicadorPrazoDiferenciado eq 'false'}"> checked</c:if>>
									<label class="custom-control-label" for="indicadorPrazoDiferenciadoNao">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Dias:</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorDiaCorridoUtil" id="indicadorDiaCorridoUtil1"
										   value="corrido" <c:if test="${chamadoAssunto.indicadorDiaCorridoUtil.chavePrimaria eq 29}">checked</c:if>>
									<label class="custom-control-label" for="indicadorDiaCorridoUtil1">Corrido</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorDiaCorridoUtil" id="indicadorDiaCorridoUtil2"
										   value="util" <c:if test="${chamadoAssunto.indicadorDiaCorridoUtil.chavePrimaria eq 30}">checked</c:if>>
									<label class="custom-control-label" for="indicadorDiaCorridoUtil2">�til</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Gera Cr�dito?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorCredito" id="indicadorCredito1"
										   value="true" onclick="habilitarCredito();"<c:if test="${chamadoAssunto.indicadorCredito eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorCredito1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorCredito" id="indicadorCredito2"
										   value="false" onclick="desabilitarCredito();"<c:if test="${chamadoAssunto.indicadorCredito eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorCredito2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Gera d�bito?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorDebito" id="indicadorDebito1"
										   value="true" onclick="habilitarDebito();"<c:if test="${chamadoAssunto.indicadorDebito eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorDebito1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorDebito" id="indicadorDebito2"
										   value="false" onclick="desabilitarDebito();"<c:if test="${chamadoAssunto.indicadorDebito eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorDebito2">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-8">
						<div class="form-row">
							<div class="col-xl-6 col-lg-6 col-md-12">
								<label for="rubricaCredito">Rubrica Cr�dito:</label>
								<input disabled class="form-control form-control-sm" type="text" name="rubricaCredito" id="rubricaCredito" disabled
									   maxlength="40" size="40" value="${chamadoAssuntoVO.rubricaCredito}"/>
							</div>
							<div class="col-xl-6 col-lg-6 col-md-12">
								<label for="rubricaDebito">Rubrica D�bito:</label>
								<input disabled class="form-control form-control-sm" type="text" name="rubricaDebito" id="rubricaDebito" disabled
									   maxlength="40" size="40" value="${chamadoAssuntoVO.rubricaDebito}"/>
							</div>
						</div>
						<div class="form-row">
							<div class="col-xl-6 col-lg-6 col-md-12">
								<label for="canalAtendimento">Canal Atendimento:</label>
								<input disabled class="form-control form-control-sm" type="text" name="canalAtendimento" id="canalAtendimento" disabled
									   maxlength="40" size="40" value="${chamadoAssuntoVO.canalAtendimento}"/>
							</div>
							<div class="col-xl-6 col-lg-6 col-md-12">
								<label for="unidadeOrganizacional">Unidade Organizacional:</label>
								<input disabled class="form-control form-control-sm" type="text" name="unidadeOrganizacional" id="unidadeOrganizacional" disabled
									   maxlength="40" size="40" value="${chamadoAssuntoVO.unidadeOrganizacional}"/>
							</div>
						</div>
						<div class="form-row">
							<div class="col-xl-6 col-lg-6 col-md-12">
								<label for="questionario">Question�rio Satisfa��o:</label>
								<input disabled class="form-control form-control-sm" type="text" name="questionario" id="questionario" disabled
									   maxlength="40" size="40" value="${chamadoAssuntoVO.questionario}"/>
							</div>
							<div class="col-xl-6 col-lg-6 col-md-12">
								<label for="unidadeOrganizacional">Respons�vel:</label>
								<input disabled class="form-control form-control-sm" type="text" name="usuarioResponsavel" id="usuarioResponsavel" disabled
									   maxlength="40" size="40" value="${chamadoAssuntoVO.usuarioResponsavel}"/>
							</div>
						</div>
					</div>
					<div id="divUnidadeOrganizacionalVisualizadoras" class="col-xl-4 col-lg-6 col-md-12">
                        <label for="unidadeOrganizacional">Unidades que podem visualizar:</label>
                        <select class="form-control form-control-sm" style="height: 83%;" name="unidadeOrganizacionalVisualizadoras" id="unidadeOrganizacionalVisualizadoras" multiple="multiple" disabled>
                            <c:forEach items="${chamadoAssuntoVO.listaUnidadesVisualizadoras}" var="unidadeOrganizacional">
                                <option title="${unidadeOrganizacional.unidadeOrganizacional.descricao}" value="<c:out value="${unidadeOrganizacional.unidadeOrganizacional.chavePrimaria}"/>">
                                    <c:out value="${unidadeOrganizacional.unidadeOrganizacional.descricao}"/>
                                </option>
                            </c:forEach>
                        </select>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="tab-pane fade <c:if test="${selectedTab eq 'contentTabCampoObrigatorio'}">show active</c:if>" id="contentTabCampoObrigatorio" role="tabpanel"
		 aria-labelledby="contentTabCampoObrigatorio">
		<div class="row">
			<div class="col-md-12">
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Cliente � Obrigat�rio?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorClienteObrigatorio" id="indicadorClienteObrigatorio1" value="true"
										   onclick="obterListaServicos();" <c:if test="${chamadoAssunto.indicadorClienteObrigatorio eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorClienteObrigatorio1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorClienteObrigatorio" id="indicadorClienteObrigatorio2" value="false" onclick="obterListaServicos();"
										   <c:if test="${chamadoAssunto.indicadorClienteObrigatorio eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorClienteObrigatorio2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Im�vel � Obrigat�rio?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorImovelObrigatorio" id="indicadorImovelObrigatorio1" value="true"
										   onclick="obterListaServicos();" <c:if test="${chamadoAssunto.indicadorImovelObrigatorio eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorImovelObrigatorio1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorImovelObrigatorio" id="indicadorImovelObrigatorio2" value="false"  onclick="obterListaServicos();"
										   <c:if test="${chamadoAssunto.indicadorImovelObrigatorio eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorImovelObrigatorio2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>RG do solicitante � Obrigat�rio?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorRgSolicitanteObrigatorio" id="indicadorRgSolicitanteObrigatorio1" value="true"
										   <c:if test="${chamadoAssunto.indicadorRgSolicitanteObrigatorio eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorRgSolicitanteObrigatorio1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorRgSolicitanteObrigatorio" id="indicadorRgSolicitanteObrigatorio2" value="false"
										   <c:if test="${chamadoAssunto.indicadorRgSolicitanteObrigatorio eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorRgSolicitanteObrigatorio2">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>CPF/CNPJ do solicitante � Obrigat�rio?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorCpfCnpjSolicitanteObrigatorio" id="indicadorCpfCnpjSolicitanteObrigatorio1" value="true"
										   <c:if test="${chamadoAssunto.indicadorCpfCnpjSolicitanteObrigatorio eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorCpfCnpjSolicitanteObrigatorio1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorCpfCnpjSolicitanteObrigatorio" id="indicadorCpfCnpjSolicitanteObrigatorio2" value="false"
										   <c:if test="${chamadoAssunto.indicadorCpfCnpjSolicitanteObrigatorio eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorCpfCnpjSolicitanteObrigatorio2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Nome do Solicitante Obrigat�rio?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorNomeSolicitanteObrigatorio" id="indicadorNomeSolicitanteObrigatorio1" value="true"
										   <c:if test="${chamadoAssunto.indicadorNomeSolicitanteObrigatorio eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorNomeSolicitanteObrigatorio1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorNomeSolicitanteObrigatorio" id="indicadorNomeSolicitanteObrigatorio2" value="false"
										   <c:if test="${chamadoAssunto.indicadorNomeSolicitanteObrigatorio eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorNomeSolicitanteObrigatorio2">N�o</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Telefone do Solicitante Obrigat�rio?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorTelefoneSolicitanteObrigatorio" id="indicadorTelefoneSolicitanteObrigatorio1" value="true"
										   <c:if test="${chamadoAssunto.indicadorTelefoneSolicitanteObrigatorio eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorTelefoneSolicitanteObrigatorio1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorTelefoneSolicitanteObrigatorio" id="indicadorTelefoneSolicitanteObrigatorio2" value="false"
										   <c:if test="${chamadoAssunto.indicadorTelefoneSolicitanteObrigatorio eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorTelefoneSolicitanteObrigatorio2">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-xl-4 col-lg-6 col-md-12">
						<div class="row">
							<div class="col-md-12">
								<label>Email do solicitante � Obrigat�rio?</label>
							</div>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorEmailSolicitanteObrigatorio" id="indicadorEmailSolicitanteObrigatorio1" value="true"
										   <c:if test="${chamadoAssunto.indicadorEmailSolicitanteObrigatorio eq 'true'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorEmailSolicitanteObrigatorio1">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input disabled class="custom-control-input" type="radio" name="indicadorEmailSolicitanteObrigatorio" id="indicadorEmailSolicitanteObrigatorio2" value="false"
										   <c:if test="${chamadoAssunto.indicadorEmailSolicitanteObrigatorio eq 'false'}">checked</c:if>>
									<label class="custom-control-label" for="indicadorEmailSolicitanteObrigatorio2">N�o</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane fade <c:if test="${selectedTab eq 'contentTabEnvioEmail'}">show active</c:if>" id="contentTabEnvioEmail" role="tabpanel" aria-labelledby="contentTabEnvioEmail">
		<div class="row">
			<div class="col-md-12">
				<div class="form-row">
					<div class="col-md-12">
						<label><b>Enviar Email ao cadastrar Chamado:</b></label>
						<label>${enviarEmailCadastro}</label>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-12">
						<label><b>Enviar Email ao tramitar Chamado:</b></label>
						<label>${enviarEmailTramitar}</label>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-12">
						<label><b>Enviar Email ao reiterar Chamado:</b></label>
						<label>${enviarEmailReiterar}</label>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-12">
						<label><b>Enviar Email ao encerrar Chamado:</b></label>
						<label>${enviarEmailEncerrar}</label>
					</div>
				</div>
			    <div class="form-row">
					<div class="col-md-12">
						<label><b>Prioridade no envio do protocolo com e-mails diferentes:</b></label>
						<label>${enviarEmailPrioridadeProtocolo}</label>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-12">
						<label for="orientacaoAssunto">Observa��o/Orienta��o: </label>
						<textarea class="form-control form-control-sm" id="orientacaoAssunto" name="orientacaoAssunto" cols="40" rows="7" maxlength="800" disabled
								  onblur="this.value = removerEspacoInicialFinal(this.value);" onkeypress="return formatarCampoTextoLivreComLimite(event,this,800);"
								  onpaste="return formatarCampoTextoLivreComLimite(event,this,800);">${chamadoAssunto.orientacao}</textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane fade <c:if test="${selectedTab eq 'contentTabServicos'}">show active</c:if>" id="contentTabServicos" role="tabpanel" aria-labelledby="contentTabServicos">
		<div class="form-row">
			<div class="col-md-12">
				<div class="table-responsive mt-1">
					<table class="table table-bordered table-striped table-hover" id="tableServico" width="100%" >
						<thead class="thead-ggas-bootstrap">
						<tr>
							<th width="70%" scope="col" class="text-left">Descri��o do Servi�o</th>
							<th width="10%" scope="col" class="text-center">Ordem</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach var="servico" items="${chamadoAssuntoVO.listaServicos}">
							<tr>
								<td class="text-left">${servico.servicoTipo.descricao}</td>
								<td class="text-center">${servico.numeroOrdem}</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>
