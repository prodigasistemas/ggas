<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
	integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css">
	
	
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"
	integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js"
	integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
	
<script type="text/javascript">
	$(document).ready(function(){
		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});

		$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});
		
	    $(".periodoEscala").inputmask("99:99", {placeholder: "_", greedy: false});
	
	    $('.periodoEscala').timepicker({
	    	  timeFormat: 'HH:mm',
	    	  defaultTime: '08:00',
	    	});
	    
	    
		$('#equipeTurnoTabela').dataTable( {
			"paging": true,
			"language": {
	            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		} );	
		
	});
	
	
	function pesquisarEquipeTurno() {
		submeter("equipeTurnoForm", "pesquisarEquipeTurno");
	}
	
	function exibirIncluirEquipeTurno() {
		submeter("equipeTurnoForm", "exibirIncluirEquipeTurno");
	}
	
	function exibirDetalhamentoEquipeTurno(chaveEquipeTurno) {
		$("#idEquipeTurno").val(chaveEquipeTurno);
		
		submeter("equipeTurnoForm", "exibirDetalhamentoEquipeTurno");
	}
	
	
	function limparFormulario(){		
		$('#equipe option[value=-1]').attr('selected','selected');
		$('#funcionario option[value=-1]').attr('selected','selected');
		
		$('#dataInicio').val('');
		$('#dataFim').val('');
	}
	
function carregarFuncionarios(){
		
		var idEquipe = $("#equipe").val();
		var listaDeFuncionarios = $("#funcionario");
		if(idEquipe != -1 && listaDeFuncionarios != null){
			listaDeFuncionarios.empty();
			listaDeFuncionarios.append('<option value="-1">Selecione</option>');
			
			AjaxService.obterFuncionariosPorEquipe(idEquipe,function(funcionario){
				for (key in funcionario){
				    const option = document.createElement("option");
				    option.value = key;
				    option.textContent = funcionario[key];
				    listaDeFuncionarios.append(option);
				}
			});
		}else{
			listaDeFuncionarios.empty();
			listaDeFuncionarios.append('<option value="-1">Selecione</option>');
			
			AjaxService.listarTodosFuncionarios(function(funcionario){
				for (key in funcionario){
				    const option = document.createElement("option");
				    option.value = key;
				    option.textContent = funcionario[key];
				    listaDeFuncionarios.append(option);
				}
			});
		}
		
	}

	
</script>

<div class="bootstrap">

	<form:form action="gerarEquipeTurno" id="equipeTurnoForm"
		name="equipeTurnoForm" method="post"
		modelAttribute="FiltroEquipeTurnoDTO">
		<input type="hidden" name="idEquipeTurno" id="idEquipeTurno"/>

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Pesquisar Escala da Equipe</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para pesquisar
					o a escala da equipe, clique em <b>Pesquisar</b> para visualizar as
					escalas cadastradas.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">

						<div class="row mb-2">

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
	                                    <label>Per�odo da Escala:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataInicio" name="dataInicio" maxlength="10" value="${equipeTurno.dataInicio}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataFim" name="dataFim" maxlength="10" value="${equipeTurno.dataFim}">
										</div>
									</div>
	                            </div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="funcionario">Funcion�rio:<span class="text-danger">*</span></label>
										<select name="funcionario" id="funcionario"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaFuncionario}" var="funcionario">
												<option value="<c:out value="${funcionario.chavePrimaria}"/>"
													<c:if test="${equipeTurno.funcionario.chavePrimaria == funcionario.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${funcionario.nome}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Equipe:<span class="text-danger">*</span></label>
										<select name="equipe" id="equipe"
											class="form-control form-control-sm"
											onchange="carregarFuncionarios();">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaEquipe}" var="equipe">
												<option value="<c:out value="${equipe.chavePrimaria}"/>"
													<c:if test="${equipeTurno.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${equipe.nome}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>

						<c:if test="${listaEquipeTurno ne null}">
							<br>
							<hr class="linhaSeparadoraPesquisa" />
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover"
									id="equipeTurnoTabela">
									<thead class="thead-ggas-bootstrap">
										<tr>
											<th scope="col" class="text-center">Equipe</th>
											<th scope="col" class="text-center">Data Inicio</th>
											<th scope="col" class="text-center">Data Fim</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listaEquipeTurno}" var="equipeTurnoTabela">
										<tr>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoEquipeTurno(<c:out value='${equipeTurnoTabela.chavePrimaria}'/>);'>
													<c:out value="${equipeTurnoTabela.equipe.nome}" />
												</a>
											</td>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoEquipeTurno(<c:out value='${equipeTurnoTabela.chavePrimaria}'/>);'>
													<fmt:formatDate
														value="${equipeTurnoTabela.dataInicio}"
														pattern="dd/MM/yyyy" /></td>
												</a>
											</td>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoEquipeTurno(<c:out value='${equipeTurnoTabela.chavePrimaria}'/>);'>
													<fmt:formatDate
														value="${equipeTurnoTabela.dataFim}"
														pattern="dd/MM/yyyy" /></td>
												</a>
											</td>											
										</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							
						<br />						
						</c:if>
						
						<br />
						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoPesquisar"
									type="button" onclick="pesquisarEquipeTurno();">Pesquisar</button>
								<button class="btn btn-primary btn-sm" id="botaoPesquisar"
									type="button" onclick="exibirIncluirEquipeTurno();">Incluir</button>									
								<button class="btn btn-secondary btn-sm" name="botaoLimpar"
									id="botaoLimpar" value="limparFormulario" type="button"
									onclick="limparFormulario();">
									<i class="far fa-trash-alt"></i> Limpar
								</button>
							</div>
						</div>						

					</div>

				</div>
			</div>
		</div>
	</form:form>

</div>