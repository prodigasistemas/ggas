<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
	integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css">
	
	
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"
	integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js"
	integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
	
<script src="https://cdn.datatables.net/rowreorder/1.4.1/js/dataTables.rowReorder.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'mm/yyyy',
			language: 'pt-BR',
		    viewMode: "months", 
		    minViewMode: "months"
		});

		$('.bootstrapDP').inputmask("99/9999",{placeholder:"_"});
		
	    $(".periodoEscala").inputmask("99:99", {placeholder: "_", greedy: false});
	    
		$('#equipeTurnoTabela').dataTable( {
			"paging": true,
			"columnDefs": [ {
				"targets": 0,
				"orderable": false
				} ],
			"order": [[1, 'asc']],
			"language": {
	            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		} );	
		
		
	});
	

	
	function atualizarEquipeTurno(idEquipeTurno) {
		
		payload = {
			horarioInicio: $('#horarioInicio_'+idEquipeTurno).val(),
			horarioFim: $('#horarioFim_'+idEquipeTurno).val(),
			funcionario:  $('#funcionario_'+idEquipeTurno).val(),
			veiculo: $('#veiculoAlterar_'+idEquipeTurno).val(),
			chavePrimaria: idEquipeTurno
		}
		
		console.log(payload);
		
		if (!payload.horarioInicio) {
			$('#horarioInicio_'+idEquipeTurno).addClass('is-invalid');
			$('#erro_horaInicio_'+idEquipeTurno).text('Data de leitura � obrigat�rio');
			return false
		};
		$('#horarioInicio_'+idEquipeTurno).removeClass('is-invalid');
		
		if (!payload.horarioFim) {
			$('#horarioFim_'+idEquipeTurno).addClass('is-invalid');
			$('#erro_horarioFim_'+idEquipeTurno).text('Valor da leitura � obrigat�rio');
			return false
		};
		$('#horarioFim_'+idEquipeTurno).removeClass('is-invalid');
		$.ajax({
			type: 'POST',
			url: '/ggas/atualizarEquipeTurnoAJAX',
			data: JSON.stringify(payload),
			success: function(data) {
				try {
					if (data.erro) {
						$('#salvar_'+idEquipeTurno).removeClass('btn-outline-primary');
						$('#salvar_'+idEquipeTurno).addClass('btn-outline-danger');
						$('#erro_equipeturno_'+idEquipeTurno).removeClass('hide');
						$('#erro_equipeturno_'+idLeituraMovimento).text(data.erro);
						return false;
					}
					$('#salvar_'+idEquipeTurno).removeClass('btn-outline-primary');
					$('#salvar_'+idEquipeTurno).addClass('btn-outline-success');
				} catch (e) {
					$('#salvar_'+idEquipeTurno).removeClass('btn-outline-primary');
					$('#salvar_'+idEquipeTurno).addClass('btn-outline-danger');
					$('#erro_equipeturno_'+idEquipeTurno).removeClass('hide');
					$('#erro_equipeturno_'+idEquipeTurno).text('Ocorreu um erro ao tentar salvar a equipe turno');
				}
			},
			beforeSend: function(jqXHR, settings) {
				$('#erro_equipeturno_'+idEquipeTurno).addClass('hide');
				$('#salvar_'+idEquipeTurno).removeClass('btn-outline-danger');
				$('#wait_'+idEquipeTurno).removeClass('hide');
				$('#salvar_'+idEquipeTurno).addClass('hide');
			},
			complete: function(data) {
				$('#wait_'+idEquipeTurno).addClass('hide');
				$('#salvar_'+idEquipeTurno).removeClass('hide');
			},
			contentType: 'application/json'
		});
		return true;
	}
	
	function voltar(){
		
		if($("#funcionario").val() == '') {
			$("#funcionario").val(-1);
		}
		
		if($("#equipe").val() == '') {
			$("#equipe").val(-1);
		}
		
		submeter('equipeTurnoForm', 'pesquisarEquipeTurno');
	}
	
	function excluirDia(){
		
		if(verificarSelecao()) {
			submeter('equipeTurnoForm', 'excluirDiaEscala');
		}
	}
	
	function habilitarBotaoAlteracao(element) {
		var idEquipeTurno = element.id.split('_')[1];
		$('#salvar_'+idEquipeTurno).removeClass('btn-outline-dark');
		$('#salvar_'+idEquipeTurno).removeClass('btn-outline-success');
		$('#salvar_'+idEquipeTurno).addClass('btn-outline-primary');
	}
	
	function atualizarEquipeTurnoLote(idEquipeTurno) {
		
		if($('#salvar_'+idEquipeTurno).hasClass('btn-outline-dark')) {
			return false;
		}
		
		if(confirm("Deseja replicar altera��es para as futuras datas?")) {
			
			$("#funcionarioLote").val($('#funcionario_'+idEquipeTurno).val());
			$("#horarioInicioLote").val($('#horarioInicio_'+idEquipeTurno).val());
			$("#horarioFimLote").val($('#horarioFim_'+idEquipeTurno).val());
			$("#veiculoLote").val($('#veiculoAlterar_'+idEquipeTurno).val());
			$("#idEquipeTurnoItem").val(idEquipeTurno);
			
			submeter('equipeTurnoForm', 'atualizacaoEmLote');
		} else {
			atualizarEquipeTurno(idEquipeTurno);
		}
		
	}
	
</script>

<div class="bootstrap">

	<form:form action="gerarEquipeTurno" id="equipeTurnoForm"
		name="equipeTurnoForm" method="post"
		modelAttribute="FiltroEquipeTurnoDTO">
		<input type=hidden name="dataInicio" id="dataInicio" value="${ equipeTurno.dataInicio }"/>
		<input type=hidden name="dataFim" id="dataFim" value="${ equipeTurno.dataFim }"/>
		<input type=hidden name="equipe" id="equipe" value="${ equipeTurno.equipe ne null ? equipeTurno.equipe.chavePrimaria : -1}"/>
		<input type=hidden name="funcionario" id="funcionario" value="${ equipeTurno.funcionario ne null ? equipeTurno.funcionario.chavePrimaria : -1}"/>
		<input type="hidden" name="idEquipeTurno" id="idEquipeTurno" value="${idEquipeTurno}"/>
		
		<input type=hidden name="equipeLote" id="equipeLote"/>
		<input type=hidden name="funcionarioLote" id="funcionarioLote"/>
		<input type=hidden name="horarioInicioLote" id="horarioInicioLote"/>
		<input type=hidden name="horarioFimLote" id="horarioFimLote"/>
		<input type=hidden name="veiculoLote" id="veiculoLote"/>
		
		<input type="hidden" name="idEquipeTurnoItem" id="idEquipeTurnoItem"/>
		
		

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Detalhamento Escala da Equipe</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Caso queira alterar um item da escala, preencha os dados e clique no bot�o ao lado para salvar
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">

						<c:if test="${listaEquipeTurno ne null}">
							<br>
							<hr class="linhaSeparadoraPesquisa" />
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover"
									id="equipeTurnoTabela">
									<thead class="thead-ggas-bootstrap">
										<tr>
											<th>
												<div
													class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
													<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
														class="custom-control-input"> <label
														class="custom-control-label p-0" for="checkAllAuto"></label>
												</div>
											</th>										
											<th scope="col" class="text-center">Dia</th>
											<th scope="col" class="text-center">Equipe</th>
											<th scope="col" class="text-center">Funcion�rio</th>
											<th scope="col" class="text-center">Ve�culo</th>
											<th scope="col" class="text-center">Hor�rio Inicial</th>
											<th scope="col" class="text-center">Hor�rio FInal</th>
											<th scope="col" class="text-center"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listaEquipeTurno}" var="equipeTurnoTabela">
											<tr id="equipeTurno_${equipeTurnoTabela.chavePrimaria}">
												<td>
													<div
														class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
														data-identificador-check="chk${equipeTurnoTabela.chavePrimaria}">
														<input id="chk${equipeTurnoTabela.chavePrimaria}" type="checkbox"
															name="chavesPrimarias" class="custom-control-input"
															value="${equipeTurnoTabela.chavePrimaria}"> <label
															class="custom-control-label p-0"
															for="chk${equipeTurnoTabela.chavePrimaria}"></label>
													</div>
												</td>											
												<td class="text-center" data-order="${equipeTurnoTabela.retornarTempoEmMilisegundos()}"> <fmt:formatDate
														value="${equipeTurnoTabela.dataInicio}"
														pattern="dd/MM/yyyy" /></td>
												<td class="text-center"><c:out
														value="${equipeTurnoTabela.equipe.nome}" /></td>
												<td class="text-right">
													<select name="funcionario_${equipeTurnoTabela.chavePrimaria}" id="funcionario_${equipeTurnoTabela.chavePrimaria}"
													    onchange="habilitarBotaoAlteracao(this);"
														class="form-control form-control-sm">
														<c:forEach items="${listaFuncionarios}" var="funcionario">
															<option value="<c:out value="${funcionario.chavePrimaria}"/>"
																<c:if test="${equipeTurnoTabela.funcionario.chavePrimaria == funcionario.chavePrimaria}">selected="selected"</c:if>>
																<c:out value="${funcionario.nome}" />
															</option>
														</c:forEach>
													</select>
												</td>
												<td class="text-center">
													<select name="veiculo"
														onchange="habilitarBotaoAlteracao(this);"
														id="veiculoAlterar_${equipeTurnoTabela.chavePrimaria}"
														class="form-control form-control-sm">
															<option value="-1">Selecione</option>
															<c:forEach items="${listaVeiculos}" var="veiculo">
																<option value="<c:out value="${veiculo.chavePrimaria}"/>"
																   <c:if test="${equipeTurnoTabela.veiculo.chavePrimaria == veiculo.chavePrimaria}">selected="selected"</c:if>>
																	<c:out value="${veiculo.modelo}-${veiculo.placa}" />
																</option>
															</c:forEach>
													</select> 
												</td>
												<td class="text-center">
												<div class="invalid-feedback" id="erro_horaInicio_${equipeTurnoTabela.chavePrimaria}"></div>
												<input style="width: 120px"
													onkeypress="habilitarBotaoAlteracao(this);"
													class="form-control periodoEscala" type="text"
													id="horarioInicio_${equipeTurnoTabela.chavePrimaria}" name="horarioInicio_${equipeTurnoTabela.chavePrimaria}"
													maxlength="10" size="6"
													value="<fmt:formatDate value="${equipeTurnoTabela.dataInicio}" pattern="HH:mm"/>">
												</td>
												<td class="text-right">
												<div class="invalid-feedback" id="erro_horarioFim_${equipeTurnoTabela.chavePrimaria}"></div>
												<input style="width: 120px"
													onkeypress="habilitarBotaoAlteracao(this);"
													class="form-control periodoEscala" type="text"
													id="horarioFim_${equipeTurnoTabela.chavePrimaria}" name="horarioFim_${equipeTurnoTabela.chavePrimaria}" maxlength="10"
													size="6"
													value="<fmt:formatDate value="${equipeTurnoTabela.dataFim}" pattern="HH:mm"/>">
												</td>
												<td class="text-center h5"><span role="button"
													id="salvar_${equipeTurnoTabela.chavePrimaria}" class="btn btn-outline-dark"
													onclick="return atualizarEquipeTurnoLote(${equipeTurnoTabela.chavePrimaria});"><i
														class="fa fa-check-square fa-3"></i></span> <span id="wait_${equipeTurnoTabela.chavePrimaria}"
													class="hide"> <i class="fa fa-spinner fa-spin"
														aria-hidden="true"></i></span>
													<div class="alert alert-danger hide" role="alert"
														id="erro_equipeturno_${equipeTurnoTabela.chavePrimaria}"></div></td>

											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							
							<br />							
						</c:if>
						
						<div class="card-footer">
							<div class="row">
								<div class="col-sm-12">
									<button class="btn btn-danger btn-sm float-right ml-1 mt-1"
										type="button" onclick="voltar();">
										<i class="fa fa-times"></i> Voltar
									</button>
									<button class="btn btn-primary float-right ml-1 mt-1 btn-sm"  id="botaoExclusaoDia"
												type="button" onclick="excluirDia();">
												Excluir Dias
										</button>										
								</div>
							</div>
						</div>						

					</div>

				</div>
			</div>
		</div>
	</form:form>

</div>