<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
	integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css">
	
	
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"
	integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js"
	integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>
	
<script src="https://cdn.datatables.net/rowreorder/1.4.1/js/dataTables.rowReorder.min.js"></script>





<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery-weekdays.js"></script>

<link rel="stylesheet" media="all" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/jquery-weekdays.css"/>

<script type="text/javascript">
	$(document).ready(function(){
		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});
		
		$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});
		
	    $(".periodoEscala").inputmask("99:99", {placeholder: "_", greedy: false});
	    
	    
		$('#equipeTurnoTabela').DataTable( {
			"paging": true,
			"language": {
	            "url": "//cdn.datatables.net/plug-ins/1.12.1/i18n/pt-BR.json"
			}
		} )
		
		
		<c:if test="${not empty equipeTurno.funcionarios}">
			var funcionarios = [];
			var i = 0;
			
			<c:set var="index" value="0" />
			<c:forEach var="funcionario" items="${equipeTurno.funcionarios}">
		     		var selectVeiculos = '<select name="veiculo" id="veiculo_'+${funcionario.chavePrimaria}+'"' +
					'class="form-control form-control-sm">' +
						'<option value="-1">Selecione</option>' +
						'<c:forEach items="${listaVeiculos}" var="veiculo">' +
							'<option value="<c:out value="${veiculo.chavePrimaria}"/>"' +
							' <c:if test="${equipeTurno.veiculo[index].chavePrimaria == veiculo.chavePrimaria}">selected="selected"</c:if>>' +
								'<c:out value="${veiculo.placa}" />' +
							'</option>' +
						'</c:forEach>' +
						'</select>'
				    var checkbox = '<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">' +
				                                   '<input id="chk_' + key + '" type="checkbox" ' +
				                                          'name="chavesPrimarias" class="custom-control-input" ' +
				                                          'value="' + funcionarios[key] + '"> ' +
				                                   '<label class="custom-control-label" for="chk_' + key + '"></label>' +
				                               '</div>';							
			      	  var funcionarioArray = [];
			      	  funcionarioArray.push(checkbox);
					  funcionarioArray.push(''+i+'');
			    	  funcionarioArray.push('${funcionario.nome} <input type="hidden" name="funcionarios" id="funcionario_'+${funcionario.chavePrimaria}+'" value="'+${funcionario.chavePrimaria}+'"/>');
			    	  funcionarioArray.push('');
			    	  funcionarioArray.push(selectVeiculos);
			    	  funcionarioArray.push('<a href="#" class="deletarFuncionario"><img title="Excluir Cliente" alt="Excluir Cliente" src="<c:url value="/imagens/deletar_x.png"/>" border="0" /> </a>');
			    	  funcionarios.push(funcionarioArray);
			    	  i++;
			<c:set var="index" value="${index + 1}" />
			</c:forEach>
				criarTabelaFuncionarios(funcionarios);
		</c:if>
		
		
		estilizarBotaoAdicionarRemoverHorario();
		
		
		var quantidadeHorarios = parseInt("${fn:length(equipeTurno.horaInicio)}");
		
		if (quantidadeHorarios > 1) {
			for(let i = 2; i <= quantidadeHorarios; i++) {
				adicionarNovosHorarios(i - 1);
			}
		}
		
		
	});
	
	function estilizarBotaoAdicionarRemoverHorario() {
	    $("input.botaoAdicionarNovoHorario").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'25px', 'margin-top': '8px','cursor':'pointer'});
	    $("input.botaoRemoverNovoHorario").css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'5px', 'margin-top' : '8px','cursor':'pointer'});
	
	    $("input.botaoAdicionarNovoHorario").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'25px', 'margin-top': '8px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/add-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'25px', 'margin-top': '8px','cursor':'pointer'});
			}
		);
	
	    $("input.botaoRemoverNovoHorario").hover(
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16-over.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'5px', 'margin-top' : '8px','cursor':'pointer'});
			},
			function () {
				$(this).css({'background':'url(<c:out value='${pageContext.request.contextPath}'/>/imagens/rem-16x16.png) no-repeat','border':'0','height':'16px','width':'16px','margin-left':'5px', 'margin-top' : '8px','cursor':'pointer'});
			}
		);
    }
	
	
	function gerarEquipeTurno() {
		
		var diasSelecionados = [];
		
		if(validarDiasSelecionados()){
			
			$('#weekdays').selectedIndexes().map(function(nome, i) {
				diasSelecionados.push(i);
			});
			
			$("#diasSemana").val(diasSelecionados);
			
			submeter("equipeTurnoForm", "gerarEquipeTurno");
		}
		
	}
	
	function limparFormulario(){		
		$('#equipe option[value=-1]').attr('selected','selected');
		$('#turno option[value=-1]').attr('selected','selected');
		
		$('#dataInicio').val('');
		$('#dataFim').val('');

		$('#horaInicio').val('');
		$('#horaFim').val('');
	}
	
	function alterarEquipeTurno(idEquipeTurno) {
		
		if($('#salvar_'+idEquipeTurno).hasClass('btn-outline-dark')) {
			return false;
		}
		
		payload = {
			horarioInicio: $('#horarioInicio_'+idEquipeTurno).val(),
			horarioFim: $('#horarioFim_'+idEquipeTurno).val(),
			veiculo: $('#veiculoAlterar_'+idEquipeTurno).val(),
			chavePrimaria: idEquipeTurno
		}
		
		console.log(payload);
		
		if (!payload.horarioInicio) {
			$('#horarioInicio_'+idEquipeTurno).addClass('is-invalid');
			$('#erro_horaInicio_'+idEquipeTurno).text('Data de leitura � obrigat�rio');
			return false
		};
		$('#horarioInicio_'+idEquipeTurno).removeClass('is-invalid');
		
		if (!payload.horarioFim) {
			$('#horarioFim_'+idEquipeTurno).addClass('is-invalid');
			$('#erro_horarioFim_'+idEquipeTurno).text('Valor da leitura � obrigat�rio');
			return false
		};
		
		
		$('#horarioFim_'+idEquipeTurno).removeClass('is-invalid');
		$.ajax({
			type: 'POST',
			url: '/ggas/alterarEquipeTurnoAJAX',
			data: JSON.stringify(payload),
			success: function(data) {
				try {
					if (data.erro) {
						$('#salvar_'+idEquipeTurno).removeClass('btn-outline-primary');
						$('#salvar_'+idEquipeTurno).addClass('btn-outline-danger');
						$('#erro_equipeturno_'+idEquipeTurno).removeClass('hide');
						$('#erro_equipeturno_'+idLeituraMovimento).text(data.erro);
						return false;
					}
					$('#salvar_'+idEquipeTurno).removeClass('btn-outline-primary');
					$('#salvar_'+idEquipeTurno).addClass('btn-outline-success');
				} catch (e) {
					$('#salvar_'+idEquipeTurno).removeClass('btn-outline-primary');
					$('#salvar_'+idEquipeTurno).addClass('btn-outline-danger');
					$('#erro_equipeturno_'+idEquipeTurno).removeClass('hide');
					$('#erro_equipeturno_'+idEquipeTurno).text('Ocorreu um erro ao tentar salvar a equipe turno');
				}
			},
			beforeSend: function(jqXHR, settings) {
				$('#erro_equipeturno_'+idEquipeTurno).addClass('hide');
				$('#salvar_'+idEquipeTurno).removeClass('btn-outline-danger');
				$('#wait_'+idEquipeTurno).removeClass('hide');
				$('#salvar_'+idEquipeTurno).addClass('hide');
			},
			complete: function(data) {
				$('#wait_'+idEquipeTurno).addClass('hide');
				$('#salvar_'+idEquipeTurno).removeClass('hide');
			},
			contentType: 'application/json'
		});
		return true;
	}
	
	function carregarFuncionarios() {
		
		var rowCount = $("#listaFuncionarios tr").length; 
		
		if(rowCount > 0) {
			$('#listaFuncionarios').DataTable().clear();
			$('#listaFuncionarios').DataTable().destroy();
			$('#listaFuncionarios').empty();
		}
		
		var idEquipe = $("#equipe").val();
	     if (idEquipe != "-1") {
	             AjaxService.obterFuncionariosPorEquipe(idEquipe, 
	                  function(funcionario) {
	            	 		 var funcionarios = [];
	            	 		 var i = 1;
	                         for (key in funcionario){
	                        	 
	                     		var selectVeiculos = '<select name="veiculo" id="veiculo_'+key+'"' +
														'class="form-control form-control-sm">' +
															'<option value="-1">Selecione</option>' +
															'<c:forEach items="${listaVeiculos}" var="veiculo">' +
																'<option value="<c:out value="${veiculo.chavePrimaria}"/>">' +
																	'<c:out value="${veiculo.modelo}-${veiculo.placa}" />' +
																'</option>' +
															'</c:forEach>' +
														'</select>'	                        	 
				                var checkbox = '<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">' +
				                                   '<input id="chk_' + key + '" type="checkbox" ' +
				                                          'name="chavesPrimarias" class="custom-control-input" ' +
				                                          'value="' + funcionarios[key] + '"> ' +
				                                   '<label class="custom-control-label" for="chk_' + key + '"></label>' +
				                               '</div>';	                        	 
	                        	  var funcionarioArray = [];
	        			      	  funcionarioArray.push(checkbox);	
	                        	  funcionarioArray.push(''+i+'');
	                        	  funcionarioArray.push(''+funcionario[key] + ' <input type="hidden" name="funcionarios" id="funcionario_'+key+'" value="'+key+'"/>');
	                        	  funcionarioArray.push(selectVeiculos);
	                        	  funcionarioArray.push('<a href="#" class="deletarFuncionario"><img title="Excluir Cliente" alt="Excluir Cliente" src="<c:url value="/imagens/deletar_x.png"/>" border="0" /> </a>');
	                        	  funcionarios.push(funcionarioArray);
	                        	  i++;
	                          }
	                         criarTabelaFuncionarios(funcionarios);
	                         
	                   });
	     } 
	}	
	
	
	
	function criarTabelaFuncionarios(funcionarios){
	    var checkbox = '<div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">' +
        '<input type="checkbox" class="custom-control-input" id="checkAll">' +
        '<label class="custom-control-label" for="checkAll"></label>' +
    	'</div>';
		
		const table = $('#listaFuncionarios').DataTable ({
		    columns: [
		    	{ title: checkbox , className: 'text-center', orderable:false },
		        { title: 'Ordem' , className: 'text-center'},
		        { title: 'Funcion�rio', className: 'text-center' },
		        { title: 'Veiculo', className: 'text-center' },
		        { title: 'A��o', className: 'text-center', visible:false },
		    ],
		    data: funcionarios,
		    "searching": false,
	        "info": false,
	        "lengthChange": false,
	        "iDisplayLength": 15,
	        "retrieve": true,
	       	"destroy" : true, 
		    rowReorder: {
		        selector: 'td:nth-child(2)'
		    },
		    columnDefs: [{
		        targets: 1,
		        visible: true
		      }]
		});
		
		$("#listaFuncionarios thead").addClass("thead-ggas-bootstrap");

	}
	
	function salvarEquipeTurno() {
		submeter("equipeTurnoForm", "salvarEquipeTurno");
	}
	
	$(document).on("click", ".deletarFuncionario", function() {
		
		var rowCount = $("#listaFuncionarios tr").length; 
		if(rowCount > 2) {
// 			  var table = $('#listaFuncionarios').DataTable ()
// 				    .row($(this).parents('tr'))
// 				    .remove()
// 				    .rows().every(function(rowidx) {
// 				        var data = this.data();
// 				        data[0] = rowidx + 1;
// 				        this.data(data);
// 				        })
// 				        .draw(false);
	        var table = $('#listaFuncionarios').DataTable()
            .row($(this).parents('tr'))
            .remove()
            .draw();

	        table.rows().every(function(rowIdx, tableLoop, rowLoop) {
	            var data = this.data();
	            data[1] = rowIdx + 1; 
	            this.data(data);
	        });
		} else {
			alert("� necess�rio haver pelo menos 1 funcion�rio!");
		}
	});
	
	function validarDiasSelecionados() {
		if($('#weekdays').selectedIndexes().length > 0) {
			return true;
		}

		alert("Selecione pelo menos um dia da semana!");
		return false;
	}
	
	$(function(){

		var dias = "<c:out value="${diasSemana}"/>";

		if(!dias) {
			dias = [1,2,3,4,5];
		} 
		
		$('#weekdays').weekdays({
			selectedIndexes: dias,
			days: ["Domingo", "Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado"]
		});
	});
	
	function cancelar() {
		submeter("equipeTurnoForm", "exibirPesquisaEquipeTurno");
	}
	
	
	function adicionarNovosHorarioClique() {
		var i = $('#horariosEscala .horarioEscala').size();
		
        if (i < 3) {
        	adicionarNovosHorarios(i)
        	i++;
        }
	}
	
	function removerNovosHorariosClique(element) {
		
		var idRemocao = element.id.split('_')[1];
		
		var adicionar =	'<input ' +
		    'id="adicionar_'+(idRemocao-1)+'"' +
			'type="button"' +
			'onClick="adicionarNovosHorarioClique();"' +
			'class="botaoAdicionarNovoHorario botaoAdicionar"' +
			'title="Adicionar" />';
			
		var remover = '<input ' +
			'type="button" ' +
			'id="remover_'+(idRemocao-1)+'"' +
			'onClick="removerNovosHorariosClique(this);"' +
			'class="botaoRemoverNovoHorario botaoRemover" ' +
			'title="Remover" />';
			

		
        $(element).parents('#escala_'+idRemocao).remove();
		
		var divEscala = $('#escala_'+ (idRemocao - 1));
		
		$(adicionar).appendTo(divEscala);	
        	
        if( (idRemocao - 1) > 0 ) {
    	 $(remover).appendTo(divEscala);	
        }
        
    	estilizarBotaoAdicionarRemoverHorario();
	}
	
	
	
	function adicionarNovosHorarios(i) {
		
        var horariosInicio = [];
        var horariosFim = [];
        var j = 0;  
        <c:forEach items="${equipeTurno.horaInicio}" var="horaInicio">
        	horariosInicio[j] = '<c:out value='${horaInicio}'/>';     
            j++;
        </c:forEach>
        
        j=0;
        <c:forEach items="${equipeTurno.horaFim}" var="horaFim">
    	horariosFim[j] = '<c:out value='${horaFim}'/>';     
        j++;
    	</c:forEach>
    	
		var divHorarios = $('#horariosEscala');
 		var divNovoHorario = '<div style="margin-top:10px" id="escala_'+i+'" class="horarioEscala input-group input-group-sm col-sm-12 pl-1 pr-0"' +
								'style="padding-left: 0px !important;">' +
								'<input class="form-control form-control-sm periodoEscala"' +
									'type="text" id="horaInicio_'+i+'" name="horaInicio" maxlength="10"' +
									'value="'+horariosInicio[i]+'">' +
								'<div class="input-group-prepend">' +
									'<span class="input-group-text">at�</span>' +
								'</div>' +
								'<input class="form-control form-control-sm periodoEscala"'	 +
									'type="text" id="horaFim_'+i+'" name="horaFim" maxlength="10"' +
									'value="'+horariosFim[i]+'">' +
								'<input ' +
								    'id="adicionar_'+i+'"' +
									'onClick="adicionarNovosHorarioClique();"' +
									'type="button"' +
									'class="botaoAdicionarNovoHorario botaoAdicionar"' +
									'title="Adicionar" />' +
								'<input ' +
									'type="button" ' +
									'onClick="removerNovosHorariosClique(this);"' +
									'id="remover_'+i+'"' +
									'class="botaoRemoverNovoHorario botaoRemover" ' +
									'title="Remover" />' +												
								'</div>';
								
    	
    	$(divNovoHorario).appendTo(divHorarios);
    	estilizarBotaoAdicionarRemoverHorario();
	    $(".periodoEscala").inputmask("99:99", {placeholder: "_", greedy: false});
        
        $("#adicionar_"+(i-1)).remove();
		$("#remover_"+(i-1)).remove();
		
        if(i == 2) {
			$("#adicionar_"+i).remove();
		}
        return false;
	}
	
	function habilitarBotaoAlteracao(element) {
		var idEquipeTurno = element.id.split('_')[1];
		$('#salvar_'+idEquipeTurno).removeClass('btn-outline-dark');
		$('#salvar_'+idEquipeTurno).removeClass('btn-outline-success');
		$('#salvar_'+idEquipeTurno).addClass('btn-outline-primary');
	}

	function excluirFuncionario(){
		
		var rowCount = 0;
		
		$('#listaFuncionarios tbody input[type="checkbox"]:checked').each(function(){
			rowCount++;
		});
		
		if(rowCount > 0) {
		    var dt = $('#listaFuncionarios').DataTable();
		    
		    $('#listaFuncionarios tbody input[type="checkbox"]:checked').each(function() {
		        var linha = $(this).closest('tr');
		        dt.row(linha).remove();
		    });
		    dt.draw(false);
		    $('#checkAll').prop('checked', false);
		}else{
			alert("� necess�rio haver pelo menos 1 funcion�rio selecionado!");
		}
	}
	

    $(document).on('change', '#checkAll', function() {
        var selecionarTodos = $(this).prop('checked');
        $('#listaFuncionarios').find('tbody').find('input[type="checkbox"]').prop('checked', selecionarTodos);
        
    });
    
    
</script>

<div class="bootstrap">

	<form:form action="gerarEquipeTurno" id="equipeTurnoForm"
		name="equipeTurnoForm" method="post"
		modelAttribute="FiltroEquipeTurnoDTO">

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Gerar Escala da Equipe</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para gerar
					a escala da equipe, clique em <b>Gerar</b> para visualizar a
					escala e depois <b>Salvar</b> para salvar a escala gerada.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">

						<div class="row mb-2">

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
	                                    <label>Per�odo da Escala:</label>
										<div class="input-group input-group-sm col-sm-12 pl-1 pr-0" style="padding-left: 0px !important;">
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataInicio" name="dataInicio" maxlength="10" value="${equipeTurno.dataInicio}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm bootstrapDP" type="text" id="dataFim" name="dataFim" maxlength="10" value="${equipeTurno.dataFim}">
										</div>
									</div>
	                            </div>
							</div>
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="quadra">Equipe:<span class="text-danger">*</span></label>
										<select name="equipe" id="equipe"
											class="form-control form-control-sm"
											onchange="carregarFuncionarios();">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaEquipe}" var="equipe">
												<option value="<c:out value="${equipe.chavePrimaria}"/>"
													<c:if test="${equipeTurno.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${equipe.nome}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							
							
							<div class="col-md-3">
								<div class="form-row">
									<label for="isConsiderarFeriado" class="col-md-12">Trabalhar no Feriado?</label>
									<div class="col-md-10">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="considerarFeriadoSim" name="isConsiderarFeriado" class="custom-control-input" value="true"
												   <c:if test="${equipeTurno.isConsiderarFeriado eq true}">checked="checked"</c:if>>
											<label class="custom-control-label" for="considerarFeriadoSim">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="considerarFeriadoNao" name="isConsiderarFeriado" class="custom-control-input" value="false"
												   <c:if test="${equipeTurno.isConsiderarFeriado eq false or equipeTurno.isConsiderarFeriado eq null}">checked="checked"</c:if>>
											<label class="custom-control-label" for="considerarFeriadoNao">N�o</label>
										</div>
									</div>
								</div>															
							</div>
							
							<div class="col-md-3">
								<div class="form-row">
									<label for="isConsiderarFeriado" class="col-md-12">Escala � Sobreaviso?</label>
									<div class="col-md-10">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="escalaSobreavisoSim" name="isEscalaSobreaviso" class="custom-control-input" value="true"
												   <c:if test="${equipeTurno.isEscalaSobreaviso eq true}">checked="checked"</c:if>>
											<label class="custom-control-label" for="escalaSobreavisoSim">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="escalaSobreavisoNao" name="isEscalaSobreaviso" class="custom-control-input" value="false"
												   <c:if test="${equipeTurno.isEscalaSobreaviso eq false or equipeTurno.isEscalaSobreaviso eq null}">checked="checked"</c:if>>
											<label class="custom-control-label" for="escalaSobreavisoNao">N�o</label>
										</div>
									</div>
								</div>															
							</div>	

							<div class="col-md-6">
								<div class="form-row">
										<label>Hor�rio da Escala:<span class="text-danger">*</span></label>
									<div id="horariosEscala" class="col-md-12">
										<div id="escala_0" class="horarioEscala input-group input-group-sm col-sm-12 pl-1 pr-0"
											style="padding-left: 0px !important;">
											<input class="form-control form-control-sm periodoEscala"
												type="text" id="horaInicio_0" name="horaInicio" maxlength="10"
												value="${equipeTurno.horaInicio[0]}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm periodoEscala"
												type="text" id="horaFim_0" name="horaFim" maxlength="10"
												value="${equipeTurno.horaFim[0]}">
											<input 
												type="button" 
												id="adicionar_0"
												onclick="adicionarNovosHorarioClique();"
												class="botaoAdicionarNovoHorario botaoAdicionar"
												title="Adicionar" />																							
										</div>
									</div>
								</div>
							</div>												
							
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label>Dias para execu��o do Servi�o:<span
											class="text-danger">*</span></label> <br />
										<br /> <input type="hidden" name="diasSemana" id="diasSemana" />
										<div class="form-row">
											<div class="col-md-12" id="weekdays"></div>
										</div>
									</div>
								</div>
							</div>
							
							<div id="divListaFuncionarios" style="margin-top:30px;" class="table-responsive">
								<table class="table table-bordered table-striped table-hover" style="width:100%;" id="listaFuncionarios"> </table>
							</div>
						</div>

						<br />
						<div class="row mt-3">
							<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
								type="button" onclick="cancelar();">
								<i class="fa fa-times"></i> Cancelar
							</button>						
							<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
								type="button" onclick="excluirFuncionario();">
								<i class="fa fa-times"></i> Excluir
							</button>						
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoGerar"
									type="button" onclick="gerarEquipeTurno();">Gerar</button>
								<button class="btn btn-secondary btn-sm" name="botaoLimpar"
									id="botaoLimpar" value="limparFormulario" type="button"
									onclick="limparFormulario();">
									<i class="far fa-trash-alt"></i> Limpar
								</button>
							</div>
						</div>


						<c:if test="${listaEquipeTurno ne null}">
							<br>
							<hr class="linhaSeparadoraPesquisa" />
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover"
									id="equipeTurnoTabela">
									<thead class="thead-ggas-bootstrap">
										<tr>
											<th scope="col" class="text-center">Dia</th>
											<th scope="col" class="text-center">Equipe</th>
											<th scope="col" class="text-center">Funcion�rio</th>
											<th scope="col" class="text-center">Ve�culo</th>
											<th scope="col" class="text-center">Turno</th>
											<th scope="col" class="text-center">Hor�rio Inicial</th>
											<th scope="col" class="text-center">Hor�rio FInal</th>
											<th scope="col" class="text-center"></th>
										</tr>
									</thead>
									<tbody>
										<c:set var="i" value="0" />
										<c:forEach items="${listaEquipeTurno}" var="equipeTurnoTabela">
											<tr id="equipeTurno_${i}">
												<td class="text-center" data-order="${equipeTurnoTabela.retornarTempoEmMilisegundos()}"><input name="chavesPrimarias"
													type="hidden" id="chavesPrimarias" value="${i}"> <fmt:formatDate
														value="${equipeTurnoTabela.dataInicio}"
														pattern="dd/MM/yyyy" /></td>
												<td class="text-center"><c:out
														value="${equipeTurnoTabela.equipe.nome}" /></td>
												<td class="text-right"><c:out
														value="${equipeTurnoTabela.funcionario.nome}" /></td>
												<td class="text-center">
													<select name="veiculo"
														id="veiculoAlterar_${i}"
														onchange="habilitarBotaoAlteracao(this);"
														class="form-control form-control-sm">
															<option value="-1">Selecione</option>
															<c:forEach items="${listaVeiculos}" var="veiculo">
																<option value="<c:out value="${veiculo.chavePrimaria}"/>"
																   <c:if test="${equipeTurnoTabela.veiculo.chavePrimaria == veiculo.chavePrimaria}">selected="selected"</c:if>>
																	<c:out value="${veiculo.modelo}-${veiculo.placa}" />
																</option>
															</c:forEach>
													</select> 
												<td class="text-right"><c:out
														value="${equipeTurnoTabela.turno.descricao}" /></td>
												<td class="text-center">
												<div class="invalid-feedback" id="erro_horaInicio_${i}"></div>
												<input style="width: 120px"
													onkeypress="habilitarBotaoAlteracao(this);"
													class="form-control periodoEscala" type="text"
													id="horarioInicio_${i}" name="horarioInicio_${i}"
													maxlength="10" size="6"
													value="<fmt:formatDate value="${equipeTurnoTabela.dataInicio}" pattern="HH:mm"/>">
												</td>
												<td class="text-right">
												<div class="invalid-feedback" id="erro_horarioFim_${i}"></div>
												<input style="width: 120px"
													onkeypress="habilitarBotaoAlteracao(this);"
													class="form-control periodoEscala" type="text"
													id="horarioFim_${i}" name="horarioFim_${i}" maxlength="10"
													size="6"
													value="<fmt:formatDate value="${equipeTurnoTabela.dataFim}" pattern="HH:mm"/>">
												</td>
												<td class="text-center h5"><span role="button"
													id="salvar_${i}" class="btn btn-outline-dark"
													onclick="return alterarEquipeTurno(${i});"><i
														class="fa fa-check-square fa-3"></i></span> <span id="wait_${i}"
													class="hide"> <i class="fa fa-spinner fa-spin"
														aria-hidden="true"></i></span>
													<div class="alert alert-danger hide" role="alert"
														id="erro_equipeturno_${i}"></div></td>

											</tr>
											<c:set var="i" value="${i + 1}" />
										</c:forEach>
									</tbody>
								</table>
							</div>
							
						<br />
						<div class="row mt-3">
							<div class="col align-self-end text-right">
								<button class="btn btn-primary btn-sm" id="botaoSalvar"
									type="button" onclick="salvarEquipeTurno();">Salvar</button>
								</button>
							</div>
						</div>							
						</c:if>

					</div>

				</div>
			</div>
		</div>
	</form:form>

</div>