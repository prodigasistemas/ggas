<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<vacess:vacess param="exibirAlteracaoServicoAutorizacao">
    <button name="buttonAlterar" id="botaoAlterar"
            class="btn btn-primary btn-sm mb-1 mr-1 float-right"
            type="button"
            onclick="exibirAlterarServicoAutorizacao();" />
        <i class="fa fa-pencil-alt"></i> Alterar
    </button>
</vacess:vacess>
<vacess:vacess param="alterarServicoAutorizacaoEmExecucao">
    <button name="buttonEmExecucao" id="botaoEmExecucao"
            class="btn btn-primary btn-sm mb-1 mr-1 float-right"
            type="button"
            onclick="alterarServicoAutorizacaoEmExecucao();">
        <i class="fa fa-cog fa-spin"></i> Em Execu��o
    </button>
</vacess:vacess>
<vacess:vacess param="exibirExecutarServicoAutorizacao">
    <button name="buttonCopiar" id="botaoExecutar"
            class="btn btn-primary btn-sm mb-1 mr-1 float-right"
            type="button"
            onclick="exibirExecutarServicoAutorizacao();">
        <i class="fa fa-play"></i> Executar
    </button>
</vacess:vacess>
<vacess:vacess param="exibirRemanejarServicoAutorizacao">
    <button name="buttonTramitar" id="botaoRemanejar"
            class="btn btn-primary btn-sm mb-1 mr-1 float-right"
            type="button"
            onclick="exibirRemanejarServicoAutorizacao();">
        <i class="fa fa-random"></i> Remanejar
    </button>
</vacess:vacess>
<vacess:vacess param="exibirEncerrarServicoAutorizacao">
    <button name="buttonReiterar" id="botaoEncerrar"
            class="btn btn-primary btn-sm mb-1 mr-1 float-right"
            type="button"
            onclick="exibirEncerrarServicoAutorizacao();">
        <i class="far fa-check-circle"></i> Encerrar
    </button>
</vacess:vacess>
<vacess:vacess param="imprimirServicoAutorizacao">
    <button name="buttonImprimir" id="buttonImprimir"
            class="btn btn-primary btn-sm mb-1 mr-1 float-right"
            type="button"
            onclick="imprimirServicoAutorizacao();">
        <i class="fa fa-print"></i> Imprimir
    </button>
</vacess:vacess>
