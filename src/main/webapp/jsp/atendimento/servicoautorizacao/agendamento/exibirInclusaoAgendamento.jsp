<!--
 Copyright (C) <2011> GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

 Este programa  um software livre; voc pode redistribu-lo e/ou
 modific-lo sob os termos de Licena Pblica Geral GNU, conforme
 publicada pela Free Software Foundation; verso 2 da Licena.

 O GGAS  distribudo na expectativa de ser til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
 COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
 Consulte a Licena Pblica Geral GNU para obter mais detalhes.

 Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
 junto com este programa; se no, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=iso-8859-1"%>



<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script>
	$(document)
			.ready(
					function() {
						
					    $.fn.dataTable.ext.type.order['hora-pre'] = function (data) {
					        var parts = data.split(':');
					        return parseInt(parts[0]) * 60 + parseInt(parts[1]);
					    };
						
						window.addEventListener('beforeunload', function(event) {
								if($('#servicoAutorizacao').val() == '') {
							        event.preventDefault();
							        event.returnValue = 'Necess�rio para alguns navegadores'; // Necess�rio para alguns navegadores
								}
						});

						$("#botaoAdicionarAutorizacao").attr("disabled",
								"disabled");
						$("#botaoImprimir").attr("disabled", "disabled");

						var exibeAlerta = "<c:out value="${exibeAlerta}"/>"
						var servicoTipo = "<c:out value="${servicoAutorizacaoVO.servicoTipo.chavePrimaria}"/>";
						if (exibeAlerta == "true"
								|| (servicoTipo != "" && servicoTipo != null
										&& servicoTipo != undefined && servicoTipo != -1)) {
							var data = "<c:out value="${servicoAutorizacaoAgenda.dataSelecionada}"/>";
							alteraServicoTipo(servicoTipo);
							if (data != "" && data != null && data != undefined) {
								alteraCorDataAtivada(data);
								carregarServicoAutorizacaoAgenda(servicoTipo,
										data);

							}
						} else {
							carregaCalendario();
						}

						var maxLength = 100;
						$('#servicoTipo > option').text(function(i, text) {
							text = text.trim();
							if (text.length > maxLength) {
								return text.substr(0, maxLength) + '...';
							}
						});
						
						if($("#dataSelecionadaChamado").val() != '') {
							
							//$('select[id=servicoTipo] option:eq(1)').attr('selected', 'selected');
							//var servicoTipoValor = $('#servicoTipo').val();
							carregarAutorizacoesChamado(); 
							carregarServicoAutorizacaoAgenda(null, null); 
							//alteraServicoTipo(servicoTipoValor);
							
							var tabela = $(".ui-datepicker-calendar .celulaAtivaHover[data='" + retornarDataAtualFormatada() + "']");

							if(tabela.length != 1) {
								tabela = $(".ui-datepicker-calendar .celulaAtivaHover");
							}
							
							selecionaData(tabela[0]);
							
						}
						
					});
	
	function mostrarAvisoPersonalizado() {
	    return confirm("Voc� tem certeza que quer sair? As mudan�as n�o salvas ser�o perdidas.");
	}

	/***************** MTODOS PARA gridServicoAutorizacaoAgendamentos *****************/
	function atualizarHoraAgendamento(chavePrimaria, horario) {
		if (horario > "23:59") {
			alert("Hor�rio inv�lido!");
			return;
		}
		var servicoTipo = $("#servicoTipo").val();
		var dataSelecionada = $("#dataSelecionada").val();
		carregarFragmento('gridServicoAutorizacaoAgendamentos',
				"alterarHorario?servicoTipo=" + servicoTipo
						+ "&dataSelecionada=" + dataSelecionada
						+ "&chavePrimaria=" + chavePrimaria + "&horario="
						+ horario);
		carregarServicoAutorizacaoAgenda();
	}
	function confirmar(chavePrimaria) {
		var servicoTipo = $("#servicoTipo").val();
		var dataSelecionada = $("#dataSelecionada").val();
		$("#gridServicoAutorizacaoAgendamentos").load(
				"confirmar?servicoTipo=" + servicoTipo + "&dataSelecionada="
						+ dataSelecionada + "&chavePrimaria=" + chavePrimaria, function() {
							carregarServicoAutorizacaoAgenda();
						});
	}
	function desconfirmar(chavePrimaria) {
		var servicoTipo = $("#servicoTipo").val();
		var dataSelecionada = $("#dataSelecionada").val();
		$("#gridServicoAutorizacaoAgendamentos").load(
				"desconfirmar?servicoTipo=" + servicoTipo + "&dataSelecionada="
						+ dataSelecionada + "&chavePrimaria=" + chavePrimaria, function() {
							carregarServicoAutorizacaoAgenda();
						});
	}

	
	function incluirServicoAutorizacaoAgenda() {

		var turno = $("#turno").val();

		if (turno != -1) {
			if (!confirm('Deseja incluir o agendamento?')) {
				return;
			}
		} else {
			alert('Turno n�o cadastrado para o tipo servi�o selecionado!');
			return false;

		}
		var turno = document.forms[0].turno.value;

		verificarSelecao();
		var data = $("#dataSelecionada").val();
		var turno = $("#turno").val();
		var servicoTipo = document.forms[0].servicoTipo.value;
		var cont = 0;
		var chavesPrimarias = [];
		$('input[name="chavesPrimarias"]:checked').each(function() {
			chavesPrimarias[cont] = this.value;
			cont++;

		});
		
		var equipe = $("#equipe").val();
		var isPossivelInclusao = true;
		
		exibirIndicador();
		var url = 'conferirPossibilidadeInclusaoAgendamentoChamado?servicoTipo='+servicoTipo+'&dataSelecionada='+data+'&chavesPrimarias='+chavesPrimarias+'&turno='+turno+'&equipe='+equipe;
		$.ajax({
		    url: url,
		    context: document.body,
		    async: true,
		    success: function(response) {
		        isPossivelInclusao = response.isPossivelInclusao;

		        if (isPossivelInclusao === false) {
		        	esconderIndicador();
		        	alert("N�o h� vagas nesse dia para o agendamento.");
		            return;
		        } 
		        
		        // Se for poss�vel incluir, realiza o segundo ajax
		        var urlAdicionar = 'adicionarServicoAutorizacaoAgendamento?servicoTipo='
		                            + servicoTipo + '&dataSelecionada=' + data
		                            + '&chavesPrimarias=' + chavesPrimarias + '&turno=' + turno;
		        $.ajax({
		            url: urlAdicionar,
		            context: document.body
		        }).done(function() {
		        	esconderIndicador();
		            carregarAutorizacoesChamado();
		            agendamentoInserido($("#dataSelecionada").val());
		            
		        });
		    },
		    error: function() {
		    	esconderIndicador();
		    }
		});

	}

	function quantidadeSelecionados() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.chavesPrimarias != undefined) {
			var total = form.chavesPrimarias.length;
			if (total != undefined) {
				for (var i = 0; i < total; i++) {
					if (form.chavesPrimarias[i].checked == true) {
						flag++;
					}
				}
			} else {
				if (form.chavesPrimarias.checked == true) {
					flag++;
				}
			}
			if (flag <= 0) {
				return 0;
			}
		} else {
			return 0;
		}
		return flag;
	}
	

	function listarTurnos() {
		var servicoTipo = $("#servicoTipo").val();
		var dataSelecionada = $("#dataSelecionada").val();
		//if (servicoTipo != -1) {
			$("#divTurnos").load(
					'carregarTurnos?servicoTipo=' + servicoTipo
							+ "&dataSelecionada=" + dataSelecionada,
					function() {
						var tabela = $(".celulaAtiva");
						
						if(tabela.length != 1) {
							tabela = $(".ui-datepicker-calendar .celulaAtivaHover");
						}
						
						console.log("Conferindo Data");
						if(dataMenor(tabela[0].getAttribute("data"))) {
							console.log("Data Menor");
							$("#botaoPesquisar").attr("disabled", "disabled");

						}
						
						$(
								'#turno option[value='
										+ $("#turnoPreferencial").val() + ']')
								.attr('selected', 'selected');
					});
		//}
	}

	function excluirAgendamento(chave) {

		var servicoTipo = $("#servicoTipo").val();
		var dataSelecionada = $("#dataSelecionada").val();
		var noCache = "noCache=" + new Date().getTime();

		var url = "removerAgendamento?servicoTipo=" + servicoTipo
				+ "&dataSelecionada=" + dataSelecionada + "&chaveAgendamento="
				+ chave + "&noCache";
		$.ajax({
			type : "GET",
			url : url,
			async : false,
		}).done(function() {
			carregarServicoAutorizacaoAgenda();
		}).responseText;

		carregaCalendario(dataSelecionada);
		var mes = dataSelecionada.substring(3, 5);
		var ano = dataSelecionada.substring(6, 10);
		//carregaQuantidadeAgendamentos(mes, ano);
		carregarAutorizacoesChamado();
		//$("td.celulaAtiva").removeClass("celulaAtiva");
		//$("#gridServicoAutorizacaoAgendamentos").hide();

	}

	function carregarServicoAutorizacaoAgenda(servicoTipo, dataSelecionada) {

		if (servicoTipo == null || servicoTipo == undefined
				|| servicoTipo == "") {
			servicoTipo = $("#servicoTipo").val();
		}
		if (dataSelecionada == null || dataSelecionada == undefined
				|| dataSelecionada == "") {
			dataSelecionada = $("#dataSelecionada").val();
		}

		var equipe = $("#equipe").val();

		$("#gridServicoAutorizacaoAgendamentos")
				.load(
						"consultarServicoAutorizacaoAgenda?servicoTipo="
								+ servicoTipo + "&dataSelecionada="
								+ dataSelecionada + "&equipe=" + equipe,
						function(responseTxt, statusTxt, xhr) {
							if ($("#listaServicoAutorizacaoPreenchida").val() == "true") {
								$("#botaoImprimir").removeAttr("disabled");
							} else {
								$("#botaoImprimir")
										.attr("disabled", "disabled");
							}

							iniciarDatatable('#servicoAutorizacaoAgenda', {
								columnDefs : [ {
										orderable: false,
										targets: 0
								    },
								    {
										type: 'hora',
										targets: 7,
						                render: function (data, type, row) {
						                    if (type === 'sort' || type === 'type') {
						                        const match = data.match(/value="(\d{2}:\d{2})"/);
						                        return match ? match[1] : '00:00';
						                    }
						                    return data;
						                }
								    },
						            {
						                targets: 8,
						                render: function (data, type, row) {
						                    if (type === 'sort' || type === 'type') {
						                        const tempDiv = document.createElement('div');
						                        tempDiv.innerHTML = data;
						                        const select = tempDiv.querySelector('select');
						                        return select ? select.options[select.selectedIndex].text.trim() : '';
						                    }
						                    return data
						                }
						            }								    
								 ]
							});
						});

	}
	/***********************************************************************************/


	function carregarAutorizacoesChamado() {
		var protocolo = document.forms[0].protocolo.value;
		var servicoTipo = document.forms[0].servicoTipo.value;
		var equipe = document.forms[0].equipe.value;
		var data = $("#dataSelecionada").val();
		
		$("#gridServicoAutorizacao").load(
				"consultarServicoAutorizacaoPorProtocolo?servicoTipo="
						+ servicoTipo + "&protocolo=" + protocolo + '&dataSelecionada='
						+ data, function() {
					listarTurnos();
				})
	}

	function exibirPopupPesquisarAutorizacaoServico() {
		if (!verificaAgendamentoDisponivel()) {
			alert("Agendamento n�o dispon�vel para esta data.");
			return;
		}
		var existeErro = verificaSeExisteAutorizacaoDeServico();
		if (existeErro == "true") {
			submeter('servicoAutorizacaoCalendarioForm',
					'exibeAlertNaoHaServicoAutorizacao');
		} else {
			var servicoTipo = document.forms[0].servicoTipo.value;
			var data = $("#dataSelecionada").val();
			var noCache = "noCache=" + new Date().getTime();
			var equipe = document.forms[0].equipe.value;
			if (data != "") {
				popup = window
						.open(
								'exibirPopupPesquisarAutorizacaoServico?servicoTipo='
										+ servicoTipo + '&dataSelecionada='
										+ data + "&noCache="
										+ new Date().getTime() + '&equipe='
										+ equipe,
								'popup',
								'height=750,width=800,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes ,modal=yes');
			} else {
				alert("Necess�rio selecionar uma data pra prosseguir!");
			}
		}
	}
	function verificaSeExisteAutorizacaoDeServico() {
		var existeErro = "false";
		servicoTipo = $("#servicoTipo").val();
		if (servicoTipo != -1) {
			AjaxService.existeAutorizacaoServico(servicoTipo, {
				callback : function(mapa) {
					existeErro = mapa['existeErro'];
				},
				async : false
			});
		}
		return existeErro;
	}
	function imprimir() {
		if ($("#dataSelecionada").val() != "") {
			submeter('servicoAutorizacaoCalendarioForm',
					'imprimirServicoAutorizacaoAgenda');
		} else {
			alert("Necess�rio selecionar a data!");
		}
	}

	/********************************** CALENDRIO **********************************/
	function carregaCalendario(data) {

		$("#calendarioDatePicker").datepicker("destroy");
		$("#calendarioDatePicker").datepicker({
			changeYear : true,
			changeMonth : true,
			dateFormat : 'dd/mm/yy',
			defaultDate : data,
	        beforeShowDay: function(date) {
	            var today = new Date();
	            today.setHours(0,0,0,0); // Reseta a hora para meia-noite para compara��o exata de datas
	            if (date < today) {
	                return [true, "dia-passado"];
	            }
	            return [true, ""];
	        }
		})
		$("#calendarioDatePicker").datepicker("refresh");

		$("#calendarioDatePicker td:has(a)").addClass("celulaAtivaHover");
		$("#calendarioDatePicker td:has(a)").removeClass("ui-state-disabled");
		$("#calendarioDatePicker td:has(a)").attr("onclick",
				"selecionaData(this);irAteTabela();");
		$("#calendarioDatePicker td:has(a)").addClass("selecionavel");
		$("#calendarioDatePicker .ui-datepicker-month").removeAttr("disabled");
		$("#calendarioDatePicker .ui-datepicker-year").removeAttr("disabled");

		$("#calendarioDatePicker td a").removeClass("ui-state-highlight");
		$("#calendarioDatePicker td a").removeClass("ui-state-active");
		$("#calendarioDatePicker td a").removeClass("ui-state-hover");
		$("#calendarioDatePicker td a").attr("href", "javascript:void(0)");
		$("#calendarioDatePicker td:has(a)").unbind("click");
		$("#calendarioDatePicker select").unbind("change");
		$("#calendarioDatePicker .ui-datepicker-month").attr("onchange",
				"alteraMes(this);");
		$("#calendarioDatePicker .ui-datepicker-year").attr("onchange",
				"alteraAno(this);");
		$("#calendarioDatePicker td:has(span)").addClass("dias");
		$("#calendarioDatePicker td:has(a)").addClass("dias");
		$(".ui-datepicker-prev").hide();
		$(".ui-datepicker-next").hide();
		montaData();
		alteraCorDataAtivada();
		
		var equipe = $("#equipe").val();
		
		if(equipe != -1) {
			var ano = parseInt($("#calendarioDatePicker .ui-datepicker-year").val());
			var mes = parseInt($("#calendarioDatePicker .ui-datepicker-month")
					.val()) + 1;
			carregaEscala(mes, ano, equipe);
		}

	}
	function alteraCorDataAtivada(dataSelecionada) {
		var td = $("#calendarioDatePicker td:has(a)");
		if (dataSelecionada == null || dataSelecionada == undefined
				|| dataSelecionada == "") {
			dataSelecionada = $("#dataSelecionada").val();
		}

		for (i = 0; i < td.length; i++) {
			var dataTd = td[i].getAttribute("data");

			if (dataTd == dataSelecionada) {
				td[i].className += " celulaAtiva";
			} else {
				td[i].classList.remove("celulaAtiva");
			}
		}
	}
	function selecionaData(element) {
		var dataSelecionada = null;
		var data = element.getAttribute("data");
		
		if(dataMenor(data)){
			$("#botaoLimpar").prop("disabled", true);
			$("#botaoPesquisar").prop("disabled", true);
		} else {
			$("#botaoLimpar").prop("disabled", false);
			$("#botaoPesquisar").prop("disabled", false);
		}

		if ($("#calendarioDatePicker td").closest('.celulaAtiva').length > 0) {
			dataSelecionada = $("#calendarioDatePicker td").closest(
					'.celulaAtiva')[0].getAttribute("data");
		}
		if (limparData(dataSelecionada, element.getAttribute("data"))) {
			data = "";
		}

		ativaData(data);
		$("#gridServicoAutorizacaoAgendamentos").show();
	}
	function agendamentoInserido(dataSelecionada) {
		ativaData(dataSelecionada);
		$("#botaoImprimir").removeAttr("disabled");
	}
	function ativaData(dataSelecionada) {
		$("#dataSelecionada").val(dataSelecionada);
		carregaCalendario(dataSelecionada);
		//carregaQuantidadeAgendamentos(dataSelecionada.substring(3, 5),
		//	dataSelecionada.substring(6, 10));
		carregarServicoAutorizacaoAgenda();
		verificaAgendamentoDisponivel();
		$("#botaoAdicionarAutorizacao").removeAttr("disabled");
	}
	function verificaAgendamentoDisponivel() {
		var agendamentosDisponiveis = $(".celulaAtiva div").text().substring(0,
				2);
		if (agendamentosDisponiveis == "00") {
			$("#agendamentoDisponivel").val(false);
			return true;
		} else {
			$("#agendamentoDisponivel").val(true);
			return true;
		}
	}
	function montaData() {
		var ano = $("#calendarioDatePicker td:has(a)").attr("data-year");
		var mes = acrescentaZeroAEsquerda(parseInt($(
				"#calendarioDatePicker td:has(a)").attr("data-month")) + 1);
		var td = $("#calendarioDatePicker td:has(a)");
		var tagFilha = td.children("a");
		for (i = 0; i < tagFilha.length; i++) {
			var dia = acrescentaZeroAEsquerda(parseInt(tagFilha[i].text));
			var data = dia + "/" + mes + "/" + ano;
			td[i].setAttribute("data", data);
		}
	}
	function alteraMes(element) {
		var mes = parseInt(element.value) + 1;
		var ano = parseInt($("#calendarioDatePicker .ui-datepicker-year").val());
		var data = "01/" + acrescentaZeroAEsquerda(mes) + "/" + ano;
		carregaCalendario(data);
		//carregaQuantidadeAgendamentos(mes, ano);
	}
	function alteraAno(element) {
		var ano = element.value;
		var mes = parseInt($("#calendarioDatePicker .ui-datepicker-month")
				.val()) + 1;
		var data = "01/" + acrescentaZeroAEsquerda(mes) + "/" + ano;
		carregaCalendario(data);
		if (new Date(ano, mes) > new Date()) {
			//carregaQuantidadeAgendamentos(mes, ano);
		} else {
			//carregaQuantidadeAgendamentos(new Date().getMonth() + 1, new Date()
			//	.getFullYear())
		}
	}
	function alteraServicoTipo(value) {
		carregaCalendario();

		var ano = parseInt($("#calendarioDatePicker .ui-datepicker-year").val());
		var mes = parseInt($("#calendarioDatePicker .ui-datepicker-month")
				.val()) + 1;
		//carregaQuantidadeAgendamentos(mes, ano);
		if ($("#dataSelecionada").val() != "") {
			carregarServicoAutorizacaoAgenda();
		}
		verificaAgendamentoDisponivel();
	}
	function carregaQuantidadeAgendamentos(mes, ano) {
		AjaxService
				.carregaListaAgendamentos(
						mes,
						ano,
						$("#servicoTipo").val(),
						{
							callback : function(listaAgendamentos) {
								var i = 0;
								for (agendamento in listaAgendamentos) {
									var manha = "00";
									var tarde = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
									var noite = "00";
									if (listaAgendamentos[agendamento]['manha'] != "null") {
										manha = acrescentaZeroAEsquerda(parseInt(listaAgendamentos[agendamento]['manha']));
									}
									if (listaAgendamentos[agendamento]['tarde'] != "null") {
										tarde = acrescentaZeroAEsquerda(parseInt(listaAgendamentos[agendamento]['tarde']));
									}
									if (listaAgendamentos[agendamento]['noite'] != "null") {
										noite = acrescentaZeroAEsquerda(parseInt(listaAgendamentos[agendamento]['noite']));
									}
									var dias = $("#calendarioDatePicker .dias");
									dias[i].innerHTML = dias[i].innerHTML
											+ "<div id='qtdAgendados'>" + manha
											+ "&nbsp;&nbsp;&nbsp; " + tarde
											+ "&nbsp;&nbsp;&nbsp;" + noite
											+ "</div>";
									i++;
								}
								var imagens = "<img id='disponivel' src='images/ico_disponivel.png' alt='disponivel' /><img id='preenchido' src='images/ico_preenchido.png' alt='preenchido' /><img id='total' src='images/ico_total.png' alt='total' />";
								$("#calendarioDatePicker td:has(a)").append(
										imagens);
								$("#calendarioDatePicker td:has(span)").append(
										imagens);
							},
							async : false
						});
	}
	function acrescentaZeroAEsquerda(valor) {
		if (valor < 10) {
			valor = "0" + valor;
		}
		return valor;
	}

	/********************************************************************************/
	function limparData(dataSelecionada, data) {

		if (dataSelecionada != null && data === dataSelecionada) {
			$("#calendarioDatePicker td").closest('.celulaAtiva')[0].classList
					.remove("celulaAtiva")
			$("#dataSelecionada").val("");
			carregarServicoAutorizacaoAgenda(null, null);
			return true;
		}
		return false;
	}

	function irAteTabela() {
		$('html, body').animate({
			scrollTop : $("#gridServicoAutorizacaoAgendamentos").offset().top
		}, 500);
	}


	function roteirizarServicoAutorizacao() {

		var servicoTipo = $("#servicoTipo").val();
		var dataSelecionada = $("#dataSelecionada").val();
		var noCache = "noCache=" + new Date().getTime();
		var equipe = $("#equipe").val()
		var cont = 0;
		var chavesPrimarias = [];
		var isSelecao = verificarSelecaoSemMensagemGeral();

		var tabela = $("#servicoAutorizacaoAgenda").DataTable();

		//var parametros = tabela.$('.confirmacao')
		
		var parametros = tabela.rows().nodes().to$().filter(function() {
		    var checkbox = $(this).find('td:eq(0) input[type="checkbox"]');
		    
		    if (checkbox.length === 0) {
		        return false;
		    }
		    
		    if (isSelecao) {
		        return checkbox.is(':checked');
		    }
		    
		    return true;
		}).map(function() {
		    return $(this).find('input.confirmacao')
		}).get();

		var form = $("#servicoAutorizacaoCalendarioForm");
		
		

		$.each(parametros, function() {
			if(this.val()) {
				chavesPrimarias[cont] = this.val();
				cont++;
			}
		});

		if (equipe == -1 || dataSelecionada == "") {
			alert("Equipe e Data necess�rio para Roteirizar!")
		} else {

			AjaxService
					.consultarEscalaDisponivel(
							dataSelecionada,
							equipe,
							function(escalaDisponivel) {
								if ((escalaDisponivel)) {
									exibirIndicador();
									var url = "roteirizarServicoAutorizacaoAgendamento?servicoTipo="
											+ servicoTipo
											+ "&dataSelecionada="
											+ dataSelecionada
											+ "&equipe="
											+ equipe
											+ '&chavesPrimarias='
											+ chavesPrimarias + "&noCache";
									$.ajax({
												type : "GET",
												url : url,
												async : true,
											})
											.done(
													function() {
														carregarServicoAutorizacaoAgenda();
														carregaCalendario(dataSelecionada);
														carregarAutorizacoesChamado();
														esconderIndicador();
														alert("Processo de Roteiriza��o Finalizado.");
													}).responseText;
								} else {
									alert("N�o h� escala dispon�vel para essa equipe!");
								}
							});
		}
	}
		
		
		function carregarInformacoesEquipe() {
			
			carregaCalendario();
			carregarAutorizacoesChamado(); 
			carregarServicoAutorizacaoAgenda(null, null);
			
		}
		
		function carregaEscala(mes, ano, equipe) {
			
			AjaxService.carregaListaEscala(mes, ano, equipe, {
				callback : function(listaEscala) {
					var tabela = $(".ui-datepicker-calendar .celulaAtivaHover");
					var i = 0;
					
					for(var i = 0 ; i < tabela.length; i++) {
					
						if(listaEscala[tabela[i].getAttribute('data')] != true) {
							tabela[i].removeAttribute('onclick')
							tabela[i].removeAttribute('data-event')
							tabela[i].removeAttribute('data-month')
							tabela[i].removeAttribute('data-year')
							tabela[i].removeAttribute('data')
							tabela[i].removeAttribute('data-handler')

							tabela[i].classList.remove('ui-datepicker-days-cell-over')
							tabela[i].classList.remove('ui-datepicker-today')
							tabela[i].classList.remove('celulaAtivaHover')
							tabela[i].classList.remove('selecionavel')


							tabela[i].classList.add('ui-datepicker-unselectable')
							tabela[i].classList.add('ui-state-disabled')

						}
				
					}
				}, async : false});
			
		}	

		function confirmarAgendamento() {
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('Deseja confirmar os agendamentos Selecionados?');
				if (retorno == true) {
					var form = document.forms[0];
					var total = form.chavesPrimarias.length;
					for (var i = 0; i< total; i++) {
						if(form.chavesPrimarias[i].checked == true){
							confirmar(form.chavesPrimarias[i].value)
							form.chavesPrimarias[i].checked = false
						}
		            }
				}
			}
		}
		
		
		function desconfirmarAgendamento() {
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('Deseja desconfirmar os agendamentos Selecionados?');
				if (retorno == true) {
					var form = document.forms[0];
					var total = form.chavesPrimarias.length;
					for (var i = 0; i< total; i++) {
						if(form.chavesPrimarias[i].checked == true){
							desconfirmar(form.chavesPrimarias[i].value)
							form.chavesPrimarias[i].checked = false
						}
		            }
				}
			}
		}
		
		function dataMenor(dataSelecionada) {
			 const [dia, mes, ano] = dataSelecionada.split('/');

			const dataConvertida = new Date(ano, mes - 1, dia);

			const dataAtual = new Date();
			dataAtual.setHours(0, 0, 0, 0);


			return dataConvertida < dataAtual;
		}
		
		function exibirChamado(chaveChamado) {
			document.getElementById('chavePrimaria').value = chaveChamado;
			submeter("servicoAutorizacaoCalendarioForm", "exibirDetalhamentoChamado", "_blank");

		}
		
		function exibirAutorizacaoServico(chaveAutorizacaoServico) {
			document.getElementById('chavePrimaria').value = chaveAutorizacaoServico;
			submeter("servicoAutorizacaoCalendarioForm", "exibirDetalhamentoServicoAutorizacao", "_blank");
		}
		
		function alterarFuncionarioAgenda(elem) {
			var idAgendamento = elem.id.split("_")[1];
			var idNovoFuncionario = elem.value;
			
			exibirIndicador();
			AjaxService.alterarFuncionarioAgenda(idAgendamento, idNovoFuncionario, {
				callback : function(retorno) {
					esconderIndicador();
				},
				async : false
			});
			
		}
		
		function retornarDataAtualFormatada() {
			var dataAtual = new Date();
			var dia = String(dataAtual.getDate()).padStart(2, '0');
			var mes = String(dataAtual.getMonth() + 1).padStart(2, '0');
			var ano = dataAtual.getFullYear();
			var dataFormatada = dia + '/' + mes + '/' + ano;
			
			return dataFormatada;
		}
		
</script>

<style>


/* Envolve os meses e anos em um flexbox para exibir em linha */
div#calendarioDatePicker .ui-datepicker-title {
    display: flex;
    align-items: center; /* Alinha os elementos verticalmente ao centro */
    justify-content: flex-start; /* Mant�m o alinhamento no in�cio da linha */
    gap: 10px; /* Espa�amento entre os elementos */
}

/* Estilos opcionais para garantir que os meses e anos fiquem com uma largura apropriada */
div#calendarioDatePicker .ui-datepicker-month,
div#calendarioDatePicker .ui-datepicker-year {
    width: auto;
    margin: 0;
}

div#calendarioDatePicker .ui-widget-content a {
	color: #222222 !important
}

div#calendarioDatePicker .ui-datepicker .ui-datepicker-calendar {
	height: 100%;
}

div#calendarioDatePicker {
	float: left;
	height: 520px;
}

div#calendarioDatePicker .ui-datepicker {
	width: 500px;
	height: 350px;
	border-style: inherit;
}

div#calendarioDatePicker .ui-state-default {
	font-weight: bold;
	font-size: 18px;
}

div#calendarioDatePicker .dias {
	width: 60px;
	height: 70px !important;
	background: rgb(159, 197, 248);
	border: 1px solid #fff;
}

div#calendarioDatePicker a {
	background: none;
	border: none;
	height: 25px;
	cursor: inherit;
}

div#calendarioDatePicker .celulaAtivaHover:hover {
	background: #ff9900 !important;
	border: 1px solid #fff;
}

div#calendarioDatePicker .celulaAtiva {
	width: 60px;
	height: 70px !important;
	background: #ff9900 !important;
	border: 1px solid #fff;
}

div#calendarioDatePicker span {
	background: none;
	border: none;
	height: 25px;
}

div#calendarioDatePicker .ui-datepicker-month {
	width: 210px !important;
	margin-right: 20px;
	margin-top: 15px;
}

div#calendarioDatePicker .ui-datepicker-year {
	width: 210px !important;
	margin-top: 15px;
}

div#calendarioDatePicker .selecionavel {
	cursor: pointer !important;
}

div#calendarioDatePicker .ui-datepicker-other-month {
	background: white;
	!
	important;
}

#servicoAutorizacaoAgenda {
	overflow-x: auto;
}

.dia-passado {
    background: #a9a5a5 !important;  /* Altere o fundo se necess�rio */
    opacity: 0.5; /* Opcional: tornar os dias anteriores menos destacados */
}

</style>


<div class="bootstrap">
	<form:form action="exibirCalendario"
		id="servicoAutorizacaoCalendarioForm"
		name="servicoAutorizacaoCalendarioForm" method="post"
		modelAttribute="ServicoAutorizacaoAgendaPesquisaVO">
		<input type="hidden" id="isPossivelInclusao" value="${isPossivelInclusao}">
		<input type="hidden" id="agendamentoDisponivel"
			name="agendamentoDisponivel" />
		<input type="hidden" id="chavePrimaria" name="chavePrimaria"/>
		<input type="hidden" id="dataSelecionada" name="dataSelecionada"
			value="${servicoAutorizacaoAgendaPesquisaVO.dataSelecionada}" />
		<input type="hidden" id="protocolo" name="protocolo"
			value="${protocolo}" />	
		
		<input type="hidden" id="dataSelecionadaChamado"
			name="dataSelecionadaChamado" value="${ dataSelecionada }">
		<input type="hidden" id="turnoPreferencial" name="turnoPreferencial"
			value="${ turnoPreferencial.chavePrimaria }">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Agendamento dos Servi�os</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Selecione um tipo de servi�o
					para visualizar o agendamento das autoriza��es de servi�o. <span
						class="destaqueOrientacaoInicial">Incluir</span>
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">
						<div class="row mb-2">
							<div id="indicadorAgendamento"></div>
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="situacaoContrato">Tipo de Servi�o:<span
											class="text-danger">*</span></label> <select name="servicoTipo"
											id="servicoTipo" class="form-control form-control-sm"
											onchange="carregarAutorizacoesChamado(); carregarServicoAutorizacaoAgenda(null, null); alteraServicoTipo(this.value);">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaTipoServico}" var="servico">
												<option value="<c:out value="${servico.chavePrimaria}"/>"
													<c:if test="${servicoAutorizacaoVO.servicoTipo.chavePrimaria == servico.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${servico.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="equipe">Equipe:<span class="text-danger">*</span></label>
										<select name="equipe" id="equipe"
											class="form-control form-control-sm"
											onchange="carregarInformacoesEquipe();">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaEquipe}" var="equipe">
												<option value="<c:out value="${equipe.chavePrimaria}"/>"
													<c:if test="${servicoAutorizacaoVO.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${equipe.nome}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							<hr class="linhaSeparadora2" />

							<div id="gridServicoAutorizacao">
								<jsp:include
									page="/jsp/atendimento/servicoautorizacao/agendamento/gridServicoAutorizacaoAgendamentoPorChamado.jsp" />
							</div>
							
							
							<div
								class="col-sm-12 d-flex align-items-center justify-content-center">
								<div class="form-row">
									<div class="col-sm-12">
										<div id="calendarioDatePicker"></div>
									</div>
								</div>
							</div>
							
							<br>
							
							<c:if test="${protocolo ne null}">
								<div id="divTurnos">
									<jsp:include page="/jsp/atendimento/servicoautorizacao/agendamento/listaTurnos.jsp" />
								</div>	
							</c:if>		
							
							<br>				

						</div>
						
						<div id="gridServicoAutorizacaoAgendamentos">
							<jsp:include
								page="/jsp/atendimento/servicoautorizacao/agendamento/gridServicoAutorizacaoAgendamentos.jsp"></jsp:include>
						</div>


						<hr class="linhaSeparadora2" />
						
						<c:if test="${protocolo eq null}">
							<div class="row mt-3">
								<div class="col align-self-end text-right">
									<button class="btn btn-primary btn-sm" id="botaoConfirmar"
										type="button" onclick="confirmarAgendamento();">Confirmar</button>
									<button class="btn btn-primary btn-sm" id="botaoConfirmar"
										type="button" onclick="desconfirmarAgendamento();">Desconfirmar</button>																
									<button class="btn btn-primary btn-sm" id="botaoRoteirizar"
										type="button" onclick="roteirizarServicoAutorizacao();">Roteirizar</button>
									<button class="btn btn-primary btn-sm" id="botaoImprimir"
										type="button" onclick="imprimir();">Imprimir</button>
									<c:if test="${ exibirBotaoAdicionar eq true }">
										<button class="btn btn-primary btn-sm" name="botaoLimpar"
											id="botaoLimpar" value="limparFormulario" type="button"
											onclick="exibirPopupPesquisarAutorizacaoServico();">
											Adicionar Autoriza��o de Servi�o</button>
									</c:if>
									<!--  <button class="btn btn-primary btn-sm" id="botaoPesquisar"
										type="button"
										onclick="window.location.assign('exibirPesquisaChamado');">
										Voltar para tela de Chamado</button>
									<button class="btn btn-primary btn-sm" id="botaoPesquisar"
										type="button"
										onclick="window.location.assign('exibirPesquisaServicoAutorizacao');">
										Voltar para tela de Autoriza��o de Servi�o</button> -->
								</div>
							</div>
						</c:if>


					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>