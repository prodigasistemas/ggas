<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Pesquisar Autoriza��o de Servi�o</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">

$(document).ready(function(){
	$(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	$(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	
	
});

	function limparFormulario() {		
		limparFormularios(document.servicoAutorizacaoForm);
		document.forms['servicoAutorizacaoForm'].condominioImovel[2].checked = true;
		document.forms['servicoAutorizacaoForm'].servicoAtraso[2].checked = true;
	}
	
	
	animatedcollapse.addDiv('mostrarFormChamado', 'fade=0,speed=400,group=chamado,persist=1,hide=0');
	animatedcollapse.addDiv('mostrarFormImovel', 'fade=0,speed=400,group=imovel,persist=1,hide=0');
	animatedcollapse.addDiv('mostrarFormContrato', 'fade=0,speed=400,group=contrato,persist=1,hide=0');
	animatedcollapse.addDiv('mostrarFormPessoaFisica', 'fade=0,speed=400,group=pessoaFisica,persist=1,hide=0');
	animatedcollapse.addDiv('mostrarFormPessoaJuridica', 'fade=0,speed=400,group=pessoaJuridica,persist=1,hide=0');
	
	function incluirServicoAutorizacaoAgenda() {
		var turno = document.forms[0].turno.value;
		
		if($("#servicoTipo").val() == '') {
			$("#servicoTipo").val(-1);
		}
		
		//var quantidade = document.getElementById(turno).value;
		//if(quantidadeSelecionados() > quantidade) {
		//	alert('Para o turno selecionado s� h� mais ' + quantidade + ' agendamentos poss�veis. ');
		//	return false;
		//}
		var selecao = verificarSelecao();
		if(!selecao){
			return false;
		}
		var data = $("#dataSelecionada").val();
		var turno = document.forms[0].turno.value;
		var servicoTipo = document.forms[0].servicoTipo.value;
		var equipe = document.forms[0].equipe.value;
		var cont = 0;
		var chavesPrimarias = [];
		$('input[name="chavesPrimarias"]:checked').each(function() {
			 chavesPrimarias[cont] = this.value;
				cont++;
				
			});
		var url = 'adicionarServicoAutorizacaoAgendamento?servicoTipo='+servicoTipo+'&dataSelecionada='+data+'&chavesPrimarias='+chavesPrimarias+'&turno='+turno+'&equipe='+equipe;
		$.ajax({
			url: url,
			context: document.body,
			async: true
			}).done(function() {
				window.opener.agendamentoInserido($("#dataSelecionada").val());
				setInterval("window.close()", 100);
			});
		
	}
	
	function pesquisar() {
		
		if($("#servicoTipo").val() == '') {
			$("#servicoTipo").val(-1);
		}
		
		submeter('servicoAutorizacaoForm','pesquisarServicoAutorizacaoPopup');
	}
	
	function quantidadeSelecionados() {
	var flag = 0;
	var form = document.forms[0];
	
	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					flag++;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				flag++;
			}
		}
	
		if (flag <= 0) {
			return 0;
		}
		
	} else {
		return 0;
	}
	
	return flag;
	}
</script>

<form:form action="pesquisarServicoAutorizacaoPopup" id="servicoAutorizacaoForm" name="servicoAutorizacaoForm" method="post" modelAttribute="ServicoAutorizacaoAgendaPesquisaVO">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="dataSelecionada" type="hidden" id="dataSelecionada" value="${servicoAutorizacaoAgenda.dataSelecionada}">
	<input name="servicoTipo" type="hidden" id="servicoTipo" value="${servicoAutorizacaoAgenda.servicoTipo.chavePrimaria}">
	
	
<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset style="padding-bottom: 20px">
	<fieldset class="colunaServicoAutorizacao">
				<label class="rotulo rotulo2Linhas" style="margin-left: 6px;" id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" >Data Previs�o <br/>de Encerramento:</label>
				<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataPrevisaoInicio" name="dataPrevisaoInicio" maxlength="10" value="${servicoAutorizacaoVO.dataPrevisaoInicio}" onblur="validaData(this);">
				<label class="rotuloEntreCampos" id="rotuloEntreCamposIntervaloCadastroDocumentos" for="dataCadastroDocumentosFinal">a</label>
				<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataPrevisaoFim" name="dataPrevisaoFim" maxlength="10" value="${servicoAutorizacaoVO.dataPrevisaoFim}" onblur="validaData(this);">
				<br />
				
			<label class="rotulo rotulo2Linhas" style="margin-left: 34px" id="rotuloNomeEmpresa" for="nomeCompletoCliente" >N�mero do<br/>protocolo<br/> do chamado:</label>
			<input class="campoTexto" type="text" style="margin-top: 8px;" name="numeroProtocoloChamado" id="numeroProtocoloChamado" maxlength="20" size="20" value="${servicoAutorizacaoVO.numeroProtocoloChamado}" onkeypress="return formatarCampoInteiro(event,10);"/><br />
	
   </fieldset>
    <fieldset id="pesquisaMedicaoCol2" class="colunaFinal">
			<label class="rotulo" id="rotuloMarca" for="equipe">Equipe:</label>
			<select name="equipe" id="equipe" class="campoSelect">
				<c:forEach items="${listaEquipe}" var="equipe">
					<option value="<c:out value="${equipe.chavePrimaria}"/>"
						<c:choose>
							<c:when test="${servicoAutorizacaoVO.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:when>
							<c:when test="${equipePrioritaria ne null and equipePrioritaria.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:when>
						</c:choose>>
						<c:out value="${equipe.nome}"/>
					</option>		
			    </c:forEach>
			</select><br />	
		</fieldset>
</fieldset>
</fieldset>
</fieldset>




<fieldset class="conteinerBotoesPesquisarDir">
<%-- 		<vacess:vacess param="pesquisarServicoAutorizacaoAgendamento"> --%>
	   		<input name="Button" type="button" class="bottonRightCol2 botaoGrande1 botaoIncluir" id="botaoPesquisar" value="Pesquisar" onclick="pesquisar();">
<%-- 	   	</vacess:vacess>		 --%>
		<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
</fieldset>

<c:if test="${listaServicoAutorizacao ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaServicoAutorizacao" sort="list" id="lista" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarServicoAutorizacaoPopup">
	        <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${lista.chavePrimaria}">
	        </display:column>	    
	        <display:column sortable="true" title="C�digo" sortProperty="chavePrimaria" style="text-align: center;">
	        	<a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.chavePrimaria}"/>
	            </a>
	        </display:column>	        
	        <display:column sortable="true" title="Data de Gera��o" sortProperty="dataGeracao" style="text-align: center;">
	        	<a href="#"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.dataGeracaoFormatada}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Data de Previs�o" sortProperty="dataPrevisaoEncerramento" style="text-align: center;">
	        	<a href="#"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.dataPrevisaoEncerramentoFormatada}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Tipo de Servi�o" sortProperty="servicoTipo.descricao" style="text-align: center;" maxLength="40">
	        	<a href="#"><span class="linkInvisivel"></span><c:out value="${lista.servicoTipo.descricao}"/></a>
	        </display:column>
	        <display:column sortable="true" title="N� do Protocolo" sortProperty="chamado.protocolo.numeroProtocolo" style="text-align: center;">
	        	<a href="#"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.chamado.protocolo.numeroProtocolo}"/>
	            </a>
	        </display:column>	
	        <display:column sortable="true" title="Nome do Cliente" sortProperty="cliente.nome" style="text-align: center;">
	        	<a href="#"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.cliente.nome}"/>
	            </a>
	        </display:column>	
	        <display:column sortable="true" title="Ponto de Consumo" sortProperty="pontoConsumo.descricao" style="text-align: center;">
	        	<a href="#"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.pontoConsumo.descricao}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Status" sortProperty="status.descricao" style="text-align: center;" maxLength="40">
	        	<a href="#"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.status.descricao}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Prioridade" sortProperty="servicoTipo.servicoTipoPrioridade.descricao" style="text-align: center;">
	        	<a href="#"><span class="linkInvisivel"></span>
	            	<c:out value="${lista.servicoTipo.servicoTipoPrioridade.descricao}"/>
	            </a>
	        </display:column>       
	    </display:table>	
	</c:if>
	<fieldset class="conteinerBotoesPopup"> 
	    <label class="rotulo" id="rotuloMarca" for="equipe">Turno:</label>
			<select name="turno" id="turno" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTurnos}" var="turno">
					<option value="<c:out value="${turno.chavePrimaria}"/>" 
						<c:if test="${servicoAutorizacaoVO.turno.chavePrimaria == turno.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${turno.descricao}"/>
					</option>		
			    </c:forEach>
			</select><br />	
	    <input id="botaoIncluirAgenda" class="bottonRightCol2 botaoGrande1" value="Incluir na Agenda" type="button" onclick="incluirServicoAutorizacaoAgenda();">
	 </fieldset>
	<c:forEach items="${listaTurnosDisponiveis}" var="turno">
		<input type="hidden" id="${turno.turno.chavePrimaria}" value="${turno.quantidadeAgendamentosTurno}" />
	</c:forEach>
</form:form>