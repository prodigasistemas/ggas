<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=iso-8859-1"%>

<script>

	$("#checkAllAuto").click(function(){
 		var tabela = $("#servicoAutorizacaoAgenda").DataTable();

			var linhas = tabela.cells().nodes();

		    $(linhas).find(':checkbox').prop('checked', this.checked);
	});

	$("#servicoAutorizacaoAgenda").on("page.dt", function(){
		$("#checkAllAuto").prop("checked", false); 
	});
</script>


<input type="hidden" id="listaServicoAutorizacaoPreenchida"
	nome="listaServicoAutorizacaoPreenchida"
	value="${listaServicoAutorizacaoPreenchida}" />
	<c:if test="${servicoAutorizacaoAgendaList ne null}">

		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover"
				id="servicoAutorizacaoAgenda" style="width: 100%">
				<thead class="thead-ggas-bootstrap">
					<tr>
						<th>
							<div
								class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
								<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
									class="custom-control-input"> <label
									class="custom-control-label p-0" for="checkAllAuto"></label>
							</div>
						</th>					
						<th scope="col" class="text-center">N. Protocolo</th>
						<th scope="col" class="text-center">N. AS</th>
						<th scope="col" class="text-center">Tipo de Servi�o</th>
						<th scope="col" class="text-center">Ponto de Consumo</th>
						<th scope="col" class="text-center">Status</th>
						<th scope="col" class="text-center">Data Agendamento</th>
						<th scope="col" class="text-center">Hor�rio</th>
						<th scope="col" class="text-center">Turno</th>
						<th scope="col" class="text-center">Funcion�rio</th>
						<th scope="col" class="text-center"></th>
						<th scope="col" class="text-center"></th>
						<th scope="col" class="text-center">Executada?</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${servicoAutorizacaoAgendaList}"
						var="servicoAutorizacaoAgenda">
						<tr>
							<td>
									<c:choose>
									    <c:when test="${servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria == 438 and (servicoAutorizacaoAgenda.servicoAutorizacao.chamado eq null or (servicoAutorizacaoAgenda.servicoAutorizacao.chamado ne null and  servicoAutorizacaoAgenda.servicoAutorizacao.chamado.status.chavePrimaria != 905))}">
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
												data-identificador-check="chk${servicoAutorizacaoAgenda.chavePrimaria}">
												<input id="chk${servicoAutorizacaoAgenda.chavePrimaria}" type="checkbox"
													name="chavesPrimarias" class="custom-control-input"
													value="${servicoAutorizacaoAgenda.chavePrimaria}"> <label
													class="custom-control-label p-0"
													for="chk${servicoAutorizacaoAgenda.chavePrimaria}"></label>
											</div>									    
									    </c:when>
									    <c:otherwise>
									    </c:otherwise>
									</c:choose>							

							</td>
							<td class="text-center">
								<c:choose>
									<c:when test="${servicoAutorizacaoAgenda.servicoAutorizacao.chamado ne null and  servicoAutorizacaoAgenda.servicoAutorizacao.chamado.status.chavePrimaria == 905}">
										<a href="javascript:exibirChamado('${servicoAutorizacaoAgenda.servicoAutorizacao.chamado.chavePrimaria}')" style="color:red;">							
											<c:out
												value="${servicoAutorizacaoAgenda.servicoAutorizacao.chamado.protocolo.numeroProtocolo}" />
										</a>
									</c:when>
									<c:otherwise>
										<a href="javascript:exibirChamado('${servicoAutorizacaoAgenda.servicoAutorizacao.chamado.chavePrimaria}')">							
											<c:out
												value="${servicoAutorizacaoAgenda.servicoAutorizacao.chamado.protocolo.numeroProtocolo}" />
										</a>
									</c:otherwise>
								</c:choose>							
							</td>
							<td class="text-center">
								<a href="javascript:exibirAutorizacaoServico('${servicoAutorizacaoAgenda.servicoAutorizacao.chavePrimaria}')">
									<c:out
											value="${servicoAutorizacaoAgenda.servicoAutorizacao.chavePrimaria}" />	
								</a>		
							</td>								
							<td class="text-center">
								<c:out
									value="${servicoAutorizacaoAgenda.servicoAutorizacao.servicoTipo.descricao}" />
							</td>
							<td class="text-center"><a href="#"
								title="NOME: ${servicoAutorizacaoAgenda.servicoAutorizacao.cliente.nome}&#13;TELEFONE: <c:forEach items="${servicoAutorizacaoAgenda.servicoAutorizacao.cliente.fones}" var="fone"><c:out value="(${fone.codigoDDD}) ${fone.numero}"/> </c:forEach>&#13;EMAIL: ${servicoAutorizacaoAgenda.servicoAutorizacao.cliente.emailPrincipal}&#13;Endere�o: ${servicoAutorizacaoAgenda.servicoAutorizacao.imovel.enderecoFormatado}"><span
									class="linkInvisivel"></span> <c:out
										value='${servicoAutorizacaoAgenda.servicoAutorizacao.pontoConsumo.descricao}' />
							</a></td>

							<td class="text-center"><c:out
									value='${servicoAutorizacaoAgenda.servicoAutorizacao.status.descricao}' />
							</td>

							<td class="text-center"><fmt:formatDate
									value="${servicoAutorizacaoAgenda.dataAgenda}"
									pattern="dd/MM/yyyy" /></td>

							<td class="text-center">
								<c:choose>
								    <c:when test="${servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria == 438 && (protocolo == null || servicoAutorizacaoAgenda.servicoAutorizacao.chamado.protocolo.numeroProtocolo == protocolo)}">
										<input
											style="width: 50px; text-align: right" class="campoTextoMedio"
											name="horario" onblur="aplicarMascaraCampoHora(this);"
											onkeypress="aplicarMascaraCampoHora(this);"
											onchange="atualizarHoraAgendamento(${servicoAutorizacaoAgenda.chavePrimaria},this.value);"
											type="text" value="<fmt:formatDate pattern="HH:mm" value="${servicoAutorizacaoAgenda.dataAgenda}"  />" />								    
								    </c:when>
								    <c:otherwise>
										<fmt:formatDate value="${servicoAutorizacaoAgenda.dataAgenda}" pattern="HH:mm"/>
								    </c:otherwise>
								</c:choose>							
							</td>

							<td class="text-center"><c:out
									value='${servicoAutorizacaoAgenda.turno.descricao}' /></td>

							<td class="text-center">
									<c:choose>
								    <c:when test="${servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria == 438 && (protocolo == null || servicoAutorizacaoAgenda.servicoAutorizacao.chamado.protocolo.numeroProtocolo == protocolo)}">
										<select name="funcionario_${servicoAutorizacaoAgenda.chavePrimaria}" id="funcionario_${servicoAutorizacaoAgenda.chavePrimaria}"
											onchange="alterarFuncionarioAgenda(this);"
											class="form-control form-control-sm">
											<option value="-1">Selecione...</option>
											<c:forEach items="${listaEquipeComponente}" var="listaEquipeComponente">
												<c:if test="${listaEquipeComponente.equipe.chavePrimaria == servicoAutorizacaoAgenda.servicoAutorizacao.equipe.chavePrimaria}">
													<option value="<c:out value="${listaEquipeComponente.funcionario.chavePrimaria}"/>"
														<c:if test="${servicoAutorizacaoAgenda.funcionario.chavePrimaria == listaEquipeComponente.funcionario.chavePrimaria}">selected="selected"</c:if>>
														<c:out value="${listaEquipeComponente.funcionario.nome}" />
													</option>
												</c:if>
										</c:forEach>
									</select>																			    
									    </c:when>
									    <c:otherwise>
											<c:out
													value='${servicoAutorizacaoAgenda.funcionario.nome}' />
									    </c:otherwise>
									</c:choose>								

							</td>
																		
							<td class="text-center">
								<c:choose>
								    <c:when test="${servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria == 438 && (protocolo == null || servicoAutorizacaoAgenda.servicoAutorizacao.chamado.protocolo.numeroProtocolo == protocolo)}">
										<c:if
												test="${servicoAutorizacaoAgenda.indicadorConfirmado eq false}">
												<input type="hidden" class="confirmacao" value="${servicoAutorizacaoAgenda.chavePrimaria}"/>
												<a onclick="return confirm('Deseja confirmar o agendamento?');"
													href="javascript:confirmar('${servicoAutorizacaoAgenda.chavePrimaria}');">
													<img title="Confirmar agendamento"
													alt="Confirmar agendamento"
													src="<c:url value="/imagens/cancel16.png"/>">
												</a>
											</c:if> <c:if
												test="${servicoAutorizacaoAgenda.indicadorConfirmado eq true}">
												<a  
													onclick="return confirm('Deseja desconfirmar o agendamento?');"
													href="javascript:desconfirmar('${servicoAutorizacaoAgenda.chavePrimaria}');">
													<img
													title="Desconfirmar agendamento"
													alt="Desconfirmar agendamento"
													src="<c:url value="/imagens/check1.gif"/>">
												</a>
											</c:if>								    
								    </c:when>
								    <c:otherwise>
								    </c:otherwise>
								</c:choose>							
							</td>

							<td class="text-center">
								<c:choose>
								    <c:when test="${servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria == 438 && (protocolo == null || servicoAutorizacaoAgenda.servicoAutorizacao.chamado.protocolo.numeroProtocolo == protocolo)}">
										<a
											onclick="return confirm('Deseja excluir o agendamento?');"
											href="javascript:excluirAgendamento('${servicoAutorizacaoAgenda.chavePrimaria}');">
												<img id="excluirAgendamento" title="Excluir agendamento"
												alt="Excluir agendamento"
												src="<c:url value="/imagens/deletar_x.png"/>">
										</a>								    
								    </c:when>
								    <c:otherwise>
								    </c:otherwise>
								</c:choose>							
							</td>
							<td class="text-center">
								<c:choose>
								    <c:when test="${servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria == 438 && (protocolo == null || servicoAutorizacaoAgenda.servicoAutorizacao.chamado.protocolo.numeroProtocolo == protocolo)}">
								    	<img src="<c:url value="/imagens/deletar_x.png"/>">
								    </c:when>
								    <c:otherwise>
								    	<img src="<c:url value="/imagens/check1.gif"/>">
								    </c:otherwise>
								</c:choose>
							</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:if>
