<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:if test="${listaTurnos ne null}">
    <fieldset class="conteinerBotoesPopup" style="display: flex; align-items: center;"> 
        <label class="rotulo" id="rotuloMarca" for="turno" style="margin-right: 10px;">Turno:</label>
        <select name="turno" id="turno" class="campoSelect" style="margin-top: 7px; margin-right: 10px;">
            <option value="-1">Selecione</option>
            <c:forEach items="${listaTurnos}" var="turno">
                <option value="<c:out value='${turno.chavePrimaria}'/>" 
                <c:if test="${turnoPreferencial.chavePrimaria == turno.chavePrimaria}">selected="selected"</c:if>>
                    <c:out value="${turno.descricao}"/>
                </option>
            </c:forEach>
        </select>
        <input id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Incluir na Agenda" type="button" onclick="incluirServicoAutorizacaoAgenda();" />
    </fieldset>
<%-- 	<c:forEach items="${listaTurnosDisponiveis}" var="turno">
		<input type="hidden" id="${turno.chavePrimaria}" value="${turno.quantidadeAgendamentosTurno}" />
	</c:forEach> --%>
	</c:if>