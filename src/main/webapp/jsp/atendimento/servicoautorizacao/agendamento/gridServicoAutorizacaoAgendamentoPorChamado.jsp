<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<c:if test="${listaServicoAutorizacao ne null}">

	<hr class="linhaSeparadora1" />
	<h5>Lista Servi�os Autoriza��o a Serem Agendadas</h5>

	<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover"
			id="servicoAutorizacao" style="width: 100%">
			<thead class="thead-ggas-bootstrap">
				<tr>
					<th>
						<div
							class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
							<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
								class="custom-control-input"> <label
								class="custom-control-label p-0" for="checkAllAuto"></label>
						</div>
					</th>
					<th scope="col" class="text-center">Data de Gera��o</th>
					<th scope="col" class="text-center">Data de Previs�o</th>
					<th scope="col" class="text-center">Tipo de Servi�o</th>
					<th scope="col" class="text-center">N. Protocolo</th>
					<th scope="col" class="text-center">Cliente</th>
					<th scope="col" class="text-center">Ponto Consumo</th>
					<th scope="col" class="text-center">Equipe</th>
					<th scope="col" class="text-center">Status</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listaServicoAutorizacao}" var="lista">
					<tr>
						<td>
							<div
								class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
								data-identificador-check="chk${lista.chavePrimaria}">
								<input id="chk${lista.chavePrimaria}" type="checkbox"
									name="chavesPrimarias" class="custom-control-input"
									value="${lista.chavePrimaria}"> <label
									class="custom-control-label p-0"
									for="chk${lista.chavePrimaria}"></label>
							</div>
						</td>
						<td class="text-center"><c:out
								value="${lista.dataGeracaoFormatada}" /></td>
						<td class="text-center"><c:out
								value="${lista.dataPrevisaoEncerramentoFormatada}" /></td>
						<td class="text-center"><c:out
								value="${lista.dataEncerramentoFormatada}" /></td>

						<td class="text-center"><c:out
								value="${lista.chamado.protocolo.numeroProtocolo}" /></td>

						<td class="text-center"><c:out value="${lista.cliente.nome}" />
						</td>

						<td class="text-center"><c:out value="${lista.equipe.nome}" />
						</td>

						<td class="text-center"><c:out
								value="${lista.status.descricao}" /></td>


						<td class="text-center"><c:out
								value="${lista.servicoTipo.servicoTipoPrioridade.descricao}" />
						</td>

					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<br><br>


</c:if>