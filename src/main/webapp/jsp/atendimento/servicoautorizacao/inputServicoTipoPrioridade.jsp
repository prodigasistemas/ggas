
<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script type="text/javascript">
$(document).ready(function(){
	var fluxoDetalhamento = "<c:out value="${ fluxoDetalhamento }" />";
	var fluxoAlteracao = "<c:out value="${ fluxoAlteracao }" />";
	var fluxoInclusao = "<c:out value="${ fluxoInclusao }" />";
	var fluxoRemanejar = "<c:out value="${ fluxoRemanejar }" />";
	var fluxoExecucao = "<c:out value="${ fluxoExecucao }" />";	
	var fluxoEncerramento = "<c:out value="${ fluxoEncerramento }" />";
	
	if(fluxoInclusao || fluxoDetalhamento || fluxoRemanejar){
		$("#dataPrevisaoEncerramento").attr("disabled","disabled");
	}

	if(fluxoAlteracao.value == "true"){
		$('#dataPrevisaoEncerramento').removeAttr('disabled');
	}
	
	
});
</script>

	<input type="hidden" name="fluxoDetalhamento" id="fluxoDetalhamento" value="${fluxoDetalhamento}">
	<input type="hidden" name="fluxoAlteracao" id="fluxoAlteracao" value="${fluxoAlteracao}">
	<input type="hidden" name="fluxoInclusao" id="fluxoInclusao" value="${fluxoInclusao}">

<label class="rotulo rotulo2Linhas"  id="rotuloIntervaloCadastroDocumentos" for="intervaloCadastroDocumentos" ><span id="spanPrevisaoEncerramento" class="campoObrigatorioSimbolo">* </span>Data Previs�o <br/>de Encerramento:</label>

<c:if test="${ fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoInclusao eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
<input class="campoData2 campo2Linhas campoHorizontal" type="text" id="dataPrevisaoEncerramento" name="dataPrevisaoEncerramento" maxlength="10" value="${previsaoEncerramento}">
</c:if>

<c:if test="${ fluxoDetalhamento eq true }">
<input class="campoTexto" type="text" id="dataPrevisaoEncerramento" disabled="disabled" name="dataPrevisaoEncerramento" size="10" value="${previsaoEncerramento}">
</c:if>
	

<br />


<label class="rotulo rotulo2Linhas" id="rotuloMarca" for="marca"><span id="spanTipoPrioridade" class="campoObrigatorioSimbolo">* </span>Prioridade do<br/>Tipo de Servi�o:</label>
<input type="text" class="campoTexto"  disabled="disabled" value="${servicoTipoPrioridade.descricao}">
<input type="hidden" name="servicoTipoPrioridade" id="servicoTipoPrioridade" value="${servicoTipoPrioridade.chavePrimaria}">

  

