<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

function visualizarInformacoesPontoConsumo(){	
	var chavePontoConsumo = document.forms['servicoAutorizacaoForm'].idPontoConsumo.value;

	if(chavePontoConsumo != ''){
		submeter('servicoAutorizacaoForm','visualizarInformacoesPontoConsumo','_blank');
	}else{
		alert("Escolha um Ponto de Consumo para Vizualizar suas informa��es.");
	}
	
}

</script>

<hr class="linhaSeparadora2" />
	<legend>Pontos de Consumo</legend>

		<display:table style="width: 420px" class="dataTableGGAS" decorator="br.com.ggas.web.cadastro.imovel.decorator.ImovelResultadoPesquisaDecorator" name="pontosConsumo" id="pontoConsumo"  sort="list" pagesize="5"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	     	
	     	<c:if test="${ fluxoInclusao eq true }">
	     	<display:column  style="width: 25px">
		    		<input type="radio" name="checkPontoConsumo" id="checkPontoConsumo" value="<c:out value="${pontoConsumo.chavePrimaria}"/>" <c:if test="${pontoConsumo.chavePrimaria == servicoAutorizacao.pontoConsumo.chavePrimaria}">checked</c:if> onClick="selecionarPontoConsumo(${pontoConsumo.chavePrimaria}), carregaroInformacaoPontoConsumo(${pontoConsumo.chavePrimaria});"   />
		    	</display:column>
		     	<display:column style="width: 30px" title="Descri��o" sortable="true" sortProperty="descricao">
		     		<a href='javascript:detalharImovel(<c:out value='${pontoConsumo.descricao}'/>);'><span class="linkInvisivel"></span>
						<c:out value='${pontoConsumo.descricao}'/>
					</a>
			    </display:column>
	     	</c:if>
	     	
	     	<c:if test="${ fluxoInclusao ne true }">
	     		<display:column  style="width: 25px">
		    		<input type="radio" name="checkPontoConsumo" id="checkPontoConsumo" value="<c:out value="${pontoConsumo.chavePrimaria}"/>" onClick="carregaroInformacaoPontoConsumo(${pontoConsumo.chavePrimaria});"/>
		    	</display:column>
				<display:column property="descricao" sortable="false" title="Descri��o"/>
	     	</c:if>
		    		    
		</display:table>
		
<label class="rotulo" id="rotuloPontoConsumo" for="pontoConsumo">Descri��o:</label>
<input class="campoTexto" type="text" id="descricaoPontoConsumo" name="descricaoPontoConsumo" disabled="disabled"  maxlength="25" size="30" value="<c:out value="${pontoConsumo.descricao}"/>" >
<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Informa��es do Ponto de Consumo" type="button" onclick="visualizarInformacoesPontoConsumo();">		
