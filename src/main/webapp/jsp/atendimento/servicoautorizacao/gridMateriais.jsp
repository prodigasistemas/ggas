<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

function incluirMaterial(){	
	var chaveMaterial = document.forms[0].material.value;
	var quantidadeMaterial = document.forms[0].quantidadeMaterial.value;
	var indexList = -1;
	var noCache = "noCache=" + new Date().getTime();	
	
	var url = "incluirMaterialServicoAutorizacao?material="+chaveMaterial+"&quantidadeMaterial="+quantidadeMaterial+"&indexList="+indexList+"&"+noCache;
	
	if(chaveMaterial!="-1" && quantidadeMaterial > 0) {
	  carregarFragmento('gridMateriaisServicoAutorizacao',url);
	} else if(chaveMaterial == "-1" || quantidadeMaterial == ""){
		alert('Os campos Descri��o do Material e Quantidade s�o de preenchimento obrigat�rio');
	} else if(quantidadeMaterial == 0){
		alert('O campo Quantidade deve ser maior que zero.');
	}
}

function alterarMaterial() {	
	var chaveMaterial = document.forms[0].material.value;
	var chavePrimaria = document.forms[0].chavePrimaria.value;
	var quantidadeMaterial = document.forms[0].quantidadeMaterial.value;
	var indexList = document.forms[0].indexList.value;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "incluirMaterialServicoAutorizacao?material="+chaveMaterial+"&quantidadeMaterial="+quantidadeMaterial+"&chavePrimaria="+chavePrimaria+"&indexList="+indexList+"&"+noCache;
	
	if(chaveMaterial!="-1" && chaveMaterial != "" && quantidadeMaterial > 0) {
		carregarFragmento('gridMateriaisServicoAutorizacao',url);
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	botaoAlterarMaterial.disabled = true;
	} else if(chaveMaterial == "-1" || quantidadeMaterial == ""){
		alert('Os campos Descri��o do Material e Quantidade s�o de preenchimento obrigat�rio');
	} else if(quantidadeMaterial == 0){
		alert('O campo Quantidade deve ser maior que zero.');
	}
}

function exibirAlteracaoMaterial(indice,idMaterial,quantidade) {	
	document.forms[0].indexList.value = indice;
	document.forms[0].material.value = idMaterial;
	document.forms[0].quantidadeMaterial.value = quantidade;
	
	var botaoAlterarMaterial = document.getElementById("botaoAlterarMaterial");
	var botaoLimparMaterial = document.getElementById("botaoLimparMaterial");	
	var botaoIncluirMaterial = document.getElementById("botaoIncluirMaterial");	
	botaoIncluirMaterial.disabled = true;
	botaoLimparMaterial.disabled = false;
	botaoAlterarMaterial.disabled = false;
}

function removerMaterial(chavePrimariaMaterial) {
	var noCache = "noCache=" + new Date().getTime();
	$("#gridMateriaisServicoAutorizacao").load("removerMaterialServicoAutorizacao?chavePrimariaMaterial="+chavePrimariaMaterial+"&"+noCache);
}

function limparMaterial() {
	document.forms[0].material.value = -1;
	document.forms[0].quantidadeMaterial.value = "";
}

</script>
<!-- 		<legend>Materiais</legend> -->

	   <div style="padding-bottom: 115px;padding-left: 10px">
                     <label class="rotulo"  id="rotuloTipo" for="tipo">Material:</label>
			      <select name="material" id="material" class="campoSelect">
				    	<option value="-1">Selecione</option>
					   <c:forEach items="${listaMateriais}" var="material">
						<option value="<c:out value="${material.chavePrimaria}"/>" >
							<c:out value="${material.descricao}"/>
						</option>		
				     </c:forEach>
			      </select>
                    
                    <div style="float: right;"> 
	                    <label class="rotulo" id="rotuloMarca" for="quantidade">Quantidade:</label>
				    	<input type="text" class="campoTexto" id="quantidadeMaterial" size="10" maxlength="5" onkeypress="return formatarCampoInteiro(event)" value="${quantidadeMaterial}">
                    </div>
                  
                     <hr class="linhaSeparadoraPesquisa" />
					
					<div class="MateriaisNecessarios">
						<input class="bottonRightCol" id="botaoLimparMaterial" name="botaoLimparMaterial" value="Limpar" type="button" onclick="limparMaterial();">
						<input class="bottonRightCol bottonLeftColUltimo" id="botaoIncluirMaterial" name="botaoIncluirMaterial" value="Adicionar" type="button" onclick="incluirMaterial();">
	   					<input class="bottonRightCol bottonLeftColUltimo" id="botaoAlterarMaterial" name="botaoAlterarMaterial" value="Alterar" type="button" onclick="alterarMaterial();">
					</div>
                     </div>
                     
           <c:set var="i" value="0" />
            <display:table class="dataTableGGAS dataTableServico" style="width:357px" name="sessionScope.listaAutorizacaoMateriais" sort="list" id="servicoAutorizacaoMaterial" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	            
	            <display:column property="material.descricao" sortable="false" headerClass="tituloTabelaEsq" title="Material" style="text-align: left; padding-left: 10px"/>
	             <display:column property="material.unidadeMedida.descricaoAbreviada" sortable="false" headerClass="tituloTabelaEsq" title="Unidade" style="text-align: left; padding-left: 10px;width:17px;"/>
	            <display:column property="quantidadeMaterial" sortable="false" headerClass="tituloTabelaEsq" title="Quantidade" style="text-align: center; padding-left: 10px;width:17px;"/>
              	
              	 <c:if test="${ fluxoAlteracao eq true || fluxoEncerramento eq true || fluxoExecucao eq true }">
	              	<display:column style="text-align: center; width: 25px">
						<a href="javascript:exibirAlteracaoMaterial('${i}','${servicoAutorizacaoMaterial.material.chavePrimaria}','${servicoAutorizacaoMaterial.quantidadeMaterial}');">
						<img title="Alterar Material" alt="Alterar Material"  src="<c:url value="/imagens/16x_editar.gif"/>">
						</a> 
					</display:column>
		
					<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
						<a onclick="return confirm('Deseja excluir o material?');" href="javascript:removerMaterial('${servicoAutorizacaoMaterial.material.chavePrimaria}');">
						<img title="Excluir Material" alt="Excluir Material"  src="<c:url value="/imagens/deletar_x.png"/>">
						</a> 
					</display:column>
              	</c:if>
	        <c:set var="i" value="${i+1}" />
          </display:table>


