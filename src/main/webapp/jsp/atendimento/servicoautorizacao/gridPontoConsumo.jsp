<!--
 Copyright (C) <2011> GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

 Este programa  um software livre; voc pode redistribu-lo e/ou
 modific-lo sob os termos de Licena Pblica Geral GNU, conforme
 publicada pela Free Software Foundation; verso 2 da Licena.

 O GGAS  distribudo na expectativa de ser til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
 COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
 Consulte a Licena Pblica Geral GNU para obter mais detalhes.

 Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
 junto com este programa; se no, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1"%>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>



<c:set var="x" value="0" />
<div class="table-responsive">

	<table class="table table-bordered table-striped table-hover"
		id="table-ponto-inserido" width="100%" style="opacity: 0;">
		<thead class="thead-ggas-bootstrap">
			<tr>
				<th scope="col" class="text-center">Ponto de Consumo</th>
				<th scope="col" class="text-center">C�digo do Ponto de Consumo</th>
				<th scope="col" class="text-center">Segmento</th>
				<th scope="col" class="text-center">Situa��o</th>
				<th scope="col" class="text-center">A��o</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listaPontoConsumoInserido}" var="pontoConsumo">
				<tr>
					<td class="text-center">
						<c:out value="${pontoConsumo.descricao} - ${pontoConsumo.imovel.nome}"/>
					</td>
					<td class="text-center">
						<c:out value="${pontoConsumo.codigoPontoConsumo}"/>
					</td>
					<td class="text-center">
						<c:out value="${pontoConsumo.segmento.descricao}"/>
					</td>
					<td class="text-center">
						<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
					</td>
					<td class="text-center"><a
						href="javascript:removerPontosGrid(<c:out value="${x}"/>, this.form);"><img
							title="Excluir Ponto de Consumo" alt="Excluir Ponto de Consumo"
							src="<c:url value="/imagens/deletar_x.png"/>" border="0" /> </a></td>
				</tr>
				<c:set var="x" value="${x+1}" />
			</c:forEach>
		</tbody>
	</table>
</div>
