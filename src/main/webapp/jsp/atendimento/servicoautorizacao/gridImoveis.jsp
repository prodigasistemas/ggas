<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

if(cliente.chavePrimaria == null){
	carregarImovel();
}

function carregarImovel(){		
	$(".mensagensSpring").show();	
	if($("input[name='chaveImovel']").size() == 1) {
		$("input[name='chaveImovel']").trigger( "click" );
	}	
}

</script>

		<display:table style="width: 500px" class="dataTableGGAS" decorator="br.com.ggas.web.cadastro.imovel.decorator.ImovelResultadoPesquisaDecorator" name="sessionScope.listaImoveisChamado" id="imovel"  sort="list"   excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	     	<display:column  style="width: 25px">
		    		<input type="radio" name="chaveImovel" id="chaveImovel" value="<c:out value="${imovel.chavePrimaria}"/>" <c:if test="${imovel.chavePrimaria == servicoAutorizacao.imovel.chavePrimaria }">checked</c:if> onClick="carregarPontosConsumo(${imovel.chavePrimaria});"  />
		    	</display:column>
	     	<display:column style="width: 30px" title="Matr�cula" sortable="true" sortProperty="chavePrimaria">
	     		<a title="Im�vel Novo: <c:if test="${imovel.indicadorObraTubulacao eq 'true'}">Sim</c:if><c:if test="${imovel.indicadorObraTubulacao ne 'true'}">N�o</c:if>&#13;Modalidade de Medi��o: ${imovel.modalidadeMedicaoImovel.descricao}&#13;" href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${imovel.chavePrimaria}'/>
				</a>
		    </display:column>
		    <display:column sortable="true" sortProperty="nome" title="Nome" headerClass="tituloTabelaEsq" >
    			<a title="Im�vel Novo: <c:if test="${imovel.indicadorObraTubulacao eq 'true'}">Sim</c:if><c:if test="${imovel.indicadorObraTubulacao ne 'true'}">N�o</c:if>&#13;Modalidade de Medi��o: ${imovel.modalidadeMedicaoImovel.descricao}&#13;" href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
					<c:out value='${imovel.nome}'/>
       			</a>
		    </display:column>		     
		    <display:column style="width: 80px" title="Situa��o" sortable="true" sortProperty="situacaoImovel">
		    	<a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    		<c:out value='${imovel.situacaoImovel.descricao}'/>
		    	</a>
		    </display:column>
		    <display:column sortable="true" title="Endere�o" sortProperty="enderecoFormatado" headerClass="tituloTabelaEsq" >
		    	<a style="width: 200px" href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    		<c:out value='${imovel.enderecoFormatado}'/>
		    	</a>
		    </display:column>
		    <display:column sortable="true" title="Tipo Medi��o" sortProperty="modalidadeMedicaoImovel" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq" style="width: 80px">
		    	<a href='javascript:detalharImovel(<c:out value='${imovel.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
		    		<c:choose>
		    			<c:when test="${imovel.imovelCondominio ne null}">
				    		<c:out value='${imovel.imovelCondominio.modalidadeMedicaoImovel.descricao}'/>
		    			</c:when>
		    			<c:otherwise>
				    		<c:out value='${imovel.modalidadeMedicaoImovel.descricao}'/>
		    			</c:otherwise>
		    		</c:choose>
		    	</a>
		    </display:column>
		</display:table>
		