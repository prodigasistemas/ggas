<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="br.com.ggas.util.DataUtil" %>
<%@ page import="org.apache.commons.lang.StringUtils"%>

<link rel="stylesheet"
	href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
	integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY="
	crossorigin="" />

<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
	integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
	crossorigin=""></script>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

    <style>
        .map-title {
            font-size: 14px;
            font-weight: bold;
            background: rgba(255, 255, 255, 0.8);
            padding: 5px 10px;
            border-radius: 5px;
            margin-right: 10px; /* Margem para garantir que o título não seja cortado */
            margin-top: 15px; /* Margem para garantir que o título não fique colado na borda superior */
        }
    </style>

<div class="bootstrap">
	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Acompanhamento do Funcionário na
				Execução da Autorização de Serviço</h5>
		</div>
		<div class="card-body">
			<div class="alert alert-primary fade show" role="alert">
				<i class="fa fa-question-circle"></i> Abaixo no mapa mostra o local
				aonde os funcionários estão executando as Autorizações de Serviço,
				caso queira filtrar por funcionario é possível.
			</div>

			<hr>

			<div class="card">
			    <div class="card-body bg-light">
			        <div class="form-row align-items-center">
			            <div class="col-mb-0">
			                <label for="funcionario" class="mb-0">Funcionário:<span class="text-danger">*</span></label>
			            </div>
			            <div class="col-md-3">
			                <select name="funcionario" id="funcionario" class="form-control form-control-sm" onchange="localizarFuncionario(this, false);">
			                    <option value="-1">Selecione</option>
			                    <c:forEach items="${listaFuncionario}" var="funcionario">
			                        <option value="<c:out value="${funcionario.chavePrimaria}"/>">
			                            <c:out value="${funcionario.nome}" />
			                        </option>
			                    </c:forEach>
			                </select>
			            </div>
						
			            	<div class="col-mb-0">						
								<label for="equipe">Equipe:</label>
							</div>
						<div class="col-md-3">
							<select name="equipe" id="equipe"
								class="form-control form-control-sm"
								onchange="atualizar();">
								<option value="-1">Selecione</option>
								<c:forEach items="${listaEquipe}" var="equipe">
									<option value="<c:out value="${equipe.chavePrimaria}"/>"
										<c:if test="${equipeTurno.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
										<c:out value="${equipe.nome}" />
									</option>
								</c:forEach>
							</select>
						</div>			            
			            <div class="col-md-3">
			                <button class="btn btn-primary btn-sm" id="atualizar" type="button" onclick="atualizar();">Atualizar</button>
			            </div>
			        </div>
			    </div>
			</div>
									
			<div id="map" style="height: 600px;"></div>
		</div>
	</div>

</div>



<script type="text/javascript">
<%
List<Object[]> listaLocalizacao = (List<Object[]>) request.getAttribute("listaLocalizacao");
Long periodicidadeLocalizacao = (Long) request.getAttribute("periodicidadeLocalizacao");
Integer quantidade = 0;



// Agrupar localizações pela coordenada
Map<String, List<Object[]>> coordenadasAgrupadas = new HashMap<>();

for (Object[] localizacao : listaLocalizacao) {
    String coordenada = (String) localizacao[1];
    if (!coordenadasAgrupadas.containsKey(coordenada)) {
        coordenadasAgrupadas.put(coordenada, new ArrayList<Object[]>());
    }
    coordenadasAgrupadas.get(coordenada).add(localizacao);
}
%>


		let map = L.map('map').setView([-9.6160604, -35.7370944], 12);
			L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
		    	maxZoom: 19,
		    }).addTo(map);
			
			var redMarker = L.icon({
			    iconUrl: 'imagens/marker-icon-2x-red.png',
			    iconSize: [25, 41], // Tamanho do ícone em pixels
			    iconAnchor: [12, 41], // Ponto de ancoragem do ícone
			    popupAnchor: [1, -34] // Ponto onde o popup aparecerá em relação ao ícone
			});
			
			var blueMarker = L.icon({
			    iconUrl: 'imagens/marker-icon-2x.png',
			    iconSize: [25, 41], // Tamanho do ícone em pixels
			    iconAnchor: [12, 41], // Ponto de ancoragem do ícone
			    popupAnchor: [1, -34] // Ponto onde o popup aparecerá em relação ao ícone
			});				
			var greenMarker = L.icon({
			    iconUrl: 'imagens/marker-icon-2x-green.png',
			    iconSize: [25, 41], // Tamanho do ícone em pixels
			    iconAnchor: [12, 41], // Ponto de ancoragem do ícone
			    popupAnchor: [1, -34] // Ponto onde o popup aparecerá em relação ao ícone
			});				
			var orangeMarker = L.icon({
			    iconUrl: 'imagens/marker-icon-2x-orange.png',
			    iconSize: [25, 41], // Tamanho do ícone em pixels
			    iconAnchor: [12, 41], // Ponto de ancoragem do ícone
			    popupAnchor: [1, -34] // Ponto onde o popup aparecerá em relação ao ícone
			});				

	let markersGroup = L.layerGroup().addTo(map);

	<%for (Map.Entry<String, List<Object[]>> entry : coordenadasAgrupadas.entrySet()) {
	String coordenada = entry.getKey();
	List<Object[]> localizacoes = entry.getValue();
	StringBuilder popupContent = new StringBuilder();
	String cor = "blue";

	for (Object[] localizacao : localizacoes) {
		String nomeFuncionario = (String) localizacao[0];
		String numeroAS = localizacao[2] != null ? String.valueOf((Long) localizacao[2]) : "";
		String horario = (String) localizacao[4];
		popupContent.append(nomeFuncionario).append(" - ").append("<b>").append(horario).append("h</b>");
		String tipoLocalizacao = (String) localizacao[5];
		Boolean isLogoff = DataUtil.verificarLogoffMobile((Date) localizacao[8],periodicidadeLocalizacao);
		Long chaveFunc = (Long) localizacao[9];
		Long previsao = (Long) localizacao [11];
		
		if(tipoLocalizacao.equals("Logout") || isLogoff) {
			cor = "red";
		} else if(tipoLocalizacao.equals("Em Deslocamento")){
            cor = "orange";
		} else if(tipoLocalizacao.equals("Em Execução")){
            cor = "azul";
		} else{
			cor = "green";
		}
		
		if(!isLogoff) {
			if (!numeroAS.isEmpty()) {
				popupContent.append("<br> <b> Em Exec. AS: </b>").append(numeroAS);
				
				String pontoConsumo = localizacao[3] != null ? String.valueOf(localizacao[3]) : "";
				
				if(!StringUtils.isEmpty(pontoConsumo)) {
					popupContent.append("<br>").append(pontoConsumo);
				}
			} else {
				String numeroASDeslocalmento = localizacao[6] != null ? String.valueOf((Long)localizacao[6]) : "";
				
				String tipoMensagem = "Em Desl. AS:";
				if(tipoLocalizacao.equals("Fim do Deslocamento")) {
					tipoMensagem = "Em Exec. AS:";
				}
				
				if(!StringUtils.isEmpty(numeroASDeslocalmento)) {
					popupContent.append("<br> <b> ").append(tipoMensagem).append("</b>").append(numeroASDeslocalmento);
					String pontoConsumo = localizacao[7] != null ? String.valueOf(localizacao[7]) : "";
					
					if(!StringUtils.isEmpty(pontoConsumo)) {
						popupContent.append("<br>").append(pontoConsumo);
					}
				}
				
				
			}
		}

		popupContent.append("<br><br>");
	}

	// Remover o último <br><br>
	if (popupContent.length() > 8) {
		popupContent.setLength(popupContent.length() - 8);
	}

	String[] coords = coordenada.split(",");
	quantidade++;
	String lat = coords[0];
	String lng = coords[1];%>
		        var marker = L.marker([<%=lat%>, <%=lng%>],  { icon:  selecionarMarcador('<%=cor%>') }).addTo(markersGroup);
		        marker.bindPopup("<%=popupContent.toString()%>");

						// Abre o popup no mouseover
						marker.on('mouseover', function() {
							this.openPopup();
						});

						// Fecha o popup no mouseout
						marker.on('mouseout', function() {
							if (!this.isPopupOpen()) {
								this.closePopup();
							}
						});

						// Abre o popup no click
						marker.on('click', function() {
							this.openPopup();
						});
	<%}%>
    addMapTitle(<%= quantidade %> + " Operador(es).");
    addLegend();
	$("div").remove(".leaflet-control-attribution");

	setTimeout(atualizar,100);
	
	var intervalo = 30 * 1000;

	var timer = setInterval(atualizar, intervalo);

	function localizarFuncionario(elem, atualizar) {
	    var chaveFuncionario = elem.value;
	    var chaveEquipe = $("#equipe").val();
	    markersGroup.clearLayers();
	    var qtdOperadores = 0;

	    AjaxService.consultarLocalizacaoFuncionario(chaveFuncionario, chaveEquipe, {
	        callback: function(listaLocalizacao) {
	            var coordenadasAgrupadas = {};

	            // Agrupar localizações pela coordenada
	            listaLocalizacao.forEach(function(localizacao) {
	                var coordenadas = localizacao['coordenada'];
	                if (!coordenadasAgrupadas[coordenadas]) {
	                    coordenadasAgrupadas[coordenadas] = [];
	                }
	                coordenadasAgrupadas[coordenadas].push(localizacao);
	            });

	            // Adicionar marcadores para cada grupo de coordenadas
	            for (var coordenadas in coordenadasAgrupadas) {
                    var cor = "blue";
	                if (coordenadasAgrupadas.hasOwnProperty(coordenadas)) {
	                    var localizacoes = coordenadasAgrupadas[coordenadas];
	                    var popupContent = "";

	                    localizacoes.forEach(function(localizacao) {
	                        popupContent += localizacao['popup']
	                        popupContent += "<br><br>";
	                        
	                        var tipoLocalizacao = localizacao['tipoLocalizacao'];
	                        var isLogoff = localizacao['isLogoff'];
	                        
	                        if(tipoLocalizacao == "Logout" || isLogoff == "true") {
	                        	cor = "red";
	                        }else if(tipoLocalizacao == "Registro de Localização"){
	                        	cor = "green";
	                        }else if(tipoLocalizacao == "Em Deslocamento"){
	                        	cor = "orange"
	                        }
	                    });

	                    // Remover o último <br><br>
	                    popupContent = popupContent.slice(0, -8);

	                    var coords = coordenadas.split(",");
	                    var lat = coords[0];
	                    var lng = coords[1];
	                    
	                    var marker = L.marker([lat, lng], { icon:  selecionarMarcador(cor) }).addTo(markersGroup);
	                    marker.bindPopup(popupContent);
	                    qtdOperadores++;

	                    // Abre o popup no mouseover
	                    marker.on('mouseover', function() {
	                        this.openPopup();
	                    });

	                    // Fecha o popup no mouseout
	                    marker.on('mouseout', function() {
	                        if (!this.isPopupOpen()) {
	                            this.closePopup();
	                        }
	                    });

	                    // Abre o popup no click
	                    marker.on('click', function() {
	                        this.openPopup();
	                    });

	                    // Centraliza o mapa na coordenada do primeiro marcador encontrado
	                    if(!atualizar) {
		                    map.setView([lat, lng], 12);
	                    }
	                    
	                    addMapTitle(qtdOperadores + " Operador(es).");
	                    addLegend();
	                    
	                }
	            }
	        },
	        async: true
	    });
	}
	
	function atualizar() {
		var elem = document.getElementById("funcionario");
		var valor = elem.value;
		
        // Capturar o nível de zoom e o centro atuais
        var currentZoom = map.getZoom();
        var currentCenter = map.getCenter();
        
		consultarFuncionarios(valor);
		localizarFuncionario(elem, true);
		
		map.setView(currentCenter, currentZoom);
	}
	
	function selecionarMarcador(cor) {
		if(cor == "blue") {
			return blueMarker;
		} else if (cor == "red"){
			return redMarker;
		}else if (cor == "green"){
			return greenMarker;
		}else{
			return orangeMarker;
		}
	}
	


	function consultarFuncionarios(valor) {
		var selectFunc = document.getElementById("funcionario");

		selectFunc.length = 0;
		var novaOpcao = new Option("Selecione", "-1");
		selectFunc.options[selectFunc.length] = novaOpcao;
		AjaxService.consultarFuncionariosAtualizados($("#equipe").val(),{
			callback : function(funcionario) {
		        // Criar uma matriz de pares chave-valor
		        var entries = Object.entries(funcionario);

		        // Ordenar a matriz de pares com base nos valores (funcionario[key])
		        entries.sort((a, b) => {
		            if (a[1] < b[1]) {
		                return -1;
		            }
		            if (a[1] > b[1]) {
		                return 1;
		            }
		            return 0;
		        });

		        // Iterar sobre os pares ordenados e adicionar as opções ao select
		        for (var i = 0; i < entries.length; i++) {
		            var key = entries[i][0];
		            var value = entries[i][1];
		            var novaOpcao = new Option(value, key);
		            selectFunc.options[selectFunc.length] = novaOpcao;
		        }
		        
		        selectFunc.value = valor;
			},
			async : false
		});
	}
	
	
    // Variável para armazenar a referência ao controle do título
    var titleControl;
	
    // Função para adicionar um título ao mapa
    function addMapTitle(title) {
        // Verificar se já existe um título e removê-lo
        if (titleControl) {
            map.removeControl(titleControl);
        }

        // Criar um novo controle de título
        titleControl = L.control({position: 'topright'});

        titleControl.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'map-title');
            div.innerHTML = title;
            return div;
        };

        titleControl.addTo(map);
    }
    
    var legendControl;
    
    function addLegend() {
    	// Verificar se já existe uma legenda e removê-la
        if (legendControl) {
            map.removeControl(legendControl);
        }
        
        // Criar um novo controle de legenda
        legendControl = L.control({position: 'bottomright'});

        legendControl.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'map-title');
            div.innerHTML = 
            "<font color='#A1224F'> Vermelho - Inativo </font>"+"</br>"+
            "<font color='#1F79C8'> Azul - Em Execução </font>"+"</br>" +
            "<font color='#3ABF00'> Verde - Ativo </font>"+"</br>" +
            "<font color='#F58A00'> Laranja - Em Deslocamento </font>";
            return div;
        };

        legendControl.addTo(map);
    }
    
	function carregarFuncionarios(){
		
		var idEquipe = $("#equipe").val();
		var listaDeFuncionarios = $("#funcionario");
		if(idEquipe != -1 && listaDeFuncionarios != null){
			listaDeFuncionarios.empty();
			listaDeFuncionarios.append('<option value="-1">Selecione</option>');
			
			AjaxService.obterFuncionariosPorEquipe(idEquipe,function(funcionario){
				for (key in funcionario){
				    const option = document.createElement("option");
				    option.value = key;
				    option.textContent = funcionario[key];
				    listaDeFuncionarios.append(option);
				}
			});
		}else{
			listaDeFuncionarios.empty();
			listaDeFuncionarios.append('<option value="-1">Selecione</option>');
			
			AjaxService.listarTodosFuncionarios(function(funcionario){
				for (key in funcionario){
				    const option = document.createElement("option");
				    option.value = key;
				    option.textContent = funcionario[key];
				    listaDeFuncionarios.append(option);
				}
			});
		}
		
	}
    
</script>
