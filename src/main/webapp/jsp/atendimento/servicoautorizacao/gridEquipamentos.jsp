<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">
function adicionarEquipamento() {
	var chaveEquipamento = document.forms[0].equipamento.value;
	var noCache = "noCache=" + new Date().getTime();
	
	var url = "adicionarEquipamento?equipamento="+chaveEquipamento+"&"+noCache;
	
	if(chaveEquipamento != "-1") {
	  carregarFragmento("gridEquipamentosServicoAutorizacao",url);
	} else {
		alert('Equipamento n�o selecionado');
	}
}

function removerEquipamento(chaveEquipamento){	
	var noCache = "noCache=" + new Date().getTime();
	$("#gridEquipamentosServicoAutorizacao").load("removerEquipamentoServicoAutorizacao?chavePrimariaEquipamento="+chaveEquipamento+"&"+noCache);
}
</script>        
            <div style="padding-bottom: 115px;padding-left: 10px">
                  <label class="rotulo"  id="rotuloTipo" for="tipo">Equipamento:</label>
			      <select name="equipamento" id="equipamento" class="campoSelect" >
				    	<option value="-1">Selecione</option>
					   <c:forEach items="${listaEquipamentos}" var="equipamento">
						<option value="<c:out value="${equipamento.chavePrimaria}"/>">
							<c:out value="${equipamento.descricao}"/>
						</option>		
				     </c:forEach>
			      </select>
                  
                     <hr class="linhaSeparadoraPesquisa" />
					
<%--                   <input name="Button" id="bottonRightCol bottonLeftColUltimo" type="button" class="bottonRightCol" value="Limpar" onclick="adicionarServico('${servico.chavePrimaria}');"> --%>
                  <input name="botaoIncluirEquipamento" id="botaoIncluirEquipamento" type="button" class="bottonRightCol" value="Adicionar" onclick="adicionarEquipamento();">
           </div>
        
        
           <c:set var="indexServicoTipo" value="0" />
            <display:table class="dataTableGGAS dataTableServico" style="width: 310px" name="sessionScope.listaAutorizacaoEquipamentos" sort="list" id="servicoAutorizacaoEquipamento" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	            <display:column property="equipamento.descricao" sortable="false" headerClass="tituloTabelaEsq" title="Equipamento" style="text-align: left; padding-left: 10px"/>              
                
                 <c:if test="${ fluxoAlteracao eq true || fluxoEncerramento eq true || fluxoExecucao eq true }">
                	<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
						<a onclick="return confirm('Deseja excluir o equipamento?');" href="javascript:removerEquipamento('${servicoAutorizacaoEquipamento.equipamento.chavePrimaria}');">
							<img title="Excluir equipamento" alt="Excluir equipamento"  src="<c:url value="/imagens/deletar_x.png"/>">
						</a> 
					</display:column>
                 </c:if>
	        <c:set var="indexServicoTipo" value="${indexServicoTipo+1}" />
          </display:table>
          
