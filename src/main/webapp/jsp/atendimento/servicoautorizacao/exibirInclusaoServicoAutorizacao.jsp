<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<link type="text/css" rel="stylesheet"
	href="${ctxWebpack}/dist/modulos/atendimentoAoPublico/autorizacaoServico/exibirInclusaoServicoAutorizacao/index.css" />

<div class="bootstrap">
	<!-- IN�CIO DE BLOCO: Vari�veis JSP utilizads no arquivo index.js -->

	<input type="hidden" name="chamadoTipo.chavePrimaria"
		value="${chamadoTipo.chavePrimaria}"> <input type="hidden"
		name="chamado.imovel.chavePrimaria"
		value="${servicoAutorizacao.chamado.imovel.chavePrimaria}"> <input
		type="hidden"
		name="servicoAutorizacao.servicoTipo.indicadorEncerramentoAuto"
		value="${servicoAutorizacao.servicoTipo.indicadorEncerramentoAuto}">
	<input type="hidden"
		name="servicoAutorizacao.servicoAutorizacaoMotivoEncerramento"
		value="${servicoAutorizacao.servicoAutorizacaoMotivoEncerramento}">
	<input type="hidden" name="servicoAutorizacao.chavePrimaria"
		value="${servicoAutorizacao.chavePrimaria}"> 
	<!-- FIM DE BLOCO: Vari�veis JSP utilizads no arquivo index.js -->

	<form:form modelAttribute="ServicoAutorizacao" method="post"
		action="incluirServicoAutorizacao" id="servicoAutorizacaoForm"
		name="servicoAutorizacaoForm" enctype="multipart/form-data">

		<input type="hidden" name="chavesPrimarias" id="chavePrimaria"
			value="${servicoAutorizacao.chavePrimaria}">
		<input type="hidden" name="chavePrimaria"
			value="${servicoAutorizacao.chavePrimaria}">
		<input type="hidden" name="versao" id="versao"
			value="${servicoAutorizacao.versao}">
		<input type="hidden" name="fluxoDetalhamento" id="fluxoDetalhamento"
			value="${fluxoDetalhamento}">
		<input type="hidden" name="fluxoAlteracao" id="fluxoAlteracao"
			value="${fluxoAlteracao}">
		<input type="hidden" name="fluxoInclusao" id="fluxoInclusao"
			value="${fluxoInclusao}">
		<input type="hidden" name="fluxoTramitacao" id="fluxoTramitacao"
			value="${fluxoTramitacao}">
		<input type="hidden" name="fluxoAlteracaoRascunho"
			id="fluxoAlteracaoRascunho" value="${fluxoAlteracaoRascunho}">
		<input type="hidden" name="fluxoRemanejar" id="fluxoRemanejar"
			value="${fluxoRemanejar}">
		<input type="hidden" name="versao" id="versao"
			value="${servicoAutorizacao.versao}">
		<input type="hidden" name="fluxoEncerramento" id="fluxoEncerramento"
			value="${fluxoEncerramento}">
		<input type="hidden" name=fluxoExecucao id="fluxoExecucao"
			value="${fluxoExecucao}">
		<input type="hidden" name="chaveEquipe" id="chaveEquipe"
			value="${document.forms[0].equipe.value}">
		<input type="hidden" name="indexList" id="indexList">
		<input type="hidden" name="fluxoVoltar" id="fluxoVoltar">
		<input type="hidden" name="idServicoAutorizacao"
			id="idServicoAutorizacao">
		<input type="hidden" name="chaveServicoAutorizacaoHistorico"
			id="chaveServicoAutorizacaoHistorico">

		<input type="hidden" name="idPontoConsumo" id="idPontoConsumo"
			value="${servicoAutorizacao.pontoConsumo.chavePrimaria}" />
		<input type="hidden" name="idOperacaoMedidor" id="idOperacaoMedidor"
			value="${servicoAutorizacao.servicoTipo.indicadorOperacaoMedidor}" />
		<input type="hidden" name="tipoAssociacao" id="tipoAssociacao"
			value="${servicoAutorizacao.servicoTipo.indicadorTipoAssociacaoPontoConsumo}" />
		<input type="hidden" name="fluxoPesquisaSatisfacao"
			id="fluxoPesquisaSatisfacao" value="${fluxoPesquisaSatisfacao}">
		<input type="hidden" name="idTurnoPreferencia" id="idTurnoPreferencia"
			value="${servicoAutorizacao.turnoPreferencia.chavePrimaria}" />
		<input name="isGerarAsAutomatica" type="hidden"
			id="isGerarAsAutomatica" value="">
		<input name="indicadorAssociacaoEmLote" type="hidden" id="indicadorAssociacaoEmLote" value="false" />

		<input name="comChamado" type="hidden" id="comChamado" value="false">
		
		<input id="idServicoTipo" type="hidden" value="${servicoAutorizacao.servicoTipo.chavePrimaria}"/>

		<input type="hidden" name="numeroSerieMedidorAtual"
			id="numeroSerieMedidorAtual"
			value="${servicoAutorizacaoTemp.medidorNovo.numeroSerie}" />
		<input type="hidden" name="leituraAtual" id="leituraAtual"
			value="${servicoAutorizacaoTemp.numeroLeituraNovo}" />
		<input type="hidden" name="leituraAnterior" id="leituraAnterior"
			value="${servicoAutorizacaoTemp.numeroLeituraOriginal}" />
		<input type="hidden" name="funcionario" id="funcionario"
			value="${servicoAutorizacaoTemp.funcionario.chavePrimaria}" />
		<input type="hidden" name="medidorMotivoOperacao"
			id="medidorMotivoOperacao"
			value="${servicoAutorizacaoTemp.motivoOperacaoMedidor.chavePrimaria}" />
		<input type="hidden" name="lacre" id="lacre"
			value="${servicoAutorizacaoTemp.numeroLacreMedidorNovo1}" />
		<input type="hidden" name="lacreDois" id="lacreDois"
			value="${servicoAutorizacaoTemp.numeroLacreMedidorNovo2}" />
		<input type="hidden" name="lacreTres" id="lacreTres"
			value="${servicoAutorizacaoTemp.numeroLacreMedidorNovo3}" />
		<input type="hidden" name="dataMedidor" id="dataMedidor"
			value=<fmt:formatDate value="${servicoAutorizacaoTemp.dataLeituraNovo != null ? servicoAutorizacaoTemp.dataLeituraNovo : servicoAutorizacaoTemp.dataLeituraOriginal}" pattern="dd/MM/yyyy"/>>
        <input type="hidden" name="isPontoCicloLeitura" id="isPontoCicloLeitura" value="false"/>
        

        <div id="dialog-confirm" title="Relat�rio Autoriza��o de Servi�o">
            <p>Deseja incluir o chamado correspondentes a essa Autoriza��o de Servi�o?</p>
        </div>


        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">
                    <c:if test="${ fluxoInclusao eq true }">
                        Incluir Autoriza��o de Servi�o
                    </c:if>
                    <c:if test="${ fluxoDetalhamento eq true }">
                        Detalhamento da Autoriza��o de Servi�o
                    </c:if>

                    <c:if test="${ fluxoAlteracao eq true }">
                        Alterar Autoriza��o de Servi�o
                    </c:if>

                    <c:if test="${ fluxoRemanejar eq true }">
                        Remanejar Autoriza��o de Servi�o
                    </c:if>

                    <c:if test="${ fluxoExecucao eq true }">
                        Executar Autoriza��o de Servi�o
                    </c:if>

                    <c:if test="${ fluxoEncerramento eq true }">
                        Encerrar Autoriza��o de Servi�o
                    </c:if>
                </h5>
            </div>

            <div class="card-body">

                <c:if test="${ fluxoInclusao eq true }">
                    <div class="alert alert-primary" role="alert">
                        <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                        <div class="d-inline">
                            Altere os dados e clique em <strong>Salvar</strong> para salvar a Autoriza��o. <br/>
                            Para cancelar clique em <strong>Cancelar</strong>.
                        </div>
                    </div>
                </c:if>
                <c:if test="${ fluxoDetalhamento eq true }">
                    <div class="alert alert-primary" role="alert">
                        <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                        <div class="d-inline">
                            Para modificar as informa��es deste registro clique em <strong>Alterar</strong> para alterar a Autoriza��o.
                        </div>
                    </div>
                </c:if>

                <c:if test="${ fluxoAlteracao eq true }">
                    <div class="alert alert-primary" role="alert">
                        <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                        <div class="d-inline">
                            Para modificar as informa��es deste registro clique em <strong>Salvar</strong> para alterar a Autoriza��o.
                        </div>
                    </div>
                </c:if>

                <c:if test="${ fluxoRemanejar eq true }">
                    <div class="alert alert-primary" role="alert">
                        <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                        <div class="d-inline">
                            Informe os dados abaixo e clique em <strong>Salvar</strong>.
                        </div>
                    </div>
                </c:if>

                <c:if test="${ fluxoExecucao eq true }">
                    <div class="alert alert-primary" role="alert">
                        <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                        <div class="d-inline">
                            Informe os dados abaixo e clique em <strong>Executar</strong>.
                        </div>
                    </div>
                </c:if>

                <c:if test="${ fluxoEncerramento eq true }">
                    <div class="alert alert-primary" role="alert">
                        <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                        <div class="d-inline">
                            Informe os dados abaixo e clique em <strong>Encerrar</strong>.
                        </div>
                    </div>
                </c:if>

                <input type="hidden" id="imovel" name="imovel" value="${servicoAutorizacao.imovel.chavePrimaria}"/>
                <input type="hidden" id="pontoConsumo" name="pontoConsumo" value="${servicoAutorizacao.pontoConsumo.chavePrimaria}"/>
                <input type="hidden" id="medidor" name="medidor" value="${servicoAutorizacao.medidor.chavePrimaria}"/>
                
                <c:if test="${fluxoInclusao eq null }">
					<input type="hidden" id="servicoTipo" name="servicoTipo" value="${servicoAutorizacao.servicoTipo.chavePrimaria}"/>
				</c:if>
				
				
                <div class="row">
                    <div class="col-md-12" id="incAltServicoTipoCol1"> <!-- Talvez id seja desnecess�rio... -->
                        <c:if test="${fluxoInclusao ne true}">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for=chavePrimaria>C�digo:</label>
                                    <input class="form-control form-control-sm" type="text" disabled="disabled" name="chavePrimaria"
                                           id="chavePrimaria"
                                           maxlength="10" size="10" value="${servicoAutorizacao.chavePrimaria}"/>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true}">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="dataGeracao">Data de gera��o:</label>
                                    <input class="form-control form-control-sm" type="text" id="dataGeracao" name="dataGeracao" size="10"
                                           value="${dataGeracao}">
                                </div>
                            </div>
                        </c:if>

                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="descricao">Descri��o: <span class="text-danger">*</span></label>
                                    <textarea class="form-control form-control-sm" name="descricao" id="descricao" cols="35" rows="6"
                                              onblur="this.value=removerEspacoInicialFinal(this.value);">${servicoAutorizacao.descricao}</textarea>
                                </div>
                            </div>

		                <div class="row">
		                
		                	<c:set var="resultado" value="" />
	                        <c:choose>
		                        <c:when test="${servicoAutorizacao.chamado ne null}">
	                    			<c:set var="resultado" value="${resultado}${servicoAutorizacao.chamado.protocolo.numeroProtocolo}" />
		                        </c:when>
	                        	<c:otherwise>
	                    			<c:set var="resultado" value="${resultado}${servicoAutorizacao.numeroProtocolo}" />	                        	
	                        	</c:otherwise>
	                        </c:choose>		                	
	                        <c:if test="${not empty resultado && servicoAutorizacao.chamado.chamadoAssunto.chamadoTipo.descricao ne null }">
                    			<c:set var="resultado" value="${resultado} - ${servicoAutorizacao.chamado.chamadoAssunto.chamadoTipo.descricao}" />
	                        </c:if>
	                        <c:if test="${not empty resultado && servicoAutorizacao.servicoTipo.descricao ne null}">
                    			<c:set var="resultado" value="${resultado} - ${servicoAutorizacao.servicoTipo.descricao}" />
	                        </c:if>
                
		                    <div class="col-md-12">
		                        <div class="form-row">
		                            <div class="col-md-12">
		                                <label for="numeroProtocolo">N�mero do Protocolo:</label>
		                                <input type="text" class="form-control form-control-sm" id="numeroProtocolo"
		                                       name="numeroProtocolo" placeholder="N�mero do protocolo"
		                                       onkeypress="return formatarCampoInteiro(event);"
		                                       maxlength="10" size="10" 
								                            <c:choose>
						                                    <c:when test="${servicoAutorizacao.chamado ne null}">
						                                    		value="${resultado}"
<%-- 																value= "<c:out value="${servicoAutorizacao.chamado.protocolo.numeroProtocolo}"/>" --%>
						                                    </c:when>
						                                    <c:otherwise>
<%--                                     						value= "<c:out value="${servicoAutorizacao.numeroProtocolo}"/>" --%>
																value="${resultado}"
															</c:otherwise>
                                							</c:choose>>
		                            </div>
		                        </div>
		
		                    </div>
		                </div>
		                
                        <div class="form-row">
                        
						<c:if test="${fluxoInclusao ne null }">
							<div class="col-md-12">
							    <label for="servicoTipo">Tipo de Servi�o: <span id="spanServicoTipo" class="text-danger">*</span></label>
							    <c:choose>
							        <c:when test="${fluxoDetalhamento}">
							            <input type="text" class="form-control form-control-sm" disabled 
							                   value="${servicoAutorizacao.servicoTipo.descricao}" name="servicoTipo"/>
							        </c:when>
							        <c:otherwise>
							            <select name="servicoTipo" id="servicoTipo" class="form-control form-control-sm" 
							                    onchange="carregarPrioridade(this.value);carregarOrientacao(this.value);carregarChaveEquipePrioritaria(this.value);verificarNecessidadeNovoMedidor(this.value);carregarTurnosEquipe();">
							                <option value="-1">Selecione</option>
							                <c:forEach items="${listaTipoServico}" var="servico">
							                    <option value="<c:out value='${servico.chavePrimaria}'/>"
							                            <c:if test="${servicoAutorizacao.servicoTipo.chavePrimaria == servico.chavePrimaria}">selected="selected"</c:if>>
							                        <c:out value="${servico.descricao}"/>
							                    </option>
							                </c:forEach>
							            </select>
							        </c:otherwise>
							    </c:choose>
							</div>
						</c:if>
						


                        </div>
                        		                
		                
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-row">
		                            <div class="col-md-12">
		                                <label for="numeroOS">N�mero da OS:</label>
		                                <input type="text" class="form-control form-control-sm" id="numeroOS"
		                                       name="numeroOS" placeholder="N�mero da OS"
		                                       onkeypress="return formatarCampoInteiro(event);"
		                                       maxlength="10" size="10" value="${servicoAutorizacao.numeroOS}">
		                            </div>
		                        </div>
		
		                    </div>
		                </div>		                
		                

                        <div class="form-row" id="inputServicoTipoPrioridade">
                            <jsp:include
                                    page="/jsp/atendimento/servicoautorizacao/bootstrap/inputServicoTipoPrioridadeBootstrap.jsp"></jsp:include>

                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="equipe">Equipe: <span id="spanEquipe" class="text-danger">*</span></label>
                                <c:choose>
                                    <c:when test="${fluxoDetalhamento}">
                                        <input type="text" class="form-control form-control-sm" disabled
                                               value="${servicoAutorizacao.equipe.nome}" name="equipe"/>
                                    </c:when>
                                    <c:otherwise>
                                        <select name="equipe" id="equipe" class="form-control form-control-sm" onchange="carregarTurnosEquipe();">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaEquipe}" var="equipe">
                                                <option value="<c:out value="${equipe.chavePrimaria}"/>"
                                                        <c:if test="${servicoAutorizacao.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${equipe.nome}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            
                            <div class="col-md-6">
                                <label for="equipe">Turno Prefer�ncia: <span id="spanEquipe" class="text-danger">*</span></label>
                                <c:choose>
                                    <c:when test="${fluxoDetalhamento}">
                                        <input type="text" class="form-control form-control-sm" disabled
                                               value="${servicoAutorizacao.turnoPreferencia.descricao}" name="turnoPreferencia"/>
                                    </c:when>
                                    <c:otherwise>
                                        <select name="turnoPreferencia" id="turnoPreferencia" class="form-control form-control-sm">
                                            <option value="-1">Selecione</option>
                                        </select>
                                    </c:otherwise>
                                </c:choose>
                            </div>                            
                        </div>


                        <c:if test="${ fluxoEncerramento eq true || fluxoExecucao eq true || fluxoDetalhamento eq true}">
                            <div class="form-row">
                                <c:if test="${fluxoDetalhamento ne true}">
                                    <div class="col-md-6">
                                        <label for="executante">Unidade Organizacional:</label>
                                        <select name="unidadeOrganizacional" id="unidadeOrganizacional" class="form-control form-control-sm"
                                                onchange="carregarFuncionarios()">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaUnidadeOrganizacional}" var="unidade">
                                                <option value="<c:out value="${unidade.chavePrimaria}"/>"
                                                        <c:if test="${unidadeOrganizacional == unidade.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${unidade.descricao}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </c:if>
                                <div class="col-md-6">
                                    <label id="rotuloExecutante" for="executante">Executante:</label>
                                    <c:choose>
                                        <c:when test="${fluxoDetalhamento}">
                                            <input type="text" name="executante" class="form-control form-control-sm"
                                                   value="${servicoAutorizacao.executante.nome}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <select name="executante" id="executante" class="form-control form-control-sm">
                                                <option value="-1">Selecione</option>
                                                <c:forEach items="${listaExecutante}" var="executante">
                                                    <option value="<c:out value="${executante.chavePrimaria}"/>"
                                                            <c:if test="${servicoAutorizacao.executante.chavePrimaria == executante.chavePrimaria}">selected="selected"</c:if>>
                                                        <c:out value="${executante.nome}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </c:if>

                        <div class="form-row">
                            <div class="col-md-6">
                                <label id="rotuloIntervaloCadastroDocumentos" for="dataSolicitacaoRamal">Data de Solicita��o de
                                    Ramal:</label>

                                <div class="input-group input-group-sm">

                                    <input type="text" aria-label="Data de Solicita��o de Ramal"
                                           class="form-control form-control-sm"
                                           onblur="validaData(this);validardataAtual(this);" id="dataSolicitacaoRamal"
                                           name="dataSolicitacaoRamal" maxlength="10"
                                           value="${dataSolicitacaoRamal}">

                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-12">
                                        <label id="labelDataExecucao" for="dataExecucao">Data de Execu��o: </label>
                                        <div class="input-group input-group-sm">
                                            <input class="form-control form-control-sm" type="text" id="dataExecucao" name="dataExecucao"
                                                   size="16"
                                                   maxlength="16"
                                                   value="<fmt:formatDate value="${servicoAutorizacao.dataExecucao}" pattern="dd/MM/yyyy HH:mm"/>">
                                        </div>
                                    </div>


                                    <c:if test="${fluxoExecucao eq true and servicoAutorizacao.servicoTipo.formulario ne null }">
                                        <div class="col-xl-6 col-lg-12">
                                            <label>&nbsp;</label>
                                            <c:choose>
                                                <c:when test="${sessionScope.listaPerguntas ne null}">
                                                    <button type="button" class="btn btn-sm btn-success btn-block" disabled="true" id="responderFormulario">
                                                        <i class="fa fa-check"></i> <span>Formul�rio Respondido!</span>
                                                    </button>
                                                </c:when>
                                                <c:otherwise>
                                                    <button class="btn btn-sm btn-primary btn-block" id="responderFormulario">
                                                        <i class="fa fa-file-signature"></i> <span>Responder Formul�rio</span>
                                                    </button>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                        </div>

                        <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoExecucao eq true}">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label>Status:</label>
                                    <input type="text" class="form-control form-control-sm" disabled="disabled"
                                           value="${servicoAutorizacao.status.descricao}">
                                </div>
                            </div>
                        </c:if>

                        <div class="form-row">
                            <c:if test="${ fluxoDetalhamento eq true || fluxoEncerramento eq true || (fluxoExecucao eq true && servicoAutorizacao.servicoTipo.indicadorEncerramentoAuto eq true)}">
                                <div class="col-md-12" id="divMotivo">
                                    <label id="rotuloMotivo">Motivo: <span id="spanMotivo" class="text-danger">*</span></label>
                                    <select name="servicoAutorizacaoMotivoEncerramento" id="servicoAutorizacaoMotivoEncerramento"
                                            class="form-control form-control-sm">
                                        <option value="-1">Selecione</option>
                                        <c:forEach items="${listaMotivoEncerramento}" var="motivo">
                                            <option value="<c:out value="${motivo.chavePrimaria}"/>"
                                                    <c:if test="${servicoAutorizacao.servicoAutorizacaoMotivoEncerramento.chavePrimaria == motivo.chavePrimaria}">selected="selected"</c:if>>
                                                <c:out value="${motivo.descricao}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </c:if>

                            <c:if test="${ (fluxoDetalhamento eq true && servicoAutorizacao.servicoTipo.indicadorEncerramentoAuto ne true) || (fluxoExecucao eq true && servicoAutorizacao.servicoTipo.indicadorEncerramentoAuto ne true)}">
                                <div class="col-md-12">
                                    <label>Execu��o conclu�da com sucesso:</label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input" type="radio" name="indicadorSucessoExecucao"
                                               id="indicadorSucessoExecucaoSim"
                                               value="true"
                                               <c:if test="${servicoAutorizacao.indicadorSucessoExecucao eq 'true'}">checked</c:if>>
                                        <label class="custom-control-label" for="indicadorSucessoExecucaoSim">Sim</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input" type="radio" name="indicadorSucessoExecucao"
                                               id="indicadorSucessoExecucaoNao"
                                               value="false"
                                               <c:if test="${servicoAutorizacao.indicadorSucessoExecucao eq 'false'}">checked</c:if>>
                                        <label class="custom-control-label" for="indicadorSucessoExecucaoNao">N�o</label>
                                    </div>
                                </div>
                            </c:if>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="orientacaoServicoTipo">Observa��o/Orienta��o:</label>
                                <textarea class="form-control form-control-sm" name="orientacaoServicoTipo" id="orientacaoServicoTipo"
                                          cols="60" rows="6"
                                          maxlength="800" disabled="disabled"><c:out
                                        value="${servicoAutorizacao.servicoTipo.orientacao}"/></textarea>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="card mt-2">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab"
                                   href="#contentTabCliente" role="tab" aria-controls="contentTabCliente"
                                   aria-selected="true"><i class="fa fa-user-tie"></i> Cliente</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab"
                                   href="#contentTabImovel" role="tab" aria-controls="contentTabImovel"
                                   aria-selected="false"><i class="fa fa fa-hotel"></i>Im�vel</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab"
                                   href="#contentTabMedidor" role="tab" aria-controls="contentTabMedidor"
                                   aria-selected="false"><i class="fa fa fa-wrench"></i>Novo Medidor</a>
                            </li>
                            
                            <c:if test="${(fluxoDetalhamento eq true || fluxoAlteracao eq true) && servicoAutorizacao.servicoAutorizacaoTemp ne null }">
                             <li class="nav-item">
                            	<a class="nav-link" data-toggle="tab"
                                   href="#contentTabMobile" role="tab" aria-controls="contentTabMobile"
                                   aria-selected="false"><i class="fa fa fa-mobile"></i>Dados Mobile</a>
                              </li>
                            </c:if>
                            
                                                       
                            
                            <c:if test="${fluxoDetalhamento ne true}">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab"
                                       href="#contentTabDocumento" role="tab" aria-controls="contentTabDocumento"
                                       aria-selected="false"><i class="fa fa-layer-group"></i> Documentos</a>
                                </li>
                            </c:if>
                            <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab"
                                       href="#contentTabMateriaisNecessarios" role="tab" aria-controls="contentTabMateriaisNecessarios"
                                       aria-selected="false"><i class="fa fa-archive"></i> Materiais Necess�rios</a>
                                </li>
                            </c:if>
                            <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab"
                                       href="#contentTabEquipamentoEspecialNecessario" role="tab"
                                       aria-controls="contentTabEquipamentoEspecialNecessario"
                                       aria-selected="false"><i class="fa fa-wrench"></i> Equipamento Especial Necess�rio</a>
                                </li>
                            </c:if>

                            <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoEncerramento eq true || fluxoExecucao eq true}">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab"
                                       href="#contentTabHistoricoAutorizacaoServico" role="tab"
                                       aria-controls="contentTabHistoricoAutorizacaoServico"
                                       aria-selected="false"><i class="fa fa-history"></i> Hist�rico de Autoriza��o de Servi�o</a>
                                </li>
                            </c:if>

                            <c:if test="${ fluxoDetalhamento eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab"
                                       href="#contentTabHistoricoChamado" role="tab" aria-controls="contentTabHistoricoChamado"
                                       aria-selected="false"><i class="fa fa-clipboard-list"></i> Hist�rico do Chamado</a>
                                </li>
                            </c:if>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="contentTabCliente" role="tabpanel"
                                 aria-labelledby="contentTabCliente">
                                <div class="form-row">
                                    <div class="col-md-12" id="divContrato">
                                        <jsp:include
                                                page="/jsp/atendimento/servicoautorizacao/bootstrap/divContratoBootstrap.jsp"></jsp:include>
                                    </div>
                                </div>
                                <div id="divCliente">
                                    <jsp:include page="/jsp/atendimento/servicoautorizacao/bootstrap/abaClienteBootstrap.jsp"></jsp:include>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabImovel" role="tabpanel" aria-labelledby="contentTabImovel">
                                <jsp:include page="/jsp/atendimento/servicoautorizacao/bootstrap/abaImovelBootstrap.jsp"></jsp:include>
                            </div>
                            
                           
	                            <div class="tab-pane fade" id="contentTabMedidor" role="tabpanel" aria-labelledby="contentTabMedidor">
	                            	<div id="divMedidor">
	                                	<jsp:include page="/jsp/atendimento/servicoautorizacao/bootstrap/abaMedidorBootstrap.jsp"></jsp:include>
	                                </div>
	                                
	                            </div>
                          
                            
                             <c:if test="${(fluxoDetalhamento eq true || fluxoAlteracao eq true) && servicoAutorizacao.servicoAutorizacaoTemp ne null }">
	                            <div class="tab-pane fade" id="contentTabMobile" role="tabpanel" aria-labelledby="contentTabMobile">
	                            	<div id="divMobile">
	                                	<jsp:include page="/jsp/atendimento/servicoautorizacao/bootstrap/abaMobileBootstrap.jsp"></jsp:include>
	                                </div>
	                                
	                            </div>
                             </c:if>
                                                        
                            <c:if test="${fluxoDetalhamento ne true}">
                                <div class="tab-pane fade" id="contentTabDocumento" role="tabpanel" aria-labelledby="contentTabDocumento">
                                    <jsp:include
                                            page="/jsp/atendimento/comumchamadoservicoautorizacao/bootstrap/selecionarAnexosBootstrap.jsp">
                                        <jsp:param name="actionAdicionarAnexo" value="adicionarAnexoServicoAutorizacao"/>
                                        <jsp:param name="actionRemoverAnexo" value="removerAnexoServicoAutorizacao"/>
                                        <jsp:param name="actionVisualizarAnexo" value="visualizarAnexoServicoAutorizacao"/>
                                        <jsp:param name="actionLimparCampoArquivo" value="limparCampoArquivoServicoAutorizacao"/>
                                        <jsp:param name="nomeForm" value="servicoAutorizacaoForm"/>
                                    </jsp:include>
                                </div>
                            </c:if>

                            <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                <div class="tab-pane fade" id="contentTabMateriaisNecessarios" role="tabpanel"
                                     aria-labelledby="contentTabMateriaisNecessarios">
                                    <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                        <div id="gridMateriaisServicoAutorizacao">
                                            <jsp:include
                                                    page="/jsp/atendimento/servicoautorizacao/bootstrap/gridMateriaisBootstrap.jsp"></jsp:include>
                                        </div>
                                    </c:if>
                                </div>
                            </c:if>

                            <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                <div class="tab-pane fade" id="contentTabEquipamentoEspecialNecessario" role="tabpanel"
                                     aria-labelledby="contentTabEquipamentoEspecialNecessario">

                                    <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">

                                        <div id="gridEquipamentosServicoAutorizacao">
                                            <jsp:include
                                                    page="/jsp/atendimento/servicoautorizacao/bootstrap/gridEquipamentosBootstrap.jsp"></jsp:include>
                                        </div>
                                    </c:if>

                                    </fieldset>
                                </div>
                            </c:if>

                            <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoEncerramento eq true || fluxoExecucao eq true}">
                                <div class="tab-pane fade" id="contentTabHistoricoAutorizacaoServico" role="tabpanel"
                                     aria-labelledby="contentTabHistoricoAutorizacaoServico">
                                    <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoRemanejar eq true || fluxoEncerramento eq true || fluxoExecucao eq true}">

                                        <div id="gridServicoAutorizacaoHistorico">
                                            <jsp:include
                                                    page="/jsp/atendimento/servicoautorizacao/bootstrap/gridServicoAutorizacaoHistoricoBootstrap.jsp"></jsp:include>
                                        </div>
                                        <hr/>
                                        <jsp:include
                                                page="/jsp/atendimento/servicoautorizacao/bootstrap/gridServicoAutorizacaoHistoricoAnexoBootstrap.jsp"></jsp:include>
                                    </c:if>
                                </div>
                            </c:if>

                            <c:if test="${ fluxoDetalhamento eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                <div class="tab-pane fade" id="contentTabHistoricoChamado" role="tabpanel"
                                     aria-labelledby="contentTabHistoricoChamado">
                                    <c:if test="${ fluxoDetalhamento eq true || fluxoExecucao eq true || fluxoEncerramento eq true}">
                                        <jsp:include
                                                page="/jsp/atendimento/chamado/bootstrap/gridChamadoHistoricoBootstrap.jsp"></jsp:include>
                                        <hr/>
                                        <jsp:include
                                                page="/jsp/atendimento/chamado/bootstrap/gridChamadoHistoricoAnexoBootstrap.jsp"></jsp:include>
                                    </c:if>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <c:if test="${ (fluxoExecucao eq true and servicoAutorizacao.servicoTipo.indicadorEncerramentoAuto ) or fluxoEncerramento eq true}">
                    <c:if test="${servicoAutorizacao.chamado ne null and servicoAutorizacao.chamado.chamadoAssunto.indicadorVerificarVazamento eq true}">

                        <div class="form-row">
                            <div class="col-md-12">
                                <label>Vazamento confirmado? <span class="text-danger">*</span></label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input" type="radio" name="indicadorVazamentoConfirmado"
                                           id="indicadorVazamentoConfirmadoSim"
                                           value="true">
                                    <label class="custom-control-label" for="indicadorVazamentoConfirmadoSim">Sim</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input" type="radio" name="indicadorVazamentoConfirmado"
                                           id="indicadorVazamentoConfirmadoNao"
                                           value="false">
                                    <label class="custom-control-label" for="indicadorVazamentoConfirmadoNao">N�o</label>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </c:if>


            </div>


            <div class="card-footer">
                <div class="form-row mt-2">
                    <div class="col-md-12">

                        <c:if test="${fluxoDetalhamento eq true && fluxoPesquisaSatisfacao ne true}">
                            <div class="row justify-content-between">

                                <div class="col-sm-6 mt-1">
                                    <button type="button"
                                            class="btn btn-secondary btn-sm acoes-cancelar"
                                            onclick="voltar();"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-chevron-circle-left"></i> Voltar
                                    </button>
                                </div>

                                <div class="col-sm-6 mt-1">
                                    <jsp:include page="/jsp/atendimento/servicoautorizacao/botoesServicoAutorizacao.jsp"></jsp:include>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoInclusao eq true }">
                            <div class="row justify-content-between">

                                <div class="col-sm-6 mt-1">
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDropInclusaoCancelar" type="button"
                                                class="btn btn-danger btn-sm dropdown-toggle acoes-cancelar"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-times"></i> A��es
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDropInclusaoCancelar">
                                            <input id="botaoCancelar" name="Button" class="dropdown-item" value="Cancelar" type="button"
                                                   onClick="cancelar();">
                                            <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                                   onclick="limparFormularioInclusao();">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <vacess:vacess param="incluirServicoAutorizacao">
                                        <button name="button" id="btnSalvar"
                                                class="btn btn-primary btn-sm float-right"
                                                type="button"
                                                onclick="incluir();">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoRemanejar eq true }">
                            <div class="row justify-content-between">

                                <div class="col-sm-6 mt-1">
                                    <div class="btn-group" role="group">
                                        <button type="button"
                                                class="btn btn-danger btn-sm dropdown-toggle acoes-cancelar"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-times"></i> A��es
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDropInclusaoCancelar">
                                            <input name="Button" type="button" id="cancelarAlteracao" class="dropdown-item"
                                                   style="float: left;" value="Cancelar" onClick="cancelar();">
                                            <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                                   onclick="limparFormularioRemanejar();">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mt-1">
                                    <vacess:vacess param="remanejarServicoAutorizacao">
                                        <button name="button" id="botaoRemanejar"
                                                class="btn btn-primary btn-sm float-right"
                                                onclick="remanejarServicoAutorizacao()" type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoEncerramento eq true }">
                            <div class="row justify-content-between">

                                <div class="col-sm-6 mt-1">
                                    <div class="btn-group" role="group">
                                        <button type="button"
                                                class="btn btn-danger btn-sm dropdown-toggle acoes-cancelar"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-times"></i> A��es
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDropInclusaoCancelar">
                                            <input name="Button" class="dropdown-item" value="Cancelar" type="button" onClick="cancelar();">
                                            <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                                   onclick="limparFormulario();">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mt-1">
                                    <vacess:vacess param="encerrarServicoAutorizacao">
                                        <button name="botaoEncerrar" id="btnSalvar"
                                                class="btn btn-primary btn-sm float-right"
                                                onclick="encerrarServicoAutorizacao()" type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoAlteracao eq true}">

                            <div class="row justify-content-between">

                                <div class="col-sm-6 mt-1">
                                    <div class="btn-group" role="group">
                                        <button type="button"
                                                class="btn btn-danger btn-sm dropdown-toggle acoes-cancelar"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-times"></i> A��es
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDropInclusaoCancelar">
                                            <input name="Button" type="button" id="botaoCancelar" class="dropdown-item" value="Cancelar"
                                                   style="float: left;" onclick="cancelar();">
                                            <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                                   onclick="limparFormularioAlteracao();">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mt-1">

                                    <vacess:vacess param="alterarServicoAutorizacao">
                                        <button name="Button" type="button"
                                                class="btn btn-primary btn-sm float-right"
                                                id="btnSalvar"
                                                onclick="alterarServicoAutorizacao();">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoExecucao eq true }">

                            <div class="row justify-content-between">

                                <div class="col-sm-6 mt-1">
                                    <div class="btn-group" role="group">
                                        <button type="button"
                                                class="btn btn-danger btn-sm dropdown-toggle acoes-cancelar"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-times"></i> A��es
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDropInclusaoCancelar">
                                            <input name="Button" class="dropdown-item" value="Cancelar" type="button" onClick="cancelar();">
                                            <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                                   onclick="limparFormulario();">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mt-1">

                                    <vacess:vacess param="executarServicoAutorizacao">
                                        <button name="buttonExecutar" value="Salvar" id="btnSalvar"
                                                class="btn btn-primary btn-sm float-right"
                                                onclick="executarServicoAutorizacao()" type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>


                        </c:if>

                    </div>
                </div>
            </div>

        </div>
    </form:form>
    <jsp:include page="../questionario/bootstrap/modalResponderFormulario.jsp" />
</div>
<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/autorizacaoServico/exibirInclusaoServicoAutorizacao/index.js"
        type="application/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/autorizacaoServico/exibirInclusaoServicoAutorizacao/index.js"
        type="application/javascript" charset="utf-8"></script>
