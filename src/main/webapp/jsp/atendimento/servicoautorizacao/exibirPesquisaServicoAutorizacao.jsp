<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $(".campoData").datepicker({changeYear: true, maxDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
        $(".campoData2").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.png"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

        $("#cpfCliente").inputmask("999.999.999-99", {placeholder: "_"});
        $("#cnpjCliente").inputmask("99.999.999/9999-99", {placeholder: "_"});
        $("#cpfSolicitante").inputmask("999.999.999-99", {placeholder: "_"});
        $("#cnpjSolicitante").inputmask("99.999.999/9999-99", {placeholder: "_"});
        $("#cepPontoConsumo").inputmask("99999-999", {placeholder: "_"});


        document.forms[0].servicoAtraso[2].checked = true;

        var siglaUnidadeFederativa = '<c:out value="${servicoAutorizacaoVO.siglaUnidadeFederacaoPontoConsumo}" />';
        carregarMunicipios(siglaUnidadeFederativa);
        var nomeMunicipio = '<c:out value="${servicoAutorizacaoVO.nomeMunicipioPontoConsumo}" />';
        carregarBairros(nomeMunicipio);
        var nomeBairro = '<c:out value="${servicoAutorizacaoVO.bairroPontoConsumo}" />';
        $("#nomeMunicipioPontoConsumo").val(nomeMunicipio);
        $("#bairroPontoConsumo").val(nomeBairro);

        var chaveChamadoTipo = $("#idTipoChamado").val();
        carregarChamadoAssunto(chaveChamadoTipo);
    });
    
    function exibirDetalhamento(chave) {
        document.forms[0].chavePrimaria.value = chave;
        submeter('servicoAutorizacaoForm', 'exibirDetalhamentoServicoAutorizacao');
    }

    function exibirAlterarServicoAutorizacao() {
        var selecao = verificarSelecaoApenasUm();
        if (selecao == true) {
            document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
            submeter('servicoAutorizacaoForm', 'exibirAlteracaoServicoAutorizacao');
        }

    }

    function exibirExecutarServicoAutorizacao() {
        var selecao = verificarSelecaoApenasUm();
        if (selecao == true) {
            document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
            submeter('servicoAutorizacaoForm', 'exibirExecutarServicoAutorizacao');
        }
    }

    function exibirRemanejarServicoAutorizacao() {
        var selecao = verificarSelecaoApenasUm();
        if (selecao == true) {
            document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
            submeter('servicoAutorizacaoForm', 'exibirRemanejarServicoAutorizacao');
        }
    }


    function exibirEncerrarServicoAutorizacao() {
        var selecao = verificarSelecaoApenasUm();
        if (selecao == true) {
            document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
            submeter('servicoAutorizacaoForm', 'exibirEncerrarServicoAutorizacao');
        }
    }


    function imprimirServicoAutorizacao() {
       	var selecao = verificarSelecao();
        if (selecao == true) {
            document.forms["servicoAutorizacaoForm"].idServicoAutorizacao.value = obterValorUnicoCheckboxSelecionado();
            if ($($("input[name=chavesPrimarias]:checked")[0]).parents("tr").find(".protocolo").text().trim() != "") {
                $('#modal-imprimir').modal('show');
            } else {
                $("#comChamado").val("false");
                submeter('servicoAutorizacaoForm', 'imprimirServicosAutorizacao');
            }
        }
    }

    function confirmarImpressao(comChamado) {
        $("#comChamado").val(comChamado);
        submeter('servicoAutorizacaoForm', 'imprimirServicosAutorizacao');
    }

    function alterarServicoAutorizacaoEmExecucao() {
        var selecao = verificarSelecao();
        if (selecao == true) {
            var retorno = confirm('Deseja alterar o status das autoriza��es de servi�o para Em Execu��o?');
            if (retorno == true) {
                submeter('servicoAutorizacaoForm', 'alterarServicoAutorizacaoEmExecucao');
            }
        }
    }


    function incluir() {
        submeter('servicoAutorizacaoForm', 'exibirInclusaoServicoAutorizacao');
    }

    function limparFormulario() {
        limparFormularios(document.servicoAutorizacaoForm);
        $("#chavePrimaria").val("");
        document.forms['servicoAutorizacaoForm'].condominioImovel[2].checked = true;
        document.forms['servicoAutorizacaoForm'].servicoAtraso[2].checked = true;
        document.forms['servicoAutorizacaoForm'].listaStatus[0].checked = "checked";
        document.forms['servicoAutorizacaoForm'].listaStatus[4].checked = "checked";
        $("#numeroProtocoloChamado").val("");
        $("#numeroImovel").val("");


    }

    function pesquisar() {
        submeter('servicoAutorizacaoForm', 'pesquisarServicoAutorizacao');
    }

    animatedcollapse.addDiv('mostrarFormChamado', 'fade=0,speed=400,group=chamado,persist=1,hide=0');
    animatedcollapse.addDiv('mostrarFormImovel', 'fade=0,speed=400,group=imovel,persist=1,hide=0');
    animatedcollapse.addDiv('mostrarFormPontoConsumo', 'fade=0,speed=400,group=pontoConsumo,persist=1,hide=0');
    animatedcollapse.addDiv('mostrarFormContrato', 'fade=0,speed=400,group=contrato,persist=1,hide=0');
    animatedcollapse.addDiv('mostrarFormPessoaFisica', 'fade=0,speed=400,group=pessoaFisica,persist=1,hide=0');
    animatedcollapse.addDiv('mostrarFormPessoaJuridica', 'fade=0,speed=400,group=pessoaJuridica,persist=1,hide=0');

    function carregarMunicipios(siglaUnidadeFederativa) {

        var selectMunicipio = document.getElementById("nomeMunicipioPontoConsumo");

        selectMunicipio.length = 0;
        var novaOpcao = new Option("Selecione", "");
        selectMunicipio.options[selectMunicipio.length] = novaOpcao;

        if (siglaUnidadeFederativa != "") {
            selectMunicipio.disabled = false;
            $("#nomeMunicipioPontoConsumo").removeClass("campoDesabilitado");
            AjaxService.consultarMunicipiosPorUnidadeFederativaPorSigla(siglaUnidadeFederativa,
                {
                    callback: function (listaMunicipio) {
                        for (key in listaMunicipio) {
                            var novaOpcao = new Option(key, key);
                            selectMunicipio.options[selectMunicipio.length] = novaOpcao;
                        }
                    }, async: false
                }
            );

        } else {
            //selectMunicipio.disabled = true;
            //$("#nomeMunicipioPontoConsumo").addClass("campoDesabilitado");
        }
    }

    function carregarBairros(nomeMunicipio) {

        var selectBairro = document.getElementById("bairroPontoConsumo");

        selectBairro.length = 0;
        var novaOpcao = new Option("Selecione", "");
        selectBairro.options[selectBairro.length] = novaOpcao;

        if (nomeMunicipio != "") {
            selectBairro.disabled = false;
            $("#bairroPontoConsumo").removeClass("bairroPontoConsumo");
            AjaxService.consultarBairrosPorMunicipio(nomeMunicipio,
                {
                    callback: function (listaBairro) {
                        for (key in listaBairro) {
                            var novaOpcao = new Option(key, listaBairro[key]);
                            selectBairro.options[selectBairro.length] = novaOpcao;
                        }
                    }, async: false
                }
            );

        } else {
            selectBairro.disabled = true;
        }
    }


    function toggleClass(id) {
        $(id).toggleClass('fa-caret-up');
        $(id).toggleClass('fa-caret-down');
    }
    
    function carregarChamadoAssunto(chaveChamadoTipo) {
        if (chaveChamadoTipo === undefined || chaveChamadoTipo <= 0) {
            $("#chamadoAssunto").empty();
            $("#chamadoAssunto").append('<option value="-1">Selecione</option>');
        } else {
            $('#chamadoAssunto').attr('disabled', true);
            $("#divAssunto").load("carregarChamadoAssuntoServico?chaveChamadoTipo=" + chaveChamadoTipo, function () {
                $('#chamadoAssunto').attr('disabled', false);
                var chamadoAssunto = '${servicoAutorizacaoVO.chamadoAssunto}'
                if(chamadoAssunto.length > 0){
                    $('#chamadoAssunto').val(chamadoAssunto).change();
                }
            });
            
        }
        
    }
    

</script>

<div class="bootstrap">
    <form:form action="pesquisarServicoAutorizacao" id="servicoAutorizacaoForm" name="servicoAutorizacaoForm"
               method="post" modelAttribute="ServicoAutorizacaoVO">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Pesquisar Autoriza��o de Servi�o</h5>
            </div>
            <div class="card-body">

                <%--<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">--%>
                <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
                <input name="idServicoAutorizacao" type="hidden" id="idServicoAutorizacao">
                <input name="comChamado" type="hidden" id="comChamado" value="false">


                <div class="alert alert-primary fade show" role="alert">
                    <i class="fa fa-question-circle"></i>
                    Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em
                    <b>Pesquisar</b>, ou clique apenas em <b>Pesquisar</b> para exibir todos. Para incluir um novo
                    registro clique em <b>Incluir</b>
                </div>

                <div class="card">
                    <div class="card-body bg-light">

                        <div class="row mb-2">
                            <div class="col-md-6">

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="chavePrimaria">C�digo</label>
                                        <input type="text" class="form-control form-control-sm" id="chavePrimaria"
                                               name="chavePrimaria" min="0" maxlength="10" size="10"
                                               onkeypress="return formatarCampoInteiro(event);"
                                               value="${servicoAutorizacaoVO.chavePrimaria}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="dataGeracaoInicio">Data de gera��o</label>
                                        <div class="input-group input-group-sm">

                                            <input type="text" aria-label="Data de Gera��o"
                                                   class="form-control form-control-sm campoData"
                                                   onblur="validaData(this);" id="dataGeracaoInicio"
                                                   name="dataGeracaoInicio" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataGeracaoInicio}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">at�</span>
                                            </div>
                                            <input type="text" aria-label="Data de Gera��o" id="dataGeracaoFim"
                                                   class="form-control form-control-sm campoData"
                                                   onblur="validaData(this);"
                                                   name="dataGeracaoFim" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataGeracaoFim}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="dataPrevisaoInicio">Data de previs�o de encerramento</label>
                                        <div class="input-group input-group-sm">

                                            <input id="dataPrevisaoInicio" type="text" aria-label="Data de previs�o de encerramento"
                                                   class="form-control form-control-sm campoData2"
                                                   onblur="validaData(this);"
                                                   name="dataPrevisaoInicio" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataPrevisaoInicio}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">at�</span>
                                            </div>
                                            <input type="text" aria-label="Data de previs�o de encerramento" id="dataPrevisaoFim"
                                                   class="form-control form-control-sm campoData2"
                                                   onblur="validaData(this);"
                                                   name="dataPrevisaoFim" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataPrevisaoFim}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="dataEncerramentoInicio">Data de encerramento</label>
                                        <div class="input-group input-group-sm">

                                            <input id="dataEncerramentoInicio" type="text" aria-label="Data de encerramento"
                                                   class="campoData2 form-control form-control-sm"
                                                   onblur="validaData(this);"
                                                   name="dataEncerramentoInicio" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataEncerramentoInicio}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">at�</span>
                                            </div>
                                            <input type="text" aria-label="Data de encerramento" id="dataEncerramentoFim"
                                                   class="form-control form-control-sm campoData2"
                                                   onblur="validaData(this);"
                                                   name="dataEncerramentoFim" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataEncerramentoFim}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="dataexecucacao">Data de execu��o</label>
                                        <div class="input-group input-group-sm">

                                            <input id="dataexecucacao" type="text" aria-label="Data de encerramento"
                                                   class="form-control form-control-sm campoData2"
                                                   onblur="validaData(this);"
                                                   name="dataExecucaoInicio" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataExecucaoInicio}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">at�</span>
                                            </div>
                                            <input type="text" aria-label="Data de encerramento"
                                                   class="form-control form-control-sm campoData2"
                                                   onblur="validaData(this);"
                                                   name="dataExecucaoFim" maxlength="10"
                                                   value="${servicoAutorizacaoVO.dataExecucaoFim}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="numeroProtocoloChamado">N�mero do protocolo</label>
                                        <input type="text" class="form-control form-control-sm" id="numeroProtocoloChamado"
                                               name="numeroProtocoloChamado" min="0" maxlength="10" size="10"
                                               onkeypress="return formatarCampoInteiro(event);"
                                               value="${servicoAutorizacaoVO.numeroProtocoloChamado}">
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="numeroOS">N�mero da OS</label>
                                        <input type="text" class="form-control form-control-sm" id="numeroOS"
                                               name="numeroOS" min="0" maxlength="10" size="10"
                                               onkeypress="return formatarCampoInteiro(event);"
                                               value="${servicoAutorizacaoVO.numeroOS}">
                                    </div>
                                </div>                                

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="servicoTipo">Tipo de servi�o</label>
                                        <select name="servicoTipo" id="servicoTipo" class="form-control form-control-sm">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaTipoServico}" var="servico">
                                                <option value="<c:out value="${servico.chavePrimaria}"/>"
                                                        <c:if test="${servicoAutorizacaoVO.servicoTipo.chavePrimaria == servico.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${servico.descricao}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="equipe">Equipe</label>
                                        <select name="equipe" id="equipe" class="form-control form-control-sm">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaEquipe}" var="equipe">
                                                <option value="<c:out value="${equipe.chavePrimaria}"/>"
                                                        <c:if test="${servicoAutorizacaoVO.equipe.chavePrimaria == equipe.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${equipe.nome}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <label class="col-md-12" for="statusservico">Status da Autoriza��o de Servi�o</label>
                                    <div class="col-md-12" id="statusservico">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input id="aberta" type="checkbox" name="listaStatus"
                                                   class="custom-control-input"
                                                   value="Aberta"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Aberta')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="aberta">Aberta</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" id="cancelada" name="listaStatus"
                                                   class="custom-control-input"
                                                   value="Cancelada"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Cancelada')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="cancelada">Cancelada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" id="encerrada" name="listaStatus"
                                                   class="custom-control-input"
                                                   value="Encerrada"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Encerrada')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="encerrada">Encerrada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" id="pendente" name="listaStatus" class="custom-control-input"
                                                   value="Pendente de Atualizacao"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Pendente de Atualizacao')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="pendente">Pendente de atualiza��o</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" id="em-execucacao" name="listaStatus"
                                                   class="custom-control-input" value="Em Execu��o"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Em Execu��o')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="em-execucacao">Em Execu��o</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" id="executada" name="listaStatus"
                                                   class="custom-control-input"
                                                   value="Executada"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'Executada')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="executada">Executado</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="servicoAutorizacaoMotivoEncerramento">Motivo de encerramento</label>

                                        <select name="servicoAutorizacaoMotivoEncerramento"
                                                id="servicoAutorizacaoMotivoEncerramento"
                                                class="form-control form-control-sm">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaMotivoEncerramento}" var="motivo">
                                                <option value="<c:out value="${motivo.chavePrimaria}"/>"
                                                        <c:if test="${servicoAutorizacaoVO.servicoAutorizacaoMotivoEncerramento ne null and servicoAutorizacaoVO.servicoAutorizacaoMotivoEncerramento.chavePrimaria == motivo.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${motivo.descricao}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <label class="col-md-12">Autorizar servi�o em atraso</label>
                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="abertaSim" name="servicoAtraso"
                                                   class="custom-control-input"
                                                   value="true"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'true')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="abertaSim">Sim</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="abertaNao" name="servicoAtraso"
                                                   class="custom-control-input"
                                                   value="false"
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'false')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="abertaNao">N�o</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="abertaTodos"  name="servicoAtraso"
                                                   class="custom-control-input" value=""
                                                   <c:if test="${fn:contains(servicoAutorizacaoVO.listaStatus,'')}">checked="checked"</c:if>>
                                            <label class="custom-control-label" for="abertaTodos">Todos</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="servicoTipoPrioridade">Prioridade do tipo de servi�o</label>

                                        <select name="servicoTipoPrioridade" id="servicoTipoPrioridade"
                                                class="form-control form-control-sm">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaServicoTipoPrioridade}" var="prioridade">
                                                <option value="<c:out value="${prioridade.chavePrimaria}"/>"
                                                        <c:if test="${servicoAutorizacaoVO.servicoTipoPrioridade.chavePrimaria == prioridade.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${prioridade.descricao}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="solicitante">Solicitante</label>
                                        <select name="solicitante" id="solicitante" class="form-control form-control-sm">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaSolicitantes}" var="solicitante">
                                                <option value="<c:out value="${solicitante.chavePrimaria}"/>"
                                                        <c:if test="${servicoAutorizacaoVO.solicitante.chavePrimaria == solicitante.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${solicitante.funcionario.nome}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="chamadoTipo">Tipo de Chamado</label>
                                        <select name="idTipoChamado" id="idTipoChamado" class="form-control form-control-sm"
                                        onchange="carregarChamadoAssunto(this.value);">
                                            <option value="-1">Selecione</option>
			                                    <c:forEach items="${listaChamadoTipo}" var="idTipoChamado">
			                                        <option value="<c:out value="${idTipoChamado.chavePrimaria}" />"
			                                        	<c:if test="${servicoAutorizacaoVO.idTipoChamado == idTipoChamado.chavePrimaria}">selected="selected"</c:if>>
                                                    	<c:out value="${idTipoChamado.descricao}"/>
			                                        </option>
			                                    </c:forEach>
                                        </select>
                                    </div>
                                </div>                          
                                <div class="form-row">
                                    <label for="chamadoAssunto">Assunto do Chamado</label>
                                    <div class="col-md-12" id="divAssunto">
										<select id="chamadoAssunto" class="form-control form-control-sm" name="chamadoAssunto">
										    <option value="-1">Selecione</option>
										    <c:forEach items="${listaChamadoAssunto}" var="assunto">
										        <option value="<c:out value="${assunto.chavePrimaria}"/>"
										            <c:if test="${servicoAutorizacaoVO.chamadoAssunto == assunto.chavePrimaria}">selected="selected"</c:if>>
                                                    	<c:out value="${assunto.descricao}"/>
										        </option>
										    </c:forEach>
										</select>                                    	                                       
                                    </div>
                                </div>      
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label>Filtro Cliente</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                                <div class="card-header p-0" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#pessoaFisica').toggleClass('show'); toggleClass('#caret-pessoa-fisica')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Pessoa f�sica <i id="caret-pessoa-fisica" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="pessoaFisica" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#pessoaFisica">
                                                    <div class="card-body">
                                                        <div class="form-row mb-1">
                                                            <label for="cpfCliente"
                                                                   class="col-sm-3  text-md-right">CPF</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control form-control-sm"
                                                                       id="cpfCliente" name="cpfCliente" maxlength="14"
                                                                       size="14"
                                                                       value="${servicoAutorizacaoVO.cpfCliente}">
                                                            </div>
                                                        </div>
                                                        <div class="form-row mb-1">
                                                            <label for="rgCliente"
                                                                   class="col-sm-3  text-md-right">RG</label>
                                                            <div class="col-sm-9">
                                                                <input type="number" class="form-control form-control-sm"
                                                                       name="rgCliente" maxlength="10" size="10" id="rgCliente"
                                                                       value="${servicoAutorizacaoVO.rgCliente}">
                                                            </div>
                                                        </div>
                                                        <div class="form-row mb-1">
                                                            <label for="nomeCliente"
                                                                   class="col-sm-3  text-md-right">Nome</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       class="form-control form-control-sm"
                                                                       id="nomeCliente" name="nomeCliente" maxlength="30"
                                                                       size="30" value="${servicoAutorizacaoVO.nomeCliente}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="numeroPassaporte"
                                                                   class="col-sm-3  text-md-right">Passaporte</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       class="form-control form-control-sm"
                                                                       id="numeroPassaporte" name="numeroPassaporte"
                                                                       maxlength="13"
                                                                       size="13"
                                                                       value="${servicoAutorizacaoVO.numeroPassaporte}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header p-0" id="headingTwo">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#pessoaJuridica').toggleClass('show'); toggleClass('#caret-pessoa-juridica')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Pessoa jur�dica <i id="caret-pessoa-juridica" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="pessoaJuridica" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#pessoaJuridica">
                                                    <div class="card-body">
                                                        <div class="form-row mb-1">
                                                            <label for="cnpjCliente"
                                                                   class="col-sm-3  text-md-right">CNPJ</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control form-control-sm"
                                                                       id="cnpjCliente" name="cnpjCliente" maxlength="18"
                                                                       size="18"
                                                                       value="${servicoAutorizacaoVO.cnpjCliente}">
                                                            </div>
                                                        </div>
                                                        <div class="form-row mb-1">
                                                            <label for="cnpjCliente"
                                                                   class="col-sm-3  text-md-right">Nome
                                                                fantasia</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       class="form-control form-control-sm"
                                                                       id="nomeFantasiaCliente" name="nomeFantasiaCliente"
                                                                       maxlength="30" size="30"
                                                                       value="${servicoAutorizacaoVO.nomeFantasiaCliente}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label>Filtro Im�vel</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion" id="accordionImovel">
                                            <div class="card" style="border: 1px solid rgba(0, 0, 0, 0.125);">
                                                <div class="card-header p-0" id="headingOneImovel">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#filtroImovel').toggleClass('show'); toggleClass('#caret-imovel')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Im�vel <i id="caret-imovel" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="filtroImovel" class="collapse" aria-labelledby="headingOneImovel"
                                                     data-parent="#filtroImovel">
                                                    <div class="card-body">


                                                        <!-- CEP -->

                                                        <jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
                                                            <jsp:param name="cepObrigatorio" value="false"/>
                                                            <jsp:param name="idCampoCep" value="cepImovel"/>
                                                            <jsp:param name="numeroCep"
                                                                       value="${servicoAutorizacaoVO.cepImovel}"/>
                                                            <jsp:param name="servico" value="true"/>
                                                        </jsp:include>

                                                        <div class="form-row mb-1">
                                                            <label for="numeroImovel"
                                                                   class="col-sm-3  text-md-right">N�mero do
                                                                im�vel</label>
                                                            <div class="col-sm-9">
                                                                <input type="number" class="form-control form-control-sm"
                                                                       id="numeroImovel" name="numeroImovel" maxlength="9"
                                                                       size="9"
                                                                       value="${servicoAutorizacaoVO.numeroImovel}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="descricaoComplemento"
                                                                   class="col-sm-3  text-md-right">Complemento</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       class="form-control form-control-sm"
                                                                       id="descricaoComplemento" name="descricaoComplemento"
                                                                       maxlength="25" size="30"
                                                                       value="${servicoAutorizacaoVO.descricaoComplemento}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="nomeImovel"
                                                                   class="col-sm-3  text-md-right">Descri��o</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       class="form-control form-control-sm"
                                                                       id="nomeImovel" name="nomeImovel" maxlength="100"
                                                                       size="100"
                                                                       value="${servicoAutorizacaoVO.nomeImovel}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="matriculaImovel"
                                                                   class="col-sm-3  text-md-right">Matr�cula</label>
                                                            <div class="col-sm-9">
                                                                <input type="number" class="form-control form-control-sm"
                                                                       id="matriculaImovel" name="matriculaImovel" maxlength="9"
                                                                       size="9"
                                                                       value="${servicoAutorizacaoVO.matriculaImovel}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-3 pr-2 text-md-right">Im�vel �
                                                                    condom�nio</label>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="condominioImovelSim"
                                                                           name="condominioImovel" class="custom-control-input"
                                                                           value="true"
                                                                           <c:if test="${fn:contains(condominioImovel,'true')}">checked="checked"</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="condominioImovelSim">Sim</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="condominioImovelNao"
                                                                           name="condominioImovel" class="custom-control-input"
                                                                           value="false"
                                                                           <c:if test="${fn:contains(condominioImovel,'false')}">checked="checked"</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="condominioImovelNao">N�o</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="condominioImovelTodos"
                                                                           name="condominioImovel" class="custom-control-input"
                                                                           value=""
                                                                           <c:if test="${fn:contains(condominioImovel,'')}">checked="checked"</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="condominioImovelTodos">Todos</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label>Localiza��o do Ponto de Consumo</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion" id="accordionLocalizacaoPontoConsumo">
                                            <div class="card" style="border: 1px solid rgba(0, 0, 0, 0.125);">
                                                <div class="card-header p-0" id="headingOnePontoConsumo">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#localizacaoPontoConsumo').toggleClass('show'); toggleClass('#caret-ponto-consumo')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Ponto de consumo <i id="caret-ponto-consumo" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="localizacaoPontoConsumo" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#localizacaoPontoConsumo">
                                                    <div class="card-body">
                                                        <div class="form-row mb-1">
                                                            <label for="siglaUnidadeFederacaoPontoConsumo"
                                                                   class="col-sm-3  text-md-right">UF</label>
                                                            <div class="col-sm-9">
                                                                <select
                                                                        name="siglaUnidadeFederacaoPontoConsumo"
                                                                        class="form-control form-control-sm"
                                                                        id="siglaUnidadeFederacaoPontoConsumo"
                                                                        onchange="carregarMunicipios(this.value);">

                                                                    <option value="">Selecione</option>
                                                                    <c:forEach items="${listaUnidadeFederacao}"
                                                                               var="unidadeFederacao">
                                                                        <option value="<c:out value="${unidadeFederacao.sigla}"/>"
                                                                                <c:if test="${servicoAutorizacaoVO.siglaUnidadeFederacaoPontoConsumo eq unidadeFederacao.sigla}">selected="selected"</c:if>>
                                                                            <c:out value="${unidadeFederacao.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="nomeMunicipioPontoConsumo"
                                                                   class="col-sm-3  text-md-right">Munic�pio</label>
                                                            <div class="col-sm-9">

                                                                <select name="nomeMunicipioPontoConsumo"
                                                                        id="nomeMunicipioPontoConsumo"
                                                                        class="form-control form-control-sm"
                                                                        onchange="carregarBairros(this.value);">
                                                                    <option value="">Selecione</option>
                                                                    <c:forEach items="${listaMunicipio}" var="municipio">
                                                                        <option value="<c:out value="${municipio.descricao}"/>"
                                                                                <c:if test="${servicoAutorizacaoVO.nomeMunicipioPontoConsumo eq municipio.descricao}">selected="selected"</c:if>>
                                                                            <c:out value="${municipio.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="bairroPontoConsumo"
                                                                   class="col-sm-3  text-md-right">Bairro</label>
                                                            <div class="col-sm-9">

                                                                <select name="bairroPontoConsumo" id="bairroPontoConsumo"
                                                                        class="form-control form-control-sm">
                                                                    <option value="">Selecione</option>
                                                                    <c:forEach items="${listaBairro}" var="cep">
                                                                        <option value="<c:out value="${cep.bairro}"/>"
                                                                                <c:if test="${servicoAutorizacaoVO.bairroPontoConsumo eq cep.bairro}">selected="selected"</c:if>>
                                                                            <c:out value="${cep.bairro}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="lougradoroPontoConsumo"
                                                                   class="col-sm-3  text-md-right">Logradouro</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       class="form-control form-control-sm"
                                                                       id="lougradoroPontoConsumo" name="lougradoroPontoConsumo"
                                                                       maxlength="50" size="27"
                                                                       value="${servicoAutorizacaoVO.lougradoroPontoConsumo}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label for="cepPontoConsumo"
                                                                   class="col-sm-3  text-md-right">CEP</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       class="form-control form-control-sm text-uppercas cep"
                                                                       id="cepPontoConsumo" name="cepPontoConsumo" maxlength="9"
                                                                       size="27"
                                                                       value="${servicoAutorizacaoVO.cepPontoConsumo}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label>Filtro Contrato</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion" id="accordionContrato">
                                            <div class="card" style="border: 1px solid rgba(0, 0, 0, 0.125);">
                                                <div class="card-header p-0" id="headingOneContrato">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#filtroContrato').toggleClass('show'); toggleClass('#caret-contrato')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Contrato <i id="caret-contrato" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="filtroContrato" class="collapse" aria-labelledby="headingOneContrato"
                                                     data-parent="#filtroContrato">
                                                    <div class="card-body">
                                                        <div class="form-row mb-1">
                                                            <label for="numeroContrato"
                                                                   class="col-sm-4  text-md-right">N�mero do
                                                                contrato</label>
                                                            <div class="col-sm-8">
                                                                <input type="number" class="form-control form-control-sm"
                                                                       id="numeroContrato" name="numeroContrato" maxlength="9"
                                                                       min="0"
                                                                       onkeypress="return formatarCampoInteiro(event);"
                                                                       size="27"
                                                                       value="${servicoAutorizacaoVO.numeroContrato}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>

                </div>


                <div class="row mt-3">
                    <div class="col align-self-end text-right">
                        <button class="btn btn-primary btn-sm" id="botaoPesquisar" type="button" onclick="pesquisar();">
                            <i class="fa fa-search"></i> Pesquisar
                        </button>
                        <button class="btn btn-secondary btn-sm" id="botaoLimpar" type="button"
                                onclick="limparFormulario();">
                            <i class="fa fa-times"></i> Limpar
                        </button>
                    </div>
                </div>


                <c:if test="${listaServicoAutorizacao ne null}">

                    <hr/>


                    <div class="text-center loading">
                        <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="table-servicos" width="100%"
                               style="opacity: 0;">
                            <thead class="thead-ggas-bootstrap">
                            <tr>
                                <th>
                                    <input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>
                                </th>
                                <th scope="col" class="text-center">C�digo</th>
                                <th scope="col" class="text-center">Ponto de consumo</th>
                                <th scope="col" class="text-center">Cliente</th>
                                <th scope="col" class="text-center">Data de gera��o</th>
                                <th scope="col" class="text-center">Data de execu��o</th>
                                <th scope="col" class="text-center">Tipo de servi�o</th>
                                <th scope="col" class="text-center">N� do Protocolo</th>
                                <th scope="col" class="text-center">Equipe</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Mobile</th>
                                <th scope="col" class="text-center">Tipo de Chamado</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${listaServicoAutorizacao}" var="lista">
                                <tr>
                                    <td>
                                        <input id="checkboxChavesPrimarias${lista.chavePrimaria}" type="checkbox" name="chavesPrimarias"
                                               value="${lista.chavePrimaria}">
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.chavePrimaria}"/>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.pontoConsumo.descricao}"/>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.cliente.nome}"/>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.dataGeracaoFormatada}"/>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.dataExecucaoFormatadaTexto}"/>
                                        </a>
                                    </td>
                                    
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span><c:out
                                                value="${lista.servicoTipo.descricao}"/></a>
                                    </td>
                                    <td class="text-center">
                                        <a class="protocolo"
                                           href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.chamado.protocolo.numeroProtocolo}"/>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.equipe.nome}"/>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"
                                           title="${lista.servicoAutorizacaoMotivoEncerramento.descricao}"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${lista.status.descricao}"/>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
										<c:choose>
                                            <c:when test="${lista.servicoAutorizacaoTemp ne null}">
                                            	SIM
                                            </c:when>
                                            <c:otherwise>
                                            	N�O
                                            </c:otherwise>
                                        </c:choose>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:exibirDetalhamento(<c:out value='${lista.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
										<c:choose>
                                            <c:when test="${lista.chamado.protocolo ne null}">
                                            	${lista.chamado.chamadoAssunto.chamadoTipo.descricao }
                                            </c:when>
                                            <c:otherwise>
                                            </c:otherwise>
                                        </c:choose>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>

                </c:if>





            </div>


                <div class="card-footer">
                            <div class="row">
                                <div class="col-sm-12">
                            <vacess:vacess param="exibirInclusaoServicoAutorizacao">
                                <button id="buttonIncluir" class="btn btn-sm btn-primary float-right ml-1 mt-1" onclick="incluir();">
                                    <i class="fa fa-plus"></i> Incluir
                                </button>
                            </vacess:vacess>
                            <c:if test="${not empty listaServicoAutorizacao}">
                                <jsp:include page="/jsp/atendimento/servicoautorizacao/botoesServicoAutorizacaoBootstrap.jsp"></jsp:include>
                            </c:if>
                        </div>
                    </div>
                </div>
        </div>
    </form:form>

    <div id="modal-imprimir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Relat�rio Autoriza��o de Servi�o</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Deseja incluir o chamado correspondentes a essa Autoriza��o de Servi�o?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="confirmarImpressao('false')">N�o</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="confirmarImpressao('true')">Sim</button>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/autorizacaoServico/pesquisarAutorizacaoServico/exibirPesquisaAutorizacaoServico/index.js"></script>
