<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script>
	function exibirQuestionario(SAHistoricoId){
		window.open('exibirRespostaFormulario?chavePrimariaASHistorico=' + SAHistoricoId ,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
</script>

<legend>Hist�rico Autoriza��o de Servi�o</legend>
	<display:table class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="servicoAutorizacaoHistorico" sort="list" id="servicoAutorizacaoHistorico" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		
		<display:column title="Data / Hora Opera��o" style="width: 180px">
			<fmt:formatDate value="${servicoAutorizacaoHistorico.ultimaAlteracao}" pattern="dd/MM/yyyy: HH:mm"/>	     		
		</display:column>
				
		<display:column property="operacao.descricao" sortable="false" title="Opera��o"/>
		
		<display:column title="Data de Previs�o de Encerramento" style="width: 250px">
			<fmt:formatDate value="${servicoAutorizacaoHistorico.dataPrevisaoEncerramento}" pattern="dd/MM/yyyy"/>	     		
		</display:column>
		
		<display:column property="equipe.nome" sortable="false" title="Equipe"/>
		<display:column property="descricao" sortable="false" title="Descri��o" maxLength="30"/>
		<display:column property="usuarioSistema.funcionario.nome" sortable="false" title="Usu�rio" maxLength="30"/>
		
		<display:column style="text-align: center; width: 30px" class="colunaSemTitulo" title="Formul�rio">
			<c:if test="${ servicoAutorizacaoHistorico.respostasQuestionario ne null and not empty servicoAutorizacaoHistorico.respostasQuestionario }">
				<a onclick="javascript:exibirQuestionario(<c:out value="${servicoAutorizacaoHistorico.chavePrimaria}"/>);">
					<img title="Formul�rio" alt="Formul�rio"  src="<c:url value="/imagens/icon_info_sistema.png"/>">
				</a>
			</c:if>	
		</display:column>
		     	
	</display:table>
