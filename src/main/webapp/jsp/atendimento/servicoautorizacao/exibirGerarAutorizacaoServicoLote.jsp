<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery-weekdays.js"></script>

<link rel="stylesheet" media="all" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/css/jquery-weekdays.css"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js" integrity="sha512-RtZU3AyMVArmHLiW0suEZ9McadTdegwbgtiQl5Qqo9kunkVg1ofwueXD8/8wv3Af8jkME3DDe3yLfR8HSJfT2g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js" integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>



<script type="text/javascript">

$(document).ready(function(){

	iniciarDatatable('#pontoConsumo');
	
	var datepicker = $.fn.datepicker.noConflict();

	var date = new Date();
	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	
	$.fn.bootstrapDP = datepicker;  
	$('.bootstrapDP').bootstrapDP({
	    autoclose: true,
		format: 'dd/mm/yyyy',
		language: 'pt-BR',
		startDate: "today"
	});

	$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});



    $("#horaServico").inputmask("99:99", {placeholder: "_", greedy: false});

    $('#horaServico').timepicker({
    	  timeFormat: 'HH:mm',
    	  defaultTime: '08:00',
    	});
    
    
	$('#turno').select2({
		language: 'pt-BR',
		theme: 'bootstrap4',
        placeholder: "Selecione uma ou mais op��es",
        containerCssClass: ':all:',
        language: 'pt-BR',
        width: null

	});
    
});

function gerarLote() {
	$("#servicoTipo").removeAttr('disabled');
	submeter('autorizacaoServicoLoteForm','gerarAutorizacaoServicoLote');
}

function roteirizar() {
	var diasSelecionados = [];

	if(validarDiasSelecionados()){
		$('#weekdays').selectedIndexes().map(function(nome, i) {
			diasSelecionados.push(i);
		}); 
		
		exibirIndicador();
		$("#servicoTipo").removeAttr('disabled');
		$("#diasSemana").val(diasSelecionados);
		
		submeter('autorizacaoServicoLoteForm','roteirizarServicoAutorizacaoLoteLote');
	}

}

$(function(){

	var dias = "<c:out value="${diasSemana}"/>";

	if(!dias) {
		dias = [1,2,3,4,5];
	} 
	
	$('#weekdays').weekdays({
		selectedIndexes: dias,
		days: ["Domingo", "Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado"]
	});
});

function validarDiasSelecionados() {
	if($('#weekdays').selectedIndexes().length > 0) {
		return true;
	}

	alert("Selecione pelo menos um dia da semana!");
	return false;
}

	
</script>


<div class="bootstrap">
	<form:form action="gerarComunicacaoLote" id="autorizacaoServicoLoteForm" name="autorizacaoServicoLoteForm" method="post" modelAttribute="GeracaoComunicaoLoteVO">
		<div class="card">
				<div class="card-header">
					<h5 class="card-title mb-0">Gerar Autoriza��o de Servi�o Em Lote</h5>
				</div>
				<div class="card-body">
					<div class="alert alert-primary fade show" role="alert">
						<i class="fa fa-question-circle"></i> Preencha os campos para
						pesquisar os pontos de consumos, clique em <b>Roterizar</b>
						para roterizar os pontos de consumo e clique em <b>Gerar</b> para gerar a autoriza��o de servi�o em lote.
					</div>
	
					<hr>
	
					<div class="card">
						<div class="card-body bg-light">
						
							<div class="row mb-2">
								<div class="col-md-12">
									<div class="form-row">
										<div class="col-md-12">
											<label for="servicoTipo">Tipo de Servi�o:<span class="text-danger">*</span></label> <select
												name="servicoTipo" id="servicoTipo"
												class="form-control form-control-sm" disabled>
												<option value="-1">Selecione</option>
												<c:forEach items="${listaServicoTipo}"
													var="servicoTipo">
													<option
														value="<c:out value="${servicoTipo.chavePrimaria}"/>"
														<c:if test="${filtroGeracaoServicoAutorizacaoLoteDTO.servicoTipo.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
														<c:out value="${servicoTipo.descricao}" />
													</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Tempo de Execu��o Prevista do Servi�o:<span class="text-danger">*</span></label>
												<input class="form-control form-control-sm" type="text" id="tempoExecucao" name="tempoExecucao" onkeypress="return formatarCampoInteiro(event);" maxlength="10" value="${filtroGeracaoServicoAutorizacaoLoteDTO.servicoTipo.quantidadeTempoMedio}">
										</div>
		                            </div>
								</div>								
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Data Prevista Servi�o:<span class="text-danger">*</span></label>
												<input class="form-control form-control-sm campoData bootstrapDP" type="text" id="dataServico" name="dataServico" maxlength="10" value="${filtroGeracaoServicoAutorizacaoLoteDTO.dataServico}">
										</div>
		                            </div>
								</div>
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Hora Prevista de Sa�da:<span class="text-danger">*</span></label>
												<input class="form-control form-control-sm" type="text" id="horaServico" name="horaServico" maxlength="5" value="${filtroGeracaoServicoAutorizacaoLoteDTO.horaServico}">
										</div>
		                            </div>
								</div>
								
								<div class="col-md-6">
									<div class="form-row">
										<div class="col-md-10">
	                                        <label for="turno" class="float-none">Turno</label>
	                                        <select class="form-control form-control-sm" id="turno" name="turno" multiple>
	                                            <c:forEach var="turno" items="${listaTurno}">
	                                                <option value="${turno.chavePrimaria}"
	                                                <c:if test="${turnosSelecionados.contains(turno.chavePrimaria)}">selected="selected"</c:if>>
	                                                ${turno.descricao}
	                                                </option>
	                                            </c:forEach>
	                                        </select>
										</div>
									</div>
								</div>							
								
								<div class="col-md-12">
									<div class="form-row">
										<div class="col-md-10">
		                                    <label>Dias para execu��o do Servi�o:<span class="text-danger">*</span></label>
		                                    <br/><br/>
		                                    <input type="hidden" name="diasSemana" id="diasSemana"/>
		                                    <div class="form-row">
		                                   		 <div class="col-md-12" id="weekdays"> </div>
		                                    </div>
										</div>
		                            </div>
								</div>									
								
							</div>
							
							<div class="row mb-2">
													
								
							</div>
							
							<br/>
							<div class="row mt-3">
								<div class="col align-self-end text-right">
									<button class="btn btn-primary btn-sm" id="botaoPesquisar"
											type="button" onclick="roteirizar();">
											<i class="fa fa-search"></i> Roteirizar Pontos de Consumo
									</button>
								</div>
							</div>
							
							<c:if test="${listaPontoConsumo ne null}">
								<hr class="linhaSeparadora1" />
								<fieldset class="conteinerBloco">
									<div class="alert alert-primary" role="alert">
										<p class="orientacaoInicial">O Ponto de Partida est� definido a partir da <b>ALG�S</b>. </p>
									</div>								
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover" id="pontoConsumo" style="width:100%">
											<thead class="thead-ggas-bootstrap">
												<tr>
													<th scope="col" class="text-center">Ponto de Consumo</th>
													<th scope="col" class="text-center">C�digo do Ponto de Consumo</th>
													<th scope="col" class="text-center">Segmento</th>
													<th scope="col" class="text-center">Dist�ncia entre Pontos (km)</th>
													<th scope="col" class="text-center">Tempo entre Pontos (minutos)</th>
													<th scope="col" class="text-center">Equipe</th>	
													<th scope="col" class="text-center">Data Execu��o</th>
													<th scope="col" class="text-center">Previs�o de Chegada (horas)</th>
													<th scope="col" class="text-center">Tempo Execu��o (minutos)</th>	
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${listaPontoConsumo}" var="autorizacaoServicoLoteVO">
													<tr>
														<td class="text-center">
												        	<c:out value="${autorizacaoServicoLoteVO.pontoConsumo.descricao} - ${autorizacaoServicoLoteVO.pontoConsumo.imovel.nome}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${autorizacaoServicoLoteVO.pontoConsumo.codigoPontoConsumo}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${autorizacaoServicoLoteVO.pontoConsumo.segmento.descricao}"/>
														</td>
														<td class="text-center"><fmt:formatNumber type="number"
															maxFractionDigits="2"
															value="${autorizacaoServicoLoteVO.distanciaEntrePontos}" />
														</td>
														<td class="text-center">
												        	<c:out value="${autorizacaoServicoLoteVO.tempoDistancia}"/>
														</td>														
														<td class="text-center">
												        	<c:out value="${autorizacaoServicoLoteVO.servicoTipoAgendamentoTurno.equipe.nome}"/>
														</td>														
														<td class="text-center">
												        	<c:out value="${autorizacaoServicoLoteVO.dataExecucao}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${fn:replace(fn:replace(autorizacaoServicoLoteVO.periodoExecucao, 'horas', ''),'e', '-')}"/>
														</td>
														<td class="text-center">
												        	<c:out value="${autorizacaoServicoLoteVO.tempoExecucao}"/>
														</td>																																																																						
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
									
								</fieldset>
								
							
								<div class="row mt-3">
								
									<div class="col align-self-end text-right">
										<button class="btn btn-primary btn-sm" id="botaoPesquisar"
												type="button" onclick="gerarLote();">
												<i class="fa fa-search"></i> Gerar Lote
										</button>
									</div>
								</div>					
							</c:if>												
							
						</div>
					</div>
				</div>
			</div>
	 </form:form>
</div>