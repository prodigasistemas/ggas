<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

    function visualizarInformacoesPontoConsumo() {
        var chavePontoConsumo = document.forms['servicoAutorizacaoForm'].idPontoConsumo.value;

        if (chavePontoConsumo != '') {
            submeter('servicoAutorizacaoForm', 'visualizarInformacoesPontoConsumo', '_blank');
        } else {
            alert("Escolha um Ponto de Consumo para Vizualizar suas informa��es.");
        }

    }
</script>

<hr/>
<h5 class="card-title mt-2">Pontos de consumo</h5>

<div class="text-center loading">
    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover" id="table-grid-pontos-consumo" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th>
            </th>
            <th scope="col" class="text-center">Descri��o</th>
        </tr>
        </thead>
        <tbody>

            <c:forEach items="${pontosConsumo}" var="pontoConsumo">
                <tr>
                    <c:if test="${ fluxoInclusao eq true }">
                        <td>
                            <input type="radio" name="checkPontoConsumo" id="checkPontoConsumo"
                                   value="<c:out value="${pontoConsumo.chavePrimaria}"/>"
                                   <c:if test="${pontoConsumo.chavePrimaria == servicoAutorizacao.pontoConsumo.chavePrimaria}">checked</c:if>
                                   onClick="selecionarPontoConsumo(${pontoConsumo.chavePrimaria}), carregaroInformacaoPontoConsumo(${pontoConsumo.chavePrimaria});"/>
                        </td>
                        <td>
                            <a href="javascript:detalharImovel('<c:out value='${pontoConsumo.descricao}'/>');">
                                <c:out value='${pontoConsumo.descricao}'/>
                            </a>
                        </td>
                    </c:if>

                    <c:if test="${ fluxoInclusao ne true }">
                        <td>
                            <input type="radio" name="checkPontoConsumo" id="checkPontoConsumo"
                                   value="<c:out value="${pontoConsumo.chavePrimaria}"/>"
                                   onClick="carregaroInformacaoPontoConsumo(${pontoConsumo.chavePrimaria});"/>
                        </td>
                        <td>
                                ${pontoConsumo.descricao}
                        </td>
                    </c:if>
                </tr>
                <c:set value="pontoConsumo" var="pontoConsumo" ></c:set>
            </c:forEach>
        </tbody>
    </table>
</div>

<div class="form-row">
    <div class="col-md-12">
        <label id="rotuloPontoConsumo" for="descricaoPontoConsumo">Descri��o:</label>
        <input class="form-control form-control-sm" type="text" id="descricaoPontoConsumo" name="descricaoPontoConsumo" disabled="disabled"
               maxlength="25" size="30" value="<c:out value="${pontoConsumo.descricao}"/>">
    </div>
</div>

<div class="form-row mt-2">
    <div class="col-md-12">
        <button type="button" name="Button" class="btn btn-primary btn-sm" id="botaoPesquisar" type="button"
                onclick="visualizarInformacoesPontoConsumo();">
            Informa��es do Ponto de Consumo
        </button>
    </div>
</div>
