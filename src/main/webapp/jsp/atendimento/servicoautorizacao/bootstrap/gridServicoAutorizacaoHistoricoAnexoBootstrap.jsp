<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<div class="table-responsive mt-1">
    <table class="table table-bordered table-striped table-hover" id="table-servico-autorizacao-historico-anexos" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th class="text-center" colspan="5">Anexos das Autoriza��es de Servi�os</th>
        </tr>
        <tr>
            <th scope="col" class="text-center">Data / Hora</th>
            <th scope="col" class="text-center">Opera��o</th>
            <th scope="col" class="text-center">Usu�rio</th>
            <th scope="col" class="text-center">Descri��o</th>
            <th scope="col" class="text-center">Anexo</th>
        </tr>
        </thead>
        <tbody>
            <c:forEach var="servicoAutorizacaoHistoricoAnexo" items="${listaServicoAutorizacaoHistoricoAnexo}">
            <tr>
                <td><fmt:formatDate value="${servicoAutorizacaoHistoricoAnexo.ultimaAlteracao}" pattern="dd/MM/yyyy: HH:mm"/></td>
                <td>${servicoAutorizacaoHistoricoAnexo.operacao}</td>
                <td>${servicoAutorizacaoHistoricoAnexo.usuarioLogin}</td>
                <td>${servicoAutorizacaoHistoricoAnexo.nomeDocumentoAnexo}</td>
                <td class="text-center">
<%--                     <button type="button" class="acaoRemoverEquipamento btn btn-default btn-sm mr-1" onclick="javascript:imprimirArquivoHistoricoAnexo(<c:out value="${servicoAutorizacaoHistoricoAnexo.idAnexo}, ${servicoAutorizacaoHistoricoAnexo.mobile}"/>);"> --%>
<!--                         <i class="fa fa-print"></i> Imprimir -->
                    <button type="button" class="acaoRemoverEquipamento btn btn-default btn-sm mr-1" onclick="javascript:visualizarImagem(<c:out value="${servicoAutorizacaoHistoricoAnexo.idAnexo}, ${servicoAutorizacaoHistoricoAnexo.mobile}"/>);">
                        <i class="fa fa-print"></i> Visualizar
                    </button>
                </td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<script>

function imprimirArquivoHistoricoAnexo(chaveServicoAutorizacaoHistorico, mobile){

	if (windows['popup'] != undefined && windows['popup'] != null) {
        windows['popup'].close();
    }
    
    
    var url = 'imprimirArquivoServicoAutorizacao';
    var popup = windows['popup'] = window.open(url + "?chaveServicoAutorizacaoHistorico=" + chaveServicoAutorizacaoHistorico + "&mobile=" + mobile ,'popup', 'height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

    if(popup){
    	window.location.reload();
    }
}

function visualizarImagem(chaveServicoAutorizacaoHistorico, mobile) {

    var url = 'visualizarImagem';
    url += '?chaveServicoAutorizacaoHistorico=' + chaveServicoAutorizacaoHistorico + '&mobile=' + mobile;
    
    var popup = windows['popup'] = window.open(url, '_blank');
    
    if(popup){
    	window.location.reload();
    }
}
</script>
