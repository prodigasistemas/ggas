<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<div class="table-responsive mt-1">
    <table class="table table-bordered table-striped table-hover" id="table-servico-autorizacao-historico" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th class="text-center" colspan="9">Hist�rico das Autoriza��es de Servi�o</th>
        </tr>
        <tr>
            <th scope="col" class="text-center">Data / Hora Opera��o</th>
            <th scope="col" class="text-center">Opera��o</th>
            <th scope="col" class="text-center">Data de Previs�o de Encerramento</th>
            <th scope="col" class="text-center">Equipe</th>
            <th scope="col" class="text-center">Descri��o</th>
            <th scope="col" class="text-center">Usu�rio</th>
            <th scope="col" class="text-center">Formul�rio</th>
            <th scope="col" class="text-center">Situa��o</th>
            <th scope="col" class="text-center">Motivo Encerramento</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="servicoAutorizacaoHistorico" items="${servicoAutorizacaoHistorico}">
            <tr>
                <td><fmt:formatDate value="${servicoAutorizacaoHistorico.ultimaAlteracao}" pattern="dd/MM/yyyy: HH:mm"/></td>
                <td>${servicoAutorizacaoHistorico.operacao.descricao}</td>
                <td><fmt:formatDate value="${servicoAutorizacaoHistorico.dataPrevisaoEncerramento}" pattern="dd/MM/yyyy"/></td>
                <td>${servicoAutorizacaoHistorico.equipe.nome}</td>
                <td>${servicoAutorizacaoHistorico.descricao}</td>
                <td>${servicoAutorizacaoHistorico.usuarioSistema.funcionario.nome}</td>
                <td class="text-center">
                    <c:choose>
                        <c:when test="${ servicoAutorizacaoHistorico.respostasQuestionario ne null and not empty servicoAutorizacaoHistorico.respostasQuestionario }">
                            <button type="button" class="btn btn-default btn-sm mr-1"
                                    name="exibirFormulario"
                                    data-chavePrimaria="${servicoAutorizacaoHistorico.chavePrimaria}">
                                <i class="fa fa-paper-plane"></i> Formul�rio
                            </button>
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>${servicoAutorizacaoHistorico.status.descricao}</td>
                <td>${servicoAutorizacaoHistorico.servicoAutorizacaoMotivoEncerramento.descricao}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<jsp:include page="../../questionario/bootstrap/modalRespostaFormulario.jsp"/>
