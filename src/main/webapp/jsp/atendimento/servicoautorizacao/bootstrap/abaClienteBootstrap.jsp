<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<script type="text/javascript">
    $(function () {

        $("#nome").autocomplete({
            source: function (request, response) {

                $.ajax({
                    url: "carregaNomesClientesServicoAutorizacao?nome=" + request.term,
                    dataType: "json",
                    data: {
                        style: "full",
                        maxRows: 12,
                        name_startsWith: request.term
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {

                                label: item.nome + '<br>' +
                                    item.endereco + '<br>' +
                                    item.numeroContrato + '<br>' +
                                    item.inicioVigenciaContrato + '<br>' +
                                    item.fimVigenciaContrato,
                                value: item.chavePrimaria,
                                idContrato: item.idContrato

                            }
                        }));
                    }
                });
            },
            create: function () {
                $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                    return $('<li>')
                        .append('<a>' + item.label + '</a>')
                        .appendTo(ul);
                };
            },
            minLength: 3,
            select: function (event, ui) {
                carregarCliente(ui.item.value, null, null, null, null, ui.item.idContrato);
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).focus(function () {
            $(this).data("uiAutocomplete").search($(this).val());
        });
        ;

        $("#nomeFantasia").autocomplete({
            source: function (request, response) {

                $.ajax({
                    url: "carregaNomesClientesServicoAutorizacao?nomeFantasia=" + request.term,
                    dataType: "json",
                    data: {
                        style: "full",
                        maxRows: 12,
                        name_startsWith: request.term
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                label: item.nomeFantasia + ' <br> ' +
                                    item.endereco + ' <br> ' +
                                    item.numeroContrato + ' <br> ' +
                                    item.inicioVigenciaContrato + ' <br> ' +
                                    item.fimVigenciaContrato,
                                value: item.chavePrimaria,
                                idContrato: item.idContrato
                            }
                        }));
                    }
                });
            },
            create: function () {
                $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                    return $('<li>')
                        .append('<a>' + item.label + '</a>')
                        .appendTo(ul);
                };
            },
            minLength: 3,
            select: function (event, ui) {
                carregarCliente(ui.item.value, null, null, null);
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });

    });

    function carregarCliente(chavePrimaria, cpfCnpj, passaporte, email, telefone) {
        document.forms[0].contrato.value = "";
        var url;
        if (chavePrimaria != null) {
            url = "carregarClienteServicoAutorizacao?chavePrimaria=" + chavePrimaria;
        }

        if (url != null) {
            carregarFragmento('divCliente', url);
            var chavePrimariaCliente = document.forms[0].chaveCliente.value;
            $("#gridServicoAutorizacaoCliente").load("carregarClienteImovelServicoAutorizacao?chavePrimariaCliente=" + chavePrimariaCliente);
            document.forms[0].cliente.value = chavePrimariaCliente;
            $("#gridImoveis").load("carregarImoveisPorClienteServicoAutorizacao?chavePrimariaCliente=" + chavePrimariaCliente);

        }

    }

    function selecionarCliente(idSelecionado) {

        var idCliente = document.getElementById("chaveCliente");
        var nomeCompletoCliente = document.getElementById("nome");
        var documentoFormatado = document.getElementById("cpfCnpj");
        var emailCliente = document.getElementById("email");
        var nomeFantasia = document.getElementById("nomeFantasia");
        var telefone = document.getElementById("telefone");
        var numeroPassaporte = document.getElementById("numeroPassaporte");
        var rg = document.getElementById("rg");

        if (idSelecionado != '') {
            AjaxService.obterClientePorChave(idSelecionado, {
                    callback: function (cliente) {
                        if (cliente != null) {

                            idCliente.value = cliente["chavePrimaria"];
                            nomeCompletoCliente.value = cliente["nome"];

                            if (cliente["cnpj"] != undefined) {
                                documentoFormatado.value = cliente["cnpj"];
                            } else {

                                if (cliente["cpf"] != undefined) {
                                    documentoFormatado.value = cliente["cpf"];
                                } else {
                                    documentoFormatado.value = "";
                                }
                            }

                            emailCliente.value = cliente["email"];
                            $('form[name="notaDebitoCreditoForm"]').change();
                        }

                        telefone.value = cliente["telefone"];
                        numeroPassaporte.value = cliente["numeroPassaporte"];
                        nomeFantasia.value = cliente["nomeFantasia"];
                        rg.value = cliente["rg"];
                    }, async: false
                }
            );
        } else {
            idCliente.value = "";
            nomeCompletoCliente.value = "";
            documentoFormatado.value = "";
            emailCliente.value = "";
        }
        $('#nome').focus();


    }

    function limparCliente(valor) {
        if (valor = "") {
            document.forms[0].cliente.value = null;
        }
    }
</script>
<div id="abaCliente">

    <input type="hidden" id="cliente" name="cliente" value="${servicoAutorizacao.cliente.chavePrimaria}"/>
    <input type="hidden" id="chaveCliente" value="${cliente.chavePrimaria}"/>

    <div class="form-row">
        <div class="col-md-12">
            <label for="nome">Nome:</label>
            <input class="form-control form-control-sm" type="text" id="nome" name="nome" maxlength="30" size="32" value="${cliente.nome}"
                   value="<c:if test='${ not empty cliente.cnpj}'>${cliente.cnpjFormatado}</c:if><c:if test='${not empty cliente.cpf}'>${cliente.cpfFormatado}</c:if>"
                   onblur="limparCliente(this.value)">
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12">
            <label for="nomeFantasia">Nome Fantasia:</label>
            <input class="form-control form-control-sm" type="text" id="nomeFantasia" name="nomeFantasia" maxlength="30" size="32"
                   value="${cliente.nomeFantasia}" onblur="limparCliente(this.value)">
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-12">
            <label for="cpfCnpj">CPF/CNPJ:</label>
            <input class="form-control form-control-sm" type="text" id="cpfCnpj" name="cpfCnpj" maxlength="19" size="32"
                   value="<c:if test='${ not empty cliente.cnpj}'>${cliente.cnpj}</c:if><c:if test='${not empty cliente.cpf}'>${cliente.cpf}</c:if>"/>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-12">
            <label for="numeroPassaporte">Passaporte:</label>
            <input class="form-control form-control-sm" type="text" id="numeroPassaporte" name="numeroPassaporte" maxlength="20" size="32"
                   value="${cliente.numeroPassaporte}">
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-12">
            <label for="email">Email:</label>
            <input class="form-control form-control-sm" type="email" id="email" name="email" maxlength="40" size="32"
                   value="${cliente.emailPrincipal}">
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-12">
            <label for="telefone">Telefone:</label>
            <input class="form-control form-control-sm" type="text" id="telefone" name="telefone" maxlength="30" size="32"
                   value="${telefone}">
        </div>
    </div>


</div>
