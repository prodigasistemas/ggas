<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<c:choose>
    <c:when test="${fluxoDetalhamento}">
        <div class="form-row">
            <label for="numeroImovel" class="col-sm-2 col-form-label text-md-right">CEP</label>
            <div class="col-sm-10">
                <input class="form-control form-control-sm" type="text" name="numeroImovel" maxlength="9" size="9"
                       value="${servicoAutorizacao.imovel.quadraFace.endereco.cep.cep}">
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
            <jsp:param name="cepObrigatorio" value="false"/>
            <jsp:param name="idCampoCep" value="cepImovel"/>
            <jsp:param name="numeroCep" value="${servicoAutorizacao.imovel.quadraFace.endereco.cep.cep}"/>
        </jsp:include>
    </c:otherwise>
</c:choose>

<div class="form-row">
    <label for="numeroImovel" class="col-sm-2 col-form-label text-md-right">N�mero do im�vel</label>
    <div class="col-sm-10">
        <input class="form-control form-control-sm" type="text" id="numeroImovel" name="numeroImovel" maxlength="9" size="9"
               oninput="formatarCampoNumeroEnderecoOnInput(event);"
               value="${servicoAutorizacao.imovel.numeroImovel}">
    </div>
</div>

<div class="form-row">
    <label for="complemento" class="col-sm-2 col-form-label text-md-right">Complemento</label>
    <div class="col-sm-10">
        <input class="form-control form-control-sm" type="text" id="complemento" name="complemento" maxlength="25" size="30"
               value="${servicoAutorizacao.imovel.descricaoComplemento}">
    </div>
</div>
<div class="form-row">

    <label for="nomeImovel" class="col-sm-2 col-form-label text-md-right">Nome</label>
    <div class="col-sm-10">
        <input class="form-control form-control-sm" type="text" id="nomeImovel" name="nomeImovel" maxlength="100" size="100"
               value="${servicoAutorizacao.imovel.nome}">
    </div>
</div>

<div class="form-row">
    <label for="matricula" class="col-sm-2 col-form-label text-md-right">Matr�cula</label>
    <div class="col-sm-10">
        <input class="form-control form-control-sm" type="text" id="matricula" name="matricula" maxlength="9" size="9"
               onkeypress="return formatarCampoInteiro(event)" value="${servicoAutorizacao.imovel.chavePrimaria}">
    </div>
</div>
