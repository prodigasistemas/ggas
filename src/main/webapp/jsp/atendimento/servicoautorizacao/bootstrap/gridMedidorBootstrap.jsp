<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<div class="text-center loading">
    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover" id="table-grid-medidor" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th>
            </th>
            <th scope="col" class="text-center">N�mero de S�rie</th>
            <th scope="col" class="text-center">Modelo</th>
            <th scope="col" class="text-center">Tipo</th>
            <th scope="col" class="text-center">Marca</th>
            <th scope="col" class="text-center">Ano de fabrica��o</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${sessionScope.listaMedidor}" var="medidor">
            <tr>
                <td>
                    <input type="radio" name="chaveMedidor" id="chaveMedidor" value="<c:out value="${medidor.chavePrimaria}"/>"
                    		onClick="selecionarMedidor(${medidor.chavePrimaria})"
                           <c:if test="${medidor.chavePrimaria == servicoAutorizacao.medidor.chavePrimaria }">checked</c:if>/>
                </td>
                <td class="text-center">
                	<c:out value='${medidor.numeroSerie}'/>
                </td>
                <td class="text-center">
                	<c:out value='${medidor.modelo.descricao}'/>
                </td>
                <td class="text-center">
                	<c:out value='${medidor.tipoMedidor.descricao}'/>
                </td>
                <td class="text-center">
                	<c:out value='${medidor.marcaMedidor.descricao}'/>
                </td>
                <td class="text-center">
                	<c:out value='${medidor.anoFabricacao}'/>
                </td>                                                                
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
