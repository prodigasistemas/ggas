<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


    <div class="form-row">
        <div class="col-md-12">
            <label for="numeroSerie">N�mero de s�rie:</label>
			<input class="form-control form-control-sm" type="text" name="numeroSerie" id="numeroSerie" 
				maxlength="20" size="20" value="${servicoAutorizacao.medidor.numeroSerie}" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/>
        </div>
    </div>
    
    
    <div class="form-row">
        <div class="col-md-12">
            <label for="nome">Marca:</label>
			<select name="marcaMedidor" id="marca" class="form-control form-control-sm">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMarcaMedidor}" var="marca">
					<option value="<c:out value="${marca.chavePrimaria}"/>" <c:if test="${servicoAutorizacao.medidor.marcaMedidor.chavePrimaria == marca.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${marca.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-12">
            <label for="nome">Tipos:</label>
			<select name="tipoMedidor" id="tipo" class="form-control form-control-sm">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaTipoMedidor}" var="tipo">
					<option value="<c:out value="${tipo.chavePrimaria}"/>" <c:if test="${servicoAutorizacao.medidor.tipoMedidor.chavePrimaria == tipo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipo.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
        </div>
    </div>    
    
    <div class="form-row">
        <div class="col-md-12">
            <label for="nome">Modelo:</label>
			<select name="modelo" id="marcaModelo" class="form-control form-control-sm">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaModeloMedidor}" var="modelo">
					<option value="<c:out value="${modelo.chavePrimaria}"/>" <c:if test="${servicoAutorizacao.medidor.modelo.chavePrimaria == modelo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${modelo.descricao}"/>
					</option>		
			    </c:forEach>
			</select>
        </div>
    </div>  
    
    
    <div class="form-row">
        <div class="col-md-12">
			<label for="modoUso" class="col-md-12">Perfil:</label>
				<div class="col-md-12">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" name="modoUso" id="modoUsoNormal" class="custom-control-input" value="${modoUsoNormal}"
							<c:if test="${servicoAutorizacao.medidor.modoUso.chavePrimaria eq modoUsoNormal}">checked="checked"</c:if>>
						<label class="custom-control-label" for="modoUsoNormal">Normal</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" name="modoUso" id="modoUsoVirtual" class="custom-control-input" value="${modoUsoVirtual}"
							<c:if test="${servicoAutorizacao.medidor.modoUso.chavePrimaria eq modoUsoVirtual}">checked="checked"</c:if>>
						<label class="custom-control-label" for="modoUsoVirtual">Virtual</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" name="modoUso" id="modoUsoIndependente" class="custom-control-input" value="${modoUsoIndependente}"
							<c:if test="${servicoAutorizacao.medidor.modoUso.chavePrimaria eq modoUsoIndependente}">checked="checked"</c:if>>
						<label class="custom-control-label" for="modoUsoIndependente">Independente</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" name="modoUso" id="modoUsoTodos" class="custom-control-input" value=""
							<c:if test="${servicoAutorizacao.medidor eq null}">checked="checked"</c:if>>
						<label class="custom-control-label" for="modoUsoTodos">Todos</label>
					</div>					
															
				</div>
        </div>
        
	    <c:if test="${ fluxoInclusao eq true  || fluxoAlteracao eq true}">
		    <div class="row mt-3 ml-1">
				<div class="col align-self-end text-right">
					<button class="btn btn-primary" name="buttonPesquisar" id="buttonPesquisarMedidor"
					type="button">
						Pesquisar
					</button>
				</div>
			</div>
		</c:if>
    </div>

</br>
    <div id="gridMedidores">
        <jsp:include page="/jsp/atendimento/servicoautorizacao/bootstrap/gridMedidorBootstrap.jsp"/>
    </div>