<!--
 Copyright (C) <2011> GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!-- O script para as a��es desta tela, est�o localizados em: web/src/modulos/atendimentoAoPublico/autorizacaoServico/exibirInclusaoServicoAutorizacao/abaMateriaisController.ts -->

<c:if test="${not fluxoDetalhamento}">
    <div id="materiaisServico" class="form-row">
    <div class="col-md-6">
        <label for="material">Descri��o do Material: <span class="text-danger">*</span></label>
        <select class="form-control form-control-sm" name="material" id="material">
            <option value="-1">Selecione</option>
            <c:forEach items="${listaMateriais}" var="material">
                <option value="<c:out value="${material.chavePrimaria}"/>">
                    <c:out value="${material.descricao}"/>
                </option>
            </c:forEach>
        </select>
        <div class="invalid-feedback" id="descricaoMaterialFeedback">
            Por favor informe a descri��o do material
        </div>
    </div>
    <div class="col-md-6">
        <label id="rotuloQuantidadeMaterial" for="quantidadeMaterial">Quantidade: <span class="text-danger">*</span></label>
        <input class="form-control form-control-sm" id="quantidadeMaterial" name="quantidadeMaterial" type="number" size="10" value="${quantidadeMaterial}"
               maxlength="5" onkeypress="return formatarCampoInteiro(event, this);">
        <div class="invalid-feedback" id="quantidadeMaterialFeedback">
            A quantidade de material � obrigat�ria e deve ser maior que zero
        </div>
    </div>

    <div class="col-md-12 mt-1">
        <button class="btn btn-danger btn-sm mb-1 mr-1" id="botaoLimparMaterial" name="botaoLimparMaterial" type="button">
            <i class="fa fa-times-circle"></i> Limpar
        </button>
        <button class="btn btn-primary btn-sm mb-1 mr-1" id="botaoIncluirMaterial" name="botaoIncluirMaterial"
                value="Adicionar" type="button">
            <i class="fa fa-plus-circle"></i> Adicionar
        </button>
        <button class="btn btn-primary btn-sm mb-1 mr-1" id="botaoAlterarMaterial" name="botaoAlterarMaterial"
                value="Alterar" type="button">
            <i class="fa fa-edit"></i> Alterar
        </button>
    </div>
</div>
</c:if>

<div class="text-center loading">
    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive mt-1">
    <table class="table table-bordered table-striped table-hover" id="servicoAutorizacaoMaterial" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th width="50%" scope="col" class="text-center">Materiais necess�rios</th>
            <th width="15%" scope="col" class="text-center">Unidade</th>
            <th width="20%" scope="col" class="text-center">Quantidade</th>
            <c:if test="${not fluxoDetalhamento}">
                <th width="15%" scope="col" class="text-center">A��es</th>
            </c:if>
        </tr>
        </thead>
        <tbody>
        <c:set var="i" value="0"/>
        <c:forEach var="servicoAutorizacaoMaterial" items="${sessionScope.listaAutorizacaoMateriais}">
            <tr>
                <td>${servicoAutorizacaoMaterial.material.descricao}</td>
                <td>${servicoAutorizacaoMaterial.material.unidadeMedida.descricao}</td>
                <td class="text-right">${servicoAutorizacaoMaterial.quantidadeMaterial}</td>
                <c:if test="${not fluxoDetalhamento}">
                    <td class="text-center">
                        <button type="button" class="acaoAlterarMaterial btn btn-primary btn-sm mr-1 mt-1" data-indice="${i}"
                                data-idmaterial="${servicoAutorizacaoMaterial.material.chavePrimaria}" data-quantidade="${servicoAutorizacaoMaterial.quantidadeMaterial}">
                            <i class="fa fa-edit"></i> Alterar
                        </button>

                        <button type="button" class="acaoRemoverMaterial btn btn-danger btn-sm mr-1 mt-1" data-id="${servicoAutorizacaoMaterial.material.chavePrimaria}">
                            <i class="fa fa-trash"></i> Excluir
                        </button>
                    </td>
                </c:if>
            </tr>
            <c:set var="i" value="${i+1}"/>
        </c:forEach>
        </tbody>
    </table>
</div>

<div id="modal-confirmar-exclusao-material-servico-tipo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmar exclus�o</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id-material-servico-excluir" />
                <p>Deseja realmente confirmar exclus�o do material necess�rio?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">N�o</button>
                <button id="confirmar-exclusao-material-servico-tipo" type="button" class="btn btn-primary">Sim</button>
            </div>
        </div>
    </div>
</div>
