<!--
Copyright (C) <2011> GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<script>
	$(document).ready(function () {
		
		$("#dataLeituraOriginal").datepicker({
		    changeYear: true,
		    showOn: 'focus', 
		    dateFormat: 'dd/mm/yy', 
		    showAnim: 'fadeIn'
		});
		
		$("#dataLeituraNovo").datepicker({
		    changeYear: true,
		    showOn: 'focus', 
		    dateFormat: 'dd/mm/yy', 
		    showAnim: 'fadeIn'
		});
		
		$("#inicioExecucao").datetimepicker({
		    format: 'd/m/Y H:i',  
		    step: 15,             
		    showAnim: 'fadeIn',    
		    closeOnDateSelect: false  
		});
		
		$("#fimExecucao").datetimepicker({
		    format: 'd/m/Y H:i',  
		    step: 15,             
		    showAnim: 'fadeIn',   
		    closeOnDateSelect: false 
		});
		
		$("#horaAtendimento").inputmask("99:99", {placeholder: "_", greedy: false});
		
		$("#coletaDataHora").datetimepicker({
		    format: 'd/m/Y H:i',  
		    step: 15,             
		    showAnim: 'fadeIn',   
		    closeOnDateSelect: false 
		});
		
		
		

	});



	function exibirDialogPesquisaMedidor(tipoDialogMedidor) {
		$("#tipoDialogMedidor").val(tipoDialogMedidor);
		$('#medidorPopup').modal('show');
	}
	
	function adicionarMedidor() {
		var medidorMarcado = $('input[name="chaveMedidorMobile"]:checked').val();
		
		var tipoDialogMedidor = $("#tipoDialogMedidor").val();
		
		if(tipoDialogMedidor == "true") {
			$("#medidorNumeroSerieOriginal").val(medidorMarcado);
		} else {
			$("#medidorNumeroSerieNovo").val(medidorMarcado);
		}
		
		$('#medidorPopup').modal('toggle');
		
	}
	
</script>

<input type="hidden" id="tipoDialogMedidor" name="tipoDialogMedidor" value ="" />
<input type="hidden" id="servicoAutorizacaoTemp" name="servicoAutorizacaoTemp" value="${servicoAutorizacao.servicoAutorizacaoTemp.chavePrimaria}" />
		<div class="form-row">
		    <div class="col-md-4">
		        <label for="medidorOriginal">Medidor Original:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.medidorNumeroSerieOriginal" id="medidorNumeroSerieOriginal" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.medidorNumeroSerieOriginal}" onkeypress="return formatarCampoAlfaNumerico(event)" 
		                onkeyup="letraMaiuscula(this);"/>
		            <div class="input-group-append">
		                <button id="botaoPesquisar" type="button" onclick="exibirDialogPesquisaMedidor(true);" class="btn btn-primary btn-sm">
		                    <i class="fa fa-search"></i>
		                </button>
		            </div>
		        </div>
		    </div>
		    
		    <div class="col-md-4">
		        <label for="numeroLeituraOriginal">Leitura Original:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.numeroLeituraOriginal" id="numeroLeituraOriginal" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.numeroLeituraOriginal}"  onkeypress="return formatarCampoInteiro(event)" />
		        </div>
		    </div>
		    
		    <div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label>Data Leitura Original:</label>
						<input class="form-control form-control-sm"
							type="text" id="dataLeituraOriginal" name="servicoAutorizacaoTemp.dataLeituraOriginal" maxlength="10"
							value="<fmt:formatDate value='${servicoAutorizacao.servicoAutorizacaoTemp.dataLeituraOriginal}' pattern='dd/MM/yyyy' />">
					</div>
				</div>
			</div>
		    		    
		    <div class="col-md-4">
		        <label for="medidorNovo">Medidor Novo:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.medidorNumeroSerieNovo" id="medidorNumeroSerieNovo" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.medidorNumeroSerieNovo}"  onkeypress="return formatarCampoAlfaNumerico(event)" 
		                onkeyup="letraMaiuscula(this);"/>
		            <div class="input-group-append">
		                <button id="botaoPesquisar" type="button" onclick="exibirDialogPesquisaMedidor(false);" class="btn btn-primary btn-sm">
		                    <i class="fa fa-search"></i>
		                </button>
		            </div>
		        </div>
		    </div>
		    

		    
		    <div class="col-md-4">
		        <label for="numeroLeituraNovo">Leitura Nova:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.numeroLeituraNovo" id="numeroLeituraNovo" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.numeroLeituraNovo}"  onkeypress="return formatarCampoInteiro(event)" />
		        </div>
		    </div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label>Data Leitura Nova:</label>
						<input class="form-control form-control-sm"
							type="text" id="dataLeituraNovo" name="servicoAutorizacaoTemp.dataLeituraNovo" maxlength="10"
							value="<fmt:formatDate value='${servicoAutorizacao.servicoAutorizacaoTemp.dataLeituraNovo}' pattern='dd/MM/yyyy' />">
					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="funcionario">Funcionário:</label> <select name="servicoAutorizacaoTemp.funcionario"
							id="servicoAutorizacaoTemp.funcionario" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaFuncionario}" var="funcionario">
								<option value="<c:out value="${funcionario.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.funcionario.chavePrimaria == funcionario.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${funcionario.nome}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="funcionario">Operação Medidor:</label> <select name="servicoAutorizacaoTemp.medidorOperacao"
							id="servicoAutorizacaoTemp.medidorOperacao" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaOperacaoMedidor}" var="medidorOperacao">
								<option value="<c:out value="${medidorOperacao.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.medidorOperacao.chavePrimaria == medidorOperacao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${medidorOperacao.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="funcionario">Motivo Operação Medidor:</label> <select name="servicoAutorizacaoTemp.motivoOperacaoMedidor" 
							id="servicoAutorizacaoTemp.motivoOperacaoMedidor" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaMotivoOperacaoMedidor}" var="motivoOperacaoMedidor">
								<option value="<c:out value="${motivoOperacaoMedidor.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.motivoOperacaoMedidor.chavePrimaria == motivoOperacaoMedidor.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${motivoOperacaoMedidor.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
		    <div class="col-md-4">
		        <label for="medidorOriginal">Lacre 1:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.numeroLacreMedidorNovo1" id="numeroLacreMedidorNovo1" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.numeroLacreMedidorNovo1}" onkeypress="return formatarCampoAlfaNumerico(event)" 
		                onkeyup="letraMaiuscula(this);"/>
		        </div>
		    </div>
		    
		    <div class="col-md-4">
		        <label for="medidorOriginal">Lacre 2:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.numeroLacreMedidorNovo2" id="numeroLacreMedidorNovo2" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.numeroLacreMedidorNovo2}" onkeypress="return formatarCampoAlfaNumerico(event)" 
		                onkeyup="letraMaiuscula(this);"/>
		        </div>
		    </div>	
		    
		    <div class="col-md-4">
		        <label for="medidorOriginal">Lacre 3:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.numeroLacreMedidorNovo3" id="numeroLacreMedidorNovo3" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.numeroLacreMedidorNovo3}" onkeypress="return formatarCampoAlfaNumerico(event)" 
		                onkeyup="letraMaiuscula(this);"/>
		        </div>
		    </div>
		    
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="medidorLocalInstalacao">Medidor Local Instalação:</label> <select name="servicoAutorizacaoTemp.medidorLocalInstalacao" 
							id="servicoAutorizacaoTemp.medidorLocalInstalacao" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaLocalInstalacao}" var="medidorLocalInstalacao">
								<option value="<c:out value="${medidorLocalInstalacao.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.medidorLocalInstalacao.chavePrimaria == medidorLocalInstalacao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${medidorLocalInstalacao.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="medidorProtecao">Medidor Proteção:</label> <select name="servicoAutorizacaoTemp.medidorProtecao" 
							id="servicoAutorizacaoTemp.medidorProtecao" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaMedidorProtecao}" var="medidorProtecao">
								<option value="<c:out value="${medidorProtecao.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.medidorProtecao.chavePrimaria == medidorProtecao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${medidorProtecao.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="faixaPressaoFornecimento">Pressão Fornecimento:</label> <select name="servicoAutorizacaoTemp.faixaPressaoFornecimento" 
							id="servicoAutorizacaoTemp.faixaPressaoFornecimento" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaFaixaPressaoFornecimento}" var="faixaPressaoFornecimento">
								<option value="<c:out value="${faixaPressaoFornecimento.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.faixaPressaoFornecimento.chavePrimaria == faixaPressaoFornecimento.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${faixaPressaoFornecimento.getDescricaoFormatada()}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="motivoEncerramentoMobible">Motivo Encerramento:</label> <select name="servicoAutorizacaoTemp.servicoAutorizacaoMotivoEncerramento" 
							id="servicoAutorizacaoTemp.servicoAutorizacaoMotivoEncerramento" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaMotivoEncerramento}" var="motivoEncerramentoMobible">
								<option value="<c:out value="${motivoEncerramentoMobible.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.servicoAutorizacaoMotivoEncerramento.chavePrimaria == motivoEncerramentoMobible.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${motivoEncerramentoMobible.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label>Data Início Execução:</label>
						<input class="form-control form-control-sm"
							type="text" id="inicioExecucao" name="servicoAutorizacaoTemp.inicioExecucao" maxlength="10"
							value="<fmt:formatDate value='${servicoAutorizacao.servicoAutorizacaoTemp.inicioExecucao}' pattern='dd/MM/yyyy HH:mm' />">
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label>Data Fim Execução:</label>
						<input class="form-control form-control-sm"
							type="text" id="fimExecucao" name="servicoAutorizacaoTemp.fimExecucao" maxlength="10"
							value="<fmt:formatDate value='${servicoAutorizacao.servicoAutorizacaoTemp.fimExecucao}' pattern='dd/MM/yyyy HH:mm' />">
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="ocorrencia">Ocorrência:</label> <select name="servicoAutorizacaoTemp.ocorrencia" 
							id="servicoAutorizacaoTemp.ocorrencia" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaOcorrenciaAS}" var="ocorrencia">
								<option value="<c:out value="${ocorrencia.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.ocorrencia.chavePrimaria == ocorrencia.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${ocorrencia.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="">Grau de Risco:</label> <select name="servicoAutorizacaoTemp.descricaoGrauRisco" 
							id="servicoAutorizacaoTemp.descricaoGrauRisco" class="form-control form-control-sm">
							<option value="">Selecione</option>
							<option value="0 - RISCO IMEDIATO ÀS PESSOAS"
							<c:if test="${fn:startsWith(servicoAutorizacao.servicoAutorizacaoTemp.descricaoGrauRisco, '0')}">selected="selected"</c:if>>
							    0 - RISCO IMEDIATO ÀS PESSOAS
							</option>
							<option value="1 - RISCO POTENCIAL COM EXPECTATIVA DE RISCO IMINENTE"
							<c:if test="${fn:startsWith(servicoAutorizacao.servicoAutorizacaoTemp.descricaoGrauRisco, '1')}">selected="selected"</c:if>>
							    1 - RISCO POTENCIAL COM EXPECTATIVA DE RISCO IMINENTE
							</option>
							<option value="2 - SEM IDENTIFICAÇÃO DE RISCO POTENCIAL EM CURTO PRAZO"
							<c:if test="${fn:startsWith(servicoAutorizacao.servicoAutorizacaoTemp.descricaoGrauRisco, '2')}">selected="selected"</c:if>>
							    2 - SEM IDENTIFICAÇÃO DE RISCO POTENCIAL EM CURTO PRAZO
							</option>
							<option value="3 - SEM PERIGO IMEDIATO COM EXPECTATIVA QUE PERMANEÇA ASSIM"
							<c:if test="${fn:startsWith(servicoAutorizacao.servicoAutorizacaoTemp.descricaoGrauRisco, '3')}">selected="selected"</c:if>>
							    3 - SEM PERIGO IMEDIATO COM EXPECTATIVA QUE PERMANEÇA ASSIM
							</option>				
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="localRede">Local:</label> <select name="servicoAutorizacaoTemp.localRede" 
							id="servicoAutorizacaoTemp.localRede" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaLocalAS}" var="localRede">
								<option value="<c:out value="${localRede.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.localRede.chavePrimaria == localRede.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${localRede.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="causaAtendimento">Causa:</label> <select name="servicoAutorizacaoTemp.causaAtendimento" 
							id="servicoAutorizacaoTemp.causaAtendimento" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCausaAS}" var="causaAtendimento">
								<option value="<c:out value="${causaAtendimento.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.causaAtendimento.chavePrimaria == causaAtendimento.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${causaAtendimento.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>	
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="solucaoAtendimento">Solução:</label> <select name="servicoAutorizacaoTemp.solucaoAtendimento" 
							id="servicoAutorizacaoTemp.solucaoAtendimento" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaSolucaoAS}" var="solucaoAtendimento">
								<option value="<c:out value="${solucaoAtendimento.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.solucaoAtendimento.chavePrimaria == solucaoAtendimento.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${solucaoAtendimento.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label>Hora Atendimento:</label>
						<input class="form-control form-control-sm"
							type="text" id="horaAtendimento" name="servicoAutorizacaoTemp.horaAtendimento" maxlength="10"
							value="${servicoAutorizacao.servicoAutorizacaoTemp.horaAtendimento}">
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label>Data Coleta:</label>
						<input class="form-control form-control-sm"
							type="text" id="coletaDataHora" name="servicoAutorizacaoTemp.coletaDataHora" maxlength="10"
							value="<fmt:formatDate value='${servicoAutorizacao.servicoAutorizacaoTemp.coletaDataHora}' pattern='dd/MM/yyyy HH:mm' />">
					</div>
				</div>
			</div>
			
		    <div class="col-md-4">
		        <label for="coletaPressao">Coleta Pressão:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.coletaPressao" id="coletaPressao" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.coletaPressao}"  onkeypress="return formatarCampoDecimalPonto(event, this, 7, 3)" />
		        </div>
		    </div>
		    
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="cilindro1">Primeiro Cilindro:</label> <select name="servicoAutorizacaoTemp.cilindro1" 
							id="servicoAutorizacaoTemp.scilindro1" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCilindroAS}" var="cilindro">
								<option value="<c:out value="${cilindro.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.cilindro1.chavePrimaria == cilindro.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${cilindro.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="cilindro1">Segundo Cilindro:</label> <select name="servicoAutorizacaoTemp.cilindro2" 
							id="servicoAutorizacaoTemp.scilindro2" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCilindroAS}" var="cilindro">
								<option value="<c:out value="${cilindro.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.cilindro2.chavePrimaria == cilindro.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${cilindro.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="cilindro1">Terceiro Cilindro:</label> <select name="servicoAutorizacaoTemp.cilindro3" 
							id="servicoAutorizacaoTemp.scilindro3" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCilindroAS}" var="cilindro">
								<option value="<c:out value="${cilindro.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.cilindro3.chavePrimaria == cilindro.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${cilindro.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="cilindro1">Quarto Cilindro:</label> <select name="servicoAutorizacaoTemp.cilindro4" 
							id="servicoAutorizacaoTemp.scilindro4" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaCilindroAS}" var="cilindro">
								<option value="<c:out value="${cilindro.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.cilindro4.chavePrimaria == cilindro.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${cilindro.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
		        <label for="coletaLacre">Lacre Coleta:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.coletaLacre" id="numeroLacreMedidorNovo3" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.coletaLacre}" onkeypress="return formatarCampoAlfaNumerico(event)" 
		                onkeyup="letraMaiuscula(this);"/>
		        </div>
		    </div>
		    
		    
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="anormalidade">Anormalidade:</label> <select name="servicoAutorizacaoTemp.anormalidade" 
							id="servicoAutorizacaoTemp.anormalidade" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaAnormalidadeAS}" var="anormalidade">
								<option value="<c:out value="${anormalidade.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.anormalidade.chavePrimaria == anormalidade.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${anormalidade.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
		        <label for="identificacao">Identificação:</label>
		        <div class="input-group">
		            <input class="form-control form-control-sm" type="text" name="servicoAutorizacaoTemp.identificacao" id="identificacao" 
		                maxlength="20" size="20" value="${servicoAutorizacao.servicoAutorizacaoTemp.identificacao}" onkeypress="return formatarCampoAlfaNumerico(event)" 
		                onkeyup="letraMaiuscula(this);"/>
		        </div>
		    </div>			
			
			
			<div class="col-md-4">
				<div class="form-row">
					<div class="col-md-10">
						<label for="localInstalacao">Local Instalação:</label> <select name="servicoAutorizacaoTemp.localInstalacao" 
							id="servicoAutorizacaoTemp.localInstalacao" class="form-control form-control-sm">
							<option value="-1">Selecione</option>
							<c:forEach items="${listaLocalInstAS}" var="localInstalacao">
								<option value="<c:out value="${localInstalacao.chavePrimaria}"/>"
									<c:if test="${servicoAutorizacao.servicoAutorizacaoTemp.localInstalacao.chavePrimaria == localInstalacao.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${localInstalacao.descricao}" />
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>					    	
		    
		    
		    													    																																													    			    		    									

</div>



		<!-- Modal Tipo Relatório -->
		<div class="modal fade" id="medidorPopup" tabindex="-1"
		    role="dialog" aria-labelledby="medidorPopup" aria-hidden="true">
		    <div class="modal-dialog modal-dialog-centered" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title" id="exampleModalCenterTitle">Pesquisar Medidor</h5>
		                <button type="button" class="close" data-dismiss="modal"
		                    aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                </button>
		            </div>
		            <div class="modal-body">
		                <div class="form-row">
		                    <div class="col-md-12">
		                        <label for="numeroSerieMobile">Número de série:</label>
		                        <input class="form-control form-control-sm" type="text" name="numeroSerie" id="numeroSerieMobile" 
		                            maxlength="20" size="20" value="" onkeypress="return formatarCampoAlfaNumerico(event)" onkeyup="letraMaiuscula(this);"/>
		                    </div>
		                </div>
		                
		                <div class="form-row">
		                    <div class="col-md-12">
		                        <label for="marcaMedidorMobile">Marca:</label>
		                        <select name="marcaMedidor" id="marcaMedidorMobile" class="form-control form-control-sm">
		                            <option value="-1">Selecione</option>
		                            <c:forEach items="${listaMarcaMedidor}" var="marca">
		                                <option value="<c:out value="${marca.chavePrimaria}"/>">
		                                    <c:out value="${marca.descricao}"/>
		                                </option>        
		                            </c:forEach>
		                        </select>
		                    </div>
		                </div>
		                
		                <div class="form-row">
		                    <div class="col-md-12">
		                        <label for="tipoMedidorMobile">Tipos:</label>
		                        <select name="tipoMedidor" id="tipoMedidorMobile" class="form-control form-control-sm">
		                            <option value="-1">Selecione</option>
		                            <c:forEach items="${listaTipoMedidor}" var="tipo">
		                                <option value="<c:out value="${tipo.chavePrimaria}"/>">
		                                    <c:out value="${tipo.descricao}"/>
		                                </option>        
		                            </c:forEach>
		                        </select>
		                    </div>
		                </div>    
		                
		                <div class="form-row">
		                    <div class="col-md-12">
		                        <label for="modeloMobile">Modelo:</label>
		                        <select name="modelo" id="modeloMobile" class="form-control form-control-sm">
		                            <option value="-1">Selecione</option>
		                            <c:forEach items="${listaModeloMedidor}" var="modelo">
		                                <option value="<c:out value="${modelo.chavePrimaria}"/>">
		                                    <c:out value="${modelo.descricao}"/>
		                                </option>        
		                            </c:forEach>
		                        </select>
		                    </div>
		                </div>  
		                
		                <div class="form-row">
		                    <div class="col-md-12">
		                        <label for="modoUso" class="col-md-12">Perfil:</label>
		                            <div class="col-md-12">
		                                <div class="custom-control custom-radio custom-control-inline">
		                                    <input type="radio" name="modoUsoMobile" id="modoUsoNormalMobile" class="custom-control-input" value="${modoUsoNormal}">
		                                    <label class="custom-control-label" for="modoUsoNormalMobile">Normal</label>
		                                </div>
		                                <div class="custom-control custom-radio custom-control-inline">
		                                    <input type="radio" name="modoUsoMobile" id="modoUsoVirtualMobile" class="custom-control-input" value="${modoUsoVirtual}">
		                                    <label class="custom-control-label" for="modoUsoVirtualMobile">Virtual</label>
		                                </div>
		                                <div class="custom-control custom-radio custom-control-inline">
		                                    <input type="radio" name="modoUsoMobile" id="modoUsoIndependenteMobile" class="custom-control-input" value="${modoUsoIndependente}">
		                                    <label class="custom-control-label" for="modoUsoIndependenteMobile">Independente</label>
		                                </div>
		                                <div class="custom-control custom-radio custom-control-inline">
		                                    <input type="radio" name="modoUsoMobile" id="modoUsoTodosMobile" checked="checked" class="custom-control-input" value="">
		                                    <label class="custom-control-label" for="modoUsoTodosMobile">Todos</label>
		                                </div>                                        
		                            </div>
		                    </div>
		                </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-primary" name="buttonPesquisarMedidorMobile" id="buttonPesquisarMedidorMobile" >Pesquisar</button>
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
		            </div>
		            <!-- Mover a grid para fora da modal-footer -->
		            <div id="gridMobileMedidores">
		                <jsp:include page="/jsp/atendimento/servicoautorizacao/bootstrap/gridMedidorMobileBootstrap.jsp"/>
		            </div>
		           <div class="row mt-3">
						<div class="col align-self-end text-right mb-1 mr-1">
		            		<button type="button" class="btn btn-primary" name="buttonAdicionarMedidor" id="buttonAdicionarMedidor" onclick="adicionarMedidor();">Adicionar</button>
		            	</div>
		            </div>
		            <br/><br/><br/><br/>
		        </div>
		    </div>
		</div>

    