<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Pesquisar Material<a class="linkHelp" href="<help:help>/*</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">

function pesquisar() {
	submeter('materialForm','pesquisarMaterial');
}

function alterarMaterial() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms["materialForm"].chavesPrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('materialForm','exibirAlteracaoMaterial');
    }
	
}

function detalharMaterial(chave){
	document.forms["materialForm"].chavesPrimaria.value = chave;
	submeter('materialForm','exibirDetalhamentoMaterial');
}

function limparFormulario(){
	document.getElementById('descricao').value = "";
	document.forms['materialForm'].habilitado[0].checked = true;
	document.forms['materialForm'].unidadeMedida.value = "-1";
		
}

function incluir() {
	location.href = '<c:url value="/exibirInclusaoMaterial"/>';
}

function removerMaterial(){
	
	var selecao = verificarSelecao();	
	
	if (selecao == true) {	
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
		if(retorno == true) {
			submeter('materialForm', 'removerMaterial');
		}
    }
}

</script>

<form action="pesquisarMaterial" id="materialForm" name="materialForm" method="post" >
	<fieldset class="conteinerPesquisarIncluirAlterar">
	
		<input name="chavesPrimaria" type="hidden" id="chavesPrimaria" > 	
		<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" > 	
		
				
		<fieldset id="materialCol1" class="coluna">
			<label class="rotulo" id="rotuloDescricao" for="descricao" >Descri��o do Material:</label>
			<input class="campoTexto" type="text" name="descricao" id="descricao" size="50" maxlength="40" value="${material.descricao}" onkeyup="letraMaiuscula(this);" /><br />
			
			<label class="rotulo" for="unidadeMedida">Unidade de Medida:</label>
			<select id="unidadeMedida" class="campoSelect" name="unidadeMedida">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadeMedida}" var="unidade">
				<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${material.unidadeMedida.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidade.descricao}"/>
				</option>
				</c:forEach>
			</select>		
		</fieldset>
		
		<fieldset id="pesquisarTabelaAuxiliarCol1" class="colunaFinalMaterial">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${habilitado eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</br>
		</fieldset>	
		<fieldset class="conteinerBotoesPesquisarDirFixo">
<%-- 			<vacess:vacess param="pesquisarMaterial">		 --%>
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" onclick="pesquisar();">
<%-- 	    	</vacess:vacess>		 --%>
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>	
	
	
	
	
	<c:if test="${listaMaterial ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaMaterial" sort="list" id="material" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

 			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="${material.chavePrimaria}">
	        </display:column>
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${material.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column>

			<display:column sortable="true" sortProperty="descricao" title="Descri��o" style="width: 130px">
				<a href="javascript:detalharMaterial(<c:out value='${material.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${material.descricao}"/>
	            </a>
<%-- 	            <input id="descricaoTooltip" name="descricaoTooltip" type="text" title="Descri��o: ${material.descricao}"/> --%>
			</display:column>				
			
			<display:column  sortable="true" sortProperty="unidadeMedida.descricao" title="Unidade de Medida" style="width: 130px">
				<a href="javascript:detalharMaterial(<c:out value='${material.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${material.unidadeMedida.descricao}"/>
	            </a>
			</display:column>
	        
	        <display:column sortable="true" sortProperty="codigoMaterial" title="C�digo ERP" style="width: 130px">
	        	<a href="javascript:detalharMaterial(<c:out value='${material.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${material.codigoMaterial}"/>
	            </a>
	        </display:column>
	        
	    </display:table>	
	</c:if>
	
	</form>
		
	<fieldset class="conteinerBotoes">
  		<c:if test="${not empty listaMaterial}">
  			<vacess:vacess param="exibirAlteracaoMaterial">
  				<input id="buttonAlterar" value="Alterar" class="bottonRightCol2" onclick="alterarMaterial()" type="button">
  			</vacess:vacess>
  			<vacess:vacess param="removerMaterial">
				<input id="buttonRemover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerMaterial()" type="button">
			</vacess:vacess>
   		</c:if>
   		<vacess:vacess param="exibirInclusaoMaterial">
   			<input id="buttonIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" onclick="incluir()" type="button">
   		</vacess:vacess>
	</fieldset>

	