<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script>
var tiposPessoaFisica = new Array();
var nacionalidadesEstrangeira = new Array();

function carregarArrays() {
	//Tipos de clientes
	<c:forEach items="${tiposClientes}" var="tipoCliente">
	 <c:if test="${tipoCliente.tipoPessoa.codigo == tipoPessoaFisica}">
	 tiposPessoaFisica[tiposPessoaFisica.length] = '${tipoCliente.chavePrimaria}';
	 </c:if>
	</c:forEach>
	//Nacionalidades
	<c:forEach items="${listaNacionalidades}" var="nacionalidade">
	 <c:if test="${nacionalidade.estrangeira == true}">
	 nacionalidadesEstrangeira[nacionalidadesEstrangeira.length] = '${nacionalidade.chavePrimaria}';
	 </c:if>
	</c:forEach>
}

function limparFormulario(){

	//Cliente - dados comuns
	document.clienteForm.nome.value = "";
	document.clienteForm.nomeAbreviado.value = "";
	document.clienteForm.idTipoCliente.value = "-1";
	document.clienteForm.emailPrincipal.value = "";
	document.clienteForm.emailSecundario.value = "";
	document.clienteForm.idSituacaoCliente.value = "-1";
	
	//Aba de documenta��o.
	document.clienteForm.cpf.value = "";
	document.clienteForm.rg.value = "";
	document.clienteForm.dataEmissaoRG.value = "";
	document.clienteForm.idOrgaoEmissor.value = "-1";
	document.clienteForm.idOrgaoEmissorUF.value = "-1";
	document.clienteForm.passaporte.value = "";
	document.clienteForm.dataNascimento.value = "";
	document.clienteForm.idProfissao.value = "-1";
	document.clienteForm.idSexo.value = "";
	document.clienteForm.nomeMae.value = "";
	document.clienteForm.nomePai.value = "";
	document.clienteForm.idFaixaRendaFamiliar.value = "-1";
	document.clienteForm.idNacionalidade.value = "-1";
	document.clienteForm.nomeFantasia.value = "";
	document.clienteForm.cnpj.value = "";
	document.clienteForm.idAtividadeEconomia.value = "-1";
	document.clienteForm.descricaoAtividadeEconomica.value = "";
	document.clienteForm.descricaoCompletaAtividadeEconomica.value = "";
	document.clienteForm.inscricaoEstadual.value = "";
	document.clienteForm.inscricaoMunicipal.value = "";
	document.clienteForm.inscricaoRural.value = "";
	document.clienteForm.nomeResponsavelSuperiorDes.value = "";
	
	//Aba Endere�o.
	document.clienteForm.idTipoEndereco.value = "-1";
	document.clienteForm.cep.value = "";
	document.clienteForm.endereco.value = "";
	document.clienteForm.cidade.value = "";
	document.clienteForm.uf.value = "";
	document.clienteForm.numeroEndereco.value = "";
	document.clienteForm.complementoEndereco.value = "";
	document.clienteForm.referenciaEndereco.value = "";
	document.clienteForm.caixaPostal.value = "";
	
	//Aba Telefone
	document.clienteForm.idTipoTelefone.value = "-1";
	document.clienteForm.dddTelefone.value = "";
	document.clienteForm.numeroTelefone.value = "";
	document.clienteForm.ramalTelefone.value = "";

	//Aba Contato
	document.clienteForm.idTipoContato.value = "-1";
	document.clienteForm.nomeContato.value = "";	
	document.clienteForm.dddContato.value = "";
	document.clienteForm.telefoneContato.value = "";
	document.clienteForm.ramalContato.value = "";
	document.clienteForm.cargoContato.value = "";
	document.clienteForm.areaContato.value = "";
	document.clienteForm.emailContato.value = "";
	
	exibirDocumentacaoNecessaria(document.clienteForm.idTipoCliente);
}


function cancelar(){
	submeter("chamadoForm", "exibirPesquisaChamado");
}

function exibirDocumentacaoNecessaria(idTipoCliente) {
	exibirCamposDocumentacao(idTipoCliente.value);	
}

function exibirCamposDocumentacao(idTipoCliente) {
	var exibirPessoaFisica = false;
	
	if (idTipoCliente != "" && idTipoCliente != -1) {
		for (var i=0; i<tiposPessoaFisica.length; i++) {
			if (tiposPessoaFisica[i] == idTipoCliente) {
				exibirPessoaFisica = true;
				break;
			}
		}
		
		if (exibirPessoaFisica == true) {
			exibirElemento('pessoaFisica');
			ocultarElemento('pessoaJuridica');
		} else {
			exibirElemento('pessoaJuridica');
			ocultarElemento('pessoaFisica');
		}	
	} else {
		ocultarElemento('pessoaJuridica');
		ocultarElemento('pessoaFisica');
	}
}

function exibirCamposDocumentacaoChamadoAlteracaoCliente(idTipoCliente) {
	var exibirPessoaFisica = false;
	
	if (idTipoCliente != "" && idTipoCliente != -1) {
		for (var i=0; i<tiposPessoaFisica.length; i++) {
			if (tiposPessoaFisica[i] == idTipoCliente) {
				exibirPessoaFisica = true;
				break;
			}
		}
		
		if (exibirPessoaFisica == true) {
			exibirElemento('pessoaFisicaChamadoAlteracaoCliente');
			ocultarElemento('pessoaJuridicaChamadoAlteracaoCliente');
		} else {
			exibirElemento('pessoaJuridicaChamadoAlteracaoCliente');
			ocultarElemento('pessoaFisicaChamadoAlteracaoCliente');
			
		}	
	} else {
		ocultarElemento('pessoaJuridicaChamadoAlteracaoCliente');
		ocultarElemento('pessoaFisicaChamadoAlteracaoCliente');
	}
}

function habilitarDocumentacaoNecessariaNacionalidade(idNacionalidade) {
	exibirDocumentacaoNecessariaNacionalidadeHabilitado(idNacionalidade.value);	
}

function exibirDocumentacaoNecessariaNacionalidadeHabilitado(idNacionalidade) {
	var desabilidarDadosNacionalidadeBrasileira = false;
	
	if (idNacionalidade != "" && idNacionalidade != -1) {
		for (var i=0; i<nacionalidadesEstrangeira.length; i++) {
			if (nacionalidadesEstrangeira[i] == idNacionalidade) {
				desabilidarDadosNacionalidadeBrasileira = true;
				break;
			}
		}
		
		if (desabilidarDadosNacionalidadeBrasileira == true) {
			
			//Desabilitar os campos da nascionalidade brasileira;
			document.forms[0].cpf.disabled = true;
			document.forms[0].rg.disabled = true;
			document.forms[0].dataEmissaoRG.disabled = true;
			document.forms[0].idOrgaoEmissor.disabled = true;
			document.forms[0].idOrgaoEmissorUF.disabled = true;
			document.forms[0].passaporte.disabled = false;
			document.getElementById("campoObrigatorioSimboloPassaporte").style.display = "";
			document.getElementById("rotuloPassaporte").className = "rotulo campoObrigatorio";
			document.getElementById("campoObrigatorioSimboloCpf").style.display = "none";
			document.getElementById("rotuloCPF").className = "rotulo";

			$(document).ready(function(){
				//Adiciona ou remove a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
				$("input[type='text']:disabled").addClass("campoDesabilitado");
				$("input[type='text']:enabled").removeClass("campoDesabilitado");
			});
		} else {
			//habilitar os campos da nascionalidade estrangeira;
			//document.forms[0].cpf.disabled = false;
			document.forms[0].rg.disabled = false;
			document.forms[0].dataEmissaoRG.disabled = false;
			document.forms[0].orgaoExpedidor.disabled = false;
			document.forms[0].unidadeFederacao.disabled = false;
			document.forms[0].numeroPassaporte.disabled = true;
			document.getElementById("campoObrigatorioSimboloPassaporte").style.display = "none";
			document.getElementById("rotuloPassaporte").className = "rotulo";
			document.getElementById("campoObrigatorioSimboloCpf").style.display = "";
			document.getElementById("rotuloCPF").className = "rotulo campoObrigatorio";

			$(document).ready(function(){
				//Adiciona ou remove a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
				$("input[type='text']:disabled").addClass("campoDesabilitado");
				$("input[type='text']:enabled").removeClass("campoDesabilitado");
				
			});
		}	
	} else {
		//Desabilitar os campos da pessoa da nascionalidade brasileira e estrangeira
		document.forms[0].cpf.disabled = true;
		document.forms[0].rg.disabled = true;
		document.forms[0].dataEmissaoRG.disabled = true;
		document.forms[0].idOrgaoEmissor.disabled = true;
		document.forms[0].idOrgaoEmissorUF.disabled = true;
		document.forms[0].passaporte.disabled = true;
		document.getElementById("campoObrigatorioSimboloPassaporte").style.display = "none";
		document.getElementById("rotuloPassaporte").className = "rotulo";
		document.getElementById("campoObrigatorioSimboloCpf").style.display = "none";
		document.getElementById("rotuloCPF").className = "rotulo";

		$(document).ready(function(){
			//Adiciona ou remove a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
			$("input[type='text']:disabled").addClass("campoDesabilitado");
		});
	}
}

function exibirDocumentacaoNecessariaNacionalidadeHabilitadoChamadoAlteracaoCliente(idNacionalidade) {
	var desabilidarDadosNacionalidadeBrasileira = false;
	
	if (idNacionalidade != "" && idNacionalidade != -1) {
		for (var i=0; i<nacionalidadesEstrangeira.length; i++) {
			if (nacionalidadesEstrangeira[i] == idNacionalidade) {
				desabilidarDadosNacionalidadeBrasileira = true;
				break;
			}
		}
		
		if (desabilidarDadosNacionalidadeBrasileira == true) {
			
			//Desabilitar os campos da nascionalidade brasileira;
			//document.forms[0].cpf.disabled = true;
			document.forms[0].rg.disabled = true;
			document.forms[0].dataEmissaoRG.disabled = true;
			document.forms[0].idOrgaoEmissor.disabled = true;
			document.forms[0].idOrgaoEmissorUF.disabled = true;
			document.forms[0].passaporte.disabled = false;
			document.getElementById("campoObrigatorioSimboloPassaporte").style.display = "";
			document.getElementById("rotuloPassaporte").className = "rotulo campoObrigatorio";
			document.getElementById("campoObrigatorioSimboloCpf").style.display = "none";
			document.getElementById("rotuloCPF").className = "rotulo";

			$(document).ready(function(){
				//Adiciona ou remove a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
				$("input[type='text']:disabled").addClass("campoDesabilitado");
				$("input[type='text']:enabled").removeClass("campoDesabilitado");
			});
		} else {
			//habilitar os campos da nascionalidade estrangeira;
			//document.forms[0].cpf.disabled = false;
			document.forms[0].rg.disabled = false;
			document.forms[0].dataEmissaoRG.disabled = false;
			document.forms[0].orgaoExpedidor.disabled = false;
			document.forms[0].unidadeFederacao.disabled = false;
			document.forms[0].numeroPassaporte.disabled = true;
			document.getElementById("campoObrigatorioSimboloPassaporte").style.display = "none";
			document.getElementById("rotuloPassaporte").className = "rotulo";
			document.getElementById("campoObrigatorioSimboloCpf").style.display = "";
			document.getElementById("rotuloCPF").className = "rotulo campoObrigatorio";

			$(document).ready(function(){
				//Adiciona ou remove a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
				$("input[type='text']:disabled").addClass("campoDesabilitado");
				$("input[type='text']:enabled").removeClass("campoDesabilitado");
			});
		}	
	} else {
		//Desabilitar os campos da pessoa da nascionalidade brasileira e estrangeira
		document.forms[0].cpf.disabled = true;
		document.forms[0].rg.disabled = true;
		document.forms[0].dataEmissaoRG.disabled = true;
		document.forms[0].idOrgaoEmissor.disabled = true;
		document.forms[0].idOrgaoEmissorUF.disabled = true;
		document.forms[0].passaporte.disabled = true;
		document.getElementById("campoObrigatorioSimboloPassaporte").style.display = "none";
		document.getElementById("rotuloPassaporte").className = "rotulo";
		document.getElementById("campoObrigatorioSimboloCpf").style.display = "none";
		document.getElementById("rotuloCPF").className = "rotulo";

		$(document).ready(function(){
			//Adiciona ou remove a classe "campoDesabilitado" para uniformizar a apar�ncia dos campos desabilitados em todos os browsers.
			$("input[type='text']:disabled").addClass("campoDesabilitado");
		});
	}
}

function manterExibicaoDocumentacaoNecessaria(idTipoCliente, idNacionalidade) {
	if (idTipoCliente != "") {
		exibirCamposDocumentacao(idTipoCliente);	
	}
	if (idNacionalidade != "") {
		exibirDocumentacaoNecessariaNacionalidadeHabilitado(idNacionalidade);	
	}
}

function manterExibicaoDocumentacaoNecessariaChamadoAlteracaoCliente(idTipoCliente, idNacionalidade) {
	if (idTipoCliente != "") {
		exibirCamposDocumentacaoChamadoAlteracaoCliente(idTipoCliente);	
	}
	if (idNacionalidade != "") {
		exibirDocumentacaoNecessariaNacionalidadeHabilitadoChamadoAlteracaoCliente(idNacionalidade);	
	}
}

function init () {
	carregarArrays();
	
	var idTipoCliente = "${chamadoAlteracaoCliente.tipoCliente.chavePrimaria}";
	var idNacionalidade = "${chamadoAlteracaoCliente.nacionalidade.chavePrimaria}";
	manterExibicaoDocumentacaoNecessariaChamadoAlteracaoCliente(idTipoCliente, idNacionalidade);
	

	idTipoCliente = "${chamadoAlteracaoCliente.cliente.tipoCliente.chavePrimaria}";
	idNacionalidade = "${chamadoAlteracaoCliente.cliente.nacionalidade.chavePrimaria}";
	manterExibicaoDocumentacaoNecessaria(idTipoCliente, idNacionalidade);
	
	var indicadorAtualizacaoCadastral = "${sessionScope.indicadorAtualizacaoCadastral}";	
	if(indicadorAtualizacaoCadastral != ''){
		$('#botaoCancelar').attr("disabled","disabled");		
	}
}

//abilita campos desabilitados para poder pegar o valor na action
function abilitarCamposDesabilitados(){
	abilitarInscricaoEstadual();
}

function salvarAlteracaoCadastroClienteChamado(){
	var chavePrimariaCliente = document.forms[0].chavePrimariaCliente.value;
	var chaveChamado = $("#chavePrimaria").val();
	submeter('chamadoForm','salvarAlteracaoCadastroClienteChamado?chavePrimariaCliente='+chavePrimariaCliente +"&chavePrimaria="+chaveChamado);
}

function preencherMapa(idCampo, valorChecked, valor){
	
	var url = "preencherMapaAlteracaoCadastroClienteChamado?idCampo=" + idCampo + "&valorChecked="
	+ valorChecked + "&valor=" + valor + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		});
	
}


addLoadEvent(init);

</script>

<h1 class="tituloInterno">Altera��o de Dados Cadastrais<a href="<help:help>/cadastroclienteinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para alterar um Cliente, confirme os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>

<form:form method="post" action="salvarAlteracaoCadastroClienteChamado" enctype="multipart/form-data" id="chamadoForm" name="chamadoForm" modelAttribute="chamadoAlteracaoCliente"> 

	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chamado.chavePrimaria}"/>
	
	
	<fieldset id="conteinerCliente" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset>
			<label class="rotulo" id="tituloSolicitacao">Solicita��o de Altera��o - Ag�ncia Virtual</label>
		</fieldset>
		
		<fieldset id="clienteCol1" class="coluna">
		<label class="rotulo campoObrigatorio" for="tipoClienteChamadoAlteracaoCliente"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
			<select name="tipoCliente" id="tipoCliente" disabled="disabled" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${tiposClientes}" var="tipoCliente">
					<option value="<c:out value="${tipoCliente.chavePrimaria}"/>" title="<c:out value="${tipoCliente.descricao}"/>" <c:if test="${chamadoAlteracaoCliente.tipoCliente.chavePrimaria == tipoCliente.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoCliente.descricao}"/>
					</option>
				</c:forEach>
			</select>
			<input class="solicitacaoAlteracaoTipoClienteCheckBox" type="checkbox" id="tipoClienteCheckbox" name="tipoClienteCheckbox" onclick="preencherMapa(this.id, this.checked, this.value);" value="<c:if test="${chamadoAlteracaoCliente.tipoCliente.chavePrimaria ne null}">${chamadoAlteracaoCliente.tipoCliente.chavePrimaria}</c:if>"><br />
		
			<label class="rotulo campoObrigatorio" id="rotuloNome" for="nome"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
			<input class="campoTexto" type="text" id="nome" name="nome" disabled="disabled" maxlength="50" size="40" value="${chamadoAlteracaoCliente.nome}"> 
			<input class="solicitacaoAlteracaoNomeCheckBox" type="checkbox" name="nomeCheckbox" id="nomeCheckbox" value="${chamadoAlteracaoCliente.nome}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
			
			<label class="rotulo" id="rotuloNomeAbreviado" for="nomeAbreviado" >Nome abreviado:</label>
			<input class="campoTexto" type="text" id="nomeAbreviado" name="nomeAbreviado" disabled="disabled" maxlength="5" size="20"  value="<c:if test="${chamadoAlteracaoCliente.nomeAbreviado ne null}">${chamadoAlteracaoCliente.nomeAbreviado}</c:if>">  
			<input class="solicitacaoAlteracaoNomeAbreviadoCheckBox" type="checkbox" id="nomeAbreviadoCheckbox" name="nomeAbreviadoCheckbox" onclick="preencherMapa(this.id, this.checked, this.value);" value="<c:if test="${chamadoAlteracaoCliente.nomeAbreviado ne null}">${chamadoAlteracaoCliente.nomeAbreviado}</c:if>"><br />
			
			<label class="rotulo campoObrigatorio" id="rotuloEmailPrincipal" for="emailPrincipal" ><span class="campoObrigatorioSimbolo">* </span>E-mail principal:</label>
			<input class="campoTexto" type="text" id="emailPrincipal" name="emailPrincipal" disabled="disabled" maxlength="80" size="40" value="<c:if test="${chamadoAlteracaoCliente.emailPrincipal ne null}">${chamadoAlteracaoCliente.emailPrincipal}</c:if>">
			<input class="solicitacaoAlteracaoEmailPrincipalCheckBox" type="checkbox" id="emailPrincipalCheckbox" name="emailPrincipalCheckbox" onclick="preencherMapa(this.id, this.checked, this.value);" value="<c:if test="${chamadoAlteracaoCliente.emailPrincipal ne null}">${chamadoAlteracaoCliente.emailPrincipal}</c:if>"><br />
			
			<label class="rotulo" id="rotuloEmailSecundario" for="emailSecundario" >E-mail secund�rio:</label>
			<input class="campoTexto" type="text" id="emailSecundario" name="emailSecundario" disabled="disabled" maxlength="80" size="40" value="<c:if test="${chamadoAlteracaoCliente.emailSecundario ne null}">${chamadoAlteracaoCliente.emailSecundario}</c:if>">
			<input class="solicitacaoAlteracaoEmailSecundarioCheckBox" type="checkbox" id="emailSecundarioCheckbox" name="emailSecundarioCheckbox" onclick="preencherMapa(this.id, this.checked, this.value);" value="<c:if test="${chamadoAlteracaoCliente.emailSecundario ne null}">${chamadoAlteracaoCliente.emailSecundario}</c:if>"><br />
			
	</fieldset>
	
		<fieldset>
			<label class="rotulo tituloCadastro" id="tituloCadastro">Cadastro do Cliente - GGAS</label>
		</fieldset>
		
		<fieldset id="clienteCol2" class="clienteCol2">
		
			<input type="hidden" name="chavePrimariaCliente" id="chavePrimariaCliente" value="${chamadoAlteracaoCliente.cliente.chavePrimaria}"/>
		
			<label class="rotulo campoObrigatorio" for="tipoClienteCliente"><span class="campoObrigatorioSimbolo">* </span>Tipo:</label>
			<select name="chamadoAlteracaoCliente.cliente.tipoCliente" disabled="disabled" id="chamadoAlteracaoCliente.cliente.tipoCliente" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${tiposClientes}" var="tipoCliente">
					<option value="<c:out value="${tipoCliente.chavePrimaria}"/>" title="<c:out value="${tipoCliente.descricao}"/>" <c:if test="${chamadoAlteracaoCliente.cliente.tipoCliente.chavePrimaria == tipoCliente.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoCliente.descricao}"/>
					</option>
				</c:forEach>
			</select>
		
			<label class="rotulo campoObrigatorio" id="rotuloNome" for="nomeCLiente"><span class="campoObrigatorioSimbolo">* </span>Nome:</label>
			<input class="campoTexto" type="text" id="chamadoAlteracaoCliente.cliente.nome" disabled="disabled"  maxlength="50" size="40" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${chamadoAlteracaoCliente.cliente.nome}"> 
			
			<label class="rotulo" id="rotuloNomeAbreviado" for="nomeAbreviadoCliente" >Nome abreviado:</label>
			<input class="campoTexto" type="text" id="chamadoAlteracaoCliente.cliente.nomeAbreviado" disabled="disabled" name="chamadoAlteracaoCliente.cliente.nomeAbreviado" maxlength="5" size="20" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${chamadoAlteracaoCliente.cliente.nomeAbreviado}"> 

			<label class="rotulo campoObrigatorio" id="rotuloEmailPrincipal" for="emailPrincipalCliente" ><span class="campoObrigatorioSimbolo">* </span>E-mail principal:</label>
			<input class="campoTexto" type="text" id="chamadoAlteracaoCliente.cliente.emailPrincipal" disabled="disabled" name="chamadoAlteracaoCliente.cliente.emailPrincipal" maxlength="80" size="40" onkeypress="return formatarCampoEmail(event);" value="${chamadoAlteracaoCliente.cliente.emailPrincipal}">

			<label class="rotulo" id="rotuloEmailSecundario" for="emailSecundarioCliente" >E-mail secund�rio:</label>
			<input class="campoTexto" type="text" id="chamadoAlteracaoCliente.cliente.emailSecundario" disabled="disabled" name="chamadoAlteracaoCliente.cliente.emailSecundario" maxlength="80" size="40" onkeypress="return formatarCampoEmail(event);" value="${chamadoAlteracaoCliente.cliente.emailSecundario}">
			
			<label class="rotulo" for="clienteSituacaoCliente">Situa��o:</label>
			<select name="chamadoAlteracaoCliente.cliente.clienteSituacao" disabled="disabled" id="chamadoAlteracaoCliente.cliente.clienteSituacao" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${situacoesClientes}" var="clienteSituacao">
				<option value="<c:out value="${clienteSituacao.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.cliente.clienteSituacao.chavePrimaria == clienteSituacao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${clienteSituacao.descricao}"/>
				</option>		
			</c:forEach>
			</select>
		</fieldset>
		

		<fieldset id="tabs" style="display: none">
		<ul>			
			
			
			
			<li><a href="#abaDadosIdentificacaoSolicitarAlteracaoCadastral"><span class="campoObrigatorioSimboloTabs">* </span>Identifica��o</a></li>
		<li><a href="#abaEnderecoSolicitarAlteracaoCadastral"><span class="campoObrigatorioSimboloTabs">* </span>Endere�o</a></li>
		<li><a href="#abaDadosTelefoneSolicitarAlteracaoCadastral"><span class="campoObrigatorioSimboloTabs">* </span>Telefone</a></li>
		<li><a href="#abaDadosContatoSolicitarAlteracaoCadastral">Contatos</a></li>
		</ul>
		<fieldset id="abaEnderecoSolicitarAlteracaoCadastral">
			<jsp:include page="/jsp/atendimento/chamado/abaEnderecoSolicitarAlteracaoCadastral.jsp"></jsp:include>
		</fieldset>
		<fieldset id="abaDadosTelefoneSolicitarAlteracaoCadastral">
			<jsp:include page="/jsp/atendimento/chamado/abaDadosTelefoneSolicitarAlteracaoCadastral.jsp"></jsp:include>
		</fieldset>
		<fieldset id="abaDadosContatoSolicitarAlteracaoCadastral">
			<jsp:include page="/jsp/atendimento/chamado/abaContatoSolicitarAlteracaoCadastral.jsp"></jsp:include>
		</fieldset>
		<fieldset id="abaDadosIdentificacaoSolicitarAlteracaoCadastral">
			<jsp:include page="/jsp/atendimento/chamado/abaIdentificacaoSolicitarAlteracaoCadastral.jsp"></jsp:include>
		</fieldset>
		
	</fieldset>

	<fieldset class="conteinerBotoes"> 

		<input name="botaoCancelar" id="botaoCancelar" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar();">
	
	    	<input name="button" class="bottonRightCol2" style="float: right;" type="button" botaoGrande1 botaoIncluir" value="Salvar"  onclick="salvarAlteracaoCadastroClienteChamado();">
  </fieldset>
</form:form> 