<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 
<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %> 
 
<script>

$(document).ready(function(){
	   $("#cpf,#cpfCliente").inputmask("999.999.999-99",{placeholder:"_"});
	   $("#cnpj,#cnpjCliente").inputmask("99.999.999/9999-99",{placeholder:"_"});
	   
	   
	});

function visualizarAnexoAbaIdentificacao(indexListaAnexoAbaEndereco){
	
	submeter('chamadoForm','visualizarAnexoAbaEndereco?chavePrimaria='+indexListaAnexoAbaEndereco);
}

function visualizarAnexoAbaIdentificacaoCliente(indexListaAnexoAbaEndereco){
	
	submeter('chamadoForm','visualizarAnexoAbaEnderecoCliente?chavePrimaria='+indexListaAnexoAbaEndereco);
}

function preencherMapaListaAnexoAlteracaoCadastroClienteChamado(chavePrimariaChamadoAnexo, chavePrimariaAnexo, valorChecked){
	
	var url = "preencherMapaListaAnexoAlteracaoCadastroClienteChamado?chavePrimariaAnexo=" + chavePrimariaAnexo + "&chavePrimariaChamadoAnexo=" + chavePrimariaChamadoAnexo + "&valorChecked="
	+ valorChecked + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		});
	
}

</script>

<div id="clienteAbaIdentificacao">
<fieldset  class="conteinerDados3">
	<fieldset id="pessoaFisicaChamadoAlteracaoCliente" class="colunaEsq">
	<fieldset id="clienteEnderecoCol1" class="colunaEsq">
	
	<input type="hidden" name="indexLista" id="indexLista">
	<input type="hidden" name="chavePrimariaClienteEndereco" id="chavePrimariaClienteEndereco">
	
	<label class="rotulo campoObrigatorio" id="labelNascionalidade" for="nacionalidade"><span class="campoObrigatorioSimbolo">* </span>Nacionalidade:</label>
		<select name="nacionalidade" id="nacionalidade" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaNacionalidades}" var="nacionalidade">
				<option value="<c:out value="${nacionalidade.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.nacionalidade.chavePrimaria == nacionalidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${nacionalidade.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<input class="abaIdentificacaoCheckboxPessoaFisica_a" type="checkbox" id="nacionalidadeCheckboxIdentificacao" name="nacionalidadeCheckboxIdentificacao" value="${chamadoAlteracaoCliente.nacionalidade.chavePrimaria}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
	
		<label class="rotulo" id="rotuloCPF" for="cpf"><span class="campoObrigatorioSimbolo" id="campoObrigatorioSimboloCpf" style="display: none;">* </span>CPF:</label>
		<input class="campoTexto" type="text" id="cpf" name="cpf" disabled="disabled"  maxlength="14" size="14" value="${chamadoAlteracaoCliente.cpf}"><br />
		<input class="abaIdentificacaoCheckboxPessoaFisica_b" type="checkbox" id="cpfCheckboxIdentificacao" name="cpfCheckboxIdentificacao" value="${chamadoAlteracaoCliente.cpf}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloRG" for="rg">RG:</label>
		<input class="campoTexto" type="text" id="rg" name="rg" disabled="disabled" maxlength="20" size="10" value="${chamadoAlteracaoCliente.rg}"><br />
		<input class="abaIdentificacaoCheckboxPessoaFisica_c" type="checkbox" id="rgCheckboxIdentificacao" name="rgCheckboxIdentificacao" value="${chamadoAlteracaoCliente.rg}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloDataEmissaoRG" for="dataEmissaoRG" >Data emiss�o:</label>
		<input class="campoData" type="text" id="dataEmissaoRG" name="dataEmissaoRG" disabled="disabled"  value="${chamadoAlteracaoCliente.dataEmissaoRGString}" maxlength="10"><br />
		<input class="abaIdentificacaoCheckboxPessoaFisica_d" type="checkbox" id="dataEmissaoRGCheckboxIdentificacao" name="dataEmissaoRGCheckboxIdentificacao" value="${chamadoAlteracaoCliente.dataEmissaoRGString}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloOrgaoEmissorRG" for="orgaoExpedidor">�rg�o emissor:</label>
		
		<select name="orgaoExpedidor" id="orgaoExpedidor" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${orgaosEmissores}" var="orgaoEmissor">
				<option value="<c:out value="${orgaoEmissor.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.orgaoExpedidor.chavePrimaria == orgaoEmissor.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${orgaoEmissor.descricaoAbreviada}"/>
				</option>		
			</c:forEach>
		</select>
		<input class="abaIdentificacaoCheckboxPessoaFisica_e" type="checkbox" id="orgaoExpedidorCheckboxIdentificacao" name="orgaoExpedidorCheckboxIdentificacao" value="${chamadoAlteracaoCliente.orgaoExpedidor.chavePrimaria}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloOrgaoEmissorUF" for="unidadeFederacao">UF:</label>
		<select name="unidadeFederacao" id="unidadeFederacao" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${unidadesFederacao}" var="unidadeFederacao">
				<option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.unidadeFederacao.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadeFederacao.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<input class="abaIdentificacaoCheckboxPessoaFisica_f" type="checkbox" id="unidadeFederacaoCheckboxIdentificacao" name="unidadeFederacaoCheckboxIdentificacao" value="${chamadoAlteracaoCliente.unidadeFederacao.chavePrimaria}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label id="rotuloPassaporte" class="rotulo" for="numeroPassaporte"><span class="campoObrigatorioSimbolo" id="campoObrigatorioSimboloPassaporte" style="display: none;">* </span>Passaporte:</label>
		<input class="campoTexto" type="text" id="numeroPassaporte" name="numeroPassaporte" disabled="disabled" maxlength="13" size="13" value="${chamadoAlteracaoCliente.numeroPassaporte}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		<input class="abaIdentificacaoCheckboxPessoaFisica_g" type="checkbox" id="numeroPassaporteCheckboxIdentificacao" name="numeroPassaporteCheckboxIdentificacao" value="${chamadoAlteracaoCliente.numeroPassaporte}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
	
		<label class="rotulo" id="rotuloDataNascimento" for="dataNascimento">Data nascimento:</label>
		<input class="campoData" type="text" id="dataNascimento" name="dataNascimento" disabled="disabled" value="${chamadoAlteracaoCliente.dataNascimentoString}" maxlength="10"/><br />
		<input class="abaIdentificacaoCheckboxPessoaFisica_h" type="checkbox" id="dataNascimentoCheckboxIdentificacao" name="dataNascimentoCheckboxIdentificacao" value="${chamadoAlteracaoCliente.dataNascimentoString}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloProfissao" for="profissao">Profiss�o:</label>
		<select name="profissao" id="profissao"  disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${profissoes}" var="profissao">
				<option value="<c:out value="${profissao.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.profissao.chavePrimaria == profissao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${profissao.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<input class="abaIdentificacaoCheckboxPessoaFisica_i" type="checkbox" id="profissaoCheckboxIdentificacao" name="profissaoCheckboxIdentificacao" value="${chamadoAlteracaoCliente.profissao.chavePrimaria}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />		
		
		<label class="rotulo campoObrigatorioRadio" id="rotuloSexo" for="sexo"><span class="campoObrigatorioSimbolo">* </span>Sexo:</label>		
		<c:forEach items="${sexos}" var="sexo">
			<input class="campoRadio" type="radio" value="${sexo.chavePrimaria}" name="sexo" id="sexo" disabled="disabled" <c:if test="${chamadoAlteracaoCliente.sexo.chavePrimaria == sexo.chavePrimaria}">checked="checked"</c:if>>
			<label class="rotuloRadio">${sexo.descricao}</label>
		</c:forEach>
		<input class="abaIdentificacaoCheckboxPessoaFisica_k" type="checkbox" id="sexoCheckboxIdentificacao" name="sexoCheckboxIdentificacao" value="${chamadoAlteracaoCliente.sexo.chavePrimaria}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloNomeMae" for="nomeMae">Nome da m�e:</label>
		<input class="campoTexto" type="text" id="nomeMae" name="nomeMae" maxlength="50" size="20" disabled="disabled" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${chamadoAlteracaoCliente.nomeMae}"><br />
		<input class="abaIdentificacaoCheckboxPessoaFisica_j" type="checkbox" id="nomeMaeCheckboxIdentificacao" name="nomeMaeCheckboxIdentificacao" value="${chamadoAlteracaoCliente.nomeMae}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloNomePai" for="nomePai">Nome do pai:</label>
		<input class="campoTexto" type="text" id="nomePai" name="nomePai" maxlength="50" size="20" disabled="disabled"  onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${chamadoAlteracaoCliente.nomePai}"><br />
		<input class="abaIdentificacaoCheckboxPessoaFisica_l" type="checkbox" id="nomePaiCheckboxIdentificacao" name="nomePaiCheckboxIdentificacao" value="${chamadoAlteracaoCliente.nomePai}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		
		<label class="rotulo" id="rotuloRendaFamiliar" for="idFaixaRendaFamiliar">Renda familiar:</label>
		<select name="idFaixaRendaFamiliar" id="idFaixaRendaFamiliar" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${faixasRenda}" var="renda">
				<option value="<c:out value="${renda.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.rendaFamiliar.chavePrimaria == renda.chavePrimaria}">selected="selected"</c:if>>
					<c:if test="${renda.valorFaixaInicio ne renda.valorFaixaFim}">
						<fmt:formatNumber value="${renda.valorFaixaInicio}" minFractionDigits="2" type="currency"/> - <fmt:formatNumber value="${renda.valorFaixaFim}" minFractionDigits="2" type="currency"/>
					</c:if>
					<c:if test="${renda.valorFaixaInicio eq renda.valorFaixaFim}">
						Acima de <fmt:formatNumber value="${renda.valorFaixaInicio}" minFractionDigits="2" type="currency"/>
					</c:if>
				</option>		
			</c:forEach>
		</select>
		<input class="abaIdentificacaoCheckboxPessoaFisica_m" type="checkbox" id="rendaFamiliarCheckboxIdentificacao" name="rendaFamiliarCheckboxIdentificacao" value="${chamadoAlteracaoCliente.rendaFamiliar.chavePrimaria}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
		<br/><br/>
		
		<fieldset id="clienteEnderecoCol1" class="colunaEsq">
		<c:set var="x" value="0" />
		<display:table class="dataTableGGAS dataTableAba" name="listaChamadoAlteracaoClienteAnexoAbaIdentificacao" sort="list" id="anexoAbaIdentificacao" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			 <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	       		<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<input type="checkbox" name="chavesPrimarias" value="${anexoAbaIdentificacao.chavePrimaria}" onclick="preencherMapaListaAnexoAlteracaoCadastroClienteChamado(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>, <c:out value='${anexoAbaIdentificacao.clienteAnexo.chavePrimaria}'/>, this.checked);">
                   </c:when>
                   <c:otherwise>
                       <input type="checkbox" name="chavesPrimarias" disabled="disabled" value="${anexoAbaIdentificacao.chavePrimaria}" onclick="preencherMapaListaAnexoAlteracaoCadastroClienteChamado(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>, <c:out value='${anexoAbaIdentificacao.clienteAnexo.chavePrimaria}'/>, this.checked);">
                   </c:otherwise>
               </c:choose>
	        </display:column>
			
			<display:column sortable="false" title="Descri��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaIdentificacao.descricaoAnexo}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaIdentificacao.descricaoAnexo}
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="Tipo" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaIdentificacao.tipoAnexo.descricao}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaIdentificacao.tipoAnexo.descricao}
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="Data �ltima Altera��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusDate" />
							<c:out value="${formattedStatusDate}"/>
							
							<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusHour" />
							<c:out value="${formattedStatusHour}"/>
						</a>
                   </c:when>
                   <c:otherwise>
                      	 <fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusDate" />
							<c:out value="${formattedStatusDate}"/>
							
							<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusHour" />
							<c:out value="${formattedStatusHour}"/>
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="A��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaIdentificacao.tipoAcao.descricao}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaIdentificacao.tipoAcao.descricao}
                   </c:otherwise>
               </c:choose>
				
			</display:column>
			
			<c:set var="x" value="${x+1}" />
		</display:table>
</fieldset>		
</fieldset>	
</fieldset>

<fieldset id="pessoaJuridicaChamadoAlteracaoCliente"  class="colunaEsq" style="display: none;">
	<fieldset id="clienteEnderecoCol1" class="colunaEsq">
	<label class="rotulo campoObrigatorio" id="rotuloNomeFantasia" for="nomeFantasia"><span class="campoObrigatorioSimbolo">* </span>Nome fantasia:</label>
	<input class="campoTexto" type="text" id="nomeFantasia" name="nomeFantasia" disabled="disabled"  maxlength="50" size="30" value="${chamadoAlteracaoCliente.nomeFantasia}">
	<input class="solicitacaoAlteracaoNomeFantasiaCheckbox" type="checkbox" id="nomeFantasiaCheckboxIdentificacao" name="nomeFantasiaCheckboxIdentificacao" value="${chamadoAlteracaoCliente.nomeFantasia}" onclick="preencherMapa(this.id, this.checked, this.value);"><br /> 
	
	<label class="rotulo campoObrigatorio" id="rotuloCNPJ" for="cnpj"><span class="campoObrigatorioSimbolo">* </span>CNPJ:</label>
	<input class="campoTexto" type="text" id="cnpj" name="cnpj" maxlength="18" disabled="disabled" size="18" value="${chamadoAlteracaoCliente.cnpj}">
	<input class="solicitacaoAlteracaoCnpjCheckbox" type="checkbox" id="cnpjCheckboxIdentificacao" name="cnpjCheckboxIdentificacao" value="${chamadoAlteracaoCliente.cnpj}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
	
	<label class="rotulo rotuloInscricaoEstadual" id="rotuloInscricaoEstadual" for="inscricaoEstadual"><span class="campoObrigatorioSimbolo" id="obrigatorioInscricao">* </span>Inscri��o estadual:</label>
	<input class="campoTexto" type="text" id="inscricaoEstadual" name="inscricaoEstadual" disabled="disabled" maxlength="14" size="25" value="${chamadoAlteracaoCliente.inscricaoEstadual}">
	<input class="solicitacaoAlteracaoInscricaoEstadualCheckbox" type="checkbox" id="inscricaoEstadualCheckboxIdentificacao" name="inscricaoEstadualCheckboxIdentificacao" value="${chamadoAlteracaoCliente.inscricaoEstadual}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
	
	<label class="rotulo rotuloHorizontal" id="rotuloRegimeRecolhimento" for="regimeRecolhimento">Isento de inscri��o estadual:</label>
	<input class="campoCheckbox" type="checkbox"  disabled="disabled" onchange="checarInscricaoEstadualAbilitada()" id="regimeRecolhimento" name="regimeRecolhimento" value="1" <c:if test="${chamadoAlteracaoCliente.regimeRecolhimento eq '1'}">checked="checked"</c:if>/>
 	<label class="rotulo rotuloHorizontal naoPossui1" id="naoPossui" for="naoPossuiIndicador">N�o possui:</label> 
 	<input class="campoCheckbox campoHorizontal regimeRecolhimento" type="checkbox"  disabled="disabled" onchange="checarInscricaoEstadualAbilitada()" id="naoPossuiIndicador" name="regimeRecolhimento" value="2" <c:if test="${chamadoAlteracaoCliente.regimeRecolhimento eq '2'}">checked="checked"</c:if>/>
 	<input class="solicitacaoAlteracaoRegimeRecolhimentoCheckbox" type="checkbox" id="regimeRecolhimentoCheckboxIdentificacao" name="regimeRecolhimentoCheckboxIdentificacao" value="${chamadoAlteracaoCliente.regimeRecolhimento}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
   
	<label class="rotulo" id="rotuloInscricaoMunicipal" for="inscricaoMunicipal">Inscri��o municipal:</label>
	<input class="campoTexto" type="text" id="inscricaoMunicipal" name="inscricaoMunicipal" disabled="disabled" maxlength="14" size="25" value="${chamadoAlteracaoCliente.inscricaoMunicipal}" onkeypress="return formatarCampoInteiro(event)"><br />
	<input class="solicitacaoAlteracaoInscricaoMunicipalCheckbox" type="checkbox" id="inscricaoMunicipalCheckboxIdentificacao" name="inscricaoMunicipalCheckboxIdentificacao" value="${chamadoAlteracaoCliente.inscricaoMunicipal}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
	
	<label class="rotulo" id="rotuloInscricaoRural" for="inscricaoRural">Inscri��o rural:</label>
	<input class="campoTexto" type="text" id="inscricaoRural" name="inscricaoRural" disabled="disabled" maxlength="14" size="25" value="${chamadoAlteracaoCliente.inscricaoRural}" onkeypress="return formatarCampoInteiro(event)">
	<input class="solicitacaoAlteracaoInscricaoRuralCheckbox" type="checkbox" id="inscricaoRuralCheckboxIdentificacao" name="inscricaoRuralCheckboxIdentificacao" value="${chamadoAlteracaoCliente.inscricaoRural}" onclick="preencherMapa(this.id, this.checked, this.value);"><br />
	
	<fieldset id="clienteEnderecoCol1" class="colunaEsq">
	<c:set var="x" value="0" />
		<display:table class="dataTableGGAS dataTableAba" name="listaChamadoAlteracaoClienteAnexoAbaIdentificacao" sort="list" id="anexoAbaIdentificacao" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			 <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	       		<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<input type="checkbox" name="chavesPrimarias" value="${anexoAbaIdentificacao.clienteAnexo.chavePrimaria}" onclick="preencherMapaListaAnexoAlteracaoCadastroClienteChamado(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>, <c:out value='${anexoAbaIdentificacao.clienteAnexo.chavePrimaria}'/>, this.checked);">
                   </c:when>
                   <c:otherwise>
                       <input type="checkbox" name="chavesPrimarias" disabled="disabled" value="${anexoAbaIdentificacao.clienteAnexo.chavePrimaria}" onclick="preencherMapaListaAnexoAlteracaoCadastroClienteChamado(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>, <c:out value='${anexoAbaIdentificacao.clienteAnexo.chavePrimaria}'/>, this.checked);">
                   </c:otherwise>
               </c:choose>
	        </display:column>
			
			<display:column sortable="false" title="Descri��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaIdentificacao.descricaoAnexo}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaIdentificacao.descricaoAnexo}
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="Tipo" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaIdentificacao.tipoAnexo.descricao}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaIdentificacao.tipoAnexo.descricao}
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="Data �ltima Altera��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusDate" />
							<c:out value="${formattedStatusDate}"/>
							
							<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusHour" />
							<c:out value="${formattedStatusHour}"/>
						</a>
                   </c:when>
                   <c:otherwise>
                      	 <fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusDate" />
							<c:out value="${formattedStatusDate}"/>
							
							<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacao.ultimaAlteracao}" var="formattedStatusHour" />
							<c:out value="${formattedStatusHour}"/>
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="A��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaIdentificacao.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaIdentificacao(<c:out value='${anexoAbaIdentificacao.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaIdentificacao.tipoAcao.descricao}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaIdentificacao.tipoAcao.descricao}
                   </c:otherwise>
               </c:choose>
				
			</display:column>
			
			<c:set var="x" value="${x+1}" />
		</display:table>
</fieldset>	
</fieldset>
</fieldset>

<fieldset >
<fieldset id="pessoaFisica" class="colunaDirChamadoAlteracaoCliente">
<fieldset id="clienteEnderecoCol1" class="colunaDir">

	<input type="hidden" name="indexLista" id="indexLista">
	<input type="hidden" name="chavePrimariaClienteEndereco" id="chavePrimariaClienteEndereco">
	
	<label class="rotulo campoObrigatorio" id="labelNascionalidade" for="nacionalidade"><span class="campoObrigatorioSimbolo">* </span>Nacionalidade:</label>
		<select name="nacionalidade" id="nacionalidade" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaNacionalidades}" var="nacionalidade">
				<option value="<c:out value="${nacionalidade.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.cliente.nacionalidade.chavePrimaria == nacionalidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${nacionalidade.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
	
		<label class="rotulo" id="rotuloCPF" for="cpf"><span class="campoObrigatorioSimbolo" id="campoObrigatorioSimboloCpf" style="display: none;">* </span>CPF:</label>
		<input class="campoTexto" type="text" id="cpfCliente" disabled="disabled" name="cpfCliente"  maxlength="14" size="14" value="${chamadoAlteracaoCliente.cliente.cpf}"><br />
		
		<label class="rotulo" id="rotuloRG" for="rg">RG:</label>
		<input class="campoTexto" type="text" id="rg" name="rg" disabled="disabled" maxlength="20" size="10" value="${chamadoAlteracaoCliente.cliente.rg}"><br />
		
		<label class="rotulo" id="rotuloDataEmissaoRG" for="dataEmissaoRG" >Data emiss�o:</label>
		<input class="campoData" type="text" id="dataEmissaoRG" disabled="disabled" name="dataEmissaoRG"  value="${chamadoAlteracaoCliente.dataEmissaoRGClienteString}" maxlength="10"><br />
		
		<label class="rotulo" id="rotuloOrgaoEmissorRG" for="orgaoExpedidor">�rg�o emissor:</label>
		<select name="orgaoExpedidor" id="orgaoExpedidor" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${orgaosEmissores}" var="orgaoEmissor">
				<option value="<c:out value="${orgaoEmissor.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.cliente.orgaoExpedidor.chavePrimaria == orgaoEmissor.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${orgaoEmissor.descricaoAbreviada}"/>
				</option>		
			</c:forEach>
		</select><br />
		<label class="rotulo" id="rotuloOrgaoEmissorUF" for="unidadeFederacao">UF:</label>
		<select name="unidadeFederacao" id="unidadeFederacao" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${unidadesFederacao}" var="unidadeFederacao">
				<option value="<c:out value="${unidadeFederacao.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.cliente.unidadeFederacao.chavePrimaria == unidadeFederacao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidadeFederacao.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<label id="rotuloPassaporte" class="rotulo" for="numeroPassaporte"><span class="campoObrigatorioSimbolo" id="campoObrigatorioSimboloPassaporte" style="display: none;">* </span>Passaporte:</label>
		<input class="campoTexto" type="text" id="numeroPassaporte" disabled="disabled" name="numeroPassaporte" maxlength="13" size="13" value="${chamadoAlteracaoCliente.cliente.numeroPassaporte}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
	
		<label class="rotulo" id="rotuloDataNascimento" for="dataNascimento">Data nascimento:</label>
		<input class="campoData" type="text" id="dataNascimento" disabled="disabled" name="dataNascimento" value="${chamadoAlteracaoCliente.dataNascimentoClienteString}" maxlength="10"/><br />
		
		<label class="rotulo" id="rotuloProfissao" for="profissao">Profiss�o:</label>
		<select name="profissao" id="profissao" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${profissoes}" var="profissao">
				<option value="<c:out value="${profissao.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.cliente.profissao.chavePrimaria == profissao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${profissao.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<label class="rotulo campoObrigatorioRadio" id="rotuloSexo" for="idSexo"><span class="campoObrigatorioSimbolo">* </span>Sexo:</label>		
		<c:forEach items="${sexos}" var="sexo">
			<input class="campoRadio" type="radio" disabled="disabled" value="${sexo.chavePrimaria}" name="idSexo" id="idSexo" <c:if test="${chamadoAlteracaoCliente.cliente.sexo.chavePrimaria == sexo.chavePrimaria}">checked="checked"</c:if>><label class="rotuloRadio">${sexo.descricao}</label>
		</c:forEach>
		<br /><br />
		<label class="rotulo" id="rotuloNomeMae" for="nomeMae">Nome da m�e:</label>
		<input class="campoTexto" type="text" id="nomeMae" name="nomeMae" disabled="disabled" maxlength="50" size="20" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${chamadoAlteracaoCliente.cliente.nomeMae}"><br />
		<label class="rotulo" id="rotuloNomePai" for="nomePai">Nome do pai:</label>
		<input class="campoTexto" type="text" id="nomePai" name="nomePai" disabled="disabled" maxlength="50" size="20"  onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${chamadoAlteracaoCliente.cliente.nomePai}"><br />
		<label class="rotulo" id="rotuloRendaFamiliar" for="idFaixaRendaFamiliar">Renda familiar:</label>
		<select name="idFaixaRendaFamiliar" id="idFaixaRendaFamiliar" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${faixasRenda}" var="renda">
				<option value="<c:out value="${renda.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.cliente.rendaFamiliar.chavePrimaria == renda.chavePrimaria}">selected="selected"</c:if>>
					<c:if test="${renda.valorFaixaInicio ne renda.valorFaixaFim}">
						<fmt:formatNumber value="${renda.valorFaixaInicio}" minFractionDigits="2" type="currency"/> - <fmt:formatNumber value="${renda.valorFaixaFim}" minFractionDigits="2" type="currency"/>
					</c:if>
					<c:if test="${renda.valorFaixaInicio eq renda.valorFaixaFim}">
						Acima de <fmt:formatNumber value="${renda.valorFaixaInicio}" minFractionDigits="2" type="currency"/>
					</c:if>
				</option>		
			</c:forEach>
		</select>
		
		<fieldset id="clienteEnderecoCol1" class="colunaDir">
		<c:set var="y" value="0" />
		<display:table class="dataTableGGAS dataTableAba" name="listaClienteAnexoAbaIdentificacao" sort="list" id="anexoAbaIdentificacaoCliente" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			<display:column sortable="false" title="Descri��o" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaIdentificacaoCliente(<c:out value='${anexoAbaIdentificacaoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					${anexoAbaIdentificacaoCliente.descricaoAnexo}
				</a>
			</display:column>
			
			<display:column sortable="false" title="Tipo" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaIdentificacaoCliente(<c:out value='${anexoAbaIdentificacaoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					${anexoAbaIdentificacaoCliente.tipoAnexo.descricao}
				</a>
			</display:column>
			
			<display:column sortable="false" title="Data �ltima Altera��o" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaIdentificacaoCliente(<c:out value='${anexoAbaIdentificacaoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacaoCliente.ultimaAlteracao}" var="formattedStatusDate" />
					<c:out value="${formattedStatusDate}"/>
					
					<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacaoCliente.ultimaAlteracao}" var="formattedStatusHour" />
					<c:out value="${formattedStatusHour}"/>
				</a>
			</display:column>
			
			<c:set var="y" value="${y+1}" />
		</display:table>
		</fieldset>	
</fieldset>		
</fieldset>
</fieldset>

<fieldset id="pessoaJuridica" class="colunaDirChamadoAlteracaoCliente" style="display: none;">
	<fieldset id="clienteEnderecoCol2" class="colunaDir">
	<label class="rotulo campoObrigatorio" id="rotuloNomeFantasia" for="nomeFantasia"><span class="campoObrigatorioSimbolo">* </span>Nome fantasia:</label>
	<input class="campoTexto" type="text" id="nomeFantasia" name="nomeFantasia" disabled="disabled" maxlength="50" size="30" value="${chamadoAlteracaoCliente.cliente.nomeFantasia}"><br />
	
	<label class="rotulo campoObrigatorio" id="rotuloCNPJ" for="cnpj"><span class="campoObrigatorioSimbolo">* </span>CNPJ:</label>
	<input class="campoTexto" type="text" id="cnpjCliente" name="cnpjCliente" disabled="disabled" maxlength="18" size="18" value="${chamadoAlteracaoCliente.cliente.cnpj}"><br />
	
	<label class="rotulo" id="rotuloInscricaoEstadual" for="inscricaoEstadual"><span class="campoObrigatorioSimbolo" id="obrigatorioInscricao">* </span>Inscri��o estadual:</label>
	<input class="campoTexto" type="text" id="inscricaoEstadual" name="inscricaoEstadual" disabled="disabled" maxlength="14" size="25" value="${chamadoAlteracaoCliente.cliente.inscricaoEstadual}"><br />
	
	<label class="rotulo rotuloHorizontal" id="rotuloRegimeRecolhimento2" for="regimeRecolhimento">Isento de inscri��o estadual:</label>
	<input class="campoCheckbox campoHorizontal regimeRecolhimento1" type="checkbox" disabled="disabled"  id="regimeRecolhimento" name="regimeRecolhimento" value="1" <c:if test="${chamadoAlteracaoCliente.cliente.regimeRecolhimento eq '1'}">checked="checked"</c:if>/>
 	<label class="rotulo rotuloHorizontal" id="naoPossui" for="naoPossuiIndicador">N�o possui:</label> 
 	<input class="campoCheckbox campoHorizontal" type="checkbox"  disabled="disabled"  id="naoPossuiIndicador" name="regimeRecolhimento" value="2" <c:if test="${chamadoAlteracaoCliente.cliente.regimeRecolhimento eq '2'}">checked="checked"</c:if>/>
	
	<label class="rotulo" id="rotuloInscricaoMunicipal" for="inscricaoMunicipal">Inscri��o municipal:</label>
	<input class="campoTexto" type="text" id="inscricaoMunicipal" disabled="disabled" name="inscricaoMunicipal" maxlength="14" size="25" value="${chamadoAlteracaoCliente.cliente.inscricaoMunicipal}" onkeypress="return formatarCampoInteiro(event)"><br />
	<label class="rotulo" id="rotuloInscricaoRural" for="inscricaoRural">Inscri��o rural:</label>
	<input class="campoTexto" type="text" id="inscricaoRural" disabled="disabled" name="inscricaoRural" maxlength="14" size="25" value="${chamadoAlteracaoCliente.cliente.inscricaoRural}" onkeypress="return formatarCampoInteiro(event)">
	
	<fieldset id="clienteEnderecoCol1" class="colunaDir">
	<c:set var="y" value="0" />
		<display:table class="dataTableGGAS dataTableAba" name="listaClienteAnexoAbaIdentificacao" sort="list" id="anexoAbaIdentificacaoCliente" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			<display:column sortable="false" title="Descri��o" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaIdentificacaoCliente(<c:out value='${anexoAbaIdentificacaoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					${anexoAbaIdentificacaoCliente.descricaoAnexo}
				</a>
			</display:column>
			
			<display:column sortable="false" title="Tipo" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaIdentificacaoCliente(<c:out value='${anexoAbaIdentificacaoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					${anexoAbaIdentificacaoCliente.tipoAnexo.descricao}
				</a>
			</display:column>
			
			<display:column sortable="false" title="Data �ltima Altera��o" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaIdentificacaoCliente(<c:out value='${anexoAbaIdentificacaoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaIdentificacaoCliente.ultimaAlteracao}" var="formattedStatusDate" />
					<c:out value="${formattedStatusDate}"/>
					
					<fmt:formatDate pattern="HH:mm" value="${anexoAbaIdentificacaoCliente.ultimaAlteracao}" var="formattedStatusHour" />
					<c:out value="${formattedStatusHour}"/>
				</a>
			</display:column>
			
			<c:set var="y" value="${y+1}" />
		</display:table>
	
</fieldset>
</fieldset>
</fieldset>

	
</fieldset>



</div>
