<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<c:set var="i" value="0" />
<legend>Hist�rico do Ponto Consumo</legend>
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas" decorator="br.com.ggas.web.cadastro.imovel.decorator.ImovelResultadoPesquisaDecorator" name="pontosConsumo" id="pontoConsumo"  sort="list" pagesize="5"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	    	
	    		<display:column sortable="false" title="Cliente">
					<div style="width: 170px; word-wrap: break-word; text-align: left; padding-left: 5px; padding-right: 5px; overflow: hidden">
					<c:out value="${cliente.nome}"/>
					</div>
				</display:column>
				<display:column sortable="false" title="Descri��o">
					<div style="width: 170px; word-wrap: break-word; text-align: left; padding-left: 5px; padding-right: 5px">
					<c:out value="${pontoConsumo.chavePrimaria}"/><c:out value=" - "/><c:out value="${pontoConsumo.descricao}"/>
					</div>
				</display:column>
				<display:column property="segmento.descricao" sortable="false" title="Segmento" style="width: 100px" />
				<display:column property="ramoAtividade.descricao" sortable="false" title="Ramo de<br />Atividade"  style="width: 100px" />
				<display:column property="cep.cep" sortable="false" title="Cep" style="width: 75px" />
				<display:column property="numeroImovel" sortable="false" title="N�mero" style="width: 50px" />
				<display:column sortable="false" title="Rota">
					<div style="width: 115px; word-wrap: break-word; text-align: center; padding-left: 5px; padding-right: 5px; overflow: hidden"><c:out value="${pontoConsumo.rota.numeroRota}"></c:out></div>
				</display:column>
				<display:column sortable="false" title="Endere�o Remoto">
					<div style="width: 115px; word-wrap: break-word; text-align: center; padding-left: 5px; padding-right: 5px; overflow: hidden"><c:out value="${pontoConsumo.codigoPontoConsumoSupervisorio}"></c:out></div>
				</display:column>
				<display:column property="situacaoConsumo.descricao" sortable="false" title="Situa��o Consumo" />			
				<display:column sortable="false" title="Indicador de uso">
						<c:if test="${pontoConsumo.habilitado eq 'true'}">ATIVO</c:if>
						<c:if test="${pontoConsumo.habilitado eq 'false'}">INATIVO</c:if>
				</display:column>		
				<c:set var="i" value="${i+1}" />				
		   	</display:table>
