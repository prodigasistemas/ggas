<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<script type="text/javascript">

function exibirAlteracaoDoFone(indice, idTipoFone, codigoDDD, numero, ramal,chavePrimariaFone, indexListaCliente, idTipoFoneCliente, codigoDDDCliente, numeroCliente, ramalCliente,chavePrimariaFoneCliente,tipoAcao, codigoInclusao, codigoAlteracao){

	if (indice != "") {
		document.forms[0].indexLista.value = indice;
		if(idTipoFone != ""){
			var tamanho = document.forms[0].tipoFoneCf.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].tipoFoneCf[i];
				if (idTipoFone == opcao.value) {
					opcao.selected = true;
				}
			}
		}
	}
		if(codigoDDD != ""){
			document.forms[0].codigoDDDCf.value = codigoDDD;
		}
		if(numero != ""){
			document.forms[0].numeroCf.value = numero;
		}
		if(ramal != ""){
			document.forms[0].ramalCf.value = ramal;
		}
		
		exibirFoneOriginal(indexListaCliente, idTipoFoneCliente, codigoDDDCliente, numeroCliente, ramalCliente,chavePrimariaFoneCliente);
		preencherCheckMapaFone(chavePrimariaFoneCliente);
		
		if(tipoAcao != "" && tipoAcao == codigoInclusao){
			
			document.forms[0].tipoFoneCfCheckboxTelefone.checked = true;
			document.forms[0].tipoFoneCfCheckboxTelefone.disabled = true;
			
			document.forms[0].codigoDDDCfCheckboxTelefone.checked = true;
			document.forms[0].codigoDDDCfCheckboxTelefone.disabled = true;
			
			document.forms[0].numeroCfCheckboxTelefone.checked = true;
			document.forms[0].numeroCfCheckboxTelefone.disabled = true;
			
		}else if(tipoAcao != "" && tipoAcao != codigoAlteracao){
			
			document.forms[0].tipoFoneCheckboxTelefone.checked = false;
			document.forms[0].tipoFoneCheckboxTelefone.disabled = false;
			
			document.forms[0].codigoDDDCheckboxTelefone.checked = false;
			document.forms[0].codigoDDDCheckboxTelefone.disabled = false;
			
			document.forms[0].numeroCheckboxTelefone.checked = false;
			document.forms[0].numeroCheckboxTelefone.disabled = false;
		}
		
		document.forms[0].chavePrimariaFoneCliente.value = chavePrimariaFoneCliente;
	
}

function exibirFoneOriginal(indexListaCliente, idTipoFoneCliente, codigoDDDCliente, numeroCliente, ramalCliente,chavePrimariaFoneCliente){
	
	if(idTipoFoneCliente != ""){
		document.forms[0].indexListaCliente.value = indexListaCliente;
		var tamanho = document.forms[0].tipoTelefoneCliente.length;
		var opcao = undefined;
		for(var i = 0; i < tamanho; i++) {
			opcao = document.forms[0].tipoTelefoneCliente[i];
			if (idTipoFoneCliente == opcao.value) {
				opcao.selected = true;
			}
		}
	}else{
		document.forms[0].tipoTelefoneCliente.value = -1;
	}
	if(codigoDDDCliente != ""){
		document.forms[0].codigoDDDClienteOriginal.value = codigoDDDCliente;
	}else{
		document.forms[0].codigoDDDClienteOriginal.value = null;
	}
	if(numeroCliente != ""){
		document.forms[0].numeroClienteOriginal.value = numeroCliente;
	}else{
		document.forms[0].numeroClienteOriginal.value = null;
	}
	if(ramalCliente != ""){
		document.forms[0].ramalClienteOriginal.value = ramalCliente;
	}else{
		document.forms[0].ramalClienteOriginal.value = null;
	}
}


function preencherCheckMapaFone(chavePrimariaFoneCliente){
	
	$("input:checkbox").attr("checked",false);
	$("input:checkbox").attr("disabled",false);
	
	var url = "retornarCheckMapa?tipoMapa=Fones" + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		}).responseText;
	
	retorno = retorno.substring(1,retorno.length-1).split("],");
	
	for(j = 0 ; j < retorno.length ; j++){
		var chaveAux = retorno[j].split(":");
		var checkBoxMarcadosListaMapa = new Object();
		
		for(i = 0 ; i < chaveAux.length/2 ;){
			
			chaveAux[i] = chaveAux[i].substring(1,chaveAux[i].length-1);
			
			checkBoxMarcadosListaMapa[chaveAux[i]] = chaveAux[++i];
			++i;
		}
		
	      if(checkBoxMarcadosListaMapa != null && checkBoxMarcadosListaMapa != ""){
	    	  
	     		for (var chaveMapa in checkBoxMarcadosListaMapa){
	     			
	     			if(chaveMapa == chavePrimariaFoneCliente){
	     				
	     				var listaCheckBoxMarcados = checkBoxMarcadosListaMapa[chaveMapa].split(",");
	     				
	     				for(i = 0 ; i < listaCheckBoxMarcados.length ; i++){
	     					
	     					var valorchaveMapaAux = listaCheckBoxMarcados[i].split("[");
	     					var valorchaveMapaAlterado;
	     					
	     					 if(valorchaveMapaAux.length == 2){
	     						valorchaveMapaAlterado = valorchaveMapaAux[1].split("]");
	     					 }else{
	     						valorchaveMapaAlterado = valorchaveMapaAux[0].split("]");
	     					 }
	     					
	     					
	        	           var valorchaveMapa = valorchaveMapaAlterado[0].split(",");

	        	               for(j = 0 ; j < valorchaveMapa.length ; j++){
	        	            	   
	        	            	   var valor = valorchaveMapa[j].replace(" ","");
	        	            	   valor = valor.replace("\"","").replace("\"","").concat("CfCheckboxTelefone"); 
	        	            	   
	        	            	   document.getElementById(valor).checked = true;
	        	               }
	     	     		}
	     			 }
	     			
	         	}
	          
	      }
	}
	
	
}


function confimarAlteracaoListaTelefones(){
	
	var chavePrimariaFoneCliente = document.forms[0].chavePrimariaFoneCliente.value;
	
	var lista = $("input:checkbox[name$='CheckboxTelefone']");
	for(var i = 0; i < lista.length; i++) {
				
		var idCampo = lista[i].id;
		var idCampoFormatado = idCampo.split("CheckboxTelefone");
		
		preencherMapaListaAlteracaoCadastroClienteTelefone(lista[i].id, lista[i].checked, document.getElementById(idCampoFormatado[0]).value, chavePrimariaFoneCliente);
	}
	
	alert("Altera��o realizada com sucesso.");
	
}

function preencherMapaListaAlteracaoCadastroClienteTelefone(idCampo, valorChecked, valor, chavePrimariaClienteFone){
	
	
	
	var url = "preencherMapaListaAlteracaoCadastroClienteChamado?idCampo=" + idCampo + "&valorChecked="
	+ valorChecked + "&valor=" + valor + "&chavePrimariaClienteEndereco=" + chavePrimariaClienteFone + "&tipoMapa=Fones" +"&posFixoTipoCheckbox=CfCheckboxTelefone"+ "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		}).responseText;
	

}

</script>


<div id="telefoneAbaEndereco">
	<input type="hidden" name="indexLista" id="indexLista">
	<input type="hidden" name="chavePrimariaFoneCliente" id="chavePrimariaFoneCliente">
<fieldset class="conteinerDados3">

<fieldset class="colunaEsq" id="clienteEnderecoCol1">

<label id="rotuloTipoTelefone" class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Tipo de Telefone:</label>
<select name="tipoFone" id="tipoFoneCf" disabled="disabled" class="campoSelect">
	<option value="-1">Selecione</option>
	<c:forEach items="${listaTipoFone}" var="tipoTelefone">
		<option value="<c:out value="${tipoTelefone.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.tipoTelefone.chavePrimaria == tipoTelefone.chavePrimaria}">selected="selected"</c:if>>
			<c:out value="${tipoTelefone.descricao}"/>
		</option>		
	</c:forEach>
</select>
<input class="abaTelefoneCheckboxPessoaFisica_a" type="checkbox" id="tipoFoneCfCheckboxTelefone" name="tipoFoneCfCheckboxTelefone"><br /><br />
		
<label id="rotuloNumeroImovel" class="rotulo campoObrigatorio" ><span class="campoObrigatorioSimbolo">* </span>DDD: </label>
<input class="campoTexto" type="text" id="codigoDDDCf" name="codigoDDDCf" disabled="disabled" maxlength="5" size="3">
<input class="abaTelefoneCheckboxPessoaFisica_b" type="checkbox" id="codigoDDDCfCheckboxTelefone" name="codigoDDDCfCheckboxTelefone"><br /><br />

<label id="rotulo" class="rotulo campoObrigatorio" ><span class="campoObrigatorioSimbolo">* </span>N�mero: </label>
<input class="campoTexto" type="text" id="numeroCf" name="numeroCf" disabled="disabled"  size="11">
<input class="abaTelefoneCheckboxPessoaFisica_c" type="checkbox" id="numeroCfCheckboxTelefone" name="numeroCfCheckboxTelefone"><br /><br />

<label id="rotulo" class="rotulo">Ramal: </label>
<input class="campoTexto" type="text" id="ramal" name="ramalCf" disabled="disabled" maxlength="5" size="3">
<input class="abaTelefoneCheckboxPessoaFisica_d" type="checkbox" id="ramalCfCheckboxTelefone" name="ramalCfCheckboxTelefone">
<br />
		<input class="bottonRightCol2 botaoGrande1 botaoIncluir" name="botaoConfirmar" id="botaoConfirmar" value="Confirmar" type="button" onclick="confimarAlteracaoListaTelefones();">
	<br/><br/>
	
	
	<c:set var="j" value="0" />
	<c:set var="i" value="0" />
	
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaChamadoAlteracaoClienteFone" id="telefone" sort="list" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		
		<display:column  sortable="false" title="Tipo" style="width: 110px">
		<c:choose>
        	<c:when test="${(sessionScope.codigoInclusao eq telefone.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq telefone.tipoAcao.chavePrimaria)}">
             	<a href="javascript:exibirAlteracaoDoFone('${i}','${telefone.tipoFone.chavePrimaria}','${telefone.codigoDDD}','${telefone.numero}','${telefone.ramal}','${telefone.chavePrimaria }','${j}','${telefone.clienteFone.tipoFone.chavePrimaria}','${telefone.clienteFone.codigoDDD}','${telefone.clienteFone.numero}','${telefone.clienteFone.ramal}','${telefone.clienteFone.chavePrimaria}','${telefone.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
 					<c:out value="${telefone.tipoFone.descricao}"/>
 				</a> 
             </c:when>
             <c:otherwise>
             	<c:out value="${telefone.clienteFone.tipoFone.descricao}"/> 
             </c:otherwise>	
        </c:choose>
        </display:column>
        
        <display:column  sortable="false" title="DDD" style="width: 80px">
		<c:choose>
        	<c:when test="${(sessionScope.codigoInclusao eq telefone.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq telefone.tipoAcao.chavePrimaria)}">
				<a href="javascript:exibirAlteracaoDoFone('${i}','${telefone.tipoFone.chavePrimaria}','${telefone.codigoDDD}','${telefone.numero}','${telefone.ramal}','${telefone.chavePrimaria }','${j}','${telefone.clienteFone.tipoFone.chavePrimaria}','${telefone.clienteFone.codigoDDD}','${telefone.clienteFone.numero}','${telefone.clienteFone.ramal}','${telefone.clienteFone.chavePrimaria}','${telefone.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
					<c:out value="${telefone.codigoDDD}"/>
				</a>
             </c:when>
             <c:otherwise>
             	<c:out value="${telefone.codigoDDD}"/>
             </c:otherwise>
        </c:choose>
        </display:column>
        
        		<display:column  sortable="false" title="N�mero" style="width: 50px">
		<c:choose>
        	<c:when test="${(sessionScope.codigoInclusao eq telefone.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq telefone.tipoAcao.chavePrimaria)}">
				<a href="javascript:exibirAlteracaoDoFone('${i}','${telefone.tipoFone.chavePrimaria}','${telefone.codigoDDD}','${telefone.numero}','${telefone.ramal}','${telefone.chavePrimaria }','${j}','${telefone.clienteFone.tipoFone.chavePrimaria}','${telefone.clienteFone.codigoDDD}','${telefone.clienteFone.numero}','${telefone.clienteFone.ramal}','${telefone.clienteFone.chavePrimaria}','${telefone.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
					<c:out value="${telefone.numero}"/>
				</a>
             </c:when>
             <c:otherwise>
             	<c:out value="${telefone.numero}"/>
             </c:otherwise>
        </c:choose>
        </display:column>
        
        		<display:column  sortable="false" title="Principal" style="width: 40px">
		<c:choose>
        	<c:when test="${(sessionScope.codigoInclusao eq telefone.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq telefone.tipoAcao.chavePrimaria)}">
			<a href="javascript:exibirAlteracaoDoFone('${i}','${telefone.tipoFone.chavePrimaria}','${telefone.codigoDDD}','${telefone.numero}','${telefone.ramal}','${telefone.chavePrimaria }','${j}','${telefone.clienteFone.tipoFone.chavePrimaria}','${telefone.clienteFone.codigoDDD}','${telefone.clienteFone.numero}','${telefone.clienteFone.ramal}','${telefone.clienteFone.chavePrimaria}','${telefone.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
				<c:if test="${telefone.indicadorPrincipal eq false}">N�o</c:if>
	            <c:if test="${telefone.indicadorPrincipal eq true}">Sim</c:if>
			</a>
             </c:when>
             <c:otherwise>
             	<c:if test="${telefone.indicadorPrincipal eq false}">N�o</c:if>
	            <c:if test="${telefone.indicadorPrincipal eq true}">Sim</c:if>
             </c:otherwise>
        </c:choose>
        </display:column>
        
        <display:column sortable="false" title="A��o" style="width: 80px">
        <c:choose>
        	<c:when test="${(sessionScope.codigoInclusao eq telefone.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq telefone.tipoAcao.chavePrimaria)}">
				<a href="javascript:exibirAlteracaoDoFone('${i}','${telefone.tipoFone.chavePrimaria}','${telefone.codigoDDD}','${telefone.numero}','${telefone.ramal}','${telefone.chavePrimaria }','${j}','${telefone.clienteFone.tipoFone.chavePrimaria}','${telefone.clienteFone.codigoDDD}','${telefone.clienteFone.numero}','${telefone.clienteFone.ramal}','${telefone.clienteFone.chavePrimaria}','${telefone.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
					${telefone.tipoAcao.descricao}
				</a>
			</c:when>
			 <c:otherwise>
					<c:out value= "${telefone.tipoAcao.descricao}"/>
			 </c:otherwise>
		</c:choose>
		</display:column>
        
	<c:set var="i" value="${i+1}" />
	</display:table>
	

</fieldset>


<fieldset id="clienteEnderecoCol1" class="colunaDir22">
<input type="hidden" name="indexListaCliente" id="indexListaCliente">
<label id="rotuloTipoTelefone" class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Tipo de Telefone:</label>
<select name="tipoTelefoneCliente" id="tipoTelefoneCliente" disabled="disabled" class="campoSelect">
	<option value="-1">Selecione</option>
	<c:forEach items="${listaTipoFone}" var="tipoTelefone">
		<option value="<c:out value="${tipoTelefone.chavePrimaria}"/>">
			<c:out value="${tipoTelefone.descricao}"/>
		</option>		
	</c:forEach>
</select><br/>

		
<label id="rotulo"class="rotulo campoObrigatorio"  ><span class="campoObrigatorioSimbolo">* </span>DDD:</label>
<input class="campoTexto" type="text" id="codigoDDDClienteOriginal" name="codigoDDDClienteOriginal" disabled="disabled" maxlength="5" size="3"><br/>


<label id="rotulo" class="rotulo campoObrigatorio"  ><span class="campoObrigatorioSimbolo">* </span>N�mero: </label>
<input class="campoTexto" type="text" id="numeroClienteOriginal" name="numeroClienteOriginal" disabled="disabled" maxlength="10" size="11"><br/>


<label id="rotulo" class="rotulo"  >Ramal: </label>
<input class="campoTexto" type="text" id="ramalClienteOriginal" name="ramalClienteOriginal" disabled="disabled" maxlength="5" size="3"><br/>

<br /><br /><br />
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaClienteFones" sort="list" id="fone" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		
		<display:column  sortable="false" title="Tipo" style="width: 110px">
			<c:out value="${fone.tipoFone.descricao}"/>
        </display:column>
        
        <display:column  sortable="false" title="DDD" style="width: 80px">
			<c:out value="${fone.codigoDDD}"/>
        </display:column>
       	<display:column  sortable="false" title="N�mero" style="width: 50px">
			<c:out value="${fone.numero}"/>
        </display:column>
        <display:column  sortable="false" title="Principal" style="width: 40px">
				<c:if test="${fone.indicadorPrincipal eq false}">N�o</c:if>
	            <c:if test="${fone.indicadorPrincipal eq true}">Sim</c:if>
        </display:column>
        <c:set var="j" value="${j+1}" />
	</display:table>

</fieldset>

</fieldset>


</div>


