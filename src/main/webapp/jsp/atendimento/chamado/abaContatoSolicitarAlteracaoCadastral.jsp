<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script>

function exibirAlteracaoDoContato(indice,indexListaCliente,idTipoContato,codigoDDD,email,fone,nome,ramal,idProfissao,descricaoArea,chavePrimariaContato, idTipoContatoCliente,codigoDDDCliente,emailCliente,foneCliente,nomeCliente,ramalCliente,idProfissaoCliente,descricaoAreaCliente,chavePrimariaClienteContato, tipoAcao, codigoInclusao, codigoAlteracao) {
	if (indice != "") {
		
		$("#nomeContato").val(nome);
		$("#codigoDDDContato").val(codigoDDD);
		$("#emailContato").val(email);
		$("#foneContato").val(fone);
		
		$("#ramalContato").val(ramal);
		$("#descricaoAreaContato").val(descricaoArea);
		$("#profissaoContato").val(idProfissao);
		$("#tipoContatoContato").val(idTipoContato);
		
		exibirAlteracaoDoContatoCliente(indexListaCliente,idTipoContatoCliente,codigoDDDCliente,emailCliente,foneCliente,nomeCliente,ramalCliente,idProfissaoCliente,descricaoAreaCliente,chavePrimariaClienteContato);
		preencherCheckMapaContato(chavePrimariaClienteContato);
		
		if(tipoAcao != "" && tipoAcao == codigoInclusao){
			
			document.forms[0].tipoContatoContatoCheckboxContato.checked = true;
			document.forms[0].tipoContatoContatoCheckboxContato.disabled = true;
			
			document.forms[0].nomeContatoCheckboxContato.checked = true;
			document.forms[0].nomeContatoCheckboxContato.disabled = true;
						
		}
		
	}
}

function preencherCheckMapaContato(chavePrimariaClienteContato){
	
	$("input:checkbox[name$='CheckboxContato']").attr("checked",false);
	$("input:checkbox[name$='CheckboxContato']").attr("disabled",false);
	
	var url = "retornarCheckMapa?tipoMapa=Contato" + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		}).responseText;
	
	retorno = retorno.substring(1,retorno.length-1).split("],");
	
	for(j = 0 ; j < retorno.length ; j++){
		var chaveAux = retorno[j].split(":");
		var checkBoxMarcadosListaMapa = new Object();
		
		for(i = 0 ; i < chaveAux.length/2 ;){
			
			chaveAux[i] = chaveAux[i].substring(1,chaveAux[i].length-1);
				
			checkBoxMarcadosListaMapa[chaveAux[i]] = chaveAux[++i];
			++i;
		}
		
	      if(checkBoxMarcadosListaMapa != null && checkBoxMarcadosListaMapa != ""){
	    	  
	     		for (var chaveMapa in checkBoxMarcadosListaMapa){
	     			
	     			if(chaveMapa == chavePrimariaClienteContato){
	     				
	     				var listaCheckBoxMarcados = checkBoxMarcadosListaMapa[chaveMapa].split(",");
	     				for(i = 0 ; i < listaCheckBoxMarcados.length ; i++){
	     					
	     					var valorchaveMapaAux = listaCheckBoxMarcados[i].split("[");
	     					var valorchaveMapaAlterado;
	     					
	     					 if(valorchaveMapaAux.length == 2){
	     						valorchaveMapaAlterado = valorchaveMapaAux[1].split("]");
	     					 }else{
	     						valorchaveMapaAlterado = valorchaveMapaAux[0].split("]");
	     					 }
	     					
	     					
	        	               var valorchaveMapa = valorchaveMapaAlterado[0].split(",");

	        	               for(j = 0 ; j < valorchaveMapa.length ; j++){
	        	            	   
	        	            	   var valor = valorchaveMapa[j].replace(" ","");
	        	            	   valor = valor.replace("\"","").replace("\"","").concat("ContatoCheckboxContato"); 
	        	            	   
	        	            	   document.getElementById(valor).checked = true;
	        	               }
	     	     		}
	     			 }
	     			
	         	}
	          
	      }
	}
	
	
}

function exibirAlteracaoDoContatoCliente(indice,idTipoContato,codigoDDD,email,fone,nome,ramal,idProfissao,descricaoArea,chavePrimariaClienteContato) {
	
	if (indice != "" && chavePrimariaClienteContato != "" || chavePrimariaClienteContato > 0) {
		document.forms[0].indexListaCliente.value = indice;
		
		$("#nomeContatoCliente").val(nome);
		$("#codigoDDDContatoCliente").val(codigoDDD);
		$("#emailContatoCliente").val(email);
		$("#foneContatoCliente").val(fone);
		
		$("#ramalContatoCliente").val(ramal);
		$("#descricaoAreaContatoCliente").val(descricaoArea);
		$("#profissaoContatoCliente").val(idProfissao);
		$("#tipoContatoCliente").val(idTipoContato);
		
	}else{
		$("#nomeContatoCliente").val(null);
		$("#codigoDDDContatoCliente").val(null);
		$("#emailContatoCliente").val(null);
		$("#foneContatoCliente").val(null);
		
		$("#ramalContatoCliente").val(null);
		$("#descricaoAreaContatoCliente").val(null);
		$("#profissaoContatoCliente").val("-1");
		$("#tipoContatoCliente").val("-1");
	}
	
	document.forms[0].chavePrimariaClienteContato.value = chavePrimariaClienteContato;
}

function preencherMapaListaAlteracaoCadastroClienteChamadoContato(idCampo, valorChecked, valor, chavePrimariaClienteContato){
	
	var url = "preencherMapaListaAlteracaoCadastroClienteChamado?idCampo=" + idCampo + "&valorChecked="
	+ valorChecked + "&valor=" + valor + "&chavePrimariaClienteEndereco=" + chavePrimariaClienteContato + "&tipoMapa=Contato" + "&posFixoTipoCheckbox=ContatoCheckboxContato" + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		}).responseText;
}

function confimarAlteracaoListaContato(){
	
	var chavePrimariaClienteContato = document.forms[0].chavePrimariaClienteContato.value;
		
	var lista = $("input:checkbox[name$='CheckboxContato']");
	
	for(var i = 0; i < lista.length; i++) {
			
		var idCampo = lista[i].id;
		var idCampoFormatado = idCampo.split("CheckboxContato");

		preencherMapaListaAlteracaoCadastroClienteChamadoContato(lista[i].id, lista[i].checked, document.getElementById(idCampoFormatado[0]).value, chavePrimariaClienteContato);
		document.getElementById(idCampoFormatado[0]).value = "";
		document.getElementById(idCampo).checked = false;
		document.getElementById(idCampo).disabled = false;
	}
	
	alert("Altera��o realizada com sucesso.");
}

</script>

<div id="clienteAbaEndereco">
<fieldset class="conteinerDados3">
	<fieldset id="clienteEnderecoCol1" class="colunaEsq">
	<input type="hidden" name="indexLista" id="indexLista">
	<input type="hidden" name="chavePrimariaClienteContato" id="chavePrimariaClienteContato">
	
		<label class="rotulo campoObrigatorio" id="rotuloTipoContato" for="tipoContato"><span class="campoObrigatorioSimbolo">* </span>Tipo de Contato:</label>
		<select name="tipoContatoContato" id="tipoContatoContato" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoContato}" var="tipoContato">
				<option value="<c:out value="${tipoContato.chavePrimaria}"/>" >
					<c:out value="${tipoContato.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<input class="solicitacaoAlteracaoTipoContatoCheckbox" type="checkbox" id="tipoContatoContatoCheckboxContato" name="tipoContatoContatoCheckboxContato"><br />
		
		<label class="rotulo" id="rotuloNomeContato" for="nomeContato"><span class="campoObrigatorioSimbolo">* </span>Nome do Contato:</label>
		<input class="campoTexto" type="text" id="nomeContato" name="nomeContato" disabled="disabled" maxlength="30" size="30" ><br />
		<input class="#" type="checkbox" id="nomeContatoCheckboxContato" name="nomeContatoCheckboxContato"><br />
		
		<label class="rotulo" id="rotuloCodigoDDDContato" for="codigoDDDContato">DDD:</label>
		<input class="campoTexto" type="text" id="codigoDDDContato" name="codigoDDDContato" disabled="disabled" maxlength="5" size="3" ><br />
		<input class="solicitacaoAlteracaoCodigoDDDCheckbox" type="checkbox" id="codigoDDDContatoCheckboxContato" name="codigoDDDContatoCheckboxContato"  ><br />
		
		<label class="rotulo" id="rotuloFoneContato" for="foneContato">Telefone:</label>
		<input class="campoTexto" type="text" id="foneContato" name="foneContato" disabled="disabled" maxlength="50" size="30"><br />
		<input class="solicitacaoAlteracaoFoneCheckbox" type="checkbox" id="foneContatoCheckboxContato" name="foneContatoCheckboxContato"  ><br />
		
		<label class="rotulo" for="ramalContato">Ramal:</label>
		<input class="campoTexto" type="text" id="ramalContato" name="ramalContato" disabled="disabled" maxlength="30" size="30" ><br />
		<input class="solicitacaoAlteracaoRamalCheckbox" type="checkbox" id="ramalContatoCheckboxContato" name="ramalContatoCheckboxContato"  ><br />
		
		<label class="rotulo" id="rotuloCargoContato" for="cargoContato">Cargo:</label>
		<select name="profissaoContato" id="profissaoContato" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaProfissao}" var="profissao">
				<option value="<c:out value="${profissao.chavePrimaria}"/>" >
				<c:out value="${profissao.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<input class="solicitacaoAlteracaoProfissaoCheckbox" type="checkbox" id="profissaoContatoCheckboxContato" name="profissaoContatoCheckboxContato"  ><br />
		
		<label class="rotulo" id="rotuloDescricaoArea" for="descricaoArea">�rea:</label>
		<input class="campoTexto" type="text" id="descricaoAreaContato" name="descricaoAreaContato" disabled="disabled" maxlength="10" size="10">
		<input class="solicitacaoAlteracaoDescricaoAreaCheckbox" type="checkbox" id="descricaoAreaContatoCheckboxContato" name="descricaoAreaContatoCheckboxContato"  ><br /><br />
		
		<label class="rotulo" id="rotuloEmailContato" for="emailContato">E-mail de Contato:</label>
		<input class="campoTexto" type="text" id="emailContato" name="emailContato" disabled="disabled" onkeypress="return formatarCampoInteiro(event);" >
		<input class="solicitacaoAlteracaoEmailContatoCheckbox" type="checkbox" id="emailContatoCheckboxContato" name="emailContatoCheckboxContato"  >
		
		<input class="bottonRightCol2 botaoGrande1 botaoIncluir" name="botaoConfirmar" id="botaoConfirmar" value="Confirmar" type="button" onclick="confimarAlteracaoListaContato();">
		
		
	<c:set var="j" value="0" />
	<c:set var="i" value="0" />
	
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba " name="listaChamadoAlteracaoClienteContato" sort="list" id="contato" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		
			<display:column  sortable="false" title="Tipo" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
		          		<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.tipoContato.descricao}"/>
						</a> 
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.tipoContato.descricao}"/>
		             </c:otherwise>
       			</c:choose>
			</display:column>		
			
			
			
			<display:column  sortable="false" title="Nome" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
		          		<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
						<c:out value="${contato.nome}"/>
					</a>
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.nome}"/>
		             </c:otherwise>
       			</c:choose> 
			</display:column>	
			
			<display:column  sortable="false" title="DDD" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
		          		<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.codigoDDD}"/>
						</a> 
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.codigoDDD}"/>
		             </c:otherwise>
       			</c:choose>
			</display:column>	
			
			<display:column  sortable="false" title="Telefone" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
		          		<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.fone}"/>
						</a> 
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.fone}"/>
		             </c:otherwise>
       			</c:choose>
			</display:column>	
			
			<display:column  sortable="false" title="Ramal" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
		          		<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.ramal}"/>
						</a> 
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.ramal}"/>
		             </c:otherwise>
       			</c:choose>
			</display:column>	
			
			<display:column  sortable="false" title="Cargo" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
		          		<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.profissao.descricao}"/>
						</a> 
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.profissao.descricao}"/>
		             </c:otherwise>
       			</c:choose> 
			</display:column>	
			
			<display:column  sortable="false" title="�rea" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
		          		<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.descricaoArea}"/>
						</a>
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.descricaoArea}"/>
		             </c:otherwise>
       			</c:choose> 
			</display:column>
			
			<display:column  sortable="false" title="E-mail" style="width: 110px">
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
						<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.email}"/>
						</a> 
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.email}"/>
		             </c:otherwise>
       			</c:choose> 
			</display:column>
						
			<display:column sortable="false" title="Principal" style="width: 25px">
				<c:choose>
			  		<c:when test="${contato.principal}"> Sim </c:when>
			  		<c:otherwise> N�o </c:otherwise>
			  	</c:choose>
			</display:column>
			
			<display:column sortable="false" title="A��o" style="width: 190px">				
				<c:choose>
		        	<c:when test="${(sessionScope.codigoInclusao eq contato.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq contato.tipoAcao.chavePrimaria)}">
						<a href="javascript:exibirAlteracaoDoContato('${i}','${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}','${contato.contatoCliente.tipoContato.chavePrimaria}','${contato.contatoCliente.codigoDDD}','${contato.contatoCliente.email}','${contato.contatoCliente.fone}','${contato.contatoCliente.nome}','${contato.contatoCliente.ramal}','${contato.contatoCliente.profissao.chavePrimaria}','${contato.contatoCliente.descricaoArea}','${contato.contatoCliente.chavePrimaria}','${contato.tipoAcao.chavePrimaria}','${sessionScope.codigoInclusao}','${sessionScope.codigoAlteracao}');">
							<c:out value="${contato.tipoAcao.descricao}"/>
						</a> 
		          	 </c:when>
		             <c:otherwise>
		             	<c:out value="${contato.tipoAcao.descricao}"/>
		             </c:otherwise>
       			</c:choose>
			</display:column>
						
		<c:set var="i" value="${i+1}" />
	</display:table>	
	
</fieldset>

	<fieldset>
	<input type="hidden" name="indexListaCliente" id="indexListaCliente">
	<fieldset id="clienteEnderecoCol1" class="colunaDir22">
		<label class="rotulo campoObrigatorio" id="rotuloProfissao" for="profissaoContatoCliente">Tipo de Contato:</label>
		<select name="tipoContatoCliente" id="tipoContatoCliente" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoContato}" var="tipoContato">
				<option value="<c:out value="${tipoContato.chavePrimaria}"/>" >
					<c:out value="${tipoContato.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		
		<label class="rotulo" id="rotuloNomeContato" for="nomeContatoCliente">Nome do Contato:</label>
		<input class="campoTexto" type="text" id="nomeContatoCliente" name="nomeContatoCliente" disabled="disabled" maxlength="30" size="30" ><br />
		
		<label class="rotulo" id="rotuloDDDContato" for="codigoDDDContatoCliente">DDD:</label>
		<input class="campoTexto" type="text" id="codigoDDDContatoCliente" name="codigoDDDContatoCliente" disabled="disabled" maxlength="5" size="3" ><br />
		
		<label class="rotulo" id="rotuloComplemento" for="foneContatoCliente">Telefone:</label>
		<input class="campoTexto" type="text" id="foneContatoCliente" name="foneContatoCliente" disabled="disabled" maxlength="50" size="30"><br />
		
		<label class="rotulo" for="ramalContato">Ramal:</label>
		<input class="campoTexto" type="text" id="ramalContatoCliente" name="ramalContatoCliente" disabled="disabled" maxlength="30" size="30" ><br />
		
		<label class="rotulo" id="rotuloCargoContatoCliente" for="cargoContatoCliente">Cargo:</label>
		<select name="profissaoContatoCliente" id="profissaoContatoCliente" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaProfissao}" var="profissao">
				<option value="<c:out value="${profissao.chavePrimaria}"/>" >
				<c:out value="${profissao.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />		
		<label class="rotulo" id="rotuloDescricaoArea" for="descricaoAreaContatoCliente">�rea:</label>
		<input class="campoTexto" type="text" id="descricaoAreaContatoCliente" name="descricaoAreaContatoCliente" disabled="disabled" maxlength="10" size="10">
		
		<label class="rotulo" id="rotuloEmailContato" for="emailContatoCliente">E-mail de Contato:</label>
		<input class="campoTexto" type="text" id="emailContatoCliente" name="emailContatoCliente" disabled="disabled"  onkeypress="return formatarCampoInteiro(event);" >
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaClienteContato" sort="list" id="contato" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			<display:column  sortable="false" title="Tipo" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.tipoContato.descricao}"/>
				</a> 
			</display:column>		
			
			<display:column  sortable="false" title="Nome" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.nome}"/>
				</a> 
			</display:column>	
			
			<display:column  sortable="false" title="DDD" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.codigoDDD}"/>
				</a> 
			</display:column>	
			
			<display:column  sortable="false" title="Telefone" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.fone}"/>
				</a> 
			</display:column>	
			
			<display:column  sortable="false" title="Ramal" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.ramal}"/>
				</a> 
			</display:column>	
			
			<display:column  sortable="false" title="Cargo" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.profissao.descricao}"/>
				</a> 
			</display:column>	
			
			<display:column  sortable="false" title="�rea" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.descricaoArea}"/>
				</a> 
			</display:column>
			
			<display:column  sortable="false" title="E-mail" style="width: 110px">
				<a href="javascript:exibirAlteracaoDoContatoCliente('${j}','${contato.tipoContato.chavePrimaria}','${contato.codigoDDD}','${contato.email}','${contato.fone}','${contato.nome}','${contato.ramal}','${contato.profissao.chavePrimaria}','${contato.descricaoArea}','${contato.chavePrimaria}');">
					<c:out value="${contato.email}"/>
				</a> 
			</display:column>
			
			<display:column sortable="false" title="Principal" style="width: 25px">
				<c:choose>
			  		<c:when test="${contato.principal}"> Sim </c:when>
			  		<c:otherwise> N�o </c:otherwise>
			  	</c:choose>
			</display:column>			
			
		<c:set var="j" value="${j+1}" />
	</display:table>

</fieldset>		
</fieldset>
	
	
</fieldset>



</div>
