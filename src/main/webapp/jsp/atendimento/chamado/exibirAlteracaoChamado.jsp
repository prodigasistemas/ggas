<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">





function alterar(){
	submeter('chamadoForm','alterarChamado');
}



function cancelar() {
	location.href = '<c:url value="/exibirPesquisaChamado"/>';
}

function limparFormulario(){
	}



</script>

<c:if test="${ fluxoDetalhamento ne true }">
	<h1 class="tituloInterno">Alterar Chamado</h1>
	<p class="orientacaoInicial">Altere os dados e clique em <span class="destaqueOrientacaoInicial">Alterar</span>
	    para alterar o Chamado.<br/>Para cancelar clique em <span class="destaqueOrientacaoInicial">Cancelar</span>.<br />
	</p>
</c:if>
<c:if test="${ fluxoDetalhamento eq true }">
	<h1 class="tituloInterno">Detalhar Chamado</h1>
	<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>
</c:if>

<form method="post" action="alterarChamado" id="chamadoForm" name="chamadoForm">
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chamado.chavePrimaria}">
	<input type="hidden" name="fluxoDetalhamento" id="fluxoDetalhamento" value="${fluxoDetalhamento}">


<div id="mensagemContratoInvalido" title="Contrato inv�lido" style="display:none">
    <p>Nenhum contrato localizado para o valor informado.</p>
</div>
<div id="mensagemClienteNaoLocalizado" title="Cliente n�o localizado" style="display:none">
    <p>Cliente n�o localizado.</p>
</div>
<fieldset class="conteinerPesquisarIncluirAlterar">
<fieldset class="coluna">
	<fieldset id="conteinerTipoMensagemVigencia">
		<input type="hidden" id="protocolo" value="${chamado.protocolo.chavePrimaria}" />
		<label class="rotulo <c:if test="${chamado.protocolo.numeroProtocolo eq null}">campoObrigatorio</c:if>" id="rotuloNumeroProposta" for="numeroProposta"><c:if test="${chamado.protocolo.numeroProtocolo eq null}"> <span class="campoObrigatorioSimbolo">* </span></c:if>N�mero do Protocolo:</label>
		<input class="campoTexto" type="text" id="numeroProtocolo" name="numeroProtocolo" <c:if test="${chamado.protocolo.numeroProtocolo ne null}"> disabled="true" </c:if> maxlength="10" size="10" onkeypress="return formatarCampoInteiro(event);" value="${chamado.protocolo.numeroProtocolo}"><br />

		<label class="rotulo" id="rotuloNomeSolicitante" for="nomeSolicitante">Nome do Solicitante:</label>
		<input class="campoTexto" type="text" id="nomeSolicitante" name="nomeSolicitante" maxlength="30" size="32"  value="${chamado.nomeSolicitante}"><br />

		<label class="rotulo" id="rotuloCpfSolicitante" for="cpfSolicitante">CPF do Solicitante:</label>
		<input class="campoTexto" type="text" id="cpfSolicitante" name="cpfSolicitante" maxlength="30" size="32"  value="${chamado.cpfCnpjSolicitante}"><br />

		<label class="rotulo" id="rotuloCnpjSolicitante" for="cnpjSolicitante">CNPJ do Solicitante:</label>
		<input class="campoTexto" type="text" id="cnpjSolicitante" name="cnpjSolicitante" maxlength="30" size="32"  value="${chamado.cpfCnpjSolicitante}"><br />

		<label class="rotulo" id="rotuloRgSolicitante" for="rgSolicitante">RG do Solicitante:</label>
		<input class="campoTexto" type="text" id="rgSolicitante" name="rgSolicitante" maxlength="10" size="10"  value="${chamado.rgSolicitante}"><br />	
        <input type="hidden" id="horaAcionamentos" name="chamado.horaAcionamento" 
                    		value="${chamado.horaAcionamento}"/>		
	</fieldset>
	<fieldset id="conteinerSegmentos" class="conteinerCampoList">
		<fieldset class="conteinerLista1">
			<label class="rotulo rotuloVertical rotuloCampoList" for="chamadosTipoDisponiveis"><span class="campoObrigatorioSimbolo">* </span>Tipo do Chamado:</label>
					<select id="chamadosTipoDisponiveis" name="chamadosTipoDisponiveis" class="campoList campoVertical" multiple="multiple" onclick="atualizarAssuntos(this);" >
						   	<c:forEach items="${chamadosTipo}" var="chamadotipo">
								<option value="<c:out value="${chamadotipo.chavePrimaria}"/>" <c:if test="${chamado.chamadoAssunto.chamadoTipo.chavePrimaria == chamadotipo.chavePrimaria}">selected="selected"</c:if>> 
									<c:out value="${chamadotipo.descricao}" />
								</option>		
						    </c:forEach>
					  </select>
		</fieldset>	
		<div id="divAssuntos">	  
			<jsp:include page="/jsp/atendimento/chamado/bootstrap/selectBoxAssuntoBootstrap.jsp"></jsp:include>
		</div>
	</fieldset>
	<fieldset id="conteinerSegmentos" class="conteinerCampoList">
		<fieldset class="conteinerLista1">
			<label class="rotulo rotuloVertical rotuloCampoList" for="unidadeOrganizacional"><span class="campoObrigatorioSimbolo">* </span>Unidade Organizacional:</label>
				<select id="unidadeOrganizacional" name="unidadeOrganizacional" class="campoList campoVertical" multiple="multiple" >
					   	<c:forEach items="${listaUnidadeOrganizacional}" var="unidadeOrganizacional">
							<option value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>" <c:if test="${chamado.unidadeOrganizacional.chavePrimaria == unidadeOrganizacional.chavePrimaria}">selected="selected"</c:if>> 
								<c:out value="${unidadeOrganizacional.descricao}"/>
							</option>		
					    </c:forEach>
				  </select><br />	
		</fieldset>	  
		<fieldset class="conteinerLista1">
		<label class="rotulo rotuloVertical rotuloCampoList" for="canalAtendimento"><span class="campoObrigatorioSimbolo">* </span>Canal de Atendimento:</label>
				<select id="canalAtendimento" name="canalAtendimento" class="campoList campoVertical" multiple="multiple" >
					   	<c:forEach items="${listaCanalAtendimento}" var="canalAtendimento">
							<option value="<c:out value="${canalAtendimento.chavePrimaria}"/>" <c:if test="${chamado.canalAtendimento.chavePrimaria == canalAtendimento.chavePrimaria}">selected="selected"</c:if>> 
								<c:out value="${canalAtendimento.descricao}"/>
							</option>		
					    </c:forEach>
				  </select>
		</fieldset>		  
	</fieldset><br/>
	
	<c:if test="${ fluxoDetalhamento eq true }">
		<div id="divResponsavel">
			<label class="rotulo" id="rotuloNomeSolicitante" for="usuarioResponsavel">Reponsavel:</label>
			<input class="campoTexto" type="text" id="usuarioResponsavel" name="usuarioResponsavel" maxlength="30" size="30"  value="${chamado.usuarioResponsavel.funcionario.nome}"><br />
		</div>
	</c:if>
	
	<label class="rotulo rotuloVertical" for="mensagem">Descri��o:</label>
			<textarea  class="campoVertical" name="descricao" id="descricao" cols="50" rows="6"
			onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" 
			onkeypress="return formatarCampoTextoLivreComLimite(event,this,120);">${mensagemFaturamentoForm.map.mensagem}</textarea>
</fieldset>


<fieldset class="colunaFinal" style="width: 500px">
	<fieldset id="tabs" style="display: none">
			<ul>
				<li><a href="#chamadoAbaCliente">Cliente</a></li>
				<li><a href="#chamadoAbaImovel">Imovel</a></li>
			</ul>
			<fieldset id="chamadoAbaCliente">
			<fieldset id="conteinerPenalidades">
					<div class="conteinerDados2">
						<fieldset id="conteinerSobreDemanda">
							<legend>Contrato:</legend>
							<div class="conteinerDados">
								<label class="rotulo campoObrigatorio" id="rotuloNumeroContrato" for="numeroContrato"><span class="campoObrigatorioSimbolo">* </span>N�mero do Contrato:</label>
								<input class="campoTexto" type="text" id="numeroContrato" name="numeroContrato" maxlength="30" size="32" onkeypress="return formatarCampoInteiro(event);" onBlur="carregarClientePorContrato(this.value);" value="${chamado.contrato.numero}"><br />
								<br class="quebraLinha" />
							</div>
						</fieldset>
						<div id="divCliente">
						<jsp:include page="/jsp/atendimento/chamado/abaCliente.jsp"></jsp:include>
						</div>
					</div>
				</fieldset>	
			</fieldset>
			<fieldset id="chamadoAbaImovel" >
				<jsp:include page="/jsp/atendimento/chamado/abaImovel.jsp"></jsp:include>
			</fieldset>
			
			<c:if test="${ fluxoDetalhamento eq true }">
				<fieldset id="gridChamadosCliente" class="conteinerDados conteinerPesquisarIncluirAlterar ">
					<fieldset id="gridChamadosCliente" class="conteinerDados conteinerPesquisarIncluirAlterar ">
<%-- 					<jsp:include page="/jsp/atendimento/chamado/gridChamadoHistorico.jsp"></jsp:include> --%>
					</fieldset>
				</fieldset>
			</c:if>
	</fieldset>
</fieldset>
</fieldset>
		<c:if test="${ fluxoDetalhamento ne true }">
			<hr class="linhaSeparadoraPesquisa" />
			<fieldset id="gridChamadosCliente" class="conteinerDados conteinerPesquisarIncluirAlterar ">
				<jsp:include page="/jsp/atendimento/chamado/gridChamadosCliente.jsp"></jsp:include>
			</fieldset>
		</c:if>

	<fieldset class="conteinerBotoes"> 
	<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onClick="cancelar();">
    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
<%--     <vacess:vacess param="incluirChamadoRascunho"> --%>
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Alterar Rascunho" type="submit">
<%--     </vacess:vacess> --%>
<%--     <vacess:vacess param="incluirChamado"> --%>
    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Alterar" type="submit" onclick="incluir();">
<%--     </vacess:vacess> --%>
 </fieldset>
	
</form>
