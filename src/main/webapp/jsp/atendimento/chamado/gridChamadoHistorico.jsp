<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->


<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

function detalhamentoChamado(chave){
	
	var form = document.createElement("form");
	
	var element1 = document.createElement("input"); 
    
    form.method = "POST";
    
    form.name = 'chamadoForm';
    
    element1.value=chave;
    element1.name="chavePrimaria";
    form.appendChild(element1);  
	
    document.body.appendChild(form);	
    
    submeter(form.name, 'exibirDetalhamentoChamado')
}

</script>


<legend>Hist�rico do Chamado</legend>
	<display:table class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="listaChamadoHistorico" sort="list" id="chamadoHistorico" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		
		<display:column title="Data / Hora">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<fmt:formatDate value="${chamadoHistorico.ultimaAlteracao}" pattern="dd/MM/yyyy: HH:mm"/>	     		
            </a>
	    </display:column>
		
		<display:column sortable="false" title="Usu�rio">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${chamadoHistorico.usuario.login}"/>
            </a>
	    </display:column>
					
		<display:column sortable="false" title="Respons�vel" maxLength="65">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${chamadoHistorico.usuarioResponsavel.funcionario.nome}"/>
            </a>
	    </display:column>
			    
			     	
		<display:column sortable="false" title="Unidade Organizacional">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${chamadoHistorico.unidadeOrganizacional.descricao}"/>
            </a>
	    </display:column>
			     	
		<display:column sortable="false" title="Descri��o" maxLength="65">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${chamadoHistorico.descricao}"/>
            </a>
	    </display:column>
		
		<display:column sortable="false" title="Motivo" maxLength="60">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${chamadoHistorico.motivo.descricao}"/>     		
            </a>
	    </display:column>
					     	
		<display:column sortable="false" title="Opera��o">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${chamadoHistorico.operacao.descricao}"/>
            </a>
	    </display:column>
			     	
		<display:column sortable="false" title="Situa��o">
			<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
				<c:out value="${chamadoHistorico.status.descricao}"/>
            </a>
	    </display:column>
	
		     	
	</display:table>
