<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script type="text/javascript">

$(document)
.ready(
		function() {
			var debitoAutomatico = '${contrato.indicadorDebitoAutomatico}';
			if (debitoAutomatico == "true") {
				animatedcollapse.show('ContainerDebitoAutomatico');
			}
		});
		
animatedcollapse.addDiv('ContainerDebitoAutomatico',
	'fade=0,speed=400,hide=1');

function detalharContrato(chaveContrato){
	document.forms['chamadoForm'].chavePrimaria.value = chaveContrato;
	submeter('chamadoForm','exibirDetalhamentoContrato');
}

function showContainerDebitoAutomatico() {
	animatedcollapse.show('ContainerDebitoAutomatico');
}

function hideContainerDebitoAutomatico() {
	animatedcollapse.hide('ContainerDebitoAutomatico');
}

function salvarDebitoAutomatico(chaveContrato){
	document.forms['chamadoForm'].chavePrimaria.value = chaveContrato;
	submeter('chamadoForm','salvarDebitoAutomaticoContrato');
}

</script>

<form:form method="post" action="exibirDetalhamentoContrato" id="chamadoForm" name="chamadoForm" target="_blank">
	
	<input type="hidden" name="chavePrimaria" id="chavePrimaria">
<input type="hidden" name="situacaoTitulo" id="situacaoTitulo" value=""/>
	<input type="hidden" id="idPontoConsumo" name="idPontoConsumo" value="${pontoConsumo.chavePrimaria}"/>
	<input type="hidden" id="isTelaChamado" name="isTelaChamado" value="1"/>
	

<div id="abaFinanceiro">
<fieldset class="conteinerDados3">
	<legend>Dados Contratuais</legend>
	
	<fieldset class="detalhamento">
		<fieldset id="detalhamentoGeral" class="coluna itemDetalhamentoLargo3">
			<label class="rotulo">N�mero Contrato:</label>
			<span class="itemDetalhamento"><c:out value="${contrato.numeroFormatadoSemAditivo}"/></span><br />
			<label class="rotulo rotulo2Linhas">N�mero Anterior do Contrato:</label>
			<span class="itemDetalhamento"><c:out value="${contrato.numeroAnterior}"/></span><br />
			<label class="rotulo">Valor do Contrato:</label>
			<span class="itemDetalhamento"><c:if test="${contrato.valorContrato > 0}"><c:out value="${contrato.valorContrato}"/></c:if></span><br />
			<label class="rotulo rotuloVertical rotuloCampoList" for="qdc">QDC:</label>
				<select id="contratoQDC" name="contratoQDC" class="campoList campoVertical" multiple="multiple" disabled="disabled" style="margin-left: 128px; margin-top: -16px; min-width:200px">
			  	 	<c:forEach items="${listaContratoQDC}" var="qdc">
						<option value="<c:out value="${qdc}"/>"> 
							<c:out value="${qdc}"/>
						</option>		
				    </c:forEach>
			</select><br />
			<label class="rotulo ">Fator �nico de Corre��o:</label>
			<span class="itemDetalhamento"><c:out value="${contratoPontoConsumo.numeroFatorCorrecao}"/></span><br />
		</fieldset>	
	
		<fieldset id="funcionarioCol2" class="colunaFinal">
			<label class="rotulo">N�mero do Aditivo:</label>
			<span class="itemDetalhamento"><c:if test="${contrato.numeroAditivo > 0}"><c:out value="${contrato.numeroAditivo}"/></c:if></span><br />
			<label class="rotulo">Descri��o do Aditivo:</label>
			<span class="itemDetalhamento"><c:out value="${contrato.descricaoAditivo}"/></span><br />
			<label class="rotulo">Data do Aditivo:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${contrato.dataAditivo}" pattern="dd/MM/yyyy"/></span><br />
			<label class="rotulo">Data da Assinatura:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${contrato.dataAssinatura}" pattern="dd/MM/yyyy"/></span><br />
			<label class="rotulo">Fim da Vig�ncia:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${contrato.dataFimGarantiaFinanciamento}" pattern="dd/MM/yyyy"/></span><br />
			<label class="rotulo">M�todo de Cobran�a:</label>
			<span class="itemDetalhamento"><c:out value="${arrecadadorContratoConvenio.tipoConvenio.descricao}"/></span><br />
		</fieldset>		
</fieldset>	

<hr class="linhaSeparadora1" />	
	
	<fieldset class="detalhamento">
		<fieldset id="funcionarioCol2" class="coluna itemDetalhamentoLargo3">
			<label class="rotulo">Multa Recis�ria:</label>
			<span class="itemDetalhamento"><c:out value="${contrato.multaRecisoria.descricao}"/></span><br />
			<label class="rotulo">Vaz�o instant�nea </br> m�xima:</label>	
			<span class="itemDetalhamento"><c:out value="${contratoPontoConsumo.medidaVazaoMaximaInstantanea}"/></span>
			<span class="itemDetalhamento">&nbsp;<c:out value="${contratoPontoConsumo.unidadeVazaoMaximaInstantanea.descricaoAbreviada}"/></span>
			<label class="rotulo rotulo2Linhas">Press�o de Fornecimento:</label>
			<span class="itemDetalhamento"><c:out value="${faixaPressaoFornecimento}"/></span>
			<span class="itemDetalhamento">&nbsp;<c:out value="${unidadeFaixaPressaoFornecimento}"/></span><br />
			<label class="rotulo rotulo2Linhas">Anteced�ncia para Negocia��o da Renova��o:</label>
			<span class="itemDetalhamento" style="margin-top: 12px"><c:out value="${contrato.diasAntecedenciaRenovacao}"/></span><br />						
		</fieldset>
		
		<fieldset id="funcionarioCol2" class="colunaFinal">
			<label class="rotulo">Renova��o Autom�tica:</label>
			<c:choose>
			<c:when test="${contrato.renovacaoAutomatica == 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span><br />
			</c:when>
			<c:when test="${contrato.renovacaoAutomatica == 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span><br />
			</c:when>
			</c:choose>			
			<label class="rotulo">Situa��o do Contrato:</label>
			<span class="itemDetalhamento"><c:out value="${contrato.situacao.descricao}"/></span><br />
			<label class="rotulo">Data de Vencimento:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${contrato.dataVencimentoObrigacoes}" pattern="dd/MM/yyyy"/></span><br />			
		</fieldset>
	</fieldset>	
	
<hr class="linhaSeparadora1" />


<%--     <div class="labelCampo">
        <label class="rotulo" forCampo="debitoAutomatico"> D�bito Autom�tico? </label>
           <input class="campoRadio" type="radio" name="debitoAutomatico" id="incluirContrato-debitoAutomatico" value="true" 
             onclick="showContainerDebitoAutomatico()" 
           <c:if test="${contrato.indicadorDebitoAutomatico eq true}" >checked="checked"</c:if>>
           <label class="rotuloRadio" for="debitoAutomaticoSim" id="incluirContrato-DebitoAutomaticoSimNao">Sim</label> 
            <input class="campoRadio" type="radio" name="debitoAutomatico" id="incluirContrato-debitoAutomatico" value="false"
              onclick="hideContainerDebitoAutomatico()" 
              <c:if test="${contrato.indicadorDebitoAutomatico eq false or empty contrato.indicadorDebitoAutomatico}" >checked="checked"</c:if> />
           <label class="rotuloRadio" for="debitoAutomaticoNao" id="incluirContrato-DebitoAutomaticoSimNao">N�o</label>								
           <br class="quebraRadio" />
        <br class="quebraLinha" />
    </div> --%>
    <br class="quebraLinha" />
    <fieldset id="ContainerDebitoAutomatico">
            <label class="rotulo" forCampo="arrecadadorConvenioDebitoAutomatico"> Arrecadador Conv�nio para D�bito Autom�tico: </label>
                <select class="campoSelect" id="arrecadadorConvenioDebitoAutomatico" name="arrecadadorConvenioDebitoAutomatico" >
                    <option value="-1">Selecione</option>
                    <c:forEach items="${listaArrecadadorConvenioDebitoAutomatico}" var="arrecadadorConvenio">
                        <option value="<c:out value="${arrecadadorConvenio.chavePrimaria}"/>"
                            <c:if test="${arrecadadorConvenio.chavePrimaria == arrecadadorConvenioDebitoAutomatico}">selected="selected"</c:if>>
                            <c:out value="${arrecadadorConvenio.codigoConvenio} - ${arrecadadorConvenio.arrecadadorContrato.arrecadador.banco.nomeAbreviado}" />
                        </option>
                    </c:forEach>
                </select>
        <div class="labelCampo">
            <label class="rotulo" forCampo="banco"> Banco: </label>
                    <select class="campoSelect" id="banco" name="banco" >
                        <option value="-1">Selecione</option>
                        <c:forEach items="${listaBanco}" var="bancoSistema">
                            <option value="<c:out value="${bancoSistema.chavePrimaria}"/>"
                                    <c:if test="${banco == bancoSistema.chavePrimaria}">selected="selected"</c:if>>
                                <c:out value="${bancoSistema.nome} - ${bancoSistema.nomeAbreviado}" />
                            </option>
                        </c:forEach>
                    </select>
        </div>
        <br class="quebraLinha" />
        <div class="labelCampo">
            <label class="rotulo" forCampo="agencia"> Ag�ncia: </label>
                    <input class="campoTexto campoHorizontal" id="agencia" name="agencia" type="text"
                           onkeypress="return formatarCampoInteiro(event);" onkeyup="validarCampoInteiro(this)"
                            maxlength="10"
                           value="<c:out value='${contrato.agencia}' />" size="10" />

        </div>
        <br class="quebraLinha" />
        <div class="labelCampo">
                <label class="rotulo" forCampo="agencia"> Conta Corrente: </label>
                    <input class="campoTexto campoHorizontal" id="contaCorrente" name="contaCorrente" type="text"
                           onkeypress="return formatarCampoInteiro(event);" onkeyup="validarCampoInteiro(this)"
                            maxlength="10"
                           value="<c:out value='${contrato.contaCorrente}' />" size="10" />
        </div>
    </fieldset>
	</br>
<%-- 	<input name="button" id="salvar-debito" class="bottonRightCol2" value="Salvar" type="button" onclick="salvarDebitoAutomatico(<c:out value="${contrato.chavePrimaria}"/>);">	
	<hr class="linhaSeparadora1" />	
--%>
<fieldset class="detalhamento">
	<fieldset class="coluna itemDetalhamentoLargo3">
	<legend class="conteinerBlocoTitulo">Hist�rico das Altera��es Contratuais</legend>
		<display:table class="dataTableGGAS" style="width: 320px" name="listaAlteracoesContrato" sort="list" id="contrato" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#informacaoPontoConsumoAbaContrato">
		       
			<display:column title="Data/Hora Altera��es Contratuais">
				<fmt:formatDate value="${contrato.ultimaAlteracao}" pattern="dd/MM/yyyy: HH:mm"/>	     		
			</display:column>
			
			<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
				<a target="_blank" onclick="javascript:detalharContrato(<c:out value="${contrato.chavePrimaria}"/>);">
					<img title="Detalhar Contrato" alt="Detalhar Contrato"  src="<c:url value="/imagens/pesquisa.gif"/>">				
				</a>			
			</display:column>
				
		</display:table>
	</fieldset>	
	
	<fieldset class="colunaFinal">
	<legend class="conteinerBlocoTitulo">Itens de Faturamento</legend>
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaContratoPontoConsumoItemFaturamento" sort="list" id="itemFaturamento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="informacaoPontoConsumoAbaContrato">
	
			<display:column property="itemFatura.descricao" sortable="false" title="Item da Fatura"/>
			
			<display:column property="tarifa.descricao" sortable="false" title="Tarifa"/>
		       		
			<display:column property="numeroDiaVencimento" sortable="false" title="Dia de Vencimento"/>		
			
			<display:column property="opcaoFaseReferencia.descricao" sortable="false" title="Op��o"/>
			
			<display:column property="faseReferencia.descricao" sortable="false" title="Ap�s"/>
									
			<display:column title="Venc. em<br />dia n�o �til" style="width: 70px">
				<c:choose>
				<c:when test="${itemVencimento.vencimentoDiaUtil eq true}">Sim</c:when>
					<c:otherwise>N�o</c:otherwise>
				</c:choose>
			</display:column>
				
		</display:table>
	</fieldset>

</fieldset>
</fieldset>
</div>
</form:form>
