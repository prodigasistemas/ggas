<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% pageContext.setAttribute("newLineChar", " \r\n "); %>

<input id="url_imagens" type="hidden" value="<c:url value="/imagens/"/>"/>

<link type="text/css" rel="stylesheet"
      href="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/incluirChamado/index.css"/>

<input type="hidden" id="informacaoAdicionalInfo" value="${fn:replace(chamado.informacaoAdicional,newLineChar,' ')}"/>

<div class="bootstrap">
    <form:form modelAttribute="Chamado" method="post" id="chamadoForm" name="chamadoForm" enctype="multipart/form-data">

        <input type="hidden" value="<c:url value="/imagens/calendario.png"/>" id="imagemCalendario"/>

        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Encerramento de Chamado em Lote</h5>
            </div>
            <div class="card-body">

                <div class="alert alert-primary" role="alert">
                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                    <div class="d-inline">
                        Informe os dados abaixo e clique em <b>Encerrar</b>
                    </div>

                </div>

                <div class="row mb-2">
                    <div class="col-md-12 mt-1">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="descricao">Descri��o: <span class="text-danger">*</span></label>
                                <textarea class="form-control form-control-sm" id="descricao"
                                          name="descricao" placeholder="Descri��o" cols="35" rows="6"
                                          onblur="this.value=removerEspacoInicialFinal(this.value);">${chamado.descricao}</textarea>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="idMotivo">Motivo: <span class="text-danger">*</span></label>
                                <select id="idMotivo" class="form-control form-control-sm" name="idMotivo">
                                    <option value="-1">Selecione</option>
                                    <c:forEach items="${listaMotivos}" var="motivo">
                                        <option value="<c:out value="${motivo.chavePrimaria}"/>"
                                                <c:if test="${idMotivo == motivo.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${motivo.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="dataResolucao">Data de Resolu��o: <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-sm" id="dataResolucao"
                                           name="dataResolucao" placeholder="Data de Resolu��o"
                                           maxlength="16" size="16"
                                           value="<fmt:formatDate value="${chamado.dataResolucao}" pattern="dd/MM/yyyy HH:mm"/>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row mb-2">

                    <div class="col-md-12 mt-1">
                        <h5>Chamados que ser�o encerrados</h5>
                    </div>
                    <div class="col-md-12 mt-1">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="table-chamados" width="100%">
                                <thead class="thead-ggas-bootstrap">
                                <tr>
                                    <th scope="col" class="text-center">Protocolo</th>
                                    <th scope="col" class="text-center">Previs�o de Encerramento</th>
                                    <th scope="col" class="text-center">Solicitante</th>
                                    <th scope="col" class="text-center">Cliente</th>
                                    <th scope="col" class="text-center">Tipo do Chamado</th>
                                    <th scope="col" class="text-center">Assunto do Chamado</th>
                                    <th scope="col" class="text-center">Status</th>
                                    <th scope="col" class="text-center">A.S. Aberta</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${chamados}" var="chamado">
                                    <tr id="chamado${chamado.chavePrimaria}">
                                        <td class="text-center"
                                            data-chave-primaria="${chamado.chavePrimaria}">${chamado.protocolo.numeroProtocolo}</td>
                                        <td class="text-center"><fmt:formatDate value="${chamado.dataPrevisaoEncerramento}"
                                                                                pattern="dd/MM/yyyy: HH:mm"/></td>
                                        <td class="text-left"><c:out value="${chamado.nomeSolicitante}"/></td>
                                        <td class="text-left"><c:out value="${chamado.cliente.nome}"/></td>
                                        <td class="text-left"><c:out value="${chamado.chamadoAssunto.chamadoTipo.descricao}"/></td>
                                        <td class="text-left"><c:out value="${chamado.chamadoAssunto.descricao}"/></td>
                                        <td class="text-center" id="status${chamado.chavePrimaria}"><c:out
                                                value="${chamado.status.descricao}"/></td>
                                        <td class="text-center">
                                            <c:choose>
                                                <c:when test="${chamado.possuiASAberta}">
                                                    <i class="fa fa-check-circle text-success"></i> Sim
                                                </c:when>
                                                <c:otherwise>
                                                    <i class="fa fa-times-circle text-danger"></i> N�o
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>

            </div>

            <div class="card-footer">
                <div class="form-row mt-2">
                    <div class="col-md-12">

                        <div class="row justify-content-between">
                            <div class="col-md-6 col-sm-12 mt-1">
                                <button id="cancelar" type="button" class="btn btn-default btn-sm mb-1 mr-1">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </button>

                                <button id="voltar" type="button" class="btn btn-danger btn-sm mb-1 mr-1">
                                    <i class="fa fa-times"></i> Limpar
                                </button>
                            </div>

                            <vacess:vacess param="encerrarChamado">
                                <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                    <button name="buttonEncerrar" id="botaoEncerrar"
                                            class="btn btn-primary btn-sm mb-1 mr-1" type="button">
                                        <i class="fa fa-check-double"></i> Encerrar
                                    </button>
                                </div>
                            </vacess:vacess>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form:form>

    <jsp:include page="../../cadastro/cliente/bootstrap/modalPesquisarCliente.jsp"/>

    <div id="modal-unidade-podem-visualizar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Unidades que podem visualizar esse chamado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul id="ul-unidades-podem-visualizar" class="list-group list-group-flush">

                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/encerrarEmLote/index.js"></script>
