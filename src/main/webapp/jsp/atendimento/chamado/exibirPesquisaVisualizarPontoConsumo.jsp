<%--
  ~ Copyright (C) <2019> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2019 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<div class="card">
	<div class="card-header">
		<h5 class="card-title">Visualizar Informa��es Ponto de Consumo</h5>
	</div>
	<div class="card-body">
		<div class="alert alert-primary" role="alert">
			<p><i class="fa fa-info-circle" aria-hidden="true"></i> Selecione pelo menos um cliente clicando em 
				<span class="destaqueOrientacaoInicial">Pesquisar Cliente</span>, 
				e/ou im�vel clicando em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span>,
				e/ou algum dos campos abaixo e clique no bot�o <span class="destaqueOrientacaoInicial">Pontos de Consumo</span>.
		</div>


<script type="text/javascript">

	$(document).ready(function(){

		$("#botaoPesquisaCliente").css("background-color", "");
		$("#botaoPesquisaCliente").css("border", "");
		 
	});

	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('nomeFantasiaImovel').value = "";
       	document.getElementById('matriculaImovel').value = "";
       	document.getElementById('numeroImovel').value = "";
       	document.getElementById('cidadeImovel').value = "";
       	document.getElementById('condominio').value = "";
       	
       	
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeImovel').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('idPontoConsumo').value = "";
		
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		// document.getElementById('situacaoContrato').value = "-1";
		
	}

	function limparCampoPesquisa(ele) {
		limparCamposPesquisa();
		desativarBotoes();
	}

	function exibirPopupPesquisaImovel() {
		popup = window
				.open(
						'exibirPesquisaImovelCompletoPopup?postBack=true',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");

		if (idSelecionado != '') {
			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {
										idImovel.value = imovel["chavePrimaria"];
										matriculaImovel.value = imovel["chavePrimaria"];
										nomeFantasia.value = imovel["nomeFantasia"];
										numeroImovel.value = imovel["numeroImovel"];
										cidadeImovel.value = imovel["cidadeImovel"];
										indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
										enderecoImovel.value = imovel["enderecoImovel"];
									}
								},
								async : false
							}

					);
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
			enderecoImovel.value = "";
		}

		document.getElementById("nomeImovel").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if (indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

		ativarBotoes();

		pesquisarPontoConsumo();

	}

	function ativarBotoes() {
		document.getElementById("botaoPontoConsumo").disabled = false;
	}

	function init() {

		var idCliente = "${faturaForm.idCliente}";
		var idImovel = "${faturaForm.idImovel}";
		if ((idCliente != undefined && idCliente > 0)) {
			ativarBotoes()
		} else if (idImovel != undefined && idImovel > 0) {
			document.getElementById("botaoPesquisar").disabled = false;
			document.getElementById("botaoPontoConsumo").disabled = false;
		}

	}

	addLoadEvent(init);


	function pesquisarPontoConsumo() {

		exibirIndicador();
		
		submeter(
				'chamadoForm',
				'pesquisaPontoConsumoChamado');
	}
	
	function visualizarPontoConsumo(idPontoConsumo){
		exibirIndicador();
		document.forms['chamadoForm'].idPontoConsumo.value = idPontoConsumo;
		submeter('chamadoForm','visualizarInformacoesPontoConsumo');
	}

	function desativarBotoes() {
		document.getElementById("botaoPontoConsumo").disabled = true;
	}
	
</script>

<form:form method="post" action="pesquisaPontoConsumoChamado" name="chamadoForm">
	<input name="acao" type="hidden" id="acao" value="pesquisaPontoConsumoChamado">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${chamadoForm.idPontoConsumo}">
		
	<fieldset id="pesquisarFatura" class="conteinerPesquisarIncluirAlterar">
		<div class="container-fluid">
			<div class="row">
			
				<div class="col-md-6">
							<jsp:include page="/jsp/faturamento/fatura/pesquisaCliente.jsp">
								<jsp:param name="idCampoIdCliente" value="idCliente"/>
								<jsp:param name="idCliente" value="${chamadoForm.idCliente}"/>
								<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
								<jsp:param name="nomeCliente" value="${cliente.nome}"/>
								<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
								<jsp:param name="documentoFormatadoCliente" value="${cliente.numeroDocumentoFormatado}"/>
								<jsp:param name="idCampoEmail" value="emailCliente"/>
								<jsp:param name="emailCliente" value="${cliente.emailPrincipal}"/>
								<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
								<jsp:param name="enderecoFormatadoCliente" value="${cliente.enderecoPrincipal.enderecoFormatado}"/>
								<jsp:param name="tipoCliente" value="${cliente.tipoCliente.descricao}"/>
								<jsp:param name="idTipoCliente" value="tipoCliente"/>
								<jsp:param name="possuiRadio" value="true"/>
								<jsp:param name="funcaoParametro" value="ativarBotoes"/>
								<jsp:param name="telaVisualizarPontoConsumo" value="true"/>
								
							</jsp:include>
				</div>
				
				<div class ="col-md-6">
				
					<div class="card">
						<div class="card-header">
							<h5>Pesquisar Im�vel</h5>
						</div>
						<div class="alert alert-primary" role="alert">
							<p><i class="fa fa-info-circle" aria-hidden="true"></i> Clique em <span class="destaqueOrientacaoInicial">Pesquisar Im�vel</span> para selecionar um Im�vel.</p>
						</div>
						<div class="card-body">
							<input name="idImovel" type="hidden" id="idImovel" value="${chamadoForm.idImovel}">
							<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovel.nome}">
							<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovel.chavePrimaria}">
							<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovel.numeroImovel}">
							<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovel.quadraFace.endereco.cep.nomeMunicipio}">
							<input name="condominio" type="hidden" id="condominio" value="${imovel.condominio}">
							<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${imovel.modalidadeMedicaoImovel.codigo}">
							<input name="telaVisualizarPontoConsumo" type="hidden" id="telaVisualizarPontoConsumo" value="true">
													
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloNomeFantasia" for="nomeImovel">Descri��o:</label>
									<input class="form-control campoDesabilitado" type="text" id="nomeImovel" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${imovel.nome}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
									<input class="form-control campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.chavePrimaria}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
									<input class="form-control campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.numeroImovel}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<div class="col-md-12">
									<label class="text-right rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
									<input class="form-control campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovel.quadraFace.endereco.cep.nomeMunicipio}"><br />
								</div>
							</div>
							
							<div class="form-row">
								<label class="text-right rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioImovelTexto">O im�vel � condom�nio:</label>
								<div class="col-md-12">
									<div class="custom-control custom-radio custom-control-inline">
										<input class="custom-control-input" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovel.condominio == 'true'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="indicadorCondominioImovelTexto1">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input class="custom-control-input" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovel.condominio == 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="indicadorCondominioImovelTexto2">N�o</label>
									</div>
								</div>
							</div>
							
							<div class="row mt-3">
								<div class="col align-self-center text-center">
									<button type="button" class="btn btn-primary " onclick="exibirPopupPesquisaImovel();">Pesquisar Im�vel</button>
								</div>
							</div>
														
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
		
		</br>
		
		<div class="row mt-3">
				
			<div class="col align-self-end text-right">
			
				<button class="btn btn-primary btn-sm" id="botaoPontoConsumo"
						type="button" onclick="pesquisarPontoConsumo();" disabled="disabled">
						Pontos de Consumo
				</button>
				
				<button class="btn btn-secondary btn-sm" id="botaoLimpar"
					type="button" onclick="limparCampoPesquisa(this.form);">
					<i class="fa fa-times"></i> Limpar
				</button>
				
			</div>
		</div>

	
		<c:if test="${listaPontoConsumo ne null}">
			<hr class="linhaSeparadora1" />
			<fieldset class="conteinerBloco">
				<div class="alert alert-primary" role="alert">
					<p class="orientacaoInicial">Clique em um Ponto de Consumo na lista abaixo para exibir as suas informa��es. </p>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="pontoConsumo">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">Ponto de Consumo</th>
								<th scope="col" class="text-center">C�digo do Ponto de Consumo</th>
								<th scope="col" class="text-center">Segmento</th>
								<th scope="col" class="text-center">Situa��o</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaPontoConsumo}" var="pontoConsumo">
								<tr>
									<td class="text-center">
										<a href="javascript:visualizarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.descricao} - ${pontoConsumo.imovel.nome}"/>
							            </a>
									</td>
									<td class="text-center">
										<a href="javascript:visualizarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.codigoPontoConsumo}"/>
							            </a>
									</td>
									<td class="text-center">
										<a href="javascript:visualizarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.segmento.descricao}"/>
							            </a>
									</td>
									<td class="text-center">
										<a href="javascript:visualizarPontoConsumo(<c:out value='${pontoConsumo.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							            	<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/>
							            </a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</fieldset>
		
		</c:if>

		
	</fieldset>
	
</form:form>

	</div>
</div>