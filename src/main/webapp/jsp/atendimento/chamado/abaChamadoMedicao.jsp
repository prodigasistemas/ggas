<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script type="text/javascript">
	$(document).ready(function(){
		var opt = {
				autoOpen: false,			
				width: 800,
				modal: true,
				minHeight: 120,
				resizable: false
		};

		$("a[rel=modal]").click( function(ev){
			 $("#fotoColetorPopup").dialog(opt).dialog("open");
	    });
	});

	function selecionarFoto(idRota, anoMesFaturamento, leituraMovimentoId){
		var idRota = idRota;
		var anoMesFaturamento = anoMesFaturamento;
		var leituraMovimentoId = leituraMovimentoId;
	
		$("#fotoColetorPopup").html("");
		if(idRota != '' || anoMesFaturamento != '') {
			AjaxService.obterFotoPorIdDadosMedicao(idRota, anoMesFaturamento, leituraMovimentoId, {
				callback: function(urlFoto) {
					 var inner = '';
					 inner = inner +'<center>';
					 inner = inner + '<img src="<c:url value="' + urlFoto + '"/>" style=" width:100%;">';
					 inner = inner +'</center>';
					 $("#fotoColetorPopup").append(inner);
				}, async:false}
	        );	 
		}  
	}


	function gerarGrafico() {
		var chavePontoConsumo = <c:out value="${pontoConsumo.chavePrimaria}"/>	

			console.log(chavePontoConsumo);

		popup = window.open('exibirGraficoVolumeGSA?chavePontoConsumo=' + chavePontoConsumo, 'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

	}
</script>


<div id="divMeicao">
<fieldset class="conteinerDados3">
	<legend>Medidor</legend>
<!-- 	<div class="conteinerDados"> -->
	<fieldset class="detalhamento">
		<fieldset id="funcionarioCol2" class="coluna itemDetalhamentoLargo3">
			<label class="rotulo">N�mero de S�rie:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.numeroSerie}"/></span><br />
			<label class="rotulo">Modelo:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.modelo.descricao}"/></span><br />
			<label class="rotulo">Marca:</label>
			<span class="itemDetalhamento"><c:out value="${medidor.marcaMedidor.descricao}"/></span><br />				
			
		</fieldset>	
	
		<fieldset id="funcionarioCol2" class="colunaFinal">				
			<label class="rotulo">Data de Ativa��o:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${pontoConsumo.instalacaoMedidor.dataAtivacao}" pattern="dd/MM/yyyy"/></span><br />				
			<label class="rotulo">Leitura de Ativa��o:</label>
			<span class="itemDetalhamento"><c:out value="${pontoConsumo.instalacaoMedidor.leituraAtivacao}"/></span><br />
			<label class="rotulo">Data de Instala��o:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${dataInstalacaoMedidor}" pattern="dd/MM/yyyy"/></span><br />
			<label class="rotulo">Leitura de Instala��o:</label>
			<span class="itemDetalhamento"><c:out value="${leituraInstalacaoMedidor}"/></span><br />
		</fieldset>
	</fieldset>
	
	<fieldset id="instalacaoMedidor" class="conteinerBloco">
			<a name="ancoraInstalacaoMedidor"></a>
			<legend class="conteinerBlocoTitulo">Hist�rico de Medidor</legend>

			<display:table id="historicoOperacaoMedidor" class="dataTableGGAS" export="false" name="listaHistoricoOperacaoMedidor" sort="list" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#informacaoPontoConsumoAbaMedicao">
	
			    <display:column media="html" title="Data">
		 			<fmt:formatDate value="${historicoOperacaoMedidor.dataRealizada}" type="both" pattern="dd/MM/yyyy" />
			    </display:column>

			    <display:column media="html" title="Opera��o">
			    	<c:out value='${historicoOperacaoMedidor.operacaoMedidor.descricao}'/>
		    	</display:column>
			    
			    <display:column media="html" title="Leitura">
		 			<c:out value='${historicoOperacaoMedidor.numeroLeitura}'/>
			    </display:column>
			    
			    <display:column media="html" title="Motivo">
		 			<c:out value='${historicoOperacaoMedidor.motivoOperacaoMedidor.descricao}'/>
			    </display:column>
		    	
		    </display:table>
		</fieldset>
	
<!-- 	</div> -->
</fieldset>
</div>

</br>

<div id="divCorretorVazao">
<%-- <fieldset class="conteinerDados3">
	<legend>Corretor de Vaz�o</legend>
<!-- 	<div class="conteinerDados"> -->
		<fieldset class="detalhamento">
		<fieldset id="funcionarioCol2" class="coluna itemDetalhamentoLargo3">
			<label class="rotulo">N�mero de S�rie:</label>
			<span class="itemDetalhamento"><c:out value="${vazaoCorretor.numeroSerie}"/></span><br />
			<label class="rotulo">Modelo:</label>
			<span class="itemDetalhamento"><c:out value="${vazaoCorretor.modelo.descricao}"/></span><br />
			<label class="rotulo">Marcar:</label>
			<span class="itemDetalhamento"><c:out value="${vazaoCorretor.marcaCorretor.descricao}"/></span><br />				
			
		</fieldset>	
	
		<fieldset id="funcionarioCol2" class="colunaFinal">			
			<label class="rotulo">Data de Instala��o:</label>
			<span class="itemDetalhamento"><fmt:formatDate value="${dataInstalacaoCorretorVazao}" pattern="dd/MM/yyyy"/></span><br />						
			<label class="rotulo">Leitura de Instala��o:</label>
			<span class="itemDetalhamento"><c:out value="${leituraInstalacaoCorretorVazao}"/></span><br />
		</fieldset>
	</fieldset>
	
	<fieldset id="instalacaoMedidor" class="conteinerBloco">
		<a name="ancoraInstalacaoMedidor"></a>
		<legend class="conteinerBlocoTitulo">Hist�rico Corretor de Vaz�o</legend>
		<display:table id="historicoCorretor" class="dataTableGGAS" export="false" name="listaVazaoCorretorHistorico" sort="list" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#informacaoPontoConsumoAbaMedicao">
			
			<display:column media="html" title="Data">
				<fmt:formatDate value="${historicoCorretor.dataRealizada}" pattern="dd/MM/yyyy"/>
			</display:column>
			
			<display:column media="html" title="Opera��o">
				<c:out value='${historicoCorretor.operacaoMedidor.descricao}'/>
			</display:column>
			
			<display:column media="html" title="Leitura">
				<c:out value='${historicoCorretor.leitura}'/>
			</display:column>
			
			<display:column media="html" title="Motivo Opera��o">
				<c:out value='${historicoCorretor.motivoOperacaoMedidor.descricao}'/>
			
			</display:column>
		</display:table>
	</fieldset>
	
<!-- 	</div> -->
	
</fieldset> --%>

<hr class="linhaSeparadora1" />	

<fieldset class="conteinerBloco">
<%-- 				<legend class="conteinerBlocoTitulo">Hist�rico de Consumo</legend>
				
				<display:table  class="dataTableGGAS dataTableCabecalho2Linhas" export="false" name="listaHistoricoConsumo" id="historicoConsumo" sort="list" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#informacaoPontoConsumoAbaMedicao">
					
					<display:column media="html" title="M�s / Ano-Ciclo" style="width: auto;">
						<c:out value='${historicoConsumo.anoMesFaturamentoFormatado}'/>-<c:out value='${historicoConsumo.numeroCiclo}'/>
					</display:column>
					
					<display:column media="html" title="Data Leitura Anterior" style="width: auto;">
						<fmt:formatDate value="${historicoConsumo.dataLeituraAnterior}" pattern="dd/MM/yyyy"/>
					</display:column>
					
					<display:column media="html" title="Leitura Anterior">
					 	<fmt:formatNumber value="${historicoConsumo.numeroLeituraAnterior}" type="number"/>
					</display:column>
					
					<display:column media="html" title="Data Leitura Atual" style="width: auto;">
						<fmt:formatDate value="${historicoConsumo.dataLeituraInformada}" pattern="dd/MM/yyyy"/>
					</display:column>	
					
					<display:column media="html" title="Leitura Atual">
					 	<fmt:formatNumber value="${historicoConsumo.numeroLeituraInformada}" type="number"/>
					</display:column>
				    
					<display:column media="html" title="Consumo (m<span class='expoente'>3</span>)">
						<fmt:formatNumber value="${historicoConsumo.consumo}" type="number" maxFractionDigits="4"/>
					</display:column>		  
					
					<display:column media="html" title="Dias de Consumo">
						<c:out value='${historicoConsumo.diasConsumo}'/>
					</display:column>  
					
					<display:column sortable="false" title="Fator PTZ" >
						<fmt:formatNumber value="${historicoConsumo.fatorPTZ}" type="number" maxFractionDigits="4"/>
					</display:column>
					
					<display:column media="html" title="Consumo Medido (m<span class='expoente'>3</span>)">
						<fmt:formatNumber value="${historicoConsumo.consumoMedido}" type="number" maxFractionDigits="4"/>
					</display:column>							
					
					<display:column sortable="false" title="Fator PCS" >
						<fmt:formatNumber value="${historicoConsumo.fatorPCS}" type="number" maxFractionDigits="4"/>
					</display:column>
					
					<display:column sortable="false" title="Fator de Corre��o" >
						<fmt:formatNumber value="${historicoConsumo.fatorCorrecao}" type="number" maxFractionDigits="4"/>
					</display:column>	
					
					<display:column media="html" title="Tipo de Consumo">
						<c:out value='${historicoConsumo.descricaoTipoConsumo}'/>
					</display:column>
					
					<c:if test="${isColetorDados eq 'true'}">
						<display:column sortable="false" title="Foto Coletor">
							<c:if test='${historicoConsumo.urlFoto ne null}'>
								<a id="fotoColetor" rel="modal" onclick="selecionarFoto(${historicoConsumo.idRota}, ${historicoConsumo.anoMesFaturamento}, ${historicoConsumo.idLeituraMovimento})">
									<img id="fotoColetor" src="<c:url value="/imagens/images.png"/>" border="0" style="width:40px; height:40px;" >
								</a>
								<div class="window" id="fotoColetorPopup"  title="Foto Coletor" style="display :none;">
								</div>
							</c:if>
						</display:column>
					</c:if>
					
					
				</display:table> --%>
				
				<legend class="conteinerBlocoTitulo">Hist�rico de Leitura (GSA)</legend>
				
				<display:table  class="dataTableGGAS dataTableCabecalho2Linhas" export="false" name="listaHistoricoLeituraGSA" id="historicoLeituraGSA" sort="list" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#informacaoPontoConsumoAbaMedicao">
				
					<display:column media="html" title="Data da Leitura Anterior">
						<c:out value='${historicoLeituraGSA.dataLeituraAnterior}'/>
					</display:column>
					
					<display:column media="html" title="Leitura Anterior">
						<c:out value='${historicoLeituraGSA.leituraAnterior}'/>
					</display:column> 		
								
					<display:column media="html" title="Data da Leitura Atual">
						<c:out value='${historicoLeituraGSA.dataLeituraAtual}'/>
					</display:column>  
														
					<display:column media="html" title="Leitura Atual">
						<c:out value='${historicoLeituraGSA.leituraAtual}'/>
					</display:column>  
					
					<display:column media="html" title="Volume Medido">
						<c:out value='${historicoLeituraGSA.volumeMedido}'/>
					</display:column>  
					
					<display:column media="html" title="Volume Faturado">
						<c:out value='${historicoLeituraGSA.volumeFaturado}'/>
					</display:column>
					
					<display:column media="html" title="Tipo de Medi��o">
						<c:out value='${historicoLeituraGSA.tipoMedicaoManual}'/>
					</display:column>  						  										  															
					
				</display:table>	
				</br>			
				<input style="text-align: center"  id="botaoGerarGraficoMedicao" name="botaoGerarGraficoMedicao" class="bottonRightCol2 bottonRightColUltimo" value="Gerar Gr�fico Volume" onclick="gerarGrafico(); exibirIndicador();">
			
			</fieldset>
</div>