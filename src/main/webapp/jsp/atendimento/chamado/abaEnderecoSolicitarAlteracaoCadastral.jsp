<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<script>

function alterarAnexoAbaEndereco(form, actionAdicionarAnexoAbaEndereco) {
	submeter('clienteForm', 'adicionarAnexoAbaEndereco');
}

function incluirAnexoAbaEndereco(form, actionAdicionarAnexoAbaEndereco) {
	document.forms[0].indexListaAnexoAbaEndereco.value = -1;
	submeter('clienteForm', 'adicionarAnexoAbaEndereco');
}

function removerAnexoAbaEndereco(actionRemoverAnexoAbaEndereco, indice) {
	document.forms[0].indexListaAnexoAbaEndereco.value = indice;
	submeter('clienteForm', 'removerAnexoAbaEndereco?operacaoLista=true');
}

function limparAnexoAbaEndereco() {
	document.forms[0].indexListaAnexoAbaEndereco.value = -1;
	document.forms[0].idTipoAnexo.selectedIndex = 0;
	document.forms[0].descricaoAnexo.value = "";
	
	var botaoAlterarAnexoAbaEndereco = document.getElementById("botaoAlterarAnexoAbaEndereco");
	var botaoLimparAnexoAbaEndereco = document.getElementById("botaoLimparAnexoAbaEndereco");	
	var botaoIncluirAnexoAbaEndereco = document.getElementById("botaoIncluirAnexoAbaEndereco");	
	var botaoVisualizarAnexo = document.getElementById("botaoVisualizarAnexo");
	
	botaoAlterarAnexoAbaEndereco.disabled = true;
	botaoLimparAnexoAbaEndereco.disabled = false;
	botaoIncluirAnexoAbaEndereco.disabled = false;
	botaoVisualizarAnexo.disabled = true;

}

function alterarEndereco(form, actionAdicionarEndereco) {
	submeter('clienteForm', 'adicionarEnderecoDoCliente#clienteAbaEndereco');
}

function incluirEndereco(form, actionAdicionarEndereco) {
	document.forms[0].indexLista.value = -1;
	submeter('clienteForm', 'adicionarEnderecoDoCliente#clienteAbaEndereco');
}

function removerEndereco(actionRemoverEndereco, indice) {
	document.forms[0].indexLista.value = indice;
	submeter('clienteForm', 'removerEnderecoDoCliente?operacaoLista=true#clienteAbaEndereco');
}

function atualizarCorrespondencia(actionAtualizarCorrespondencia, indice) {
	document.forms[0].indexLista.value = indice;
	submeter('clienteForm', 'atualizarCorrespondenciaDoCliente?operacaoLista=true#clienteAbaEndereco');
}

function limparEndereco() {
	document.forms[0].indexLista.value = -1;
	document.forms[0].idTipoEndereco.selectedIndex = 0;
	document.forms[0].cep.value = "";
	document.forms[0].chaveCep.value = "";
	document.forms[0].numeroEndereco.value = "";
	document.forms[0].complementoEndereco.value = "";
	document.forms[0].referenciaEndereco.value = "";
	document.forms[0].caixaPostal.value = "";
	document.forms[0].indexLista.disabled = false;
	document.forms[0].idTipoEndereco.disabled = false;
	document.forms[0].cep.disabled = false;
	document.forms[0].numeroEndereco.disabled = false;
	document.forms[0].complementoEndereco.disabled = false;
	document.forms[0].referenciaEndereco.disabled = false;
	document.forms[0].caixaPostal.disabled = false;
	var selectCep = document.getElementById("ceps"+'${param.idCampoCep}');
	removeAllOptions(selectCep);
	animatedcollapse.hide('pesquisarCep<c:out value='${param.idCampoCep}' default=""/>');
	limparDialog();	
	
	var botaoAlterarEndereco = document.getElementById("botaoAlterarEndereco");
	var botaoLimparEndereco = document.getElementById("botaoLimparEndereco");	
	var botaoIncluirEndereco = document.getElementById("botaoIncluirEndereco");	
	
	botaoAlterarEndereco.disabled = true;
	botaoLimparEndereco.disabled = false;
	botaoIncluirEndereco.disabled = false;

}

function exibirAlteracaoDoEndereco(indice,idTipoEndereco,cep,numeroEndereco,complementoEndereco,referenciaEndereco,caixaPostal,chaveCep,chavePrimariaEndereco,indexListaCliente,idTipoEnderecoCliente,cepCliente,numeroEnderecoCliente,complementoEnderecoCliente,referenciaEnderecoCliente,caixaPostalCliente,chaveCepCliente,chavePrimariaClienteEndereco, tipoAcao, codigoInclusao, codigoAlteracao) {
	if (indice != "") {
		document.forms[0].indexLista.value = indice;
		if (idTipoEndereco != "") {
			var tamanho = document.forms[0].tipoEndereco.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].tipoEndereco.options[i];
				if (idTipoEndereco == opcao.value) {
					opcao.selected = true;
				}
			}
			
		}
		if (cep != "") {
			document.forms[0].valorCep.value = cep;
			document.forms[0].cep.value = chaveCep;
			
		} else {
			document.forms[0].cep.value = "";
			document.forms[0].chaveCep.value = "";
			
		}
		if (numeroEndereco != "") {
			document.forms[0].numero.value = numeroEndereco;
			
		} else {
			document.forms[0].numero.value = '';
			
		}
		if (complementoEndereco != "") {
			document.forms[0].complemento.value = complementoEndereco;
			
		}else{
			document.forms[0].complemento.value = "";
			
		}
		if (referenciaEndereco != "") {
			document.forms[0].enderecoReferencia.value = referenciaEndereco;
			
		} else {
			document.forms[0].enderecoReferencia.value = "";
			
		}
		if (caixaPostal != "") {
			document.forms[0].caixaPostal.value = caixaPostal;
			
		}else{
			document.forms[0].caixaPostal.value = "";
			
		}
		
		exibirAlteracaoDoEnderecoCliente(indexListaCliente,idTipoEnderecoCliente,cepCliente,numeroEnderecoCliente,complementoEnderecoCliente,referenciaEnderecoCliente,caixaPostalCliente,chaveCepCliente,chavePrimariaClienteEndereco);
		preencherCheckMapa(chavePrimariaClienteEndereco);
		
		if(tipoAcao != "" && tipoAcao == codigoInclusao){
			
			document.forms[0].tipoEnderecoCheckboxEndereco.checked = true;
			document.forms[0].tipoEnderecoCheckboxEndereco.disabled = true;
			
			document.forms[0].cepCheckboxEndereco.checked = true;
			document.forms[0].cepCheckboxEndereco.disabled = true;
			
			document.forms[0].numeroCheckboxEndereco.checked = true;
			document.forms[0].numeroCheckboxEndereco.disabled = true;
			
		}
		
	}
}

function preencherCheckMapa(chavePrimariaClienteEndereco){
	
	$("input:checkbox[name$='CheckboxEndereco']").attr("checked",false);
	$("input:checkbox[name$='CheckboxEndereco']").attr("disabled",false)
	
	var url = "retornarCheckMapa?tipoMapa=Endereco" + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		}).responseText;
	
	retorno = retorno.substring(1,retorno.length-1).split("],");
	
	for(j = 0 ; j < retorno.length ; j++){
		var chaveAux = retorno[j].split(":");
		var checkBoxMarcadosListaMapa = new Object();
		
		for(i = 0 ; i < chaveAux.length/2 ;){
			
			chaveAux[i] = chaveAux[i].substring(1,chaveAux[i].length-1);
			
			checkBoxMarcadosListaMapa[chaveAux[i]] = chaveAux[++i];
			++i;
		}
		
	      if(checkBoxMarcadosListaMapa != null && checkBoxMarcadosListaMapa != ""){
	    	  
	     		for (var chaveMapa in checkBoxMarcadosListaMapa){
	     			
	     			if(chaveMapa == chavePrimariaClienteEndereco){
	     				
	     				var listaCheckBoxMarcados = checkBoxMarcadosListaMapa[chaveMapa].split(",");
	     				
	     				for(i = 0 ; i < listaCheckBoxMarcados.length ; i++){
	     					
	     					var valorchaveMapaAux = listaCheckBoxMarcados[i].split("[");
	     					var valorchaveMapaAlterado;
	     					
	     					 if(valorchaveMapaAux.length == 2){
	     						valorchaveMapaAlterado = valorchaveMapaAux[1].split("]");
	     					 }else{
	     						valorchaveMapaAlterado = valorchaveMapaAux[0].split("]");
	     					 }
	     					
	     					
	        	               var valorchaveMapa = valorchaveMapaAlterado[0].split(",");

	        	               for(j = 0 ; j < valorchaveMapa.length ; j++){
	        	            	   
	        	            	   var valor = valorchaveMapa[j].replace(" ","");
	        	            	   valor = valor.replace("\"","").replace("\"","").concat("CheckboxEndereco"); 
	        	            	   
	        	            	   document.getElementById(valor).checked = true;
	        	               }
	     	     		}
	     			 }
	     			
	         	}
	          
	      }
	}
	
	
}

function exibirAlteracaoDoEnderecoCliente(indice,idTipoEndereco,cep,numeroEndereco,complementoEndereco,referenciaEndereco,caixaPostal,chaveCep,chavePrimariaClienteEndereco) {
	if (indice != "" && chavePrimariaClienteEndereco != "" || chavePrimariaClienteEndereco > 0) {
		document.forms[0].indexListaCliente.value = indice;
		if (idTipoEndereco != "") {
			var tamanho = document.forms[0].tipoEnderecoCliente.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].tipoEnderecoCliente.options[i];
				if (idTipoEndereco == opcao.value) {
					opcao.selected = true;
				}
			}
		}else{
			document.forms[0].tipoEnderecoCliente.value = "-1";
		}
		if (cep != "") {
			document.forms[0].cepCliente.value = cep;
		} else {
			document.forms[0].cepCliente.value = "";
		}
		if (numeroEndereco != "") {
			document.forms[0].numeroCliente.value = numeroEndereco;
		} else {
			document.forms[0].numeroCliente.value = '';
		}
		if (complementoEndereco != "") {
			document.forms[0].complementoCliente.value = complementoEndereco;
		}else{
			document.forms[0].complementoCliente.value = "";
		}
		if (referenciaEndereco != "") {
			document.forms[0].enderecoReferenciaCliente.value = referenciaEndereco;
		} else {
			document.forms[0].enderecoReferenciaCliente.value = "";
		}
		if (caixaPostal != "") {
			document.forms[0].caixaPostalCliente.value = caixaPostal;
		}else{
			document.forms[0].caixaPostalCliente.value = "";
		}
		
	}else{
			document.forms[0].indexListaCliente.value = -1;
			document.forms[0].tipoEnderecoCliente.selectedIndex = 0;
			document.forms[0].cepCliente.value = "";
			document.forms[0].numeroCliente.value = '';
			document.forms[0].complementoCliente.value = "";
			document.forms[0].enderecoReferenciaCliente.value = "";
			document.forms[0].caixaPostalCliente.value = "";
			
	}
	
	document.forms[0].chavePrimariaClienteEndereco.value = chavePrimariaClienteEndereco;
}

function exibirAlteracaoDoAnexo(indexListaAnexoAbaEndereco,chavePrimariaAnexo,descricaoAnexo,idTipoAnexo) {
	if (indexListaAnexoAbaEndereco != "") {
		document.forms[0].indexListaAnexoAbaEndereco.value = indexListaAnexoAbaEndereco;
		if (idTipoAnexo != "") {
			var tamanho = document.forms[0].idTipoAnexo.length;
			var opcao = undefined;
			for(var i = 0; i < tamanho; i++) {
				opcao = document.forms[0].idTipoAnexo.options[i];
				if (idTipoAnexo == opcao.value) {
					opcao.selected = true;
					break;
				}
			}
		}
		
		if (descricaoAnexo != "") {
			document.forms[0].descricaoAnexo.value = descricaoAnexo;
		}else{
			document.forms[0].descricaoAnexo.value = "";
		}
		
		if (chavePrimariaAnexo != ""){
			document.forms[0].chavePrimariaAnexo.value = chavePrimariaAnexo;
		}
		var botaoAlterarAnexoAbaEndereco = document.getElementById("botaoAlterarAnexoAbaEndereco");
		var botaoLimparAnexoAbaEndereco = document.getElementById("botaoLimparAnexoAbaEndereco");	
		var botaoIncluirAnexoAbaEndereco = document.getElementById("botaoIncluirAnexoAbaEndereco");
		var botaoVisualizarAnexo = document.getElementById("botaoVisualizarAnexo");
		
		botaoAlterarAnexoAbaEndereco.disabled = false;
		botaoLimparAnexoAbaEndereco.disabled = false;
		botaoIncluirAnexoAbaEndereco.disabled = true;
		botaoVisualizarAnexo.disabled = false;
	}
}

function visualizarAnexoAbaEndereco(indexListaAnexoAbaEndereco){
	
	submeter('chamadoForm','visualizarAnexoAbaEndereco?chavePrimaria='+indexListaAnexoAbaEndereco);
}

function visualizarAnexoAbaEnderecoCliente(indexListaAnexoAbaEndereco){
	
	submeter('chamadoForm','visualizarAnexoAbaEnderecoCliente?chavePrimaria='+indexListaAnexoAbaEndereco);
}

function preencherMapaListaAlteracaoCadastroClienteChamado(idCampo, valorChecked, valor, chavePrimariaClienteEndereco){
	
	var url = "preencherMapaListaAlteracaoCadastroClienteChamado?idCampo=" + idCampo + "&valorChecked="
	+ valorChecked + "&valor=" + valor + "&chavePrimariaClienteEndereco=" + chavePrimariaClienteEndereco + "&tipoMapa=Endereco" + "&posFixoTipoCheckbox=CheckboxEndereco"   + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		}).responseText;
	

}


function confimarAlteracaoLista(){
	
	var chavePrimariaClienteEndereco = document.forms[0].chavePrimariaClienteEndereco.value;
	
	var lista = $("input:checkbox[name$='CheckboxEndereco']");
	for(var i = 0; i < lista.length; i++) {
			
		var idCampo = lista[i].id;
		var idCampoFormatado = idCampo.split("CheckboxEndereco");
		
		preencherMapaListaAlteracaoCadastroClienteChamado(lista[i].id, lista[i].checked, document.getElementById(idCampoFormatado[0]).value, chavePrimariaClienteEndereco);
		document.getElementById(idCampoFormatado[0]).value = "";
		document.getElementById(idCampo).checked = false;
	}
	
	document.getElementById("valorCep").value = "";
	document.getElementById("tipoEndereco").value = "-1";
	
	alert("Altera��o realizada com sucesso.");
}

function preencherMapaListaAnexoAlteracaoCadastroClienteChamado(chavePrimariaAnexo, valorChecked){
	
	var url = "preencherMapaListaAnexoAlteracaoCadastroClienteChamado?chavePrimariaAnexo=" + chavePrimariaAnexo + "&valorChecked="
	+ valorChecked + "&noCache"
	var retorno = $.ajax({
		type: "GET",
          url: url,
          async: false,
		});
	
}




</script>

<div id="clienteAbaEndereco">
<fieldset class="conteinerDados3">
	<fieldset id="clienteEnderecoCol1" class="colunaEsq">
	<input type="hidden" name="indexLista" id="indexLista">
	<input type="hidden" name="chavePrimariaClienteEndereco" id="chavePrimariaClienteEndereco">
	<input type="hidden" name="cep" id="cep">
	
		<label class="rotulo campoObrigatorio" id="rotuloTipoEndereco" for="tipoEndereco"><span class="campoObrigatorioSimbolo">* </span>Tipo de Endere�o:</label>
		<select name="tipoEndereco" id="tipoEndereco" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTiposEndereco}" var="tipoEndereco">
				<option value="<c:out value="${tipoEndereco.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.tipoEndereco.chavePrimaria == tipoEndereco.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoEndereco.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<input class="solicitacaoAlteracaoTipoEnderecoCheckbox" type="checkbox" id="tipoEnderecoCheckboxEndereco" name="tipoEnderecoCheckboxEndereco"><br />
		
		<label class="rotulo" id="rotuloNumeroImovel" for="cep"><span class="campoObrigatorioSimbolo">* </span>Cep:</label>
		<input class="campoTexto" type="text" id="valorCep" name="valorCep" disabled="disabled" onkeypress="return formatarCampoNumeroEndereco(event, this);" maxlength="9" size="9" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.cep.value}"><br />
		<input class="solicitacaoAlteracaoCepCheckbox" type="checkbox" id="cepCheckboxEndereco" name="cepCheckboxEndereco"><br />
		
		<label class="rotulo" id="rotuloNumeroImovel" for="numero"><span class="campoObrigatorioSimbolo">* </span>N�mero:</label>
		<input class="campoTexto" type="text" id="numero" name="numero" disabled="disabled" onkeypress="return formatarCampoNumeroEndereco(event, this);" maxlength="5" size="3" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.numero}"><br />
		<input class="solicitacaoAlteracaoNumeroCheckbox" type="checkbox" id="numeroCheckboxEndereco" name="numeroCheckboxEndereco"  ><br />
		
		<label class="rotulo" id="rotuloComplemento" for="complemento">Complemento:</label>
		<input class="campoTexto" type="text" id="complemento" name="complemento" disabled="disabled" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="255" size="30" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.complemento}"><br />
		<input class="solicitacaoAlteracaoComplementoCheckbox" type="checkbox" id="complementoCheckboxEndereco" name="complementoCheckboxEndereco"  ><br />
		
		<label class="rotulo" for="referenciaEndereco">Refer�ncia:</label>
		<input class="campoTexto" type="text" id="enderecoReferencia" name="enderecoReferencia" disabled="disabled" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="30" size="30" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.enderecoReferencia}"><br />
		<input class="solicitacaoAlteracaoEnderecoReferenciaCheckbox" type="checkbox" id="enderecoReferenciaCheckboxEndereco" name="enderecoReferenciaCheckboxEndereco"  ><br />
		
		<label class="rotulo" id="rotuloCaixaPostal" for="caixaPostal">Caixa Postal:</label>
		<input class="campoTexto" type="text" id="caixaPostal" name="caixaPostal" disabled="disabled" maxlength="5" size="3" onkeypress="return formatarCampoInteiro(event);" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.caixaPostal}">
		<input class="solicitacaoAlteracaoCaixaPostalCheckbox" type="checkbox" id="caixaPostalCheckboxEndereco" name="caixaPostalCheckboxEndereco"  ><br />
		
		<br/><br/>
		<input class="bottonRightCol2 botaoGrande1 botaoIncluir" name="botaoConfirmar" id="botaoConfirmar" value="Confirmar" type="button" onclick="confimarAlteracaoLista();">
		
		<br/><br/>
		
	<c:set var="j" value="0" />
	<c:set var="i" value="0" />
	<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaChamadoAlteracaoClienteEndereco" sort="list" id="endereco" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		
		<display:column  sortable="false" title="Tipo" style="width: 110px">
			 <c:choose>
                    <c:when test="${(sessionScope.codigoInclusao eq endereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq endereco.tipoAcao.chavePrimaria)}">
                       <a href="javascript:exibirAlteracaoDoEndereco('${i}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}', '${j}','${endereco.clienteEndereco.tipoEndereco.chavePrimaria}','${endereco.clienteEndereco.cep.cep}','${endereco.clienteEndereco.numero}','${endereco.clienteEndereco.complemento}','${endereco.clienteEndereco.enderecoReferencia}','${endereco.clienteEndereco.caixaPostal}','${endereco.clienteEndereco.cep.chavePrimaria}','${endereco.clienteEndereco.chavePrimaria}', '${endereco.tipoAcao.chavePrimaria}', '${sessionScope.codigoInclusao}', '${sessionScope.codigoAlteracao}');">
							<c:out value="${endereco.tipoEndereco.descricao}"/>
						</a>
                    </c:when>
                    <c:otherwise>
                         <c:out value="${endereco.tipoEndereco.descricao}"/>
                    </c:otherwise>
                </c:choose>
		</display:column>
		
		<display:column sortable="false"  title="Cep" style="width: 80px">
	 		<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq endereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq endereco.tipoAcao.chavePrimaria)}">
                     <a href="javascript:exibirAlteracaoDoEndereco('${i}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}', '${j}','${endereco.clienteEndereco.tipoEndereco.chavePrimaria}','${endereco.clienteEndereco.cep.cep}','${endereco.clienteEndereco.numero}','${endereco.clienteEndereco.complemento}','${endereco.clienteEndereco.enderecoReferencia}','${endereco.clienteEndereco.caixaPostal}','${endereco.clienteEndereco.cep.chavePrimaria}','${endereco.clienteEndereco.chavePrimaria}', '${endereco.tipoAcao.chavePrimaria}', '${sessionScope.codigoInclusao}', '${sessionScope.codigoAlteracao}');">
					<c:out value="${endereco.cep.cep}"/>
				</a> 
                   </c:when>
                   <c:otherwise>
                       <c:out value="${endereco.cep.cep}"/>
                   </c:otherwise>
               </c:choose>
		</display:column>
		
		<display:column sortable="false"  title="Endere�o" style="padding: 0 5px" class="quebraLinhaTexto">
			<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq endereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq endereco.tipoAcao.chavePrimaria)}">
                     <a href="javascript:exibirAlteracaoDoEndereco('${i}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}', '${j}','${endereco.clienteEndereco.tipoEndereco.chavePrimaria}','${endereco.clienteEndereco.cep.cep}','${endereco.clienteEndereco.numero}','${endereco.clienteEndereco.complemento}','${endereco.clienteEndereco.enderecoReferencia}','${endereco.clienteEndereco.caixaPostal}','${endereco.clienteEndereco.cep.chavePrimaria}','${endereco.clienteEndereco.chavePrimaria}', '${endereco.tipoAcao.chavePrimaria}', '${sessionScope.codigoInclusao}', '${sessionScope.codigoAlteracao}');">
						<span style="width: 280px;"><c:out value="${endereco.cep.tipoLogradouro}"/>
							<c:out value="${endereco.cep.logradouro}"/>,&nbsp;
							<c:out value="${endereco.numero}"/>,&nbsp;
							<c:out value="${endereco.cep.bairro}"/>,&nbsp;
							<c:out value="${endereco.cep.nomeMunicipio}"/>&nbsp;-&nbsp;
							<c:out value="${endereco.cep.uf}"/>&nbsp;-&nbsp;
							<c:out value="${endereco.complemento}"/>
						</span>
					</a>
                   </c:when>
                   <c:otherwise>
                       <span style="width: 280px;"><c:out value="${endereco.cep.tipoLogradouro}"/>
							<c:out value="${endereco.cep.logradouro}"/>,&nbsp;
							<c:out value="${endereco.numero}"/>,&nbsp;
							<c:out value="${endereco.cep.bairro}"/>,&nbsp;
							<c:out value="${endereco.cep.nomeMunicipio}"/>&nbsp;-&nbsp;
							<c:out value="${endereco.cep.uf}"/>&nbsp;-&nbsp;
							<c:out value="${endereco.complemento}"/>
						</span>
                   </c:otherwise>
               </c:choose>
		</display:column>
		
		<display:column sortable="false"  title="A��o" style="width: 80px">
			<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq endereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq endereco.tipoAcao.chavePrimaria)}">
                     <a href="javascript:exibirAlteracaoDoEndereco('${i}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}', '${j}','${endereco.clienteEndereco.tipoEndereco.chavePrimaria}','${endereco.clienteEndereco.cep.cep}','${endereco.clienteEndereco.numero}','${endereco.clienteEndereco.complemento}','${endereco.clienteEndereco.enderecoReferencia}','${endereco.clienteEndereco.caixaPostal}','${endereco.clienteEndereco.cep.chavePrimaria}','${endereco.clienteEndereco.chavePrimaria}', '${endereco.tipoAcao.chavePrimaria}', '${sessionScope.codigoInclusao}', '${sessionScope.codigoAlteracao}');">
						<c:out value="${endereco.tipoAcao.descricao}"/>
					</a>
                   </c:when>
                   <c:otherwise>
                       <c:out value="${endereco.tipoAcao.descricao}"/>
                   </c:otherwise>
               </c:choose>
		</display:column>
		<c:set var="i" value="${i+1}" />
	</display:table>
	
	<c:set var="x" value="0" />
		<display:table class="dataTableGGAS dataTableAba" name="listaChamadoAlteracaoClienteEnderecoAnexoAbaEndereco" sort="list" id="anexoAbaEndereco" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			 <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	       		<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaEndereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaEndereco.tipoAcao.chavePrimaria)}">
						<input type="checkbox" name="chavesPrimarias" value="${anexoAbaEndereco.chavePrimaria}" onclick="preencherMapaListaAnexoAlteracaoCadastroClienteChamado(<c:out value='${anexoAbaEndereco.chavePrimaria}'/>, this.checked);">
                   </c:when>
                   <c:otherwise>
                       <input type="checkbox" name="chavesPrimarias" disabled="disabled" value="${anexoAbaEndereco.chavePrimaria}" onclick="preencherMapaListaAnexoAlteracaoCadastroClienteChamado(<c:out value='${anexoAbaEndereco.chavePrimaria}'/>, this.checked);">
                   </c:otherwise>
               </c:choose>
	        </display:column>
			
			<display:column sortable="false" title="Descri��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaEndereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaEndereco.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaEndereco(<c:out value='${anexoAbaEndereco.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaEndereco.descricaoAnexo}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaEndereco.descricaoAnexo}
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="Tipo" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaEndereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaEndereco.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaEndereco(<c:out value='${anexoAbaEndereco.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaEndereco.tipoAnexo.descricao}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaEndereco.tipoAnexo.descricao}
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="Data �ltima Altera��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaEndereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaEndereco.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaEndereco(<c:out value='${anexoAbaEndereco.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaEndereco.ultimaAlteracao}" var="formattedStatusDate" />
							<c:out value="${formattedStatusDate}"/>
							
							<fmt:formatDate pattern="HH:mm" value="${anexoAbaEndereco.ultimaAlteracao}" var="formattedStatusHour" />
							<c:out value="${formattedStatusHour}"/>
						</a>
                   </c:when>
                   <c:otherwise>
                      	 <fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaEndereco.ultimaAlteracao}" var="formattedStatusDate" />
							<c:out value="${formattedStatusDate}"/>
							
							<fmt:formatDate pattern="HH:mm" value="${anexoAbaEndereco.ultimaAlteracao}" var="formattedStatusHour" />
							<c:out value="${formattedStatusHour}"/>
                   </c:otherwise>
               </c:choose>
			</display:column>
			
			<display:column sortable="false" title="A��o" style="width: 80px">
				<c:choose>
                   <c:when test="${(sessionScope.codigoInclusao eq anexoAbaEndereco.tipoAcao.chavePrimaria) or (sessionScope.codigoAlteracao eq anexoAbaEndereco.tipoAcao.chavePrimaria)}">
						<a href='javascript:visualizarAnexoAbaEndereco(<c:out value='${anexoAbaEndereco.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
							${anexoAbaEndereco.tipoAcao.descricao}
						</a>
                   </c:when>
                   <c:otherwise>
                       ${anexoAbaEndereco.tipoAcao.descricao}
                   </c:otherwise>
               </c:choose>
				
			</display:column>
			
			<c:set var="x" value="${x+1}" />
		</display:table>
	
	
	
</fieldset>

	<fieldset >
	<input type="hidden" name="indexListaCliente" id="indexListaCliente">
	<fieldset id="clienteEnderecoCol1" class="colunaDir22">
		<label class="rotulo campoObrigatorio" id="rotuloTipoEnderecoCliente" for="tipoEnderecoCliente"><span class="campoObrigatorioSimbolo">* </span>Tipo de Endere�o:</label>
		<select name="tipoEnderecoCliente" id="tipoEnderecoCliente" disabled="disabled" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTiposEndereco}" var="tipoEndereco">
				<option value="<c:out value="${tipoEndereco.chavePrimaria}"/>" <c:if test="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.tipoEndereco.chavePrimaria == tipoEndereco.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoEndereco.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		
		<label class="rotulo" id="rotuloNumeroImovel" for="cepCliente"><span class="campoObrigatorioSimbolo">* </span>Cep:</label>
		<input class="campoTexto" type="text" id="cepCliente" name="cepCliente" disabled="disabled" onkeypress="return formatarCampoNumeroEndereco(event, this);" maxlength="5" size="9" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.cep.cep}"><br />
		
		<input type="hidden" id="chavePrimariaEndereco" name="chavePrimariaEndereco" />
		<label class="rotulo" id="rotuloNumeroImovel" for="numeroCliente"><span class="campoObrigatorioSimbolo">* </span>N�mero:</label>
		<input class="campoTexto" type="text" id="numeroCliente" name="numeroCliente" disabled="disabled" onkeypress="return formatarCampoNumeroEndereco(event, this);" maxlength="5" size="3" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.numero}"><br />
		
		<label class="rotulo" id="rotuloComplemento" for="complementoCliente">Complemento:</label>
		<input class="campoTexto" type="text" id="complementoCliente" name="complementoCliente" disabled="disabled" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="50" size="30" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.complemento}"><br />
		
		<label class="rotulo" for="referenciaEnderecoCliente">Refer�ncia:</label>
		<input class="campoTexto" type="text" id="enderecoReferenciaCliente" name="enderecoReferenciaCliente" disabled="disabled" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoTexto(event)');" maxlength="30" size="30" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.enderecoReferencia}"><br />
		
		<label class="rotulo" id="rotuloCaixaPostal" for="caixaPostalCliente">Caixa Postal:</label>
		<input class="campoTexto" type="text" id="caixaPostalCliente" name="caixaPostalCliente" disabled="disabled" maxlength="5" size="3" onkeypress="return formatarCampoInteiro(event);" value="${chamadoAlteracaoCliente.chamadoAlteracaoClienteEndereco.caixaPostal}">
		
		
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas dataTableAba" name="listaClienteEndereco" sort="list" id="endereco" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
		<display:column  sortable="false" title="Tipo" style="width: 110px">
			<a href="javascript:exibirAlteracaoDoEnderecoCliente('${j}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}');">
				<c:out value="${endereco.tipoEndereco.descricao}"/>
			</a> 
		</display:column>
		
		<display:column sortable="false"  title="Cep" style="width: 80px">
			<a href="javascript:exibirAlteracaoDoEnderecoCliente('${j}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}');">
				<c:out value="${endereco.cep.cep}"/>
			</a> 
		</display:column>
		
		<display:column sortable="false"  title="Endere�o" style="padding: 0 5px" class="quebraLinhaTexto">
			<a href="javascript:exibirAlteracaoDoEnderecoCliente('${j}','${endereco.tipoEndereco.chavePrimaria}','${endereco.cep.cep}','${endereco.numero}','${endereco.complemento}','${endereco.enderecoReferencia}','${endereco.caixaPostal}','${endereco.cep.chavePrimaria}','${endereco.chavePrimaria}');">
				<span style="width: 280px;"><c:out value="${endereco.cep.tipoLogradouro}"/>
					<c:out value="${endereco.cep.logradouro}"/>,&nbsp;
					<c:out value="${endereco.numero}"/>,&nbsp;
					<c:out value="${endereco.cep.bairro}"/>,&nbsp;
					<c:out value="${endereco.cep.nomeMunicipio}"/>&nbsp;-&nbsp;
					<c:out value="${endereco.cep.uf}"/>&nbsp;-&nbsp;
					<c:out value="${endereco.complemento}"/>
				</span>
			</a>
		</display:column>
		<c:set var="j" value="${j+1}" />
	</display:table>
	
	<c:set var="y" value="0" />
		<display:table class="dataTableGGAS dataTableAba" name="listaClienteEnderecoAnexoAbaEndereco" sort="list" id="anexoAbaEnderecoCliente" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
			<display:column sortable="false" title="Descri��o" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaEnderecoCliente(<c:out value='${anexoAbaEnderecoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					${anexoAbaEnderecoCliente.descricaoAnexo}
				</a>
			</display:column>
			
			<display:column sortable="false" title="Tipo" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaEnderecoCliente(<c:out value='${anexoAbaEnderecoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					${anexoAbaEnderecoCliente.tipoAnexo.descricao}
				</a>
			</display:column>
			
			<display:column sortable="false" title="Data �ltima Altera��o" style="width: 80px">
				<a href='javascript:visualizarAnexoAbaEnderecoCliente(<c:out value='${anexoAbaEnderecoCliente.chavePrimaria}'/>);'><span class="linkInvisivel"></span>  
					<fmt:formatDate pattern="dd/MM/yyyy" value="${anexoAbaEnderecoCliente.ultimaAlteracao}" var="formattedStatusDate" />
					<c:out value="${formattedStatusDate}"/>
					
					<fmt:formatDate pattern="HH:mm" value="${anexoAbaEnderecoCliente.ultimaAlteracao}" var="formattedStatusHour" />
					<c:out value="${formattedStatusHour}"/>
				</a>
			</display:column>
			
			<c:set var="y" value="${y+1}" />
		</display:table>
</fieldset>		
</fieldset>
	
	
</fieldset>



</div>
