<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 <%@ page contentType="text/html; charset=iso-8859-1" %>
 <%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

</script>

<div id="abaFinanceiro">
<fieldset class="conteinerDados3">
	<legend>Hist�rico de Agendamento</legend>
		<display:table class="dataTableGGAS" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" name="listaServicoAutorizacaoAgenda" id="servicoAutorizacaoAgenda"  sort="list" pagesize="15"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			<display:column sortable="false" title="N�mero da AS">
				<a href="javascript:exibirServicoAutorizacao('${servicoAutorizacaoAgenda.servicoAutorizacao.chavePrimaria}')">
					<c:out value="${servicoAutorizacaoAgenda.servicoAutorizacao.chavePrimaria}" />
				</a>
			</display:column>
			<display:column title="Data de Abertura">
				<fmt:formatDate value="${servicoAutorizacaoAgenda.ultimaAlteracao}" pattern="dd/MM/yyyy"/>	     		
			</display:column>
			
			<display:column title="Data de Agendamento">
				<fmt:formatDate value="${servicoAutorizacaoAgenda.dataAgenda}" pattern="dd/MM/yyyy"/>	     		
			</display:column>
			
			<display:column title="Previs�o de Encerramento">
				<fmt:formatDate value="${servicoAutorizacaoAgenda.servicoAutorizacao.dataPrevisaoEncerramento}" pattern="dd/MM/yyyy"/>	     		
			</display:column>
		
			<display:column property="servicoAutorizacao.chamado.protocolo.numeroProtocolo" sortable="false" title="N�mero do Protocolo"/>
			
			<display:column property="turno.descricao" sortable="false" title="Turno"/>
			
			<display:column property="servicoAutorizacao.servicoTipo.descricao" sortable="false" title="Tipo de Servi�o"  maxLength="40"/>
			
			<display:column property="servicoAutorizacao.status.descricao" sortable="false" title="Status"/>

			<display:column title="Funcion�rio">
			<c:if test="${ servicoAutorizacaoAgenda.servicoAutorizacao.status.descricao eq 'ENCERRADA' or servicoAutorizacaoAgenda.servicoAutorizacao.status.descricao eq 'CANCELADA'}">
				<c:out value="${servicoAutorizacaoAgenda.servicoAutorizacao.executante.nome }"/>
			</c:if>
			
			</display:column>
			
			
		</display:table>
</fieldset>
</div>