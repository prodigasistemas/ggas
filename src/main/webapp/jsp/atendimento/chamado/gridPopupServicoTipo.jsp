<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<script>
	$("#listaEquipe").removeAttr('disabled')
	$("#checkTable").removeAttr('disabled')
</script>

<table class="dataTableGGAS dataTableDialog dataTableCabecalho2Linhas">
	<thead>
		<tr>
			<th><input type="checkbox" id="checkTable" checked onclick="selectAll(this);"/></th>
			<th>Descri��o</th>
			<th>Prioridade</th>
			<th>Servi�o Regulamentado</th>
			<th>Servi�o Restrito para Im�vel</th>
		</tr>
	</thead>
	<tbody id="corpoGridServicoTipo"></tbody>
</table>

<br />

<label for="listaEquipe" class="rotulo">* Equipe : </label>
<select id="listaEquipe" name="equipe" style="margin-top: 5px;" onchange="carregarTurnosEquipe();">
    <option value="-1">Selecione</option>
</select>

<br />
<label for="turnoPreferencia" class="rotulo">Turno Prefer�ncia: </label>
<select name="turnoPreferencia" id="turnoPreferencia" style="margin-top: 13px;">
	<option value="-1">Selecione</option>
</select>

<br />
<div style="float: right; padding-bottom: 10px;">
	<vacess:vacess param="gerarAutorizacao">
		<input  name="botaoGerarAutorizacao" class="bottonRightCol2 botaoGrande1" id="botaoGerar" value="Gerar" type="button" onclick="gerarAutorizacaoServico();">
	</vacess:vacess>
</div>

