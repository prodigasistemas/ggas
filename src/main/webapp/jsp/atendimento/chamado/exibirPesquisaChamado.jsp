<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
        src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<%@ page import="br.com.ggas.atendimento.questionario.dominio.SituacaoQuestionario" %>

<div class="bootstrap">
    <form:form action="pesquisarChamado" id="chamadoForm" name="chamadoForm" method="post" modelAttribute="ChamadoVO">
        <input name="chavePrimaria" type="hidden" id="chavePrimaria">
        <input name="chavesPrimariasLote" type="hidden" id="chavesPrimariasLote">
        <input name="equipe" type="hidden" id="idEquipe">
        <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
        <input name="codigoTipoPessoa" type="hidden" id="codigoTipoPessoa">
        <input name="indicadorCheckboxTipoChamado" type="hidden" id="indicadorCheckboxTipoChamado" value="false">
        <input name="comAS" type="hidden" id="comAS" value="false">
        <input name="imagemCalendario" type="hidden" id="imagemCalendario" value="<c:url value="/imagens/calendario.png"/>">
        <input type="hidden" id="chamadoVO.siglaUnidadeFederacaoPontoConsumoChamado" value="<c:out value="${chamadoVO.siglaUnidadeFederacaoPontoConsumoChamado}" />">
        <input type="hidden" id="chamadoVO.nomeMunicipioPontoConsumoChamado" value="${chamadoVO.nomeMunicipioPontoConsumoChamado}" />
        <input type="hidden" id="chamadoVO.bairroPontoConsumoChamado" value="${chamadoVO.bairroPontoConsumoChamado}" />
        <input type="hidden" id="chamadoVO.idSegmentoChamado" value="${chamadoVO.idSegmentoChamado}" />
        <input type="hidden" id="chamadoVO.idChamadoTipo" value="${chamadoVO.idChamadoTipo}" />
        <input name="chavesPrimariasDialog" type="hidden" id="chavesPrimariasDialog">
        <input name="idTurnoPreferencia" type="hidden" id="idTurnoPreferencia">
        


        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Pesquisar Chamado</h5>
            </div>
            <div class="card-body">

                <div class="alert alert-primary fade show" role="alert">
                    <i class="fa fa-question-circle"></i>
                    Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em
                    <b>Pesquisar</b>, ou clique apenas em <b>Pesquisar</b> para exibir todos. Para incluir um novo
                    registro clique em <b>Incluir</b>
                </div>

                <div class="card">
                    <div class="card-body bg-light">
                        <div class="row mb-2">
                            <div class="col-md-6">

                                <div class="form-row mt-3">
                                    <div class="col-md-12">
                                        <h5>Filtro Chamado</h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                                <div class="card-header p-0" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#aba-chamado').toggleClass('show'); toggleClass('#caret-aba-chamado')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Chamado <i id="caret-aba-chamado" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="aba-chamado" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#aba-chamado">
                                                    <div class="card-body" style="border-bottom: 1px solid #dfdfdf;">
                                                        <div class="form-row mb-1">
                                                            <label for="numeroProtocolo" class="col-sm-4 text-md-right">N�mero do
                                                                Protocolo:</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control form-control-sm"
                                                                       id="numeroProtocolo" name="numeroProtocolo" maxlength="10"
                                                                       size="14"
                                                                       onkeypress="return formatarCampoInteiro(event);"
                                                                       value="${chamadoVO.numeroProtocolo}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="nomeSolicitante">Nome do
                                                                Solicitante:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="nomeSolicitante"
                                                                       name="nomeSolicitante" maxlength="30"
                                                                       size="30" value="${chamadoVO.nomeSolicitante}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);"
                                                                       onkeyup="letraMaiuscula(this);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="cpfSolicitante">CPF do
                                                                Solicitante:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="cpfSolicitante"
                                                                       name="cpfSolicitante" maxlength="14"
                                                                       size="14" value="${chamadoVO.cpfSolicitante}"
                                                                       onkeyup="desabilitarCpfCnpj();">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="rotuloCnpj">CNPJ do
                                                                Solicitante:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="cnpjSolicitante"
                                                                       name="cnpjSolicitante" maxlength="18"
                                                                       size="18" value="${chamadoVO.cnpjSolicitante}"
                                                                       onkeyup="desabilitarCpfCnpj();">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="idSegmentoChamado">Segmento do
                                                                Chamado:</label>
                                                            <div class="col-sm-8">
                                                                <select id="idSegmentoChamado" class="form-control form-control-sm"
                                                                        name="idSegmentoChamado">
                                                                    <option value="" <c:if test="${tipo.chavePrimaria eq null}">checked</c:if>>Selecione</option>
                                                                    <c:forEach items="${listaSegmento}" var="segmento">
                                                                        <option value="<c:out value="${segmento.chavePrimaria}"/>"
                                                                                <c:if test="${chamadoVO.idSegmentoChamado == segmento.chavePrimaria}">selected="selected"</c:if>>
                                                                            <c:out value="${segmento.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4  text-md-right" for="idChamadoTipo">Tipo do
                                                                Chamado:</label>
                                                            <div class="col-sm-8" id="divTipos">
                                                                <jsp:include
                                                                        page="/jsp/atendimento/chamado/bootstrap/selectTipoBootstrap.jsp"></jsp:include>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloAssuntoChamado"
                                                                   for="chamadoAssunto">Assunto do Chamado:</label>
                                                            <div class="col-sm-8" id="divAssunto">
                                                                <jsp:include
                                                                        page="/jsp/atendimento/chamado/bootstrap/selectAssuntoBootstrap.jsp"></jsp:include>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloTipoDeServico"
                                                                   for="idServicoTipo">Tipo do Servi�o:</label>
                                                            <div class="col-sm-8" id="divServicoTipoPesquisa">
                                                                <select id="idServicoTipo" class="form-control form-control-sm"
                                                                        name="idServicoTipo">
                                                                    <option value="-1">Selecione</option>
                                                                    <c:forEach items="${listaTipoServico}" var="servico">
                                                                        <option value="<c:out value="${servico.chavePrimaria}"/>"
                                                                                <c:if test="${chamadoVO.idServicoTipo == servico.chavePrimaria}">selected="selected"</c:if>>
                                                                            <c:out value="${servico.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="form-row mb-1">
                                                            <div class="col-md-12">
                                                                <label class="col-md-4 text-md-right">Status do chamado</label>
                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input id="aberto" class="custom-control-input" type="checkbox"
                                                                           name="listaStatus"
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'Aberto')}">checked="checked"</c:if>
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'ABERTO')}">checked="checked"</c:if>
                                                                           value="ABERTO">

                                                                    <label class="custom-control-label"
                                                                           for="aberto">Aberto</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input id="emAndamento" class="custom-control-input" type="checkbox"
                                                                           name="listaStatus"
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'Em atendimento')}">checked="checked"</c:if>
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'EM ANDAMENTO')}">checked="checked"</c:if>
                                                                           value="EM ANDAMENTO">
                                                                    <label class="custom-control-label"
                                                                           for="emAndamento">Em atendimento</label>
                                                                </div>

                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" id="rascunho" type="checkbox"
                                                                           name="listaStatus"
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'Rascunho')}">checked="checked"</c:if>
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'RASCUNHO')}">checked="checked"</c:if>
                                                                           value="RASCUNHO">
                                                                    <label class="custom-control-label"
                                                                           for="rascunho">Rascunho</label>
                                                                </div>

                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" type="checkbox" name="listaStatus"
                                                                           id="status_finalizado"
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'Finalizado')}">checked="checked"</c:if>
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'FINALIZADO')}">checked="checked"</c:if>
                                                                           value="FINALIZADO">
                                                                    <label class="custom-control-label"
                                                                           for="status_finalizado">Finalizado</label>
                                                                </div>
                                                                
                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" type="checkbox" name="listaStatus"
                                                                           id="status_pendente"
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'Pendente')}">checked="checked"</c:if>
                                                                           <c:if test="${fn:contains(chamadoVO.listaStatus,'PENDENTE')}">checked="checked"</c:if>
                                                                           value="PENDENTE">
                                                                    <label class="custom-control-label"
                                                                           for="status_pendente">Pendente</label>
                                                                </div>                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-4 pr-2 text-md-right">Origem Ag�ncia Reguladora</label>
                                                                <div class="custom-control custom-radio custom-control-inline ml-1 mt-2">
                                                                    <input class="custom-control-input" type="radio"
                                                                           name="indicadorAgenciaReguladora"
                                                                           id="origem-sim" value="true"
                                                                           <c:if test="${chamadoVO.indicadorAgenciaReguladora eq 'true'}">checked</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="origem-sim">Sim</label>
                                                                </div>

                                                                <div class="custom-control custom-radio custom-control-inline ml-1">
                                                                    <input class="custom-control-input" type="radio"
                                                                           name="indicadorAgenciaReguladora"
                                                                           id="origem-nao" value="false"
                                                                           <c:if test="${chamadoVO.indicadorAgenciaReguladora eq 'false'}">checked</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="origem-nao">N�o</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline ml-1">
                                                                    <input class="custom-control-input" type="radio"
                                                                           name="indicadorAgenciaReguladora"
                                                                           id="origem-todos" value=""
                                                                           <c:if test="${chamadoVO.indicadorAgenciaReguladora eq null}">checked</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="origem-todos">Todos</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <div class="col-md-12">
                                                                <label class="col-md-4 text-md-right">Prazo</label>
                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" id="prazoAtrasado" type="checkbox"
                                                                           <c:if test="${fn:contains(chamadoVO.listaPrazo,'ATRASADOS')}">checked="checked"</c:if>
                                                                           name="listaPrazo" value="ATRASADOS">

                                                                    <label class="custom-control-label"
                                                                           for="prazoAtrasado">Atrasados</label>
                                                                </div>

                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" id="prazoHoje" type="checkbox"
                                                                           <c:if test="${fn:contains(chamadoVO.listaPrazo,'HOJE')}">checked="checked"</c:if>
                                                                           name="listaPrazo" value="HOJE" style="margin-left: 45px">

                                                                    <label class="custom-control-label"
                                                                           for="prazoHoje">a Vencer</label>
                                                                </div>

                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" id="prazoFuturos" type="checkbox"
                                                                           <c:if test="${fn:contains(chamadoVO.listaPrazo,'FUTUROS')}">checked="checked"</c:if>
                                                                           name="listaPrazo" value="FUTUROS">

                                                                    <label class="custom-control-label"
                                                                           for="prazoFuturos">no Prazo</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <div class="col-md-12">
                                                                <label class="col-md-4 text-md-right">Houve Acionamento</label>
                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" id="acionamentoGasista"
                                                                           type="checkbox"
                                                                           <c:if test="${chamadoVO.indicadorAcionamentoGasista eq 'true'}">checked="checked"</c:if>
                                                                           name="indicadorAcionamentoGasista" value="true">
                                                                    <label class="custom-control-label"
                                                                           for="acionamentoGasista">Gasista</label>
                                                                </div>

                                                                <div class="custom-control custom-checkbox custom-control-inline ml-1">
                                                                    <input class="custom-control-input" id="acionamentoPlantonista"
                                                                           type="checkbox"
                                                                           <c:if test="${chamadoVO.indicadorAcionamentoPlantonista eq 'true'}">checked="checked"</c:if>
                                                                           name="indicadorAcionamentoPlantonista" value="true"
                                                                           style="margin-left: 45px">

                                                                    <label class="custom-control-label"
                                                                           for="acionamentoPlantonista">Plantonista</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="situacaoQuestionario">Situa��o
                                                                Question�rio:</label>
                                                            <div class="col-sm-8">
                                                                <select id="situacaoQuestionario" class="form-control form-control-sm"
                                                                        name="situacaoQuestionario">
                                                                    <option value="-1">Todas</option>
                                                                    <% for (SituacaoQuestionario situacaoQuestionario : SituacaoQuestionario
                                                                            .values()) { %>
                                                                    <c:set var='codigo' value="<%=situacaoQuestionario.getCodigo() %>"/>
                                                                    <option value="<%= situacaoQuestionario.getCodigo() %>"
                                                                            <c:if test="${chamadoVO.situacaoQuestionario.codigo == codigo}">selected="selected"</c:if>>
                                                                        <%= situacaoQuestionario.getDescricao() %>
                                                                    </option>
                                                                    <% } %>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="canalAtendimento">Canal de
                                                                Atendimento:</label>
                                                            <div class="col-sm-8">
                                                                <select id="canalAtendimento" class="form-control form-control-sm"
                                                                        name="listaCanalAtendimento">
                                                                    <option value="-1">Selecione</option>
                                                                    <c:forEach items="${listaCanalAtendimento}" var="canal">
                                                                        <option value="<c:out value="${canal.chavePrimaria}"/>"

                                                                                <c:forEach items="${chamadoVO.listaCanalAtendimento}"
                                                                                           var="canalAtendimento">
                                                                                    <c:if test="${canalAtendimento.chavePrimaria == canal.chavePrimaria}">selected="selected"</c:if>
                                                                                </c:forEach>
                                                                        >
                                                                            <c:out value="${canal.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="unidadeOrganizacional">Unidade
                                                                Organizacional:</label>
                                                            <div class="col-sm-8">
                                                                <select id="unidadeOrganizacional" class="form-control form-control-sm"
                                                                        name="unidadeOrganizacional"
                                                                        onclick="carregarResponsavel(this.value);">
                                                                    <option value="-1">Selecione</option>
                                                                    <c:forEach items="${listaUnidadeOrganizacional}" var="unidade">
                                                                        <option value="<c:out value="${unidade.chavePrimaria}"/>"
                                                                                <c:if test="${chamadoVO.unidadeOrganizacional.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
                                                                            <c:out value="${unidade.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>

                                                        </div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																id="rotuloUnidadesVisualizadora"
																for="unidadesVisualizadora">Unidades que podem
																Visualizar o chamado:</label>
															<div class="col-sm-8">
																<select id="unidadesVisualizadora"
																	class="form-control form-control-sm"
																	name="unidadesVisualizadora" multiple="multiple"
																	size="5">
																	<c:forEach items="${listaUnidadeOrganizacional}"
																		var="unidade">
																		<option
																			value="<c:out value="${unidade.chavePrimaria}"/>"
																			<c:if test="${chamadoVO.unidadesVisualizadorasIds ne null }">                                                                                
                                                                                <c:forEach
                                                                                            items="${chamadoVO.unidadesVisualizadorasIds}"
                                                                                            var="unid">
                                                                                        <c:if test="${unid eq unidade.chavePrimaria}">
                                                                                            selected="selected"
                                                                                        </c:if>
																				</c:forEach>
																			</c:if>>
																		<c:out value="${unidade.descricao}" />
																		</option>
																	</c:forEach>
																</select>
															</div>
														</div>

														<div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloRespons�vel" for="respons�vel">Respons�vel:</label>
                                                            <div class="col-sm-8" id="divResponsavel">
                                                                <select id="usuarioResponsavel" class="form-control form-control-sm"
                                                                        name="usuarioResponsavel">
                                                                    <option value="-1">Selecione</option>
                                                                    <c:forEach items="${listaUsuarioResponsavel}" var="responsavel">
                                                                        <option value="<c:out value="${responsavel.usuario.chavePrimaria}"/>"
                                                                                <c:if test="${chamadoVO.usuarioResponsavel.funcionario.chavePrimaria == responsavel.chavePrimaria}">selected="selected"</c:if>>
                                                                            <c:out value="${responsavel.nome}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <div class="col-md-12">
                                                                <label class="col-sm-4 text-md-right">Data de Abertura</label>
                                                                <div class="input-group input-group-sm col-sm-8 pl-1 pr-0">

                                                                    <input class="form-control form-control-sm campoData" type="text"
                                                                           id="dataInicioAbertura"
                                                                           name="dataInicioAbertura" maxlength="10"
                                                                           value="${chamadoVO.dataInicioAbertura}">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">at�</span>
                                                                    </div>
                                                                    <input class="form-control form-control-sm campoData" type="text"
                                                                           id="dataFimAbertura"
                                                                           name="dataFimAbertura" maxlength="10"
                                                                           value="${chamadoVO.dataFimAbertura}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <div class="col-md-12">
                                                                <label class="col-sm-4 text-md-right">Data de Encerramento</label>
                                                                <div class="input-group input-group-sm col-sm-8 pl-1 pr-0">

                                                                    <input class="form-control form-control-sm campoData" type="text"
                                                                           id="dataInicioEncerramento"
                                                                           name="dataInicioEncerramento" maxlength="10"
                                                                           value="${chamadoVO.dataInicioEncerramento}">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">at�</span>
                                                                    </div>
                                                                    <input class="form-control form-control-sm campoData" type="text"
                                                                           id="dataFimEncerramento"
                                                                           name="dataFimEncerramento" maxlength="10"
                                                                           value="${chamadoVO.dataFimEncerramento}">
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <div class="col-md-12">
                                                                <label class="col-sm-4 text-md-right">Data de Resolu��o</label>
                                                                <div class="input-group input-group-sm col-sm-8 pl-1 pr-0">

                                                                    <input class="form-control form-control-sm campoData" type="text"
                                                                           id="dataInicioResolucao"
                                                                           name="dataInicioResolucao" maxlength="10"
                                                                           value="${chamadoVO.dataInicioResolucao}">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">at�</span>
                                                                    </div>
                                                                    <input class="form-control form-control-sm campoData" type="text"
                                                                           id="dataFimResolucao"
                                                                           name="dataFimResolucao" maxlength="10"
                                                                           value="${chamadoVO.dataFimResolucao}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-3">
                                    <div class="col-md-12">
                                        <h5>Filtro Cliente</h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion">
                                            <div class="card">
                                                <div class="card-header p-0">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#aba-pessoa-fisica').toggleClass('show'); toggleClass('#caret-aba-pessoa-fisica')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Pessoa F�sica <i id="caret-aba-pessoa-fisica" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="aba-pessoa-fisica" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#aba-pessoa-fisica">
                                                    <div class="card-body">
                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloCpf"
                                                                   for="cpfCliente">CPF:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="cpfCliente"
                                                                       name="cpfCliente" maxlength="14" size="14"
                                                                       value="${chamadoVO.cpfCliente}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="rgCliente">RG:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="rgCliente"
                                                                       name="rgCliente" maxlength="13" size="14"
                                                                       onkeypress="return formatarCampoInteiro(event);"
                                                                       value="${chamadoVO.rgCliente}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloNome"
                                                                   for="nomeCliente">Nome:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="nomeCliente"
                                                                       name="nomeCliente" maxlength="30"
                                                                       size="30" value="${chamadoVO.nomeCliente}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);"
                                                                       onkeyup="letraMaiuscula(this);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloPassaporte"
                                                                   for="numeroPassaporte">Passaporte:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text"
                                                                       id="numeroPassaporte"
                                                                       name="numeroPassaporte"
                                                                       maxlength="13"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);"
                                                                       onkeypress="return formatarCampoNome(event);limparFormulario();"
                                                                       onkeyup="letraMaiuscula(this);" size="13"
                                                                       value="${chamadoVO.numeroPassaporte}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="idSegmentoCliente">Segmento:</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control form-control-sm" name="idSegmentoCliente"
                                                                        id="idSegmentoCliente">
                                                                    <option value="-1">Selecione</option>
                                                                    <c:forEach items="${listaSegmento}" var="segmento">
                                                                        <option value="<c:out value="${segmento.chavePrimaria}"/>"
                                                                                <c:if test="${chamadoVO.idSegmentoCliente == segmento.chavePrimaria}">selected="selected"</c:if>>
                                                                            <c:out value="${segmento.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="accordion">
                                            <div class="card">
                                                <div class="card-header p-0">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#aba-pessoa-juridica').toggleClass('show'); toggleClass('#caret-aba-pessoa-juridica')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Pessoa Jur�dica <i id="caret-aba-pessoa-juridica" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="aba-pessoa-juridica" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#aba-pessoa-juridica">
                                                    <div class="card-body" style="border-bottom: 1px solid #dfdfdf;">
                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloCnpj"
                                                                   for="cnpjCliente">CNPJ:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="cnpjCliente"
                                                                       name="cnpjCliente" maxlength="18"
                                                                       size="18" value="${chamadoVO.cnpjCliente}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloNomeFantasia"
                                                                   for="nomeFantasiaCliente">Nome Fantasia:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text"
                                                                       id="nomeFantasiaCliente"
                                                                       name="nomeFantasiaCliente"
                                                                       maxlength="30" size="30" value="${chamadoVO.nomeFantasiaCliente}"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);"
                                                                       onkeyup="letraMaiuscula(this);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-row mt-3">
                                    <div class="col-md-12">
                                        <h5>Localiza��o Ponto de Consumo</h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion">
                                            <div class="card">
                                                <div class="card-header p-0">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#aba-localizacao-ponto-consumo').toggleClass('show'); toggleClass('#caret-aba-localizacao-ponto-consumo')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Localiza��o Ponto de Consumo <i id="caret-aba-localizacao-ponto-consumo"
                                                                                            class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="aba-localizacao-ponto-consumo" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#aba-localizacao-ponto-consumo">
                                                    <div class="card-body" style="border-bottom: 1px solid #dfdfdf;">
                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right"
                                                                   for="siglaUnidadeFederacaoPontoConsumoChamado">UF:</label>
                                                            <div class="col-sm-8">
                                                                <select name="siglaUnidadeFederacaoPontoConsumoChamado"
                                                                        class="form-control form-control-sm"
                                                                        id="siglaUnidadeFederacaoPontoConsumoChamado"
                                                                        onchange="carregarMunicipios(this.value);">
                                                                    <option value="">Selecione</option>
                                                                    <c:forEach items="${listaUnidadeFederacao}" var="unidadeFederacao">
                                                                        <option value="<c:out value="${unidadeFederacao.sigla}"/>"
                                                                                <c:if test="${chamadoVO.siglaUnidadeFederacaoPontoConsumoChamado eq unidadeFederacao.sigla}">selected="selected"</c:if>>
                                                                            <c:out value="${unidadeFederacao.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloCidade"
                                                                   for="nomeMunicipioPontoConsumoChamado">Munic�pio:</label>
                                                            <div class="col-sm-8">
                                                                <select name="nomeMunicipioPontoConsumoChamado"
                                                                        id="nomeMunicipioPontoConsumoChamado"
                                                                        class="form-control form-control-sm"
                                                                        onchange="carregarBairros(this.value);"">
                                                                <option value="">Selecione</option>
                                                                <c:forEach items="${listaMunicipio}" var="municipio">
                                                                    <option value="<c:out value="${municipio.descricao}"/>"
                                                                            <c:if test="${chamadoVO.nomeMunicipioPontoConsumoChamado eq municipio.descricao}">selected="selected"</c:if>>
                                                                        <c:out value="${municipio.descricao}"/>
                                                                    </option>
                                                                </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloBairro"
                                                                   for="bairroPontoConsumoChamado">Bairro:</label>
                                                            <div class="col-sm-8">
                                                                <select name="bairroPontoConsumoChamado" id="bairroPontoConsumoChamado"
                                                                        class="form-control form-control-sm">
                                                                    <option value="">Selecione</option>
                                                                    <c:forEach items="${listaBairro}" var="cep">
                                                                        <option value="<c:out value="${cep.bairro}"/>"
                                                                                <c:if test="${chamadoVO.bairroPontoConsumoChamado eq cep.bairro}">selected="selected"</c:if>>
                                                                            <c:out value="${cep.bairro}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloLogradouro"
                                                                   for="lougradoroPontoConsumoChamado">Logradouro:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text"
                                                                       name="lougradoroPontoConsumoChamado"
                                                                       id="lougradoroPontoConsumoChamado" size="27" maxlength="50"
                                                                       value="${chamadoVO.lougradoroPontoConsumoChamado}"
                                                                       onkeyup="letraMaiuscula(this);"
                                                                       onblur="this.value = removerEspacoInicialFinal(this.value);"/>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloCepFalse"
                                                                   for="cepPontoConsumoChamado">CEP:</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="cepPontoConsumoChamado" id="cepPontoConsumoChamado"
                                                                       class="form-control form-control-sm cep" maxlength="9"
                                                                       value="${chamadoVO.cepPontoConsumoChamado}">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">

                                <div class="form-row mt-3">
                                    <div class="col-md-12">
                                        <h5>Filtro Im�vel</h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion">
                                            <div class="card">
                                                <div class="card-header p-0">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#aba-imovel').toggleClass('show'); toggleClass('#caret-aba-imovel')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Im�vel <i id="caret-aba-imovel" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="aba-imovel" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#aba-imovel">
                                                    <div class="card-body" style="border-bottom: 1px solid #dfdfdf;">
                                                        <jsp:include page="/jsp/cadastro/localidade/pesquisarCEPNovo.jsp">
                                                            <jsp:param name="cepObrigatorio" value="false"/>
                                                            <jsp:param name="idCampoCep" value="cepImovel"/>
                                                            <jsp:param name="numeroCep" value="${chamadoVO.cepImovel}"/>
                                                            <jsp:param name="chamado" value="true"/>
                                                        </jsp:include>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloNumeroImovel"
                                                                   for="numeroImovel">N�mero do Im�vel:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="numeroImovel"
                                                                       name="numeroImovel" maxlength="9"
                                                                       size="9"
                                                                       onkeypress="return formatarCampoNumeroEndereco(event, this);"
                                                                       value="${chamadoVO.numeroImovel}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right"
                                                                   for="descricaoComplemento">Complemento:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text"
                                                                       id="descricaoComplemento" name="descricaoComplemento"
                                                                       maxlength="25" size="30" value="${chamadoVO.descricaoComplemento}"
                                                                       onkeyup="letraMaiuscula(this);">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloComplemento" for="nomeImovel">Descri��o:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="nomeImovel"
                                                                       name="nomeImovel" maxlength="30" size="30"
                                                                       value="${chamadoVO.nomeImovel}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="matriculaImovel">Matr�cula:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="matriculaImovel"
                                                                       name="matriculaImovel" maxlength="9"
                                                                       size="9" onkeypress="return formatarCampoInteiro(event)"
                                                                       value="${chamadoVO.matriculaImovel}">
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-4 pr-2 text-md-right">Imovel � Condom�nio:</label>
                                                                <div class="custom-control custom-radio custom-control-inline ml-1 mt-1">

                                                                    <input class="custom-control-input" type="radio" name="condominioImovel"
                                                                           id="condominioImovel"
                                                                           value="true"
                                                                           <c:if test="${ condominioImovel eq 'true'}">checked</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="condominioImovel">Sim</label>
                                                                </div>

                                                                <div class="custom-control custom-radio custom-control-inline ml-1">
                                                                    <input class="custom-control-input" type="radio" name="condominioImovel"
                                                                           id="condominioImovelNao"
                                                                           value="false"
                                                                           <c:if test="${ condominioImovel eq 'false'}">checked</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="condominioImovelNao">N�o</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline ml-1">
                                                                    <input class="custom-control-input" type="radio" name="condominioImovel"
                                                                           id="condominioImovelTodos" value=""
                                                                           <c:if test="${ condominioImovel eq ''}">checked</c:if>>
                                                                    <label class="custom-control-label"
                                                                           for="condominioImovelTodos">Todos</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" for="idSegmentoImovel">Segmento:</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control form-control-sm" name="idSegmentoImovel"
                                                                        id="idSegmentoImovel">
                                                                    <option value="-1">Selecione</option>
                                                                    <c:forEach items="${listaSegmento}" var="segmento">
                                                                        <option value="<c:out value="${segmento.chavePrimaria}"/>"
                                                                                <c:if test="${chamadoVO.idSegmentoImovel == segmento.chavePrimaria}">selected="selected"</c:if>>
                                                                            <c:out value="${segmento.descricao}"/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-row mt-3">
                                    <div class="col-md-12">
                                        <h5>Filtro Contrato</h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="accordion">
                                            <div class="card">
                                                <div class="card-header p-0">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link btn-sm" type="button"
                                                                onclick="$('#aba-contrato').toggleClass('show'); toggleClass('#caret-aba-contrato')"
                                                                aria-expanded="true" aria-controls="collapseOne">
                                                            Contrato <i id="caret-aba-contrato" class="fa fa-caret-down"></i>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="aba-contrato" class="collapse" aria-labelledby="headingOne"
                                                     data-parent="#aba-contrato">
                                                    <div class="card-body" style="border-bottom: 1px solid #dfdfdf;">
                                                        <div class="form-row mb-1">
                                                            <label class="col-sm-4 text-md-right" id="rotuloNumeroContrato"
                                                                   for="numeroContrato">N�mero do Contrato:</label>
                                                            <div class="col-sm-8">
                                                                <input class="form-control form-control-sm" type="text" id="numeroContrato"
                                                                       name="numeroContrato" maxlength="9"
                                                                       size="8" onkeypress="return formatarCampoInteiro(event);"
                                                                       value="${chamadoVO.numeroContrato}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-row mt-5">
									<div class="col-md-12">
										<div class="form-row">
											<div class="col-md-12">
												<label for="codigolegado"><strong>C�digo do Ponto de Consumo:</strong></label> 
		                                       	<input class="form-control form-control-sm" type="text" id="codigoLegado"
		                                        	name="codigoLegado" maxlength="9"
		                                            size="8" onkeypress="return formatarCampoInteiro(event);"
		                                            value="${chamadoVO.codigoLegado}">
											</div>
										</div>
									</div>
                                </div>                                

                            </div>
                        </div>
                    </div>
                </div>


                <div id="dialogGerarAutorizacaoServico" title="Gerar Autoriza��o de Servi�o" style="display: none">
                    <jsp:include page="/jsp/atendimento/chamado/gridPopupServicoTipo.jsp"/>
                </div>

                <div id="dialogAlterarPrevisaoEncerramento" title="Alterar Previs�o Encerramento" style="display: none">
                    <jsp:include page="/jsp/atendimento/chamado/alterarPrevisaoEncerramento.jsp">
                        <jsp:param name="listaMotivos" value="${listaMotivos}"/>
                    </jsp:include>
                </div>




                <div class="row mt-3">
                    <div class="col align-self-end text-right">
                        <button class="btn btn-primary btn-sm" id="botaoPesquisar" type="button" onclick="pesquisar();">
                            <i class="fa fa-search"></i> Pesquisar
                        </button>
                        <button class="btn btn-secondary btn-sm" id="botaoLimpar" type="button"
                                onclick="limparFormulario();">
                            <i class="fa fa-times"></i> Limpar
                        </button>
                    </div>
                </div>


                <c:if test="${listaChamados ne null}">

                    <hr/>

                    <div class="text-center loading">
                        <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="table-chamados" width="100%"
                               style="display: none;">
                            <thead class="thead-ggas-bootstrap">
                            <tr>
                                <th>
                                    <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
                                        <input id="checkAllAuto" type="checkbox"  name="checkAllAuto"
                                               class="custom-control-input">
                                        <label class="custom-control-label p-0" for="checkAllAuto"></label>
                                    </div>
                                </th>
                                <th scope="col" class="text-center">Prazo</th>
                                <th scope="col" class="text-center">Protocolo</th>
                                <th scope="col" class="text-center">Abertura</th>
                                <th scope="col" class="text-center">Previs�o de Encerramento</th>
                                <th scope="col" class="text-center">Solicitante</th>
                                <th scope="col" class="text-center">Cliente</th>
								<th scope="col" class="text-center">Ponto de Consumo</th>                                
                                <th scope="col" class="text-center">Tipo do Chamado</th>
                                <th scope="col" class="text-center">Assunto do Chamado</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Usu�rio</th>
                                <th scope="col" class="text-center">Possui A.S.</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${listaChamados}" var="chamado">
                                <tr>
                                    <td>
                                        <div class="custom-control custom-checkbox custom-control-inline mr-0 ml-1" data-identificador-check="chk${chamado.chavePrimaria}">
                                            <input id="chk${chamado.chavePrimaria}" type="checkbox"  name="chavesPrimarias"
                                                   data-imovel-pai="${chamado.imovel.imovelCondominio.chavePrimaria}"
                                                   data-status="${chamado.status.descricao}"
                                                   data-permite-abrir-em-lote="${chamado.chamadoAssunto.permiteAbrirEmLote}"
                                                   class="custom-control-input"
                                                   value="${chamado.chavePrimaria}">
                                            <label class="custom-control-label p-0" for="chk${chamado.chavePrimaria}"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <img src="<c:url value="${chamado.imagemPrazo }"/>" border="0">
                                    </td>
                                    <td>
                                        <a title="Unidade Organizacional: ${chamado.unidadeOrganizacional.descricao}&#13;Responsavel: ${chamado.usuarioResponsavel.funcionario.nome}&#13;"
                                           href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${chamado.protocolo.numeroProtocolo}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <fmt:formatDate value="${chamado.protocolo.ultimaAlteracao}"
                                                            pattern="dd/MM/yyyy: HH:mm"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <fmt:formatDate value="${chamado.dataPrevisaoEncerramento}"
                                                            pattern="dd/MM/yyyy: HH:mm"/>
                                        </a>
                                    </td>
                                    <td>
                                        <c:if test="${chamado.nomeSolicitante ne null}">
                                            <a title="NOME: ${chamado.nomeSolicitante}&#13;CPF/CNPJ: ${chamado.cpfCnpjSolicitanteFormatado}&#13;RG: ${chamado.rgSolicitante}&#13;TELEFONE: ${chamado.telefoneSolicitante}&#13;EMAIL: ${chamado.emailSolicitante}"
                                               href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                    class="linkInvisivel"></span>
                                                <c:out value="${chamado.nomeSolicitante}"/>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${chamado.cliente ne null}">
                                            <a title="NOME: ${chamado.cliente.nome}&#13;CPF/CNPJ: <c:if test="${chamado.cliente.cpf != ''}">${chamado.cliente.cpfFormatado}</c:if><c:if test="${chamado.cliente.cnpj != ''}">${chamado.cliente.cnpjFormatado}</c:if>&#13;RG: ${chamado.cliente.rg}&#13;TELEFONE: <c:forEach items="${chamado.cliente.fones}" var="fone"><c:out value="(${fone.codigoDDD}) ${fone.numero}"/> </c:forEach>&#13;EMAIL: ${chamado.cliente.emailPrincipal}&#13;DESCRI��O DO IM�VEL: ${chamado.imovel.nome}"
                                               href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                    class="linkInvisivel"></span>
                                                <c:out value="${chamado.cliente.nome}"/>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${chamado.pontoConsumo ne null}">
                                            <a
                                               href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                    class="linkInvisivel"></span>
                                                <c:out value="${chamado.pontoConsumo.descricao}"/>
                                            </a>
                                        </c:if>
                                    </td>                                    
                                    <td>
                                        <a href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${chamado.chamadoAssunto.chamadoTipo.descricao}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${chamado.chamadoAssunto.descricao}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="linkStatusChamado"
                                           href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${chamado.status.descricao}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:out value="${chamado.loginUltimoUsuario}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                class="linkInvisivel"></span>
                                            <c:choose>
                                                <c:when test="${chamado.listaServicoAutorizacao ne null and not empty chamado.listaServicoAutorizacao }">
                                                    <a class='comAS'
                                                       href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                            class="linkInvisivel"></span>
                                                        <i class="fa fa-check-circle text-success"></i> Sim (${fn:length(chamado.listaServicoAutorizacao)})
                                                        
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a class='comAS'
                                                       href="javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);"><span
                                                            class="linkInvisivel"></span>
                                                        <i class="fa fa-times-circle text-danger"></i> N�o
                                                    </a>
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:if>

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="row justify-content-end">
                            <div class="col text-right mt-2">
                                <vacess:vacess param="exibirInclusaoChamado">
                                    <button name="buttonIncluirChamadosEmLote" id="buttonIncluirChamadosEmLote" type="button" class="btn btn-primary btn-sm ml-1"
                                            onclick="incluirChamadosEmLote()"><i class="fa fa-plus-circle"></i> Incluir Chamados em Lote
                                    </button>

                                    <button name="buttonIncluir" id="buttonIncluir" type="button" class="btn btn-primary btn-sm ml-1"
                                            onclick="incluir()"><i class="fa fa-plus-circle"></i> Incluir novo chamado
                                    </button>
                                </vacess:vacess>
                            </div>
                        </div>


                        <div class="row justify-content-between mt-2">
                            <div class="col mt-2">
                                <c:if test="${not empty listaChamados}">
                                    <jsp:include page="/jsp/atendimento/chamado/botoesChamadoBootstrap.jsp">
                                        <jsp:param name="isAdministradorAtendimento" value="${isAdministradorAtendimento }"/>
                                    </jsp:include>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalEncerrarEmLote" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Encerrar em lote</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modalEncerrarEmLoteBody">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </form:form>
</div>

<script>
    $(document).ready(function () {
        $("#buttonResponderQuestionario").removeAttr("disabled");

    });


</script>

<%--<script src="${pageContext.request.contextPath}/js/teste.js"></script>--%>
<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/pesquisarChamado/exibirPesquisaChamado/index.js"></script>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/chamado/exibirPesquisaChamado/index.js"
        type="application/javascript"></script>
