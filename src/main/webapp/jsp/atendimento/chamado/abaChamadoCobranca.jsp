<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script type="text/javascript">



</script>
<form method="post" action="#" id="notaDebitoCreditoForm" name="notaDebitoCreditoForm">	
	<div id="divMeicao">
	<fieldset class="conteinerDados3">
		<legend>Hist�rico de Cobran�as</legend>
	<!-- 	<div class="conteinerDados"> -->
		<fieldset id="conteinerDocumentos" class="conteinerBloco">
	<!-- 		<legend class="conteinerBlocoTitulo">Hist�rico de Conbran�as</legend>		 -->
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaFaturaCobranca" id="fatura" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
					
					<display:column headerClass="headerNumeroDocumento" style="width: 105px" property="idFatura" sortable="false" title="N� do<br />Documento" />
			   		
			   		<display:column headerClass="headerCicloReferencia" style="width: 100px" property="cicloReferencia" sortable="false" title="M�s/Ano-Ciclo" />
			   		
			   		<display:column headerClass="headerValorVencimento" style="width: 120px" property="valor" sortable="false" title="Valor (R$)" />
			   		
			   		<display:column headerClass="headerValorVencimento" style="width: 120px" property="saldo" sortable="false" title="Saldo (R$)" />
			   		
			   		<display:column headerClass="headerDataEmissao" style="width: 65px" property="dataEmissao" sortable="false" title="Data de<br />Emiss�o" />
			   		
			   		<display:column headerClass="headerVencimento" style="width: 65px" property="dataVencimento" sortable="false" title="Vencimento" />
			   		
			   		<display:column headerClass="headerTipoDocumento" style="width: 135px" property="tipoDocumento" sortable="false" title="Tipo de<br />Documento" />	   		
			   		
			   		<display:column headerClass="headerSituacaoPagamento" style="width: 135px" property="situacaoPagamento" sortable="false" title="Situa��o de<br /> Pagamento" />
			   						
					<display:column headerClass="headerSegundaVia" style="width: 135px" sortable="false" title="2� Via" >
						<a href='javascript:imprimirSegundaVia(<c:out value='${fatura.idFatura}'/>);'>
				    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via" title="Imprimir 2� Via" border="0" style="margin-top: 5px;" >
				    	</a>
						<a href='javascript:enviarEmailFaturaChamado(<c:out value='${fatura.idFatura}'/>);'>
						    <img src="<c:url value="/imagens/icon_email.gif" />" alt="Enviar Fatura por Email" title="Enviar Fatura por Email" border="0" style="margin-top: 5px;" >
						</a>
					</display:column>
					
			   	</display:table>
			</fieldset>
		
	<!-- 	</div> -->
	</fieldset>
	</div>
</form>