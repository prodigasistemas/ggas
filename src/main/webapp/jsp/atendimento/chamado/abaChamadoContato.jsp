<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir='/WEB-INF/tags' prefix='ggas'%>

<script>


function alterarContato(indice) {	
	var url = "alterarChamadoContato?nomeContato="+$("#nomeContato").val() 
    +"&codigoDDD="+$("#codigoDDD").val()+"&foneContato="
    +$("#foneContato").val()+"&ramalContato="+$("#ramalContato").val()+"&descricaoArea="
    +$("#descricaoArea").val()+"&emailContato="+$("#emailContato").val()+"&idTipoContato="
    +$("#tipoContato").val()+"&idProfissaoContato="+$("#profissaoContato").val()+"&indexListaContatos="+$("#indexListaContatos").val()
    carregarFragmento('gridAbaChamadoContato',url);

 	$("#indexListaContatos").val(null);
 	$("#botaoAlterarContato").attr("disabled","disabled");
 	$("#botaoIncluirContato").removeAttr("disabled");
 	limparCamposContato();
}

function limparCamposContato(){	
	$("#tipoContato").val('-1');
	$("#profissaoContato").val('-1');
	$("#foneContato").val(null);
	$("#nomeContato").val(null);
	$("#codigoDDD").val(null);
	$("#ramalContato").val(null);
	$("#descricaoArea").val(null);
	$("#emailContato").val(null);
	
}

function incluirContato() {	
	
	if ($("#tipoContato").val() >0 && $("#nomeContato") != null && $("#nomeContato").val() != "" ){
		var url = "adicionarChamadoContato?nomeContato="+$("#nomeContato").val() 
			           +"&codigoDDD="+$("#codigoDDD").val()+"&foneContato="
			           +$("#foneContato").val()+"&ramalContato="+$("#ramalContato").val()+"&descricaoArea="
			           +$("#descricaoArea").val()+"&emailContato="+$("#emailContato").val()+"&idTipoContato="
			           +$("#tipoContato").val()+"&idProfissaoContato="+$("#profissaoContato").val()
	carregarFragmento('gridAbaChamadoContato',url);
	limparCamposContato();}
}
   
function removerContato(indice) {

	var noCache = "noCache=" + new Date().getTime();	
	$("#gridAbaChamadoContato").load("removerChamadoContato?indexListaContatos="+indice+"&"+noCache);	
	$("#indexListaContatos").val(null);
	$("#botaoAlterarContato").attr("disabled","disabled");
	$("#botaoIncluirContato").removeAttr("disabled");
}

function atualizarContatoPrincipal(indice) {
	
	var noCache = "noCache=" + new Date().getTime();	
	
	$("#gridAbaChamadoContato").load("atualizarChamadoContatoPrincipal?indexListaContatos="+indice+"&"+noCache);	
}

function exibirAlteracaoDoContato(indice,idTipoContato,nomeContato,dddContato,telefoneContato,ramalContato,cargoContato,areaContato,emailContato,chavePrimariaContato) {
	$("#indexListaContatos").val(indice);
	$("#tipoContato").val(idTipoContato);
	$("#profissaoContato").val(cargoContato);
	$("#foneContato").val(telefoneContato);
	$("#nomeContato").val(nomeContato);
	$("#codigoDDD").val(dddContato);
	$("#ramalContato").val(ramalContato);
	$("#descricaoArea").val(areaContato);
	$("#emailContato").val(emailContato);
	
	$("#botaoIncluirContato").attr("disabled","disabled");
	$("#botaoAlterarContato").removeAttr("disabled");
	
}

</script>
<form:form id="contatoForm" name="contatoForm" method="post" modelAttribute="ClienteContatoTO">

<input name="acaoRemocaoContato" type="hidden" id="acaoRemocaoContato">
<input name="indexListaContatos" type="hidden" id="indexListaContatos">

<fieldset>
	<fieldset id="clienteContatoCol1" class="colunaEsq">
		<input type="hidden" id="chavePrimariaContato" name="chavePrimariaContato" />
		<label class="rotulo campoObrigatorio" id="rotuloTipoContato" for="idTipoContato"><span class="campoObrigatorioSimbolo2">* </span>Tipo do contato:</label>
		<select name="tipoContato" id="tipoContato" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoContato}" var="contatoTipo">
				<option value="<c:out value="${contatoTipo.chavePrimaria}"/>" <c:if test="${clienteTO.clienteContatoTO.tipoContato.chavePrimaria == contatoTipo.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${contatoTipo.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<label class="rotulo campoObrigatorio" id="rotuloNomeContato" for="nomeContato"><span class="campoObrigatorioSimbolo2">* </span>Nome do contato:</label>
		<input class="campoTexto" type="text" id="nomeContato" name="nomeContato" maxlength="20" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${clienteTO.clienteContatoTO.nome}"><br />
		<label class="rotulo" id="rotuloDDDContato" for="codigoDDD">DDD:</label>
		<input class="campoTexto campoHorizontal" type="text" id="codigoDDD" name="codigoDDD" onkeypress="return formatarCampoInteiro(event);" maxlength="2" value="${clienteTO.clienteContatoTO.codigoDDD}">
		<label class="rotulo rotuloHorizontal foneContato" id="rotuloNumeroContato" for="foneContato">Telefone:</label>
		<input class="campoTexto campoHorizontal foneContato2" type="text" id="foneContato" name="foneContato" maxlength="8" onkeypress="return formatarCampoInteiro(event)" value="${clienteTO.clienteContatoTO.fone}">
		<label class="rotulo rotuloHorizontal ramalContato" id="rotuloRamalContato" for="ramalContato">Ramal:</label>
		<input class="campoTexto campoHorizontal" type="text" id="ramalContato" name="ramalContato" maxlength="4" onkeypress="return formatarCampoInteiro(event)" value="">
	</fieldset>
	<fieldset id="clienteContatoCol2">
		<label class="rotulo" for="cargoContato">Cargo:</label>
		<select name="profissaoContato" id="profissaoContato" class="campoSelect">
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoCargo}" var="contatoProfissao">
				<option value="<c:out value="${contatoProfissao.chavePrimaria}"/>" <c:if test="${clienteTO.clienteContatoTO.profissao.chavePrimaria == contatoProfissao.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${contatoProfissao.descricao}"/>
				</option>		
			</c:forEach>
		</select><br />
		<label class="rotulo" for="areaContato">�rea:</label>
		<input class="campoTexto" type="text" id="descricaoArea" name="descricaoArea" maxlength="50" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${clienteTO.clienteContatoTO.descricaoArea}"><br />
		<label class="rotulo" id="rotuloEmailContato" for="emailContato">E-mail do contato:</label>
		<input class="campoTexto" type="text" id="emailContato" name="emailContato" maxlength="80" onkeypress="return formatarCampoEmail(event)" value="${clienteTO.clienteContatoTO.email}">
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campos obrigat�rios apenas para cadastrar Contatos.</p>
</fieldset>

<fieldset class="conteinerBotoesAba"> 
	<input class="bottonRightCol2" name="botaoLimparContato" id="botaoLimparContato" value="Limpar" type="button" onclick="limparContato();">
	<input class="bottonRightCol2" name="botaoIncluirContato" id="botaoIncluirContato" value="Adicionar contato" type="button" onclick="incluirContato();">
  	<input class="bottonRightCol2" disabled="disabled" name="botaoAlterarContato" id="botaoAlterarContato" value="Alterar contato" type="button" onclick="alterarContato(this.form,'<c:out value="${param['actionAdicionarContato']}"/>');"> 
</fieldset>




<div id="gridAbaChamadoContato">
	<jsp:include page="/jsp/atendimento/chamado/gridAbaChamadoContato.jsp"></jsp:include>     
</div>

</form:form>	
