<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
    
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>

<script type="text/javascript">

$(document).ready(function(){
	
	if(window.opener == null) {
		$('#botaoIncluirChamado').get(0).type = 'button'; 
	}
});

function voltar(){
	
	if(window.opener != null) {
		window.close();
	} else {
			submeter(
					'chamadoForm',
					'exibirPesquisaVisualizarPontoConsumo');
		}
}

function imprimirSegundaVia(idNota) {
	document.getElementById('chavePrimaria').value = idNota;
	submeter('0', 'imprimirSegundaViaNotaChamado');
}

function enviarEmailFaturaChamado(idFatura) {
	document.getElementById('chavePrimaria').value = idFatura;
	submeter('0','enviarEmailFaturaChamado');
}

function incluirChamado(){
	 submeter('chamadoForm', 'exibirInclusaoChamado');
}


function exibirCliente(cpfcnpj) {
	
	AjaxService.consultarClientePorCPFCNPJ(cpfcnpj, {
			callback : function(chaveCliente) {
				
				if(chaveCliente <= 0) {
					alert("N�o foi encontrado nenhum cliente no GGAS!");
				} else {
					document.getElementById('chavePrimaria').value = chaveCliente;
					submeter("chamadoForm", "exibirDetalhamentoCliente", "_blank");
				}

			},
			async : false
	});

}

function exibirServicoAutorizacao(chavePrimaria) {
	document.getElementById('chavePrimaria').value = chavePrimaria;
	submeter("chamadoForm", "exibirDetalhamentoServicoAutorizacao", "_blank");
}

function filtrarSituacaoTitulo(elem) {
	
	document.getElementById('situacaoTitulo').value = elem.value;
	
	submeter("chamadoForm", "visualizarInformacoesPontoConsumo");
}



</script>

	<h1 class="tituloInterno">Consolida��o de Dados do Ponto de Consumo</h1>
	
		<fieldset id="conteinerCliente" class="conteinerPesquisarIncluirAlterar">
			<fieldset id="consolidacaoPontoConsumoCol2" class="colunaFunc">
				<label class="rotulo">Cliente:</label>
				<span class="itemDetalhamento"><c:out value="${cliente.nome}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento"><c:out value="${endereco}"/></span><br />
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${cep}"/></span><br />				
			</fieldset>
		
		
		<fieldset id="funcionarioCol2" class="colunaFinal">
			<label class="rotulo">CPF / CNPJ:</label>
			<c:choose>
				<c:when test="${cliente.cpf ne null}">
					<span class="itemDetalhamento"><c:out value="${cliente.cpf}"/></span><br />
				</c:when>
				<c:when test="${cliente.cnpj ne null}">
					<span class="itemDetalhamento"><c:out value="${cliente.cnpj}"/></span><br />
				</c:when>
			</c:choose>		
			<label class="rotulo "> Ponto de Consumo:</label>
			<span class="itemDetalhamento"><c:out value="${pontoConsumo.codigoLegado}"/>&nbsp; - &nbsp;</span>
			<span class="itemDetalhamento"><c:out value="${pontoConsumo.descricao}"/></span>
			<span class="itemDetalhamento"  style="font-weight:bold">&nbsp; - &nbsp;<c:out value="${pontoConsumo.situacaoConsumo.descricao}"/></span><br />
			<label class="rotulo ">Rota :</label>
			<span class="itemDetalhamento"><c:out value="${pontoConsumo.rota}"/>   </span><br />
	<!-- 		<label class="rotulo rotulo2Linhas">Regra de Vencimento:</label> -->
	<%-- 		<span class="itemDetalhamento"><c:out value="${pontoConsumo.rota}"/></span><br /> --%>
			
			<label class="rotulo ">Segmento:</label>
			<span class="itemDetalhamento"><c:out value="  ${pontoConsumo.segmento.descricao}"/></span><br />
		<p>	<label class="rotulo ">Endere�o de Fatura:</label>
			<span class="itemDetalhamento"><c:out value="${enderecoFaturamento}"/></span><br /> </p>
			<label class="rotulo ">CEP da Fatura:</label>
			<span class="itemDetalhamento"><c:out value="  ${cepFaturamento}"/></span><br />			
			
		</fieldset>
	</fieldset>	
	
		<fieldset id="tabs" style="display: none">
			<ul>			
				<li><a href="#informacaoPontoConsumoAbaFinanceiro">Financeiro</a></li>
				<!-- <li><a href="#informacaoPontoConsumoAbaCobranca">Cobran�as</a></li> -->
				<li><a href="#informacaoPontoConsumoAbaContrato">Contrato</a></li>
				<li><a href="#informacaoPontoConsumoAbaAgendamento">Agendamento</a></li>
				<li><a href="#informacaoPontoConsumoAbaMedicao">Medi��o</a></li>
				<li><a href="#informacaoPontoConsumoDeclaracaoQuitacaoAnual">Declara��o Quita��o Anual</a></li>
			</ul>
			<fieldset id="informacaoPontoConsumoAbaFinanceiro">
				<jsp:include page="/jsp/atendimento/chamado/abaChamadoFinanceiro.jsp"></jsp:include>
			</fieldset>
<%-- 			<fieldset id="informacaoPontoConsumoAbaCobranca">
				<jsp:include page="/jsp/atendimento/chamado/abaChamadoCobranca.jsp"></jsp:include>
			</fieldset> --%>
			<fieldset id="informacaoPontoConsumoAbaContrato">
				<jsp:include page="/jsp/atendimento/chamado/abaChamadoContrato.jsp"></jsp:include>
			</fieldset>
			<fieldset id="informacaoPontoConsumoAbaAgendamento">
				<jsp:include page="/jsp/atendimento/chamado/abaChamadoAgendamento.jsp"></jsp:include>
			</fieldset>
			<fieldset id="informacaoPontoConsumoAbaMedicao">
				<jsp:include page="/jsp/atendimento/chamado/abaChamadoMedicao.jsp"></jsp:include>
			</fieldset>
			<fieldset id="informacaoPontoConsumoDeclaracaoQuitacaoAnual">
				<jsp:include page="/jsp/atendimento/chamado/abaChamadoQuitacaoAnual.jsp"></jsp:include>
			</fieldset>
		</fieldset>
	
	<fieldset class="conteinerBotoes"> 
		<input name="botaoVoltar" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
		<input id="botaoIncluirChamado" name="botaoIncluirChamado" class="bottonRightCol" value="Incluir Chamado" type="hidden" onclick="incluirChamado();">
	</fieldset>

