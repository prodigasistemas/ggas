<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<vacess:vacess param="exibirReiteracaoChamado">
    <input name="buttonReiterar" id="abrirReiteracao" value="Reiterar" class="bottonRightCol" onclick="exibirReiteracaoChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="exibirAlteracaoChamado">
    <input name="buttonAlterar" id="abrirAlteracao" value="Alterar" class="bottonRightCol" onclick="exibirAlterarChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="copiarChamado">
    <input name="buttonCopiar" value="Copiar" class="bottonRightCol" onclick="copiarChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="exibirReaberturaChamado">
    <input name="buttonReabrir" id="abrirReabertura" value="Reabrir" class="bottonRightCol" onclick="exibirReaberturaChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="exibirTramitacaoChamado">
    <input name="buttonTramitar" id="abrirTramitacao" value="Tramitar" class="bottonRightCol" onclick="exibirTramitacaoChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="exibirCapturarChamado">
    <input name="buttonCapturar" value="Capturar" class="bottonRightCol" onclick="exibirCapturarChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="exibirReativarChamado">
    <input name="buttonReativar" id="abrirReativacao" value="Reativar" class="bottonRightCol" onclick="exibirReativacaoChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="exibirEncerrarChamado">
    <input name="buttonEncerrar" id="abrirEncerramento" value="Encerrar" class="bottonRightCol" onclick="exibirEncerrarChamado()" type="button">
</vacess:vacess>
<vacess:vacess param="imprimirChamado">
    <input name="buttonImprimir" value="Imprimir" class="bottonRightCol" onclick="imprimirChamadoDetalhar()" type="button">
</vacess:vacess>
</br></br>
<vacess:vacess param="gerarAutorizacaoServico">
    <input name="botaoGerarServicoAutorizacao" class="bottonRightCol" id="botaoPesquisar" value="Gerar Autoriza��o de Servi�o" type="button" onclick="exibirDialogGerarAutorizacaoServico();">
</vacess:vacess>
<vacess:vacess param="exibirResponderQuestionario">
    <input name="buttonResponderQuestionario" id="buttonResponderQuestionario" value="Responder Questionario" class="bottonRightCol" onclick="exibirResponderQuestionario()" type="button">
</vacess:vacess>
<vacess:vacess param="exibirAlteracaoCadastroClienteChamado">
    <input name="buttonAlterarCadastroCliente" id="buttonAlterarCadastroCliente" value="Alterar Cadastro Cliente" class="bottonRightCol" onclick="exibirAlteracaoCadastroClienteChamado()" type="button">
</vacess:vacess>
<!-- 		<vacess:vacess param="exibirSolicitacaoAlteracaoDataVencimento"> -->
    <!-- 			<input name="buttonAlterarDataVencimento" id="buttonAlterarDataVencimento" value="Alterar Data de Vencimento" class="bottonRightCol" onclick="exibirSolicitacaoAlteracaoDataVencimento()" type="button"> -->
    <!-- 		</vacess:vacess>	 -->
<c:if test="${isAdministradorAtendimento ne null && isAdministradorAtendimento eq true}">
    <input name="buttonAlterarDataPrev" id="buttonAlterarDataPrev" value="Alterar Data Previs�o de Encerramento" class="bottonRightCol" onclick="exibirSolicitacaoAlteracaoDataPrev()" type="button">
</c:if>