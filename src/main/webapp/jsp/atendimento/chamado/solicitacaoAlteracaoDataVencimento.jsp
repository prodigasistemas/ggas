<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

function cancelar() {
	
	if(document.forms['chamadoForm'].usuarioResponsavel != undefined){
		  document.getElementById("usuarioResponsavel").value=''; 
		}
	submeter('chamadoForm','exibirPesquisaChamado');
}

function salvar(){	
	
	var selecao = verificarSelecao();
	if (selecao == true) {				
		submeter('chamadoForm', 'salvarAlteracaoDataVencimento');		
	}	
		
}



</script>

<h1 class="tituloInterno">Solicita��o de Altera��o da Data de Vencimento - Ag�ncia Virtual</h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form:form id="chamadoForm" name="chamadoForm" modelAttribute="Chamado">

	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chamado.chavePrimaria}">
	<input type="hidden" name="idMotivo" id="idMotivo" value="${idMotivo}">

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="materialCol1">				
			<label class="rotulo">N�mero do Contrato:</label>
			<span class="itemDetalhamento"><c:out value="${chamado.contrato.numeroCompletoContrato}"/></span><br />
			<label class="rotulo">Nome do Cliente:</label>
			<span class="itemDetalhamento"><c:out value="${chamado.cliente.nome}"/></span><br />
		</fieldset>	
	</fieldset>
	
<hr class="linhaSeparadoraPesquisa" />
	
	<fieldset class="conteinerBloco">
		<legend>Selecione os Pontos de Consumos para confirmar as altera��es.</legend>		
			<display:table class="dataTableGGAS" name="listaPontoConsumoTO" sort="list" id="pontoConsumoTO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
						       
			<display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input type="checkbox" name="chavesPrimarias" value="${pontoConsumoTO.chavePrimaria}">
	        </display:column>
			
			<display:column title="Descri��o">        
            	<c:out value="${pontoConsumoTO.descricao}"/>
	        </display:column> 
	             
	        <display:column title="Vencimento Vigencia">        
            	<c:out value="${pontoConsumoTO.vencimentoVigente}"/>
	        </display:column>
	        
	        <display:column title="Vencimento Solicitado">        
            	<c:out value="${pontoConsumoTO.vencimentoSolicitado}"/>
	        </display:column> 	
			
			</display:table>
	</fieldset>

	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="cancelar();">
	<%--     <vacess:vacess param="salvarAlteracaoDataVencimento"> --%>
	    	<input name="button" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Salvar" onclick="salvar();"  type="button">
	<%--     </vacess:vacess> --%>
	</fieldset>

</form:form>