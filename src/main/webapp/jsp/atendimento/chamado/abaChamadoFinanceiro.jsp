<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>

<script type="text/javascript">


</script>


<form method="post" action="#" id="notaDebitoCreditoForm" name="notaDebitoCreditoForm">	
	<div id="abaFinanceiro">
		<fieldset class="conteinerDados3">
<%-- 			<legend>Faturas</legend>
			<input name="chavePrimaria" type="hidden" id="chavePrimaria">
			
			
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				name="listaFaturaVO" id="faturaVO" sort="list" pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="#">
	
				<display:column property="fatura.valorTotal" sortable="false"
					title="Valor Total (R$)" />
	
				<display:column title="Consumo (m�)">
					<c:out value="${faturaVO.fatura.historicoConsumo.consumo}" />
				</display:column>
	
				<display:column title="Consumo Corrigido (m�)">
					<c:out value="${faturaVO.fatura.historicoConsumo.consumoApurado}" />
				</display:column>
	
				<display:column property="numeroCodigoBarras" sortable="false"
					title="N� C�digo de Barras" />
	
				<display:column property="valorAtualizado" sortable="false"
					title="Valor Atualizado" />
	
				<display:column title="Data de Pagamento">
					<fmt:formatDate value="${faturaVO.dataPagamento}"
						pattern="dd/MM/yyyy" />
				</display:column>
	
				<display:column title="Data de Emiss�o">
					<fmt:formatDate value="${faturaVO.fatura.dataEmissao}"
						pattern="dd/MM/yyyy" />
				</display:column>
	
				<display:column title="Data de Vencimento">
					<fmt:formatDate value="${faturaVO.fatura.dataVencimento}"
						pattern="dd/MM/yyyy" />
				</display:column>
	
				<display:column title="N�mero da Nota Fiscal">
					<c:out value="${faturaVO.documentoFiscal.numero}" />
				</display:column>
				
				<display:column title="Tipo de Documento">
					<c:out value="${faturaVO.fatura.tipoDocumento.descricao}"/>
				</display:column>
	
				<display:column title="Status" style="width: 95px">
					<c:if test="${faturaVO.fatura.creditoDebitoSituacao.valido}">
						<c:out value="${faturaVO.fatura.situacaoPagamento.descricao}" />
					</c:if>
				</display:column>
				
				<display:column title="2� Via" style="width: 45px">
			        <c:if test="${faturaVO.fatura ne null}">
						<a href='javascript:imprimirSegundaVia(<c:out value='${faturaVO.fatura.chavePrimaria}'/>);'>
					    	<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir 2� Via" title="Imprimir 2� Via" border="0" style="margin-top: 5px;" >					    	
					    </a>
						<a href='javascript:enviarEmailFaturaChamado(<c:out value='${faturaVO.fatura.chavePrimaria}'/>);'>
						    <img src="<c:url value="/imagens/icon_email.gif" />" alt="Enviar Fatura por Email" title="Enviar Fatura por Email" border="0" style="margin-top: 5px;" >
						</a>
					 </c:if>					
				</display:column>
									
			</display:table> --%>
			
			<legend>Titulos (GSA)</legend>				
			
			<label class="rotulo campoObrigatorio" for="situacaoTitulo"><span
					class="campoObrigatorioSimbolo">*</span>Situa��o:</label> <select
					name="situacoesTitulo" id="situacoesTitulo" class="campoSelect" onchange="filtrarSituacaoTitulo(this);">
					<option value=""<c:if test="${situacaoSelecionada eq null or situacaoSelecionada eq ''}">selected="selected"</c:if>>Todos</option>
					<option value="Aberto" <c:if test="${situacaoSelecionada eq 'Aberto'}">selected="selected"</c:if>>Aberto</option>
					<option value="Liquidado" <c:if test="${situacaoSelecionada eq 'Liquidado'}">selected="selected"</c:if>>Liquidado</option>
					<option value="Cancelado" <c:if test="${situacaoSelecionada eq 'Cancelado'}">selected="selected"</c:if>>Cancelado</option>
					<option value="Desativado" <c:if test="${situacaoSelecionada eq 'Desativado'}">selected="selected"</c:if>>Desativado</option>
				</select>
			
			
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				name="listaTitulosGSA" id="tituloGSA" sort="list" pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="#">
	
				<display:column title="N�mero T�tulo">
					<c:out value="${tituloGSA.numeroTitulo}" />
				</display:column>
				
				<display:column title="Data Emiss�o">
					<c:out value="${tituloGSA.dataEmissao}" />
				</display:column>
				
				<display:column title="Data Vencimento">
					<c:out value="${tituloGSA.dataVencimento}" />
				</display:column>
				
				<display:column title="Atraso">
				<c:if test="${tituloGSA.situacaoTitulo eq 'Aberto' }">
					<c:out value="${tituloGSA.diasAtraso}" />
				</c:if>
				</display:column>				
				
				<display:column title="Data Pagamento">
					<c:out value="${tituloGSA.dataDoPagamento}" />
				</display:column>				
				
				<display:column title="Valor Documento (R$)">
					<c:out value="${tituloGSA.valorDocumento}" />
				</display:column>
				
				<display:column title="Situa��o">
					<c:out value="${tituloGSA.situacaoTitulo}" />
				</display:column>
				
				<display:column title="N�mero Nota Fiscal">
					<c:out value="${tituloGSA.numeroNotaFiscal}" />
				</display:column>
				
				<display:column title="Data Protocolo Corte">
						<c:out value="${tituloGSA.dataProtocoloCorte}" />
				</display:column>					
				
				<display:column title="Titular do T�tulo">
					<a href="javascript:exibirCliente('${tituloGSA.cnpjcpf}')">
						<c:out value="${tituloGSA.titularTitulo}" />
					</a>
				</display:column>
				
									
			</display:table>			
			
		</fieldset>
	</div>
</form>