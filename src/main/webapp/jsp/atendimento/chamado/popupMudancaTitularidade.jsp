 <!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script>

    $(document).ready(function(){


    });
           
	function limparPessoa(){
		
		document.forms[0].idCliente.value = "";
		document.forms[0].nomeCliente.value = "";
		document.forms[0].documentoCliente.value = "";
		document.forms[0].enderecoCliente.value = "";
		document.forms[0].emailCliente.value = "";
		alert(document.forms[0].nomeCliente.value);
		
	}

	function cancelar(){
		window.close();
	}


	function incluir(){
		submeter('contratoForm', 'incluirMudancaTitularidade');
	}

	

</script>

<h1 class="tituloInterno">Mudan�a de Titularidade</h1>
<form method="post" action="mudarTitularidade" id="formMudarTitularidade" name="contratoForm">
<fieldset id="contratoCol1" class="colunaEsq">
	<fieldset id="pesquisarCliente">
		<ggas:labelContrato styleClass="asteriscoLegend"
			forCampo="clienteAssinatura">
		</ggas:labelContrato>
		<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
			<jsp:param name="idCampoIdCliente" value="idCliente" />
			<jsp:param name="idCliente" value="${contratoVO.idCliente}" />
			<jsp:param name="idCampoNomeCliente" value="nomeCliente" />
			<jsp:param name="nomeCliente" value="${contratoVO.nomeCliente}" />
			<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente" />
			<jsp:param name="documentoFormatadoCliente"
				value="${contratoVO.documentoCliente}" />
			<jsp:param name="idCampoEmail" value="emailCliente" />
			<jsp:param name="emailCliente"
				value="${contratoVO.emailCliente}" />
			<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente" />
			<jsp:param name="enderecoFormatadoCliente"
				value="${contratoVO.enderecoCliente}" />
			<jsp:param name="dadosClienteObrigatorios" value="false" />
			<jsp:param name="nomeComponente" value="Cliente Assinatura" />
		</jsp:include>
	</fieldset>
	
	<div class="labelCampo">
			<label class="rotulo" id="dataAssinatura" for="dataAssinatura"> Data da assinatura:</label>
			<input class="campoData" type="text" id="dataAssinatura" name="dataAssinatura" maxlength="10">
			 <br class="quebraLinha2" />
	</div>
	
</fieldset>

<fieldset class="conteinerBotoes"> 
	<input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onClick="cancelar()">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormularioDadosCliente();">
    <vacess:vacess param="incluirImovel">
    	<input name="Button" class="bottonRightCol2" id="botaoSalvar" value="Salvar" type="button" onclick="incluir()">
    </vacess:vacess>
    <input name="Button" class="bottonLeftRotulo" value="Fechar" type="button" onClick="cancelar()">
 	</fieldset>

</form>