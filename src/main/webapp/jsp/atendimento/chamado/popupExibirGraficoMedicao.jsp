<%@page import="br.com.ggas.util.Util" %>
<%@page import="org.joda.time.DateTime" %>
<%@page import="br.com.ggas.util.Constantes"%>
<%@page import="com.gargoylesoftware.htmlunit.WebConsole.Logger"%>
<%@page import="br.com.ggas.web.medicao.leitura.HistoricoLeituraGSAVO"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.JsonObject"%>

<%
Gson gsonObj = new Gson();

Collection<HistoricoLeituraGSAVO> listaLeituraGSA = (Collection<HistoricoLeituraGSAVO>)request.getAttribute("leiturasNoPeriodo");

String[] listaVolumesFaturados = new String[listaLeituraGSA.size()];
String[] datasVolumesFaturados = new String[listaLeituraGSA.size() + 1];

int valorTotal = 0;
int media = 1;


int index = 0;
for(HistoricoLeituraGSAVO historicoleitura : listaLeituraGSA){
	listaVolumesFaturados[index] = historicoleitura.getVolumeFaturado();
	datasVolumesFaturados[index] = historicoleitura.getDataLeituraAtual();
	valorTotal = valorTotal + Integer.valueOf(historicoleitura.getVolumeFaturado());
	
	index++;
}

DateTime dataAtual = new DateTime();
String dataFim = Util.obterUltimoDiaMes(dataAtual.minusMonths(1).toDate(), Constantes.FORMATO_DATA_BR);
String dataInicio = Util.obterPrimeiroDiaMes(dataAtual.minusYears(1).toDate(), Constantes.FORMATO_DATA_BR);
String dataAtualFormatada = Util.obterPrimeiroDiaMes(dataAtual.toDate(), Constantes.FORMATO_DATA_BR);

String mensagemTitulo = "Volumes Faturados no Período de: " + dataInicio + " à " + dataFim;

media = (int)Math.ceil((double)valorTotal/ (double)index);

datasVolumesFaturados[index] = dataAtualFormatada;
String dataPoints = gsonObj.toJson(listaVolumesFaturados);
String labels = gsonObj.toJson(datasVolumesFaturados);



%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script> 

<div class="bootstrap">
	<div style="width:100%; text-align:center">
		<h1 class="tituloInternoPopup"><%= mensagemTitulo %></h1>
	</div>
	<br/><br/>
	<canvas id="volumesGSA" aria-label="chart" heigth="350" width="580"></canvas>


<script type="text/javascript">
	
	window.opener.esconderIndicador();

	function translate_month( month ) {

	    var result = month;

	    switch(month) {
	        case 'Feb':
	            result = 'Fev' ;
	            break;
	        case 'Apr':
	            result = 'Abr' ;
	            break;
	        case 'May':
	            result = 'Mai' ;
	            break;
	        case 'Aug':
	            result = 'Ago' ;
	            break;
	        case 'Sep':
	            result = 'Set' ;
	            break;
	        case 'Oct':
	            result = 'Out' ;
	            break;	            
	        case 'Dec':
	            result = 'Dez' ;
	            break;

	    }

	    return result;
	}


	function translate_this_label( label ) {

	    month = label.match(/Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec/g);

	    if ( ! month ) 
	        return label;

	    translation = translate_month( month[0] );
	    return label.replace( month, translation, 'g' );
	}


	var ctx = document.getElementById('volumesGSA').getContext('2d');
	new Chart(ctx, {
		type : 'line',
		data : {
			labels : <%= labels %>,
			datasets : [
					{
						data : <%= dataPoints %>,
						label : "Volume Faturado",
						borderColor : "#4182cc",
						fill : false,
						pointRadius: 10,
						tension:0
					}]
		},
		  options: {
			    scales: {
			      yAxes: [{
			      }],
			      xAxes: [{
			        type: 'time',
			        time: {
			          parser: 'DD-MM-YYYY',
			          unit: 'month',
			          displayFormats: {
			             month: 'MMM/YY'
			          },
			          tooltipFormat: 'DD/MM/YY'
			        },
	                ticks : {
	                    callback: function( label, index, labels ) {

	                        return translate_this_label( label );
	                    }
	                }
			      }],
			    },
			  
			},
			plugins: [{
				   afterDatasetsDraw: function(chart) {
				      var ctx = chart.ctx;
				      chart.data.datasets.forEach(function(dataset, index) {
				         var datasetMeta = chart.getDatasetMeta(index);
				         if (datasetMeta.hidden) return;
				         datasetMeta.data.forEach(function(point, index) {
				            var value = dataset.data[index],
				                x = point.getCenterPoint().x,
				                y = point.getCenterPoint().y,
				                radius = point._model.radius,
				                fontSize = 12,
				                fontFamily = 'Verdana',
				                fontColor = 'black',
				                fontStyle = 'normal';
				            ctx.save();
				            ctx.textBaseline = 'middle';
				            ctx.textAlign = 'center';
				            ctx.font = fontStyle + ' ' + fontSize + 'px' + ' ' + fontFamily;
				            ctx.fillStyle = fontColor;
				            ctx.fillText(value, x, y - radius - fontSize);
				            ctx.restore();
				         });
				      });
				   }
				}]	
	});

</script>

	<fieldset class="conteinerBotoesPopup">
		<div style="width:20%; text-align:center; border-width: 2px; border-style: solid; border-color: black; float: right;">
			<span style="font-family: arial; font-size: 15px; font-weight: bold;">Volume Médio Mensal:<%= media %>&nbsp;m3/mês</span>
		</div>
		<input name="Button" class="bottonRightCol2" value="Fechar" type="button" onclick="window.close();">
	 </fieldset>
</div>

