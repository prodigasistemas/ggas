<%--
  ~ Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
  ~
  ~ Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
  ~
  ~ Este programa é um software livre; você pode redistribuí-lo e/ou
  ~ modificá-lo sob os termos de Licença Pública Geral GNU, conforme
  ~ publicada pela Free Software Foundation; versão 2 da Licença.
  ~
  ~ O GGAS é distribuído na expectativa de ser útil,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
  ~ COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
  ~ Consulte a Licença Pública Geral GNU para obter mais detalhes.
  ~
  ~ Você deve ter recebido uma cópia da Licença Pública Geral GNU
  ~ junto com este programa; se não, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
  --%>

<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 11/01/19
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="dialogGarantiaServico" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width: 1024px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo-servicos-garantia-indisponivel">Os serviços do Assunto do chamado estão sem garantia</h5>
                <h5 class="modal-title" id="titulo-servicos-garantia-disponivel">Prazo de garantia disponível dos serviços do Assunto do chamado</h5>
            </div>
            <div class="modal-body">
                <div class="alert alert-primary" role="alert" id="notificacao-servico-disponivel">
                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                    <div class="d-inline">
                        <b>Informativo!</b> Veja abaixo, os tipos de serviços que ainda possuem garantia para o ponto de consumo.
                    </div>
                </div>
                <div class="alert alert-warning" role="alert" id="notificacao-servico-indisponivel">
                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                    <div class="d-inline">
                        <b>Atenção!</b> Para abrir o chamado é necessário escolher outro assunto ou remover os pontos de consumo sem garantia.
                    </div>
                </div>
                <table id="tabelaGarantia" class="table table-bordered table-striped table-hover">
                    <thead class="thead-ggas-bootstrap">
                    <tr>
                        <th>Garantia</th>
                        <th>Tipo de Serviço</th>
                        <th>Ponto de Consumo</th>
                        <th>Cliente</th>
                        <th>Execuções Possíveis</th>
                        <th>Execuções Disponíveis</th>
                        <th>Data de Execução</th>
                        <th>Prazo de Garantia (em dias)</th>
                        <th>Data Limite</th>
                    </tr>
                    </thead>
                    <tbody id="corpoGridServicoTipoGarantia" style="background-color: #f1f5f1;"></tbody>
                </table>
            </div>
            <div id="footer-indisponivel" class="modal-footer" style="display: none">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i> Fechar e
                    escolher outros pontos de consumo</button>
            </div>

            <div id="footer-disponivel" class="modal-footer" style="display: none">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="button-salvar-servico-disponivel"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </div>
    </div>
</div>
