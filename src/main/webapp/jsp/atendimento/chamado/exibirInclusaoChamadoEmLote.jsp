<%--
  ~ Copyright (C) <2019> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2019 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% pageContext.setAttribute("newLineChar", " \r\n "); %>

<input id="url_imagens" type="hidden" value="<c:url value="/imagens/"/>" />

<link type="text/css" rel="stylesheet"
      href="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/incluirChamado/index.css"/>

<input type="hidden" id="informacaoAdicionalInfo" value="${fn:replace(chamado.informacaoAdicional,newLineChar,' ')}"/>

<div class="bootstrap">
    <form:form modelAttribute="Chamado" method="post" id="chamadoForm" name="chamadoForm" enctype="multipart/form-data">

        <input type="hidden" value="<c:url value="/imagens/calendario.png"/>" id="imagemCalendario"/>

        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Incluir Chamado em Lote</h5>
            </div>
            <div class="card-body">
                <span>

                    <input type="hidden" id="chamadoTipoChavePrimaria" value="${chamadoTipo.chavePrimaria}">

                    <input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chamado.chavePrimaria}">
                    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${chamado.chavePrimaria}">
                    <input name="chavesPrimariasDialog" type="hidden" id="chavesPrimariasDialog">
                    <input name="equipe" type="hidden" id="idEquipe">
                    <input type="hidden" name="versao" id="versao" value="${chamado.versao}">
                    <input type="hidden" name="chaveChamadoHistorico" id="chaveChamadoHistorico">
                    <input name="comAS" type="hidden" id="comAS" value="false">

                    <input type="hidden" name="rascunho" id="rascunho">
                    <input type="hidden" id="chaveAssunto" name="chaveAssunto" value="${chaveAssunto}">
                    <input type="hidden" id="unidade" name="unidade" value="${unidade}">
                    <input type="hidden" id="idPontoConsumo" name="idPontoConsumo"/>
                    <input type="hidden" name="idChamado" id="idChamado" value="${chamado.contrato.chavePrimaria}">
                    <input type="hidden" name="chamadoEmLote" id="chamadoEmLote" value="true">

                    <input type="hidden" id="protocolo" name="protocolo" value="${chamado.protocolo.chavePrimaria}"/>
                            <input type="hidden" id="imovel" name="imovel" value="${chamado.imovel.chavePrimaria}"/>
                            <input type="hidden" id="pontoConsumo" name="pontoConsumo" value=""/>

                </span>

                <div class="alert alert-primary" role="alert">
                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                    <div class="d-inline">
                        Utilize este formul�rio para incluir chamados em lote a partir de um ponto de consumo pai (Condom�nio, por exemplo).
                        Ao finalizar o preenchimento dos campos clique em <b>Incluir</b> ou clique em <b>Voltar</b> para retornar para a tela
                        de listagem.
                    </div>

                </div>

                <div id="dialogGerarAutorizacaoServico" title="Gerar Autoriza��o de Servi�o" style="display: none">
                    <jsp:include page="/jsp/atendimento/chamado/gridPopupServicoTipo.jsp"/>
                </div>
                
                <jsp:include page="garantiaServicoEmLote.jsp"/>

                <div id="dialog-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Relat�rio chamado</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Deseja incluir as Autoriza��es de Servi�os correspondentes a esse chamado?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="imprimirSemAs()">N�o</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="imprimirComAs()">Sim</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-md-7 mt-1">

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="descricao">Descri��o: <span class="text-danger">*</span></label>
                                <textarea class="form-control form-control-sm" id="descricao"
                                          name="descricao" placeholder="Descri��o" cols="35" rows="6"
                                          onblur="this.value=removerEspacoInicialFinal(this.value);">${chamado.descricao}</textarea>
                            </div>
                        </div>

                        <div class="form-row">

                            <div class="col-sm-12">
                                <label for="segmento">Segmento:<span class="text-danger">*</span></label>
                                <select name="segmento" id="segmento" class="form-control form-control-sm" onchange="atualizarListaTipoChamado(this.value);">
                                    <option value="">Todos</option>
                                    <c:forEach items="${segmentos}" var="segmento">
                                        <option value="${segmento.chavePrimaria}">${segmento.descricao}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-md-6" id="divTiposChamados">
                                <label for="chamadoTipo">Tipo do chamado:<span class="text-danger">*</span></label>
                                <select name="chamadoTipo" id="chamadoTipo" class="form-control form-control-sm"
                                onchange="atualizarAssuntos(this.value);carregarResponsavelPorChamadoAssunto('0');">
                                    <option value="">Selecione o tipo de chamado...</option>
                                    <c:forEach items="${chamadosTipo}" var="chamadotipo">
                                        <option value="<c:out value='${chamadotipo.chavePrimaria}'/>"
                                                title="${chamadotipo.descricao}"
                                                <c:if test="${chamado.chamadoAssunto ne null && chamado.chamadoAssunto.chamadoTipo.chavePrimaria == chamadotipo.chavePrimaria}">selected="selected"</c:if>
                                                <c:if test="${chamado.chamadoAssunto eq null && chamadoTipo.chavePrimaria == chamadotipo.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${chamadotipo.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-md-6" id="divAssuntos">
                                <jsp:include
                                        page="/jsp/atendimento/chamado/bootstrap/selectBoxAssuntoBootstrap.jsp"></jsp:include>
                            </div>


                            <div class="col-md-6" id="divUnidadeOrganizacional">
                                        <jsp:include page="/jsp/atendimento/chamado/bootstrap/selectBoxUnidadeOrganizacionalBootstrap.jsp"></jsp:include>
                            </div>

                            <div class="col-md-6">
                                <label for="canalAtendimento">Canal de atendimento:<span
                                        class="text-danger">*</span></label>
                                <select name="canalAtendimento" id="canalAtendimento"
                                        class="form-control form-control-sm">
                                        <option value="">Selecione canal de atendimento...</option>
                                    <c:forEach items="${listaCanalAtendimento}" var="canalAtendimento">
                                        <option value="<c:out value="${canalAtendimento.chavePrimaria}"/>"
                                                title="${canalAtendimento.descricao}"
                                                <c:if test="${chamado.canalAtendimento.chavePrimaria == canalAtendimento.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${canalAtendimento.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="form-row mt-1">
                            <div class="col-md-12">
                                <label>Outras unidades podem visualizar esse chamado?</label>
                            </div>
                            <div class="col-md-12">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <vacess:vacess param="alterarUnidadesVisualizadoras">
                                        <input id="flagUnidadesVisualizadorasSim" class="custom-control-input"
                                               type="radio"
                                               value="true"
                                               onclick="mudarEstadoUnidadesVisualizadoras(this)"
                                               name="flagUnidadesVisualizadoras"
                                               <c:if test="${indicadorUnidadesOrganizacionalVisualizadora ne null && indicadorUnidadesOrganizacionalVisualizadora eq 'true'}">checked="checked"</c:if> />
                                    </vacess:vacess>
                                    <label class="custom-control-label"
                                           for="flagUnidadesVisualizadorasSim">Sim</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <vacess:vacess param="alterarUnidadesVisualizadoras">
                                        <input id="flagUnidadesVisualizadorasNao" class="custom-control-input"
                                               type="radio"
                                               value="false"
                                               onclick="mudarEstadoUnidadesVisualizadoras(this)"
                                               name="flagUnidadesVisualizadoras"
                                               <c:if test="${indicadorUnidadesOrganizacionalVisualizadora ne null && indicadorUnidadesOrganizacionalVisualizadora eq 'false'}">checked="checked"</c:if> />
                                    </vacess:vacess>
                                    <label class="custom-control-label"
                                           for="flagUnidadesVisualizadorasNao">N�o</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-row mt-1">
                            <div class="col-md-12" id="divUnidadeOrganizacionalVisualizadoras"
                                 <c:if test="${indicadorUnidadesOrganizacionalVisualizadora eq null || indicadorUnidadesOrganizacionalVisualizadora eq 'false'}">style="display: none;"</c:if>>
                                <label for="unidadeOrganizacionalVisualizadoras">Unidades que podem visualizar: <span
                                        class="text-danger">*</span></label>
                                <div class="input-group">
                                    <vacess:vacess param="alterarUnidadesVisualizadoras">
                                        <select id="unidadeOrganizacionalVisualizadoras"
                                                name="unidadeOrganizacionalVisualizadoras"
                                                class="form-control form-control-sm"
                                                multiple="multiple">
                                            <c:forEach items="${listaUnidadeOrganizacional}"
                                                       var="unidadeOrganizacional">
                                                <option title="${unidadeOrganizacional.descricao}"
                                                        value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>"
                                                        <c:if test="${listaChamadoUnidadeOrganizacional ne null }">
                                                            <c:forEach
                                                                    items="${listaChamadoUnidadeOrganizacional}"
                                                                    var="chamadoUnidadeOrganizacional" >
                                                                <c:if test="${chamadoUnidadeOrganizacional.unidadeOrganizacional.chavePrimaria eq unidadeOrganizacional.chavePrimaria}">
                                                                    selected="selected"
                                                                </c:if>
                                                                >
                                                            </c:forEach>
                                                        </c:if>
                                                        >
                                                        <c:out value="${unidadeOrganizacional.descricao}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </vacess:vacess>
                                    <div class="input-group-append d-md-none">
                                                <span class="input-group-text" style="height: 100%;"><i
                                                        class="fa fa-search-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- END Outras unidades podem visualizar -->
                    </div>

                    <div class="col-md-5 mt-1">
                        <!-- ISSO tem a ver com os tipos de chamado, assunto do chamado, etc -->
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="orientacaoAssunto">Observa��o/Orienta��o:
                                    <button id="btn-copiar-observacao" onclick="copiarObservacao()" type="button"
                                            class="btn btn-primary btn-sm copiar-observacao ml-2"><i class="fa fa-copy"></i> Copiar
                                    </button>
                                </label>
                                <textarea class="form-control form-control-sm" name="orientacaoAssunto"
                                          id="orientacaoAssunto" cols="60"
                                          rows="6"
                                          maxlength="800" disabled></textarea>
                            </div>
                        </div>

                        <div id="divPrazoDiferenciado">
                            <jsp:include page="/jsp/atendimento/chamado/divPrazoDiferenciado.jsp"></jsp:include>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="divPrazo">Prazo previsto(em horas):</label>
                                <input type="number" class="form-control form-control-sm" id="divPrazo"
                                       name="divPrazo"
                                       maxlength="9" size="9" placeholder="Escolha um assunto para preencher o prazo..."
                                       value="${chamado.chamadoAssunto.quantidadeHorasPrevistaAtendimento}"
                                       disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="dataPrevisaoEncerramento">Previs�o de encerramento:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control form-control-sm"
                                           id="dataPrevisaoEncerramento"
                                           name="dataPrevisaoEncerramento" placeholder="Previs�o de encerramento"
                                           maxlength="16" size="16"
                                           value="<fmt:formatDate value="${chamado.dataPrevisaoEncerramento}" pattern="dd/MM/yyyy HH:mm"/>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" id="tab-imovel" data-toggle="tab"
                                   href="#contentTabImovel" role="tab" aria-controls="contentTabImovel"
                                   aria-selected="true"><i class="fa fa-hotel"></i> Im�vel</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-docs" data-toggle="tab"
                                   href="#contentTabDocs" role="tab" aria-controls="contentTabDocs"
                                   aria-selected="false"><i class="fa fa-layer-group"></i> Documentos</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="content-incluir-chamado">
                            <div class="tab-pane fade show active" id="contentTabImovel" role="contentTabImovel"
                                 aria-labelledby="contentTabImovel">
                                <div id="chamadoAbaImovel">
                                    <jsp:include
                                            page="/jsp/atendimento/chamado/bootstrap/abaImovelCondominioBootstrap.jsp"></jsp:include>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabDocs" role="contentTabDocs"
                                     aria-labelledby="contentTabDocs">

                                    <jsp:include
                                            page="/jsp/atendimento/comumchamadoservicoautorizacao/bootstrap/selecionarAnexosBootstrap.jsp">
                                        <jsp:param name="actionAdicionarAnexo" value="adicionarAnexoChamado"/>
                                        <jsp:param name="actionRemoverAnexo" value="removerAnexoChamado"/>
                                        <jsp:param name="actionVisualizarAnexo" value="visualizarAnexoChamado"/>
                                        <jsp:param name="actionLimparCampoArquivo" value="limparCampoArquivoChamado"/>
                                        <jsp:param name="nomeForm" value="chamadoForm"/>
                                    </jsp:include>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer">

                <div class="row justify-content-between mt-2">
                    <div class="col-md-4 col-sm-12 mt-1">
                        <button class="btn btn-default btn-sm mb-1 mr-1" type="button" id="btnVoltar">
                            <i class="fa fa-arrow-left"></i> Voltar
                        </button>
                        <button name="button"
                                id="limparFormulario"
                                class="btn btn-danger btn-sm mb-1 mr-1"
                                type="button">
                            <i class="fa fa-times"></i> Limpar
                        </button>
                    </div>
                    <div class="col-md-8 col-sm-12 mt-1 text-md-right">
                        <button name="button" id="botaoIncluir"
                                class="btn btn-primary btn-sm mb-1 mr-1"
                                type="submit">
                            <i class="fa fa-save"></i> Salvar
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </form:form>

    <jsp:include page="../../cadastro/cliente/bootstrap/modalPesquisarCliente.jsp"/>


    <div id="modal-colar-observacao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Colar conte�do</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Gostaria de substituir o conte�do da descri��o pelo que foi copiado da observa��o?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">N�o</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="confirmarColaObservacao()">Sim</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-unidade-podem-visualizar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Unidades que podem visualizar esse chamado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul id="ul-unidades-podem-visualizar" class="list-group list-group-flush">

                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/chamado/incluirChamado/chamadoEmLote.js"
        type="application/javascript"></script>
<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/incluirChamadoEmLote/index.js" type="application/javascript"></script>

