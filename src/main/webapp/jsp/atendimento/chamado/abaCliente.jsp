<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 <%@ page contentType="text/html; charset=iso-8859-1" %>
 <%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<script type="text/javascript">


$(function() {

	 $( "#nome" ).autocomplete({
		 source: function( request, response ) {
			
		 				$.ajax({
		 							url: "carregaNomesClientes?nome="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
			 											
		 											response( $.map( data, function( item ) {
		 												//alert (item.nome+item.cpfCnpj);
		 											return {
		 												     
		 												      
		 													 label:  item.nome + '<br>' +
		 													 item.cpfCnpj +'<br>' +
		 													 item.endereco +'<br>' +
		 													 item.numeroContrato + '<br>' +
		 													 item.inicioVigenciaContrato +'<br>' +
		 													 item.fimVigenciaContrato,		 													
		 													 value: item.chavePrimaria,
		 													 idContrato: item.idContrato
		 													
		 													
		 													}
		 											}));
		 										}
		 						});
		 		}, 
		 		create: function () {
	                $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	                    return $('<li>')
	                        .append('<a>' + item.label +'</a>')
	                        .appendTo(ul);
	                };
	            },
		 minLength: 3,
		 select: function( event, ui ) {
			 
			 
		 		carregarCliente(ui.item.value,null,null,null, null,ui.item.idContrato);
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
			 
		 }).focus(function(){            
	            // The following works only once.
	            // $(this).trigger('keydown.autocomplete');
	            // As suggested by digitalPBK, works multiple times
	            // $(this).data("autocomplete").search($(this).val());
	            // As noted by Jonny in his answer, with newer versions use uiAutocomplete
	            $(this).data("uiAutocomplete").search($(this).val());
	        });;
	 
	 $( "#nomeFantasia" ).autocomplete({
					 source: function( request, response ) {	
							     
		 				$.ajax({
		 							url: "carregaNomesClientes?nomeFantasia="+request.term,
		 							dataType: "json",
		 							data: {
		 								style: "full",
		 								maxRows: 12,
		 								name_startsWith: request.term
		 								},
		 							success: function( data ) {
		 												
		 											response( $.map( data, function( item ) {
		 												return {
		 													 label:  item.nomeFantasia + ' <br> ' +
		 													 item.cpfCnpj +' <br> ' +
		 													 item.endereco +' <br> ' +
		 													 item.numeroContrato + ' <br> ' +
		 													 item.inicioVigenciaContrato +' <br> ' +
		 													 item.fimVigenciaContrato,	
		 													value: item.chavePrimaria,
		 													idContrato: item.idContrato
		 													}
		 											}));
		 										}
		 						});
		 		},
		 		create: function () {
	                $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	                    return $('<li>')
	                        .append('<a>' + item.label +'</a>')
	                        .appendTo(ul);
	                };
	            },
		 minLength: 3,
		 select: function( event, ui ) {
		 		
		 		carregarCliente(ui.item.value,null,null,null,null,ui.item.idContrato);
		 			},
	 	open: function() {
				 $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			 },
		 close: function() {
		 	$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		 	}
		 });
});

function carregarCliente(chavePrimaria,cpfCnpj,passaporte,email,telefone,idContrato) {
	
	document.forms[0].contrato.value = "";
	var url;
	if(idContrato!=null){
		url = "carregarClientePorIdContrato?chavePrimariaContrato="+idContrato;
	} else if (chavePrimaria!=null) {
		url = "carregarCliente?chavePrimaria="+chavePrimaria;
	}
	/*else if(cpfCnpj!=null&&trim(cpfCnpj)!="") {
		url = "carregarCliente?cpfCnpj="+cpfCnpj;
	} else if(passaporte!=null&&trim(passaporte)!="") {
		url = "carregarCliente?passaporte="+passaporte;
	} else if(email!=null&&trim(email)!="") {
		url = "carregarCliente?email="+email;
	} else if(telefone!=null&&trim(telefone)!="") {
		url = "carregarCliente?telefone="+telefone;
	}*/
	if(url!=null) {
			carregarFragmento('divCliente',url);
			var chavePrimariaCliente = document.forms[0].chaveCliente.value;
			
			 if ($('#exibirPassaporte').val()=="2"){
					$("#divPassaporte").hide();
				}
			$("#gridChamadosCliente").load("carregarChamadosClienteImovel?chavePrimariaCliente="+chavePrimariaCliente);
			document.forms[0].cliente.value = chavePrimariaCliente;
			document.forms[0].chaveContrato.value = idContrato;
			
			if(idContrato!= null) {
				$("#gridImoveis").load("carregarImovelPorContrato?chavePrimariaContrato="+idContrato+"&numeroContrato=", 
		                function () {
							
							if($("input[name='chaveImovel']").size() == 1) {
								
								$("input[name='chaveImovel']").trigger( "click" );
							}
	                } );   		
			} else {
				$("#gridImoveis").load("carregarImovelPorContrato?chavePrimariaContrato=-1&numeroContrato=");
				$("#gridChamadoPontosConsumo").load("carregarPontosConsumo?chavePrimariaImovel=-1&fluxoExecucao=fluxoInclusao&chavePrimariaContrato=-1");
			}
							
			
			if($("#nome").val() != $("#nomeSolicitante").val() || $("#cpfCnpj").val() != $("#cpfCnpjSolicitante").val() || 
					$("#email").val() != $("#emailSolicitante").val() || $("#telefone").val() != $("#telefoneSolicitante").val()  
					|| $("#rg").val() != $("#rgSolicitante").val() ) {
				
				if( confirm("Deseja replicar os dados do cliente para o solicitante?") ){
					
					$("#nomeSolicitante").val($("#nome").val());
					$("#cpfCnpjSolicitante").val($("#cpfCnpj").val());
					$("#emailSolicitante").val($("#email").val());
					$("#telefoneSolicitante").val($("#telefone").val());
					$("#rgSolicitante").val($("#rg").val());
					
				}
			}
					
			
			document.getElementById('bottonChamadoContato').disabled = false;
			var url =encodeURI("exibirContatoCliente?chavePrimariaCliente="+chavePrimariaCliente);
			carregarFragmento('divChamadoContatoCliente',url);
						
	}
}


function exibirPopupPesquisaCliente() {	
	
	popup = window.open('exibirPesquisaClientePopup' ,'popup','height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}
	
function selecionarCliente(idSelecionado)	{
	
	var idCliente = document.getElementById("chaveCliente");
	var nomeCompletoCliente = document.getElementById("nome");
	var documentoFormatado = document.getElementById("cpfCnpj");
	var emailCliente = document.getElementById("email");
	var nomeFantasia = document.getElementById("nomeFantasia");
	var telefone = document.getElementById("telefone");
	var numeroPassaporte = document.getElementById("numeroPassaporte");
	var rg = document.getElementById("rg");
	  
	if(idSelecionado != '') {		
	AjaxService.obterClientePorChave( idSelecionado, {
          	callback: function(cliente) {	           		
          		if(cliente != null){  	           			        		      		         		
               	
          		idCliente.value = cliente["chavePrimaria"];
               	nomeCompletoCliente.value = cliente["nome"];
               
               	if(cliente["cnpj"] != undefined ){
               		documentoFormatado.value = cliente["cnpj"];
               	} else {
	               	
               		if(cliente["cpf"] != undefined ){
	               		documentoFormatado.value = cliente["cpf"];
	               	} else {
	               		documentoFormatado.value = "";
	               	}
               	}
               	
               	emailCliente.value = cliente["email"];
               	$('form[name="notaDebitoCreditoForm"]').change();
              	}
          		
          		telefone.value = cliente["telefone"];
          		numeroPassaporte.value = cliente["numeroPassaporte"];
          		nomeFantasia.value = cliente["nomeFantasia"];
          		rg.value = cliente["rg"];
          		
       	}, async:false}
       	
       );	        
      } else {
     	idCliente.value = "";
      	nomeCompletoCliente.value = "";
      	documentoFormatado.value = "";
      	emailCliente.value = "";
     	}
	
	if($("#nome").val() != $("#nomeSolicitante").val() || $("#cpfCnpj").val() != $("#cpfCnpjSolicitante").val() || 
			$("#email").val() != $("#emailSolicitante").val() || $("#telefone").val() != $("#telefoneSolicitante").val() 
			|| $("#rg").val() != $("#rgSolicitante").val() ) {
		
		if( confirm("Deseja replicar os dados do cliente para o solicitante?") ){
			
			$("#nomeSolicitante").val($("#nome").val());
			$("#cpfCnpjSolicitante").val($("#cpfCnpj").val());
			$("#emailSolicitante").val($("#email").val());
			$("#telefoneSolicitante").val($("#telefone").val());
			$("#rgSolicitante").val($("#rg").val());
			
		}
	}
	
	$('#nome').focus();

}

function selecionarContatoCliente(idContato) {
	

	var nomeSolicitante = document.getElementById("nomeSolicitante");
	var emailSolicitante = document.getElementById("emailSolicitante");
	var telefoneSolicitante = document.getElementById("telefoneSolicitante");
	var rgSolicitante = document.getElementById("rgSolicitante");
	var cpfCnpjSolicitante = document.getElementById("cpfCnpjSolicitante");
	var tipoContatoSolicitante = document.getElementById("tipoContatoSolicitante");

	AjaxService.obterContatoClientePorId( idContato, {
           	callback: function(contato) {	           		
           		if(contato != null){           			           			
					
           			nomeSolicitante.value = contato["nomeContato"];
           			emailSolicitante.value = contato["emailContato"];
           			telefoneSolicitante.value = contato["telefone1Formatado"];
           			rgSolicitante.value = contato["rg"];
           			cpfCnpjSolicitante.value = contato["cpf"];
           			tipoContatoSolicitante.value = contato["idTipoContato"];
           			
           		}
        	}, async:false}
        	
        );        
}

function exibirPopupContatosPessoa() {
	
	var idCliente = $("#cliente").val();
	
	if(idCliente != null && idCliente > 0) {
    	popup = window.open('popupContatosPessoa?chavePrimaria='+idCliente,'popup','height=750,width=1080,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	} else {
		alert('Selecione um cliente para visualizar os seus contatos.');
	}
}

</script>
<div id="abaCliente">

<fieldset id="conteinerSobreDemanda">
	
	<legend>Cliente</legend>
	<div class="conteinerDados">	
	
	<input type="hidden" id="exibirPassaporte" name="exibirPassaporte" value="${exibirPassaporte}"/> 
	<input type="hidden" id="cliente" name="cliente" value="${chamado.cliente.chavePrimaria}"/> 
		<input type="hidden" id="chaveCliente" value="${cliente.chavePrimaria}" />
		<label class="rotulo" id="rotuloNome" for="nome">Nome:</label>
		<input class="campoTexto" type="text" id="nome" name="nome" maxlength="30" size="32" value="${cliente.nome}" ><br />
		<label class="rotulo" id="rotuloNomeFantasia" for="nomeFantasia">Nome Fantasia:</label>
		<input class="campoTexto" type="text" id="nomeFantasia" name="nomeFantasia" maxlength="30" size="32"  value="${cliente.nomeFantasia}" ><br />
		<label class="rotulo" id="rotuloCpf" for="cpfCnpj">CPF/CNPJ:</label>
		<input class="campoTexto" type="text" id="cpfCnpj" name="cpfCnpj" maxlength="19" size="32" onblur="carregarCliente(null,this.value,null,null)" value="<c:if test='${ not empty cliente.cnpj}'>${cliente.cnpjFormatado}</c:if><c:if test='${not empty cliente.cpf}'>${cliente.cpfFormatado}</c:if>" onkeypress="return formatarCampoInteiro(event);"/> <br />
		
		<label class="rotulo" id="rotuloRg" for="rg">RG:</label>
		<input class="campoTexto" type="text" id="rg" name="rg" maxlength="19" size="32" value="${cliente.rg}" onkeypress="return formatarCampoInteiro(event);"/> <br />
		
		<div id="divPassaporte" >
		<label class="rotulo" id="rotuloNumeroPassaporte" for="numeroPassaporte">Passaporte:</label>
		<input class="campoTexto" type="text" id="numeroPassaporte" name="numeroPassaporte" maxlength="20" size="32"  value="${cliente.numeroPassaporte}" ><br />
		</div>
		
		<label class="rotulo" id="rotuloEmail" for="email">Email:</label>
		<input class="campoTexto" type="text" id="email" name="email" maxlength="40" size="32" value="${cliente.emailPrincipal}" ><br />
		<label class="rotulo" id="rotuloTelefone" for="email">Telefone:</label>
		<input class="campoTexto" type="text" id="telefone" name="telefone" maxlength="30" size="32" value="${telefone}"><br />
		
		<c:if test="${ fluxoInclusao eq true}">
			<fieldset class="conteinerBotoesEsq">
				<input name="button" class="bottonRightCol2" title="Pesquisar Cliente"  value="Pesquisar Cliente" onclick="exibirPopupPesquisaCliente();" type="button">
				<input name="button" class="bottonRightCol2" value="Contatos Cliente" type="button" onclick="exibirPopupContatosPessoa();">		
			</fieldset>
		</c:if>
	</div>
	
</fieldset>


</div>