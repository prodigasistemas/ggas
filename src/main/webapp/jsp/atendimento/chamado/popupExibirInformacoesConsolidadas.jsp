<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<style>
.bootstrap {
	background: white;
}
</style>
 

<div class="bootstrap">
	<input type="hidden" id="nome" value="${nome}">
	<input type="hidden" id="isEmailEnviado" value="${isEmailEnviado}">
                   
	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Consolida��o de Dados da Pessoa</h5>
		</div>

		<div class="card-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-row">
						<label for="nome" class="col-sm-1,5">Nome:</label> <label
							id="nome" class="col-sm-10">${cliente.nome}</label>
					</div>
					<div class="form-row">
						<label for="nome" class="col-sm-1,5">Email:</label> <label
							id="email" class="col-sm-10">${cliente.emailPrincipal}</label>
					</div>
					<div class="form-row">
						<label for="nome" class="col-sm-2,5">Denegado:</label> <label
							id="denegado" class="col-sm-9">${cliente.denegadoFormatado}</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-row">
						<label for="nome" class="col-md-1,5">
							<c:if test="${cliente.cpf ne null}">CPF:</c:if>
							<c:if test="${cliente.cnpj ne null}">CNPJ:</c:if>
						</label>
						<label class="col-md-10" type="text" id="cpfOuCnpj" name="cpfOuCnpj">${cliente.numeroDocumentoFormatado}</label>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header">
					<ul class="nav nav-tabs card-header-tabs" id="tab-content"
						role="tablist">
						<li class="nav-item"><a class="nav-link active" href="#contentTabFaturamento"
							id="tab-faturamento" data-toggle="tab" aria-controls="contentTabFaturamento">Faturamento</a></li>
						<li class="nav-item"><a class="nav-link" href="#contentTabCobranca"
							id="tab-cobrancas" data-toggle="tab" aria-controls="contentTabCobranca">Cobran�as</a></li>
						<li class="nav-item"><a class="nav-link" href="#contentTabAgendamento"
							id="tab-agendamento" data-toggle="tab" aria-controls="contentTabAgendamento">Agendamento</a></li>
					</ul>
				</div>
				<div class="card-body">
					<div class="tab-content">
						<div class="tab-pane fade show active" id="contentTabFaturamento">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover" id="table-grid-agendamento" width="100%">
									<thead class="thead-ggas-bootstrap">
										<th scope="col" class="text-center">Nota Fiscal</th>
										<th scope="col" class="text-center">Per�odo de Consumo</th>
										<th scope="col" class="text-center">Consumo<br/>Apurado (m�)</th>
										<th scope="col" class="text-center">Valor Total</th>
										<th scope="col" class="text-center">Valor Atualizado</th>
										<th scope="col" class="text-center">Data de Vencimento</th>
										<th scope="col" class="text-center">Data de Pagamento</th>
										<th scope="col" class="text-center">Situa��o</th>
										<th scope="col" class="text-center">Contrato</th>
										<th scope="col" class="text-center">2� Via</th>
									</thead>
									<tbody>
										<c:forEach items="${listaDocFiscal}" var="docFiscal">
											<tr>
												<td class="text-center">
													<c:out value="${docFiscal.numeroDocumentoFiscalFormatado}"/>
												</td>
												<td class="text-center">
													<c:out value="${docFiscal.periodoConsumoFormatado}"/> 
												</td>
												<td class="text-center">
													<fmt:formatNumber value="${docFiscal.consumoApurado}" minFractionDigits="${escalaConsumoApurado}"/>
												</td>
												<td class="text-center">
													<fmt:formatNumber type="currency" value="${docFiscal.valorTotal}"/>
												</td>
												<td class="text-center">
													<fmt:formatNumber type="currency" value="${docFiscal.valorAtualizado}"/>
												</td>
												<td class="text-center">
													<c:out value="${docFiscal.dataVencimentoFormatada}" />
												</td>
												<td class="text-center">
													<c:out value="${docFiscal.dataPagamentoFormatada}"/>
												</td>
												<td class="text-center">
													<c:out value="${docFiscal.situacaoDoPagamento}"/>
												</td>
												<td class="text-center">
													<c:out value="${docFiscal.numeroContratoFormatado}"/>
												</td>
												<td class="text-center" style="min-width: 105px">
				                                    <a id="enviarEmailChamadoFaturamento" name="button" class="btn btn-primary btn-sm mb-1 mr-1" type="submit" title="Enviar Email"
														href="enviarEmailChamadoFaturamento?email=${cliente.emailPrincipal}&idFatura=${docFiscal.faturaID}&idDocumentoFiscal=${docFiscal.docFiscalID}"> 
														<i class="fa fa-envelope"></i>
				                                    </a>
				                                    <a id="imprimirSegundaViaFatura"  class="btn btn-primary btn-sm mb-1 mr-1" type="submit" title="Imprimir Segunda Via"
														href="imprimirSegundaViaFatura?idFatura=${docFiscal.faturaID}&idDocumentoFiscal=${docFiscal.docFiscalID}"> <i class="fa fa-print"></i>
				                                    </a>
				                               </td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<div>
								<c:out value="Valor total Atualizado: "/><fmt:formatNumber type="currency" value="${valorTotalAtualizadoFaturamento}"/>
							</div>
						</div>
						
						<div class="tab-pane fade" id="contentTabCobranca">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover">
									<thead class="thead-ggas-bootstrap">
										<th scope="col" class="text-center">Tipo de Documento</th>
										<th scope="col" class="text-center">N�mero do Documento</th>
										<th scope="col" class="text-center">Data de Emiss�o</th>
										<th scope="col" class="text-center">Valor Total</th>
										<th scope="col" class="text-center">Valor atualizado</th>
										<th scope="col" class="text-center">Data de Vencimento</th>
										<th scope="col" class="text-center">Data de Pagamento</th>
										<th scope="col" class="text-center">Situa��o</th>
										<th scope="col" class="text-center">2� Via</th>
									</thead>
									<tbody>
										<c:forEach items="${listaDocCobranca}" var="docCobranca">
											<tr>
												<td class="text-center">
													<c:out value="${docCobranca.tipoDocDescricao}"/>
												</td>
												<td class="text-center">
													<c:out value="${docCobranca.chavePrimaria}"/>
												</td>
												<td class="text-center">
													<c:out value="${docCobranca.dataEmissaoFormatada}"/>
												</td>
												<td class="text-center">
													<fmt:formatNumber type="currency" value="${docCobranca.valorTotal}"/>
												</td>
												<td class="text-center">
													<fmt:formatNumber type="currency" value="${docCobranca.valorAtualizado}"/>
												</td>
												<td class="text-center">
													<c:out value="${docCobranca.dataVencimentoFormatada}"/>
												</td>
												<td class="text-center">
													<c:out value="${docCobranca.dataPagamentoFormatada}"/>
												</td>
												<td class="text-center">
													<c:out value="${docCobranca.situacaoDescricao}"/>
												</td>
												<td class="text-center" style="min-width: 105px">
				                                    <a id="enviarEmailChamadoCobranca"  class="btn btn-primary btn-sm mb-1 mr-1" type="submit" title="Enviar Email"
														href="enviarEmailChamadoCobranca?email=${cliente.emailPrincipal}&chavePrimaria=${cliente.chavePrimaria}&idFatura=${docCobranca.faturaID}&idDocumentoCobranca=${docCobranca.chavePrimaria}"> <i class="fa fa-envelope"></i>
				                                    </a>
				                                    <a id="imprimirSegundaViaCobranca"  class="btn btn-primary btn-sm mb-1 mr-1" type="submit" title="Imprimir Segunda Via"
														href="imprimirSegundaViaCobranca?chavePrimaria=${cliente.chavePrimaria}&idFatura=${docCobranca.faturaID}&idDocumentoCobranca=${docCobranca.chavePrimaria}"> <i class="fa fa-print"></i>
				                                    </a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<div>
								<c:out value="Valor total Atualizado: "/><fmt:formatNumber type="currency" value="${valorTotalAtualizadoCobranca}"/>
							</div>
						</div>
						
						<div class="tab-pane fade" id="contentTabAgendamento">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover" id="table-grid-agendamento" width="100%" >
									<thead class="thead-ggas-bootstrap">
										<th scope="col" class="text-center">Tipo de Servi�o</th>
										<th scope="col" class="text-center">Data de Abertura</th>
										<th scope="col" class="text-center">Data de Agendamento</th>
										<th scope="col" class="text-center">Turno</th>
										<th scope="col" class="text-center">Equipe</th>
										<th scope="col" class="text-center">Protocolo</th>
										<th scope="col" class="text-center">Status</th>
									</thead>
									
									<tbody>
										<c:forEach items="${servAutorizAgendas}" var="agenda">
											<tr>
												<td class="text-center">
													<c:out value="${agenda.tipoServicoDescricao}"/>
												</td>
												<td class="text-center">
													<c:out value="${agenda.dataAberturaFormatada}"/>
												</td>
												<td class="text-center">
													<c:out value="${agenda.dataAgendamentoFormatada}"/>
												</td>
												<td class="text-center">
													<c:out value="${agenda.turnoDescricao}"/>
												</td>
												<td class="text-center">
													<c:out value="${agenda.equipeDescricao}"/>
												</td>
												<td class="text-center">
													<c:out value="${agenda.numeroProtocolo}"/>
												</td>
												<td class="text-center">
													<c:out value="${agenda.status}"/>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="card-footer">
			<div class="row">
				<div class="col-sm-12">
					
				</div>
			</div>
		</div>
	</div>

</div>
<script>
$(document).ready(function(){ 
var asd = $("#isEmailEnviado").val();
	enviarEmailChamadoCobranca(asd);
})
function enviarEmailChamadoCobranca(asd){
	if(asd === 'true'){
		alert("E-mail enviado com sucesso!");
	}
}


</script>

<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/incluirChamado/index.js" type="application/javascript"></script>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/chamado/incluirChamado/index.js" type="application/javascript"></script>
<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/pesquisarChamado/exibirPesquisaChamado/index.js"></script>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/chamado/exibirPesquisaChamado/index.js" type="application/javascript"></script>