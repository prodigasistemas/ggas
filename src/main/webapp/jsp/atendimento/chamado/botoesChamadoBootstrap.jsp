<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<div class="mb-1">
    <vacess:vacess param="exibirReiteracaoChamado">
        <button name="buttonReiterar" id="abrirReiteracao" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirReiteracaoChamado()"><i class="fa fa-exchange-alt"></i> Reiterar
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirAlteracaoChamado">
        <button name="buttonAlterar" id="abrirAlteracao" type="button" class="btn btn-primary btn-sm  mb-1 mr-1"
                onclick="exibirAlterarChamado()"><i class="fa fa-pencil-alt"></i> Alterar
        </button>
    </vacess:vacess>
    <vacess:vacess param="copiarChamado">
        <button name="buttonCopiar" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="copiarChamado()"><i class="fa fa-clone"></i> Copiar
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirReaberturaChamado">
        <button name="buttonReabrir" id="abrirReabertura" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirReaberturaChamado()"><i class="fa fa-sync"></i> Reabrir
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirTramitacaoChamado">
        <button name="buttonTramitar" id="abrirTramitacao" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirTramitacaoChamado()"><i class="far fa-share-square"></i> Tramitar
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirCapturarChamado">
        <button name="buttonCapturar" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirCapturarChamado()"><i class="far fa-check-square"></i> Capturar
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirReativarChamado">
        <button name="buttonReativar" id="abrirReativacao" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirReativacaoChamado()"><i class="fa fa-check-circle"></i> Reativar
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirEncerrarChamado">
        <button name="buttonEncerrar" id="abrirEncerramento" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirEncerrarChamado()"><i class="far fa-check-circle"></i> Encerrar
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirEncerrarChamado">
        <button name="buttonEncerrar" id="abrirEncerramento" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirEncerrarEmLoteChamado()"><i class="fa fa-check-double"></i> Encerrar em Lote
        </button>
    </vacess:vacess>
    <vacess:vacess param="imprimirChamado">
        <button name="buttonImprimir" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="imprimirChamadoDetalhar()"><i class="fa fa-print"></i> Imprimir
        </button>
    </vacess:vacess>
</div>

<div class="mt-1">
    <vacess:vacess param="gerarAutorizacaoServico">
        <button name="botaoGerarServicoAutorizacao" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirDialogGerarAutorizacaoServico()"><i class="fa fa-gavel"></i> Gerar Autoriza��o de Servi�o
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirResponderQuestionario">
        <button name="buttonResponderQuestionario" id="buttonResponderQuestionario" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirResponderQuestionario()"><i class="fa fa-pen-square"></i> Responder Question�rio
        </button>
    </vacess:vacess>
    <vacess:vacess param="exibirAlteracaoCadastroClienteChamado">
        <button name="buttonAlterarCadastroCliente" id="buttonAlterarCadastroCliente" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirAlteracaoCadastroClienteChamado()"><i class="fa fa-id-card"></i> Alterar Cadastro Cliente
        </button>
    </vacess:vacess>
    <!-- <vacess:vacess param="exibirSolicitacaoAlteracaoDataVencimento"> -->
    <!-- <input name="buttonAlterarDataVencimento" id="buttonAlterarDataVencimento" value="Alterar Data de Vencimento" class="bottonRightCol" onclick="exibirSolicitacaoAlteracaoDataVencimento()" type="button"> -->
    <!-- </vacess:vacess> -->
    <c:if test="${isAdministradorAtendimento ne null && isAdministradorAtendimento eq true}">
        <button name="buttonAlterarDataPrev" id="buttonAlterarDataPrev" type="button" class="btn btn-primary btn-sm mb-1 mr-1"
                onclick="exibirSolicitacaoAlteracaoDataPrev()"><i class="fa fa-calendar-alt"></i> Alterar Data Previs�o de Encerramento
        </button>
    </c:if>
</div>
