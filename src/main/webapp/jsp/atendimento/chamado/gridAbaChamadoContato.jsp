<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir='/WEB-INF/tags' prefix='ggas'%>


<fieldset class="conteinerBloco">
<legend class="conteinerBlocoTitulo">Contatos</legend>

<c:set var="indexListaChamadoContato" value="0" />
<display:table class="dataTableGGAS dataTableAba" name="listaChamadoContato" sort="list" id="contato" pagesize="15" decorator="br.com.ggas.util.DisplayTagGenericoDecorator"  excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#" >
	<display:column property="tipoContato.descricao" sortable="false" title="Tipo" style="width: 60px" />
	<display:column property="nome" sortable="false"  title="Nome" />
	<display:column property="codigoDDD" sortable="false"  title="DDD" style="width: 24px" />
	<display:column property="fone" sortable="false"  title="Telefone" style="width: 65px" />
	<display:column property="ramal" sortable="false"  title="Ramal" style="width: 40px" />
	
	<display:column sortable="false" title="Cargo" style="width: 80px">
		${contato.profissao.descricao}
	</display:column>
	
	<display:column sortable="false" title="�rea" style="width: 150px">
		<div style="width: 140px; word-wrap: break-word; overflow: hidden">${contato.descricaoArea}</div>
	</display:column>
	
	<display:column sortable="false" title="Email" style="width: 190px">
		<div style="width: 180px; word-wrap: break-word; overflow: hidden">${contato.email}</div>
	</display:column>
	
<c:if test="${ fluxoDetalhamento ne true }">	
	<display:column sortable="false" title="Principal" style="width: 25px">
		<c:choose>
	  		<c:when test="${contato.principal}">
	  			Sim <a href="javascript:atualizarContatoPrincipal(<c:out value="${indexListaChamadoContato}"/>);"></a>
	  		</c:when>
	  		<c:otherwise>	  			
	  				N�o <a href="javascript:atualizarContatoPrincipal(<c:out value="${indexListaChamadoContato}"/>);"><img alt="Tornar Contato Principal" title="Tornar Contato Principal" src="<c:url value="/imagens/user_business_boss.png"/>" border="0" /></a>
			</c:otherwise>
	  	</c:choose>
	</display:column>
      
	<display:column class="colunaSemTitulo" style="text-align: center; width: 25px"> 
		<a href="javascript:exibirAlteracaoDoContato('${indexListaChamadoContato}','${contato.tipoContato.chavePrimaria},${contato.tipoContato.descricao}','${contato.nome}','${contato.codigoDDD}','${contato.fone}','${contato.ramal}','${contato.profissao.chavePrimaria},${contato.profissao.descricao}','${contato.descricaoArea}','${contato.email}','${contato.chavePrimaria}');"><img title="Alterar contato" alt="Alterar contato"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0"></a> 
	</display:column>
	
	<display:column style="text-align: center; width: 25px">
		<a onclick="return confirm('Deseja excluir o contato?');" href="javascript:removerContato(<c:out value="${indexListaChamadoContato}"/>);"><img title="Excluir contato" alt="Excluir contato"  src="<c:url value="/imagens/deletar_x.png"/>" border="0" /></a>
	</display:column>
	</c:if>
	<c:set var="indexListaChamadoContato" value="${indexListaChamadoContato+1}" />
    </display:table>

</fieldset>


