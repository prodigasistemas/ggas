<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>


<script type="text/javascript">
	function gerar(){
		submeter('extratoDebitoForm', 'gerarExtratoQuitacaoAnualChamado');
	}
</script>

<form:form method="post" name="extratoDebitoForm" action="gerarExtratoQuitacaoAnualChamado">
	<input type="hidden" name="idPontoConsumo" id="idPontoConsumo"  value="${pontoConsumo.chavePrimaria}"/>
	<div id="divQuitacaoAnual">
		<fieldset class="conteinerDados3">
			<legend>Declara��o de Quita��o Anual</legend>
			<hr class="linhaSeparadoraPesquisa" />
			<display:table class="dataTableGGAS dataTablePesquisa"
				decorator="br.com.ggas.util.DisplayTagGenericoDecorator"
				name="listaContratoPontoConsumos" sort="list"
				id="contratoPontoConsumo" pagesize="15"
				excludedParams="org.apache.struts.taglib.html.TOKEN acao"
				requestURI="pesquisarPontosConsumo">
				<display:column property="contrato.numero" sortable="false"
					title="Contrato" style="width: 120px" />
				<display:column sortable="false" title="Im�vel - Ponto de Consumo"
					headerClass="tituloTabelaEsq" style="text-align: left">
					<c:out value='${contratoPontoConsumo.pontoConsumo.imovel.nome}' />
					<c:out value=" - " />
					<c:out value="${contratoPontoConsumo.pontoConsumo.descricao}" />
				</display:column>
				<display:column sortable="false" title="Situa��o"
					style="width: 100px">
					<c:choose>
						<c:when
							test="${contratoPontoConsumo.pontoConsumo.habilitado eq true}">
							<c:out value='Ativo' />
						</c:when>
						<c:otherwise>
							<c:out value='Inativo' />
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column property="pontoConsumo.segmento.descricao"
					sortable="false" title="Segmento" style="width: 100px" />
				<display:column property="pontoConsumo.ramoAtividade.descricao"
					sortable="false" title="Ramo de Atua��o" style="width: 150px" />
			</display:table>
		
			<br/>
			
			<fieldset id="conteinerInferiorPesquisarExtratoQuitacaoAnual"
				style="padding-left: 0">
	
				<label class="rotulo campoObrigatorio" for="anoGeracao"><span
					class="campoObrigatorioSimbolo">*</span>Ano para Gera��o:</label> <select
					name="anoGeracao" id="anoGeracao" class="campoSelect">
					<option value="">Selecione</option>
					<c:forEach items="${listaAnos}" var="ano">
						<option value="<c:out value="${ano}"/>" />
						<c:out value="${ano}" />
						</option>
					</c:forEach>
				</select>
				<fieldset class="conteinerBotoesPesquisarDirFixo">
					<vacess:vacess param="pesquisarPontosConsumoAnual">
						<input id="botaoPesquisarGerar"
							class="bottonRightCol2 bottonRightColUltimo" type="button"
							value="Gerar" align="right" onclick="gerar();" />
					</vacess:vacess>
				</fieldset>
				
			</fieldset>
	
		</fieldset>
	</div>
</form:form>
