<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>
$(document).ready(function(){
	$("#dataPrevisaoEncerramento").inputmask("d/m/y 99:99",{placeholder:"_"});
	$("#dataPrevisaoEncerramento").datetimepicker({minDate: '+0d', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', timeFormat:'HH:mm'});
});
</script>

<form:form action="alterarDataPrevisaoEncerramento" id="chamadoDataEncerramentoForm" name="chamadoDataEncerramentoForm" method="post" >
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<label class="rotulo" for="mensagem"><span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
	<textarea  class="campoTexto" name="descricao" id="descricao" cols="35" rows="6" maxlength="1000" onblur="this.value = removerEspacoInicialFinal(this.value);"
		onkeypress="return formatarCampoTextoLivreComLimite(event,this,800);" onpaste="return formatarCampoTextoLivreComLimite(event,this,800);"></textarea>
	<br/>
				
	<label class="rotulo2Linhas" style="margin-left: 16px;margin-top: -2px" id="rotuloMotivo" for="idMotivo">
		<span id="spanMotivo" class="campoObrigatorioSimbolo">* </span> Motivo: 
	</label>
	<select id="idMotivo" class="campoSelect" name="idMotivo">
	<option value="-1">Selecione</option>
		<c:forEach items="${listaMotivos}" var="motivo">
			<option value='<c:out value="${motivo.chavePrimaria}"/>' <c:if test="${idMotivo == motivo.chavePrimaria}">selected="selected"</c:if>>
				<c:out value="${motivo.descricao}"/>
			</option>
		</c:forEach>
	</select><br/>
	
	<label class="rotulo" id="rotuloDataPrevisaoEncerramento" for="dataPrevisaoEncerramento"><span id="spanDescricao" class="campoObrigatorioSimbolo">* </span> Previs�o de Encerramento:</label>
	<input class="campoDataHora campo2Linhas campoHorizontal" type="text" id="dataPrevisaoEncerramento" name="dataPrevisaoEncerramento" maxlength="16" size="16" style="margin-top: 6px">
		
	<div style="float: right; padding-bottom: 10px;">
		<input name="alterarDataEncerramento" class="bottonRightCol2 botaoGrande1" id="botaoAlterarData" value="Alterar" type="button" onclick="alterarDataEncerramento();">
	</div>
</form:form>