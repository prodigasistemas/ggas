<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir='/WEB-INF/tags' prefix='ggas' %>


<form:form id="contatoForm" name="contatoForm" method="post" modelAttribute="ClienteContatoTO">
    <input type="hidden" id="chavePrimariaContato" name="chavePrimariaContato"/>
    <input name="acaoRemocaoContato" type="hidden" id="acaoRemocaoContato">
    <input name="indexListaContatos" type="hidden" id="indexListaContatos">

    <div class="shadow-sm bg-light mt-3 p-3">
        <div class="form-row">
            <div class="col-sm-12 col-md-6">
                <label for="tipoContato">Tipo do contato: <span class="text-danger">*</span></label>
                <select name="tipoContato" id="tipoContato" class="form-control form-control-sm">
                    <option value="-1">Selecione</option>
                    <c:forEach items="${listaTipoContato}" var="contatoTipo">
                        <option value="<c:out value="${contatoTipo.chavePrimaria}"/>"
                                <c:if test="${clienteTO.clienteContatoTO.tipoContato.chavePrimaria == contatoTipo.chavePrimaria}">selected="selected"</c:if>>
                            <c:out value="${contatoTipo.descricao}"/>
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-sm-12 col-md-6">
                <label for="nomeContato">Nome do contato: <span class="text-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" id="nomeContato" name="nomeContato"
                       maxlength="20"
                       onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                               value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
                       value="${clienteTO.clienteContatoTO.nome}">
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-2 col-sm-12">
                <label for="codigoDDD">DDD:</label>
                <input class="form-control form-control-sm" type="text" id="codigoDDD" name="codigoDDD"
                       min="0" type="number"
                       onkeypress="return formatarCampoInteiro(event);" maxlength="2"
                       value="${clienteTO.clienteContatoTO.codigoDDD}">
            </div>

            <div class="col-md-8 col-sm-12">
                <label for="foneContato">Telefone:</label>
                <input class="form-control form-control-sm" type="text" id="foneContato" name="foneContato"
                       maxlength="9" onkeypress="return formatarCampoInteiro(event)"
                       value="${clienteTO.clienteContatoTO.fone}">
            </div>
            <div class="col-md-2 col-sm-12">
                <label for="ramalContatoBootstrap">Ramal:</label>
                <input class="form-control form-control-sm" min="0" max="9999" type="number" id="ramalContatoBootstrap" name="ramalContato"
                       maxlength="4"
                       onkeypress="return formatarCampoInteiro(event)" value="">
            </div>
        </div>

        <hr/>

        <div class="form-row">
            <div class="col-md-6 col-sm-12">
                <label for="profissaoContato">Cargo:</label>
                <select name="profissaoContato" id="profissaoContato" class="form-control form-control-sm">
                    <option value="-1">Selecione</option>
                    <c:forEach items="${listaTipoCargo}" var="contatoProfissao">
                        <option value="<c:out value="${contatoProfissao.chavePrimaria}"/>"
                                <c:if test="${clienteTO.clienteContatoTO.profissao.chavePrimaria == contatoProfissao.chavePrimaria}">selected="selected"</c:if>>
                            <c:out value="${contatoProfissao.descricao}"/>
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-6 col-sm-12">
                <label for="descricaoArea">�rea:</label>
                <input class="form-control form-control-sm" type="text" id="descricaoArea" name="descricaoArea"
                       maxlength="50"
                       onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out
                               value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
                       value="${clienteTO.clienteContatoTO.descricaoArea}"/>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-12">
                <label for="emailContato">E-mail do contato:</label>
                <input class="form-control form-control-sm" type="email" id="emailContato" name="emailContato" maxlength="80"
                       value="${clienteTO.clienteContatoTO.email}">
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-12">
                <span class="text-danger">*</span> campos obrigat�rios apenas para cadastrar Contatos.
            </div>
        </div>

        <div class="form-row mt-3">
            <div class="col-md-12">
                <button class="btn btn-danger btn-sm mt-1" name="botaoLimparContato" id="botaoLimparContato"
                        type="button">
                    <i class="fa fa-times-circle"></i> Limpar
                </button>
                <button class="btn btn-primary btn-sm mt-1" name="botaoIncluirContato" id="botaoIncluirContato"
                        type="button">
                    <i class="fa fa-plus-circle"></i> Adicionar contato
                </button>
                <button class="btn btn-primary btn-sm mt-1" disabled="disabled" name="botaoAlterarContato"
                        id="botaoAlterarContato"
                        type="button">
                    <i class="fa fa-edit"></i> Alterar contato
                </button>
            </div>
        </div>

        <div id="gridAbaChamadoContato" class="mt-3">
            <jsp:include page="/jsp/atendimento/chamado/bootstrap/gridAbaChamadoContatoBootstrap.jsp"></jsp:include>
        </div>
    </div>

</form:form>	
