<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<script type="text/javascript">
	document.addEventListener('keypress', function(e){
		if(e.which == 13){
	    	var cpfCnpj = document.getElementById("cpfCnpj").value;

			if(cpfCnpj != 0 && cpfCnpj.length >= 14 && cpfCnpj != ""){
				pesquisarCpfCnpj();
			}
		}	
	}, false);
	
    function selecionarContatoCliente(idContato) {


        var nomeSolicitante = document.getElementById("nomeSolicitante");
        var emailSolicitante = document.getElementById("emailSolicitante");
        var telefoneSolicitante = document.getElementById("telefoneSolicitante");
        var rgSolicitante = document.getElementById("rgSolicitante");
        var cpfCnpjSolicitante = document.getElementById("cpfCnpjSolicitante");
        var tipoContatoSolicitante = document.getElementById("tipoContatoSolicitante");

        AjaxService.obterContatoClientePorId(idContato, {
                callback: function (contato) {
                    if (contato != null) {

                        nomeSolicitante.value = contato["nomeContato"];
                        emailSolicitante.value = contato["emailContato"];
                        telefoneSolicitante.value = contato["telefone1Formatado"];
                        rgSolicitante.value = contato["rg"];
                        cpfCnpjSolicitante.value = contato["cpf"];
                        tipoContatoSolicitante.value = contato["idTipoContato"];

                    }
                }, async: false
            }
        );
    }

    function exibirPopupContatosPessoa() {

        var idCliente = $("#cliente").val();

        if (idCliente != null && idCliente > 0) {
            popup = window.open('popupContatosPessoa?chavePrimaria=' + idCliente, 'popup', 'height=750,width=1080,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
        } else {
            alert('Selecione um cliente para visualizar os seus contatos.');
        }
    }

    function exibirPopupInformacoesConsolidadas() { 
    	var idCliente = $("#cliente").val();
    	if (idCliente != null && idCliente > 0) {
    		popup = window.open('popupExibirInformacoesConsolidadas?chavePrimaria=' + idCliente, 'popup', 'height=750, width=1000, toolbar=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no ,modal=yes');
    	} else {
    		alert('Selecione um cliente para visualizar suas informa��es.');
    	}
    }
    
    function pesquisarCpfCnpj(){
    	var idCliente = document.getElementById("chaveCliente");
    	var nomeCompletoCliente = document.getElementById("nome");
    	var nomeFantasia = document.getElementById("nomeFantasia");
    	var cpfCnpj = document.getElementById("cpfCnpj").value;
    	var rg = document.getElementById("rg");
    	var emailCliente = document.getElementById("email");
    	var telefone = document.getElementById("telefone");
    	var numeroPassaporte = document.getElementById("numeroPassaporte");

    	if(cpfCnpj != null || cpfCnpj != 0){
//     		if(cpfCnpj.length >= 14){
    			AjaxService.carregarClientePorCpfCnpj( cpfCnpj, {
    	          	callback: function(cliente) {	           		
    	          		if(cliente != null){
    	               		idCliente.value = cliente["chavePrimaria"];
	    	               	nomeCompletoCliente.value = cliente["nome"];
	    	               	emailCliente.value = cliente["email"];
	    	               	$('form[name="notaDebitoCreditoForm"]').change();
	    	          		telefone.value = cliente["telefone"];
	    	          		numeroPassaporte.value = cliente["numeroPassaporte"];
	    	          		nomeFantasia.value = cliente["nomeFantasia"];
	    	          		rg.value = cliente["rg"];
	    	          		if(nomeCompletoCliente.value == ""){
	    	          			alert("CPF/CNPJ n�o cadastrado ou Inv�lido!");
	    	          		}else{
	    	          			if($("#nome").val() != $("#nomeSolicitante").val() || $("#cpfCnpj").val() != $("#cpfCnpjSolicitante").val() || 
	    	        					$("#email").val() != $("#emailSolicitante").val() || $("#telefone").val() != $("#telefoneSolicitante").val() 
	    	        					|| $("#rg").val() != $("#rgSolicitante").val()) {
	    	          				if( confirm("Deseja replicar os dados do cliente para o solicitante?") ){
	    	        					$("#nomeSolicitante").val($("#nome").val());
	    	        					$("#cpfCnpjSolicitante").val($("#cpfCnpj").val());
	    	        					$("#emailSolicitante").val($("#email").val());
	    	        					$("#telefoneSolicitante").val($("#telefone").val());
	    	        					$("#rgSolicitante").val($("#rg").val());
	    	        				}else{
	    	        					$("#nomeSolicitante").val("");
	    	        					$("#cpfCnpjSolicitante").val("");
	    	        					$("#emailSolicitante").val("");
	    	        					$("#telefoneSolicitante").val("");
	    	        					$("#rgSolicitante").val("");
		    	        			}
	    	        			}
//  	    	        			$('#nome').focus();
	    	          		}
    	              	}
    	          		
    	       	},
    	       	async:false});
//     		}
    	}else{
    		alert("Informe um cpf ou cnpj v�lido.");
    		idCliente.value = "";
          	nomeCompletoCliente.value = "";
          	nomeFantasia.value = "";
          	rg.value = "";
          	numeroPassaporte.value = "";
          	emailCliente.value = "";
          	telefone.value = "";
    	}
    }
</script>


<div id="abaCliente">
    <div class="form-row mb-2">

        <input type="hidden" id="exibirPassaporteCliente" name="exibirPassaporte" value="${exibirPassaporte}"/>
        <input type="hidden" id="cliente" name="cliente" value="${chamado.cliente.chavePrimaria}"/>
        <input type="hidden" id="chaveCliente" value="${cliente.chavePrimaria}"/>

        <div class="col-md-12">

            <label for="nome">Nome:</label>
            <input class="form-control form-control-sm" type="text" id="nome" name="nome" maxlength="30" size="32"
                   value="${cliente.nome}" onkeypress=" return letraMaiuscula(this)">
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12">
            <label for="nomeFantasia">Nome Fantasia:</label>
            <input class="form-control form-control-sm" type="text" id="nomeFantasia" name="nomeFantasia" maxlength="30"
                   size="32" value="${cliente.nomeFantasia}" onkeypress=" return letraMaiuscula(this)">
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12">
            <label for="cpfCnpj">CPF/CNPJ:</label>
            <input class="form-control form-control-sm cpfOuCnpj" type="text" id="cpfCnpj" name="cpfCnpj" maxlength="19" size="32"
                   value="<c:if test='${ not empty cliente.cnpj}'>${cliente.cnpjFormatado}</c:if><c:if test='${not empty cliente.cpf}'>${cliente.cpfFormatado}</c:if>"
                   onclick="mascaraCampoCpfCnpj(this);" />
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12">
            <label for="rg">RG:</label>
            <input class="form-control form-control-sm rg" type="text" id="rg" name="rg" maxlength="19" size="32"
                   value="${cliente.rg}"  onkeypress=" return letraMaiuscula(this)" onclick="mascaraCampoRg(this.value);"/>
        </div>
    </div>

    <div class="form-row" id="divPassaporte">
        <div class="col-md-12">
            <label for="numeroPassaporte">Passaporte:</label>
            <input class="form-control form-control-sm" type="text" id="numeroPassaporte" name="numeroPassaporte"
                   maxlength="20" size="32" value="${cliente.numeroPassaporte}">
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12">
            <label for="email">Email:</label>
            <input class="form-control form-control-sm" type="email" id="email" name="email" maxlength="40" size="32"
                   value="${cliente.emailPrincipal}">
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12">
            <label for="email">Telefone:</label>
            <input class="form-control form-control-sm telefone" type="phone" id="telefone" name="telefone" maxlength="11"
                   size="32" value="${telefone}" onkeypress=" return formatarCampoInteiro(event)" onclick="mascaraCampoTelefone();">
        </div>
    </div>

    <c:if test="${ fluxoInclusao eq true}">
        <div class="form-row mt-2">
            <div class="col-md-12">
                <button name="button" class="btn btn-primary btn-sm mt-1" title="Pesquisar Cliente"
                        id="abaCliente_pesquisarCliente"
                        type="button">
                    <i class="fa fa-search-plus"></i> Pesquisar Cliente
                </button>
                <button name="button" class="btn btn-primary btn-sm mt-1" type="button"
                        onclick="exibirPopupContatosPessoa();">
                    <i class="fa fa-user-friends"></i> Contatos Cliente
                </button>
                <button name="buttons" class="btn btn-primary btn-sm mt-1" type="button"
                        onclick="exibirPopupInformacoesConsolidadas();">
                    <i class="fa fa-user-friends"></i> Informa��es da Pessoa
                </button>
            </div>
        </div>

    </c:if>
</div>
