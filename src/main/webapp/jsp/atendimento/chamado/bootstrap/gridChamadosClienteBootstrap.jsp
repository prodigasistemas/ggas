<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<div class="text-center loading">
	<img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive">

	<h5 class="card-title">Chamados do Cliente</h5>
	<table class="table table-bordered table-striped table-hover" id="table-grid-chamado-cliente" width="100%"
		   style="opacity: 0;">
		<thead class="thead-ggas-bootstrap">
		<tr>
			<th scope="col" class="text-center"><input type='checkbox' name='checkAllAuto' id='checkAllAuto'/></th>
			<th scope="col" class="text-center">Data</th>
			<th scope="col" class="text-center">Protocolo</th>
			<th scope="col" class="text-center">Tipo do Chamado</th>
			<th scope="col" class="text-center">Assunto do Chamado</th>
			<th scope="col" class="text-center">Status</th>
			<th scope="col" class="text-center">E-mail</th>
		</tr>
		</thead>
		<tbody>
		<c:forEach items="${chamadosCliente}" var="chamado">
			<tr>
				<td>
					<input type="checkbox" name="chavesPrimarias" value="${chamado.chavePrimaria}">
				</td>
				<td>
					<a href='javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
						<fmt:formatDate value="${chamado.ultimaAlteracao}" pattern="dd/MM/yyyy HH:mm" />
					</a>
				</td>
				<td>
					<a href='javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
						<c:out value='${chamado.protocolo.numeroProtocolo}'/>
					</a>
				</td>
				<td>
					<a href='javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
						<c:out value='${chamado.chamadoAssunto.chamadoTipo.descricao}'/>
					</a>
				</td>
				<td>
					<a href='javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
						<c:out value="${chamado.chamadoAssunto.descricao}" />
					</a>
				</td>
				<td>
					<a href='javascript:detalharChamado(<c:out value='${chamado.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
						<c:out value="${chamado.status.descricao}" />
					</a>
				</td>
				<td class="text-center">
                    <a href="javascript:enviarEmail(${chamado.protocolo.numeroProtocolo},'${chamado.cliente.emailPrincipal}','${chamado.chamadoAssunto.descricao}','${chamado.protocolo.ultimaAlteracao}','${chamado.dataPrevisaoEncerramento}', '${chamado.status.descricao}', '${chamado.nomeSolicitante}')(<c:out value="${i}"/>);"><img title="Enviar protocolo" alt="Enviar protocolo"" src="<c:url value="/images/email.png"/>" height="25" width="25" border="25"/></a>
                </td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>


<button name="Button" class="btn btn-primary btn-sm mt-1" type="button" onclick="exibirAlterarChamadoGrid();">
	<i class="fa fa-edit"></i> Alterar
</button>

<button name="Button" class="btn btn-primary btn-sm mt-1" type="button" onclick="copiarChamadoGrid();">
	<i class="fa fa-copy"></i> Copiar
</button>
