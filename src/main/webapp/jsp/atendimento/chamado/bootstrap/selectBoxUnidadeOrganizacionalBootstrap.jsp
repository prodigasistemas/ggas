<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

    $(document).ready(function () {
        if ($("#unidadeOrganizacional").val() != null) {
            desabilitarUnidadeVisualizadora();
        }

        if ($('#fluxoDetalhamento').val() == 'true') {
            $("#unidadeOrganizacional").attr('disabled', 'disabled');
        } else {
            $("#unidadeOrganizacional").removeAttr('disabled');
        }
        
        var unidadeOrganizacional = $("#unidadeOrganizacional").val();
        if(unidadeOrganizacional != ""){
        	carregarResponsavel(unidadeOrganizacional);
        }
    });

    function desabilitarUnidadeVisualizadora() {
        var idUO = $("#unidadeOrganizacional :selected").val();
        $("#unidadeOrganizacionalVisualizadoras option").attr('disabled', false);
        $("#unidadeOrganizacionalVisualizadoras option").each(function () {
            if ($(this).val() == idUO) {
                $(this).attr('disabled', true);
            }
        });
    };
    
</script>

<label for="unidadeOrganizacional">Unidade organizacional:<span class="text-danger">*</span></label>


    <select id="unidadeOrganizacional" name="unidadeOrganizacional" class="form-control form-control-sm" onclick="carregarResponsavel(this.value);desabilitarUnidadeVisualizadora();">
        <option value="">Selecione uma unidade...</option>
        <c:forEach items="${listaUnidadeOrganizacional}" var="unidadeOrganizacional">
            <option title="${unidadeOrganizacional.descricao}" value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>"
                    <c:if test="${unidade == unidadeOrganizacional.chavePrimaria}">selected="selected"</c:if>>
                <c:out value="${unidadeOrganizacional.descricao}"/>
            </option>
        </c:forEach>
    </select>




