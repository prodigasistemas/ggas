<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1"%>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>


<script>

	$(document).ready(
			function() {
				$('#selecionarTudo').change(
						function() {
							$('input[name="numeroTitulo"]').prop('checked',
									$(this).prop('checked'));
						});
			});
</script>

<div class="text-center loading">
	<img src="${pageContext.request.contextPath}/imagens/loading.gif"
		class="img-responsive" />
</div>

<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover"
		id="table-grid-titulos" width="100%" style="opacity: 0;">
		<thead class="thead-ggas-bootstrap">
			<tr>
				<th scope="col" class="text-center">
					<div class="custom-control custom-checkbox custom-control-inline ">
						<input id="selecionarTudo" type="checkbox" name="selecionarTudo"
							class="custom-control-input"> <label
							class="custom-control-label p-0" for="selecionarTudo"></label>
					</div>
				</th>
				<th scope="col" class="text-center">N�mero T�tulo</th>
				<th scope="col" class="text-center">Data Emiss�o</th>
				<th scope="col" class="text-center">Data Vencimento</th>
				<th scope="col" class="text-center">Dias Atraso</th>
				<th scope="col" class="text-center">Data Protocolo</th>
				<th scope="col" class="text-center">Valor Documento (R$)</th>
				<th scope="col" class="text-center">Nota Fiscal</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listaTitulosAbertos}" var="titulo">
				<tr>
					<td>
						<div
							class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
							data-identificador-check="chk${titulo.sequencialTitulo}">
							<input id="chk${titulo.sequencialTitulo}" type="checkbox"
								name="numeroTitulo" class="custom-control-input"
								value="${titulo.sequencialTitulo}"> <label
								class="custom-control-label p-0" for="chk${titulo.sequencialTitulo}"></label>
						</div>
					</td>
					<td class="text-center"><c:out value='${titulo.numeroTitulo}' />
					</td>
					<td class="text-center"><c:out value='${titulo.dataEmissao}' />
					</td>
					<td class="text-center"><c:out
							value='${titulo.dataVencimento}' /></td>
					<td class="text-center"><c:out value='${titulo.diasAtraso}' />
					</td>
					<td class="text-center"><c:out
							value='${titulo.dataProtocoloCorte}' /></td>
					<td class="text-center"><c:out
							value='${titulo.valorDocumento}' /></td>
					<td class="text-center"><c:out
							value='${titulo.numeroNotaFiscal}' /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>


	<button type="button" class="btn btn-primary"
		name="buttonAdicionarAnalise" id="buttonAdicionarAnalise">Adicionar</button>

</div>
