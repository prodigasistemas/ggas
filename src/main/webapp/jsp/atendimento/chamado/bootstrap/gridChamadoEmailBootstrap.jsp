<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

function removerChamadoEmail(index){
	
	var noCache = "noCache=" + new Date().getTime();
	$("#gridChamadoEmail").load("removerChamadoEmail?indexListaChamadoEmail="+index+"&"+noCache, function() {
        $('.loading').hide();
        $('#table-grid-email').css({'opacity': 1, 'display': 'table'});
        iniciarDatatable('#table-grid-email');
    });

}
function exibirDescricaoEmail(descricaoEmail,index){
	var cont = 0;
	var arrayDescricaoEmail = [];
	$('input[name="descEmail1"]').each(function() {
		arrayDescricaoEmail[cont] = this.value;
		cont++; 
	});
	$("#descricaoEmail").val(arrayDescricaoEmail[index]);
	$("#indexListaChamadoEmail").val(index);
	document.getElementById('bottonAlterarEmail').disabled = false;
	document.getElementById('bottonAdicionarEmail').disabled = true;
	
}
	
</script>



<div class="text-center loading">
    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive">

    <c:set var="indexListaChamadoEmail" value="0" />
    <table class="table table-bordered table-striped table-hover" id="table-grid-email" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th scope="col" class="text-center">Email</th>
            <c:if test="${ fluxoInclusao eq true || fluxoAlteracao eq true }">
            	<th scope="col" class="text-center">A��o</th>
            </c:if>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${sessionScope.listaChamadoEmail}" var="chamadoEmail">
            <c:if test="${ fluxoInclusao eq true || fluxoAlteracao eq true }">
                <tr>
                    <td>
                        <a  onclick="exibirDescricaoEmail(this.value,<c:out value="${indexListaChamadoEmail}"/>);" > <c:out  value="${chamadoEmail.descricao}"  />  </a>
                        <input name="descEmail1" type="hidden" id="descEmail1"   value="${chamadoEmail.descricao}"  >
                    </td>
                    <td>
                        <a onclick="javascript:removerChamadoEmail(<c:out value="${indexListaChamadoEmail}"/>);" title="Excluir e-mail">
                            <i class="fa fa-trash text-danger"></i>
                        </a>
                    </td>
                </tr>
            </c:if>
            <c:if test="${ fluxoReiteracao eq true   || fluxoReativacao eq true   || fluxoReabertura eq true  || fluxoEncerramento eq true  || fluxoCapturar eq true || fluxoDetalhamento eq true }">
				<tr>
					<td>
                        <a  onclick="exibirDescricaoEmail(this.value,<c:out value="${indexListaChamadoEmail}"/>);" > <c:out  value="${chamadoEmail.descricao}"  />  </a>
                        <input name="descEmail1" type="hidden" id="descEmail1"   value="${chamadoEmail.descricao}"  >
                    </td>
               </tr>
            </c:if>
            <c:set var="indexListaChamadoEmail" value="${indexListaChamadoEmail + 1}" />
        </c:forEach>
        </tbody>
    </table>
</div>
