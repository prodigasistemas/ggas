<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script type="text/javascript">

    function visualizarInformacoesPontoConsumo() {
        var chavePontoConsumo = document.forms['chamadoForm'].idPontoConsumo.value;

        if (chavePontoConsumo != '') {
            submeter('chamadoForm', 'visualizarInformacoesPontoConsumo', '_blank');
        } else {
            alert("Escolha um Ponto de Consumo para Vizualizar suas informa��es.");
        }


    }

    function exibirPopupContatosImovel() {
        if ($("input[type=radio]#chaveImovel:checked").val() != undefined) {
            var idImovel = $("input[type=radio]#chaveImovel:checked").val()
            popup = window.open('exibirPopupContatosImovel?acao=exibirPopupContatosImovel&postBack=false&chavePrimaria=' + idImovel, 'popup', 'height=750,width=1080,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
        } else {
            alert("Selecione um im�vel para visualizar seus contatos")
        }
    }

    function selecionarContatoImovel(idContato) {

        if (idContato != undefined && idContato != "") {
            AjaxService.obterContatoImovelPorId(idContato, {
                callback: function (contato) {
                    $("#nomeSolicitante").val(contato['nomeContato']);
                    $("#telefoneSolicitante").val(contato['fone1Formatado']);
                    $("#emailSolicitante").val(contato['emailContato']);
                    $("#tipoContatoSolicitante").val(contato['idTipoContato']);
                    $("#cpfCnpjSolicitante").val(contato['']);
                    $("#rgSolicitante").val(contato['']);
                },
                async: false
            });
        }
    }

</script>

<c:if test="${ fluxoInclusao eq true}">
    <div class="form-row mb-2 mt-1">
        <div class="col-md-12">
            <button name="button" class="btn btn-primary btn-sm" type="button"
                    onclick="exibirPopupContatosImovel();">
                <i class="fa fa-user-friends"></i> Contatos Im�vel
            </button>
        </div>
    </div>
</c:if>

<hr/>

<h5 class="card-title mt-2">Pontos de consumo</h5>

<div class="text-center loading">
    <img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<c:set var="pontoConsumoDescricao" value="" />
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover" id="table-grid-pontos-consumo" width="100%"
           style="opacity: 0;">
        <thead class="thead-ggas-bootstrap">
        <tr>
            <th>
            </th>
            <th scope="col" class="text-center">Descri��o</th>
            <th scope="col" class="text-center">Segmento</th>
            <th scope="col" class="text-center">Endere�o</th>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${pontosConsumo}" var="pontoConsumo">
                <c:set var="pontoConsumoDescricao" value="${pontoConsumo.descricao}" />
                <c:if test="${ fluxoInclusao eq true }">
                    <tr>
                        <td class="text-center">
                            <input type="radio" name="checkPontoConsumo" id="checkPontoConsumo"
                                   value="<c:out value="${pontoConsumo.chavePrimaria}"/>"
                                   <c:if test="${pontoConsumo.chavePrimaria == chamado.pontoConsumo.chavePrimaria}">checked</c:if>
                                   onClick="selecionarPontoConsumo(${pontoConsumo.chavePrimaria}), carregaroInformacaoPontoConsumo(${pontoConsumo.chavePrimaria});"/>
                        </td>
                        <td>
                            <a href="javascript:visualizarInformacoesPontoConsumo();"><span
                                    class="linkInvisivel"></span>
                                <c:out value='${pontoConsumo.descricao}'/>
                            </a>
                        </td>
                        <td>
                            <a href="javascript:visualizarInformacoesPontoConsumo();"><span
                                    class="linkInvisivel"></span>
                                <c:out value='${pontoConsumo.segmento.descricao}'/>
                            </a>
                        </td>
                        <td>
                            <a href="javascript:visualizarInformacoesPontoConsumo();"><span
                                    class="linkInvisivel"></span>
                                <c:out value='${pontoConsumo.enderecoFormatado}'/>
                            </a>
                        </td>
                    </tr>
                </c:if>

                <c:if test="${ fluxoInclusao ne true }">
                    <tr>
                        <td>
                            <input type="radio" name="checkPontoConsumo" id="checkPontoConsumo"
                                   value="<c:out value="${pontoConsumo.chavePrimaria}"/>"
                                   onClick="carregaroInformacaoPontoConsumo(${pontoConsumo.chavePrimaria});"/>
                        </td>
                        <td>${pontoConsumo.descricao}</td>
                        <td>-</td>
                        <td>${pontoConsumo.enderecoFormatado}</td>
                    </tr>
                </c:if>

            </c:forEach>
        </tbody>
    </table>
</div>

<div class="form-row mb-2">
    <div class="col-md-12">
        <label for="descricaoPontoConsumo">Descri��o:</label>
        <input class="form-control form-control-sm" type="text" id="descricaoPontoConsumo" name="descricaoPontoConsumo"
               disabled="disabled"
               maxlength="25" size="30" value="<c:out value="${pontoConsumoDescricao}"/>">

        <button name="Button" class="btn btn-primary btn-sm mt-2" id="botaoPesquisar" type="button"
                onclick="visualizarInformacoesPontoConsumo();">
            <i class="fa fa-info-circle"></i> Informa��es do Ponto de Consumo
        </button>
    </div>
</div>
