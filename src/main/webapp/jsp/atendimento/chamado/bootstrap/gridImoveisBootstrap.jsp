<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<div class="text-center loading">
	<img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover" id="table-grid-imoveis" width="100%"
		   style="opacity: 0;">
		<thead class="thead-ggas-bootstrap">
		<tr>
			<th>
			</th>
			<th scope="col" class="text-center">Matr�cula</th>
			<th scope="col" class="text-center">Nome</th>
			<th scope="col" class="text-center">Situa��o</th>
			<th scope="col" class="text-center">Endere�o</th>
			<th scope="col" class="text-center">Tipo Medi��o</th>
		</tr>
		</thead>
		<tbody>
		<c:forEach items="${sessionScope.listaImoveisChamado}" var="imovel">
			<tr>
				<td class="text-center">
					<input type="radio" name="chaveImovel" id="chaveImovel"
						   value="<c:out value="${imovel.chavePrimaria}"/>"
						   <c:if test="${imovel.chavePrimaria == chamado.imovel.chavePrimaria || idImovel ne null }">checked</c:if>
						   onClick="carregarPontosConsumo(${imovel.chavePrimaria}); verificarGarantia();"
						   />
				</td>
				<td class="text-center">
					<a title="Im�vel Novo: <c:if test="${imovel.indicadorObraTubulacao eq 'true'}">Sim</c:if><c:if test="${imovel.indicadorObraTubulacao ne 'true'}">N�o</c:if>&#13;Modalidade de Medi��o: ${imovel.modalidadeMedicaoImovel}&#13;"
                       data-chavePrimaria="${imovel.chavePrimaria}"
                       target="_blank" href='${ctx}/exibirDetalhamentoImovel?chavePrimaria=${imovel.chavePrimaria}&acao=exibirDetalhamentoImovel'>
                        <span class="linkInvisivel"></span>
						<c:out value='${imovel.chavePrimaria}'/>
					</a>
				</td>
				<td class="text-center">
					<a title="Im�vel Novo: <c:if test="${imovel.indicadorObraTubulacao eq 'true'}">Sim</c:if><c:if test="${imovel.indicadorObraTubulacao ne 'true'}">N�o</c:if>&#13;Modalidade de Medi��o: ${imovel.modalidadeMedicaoImovel}&#13;"
                       data-chavePrimaria="${imovel.chavePrimaria}" target="_blank"
                       href='${ctx}/exibirDetalhamentoImovel?chavePrimaria=${imovel.chavePrimaria}&acao=exibirDetalhamentoImovel'><span class="linkInvisivel"></span>
						<c:out value='${imovel.nome}'/>
					</a>
				</td>
				<td class="text-center">
					<a data-chavePrimaria="${imovel.chavePrimaria}" target="_blank"
                       href='${ctx}/exibirDetalhamentoImovel?chavePrimaria=${imovel.chavePrimaria}&acao=exibirDetalhamentoImovel'><span class="linkInvisivel"></span>
						<c:out value='${imovel.situacaoImovel.descricao}'/>
					</a>
				</td>
				<td class="text-center">
					<a style="width: 200px" data-chavePrimaria="${imovel.chavePrimaria}" target="_blank"
                       href='${ctx}/exibirDetalhamentoImovel?chavePrimaria=${imovel.chavePrimaria}&acao=exibirDetalhamentoImovel'><span class="linkInvisivel"></span>
						<c:out value='${imovel.enderecoFormatado}'/>
					</a>
				</td>
				<td class="text-center">
					<a data-chavePrimaria="${imovel.chavePrimaria}" target="_blank"
                       href='${ctx}/exibirDetalhamentoImovel?chavePrimaria=${imovel.chavePrimaria}&acao=exibirDetalhamentoImovel'><span class="linkInvisivel"></span>
						<c:choose>
							<c:when test="${imovel.imovelCondominio ne null}">
								<c:out value='${imovel.imovelCondominio.modalidadeMedicaoImovel.descricao}'/>
							</c:when>
							<c:otherwise>
								<c:out value='${imovel.modalidadeMedicaoImovel.descricao}'/>
							</c:otherwise>
						</c:choose>
					</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>
