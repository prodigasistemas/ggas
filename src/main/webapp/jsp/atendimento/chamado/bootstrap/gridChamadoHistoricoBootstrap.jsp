<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<script>

    function detalhamentoChamado(chave) {

        var form = document.createElement("form");

        var element1 = document.createElement("input");

        form.method = "POST";

        form.name = 'chamadoForm';

        element1.value = chave;
        element1.name = "chavePrimaria";
        form.appendChild(element1);

        document.body.appendChild(form);

        submeter(form.name, 'exibirDetalhamentoChamado')
    }

</script>


<div class="text-center loading">
	<img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive">

	<table class="table table-bordered table-striped table-hover" id="table-grid-chamado-historico" width="100%"
		   style="opacity: 0;">
		<thead class="thead-ggas-bootstrap">
		<tr>
			<th class="text-center" colspan="8">Hist�rico do Chamado</th>
		</tr>
		<tr>
			<th scope="col" class="text-center">Data / Hora</th>
			<th scope="col" class="text-center">Usu�rio</th>
			<th scope="col" class="text-center">Respons�vel</th>
			<th scope="col" class="text-center">Unidade Organizacional</th>
			<th scope="col" class="text-center">Descri��o</th>
			<th scope="col" class="text-center">Motivo</th>
			<th scope="col" class="text-center">Opera��o</th>
			<th scope="col" class="text-center">Situa��o</th>
		</tr>
		</thead>
		<tbody>
		<c:forEach items="${listaChamadoHistorico}" var="chamadoHistorico">
			<tr>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<fmt:formatDate value="${chamadoHistorico.ultimaAlteracao}" pattern="dd/MM/yyyy: HH:mm"/>
					</a>
				</td>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<c:out value="${chamadoHistorico.usuario.login}"/>
					</a>
				</td>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<c:out value="${chamadoHistorico.usuarioResponsavel.funcionario.nome}"/>
					</a>
				</td>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<c:out value="${chamadoHistorico.unidadeOrganizacional.descricao}"/>
					</a>
				</td>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<c:out value="${chamadoHistorico.descricao}"/>
					</a>
				</td>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<c:out value="${chamadoHistorico.motivo.descricao}"/>
					</a>
				</td>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<c:out value="${chamadoHistorico.operacao.descricao}"/>
					</a>
				</td>
				<td>
					<a href="javascript:detalhamentoChamado(<c:out value='${chamadoHistorico.chamado.chavePrimaria}'/>);"><span
							class="linkInvisivel"></span>
						<c:out value="${chamadoHistorico.status.descricao}"/>
					</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>
