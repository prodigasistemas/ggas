<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir='/WEB-INF/tags' prefix='ggas'%>




<div class="text-center loading">
	<img src="${pageContext.request.contextPath}/imagens/loading.gif" class="img-responsive"/>
</div>

<div class="table-responsive">

	<h5 class="card-title">Contatos</h5>
	<c:set var="indexListaChamadoContato" value="0" />
	<table class="table table-bordered table-striped table-hover" id="table-grid-chamado-contato" width="100%"
		   style="opacity: 0;">
		<thead class="thead-ggas-bootstrap">
		<tr>
			<th scope="col" class="text-center">Tipo</th>
			<th scope="col" class="text-center">Nome</th>
			<th scope="col" class="text-center">DDD</th>
			<th scope="col" class="text-center">Telefone</th>
			<th scope="col" class="text-center">Ramal</th>
			<th scope="col" class="text-center">Cargo</th>
			<th scope="col" class="text-center">�rea</th>
			<th scope="col" class="text-center">E-mail</th>
			<c:if test="${ fluxoDetalhamento ne true }">
				<th scope="col" class="text-center">Principal</th>
				<th scope="col" class="text-center">A��es</th>
			</c:if>
		</tr>
		</thead>
		<tbody>
		<c:forEach items="${listaChamadoContato}" var="contato">
				<tr>
					<td>
						${contato.tipoContato.descricao}
					</td>
					<td>
							${contato.nome}
					</td>
					<td>
							${contato.codigoDDD}
					</td>
					<td>
							${contato.fone}
					</td>
					<td>
							${contato.ramal}
					</td>
					<td>
							${contato.profissao.descricao}
					</td>
					<td>
						<div style="width: 140px; word-wrap: break-word; overflow: hidden">${contato.descricaoArea}</div>
					</td>
					<td>
							${contato.email}
					</td>
					<c:if test="${ fluxoDetalhamento ne true }">
						<td>
							<c:choose>
								<c:when test="${contato.principal}">
									Sim <a id="setContatoPrincipal"
                                           data-index="${indexListaChamadoContato}"
                                           href="javascript:void(0)"></a>
								</c:when>
								<c:otherwise>
									N�o <a href="javascript:atualizarContatoPrincipal(<c:out value="${indexListaChamadoContato}"/>);"><img alt="Tornar Contato Principal" title="Tornar Contato Principal" src="<c:url value="/imagens/user_business_boss.png"/>" border="0" /></a>
								</c:otherwise>
							</c:choose>
						</td>
						<td>

							<a id="editarContato"
                               data-indexListaChamadoContato="${indexListaChamadoContato}"
                               data-idTipoContato="${contato.tipoContato.chavePrimaria}"
                               data-descricao="${contato.tipoContato.descricao}"
                               data-nome="${contato.nome}"
                               data-dddContato="${contato.codigoDDD}"
                               data-telefoneContato="${contato.fone}"
                               data-ramalContato="${contato.ramal}"
                               data-cargoContato="${contato.profissao.chavePrimaria}"
                               data-profissaoDescricao="${contato.profissao.chavePrimaria}"
                               data-areaContato="${contato.descricaoArea}"
                               data-emailContato="${contato.email}"
                               title="Clique para Editar Contato"
                               class="text-secondary" href="javascript:void(0)">
                                <i class="fa fa-edit fa-fw"/>
                            </a>
							<a id="removerContato"
                               title="Clique para excluir este contato"
                               class="text-danger"
                               data-indexListaChamadoContato="${indexListaChamadoContato}"
                               href="javascript:void(0)">
                                <i class="fa fa-trash"/>
                            </a>
						</td>
					</c:if>
				</tr>
			<c:set var="indexListaChamadoContato" value="${indexListaChamadoContato+1}" />
		</c:forEach>
		</tbody>
	</table>
</div>


