<!--
 Copyright (C) <2011> GGAS - Sistema de Gestï¿½o Comercial (Billing) de Serviï¿½os de Distribuiï¿½ï¿½o de Gï¿½s

 Este arquivo ï¿½ parte do GGAS, um sistema de gestï¿½o comercial de Serviï¿½os de Distribuiï¿½ï¿½o de Gï¿½s

 Este programa ï¿½ um software livre; vocï¿½ pode redistribuï¿½-lo e/ou
 modificï¿½-lo sob os termos de Licenï¿½a Pï¿½blica Geral GNU, conforme
 publicada pela Free Software Foundation; versï¿½o 2 da Licenï¿½a.

 O GGAS ï¿½ distribuï¿½do na expectativa de ser ï¿½til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implï¿½cita de
 COMERCIALIZAï¿½ï¿½O ou de ADEQUAï¿½ï¿½O A QUALQUER PROPï¿½SITO EM PARTICULAR.
 Consulte a Licenï¿½a Pï¿½blica Geral GNU para obter mais detalhes.

 Vocï¿½ deve ter recebido uma cï¿½pia da Licenï¿½a Pï¿½blica Geral GNU
 junto com este programa; se nï¿½o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gestï¿½o Comercial (Billing) de Serviï¿½os de Distribuiï¿½ï¿½o de Gï¿½s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="br.com.ggas.cadastro.unidade.UnidadeOrganizacional" %>
<%@ page import="br.com.ggas.atendimento.chamado.dominio.Chamado" %>
<%@ page import="br.com.ggas.atendimento.chamado.dominio.ChamadoUnidadeOrganizacional" %>


<%@ page import="java.util.*"%>

<div class="bootstrap">
    <form:form modelAttribute="Chamado" method="post" id="chamadoForm" name="chamadoForm" enctype="multipart/form-data">

        <input type="hidden" value="<c:url value="/imagens/calendario.png"/>" id="imagemCalendario"/>

        <div class="card">
            <div class="card-header">
                <c:choose>
                    <c:when test="${ fluxoInclusao eq true }">Incluir Chamado</c:when>
                    <c:when test="${ fluxoDetalhamento eq true }">Detalhamento do Chamado</c:when>
                    <c:when test="${ fluxoAlteracao eq true || fluxoAlteracaoRascunho eq true}">Alterar Chamado</c:when>
                    <c:when test="${ fluxoTramitacao eq true }">Tramitação do Chamado</c:when>
                    <c:when test="${ fluxoReiteracao eq true }">Reiteração do Chamado</c:when>
                    <c:when test="${ fluxoReativacao eq true }">Reativação do Chamado</c:when>
                    <c:when test="${ fluxoReabertura eq true }">Reabertura do Chamado</c:when>
                    <c:when test="${ fluxoEncerramento eq true }">Encerramento do Chamado</c:when>
                    <c:when test="${ fluxoCapturar eq true }">Capturar Chamado</c:when>
                </c:choose>
            </div>
            <div class="card-body">
                <span>

                    <!-- Utilizado pelo arquivo index.js -->
                    <input type="hidden" name="informacaoAdicionalInfo" id="informacaoAdicionalInfo"
                           value="${fn:replace(chamado.informacaoAdicional,newLineChar,' ')}"/>
                    <input type="hidden" id="IndicadorObrigatorio" value="${IndicadorObrigatorio}">
                    <input type="hidden" name="chamado.chamadoAssunto.chavePrimaria"
                           value="${chamado.chamadoAssunto.chavePrimaria}">
                    <input type="hidden" id="exibirPassaporte" value="${exibirPassaporte}">
                    <input type="hidden" id="exibirMensagemTela" value="${exibirMensagemTela}">
                    <input type="hidden" name="chamado.chamadoAssunto.indicadorVerificarVazamento"
                           value="${chamado.chamadoAssunto.indicadorVerificarVazamento}">
                    <input type="hidden" id="exibirAcionamentoGasista" value="${exibirAcionamentoGasista}">
                    <input type="hidden" id="habilitaAcionamento" value="${habilitaAcionamento}">
                    <input type="hidden" name="chamado.indicadorAcionamentoPlantonista"
                           value="${chamado.indicadorAcionamentoPlantonista}">
                    <input type="hidden" name="chamado.indicadorAcionamentoGasista"
                           value="${chamado.indicadorAcionamentoGasista}">
        			<input type="hidden" id="horaAcionamentos" name="chamado.horaAcionamento" 
                    		value="${chamado.horaAcionamento}"/>	                           



                    <input type="hidden" id="idUnidadeOrganizacionalChamado" value="${chamado.unidadeOrganizacional.chavePrimaria}">

                    <input type="hidden" id="chamadoTipoChavePrimaria" value="${chamadoTipo.chavePrimaria}">


                    <input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chamado.chavePrimaria}">
                    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" value="${chamado.chavePrimaria}">
                    <input name="chavesPrimariasDialog" type="hidden" id="chavesPrimariasDialog">
                    <input name="equipe" type="hidden" id="idEquipe">
                    <input name="idTurnoPreferencia" type="hidden" id="idTurnoPreferencia">
                    <input type="hidden" name="fluxoDetalhamento" id="fluxoDetalhamento" value="${fluxoDetalhamento}">
                    <input type="hidden" name="fluxoAlteracao" id="fluxoAlteracao" value="${fluxoAlteracao}">
                    <input type="hidden" name="fluxoInclusao" id="fluxoInclusao" value="${fluxoInclusao}">
                    <input type="hidden" name="fluxoTramitacao" id="fluxoTramitacao" value="${fluxoTramitacao}">
                    <input type="hidden" name="fluxoAlteracaoRascunho" id="fluxoAlteracaoRascunho"
                           value="${fluxoAlteracaoRascunho}">
                    <input type="hidden" name="fluxoReiteracao" id="fluxoReiteracao" value="${fluxoReiteracao}">
                    <input type="hidden" name="fluxoReativacao" id="fluxoReativacao" value="${fluxoReativacao}">
                    <input type="hidden" name="fluxoReabertura" id="fluxoReabertura" value="${fluxoReabertura}">
                    <input type="hidden" name="versao" id="versao" value="${chamado.versao}">
                    <input type="hidden" name="fluxoEncerramento" id="fluxoEncerramento" value="${fluxoEncerramento}">
                    <input type="hidden" name="fluxoCapturar" id="fluxoCapturar" value="${fluxoCapturar}">
                    <input type="hidden" name="indicadorChamadoPesquisa" id="indicadorChamadoPesquisa"
                           value="${indicadorChamadoPesquisa}">
                    <input type="hidden" name="fluxoVoltar" id="fluxoVoltar">
                    <input type="hidden" name="fluxoPesquisaSatisfacao" id="fluxoPesquisaSatisfacao"
                           value="${fluxoPesquisaSatisfacao}">
                    <input type="hidden" name="chaveChamadoHistorico" id="chaveChamadoHistorico">
                    <input name="comAS" type="hidden" id="comAS" value="false">

                    <input type="hidden" name="rascunho" id="rascunho">
                    <input type="hidden" id="chaveAssunto" name="chaveAssunto" value="${chaveAssunto}">
                    <input type="hidden" id="unidade" name="unidade" value="${unidade}">
                    <input type="hidden" id="idPontoConsumo" name="idPontoConsumo"/>
                    <input type="hidden" name="idChamado" id="idChamado" value="${chamado.contrato.chavePrimaria}">
                    <input type="hidden" name="chamadoTela" id="chamadoTela" value="True">

                    <input type="hidden" name="indicadorChamadoAlteracaoCadastral"
                           id="indicadorChamadoAlteracaoCadastral"
                           value="${indicadorChamadoAlteracaoCadastral}">
                    <input type="hidden" name="indicadorChamadoAlteracaoDataVencimento"
                           id="indicadorChamadoAlteracaoDataVencimento"
                           value="${indicadorChamadoAlteracaoDataVencimento}">

                    <input type="hidden" id="protocolo" name="protocolo" value="${chamado.protocolo.chavePrimaria}"/>
                            <input type="hidden" id="imovel" name="imovel" value="${chamado.imovel.chavePrimaria}"/>
                            <input type="hidden" id="pontoConsumo" name="pontoConsumo" value=""/>

                </span>

                <div class="alert alert-primary" role="alert">
                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                    <div class="d-inline">
                        <c:choose>
                            <c:when test="${ fluxoInclusao eq true }">
                                Altere os dados e clique em <b>Incluir</b>
                                para alterar o Chamado.<br/>Para cancelar clique em <b>Cancelar</b>.

                            </c:when>
                            <c:when test="${ fluxoDetalhamento eq true }">
                                Para modificar as informações deste registro clique em <b>Alterar</b>.
                            </c:when>

                            <c:when test="${ fluxoAlteracao eq true || fluxoAlteracaoRascunho eq true}">
                                Para modificar as informações deste registro clique em <b>Alterar</b>.
                            </c:when>

                            <c:when test="${ fluxoTramitacao eq true }">
                                Informe os dados abaixo e clique em <b>Tramitar</b>.
                            </c:when>

                            <c:when test="${ fluxoReiteracao eq true }">
                                Informe os dados abaixo e clique em <b>Reiterar</b>.
                            </c:when>

                            <c:when test="${ fluxoReativacao eq true }">
                                Informe os dados abaixo e clique em <b>Reativar</b>.
                            </c:when>

                            <c:when test="${ fluxoReabertura eq true }">
                                Informe os dados abaixo e clique em <b>Reabrir</b>.
                            </c:when>

                            <c:when test="${ fluxoEncerramento eq true }">
                                Informe os dados abaixo e clique em <b>Encerrar</b>.
                            </c:when>

                            <c:when test="${ fluxoCapturar eq true }">
                                Informe os dados abaixo e clique em <b>Capturar</b>.
                            </c:when>
                        </c:choose>
                    </div>

                </div>

                <div id="dialogGerarAutorizacaoServico" title="Gerar Autorização de Serviço" style="display: none">
                    <jsp:include page="/jsp/atendimento/chamado/gridPopupServicoTipo.jsp"/>
                </div>

                <div id="dialogGarantiaServico" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="titulo-servicos-garantia-indisponivel">Os serviços do Assunto do chamado estão sem garantia</h5>
                                <h5 class="modal-title" id="titulo-servicos-garantia-disponivel">Prazo de garantia disponível dos serviços do Assunto do chamado</h5>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-primary" role="alert" id="notificacao-servico-disponivel">
                                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                                    <div class="d-inline">
                                        <b>Informativo!</b> Veja abaixo, os tipos de serviços que ainda possuem garantia para o ponto de consumo.
                                    </div>
                                </div>
                                <div class="alert alert-warning" role="alert" id="notificacao-servico-indisponivel">
                                    <div class="d-inline"><i class="fa fa-lg fa-fw fa-info-circle"></i></div>
                                    <div class="d-inline">
                                        <b>Atenção!</b> Para abrir o chamado é necessário escolher outro assunto.
                                    </div>
                                </div>
                                <table class="table table-bordered table-striped table-hover">
                                    <thead class="thead-ggas-bootstrap">
                                    <tr>
                                        <th>Garantia</th>
                                        <th>Tipo de Serviço</th>
                                        <th>Execuções Possíveis</th>
                                        <th>Execuções Disponíveis</th>
                                        <th>Data de Garantia</th>
                                        <th>Prazo de Garantia (em dias)</th>
                                        <th>Data Limite</th>
                                    </tr>
                                    </thead>
                                    <tbody id="corpoGridServicoTipoGarantia" style="background-color: #f1f5f1;"></tbody>
                                </table>
                            </div>
                            <div id="footer-indisponivel" class="modal-footer" style="display: none">
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="atualizarAssuntosLiberado()"><i class="fas fa-sync"></i> Fechar e recarregar assuntos possíveis</button>
                                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i> Fechar e escolher outro assunto</button>
                            </div>

                            <div id="footer-disponivel" class="modal-footer" style="display: none">
                                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="button-salvar-servico-disponivel"><i class="fa fa-save"></i> Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row mb-2">
                    <div class="col-md-12">

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="numeroProtocolo">Número do Protocolo:
                                    <c:if test="${chamado.protocolo.chavePrimaria eq null or chamado.protocolo.chavePrimaria eq 0}">
                                        <span class="text-danger">*</span>
                                    </c:if>
                                </label>
                                <input type="number" class="form-control form-control-sm" id="numeroProtocolo"
                                       name="numeroProtocolo" placeholder="Numero do protocolo"
                                <c:if test="${chamado.protocolo.chavePrimaria ne null && chamado.protocolo.chavePrimaria ne 0}">
                                       disabled="true" </c:if>
                                       maxlength="10" size="10" value="${chamado.protocolo.numeroProtocolo}">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-7 mt-1">

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="descricao">Descrição: <span class="text-danger">*</span></label>
                                <textarea class="form-control form-control-sm" id="descricao"
                                          name="descricao" placeholder="Descricao" cols="35" rows="6"
                                          onblur="this.value=removerEspacoInicialFinal(this.value);">${chamado.descricao}</textarea>
                            </div>
                        </div>

                        <c:if test="${fluxoInclusao eq true}">
                            <div class="form-row">

                                <div class="col-md-12">
                                    <label for="idSegmentoChamadoTipo">Segmento:</label>
                                    <select name="idSegmentoChamadoTipo" id="idSegmentoChamadoTipo"
                                            class="form-control form-control-sm" onchange="atualizarListaTipoChamado(this.value);">
                                        <option value="-1">Todos</option>
                                        <c:forEach items="${listaSegmento}" var="segmento">
                                            <option value="<c:out value="${segmento.chavePrimaria}"/>">
                                                <c:out value="${segmento.descricao}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </c:if>

                        <div class="form-row">

                            <div class="col-md-6" id="divTiposChamados">
                                <label for="chamadoTipo">Tipo do chamado:<span class="text-danger">*</span></label>
                                <select name="chamadoTipo" id="chamadoTipo" class="form-control form-control-sm"
                                        onchange="atualizarAssuntos(this.value);carregarResponsavelPorChamadoAssunto('0');">
                                    <option value="">Selecione o tipo de chamado...</option>
                                    <c:forEach items="${chamadosTipo}" var="chamadotipo">
                                        <option value="<c:out value='${chamadotipo.chavePrimaria}'/>"
                                                title="${chamadotipo.descricao}"
                                                <c:if test="${chamado.chamadoAssunto ne null && chamado.chamadoAssunto.chamadoTipo.chavePrimaria == chamadotipo.chavePrimaria}">selected="selected"</c:if>
                                                <c:if test="${chamado.chamadoAssunto eq null && chamadoTipo.chavePrimaria == chamadotipo.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${chamadotipo.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-md-6" id="divAssuntos">
                                <jsp:include
                                        page="/jsp/atendimento/chamado/bootstrap/selectBoxAssuntoBootstrap.jsp"></jsp:include>
                            </div>


                            <div class="col-md-6" id="divUnidadeOrganizacional">

                                <%--<c:choose>--%>
                                    <%--<c:when test="${fluxoTramitacao eq true}">--%>
                                        <jsp:include
                                                page="/jsp/atendimento/chamado/bootstrap/selectBoxUnidadeOrganizacionalBootstrap.jsp"></jsp:include>
                                    <%--</c:when>--%>
                                    <%--<c:otherwise>--%>

                                        <%--<vacess:vacess param="alterarUnidadesVisualizadoras">--%>
                                            <%--<jsp:include
                                                page="/jsp/atendimento/chamado/bootstrap/selectBoxUnidadeOrganizacionalBootstrap.jsp"></jsp:include>--%>
                                        <%--</vacess:vacess>--%>
                                    <%--</c:otherwise>
                                </c:choose>--%>

                            </div>

                            <div class="col-md-6">
                                <label for="canalAtendimento">Canal de atendimento:<span
                                        class="text-danger">*</span></label>
                                <select name="canalAtendimento" id="canalAtendimento"
                                        class="form-control form-control-sm">
                                        <option value="">Selecione um canal...</option>
                                    <c:forEach items="${listaCanalAtendimento}" var="canalAtendimento">
                                        <option value="<c:out value="${canalAtendimento.chavePrimaria}"/>"
                                                title="${canalAtendimento.descricao}"
                                                <c:if test="${chamado.canalAtendimento.chavePrimaria == canalAtendimento.chavePrimaria}">selected="selected"</c:if>>
                                            <c:out value="${canalAtendimento.descricao}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <!-- antigo -->
                        <c:choose>
                            <c:when test="${fluxoInclusao eq true}">
                                <div class="form-row mt-1">
                                    <div class="col-md-12">
                                        <label>Outras unidades podem visualizar esse chamado?</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <vacess:vacess param="alterarUnidadesVisualizadoras">
                                                <input id="flagUnidadesVisualizadorasSim" class="custom-control-input"
                                                       type="radio"
                                                       value="true"
                                                       onclick="mudarEstadoUnidadesVisualizadoras(this)"
                                                       name="flagUnidadesVisualizadoras"
                                                       <c:if test="${indicadorUnidadesOrganizacionalVisualizadora ne null && indicadorUnidadesOrganizacionalVisualizadora eq 'true'}">checked="checked"</c:if> />
                                            </vacess:vacess>
                                            <label class="custom-control-label"
                                                   for="flagUnidadesVisualizadorasSim">Sim</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <vacess:vacess param="alterarUnidadesVisualizadoras">
                                                <input id="flagUnidadesVisualizadorasNao" class="custom-control-input"
                                                       type="radio"
                                                       value="false"
                                                       onclick="mudarEstadoUnidadesVisualizadoras(this)"
                                                       name="flagUnidadesVisualizadoras"
                                                       <c:if test="${indicadorUnidadesOrganizacionalVisualizadora ne null && indicadorUnidadesOrganizacionalVisualizadora eq 'false'}">checked="checked"</c:if> />
                                            </vacess:vacess>
                                            <label class="custom-control-label"
                                                   for="flagUnidadesVisualizadorasNao">Não</label>
                                        </div>

                                    </div>
                                </div>
                            <c:choose>
	                            <c:when test="${ fluxoTramitacao eq true }">                                
	                                <div class="form-row mt-1">
	                                    <div class="col-md-12" id="divUnidadeOrganizacionalVisualizadoras"
	                                         <c:if test="${indicadorUnidadesOrganizacionalVisualizadora eq null || indicadorUnidadesOrganizacionalVisualizadora eq 'false'}">style="display: none;"</c:if>>
	                                        <label for="unidadeOrganizacionalVisualizadoras">Unidades que podem visualizar: <span
	                                                class="text-danger">*</span></label>
	                                        <div class="input-group">
	                                            <vacess:vacess param="alterarUnidadesVisualizadoras">
	                                            <select id="unidadeOrganizacionalVisualizadoras"
	                                                    name="unidadeOrganizacionalVisualizadoras"
	                                                    class="form-control form-control-sm"
	                                                    multiple="multiple">
	                                                <c:forEach items="${listaUnidadeOrganizacional}"
	                                                           var="unidadeOrganizacional">
	                                                    <option title="${unidadeOrganizacional.descricao}"
	                                                            value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>"
	                                                            <c:if test="${listaChamadoUnidadeOrganizacional ne null }">
	                                                                <c:forEach
	                                                                        items="${listaChamadoUnidadeOrganizacional}"
	                                                                        var="chamadoUnidadeOrganizacional" >
	                                                                    <c:if test="${chamadoUnidadeOrganizacional.unidadeOrganizacional.chavePrimaria eq unidadeOrganizacional.chavePrimaria}">
	                                                                        selected="selected"
	                                                                    </c:if>
	                                                                 >
	                                                                </c:forEach>
	                                                            </c:if>
	                                                          >
	                                                        <c:out value="${unidadeOrganizacional.descricao}"/>
	                                                    </option>
	                                                </c:forEach>
	                                            </select>
	                                                </vacess:vacess>
	                                            <div class="input-group-append d-md-none">
	                                                <span class="input-group-text" style="height: 100%;"><i
	                                                        class="fa fa-search-plus"></i></span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                </c:when>
                            		<c:otherwise>
	                                            <div class="col-md-12" id="divUnidadeOrganizacionalVisualizadoras">
	                                                <label for="unidadeOrganizacionalVisualizadoras">Unidades que podem
	                                                    visualizar:</label>		                            		
													<%
														Chamado chamado = (Chamado) request.getAttribute("chamado");
													    StringBuilder descricoes = new StringBuilder();
													    
													    for(ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional : chamado.getListaUnidadeOrganizacionalVisualizadora()){
													    	descricoes.append(chamadoUnidadeOrganizacional.getUnidadeOrganizacional().getDescricao());
													    	descricoes.append(", ");
													    }
													    
													    if (descricoes.length() > 0) {
													        descricoes.setLength(descricoes.length() - 2);
													    }
													%>
													<div class="input-group">
													    <span class="form-control"><%= descricoes.toString() %></span>
													    <div class="input-group-append d-md-none" id="unidadeOrganizacionalVisualizadoras">
													        <span class="input-group-text" style="height: 100%;"><i class="fa fa-search-plus"></i></span>
													    </div>
													</div>		                            		
												</div>                            		
                            		</c:otherwise>
                            	</c:choose>
	                               
                            </c:when>
                            <c:otherwise>

                                <c:choose>
                                    <c:when test="${chamado.indicadorUnidadesOrganizacionalVisualizadora eq true}">
                                        <div class="form-row mt-1">
                                            <label class="col-md-12 pr-1">Outras Unidades podem visualizar esse chamado?</label>
                                            <div class="col-md-12">

                                                <label for="flagUnidadesVisualizadorasSim"><i class="fa fa-check-circle text-success"></i>
                                                    Sim </label>
                                            </div>
			                            <c:choose>
				                            <c:when test="${ fluxoTramitacao eq true }">                                        
	                                            <div class="col-md-12" id="divUnidadeOrganizacionalVisualizadoras">
	                                                <label for="unidadeOrganizacionalVisualizadoras">Unidades que podem
	                                                    visualizar:</label>
	                                                <div class="input-group">
	                                                    <vacess:vacess param="alterarUnidadesVisualizadoras">
	                                                    <select id="unidadeOrganizacionalVisualizadoras"
	                                                            name="unidadeOrganizacionalVisualizadoras"
	                                                            class="form-control form-control-sm" multiple="multiple">
	                                                        </vacess:vacess>
	                                                        <c:forEach items="${listaUnidadeOrganizacional}"
	                                                                   var="unidadeOrganizacional">
	                                                            <option title="${unidadeOrganizacional.descricao}"
	                                                                    value="<c:out value="${unidadeOrganizacional.chavePrimaria}"/>"
	                                                                    <c:forEach
	                                                                            items="${chamado.listaUnidadeOrganizacionalVisualizadora}"
	                                                                            var="unidadeVisualizadora">
	                                                                        <c:if test="${unidadeVisualizadora.unidadeOrganizacional.chavePrimaria == unidadeOrganizacional.chavePrimaria}">
	                                                                            selected="selected"
	                                                                        </c:if>
	                                                                    </c:forEach>>
	                                                                <c:out value="${unidadeOrganizacional.descricao}"/>
	                                                            </option>
	                                                        </c:forEach>
	                                                    </select>
	                                                    <div class="input-group-append d-md-none">
	                                                        <span class="input-group-text" style="height: 100%;"><i
	                                                                class="fa fa-search-plus"></i></span>
	                                                    </div>
	                                                </div>
	                                            </div>
			                                </c:when>
		                            		<c:otherwise>
	                                            <div class="col-md-12" id="divUnidadeOrganizacionalVisualizadoras">
	                                                <label for="unidadeOrganizacionalVisualizadoras">Unidades que podem
	                                                    visualizar:</label>		                            		
													<%
														Chamado chamado = (Chamado) request.getAttribute("chamado");
													    StringBuilder descricoes = new StringBuilder();
													    
													    for(ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional : chamado.getListaUnidadeOrganizacionalVisualizadora()){
													    	descricoes.append(chamadoUnidadeOrganizacional.getUnidadeOrganizacional().getDescricao());
													    	descricoes.append(", ");
													    }
													    
													    if (descricoes.length() > 0) {
													        descricoes.setLength(descricoes.length() - 2);
													    }
													%>
													<div class="input-group">
													    <span class="form-control"><%= descricoes.toString() %></span>
													    <div class="input-group-append d-md-none" id="unidadeOrganizacionalVisualizadoras">
													        <span class="input-group-text" style="height: 100%;"><i class="fa fa-search-plus"></i></span>
													    </div>
													</div>		                            		
												</div>                            		
											</c:otherwise>
		                            	</c:choose>                                            
                                        </div>
                                    </c:when>
                                    <c:otherwise>

                                        <div class="form-row mt-1">
                                            <label class="pr-1 col-md-12">Outras Unidades podem visualizar esse chamado?</label>
                                            <div class="col-md-12">
                                                <label for="flagUnidadesVisualizadorasNao"><i
                                                        class="fa fa-times-circle text-danger"></i> Não </label>
                                            </div>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>

                        <!-- END Outras unidades podem visualizar -->
                    </div>

                    <div class="col-md-5 mt-1">
                        <!-- ISSO tem a ver com os tipos de chamado, assunto do chamado, etc -->
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="orientacaoAssunto">Observações/Orientações:
                                    <button id="btn-copiar-observacao" onclick="copiarObservacao()" type="button"
                                            class="btn btn-primary btn-sm copiar-observacao"><i class="fa fa-copy"></i> Copiar
                                    </button>
                                </label>
                                <textarea class="form-control form-control-sm" name="orientacaoAssunto"
                                          id="orientacaoAssunto" cols="60"
                                          rows="6"
                                          maxlength="800" disabled></textarea>
                            </div>
                        </div>

                        <div id="divPrazoDiferenciado">
                            <jsp:include page="/jsp/atendimento/chamado/divPrazoDiferenciado.jsp"></jsp:include>
                        </div>

                        <c:if test="${ fluxoInclusao eq true }">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="divPrazo">Prazo previsto(em horas):</label>
                                    <input type="number" class="form-control form-control-sm" id="divPrazo"
                                           name="divPrazo"
                                           maxlength="9" size="9" placeholder="Escolha um assunto para preencher o prazo..."
                                           value="${chamado.chamadoAssunto.quantidadeHorasPrevistaAtendimento}"
                                           disabled>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoInclusao ne true }">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="prazoPrevisto">Prazo previsto(em horas):</label>
                                    <input type="number" class="form-control form-control-sm" id="prazoPrevisto"
                                           name="prazoPrevisto" placeholder="Prazo previsto em horas"
                                           maxlength="9" size="9"
                                           value="${chamado.chamadoAssunto.quantidadeHorasPrevistaAtendimento}">
                                </div>
                            </div>
                        </c:if>

                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="dataPrevisaoEncerramento">Previsão de encerramento:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control form-control-sm"
                                           id="dataPrevisaoEncerramento"
                                           name="dataPrevisaoEncerramento" placeholder="Previsao de encerramento"
                                           maxlength="16" size="16"
                                           value="<fmt:formatDate value="${chamado.dataPrevisaoEncerramento}" pattern="dd/MM/yyyy HH:mm"/>">
                                </div>
                            </div>
                        </div>

                        <c:if test="${ fluxoInclusao ne true }">
                            <c:if test="${ fluxoTramitacao ne true || fluxoReiteracao eq true}">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="usuarioResponsavelNome">Responsável: <span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control form-control-sm"
                                               id="usuarioResponsavelNome"
                                               name="usuarioResponsavelNome" placeholder="Responsavel"
                                               maxlength="30" size="30"
                                               value="${chamado.usuarioResponsavel.funcionario.nome}">

                                        <input type="hidden" id="usuarioResponsavel" name="usuarioResponsavel"
                                               value="${chamado.usuarioResponsavel.chavePrimaria}">
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${ fluxoTramitacao eq true }">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="usuarioResponsavel">Responsável: <span
                                                class="text-danger">*</span></label>
                                        <div id="divResponsavel">
                                            <jsp:include
                                                    page="/jsp/atendimento/chamado/bootstrap/selectBoxResponsavelBootstrap.jsp"></jsp:include>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${ fluxoReativacao eq true || fluxoReabertura eq true || fluxoEncerramento eq true}">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="idMotivo">Motivo: <span class="text-danger">*</span></label>
                                        <select id="idMotivo" class="form-control form-control-sm" name="idMotivo">
                                            <option value="-1">Selecione</option>
                                            <c:forEach items="${listaMotivos}" var="motivo">
                                                <option value="<c:out value="${motivo.chavePrimaria}"/>"
                                                        <c:if test="${idMotivo == motivo.chavePrimaria}">selected="selected"</c:if>>
                                                    <c:out value="${motivo.descricao}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${fluxoDetalhamento eq true || fluxoReabertura eq true}">

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="dataResolucao">Data de resolução:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-sm" id="dataResolucao"
                                                   name="dataResolucao" placeholder="Data de resolucao"
                                                   maxlength="16" size="16"
                                                   value="<fmt:formatDate value="${chamado.dataResolucao}" pattern="dd/MM/yyyy HH:mm"/>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label>Agência reguladora quem originou?</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="indicadorAgenciaReguladoraSim"
                                                   name="indicadorAgenciaReguladora" class="custom-control-input"
                                                   value="true"
                                                   <c:if test="${chamado.indicadorAgenciaReguladora eq 'true'}">checked</c:if>>
                                            <label class="custom-control-label"
                                                   for="indicadorAgenciaReguladoraSim">Sim</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="indicadorAgenciaReguladoraNao"
                                                   name="indicadorAgenciaReguladora" class="custom-control-input"
                                                   value="true"
                                                   <c:if test="${chamado.indicadorAgenciaReguladora eq 'false'}">checked</c:if>>
                                            <label class="custom-control-label"
                                                   for="indicadorAgenciaReguladoraNao">Não</label>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:if>

                        <c:if test="${fluxoDetalhamento ne true}">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="dataResolucao">Data de resolução:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" id="dataResolucao"
                                               name="dataResolucao" placeholder="Data de resolucao"
                                               maxlength="16" size="16"
                                               value="<fmt:formatDate value="${chamado.dataResolucao}" pattern="dd/MM/yyyy HH:mm"/>">
                                    </div>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${(fluxoEncerramento eq true || fluxoDetalhamento eq true) && chamado.chamadoAssunto.indicadorVerificarVazamento eq true}">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label class="pt-1">Vazamento confirmado: <span
                                            class="text-danger">*</span></label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="indicadorVazamentoConfirmadoSim"
                                               name="indicadorVazamentoConfirmado" class="custom-control-input"
                                               value="true"
                                               <c:if test="${chamado.indicadorVazamentoConfirmado eq 'true'}">checked</c:if>>
                                        <label class="custom-control-label"
                                               for="indicadorVazamentoConfirmadoSim">Sim</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="indicadorVazamentoConfirmadoNao"
                                               name="indicadorVazamentoConfirmado" class="custom-control-input"
                                               value="true"
                                               <c:if test="${chamado.indicadorVazamentoConfirmado eq 'false'}">checked</c:if>>
                                        <label class="custom-control-label"
                                               for="indicadorVazamentoConfirmadoNao">Não</label>
                                    </div>
                                </div>
                            </div>
                        </c:if>


                        <c:if test="${ fluxoInclusao eq true }">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="usuarioResponsavel">Responsável:</label>
                                    <div id="divResponsavelPorChamadoAssunto">
                                        <jsp:include
                                                page="/jsp/atendimento/chamado/bootstrap/selectBoxResponsavelBootstrap.jsp"></jsp:include>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <!-- ISSO tem a ver com os tipos de chamado, assunto do chamado, etc -->
                    </div>
                </div>

                <hr/>

                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="tab-content" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-solicitante" data-toggle="tab"
                                   href="#contentTabSolicitante" role="tab" aria-controls="contentTabSolicitante"
                                   aria-selected="true"><i class="fa fa-chalkboard-teacher"></i> Solicitante</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-cliente" data-toggle="tab"
                                   href="#contentTabCliente" role="tab" aria-controls="contentTabCliente"
                                   aria-selected="false"><i class="fa fa-user-tie"></i> Cliente</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-imovel" data-toggle="tab"
                                   href="#contentTabImovel" role="tab" aria-controls="contentTabImovel"
                                   aria-selected="false"><i class="fa fa-hotel"></i> Imóvel</a>
                            </li>
                            <c:if test="${fluxoDetalhamento ne true}">
                                <li class="nav-item">
                                    <a class="nav-link" id="tab-docs" data-toggle="tab"
                                       href="#contentTabDocs" role="tab" aria-controls="contentTabDocs"
                                       aria-selected="false"><i class="fa fa-layer-group"></i> Documentos</a>
                                </li>
                            </c:if>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-email" data-toggle="tab"
                                   href="#contentTabEmail" role="tab" aria-controls="contentTabEmail"
                                   aria-selected="false"><i class="fa fa-envelope"></i> E-mail</a>
                            </li>
                            <c:if test="${ fluxoInclusao ne true}">
                                <li class="nav-item">
                                    <a class="nav-link" id="tab-historico" data-toggle="tab"
                                       href="#contentTabHistorico" role="tab" aria-controls="contentTabHistorico"
                                       aria-selected="false"><i class="fa fa-archive"></i> Histórico</a>
                                </li>
                            </c:if>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-controle-operacional" data-toggle="tab"
                                   href="#contentTabControleOperacional" role="tab" aria-controls="contentTabControleOperacional"
                                   aria-selected="false"><i class="fa fa-cog"></i> Controle operacional</a>
                            </li>
                            <c:if test="${fluxoInclusao eq true || fluxoDetalhamento eq true || fluxoAlteracao eq true}">
	                            <li class="nav-item" id="endereco">
	                                <a class="nav-link" id="tab-imovel" data-toggle="tab"
	                                   href="#contentTabEndereco" role="tab" aria-controls="contentTabEndereco"
	                                   aria-selected="false"><i class="fa fa-road"></i> Endereço</a>
	                            </li>
                            </c:if>                                 
                            <li class="nav-item">
                                <a class="nav-link" id="tab-titulos-abertos" data-toggle="tab"
                                   href="#contentTabTitulosAbertos" role="tab" aria-controls="contentTabTitulosAbertos"
                                   aria-selected="false"><i class="fa fa-credit-card"></i> Títulos Abertos</a>
                            </li>                              
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="content-incluir-chamado">
                            <div class="tab-pane fade show active" id="contentTabSolicitante" role="tabpanel"
                                 aria-labelledby="contentTabSolicitante">

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-row">

                                            <div class="col-md-12">
                                                <label for="nomeSolicitante">Nome:</label>
                                                <input type="text" class="form-control form-control-sm"
                                                       id="nomeSolicitante"
                                                       name="nomeSolicitante" placeholder="Nome do solicitante"
                                                       onblur="this.value=removerEspacoInicialFinal(this.value);"
                                                       maxlength="255" size="32" value="${chamado.nomeSolicitante}">
                                            </div>
                                            <div class="col-md-12">
                                                <label for="cpfCnpjSolicitante">CPF/CNPJ:</label>
                                                <input type="text" class="form-control form-control-sm" id="cpfCnpjSolicitante"
                                                       name="cpfCnpjSolicitante" placeholder="CPF/CNPJ"
                                                       onblur="this.value=removerEspacoInicialFinal(this.value);"
                                                       maxlength="18" size="32" value="${chamado.cpfCnpjSolicitante}">
                                            </div>

                                            <div class="col-md-12">
                                                <label for="rgSolicitante">RG:</label>
                                                <input type="text" class="form-control form-control-sm" id="rgSolicitante"
                                                       name="rgSolicitante" placeholder="RG do solicitante"
                                                       onblur="this.value=removerEspacoInicialFinal(this.value);"
                                                       maxlength="18" size="32" value="${chamado.rgSolicitante}">
                                            </div>

                                            <div class="col-md-12">
                                                <label for="telefoneSolicitante">Telefone:</label>
                                                <input type="text" class="form-control form-control-sm" id="telefoneSolicitante"
                                                       name="telefoneSolicitante" placeholder="Telefone do solicitante"
                                                       maxlength="30" size="32" value="${chamado.telefoneSolicitante}">
                                            </div>

                                            <div class="col-md-12">
                                                <label for="emailSolicitante">E-mail:</label>
                                                <input type="email" class="form-control form-control-sm" id="emailSolicitante"
                                                       name="emailSolicitante" placeholder="E-mail do solicitante"
                                                       maxlength="80" size="32" value="${chamado.emailSolicitante}"
                                                       onblur="this.value=removerEspacoInicialFinal(this.value);">
                                            </div>

                                            <div class="col-md-12">
                                                <label for="tipoContatoSolicitante">Função:</label>
                                                <select name="tipoContatoSolicitante" id="tipoContatoSolicitante"
                                                        class="form-control form-control-sm">
                                                    <option value="-1">Selecione</option>
                                                    <c:forEach items="${listaTiposContato}" var="tipoContato">
                                                        <option value="<c:out value="${tipoContato.chavePrimaria}"/>"
                                                                <c:if test="${chamado.tipoContatoSolicitante.chavePrimaria == tipoContato.chavePrimaria}">selected="selected"</c:if>>
                                                            <c:out value="${tipoContato.descricao}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabCliente" role="contentTabCliente"
                                 aria-labelledby="contentTabCliente">
                                <div id="divContrato">
                                    <jsp:include
                                            page="/jsp/atendimento/chamado/bootstrap/divContratoBootstrap.jsp"></jsp:include>
                                </div>
                                <div id="divCliente">
                                    <jsp:include
                                            page="/jsp/atendimento/chamado/bootstrap/abaClienteBootstrap.jsp"></jsp:include>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contentTabImovel" role="contentTabImovel"
                                 aria-labelledby="contentTabImovel">
                                <div id="chamadoAbaImovel">
                                    <jsp:include
                                            page="/jsp/atendimento/chamado/bootstrap/abaImovelBootstrap.jsp"></jsp:include>
                                </div>
                            </div>
                            <c:if test="${fluxoDetalhamento ne true}">
                                <div class="tab-pane fade" id="contentTabDocs" role="contentTabDocs"
                                     aria-labelledby="contentTabDocs">

                                    <jsp:include
                                            page="/jsp/atendimento/comumchamadoservicoautorizacao/bootstrap/selecionarAnexosBootstrap.jsp">
                                        <jsp:param name="actionAdicionarAnexo" value="adicionarAnexoChamado"/>
                                        <jsp:param name="actionRemoverAnexo" value="removerAnexoChamado"/>
                                        <jsp:param name="actionVisualizarAnexo" value="visualizarAnexoChamado"/>
                                        <jsp:param name="actionLimparCampoArquivo" value="limparCampoArquivoChamado"/>
                                        <jsp:param name="nomeForm" value="chamadoForm"/>
                                    </jsp:include>
                                </div>
                            </c:if>
                            <div class="tab-pane fade" id="contentTabEmail" role="contentTabEmail"
                                 aria-labelledby="contentTabEmail">
                                <div class="row">
                                    <div class="col-md-12 pb-3">
                                        <c:if test="${fluxoInclusao eq true ||  fluxoAlteracao eq true }">
                                            <div class="form-row">
                                                <div class="col-md-12">
                                                    <label for="descricaoEmail">E-mail:</label>
                                                    <input class="form-control form-control-sm" type="email" id="descricaoEmail"
                                                           name="descricaoEmail" maxlength="100"
                                                           onblur="this.value=removerEspacoInicialFinal(this.value);"
                                                           onkeypress="formatarCampoTexto(this.value);">
                                                    <input name="indexListaChamadoEmail" type="hidden" id="indexListaChamadoEmail">
                                                </div>
                                            </div>

                                            <button class="btn btn-primary btn-sm mt-2" type="button" id="bottonAdicionarEmail"
                                                    name="button"
                                                    onclick="adicionarEmail( ); limparElemento(descricaoEmail);">
                                                <i class="fa fa-plus-circle"></i> Adicionar Email
                                            </button>
                                            <button class="btn btn-primary btn-sm mt-2" type="button" disabled="disabled"
                                                    id="bottonAlterarEmail" name="button"
                                                    onclick="alterarChamadoEmail(); limparElemento(descricaoEmail);desabilitarHabilitarBotton('bottonAdicionarEmail','bottonAlterarEmail');">
                                                <i class="fa fa-edit"></i> Alterar Email
                                            </button>
                                        </c:if>

                                        <div id="gridChamadoEmail">
                                            <jsp:include
                                                    page="/jsp/atendimento/chamado/bootstrap/gridChamadoEmailBootstrap.jsp"></jsp:include>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <c:if test="${ fluxoInclusao ne true}">
                                <div class="tab-pane fade" id="contentTabHistorico" role="contentTabHistorico"
                                     aria-labelledby="contentTabHistorico">
                                    <div class="row mb-2">
                                        <div class="col-md-12 pb-3 mb-2">
                                            <c:if test="${ fluxoDetalhamento eq true || fluxoAlteracao eq true || fluxoTramitacao eq true || fluxoAlteracaoRascunho eq true || fluxoReiteracao eq true || fluxoReativacao eq true  || fluxoReabertura eq true || fluxoEncerramento eq true}">

                                                <jsp:include
                                                        page="/jsp/atendimento/chamado/bootstrap/gridChamadoHistoricoBootstrap.jsp"></jsp:include>
                                                 	<button name="button"
				                                            class="btn btn-primary btn-sm mb-1 mr-1"
				                                            type="button"
				                                            onclick="enviarEmailHistoricoChamado(${chamado.chavePrimaria})">
				                                        <i class="fa fa-mail-bulk"></i> Enviar Histórico
				                                    </button>

                                                <hr/>

                                                <jsp:include
                                                        page="/jsp/atendimento/chamado/bootstrap/gridChamadoHistoricoAnexoBootstrap.jsp"></jsp:include>

                                            </c:if>
                                            <c:if test="${ fluxoDetalhamento eq true or fluxoTramitacao eq true or fluxoReiteracao eq true or fluxoEncerramento eq true }">
                                                <hr/>

                                                <jsp:include
                                                        page="/jsp/atendimento/chamado/bootstrap/gridChamadoServicoAutorizacaoBootstrap.jsp"></jsp:include>

                                            </c:if>

                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <div class="tab-pane fade" id="contentTabControleOperacional" role="contentTabControleOperacional"
                                 aria-labelledby="contentTabControleOperacional">

                                <div class="row mb-2">

                                    <div class="col-md-12 pb-3 mb-2">
                                        <div class="form-row" id="divInformacaoGasista">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-2">

                                    <div class="col-md-12 pb-3 mb-2">
                                        <div class="form-row">

                                            <div class="col-md-12">
                                                <label for="manifestacao">Manifestação: <span class="text-danger">*</span></label>
                                                <select name="manifestacao" id="manifestacao" class="form-control form-control-sm">
                                                    <c:forEach items="${listaTipoManifestacao}" var="manifestacao">
                                                        <option value="<c:out value="${manifestacao.chavePrimaria}"/>"
                                                                <c:if test="${chamado.manifestacao.chavePrimaria == manifestacao.chavePrimaria}">selected="selected"</c:if>>
                                                            <c:out value="${manifestacao.descricao}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 pb-3 mb-2">
                                        <c:if test="${fluxoInclusao eq true ||  fluxoAlteracao eq true }">
                                            <hr/>
                                            <button id="bottonChamadoContato" name="button" class="btn btn-primary btn-sm" type="button"
                                                    onclick='mostraOculta(divChamadoContatoCliente)' disabled="disabled">
                                                <i class="fa fa-user-alt"></i> Contato
                                            </button>

                                            <div style="display:none" id="divChamadoContatoCliente">
                                                <jsp:include
                                                        page="/jsp/atendimento/chamado/bootstrap/abaChamadoContatoBootstrap.jsp"></jsp:include>
                                            </div>
                                        </c:if>
                                        <c:if test="${fluxoDetalhamento eq true}">
                                            <hr class="linhaSeparadoraPesquisa"/>
                                            <div id="divGridContato">
                                                <jsp:include
                                                        page="/jsp/atendimento/chamado/bootstrap/gridAbaChamadoContatoBootstrap.jsp"></jsp:include>
                                            </div>
                                        </c:if>
                                        <c:if test="${ fluxoInclusao eq true }">
                                            <hr/>
                                            <div id="gridChamadosCliente">
                                                <jsp:include
                                                        page="/jsp/atendimento/chamado/bootstrap/gridChamadosClienteBootstrap.jsp"></jsp:include>
                                            </div>

                                            <hr/>
                                            <div id="gridChamadoTabelaPontoConsumo">
                                                <jsp:include
                                                        page="/jsp/atendimento/chamado/bootstrap/gridChamadoTabelaPontoConsumoBootstrap.jsp"></jsp:include>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>

                                <hr/>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <button name="button" class="btn btn-primary btn-sm mt-1" type="button"
                                                onclick="exibirPopupInclusaoCliente();">
                                            <i class="fa fa-plus-circle"></i> Adicionar Cliente
                                        </button>
                                        <button name="button" class="btn btn-primary btn-sm mt-1" type="button"
                                                onclick="exibirPopupInclusaoImovel();">
                                            <i class="fa fa-home"></i> Adicionar Imóvel
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <c:if test="${fluxoInclusao eq true || fluxoDetalhamento eq true || fluxoAlteracao eq true}">
	                            <div class="tab-pane fade" id="contentTabEndereco" role="contentTabEndereco"
	                                 aria-labelledby="contentTabEndereco">
	                                    <jsp:include
	                                            page="/jsp/atendimento/chamado/bootstrap/abaEnderecoBootstrap.jsp"></jsp:include>
	                            </div>
                            </c:if>                            
                                <div class="tab-pane fade" id="contentTabTitulosAbertos" role="contentTabTitulosAbertos"
                                 aria-labelledby="contentTabTitulosAbertos">
	                                <div id="chamadoAbaTitulosAbertos">
	                                    <jsp:include
	                                            page="/jsp/atendimento/chamado/bootstrap/abaTitulosAbertos.jsp"></jsp:include>
	                                </div>
                            	</div>                             
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="form-row mt-2">
                    <div class="col-md-12">
                        <c:if test="${fluxoDetalhamento eq true && fluxoPesquisaSatisfacao ne true}">
                            <div class="mb-4">
                                <button class="btn btn-primary btn-sm mr-1" type="button"
                                        onclick="voltar();">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </button>
                            </div>
                            <jsp:include page="/jsp/atendimento/chamado/botoesChamadoBootstrap.jsp"></jsp:include>
                        </c:if>

                        <c:if test="${ fluxoInclusao eq true }">

                            <div class="row justify-content-between">

                                <div class="col-md-4 col-sm-12 mt-1">
                                    <button name="button"
                                            class="btn btn-default btn-sm mb-1 mr-1"
                                            type="button"
                                            onclick="cancelar()">
                                        <i class="fa fa-reply"></i> Cancelar
                                    </button>
                                    <button name="button"
                                            class="btn btn-danger btn-sm mb-1 mr-1"
                                            type="button"
                                            onclick="limparFormulario()">
                                        <i class="fa fa-times"></i> Limpar
                                    </button>
                                </div>

                                <div class="col-md-8 col-sm-12 mt-1 text-md-right">

									 <vacess:vacess param="mudarTitularidade">
                                        <button id="botaoMudancaTitularidade"
                                                name="button"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                type="button"
                                                disabled="disabled"
                                                onclick="exibirPopupMudancaTitularidade()">
                                            <i class="fa fa-pen-square"></i> Mudanï¿½a de Titularidade
                                        </button>
                                    </vacess:vacess>
                                    <vacess:vacess param="incluirChamadoRascunho">
                                        <button id="botaoIncluirRascunho"
                                                name="button"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                type="button"
                                                onclick="incluirRascunho()">
                                            <i class="fa fa-pen-square"></i> Salvar Rascunho
                                        </button>
                                    </vacess:vacess>
                                    <vacess:vacess param="incluirChamado">
                                        <button name="button"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                type="button"
                                                onclick="incluirCopiar();">
                                            <i class="fa fa-plus-square"></i> Manter Prot./Incluir Chamado
                                        </button>
                                        <button id="botaoSalvarAgendar"
                                                name="button"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                type="button"
                                                onclick="salvarAgendar();">
                                            <i class="fa fa-calendar-check"></i> Salvar e Agendar
                                        </button>
                                        <button name="button" id="botaoIncluir"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                type="button"
                                                onclick="incluir();">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoTramitacao eq true }">
                            <div class="row justify-content-between">

                                <div class="col-md-6 col-sm-12 mt-1">

                                    <button id="btnGroupDrop2" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop2">
                                        <input name="Button" class="dropdown-item" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                               onclick="limparFormTramitacao();">
                                    </div>
                                </div>
                                <vacess:vacess param="tramitarChamado">
                                    <div class="col-md-6 col-sm-12 mt-1">
                                        <button name="buttonTramitar" id="botaoTramitar"
                                                class="btn btn-primary btn-sm float-right"
                                                onclick="tramitarChamado()" type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </vacess:vacess>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoReiteracao eq true }">
                            <div class="row justify-content-between">

                                <div class="col-md-6 col-sm-12 mt-1">

                                    <button id="btnGroupDropReiterarCancelar" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop2">
                                        <input name="Button" class="dropdown-item" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                               onclick="limparFormExpecifico();">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                    <vacess:vacess param="reiterarChamado">
                                        <button id="botaoReiterar" name="buttonReiterar" class="btn btn-primary btn-sm mb-1 mr-1"
                                                onclick="reiterarChamado()"
                                                type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoReativacao eq true }">
                            <div class="row justify-content-between">
                                <div class="col-md-6 col-sm-12 mt-1">
                                    <button id="btnGroupDropReativacao" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDropReativacao">

                                        <input name="Button" class="bottonRightCol" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar"
                                               type="button"
                                               onclick="limparFormExpecifico();">
                                    </div>
                                </div>
                                <vacess:vacess param="reativarChamado">
                                    <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                        <button id="botaoReativar"
                                                name="buttonTramitar"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                onclick="reativarChamado()"
                                                type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </vacess:vacess>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoReabertura eq true }">
                            <div class="row justify-content-between">
                                <div class="col-md-6 col-sm-12 mt-1">
                                    <button id="btnGroupDropReabertura" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDropReabertura">
                                        <input name="Button" class="dropdown-item" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                               onclick="limparFormExpecifico();">
                                    </div>
                                </div>
                                <vacess:vacess param="reabrirChamado">
                                    <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                        <button id="botaoReabrir"
                                                name="buttonTramitar"
                                                class="btn btn-primary btn-sm"
                                                onclick="javascript:reabrirChamado()" type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </vacess:vacess>
                            </div>
                        </c:if>
                        <c:if test="${ fluxoEncerramento eq true }">

                            <div class="row justify-content-between">
                                <div class="col-md-6 col-sm-12 mt-1">
                                    <button id="btnGroupDropEncerramento" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDropEncerramento">
                                        <input name="Button" class="dropdown-item" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                               onclick="limparFormExpecifico();">
                                    </div>
                                </div>

                                <vacess:vacess param="encerrarChamado">
                                    <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                        <button name="buttonEncerrar" id="botaoEncerrar"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                onclick="encerrarChamado()" type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </vacess:vacess>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoAlteracao eq true || fluxoAlteracaoRascunho eq true}">

                            <div class="row justify-content-between">

                                <div class="col-md-6 col-sm-12 mt-1">
                                    <button id="btnGroupDropEditar" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDropEditar">

                                        <input name="Button" class="dropdown-item" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <c:if test="${ fluxoAlteracao eq true}">
                                            <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                                   onclick="limparFormExpecifico();">
                                        </c:if>
                                        <c:if test="${ fluxoAlteracaoRascunho eq true}">
                                            <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                                   onclick="limparFormRascunho();">
                                        </c:if>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                    <vacess:vacess param="alterarChamado">
                                        <button name="Button" id="botaoAlterar" type="button"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                onclick="alterarChamado();">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </vacess:vacess>
                                    <vacess:vacess param="alterarChamadoRascunho">
                                        <button name="button" type="button" id="alterarRascunho"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                onclick="alterarRascunhoChamado();">
                                            <i class="fa fa-pen-square"></i> Salvar Rascunho
                                        </button>
                                    </vacess:vacess>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${ fluxoCapturar eq true }">


                            <div class="row justify-content-between">

                                <div class="col-md-6 col-sm-12 mt-1">
                                    <button id="btnGroupDropCapturar" type="button"
                                            class="btn btn-danger btn-sm dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações (cancelar)
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDropCapturar">

                                        <input name="Button" class="dropdown-item" value="Cancelar" type="button"
                                               onClick="cancelar();">
                                        <input name="Button" class="dropdown-item" value="Limpar" type="button"
                                               onclick="limparFormExpecifico();">
                                    </div>
                                </div>
                                <vacess:vacess param="capturarChamado">
                                    <div class="col-md-6 col-sm-12 mt-1 text-md-right">
                                        <button id="botaoCapturar"
                                                name="buttonCapturar"
                                                class="btn btn-primary btn-sm mb-1 mr-1"
                                                onclick="capturarChamado()"
                                                type="button">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </vacess:vacess>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </form:form>

    <jsp:include page="../../cadastro/cliente/bootstrap/modalPesquisarCliente.jsp"/>

<script src="${ctxWebpack}/dist/modulos/atendimentoAoPublico/chamado/incluirChamado/index.js"
        type="application/javascript" charset="UTF-8"></script>
<script src="${pageContext.request.contextPath}/js/atendimentoAoPublico/chamado/incluirChamado/index.js"
        type="application/javascript" charset="UTF-8"></script>
