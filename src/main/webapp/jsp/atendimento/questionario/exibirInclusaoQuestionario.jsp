<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>


 	<c:if test="${ acao eq 'alterar' }">
 		<h1 class="tituloInterno">Alterar Question�rio</h1>
 		<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Salvar</span></p>
 	</c:if>
 	
 	<c:if test="${ acao eq 'incluir' }">
 		<h1 class="tituloInterno">Incluir Question�rio</h1>
 		<p class="orientacaoInicial">Informe os dados abaixo e clice em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar</p>
 		
 	</c:if>

<script type="text/javascript">
$(document).ready(function(){
	var botaoAlterarPergunta = document.getElementById("botaoAlterarPergunta");
	botaoAlterarPergunta.disabled = true;
	var objetiva = "<c:out value="${pergunta.objetiva}" />"
	if(objetiva!='false') {
		$('#notaMinima').removeAttr("disabled");
		$('#notaMaxuna').removeAttr("disabled");
	} else {
		$('#notaMinima').attr('disabled','disabled');
		$('#notaMaxima').attr('disabled','disabled');
	}
	habilidarDesabilidarCampoIndicadorUso(true);

	$("input[name=objetiva]").change(function(){
		if ($(this).val() != "false"){
			$("#respostaObjetiva").show();
		}else{
			$("#respostaObjetiva").hide();
		}
	});

	$("input[name=valorPersonalizado]").change(function(){
		if ($(this).val() != "false"){
			$("#intervaloNotas").hide();
	        $("#respostaPersonalizada").show();			
		}else{
			$("#intervaloNotas").show();
	        $("#respostaPersonalizada").hide();
		}
	});

	$("#addResposta").click(function(){
		var resposta = $("#resposta").val();
		if (trim(resposta).length > 0){
			var option = new Option(resposta,resposta);
			 $('#listaRespostaPersonalizada').append(option);
			 $("#resposta").val("");
		}
	});
	
});


function adicionarPergunta(){
	var chavePrimaria = document.getElementById('chavePrimaria').value;
	var nomePergunta = document.getElementById("nomePergunta").value;
	var objetiva = false;
	if(document.forms[0].objetiva[0].checked){
		objetiva = true;
	}
	
	var notaMinima = document.getElementById("notaMinima").value;
	var notaMaxima = document.getElementById("notaMaxima").value;
	var indexList;
	if(document.forms[0].indexList.value==null){
		indexList = "0";
	}else{
		indexList = document.forms[0].indexList.value;
	}
	
	var habilitado = true;
	var acao = "<c:out value="${acao}" />"
	if(acao != 'incluir'){
		if(!document.forms[0].habilitado3[0].checked){
			habilitado = false;
		}
	}

	var valorPersonalizado = $("input[name=valorPersonalizado]:checked").val();
	var multiplaEscolha = $("#multiplaEscolha").is(":checked");
	var respostaObrigatoria = $("#obrigatoriaSim").is(":checked");

	var alternativas = [];
	
	$("#listaRespostaPersonalizada option").each(function(){
		alternativas.push($(this).val());
	});
	
	if(document.forms[0].objetiva[0].checked == false ){
	
		var url = "carregarAdicionarPergunta?chavePrimariaQuestionario="+chavePrimaria
				+"&"+"nomePergunta="+nomePergunta
				+"&"+"objetiva="+objetiva
				+"&"+"indexList="+indexList
				+"&"+"habilitado="+habilitado
				+"&"+"respostaObrigatoria="+respostaObrigatoria
				+"&"+"multiplaEscolha="+multiplaEscolha
				+"&"+"alternativas="+alternativas
				+"&"+"valorPersonalizado="+valorPersonalizado;
		
	} else {
		var url = "carregarAdicionarPergunta?chavePrimariaQuestionario="+chavePrimaria
				+"&"+"nomePergunta="+nomePergunta
				+"&"+"objetiva="+objetiva
				+"&"+"indexList="+indexList
				+"&"+"habilitado="+habilitado
				+"&"+"respostaObrigatoria="+respostaObrigatoria
				+"&"+"multiplaEscolha="+multiplaEscolha
				+"&"+"alternativas="+alternativas
				+"&"+"valorPersonalizado="+valorPersonalizado
				+"&"+"notaMinima="+notaMinima
				+"&"+"notaMaxima="+notaMaxima;
		
	}
	
	 var isSucesso = carregarFragmento('gridPerguntasQuestionario',url);
	 
	 if(isSucesso){ 
		document.getElementById("nomePergunta").value ='';
		document.forms[0].objetiva[1].checked = true;
		document.getElementById("notaMinima").value ='';
		document.getElementById("notaMaxima").value=''; 
	 	document.getElementById("indexList").value = null;
	 	document.getElementById("botaoAlterarPergunta").disabled=true;
    	document.getElementById("botaoAdicionar").disabled=false;
    	$("#valorPersonalizadoFalse").attr("checked", true);
    	$("#intervaloNotas").show();
        $("#respostaPersonalizada").hide();
    	$("#multiplaEscolha").attr("checked", false);
    	$("#respostaObjetiva").hide();
    	$("#listaRespostaPersonalizada option").remove();
	 }
 
// 	$("#gridPerguntasQuestionario").load ("carregarAdicionarPergunta?chavePrimariaQuestionario="+chavePrimaria+"&"+"nomePergunta="+nomePergunta+"&"+"objetiva="+objetiva+"&"+"notaMinima="+notaMinima+"&"+"notaMaxima="+notaMaxima);
}

function desalibitarHabilitarCamposNotas(valor){
	document.getElementById("notaMinima").value = '';
	document.getElementById("notaMaxima").value = '';
	document.getElementById("notaMinima").disabled = valor;
	document.getElementById("notaMaxima").disabled = valor;
}

function salvar(){
	 submeter('formQuestionario','inserirQuestionario');
}

function alterarQuestionario(){
	
	submeter('formQuestionario','alterarQuestionario');
}

function habilidarDesabilidarCampoIndicadorUso(valor){
	if(document.forms[0].chavePrimaria.value!=0) {
		document.forms[0].habilitado3[0].disabled = valor;
		document.forms[0].habilitado3[1].disabled = valor;
	}
}

function cancelar(){
	submeter('formQuestionario','cancelarOperacao');
}

function limparCampos(){
	submeter('formQuestionario','exibirInclusaoQuestionario');
}

function carregarParaAlterarPergunta(indexPergunta, perguntaJSON){

	var pergunta = JSON.parse(perguntaJSON);

	var botaoAlterarPergunta = document.getElementById("botaoAlterarPergunta");
	botaoAlterarPergunta.disabled = false;
	
	var botaoAdicionar = document.getElementById("botaoAdicionar");
	botaoAdicionar.disabled = true;
	habilidarDesabilidarCampoIndicadorUso(false);
	document.getElementById("nomePergunta").value = pergunta.nomePergunta;

	if(pergunta.respostaObrigatoria){
		$("#obrigatoriaSim").attr("checked", true);
	}else{
		$("#obrigatoriaNao").attr("checked", true);
	}
	
	if(pergunta.objetiva){
		document.forms[0].objetiva[0].checked = true;
		$("#respostaObjetiva").show();
		desalibitarHabilitarCamposNotas(false);
	}else{
		$("#respostaObjetiva").hide();
		document.forms[0].objetiva[1].checked = true;
		desalibitarHabilitarCamposNotas(true);
	}
	
	var acao = "<c:out value="${acao}" />"
	if(acao == 'alterar'){
		if(pergunta.habilitado){
			document.forms[0].habilitado3[0].checked = true;
		}else{
			document.forms[0].habilitado3[1].checked = true;
		}
	}

	if (pergunta.valorPersonalizado){
		$("#intervaloNotas").hide();
        $("#respostaPersonalizada").show();
		$("#valorPersonalizadoTrue").attr("checked", true);
	}else{
		$("#intervaloNotas").show();
        $("#respostaPersonalizada").hide();
		$("#valorPersonalizadoFalse").attr("checked", true);
		document.getElementById("notaMinima").value = pergunta.notaMinima;
		document.getElementById("notaMaxima").value= pergunta.notaMaxima;
	}

	if (pergunta.multiplaEscolha){
		$("#multiplaEscolha").attr("checked", true);
	}

	$('#listaRespostaPersonalizada option').remove()
	pergunta.alternativas.forEach(function(element, index){
		var option = new Option(element.nomeAlternativa,element.nomeAlternativa);
		 $('#listaRespostaPersonalizada').append(option);
	});
	
	document.forms[0].indexList.value = indexPergunta;
	
}

function excluirPergunta(valorChave){
	carregarFragmento('gridPerguntasQuestionario', "removerPergunta?indexPergunta="+valorChave);
}

function alterarOrdemListaPerguntas(indexPergunta, acao){	
	var noCache = "noCache=" + new Date().getTime();
	$("#gridPerguntasQuestionario").load("alterarOrdemListaPerguntas?indexPergunta="+indexPergunta+"&"+"acao="+acao+"&"+noCache);	
}

function removerRespostas(){
	$("#listaRespostaPersonalizada :selected").remove();
}

</script>

<form:form method="post" id="formQuestionario" name="formQuestionario" modelAttribute="Questionario">

<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${questionario.chavePrimaria}">
<input type="hidden" name="versao" id="versao" value="${questionario.versao}">
 <input type="hidden" name="indexList" id="indexList" >
  <input type="hidden" name="habilitadoPergunta" id="habilitado" >
 

<fieldset class="conteinerPesquisarIncluirAlterar">

 <fieldset id="pesquisarQuestionario" class="coluna2">
	     	<label class="rotulo"  id="rotuloDescricaoQuesqionario" for="descricaoQuestionario" > <span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Nome do Question�rio:</label>
	     	<input type="text" id="nomeQuestionario" name="nomeQuestionario" class="compoTexto" value="${questionario.nomeQuestionario}" size="50" maxlength="250" onkeypress="return formatarCampoTextoNomeQuestionario(event)" onkeyup="letraMaiuscula(this);" style="margin-top: 6px"><br />
	     	<label class="rotulo2Linhas"> <span id="spanDescricao" class="campoObrigatorioSimbolo" style="margin-top: 0px"	>* </span>Tipo Question�rio: </label>
		     	<input class="campoRadio" type="checkbox"  name="tipoQuestionarioChamado" id="tipoQuestionarioChamado" value="true" <c:if test="${questionario.tipoQuestionarioChamado}"> checked="checked" </c:if> />
		     	<label class="rotuloRadio" for="tipoQuestionarioChamado" >&nbsp;Chamado&nbsp;</label>
		     	<input class="campoRadio" type="checkbox"  name="tipoQuestionarioAS" id="tipoQuestionarioAS"  value="true" <c:if test="${questionario.tipoQuestionarioAS}"> checked="checked" </c:if> />
		     	<label class="rotuloRadio" for="tipoQuestionarioAS" >&nbsp;Autoriza��o de Servi�o&nbsp;</label><br /><br /><br />
	     	
	     </fieldset>
	     
	     <fieldset id="pesquisarQuestionario2" class="colunaFinal20">
	     <label class="rotulo" id="rotuloSegmento" for="segmento">Segmento:</label>
	     	<select name="segmento" class="campoSelect" id="segmento" ><br />
                <option value="-1">Selecione</option>
                <c:forEach items="${listaSegmento}" var="segmento">
                    <option value="<c:out value="${segmento.chavePrimaria}"/>"  <c:if test="${questionario.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${segmento.descricao}"/>
                    </option>       
                </c:forEach>    
            </select> 
            <c:if test="${ acao eq 'alterar' }">
            <br />
	  			<label class="rotulo" id="rotuloHabilitado" for="habilitado">Indicador de Uso:</label>
			    <input class="campoRadio" type="radio" name="habilitado" id="habilitado1" value="true" <c:if test="${questionario.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
			   	<input class="campoRadio" type="radio" name="habilitado" id="habilitado2" value="false" <c:if test="${questionario.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
             	<br />
            </c:if>
            
	     </fieldset>
	     <br> <br><br>
	     
	     <fieldset class="pesquisarChamadoAssunto" style="float: left; margin:0 0 20px 0">
		       	<h1 style="font-size: 16; font-weight: bold; ">Perguntas</h1> <br/>
		       	
		       	<fieldset class="colunaFinal">
			       	<label class="rotulo" id="rotuloNomePergunta" for="nomePergunta" ><span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Pergunta:</label>
			       	<input type="text" id="nomePergunta" name="nomePergunta" class="texto" value="${pergunta.nomePergunta}" onkeypress="return formatarCampoPergunta(event)" onkeyup="letraMaiuscula(this);" size="80" maxlength="200" style="margin-top: 6px"> <br />
			       	
			       	<label class="rotulo2Linhas" id="rotuloPerguntaObrigatoria"> <span id="spanDescricao" class="campoObrigatorioSimbolo" style="margin-top: 0px">* </span>Resposta Obrigat�ria?</label>
			     	<input class="campoRadio" type="radio"  name="respostaObrigatoria" id="obrigatoriaSim"  value="true"  <c:if test="${pergunta.respostaObrigatoria eq true}">checked</c:if>>
			     	<label class="rotuloRadio" for="obrigatoriaSim" >&nbsp;Sim&nbsp;</label>
			     	<input class="campoRadio" type="radio"  name="respostaObrigatoria" id="obrigatoriaNao"  value="false" <c:if test="${pergunta.respostaObrigatoria eq false}">checked</c:if>>
			     	<label class="rotuloRadio" for="obrigatoriaNao" >&nbsp;N�o&nbsp;</label><br /><br /><br />
			     	
			       	<label class="rotulo2Linhas" id="rotuloPerguntaObjetiva"> <span id="spanDescricao" class="campoObrigatorioSimbolo" style="margin-top: 0px"	>* </span>Resposta Objetiva?</label>
			     	<input class="campoRadio" type="radio"  name="objetiva" id="objetiva"  value="true"  <c:if test="${pergunta.objetiva eq true}">checked</c:if> onclick="desalibitarHabilitarCamposNotas(false)" >
			     	<label class="rotuloRadio" id="rotuloTipoDeServico" for="objetiva" >&nbsp;Sim&nbsp;</label>
			     	<input class="campoRadio" type="radio"  name="objetiva" id="objetiva"  value="false" <c:if test="${pergunta.objetiva eq false}">checked</c:if> onclick="desalibitarHabilitarCamposNotas(true)">
			     	<label class="rotuloRadio" id="rotuloTipoDeServico" for="objetiva" >&nbsp;N�o&nbsp;</label><br /><br /><br />

					<div id="respostaObjetiva" style="display: none">
				       	<label class="rotulo2Linhas" id="rotuloValoresPersonalizados"> Valores Personalizados?</label>
				     	<input class="campoRadio" type="radio"  name="valorPersonalizado" id="valorPersonalizadoTrue"  value="true"  <c:if test="${pergunta.valoresPersonalizados eq true}">checked</c:if> />
				     	<label class="rotuloRadio" for="valorPersonalizadoTrue" >&nbsp;Sim&nbsp;</label>
				     	<input class="campoRadio" type="radio"  name="valorPersonalizado" id="valorPersonalizadoFalse"  value="false" <c:if test="${pergunta.valoresPersonalizados eq false}">checked</c:if> />
				     	<label class="rotuloRadio" for="valorPersonalizadoFalse" >&nbsp;N�o&nbsp;</label><br /><br /><br />
				       
				       	<div id="intervaloNotas" style="margin-left: 40px">
					      	<label class="rotulo rotuloNotaMinima" id="rotuloNotaMinima" for="notaMinima">Nota m�nima:</label>
					       	<input type="text" id="notaMinima" name="notaMinima" value ="${pergunta.notaMinima}" class="compoTexto" style="margin-top: 5px" size="2" maxlength="2" onkeypress="return formatarCampoInteiro(event)"><br />
					       	<label class="rotulo" id="rotuloNotaMaxima" for="notaMaxima">Nota m�xima:</label>
					       	<input type="text" id="notaMaxima" name="notaMaxima" value="${pergunta.notaMaxima}" class="compoTexto notaMaxima" size="2" maxlength="2" onkeypress="return formatarCampoInteiro(event)"><br />
				       	</div>
				       	
				       	<div id="respostaPersonalizada" style="display: none; margin-left: 40px">
					      	<input type="checkbox" name="multiplaEscolha" id="multiplaEscolha" value="true" <c:if test="${pergunta.multiplaEscolha eq true}">checked</c:if> />
					       	<label class="rotulo" for="multiplaEscolha">Multipla Escolha</label>

							<br />
					       	<label class="rotulo" for="resposta">Alternativa: </label>					       	
					       	<input type="text" name="resposta" id="resposta" style="float: left;margin-top: 5px;"  maxlength="256"/>
					       	<img id="addResposta" src="<c:url value='/imagens/add-16x16.png' />" border="0" style="float: left;margin: 6px;"/>
					       	
					       	
					       	<select id="listaRespostaPersonalizada" name="respostaPersonalizada" class="campoList campoVertical" size="5" multiple="multiple">					       	
					       	</select> 
					       	<input class="bottonLeftRotulo" id="removerResposta" value="Remover" type="button" onclick="removerRespostas()" style="margin-top: 98px;" />
				       	</div>
					</div>
			       	
			       	<c:if test="${ acao eq 'alterar' }">
				       	<label class="rotulo" id="rotuloHabilitado" for="habilitado3">Indicador de Uso:</label>
			    		<input class="campoRadio" type="radio" name="habilitado3" id="habilitado3" value="true" <c:if test="${pergunta.habilitado == 'true'}">checked</c:if>><label class="rotuloRadio">Ativo</label>
			   			<input class="campoRadio" type="radio" name="habilitado3" id="habilitado3" value="false" <c:if test="${pergunta.habilitado == 'false'}">checked</c:if>><label class="rotuloRadio">Inativo</label>
			       	</c:if>
			       	  	<br />
					<hr class="linhaSeparadoraPesquisa" />
					
					<fieldset class="conteinerBotoesPesquisarDir">
			   			<input name="Button" class="bottonRightCol2" id="botaoAdicionar" value="Adicionar pergunta" type="button" onclick="adicionarPergunta()">
			   			<input name="Button" class="bottonRightCol2" id="botaoAlterarPergunta" value="Alterar pergunta" type="button" onclick="adicionarPergunta()">
					</fieldset>
				</fieldset>	       
	      </fieldset>
	      
	      
<%-- 		<c:if test="${!empty listaPerguntas}"> --%>
	       <fieldset id="gridPerguntasQuestionario" class="conteinerBloco" style="width: 95.8%">
	       		<jsp:include page="/jsp/atendimento/questionario/gridPerguntas.jsp"></jsp:include>
	       </fieldset>
<%-- 	   </c:if> --%>
	      
	      
	      
	      <hr class="linhaSeparadoraPesquisa" />
	      
	      <fieldset class="conteinerBotoesPesquisarDir" >
	      	  <c:if test="${ acao eq 'incluir' }">
	      	  <vacess:vacess param="incluirQuestionario">
	      	<input name="Button" class="bottonRightCol2" id="botaoIncluir" value="Salvar" type="button" onclick="salvar()">
	      	</vacess:vacess>
	      	</c:if>
	      	<c:if test="${ acao eq 'alterar' }">
	      	<vacess:vacess param="alterarQuestionario">
	      	<input name="Button" class="bottonRightCol2" id="botaoAlterar" value="Salvar" type="button" onclick="alterarQuestionario()">
	      	</vacess:vacess>
	      	</c:if>
	      </fieldset>
	      <fieldset  class="conteinerBotoesPesquisarEsq">
	      	<input name="Button" class="bottonLeftRotulo" id="botaoCancelar" value="Cancelar" type="button" onclick="cancelar()">
	      	<input name="Button" class="bottonLeftRotulo" id="botaoLimpar" value="Limpar" type="button" onclick="limparCampos()">
	      </fieldset>
	      
	      
	       


</fieldset>



</form:form>