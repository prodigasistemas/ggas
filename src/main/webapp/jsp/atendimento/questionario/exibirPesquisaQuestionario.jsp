<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Pesquisar Question�rio</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script type="text/javascript">

function pesquisar(){
	submeter('formPesquisaQuestionario','pesquisarQuestionario');
}

function limpar(){
	document.getElementById("nomeQuestionario").value="";
	document.getElementById("segmento").value="-1";
	document.forms[0].habilitado[2].checked = true;
	$("#tipoQuestionarioAS").attr('checked', false)
	$("#tipoQuestionarioChamado").attr('checked', false)
}

function detalharQuestionario(valorChave){
	document.forms[0].chavePrimaria.value = valorChave;
	submeter('formPesquisaQuestionario','exibirDetalharQuestionario');
}

function incluir(){
	submeter('formPesquisaQuestionario','exibirInclusaoQuestionario');
}

function exibirAlterarChamado() {
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('formPesquisaQuestionario','exibirAlteracaoQuestionario');
    }
	
}

function removerQuestionario(){
	if(confirm('Deseja excluir o Questionario?')){
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {	
			var chavePrimaria = document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('formPesquisaQuestionario','removerQuestionario?chavePrimaria='+chavePrimaria);
		}
	}
}



</script>

<form:form method="post" id="formPesquisaQuestionario" name="formPesquisaQuestionario" modelAttribute="QuestionarioVO" >
<input name="chavePrimaria" type="hidden" id="chavePrimaria">
	<fieldset class="conteinerPesquisarIncluirAlterar">
	     
	     <fieldset id="funcionarioCol2" class="coluna">
	     	<label class="rotulo"  id="rotuloDescricaoQuesqionario" for="descricaoQuestionario" >Nome do Question�rio:</label>
	     	<input type="text" id="nomeQuestionario" name="nomeQuestionario" class="compoTexto" value="${questionarioVO.nomeQuestionario}"style="margin-left: 10px;margin-top: 5px;" size="40" maxlength="40" onkeyup="letraMaiuscula(this);">
	     	
	     	<label class="rotulo2Linhas"> <span id="spanDescricao" class="campoObrigatorioSimbolo" style="margin-top: 0px"	>* </span>Tipo Question�rio: </label>
		     	<input class="campoRadio" type="checkbox"  name="tipoQuestionarioChamado" id="tipoQuestionarioChamado" value="true" <c:if test="${questionarioVO.tipoQuestionarioChamado}"> checked="checked" </c:if> />
		     	<label class="rotuloRadio" for="tipoQuestionarioChamado" >&nbsp;Chamado&nbsp;</label>
		     	<input class="campoRadio" type="checkbox"  name="tipoQuestionarioAS" id="tipoQuestionarioAS"  value="true" <c:if test="${questionarioVO.tipoQuestionarioAS}"> checked="checked" </c:if> />
		     	<label class="rotuloRadio" for="tipoQuestionarioAS" >&nbsp;Autoriza��o de Servi�o&nbsp;</label><br /><br /><br />
	     	
	     	 <label class="rotulo" id="rotuloSegmento" for="segmento">Segmento:</label>
	     	<select name="segmento" class="campoSelect" id="segmento" >
                <option value="-1">Selecione</option>
                <c:forEach items="${listaSegmento}" var="segmento">
                    <option value="<c:out value="${segmento.chavePrimaria}"/>"  <c:if test="${questionarioVO.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${segmento.descricao}"/>
                    </option>       
                </c:forEach>    
            </select>
	     
	     </fieldset>
	     
 		<fieldset id="pesquisarQuestionario2" class="colunaFinal">
	      
	     	<label class="rotulo" id="rotuloIndicadorUso" for="habilitado" style="margin-left: 10px;">Indicado de Uso:</label>
			<input class="campoRadio" type="radio"  name="habilitado" id="habilitado"  value="1" <c:if test="${questionarioVO.habilitado eq true}">checked</c:if>>
			<label class="rotuloRadio" id="rotuloHabilitado" for="habilitado" >&nbsp;Ativo&nbsp;</label>
			<input class="campoRadio" type="radio"  name="habilitado" id="habilitado"  value="0" <c:if test="${questionarioVO.habilitado eq false}">checked</c:if>>
			<label class="rotuloRadio" id="rotuloHabilitado" for="habilitado">&nbsp;Inativo&nbsp;</label>
			<input class="campoRadio" type="radio"  name="habilitado" id="habilitado"   value=""  <c:if test="${questionarioVO.habilitado eq null}">checked</c:if>>
			<label class="rotuloRadio" id="rotuloHabilitado" for="habilitado">&nbsp;Todos&nbsp;</label>
	     
	     </fieldset>
	     
	     <fieldset class="conteinerBotoesPesquisarDirFixo">
	   		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="button" onclick="pesquisar();">
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limpar();">		
		</fieldset>
	 </fieldset>    
	  <c:if test="${listaQuestionarios ne null}">   
	     <hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaQuestionarios" sort="list" id="questionario" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="10" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarQuestionario">
	        <display:column sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
	         	<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${questionario.chavePrimaria}">
	        </display:column>	  
	        
	        <display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${questionario.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>
				</c:choose>
			</display:column> 
	             
	        <display:column sortable="true" title="Nome do Question�rio" sortProperty="nomeQuestionario" style="text-align: center;">
	        	<a href="javascript:detalharQuestionario(<c:out value='${questionario.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${questionario.nomeQuestionario}"/>
	            </a>
	        </display:column>
	        
	         <display:column sortable="true" title="Tipo de Servi�o" sortProperty="tipoServico.descricao" style="text-align: center;">
	        	<a href="javascript:detalharQuestionario(<c:out value='${questionario.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${questionario.tipoServico.descricao}"/>
	            </a>
	        </display:column>
	        
	         <display:column sortable="true" title="Segmento" sortProperty="segmento.descricao" style="text-align: center;">
	        	<a href="javascript:detalharQuestionario(<c:out value='${questionario.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${questionario.segmento.descricao}"/>
	            </a>
	        </display:column>
	        
	         <display:column sortable="true" title="Quantidade de Perguntas"  sortProperty= "totalPerguntas"  style="text-align: center; width: 5%"  >
	        	<a href="javascript:detalharQuestionario(<c:out value='${questionario.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${questionario.totalPerguntas}"/>
	            </a>
	        </display:column>
	     </display:table>
	    </c:if> 
	     

	     
	      <hr class="linhaSeparadoraPesquisa" />
	   <fieldset class="conteinerBotoesPesquisarDir">
	       <vacess:vacess param="exibirInclusaoQuestionario">
	   		<input name="Button" class="bottonRightCol2" id="botaoIncluir" value="Incluir" type="button" onclick="incluir()">
			</vacess:vacess>
	</fieldset>
	 <c:if test="${!empty listaQuestionarios}">
	<fieldset  class="conteinerBotoesPesquisarEsq">
	<vacess:vacess param="exibirAlteracaoQuestionario">
	 		<input name="Button" class="bottonRightCol2" id="botaoAlterar" value="Alterar" type="button" onclick="exibirAlterarChamado()">
	 </vacess:vacess>
	 <vacess:vacess param="removerQuestionario">
			<input name="Button" class="bottonRightCol2" id="botaoRemover" value="Remover" type="button" onclick="removerQuestionario()">
	</vacess:vacess>				
	</fieldset>
	</c:if>
	
</form:form>
 