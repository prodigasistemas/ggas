<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 
<%@ page contentType="text/html; charset=iso-8859-1" %>

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:set var="indexPergunta" value="0" />
 <display:table class="dataTableGGAS" sort="list" name="sessionScope.listaPerguntas" id="pergunta" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">

				<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${pergunta.habilitado}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
	          <display:column sortable="false"  title="Pergunta" style="text-align: center;">
	            	<c:out value="${pergunta.nomePergunta}"/>
	        </display:column>
	        <display:column  title="Resposta Obrigat�ria" sortable="false"  style="text-align: center; width: 70px ">
	            	 <c:if test="${pergunta.respostaObrigatoria eq false}">N�o</c:if>
	            	  <c:if test="${pergunta.respostaObrigatoria eq true}">Sim</c:if>
	        </display:column>
	        <display:column  title="Resposta Objetiva" sortable="false"  style="text-align: center; width: 70px ">
	            	 <c:if test="${pergunta.objetiva eq false}">N�o</c:if>
	            	  <c:if test="${pergunta.objetiva eq true}">Sim</c:if>
	        </display:column>
	        <display:column title="Nota M�nima" sortable="false" style="text-align: center;  width: 30px">
	            	<c:out value="${pergunta.notaMinima}"/>
	        </display:column>
	          <display:column  title="Nota M�xima" sortable="false" style="text-align: center; width: 30px">
	            	<c:out value="${pergunta.notaMaxima}"/>
	        </display:column>
	        <c:if test="${acao eq null || acao ne 'detalhar'}" >
	          <display:column  style="text-align: center;"> 
                	<c:if test="${indexPergunta > 0}">
               			<a href="javascript:alterarOrdemListaPerguntas(<c:out value="${indexPergunta}"/>, 'subirPosicao')"><img title="Alterar Posi��o Pergunta" alt="Alterar Posi��o Pergunta" src=<c:url value="/imagens/setaCima.png"/>></a> 
                	</c:if>

                	<c:if test="${fn:length(listaPerguntas) - 1 !=  indexPergunta }">
                		<a href="javascript:alterarOrdemListaPerguntas(<c:out value="${indexPergunta}"/>, 'descerPosicao')"><img title="Alterar Posi��o Pergunta" alt="Alterar Posi��o Pergunta" src=<c:url value="/imagens/setaBaixo.png"/>></a>
                	</c:if>
                	
                </display:column>
	          <display:column sortable="false" style="text-align: center; ">
	           	<a href='javascript:carregarParaAlterarPergunta("${indexPergunta}",JSON.stringify(${pergunta.perguntaJSON}));'><span class="linkInvisivel"></span>
	            	<img id="alterarPergunta" title="Alterar pergunta" alt="Alterar Pergunta"  src="<c:url value="/imagens/16x_editar.gif"/>" border="0">
	            	</a>
	        </display:column>
	          <display:column sortable="false" style="text-align: center;  ">
	           	<a onclick="return confirm('Deseja excluir a pergunta?');" href="javascript:excluirPergunta(<c:out value='${indexPergunta}'/>);"><span class="linkInvisivel"></span>
	            	<img title="Excluir pergunta" alt="Excluir Pergunta"  src="<c:url value="/imagens/deletar_x.png"/>" border="0">
	            </a>
	        </display:column>
	        </c:if>
	        <c:set var="indexPergunta" value="${indexPergunta+1}" />
</display:table>

