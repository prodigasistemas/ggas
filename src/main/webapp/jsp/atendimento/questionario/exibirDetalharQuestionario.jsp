<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h1 class="tituloInterno">Detalhar Question�rio</h1>
 <p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script type="text/javascript">

function exibirAlterarQuestionario(){
	submeter('formQuestionario','exibirAlteracaoQuestionario');
}

function cancelar(){
	submeter('formQuestionario','cancelarOperacao');
}
</script>


<form:form method="post" id="formQuestionario" name="formQuestionario" modelAttribute="Questionario">

<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${questionario.chavePrimaria}">

<fieldset class="conteinerPesquisarIncluirAlterar" disabled="disabled">

 
 <fieldset id="pesquisarQuestionario" class="coluna2">
	     	<label class="rotulo"  id="rotuloDescricaoQuesqionario" for="descricaoQuestionario" > <span id="spanDescricao" class="campoObrigatorioSimbolo">* </span>Nome do Question�rio:</label>
	     	<input type="text" id="nomeQuestionario" name="nomeQuestionario" class="compoTexto" value="${questionario.nomeQuestionario}" size="50" maxlength="250" style="margin-top: 6px"><br />
	     	<label class="rotulo2Linhas"> <span id="spanDescricao" class="campoObrigatorioSimbolo" style="margin-top: 0px"	>* </span>Tipo Question�rio: </label>
		     	<input class="campoRadio" type="checkbox"  name="tipoQuestionarioChamado" id="tipoQuestionarioChamado" value="true" <c:if test="${questionario.tipoQuestionarioChamado}"> checked="checked" </c:if> />
		     	<label class="rotuloRadio" for="tipoQuestionarioChamado" >&nbsp;Chamado&nbsp;</label>
		     	<input class="campoRadio" type="checkbox"  name="tipoQuestionarioAS" id="tipoQuestionarioAS"  value="true" <c:if test="${questionario.tipoQuestionarioAS}"> checked="checked" </c:if> />
		     	<label class="rotuloRadio" for="tipoQuestionarioAS" >&nbsp;Autoriza��o de Servi�o&nbsp;</label><br /><br /><br />
	     </fieldset>
	     
	     <fieldset id="pesquisarQuestionario2" class="colunaFinal20">
	     <label class="rotulo" id="rotuloSegmento" for="segmento">Segmento:</label>
	     	<select name="segmento" class="campoSelect" id="segmento" ><br />
                <option value="-1">Selecione</option>
                <c:forEach items="${listaSegmento}" var="segmento">
                    <option value="<c:out value="${segmento.chavePrimaria}"/>"  <c:if test="${questionario.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
                        <c:out value="${segmento.descricao}"/>
                    </option>       
                </c:forEach>    
            </select> 
           
            
	     </fieldset>
	     <br> <br><br>
	     
<%-- 		<c:if test="${!empty listaPerguntas}"> --%>
	       <fieldset id="gridPerguntasQuestionario" class="conteinerBloco" disabled="disabled"  style="width: 95.8%">
	       		<jsp:include page="/jsp/atendimento/questionario/gridPerguntas.jsp"></jsp:include>
	       </fieldset>
<%-- 	   </c:if> --%>
	      
	      
	      
	      
	      
	      <hr class="linhaSeparadoraPesquisa" />
	      
	  
	 
</fieldset>
<fieldset  class="conteinerBotoesPesquisarEsq">
	<input name="Button" class="bottonLeftRotulo" id="botaoCancelar" value="Voltar" type="button" onclick="cancelar()">
</fieldset>
<fieldset class="conteinerBotoesPesquisarDir" >
<vacess:vacess param="exibirAlteracaoQuestionario">
	<input name="Button" class="bottonRightCol2" id="botaoAlterar" value="Alterar" type="button" onclick="exibirAlterarQuestionario()">
	</vacess:vacess>      	
</fieldset>


</form:form>