
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<fieldset class="pesquisarChamadoAssunto" style="float: left; width: 97%"  >
	<legend style="float: left;padding: 10px;">Questionário</legend>
	<br/><br/><br/> 
	<h1>Questionário: <c:out value="${ questionario.nomeQuestionario }" /></h1>
 	<br/>
	<c:set var="index1" value="0" />
	
	<c:forEach var="resposta" items="${respostas}">
		<label id="LabelPergunta${index1}" class="rotulo"> <c:out value="${index1+1} "/>&ensp;-&ensp; <c:out value="${resposta.pergunta.nomePergunta}"/> 
		
		
		<c:if test="${resposta.pergunta.objetiva eq true}">
			<c:choose>
				<c:when test="${resposta.pergunta.valoresPersonalizados eq true}">
					</label><br style="line-height: 35px"/>
					<p>R.: <c:out value="${resposta.descricaoResposta }" /> </p>				
				</c:when>
				<c:otherwise>
					- Valores de <c:out value="${resposta.pergunta.notaMinima }" /> a <c:out value="${resposta.pergunta.notaMaxima }" />
					</label><br style="line-height: 35px"/>
					<p>R.: <c:out value="${resposta.notaResposta }" /> </p>				
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${resposta.pergunta.objetiva eq false}">
			</label><br style="line-height: 35px"/>
		
					<p>R.: <c:out value="${resposta.descricaoResposta }" /> </p>				
		</c:if>
	 	<c:set var="index1" value="${index1+1}" />
	</c:forEach>
 		
</fieldset>