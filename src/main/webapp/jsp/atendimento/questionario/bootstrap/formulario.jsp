<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<%--
    @author jose.victor@logiquesistemas.com.br
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body bg-light">
                <h6>Formul�rio: <c:out value="${ questionario.nomeQuestionario }" /></h6>

                <c:forEach var="pergunta" items="${listaPerguntas}" varStatus="status">
                    <label id="LabelPergunta${status.index}" class="col-sm-12">
                        <c:if test="${pergunta.respostaObrigatoria}">
                            <span class="text-danger"> * </span>
                        </c:if>
                        <c:out value="${status.index+1} - "/>
                        <c:out value="${pergunta.nomePergunta}"/>
                    </label>

                    <c:set var="pergunta" scope="request" value="${pergunta}"/>
                    <c:set var="status" scope="request" value="${status}"/>

                    <c:if test="${pergunta.objetiva}">
                        <c:choose>
                            <c:when test="${pergunta.valoresPersonalizados eq true }">
                                <jsp:include page="incluirRespostaSelect.jsp"/>
                            </c:when>
                            <c:otherwise>
                                <jsp:include page="incluirRespostaNota.jsp"/>
                            </c:otherwise>
                        </c:choose>
                    </c:if>

                    <c:if test="${not pergunta.objetiva}">
                        <jsp:include page="incluirRespostaDiscursiva.jsp"/>
                    </c:if>

                </c:forEach>
            </div>
        </div>
    </div>
</div>

