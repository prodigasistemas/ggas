<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%--
  ~ Copyright (C) <2018> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
  ~
  ~ Este programa � um software livre; voc� pode redistribu�-lo e/ou
  ~ modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
  ~ publicada pela Free Software Foundation; vers�o 2 da Licen�a.
  ~
  ~ O GGAS � distribu�do na expectativa de ser �til,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
  ~ COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
  ~ Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
  ~
  ~ Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
  ~ junto com este programa; se n�o, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
  --%>

<!--
Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->

<%--
    JSP respons�vel por gerenciar a exibi��o dos question�rios de autoriaza��o de servi�o atrav�s de um modal

    Para utiliz�-la importe: web/componentes/responderFormulario/modal.ts e
    adicione esta JSP em sua p�gina com <jsp:include/>

    OBS: N�o Inclua esta JSP dentro de um <form> j� existente, caso contr�rio n�o ir� funcionar corretamente

    Verifique o c�digo fonte 'modal.ts' para mais informa��es sobre o uso desse componente

    @author jose.victor@logiquesistemas.com.br

--%>

<div id="modalResponderFormulario"  class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Responder Formul�rio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="modalResponderFormulario_form">
                <input type="hidden" name="fluxoVoltar" id="fluxoVoltar" >
                <input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${servicoAutorizacao.chavePrimaria}">

                <div class="modal-body">

                    <div class="alert alert-primary" role="alert">
                        <i class="fa fa-lg fa-fw fa-info-circle"></i>
                        Informe os dados abaixo e clique em <b>Responder</b>
                    </div>

                    <div class="alert alert-danger fade fadeIn hide show">
                        <i class="fa fa-lg fa-fw fa-exclamation-circle"></i>
                        <span></span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="modalResponderFormulario_nome">Nome:</label>
                                <input type="text" class="form-control form-control-sm"
                                       name="nomeCliente"
                                       value="${servicoAutorizacao.cliente.nome}"
                                       disabled="true"
                                       id="modalResponderFormulario_nome" />
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalResponderFormulario_pontoConsumo">Ponto de Consumo:</label>
                                <input type="text" class="form-control form-control-sm" disabled="true"
                                       name="pontoConsumo"
                                       id="modalResponderFormulario_pontoConsumo" value="${servicoAutorizacao.pontoConsumo.descricao}" />
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalResponderFormulario_cpfCnpj">CPF / CNPJ:</label>
                                <c:choose>
                                    <c:when test="${servicoAutorizacao.cliente.cpf ne null}">
                                        <c:set var="cpfCnpJ" value="${servicoAutorizacao.cliente.cpfFormatado}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="cpfCnpJ" value="${servicoAutorizacao.cliente.cnpjFormatado}"/>
                                    </c:otherwise>
                                </c:choose>
                                <input type="text" class="form-control form-control-sm" disabled="true"
                                       name="cpfCnpjCliente"
                                       id="modalResponderFormulario_cpfCnpj" value="${cpfCnpJ}" />
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalResponderFormulario_tipoServico">Tipo de Servi�o:</label>
                                <input type="text" class="form-control form-control-sm" disabled="true"
                                       value="${servicoAutorizacao.servicoTipo.descricao}"
                                       name="tipoServico"
                                       id="modalResponderFormulario_tipoServico" />
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="modalResponderFormulario_dataPrevisaoEncerramento">Data de Previs�o de Encerramento:</label>
                                <input type="text" class="form-control form-control-sm" disabled="true"
                                       value="<fmt:formatDate value='${servicoAutorizacao.dataPrevisaoEncerramento}' pattern='dd/MM/yyyy'/>"
                                       name="dataEncerramento" id="modalResponderFormulario_dataPrevisaoEncerramento" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <label for="modalResponderFormulario_telefoneCliente">Telefone(s) do Cliente:</label>
                            <table id="modalResponderFormulario_telefoneCliente" class="table table-bordered table-striped table-hover"
                                   width="100%">
                                <thead class="thead-ggas-bootstrap">
                                <tr>
                                    <th class="text-center">Tipo</th>
                                    <th class="text-center">DDD</th>
                                    <th class="text-center">N�mero</th>
                                    <th class="text-center">Ramal</th>
                                    <th class="text-center">Principal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:choose>
                                    <c:when test="${not empty servicoAutorizacao.cliente.fones}">
                                        <c:forEach items="${servicoAutorizacao.cliente.fones}" var="clienteFone">
                                            <tr>
                                                <td><c:out value="${clienteFone.tipoFone.descricao}"/></td>
                                                <td><c:out value="${clienteFone.codigoDDD} "/></td>
                                                <td><c:out value="${clienteFone.numero} "/></td>
                                                <td><c:if test="${clienteFone.ramal ne null}">${clienteFone.ramal}</c:if></td>
                                                <td>
                                                    <c:if test="${clienteFone.indicadorPrincipal eq false}">N�o</c:if>
                                                    <c:if test="${clienteFone.indicadorPrincipal eq true}">Sim</c:if>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <tr><td colspan="5" class="text-center">
                                            N�o h� telefones cadastrados para o cliente
                                        </td></tr>
                                    </c:otherwise>
                                </c:choose>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <hr/>

                    <jsp:include page="formulario.jsp"/>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">
                        <i class="fa fa-times fa-fw"></i>Cancelar
                    </button>
                    <button data-style="expand-left" name="modalResponderFormulario_pesquisar" type="submit"
                            class="btn ladda-button btn-success btn-sm">
                        <i class="fa fa-check fa-fw"></i>
                        <span class="ladda-label">Responder</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

