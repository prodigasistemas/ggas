<%--
  ~ Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
  ~
  ~ Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
  ~
  ~ Este programa é um software livre; você pode redistribuí-lo e/ou
  ~ modificá-lo sob os termos de Licença Pública Geral GNU, conforme
  ~ publicada pela Free Software Foundation; versão 2 da Licença.
  ~
  ~ O GGAS é distribuído na expectativa de ser útil,
  ~ mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
  ~ COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
  ~ Consulte a Licença Pública Geral GNU para obter mais detalhes.
  ~
  ~ Você deve ter recebido uma cópia da Licença Pública Geral GNU
  ~ junto com este programa; se não, escreva para Free Software
  ~ Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  ~
  ~
  ~ Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
  ~
  ~ This file is part of GGAS, a commercial management system for Gas Distribution Services
  ~
  ~ GGAS is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; version 2 of the License.
  ~
  ~ GGAS is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
  --%>

<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 31/10/18
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<h6 class="text-center my-4"><u>Questionário: ${questionario.nomeQuestionario}</u></h6>
<div class="row">

    <c:forEach var="resposta" items="${respostas}" varStatus="status">
        <div class="col-sm-12">
            <label id="pergunta${status.index}">
                <b>
                    <c:out value="${status.index+1} - "/>
                    <c:out value="${resposta.pergunta.nomePergunta}"/>
                </b>
            </label>
        </div>
        <div class="col-sm-12">

            <c:if test="${resposta.pergunta.objetiva}">
                <c:choose>
                    <c:when test="${resposta.pergunta.valoresPersonalizados}">
                        <span>R.: <c:out value="${resposta.descricaoResposta}"/></span>
                    </c:when>
                    <c:otherwise>
                        <span>R.: <c:out value="${resposta.notaResposta}"/></span>
                        <br/>
                        <small><i>OBS: Valores de <c:out value="${resposta.pergunta.notaMinima }" /> a <c:out value="${resposta.pergunta.notaMaxima }" /></i></small>
                    </c:otherwise>
                </c:choose>
            </c:if>

            <c:if test="${not resposta.pergunta.objetiva}">
                <span>R.: <c:out value="${resposta.descricaoResposta }" /></span>
            </c:if>

        </div>
    </c:forEach>
</div>
