<!--
 Copyright (C) <2011> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 -->
 
 
<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas" %>



<h1 class="tituloInterno">Responder Question�rio</h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em<span class="destaqueOrientacaoInicial"> Encerrar</span></p>

<script type="text/javascript">

function alterarValor(idCampoHidden,idCampoRadio){
	
	var valorCampoRadio = document.getElementById(idCampoRadio).value;
	
	document.getElementById(idCampoHidden).value = valorCampoRadio;	
}

function encerrar(){
	 submeter('formResponderQuestionario','responderQuestionario');
}

function tentativaContato(){
	 submeter('formResponderQuestionario','registrarTentativaContato');
}

function voltar(){
	
	if(document.forms['formResponderQuestionario'].usuarioResponsavel != undefined){
		  document.getElementById("usuarioResponsavel").value=''; 
		}
	document.forms[0].fluxoVoltar.value = "fluxoVoltar";
	submeter('formResponderQuestionario','pesquisarChamado');
}

function exibirDetalhamentoChamado(){	
	var chaveChamadoOrigem = $("#chaveChamadoOrigem").val();;
	
	if(chaveChamadoOrigem != ''){
		submeter('formResponderQuestionario','exibirDetalhamentoChamadoPesquisaSatisfacao','_blank');		
	}else{
		submeter('formResponderQuestionario','exibirDetalhamentoChamadoPesquisaSatisfacao');
	}
	
}

function exibirDetalhamentoServicoAutorizacao(){
	var chaveServicoAutorizacao = $("#chaveServicoAutorizacao").val();
	
	if(chaveServicoAutorizacao != ''){
		submeter('formResponderQuestionario','exibirDetalhamentoServicoAutorizacaoPesquisaSatisfacao','_blank');		
	}else{
		submeter('formResponderQuestionario','exibirDetalhamentoServicoAutorizacaoPesquisaSatisfacao');
	}
}

</script>
 
 <form:form name="formResponderQuestionario"  id="formResponderQuestionario" method="post" modelAttribute="Chamado">
 	<input type="hidden" name="fluxoVoltar" id="fluxoVoltar" >
  	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${chamado.chavePrimaria}">
	<input type="hidden" name="chaveChamadoOrigem" id="chaveChamadoOrigem" value="${chamadoOrigem.chavePrimaria}">
	<input type="hidden" name="chaveServicoAutorizacao" id="chaveServicoAutorizacao" value="${chaveServicoAutorizacao}">

   <fieldset class="conteinerPesquisarIncluirAlterar">
   
   
   <fieldset id="pesquisarQuestionario" class="coluna" disabled="disabled">
   <label id="labelNomeCliente" class="rotuloNomeCliente" for="nomeCliente" >Nome Cliente:</label>
   <input type="text" id="nomeCliente" name="nomeCliente" class="campoTexto" value="${chamado.cliente.nome}">
   <label id="labelCpfCnpj" class="rotuloCPFCNPJCliente" for="rotuloCPFCNPJCliente" >CPF / CNPJ:</label>
   <input type="text" id="cpfCnpjCliente" name="cpfCnpjCliente" class="campoTexto" value="<c:if test='${ not empty chamado.cliente.cnpj}'>${chamado.cliente.cnpjFormatado}</c:if><c:if test='${not empty chamado.cliente.cpf}'>${chamado.cliente.cpfFormatado}</c:if>">
   
   <label id="labelPontoConsumo" class="rotulo" for="cpfCnpjCliente" >Ponto Consumo:</label>
   <input type="text" id="pontoConsumo" name="pontoConsumo" class="campoTexto" value="${chamado.pontoConsumo.descricao} ">

   </fieldset>
   
	<fieldset id="pesquisarQuestionario" class="coluna colunaEditada" disabled="disabled">
   		<label id="labelProtocolo" class="rotuloProtocoloQuestionario" for="Protocolo" >Protocolo:</label>
   		<input type="text" id="Protocolo" name="Protocolo" class="campoTexto" value="${chamadoOrigem.protocolo.numeroProtocolo}">
   		<label id="labelTipoServico" class="rotuloTipoServicoQuestionario" for="TipoServico" >Tipo de Servi�o:</label>  		
   		<input type="text" id="tipoServico" name="tipoServico" class="campoTexto" value="${servicoAutorizacao.servicoTipo.descricao}">
   		<label id="labeltipoAssunto" class="rotuloAssuntoQuestionario" for="tipoAssunto" >Assunto do Chamado:</label>
   		<input type="text" id="tipoAssunto" name="tipoAssunto" class="campoTexto" value="${chamado.chamadoAssunto.descricao}">
   		<label id="labelDataEncerramento" class="rotulo" for="dataEncerramento" >Data de Encerramento:</label>
   		<input type="text" id="dataEncerramento" name="dataEncerramento" class="campoTexto" value="${dataEncerramento}">
	</fieldset> 

	<fieldset id="pesquisarQuestionario" class="colunaEditada" disabled="disabled"> 
   		<label id="labelNome" class="rotuloNomeSolicitanteQuestionario" for="nome" >Nome do Solicitante:</label>
   		<input type="text" id="nome" name="nome" class="campoTextoNome" value="${chamadoOrigem.protocolo.numeroProtocolo}">
   		<label id="labelEmail" class="rotuloEmailQuestionario" for="email" >E-Mail:</label>
   		<input type="text" id="email" name="email" class="campoTextoNome" value="${chamado.questionario.tipoServico.descricao}">
   		<label id="labelTelefone" class="rotuloTelefoneQuestionario" for="telefone" >Telefone:</label>
   		<input type="text" id="telefone" name="telefone" class="campoTexto" value="${dataEncerramento}">
	</fieldset> 
 
 	<fieldset class="conteinerBloco">
 		<legend>Telefones do Cliente</legend>
 		
 		<display:table class="dataTableGGAS" name="chamado.cliente.fones" id="clienteFone" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
 			<display:column sortable="false" title="Tipo" style="text-align: center;">
 	            	<c:out value="${clienteFone.tipoFone.descricao}"/>
	        </display:column>
	        <display:column  title="DDD" style="text-align: center;">
	            	<c:out value="${clienteFone.codigoDDD} "/>
	        </display:column>
	        <display:column title="N�mero" style="text-align: center;">
	            	<c:out value="${clienteFone.numero} "/>
	        </display:column>
	        <display:column  title="Ramal" style="text-align: center;">	            	
	            	<c:if test="${clienteFone.ramal ne null}">${clienteFone.ramal}</c:if>
	        </display:column>		
	        <display:column  title="Principal" style="text-align: center;">
	         		<c:if test="${clienteFone.indicadorPrincipal eq false}">N�o</c:if>
	         		<c:if test="${clienteFone.indicadorPrincipal eq true}">Sim</c:if>
	        </display:column>
 		</display:table>
 		 	
 	</fieldset>
 
 	<fieldset class="pesquisarChamadoAssunto" style="float: left; width: 97%"  >
 		<legend>Question�rio</legend> 
 		<h1>Question�rio: Pesquisa de Satisfa��o</h1>
 		 <br/>
 		<c:set var="index1" value="0" />
 		<c:forEach var="pergunta" items="${listaPerguntas}">
			<label id="LabelPergunta${index1}" class="rotulo"> <c:out value="${index1+1} "/>&ensp;-&ensp; <c:out value="${pergunta.nomePergunta}"/> </label><br class="quebraLinha"/>
			<c:if test="${pergunta.objetiva eq true}">
				<c:forEach begin="${pergunta.notaMinima}" end="${pergunta.notaMaxima}" var="x">
					<input class="campoRadio" type="radio"  name="resposta${index1}" id="resposta${index1}"  <c:if test="${pergunta.notaPergunta eq x}">checked</c:if> value="${x}" onclick="alterarValor(notaPergunta${index1}.id,this.name);" >
					<label class="rotuloRadio" id="rotuloRadio" for="resposta${index1}"> <c:out value="${x}"/></label>
					
				</c:forEach>
				<input type="hidden" name="notaPergunta" id="notaPergunta">
			</c:if>
			<c:if test="${pergunta.objetiva eq false}">
				<input type="text" id="respostaDiscursiva${index1}" onkeypress=" return formatarCampoPergunta(event)" name="respostaDiscursiva${index1}" class="campoTexto" maxlength="250" size="150" value="${pergunta.descricaoResposta}"> <br/>
			</c:if>
 		 <c:set var="index1" value="${index1+1}" />
 		</c:forEach>
 		
 	</fieldset> 
</fieldset>
<fieldset  class="conteinerBotoesEsq">
	<input name="button" class="bottonRightCol" value="Voltar" type="button" style="float: left; margin-top: -10px; " onClick="voltar();">
</fieldset>

 <fieldset  class="conteinerBotoesPesquisarDir">
  <vacess:vacess param="contactarCliente">
 	<input name="buttonDetalharChamado" id="buttonDetalharChamado" value="Detalhar Chamado" class="bottonRightCol" onclick="exibirDetalhamentoChamado()" type="button">	
 	<input name="buttonDetalharAutorizacao" id="buttonDetalharAutorizacao" value="Detalhar Autoriza��o de Servi�o" class="bottonRightCol" onclick="exibirDetalhamentoServicoAutorizacao()" type="button">
  	<input name="Button" class="bottonRightCol" id="botaoTentativaContato" value="Reg. Tentativa de Contato" type="button" onclick="tentativaContato()">
  	<input name="Button" class="bottonRightCol" id="botaoEncerra" value="Encerrar" type="button" onclick="encerrar()">
  	</vacess:vacess>
 </fieldset>
 
 </form:form>