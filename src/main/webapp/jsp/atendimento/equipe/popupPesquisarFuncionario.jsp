<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@page import="br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente"%>
<%@page import="br.com.ggas.atendimento.equipe.dominio.Equipe"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<h1 class="tituloInterno">Pesquisar Funcion�rio</h1>
<p class="orientacaoInicialPopup">Para pesquisar um Funcion�rio, informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span></p>

<script language="javascript">

	$(document).ready(function(){
	
		//Coloca o cursor no primeiro campo de pesquisa
		$("#nomeFuncionario").focus();
	
		/*Executa a pesquisa quando a tecla ENTER � pressionada, 
		caso o cursor esteja em um dos campos do formul�rio*/
		$(':text').keypress(function(event) {
			if (event.keyCode == '13') {
				pesquisarFuncionario();
			}
	   	});
	
	});
	
	function adicionarComponente(chave){
			
		window.opener.selecionarFuncionario(chave);
		window.close();
	}

	
	function limparDados(){
		document.getElementById('nome').value = '';
		document.getElementById('matricula').value = '';
	}
	
	function pesquisarFuncionario() {
		$("#botaoPesquisar").attr('disabled','disabled');
		submeter('pesquisaFuncionarioForm', 'pesquisarFuncionario');
	}
	
	function init() {
		<c:if test="${funcionarios ne null}">
			$.scrollTo($('#funcionario'),800);
		</c:if>
	}
	addLoadEvent(init);

</script>

<form:form method="post" action="pesquisarFuncionario" id="pesquisaFuncionarioForm" name="pesquisaFuncionarioForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	
	<fieldset id="pesquisarFuncionarioPopup">
		<label class="rotulo" id="rotuloNome" for="nome">Nome:</label>
		<input class="campoTexto" type="text" id="nome" name="nome" maxlength="30" size="30" onkeyup="letraMaiuscula(this);" value="${funcionario.nome}"><br />
		
		<label class="rotulo" id="rotuloMatricula" for="matricula">Matr�cula:</label>
		<input class="campoTexto" type="text" id="matricula" name="matricula" maxlength="10" size="10" value="${funcionario.matricula}" onkeyup="letraMaiuscula(this);" onkeypress="return formatarCampoAlfaNumericoSemCaracteresEspeciais(event)" value="${funcionario.matricula}"><br />
				
	</fieldset>
	
	<fieldset class="conteinerBotoesPopup"> 
		<input class="bottonRightCol" value="Cancelar" type="button" onclick="window.close();">
	    <input class="bottonRightCol" value="Limpar" type="button" onclick="limparDados();">
	    
	    <input id="botaoPesquisar" class="bottonRightCol2 botaoGrande1" value="Pesquisar" type="button" onclick="pesquisarFuncionario()">
	    
	 </fieldset>
	
	<c:if test="${funcionarios ne null}">
	<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTablePopup" name="funcionarios" sort="list" id="funcionario" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
			<display:column title="Nome" sortable="true" sortProperty="nome">  
				<a href='javascript:adicionarComponente(<c:out value='${funcionario.chavePrimaria}'/>);'>
					<c:out value='${funcionario.nome}'/>
				</a>
			</display:column>
			<display:column title="Matr�cula" sortable="true" sortProperty="nome">  
				<a href='javascript:adicionarComponente(<c:out value='${funcionario.chavePrimaria}'/>);'>
					<c:out value='${funcionario.matricula}'/>
				</a>
			</display:column>			
		</display:table>   
	</c:if>

</form:form>