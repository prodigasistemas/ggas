<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1 class="tituloInterno">Alterar Equipe</h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script>
$(document).ready(function(){
	
	// Dialog			
	$("#pesquisaFuncionarioPopup").dialog({
		autoOpen: false,
		draggable: false,
		width: 580,
		modal: true,
		minHeight: 110,
		position: "center",
		resizable: false
	});
	
});

function exibirPopup(popup) {
	exibirJDialog("#"+popup);	
}

function limparFormulario(){		
	var form = document.equipeForm;
	limparFormularios(form);	
	document.forms['equipeForm'].habilitado[0].checked = true;
		
}
	
function voltar() {	
	submeter("equipeForm", "exibirPesquisaEquipe");
}

function alertInativacao() {
	var habilitado = document.getElementById("habilitado").checked;
	var habilitadoOriginal = document.getElementById("habilitadoOriginal").value;
		
		if(habilitado == false && habilitadoOriginal == "true"){
			if(confirm('Deseja inativar a equipe?')){
				return true;  
			}else{
				return false;
			} 	
		}
	}

function selecionarFuncionario(idFuncionario){
	var noCache = "noCache=" + new Date().getTime();
	$("#gridComponentesEquipe").load("adicionarComponente?idFuncionario="+idFuncionario+"&"+noCache);
	}

	function exibirPopupPesquisaFuncionario() {
	popup = window.open('exibirPopupPesquisaFuncionario','popup','height=750,width=800,toolbar=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes ,modal=yes');
	}


</script>

<form:form method="post" action="alterarEquipe" id="equipeForm" name="equipeForm">

	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${equipe.chavePrimaria}">
	<input name="indexListaComponentesEquipe" type="hidden" id="indexListaComponentesEquipe">
	<input name="idFuncionario" type="hidden" id="idFuncionario" >

<fieldset class="conteinerPesquisarIncluirAlterar">
	
	<fieldset id="materialCol1" class="coluna">		
			<label class="rotulo  campoObrigatorio" id="rotuloLocalidade" for="nome"><span class="campoObrigatorioSimbolo">* </span>Descri��o da Equipe:</label>
			<input class="campoTexto" type="text" name="nome" id="nome" value="${equipe.nome}" maxlength="50" size="50" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"/><br />		
			<label class="rotulo rotulo2Linhas campoObrigatorio" id="rotuloLocalidade" for="quantidadeHorasDia"><span class="campoObrigatorioSimbolo">* </span>Carga de Trabalho Di�ria:</label>
			<input class="campoTexto" type="text" name="quantidadeHorasDia" id="quantidadeHorasDia" maxlength="10" size="10" value="${equipe.quantidadeHorasDia}" onkeypress="return formatarCampoInteiro(event,8);"/><br />
			
			<label class="rotulo campoObrigatorio" for="unidadeOrganizacional"><span class="campoObrigatorioSimbolo">* </span>Unidade Organizacional:</label>			<select id="unidadeOrganizacional" class="campoSelect" name="unidadeOrganizacional">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadeOrganizacional}" var="unidade">
				<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${equipe.unidadeOrganizacional.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidade.descricao}"/>
				</option>
				</c:forEach>
			</select><br />
	</fieldset>
	
	<fieldset id="localidadeCol2" class="colunaFinal">						
			<label class="rotulo" id="rotuloPlaca" for="descricaoPlacaVeiculo" >Placa do Ve�culo:</label>
			<input class="campoTexto" type="text" name="descricaoPlacaVeiculo" id="descricaoPlacaVeiculo" maxlength="7" size="10" value="${equipe.descricaoPlacaVeiculo}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"/>
			
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${equipe.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${equipe.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input type="hidden" id="habilitadoOriginal" value="${equipe.habilitado}" />				
	</fieldset>
	
	<div id="pesquisaFuncionarioPopup" title="GGAS - Adicionar Componente">

		<jsp:include page="popPesquisar.jsp"></jsp:include>
		
	</div>
	
	<hr class="linhaSeparadora1" />
	
	<fieldset id="conteinerComponentesEquipe" class="conteinerBloco">
		<fieldset class="conteinerBotoesDirFixo">
			<input name="button" class="bottonRightCol botaoAdicionar" value="Adicionar Componente" type="button" onclick="exibirPopupPesquisaFuncionario();">		
		</fieldset>
	</fieldset><br/>
		
	<div id="gridComponentesEquipe">
		<jsp:include page="/jsp/atendimento/equipe/gridComponentesEquipe.jsp"></jsp:include>		
	</div>
		
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="voltar();">
    <input name="Button" class="bottonRightCol bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="alterarEquipe">
    	<input id="buttonSalvar" name="button" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Salvar" type="submit" onclick="return alertInativacao()">
    </vacess:vacess>
</fieldset>

</form:form> 