<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

function removerComponente(index){
	var noCache = "noCache=" + new Date().getTime();
	$("#gridComponentesEquipe").load("removerComponente?indexListaComponentesEquipe="+index+"&"+noCache);

}

function atualizarResponsavel(index){
	var confirmacao = confirm('Deseja tornar este componente o respons�vel pela equipe?');
	
	var noCache = "noCache=" + new Date().getTime();
	
	if(confirmacao == true){
		$("#gridComponentesEquipe").load("atualizarResponsavel?indexListaComponentesEquipe="+index+"&"+noCache);

	}	
}

</script>

<fieldset class="conteinerBloco">
<legend class="conteinerBlocoTitulo"><span class="campoObrigatorioSimbolo">* </span>Componentes da Equipe</legend>
<c:set var="indexListaComponentesEquipe" value="0" />
	<display:table class="dataTableGGAS" name="sessionScope.listaComponentesEquipe" sort="list" id="equipeComponente" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	       
			<display:column title="Matr�cula">        
            	<c:out value="${equipeComponente.funcionario.matricula}"/>
	        </display:column>      
	        <display:column title="Nome">        
            	<c:out value="${equipeComponente.funcionario.nome}"/>
	        </display:column>
	         <display:column title="Respons�vel" style="width: 80px">
				<c:choose>
			  		<c:when test="${equipeComponente.indicadorResponsavel == true}">
			  			Sim <a href="javascript:atualizarResponsavel('<c:out value="${indexListaComponentesEquipe}"/>');"></a>
			  		</c:when>
			  		<c:otherwise>
			  			N�o <a href="javascript:atualizarResponsavel('<c:out value="${indexListaComponentesEquipe}"/>');"><img alt="Tornar Responsavel" title="Tornar Responsavel" src="<c:url value="/imagens/user_business_boss.png"/>" border="0" /></a>
			  		</c:otherwise>
			  	</c:choose>
			</display:column>
			
			<display:column style="text-align: center; width: 25px" class="colunaSemTitulo"> 
				<a onclick="javascript:removerComponente(<c:out value="${indexListaComponentesEquipe}"/>);">
					<img title="Excluir Componente " alt="Excluir Componente"  src="<c:url value="/imagens/deletar_x.png"/>">
				</a>			  		
			</display:column>
	<c:set var="indexListaComponentesEquipe" value="${indexListaComponentesEquipe + 1}" />
	</display:table>
</fieldset>