<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>


<link rel="stylesheet" type="text/css" href="<c:url value="/css/displaytag.css"/>" title="Normal Screen and Print Layout" />	

<h1 class="tituloInterno">Detalhar Equipe</h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<script>		
		function voltar(){	
			submeter("equipeForm", "pesquisarEquipe");
		}
		
		function alterar(){			
			submeter('equipeForm', 'exibirAlteracaoEquipe');
		}
		
		$(function() {
		    var max = 0;
		    $('#equipeCol2 > label').each(function(){
		        if ($(this).width() > max)
		            max = $(this).width();    
		    });
		    $('#equipeCol2 > label').width(max);
		});
</script>

<form:form method="post" action="equipe/alterarEquipe" id="equipeForm" name="equipeForm">
		
	<input type="hidden" name="chavePrimaria" id="chavePrimaria" value="${equipe.chavePrimaria}">
	<input type="hidden" name="chavesPrimarias" id="chavesPrimarias" value="${equipe.chavePrimaria}">
	<input type="hidden" name="habilitado" id="habilitado" value="${ material.habilitado }">	
	
	<fieldset id="detalhamentoEquipe" class="detalhamento">
		<fieldset id="equipeCol2" class="colunaFunc">
			
			<label class="rotulo">Descri��o da Equipe:</label>
			<span class="itemDetalhamento"><c:out value="${equipe.nome}"/></span><br />
			<label class="rotulo">Carga do Trabalho Di�ria:</label>
			<span class="itemDetalhamento"><c:out value="${equipe.quantidadeHorasDia}"/></span><br />
			<label class="rotulo">Unidade organizacional:</label>		
			<span class="itemDetalhamento"><c:out value="${equipe.unidadeOrganizacional.descricao}"/></span><br />		
		</fieldset>		
		
		<fieldset id="equipeCol2" class="colunaFinal">
			<label class="rotulo">Placa do Ve�culo:</label>
			<span class="itemDetalhamento"><c:out value="${equipe.descricaoPlacaVeiculo}"/></span>			
			
			<label class="rotulo">Indicador de Uso:</label>
			<span class="itemDetalhamento"> 
				<c:choose>
					<c:when test="${equipe.habilitado == 'true'}">Ativo</c:when>
					<c:otherwise>Inativo</c:otherwise>
				</c:choose>
			</span>						
		</fieldset>
						
		
		<hr class="linhaSeparadora1" />
		
		<fieldset class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Componentes da Equipe</legend>
			<display:table class="dataTableGGAS" name="listaComponentesEquipe" sort="list" id="equipeComponente" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
		        
				<display:column title="Matr�cula">
		            <c:out value="${equipeComponente.funcionario.matricula}"/>
		        </display:column>      
		        <display:column title="Nome">        
		           <c:out value="${equipeComponente.funcionario.nome}"/>
		        </display:column>
		        <display:column title="Respons�vel" style="width: 100px">
			     	<c:choose>
						<c:when test="${equipeComponente.indicadorResponsavel == true}">
							Sim
						</c:when>
						<c:otherwise>
							N�o
						</c:otherwise>				
					</c:choose>
				</display:column>
			</display:table>
		</fieldset>
	</fieldset>
	
	<fieldset class="conteinerBotoes"> 
	    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">
		    <vacess:vacess param="exibirAlteracaoEquipe">    
	    		<input name="button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();">
		    </vacess:vacess>
	</fieldset>
</form:form>
