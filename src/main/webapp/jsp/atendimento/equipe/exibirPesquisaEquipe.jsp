<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<h1 class="tituloInterno">Pesquisar Equipe</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<script>
	
		function removerEquipe(){		
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('equipeForm', 'removerEquipe');
				}
		    }
		}
	
		function limparFormulario(){
			document.equipeForm.nome.value = "";
			document.getElementById('unidadeOrganizacional').value = "-1";
			document.forms['equipeForm'].habilitado[0].checked = true;			
		}
		
		function alterarEquipe() {		
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms['equipeForm'].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter('equipeForm', 'exibirAlteracaoEquipe');
		    }
		}	
	
		function incluir() {		
			location.href = '<c:url value="/exibirInclusaoEquipe"/>';
		}
		
		function detalharEquipe(chave){
			document.forms['equipeForm'].chavePrimaria.value = chave;
			submeter("equipeForm", "exibirDetalhamentoEquipe");
		}
		
		$(function() {
		    var max = 0;
		    $('#pesquisaEquipeCol2 > label.rotulo').each(function(){
		        if ($(this).width() > max)
		            max = $(this).width();    
		    });
		    $('#pesquisaEquipeCol2 > label.rotulo').width(max);
		});
</script>

<form:form method="post" action="pesquisarEquipe" id="equipeForm" name="equipeForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias">	
	
	<fieldset class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisaEquipeCol2" class="coluna">
		
			<label class="rotulo" for="nome">Descri��o da Equipe:</label>
			<input class="campoTexto" type="text" name="nome" id="nome" maxlength="40" size="30" value="${equipe.nome}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="letraMaiuscula(this);"/><br />
			
			<label class="rotulo" for="unidadeOrganizacional">Unidade Organizacional:</label>
			<select id="unidadeOrganizacional" class="campoSelect" name="unidadeOrganizacional">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaUnidadeOrganizacional}" var="unidade">
				<option value="<c:out value="${unidade.chavePrimaria}"/>" <c:if test="${equipe.unidadeOrganizacional.chavePrimaria == unidade.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${unidade.descricao}"/>
				</option>
				</c:forEach>
			</select>					

		</fieldset>
				
		<fieldset class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${habilitado eq ''}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDir">
	    	<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">		
			<input name="Button" class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaEquipe ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaEquipe" sort="list" id="equipe" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="#">
	        <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input type="checkbox" name="chavesPrimarias" value="${equipe.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${equipe.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
			<display:column sortable="true" sortProperty="nome" title="Descri��o">        
	            <a href="javascript:detalharEquipe(<c:out value='${equipe.chavePrimaria}'/>);">
	            	<c:out value="${equipe.nome}"/>
	            </a>
	        </display:column>      
	        <display:column sortable="true" sortProperty="quantidadeHorasDia" title="Carga de Trabalho" >        
	            <a href="javascript:detalharEquipe(<c:out value='${equipe.chavePrimaria}'/>);">
	            	<c:out value="${equipe.quantidadeHorasDia}"/>
	            </a>
	        </display:column>	        
		   <display:column sortable="true" sortProperty="unidadeOrganizacional.descricao" title="Unidade Organizacional">
		        <a href="javascript:detalharEquipe(<c:out value='${equipe.chavePrimaria}'/>);">
	            	<c:out value="${equipe.unidadeOrganizacional.descricao}"/>
	            </a>
		   </display:column>
		</display:table>	
	</c:if>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${listaEquipe ne null}">
			<input id="buttonAlterar" name="Alterar" value="Alterar" class="bottonRightCol2" onclick="alterarEquipe()" type="button">
			<vacess:vacess param="removerEquipe">
				<input id="buttonRemover" name="Remover" value="Remover" class="bottonRightCol bottonLeftColUltimo" onclick="removerEquipe();" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInclusaoEquipe">
			<input id="buttonIncluir" name="button" value="Incluir" class="bottonRightCol2 botaoGrande1 botaoIncluir" onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>

</form:form>