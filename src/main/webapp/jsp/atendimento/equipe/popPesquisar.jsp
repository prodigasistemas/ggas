<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

function pesquisarFuncionario(){		
	var nomeFuncionario = document.getElementById("nomeFuncionario").value;
	var matriculaFuncionario = document.getElementById("matriculaFuncionario").value;
			
	$("#pesquisaFuncionarioPopup").load("pesquisarFuncionario?nome="+nomeFuncionario+"&matricula="+matriculaFuncionario);	
}

function adicionarComponente(chaveFuncionario){
	var noCache = "noCache=" + new Date().getTime();
	
	$("#gridComponentesEquipe").load("adicionarComponente?idFuncionario="+chaveFuncionario+"&"+noCache);
	$("#pesquisaFuncionarioPopup").dialog("close");
	
}

</script>

<fieldset class="conteinerPesquisarIncluirAlterar">
	<fieldset id="clienteCol1" >
		<label class="rotulo" for="nomeFuncionario">Nome:</label>
		<input class="campoTexto" type="text" name="nomeFuncionario" id="nomeFuncionario" maxlength="30" size="30" onkeyup="letraMaiuscula(this);" value="${funcionario.nome}"/><br />
		 
		<label class="rotulo" for="matriculaFuncionario">Matr�cula:</label>
		<input class="campoTexto" type="text" name="matriculaFuncionario" id="matriculaFuncionario" maxlength="10" size="10" value="${funcionario.matricula}" onkeypress="return formatarCampoInteiro(event,8);"/><br />
				
		<hr class="linhaSeparadoraPopup" />
		<br/>
		
		<fieldset class="conteinerBloco">		
			<fieldset class="conteinerBotoesDirFixo">
				<input name="Button" id="botaoPesquisar" class="bottonRightCol botaoAdicionar" value="Pesquisar" type="button" onclick="pesquisarFuncionario();">			
			</fieldset>				
		</fieldset><br/>
		
	</fieldset>
</fieldset><br/><br/>

	<fieldset class="conteinerPesquisarIncluirAlterar">
		<c:if test="${funcionarios ne null}">
		<display:table class="dataTableGGAS dataTableCabecalho dataTableDialog" name="funcionarios" sort="list" id="funcionario" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" requestURI="#">
	       	<display:column title="Matr�cula"> 
	       		<a href="javascript:adicionarComponente(<c:out value='${funcionario.chavePrimaria}'/>);">
            		<c:out value="${funcionario.matricula}"/>
            	</a>
	        </display:column>      
	        <display:column title="Nome">        
	        	<a href="javascript:adicionarComponente(<c:out value='${funcionario.chavePrimaria}'/>);">
	            	<c:out value="${funcionario.nome}"/>
	            </a>
	        </display:column>
		</display:table>
		</c:if>
	
</fieldset>
	


		