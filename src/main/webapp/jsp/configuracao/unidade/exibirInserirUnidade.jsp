<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.filestyle.mini.js"></script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<h1 class="tituloInterno">Incluir Unidade<a href="<help:help>/cadastrodasunidadescontedoinclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<script language="javascript">

    $(document).ready(function(){

         var max = 0;

          $('.rotulo').each(function(){
                  if ($(this).width() > max)
                     max = $(this).width();   
              });
          $('.rotulo').width(max);
         
    });
    
   function limpar(){
	   $('#descricao').val("");
	   	$('#descricaoAbreviada').val("");
	   	$('#classeUnidade').val("-1");
	   document.unidadeForm.indicadorPadrao[0].checked = true;
   }
    
    function salvar(){

        var url = 'inserirUnidade';
        
        submeter('unidadeForm', url);

    }

    function cancelar(){    
    	location.href='exibirPesquisaUnidade';
    }
    
</script>

<form method="post" id="unidadeForm" name="unidadeForm" enctype="multipart/form-data" action="inserirUnidade">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="habilitado" type="hidden" id="habilitado" value="${true}" >

<fieldset class="conteinerPesquisarIncluirAlterar">
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
        
        	<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
       		<input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="250" size="30" value="${unidade.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
        
        	<label class="rotulo">Descri��o Abreviada:</label>
        
       		<input class="campoTexto campoHorizontal" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${unidade.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"/>
             
              
              
         <p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios</p>	
            
        </fieldset>
        <fieldset id="pesquisarTabelaAuxiliarCol2" class="colunaFinal">

			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Unidade Classe:</label>
	        <select name="classeUnidade" class="campoSelect" id="classeUnidade">
	            <option value="-1">Selecione</option>
	            <c:forEach items="${listaUnidadeClasse}" var="unidadeClasse">
	                <option value="<c:out value="${unidadeClasse.chavePrimaria}"/>" <c:if test="${unidade.classeUnidade.chavePrimaria == unidadeClasse.chavePrimaria}">selected="selected"</c:if>>
	                    <c:out value="${unidadeClasse.descricao}"/>
	                </option>       
	            </c:forEach>    
	        </select>

			<label class="rotulo" id="rotuloIndicadorPadrao" for="indicadorPadrao">Indicador Padr�o:</label>
            <input class="campoRadio" type="radio" name="indicadorPadrao" id="indicadorPadrao" value="true" checked="checked" <c:if test="${unidade.indicadorPadrao eq 'true'}">checked</c:if>>
            <label class="rotuloRadio" for="indicadorPadrao">Sim</label>
            <input class="campoRadio" type="radio" name="indicadorPadrao" id="indicadorPadrao" value="false" <c:if test="${unidade.indicadorPadrao eq 'false'}">checked</c:if>>
            <label class="rotuloRadio" for="indicadorPadrao">N�o</label>
			
       </fieldset>
    </fieldset>
        
	
	<fieldset class="conteinerBotoes">
	    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="javascript:cancelar();">
	    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limparFormulario" value="Limpar" type="button" onclick="limpar();">
	    <input name="button" class="bottonRightCol2 botaoGrande1 botaoSalvar" value="Salvar" type="button" id="botaoSalvar" onclick="javascript:salvar();">
	</fieldset>
</form>
