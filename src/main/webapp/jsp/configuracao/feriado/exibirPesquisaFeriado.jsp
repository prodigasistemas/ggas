<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
 

<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/engine.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/DataParadaProgramadaPrototype.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.datepick-pt-BR.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/jquery.maxlength.js"></script>


<h1 class="tituloInterno">Feriado<a href="<help:help></help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>



<form method="post" id="feriadoForm" name="feriadoForm">
    
   
    
<script lang="javascript">

    $(document).ready(function(){
        $("#codigoCnae").inputmask("a9999-9/99",{placeholder:"_"});

    // Datepicker
    $(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

    var max = 0;

      $('.rotulo').each(function(){
              if ($(this).width() > max)
                 max = $(this).width();   
          });
      $('.rotulo').width(max);
        
    
    
        var datasFeriado = new Array();
    
        var date;
        <c:set var="i" value="0" />
        
        <c:forEach items="${listaTabelaAuxiliar}" var="feriado">
        
	         date = new Date('${feriado.dataFormatada}');
	         date.comentario = '${feriado.descricao}';
	         date.indicadorSolicitante = '${feriado.indicadorTipo}';
	        
	         
	         datasFeriado[${i}] = date;
	         <c:set var="i" value="${i+1}" />
         </c:forEach>
    
         <c:if test="${not empty feriadoVO.datasFeriado}">
	         var datasProgramadasForm = '${feriadoVO.datasFeriado}';
	         var arrDatasProgramadasForm = datasProgramadasForm.split(";");
	         var arrData;
    
         </c:if>

    
        $(function() {
            
            $('#dataCalendario').datepick({
                multiSelect: 999, numberOfMonths: [2,6], 
                multiSeparator: ';',
                dateFormat: 'dd/mm/yy',
                defaultDate: new Date ('<c:out value="${anoAtual}"/>', 00, 01),
                changeMonth: false,
                changeYear: true,
                yearRange: '<c:out value="${intervaloAnosData}"/>',
                hideIfNoPrevNext: true,
            
            });
    
            $("#dataCalendario").datepick('enable'); 
            
            if (datasFeriado != undefined && datasFeriado.length > 0) {
                
                $("#dataCalendario").datepick('setDate', datasFeriado); 
            }
            $('#dataCalendario').datepick('setIndicadorSolicitante', 'supridora');
            
            
        });
    });
    

    function limparFormulario(){
        
        document.getElementById('descricao').value = "";
        document.forms[0].indicadorTipo[0].checked = true;
        document.forms[0].indicadorMunicipal[0].checked = true;
        document.getElementById('idMunicipio').value = "-1";
        document.getElementById('idMunicipio').disabled = true;
    }

    function salvarDatas() {

        var datasCalendario = $('#dataCalendario').datepick('getDate');
        var datas = "";

        for (j = 0; j<datasCalendario.length; j++) {
            
            var dataAux = datasCalendario[j].format("d/m/Y");
            
            if(j == datasCalendario.length-1){
                
                datas = datas + dataAux;
            }else{
                datas = datas + dataAux + ";" ;
            }
            
        }

        document.forms[0].datasFeriado.value = datas;
        
        submeter('feriadoForm', 'incluirFeriado');
        
    }


    function habilitarDesabilitarMunicipio(){
        
        if(document.forms[0].indicadorMunicipal[0].checked == true){
            document.getElementById('idMunicipio').disabled = true;
        }else{
            document.getElementById('idMunicipio').disabled = false;
        } 
    }
    

    function replicarFeriadosFixos(){
        submeter('feriadoForm', 'inserirFeriadoReplicado');
    }

    function init() {

        habilitarDesabilitarMunicipio();
    }
    
    addLoadEvent(init);

    </script>
    
    <input name="chavePrimaria" type="hidden" id="chavePrimaria" >
    <input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
    <input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
    <input name="datasFeriado" type="hidden" id="datasFeriado"/>
    <input name="datasSelecionadas" type="hidden" id="datasSelecionadas"/>
    
    <fieldset class="conteinerPesquisarIncluirAlterar">    
            <label class="rotulo" for="indicadorAceite">Tipo de Feriado:</label>
            <img id="legendaParadaProgramadaCDL" src="<c:url value="/imagens/legendaCDL.gif"/>" alt="CDL" title="CDL" /><label id="rotuloLegendaParadaProgramadaCDL" class="rotuloRadio" for="indicadorSolicitante1">M�vel</label>
            <img id="legendaParadaProgramadaCliente" src="<c:url value="/imagens/legendaCliente.gif"/>" alt="Cliente" title="Cliente" /><label id="rotuloLegendaParadaProgramadaCliente" class="rotuloRadio" for="indicadorSolicitante2">Fixo</label>
            
            
            <div id="dataCalendario" class="paradaProgramada" "></div>
            
            <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
            
                <label class="rotulo" id="rotuloDescricao" for="descricao"><span class="campoObrigatorioSimbolo2">* </span>Descri��o:</label>
                <input class="campoTexto campoHorizontal" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${feriadoVO.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" /><br />
                
                <label class="rotulo">Tipo Feriado:</label>
                <input class="campoRadio" type="radio" name="indicadorTipo" id="indicadorTipo" value="false"  <c:if test="${feriadoVO.indicadorTipo eq 'false'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorTipo">Fixo</label>
                <input class="campoRadio" type="radio" name="indicadorTipo" id="indicadorTipo" value="true"  <c:if test="${feriadoVO.indicadorTipo eq 'true'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorTipo">M�vel</label><br /><br />
            
            
                <label class="rotulo">Feriado Municipal:</label>
                <input class="campoRadio" type="radio" name="indicadorMunicipal" id="indicadorMunicipal" onclick="javascript:habilitarDesabilitarMunicipio();" value="false" <c:if test="${feriadoVO.indicadorMunicipal eq 'false'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorMunicipal">N�o</label>
                <input class="campoRadio" type="radio" name="indicadorMunicipal" id="indicadorMunicipal" onclick="javascript:habilitarDesabilitarMunicipio();" value="true" <c:if test="${feriadoVO.indicadorMunicipal eq 'true'}">checked</c:if>>
                <label class="rotuloRadio" for="indicadorMunicipal">Sim</label>
                
                <br /><br /><br /><br />
                <label class="rotulo">Munic�pio:</label>
                <select name="idMunicipio" class="campoSelect" id="idMunicipio" >
                    <option value="-1">Selecione</option>
                    <c:forEach items="${listaMunicipios}" var="municipio">
                        <option value="<c:out value="${municipio.chavePrimaria}"/>" <c:if test="${feriadoVO.idMunicipio == municipio.chavePrimaria}">selected="selected"</c:if>>
                            <c:out value="${municipio.descricao}"/>
                        </option>       
                    </c:forEach>    
                </select><br />
                
                <p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>campo obrigat�rio para adi��o de Feriado.</p>
            </fieldset>
            
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
        </fieldset>
        
            
        </fieldset>
    
    <fieldset class="conteinerBotoes">
          <vacess:vacess param="incluirFeriado">
                <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" id="limpar" value="Limpar" type="button" onclick="javascript:limparFormulario();">
            
                <input name="Button" class="bottonRightCol2 botaoAditar" id="replicarDatas" value="Replicar" type="button" onclick="javascript:replicarFeriadosFixos();">
                
                <input id="adicionarCampos" name="button" class="bottonRightCol2 botaoGrande1 botaoIncluir" value="Salvar" type="button" onclick="javascript:salvarDatas();">
          </vacess:vacess>  
    </fieldset>
</form>
