<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Entidade Classe
	<a	class="linkHelp"
		href="<help:help>/consultadasvalidaesdasmedieshorriasdosupervisrio.htm</help:help>"
		target="right" 
		onclick="exibirJDialog('#janelaHelp');"></a>
</h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.
Para incluir uma novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>


<script>

	$(document).ready(function(){
	    
	    var max = 0;
	
	      $('.rotulo').each(function(){
	              if ($(this).width() > max)
	                 max = $(this).width();   
	          });
	      $('.rotulo').width(max);
	});

	function limparFormulario(){
		document.forms[0].habilitado[0].checked = true;
		$("#descricao").val("");
		$("#descricaoAbreviada").val("");
		$("#habilitadoNull").val(true);
	}

	function pesquisar() {
		
		submeter("entidadeClasseForm", "pesquisarEntidadeClasse");
		
	}
	
	function detalhar(chave){
		document.forms['entidadeClasseForm'].chavePrimaria.value = chave;
		submeter("entidadeClasseForm", "exibirDetalhamentoEntidadeClasse");
	}
	
	function incluir() {		
		location.href = '<c:url value="/exibirInserirEntidadeClasse"/>';
	}
	
	function alterar() {		
		var selecao = verificarSelecaoApenasUm();
		if (selecao == true) {
			document.forms['entidadeClasseForm'].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('entidadeClasseForm', 'exibirAtualizarEntidadeClasse');
	    }
	}
	
	function remover(){		
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				document.forms[0].chavePrimaria.value = "";
				submeter('entidadeClasseForm', 'removerEntidadeClasse');
			}
	    }
	}
	
	
</script>


<form:form method="post" action="pesquisarEntidadeClasse" id="entidadeClasseForm" name="entidadeClasseForm">

	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	
	<fieldset class="conteinerPesquisarIncluirAlterar"> 
        <fieldset id="pesquisarTabelaAuxiliarCol1" class="coluna">
		<label class="rotulo" id="rotuloDescricao" for="descricao">Descri��o:</label>
		<input class="campoTexto" type="text" name="descricao" id="descricao" maxlength="30" size="30" value="${entidadeClasse.descricao}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/>
		
		<label class="rotulo" id="rotuloDescricaoAbreviada" for="descricaoAbreviada">Descri��o Abreviada:</label>
		<input class="campoTexto" type="text" name="descricaoAbreviada" id="descricaoAbreviada" maxlength="6" size="6" value="${entidadeClasse.descricaoAbreviada}" onblur="this.value = removerEspacoInicialFinal(this.value);" onkeyup="return validarCriteriosParaCampo(this, '1', '0', 'formatarCampoNome(event)');"/>
			
		</fieldset>
				
		<fieldset class="colunaFinal">
			<label class="rotulo" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq true}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq false}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked = "checked"</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label>
		</fieldset>
		
		 <fieldset class="conteinerBotoesPesquisarDirFixo">
	         <vacess:vacess param="pesquisarEntidadeClasse">
	             <input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	         </vacess:vacess>
	         <input name="Button" class="bottonRightCol2 bottonRightColUltimo" id="limpar" value="Limpar" type="button" onclick="limparFormulario();">
         </fieldset>
	</fieldset>
	
	<c:if test="${listaEntidadeClasse ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS" name="listaEntidadeClasse" sort="list" id="entidadeClasse" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarEntidadeClasse"> 
	        <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
	      		<input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${entidadeClasse.chavePrimaria}">
	     	</display:column>
	     	<display:column title="Ativo" style="width: 30px">
		     	<c:choose>
					<c:when test="${entidadeClasse.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			</display:column>
			<display:column sortable="true" sortProperty="descricao" title="Descri��o">        
	            <a href="javascript:detalhar(<c:out value='${entidadeClasse.chavePrimaria}'/>);">
	            	<c:out value="${entidadeClasse.descricao}"/>
	            </a>
	        </display:column>              

		</display:table>	
	</c:if>

	<fieldset class="conteinerBotoes">
		<c:if test="${not empty listaEntidadeClasse}">
			<vacess:vacess param="atualizarEntidadeClasse">
				<input id="botaoAlterar" name="Alterar" value="Alterar"
					class="bottonRightCol2" onclick="alterar()" type="button">
			</vacess:vacess>
			<vacess:vacess param="removerEntidadeClasse">
				<input id="botaoRemover" name="Remover" value="Remover"
					class="bottonRightCol2" onclick="remover();" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="inserirEntidadeClasse">
			<input id="botaoIncluir" name="button" value="Incluir"
				class="bottonRightCol2 botaoGrande1 botaoIncluir"
				onclick="incluir()" type="button">
		</vacess:vacess>
	</fieldset>

</form:form>