<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Acompanhamento de Cobran�a <a class="linkHelp" href="<help:help>/consultandocontrato.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.</p>

<script lang="javascript">


	
	$(document).ready(function(){

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
		
		//s� executa se a funcionalidade de migra��o do modelo do contrato for realizada
		var logErroMigracaoContrato = "<c:out value="${sessionScope.logErroMigracao}"/>";
		if(logErroMigracaoContrato != ""){
			submeter('form', 'exibirLogErroMigracaoContrato');
		}
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};			
	
	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);		
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();			
		}else{
			
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
			
		}	
	}
	
	function selecionarImovel(idSelecionado){
		
		ativarBotaoPesquisarPontoConsumo();
		ativarBotaoPesquisar();
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
              	cidadeImovel.value = "";
              	indicadorCondominio.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

	}
	
	function limparFormulario(){
		document.form.numeroContrato.value = "";
		document.form.habilitado[0].checked = true;	
		
    	limparCamposPesquisa();
    	
    	document.form.indicadorPesquisa[0].checked = false;
    	document.form.indicadorPesquisa[1].checked = false;
    	
       	pesquisarCliente(false);
		pesquisarImovel(false);
	}
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function pesquisarAvisoCorte(){
		submeter("form", "pesquisarAvisoCorte");
	}

	function limparCamposPesquisa(){		
		limparFormularioDadosCliente();		
		document.getElementById('nomeImovelTexto').value = "";
       	document.getElementById('numeroImovelTexto').value = "";
       	document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		desativarBotaoPesquisarPontoConsumo();
		desativarBotaoPesquisar();
    }
	
	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}

	function init() {
		
		<c:choose>
			<c:when test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
			</c:when>
			<c:when test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>
		habilitaBotoes()
	}	
	
	addLoadEvent(init);		
	
	
	function pesquisarPontoPonsumo(){
		submeter("form", "pesquisarPontoConsumoAcompanhamentoCobranca");
	}
	
	function carregarDadosPontosConsumo(){
		submeter("form", "pesquisarAvisoCorte");
	}
	function pesquisarAvisoCortePorImovelCliente(){
		submeter("form", "pesquisarAvisoCortePorImovelCliente");
	}
	
	function desativarBotaoPesquisarPontoConsumo(){
		document.getElementById("ButtonPesquisarPontoConsumo").disabled = true;
		
	}
	
	function desativarBotaoPesquisar(){
		document.getElementById("buttonPesquisar").disabled = true;
	}
	
	function habilitaBotoes(){
		
		var idImovel = document.getElementById("idImovel").value;
		var idCliente = document.getElementById("idCliente").value;
		
		if(idImovel == "" && idCliente == ""){
			
			desativarBotaoPesquisarPontoConsumo();
			desativarBotaoPesquisar();
		}
	}
	
	function ativarBotaoPesquisar() {
		document.getElementById("buttonPesquisar").disabled = false;
		document.getElementById("ButtonPesquisarPontoConsumo").disabled = false;
	}
	
	function ativarBotaoPesquisarPontoConsumo(){
		document.getElementById("ButtonPesquisarPontoConsumo").disabled = false;
	}
	
	</script>

<form method="post" action="pesquisarAvisoCorte" name="form" id="form"> 
	<input name="acao" type="hidden" id="acao" value="pesquisarAvisoCorte"/>
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value=""/>
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idCliente" type="hidden" id="idCliente" value="${clientePopupVO.idCliente}"/>
	
	<fieldset id="pesquisarContrato" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" name="indicadorPesquisa" <c:if test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">			
			
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${clientePopupVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCliente"/>
				<jsp:param name="nomeCliente" value="${clientePopupVO.nomeCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoCliente"/>
				<jsp:param name="documentoFormatadoCliente" value="${clientePopupVO.documentoCliente}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${clientePopupVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${clientePopupVO.enderecoCliente}"/>		
				<jsp:param name="dadosClienteObrigatorios" value="false"/>
				<jsp:param name="funcaoParametro" value="ativarBotaoPesquisar"/>
				<jsp:param name="possuiRadio" value="true"/>
			</jsp:include>		
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${clientePopupVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${imovelPopupVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${imovelPopupVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${imovelPopupVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${imovelPopupVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${imovelPopupVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${imovelPopupVO.condominio}">
				<input type="hidden" name="chavePontoConsumo" id="chavePontoConsumo" />

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" disabled="disabled" value="${imovelPopupVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovelPopupVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovelPopupVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${imovelPopupVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${imovelPopupVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${imovelPopupVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
	
		<fieldset class="conteinerBloco">
			<fieldset id="conteinerBotoesPesquisarDirPesquisarImovel" class="conteinerBotoesPesquisarDirFixo">
				<input id="ButtonPesquisarPontoConsumo" name="ButtonPesquisarPontoConsumo" type="button" class="bottonRightCol2"  value="Pontos de Consumo"  onclick="pesquisarPontoPonsumo();">
				<input id="buttonPesquisar" name="buttonPesquisar" type="button" class="bottonRightCol2"  value="Pesquisar"  onclick="pesquisarAvisoCortePorImovelCliente();">
				
				<input id="ButtonLimpar" name="ButtonLimpar" type="button" class="bottonRightCol2"  value="Limpar"  onclick="limparCamposPesquisa();">
			</fieldset>
		</fieldset>
	</fieldset>
	<fieldset>
		<c:if test="${listaPontoConsumo ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		
		<display:table class="dataTableGGAS" name="listaPontoConsumo" sort="list" id="pontoConsumo" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontoConsumoAcompanhamentoCobranca">
			
			<display:column  style="width: 25px">
		    	<input type="radio" name="idPontoConsumo" id="idPontoConsumo" value="<c:out value="${pontoConsumo.chavePrimaria}"/>" <c:if test="${idPontoConsumo == pontoConsumo.chavePrimaria}">checked="checked"</c:if> onClick="carregarDadosPontosConsumo();" />
		    	
		    </display:column>
			
			<display:column sortable="true" title="C�digo Ponto Consumo" style="width: 110px" sortProperty="fatura.chavePrimaria">
				<c:out value='${pontoConsumo.chavePrimaria}'/>
			</display:column>
			
			<display:column sortable="true" title="Ponto de Consumo" style="width: 110px" sortProperty="avisoCorte.pontoConsumo.descricao">
				<c:out value='${pontoConsumo.descricao}'/>
			</display:column>
	    	
	    </display:table>
		</c:if>
	
	</fieldset>
		
			
	<fieldset>
		<c:if test="${listaServicoAutorizacaoVO ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		
		<display:table class="dataTableGGAS" name="listaServicoAutorizacaoVO" sort="list" id="ServicoAutorizacaoVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAvisoCorte">
			
			<display:column sortable="true" title="Fatura" style="width: 110px" sortProperty="avisoCorte.fatura.chavePrimaria">
				<a href="#"><span class="linkInvisivel"></span>
					<c:out value='${ServicoAutorizacaoVO.avisoCorte.fatura.chavePrimaria}'/>
				</a>
			</display:column>
			
			 <display:column sortable="true" title="Data Notifica��o" style="width: 110px" sortProperty="avisoCorte.dataNotificacao">
			 	<a href="#"><span class="linkInvisivel"></span>
			 		<fmt:formatDate value="${ServicoAutorizacaoVO.avisoCorte.dataNotificacao}" pattern="dd/MM/yyyy"/>
			 	</a>
			 </display:column>
			 
			 <display:column sortable="true" title="Data do Aviso Corte" style="width: 110px" sortProperty="avisoCorte.dataAviso">
				<a href="#"><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.avisoCorte.dataAviso}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			
	    	<display:column sortable="true" title="Data da Autoriza��o do Corte" style="width: 110px" sortProperty="avisoCorte.dataCorte">
				<a href="#"><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.dataAutorizacao}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			
			<display:column sortable="true" title="Data do Corte" style="width: 110px" sortProperty="avisoCorte.dataCorte" >
				<a href="#" title="EQUIPE: ${ServicoAutorizacaoVO.nomeEquipeCorte}&#13;OPERADOR: ${ServicoAutorizacaoVO.nomeOperadorCorte}&#13;PRIORIDADE: ${ServicoAutorizacaoVO.prioridadeCorte}&#13;TIPO DE SERVI�O: ${ServicoAutorizacaoVO.servicoTipoCorte}&#13;STATUS: ${ServicoAutorizacaoVO.statusCorte}"/><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.dataCorte}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			
			<display:column sortable="true" title="Data da Religa��o" style="width: 110px" sortProperty="avisoCorte.dataCorte">
				<a href="#" title="EQUIPE: ${ServicoAutorizacaoVO.nomeEquipeReligacao}&#13;OPERADOR: ${ServicoAutorizacaoVO.nomeOperadorReligacao}&#13;PRIORIDADE: ${ServicoAutorizacaoVO.prioridadeReligacao}&#13;TIPO DE SERVI�O: ${ServicoAutorizacaoVO.servicoTipoReativacao}&#13;STATUS: ${ServicoAutorizacaoVO.statusReligacao}" /><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.dataReativacao}" pattern="dd/MM/yyyy" />
				<a/>
				
			</display:column>
	    </display:table>
	</c:if>
	</fieldset>
	
	<fieldset>
		<c:if test="${listaServicoAutorizacaoVO2 ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		
		<display:table class="dataTableGGAS" name="listaServicoAutorizacaoVO2" sort="list" id="ServicoAutorizacaoVO" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAvisoCortePorImovelCliente">
			
			<display:column sortable="true" title="Fatura" style="width: 110px" sortProperty="avisoCorte.fatura.chavePrimaria">
				<a href="#"><span class="linkInvisivel"></span>
					<c:out value='${ServicoAutorizacaoVO.avisoCorte.fatura.chavePrimaria}'/>
				</a>
			</display:column>
			
			 <display:column sortable="true" title="Data Notifica��o" style="width: 110px" sortProperty="avisoCorte.dataNotificacao">
			 	<a href="#"><span class="linkInvisivel"></span>
			 		<fmt:formatDate value="${ServicoAutorizacaoVO.avisoCorte.dataNotificacao}" pattern="dd/MM/yyyy"/>
			 	</a>
			 </display:column>
			 
			 <display:column sortable="true" title="Data do Aviso Corte" style="width: 110px" sortProperty="avisoCorte.dataAviso">
				<a href="#"><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.avisoCorte.dataAviso}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			
	    	<display:column sortable="true" title="Data da Autoriza��o do Corte" style="width: 110px" sortProperty="avisoCorte.dataCorte">
				<a href="#"><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.dataAutorizacao}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			
			<display:column sortable="true" title="Data do Corte" style="width: 110px" sortProperty="avisoCorte.dataCorte" >
				<a href="#" title="EQUIPE: ${ServicoAutorizacaoVO.nomeEquipeCorte}&#13;OPERADOR: ${ServicoAutorizacaoVO.nomeOperadorCorte}&#13;PRIORIDADE: ${ServicoAutorizacaoVO.prioridadeCorte}&#13;TIPO DE SERVI�O: ${ServicoAutorizacaoVO.servicoTipoCorte}&#13;STATUS: ${ServicoAutorizacaoVO.statusCorte}"/><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.dataCorte}" pattern="dd/MM/yyyy"/>
				</a>
			</display:column>
			
			<display:column sortable="true" title="Data da Religa��o" style="width: 110px" sortProperty="avisoCorte.dataCorte">
				<a href="#" title="EQUIPE: ${ServicoAutorizacaoVO.nomeEquipeReligacao}&#13;OPERADOR: ${ServicoAutorizacaoVO.nomeOperadorReligacao}&#13;PRIORIDADE: ${ServicoAutorizacaoVO.prioridadeReligacao}&#13;TIPO DE SERVI�O: ${ServicoAutorizacaoVO.servicoTipoReativacao}&#13;STATUS: ${ServicoAutorizacaoVO.statusReligacao}" /><span class="linkInvisivel"></span>
					<fmt:formatDate value="${ServicoAutorizacaoVO.dataReativacao}" pattern="dd/MM/yyyy" />
				<a/>
				
			</display:column>
	    </display:table>
	</c:if>
	</fieldset>
</form> 
