<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>


<h1 class="tituloInterno">Detalhar A��o de Cobran�a<a class="linkHelp" href="<help:help>/detalhamentodocadastrodaaodecobrana.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<form method="post" name="acaoCobrancaForm" id="acaoCobrancaForm" action="exibirDetalhamentoAcaoCobranca"> 

<script>

	function voltar() {
		location.href = '<c:url value="/pesquisarAcaoCobranca"/>';
	}

	function alterar(){
		document.forms[0].postBack.value = false;
		submeter("acaoCobrancaForm", "exibirAlteracaoAcaoCobranca");
	}

</script>

<input type="hidden" name="acao" id="acao" value="exibirDetalhamentoAcaoCobranca">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${acaoCobrancaVO.chavePrimaria}"/>

<fieldset id="detalharAcaoCobranca" class="detalhamento">
	<fieldset class="coluna">
		<label class="rotulo">A��o predecessora:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${acaoCobrancaVO.descricaoAcao}"/></span><br />
	    
		<label class="rotulo">Descri��o da a��o:</label>
		<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${acaoCobrancaVO.descricao}"/></span><br />
		
		<label class="rotulo rotulo2Linhas">N�mero de Dias de Validade da a��o:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas">
			<c:if test="${acaoCobrancaVO.numeroDiasValidadeAcao ne 'null'}">
				<c:out value="${acaoCobrancaVO.numeroDiasValidadeAcao}"/>
				<c:if test="${acaoCobrancaVO.descricaoTipoNumeroDiasValidade ne 'null' && acaoCobrancaVO.descricaoTipoNumeroDiasValidade ne ''}">
					<c:out value="(${acaoCobrancaVO.descricaoTipoNumeroDiasValidade})"/>
				</c:if>
			</c:if>
		</span>
		<br />
		
		<label class="rotulo rotulo2Linhas">N�mero de dias para o vencimento:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas">
			<c:if test="${acaoCobrancaVO.numeroDiasVencimento ne 'null'}">
				<c:out value="${acaoCobrancaVO.numeroDiasVencimento}"/>
				<c:if test="${acaoCobrancaVO.descricaoTipoNumeroDiasVencimento ne 'null' && acaoCobrancaVO.descricaoTipoNumeroDiasVencimento ne ''}">
					<c:out value="(${acaoCobrancaVO.descricaoTipoNumeroDiasVencimento})"/>
				</c:if>
			</c:if>
		</span>
		
	</fieldset>
	
	<fieldset class="colunaFinal">
		<label class="rotulo rotulo2Linhas">Tipo do documento<br />a ser gerado:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="${acaoCobrancaVO.descricaoTipoDocumento}"/></span><br />
	   
	    <label class="rotulo">Gerar ordem de servi�o:</label>
		<span class="itemDetalhamento">
			<c:choose> 
				<c:when test="${acaoCobrancaVO.gerarOrdemServico}">
					Sim
				</c:when>
				<c:otherwise>
					N�o
				</c:otherwise>
			</c:choose>
		</span><br />
		
		<label class="rotulo" for="consideraDebitosVencer">Considera d�bitos a vencer:</label>
		<span class="itemDetalhamento">
			<c:choose> 
				<c:when test="${acaoCobrancaVO.consideraDebitosVencer}">
					Sim
				</c:when>
				<c:otherwise>
					N�o
				</c:otherwise>
			</c:choose>
		</span><br />
		
		<label class="rotulo rotulo2Linhas" for="bloquearEmissaoDocumentosPagaveis">Bloquear emiss�o de documentos pag�veis:</label>
		<span class="itemDetalhamento itemDetalhamento2Linhas">
			<c:choose> 
				<c:when test="${acaoCobrancaVO.bloquearEmissaoDocumentosPagaveis}">
					Sim
				</c:when>
				<c:otherwise>
					N�o
				</c:otherwise>
			</c:choose>
		</span>
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Voltar" type="button" onclick="voltar();">
    
    <vacess:vacess param="exibirAlteracaoAcaoCobranca">
    	<c:if test="${locked == false}"><input name="Button" class="bottonRightCol2 botaoGrande1" value="Alterar" type="button" onclick="alterar();"></c:if>
    </vacess:vacess>    
</fieldset>

</form> 